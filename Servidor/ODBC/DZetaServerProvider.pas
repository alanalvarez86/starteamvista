unit DZetaServerProvider;
interface

{$define NEWCOMPARTE}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Db, Provider, DBClient, ODSI, OCL, OCI,
     ZGlobalTress,
     ZToolsPE,
     {$ifndef VER130}Variants,{$endif}
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaRegistryServer,
     ZetaServerDataSet;

{$define QUINCENALES}
{.$undefine QUINCENALES}
     
type
  {$ifdef VER130}
  TzProviderClientDataset = TClientDataset;
  {$else}
  TzProviderClientDataset = TCustomClientDataset;
  {$endif}
  TzProviderDataEvent = procedure(Sender: TObject; DataSet: TzProviderClientDataset) of object;
  TzBeforeUpdateRecordEvent = procedure(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean) of object;
  TzAfterUpdateRecordEvent = procedure(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataset; UpdateKind: TUpdateKind) of object;
  TzResolverErrorEvent = procedure(Sender: TObject; DataSet: TzProviderClientDataset; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse) of object;
type
  TQREvResultType = (resInt, resDouble, resString, resBool, resError);
  {.$IFDEF RETROACTIVO}
  eTipoEvaluacion = (teNormal, teRetroactiva);
  {.$ENDIF}


  TQREvResult = record
    resFecha : Boolean;
    case Kind : TQREvResultType of
      resInt    : (intResult : longint);
      resDouble : (dblResult : double);
      resString : (strResult : string[255]);
      resBool   : (booResult : boolean);
  end;
type
    TZetaCursor = TDataset;
    TZetaCursorLocate=TDataset;
    TZetaField  = TField;
    TZetaCursorParam  = TParam;
    TZetaComando = TOEQuery;
    TParameter = TParam;
type
  TZPUpdate = TOEQuery;
  eTipoQuery = (etqSelect, etqInsert, etqUpdate, etqDelete );
  EAdvertencia = class( Exception ); // Excepciones que se deben manejar como advertencias //
  TBitacoraHora = String[ 8 ];
  TBitacoraTexto = String[ 50 ];
  TLogStatus = String[ 1 ];
  TdmZetaServerProvider = class;
  TZetaLog = class( TObject )
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FMaxSteps: Integer;
    FIncrement: Integer;
    FSteps: Integer;
    FCounter: Integer;
    FCancelado: Boolean;
    FErrorCount: Word;
    FEventCount: Word;
    FWarningCount: Word;
    FFolio: Integer;
    FInicio: TDateTime;
    FEmpleado: TNumEmp;
    FProceso: Procesos;
    FDataset: TDataset;
    FUpdate: TDataset;
    function Abierto: Boolean;
    procedure Init;
    procedure Conteo( const eTipo: eTipoBitacora );
    procedure Escribir( const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
    function GetStatus: TLogStatus;
    function GetEnumStatus: eProcessStatus;
  public
    { Public declarations }
    property Folio: integer read FFolio;
    constructor Create( oProvider: TdmZetaServerProvider );
    function CanContinue( const iEmpleado: TNumEmp; const lTx: Boolean ): Boolean;
    function CloseProcess: OleVariant;
    function OpenProcess( const eProceso: Procesos; const iMaxSteps: Integer; const sParametros: string = VACIO; const sFiltro: string = VACIO; const sFormula: string = VACIO ): Boolean;
    procedure Cambio( const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; const sCampoEmpleado, sCampoFecha: String; Dataset: TDataSet );
    procedure CancelProcess( const iFolio: Integer );
    procedure Evento( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto ); overload;
    procedure Evento( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String ); overload;
    procedure Advertencia( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto ); overload;
    procedure Advertencia( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: String ); overload;
    procedure Advertencia( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String ); overload;
    procedure Error( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: String );
    procedure ErrorGrave( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto );
    procedure Excepcion( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception ); overload;
    procedure Excepcion( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception; const sTexto: String ); overload;
    function HayErrores: Boolean;
    procedure HuboRollBack;
    procedure PreparaQuerys;
    procedure BorraQuerys;
  end;
  TTablaInfo = class(TObject)
  private
    FTabla : String;
    FListaCampos : TStringList;
    FListaKey : TStringList;
    FListaOrder : TStringList;
    FListaForeignKey : TStringList;
    FSQL : TStrings;
    FParams : TParams;
    FFiltro : String;
    FOnUpdateData : {$ifdef VER130}TProviderDataEvent;{$else}TzProviderDataEvent;{$endif}
    FBeforeUpdateRecord : {$ifdef VER130}TBeforeUpdateRecordEvent;{$else}TzBeforeUpdateRecordEvent;{$endif}
    FAfterUpdateRecord : {$ifdef VER130}TAfterUpdateRecordEvent;{$else}TzAfterUpdateRecordEvent;{$endif}
    FOnUpdateError : {$ifdef VER130}TResolverErrorEvent;{$else}TzResolverErrorEvent;{$endif}
    function  ConstruyeQuery( const eTipo : eTipoQuery; Delta : TDataset; qryCambios : TOEQuery ) : String;
    function  ConstruyeSelectSQL : String;
    function  ConstruyeUpdateSQL( const UpdateKind : TUpdateKind; Delta : TDataset; qryCambios : TOEQuery ) : String;
  public
    constructor Create;
    destructor  Destroy; override;
    procedure SetCampos( const sCampos : String );
    procedure SetKey( const sCampos : String );
    procedure SetOrder( const sCampos : String );
    procedure SetForeignKey( const sCampos : String );
    procedure SetInfo( const sTabla, sCampos, sKey : String );
    procedure SetInfoDetail( const sTabla, sCampos, sKey, sForeignKey : String );
    procedure SetMasterFields( Detalle : TOEQuery );
    property  Filtro : String read FFiltro write FFiltro;
    property  OnUpdateData : {$ifdef VER130}TProviderDataEvent{$else}TzProviderDataEvent{$endif}  read FOnUpdateData write FOnUpdateData;
    property  BeforeUpdateRecord : {$ifdef VER130}TBeforeUpdateRecordEvent{$else}TzBeforeUpdateRecordEvent{$endif} read FBeforeUpdateRecord write FBeforeUpdateRecord;
    property  AfterUpdateRecord : {$ifdef VER130}TAfterUpdateRecordEvent{$else}TzAfterUpdateRecordEvent{$endif} read FAfterUpdateRecord write FAfterUpdateRecord;
    property  OnUpdateError : {$ifdef VER130}TResolverErrorEvent{$else}TzResolverErrorEvent{$endif} read FOnUpdateError write FOnUpdateError;
    property  SQL : TStrings read FSQL;
  end;
  TdmZetaServerProvider = class(TDataModule)
    prvUnico: TDataSetProvider;
    HdbcEmpresa: THdbc;
    HdbcComparte: THdbc;
    qryMaster: TOEQuery;
    dsMaster: TDataSource;
    qryDetail: TOEQuery;
    qryCambios: TOEQuery;
    procedure dmZetaProviderCreate(Sender: TObject);
    procedure dmZetaProviderDestroy(Sender: TObject);
    {$ifdef VER130}
    procedure prvUnicoUpdateData(Sender: TObject; DataSet: TClientDataset);
    procedure prvUnicoBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure prvUnicoGetData(Sender: TObject; DataSet: TClientDataset);
    procedure prvUnicoAfterUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataset; UpdateKind: TUpdateKind);
    procedure prvUnicoUpdateError(Sender: TObject; DataSet: TClientDataset; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
    {$else}
    procedure prvUnicoUpdateData(Sender: TObject; DataSet: TCustomClientDataset);
    procedure prvUnicoBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure prvUnicoGetData(Sender: TObject; DataSet: TCustomClientDataset);
    procedure prvUnicoAfterUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset; UpdateKind: TUpdateKind);
    procedure prvUnicoUpdateError(Sender: TObject; DataSet: TCustomClientDataset; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
    {$endif}
    procedure qryDetailBeforeOpen(DataSet: TDataSet);
    procedure HdbcEmpresaBeforeConnect(Sender: TObject);
    function  GetCodigoGrupo: integer;
  private
    { Private declarations }
    FTablaInfo : TTablaInfo;
    FDetailInfo : TTablaInfo;
    {$ifdef NEWCOMPARTE}
    FComparte: Variant;
    FComparteAlias: String;
    FRegistryOpened: Boolean;
    {$else}
    FRegistry: TZetaRegistryServer;
    {$endif}
    FRecsOut : Integer;
    FLog: TZetaLog;
    FQueryGlobal : TOEQuery;
    FEmpresa : Variant;
    FConnection : THdbc;
    FParamList : TZetaParams;
    //oQryInsert : TDataSet;
    FQryBitacora : TDataSet;
    FQryBitacoraSistema : TDataSet;
    FQryBitacoraKiosco: TDataSet;
    { Valores Activos }
    FDatosPeriodo: TDatosPeriodo;
    FPeriodoObtenido: Boolean;
    FDatosImss: TDatosImss;
    FYearDefault: Integer;
    FMesDefault: Integer;
    FEmpleadoActivo: Integer;
    FNombreUsuario, FCodigoEmpresa: String;
    FFechaDefault: TDate;
    FFechaAsistencia: TDate;
    FVerConfidencial: Boolean;
    FNumeroNiveles : Integer;
    FNombreNiveles : array[ 1..{$ifdef ACS}12{$else}9{$endif} ] of String;{ACSL: No se cambio a la constante global K_GLOBAL_NIVEL_MAX ya que esta no es encuentra en todos los ZGlobalTress}
    FChecadaComida: TDatosComida;
    FDatosCalendario: TCalendario;
    FDatosPrestamo : TDatosPrestamo;
    FDatosNOMM: TDatosNomm;
    FUltimaFechaBalanza: TDate;     
    FEmpleadosConAjuste : TObject;

    {Valores por Nomina de la Funci�n  V_NOM_INFO}
    FCdsPeriodosNomInfo : TClientDataSet;
    FDatosNomInfo : TDatosNomInfo;
    {.$IFDEF RETROACTIVO}
    FEvaluacionTipo: eTipoEvaluacion;
    {.$ENDIF}
    FOnUpdateData : {$ifdef VER130}TProviderDataEvent;{$else}TzProviderDataEvent;{$endif}
    FBeforeUpdateRecord : {$ifdef VER130}TBeforeUpdateRecordEvent;{$else}TzBeforeUpdateRecordEvent;{$endif}
    FAfterUpdateRecord : {$ifdef VER130}TAfterUpdateRecordEvent;{$else}TzAfterUpdateRecordEvent;{$endif}
    FOnUpdateError : {$ifdef VER130}TResolverErrorEvent;{$else}TzResolverErrorEvent;{$endif}
    function ConectaBaseDeDatos( const Empresa: Variant ): THdbc;
    function GetParametroPos(Dataset: TOEQuery;const iPos : Integer): TParameter;
    procedure CreateLog;
    procedure ReleaseLog;
    procedure PreparaDetail;
    procedure PreparaTabla;
    { Grabar Tablas con Joins }
    {$ifdef VER130}
    procedure UpdateTabla(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
    {$else}
    procedure UpdateTabla(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
    {$endif}
    procedure ConectaEmpresa( const Empresa: Variant );
    { Globales }
    function LeeGlobal( const iCodigo : Integer ) : String;
    { Empresas }
    function GetComparte: Variant;
    procedure SetEmpresa( Empresa : Variant );
    function GetUsuario: Integer;
    function GetCodigoEmpresaActiva: String;
    function GetApplicationID: Integer;
    {$ifdef NEWCOMPARTE}
    procedure RegistryInit;
    procedure RegistryRead;
    {$endif}
    procedure EscribeParametrosBitacora( oQryInsert: TDataSet; const iTipo: Integer;
                const iClase: Integer; const iEmpleado: TNumEmp;
                dMovimiento: TDate; const sMensaje: TBitacoraTexto;
                const sTexto: String; const iUsuario: Integer );

  public
    { Public declarations }
    function GetDatasetInfo(Dataset: TDataset; const lBorrar : Boolean; sConfidenciales: String = VACIO ): String;
    function GetInsertScript( const sTabla, sExcluidos: String; Source: TZetaCursor): String;
    function GetMasterDetail( Empresa : Variant ) : OleVariant;
    function GetTabla( Empresa : Variant ) : OleVariant;
    function GrabaMasterDetail( Empresa : Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    function GrabaTabla( Empresa : Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    function GrabaTablaGrid( Empresa : Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    procedure ExecSQL( Empresa : Variant; const sSQL : String );
    function  OpenSQL( Empresa : Variant; const sSQL : String; const lDatos : Boolean ) : OleVariant; overload;
    function  OpenSQL( Empresa : Variant; const sSQL : String; const nRecords : Integer ) : OleVariant; overload;
    { Master y Detail }
    property TablaInfo : TTablaInfo read FTablaInfo;
    property DetailInfo : TTablaInfo read FDetailInfo;
    property RecsOut : Integer read FRecsOut;
    { Bitacora }
    property Log: TZetaLog read FLog;
    function OpenProcess( const eProceso: Procesos; const iMaxSteps: Integer; sParametros: string = VACIO; sFormula: string = VACIO ): Boolean;
    function CanContinue: Boolean; overload;
    function CanContinue( const iEmpleado: TNumEmp ): Boolean; overload;
    function CanContinue( const iEmpleado: TNumEmp; const lTx: Boolean ): Boolean; overload;
    function CloseProcess: OleVariant;
    procedure AbreBitacora;
    procedure CancelProcess( const iFolio: Integer );
    { F�brica de Querys }
    function  CreateQuery : TDataset; overload;
    function  CreateQuery( const sScript: String ) : TDataset; overload;
    function  CreateStoredProc( const sProcName: String ) : TDataset;
    function  CreateQueryLocate: TDataset;
    procedure PreparaQuery( Query: TDataSet; sScript: String );
    procedure PreparaStoredProc( Query : TDataSet; const sProcName: String );
    function  Ejecuta(Dataset: TDataset) : Integer;
    function  EjecutaAndFree(const sSQL: String): Integer;
    function  AbreQueryScript( Dataset : TDataset; const sScript: String ) : Boolean;
    procedure ParamAsInteger( Dataset: TDataset; const sName: String; const iValue: Integer );
    procedure ParamAsFloat( Dataset: TDataset; const sName: String; const rValue: Extended );
    procedure ParamAsDate( Dataset: TDataset; const sName: String; const dValue: TDate );
    procedure ParamAsString( Dataset: TDataset; const sName, sValue: String );
    procedure ParamAsVarChar( Dataset: TDataset; const sName, sValue: String; const iAncho: Integer );
    procedure ParamAsChar(Dataset: TDataset; const sName, sValue: String; const iAncho: Integer );
    procedure ParamAsBlob( Dataset: TDataset; const sName: String; const sValue: String);
    procedure ParamAsBoolean( Dataset: TDataset; const sName: String; const lValue: Boolean );
    procedure ParamVariant( Dataset: TDataset; const iPos: Integer; const Valor: Variant );
    procedure ParamAsNull( Dataset: TDataset; const sName: String );
    procedure ParamAsGUID( Dataset: TDataset; const sName: String; const Valor: TGUID );
    procedure ParamAsDateTime( Dataset: TDataset; const sName: String; const dValue: TDateTime );
    function GetParametro(Dataset: TDataset; const sName: String): TParameter;
    procedure ParamSalida( Dataset: TDataset; const sName: String );
    // function  Params( DataSet : TDataset ) : TParams;
    { GetGlobales }
    procedure InitGlobales;
    function  GetGlobalBooleano( const iCodigo: Integer ): Boolean;
    function  GetGlobalString( const iCodigo: Integer ): String;
    function  GetGlobalInteger( const iCodigo: Integer ): Integer;
    function  GetGlobalReal( const iCodigo: Integer ): Real;
    function  GetGlobalDate( const iCodigo: Integer ): TDateTime;
    function  NombreNivel( const Index: Integer ): String;
    function  NumNiveles: Integer;
    function  OK_ProyectoEspecial( const eProgEspecial: eProyectoEspecial ): Boolean;

    { Empresas }
    property Comparte: Variant read GetComparte;
    property EmpresaActiva : Variant read FEmpresa write SetEmpresa;
    property UsuarioActivo : Integer read GetUsuario;
    property CodigoEmpresaActiva : String read GetCodigoEmpresaActiva;
    property ParamList : TZetaParams read FParamList;
    property CodigoGrupo: integer read GetCodigoGrupo;
    property ApplicationID: integer read GetApplicationID;
    function AliasComparte : String;
    procedure Activate;
    procedure Deactivate;
    procedure AsignaDataSetParams( oDataSet : TDataset );
    procedure AsignaDataSetOtros( oDataSet : TDataset; oListaPares : Variant );
    procedure AsignaParamsDataSet( Origen, Destino: TDataset; const sExcluidos: String = '' );
    { Valores Activos }
    property DatosPeriodo : TDatosPeriodo read FDatosPeriodo;
    property DatosImss : TDatosImss read FDatosImss;
    property DatosComida : TDatosComida read FChecadaComida write FChecadaComida;
    property DatosPrestamo : TDatosPrestamo read FDatosPrestamo write FDatosPrestamo;
    property DatosCalendario : TCalendario read FDatosCalendario;
    property DatosNomInfo : TDatosNomInfo read FDatosNomInfo write FDatosNomInfo;

    property DatosNOMM : TDatosNomm read FDatosNOMM write FDatosNOMM;
    property FechaAsistencia : TDate read FFechaAsistencia;
    property FechaDefault : TDate read FFechaDefault;
    property YearDefault : Integer read FYearDefault write FYearDefault;
    property MesDefault : Integer read FMesDefault write FMesDefault;
    property EmpleadoActivo : Integer read FEmpleadoActivo;
    property NombreUsuario : String read FNombreUsuario;
    property CodigoEmpresa : String read FCodigoEmpresa;
    property VerConfidencial : Boolean read FVerConfidencial write FVerConfidencial;
    {.$IFDEF RETROACTIVO}
    property EvaluacionTipo: eTipoEvaluacion read FEvaluacionTipo write FEvaluacionTipo default teNormal;
    {.$ENDIF}

    property UltimaFechaBalanza: TDate read FUltimaFechaBalanza write FUltimaFechaBalanza;

    {Bloque de propiedades para optimizacion de V_NOM_INFO}
    property PeriodosNomInfo : TClientDataSet read FcdsPeriodosNomInfo write FcdsPeriodosNomInfo;
    property EmpleadosConAjuste : TObject read FEmpleadosConAjuste write FEmpleadosConAjuste;
    procedure InicializaValoresNomINFO;
    procedure LiberaEmpleadosConAjuste;

    procedure GetDatosPeriodo;
    procedure GetDatosImss;
    procedure GetDatosActivos;
    procedure GetEncabPeriodo;

    procedure InicializaValoresActivos;
    procedure AsignaParamList( oParams : OleVariant );
    function  DescripcionParams : String;

    procedure EmpiezaTransaccion;
    procedure TerminaTransaccion( lCommit : Boolean );
    procedure RollBackTransaccion;

    procedure ActualizaTabla(OrigenDS: TDataSet; UpdateKind: TUpdateKind);
    procedure CambioCatalogo( const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; Dataset: TDataSet; const iEmpleado: integer = 0; const sMasInfo : string = VACIO );
    procedure BorraCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; Dataset: TDataSet; const iEmpleado: integer = 0; const sMasInfo : string = VACIO );
    procedure EscribeBitacora(const eTipo: eTipoBitacora;
                const eClase: eClaseBitacora; const iEmpleado: TNumEmp;
                dMovimiento: TDate; const sMensaje: TBitacoraTexto;
                const sTexto: String);
    property  OnUpdateData : {$ifdef VER130}TProviderDataEvent{$else}TzProviderDataEvent{$endif}  read FOnUpdateData write FOnUpdateData;
    property  BeforeUpdateRecord : {$ifdef VER130}TBeforeUpdateRecordEvent{$else}TzBeforeUpdateRecordEvent{$endif} read FBeforeUpdateRecord write FBeforeUpdateRecord;
    property  AfterUpdateRecord : {$ifdef VER130}TAfterUpdateRecordEvent{$else}TzAfterUpdateRecordEvent{$endif} read FAfterUpdateRecord write FAfterUpdateRecord;
    property  OnUpdateError : {$ifdef VER130}TResolverErrorEvent{$else}TzResolverErrorEvent{$endif} read FOnUpdateError write FOnUpdateError;
    procedure CambioCatalogoComparte( const sMensaje: TBitacoraTexto; const eClase: eClaseSistBitacora; Dataset: TDataSet; const iUsuario: Integer );
    procedure BorraCatalogoComparte(const sMensaje: TBitacoraTexto; const eClase: eClaseSistBitacora; Dataset: TDataSet; const iUsuario: Integer );
    procedure EscribeBitacoraComparte(const eTipo: eTipoBitacora;
                const eClase: eClaseSistBitacora; dMovimiento: TDate; const sMensaje: TBitacoraTexto;
                const sTexto: String; const iUsuario: Integer);

    procedure EscribeBitacoraKiosco(const eTipo: eTipoBitacora; const sEmpresa: String;
                                                             const eClase: eClaseBitacora; const sTexto, sMarco, sKiosco: String; const iEmpleado: Integer);

    { Arreglos variables de aTipoPeriodo y aTipoNomina } 
    function GetTiposPeriodo( Empresa : Variant ) : OleVariant; //acl 
    procedure InitArregloTPeriodo;  //ap
    procedure SetReadUncommited;
  end;

function PK_Violation( E : Exception ) : Boolean;
function GetEmptyProcessResult( eProceso: Procesos ): OleVariant;
function GetEmptyProcessResultFecha( eProceso: Procesos ): OleVariant;
procedure SumaProcessResult( var Result:Olevariant; Process1, Process2: OleVariant );
procedure SetProcessOK( var Resultado: OleVariant );
procedure SetProcessError( var Resultado: OleVariant );
function  CursorDelCampo( oCampo : TZetaField ) : TZetaCursor;
function TipoCampo( oCampo : TField ) : eTipoGlobal;
function  AsignaResultado( oCampo: TField): TQREvResult;

function GetZetaProvider( AOwner: TComponent ): TdmZetaServerProvider;
procedure FreeZetaProvider( oProvider: TdmZetaServerProvider );
{$ifdef DOS_CAPAS}
procedure FreeAll;
procedure InitAll;
{$endif}

implementation

uses ZetaCommonTools,
     ZetaServerTools;

const
     K_ANCHO_LOG_HORA = 8;
     K_ANCHO_LOG_TEXTO = 50;
     K_WRITE_SIMPLE_LOG = 'insert into BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_TIPO, BI_CLASE, BI_TEXTO, CB_CODIGO, BI_FEC_MOV, BI_DATA ) values '+
                          '( :US_CODIGO, :BI_FECHA, :BI_HORA, :BI_TIPO, :BI_CLASE, :BI_TEXTO, :CB_CODIGO, :BI_FEC_MOV, :BI_DATA )';

     K_AGREGA_BITACORA_KIOSCO = 'execute dbo.SP_AGREGA_BITACORA_KIOSCO :CB_CODIGO, :CM_CODIGO, :BI_TIPO, :BI_ACCION, :BI_NUMERO, :BI_TEXTO, ' +
                                ':BI_UBICA, :BI_KIOSCO, :BI_FEC_MOV ';

{$ifdef DOS_CAPAS}
var
   oZetaProvider: TdmZetaServerProvider;
{$endif}

{$R *.DFM}

function GetZetaProvider( AOwner: TComponent ): TdmZetaServerProvider;
begin
{$ifdef DOS_CAPAS}
     Result := oZetaProvider;
{$else}
     Result := TdmZetaServerProvider.Create( AOwner );
{$endif}
end;

procedure FreeZetaProvider( oProvider: TdmZetaServerProvider );
begin
{$ifndef DOS_CAPAS}
     oProvider.Free;
{$endif}
end;

{$ifdef DOS_CAPAS}
procedure InitAll;
begin
     oZetaProvider := TdmZetaServerProvider.Create( nil );
end;

procedure FreeAll;
begin
     FreeAndNil( oZetaProvider );
end;

{$endif}

function TipoCampo( oCampo : TField ) : eTipoGlobal;
begin
    case oCampo.DataType of
         ftString,ftMemo,ftBlob,ftFmtMemo: Result := tgTexto;
         ftSmallint, ftInteger, ftWord: Result := tgNumero;
         ftBoolean: Result := tgBooleano;
         ftFloat, ftCurrency, ftBCD, ftTime : Result := tgFloat;
         ftDate,ftDateTime: Result := tgFecha;
         else Result := tgAutomatico;
    end;
end;

function AsignaResultado( oCampo : TField ) : TQREvResult;
begin
     with oCampo, Result do
     begin
          case TipoCampo( oCampo ) of
               tgTexto:
               begin
                    Kind := resString;
                    strResult := AsString;
               end;
               tgNumero:
               begin
                    Kind := resInt;
                    intResult := AsInteger;
               end;
               tgBooleano:
               begin
                    Kind := resBool;
                    booResult := AsBoolean;
               end;

               tgFloat :
               begin
                    Kind := resDouble;
                    resFecha  := FALSE;
                    dblResult := AsFloat;
               end;
               tgFecha:
               begin
                    Kind := resDouble;
                    resFecha := TRUE;
                    dblResult := AsFloat;
               end
          else
              Kind := resError;
          end;
     end;
end;

function  CursorDelCampo( oCampo : TZetaField ) : TZetaCursor;
begin
    Result := oCampo.DataSet;
end;

function TimeToStrSQL( const dValue: TDateTime ): String;
begin
     Result := FormatDateTime( 'hh:nn:ss', dValue );
end;


function PK_Violation( E : Exception ) : Boolean;
begin
    Result := (  Pos( 'VIOLATION OF PRIMARY', UpperCase( E.Message ) ) > 0 );
end;

procedure AsignaParamCampo(oParam: TParameter; oCampo: TField);
begin
     with oParam do
     begin
       Assign( oCampo );
{ ODBCExpress
       if DataType = ftBlob then
       begin
            DataType := ftString;
            Value    := oCampo.AsString;
       end
       else if DataType = ftString then
       begin
            DataType := ftFixedChar;
            Value    := oCampo.AsString;
       end;
}
     end;
end;
{
function GetExceptionInfo( Error: Exception ): String;
begin
     Result := GetExceptionMsg( Error.Message );
end;
}
function GetExceptionLogType( Problema: Exception ): eTipoBitacora;
begin
     if ( Problema is EAdvertencia ) then
        Result := tbAdvertencia
     else
         Result := tbErrorGrave;
end;

function GetEmptyProcessResult( eProceso: Procesos ): OleVariant;
begin
     Result := VarArrayOf( [ 0, Ord( epsEjecutando ), Ord( eProceso ), 0, 0, 0, Now, Now, 0, 0, 0 ] );
end;

function GetEmptyProcessResultFecha( eProceso: Procesos ): OleVariant;
begin
     Result := GetEmptyProcessResult(eProceso);
     Result[ K_PROCESO_INICIO ] := NOW+9999; //Esto es para que al sumarse varios procesos, se queda la primer fecha.
end;

procedure SumaProcessResult( var Result:OleVariant ;Process1, Process2: OleVariant );
begin
     //Este codigo es para poder reportar solamente una vez que ya termino el proceso
     Result[ K_PROCESO_STATUS ] := iMax( Ord(eProcessStatus(Process1[ K_PROCESO_STATUS ])) ,
                                         Ord(eProcessStatus(Process2[ K_PROCESO_STATUS ])) );

     Result[ K_PROCESO_MAXIMO ] := Process1[ K_PROCESO_MAXIMO ] + Process2[ K_PROCESO_MAXIMO ];
     Result[ K_PROCESO_PROCESADOS ] := Process1[ K_PROCESO_PROCESADOS ] + Process2[ K_PROCESO_PROCESADOS ];
     Result[ K_PROCESO_ULTIMO_EMPLEADO ] := iMax( Process1[ K_PROCESO_ULTIMO_EMPLEADO ],
                                                  Process2[ K_PROCESO_ULTIMO_EMPLEADO ] );

     Result[ K_PROCESO_INICIO ] := rMin( Process1[ K_PROCESO_INICIO ] , Process2[ K_PROCESO_INICIO ]);
     Result[ K_PROCESO_FIN ]    := rMax( Process1[ K_PROCESO_FIN ] , Process2[ K_PROCESO_FIN ] );

     Result[ K_PROCESO_ERRORES ] := Process1[ K_PROCESO_ERRORES ] + Process2[ K_PROCESO_ERRORES ];
     Result[ K_PROCESO_ADVERTENCIAS ] := Process1[ K_PROCESO_ADVERTENCIAS ] + Process2[ K_PROCESO_ADVERTENCIAS ];
     Result[ K_PROCESO_EVENTOS ] := Process1[ K_PROCESO_EVENTOS ] + Process2[ K_PROCESO_EVENTOS ];
end;


procedure SetProcessOK( var Resultado: OleVariant );
begin
     Resultado[ K_PROCESO_STATUS ] := Ord( epsOK );
     Resultado[ K_PROCESO_FIN ] := Now;
end;

procedure SetProcessError( var Resultado: OleVariant );
begin
     Resultado[ K_PROCESO_STATUS ] := Ord( epsError );
     Resultado[ K_PROCESO_FIN ] := Now;
end;

{ ****************** TZetaLog ******************* }

constructor TZetaLog.Create( oProvider: TdmZetaServerProvider );
begin
     oZetaProvider := oProvider;
end;

procedure TZetaLog.Init;
begin
     FMaxSteps := 0;
     FIncrement := 0;
     FSteps := 1;
     FCounter := 0;
     FCancelado := False;
     FFolio := 0;
     FInicio := NullDateTime;
     FEmpleado := 0;
     FErrorCount := 0;
     FEventCount := 0;
     FWarningCount := 0;
end;

procedure TZetaLog.Conteo( const eTipo: eTipoBitacora );
begin
     case eTipo of
          tbNormal: Inc( FEventCount );
          tbAdvertencia: Inc( FWarningCount );
          tbError: Inc( FErrorCount );
          tbErrorGrave: Inc( FErrorCount );
     end;
end;

function TZetaLog.Abierto: Boolean;
begin
     Result := ( FFolio <> 0 );
end;

function TZetaLog.HayErrores: Boolean;
begin
     Result := ( FErrorCount > 0 );
end;

function TZetaLog.GetStatus: TLogStatus;
begin
     if FCancelado then
        Result := B_PROCESO_CANCELADO
     else
         Result := ZetaCommonTools.zBoolToStr( HayErrores );
end;

function TZetaLog.GetEnumStatus: eProcessStatus;
var
   cStatus: TLogStatus;
begin
     cStatus := GetStatus;
     if ( cStatus = B_PROCESO_CANCELADO ) then
        Result := epsCancelado
     else
         if ( cStatus = B_PROCESO_OK ) then
            Result := epsOK
         else
             if ( cStatus = B_PROCESO_ERROR ) then
                Result := epsError
             else
                 if ( cStatus = B_PROCESO_ABIERTO ) then
                    Result := epsError
                 else
                     Result := epsCatastrofico;
end;

procedure TZetaLog.BorraQuerys;
begin
    with oZetaProvider do
    begin
        FreeAndNil( FDataset );
        FreeAndNil( FUpdate );
    end;
end;

procedure TZetaLog.PreparaQuerys;
const
     K_WRITE_LOG = 'insert into BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_PROC_ID, BI_TIPO, BI_CLASE, BI_NUMERO, BI_TEXTO, CB_CODIGO, BI_FEC_MOV, BI_DATA ) values '+
                   '( :US_CODIGO, :BI_FECHA, :BI_HORA, :BI_PROC_ID, :BI_TIPO, :BI_CLASE, :BI_NUMERO, :BI_TEXTO, :CB_CODIGO, :BI_FEC_MOV, :BI_DATA )';
     K_UPDATE_PROCESS = 'select STATUS from UPDATE_PROCESS_LOG( %d, :Empleado, :Paso, :Fecha, :Hora )';
begin
    with oZetaProvider do
    begin
      FDataset := CreateQuery( K_WRITE_LOG );  { Prepara Query Para Escribir Eventos }
      ParamAsInteger( FDataset, 'US_CODIGO', EmpresaActiva[ P_USUARIO ] );
      ParamAsInteger( FDataset, 'BI_PROC_ID', Ord( FProceso ) );
      ParamAsInteger( FDataset, 'BI_NUMERO', FFolio );
{$ifdef INTERBASE }
      FUpdate := CreateQuery( Format( K_UPDATE_PROCESS, [ FFolio ] ) ); { Prepara Query Para Actualizar Status del Proceso }
{$else}
      FUpdate := CreateStoredProc( 'UPDATE_PROCESS_LOG' );
      ParamAsInteger( FUpdate, '@Proceso', FFolio );
{$endif}
    end;
end;

function TZetaLog.OpenProcess( const eProceso: Procesos; const iMaxSteps: Integer; const sParametros: string = VACIO; const sFiltro: string = VACIO; const sFormula: string = VACIO ): Boolean;
const
{$ifdef INTERBASE}
     K_INIT_SQL = 'select FOLIO from INIT_PROCESS_LOG( :Proceso, :Usuario, :Fecha, :Hora, :Maximo, :Param )';
{$else}
     K_INIT_SQL = '{CALL INIT_PROCESS_LOG( :Proceso, :Usuario, :Fecha, :Hora, :Maximo, :Param, :Folio )}';
{$endif}
var
   iUsuario: Integer;
   oQuery: TDataSet;
begin
     Init;
     FInicio := Now;
     FProceso := eProceso;
     with oZetaProvider do
     begin
          iUsuario := EmpresaActiva[ P_USUARIO ];
          oQuery := CreateQuery( K_INIT_SQL );
          try
             with oQuery do
             begin
                  EmpiezaTransaccion;
                  try
                     ParamAsInteger( oQuery, 'Proceso', Ord( FProceso ));
                     ParamAsInteger( oQuery, 'Usuario', iUsuario );
                     ParamAsDate( oQuery, 'Fecha', Trunc( FInicio ));
                     ParamAsString( oQuery, 'Hora', TimeToStrSQL( FInicio ));
                     ParamAsInteger( oQuery, 'Maximo', iMaxSteps );
                     ParamAsVarChar( oQuery, 'Param', sParametros, ZetaCommonClasses.K_MAX_VARCHAR );

   {$IFDEF INTERBASE}
                     Active := TRUE;
                     Result := NOT EOF;
   {$ENDIF}
   {$IFDEF MSSQL}
                     ParamSalida( oQuery, 'Folio' );
                     Ejecuta( oQuery );
                     Result := TRUE;
   {$ENDIF}
                     TerminaTransaccion( True );
                  except
                        on Error: Exception do
                        begin
                             TerminaTransaccion( False );
                             Result := False;
                        end;
                  end;
                  if Result then
                  begin
   {$ifdef INTERBASE}
                       FFolio := FieldByName( 'FOLIO' ).AsInteger;
   {$else}
   //                    FFolio := GetParametro( oQuery, '@Folio' ).AsInteger;
                       FFolio := GetParametro( oQuery, 'Folio' ).AsInteger;
   {$endif}
                       FMaxSteps := iMaxSteps;
                       FIncrement := ZetaCommonTools.iMin( 25, ZetaCommonTools.iMax( 1, Trunc( 0.1 * FMaxSteps ) ) ); { PENDIENTE: 25 y 10% deben ser configurables }
                       PreparaQuerys;
                  end
                  else
                      DataBaseError( 'Error al Crear Bit�cora del Proceso' );
                  Active := False;
                  //Evento que se Genera cuando se pasa de 255 caracteres de longitud
                  if strLleno( sFiltro ) and strLleno( sFormula ) then
                     Evento( clbNinguno, 0, FInicio, 'F�rmulas y Filtros', sFormula + CR_LF + sFiltro )
                  else
                  begin
                       if strLleno( sFiltro ) then
                          Evento( clbNinguno, 0, FInicio, 'Filtros', sFiltro )
                       else
                       begin
                            if strLleno( sFormula ) then
                               Evento( clbNinguno, 0, FInicio, 'F�rmulas', sFormula );
                       end;
                  end;
             end;
          finally
             // Probando
             TOEQuery( oQuery ).hDBC := NIL;
             FreeAndNil( oQuery );
          end;
     end;
end;

procedure TZetaLog.HuboRollBack;
begin
    BorraQuerys;
    PreparaQuerys;
end;

function TZetaLog.CanContinue( const iEmpleado: TNumEmp; const lTx: Boolean ): Boolean;
const
     K_ANCHO_HORA = 8;
var
   dValue: TDateTime;
begin
     FCounter := FCounter + 1;
     FSteps := FSteps + 1;
     FEmpleado := iEmpleado;
     if FCancelado then
        Result := False
     else
         if ( FSteps > FIncrement ) then
         begin
              dValue := Now;
              with oZetaProvider do
              begin
                   { � Es correcto abrir/cerrar transacci�n }
                   { si el c�digo que llama a este m�todo }
                   { puede estar ya dentro de una transacci�n ? }
                   if lTx then
                      EmpiezaTransaccion;
                   try
{$ifdef INTERBASE}
                      ParamAsInteger( FUpdate, 'Empleado', iEmpleado );
                      ParamAsInteger( FUpdate, 'Paso', FCounter );
                      ParamAsDate( FUpdate, 'Fecha', Trunc( dValue ) );
                      ParamAsVarChar( FUpdate, 'Hora', TimeToStrSQL( dValue ), K_ANCHO_HORA );
                      with FUpdate do
                      begin
                           Active := True;
                           Result := ( Fields[ 0 ].AsInteger = 0 );
                           Active := False;
                      end;
{$else}
                      ParamAsInteger( FUpdate, '@Empleado', iEmpleado );
                      ParamAsInteger( FUpdate, '@Paso', FCounter );
                      ParamAsDate( FUpdate, '@Fecha', Trunc( dValue ) );
                      ParamAsVarChar( FUpdate, '@Hora', TimeToStrSQL( dValue ), K_ANCHO_HORA );
                      Ejecuta( FUpdate );
                      Result := ( GetParametro( FUpdate, '@Status' ).AsInteger = 0 );
{$endif}
                      if lTx then
                         TerminaTransaccion( True );
                   except
                         on Error: Exception do
                         begin
                              if lTx then
                                 TerminaTransaccion( False );
                              Result := not FCancelado; { � Es correcto suponer esto ? }
                         end;
                   end;
              end;
              FSteps := 1;
              FCancelado := not Result;
         end
         else
             Result := not FCancelado;
end;

function TZetaLog.CloseProcess: OleVariant;
const
     K_CLOSE_SQL = 'update PROCESO set '+
                   'PC_FEC_FIN = ''%s'', '+
                   'PC_HOR_FIN = ''%s'', '+
                   'PC_PASO = %d, '+
                   'PC_ERROR = ''%s'', '+
                   'CB_CODIGO = %d ' +
                   'where ( PC_NUMERO = %d )';
var
   dValue: TDateTime;
begin
     dValue := Now;
     with oZetaProvider do
     begin
          if Abierto then
          begin
{$ifdef MSSQL}
               EmpiezaTransaccion;
{$endif}
               ExecSQL( EmpresaActiva, Format( K_CLOSE_SQL, [ DateToStrSQL( dValue ),
                                                              TimeToStrSQL( dValue ),
                                                              FCounter,
                                                              GetStatus,
                                                              FEmpleado,
                                                              FFolio ] ) );

{$ifdef MSSQL}
               TerminaTransaccion( TRUE );
{$endif}
          end;
     end;
     Result := GetEmptyProcessResult( FProceso );
     Result[ K_PROCESO_FOLIO ] := FFolio;
     Result[ K_PROCESO_STATUS ] := Ord( GetEnumStatus );
     Result[ K_PROCESO_MAXIMO ] := FMaxSteps;
     Result[ K_PROCESO_PROCESADOS ] := FCounter;
     Result[ K_PROCESO_ULTIMO_EMPLEADO ] := FEmpleado;
     Result[ K_PROCESO_INICIO ] := FInicio;
     Result[ K_PROCESO_FIN ] := dValue;
     Result[ K_PROCESO_ERRORES ] := FErrorCount;
     Result[ K_PROCESO_ADVERTENCIAS ] := FWarningCount;
     Result[ K_PROCESO_EVENTOS ] := FEventCount;
     BorraQuerys;            // Liberar Cursores FDataSet y FUpdate
end;

procedure TZetaLog.CancelProcess( const iFolio: Integer );
const
     K_CANCEL_SQL = 'update PROCESO set '+
                   'PC_FEC_FIN = ''%s'', '+
                   'PC_HOR_FIN = ''%s'', '+
                   'PC_ERROR = ''' + B_PROCESO_CANCELADO + ''', '+
                   'US_CANCELA = %d '+
                   'where ( PC_NUMERO = %d )';
var
   dValue: TDate;
   iUsuario: Integer;
begin
     with oZetaProvider do
     begin
          dValue := Now;
          iUsuario := EmpresaActiva[ P_USUARIO ];
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( K_CANCEL_SQL, [ DateToStrSQL( dValue ),
                                                             TimeToStrSQL( dValue ),
                                                             iUsuario,
                                                             iFolio ] ) );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                     raise;
                end;
          end;
     end;
end;

procedure TZetaLog.Escribir( const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
var
   dValor: TDateTime;
   dGravaMov: TDate;
   sData: String;
begin
     dValor := Now;
     if ( dMovimiento = NullDateTime ) then
        dGravaMov := Trunc( dValor )   { Se usa Trunc() para que la base de datos no guarde la parte fraccionaria ( que representa la hora ) y los filtros de reportes puedan funcionar bien }
     else
         dGravaMov := dMovimiento;
     sData := sTexto;
     if strVacio( sData ) then
        sData := ' ';           // Si no marca error al grabar el Blob
     with oZetaProvider do
     begin
          try
             ParamAsDate( FDataset, 'BI_FECHA', Trunc( dValor ) );  { Se usa Trunc() para que la base de datos no guarde la parte fraccionaria ( que representa la hora ) y los filtros de reportes puedan funcionar bien }
             ParamAsVarChar( FDataset, 'BI_HORA', TimeToStrSQL( dValor ), K_ANCHO_LOG_HORA );
             ParamAsInteger( FDataset, 'BI_TIPO', Ord( eTipo ) );
             ParamAsInteger( FDataset, 'BI_CLASE', Ord( eClase ) );
             ParamAsVarChar( FDataset, 'BI_TEXTO', Copy( sMensaje, 1, 50 ), K_ANCHO_LOG_TEXTO );
             ParamAsInteger( FDataset, 'CB_CODIGO', iEmpleado );
             ParamAsDate( FDataset, 'BI_FEC_MOV', dGravaMov );
             ParamAsBlob( FDataset, 'BI_DATA', sData );
{$ifdef MSSQL}
{
             if FConnection.InTransaction then
                FConnection.Commit
             else
                FConnection.StartTransact;
}
{$endif}
             Ejecuta( FDataset );
{$ifdef MSSQL}
//             FConnection.Commit;
{$endif}
             Conteo( eTipo );
          except
                on Error: Exception do
                begin
                    // ShowMessage( Error.Message );
{$ifdef MSSQL}
//                    TerminaTransaccion( FALSE );
{$endif}
                    Conteo( tbErrorGrave );
                    { � Que se debe hacer aqu� ? }
                end;
          end;
     end;
end;

procedure TZetaLog.Cambio( const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; const sCampoEmpleado, sCampoFecha: String; Dataset: TDataSet );
begin
     Escribir( tbNormal, eClase, 0, Now, sMensaje, sMensaje );
end;

procedure TZetaLog.Evento( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto );
begin
     Escribir( tbNormal, eClase, iEmpleado, dMovimiento, sMensaje, sMensaje );
end;

procedure TZetaLog.Evento( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
begin
     Escribir( tbNormal, eClase, iEmpleado, dMovimiento, sMensaje, sTexto );
end;

procedure TZetaLog.Advertencia( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto );
begin
     Escribir( tbAdvertencia, clbNinguno, iEmpleado, NullDateTime, sMensaje, sMensaje );
end;

procedure TZetaLog.Advertencia( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: String );
begin
     Escribir( tbAdvertencia, clbNinguno, iEmpleado, NullDateTime, sMensaje, sTexto );
end;

procedure TZetaLog.Advertencia( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
begin
     Escribir( tbAdvertencia, eClase, iEmpleado, dMovimiento, sMensaje, sTexto );
end;

procedure TZetaLog.Error( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: String );
begin
     Escribir( tbError, clbNinguno, iEmpleado, NullDateTime, sMensaje, sTexto );
end;

procedure TZetaLog.ErrorGrave( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto );
begin
     Escribir( tbErrorGrave, clbNinguno, iEmpleado, NullDateTime, sMensaje, sMensaje );
end;

procedure TZetaLog.Excepcion( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception; const sTexto: String );
begin
     Escribir( GetExceptionLogType( Problema ), clbNinguno, iEmpleado, NullDateTime, sMensaje, ZetaServerTools.GetExceptionInfo( Problema ) + CR_LF + sTexto );
end;

procedure TZetaLog.Excepcion( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception );
begin
     Excepcion( iEmpleado, sMensaje, Problema, '' );
end;

{ TTablaInfo }

constructor TTablaInfo.Create;
begin
     FListaCampos := TStringList.Create;
     FListaKey := TStringList.Create;
     FListaOrder := TStringList.Create;
     FListaForeignKey := TStringList.Create;
     FParams := TParams.Create;
     FSQL := TStringList.Create;
     FFiltro := '';
     FOnUpdateData := NIL;
     FBeforeUpdateRecord := NIL;
     FAfterUpdateRecord := NIL;
     FOnUpdateError := NIL;
end;

destructor TTablaInfo.Destroy;
begin
     FListaCampos.Free;
     FListaKey.Free;
     FListaOrder.Free;
     FListaForeignKey.Free;
     FParams.Free;
     FSQL.Free;
     inherited;
end;

procedure TTablaInfo.SetCampos(const sCampos: String);
begin
     FListaCampos.CommaText := sCampos;
end;

procedure TTablaInfo.SetKey(const sCampos: String);
begin
     FListaKey.CommaText := sCampos;
     FListaOrder.Assign( FListaKey );
end;

procedure TTablaInfo.SetOrder(const sCampos: String);
begin
     FListaOrder.CommaText := sCampos;
end;

procedure TTablaInfo.SetForeignKey(const sCampos: String);
begin
     FListaForeignKey.CommaText := sCampos;
end;


procedure TTablaInfo.SetInfo( const sTabla, sCampos, sKey : String );
begin
     FTabla := sTabla;
     SetCampos( sCampos );
     SetKey( sKey );
     Filtro := '';
     SetForeignKey( '' );
     OnUpdateData := NIL;
     BeforeUpdateRecord := NIL;
     AfterUpdateRecord := NIL;
     OnUpdateError := NIL;

end;

procedure TTablaInfo.SetInfoDetail( const sTabla, sCampos, sKey, sForeignKey : String );
begin
   SetInfo( sTabla, sCampos, sKey );
   SetForeignKey( sForeignKey );
end;




function TTablaInfo.ConstruyeQuery( const eTipo : eTipoQuery; Delta : TDataset; qryCambios : TOEQuery ) : String;
var
    lEsDetail : Boolean;
    oMasterDataSet : TDataSet;

  procedure Agrega( const sLinea : String );
  begin
       SQL.Add( sLinea );
  end;

  procedure AgregaLista( oLista : TStringList  );
  begin
       Agrega( oLista.CommaText );
  end;

  procedure ConstruyeSelect;
  begin
       Agrega( 'SELECT' );
       AgregaLista( FListaCampos );
  end;

  procedure ConstruyeDelete;
  begin
       Agrega( 'DELETE' );
  end;

  procedure ConstruyeFrom;
  begin
       Agrega( 'FROM ' + FTabla );
  end;

  function CampoConstante( const Valor : Variant; const Tipo : TFieldType ) : String;
  begin
       case Tipo of
            ftSmallint, ftInteger, ftWord :
              Result := IntToStr( Valor );
            ftString :
              Result := '''' + Valor + '''';
            ftFloat, ftCurrency, ftBCD :
              Result := FloatToStr( Valor );
            ftDate, ftTime, ftDateTime :
              Result := '''' + FormatDateTime( 'mm/dd/yyyy', Valor ) + '''';
       end;
  end;


  procedure RevisaCambios;
  var
     i, iPos : Integer;
  begin
       with Delta do
         for i := 0 to FieldCount-1 do
             with Fields[ i ] do
               if not IsNull and ( NewValue <> OldValue ) then
               begin
                 iPos := FListaCampos.IndexOf( FieldName );
                 // Si el campo existe en esta tabla
                 if ( iPos >= 0 ) then
                    FParams.CreateParam( DataType, FieldName, ptInput );
               end;
  end;

{
  procedure AgregaListaCambios( oLista : TStringList  );
  var
     i, iPos : Integer;
     sCampo, sValor : String;
     dCampo : TDate;
  begin
       with Delta do
         for i := 0 to FieldCount-1 do
           with Fields[ i ] do
                // Si el campo cambi� de valor
                if not IsNull and ( NewValue <> OldValue ) then
                begin
                     iPos := oLista.IndexOf( FieldName );
                     // Si el campo existe en esta tabla
                     if ( iPos >= 0 ) then
                     begin
                        // Si llamo CampoConstante, marca invalida variant type conversion
                        if DataType in [ ftDate, ftTime, ftDateTime ] then
                        begin
                            dCampo := NewValue;
                            sValor := '''' + FormatDateTime( 'mm/dd/yyyy', dCampo ) + '''';
                        end
                        else
                            sValor := CampoConstante( NewValue, DataType );

                        sCampo := FieldName + ' = ' + sValor;
                        sCampo := sCampo + ',';
                        Agrega( sCampo );
                     end;
                end;
       // Quita �ltima ','
       sCampo := SQL[SQL.Count-1];
       SQL[SQL.Count-1] := Copy(sCampo, 1, Length(sCampo) - 1);
  end;
}

  procedure AgregaCamposUpdate;
  var
     i : Integer;
     sCampo : String;
  begin
       with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := Items[ i ].Name;
           sCampo := sCampo + ' = :' + sCampo;
           if ( i < Count-1 ) then
               sCampo := sCampo + ',';
           Agrega( sCampo );
         end;
  end;


  {
  insert into edocivil
  (TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO)
values
  (:TB_CODIGO, :TB_ELEMENT, :TB_INGLES, :TB_NUMERO, :TB_TEXTO)
}
  function EsForeignKey( const sCampo : String ) : Boolean;
  begin
    Result := FListaForeignKey.IndexOf( sCampo ) >= 0;
  end;

  procedure RevisaValores;
  var
     i, iPos : Integer;
  begin
       with Delta do
         for i := 0 to FieldCount-1 do
             with Fields[ i ] do
               if ( Not IsNull ) or ( lEsDetail and EsForeignKey( FieldName )) then
               begin
                 iPos := FListaCampos.IndexOf( FieldName );
                 // Si el campo existe en esta tabla
                 if ( iPos >= 0 ) then
                    FParams.CreateParam( DataType, FieldName, ptInput );
               end;
  end;


  procedure AgregaCamposInsert;
  var
     i : Integer;
     sCampo : String;
  begin
       with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := Items[ i ].Name;
            if ( i < Count-1 ) then
               sCampo := sCampo + ',';
            Agrega( sCampo );
         end;

  end;

  procedure AgregaValoresInsert;
  var
     i : Integer;
     sCampo : String;
//     oField : TField;
  begin
{
       with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := Items[ i ].Name;
           oField := Delta.FindField( sCampo );
           if ( lEsDetail and EsForeignKey( oField.FieldName )) then
             with oMasterDataSet.FieldByName( sCampo ) do
               sCampo := CampoConstante( Value, DataType )
           else
             sCampo := CampoConstante( oField.Value, oField.DataType );
           if ( i < Count-1 ) then
              sCampo := sCampo + ',';
           Agrega( sCampo );
         end;
}
      // Temporal: La unica diferencia con agrega campos Insert es ':'
      with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := ':' + Items[ i ].Name;
            if ( i < Count-1 ) then
               sCampo := sCampo + ',';
            Agrega( sCampo );
         end;

  end;

  procedure AsignaParams;
  var
     i : Integer;
     sCampo : String;
     oField : TField;
  begin
       with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := Items[ i ].Name;
           oField := Delta.FindField( sCampo );

           if ( eTipo = etqInsert ) and ( lEsDetail and EsForeignKey( oField.FieldName )) then
                oField := oMasterDataSet.FieldByName( sCampo );

           AsignaParamCampo( qryCambios.Params[ i ], oField );
         end;
  end;

  procedure ConstruyeInsert;
  begin
       Agrega( 'INSERT INTO ' + FTabla );
       Agrega( '(' );
       RevisaValores;
       AgregaCamposInsert;
       Agrega( ') VALUES (' );
       AgregaValoresInsert;
       Agrega( ')' );
  end;

  procedure ConstruyeUpdate;
  begin
       Agrega( 'UPDATE ' + FTabla );
       Agrega( 'SET' );
       RevisaCambios;
       AgregaCamposUpdate;
  end;


  procedure ConstruyeWhereDetail;
  var
     i : Integer;
     sCampoMaster, sCampoDetail : String;
     sCampo : String;
  begin
       with FListaForeignKey do
       begin
         Agrega( 'WHERE' );
         for i := 0 to Count-1 do
         begin
              sCampoMaster := Strings[ i ];
              // Si se tiene un Pare Name=Value
              if Pos( '=', sCampoMaster ) > 0 then
              begin
                   sCampoMaster := Names[i];
                   sCampoDetail := Values[ sCampoMaster ];
              end
              else
                  sCampoDetail := FListaKey.Strings[ i ];
              sCampo := sCampoDetail + ' = :' + sCampoMaster;
              if ( i < Count-1 ) then
                   sCampo := sCampo + ' and';
              Agrega( sCampo );
         end;
       end;
  end;

  procedure ConstruyeWherePadre;
  var
     i : Integer;
     sCampoMaster, sCampoDetail : String;
     sCampo : String;
  begin
       with FListaForeignKey do
       begin
         Agrega( 'WHERE' );
         for i := 0 to Count-1 do
         begin
              sCampoDetail := FListaKey.Strings[ i ];
              sCampoMaster := Strings[ i ];
              with Delta.FieldByName( sCampoMaster ) do
                   sCampoMaster := CampoConstante( NewValue, DataType );
              sCampo := sCampoDetail + ' = ' + sCampoMaster;
              if ( i < Count-1 ) then
                   sCampo := sCampo + ' and';
              Agrega( sCampo );
         end;
       end;
  end;


  procedure ConstruyeWhereFiltro;
  begin
       Agrega( 'WHERE' );
       Agrega( FFiltro );
  end;

  procedure AgregaListaWhere( oLista : TStringList  );
  var
     i : Integer;
     sCampo, sValor : String;
     oField : TField;
     dCampo : TDate;
  begin
       with oLista do
         for i := 0 to Count-1 do
         begin
              sCampo := Strings[ i ];
              oField := Delta.FindField( sCampo );
              if Assigned( oField ) then
              begin
                if oField.DataType in [ ftDate, ftTime, ftDateTime ] then
                begin
                    dCampo := oField.OldValue;
                    sValor := '''' + FormatDateTime( 'mm/dd/yyyy', dCampo ) + '''';
                end
                else
                    sValor := CampoConstante( oField.OldValue, oField.DataType );
                sCampo := sCampo + ' = ' + sValor;
                if ( i < Count-1 ) then
                   sCampo := sCampo + ' and';
                Agrega( sCampo );
              end;
         end;
  end;

  procedure ConstruyeWhere;
  begin
       Agrega( 'WHERE' );
       AgregaListaWhere( FListaKey );
  end;

  procedure ConstruyeOrder;
  begin
       Agrega( 'ORDER BY' );
       AgregaLista( FListaOrder );
  end;



begin // ConstruyeQuery
     FParams.Clear;
     SQL.Clear;
     lEsDetail := FListaForeignKey.Count > 0;
     if ( lEsDetail ) then
        oMasterDataSet := TClientDataSet( Delta ).DataSetField.DataSet;

     case eTipo of
          etqSelect :
            begin
                 ConstruyeSelect;
                 ConstruyeFrom;
                 if ( lEsDetail ) then
                   ConstruyeWhereDetail
                 else if ( Length( FFiltro ) > 0 ) then
                     ConstruyeWhereFiltro;
                 ConstruyeOrder;
            end;
          etqDelete :
            begin
                 ConstruyeDelete;
                 ConstruyeFrom;
                 ConstruyeWhere;
                 qryCambios.SQL.Assign( SQL );
            end;
          etqUpdate :
            begin
                 ConstruyeUpdate;
                 ConstruyeWhere;
                 qryCambios.SQL.Assign( SQL );
                 AsignaParams;
            end;
          etqInsert :
            begin
                 ConstruyeInsert;
                 qryCambios.SQL.Assign( SQL );
                 AsignaParams;
            end;
     end;
     Result := SQL.Text;

{
     if eTipo <> etqSelect then
      ShowMessage( Result );
}      

end;


function TTablaInfo.ConstruyeUpdateSQL(const UpdateKind: TUpdateKind;
  Delta: TDataset; qryCambios : TOEQuery ) : String;
var
   TipoQuery : eTipoQuery;
begin
     case UpdateKind of
          ukModify : TipoQuery := etqUpdate;
          ukDelete : TipoQuery := etqDelete;
          else
                   TipoQuery := etqInsert;
     end;
     Result := ConstruyeQuery( TipoQuery, Delta, qryCambios );
end;

function TTablaInfo.ConstruyeSelectSQL : String;
begin
     Result := ConstruyeQuery( etqSelect, NIL, NIL );
end;

{ TdmZetaProvider }

procedure TdmZetaServerProvider.dmZetaProviderCreate(Sender: TObject);
begin
     Environment;
     FTablaInfo := TTablaInfo.Create;
     FDetailInfo := TTablaInfo.Create;
     {$ifdef NEWCOMPARTE}
     RegistryInit;
     {$else}
     {$ifdef DOS_CAPAS}
     FRegistry := TZetaRegistryServer.Create;
     {$else}
     FRegistry := TZetaRegistryServer.Create( False );
     {$endif}
     {$endif}
     Activate;
{$ifdef MSSQL}
     // EZM: En Master-Detail, marca error Connection Busy
     // por abrir 2 Unidireccionales al mismo tiempo, en la misma conecci�n
     // Adem�s, se tienen problemas con BLOB's
     qryMaster.UniDirectional := FALSE;
     qryDetail.UniDirectional := FALSE;
     qryCambios.UniDirectional := FALSE;
{$endif}
     with prvUnico do
     begin
          OnGetData := prvUnicoGetData;
          OnUpdateData := prvUnicoUpdateData;
          OnUpdateError := prvUnicoUpdateError;
          AfterUpdateRecord := prvUnicoAfterUpdateRecord;
          BeforeUpdateRecord := prvUnicoBeforeUpdateRecord;
     end;
     {$IFDEF WORKFLOWCFG}
     //Creacion de listas
     FArregloPeriodo := TStringList.Create;
     FArregloTipoNomina := TStringList.Create;
     {$ENDIF}
end;

procedure TdmZetaServerProvider.dmZetaProviderDestroy(Sender: TObject);
begin
     ReleaseLog;
     {$ifndef NEWCOMPARTE}
     FRegistry.Free;
     {$endif}
     FDetailInfo.Free;
     FTablaInfo.Free;
     FreeAndNil(FCdsPeriodosNomInfo);

     {$IFDEF WORKFLOWCFG}
      //Destruccion de listas
     FreeAndNil(FArregloPeriodo);
     FreeAndNil(FArregloTipoNomina);
     {$ENDIF}
end;

procedure TdmZetaServerProvider.Activate;
begin
     { GA: Este c�digo estaba antes en el DataModuleCreate }
     ZetaCommonTools.SetVariantToNull( FEmpresa );
     FPeriodoObtenido:= FALSE;
     FVerConfidencial := TRUE;
     FNumeroNiveles   := 0;
end;

procedure TdmZetaServerProvider.Deactivate;
begin
     hdbcEmpresa.Connected := False;
     hdbcComparte.Connected := False;
end;

procedure TdmZetaServerProvider.PreparaDetail;
begin
     // qryDetail.TableName    := FDetailInfo.FTabla;
     FDetailInfo.SetMasterFields( qryDetail );
end;

function TdmZetaServerProvider.GetMasterDetail( Empresa : Variant ) : OleVariant;
begin
     dsMaster.DataSet := qryMaster;
     try
        ConectaEmpresa( Empresa );
        PreparaTabla;
        PreparaDetail;
        FRecsOut := 0;
        Result := prvUnico.GetRecords( -1, FRecsOut, Ord(grMetaData)+Ord(grReset));
     finally
        dsMaster.DataSet := NIL;
     end;
end;

procedure TdmZetaServerProvider.PreparaTabla;
begin
    qryMaster.SQL.Text := FTablaInfo.ConstruyeSelectSQL;
end;

function TdmZetaServerProvider.GetTabla( Empresa : Variant ) : OLEVariant;
begin
    ConectaEmpresa( Empresa );
    PreparaTabla;
    FRecsOut := 0;
    Result := prvUnico.GetRecords( -1, FRecsOut, Ord(grMetaData)+Ord(grReset));
end;

function TdmZetaServerProvider.GrabaTabla( Empresa : Variant; oDelta: OleVariant; var ErrorCount : Integer): OleVariant;
var
    OwnerData : OleVariant;
begin
    ConectaEmpresa( Empresa );
    EmpiezaTransaccion;
    Result := prvUnico.ApplyUpdates( oDelta, -1, ErrorCount, OwnerData );
    if ErrorCount = 0 then
       TerminaTransaccion( TRUE )
    else
       RollBackTransaccion;
       // TerminaTransaccion( FALSE );
end;

function TdmZetaServerProvider.GrabaTablaGrid(Empresa: Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
var
    OwnerData : OleVariant;
begin
    ConectaEmpresa( Empresa );
    EmpiezaTransaccion;
    Result := prvUnico.ApplyUpdates( oDelta, -1, ErrorCount, OwnerData );
    TerminaTransaccion( TRUE )
end;

function TdmZetaServerProvider.GrabaMasterDetail( Empresa : Variant;
                                            oDelta: OleVariant;
                                            var ErrorCount: Integer): OleVariant;
begin
     try
        dsMaster.DataSet := qryMaster;
        Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     finally
        dsMaster.DataSet := NIL;
     end;
end;

{$ifdef VER130}
procedure TdmZetaServerProvider.UpdateTabla(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
{$else}
procedure TdmZetaServerProvider.UpdateTabla(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
{$endif}
var
   TablaInfo : TTablaInfo;
begin
     if Assigned( DeltaDS.DataSetField ) then
        TablaInfo := FDetailInfo
     else
         TablaInfo := FTablaInfo;
//     qryCambios.CommandText := TablaInfo.ConstruyeUpdateSQL( UpdateKind, DeltaDS );
     TablaInfo.ConstruyeUpdateSQL( UpdateKind, DeltaDS, qryCambios );
//     try
        qryCambios.ExecSQL;
{
     // PENDIENTE
        if qryCambios.RowsAffected > 1 then
           DatabaseError('Demasiados registros cambiaron');
        if qryCambios.RowsAffected < 1 then
           DatabaseError('Registro modificado por otro usuario');
}
        Applied:= TRUE;
//     except
//      Applied := FALSE;
//     end;

end;

procedure TdmZetaServerProvider.ExecSQL( Empresa : Variant; const sSQL : String);
begin
     ConectaEmpresa( Empresa );
     with qryCambios do
     begin
          //ParamCheck  := FALSE; // Para cuando se modifican StoredProcedures (ej. Supervisores)
          hstmt.SQLParsing := FALSE;     // Para cuando se modifican StoredProcedures
          SQL.Text    := sSQL;
          ExecSQL;
          hstmt.SQLParsing := TRUE;
          //ParamCheck  := TRUE;
     end;
end;

function TdmZetaServerProvider.OpenSQL( Empresa : Variant; const sSQL : String; const lDatos: Boolean): OleVariant;
var
    nRecords : Integer;
begin
     with qryMaster do
     begin
          Active   := FALSE;
          ConectaEmpresa( Empresa );
          SQL.Text := sSQL;
          FRecsOut := 0;
          if ( lDatos ) then
            nRecords := -1
          else
            nRecords := 0;
          Result := prvUnico.GetRecords( nRecords, FRecsOut, Ord(grMetaData)+Ord(grReset) )
     end;
end;

function TdmZetaServerProvider.OpenSQL(Empresa: Variant; const sSQL: String; const nRecords: Integer): OleVariant;
begin
     with qryMaster do
     begin
          Active   := FALSE;
          ConectaEmpresa( Empresa );
          SQL.Text := sSQL;
          FRecsOut := 0;
          Result := prvUnico.GetRecords( nRecords, FRecsOut, Ord(grMetaData)+Ord(grReset) )
     end;
end;


function TdmZetaServerProvider.ConectaBaseDeDatos( const Empresa: Variant ): THdbc;
var
   sAlias, sUser, sPassword: String;
{$ifdef MSSQL}
   sServer, sDatabase: String;
{$endif}
begin
     sAlias := Empresa[ P_DATABASE ];
     {$ifdef NEWCOMPARTE}
     if ( sAlias = FComparte[ P_DATABASE ] ) then
     begin
          Result := HdbcComparte;
          sPassword := Empresa[ P_PASSWORD ]; { Ya viene decifrado }
     end
     else
     begin
          Result := HdbcEmpresa;
          sPassword := ZetaServerTools.Decrypt( Empresa[ P_PASSWORD ] ); { Viene cifrado }
     end;
     {$else}
     with FRegistry do
     begin
          if ( sAlias = Database ) then
          begin
               Result := HdbcComparte;
               sPassword := Empresa[ P_PASSWORD ]; { Ya viene decifrado }
          end
          else
          begin
               Result := HdbcEmpresa;
               sPassword := ZetaServerTools.Decrypt( Empresa[ P_PASSWORD ] ); { Viene cifrado }
          end;
     end;
     {$endif}
     { Nota: No est� dise�ado para estar cambiando de Empresa.
      Una vez que se conecta, ya queda conectada con ese Alias }
     try
        with Result do
        begin
{$ifdef DOS_CAPAS}
             if not Connected or ( Datasource <> sAlias ) then
{$else}
             if not Connected then
{$endif}
             begin
{$ifdef DOS_CAPAS}
                  Connected := False;
{$endif}
                  sUser := Empresa[ P_USER_NAME ];
                  UserName   := sUser;
                  Password   := sPassword;
{$ifdef INTERBASE}
                  Driver := 'INTERSOLV InterBase ODBC Driver (*.gdb)';
                  with Attributes do
                  begin
                       Clear;
                       Add( Format( 'Database=%s', [ sAlias ] ) );
                       Add( 'ApplicationUsingThreads=1' );
                       Add( 'LockTimeOut=0' );
                       Add( 'WorkArounds=1' );
                  end;
{$endif}
{$ifdef MSSQL}
                  ZetaServerTools.GetServerDatabase( sAlias, sServer, sDatabase );
                  Driver := 'SQL Server';
                  with Attributes do
                  begin
                       Clear;
                       Add( Format( 'Server=%s', [ sServer ] ) );     // Este dato tambi�n deber� viajar
                       Add( Format( 'Database=%s', [ sDatabase ] ) );
                       { Para que las fechas se formateen bien }
                       { EXEC SP_CONFIG 'DefaultLanguage', 0: Fija el default para todos los usuarios }
                       { EXEC SP_DEFAULTLANGUAGE 'sa', 'us_english': Fija el default para el usuario 'sa' }
                       Add( Format( 'Language=%s', [ 'us_english' ] ) );
                  end;
{$endif}
                  Connected := True;
             end;
        end;
     except
           on Error: Exception do
           begin
                Error.Message := 'Error al Conectar a la Base de Datos: ' + sAlias + CR_LF + sAlias + CR_LF + Error.Message;
                raise;
           end;
     end;
end;

function TdmZetaServerProvider.GetInsertScript( const sTabla, sExcluidos: String; Source: TZetaCursor ): String;
var
   i: Integer;
   sScript, sCampos, sField: String;
begin
     sScript := 'insert into ' + sTabla + ' ( ';
     sCampos := ' ) values ( ';
     with Source do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               with Fields[ i ] do
               begin
                    sField := FieldName;
                    if ( FieldKind = fkData ) and ( Pos( sField, sExcluidos ) = 0 ) then
                    begin
                         sScript := sScript + sField + ',';
                         sCampos := sCampos + ':' + sField + ',';
                    end;
               end;
          end;
     end;
     Result := ZetaCommonTools.CortaUltimo( sScript ) + ZetaCommonTools.CortaUltimo( sCampos ) + ' )';
end;

procedure TdmZetaServerProvider.ConectaEmpresa( const Empresa: Variant );
begin
     FConnection := ConectaBaseDeDatos( Empresa );
     qryMaster.hDbc := FConnection;
     qryDetail.hDbc := FConnection;
     qryCambios.hDbc := FConnection;
end;

{ ************ L�gica de Manejo de Queries / Stored Procedures ************** }

function TdmZetaServerProvider.CreateQuery : TDataset;
{$ifdef FALSExMSSQL}
const
     { GA: Se tomaron del archivo SQLEXT.H de ODBC / MDAC SDK 2.6 }
     { SQL_CONCURRENCY options }
     SQL_CONCUR_READ_ONLY            = 1;
     SQL_CONCUR_LOCK                 = 2;
     SQL_CONCUR_ROWVER               = 3;
     SQL_CONCUR_VALUES               = 4;
     {SQL_CURSOR_TYPE options }
     SQL_CURSOR_FORWARD_ONLY         = 0;
     SQL_CURSOR_KEYSET_DRIVEN        = 1;
     SQL_CURSOR_DYNAMIC              = 2;
     SQL_CURSOR_STATIC               = 3;

{$endif}
begin
     Result := TOEQuery.Create( self );
{$ifdef INTERBASE}
     TOEQuery( Result ).UniDirectional := TRUE;
{$endif}
{$ifdef FALSExMSSQL}
     {GA: Se intent� hacer unidireccionales los querys de SQL SERVER}
     {sin �xito. Hay problemas de concurrencia (como si fuera deadlocks) }
     {y a veces se barren mal los resultados ( el mismo while not eof..next }
     {produce resultados diferentes con y sin este c�digo ) }
     with TOEQuery( Result ) do
     begin
          RequestLive := False;
          with hStmt do
          begin
               CursorType := SQL_CURSOR_FORWARD_ONLY;
               ConcurrencyType := SQL_CONCUR_READ_ONLY;
               RowSetSize := 10000;
          end;
          Unidirectional := True;
     end;
{$endif}
end;

function TdmZetaServerProvider.CreateQuery(const sScript: String): TDataset;
begin
     Result := CreateQuery;
     try
        PreparaQuery( Result, sScript );
     except
        FreeAndNil( Result );
        raise;
     end;
end;

function TdmZetaServerProvider.CreateStoredProc(const sProcName: String): TDataset;
begin
    Result := CreateQuery;
    PreparaStoredProc( Result, sProcName );
end;


function TdmZetaServerProvider.CreateQueryLocate: TDataset;
begin
     Result := TOEQuery.Create( Self );
{$ifdef INTERBASE}
     TOEQuery( Result ).Cached := TRUE;
{$endif}
end;

procedure TdmZetaServerProvider.PreparaQuery( Query : TDataset; sScript: String );
begin
{$ifdef MSSQL}
    if UpperCase( Copy( sScript, 1, 17 )) = 'EXECUTE PROCEDURE' then
        sScript := '{CALL' + Copy( sSCript, 18, MAXINT ) + '}';
{$endif}

     with Query as TOEQuery do
     begin
          // Marcar los Querys que hacen SELECT pero que se ejecutan
          if UpperCase( Copy( sScript, 1, 6 )) = 'SELECT' then
            Tag := 1
{$ifdef MSSQL}
          else if Pos( 'CALL ', UpperCase( sScript )) > 0 then
          begin
            Tag := 2;    // Stored Procedures
            // Unidirectional := TRUE;
          end
{$endif}
          else
            Tag := 0;
          Active   := FALSE;
          hDbc     := ConectaBaseDeDatos( EmpresaActiva );
          SQL.Text := sScript;
          Prepared := TRUE;
{$ifdef MSSQL}
          if ( Tag = 2 ) then
            Unidirectional := TRUE;
{$endif}
     end;
end;

procedure TdmZetaServerProvider.PreparaStoredProc(Query: TDataSet; const sProcName: String);
begin
     with Query as TOEQuery do
     begin
        hDbc           := ConectaBaseDeDatos( EmpresaActiva );
        StoredProcName := sProcName;
        Unidirectional := TRUE;
        Tag            := 2;  // Por si se necesita distinguir
     end;
end;


function TdmZetaServerProvider.Ejecuta( Dataset: TDataset ) : Integer;
begin
       with TOEQuery( Dataset ) do
       begin
            // Es un SELECT a un StoredProcedure que modifica la Base de Datos
            // se tiene que ejecutar con Close/Open
            if ( Tag = 1 ) then
            begin
                Active := FALSE;
                Active := TRUE;
                Result := 1;
            end
            else
            begin
                ExecSQL;
{$ifdef INTERBASE}
                Result := RowsAffected;
{$endif}
{$ifdef MSSQL}
{
                // Para extraer par�metro RETURN_VALUE en llamadas por StoredProc's CALL
                if ( Tag = 2 ) and ( StoredProcName = '' ) then
                    NextResultSet;
}
                if ( Tag = 0 ) then
                    Result := RowsAffected      // RowsAffected marca error en StoredProcs
                else
                    Result := 0;
{$endif}
            end;
       end
end;

function TdmZetaServerProvider.EjecutaAndFree( const sSQL: String ): Integer;
var
   FDataSet: TZetaCursor;
   lEnTransaccion : Boolean;
begin
     Result := 0;
     lEnTransaccion := FConnection.InTransaction;
     FDataSet := CreateQuery( sSQL );
     try
        try
           if not lEnTransaccion then
              EmpiezaTransaccion;
           Result := Ejecuta( FDataSet );
           if not lEnTransaccion then
              TerminaTransaccion(True);
        except
           if not lEnTransaccion then
              RollBackTransaccion;
           raise;
        end;
     finally
        FreeAndNil( FDataSet );
     end;
end;

function TdmZetaServerProvider.GetParametro( Dataset: TDataset; const sName: String ): TParameter;
begin
    Result := TOEQuery( Dataset ).ParamByName( sName )
end;

procedure TdmZetaServerProvider.ParamSalida(Dataset: TDataset;
  const sName: String);
begin
    GetParametro( Dataset, sName ).ParamType := ptOutput;
end;

function TdmZetaServerProvider.GetParametroPos(Dataset: TOEQuery;
  const iPos: Integer): TParameter;
begin
     Result := Dataset.Params.Items[ iPos ];
end;

procedure TdmZetaServerProvider.ParamVariant(Dataset: TDataset;
  const iPos: Integer; const Valor: Variant);
begin
     GetParametroPos( TOEQuery( Dataset ), iPos ).Value := Valor;
end;

procedure TdmZetaServerProvider.ParamAsInteger( Dataset: TDataset; const sName: String; const iValue: Integer );
begin
     GetParametro( Dataset, sName ).Value := iValue;
end;

procedure TdmZetaServerProvider.ParamAsFloat( Dataset: TDataset; const sName: String; const rValue: Extended );
begin
     GetParametro( Dataset, sName ).Value := rValue;
end;

procedure TdmZetaServerProvider.ParamAsDate( Dataset: TDataset; const sName: String; const dValue: TDate );
begin
     {$ifdef INTERBASE}
     // El ADO no sabe que el Variant es Fecha?
     with GetParametro( Dataset, sName ) do
     begin
          if DataType = ftUnknown then
             DataType := ftDate;
          Value := dValue;
     end;
    {$endif}
{$ifdef MSSQL}
     GetParametro( Dataset, sName ).AsDateTime := dValue;
{$endif}
end;

procedure TdmZetaServerProvider.ParamAsString( Dataset: TDataset; const sName, sValue: String );
begin
    with GetParametro( Dataset, sName ) do
    begin
{ ODBCExpress
        // Problemas con CHAR<>VARCHAR en ADO
        if ( DataType = ftUnknown ) then
            DataType := ftFixedChar;
        if ( DataType = ftFixedChar ) and ( Length( sValue ) = 0 ) then
            // Problemas con ADO cuand es FixedChar y Strings VACIOS
            Value := ' '
        else
}
            Value := sValue;
    end;
end;

procedure TdmZetaServerProvider.ParamAsVarChar(Dataset: TDataset; const sName, sValue: String; const iAncho: Integer );
begin
    with GetParametro( Dataset, sName ) do
    begin
         // Size := iAncho;
         // Value := sValue;
         if ( iAncho > 0 ) then
            AsString := Copy( sValue, 1, iAncho )
         else
            AsString := sValue;
    end;
end;

procedure TdmZetaServerProvider.ParamAsChar(Dataset: TDataset; const sName, sValue: String; const iAncho: Integer );
begin
    with GetParametro( Dataset, sName ) do
    begin
        AsString := sValue;
{ ODBCExpress
        if ( DataType = ftUnknown ) then
            DataType := ftFixedChar;
        Size := iAncho;
        if ( DataType = ftFixedChar ) and ( Length( sValue ) = 0 ) then
            // Problemas con ADO cuand es FixedChar y Strings VACIOS
            Value := ' '
        else
            Value := sValue;
}
    end;
end;


procedure TdmZetaServerProvider.ParamAsBoolean( Dataset: TDataset; const sName: String; const lValue: Boolean );
begin
     // GetParametro( Dataset, sName ).AsString := ZetaCommonTools.zBoolToStr( lValue );
     ParamAsChar( Dataset, sName, ZetaCommonTools.zBoolToStr( lValue ), 1 );
end;

procedure TdmZetaServerProvider.ParamAsBlob( Dataset: TDataset; const sName: String; const sValue: String );
begin
     with GetParametro( Dataset, sName ) do
     begin
        AsBlob := sValue;
{
        if ( DataType = ftUnknown ) then
            DataType := ftMemo;
          Value := sValue;
}
     end;
end;

procedure TdmZetaServerProvider.ParamAsNull( Dataset: TDataset; const sName: String );
begin
     with GetParametro( Dataset, sName ) do
     begin
          Clear;
          Bound := True;
     end;
end;

procedure TdmZetaServerProvider.ParamAsGUID( Dataset: TDataset; const sName: String; const Valor: TGUID );
begin
     with GetParametro( Dataset, sName ) do
     begin
          SetBlobData( @Valor, SizeOf( Valor ) );
     end;
end;

procedure TdmZetaServerProvider.ParamAsDateTime( Dataset: TDataset; const sName: String; const dValue: TDateTime );
begin
     {$ifdef INTERBASE}
     // El ADO no sabe que el Variant es Fecha?
     with GetParametro( Dataset, sName ) do
     begin
          if DataType = ftUnknown then
             DataType := ftDate;
          Value := dValue;
     end;
    {$endif}
    {$ifdef MSSQL}
     GetParametro( Dataset, sName ).AsDateTime := dValue;
     {$endif}
end;

procedure TdmZetaServerProvider.CreateLog;
begin
     if not Assigned( FLog ) then
     begin
          FLog := TZetaLog.Create( Self );
     end;
end;

procedure TdmZetaServerProvider.ReleaseLog;
begin
     if Assigned( FLog ) then
     begin
          FreeAndNil( FLog );
     end;
end;

function TdmZetaServerProvider.CanContinue: Boolean;
begin
     Result := Log.CanContinue( 0, True );
end;

function TdmZetaServerProvider.CanContinue( const iEmpleado: TNumEmp ): Boolean;
begin
     Result := Log.CanContinue( iEmpleado, True );
end;

function TdmZetaServerProvider.CanContinue( const iEmpleado: TNumEmp; const lTx: Boolean ): Boolean;
begin
     Result := Log.CanContinue( iEmpleado, lTx );
end;

function TdmZetaServerProvider.OpenProcess( const eProceso: Procesos; const iMaxSteps: Integer; sParametros: string = VACIO; sFormula: string = VACIO ): Boolean;
var
   sFiltro: string;
   lFiltro: boolean;

   function GetFiltro( sCampo: string ): string;
   var
      oParam: TParam;
   begin
        oParam := FParamList.FindParam( sCampo );
        if ( oParam <> nil ) then
           Result := oParam.AsString
        else
            Result := VACIO;
   end;

begin
     CreateLog;
     sFiltro := VACIO;
     if Not ( FParamList = NIL ) then
     begin
          with FParamList do
          begin
               lFiltro := (FindParam( 'RangoLista' ) <> nil ) and (FindParam( 'Condicion' ) <> nil ) and
                          (FindParam( 'Filtro' ) <> nil );
               ZetaServertools.GetFiltroEmpleado( GetFiltro( 'RangoLista' ), GetFiltro( 'Condicion' ), GetFiltro( 'Filtro' ), lFiltro,  sParametros, sFiltro, sFormula );
          end;
     end;
     if ( Length( sParametros ) > ZetaCommonClasses.K_MAX_VARCHAR ) then
        sParametros := Copy( sParametros, 1, ZetaCommonClasses.K_MAX_VARCHAR );
     Result := Log.OpenProcess( eProceso, iMaxSteps, sParametros, sFiltro, sFormula );
end;

function TdmZetaServerProvider.CloseProcess: OleVariant;
begin
     Result := Log.CloseProcess;
     ReleaseLog;
end;

procedure TdmZetaServerProvider.CancelProcess( const iFolio: Integer );
begin
     { Hay que crear y destruir la instancia de Log ya que }
     { Esto no se va a llamar como parte de un wizard }
     { sino que desde la pantalla de consulta del status de procesos }
     try
        CreateLog;
        Log.CancelProcess( iFolio );
     finally
            ReleaseLog;
     end;
end;

procedure TdmZetaServerProvider.AbreBitacora;
begin
     CreateLog;
end;


function TdmZetaServerProvider.AbreQueryScript(Dataset: TDataset; const sScript: String): Boolean;
begin
     try
       with Dataset as TOEQuery do
       begin
            Active       := FALSE;
            hDBC         := ConectaBaseDeDatos( EmpresaActiva );
            SQL.Text     := sScript;
            Active       := TRUE;
            Result       := TRUE;
       end;
     except
       Result := FALSE;
     end;
end;

function TdmZetaServerProvider.GetGlobalBooleano( const iCodigo: Integer): Boolean;
begin
     Result := zStrToBool( LeeGlobal( iCodigo ));
end;

function TdmZetaServerProvider.GetGlobalDate( const iCodigo: Integer): TDateTime;
begin
     Result := StrToFecha( LeeGlobal( iCodigo ));
end;

function TdmZetaServerProvider.GetGlobalInteger( const iCodigo: Integer): Integer;
begin
     Result := StrToIntDef( LeeGlobal( iCodigo ), 0 );
end;

function TdmZetaServerProvider.GetGlobalReal(const iCodigo: Integer): Real;
begin
     Result := StrToReal( LeeGlobal( iCodigo ) );
end;

function TdmZetaServerProvider.GetGlobalString( const iCodigo: Integer): String;
begin
     Result := LeeGlobal( iCodigo );
end;

procedure TdmZetaServerProvider.InitGlobales;
const
     K_SELECT_GLOBAL = 'select GL_FORMULA from GLOBAL where GL_CODIGO = :Codigo';
begin
     if ( FQueryGlobal = NIL ) then
          FQueryGlobal := CreateQuery as TOEQuery;
     PreparaQuery( FQueryGlobal, K_SELECT_GLOBAL );
end;

// Como es dentro de esta unidad, puedo saber el tipo Exacto
// del DataSet y no tengo por qu� usar los m�todos abstractos
// Esto nos da mayor velocidad.
// Al quitar los TQuerys, cambia ligeramente la implementaci�n
function TdmZetaServerProvider.LeeGlobal(const iCodigo: Integer): String;
begin
     if ( FQueryGlobal = NIL ) then
        raise Exception.Create( 'Falta Llamar a InitGlobales antes de usar GetGlobal<x>' );
     with FQueryGlobal do
     begin
          Active := FALSE;
          Params[ 0 ].Value := iCodigo;
          // Params[ 0 ].AsInteger := iCodigo;
          Active := TRUE;
          Result := Fields[ 0 ].AsString;
     end;
end;

function TdmZetaServerProvider.OK_ProyectoEspecial( const eProgEspecial: eProyectoEspecial ): Boolean;
begin
     //Solo para version corporativa y para los ejecutables de Tress aplica esta validacion.
     {$ifdef MSSQL}
       {$ifdef TRESS}
       InitGlobales;
       Result := ZetaCommonTools.ProgramacionEspecial( eProgEspecial, GetGlobalString(K_GLOBAL_PROYECTOS_ESPECIALES) )
                 {and [sentinel].OK_ProyectoEspecial( eProgEspecial ) <-- A futuro se valida tambien contra el sentinel};
       {$else}
       Result := FALSE; //Tengo que poner el FALSE en cada else, porque si no me marca un warning
       {$endif}
     {$else}
       Result := FALSE;
     {$endif}
end;

{$ifdef NEWCOMPARTE}
procedure TdmZetaServerProvider.RegistryInit;
const
     K_DUMMY = '********';
begin
     FRegistryOpened := False;
     FComparte := VarArrayOf( [ K_DUMMY,  { Datos }
                                K_DUMMY,  { UserName }
                                K_DUMMY,  { Password }
                                0 ] );    { # Usuario }
     FComparteAlias := K_DUMMY;
end;

procedure TdmZetaServerProvider.RegistryRead;
var
   FRegistry: TZetaRegistryServer;
begin
     if not FRegistryOpened then
     begin
          {$ifdef DOS_CAPAS}
          FRegistry := TZetaRegistryServer.Create;
          {$else}
          FRegistry := TZetaRegistryServer.Create( False );
          {$endif}
          try
             with FRegistry do
             begin
                  FComparte[ P_DATABASE ] := Database;
                  FComparte[ P_USER_NAME ] := UserName;
                  FComparte[ P_PASSWORD ] := Password;
                  FComparteAlias := AliasComparte;
             end;
          finally
                 FreeAndNil( FRegistry );
          end;
          FRegistryOpened := True;
     end;
end;
{$endif}

function TdmZetaServerProvider.GetComparte: Variant;
begin
     {$ifdef NEWCOMPARTE}
     RegistryRead;
     Result := FComparte;
     {$else}
     with FRegistry do
     begin
          Result := VarArrayOf( [ Database, UserName, Password, 0 ] );
     end;
     {$endif}
end;

function TdmZetaServerProvider.AliasComparte: String;
begin
     {$ifdef NEWCOMPARTE}
     RegistryRead;
     Result := FComparteAlias;
     {$else}
     with FRegistry do
     begin
          // Result := DataLinkName;
          Result := AliasComparte;
     end;
     {$endif}
end;

procedure TdmZetaServerProvider.SetEmpresa(Empresa: Variant);
begin
     FEmpresa := Empresa;
     ConectaEmpresa( FEmpresa );
end;

function TdmZetaServerProvider.GetUsuario: Integer;
begin
     Result := FEmpresa[ P_USUARIO ];
end;

function TdmZetaServerProvider.GetCodigoEmpresaActiva: String;
begin
     Result := FEmpresa[ P_CODIGO ];
end;

function TdmZetaServerProvider.GetApplicationID: Integer;
begin
     try
        Result := FEmpresa[ P_APPID ];
     except
           on Error : Exception do
              Result := 0;
     end;
end;

procedure TdmZetaServerProvider.AsignaDataSetParams(oDataSet: TDataset );
var
    oDatasetParams : TParams;
    oDSParam : TParameter;
    oParam : TParam;
    i : Integer;
begin
    if ( oDataset is TOEQuery ) then
      oDataSetParams := TOEQuery( oDataset ).Params
    else
            raise Exception.Create( 'Data set no es v�lido' );

    // Barre la lista de par�metros del Dataset buscando
    // par�metros con el mismo nombre en FListaParams
    // Si existen, entonces asigna el Valor el param del Dataset
    with oDataSetParams do
        for i := 0 to Count-1 do
        begin
            oDSParam := Items[ i ];
            oParam := FParamList.FindParam( oDSParam.Name );
            if ( oParam <> NIL ) then
                oDSParam.Value := oParam.Value;
        end;
end;

procedure TdmZetaServerProvider.AsignaDataSetOtros(oDataSet: TDataset; oListaPares : Variant );
var
    oDatasetParams : TParams;
    i, iElementos : Integer;
begin
    if ( oDataset is TOEQuery ) then
      oDataSetParams := TOEQuery( oDataset ).Params
    else
            raise Exception.Create( 'Data set no es v�lido' );

    if ( VarIsArray( oListaPares )) then
    begin
        i := VarArrayLowBound( oListaPares, 1 );
        iElementos := VarArrayHighBound( oListaPares, 1 );
        while ( i < iElementos ) do
        begin
            oDataSetParams.ParamByName( oListaPares[ i ] ).Value :=
                oListaPares[ i + 1 ];
            // Pasa al siguiente 'par'
            i := i + 2;
        end;
    end;

end;

procedure TTablaInfo.SetMasterFields( Detalle : TOEQuery );
var
   i : Integer;
   sCampoMaster, sCampoDetail : String;
   sCampo, sSQL : String;
begin
    sSQL := 'select * from ' + FTabla + ' where ';
    with FListaForeignKey do
    begin
      for i := 0 to Count-1 do
      begin
           sCampoMaster := Strings[ i ];
           // Si se tiene un Par Name=Value
           if Pos( '=', sCampoMaster ) > 0 then
           begin
                sCampoMaster := Names[i];
                sCampoDetail := Values[ sCampoMaster ];
           end
           else
               sCampoDetail := FListaKey.Strings[ i ];
           sCampo := sCampoDetail + ' = :' + sCampoMaster;
           if ( i < Count-1 ) then
                sCampo := sCampo + ' and';

           sSQL := sSQL + sCampo + ' ';
      end;
    end;
    Detalle.SQL.Text := sSQL;
end;

{
var
    i : Integer;
    sCampoMaster, sMasterFields, sCampoDetail, sIndexFields : String;
  sMasterFields := '';
  sIndexFields  := '';
  with FListaForeignKey do
    for i := 0 to Count-1 do
    begin
        sCampoMaster := Strings[ i ];
        // Si se tiene un Par Name=Value
        if Pos( '=', sCampoMaster ) > 0 then
        begin
             sCampoMaster := Names[i];
             sCampoDetail := Values[ sCampoMaster ];
        end
        else
            sCampoDetail := FListaKey.Strings[ i ];

        if ( i < Count-1 ) then
        begin
             sCampoMaster := sCampoMaster + ';';
             sCampoDetail := sCampoDetail + ';';
        end;
        sMasterFields := sMasterFields + sCampoMaster;
        sIndexFields  := sIndexFields  + sCampoDetail;
    end;

    with Detalle do
    begin
        MasterFields    := sMasterFields;
        IndexFieldNames := sIndexFields;
    end;
}

procedure TdmZetaServerProvider.GetDatosPeriodo;
 const Q_DATOS_PERIODO = 'SELECT PE_YEAR, PE_TIPO, PE_NUMERO, '+
                         'PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, '+
{$ifdef QUINCENALES}
                         'PE_ASI_INI, PE_ASI_FIN, '+
                         'TP_DIAS_BT, TP_DIAS_7, TP_DIAS_EV, TP_HORASJO, '+
{$endif}
                         'TP_TOT_EV1, TP_TOT_EV2, TP_TOT_EV3, TP_TOT_EV4, TP_TOT_EV5, '+
                         'PE_DIAS, PE_USO, PE_STATUS, PE_MES, '+
                         'PE_INC_BAJ, PE_SOLO_EX, PE_AHORRO, PE_PRESTAM, '+
                         'PE_PER_MES, PE_POS_MES, PE_DIAS_AC,PE_CAL ' +
                         'FROM PERIODO '+
{$ifdef QUINCENALES}
                         'left outer join TPERIODO on TPERIODO.TP_TIPO = PERIODO.PE_TIPO ' +
{$endif}
                         'WHERE PE_YEAR = %d AND PE_TIPO = %d '+
                         'AND PE_NUMERO = %d ';
 var FDataSetPeriodo : TDataSet;
begin
     if FParamList = NIL then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if ( not FPeriodoObtenido ) then
     begin
          FDataSetPeriodo := CreateQuery;
          try
             if AbreQueryScript( FDataSetPeriodo,
                                 Format( Q_DATOS_PERIODO,
                                      [ fParamList.ParamByName('Year').AsInteger,
                                        fParamList.ParamByName('Tipo').AsInteger,
                                        fParamList.ParamByName('Numero').AsInteger ] )) then
             begin
                  with FDatosPeriodo, FDataSetPeriodo do
                  begin
                       Year:= FieldByName('PE_YEAR').AsInteger;
                       Tipo:= eTipoPeriodo(FieldByName('PE_TIPO').AsInteger);
                       Numero:= FieldByName('PE_NUMERO').AsInteger;
                       Inicio:= FieldByName('PE_FEC_INI').AsDateTime;
                       Fin:= FieldByName('PE_FEC_FIN').AsDateTime;
                       Pago:= FieldByName('PE_FEC_PAG').AsDateTime;
{$ifdef QUINCENALES}
                       InicioAsis := FieldByName( 'PE_ASI_INI' ).AsDateTime;
                       FinAsis := FieldByName( 'PE_ASI_FIN' ).AsDateTime;
                       TipoDiasBase := eTipoDiasBase( FieldByName( 'TP_DIAS_BT' ).AsInteger );
                       DiasBase := FieldByName( 'TP_DIAS_7' ).AsFloat;
                       FormulaDias := FieldByName( 'TP_DIAS_EV' ).AsString;
                       SumarJornadaHorarios := zStrToBool( FieldByName( 'TP_HORASJO' ).AsString );
{$endif}
                       Dias := FieldByName( 'PE_DIAS' ).AsInteger;
                       Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
                       Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
                       Mes := FieldByName( 'PE_MES' ).AsInteger;
                       SoloExcepciones := zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
                       IncluyeBajas := zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
                       DescuentaAhorros := zStrToBool( FieldByName( 'PE_AHORRO' ).AsString );
                       DescuentaPrestamos := zStrToBool( FieldByName( 'PE_PRESTAM' ).AsString );
                       PosMes := FieldByName( 'PE_POS_MES' ).AsInteger;
                       PerMes := FieldByName( 'PE_PER_MES' ).AsInteger;
                       DiasAcumula := FieldByName( 'PE_DIAS_AC' ).AsInteger;
                       EsCalendario := zStrToBool( FieldByName( 'PE_CAL' ).AsString );
                       EsUltimaNomina := (Uso = upOrdinaria) and (PosMes=PerMes);
                       FormulaTotal1 := FieldByName( 'TP_TOT_EV1' ).AsString;
                       FormulaTotal2 := FieldByName( 'TP_TOT_EV2' ).AsString;
                       FormulaTotal3 := FieldByName( 'TP_TOT_EV3' ).AsString;
                       FormulaTotal4 := FieldByName( 'TP_TOT_EV4' ).AsString;
                       FormulaTotal5 := FieldByName( 'TP_TOT_EV5' ).AsString;
                       HayFuncionesTotales := strLleno ( FormulaTotal1 ) or strLleno ( FormulaTotal2 ) or strLleno ( FormulaTotal3 ) or
                                              strLleno ( FormulaTotal4 ) or strLleno ( FormulaTotal5 );
                  end;
                  FMesDefault := FDatosPeriodo.Mes;
                  FPeriodoObtenido:= TRUE;
             end
             else raise Exception.Create('No existe periodo activo');
          finally
             FreeAndNil( FDataSetPeriodo );
          end;
     end;
end;

procedure TdmZetaServerProvider.GetDatosImss;
begin
     if FParamList = NIL then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else
     begin
          with FDatosImss, fParamList do
          begin
               Patron := ParamByName('RegistroPatronal').AsString;
               Year := ParamByName('ImssYear').AsInteger;
               Mes := ParamByName('ImssMes').AsInteger;
               Tipo := eTipoLiqIMSS(ParamByName('ImssTipo').AsInteger);
               {CV: Mientras no los requiera nadie, no se van a inicializar
               Status := eStatusLiqIMSS(Fields[0].AsInteger);
               Modifico := Fields[1].AsInteger;
               }
          end
     end;
end;

procedure TdmZetaServerProvider.GetDatosActivos;
begin
     if FParamList = NIL then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if FYearDefault = 0 then
          with FParamList do
          begin
               FFechaAsistencia := ParamByName('FechaAsistencia').AsDate;
               FFechaDefault := ParamByName('FechaDefault').AsDate;
               FYearDefault := ParamByName('YearDefault').AsInteger;
               FEmpleadoActivo := ParamByName('EmpleadoActivo').AsInteger;
               FNombreUsuario := ParamByName('NombreUsuario').AsString;
               FCodigoEmpresa := ParamByName('CodigoEmpresa').AsString;
          end;
end;

procedure TdmZetaServerProvider.GetEncabPeriodo;
begin
     if FParamList = NIL then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if FDatosPeriodo.Year = 0 then
          with FDatosPeriodo, FParamList do
          begin
               Year := ParamByName('Year').AsInteger;
               Tipo := eTipoPeriodo( ParamByName('Tipo').AsInteger );
               Numero := ParamByName('Numero').AsInteger;
          end;
end;

procedure TdmZetaServerProvider.InicializaValoresActivos;
begin
     FPeriodoObtenido:= FALSE;
     FYearDefault := 0;
     FDatosPeriodo.Year := 0;
end;

procedure TdmZetaServerProvider.InicializaValoresNomINFO;
begin
     FreeAndNil( FCdsPeriodosNomInfo );

     with FDatosNomInfo do
     begin
          dInicialRangoNomInfo := NullDateTime;
          dFinalRangoNomInfo := NullDateTime;
          Empleado := 0;
          CuotaBimestralNomInfo := 0;
          DiasCotizadosBimestreNomInfo := 0;
          SeguroViviendaNomInfo := 0;
          iYearIMSS := 0;
          iMesIMSS:= 0;
     end;
end;

procedure TdmZetaServerProvider.LiberaEmpleadosConAjuste;
begin
     FreeAndNil( FEmpleadosConAjuste );
end;

procedure TdmZetaServerProvider.AsignaParamList(oParams: OleVariant);
begin
     if FParamList = NIL then
        FParamList := TZetaParams.Create
     else FParamList.Clear;
     InicializaValoresActivos;
     FParamList.VarValues := oParams;
end;

function TdmZetaServerProvider.DescripcionParams: String;
var
   i : Integer;
begin
     Result := '';
     with FParamList do
          for i := 0 to Count-1 do
              with Items[ i ] do
                   Result := Result + Name + '=' + AsString + CR_LF;
end;

procedure TdmZetaServerProvider.EmpiezaTransaccion;
begin
    FConnection.StartTransact;
end;

procedure TdmZetaServerProvider.TerminaTransaccion(lCommit: Boolean);
begin
     FConnection.Commit;
     FConnection.EndTransact;
end;

procedure TdmZetaServerProvider.RollBackTransaccion;
begin
    FConnection.Rollback;
    FConnection.EndTransact;
    if ( Log <> NIL ) then
        Log.HuboRollBack;
end;

procedure TdmZetaServerProvider.AsignaParamsDataSet(Origen, Destino: TDataset; const sExcluidos: String = '' );
var
   i: Integer;
   sField: String;
begin
     with Origen do
     begin
          for i := ( FieldCount - 1 ) downto 0 do
          begin
               sField := Fields[ i ].FieldName;
               if ( sExcluidos = '' ) or ( Pos( sField, sExcluidos ) = 0 ) then
                  AsignaParamCampo( GetParametro( Destino, sField ), Fields[ i ] );
          end;
     end;
end;

procedure TdmZetaServerProvider.ActualizaTabla( OrigenDS: TDataSet; UpdateKind: TUpdateKind );
begin
     FTablaInfo.ConstruyeUpdateSQL( UpdateKind, OrigenDS, qryCambios );
     qryCambios.ExecSQL;
end;


{$ifdef VER130}
procedure TdmZetaServerProvider.prvUnicoUpdateData(Sender: TObject; DataSet: TClientDataset);
{$else}
procedure TdmZetaServerProvider.prvUnicoUpdateData(Sender: TObject; DataSet: TCustomClientDataset);
{$endif}
begin
     with FTablaInfo do
          if Assigned( OnUpdateData ) then
             OnUpdateData( Sender, DataSet );

{ No funciona porque se altera el ORDEN del ClientDataSet
     oCampo := DataSet.FindField( 'qryDetail' ) as TDataSetField;
     if ( oCampo <> NIL ) then
       with oCampo.NestedDataSet do
         while not Eof do
         begin
            Edit;
            FieldByName( 'TB_CODIGO' ).AsString := DataSet.FieldByName( 'TB_CODIGO' ).AsString;
            Post;
            Next;
         end;
}
end;

{$ifdef VER130}
procedure TdmZetaServerProvider.prvUnicoBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
{$else}
procedure TdmZetaServerProvider.prvUnicoBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset; UpdateKind: TUpdateKind; var Applied: Boolean);
{$endif}
begin
     // Llama al BeforeUpdateRecord del programador
     with FTablaInfo do
     begin
          if Assigned( BeforeUpdateRecord ) then
             BeforeUpdateRecord( Sender, SourceDS, DeltaDS, UpdateKind, Applied );

          if ( not Applied ) then
             UpdateTabla( Sender, SourceDS, DeltaDS, UpdateKind, Applied );
     end;
end;

{ NOTA: Esto se tiene que hacer porque el ADO regresa los campos
  tipo CHAR con espacios a la derecha, a diferencia de los VARCHAR.
  Con el BDE esto no sucede.
  Se tiene que barrer todo el DataSet para hacerle un TRIM a los
  campos tipo 'String' identificados como 'FixedChar' }
{$ifdef VER130}
procedure TdmZetaServerProvider.prvUnicoGetData(Sender: TObject; DataSet: TClientDataset);
{$else}
procedure TdmZetaServerProvider.prvUnicoGetData(Sender: TObject; DataSet: TCustomClientDataset);
{$endif}
begin
{ ODBCExpress

var
    i : Integer;
    oField : TField;
    oListaFixed : TList;
    oListaFixed := TList.Create;
begin
    with DataSet do
    begin
        for i := FieldCount-1 downto 0 do
        begin
            oField := Fields[ i ];
            if ( oField.DataType = ftString ) and
                TStringField( oField ).FixedChar then
                oListaFixed.Add( oField );
        end;

        if ( oListaFixed.Count > 0 ) then
          while not Eof do
          begin
              Edit;
              for i := oListaFixed.Count-1 downto 0 do
                with TField( oListaFixed.Items[ i ] ) do
                    AsString := Trim( AsString );
              Post;
              Next;
          end;
    end;
    oListaFixed.Free;
}
end;

{$ifdef VER130}
procedure TdmZetaServerProvider.prvUnicoAfterUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataset; UpdateKind: TUpdateKind);
{$else}
procedure TdmZetaServerProvider.prvUnicoAfterUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataset; UpdateKind: TUpdateKind);
{$endif}
begin
     with FTablaInfo do
     begin
          if Assigned( AfterUpdateRecord ) then
             AfterUpdateRecord( Sender, SourceDS, DeltaDS, UpdateKind )
     end;
end;

{$ifdef VER130}
procedure TdmZetaServerProvider.prvUnicoUpdateError(Sender: TObject; DataSet: TClientDataset; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
{$else}
procedure TdmZetaServerProvider.prvUnicoUpdateError(Sender: TObject; DataSet: TCustomClientDataset; E: EUpdateError; UpdateKind: TUpdateKind; var Response: TResolverResponse);
{$endif}
begin
     with FTablaInfo do
          if Assigned( OnUpdateError ) then
             OnUpdateError( Sender, DataSet, E, UpdateKind, Response );
end;

procedure TdmZetaServerProvider.BorraCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora;  Dataset: TDataSet; const iEmpleado: integer; const sMasInfo : string );
begin
    EscribeBitacora( tbNormal, eClase, iEmpleado, NullDateTime, 'Borrado de ' + sMensaje, sMasInfo + GetDatasetInfo( Dataset, TRUE ));
end;

procedure TdmZetaServerProvider.CambioCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora;  Dataset: TDataSet; const iEmpleado: integer; const sMasInfo : string );
begin
    EscribeBitacora( tbNormal, eClase, iEmpleado, NullDateTime, 'Modificaci�n de ' + sMensaje, sMasInfo + GetDatasetInfo( Dataset, FALSE ));
end;

procedure TdmZetaServerProvider.EscribeBitacora( const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp; dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
begin
     InitGlobales;
     if ZetaServerTools.RegistrarBitacora( eClase, GetGlobalString( K_GLOBAL_GRABA_BITACORA ) ) then
     begin
          if ( FQryBitacora = nil) then
             FQryBitacora := CreateQuery( K_WRITE_SIMPLE_LOG );

          EscribeParametrosBitacora ( FQryBitacora, Ord(eTipo), Ord( eClase ), iEmpleado, dMovimiento, sMensaje, sTexto, UsuarioActivo );
     end;
end;

procedure TdmZetaServerProvider.CambioCatalogoComparte( const sMensaje: TBitacoraTexto; const eClase: eClaseSistBitacora; Dataset: TDataSet; const iUsuario: Integer );
begin
     EscribeBitacoraComparte( tbNormal, eClase, NullDateTime, 'Modificaci�n de ' + sMensaje, GetDatasetInfo( Dataset, FALSE ), iUsuario);
end;

procedure TdmZetaServerProvider.BorraCatalogoComparte(const sMensaje: TBitacoraTexto; const eClase: eClaseSistBitacora; Dataset: TDataSet; const iUsuario: Integer );
begin
     EscribeBitacoraComparte( tbNormal, eClase, NullDateTime, 'Borrado de ' + sMensaje, GetDatasetInfo( Dataset, TRUE ), iUsuario);
end;

procedure TdmZetaServerProvider.EscribeBitacoraComparte(const eTipo: eTipoBitacora; const eClase: eClaseSistBitacora; dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String; const iUsuario: Integer);
begin
     if ( FQryBitacoraSistema = nil ) then
        FQryBitacoraSistema := CreateQuery( K_WRITE_SIMPLE_LOG );

     EscribeParametrosBitacora ( FQryBitacoraSistema, Ord(eTipo), Ord( eClase ), 0, dMovimiento, sMensaje, sTexto, iUsuario );
end;

procedure TdmZetaServerProvider.EscribeBitacoraKiosco(const eTipo: eTipoBitacora; const sEmpresa: String;
                                                             const eClase: eClaseBitacora; const sTexto, sMarco, sKiosco: String; const iEmpleado: Integer);
begin
     InitGlobales;
     if ZetaServerTools.RegistrarBitacora( eClase, GetGlobalString( K_GLOBAL_GRABA_BITACORA ) ) then
     begin
          FQryBitacoraKiosco := CreateQuery( K_AGREGA_BITACORA_KIOSCO );
          try
             ParamAsInteger( FQryBitacoraKiosco, 'CB_CODIGO', iEmpleado );
             ParamAsVarChar( FQryBitacoraKiosco, 'CM_CODIGO', sEmpresa, K_ANCHO_OPERACION );
             ParamAsInteger( FQryBitacoraKiosco, 'BI_TIPO', Ord( eTipo ) );
             ParamAsInteger( FQryBitacoraKiosco, 'BI_ACCION', Ord( eClase ) );
             ParamAsInteger( FQryBitacoraKiosco, 'BI_NUMERO', 0 );
             ParamAsVarChar( FQryBitacoraKiosco, 'BI_TEXTO', sTexto, K_ANCHO_FORMULA );
             ParamAsVarChar( FQryBitacoraKiosco, 'BI_UBICA', sMarco, K_ANCHO_OBSERVACIONES  );
             ParamAsVarChar( FQryBitacoraKiosco, 'BI_KIOSCO', sKiosco, K_ANCHO_COMPUTERNAME  );
             ParamAsDate( FQryBitacoraKiosco, 'BI_FEC_MOV', NullDateTime );
             Ejecuta( FQryBitacoraKiosco );
          except
                raise;
          end;
     end;
end;

procedure TdmZetaServerProvider.EscribeParametrosBitacora ( oQryInsert: TDataSet; const iTipo: Integer; const iClase: Integer; const iEmpleado: TNumEmp; dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String; const iUsuario: Integer );
var
    dBitacora : TDateTime;
    sData : String;
begin
     try
        dBitacora := Now;
        sData := sTexto;
        if strVacio( sData ) then
           sData := ' ';                  // Si no marca error al grabar el Blob
        if ( dMovimiento = 0 ) then
           dMovimiento := dBitacora;

        ParamAsInteger( oQryInsert, 'US_CODIGO', iUsuario );
        ParamAsDate(    oQryInsert, 'BI_FECHA', Trunc( dBitacora ));
        ParamAsVarChar( oQryInsert, 'BI_HORA', TimeToStrSQL( dBitacora ), K_ANCHO_LOG_HORA );
        ParamAsInteger( oQryInsert, 'BI_TIPO', iTipo );
        ParamAsInteger( oQryInsert, 'BI_CLASE', iClase );
        ParamAsVarChar( oQryInsert, 'BI_TEXTO', Copy( sMensaje, 1, K_ANCHO_LOG_TEXTO ), K_ANCHO_LOG_TEXTO );
        ParamAsInteger( oQryInsert, 'CB_CODIGO', iEmpleado );
        ParamAsDate(    oQryInsert, 'BI_FEC_MOV', dMovimiento );
        ParamAsBlob(    oQryInsert, 'BI_DATA', sData );
        Ejecuta( oQryInsert );

     except
           raise;
     end;
end;

function TdmZetaServerProvider.GetDatasetInfo(Dataset: TDataset; const lBorrar : Boolean; sConfidenciales: String ): String;
var
    i : Integer;
    sCampo, sOld, sNew : String;
begin
    Result := '';
    with DataSet do
         for i := 0 to FieldCount-1 do
         begin
              if ( DataSet.fields[i].DataType <> ftDataset ) then
              begin
                   sOld := ZetaServerTools.CampoOldAsVar( Fields[ i ] );
                   sNew := ZetaServerTools.CampoAsVar( Fields[ i ] );
                   with Fields[ i ] do
                        if ( lBorrar ) or ( not IsNull and ( sNew <> sOld ) ) then
                        begin
                             if strLleno ( sConfidenciales ) then
                             begin
                                  if( Pos(Fields[i].FieldName,sConfidenciales) > 0 ) then
                                  begin
                                       sNew:= Replicate('*',Length(sNew));
                                       sOld:= Replicate('*',Length(sOld));
                                  end;
                             end;
                             // (JB) Se realiza la validacion que el Delta no traiga un campo llave
                             //      Ya que en el detail de tablas se realiza un select * from y es
                             //      posible que traiga campo llave. Fue Validado con ER.
                             if ( FieldName <> 'LLAVE' ) then
                             begin
                                  if ( lBorrar ) then
                                      sCampo := FieldName + ' = ' + sOld
                                  else
                                      sCampo := FieldName + CR_LF + ' De: ' + sOld + CR_LF + ' A : ' + sNew;
                                  Result := Result + sCampo + CR_LF;
                             end;
                        end;
              end;
         end;
end;

function TdmZetaServerProvider.NombreNivel(const Index: Integer): String;
begin
    Result := FNombreNiveles[ Index ];
end;

function TdmZetaServerProvider.NumNiveles: Integer;
const
    //  K_GLOBAL_NIVEL1  = 13; { Nombre de Nivel #1}
    //  K_GLOBAL_NIVEL9  = 21; { Nombre de Nivel #9}

    {$ifdef ACS}
    Q_GET_NIVELES = 'select GL_FORMULA from GLOBAL where ( GL_CODIGO >= 13 and GL_CODIGO <= 21 ) and ( GL_CODIGO >= 234 and GL_CODIGO <= 236) order by GL_CODIGO';
    {$else}
    Q_GET_NIVELES = 'select GL_FORMULA from GLOBAL where GL_CODIGO >= 13 and GL_CODIGO <= 21 order by GL_CODIGO';
    {$endif}
var
    oQueryNiveles : TDataset;
    sNivel : String;
begin
    if ( FNumeroNiveles = 0 ) then
    begin
        oQueryNiveles := CreateQuery( Q_GET_NIVELES );
        try
           with oQueryNiveles do
           begin
               Active := TRUE;
               while not Eof do
               begin
                   sNivel := Fields[ 0 ].AsString;
                   if StrLleno( sNivel ) then
                   begin
                       Inc( FNumeroNiveles );
                       FNombreNiveles[ FNumeroNiveles ] := sNivel;
                   end
                   else
                       break;
                   Next;
               end;
           end;
        finally
           FreeAndNil( oQueryNiveles );
        end;
    end;
    Result := FNumeroNiveles;
end;


procedure TdmZetaServerProvider.qryDetailBeforeOpen(DataSet: TDataSet);
var i : Integer;
begin
    // ODBCExpress marca error si no se especifican los Tipos de los Par�metros
    with qryDetail do
    begin
        // S�lo se tiene que hacer para el primer Registro del Master
        if Params[ 0 ].DataType = ftUnknown then
          for i := 0 to ParamCount-1 do
            Params[ i ].DataType := qryMaster.FieldByName( Params[i].Name ).DataType;
    end;
end;



procedure TdmZetaServerProvider.HdbcEmpresaBeforeConnect(Sender: TObject);
const
     SQL_PRESERVE_CURSORS = 1204;
     SQL_PC_ON = 1;
begin
{$ifdef MSSQL}
     SQLSetConnectAttr(THdbc( Sender ).Handle, SQL_PRESERVE_CURSORS, Pointer(SQL_PC_ON), 0);
{$endif}
end;

function TdmZetaServerProvider.GetCodigoGrupo: integer;
begin
     Result := FEmpresa[ P_GRUPO ];
end;

{ M�todos para llenar el arrego de aTipoPeriodo y aTipoNomina } 
 
{acl} 
function TdmZetaServerProvider.GetTiposPeriodo(Empresa: Variant ): OLEVariant; 
begin 
     Result:=  OpenSQL( Empresa, 'select TP_TIPO, TP_NOMBRE, TP_DESCRIP, TP_NIVEL0 from TPERIODO', true ) 
end;

{$IFNDEF TRESS_DELPHIXE5_UP}
//Ocupamos la lista de objetos y la lista de strings para los combos que devuelven la descripcion.
procedure TdmZetaServerProvider.InitArregloTPeriodo;
var
   sValorLista: String;
   iPos: Integer;
begin
     { Inicializa Lista Tipo Periodo: Para llamar este m�todo la propiedad de
       Empresa tiene que estar asignada y tiene que ser una empresa de Tress. }

     with TClientDataset.Create(Self) do
     begin
          try
             Data := GetTiposPeriodo(FEmpresa);
             if not Assigned( FArregloPeriodo ) then FArregloPeriodo := TStringList.Create;
             if not Assigned( FArregloTipoNomina ) then FArregloTipoNomina := TStringList.Create;
             FArregloPeriodo.BeginUpdate;
             FArregloTipoNomina.BeginUpdate;

             FArregloPeriodo.Clear;
             FArregloTipoNomina.Clear;

             while not EOF do
             begin
                  sValorLista:= FieldByName('TP_DESCRIP').AsString;
                  iPos:= Pos ( '/', sValorLista );

                  //Llena lista del periodo
                  if ( ( iPos > 0 ) and ( sValorLista[iPos+1] <> '' ) and ( sValorLista[iPos-1] <> '' ) ) then
                       FArregloPeriodo.Add(FieldByName('TP_TIPO').AsString + '=' + Copy( sValorLista, 1, iPos - 1 ))   //Ejemplo: 1 = Semanal
                  else
                       FArregloPeriodo.Add(FieldByName('TP_TIPO').AsString + '=' + sValorLista);

                  //Llena lista de la nomina
                  FArregloTipoNomina.Add(FieldByName('TP_TIPO').AsString + '=' + Copy( sValorLista, iPos + 1, Length ( sValorLista )));  //Ejemplo: 1 = Semana
                  Next;
             end;
          finally
                 Free; // Liberar el ClientDataSet creado
          end;
     end;
end;
{$ELSE}
procedure TdmZetaServerProvider.InitArregloTPeriodo;
var
   oListaTPeriodo: TStrings;
begin
     {Inicializa Lista Tipo Periodo: Para llamar este m�todo la propiedad de
      Empresa tiene que estar asignada y tiene que ser una empresa de Tress. }
 
     oListaTPeriodo:= TStringList.Create;
     with TClientDataSet.Create( Self ) do
     begin
          try
             Data:= GetTiposPeriodo( FEmpresa ); 
             while not Eof do 
             begin 
                  oListaTPeriodo.Add( FieldByName( 'TP_TIPO' ).AsString + '=' + FieldByName( 'TP_DESCRIP' ).AsString );
                  Next; 
             end; 
             LlenaTipoPeriodo( oListaTPeriodo );
          finally 
                 Free;             // Liberar el ClientDataSet creado 
                 FreeAndNil( oListaTPeriodo );
          end; 
     end;

end;
{$ENDIF}

procedure TdmZetaServerProvider.SetReadUncommited;
begin
     HdbcEmpresa.IsolationLevel := 1; //Sirve para ejecutar queries sobre registros que no todavia no tienen el Commit.
end;


end.


