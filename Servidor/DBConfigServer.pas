unit DBConfigServer;

interface

uses
     SysUtils, Classes, DB,
     {$ifndef DOTNET}Windows, Messages, Graphics, Controls,Forms, Dialogs,{$endif}
     ZetaCommonLists,
     ZetaCommonClasses,
     DZetaServerProvider,
     ZetaRegistryServer,
     ZetaLicenseClasses,
     FAutoServer;

type
  TEmpresaConfig = class ( TObject )
  public
        Codigo, Datos, ControlReal, Control, CheckSum, Digito: string;
        OldCodigo, OldDatos, OldControlReal, OldControl, OldCheckSum : string;
        lAcumula : boolean;
  end;

type
  TDBConfigServer = class(TObject, IDBConfigStoreHandler)
  private
    { Private declarations }
    FAutoServer : TAutoServer;

    FRegistry: TZetaRegistryServer;
    FGlobalLicense  : TGlobalLicenseValues;
    FDBConfigMgr    : TDbConfigManager;

    FTipos: TStrings;
    FNombres: TStrings;
    FProvider: TdmZetaServerProvider;

    function EjecutaScriptComparte(sScript: string): boolean;

  public
     constructor Create( oProvider: TdmZetaServerProvider ; oAutoServer : TAutoServer );
     destructor Destroy; override;

     property AutoServer : TAutoServer read FAutoServer;
     property Registry: TZetaRegistryServer read FRegistry;
     property DBConfigMgr: TDbConfigManager read FDBConfigMgr;

     procedure SetAutoServer( value : TAutoServer );

     function ExisteKeyEnBD( iKey : integer ) : Boolean;
     function ExistenKeys : Boolean;


     function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
     function _AddRef: Integer; stdcall;
     function _Release: Integer; stdcall;


     function EsDbConfigInicial : boolean;
     function ReadDBConfigFromDatabase( iKey : integer) : string;
     function WriteDBConfigFromDatabase( iKey : integer; encryptedValue : string ) : boolean;
     function ReadRegistryComparteBD : string;
     function ReadRegistryChecksum : string;
     function WriteRegistryChecksum( encryptedValue : string ) : boolean;


     function ValidarAltaBDProduccion(  var sAdvertencia : string )  : boolean;
     function IncrementarAltaBDProduccion( oEmpresa : TEmpresaConfig ) : boolean;
     function ContarDuplicidadBD( oEmpresa : TEmpresaConfig ): integer;
     function ContarConfiguracionBDProduccion( oEmpresa : TEmpresaConfig ): integer;

     //Utilitarias
     function GetTipoCompanyName( const sValor: String ): eTipoCompany;
     function EsValidoOldChecksum(oEmpresa : TEmpresaConfig )  : Boolean;


     //operacional
     procedure BeforeEmpresaObjPost(oEmpresa : TEmpresaConfig; UpdateKind: TUpdateKind; var sAdvertencia : string);
     procedure AfterEmpresaObjPost(oEmpresa : TEmpresaConfig );     
  end;



implementation

uses ZetaCommonTools,
     ZetaServerTools;


     const

     K_QTY_DB_KEYS = 11;

     DB_KEYS : array[0..10] of string =
         ( 'GET_MINUTOS_LOG_S', 'FN_HORAS_MINUTOS', 'GET_TIMESTAMP_TRESS', 'GET_TRESS_AROUND' , 'SP_COMPARTE_INVESTIGA',
           'FN_JEFE_US_CODIGO', 'SP_HORAS_NT_COMPARTE', 'SP_AS_C',  'FN_CONFLICTO_EMPRESA' , 'FN_US_CODIGO_JEFE', 'GET_REPORTE_NIVEL');



     Q_CREATE_KEY         = 'create function %s() returns Accesos WITH ENCRYPTION as begin  return ''%s''  end ';
     Q_CREATE_KEY_REPORTE_CRYPT  = 'D0598A95B37AEF63828E91B550FC3E9CCFA492BA4ED76FE607253CE942E055DB609592F16BC674A75B92CE9DAB62AB54C174C6DF73D832FA2ED90925A8BB49C1598786B74B5B081424151C46CE4FA2'+
                            '87AB82BB44E97DCF01076CB770967AAD9F98719CC580E15883BE0519CE97B998C553CD3FFE18202A0B513F0D265C8593659B74A795C95F85CA7F85A346C13BFA1B16081A030A0B62B24D0C583938E4'+
                            '1909422910133DC96A8494A289C963E940D173AAB992C28FD9984EE166A850C78AEB2AC96BA99CA178ADBD6AEE105FDF62C97287AC453F580B5FB175D486E376D44D88946CCD71D4A9C87C8EA89B9D'+
                            'BE9BE923307202193FC97ABE1D4E25F03EEC61ED618597A090B0B4728396D357FD2B10141D350F085B2FE12504563321BF5A9B7889E85C393245FB002A1323301567A0B3F77887AD5888A827341F46'+
                            'C76BA08B80F539D351CE55D26390E147DD4AF57BE14F2A7AF0110E432A3035F16B92BC501D1B412D2B44E16CD496F762CEBCF58D4BC9538981AA9888DB0B14EE5CD56390E340C66EC254D25DEE0825'+
                            '300C32C76BF76BA3BF94E423C892FF728283A767A14F151765BC63F33DEF0350350700531759A64DBB74B413402B32FB1FF466FE7DAFB28EB9BF65FF0150E07BC67D85AE93EE3F3BEF2F220B51D411'+
                            '4AB352C78A86DC41DA1D1810200C72E84C2777F3B753D50BFA28C4D487EC28E7163ED477E65CE04AE67BA0AB5281B74CE05CDC55EC60B892F97A8EB5AF92E0110C3ECD5E8BD14A386FCC54DA7286B3'+
                            '9FED3E147FF1974BCA7ABD001D76EF7DA079AD89EC4EE651C968D1BCB7E783A777FA62F87CB812FB2AEF7DFB66C85E9D98A791BEA4BE9B9686FC4DC641F410C764B4A78C8986B4989BBB2B1D270C3F'+
                            'CA6EF41B5ACA5BD14FE05CEA7EAEA585DC88B06DC8A2F240E536C166A88D9888DC69CF4DD867CBB9E960CBADDB0F0EC76FD13318410C31E91C186ECF64C07999A6B0A6DF54E831161A1A1A10095A31'+
                            '43D87F89BB7CDA89FB5AF8580267D5C596FD67EA4A1075C7D784EF69E4441E7BC9D98AE17BF6560C69DBCB42FB19';

     Q_CREATE_KEY_REPORTE = 'create function  GET_REPORTE_NIVEL( @nivel Codigo )  returns Formula  WITH ENCRYPTION '+ CR_LF +
                            'as '+CR_LF +
                            'begin '+     CR_LF +
                            '	if ( @nivel = ''PLANTA''  ) '+CR_LF +
                            '		return dbo.GET_MINUTOS_LOG_S() '+ CR_LF +
                            '	if ( @nivel = ''DEPTO''  ) '+CR_LF +
                            '		return  dbo.FN_HORAS_MINUTOS() '+CR_LF +
                            '	if ( @nivel = ''OFICNA''  ) '+CR_LF +
                            '		return  dbo.GET_TIMESTAMP_TRESS() '+CR_LF +
                            '	if ( @nivel = ''CCOSTO''  ) '+CR_LF +
                            '		return   dbo.GET_TRESS_AROUND () '+CR_LF +
                            '	if ( @nivel = ''LINEA''  ) '+CR_LF +
                            '		return   dbo.SP_COMPARTE_INVESTIGA() '+CR_LF +
                            '	if ( @nivel = ''SUPER''  ) '+CR_LF +
                            '		return   dbo.FN_JEFE_US_CODIGO() '+CR_LF +
                            '	if ( @nivel = ''GRNCIA''  ) '+CR_LF +
                            '		return   dbo.SP_HORAS_NT_COMPARTE() '+CR_LF +
                            '	if ( @nivel = ''PRODUC''  ) '+CR_LF +
                            '		return   dbo.SP_AS_C() '+CR_LF +
                            '	if ( @nivel = ''CONTAB''  ) '+CR_LF +
                            '		return   dbo.FN_CONFLICTO_EMPRESA () '+CR_LF +
                            '	if ( @nivel = ''EMPLD''  ) '+CR_LF +
                            '		return   dbo.FN_US_CODIGO_JEFE()  '+CR_LF +
                            '	return '''';                                '+CR_LF +
                            ' end ';

     Q_DROP_KEY = 'drop function %s';
     Q_GET_KEY_REPORTE = 'select VALOR=dbo.%s(%s)';
     DB_KEYS_REPORTE : array[0..9] of string = ( 'PLANTA', 'DEPTO', 'OFICNA', 'CCOSTO' , 'LINEA',  'SUPER', 'GRNCIA', 'PRODUC',  'CONTAB' , 'EMPLD');
     Q_EXISTE_KEY = 'select count(*) as CUANTOS from sysobjects where id = object_id(N''%s'') and xtype in (N''FN'', N''IF'', N''TF'')';
     N_EXISTE_KEY_REPORTE = 10 ;
     Q_EXISTE_KEY_REPORTE = 'select count(*) as CUANTOS from sysobjects where id = object_id(N''GET_REPORTE_NIVEL'') and xtype in (N''FN'', N''IF'', N''TF'')';


     Q_CUENTA_CONFIG_BD_PROD = 'SELECT count(*) AS CUANTOS  from DB_INFO where UPPER(DB_DATOS) = UPPER( ''%s'' ) and ( DB_CONTROL = ''3DATOS''  or DB_CONTROL = '''' )  and ( DB_CTRL_RL = ''3DATOS'' or DB_CTRL_RL = ''''  ) ';
     Q_VALIDA_CONFIG_BD_PROD = 'SELECT DB_CODIGO, DB_CONTROL, DB_DATOS, DB_CTRL_RL, DB_CHKSUM  from DB_INFO where UPPER(DB_DATOS) = UPPER( ''%s'' ) and ( DB_CONTROL = ''3DATOS''  or DB_CONTROL = '''' )  and ( DB_CTRL_RL = ''3DATOS'' or DB_CTRL_RL = '''' ) ';
     Q_CUENTA_DUPLICIDAD_BD = 'SELECT count(*) AS CUANTOS  from DB_INFO where UPPER(DB_DATOS) = UPPER( ''%s'' ) and DB_CONTROL in (%s) and ( DB_CTRL_RL not in (%s) ) and DB_CODIGO <> ''%s'' ';



procedure TDBConfigServer.BeforeEmpresaObjPost(oEmpresa : TEmpresaConfig; UpdateKind: TUpdateKind; var sAdvertencia : string);
var
    FInsert: Boolean;
    function setCheckSumInternal: String;
    begin
         with oEmpresa do
         begin
            CheckSum := ZetaServerTools.GetCompanyChecksumFromData(Codigo, ControlReal, Control, Datos, Now);
         end;
    end;

    begin

     oEmpresa.lAcumula  := FALSE;   //Si Es nuevo Acumula
     FInsert := FALSE;

     if  UpdateKind in [ ukInsert ] then
        FInsert := TRUE;

     with oEmpresa  do
     begin
          if ( GetTipoCompanyName( Control ) <> tc3Datos ) then
          begin
             Digito := VACIO;
             setCheckSumInternal;
          end
          else
          begin
             if ( FInsert ) or  ( EsValidoOldChecksum(oEmpresa) ) then
             begin
                setCheckSumInternal;
             end
             else
             begin
                  ControlReal := '3PRUEBA';
                  setCheckSumInternal;
             end;

             if  (FInsert) or ( OldControlReal <> ControlReal  )  or
                 ( UpperCase( OldDatos )  <> UpperCase( Datos )   )  then
             begin
                  if (  ControlReal = '3DATOS' ) then
                  begin
                     oEmpresa.lAcumula := TRUE;
                  end;

                  if  StrLleno( Datos ) and  ( ContarConfiguracionBDProduccion( oEmpresa )  = 0 ) then
                  begin
                     if  ( ControlReal = '3DATOS' )  and ( not ValidarAltaBDProduccion(sAdvertencia)  ) then
                     begin
                         if ( AutoServer.EsDemo ) then
                            DatabaseError( 'No es posible agregar bases de datos de tipo Tress en modo DEMO')
                         else
                             DatabaseError( 'Seg�n los t�rminos de su licencia se ha agotado el n�mero de altas permitidas de bases de datos de tipo Tress');
                     end;
                  end;
             end;
          end;
     end;
end;



{ ******** TdmDBConfigServer ********** }

function TDBConfigServer.ExisteKeyEnBD(iKey: integer): Boolean;
var
   sQuery : string;
   tqScriptComparteAux : TZetaCursor;
 {$ifdef DEBUG_KEYS}
       slKeys : TStringList;
 {$endif}

begin

   Result := False;
       {$ifdef DEBUG_KEYS}
       slKeys := TStringList.Create;
       if (FileExists('C:\TEMP\KEYS2.TXT') ) then
          slKeys.LoadFromFile('C:\TEMP\KEYS2.TXT');
       {$endif}
   try
          if ( iKey = N_EXISTE_KEY_REPORTE ) then
          begin
             sQuery := Q_EXISTE_KEY_REPORTE;
          end
          else
          begin
              sQuery := Format(Q_EXISTE_KEY, [ DB_KEYS[iKey] ] );
          end;

          with FProvider do
          begin
               tqScriptComparteAux := CreateQuery( sQuery );
          end;

          with tqScriptComparteAux do
          begin
              try
                  Active := True;
                  while not Eof do
                  begin
                       Result := FieldByName( 'CUANTOS' ).AsInteger > 0 ;
                       Next;
                  end;
               finally
                      Active := False;
                      FreeAndNil( tqScriptComparteAux ); 
               end;
          end;
   except
         on Error: Exception do
         begin
              {$ifdef DEBUG_KEYS}
              slKeys.Add('Error en ExisteKeyEnBD: ' + Error.Message);
              slKeys.SaveToFile('C:\TEMP\KEYS2.TXT');
              FreeAndNil( slKeys );
              {$endif}
              Result := False;
         end;
   end;
end;

function TDBConfigServer.ExistenKeys: Boolean;
var
    iKey : integer;
begin
   Result := False;
   for iKey:= 0 to 9 do
   begin
      if ExisteKeyEnBD( iKey ) then
         Result := True;
   end;
end;

function TDBConfigServer.EsDbConfigInicial: boolean;
begin
     Result := not ExistenKeys;
end;

function TDBConfigServer.ReadDBConfigFromDatabase(iKey: integer): string;
var
   sQuery : string;
   tqScriptComparteAux : TZetaCursor;
 {$ifdef DEBUG_KEYS}
       slKeys : TStringList;
 {$endif}

begin
//Ejecuta un Selecto from Funcion
       {$ifdef DEBUG_KEYS}
       slKeys := TStringList.Create;
       if (FileExists('C:\TEMP\KEYS2.TXT') ) then
          slKeys.LoadFromFile('C:\TEMP\KEYS2.TXT');
       // slKeys.Add( DateTimeToStr( Self.FGlobalLicenseValues.FechaActual  ) );
       {$endif}


   Result := VACIO;
   try
      try

         sQuery := Format(Q_GET_KEY_REPORTE, [ 'GET_REPORTE_NIVEL', EntreComillas( DB_KEYS_REPORTE[iKey] ) ] ) ; 

         tqScriptComparteAux := FProvider.CreateQuery( sQuery );

         with tqScriptComparteAux do
         begin
            Active := True;
            while not Eof do
            begin
                 Result := FieldByName( 'VALOR' ).AsString;
                 Next;
            end;
         end;
      finally
             FreeAndNil( tqScriptComparteAux );
      end;
   except
         on Error: Exception do
         begin
              {$ifdef DEBUG_KEYS}
              slKeys.Add('Error en ReadDBConfigFromDatabase: ' + Error.Message);
              slKeys.SaveToFile('C:\TEMP\KEYS2.TXT');
              FreeAndNil( slKeys );
              {$endif}

              Result := VACIO;
         end;
   end;

end;

function TDBConfigServer.ReadRegistryChecksum: string;
{$ifdef DEBUG_KEYS}
var
 slKeys : TStringList;
{$endif}
begin
  try
       {$ifdef DEBUG_KEYS}
       slKeys := TStringList.Create;
       if (FileExists('C:\TEMP\KEYS2.TXT') ) then
          slKeys.LoadFromFile('C:\TEMP\KEYS2.TXT');
       {$endif}
     Result := Registry.ChecksumRaw;
  except
      on Error: Exception do
      begin

              {$ifdef DEBUG_KEYS}
              slKeys.Add('Error en WriteRegistryChecksum: ' + Error.Message);
              slKeys.SaveToFile('C:\TEMP\KEYS2.TXT');
              FreeAndNil( slKeys );
              {$endif}
           Result := VACIO;
      end;
  end;
end;

function TDBConfigServer.ReadRegistryComparteBD: string;
begin
  try
     Result := UpperCase( Registry.Database );
  except
      on Error: Exception do
      begin
           Result := VACIO;
      end;
  end;
end;

function TDBConfigServer.WriteDBConfigFromDatabase(iKey: integer;
  encryptedValue: string): boolean;
begin
   //Borra la funcion anterior
   if ( iKey >= 0 ) and ( iKey <= 9 ) then
   begin
      if ExisteKeyEnBD( iKey ) then
            EjecutaScriptComparte( Format ( Q_DROP_KEY , [DB_KEYS[iKey]] ));
      Result := EjecutaScriptComparte( Format ( Q_CREATE_KEY , [DB_KEYS[iKey], encryptedValue] ))
   end
   else
       Result := False;

   if (not ExisteKeyEnBD( N_EXISTE_KEY_REPORTE )) then
      EjecutaScriptComparte( Q_CREATE_KEY_REPORTE )

end;

function TDBConfigServer.WriteRegistryChecksum(
  encryptedValue: string): boolean;
{$ifdef DEBUG_KEYS}
var
 slKeys : TStringList;
{$endif}
begin
   try
       {$ifdef DEBUG_KEYS}
       slKeys := TStringList.Create;
       if (FileExists('C:\TEMP\KEYS2.TXT') ) then
          slKeys.LoadFromFile('C:\TEMP\KEYS2.TXT');
       {$endif}

        Registry.ChecksumRaw := encryptedValue;
        Result := True;
     except
         on Error: Exception do
         begin
              {$ifdef DEBUG_KEYS}
              slKeys.Add('Error en WriteRegistryChecksum: ' + Error.Message);
              slKeys.SaveToFile('C:\TEMP\KEYS2.TXT');
              FreeAndNil( slKeys );
              {$endif}
              Result := False;
         end;
     end;
end;

procedure TDBConfigServer.SetAutoServer(value: TAutoServer);
begin
     Self.FAutoServer := value;
     Self.FGlobalLicense.AutoServer := value;
end;

constructor TDBConfigServer.Create(oProvider: TdmZetaServerProvider; oAutoServer : TAutoServer );
var
   eCiclo: eTipoCompany;
begin
     {$ifdef DOTNET}
     inherited Create;
     {$endif}

     FRegistry := TZetaRegistryServer.Create;
     FTipos := TStringList.Create;
     FNombres :=TStringList.Create;

     FGlobalLicense := TGlobalLicenseValues.Create( Self.FAutoServer );
     FGlobalLicense.FechaActual := Date;
     FDBConfigMgr := TDbConfigManager.Create(FGlobalLicense, IDBConfigStoreHandler( Self )  );



     for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
     begin
          FTipos.Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ), ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo ) ) ] ) );
          FNombres.Add( Format( '%s=%s', [ AnsiUpperCase( ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo ) ) ), ZetaCommonLists.ObtieneElemento( lfTipoCompany, Ord( eCiclo ) ) ] ) );
     end;

     FProvider := oProvider;
     FProvider.EmpresaActiva := FProvider.Comparte;

     SetAutoServer( oAutoServer );
     FDBConfigMgr.LoadDbConfig;
     FDBConfigMgr.SaveDbConfig;

end;

function TDBConfigServer.GetTipoCompanyName( const sValor: String ): eTipoCompany;
var
   iValor: Integer;
begin
     Result := tc3Datos;                        // El default siempre es 3Datos
     if strLleno( sValor ) then
     begin
          iValor := FTipos.IndexOfName( AnsiUpperCase( sValor ) );
          if ( iValor >= 0 ) then
             Result := eTipoCompany( iValor );
     end;
end;


function TDBConfigServer._AddRef: Integer;
begin
    result := 0;
end;

function TDBConfigServer._Release: Integer;
begin
    result := 0;
end;

function TDBConfigServer.QueryInterface(const IID: TGUID;
  out Obj): HResult;
begin
    result := 0;
end;

destructor TDBConfigServer.Destroy;
begin
  FreeAndNil( FTipos );
  FRegistry.Free;
  inherited;
end;

function TDBConfigServer.EjecutaScriptComparte(sScript: string): boolean;
var
 oQuery : TZADOQuery;
{$ifdef DEBUG_KEYS}
 slKeys : TStringList;
{$endif}

begin
     with FProvider do
     begin
         EmpresaActiva := Comparte;
         try
             //EmpiezaTransaccion;
             {$ifdef DEBUG_KEYS}
             slKeys := TStringList.Create;
             if (FileExists('C:\TEMP\KEYS2.TXT') ) then
                slKeys.LoadFromFile('C:\TEMP\KEYS2.TXT');
             {$endif}

             try

                 oQuery := FProvider.CreateQuery(sScript) as TZADOQuery;
                  try
                      oQuery.ExecSQL;
                  finally
                      oQuery.Free;
                  end;

                  Result := TRUE;
                //TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin

                        {$ifdef DEBUG_KEYS}
                        slKeys.Add('Error en EjecutaScriptComparte: ' + Error.Message);
                        slKeys.Add(' Script: ' + sScript);
                        slKeys.SaveToFile('C:\TEMP\KEYS2.TXT');
                        FreeAndNil( slKeys );
                        {$endif}

                        //RollBackTransaccion;
                        Result := FALSE;
                   end;
             end;
          finally

          end;
     end;
end;

function TDBConfigServer.ContarConfiguracionBDProduccion( oEmpresa : TEmpresaConfig ): integer;
var
   sCM_DATOS : string;
   sCompany, sControlReal, sControl, sDatos : String;
   dFecha : TDate;
   sCheckSum, sQuery : string;
   tqValidaDuplicidadBD : TZetaCursor;
begin
   Result := 0;

   sCM_DATOS := oEmpresa.Datos;

   if StrVacio( sCM_DATOS ) then
      Result := 1  //Para que no sea tomado en cuenta
   else
   begin
      try
         sQuery :=  Format(Q_VALIDA_CONFIG_BD_PROD, [ sCM_DATOS ] ) ;
         tqValidaDuplicidadBD := FProvider.CreateQuery( sQuery );
         try
            with tqValidaDuplicidadBD do
            begin
                  // Comentar Active := True
                  // Active := True;
                  while not Eof do
                  begin
                       sCheckSum := FieldByName( 'DB_CHKSUM' ).AsString;

                       ZetaServerTools.GetCompanyDataFromChecksum( sCheckSum, sCompany, sControlReal, sControl, sDatos, dFecha );
                       if  ( sCompany = FieldByName('DB_CODIGO').AsString  ) and ( sControlReal = FieldByName('DB_CTRL_RL').AsString  ) and
                           ( sControl = FieldByName('DB_CONTROL').AsString ) and ( sDatos = FieldByName('DB_DATOS').AsString ) then
                       begin
                            Inc(Result);
                       end;

                       Next;
                  end;
            end;
        finally
               FreeAndNil( tqValidaDuplicidadBD );
        end;
      except
            on Error: Exception do
            begin
                 Result := 0;
            end;
      end;
   end;
end;

function TDBConfigServer.ContarDuplicidadBD( oEmpresa : TEmpresaConfig ): integer;
var
   sCM_CODIGO : string;
   sCM_DATOS : string;
   sCM_CONTROL : string;
   sCM_CTRL_RL : string;
   sCM_CTRL_RL_Contrario : string;

   sQuery : string;
   tqValidaDuplicidadBD : TZetaCursor;

begin
   Result := 0;

   sCM_CODIGO := oEmpresa.Codigo;
   sCM_DATOS :=  oEmpresa.Datos;
   sCM_CONTROL := '3DATOS';
   sCM_CTRL_RL := oEmpresa.ControlReal;

   if ( sCM_CTRL_RL = '3DATOS' ) then
      sCM_CTRL_RL_Contrario := '3PRUEBA'
   else
   if ( sCM_CTRL_RL = '3PRUEBA+' ) then
      sCM_CTRL_RL_Contrario := '3DATOS'
   else
       sCM_CTRL_RL_Contrario := VACIO;

   if StrLleno ( sCM_CTRL_RL_Contrario)  then
   begin
      try
          try
             sQuery :=  Format(Q_CUENTA_DUPLICIDAD_BD, [ sCM_DATOS, sCM_CONTROL, sCM_CTRL_RL, sCM_CODIGO ] );
             tqValidaDuplicidadBD := FProvider.CreateQuery( sQuery );

             with   tqValidaDuplicidadBD do
             begin
                Active := True;
                while not Eof do
                begin
                     Result := FieldByName( 'CUANTOS' ).AsInteger;
                     Next;
                end;
             end;
          finally
                 FreeAndNil( tqValidaDuplicidadBD) ;
          end;
      except
            on Error: Exception do
            begin
                 Result := 999;
            end;
      end;
   end;
end;

function TDBConfigServer.IncrementarAltaBDProduccion( oEmpresa : TEmpresaConfig ) : boolean;
begin
     Result := TRUE;

     with oEmpresa do
     begin
        if StrLleno( Codigo  ) and ( ControlReal = '3DATOS' )  and ( ContarConfiguracionBDProduccion(oEmpresa) < 1 )   then
        begin
             FDBConfigMgr.LoadDbConfig;
             FDBConfigMgr.IncrementarDB;
             Result := FDBConfigMGr.SaveDbConfig;
        end;
     end;
end;

function TDBConfigServer.ValidarAltaBDProduccion( var sAdvertencia : string ) : boolean;
var
    sMensajeAdv : string;
begin
     SetAutoServer( Self.AutoServer );
     FDBConfigMgr.LoadDbConfig;
     Result := FDBConfigMgr.PuedeAgregarDB;
     if ( Result ) and ( FDBConfigMgr.AdvertirAgregarDB( sMensajeAdv ) ) then
     begin
          sAdvertencia := sMensajeAdv;
     end;
end;


function TDBConfigServer.EsValidoOldChecksum(oEmpresa : TEmpresaConfig )  : Boolean;
var
  sCompany, sControlReal, sControl, sDatos : String;
  dFecha : TDate;
  sCheckSum : string;
begin
          sCheckSum := oEmpresa.OldCheckSum;
          ZetaServerTools.GetCompanyDataFromChecksum( sCheckSum, sCompany, sControlReal, sControl, sDatos, dFecha );

          Result := ( sCompany =  oEmpresa.OldCodigo ) and
                         ( sControlReal =  oEmpresa.OldControlReal ) and
                         ( sControl =  oEmpresa.OldControl  ) and
                         ( sDatos = oEmpresa.OldDatos  );

end;


procedure TDBConfigServer.AfterEmpresaObjPost(oEmpresa: TEmpresaConfig);
begin
     if oEmpresa.lAcumula then
        IncrementarAltaBDProduccion(oEmpresa);
end;

end.
