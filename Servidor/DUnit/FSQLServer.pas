unit FSQLServer;

interface

uses
  Forms, StdCtrls, Controls, Classes, Dialogs;

type

  // Forma modal para pedir parámetros de conexión SQL Server
  TdlgConexionSQL = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtServidor: TEdit;
    Label2: TLabel;
    edtUsuario: TEdit;
    Label3: TLabel;
    edtContrasena: TEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    edtComparte: TEdit;
    Label5: TLabel;
    btnComparte: TButton;
    edtDatos: TEdit;
    btnDatos: TButton;
    btnAceptar: TButton;
    Button4: TButton;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure btnComparteClick(Sender: TObject);
    procedure btnDatosClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
    sIni: string;
    oParams: TStringList;
    function GetServidor: string;
    function GetUsuario: string;
    function GetContrasena: string;
    function GetComparte: string;
    function GetDatos: string;
  public
    { Public declarations }
    property Servidor  : string read GetServidor;
    property Usuario   : string read GetUsuario;
    property Contrasena: string read GetContrasena;
    property Comparte  : string read GetComparte;
    property Datos     : string read GetDatos;
  end;

var
  dlgConexionADO: TdlgConexionSQL;

implementation

{$R *.dfm}

// -----------------------------------------------------------------------------
procedure TdlgConexionSQL.FormCreate(Sender: TObject);
begin
  oParams := TStringList.Create;
  sIni := ChangeFileExt(Application.ExeName, '.ini');
  if FileExists(sIni) then
    oParams.LoadFromFile(sIni);
  edtServidor.Text   := oParams.Values['Servidor'];
  edtUsuario.Text    := oParams.Values['Usuario'];
  edtContrasena.Text := oParams.Values['Contrasena'];
  edtComparte.Text   := oParams.Values['Comparte'];
  edtDatos.Text      := oParams.Values['Datos'];
end;

procedure TdlgConexionSQL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ;
end;

procedure TdlgConexionSQL.FormDestroy(Sender: TObject);
begin
  oParams.Free
end;

function TdlgConexionSQL.GetServidor: string;
begin
  Result := edtServidor.Text
end;

function TdlgConexionSQL.GetUsuario: string;
begin
  Result := edtUsuario.Text
end;

function TdlgConexionSQL.GetContrasena: string;
begin
  Result := edtContrasena.Text
end;

function TdlgConexionSQL.GetComparte: string;
begin
  Result := edtComparte.Text
end;

function TdlgConexionSQL.GetDatos: string;
begin
  Result := edtDatos.Text
end;

procedure TdlgConexionSQL.btnComparteClick(Sender: TObject);
begin
  OpenDialog.InitialDir := ExtractFileDir(edtComparte.Text);
  OpenDialog.FileName := ExtractFilename(edtComparte.Text);
  if OpenDialog.Execute then
    edtComparte.Text := OpenDialog.FileName
end;

procedure TdlgConexionSQL.btnDatosClick(Sender: TObject);
begin
  OpenDialog.InitialDir := ExtractFileDir(edtDatos.Text);
  OpenDialog.FileName := ExtractFilename(edtDatos.Text);
  if OpenDialog.Execute then
    edtDatos.Text := OpenDialog.FileName
end;

procedure TdlgConexionSQL.btnAceptarClick(Sender: TObject);
begin
  oParams.Values['Servidor']   := edtServidor.Text;
  oParams.Values['Usuario']    := edtUsuario.Text;
  oParams.Values['Contrasena'] := edtContrasena.Text;
  oParams.Values['Comparte']   := edtComparte.Text;
  oParams.Values['Datos']      := edtDatos.Text;
  try
    oParams.SaveToFile(sIni);
  finally
    ;
  end;
  ModalResult := mrOk
end;

end.

