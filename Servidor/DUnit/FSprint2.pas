unit FSprint2;

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Sistema............: TRESS
// Prop�sito..........: M�dulo para pruebas unitarias del M�dulo ServerCatalogos, Sprint 2
// Desarrollador......: Ricardo Carrillo Morales, 5/Ago/2013
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//  TAREAS DEL SPRINT 2
//
// ESCENARIO DE FUNCIONES DIRECTAS DE SQL ----------------------------------------------------------
//  1. Ejecuci�n directa de ExecSQL, buscar en DServerCatalogos ejemplos como GrabaPercepFijas.
//     Validar solamente que no arroje excepci�n.
//  2. Ejecuci�n directa de OpenSQL, Ejemplo en TdmServerCatalogos.GetCursos. Validar solamente que
//     no arroje excepci�n.
//  3. Ejecuci�n directa de GetInsertScript. Ejemplo en DServerCalcNomina,
//     FDestino := CreateQuery(GetInsertScript( sTabla, K_EXCLUIDOS, FOrigen ));
//     Validar solamente que no arroje excepci�n y visualizar texto del script.
// ESCENARIO DE FUNCIONES DEPENDIENTES DE TOEQuery - PREPARACION DE QUERYS -------------------------
//  4. CreateQueryLocate, utilizar ejemplo de MTS\DServerRecursos :
//     TdmServerRecursos.AbreListaTipoKardex; Validar solamente que no arroje excepci�n.
//  5. CreateQuery, usar ejemplos de MTS\DQueries.pas, GetEmpleadoDatosTurnoBegin. Validar solamente
//     que no arroje excepci�n.
//  6. PreparaQuery, usar ejemplo MTS\DServerRecursos.pas,
//     TdmServerRecursos.PreparaDerechosAdicionales, Validar solamente que no arroje excepci�n.
// ESCENARIO DE FUNCIONES DEPENDIENTES DE TOEQuery - EJECUCION -------------------------------------
//  7. Ejecuta, usar ejemplo MTS\DServerCatalogos, dmServerCatalogos.CopiarPerfil, el stored
//     procedure SP_COPIA_PERFIL
//       1. Debe existir en datos un registro en la tabla PERFIL con PU_CODIGO = '001'
//       2. Al ejecutar SP_COPIA_PERFIL '001', '002' deber� crear un registro nuevo en la tabla
//          PERFIL con el PU_CODIGO = '002'
//       3. Validar que no arroje excepci�n
//       4. Validar que exista este nuevo elemento
// ESCENARIO DE FUNCIONES DEPENDIENTES DE TOEQuery - SECUNDARIAS -----------------------------------
//  8. AsignaDataSetParams
//  9. AsignaDataSetOtros
// 10. GetParametro
// 11. GetParametroPos (M�todo privado, no se puede acceder directamente)
// MANEJO DE PARAMETROS ----------------------------------------------------------------------------
// 12. ParamAsInteger, ParamAsFloat, ParamAsDate, ParamAsString, ParamAsVarChar, ParamAsChar,
//     ParamAsBlob, ParamAsBoolean, ParamVariant, ParamAsNull, ParamAsGUID, ParamAsDateTime.
// 13. AsignaParamsDataSet
//__________________________________________________________________________________________________

interface

{$INCLUDE JEDI.INC}

uses
  TestFramework, FSprint0, SysUtils, Variants, DateUtils, DB, ADODB,
  {$IFNDEF DELPHIXE3_UP} ODSI,{$ENDIF}
  //
  SQLServer,
  // Sistema TRESS
  DZetaServerProvider, ZetaServerDataSet, ZetaClientDataSet, ZetaCommonTools, ZetaCommonClasses,
  DServerCatalogos, ZetaCommonLists;

type
  TSprint2 = class(TSprint0)

  private
    {private declarations}
   procedure CalculaCosteoParametros(FListaParametros: string);
  public
    {public declarations}
  published
    procedure Escenario_FuncionesSQL_ExecSQL;
    procedure Escenario_FuncionesSQL_OpenSQL;
    procedure Escenario_FuncionesSQL_GetInsertScript;
    procedure Escenario_FuncionesTOEQuery_CreateQueryLocate;
    procedure Escenario_FuncionesTOEQuery_CreateQuery;
    procedure Escenario_FuncionesTOEQuery_PreparaQuery;
    procedure Escenario_FuncionesTOEQuery_CopiarPerfil;
    procedure Escenario_FuncionesTOEQuery_AsignaDataSetParams;
    procedure Escenario_FuncionesTOEQuery_AsignaDataSetOtros;
    procedure Escenario_FuncionesTOEQuery_GetParametro;
    procedure Escenario_Parametros_ParamAs;
    procedure Escenario_Parametros_AsignaParamsDataSet;
  end;

implementation //===================================================================================

const
  K_SQL1: string = 'delete from PTOFIJAS where PU_CODIGO = ''000005''';
  K_SQL2: string = 'select CU_CODIGO,CU_ACTIVO,CU_CLASIFI,CU_CLASE,CU_COSTO1,CU_COSTO2,CU_COSTO3,' +
                          'CU_HORAS,CU_INGLES,CU_NOMBRE,CU_TEXTO1,CU_TEXTO2,MA_CODIGO,CU_NUMERO,' +
                          'CU_TEXTO,CU_REVISIO,CU_FEC_REV,CU_STPS,CU_FOLIO,CU_DOCUM,AT_CODIGO,' +
                          'CU_OBJETIV,CU_MODALID,CU_NOMBRE as CU_DESCRIP ' +
                   'from CURSO ' +
                   'order by CU_CODIGO';

procedure TSprint2.CalculaCosteoParametros(FListaParametros: string);
begin
  with Fixture.oZetaProvider.DatosPeriodo do begin
    FListaParametros := VACIO;
    FListaParametros := 'Periodo: ' + ZetaCommonTools.ShowNomina(Year, Ord(Tipo), Numero);
  end;
end;

procedure TSprint2.Escenario_FuncionesSQL_ExecSQL;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
  ConectaDemo;
  Status(Format('Ejecutando oZetaProvider.ExecSQL( %s )',[ K_SQL1]));
  with Fixture.oZetaProvider do
    ExecSQL( EmpresaActiva, K_SQL1 ); // <----------------------------------------------------------
  Status(' -> Ok');
end;

procedure TSprint2.Escenario_FuncionesSQL_OpenSQL;
begin
  ConectaDemo;
  oServerDS := TServerDataSet.Create(nil);
  Tabla := 'CURSO';
  try
    with Fixture.oZetaProvider do begin
      with oServerDS do begin
        Lista := OpenSQL( Fixture.oZetaProvider.EmpresaActiva, K_SQL2, TRUE ); // <-----------------
        while ( not EOF ) do begin
          if strLleno( FieldByName( 'CU_FOLIO' ).AsString ) then begin
            Edit;
            FieldByName( 'CU_DESCRIP' ).AsString := FieldByName( 'CU_FOLIO' ).AsString + ' : ' +
                                                    FieldByName( 'CU_NOMBRE' ).AsString;
            Post;
          end;
          Next;
        end;
        MergeChangeLog;
        MostrarCampos(oServerDS);
        MostrarDatos(oServerDS);
      end;
    end;
  finally
    FreeAndNil(oServerDS)
  end;
  //SetComplete;
end;

procedure TSprint2.Escenario_FuncionesSQL_GetInsertScript;
var
  s, sScriptBaseColabora : string;
begin
  ConectaDemo;
  Tabla := 'COLABORA';

  sScriptBaseColabora :=
    'insert into COLABORA ( CB_CODIGO,CB_ACTIVO,CB_APE_MAT,CB_APE_PAT,CB_AUTOSAL,CB_BAN_ELE,CB_CARRERA,CB_CHECA,'
    + 'CB_CIUDAD,CB_CLASIFI,CB_CODPOST,CB_CONTRAT,CB_CREDENC,CB_CURP,CB_CALLE,CB_COLONIA,CB_EDO_CIV,CB_FEC_RES,CB_EST_HOR,'
    + 'CB_EST_HOY,CB_ESTADO,CB_ESTUDIO,CB_EVALUA,CB_EXPERIE,CB_FEC_ANT,CB_FEC_BAJ,CB_FEC_BSS,CB_FEC_CON,CB_FEC_ING,CB_FEC_INT,CB_FEC_NAC,CB_FEC_REV,CB_FEC_VAC,CB_G_FEC_1,'
    + 'CB_G_FEC_2,CB_G_FEC_3,CB_G_LOG_1,CB_G_LOG_2,CB_G_LOG_3,' +
    'CB_G_NUM_1,CB_G_NUM_2,CB_G_NUM_3,CB_G_TAB_1,CB_G_TAB_2,CB_G_TAB_3,CB_G_TAB_4,CB_G_TEX_1,CB_G_TEX_2,CB_G_TEX_3,CB_G_TEX_4,CB_HABLA,CB_TURNO,CB_IDIOMA,CB_INFCRED,CB_INFMANT,CB_INFTASA,CB_LA_MAT,CB_LAST_EV,CB_LUG_NAC,CB_MAQUINA,'
    + 'CB_MED_TRA,CB_PASAPOR,CB_FEC_INC,CB_FEC_PER,CB_NOMYEAR,CB_NACION,CB_NOMTIPO,CB_NEXT_EV,CB_NOMNUME,'
    + 'CB_NOMBRES,CB_PATRON,CB_PUESTO,CB_RFC,CB_SAL_INT,CB_SALARIO,CB_SEGSOC,CB_SEXO,CB_TABLASS,CB_TEL,CB_VIVECON,'
    + 'CB_VIVEEN,CB_ZONA,CB_ZONA_GE,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_INFTIPO,CB_NIVEL8,CB_NIVEL9,CB_DER_FEC,CB_DER_PAG,CB_V_PAGO,CB_DER_GOZ,CB_V_GOZO,CB_TIP_REV,CB_MOT_BAJ,CB_FEC_SAL,CB_INF_INI,'
    + 'CB_INF_OLD,CB_OLD_SAL,CB_OLD_INT,CB_PRE_INT,CB_PER_VAR,CB_SAL_TOT,CB_TOT_GRA,CB_FAC_INT,CB_RANGO_S,CB_CLINICA,CB_NIVEL0,CB_AREA,CB_CANDIDA,CB_ID_NUM,CB_ENT_NAC,'
    + 'CB_COD_COL,CB_G_FEC_4,CB_G_FEC_5,CB_ID_BIO,CB_GP_COD,CB_G_FEC_6,CB_G_FEC_7,CB_G_FEC_8,CB_G_LOG_4,CB_G_LOG_5,CB_G_LOG_6,CB_G_LOG_7,CB_G_LOG_8,CB_G_NUM_4,CB_G_NUM_5,CB_G_NUM_6,'
    + 'CB_G_NUM_7,CB_G_NUM_8,CB_G_NUM_9,CB_G_NUM10,CB_G_NUM11,CB_G_NUM12,CB_G_NUM13,CB_G_NUM14,CB_G_NUM15,'
    + 'CB_G_NUM16,CB_G_NUM17,CB_G_NUM18,CB_G_TAB_5,CB_G_TAB_6,CB_G_TAB_7,CB_G_TAB_8,CB_G_TAB_9,CB_G_TAB10,CB_G_TAB11,'
    + 'CB_G_TAB12,CB_G_TAB13,CB_G_TAB14,CB_G_TEX_5,CB_G_TEX_6,CB_G_TEX_7,CB_G_TEX_8,CB_G_TEX_9,CB_G_TEX10,CB_G_TEX11,CB_G_TEX12,CB_G_TEX13,CB_G_TEX14,CB_G_TEX15,'
    + 'CB_G_TEX16,CB_G_TEX17,CB_G_TEX18,CB_G_TEX19,CB_SUB_CTA,CB_NETO,CB_DER_PV,CB_V_PRIMA,CB_NOMINA,CB_RECONTR,CB_DISCAPA,CB_INDIGE,CB_FONACOT,CB_EMPLEO,US_CODIGO,CB_FEC_COV,CB_G_TEX20,CB_G_TEX21,CB_G_TEX22,CB_G_TEX23,'
    + 'CB_G_TEX24,CB_INFACT,CB_INFDISM,CB_NUM_EXT,CB_NUM_INT,CB_INF_ANT,CB_TDISCAP,CB_ESCUELA,CB_TESCUEL,CB_TITULO,CB_YTITULO,CB_CTA_VAL,CB_CTA_GAS,CB_MUNICIP,CB_TSANGRE,CB_ALERGIA,CB_BRG_ACT,CB_BRG_TIP,CB_BRG_ROL,CB_BRG_JEF,CB_BRG_CON,CB_BRG_PRA,CB_BRG_NOP )'
    + ' values ( :CB_CODIGO,:CB_ACTIVO,:CB_APE_MAT,:CB_APE_PAT,' +
    ':CB_AUTOSAL,:CB_BAN_ELE,:CB_CARRERA,:CB_CHECA,:CB_CIUDAD,:CB_CLASIFI,:CB_CODPOST,:CB_CONTRAT,:CB_CREDENC,:CB_CURP,:CB_CALLE,:CB_COLONIA,:CB_EDO_CIV,:CB_FEC_RES,:CB_EST_HOR,:CB_EST_HOY,:CB_ESTADO,:CB_ESTUDIO,:CB_EVALUA,:CB_EXPERIE,:CB_FEC_ANT,:CB_FEC_BAJ,'
    + ':CB_FEC_BSS,:CB_FEC_CON,:CB_FEC_ING,:CB_FEC_INT,:CB_FEC_NAC,' +
    ':CB_FEC_REV,:CB_FEC_VAC,:CB_G_FEC_1,:CB_G_FEC_2,:CB_G_FEC_3,:CB_G_LOG_1,:CB_G_LOG_2,:CB_G_LOG_3,:CB_G_NUM_1,'
    + ':CB_G_NUM_2,:CB_G_NUM_3,:CB_G_TAB_1,:CB_G_TAB_2,:CB_G_TAB_3,:CB_G_TAB_4,:CB_G_TEX_1,:CB_G_TEX_2,:CB_G_TEX_3,:CB_G_TEX_4,:CB_HABLA,:CB_TURNO,:CB_IDIOMA,:CB_INFCRED,:CB_INFMANT'
    + ',:CB_INFTASA,:CB_LA_MAT,:CB_LAST_EV,:CB_LUG_NAC,:CB_MAQUINA,:CB_MED_TRA,:CB_PASAPOR,:CB_FEC_INC,:CB_FEC_PER,'
    + ':CB_NOMYEAR,:CB_NACION,:CB_NOMTIPO,:CB_NEXT_EV,:CB_NOMNUME,:CB_NOMBRES,:CB_PATRON,:CB_PUESTO,:CB_RFC,:CB_SAL_INT,:CB_SALARIO,:CB_SEGSOC,:CB_SEXO,:CB_TABLASS,:CB_TEL,:CB_VIVECON,:CB_VIVEEN,:CB_ZONA,:CB_ZONA_GE,:CB_NIVEL1,:CB_NIVEL2,:CB_NIVEL3,:CB_NIVEL4,'
    + ':CB_NIVEL5,:CB_NIVEL6,:CB_NIVEL7,:CB_INFTIPO,:CB_NIVEL8,:CB_NIVEL9,:CB_DER_FEC,:CB_DER_PAG,:CB_V_PAGO,:CB_DER_GOZ,:CB_V_GOZO,:CB_TIP_REV,:CB_MOT_BAJ,:CB_FEC_SAL,:CB_INF_INI,'
    + ':CB_INF_OLD,:CB_OLD_SAL,:CB_OLD_INT,:CB_PRE_INT,:CB_PER_VAR,:CB_SAL_TOT,:CB_TOT_GRA,:CB_FAC_INT,:CB_RANGO_S,:CB_CLINICA,'
    + ':CB_NIVEL0,:CB_AREA,:CB_CANDIDA,:CB_ID_NUM,:CB_ENT_NAC,:CB_COD_COL,:CB_G_FEC_4,:CB_G_FEC_5,:CB_ID_BIO,:CB_GP_COD,'
    + ':CB_G_FEC_6,:CB_G_FEC_7,:CB_G_FEC_8,:CB_G_LOG_4,:CB_G_LOG_5,:CB_G_LOG_6,:CB_G_LOG_7,:CB_G_LOG_8,:CB_G_NUM_4,'
    + ':CB_G_NUM_5,:CB_G_NUM_6,:CB_G_NUM_7,:CB_G_NUM_8,:CB_G_NUM_9,:CB_G_NUM10,:CB_G_NUM11,:CB_G_NUM12,:CB_G_NUM13,'
    + ':CB_G_NUM14,:CB_G_NUM15,:CB_G_NUM16,:CB_G_NUM17,:CB_G_NUM18,:CB_G_TAB_5,:CB_G_TAB_6,:CB_G_TAB_7,:CB_G_TAB_8,'
    + ':CB_G_TAB_9,:CB_G_TAB10,:CB_G_TAB11,:CB_G_TAB12,:CB_G_TAB13,:CB_G_TAB14,:CB_G_TEX_5,:CB_G_TEX_6,:CB_G_TEX_7,'
    + ':CB_G_TEX_8,:CB_G_TEX_9,:CB_G_TEX10,:CB_G_TEX11,:CB_G_TEX12,:CB_G_TEX13,:CB_G_TEX14,:CB_G_TEX15,:CB_G_TEX16,'
    + ':CB_G_TEX17,:CB_G_TEX18,:CB_G_TEX19,:CB_SUB_CTA,:CB_NETO,' +
    ':CB_DER_PV,:CB_V_PRIMA,:CB_NOMINA,:CB_RECONTR,:CB_DISCAPA,:CB_INDIGE,:CB_FONACOT,:CB_EMPLEO,:US_CODIGO,'
    + ':CB_FEC_COV,:CB_G_TEX20,:CB_G_TEX21,:CB_G_TEX22,:CB_G_TEX23,:CB_G_TEX24,:CB_INFACT,:CB_INFDISM,:CB_NUM_EXT,'
    + ':CB_NUM_INT,:CB_INF_ANT,:CB_TDISCAP,:CB_ESCUELA,:CB_TESCUEL,:CB_TITULO,:CB_YTITULO,:CB_CTA_VAL,:CB_CTA_GAS,'
    + ':CB_MUNICIP,:CB_TSANGRE,:CB_ALERGIA,:CB_BRG_ACT,:CB_BRG_TIP,:CB_BRG_ROL,:CB_BRG_JEF,:CB_BRG_CON,:CB_BRG_PRA,:CB_BRG_NOP )';

  try
    oZClientDS := TZetaClientDataSet.Create(nil);
    Fixture.SetTablaInfo(eDatosEmpleado);
    oZClientDS.Data := Fixture.oZetaProvider.GetTabla(Fixture.oZetaProvider.EmpresaActiva);
    Status('Ejecutando oZetaProvider.GetInsertScript( Tabla, '', oZClientDS )');
    s := Fixture.oZetaProvider.GetInsertScript(Tabla, '', oZClientDS); // <-------------------------
    Check(s = sScriptBaseColabora, 'Script de insercion no es el mismo');
    Status('  -> Ok');
    Status('  => ' + s);
    Status('');
    MostrarCampos(oZClientDS);
  finally
    FreeAndNil(oZClientDS);
  end;
end;

procedure TSprint2.Escenario_FuncionesTOEQuery_CreateQueryLocate;
var
  FTipoKardex : TZetaCursorLocate;
  s: string;
begin
  ConectaDemo;
  Tabla := 'TKARDEX';
  try
    with Fixture.oZetaProvider do begin
      Status('Ejecutando GetScript( eTipoKardex )');
      s := Fixture.GetScript( eTipoKardex );
      Status('  => ' + s);
      FTipoKardex := CreateQueryLocate; // <--------------------------------------------------------
      PreparaQuery( FTipoKardex, s );
    end;
    with FTipoKardex do begin
      Close;                // Por si Cambia de Compa�ia
      Open;
    end;
    Status('  -> Ok');
    Status('');
    MostrarCampos(FTipoKardex);
    MostrarDatos(FTipoKardex);
  finally
    FreeAndNil( FTipoKardex )
  end
end;

procedure TSprint2.Escenario_FuncionesTOEQuery_CreateQuery;
var
  FTurnoLee: TZetaCursor;
  s: string;
begin
  ConectaDemo;
  Tabla := 'TURNO_EMPLEADO';
  with Fixture.oZetaProvider do try
    Status('Ejecutando GetSQLScript( Q_TURNO_GET_DATOS )');
    s := Fixture.GetSQLScript( Q_TURNO_GET_DATOS );
    Status('  => ' + s);
    FTurnoLee := CreateQuery( s ); // <-------------------------------------------------------------
//    {$IFDEF DELPHIXE3_UP}
//    TZADOQuery(fTurnoLee).Parameters.ParamByName('Fecha').Value := 0;
//    TZADOQuery(fTurnoLee).Parameters.ParamByName('Empleado').Value := '001';
//    {$ELSE}
//    TOEQuery(fTurnoLee).Params.ParamByName('Fecha').Value := 0;
//    TOEQuery(fTurnoLee).Params.ParamByName('Empleado').Value := '001';
//    {$ENDIF}
    ParamAsDateTime(fTurnoLee, 'Fecha', 0);
    ParamAsString(fTurnoLee, 'Empleado', '001');

    FTurnoLee.Open;
    Status('  -> Ok');
    Status('');
    MostrarCampos(FTurnoLee);
    MostrarDatos(FTurnoLee);
  finally
    FreeAndNil(FTurnoLee)
  end;
end;

procedure TSprint2.Escenario_FuncionesTOEQuery_PreparaQuery;
var
  FAdicionalesValida :TZetaCursorLocate;
begin
  ConectaDemo;
  Tabla := 'CAMPO_AD';
  with Fixture.oZetaProvider do try
    FAdicionalesValida := CreateQueryLocate;
    Status('Ejecutando PreparaQuery()');
    PreparaQuery(FAdicionalesValida,                          // <----------------------------------
                 Format(Fixture.GetScript(eCamposAdicAcceso),
                        [EntreComillas(CodigoEmpresaActiva), CodigoGrupo, D_GRUPO_SIN_RESTRICCION]));
    {$IFDEF DELPHIXE3_UP}
    Status('  => ' + Trim(TADOQuery(FAdicionalesValida).SQL.Text));
    {$ELSE}
    Status('  => ' + Trim(TOEQuery(FAdicionalesValida).SQL.Text));
    {$ENDIF}
    FAdicionalesValida.Active := TRUE;
    Status('  -> Ok');
    Status('');
    MostrarCampos(FAdicionalesValida);
    MostrarDatos(FAdicionalesValida);
  finally
    FreeAndNil(FAdicionalesValida)
  end;
end;

procedure TSprint2.Escenario_FuncionesTOEQuery_CopiarPerfil;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
  ConectaDemo;
  Tabla := 'PERFIL';
  try
    oZClientDS := TZetaClientDataSet.Create( nil );

    Fixture.oZetaProvider.ExecSQL( Fixture.oZetaProvider.EmpresaActiva, 'delete from PERFIL where PU_CODIGO = ''002''');

    Fixture.SetTablaInfo(ePerfiles);
    Status('Antes de ejecutar CopiarPerfil(oZetaProvider.EmpresaActiva, ''001'', ''002'')');
    Status('');
    Fixture.oZetaProvider.TablaInfo.Filtro := 'PU_CODIGO = ''002''';
    oZClientDS.Data := Fixture.oZetaProvider.GetTabla(Fixture.oZetaProvider.EmpresaActiva );
    MostrarCampos(oZClientDS);
    MostrarDatos(oZClientDS);
    ValidarRecordCount(oZClientDS, 0);

    Status('Ejecutando CopiarPerfil(oZetaProvider.EmpresaActiva, ''001'', ''002'')');
    Fixture.CopiarPerfil(Fixture.oZetaProvider.EmpresaActiva, '001', '002'); // <-------------------
    oZClientDS.Data := Fixture.oZetaProvider.GetTabla(Fixture.oZetaProvider.EmpresaActiva );
    Status('  -> Ok');
    Status('');

    Status('Despu�s de ejecutar CopiarPerfil(oZetaProvider.EmpresaActiva, ''001'', ''002'')');
    Status('');
    MostrarDatos(oZClientDS);
    ValidarRecordCount(oZClientDS, 1);
  finally
    FreeAndNil(oZClientDS)
  end
end;

// C�digo tomado de DServerAsistencia.pas
//function CalculaCosteo(Empresa,  Parametros: OleVariant): OleVariant;
procedure TSprint2.Escenario_FuncionesTOEQuery_AsignaDataSetParams;
var
  FListaParametros: string;
  Parametros: TZetaParams;
  FCuenta  : TZetaCursor;
  FCalcula : TZetaCursor;
  iCuantos: integer;
  Result  : OleVariant;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');

  ConectaDemo;
  Tabla := '';
  Parametros := TZetaParams.Create;
  Parametros.AddInteger('Year', 2008);
  Parametros.AddInteger('Tipo', 1);
  Parametros.AddInteger('Numero', 1);

  with Fixture.oZetaProvider do begin
    //EmpresaActiva := Empresa;
    AsignaParamList( Parametros.VarValues );
    InitArregloTPeriodo;
    GetDatosPeriodo;

    CalculaCosteoParametros(FListaParametros);

    FCuenta := CreateQuery( Fixture.GetSQLScript( Q_COSTEO_CUENTA_NOMINAS ) );
    try
      AsignaDataSetParams( FCuenta ); // <----------------------------------------------------------
      with FCuenta do begin
        Active := True;
        iCuantos := Fields[ 0 ].AsInteger;
        Active := False;
      end;

      Status('Ejecutando oZetaProvider.OpenProcess()');
      if Fixture.oZetaProvider.OpenProcess( prNoCalculaCosteo, iCuantos, FListaParametros) then begin
        Status('  -> Ok');
        Status('');
        FCalcula := CreateQuery( Fixture.GetSQLScript( Q_COSTEO_CALCULA ) );
        try
          EmpiezaTransaccion;
          try
            Status(Format('Asignando Par�metros: Year=%d Tipo=%d Numero=%d',
                          [Parametros.ParamByName('Year').AsInteger,
                           Parametros.ParamByName('Tipo').AsInteger,
                           Parametros.ParamByName('Numero').AsInteger ] ) );
            AsignaDataSetParams( FCalcula ); // <---------------------------------------------------
            Ejecuta( FCalcula );
            Status('  -> Ok');
            Status('');
            TerminaTransaccion( True );
          except
            on Error: Exception do begin
              RollBackTransaccion;
              Log.Excepcion( 0, '  -> Error Al Calcular Costeo', Error );
              //Check(False, Error.Message);
              raise
            end;
          end;
        finally
          FreeAndNil(FCalcula);
        end;
        Status('Ejecutando oZetaProvider.CloseProcess()');
        Result := CloseProcess;
        Status('  -> Ok')
      end;
    finally
      FreeAndNil(FCuenta);
      FreeAndNil(Parametros);
    end;
  end;
end;

procedure TSprint2.Escenario_FuncionesTOEQuery_AsignaDataSetOtros;
var
  i: Integer;
begin
  ConectaDemo;
  Tabla := 'CONCEPTO';
  with Fixture.oZetaProvider do try
    oZCursor := CreateQuery( 'select * from CONCEPTO where CO_NUMERO = :CO_NUMERO' );
    for i := 0 to 10 do begin
      Status(Format('Filtro con CO_NUMERO = %d', [i]));
      AsignaDataSetOtros(oZCursor, VarArrayOf(['CO_NUMERO', i])); // <--------------------------------
      oZCursor.Open;
      Status('  -> Ok');
      Status('');
      if i = 0 then
        MostrarCampos(oZCursor);

      MostrarDatos(oZCursor);
      oZCursor.Close
    end
  finally
    FreeAndNil(oZCursor)
  end;
end;

// C�digo tomado de DServerCatalogos.pPas
//function TdmServerCatalogos.CopiaValuacion(Empresa, Parametros: OleVariant; var ErrorCount: Integer): Integer;
procedure TSprint2.Escenario_FuncionesTOEQuery_GetParametro;
var
  Parametros: TZetaParams;
  oCopiar: TZetaCursor;
  Result : Integer;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
  ConectaDemo;
  Tabla := 'VAL_PTO';

  Parametros := TZetaParams.Create;
  Parametros.AddInteger('Folio', 2);
  Parametros.AddString('Puesto', '011');
  Parametros.AddDate('Fecha', Today);
  Parametros.AddInteger('Usuario', 1);


  with Fixture.oZetaProvider do begin

    ExecSQL( EmpresaActiva, 'delete from VAL_PTO where VP_FOLIO > 4');

    Status('Contenido antes de copiar');

    Fixture.SetTablaInfo(eValuacion);
    oZClientDS := TZetaClientDataSet.Create(nil);
    oZClientDS.Data := GetTabla(EmpresaActiva);
    MostrarDatos(oZClientDS);

    //EmpresaActiva := Empresa;
    AsignaParamList( Parametros.VarValues );
    oCopiar := CreateQuery( Q_VAL_COPIA_PUESTO );
    try
      EmpiezaTransaccion;
      try
        ParamAsInteger( oCopiar, 'VP_FOLIO' , ParamList.ParamByName('Folio').AsInteger );
        ParamAsChar   ( oCopiar, 'PU_CODIGO', ParamList.ParamByName('Puesto').AsString, K_ANCHO_CODIGO );
        ParamAsDate   ( oCopiar, 'VP_FECHA' , ParamList.ParamByName('Fecha').AsDate ) ;
        ParamAsInteger( oCopiar, 'US_CODIGO', ParamList.ParamByName('Usuario').AsInteger );
        ParamSalida   ( oCopiar, 'NewFolio');
        Status('Copiando Valuaci�n con folio "2"');
        {$IFDEF DELPHIXE3_UP}
        Status('  => ' + Trim(TADOQuery(oCopiar).SQL.Text));
        {$ELSE}
        Status('  => ' + Trim(TOEQuery(oCopiar).SQL.Text));
        {$ENDIF}
        Ejecuta( oCopiar );
        Status('  -> Ok');
        Status('');
        TerminaTransaccion( True );

        Status('Contenido despues de copiar');
        oZClientDS.Data := GetTabla(EmpresaActiva);
        MostrarDatos(oZClientDS);
      except
        on Error: Exception do begin
          RollBackTransaccion;
          if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then begin
            DatabaseError( Format('� Valuaci�n ya Registrada con el Puesto: %s y Fecha: %s !',
                                   [ ParamList.ParamByName('Puesto').AsString ,
                                     FechaCorta( ParamList.ParamByName('Fecha').AsDate )  ] ) );
          end;
          raise;
        end;
      end;

      Result := GetParametro( oCopiar, 'NewFolio').Value; // <----------------------------------
      Status(Format('Valuaci�n ha sido copiado con el nuevo folio "%d"', [Result]));

    finally
      FreeAndNil( oCopiar );
      FreeAndNil(Parametros);
      FreeAndNil(oZClientDS);
    end;
  end;
  //SetComplete;
end;

// 12. , , ParamAsDate, , ParamAsVarChar, ParamAsChar,
//     ParamAsBlob, ParamAsBoolean, ParamVariant, ParamAsNull, ParamAsGUID, ParamAsDateTime.
procedure TSprint2.Escenario_Parametros_ParamAs;
begin
  ConectaDemo;
  Tabla := 'CONCEPTO';
  with Fixture.oZetaProvider do try
    oZCursor := CreateQuery( 'select * from CONCEPTO where ' +
                           'CO_NUMERO = :CO_NUMERO or ' +
                           'CO_LIM_SUP < :CO_LIM_SUP or ' +
                           'CO_FORMULA <> :CO_FORMULA'
                         );

    Status('Ejecutando ParamAsInteger(oZCursor, ''CO_NUMERO'', 1)');
    ParamAsInteger(oZCursor, 'CO_NUMERO', 1); // <----------------------------------------------------
    Status('Ejecutando ParamAsFloat(oZCursor, ''CO_LIM_SUP'', 1)');
    ParamAsFloat(oZCursor, 'CO_LIM_SUP', 1);
    Status('Ejecutando ParamAsString(oZCursor, ''CO_FORMULA'', '')');
    ParamAsString(oZCursor, 'CO_FORMULA', '');
    Status('  -> Ok');
    Status('');
    oZCursor.Open;
    MostrarCampos(oZCursor);
    MostrarDatos(oZCursor);
  finally
    FreeAndNil(oZCursor)
  end;
end;

procedure TSprint2.Escenario_Parametros_AsignaParamsDataSet;
const
  sCampos    = 'PU_CODIGO';
  sExcluidos = 'PF_FECHA,PF_ESTUDIO,PF_DESCRIP,PF_OBJETIV,PF_EXP_PTO,PF_POSTING,PF_CONTRL1,PF_CONTRL2,' +
               'PF_NUMERO1,PF_NUMERO2,PF_TEXTO1,PF_TEXTO2,PF_EDADMIN,PF_EDADMAX,PF_SEXO';
  sSQL = 'select ' + sCampos + ',' + sExcluidos + ' from PERFIL where PU_CODIGO = (:PU_CODIGO)';
var
  oOrigen, oDestino: TDataSet;
begin
  ConectaDemo;
  Tabla := 'PERFIL';
  with Fixture.oZetaProvider do try
    oOrigen := CreateQuery( sSQL );
    ParamAsString(oOrigen, 'PU_CODIGO', '001');
    Status('Ejecutando primer consulta con par�metros');
    Status('  => ' + sSQL);
    oOrigen.Open;
    Status('  -> Ok');
    Status('');
    MostrarDatos(oOrigen);

    oDestino := CreateQuery( sSQL );
    AsignaParamsDataSet(oOrigen, oDestino, sExcluidos); // <----------------------------------------
    Status('Ejecutando segunda consulta con par�metros asignados de la primera');
    Status('  => ' + sSQL);
    oDestino.Open;
    Status('  -> Ok');
    Status('');
    MostrarDatos(oDestino);
  finally
    FreeAndNil(oOrigen);
    FreeAndNil(oDestino);
  end;
end;

initialization
  RegisterTests(TSprint2.SuiteName, [TSprint2.Suite]);

end.
