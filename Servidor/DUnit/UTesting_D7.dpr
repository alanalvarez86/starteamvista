program UTesting_D7;

uses
  Forms,
  GUITestRunner,
  FSprint0 in 'FSprint0.pas',
  FSprint1 in 'FSprint1.pas',
  FSprint2 in 'FSprint2.pas',
  FSprint3 in 'FSprint3.pas',
  ZetaRegistryServer in 'ZetaRegistryServer.pas',
  DServerCatalogos in 'DServerCatalogos.pas',
  StopWatch in 'StopWatch.pas',
  CheckPrevious;

{$R *.res}

begin
  if not CheckPrevious.RestoreIfRunning(Application.Handle, 1) then begin
    Application.Initialize;
    TGUITestRunner.RunRegisteredTests;
  end;
end.
