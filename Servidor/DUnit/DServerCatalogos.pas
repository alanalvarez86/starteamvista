unit DServerCatalogos;

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Sistema............: TRESS
// Prop�sito..........: Versi�n personalizada de DServerCatalogos para pruebas unitarias del
//                      M�dulo ServerCatalogos, Sprint 1
// Desarrollador......: Ricardo Carrillo Morales, 5/Ago/2013
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

interface

uses
  SysUtils, Variants, DB,
  // Sistema TRESS
  DZetaServerProvider, ZetaServerTools, ZetaCommonLists, ZetaCommonClasses;

type
eTipoCatalogo = ( ePuestos,     {0}
                    eTurnos,      {1}
                    eCursos,      {2}
                    eCalendario,  {3}
                    eClasifi,     {4}
                    eConceptos,   {5}
                    eCondiciones, {6}
                    eContratos,   {7}
                    eEventos,     {8}
                    eFestTurno,   {9}
                    eFolios,      {10}
                    eHorarios,    {11}
                    eInvitadores, {12}
                    eMaestros,    {13}
                    eMatrizCurso, {14}
                    eMatrizPuesto,{15}
                    eOtrasPer,    {16}
                    ePeriodos,    {17}
                    ePrestaciones,{18}
                    eReglas,      {19}
                    eRPatron,     {20}
                    eNomParam,    {21}
                    eOrdFolios,   {22}
                    eTools,       {23}
                    ePtoFijas,    {24}
                    ePtoTools,    {25}
                    eSesiones,    {26}
                    eKarCurso,    {27}
                    eAccReglas,   {28}
                    eAulas, 	  {29}
                    ePrerequisitosCurso, {30}
                    eCertificaciones,    {31}
                    eTiposPoliza,        {32}
{$ifdef QUINCENALES}
                    eTPeriodos,          {33}
{$endif}
                    ePerfilPuestos,      {34}
                    eSeccionesPerfil,    {35}
                    eCamposPerfil,       {36}
                    ePerfiles,           {37}
                    eDescPerfil,         {38}
                    eProvCap             {39}
                    ,eVistaValPlant      {40}
                    ,eValPlant           {41}
                    ,eVistaValFact       {42}
                    ,eValFact            {43}
                    ,eVistaValSubfact    {44}
                    ,eValSubFact         {45}
                    ,eValNiveles         {46}
                    ,eValuacion          {47}
                    ,eValPuntos          {48}
                    ,eRSocial            {49}
                    ,eMatrizCertificPuesto {50}
                    ,eMatrizPuestoCertific {51}
                    ,eReglasPrestamos      {52}
                    ,ePrestaXRegla         {53}
                    ,eHistRev              {54}
                    ,eEstablecimientos     {55}
                    ,eTipoPension          {56}
                    ,eSegurosGastosMed     {57}
                    ,eVigenciasSeguro      {58}
                    ,eViewVigenciasSeguro  {59}
                    ,eCosteoGrupos         {60: Grupos de Costeo}
                    ,eCosteoCriterios      {61: Criterios de Costeo}
                    ,eCriteriosPorConcepto {62: Criterios por Concepto}
                    ,eCatTablaAmortizacion {63}
                    ,eCatCostosTablaAmort  {64}
                    ,eCompetencias         {65}
                    ,eNivelesCompetencias  {66}
                    ,eRevisionCompetencias {67}
                    ,eCursosCompetencias   {68}
                    ,eCPerfiles            {69}
                    ,eRevisionPerfiles     {70}
                    ,ePerfilesCompetencias {71}
                    ,ePuestoGpoCompeten    {72}
                    //Extras
                    ,eUsuario
                    ,eDatosEmpleado
                    ,eEmpFoto
                    ,fnDatosNomina
                     );

  // C�digo tomado de TdmServerRecursos
  eTipoRecursos = ( eTipoKardex, eCamposAdicAcceso);

const
  CatalogosConDetail = [ ePrestaciones{,
                         eRPatron,
                         eFolios,
                         eMatrizPuesto,
                         eMatrizCurso ,
                         eMatrizCertificPuesto,
                         eMatrizPuestoCertific}];
  //CatalogosConBitacora = [ eTurnos, eConceptos, eHorarios, eCondiciones, eReglas ];

  // C�digo tomado de MTS\DQueries.pas
  Q_TURNO_GET_DATOS = 5;
  // C�digo tomado de MTS\DServerAsistencia.pas
  Q_COSTEO_CUENTA_NOMINAS = 37;
  Q_COSTEO_CALCULA = 38;
  // C�digo tomado de MTS\DServerCalcNomina.pas
  Q_RECALCULA_KARDEX = 21;{OP: 28/05/08}

type
  TdmServerCatalogos = class(TObject)
    oZetaProvider: TdmZetaServerProvider;
  private
    {private declarations}
    FTipoCatalogo: eTipoCatalogo;
  public
    {public declarations}
    constructor Create;
    destructor Destroy; override;
    procedure SetTablaInfo(eCatalogo: ETipoCatalogo);
    procedure SetDetailInfo(eDetail: eTipoCatalogo);
    function GetCatalogo( const eCatalogo: eTipoCatalogo; const Empresa: OleVariant ): OLEVariant;
    function GetPerfiles( Empresa: OleVariant;const sPuesto: WideString): OleVariant;
    function CopiarPerfil(Empresa: OleVariant; const sFuente, sDestino: WideString): OleVariant;
    // C�digo tomado de TdmServerRecursos
    function GetScript(const eTipo: eTipoRecursos): String;
    // C�digo tomado de MTS\DQueries.pas
    function GetSQLScript( const iScript: Integer ): String;
    procedure GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet;
      DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure GrabaCambiosPuesto(Sender: TObject; SourceDS: TDataSet;
      DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    function GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant;
      out ErrorCount: Integer): OleVariant;
  end;

implementation //===================================================================================

uses
  ZetaCommonTools;

const
  QRY_DELETE = 'delete from %s where ( PU_CODIGO = %s )';

constructor TdmServerCatalogos.Create;
begin
  inherited;
  oZetaProvider := TdmZetaServerProvider.Create(nil);
end;

destructor TdmServerCatalogos.Destroy;
begin
  FreeAndNil(oZetaProvider);
  inherited;
end;

procedure TdmServerCatalogos.SetTablaInfo(eCatalogo: ETipoCatalogo);
begin
  with oZetaProvider.TablaInfo do
    case eCatalogo of
      ePuestos:
        SetInfo( 'PUESTO', 'PU_CODIGO,PU_DESCRIP,PU_INGLES,PU_CLASIFI,PU_NUMERO,PU_TEXTO, ' +
                           'PU_DETALLE, PU_REPORTA, PU_PLAZAS, PU_NIVEL0, PU_NIVEL1, PU_NIVEL2, ' +
                           'PU_NIVEL3, PU_NIVEL4,PU_NIVEL5, PU_NIVEL6, PU_NIVEL7, PU_NIVEL8, ' +
                           'PU_NIVEL9, '+
                           {$ifdef ACS}
                           'PU_NIVEL10, PU_NIVEL11, PU_NIVEL12, ' +
                           {$endif}
                           'PU_TURNO, PU_AUTOSAL, PU_CONTRAT, PU_TABLASS, PU_PATRON, PU_AREA, ' +
                           'PU_ZONA_GE, PU_CHECA, PU_SALARIO, PU_PER_VAR, PU_TIPO, PU_ACTIVO, ' +
                           'PU_COSTO1, PU_COSTO2, PU_COSTO3, PU_LEVEL, PU_SAL_MIN, PU_SAL_MAX, ' +
                           'PU_SAL_MED, PU_SAL_EN1, PU_SAL_EN2,PU_SUB_CTA,PU_CLAVE',
                  'PU_CODIGO' );
      eConceptos:
        SetInfo('CONCEPTO', 'CO_A_PTU,CO_ACTIVO,CO_CALCULA,CO_DESCRIP,CO_FORMULA,CO_G_IMSS,' +
                            'CO_G_ISPT,CO_IMP_CAL,CO_IMPRIME,CO_LISTADO,CO_MENSUAL,CO_NUMERO,' +
                            'CO_QUERY,CO_RECIBO,CO_TIPO,CO_X_ISPT,CO_SUB_CTA,CO_ISN,CO_FRM_ALT,CO_USO_NOM,' +
                            {$IFDEF QUINCENALES}
                            'CO_CAMBIA,' +
                            {$ENDIF}
                            'CO_D_EXT,CO_D_NOM,CO_D_BLOB,CO_NOTA,CO_VER_INF,CO_VER_SUP,CO_LIM_INF,' +
                            'CO_LIM_SUP,CO_VER_ACC,CO_GPO_ACC, CO_SUMRECI',
                'CO_NUMERO');
      ePrestaciones:
        SetInfo( 'SSOCIAL',
                   'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_PRIMAVA,TB_DIAS_AG,' +
                   'TB_PAGO_7,TB_PRIMA_7,TB_PRIMADO,TB_DIAS_AD,TB_ACTIVO,TB_NIVEL0',
                 'TB_CODIGO' );
      {$ifdef QUINCENALES}
      eTPeriodos:
        SetInfo( 'TPERIODO',
                   'TP_TIPO,TP_NOMBRE,TP_DESCRIP,TP_DIAS,TP_HORAS,TP_DIAS_BT,TP_DIAS_EV,' +
                   'TP_HORASJO,TP_DIAS_7,TP_NIVEL0,TP_TOT_EV1,TP_TOT_EV2,TP_TOT_EV3,TP_TOT_EV4,' +
                   'TP_TOT_EV5',
                 'TP_TIPO' );
      {$endif}
      eUsuario:
        SetInfo( 'USUARIO',
                   'US_CODIGO, US_CORTO, GR_CODIGO, US_NIVEL, US_NOMBRE, US_PASSWRD, US_BLOQUEA, ' +
                   'US_CAMBIA, US_FEC_IN, US_FEC_OUT, US_FEC_SUS, US_DENTRO, US_ARBOL, US_FORMA, ' +
                   'US_BIT_LST, US_FEC_PWD, US_EMAIL, US_FORMATO, US_LUGAR, US_MAQUINA, ' +
                   'CM_CODIGO, CB_CODIGO, US_DOMAIN, US_PORTAL, US_ACTIVO, US_JEFE ',
                 'US_CODIGO' );
      eDatosEmpleado:
        setInfo( 'COLABORA',
                   'CB_CODIGO,CB_ACTIVO,CB_APE_MAT,CB_APE_PAT,CB_AUTOSAL,CB_BAN_ELE,CB_CARRERA,' +
                   'CB_CHECA,CB_CIUDAD,CB_CLASIFI,CB_CODPOST,CB_CONTRAT,CB_CREDENC,CB_CURP,' +
                   'CB_CALLE,CB_COLONIA,CB_EDO_CIV,CB_FEC_RES,CB_EST_HOR,CB_EST_HOY,CB_ESTADO,' +
                   'CB_ESTUDIO,CB_EVALUA,CB_EXPERIE,CB_FEC_ANT,CB_FEC_BAJ,CB_FEC_BSS,CB_FEC_CON,' +
                   'CB_FEC_ING,CB_FEC_INT,CB_FEC_NAC,CB_FEC_REV,CB_FEC_VAC,CB_G_FEC_1,CB_G_FEC_2,' +
                   'CB_G_FEC_3,CB_G_LOG_1,CB_G_LOG_2,CB_G_LOG_3,CB_G_NUM_1,CB_G_NUM_2,CB_G_NUM_3,' +
                   'CB_G_TAB_1,CB_G_TAB_2,CB_G_TAB_3,CB_G_TAB_4,CB_G_TEX_1,CB_G_TEX_2,CB_G_TEX_3,' +
                   'CB_G_TEX_4,CB_HABLA,CB_TURNO,CB_IDIOMA,CB_INFCRED,CB_INFMANT,CB_INFTASA,' +
                   'CB_LA_MAT,CB_LAST_EV,CB_LUG_NAC,CB_MAQUINA,CB_MED_TRA,CB_PASAPOR,CB_FEC_INC,' +
                   'CB_FEC_PER,CB_NOMYEAR,CB_NACION,CB_NOMTIPO,CB_NEXT_EV,CB_NOMNUME,CB_NOMBRES,' +
                   'CB_PATRON,CB_PUESTO,CB_RFC,CB_SAL_INT,CB_SALARIO,CB_SEGSOC,CB_SEXO,' +
                   'CB_TABLASS,CB_TEL,CB_VIVECON,CB_VIVEEN,CB_ZONA,CB_ZONA_GE,CB_NIVEL1,' +
                   'CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_INFTIPO,'+
                   'CB_NIVEL8,CB_NIVEL9,' +
                   {$ifdef ACS}
                   'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12,'+
                   {$endif}
                   'CB_DER_FEC,CB_DER_PAG,CB_V_PAGO,CB_DER_GOZ,CB_V_GOZO,CB_TIP_REV,CB_MOT_BAJ,' +
                   'CB_FEC_SAL,CB_INF_INI,CB_INF_OLD,CB_OLD_SAL,CB_OLD_INT,CB_PRE_INT,CB_PER_VAR,' +
                   'CB_SAL_TOT,CB_TOT_GRA,CB_FAC_INT,CB_RANGO_S,CB_CLINICA,CB_NIVEL0,CB_AREA,' +
                   'CB_CANDIDA,CB_ID_NUM,CB_ENT_NAC,CB_COD_COL,CB_G_FEC_4,CB_G_FEC_5,'+
                   {$ifndef DOS_CAPAS}
                   'CB_ID_BIO, CB_GP_COD, '+
                   {$endif} // SYNERGY
                   'CB_G_FEC_6,CB_G_FEC_7,CB_G_FEC_8,CB_G_LOG_4,CB_G_LOG_5,CB_G_LOG_6,CB_G_LOG_7,' +
                   'CB_G_LOG_8,CB_G_NUM_4,CB_G_NUM_5,CB_G_NUM_6,CB_G_NUM_7,CB_G_NUM_8,CB_G_NUM_9,' +
                   'CB_G_NUM10,CB_G_NUM11,CB_G_NUM12,CB_G_NUM13,CB_G_NUM14,CB_G_NUM15,CB_G_NUM16,' +
                   'CB_G_NUM17,CB_G_NUM18,CB_G_TAB_5,CB_G_TAB_6,CB_G_TAB_7,CB_G_TAB_8,CB_G_TAB_9,' +
                   'CB_G_TAB10,CB_G_TAB11,CB_G_TAB12,CB_G_TAB13,CB_G_TAB14,CB_G_TEX_5,CB_G_TEX_6,' +
                   'CB_G_TEX_7,CB_G_TEX_8,CB_G_TEX_9,CB_G_TEX10,CB_G_TEX11,CB_G_TEX12,CB_G_TEX13,' +
                   'CB_G_TEX14,CB_G_TEX15,CB_G_TEX16,CB_G_TEX17,CB_G_TEX18,CB_G_TEX19,CB_SUB_CTA,' +
                   'CB_NETO,CB_DER_PV,CB_V_PRIMA,CB_NOMINA,CB_RECONTR,CB_DISCAPA,CB_INDIGE,' +
                   'CB_FONACOT,CB_EMPLEO,US_CODIGO, CB_FEC_COV,CB_G_TEX20,CB_G_TEX21,CB_G_TEX22,' +
                   'CB_G_TEX23,CB_G_TEX24,CB_INFACT,CB_INFDISM, CB_NUM_EXT, CB_NUM_INT,' +
                   'CB_INF_ANT,CB_TDISCAP,CB_ESCUELA,CB_TESCUEL,CB_TITULO,CB_YTITULO,CB_CTA_VAL,' +
                   'CB_CTA_GAS,CB_MUNICIP,CB_TSANGRE,CB_ALERGIA,CB_BRG_ACT,CB_BRG_TIP,CB_BRG_ROL,' +
                   'CB_BRG_JEF,CB_BRG_CON,CB_BRG_PRA,CB_BRG_NOP',
                 'CB_CODIGO' );
      eEmpFoto:
        setInfo( 'IMAGEN', 'CB_CODIGO,IM_TIPO,IM_BLOB,IM_OBSERVA', 'CB_CODIGO,IM_TIPO' );
      fnDatosNomina:
        SetInfo( 'NOMINA',
                   'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO,CB_CLASIFI,CB_TURNO,CB_PATRON,CB_PUESTO,' +
                   'CB_ZONA_GE,NO_FOLIO_1,NO_FOLIO_2,NO_FOLIO_3,NO_FOLIO_4,NO_FOLIO_5,NO_OBSERVA,' +
                   'NO_STATUS,US_CODIGO,NO_USER_RJ,CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,' +
                   'CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,CB_NIVEL9,' +
                   {$ifdef ACS}
                   'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12, ' +
                   {$endif}
                   'CB_SAL_INT,CB_SALARIO,NO_DIAS,NO_ADICION,NO_DEDUCCI,NO_NETO,NO_DES_TRA,' +
                   'NO_DIAS_AG,NO_DIAS_VA,NO_DOBLES,NO_EXTRAS,NO_FES_PAG,NO_FES_TRA,NO_HORA_CG,' +
                   'NO_HORA_SG,NO_HORAS,NO_IMP_CAL,NO_JORNADA,NO_PER_CAL,NO_PER_MEN,NO_PERCEPC,' +
                   'NO_TARDES,NO_TRIPLES,NO_TOT_PRE,NO_VAC_TRA,NO_X_CAL,NO_X_ISPT,NO_X_MENS,' +
                   'NO_D_TURNO,NO_DIAS_AJ,NO_DIAS_AS,NO_DIAS_CG,NO_DIAS_EM,NO_DIAS_FI,NO_DIAS_FJ,' +
                   'NO_HORASNT,NO_HORAPDT,NO_DIAS_FV,NO_DIAS_IN,NO_DIAS_NT,NO_DIAS_OT,' +
                   'NO_DIAS_RE,NO_DIAS_SG,NO_DIAS_SS,NO_DIAS_SU,NO_HORA_PD,NO_LIQUIDA,NO_FUERA,' +
                   'NO_EXENTAS,NO_FEC_PAG,NO_USR_PAG,CB_NIVEL0,NO_DIAS_SI,NO_SUP_OK,NO_FEC_OK,' +
                   'NO_HOR_OK,CB_BAN_ELE,NO_APROBA,NO_GLOBAL,CB_CTA_GAS,CB_CTA_VAL,NO_PER_ISN',
                 'PE_YEAR,PE_TIPO,PE_NUMERO,CB_CODIGO' );
      ePtoFijas:
        SetInfo( 'PTOFIJAS', 'PU_CODIGO,PF_FOLIO,PF_CODIGO', 'PU_CODIGO,PF_FOLIO' );
      ePerfiles:
        SetInfo( 'PERFIL',
                   'PU_CODIGO,PF_ESTUDIO,PF_DESCRIP,PF_OBJETIV,PF_FECHA,PF_EXP_PTO,PF_POSTING,' +
                   'PF_CONTRL1,PF_CONTRL2,PF_NUMERO1,PF_NUMERO2,PF_TEXTO1,PF_TEXTO2,PF_EDADMIN,' +
                   'PF_EDADMAX,PF_SEXO',
                 'PU_CODIGO');
      eValuacion:
        SetInfo( 'VAL_PTO',
                   'VP_FOLIO,VL_CODIGO,PU_CODIGO,VP_FECHA,VP_COMENTA,VP_GRADO,VP_PT_CODE,VP_PT_NOM,' +
                   'VP_PT_GRAD,VP_FEC_INI,VP_FEC_FIN,US_CODIGO,VP_STATUS',
                 'VP_FOLIO');
    end;
end;

procedure TdmServerCatalogos.SetDetailInfo(eDetail: eTipoCatalogo);
begin
  with oZetaProvider.DetailInfo do
    case eDetail of
      ePrestaciones:
        SetInfoDetail( 'PRESTACI',
                         'TB_CODIGO,PT_YEAR,PT_DIAS_VA,PT_PRIMAVA,PT_DIAS_AG,PT_DIAS_AD,' +
                         'PT_PRIMADO,PT_PAGO_7,PT_PRIMA_7,PT_FACTOR',
                       'TB_CODIGO,PT_YEAR', 'TB_CODIGO' );
      {eRPatron:
        SetInfoDetail( 'PRIESGO',
                         'TB_CODIGO,RT_FECHA,RT_PRIMA,RT_PTANT,RT_S,RT_I,RT_D,RT_N,RT_M,RT_F',
                       'TB_CODIGO,RT_FECHA', 'TB_CODIGO' );
      eFolios:
        SetInfoDetail( 'ORDFOLIO',
                         'FL_CODIGO,OF_POSICIO,OF_CAMPO,OF_TITULO,OF_DESCEND',
                       'FL_CODIGO,OF_POSICIO', 'FL_CODIGO' );
      eMatrizCurso, eMatrizPuesto:
        SetInfoDetail( 'ENTNIVEL',
                         'PU_CODIGO,CU_CODIGO,ET_CODIGO',
                       'PU_CODIGO,CU_CODIGO,ET_CODIGO', 'PU_CODIGO,CU_CODIGO' );
      eMatrizCertificPuesto,eMatrizPuestoCertific:
        SetInfoDetail( 'CERNIVEL',
                         'PU_CODIGO,CI_CODIGO,CN_CODIGO',
                       'PU_CODIGO,CI_CODIGO,CN_CODIGO', 'PU_CODIGO,CI_CODIGO' );}
//      eHistRev:
//        SetInfoDetail( 'CUR_REV',
//                         'CU_CODIGO,CH_REVISIO,CH_FECHA,CH_OBSERVA,US_CODIGO',
//                       'CU_CODIGO, CH_REVISIO, CH_FECHA', 'CU_CODIGO' );
    end;
end;

function TdmServerCatalogos.GetCatalogo( const eCatalogo: eTipoCatalogo; const Empresa: OleVariant ): OLEVariant;
begin
  SetTablaInfo( eCatalogo );
  if ( eCatalogo in CatalogosConDetail ) then begin
    SetDetailInfo( eCatalogo );
    Result := oZetaProvider.GetMasterDetail( Empresa );
  end else
    Result := oZetaProvider.GetTabla( Empresa );
end;

function TdmServerCatalogos.GetPerfiles( Empresa: OleVariant;const sPuesto: WideString): OleVariant;
begin
  with oZetaProvider do begin
    SetTablaInfo( ePerfiles );
    TablaInfo.Filtro := Format( 'PU_CODIGO = %s', [ EntreComillas(sPuesto) ] );
    Result := GetTabla( Empresa );
  end;
  //SetComplete;
end;

function TdmServerCatalogos.CopiarPerfil(Empresa: OleVariant; const sFuente, sDestino: WideString): OleVariant;
const
  {$ifdef INTERBASE}
  Q_SP_COPIA_PERFIL = 'execute procedure SP_COPIA_PERFIL ( %s , %s )';
  {$endif}
  {$ifdef MSSQL}
  Q_SP_COPIA_PERFIL = 'exec dbo.SP_COPIA_PERFIL  %s, %s ' ;
  {$endif}
var
  FCopiar: TZetaCursor;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    FCopiar := CreateQuery( Format( Q_SP_COPIA_PERFIL, [ EntreComillas( sFuente ),EntreComillas( sDestino )  ] ) );
    try
      EmpiezaTransaccion;
      try
        Ejecuta( FCopiar );
        TerminaTransaccion( True );
      except
        on Error: Exception do begin
          RollBackTransaccion;
          raise;
        end;
      end;
    finally
      FreeAndNil( FCopiar );
    end;
    Result := self.GetPerfiles( Empresa, sDestino );
  end;
  //SetComplete;
end;

function TdmServerCatalogos.GetScript(const eTipo: eTipoRecursos): String;
begin
  Result := '';
  case etipo of
    eTipoKardex:
      Result := 'select TB_CODIGO, TB_ELEMENT, TB_NIVEL from TKARDEX';
    eCamposAdicAcceso:
      Result := 'select CA.GX_CODIGO,CA.CX_NOMBRE, CA.CX_OBLIGA, CA.CX_TITULO ' +
                'from CAMPO_AD CA ' +
                  'left outer join GR_AD_ACC GA on GA.CM_CODIGO = %0:s and GA.GR_CODIGO = %1:d and ' +
                                                  'GA.GX_CODIGO = CA.GX_CODIGO ' +
                'where ( GA.GX_DERECHO >= 8 ) or ( %2:d = %1:d ) ';
  end;
end;

function TdmServerCatalogos.GetSQLScript( const iScript: Integer ): String;
begin
  case iScript of
    Q_TURNO_GET_DATOS:
      Result := 'select ' +
                  'TU_RIT_PAT, TU_HOR_1, TU_HOR_2, TU_HOR_3, TU_HOR_4, TU_HOR_5, TU_HOR_6, ' +
                  'TU_HOR_7, TU_TIP_1, TU_TIP_2, TU_TIP_3, TU_TIP_4, TU_TIP_5, TU_TIP_6, ' +
                  'TU_TIP_7, TU_CODIGO, TU_RIT_INI, TU_HORARIO, TU_HOR_FES, TU_VACA_HA, ' +
                  'TU_VACA_SA, TU_VACA_DE, '+
                {$ifdef INTERBASE}
                  'COLABORA.CB_CREDENC '+
                'from ' +
                  'TURNO, SP_FECHA_KARDEX( :Fecha, :Empleado ) FK, COLABORA ' +
                'where ' +
                  '( TURNO.TU_CODIGO = FK.CB_TURNO ) and ( COLABORA.CB_CODIGO = :Employee )';
                {$endif}
                {$ifdef MSSQL}
                  'CB_CREDENC ' +
                'from ' +
                  'DBO.TURNO_EMPLEADO( :Fecha, :Empleado )';
                {$endif}
      Q_COSTEO_CALCULA:
        Result := 'execute procedure dbo.Costos_CalculaPeriodo( :Year, :Tipo, :Numero )';
      Q_COSTEO_CUENTA_NOMINAS:
        Result := 'select COUNT(*) Cuantos from NOMINA where  ( PE_YEAR = :Year ) and ' +
                                                             '( PE_TIPO = :Tipo ) and '+
                                                             '( PE_NUMERO = :Numero ) ';
      Q_RECALCULA_KARDEX:
        Result := 'EXECUTE PROCEDURE RECALCULA_KARDEX( :EMPLEADO )';{OP: 27/05/08}
      else
        Result := '';
  end;
end;

procedure TdmServerCatalogos.GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
  eClase: eClaseBitacora;
  sTitulo: string;

  procedure DatosBitacora(eClaseCatalogo: eClaseBitacora; sDescripcion, sKeyField: string);
  var
    oField: TField;
  begin
    eClase := eClaseCatalogo;
    sTitulo := sDescripcion;

    if StrLleno(sKeyField) then begin
      oField := DeltaDS.FieldByName(sKeyField);

      if oField is TNumericField then
        sTitulo := sTitulo + ' ' + IntToStr(ZetaServerTools.CampoOldAsVar(oField))
      else
        sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar(oField);
    end;
  end;

begin
  if UpdateKind in [ukModify, ukDelete] then begin
    eClase := clbNinguno;

    case FTipoCatalogo of
      ePuestos:
        DatosBitacora(clbPuesto, 'Puesto :', 'PU_CODIGO');
      eTurnos:
        DatosBitacora(clbTurno, 'Turno :', 'TU_CODIGO');
      eCursos:
        DatosBitacora(clbCursos, 'Curso :', 'CU_CODIGO');
      eClasifi:
        DatosBitacora(clbClasifi, 'Clasificaci�n :', 'TB_CODIGO');
      eConceptos:
        DatosBitacora(clbConcepto, 'Concepto #', 'CO_NUMERO');
      eCondiciones:
        DatosBitacora(clbCondicion, 'Condici�n :', 'QU_CODIGO');
      eContratos:
        DatosBitacora(clbContratos, 'Contrato :', 'TB_CODIGO');
      eEventos:
        DatosBitacora(clbEventos, 'Evento :', 'EV_CODIGO');
      eFolios:
        DatosBitacora(clbFolios, 'Folio :', 'FL_CODIGO');
      eHorarios:
        DatosBitacora(clbHorario, 'Horario :', 'HO_CODIGO');
      eInvitadores:
        DatosBitacora(clbInvitadores, 'Invitador :', 'IV_CODIGO');
      eMaestros:
        DatosBitacora(clbMaestros, 'Maestro :', 'MA_CODIGO');
      eOtrasPer:
        DatosBitacora(clbOtrasPer, 'Percepci�n Fija :', 'TB_CODIGO');
      ePrestaciones:
        DatosBitacora(clbPrestaciones, 'Prestaci�n :', 'TB_CODIGO');
      eReglas:
        DatosBitacora(clbReglas, 'Regla de Cafeter�a :', 'CL_CODIGO');
      eAccReglas:
        DatosBitacora(clbAccReglas, 'Regla de Acceso :', 'AE_CODIGO');
      eRPatron:
        DatosBitacora(clbRPatron, 'Registro Patronal :', 'TB_CODIGO');
      eNomParam:
        DatosBitacora(clbNomParam, 'Par�metro de N�mina :', 'NP_FOLIO');
      eTools:
        DatosBitacora(clbTools, 'Herramienta :', 'TO_CODIGO');
      eCalendario:
        DatosBitacora(clbCalendario, 'Curso: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CU_CODIGO')) + ' Fecha: ' + FormatDateTime('dd/mmm/yyyy',
          ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CC_FECHA'))), VACIO);

      eFestTurno:
        DatosBitacora(clbFestTurno, 'Turno: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('TU_CODIGO')) + ' Mes: ' +
          IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('FE_MES'))) + ' D�a: ' +
          IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('FE_DIA'))), VACIO);
      ePeriodos: begin
          oZetaProvider.InitArregloTPeriodo;
          DatosBitacora(clbPeriodos,
            'Periodo ' + GetPeriodoInfo(CampoOldAsVar(DeltaDS.FieldByName('PE_YEAR')),
            CampoOldAsVar(DeltaDS.FieldByName('PE_NUMERO')),
            eTipoPeriodo(CampoOldAsVar(DeltaDS.FieldByName('PE_TIPO')))), VACIO);
        end;
      eMatrizCurso:
        DatosBitacora(clbMatrizCurso, 'Curso: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CU_CODIGO')) + ' Puesto: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PU_CODIGO')), VACIO);
      eMatrizPuesto:
        DatosBitacora(clbMatrizPuesto, 'Puesto: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PU_CODIGO')) + ' Curso: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CU_CODIGO')), VACIO);
      eSesiones:
        DatosBitacora(clbSesiones, 'Puesto: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CB_PUESTO')) + ' Curso: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CU_CODIGO')), VACIO);
      eAulas:
        DatosBitacora(clbAulas, 'Aula :', 'AL_CODIGO');

      ePrerequisitosCurso:
        DatosBitacora(clbPrerequisitosCurso,
          'Curso: ' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CU_CODIGO')) +
          'Curso Requerido: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CP_CURSO')), VACIO);

      eCertificaciones:
        DatosBitacora(clbCertificaciones, 'Certificaci�n: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CI_CODIGO')), VACIO);
      eTiposPoliza:
        DatosBitacora(clbTiposPoliza, 'Tipos de P�liza: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PT_CODIGO')), VACIO);
      {$IFDEF QUINCENALES}
      eTPeriodos:
        DatosBitacora(clbPeriodos, 'Tipo de periodo: ', 'TP_TIPO');
      {$ENDIF}
      eSeccionesPerfil:
        DatosBitacora(clbSeccionesPerfil, 'Secci�n de Perfil: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('DT_CODIGO')), VACIO);

      eCamposPerfil:
        DatosBitacora(clbCamposPerfil, 'Campo de Perfil: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('DT_CODIGO')), VACIO);

      ePerfiles:
        DatosBitacora(clbPerfiles, 'Perfil: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PU_CODIGO')), VACIO);

      eProvCap:
        DatosBitacora(clbProvCap, 'Proveedor de Capacitaci�n: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PC_CODIGO')), VACIO);
      {$IFNDEF DOS_CAPAS}
      eValPlant:
        DatosBitacora(clbValPlantillas,
          'Plantilla: ' + IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('VL_CODIGO')
          )), VACIO);

      eValFact:
        DatosBitacora(clbFactores, 'Plantilla: ' +
          IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('VL_CODIGO'))) + ' Factor:' +
          ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('VF_CODIGO')), VACIO);

      eValSubFact:
        DatosBitacora(clbSubfactores, 'Plantilla: ' +
          IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('VL_CODIGO'))) + ' Subfactor:'
          + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('VS_CODIGO')), VACIO);

      eValNiveles:
        DatosBitacora(clbValNiveles, 'Nivel: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('VN_CODIGO')) + ' Subfactor:' +
          IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('VS_ORDEN'))), VACIO);
      {$ENDIF}
      eRSocial:
        DatosBitacora(clbRSocial, 'Raz�n Social: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('RS_CODIGO')), VACIO);

      eMatrizCertificPuesto:
        DatosBitacora(clbMatrizCertificaPuesto, 'Certificaci�n: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CI_CODIGO')) + ' Puesto: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PU_CODIGO')), VACIO);
      eMatrizPuestoCertific:
        DatosBitacora(clbMatrizPuestoCertifica,
          'Puesto: ' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('PU_CODIGO')) +
          ' Certificaci�n: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CI_CODIGO')), VACIO);
      eReglasPrestamos:
        DatosBitacora(clbReglaPrestamo,
          'C�digo: ' + IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RP_CODIGO'))) +
          ' Mensaje: ' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RP_LETRERO')), VACIO);
      ePrestaXRegla:
        DatosBitacora(clbPrestaXRegla,
          'C�digo: ' + IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RP_CODIGO'))) +
          ' Tipo de Pr�stamo: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('TB_CODIGO')), VACIO);
      eTipoPension:
        DatosBitacora(clbCatNomTPension, 'Tipo de Pensi�n: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('TP_CODIGO')), VACIO);

      eSegurosGastosMed:
        DatosBitacora(clbCatGralSGM, 'Seguro de Gastos M�dicos: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PM_CODIGO')), VACIO);

      eVigenciasSeguro:
        DatosBitacora(clbCatGralSGM, 'Vigencias de SGM: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PM_CODIGO')) + ' Vigencia: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PV_REFEREN')), VACIO);

      eCosteoGrupos:
        DatosBitacora(clbCosteoGrupos, 'Grupo de Costeo:', 'GpoCostoCodigo');
      eCosteoCriterios:
        DatosBitacora(clbCosteoCriterios, 'Criterio de Costeo:', 'CritCostoID');
      eCriteriosPorConcepto:
        DatosBitacora(clbCosteoCriteriosPorConcepto, 'Concepto Criterio:', 'CO_NUMERO');

      eCatTablaAmortizacion:
        DatosBitacora(clbCatGralSGM, 'Tabla de Cotizaci�n SGM: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('AT_CODIGO')), VACIO);
      eCatCostosTablaAmort:
        DatosBitacora(clbCatGralSGM, 'Costos de Cotizaci�n SGM: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('AT_CODIGO')), VACIO);

      eCompetencias:
        DatosBitacora(clbCatCompetencias, 'Competencia:', 'CC_CODIGO');
      eRevisionCompetencias:
        DatosBitacora(clbCatCompetencias, 'Competencia: ' +
          IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CC_CODIGO'))) + ' Revision: '
          + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RC_REVISIO')), VACIO);
      eNivelesCompetencias:
        DatosBitacora(clbCatCompetencias, 'Competencia: ' +
          IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CC_CODIGO'))) + ' Nivel: ' +
          ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('NC_NIVEL')), VACIO);
      eCursosCompetencias:
        DatosBitacora(clbCatCompetencias, 'Competencia: ' +
          IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CC_CODIGO'))) + ' Curso: ' +
          ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CU_CODIGO')), VACIO);
      eCPerfiles:
        DatosBitacora(clbCatPerfiles, 'Perfil:', 'CP_CODIGO');
      eRevisionPerfiles:
        DatosBitacora(clbCatPerfiles,
          'Perfil: ' + IntToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CP_CODIGO'))) +
          ' Revisi�n: ' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RP_REVISIO')), VACIO);
      ePerfilesCompetencias:
        DatosBitacora(clbPerfilesCompetencias,
          'Perfil: ' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CP_CODIGO')) +
          ' Competencia: ' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('CC_CODIGO')
          ), VACIO);
      ePuestoGpoCompeten:
        DatosBitacora(clbPuesto, 'Grupo Compentencia: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('CP_CODIGO')) + ' Puesto: ' + ZetaServerTools.CampoOldAsVar
          (DeltaDS.FieldByName('PU_CODIGO')), VACIO);

      else
        raise Exception.Create
          ('No ha sido Integrado el Cat�logo al Case de <DServerCatalogos.GrabaCambiosBitacora>')
    end;

    with oZetaProvider do
      if UpdateKind = ukModify then
        CambioCatalogo(sTitulo, eClase, DeltaDS)
      else
        BorraCatalogo(sTitulo, eClase, DeltaDS);
  end;
end;

procedure TdmServerCatalogos.GrabaCambiosPuesto(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
const
  {$IFDEF INTERBASE}
  Q_CHECAPUESTOS = 'execute procedure CHECK_REPORT_CHAIN( %s )';
  {$ENDIF}
  {$IFDEF MSSQL}
  Q_CHECAPUESTOS = 'exec dbo.CHECK_REPORT_CHAIN %s';
  {$ENDIF}
var
  FPuestos: TZetaCursor;
begin
  if (UpdateKind in [ukInsert, ukModify]) then begin
    if CambiaCampo(DeltaDS.FieldByName('PU_REPORTA')) then begin
      with oZetaProvider do begin
        FPuestos := CreateQuery(Format(Q_CHECAPUESTOS,
          [EntreComillas(DeltaDS.FieldByName('PU_REPORTA').AsString)]));
        try
          Ejecuta(FPuestos);
        finally
          FreeAndNil(FPuestos);
        end;
      end;
    end;
  end;
  GrabaCambiosBitacora(Sender, SourceDS, DeltaDS, UpdateKind);
end;

function TdmServerCatalogos.GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer;
  oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
  FTipoCatalogo := eTipoCatalogo(iCatalogo);

  SetTablaInfo(FTipoCatalogo);
  with oZetaProvider do begin
    EmpresaActiva := Empresa; // Lo requiere Bit�cora
    InitGlobales;
    if (FTipoCatalogo = eSesiones) then
      begin end //TablaInfo.BeforeUpdateRecord := BeforeUpdateSesion
    else if (FTipoCatalogo = eInvitadores) then
      begin end //TablaInfo.BeforeUpdateRecord := BeforeUpdateInvitadores
    else if (FTipoCatalogo = eCursos) then
      begin end; //TablaInfo.BeforeUpdateRecord := BeforeUpdateCursos;
    if (FTipoCatalogo = ePuestos) then
      TablaInfo.AfterUpdateRecord := GrabaCambiosPuesto
    else
      TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
    if (FTipoCatalogo in CatalogosConDetail) then begin
      SetDetailInfo(FTipoCatalogo);
      Result := GrabaMasterDetail(Empresa, oDelta, ErrorCount);
    end else
      Result := GrabaTabla(Empresa, oDelta, ErrorCount);
  end;
//  SetComplete;
end;

end.
