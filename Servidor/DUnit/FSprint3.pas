unit FSprint3;

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Sistema............: TRESS
// Prop�sito..........: M�dulo para pruebas unitarias del M�dulo ServerCatalogos, Sprint 3
// Desarrollador......: Ricardo Carrillo Morales, 23/Ago/2013
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

interface

{$INCLUDE JEDI.INC}

uses
  TestFramework, FSprint0, SysUtils, Windows, Controls, DateUtils, DB, ADODB,
  {$IFNDEF DELPHIXE3_UP} ODSI,{$ENDIF}
  //
  SQLServer,
  // Sistema TRESS
  DZetaServerProvider, ZetaServerDataSet, ZetaClientDataSet, ZetaCommonTools, ZetaCommonClasses,
  DServerCatalogos, ZetaCommonLists;

type
  TSprint3 = class(TSprint0)

  private
    {private declarations}
  protected
    procedure PollError( const sDatos, sError: string);
    procedure PollException(const sDatos: string; Error: Exception);
  public
    {public declarations}
  published
    procedure Escenario_Procedure_ExecuteProcedureParams;
    procedure Escenario_Procedure_Call;
    procedure Escenario_Procedure_SelectEscalarNumero;
    procedure Escenario_Procedure_SelectEscalarTexto;
    procedure Escenario_Procedure_ExecuteProcedureFormat;
    procedure Escenario_InsercionDirecta;
    procedure Escenario_RollBackTransaction;
    procedure Escenario_UpdateProcessLog;
    procedure Escenario_Procedure_ParamsAs;
    procedure Escenario_GrabaTabla_Insercion;
    procedure Escenario_Blancos_Finales;
    procedure Escenario_PenalizacionPorTrimRight;
    procedure Escenario_IssueColabora;
    procedure Escenario_Transacciones;
  end;

implementation //===================================================================================

const
  K_POLL_ESCRIBE = 'insert into POLL(  PO_LINX,  PO_EMPRESA,  PO_NUMERO,  PO_LETRA,  PO_FECHA,  PO_HORA ) ' +
                            'values ( :PO_LINX, :PO_EMPRESA, :PO_NUMERO, :PO_LETRA, :PO_FECHA, :PO_HORA )';
  K_POST_ERROR = 'Cia %s Emp %d Cred %s Fecha %s Hora %s';
  K_ANCHO_EMPRESA =  1;
  K_ANCHO_LINX    = 11;
  K_ANCHO_LETRA   =  1;

procedure TSprint3.Escenario_Procedure_ExecuteProcedureParams;
const
  K_CODIGO = 3;
  K_LLAVE = 992;
  K_SALARIO = 4999.9;
var
  FDataSet: TDataSet;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
  ConectaDemo;
  Tabla := 'COLABORA';
  with Fixture.oZetaProvider, Fixture do try
    try
      // Mostrar salario
      Status('Salario original');
      FDataSet := CreateQuery( 'select CB_SALARIO from COLABORA where CB_CODIGO = :CB_CODIGO' );
      ParamAsInteger( FDataSet, 'CB_CODIGO', K_CODIGO);
      {$IFDEF DELPHIXE3_UP}
      Status('  => ' + Trim(TADOQuery(FDataSet).SQL.Text));
      {$ELSE}
      Status('  => ' + Trim(TOEQuery(FDataSet).SQL.Text));
      {$ENDIF}
      FDataSet.Open;
      Status('  -> Ok'); Status('');
      MostrarDatos(FDataSet);
      FreeAndNil(FDataSet);

      EmpiezaTransaccion;
      // Modificar el salario
      FDataSet := CreateQuery( 'UPDATE KARDEX set CB_SALARIO = :CB_SALARIO where LLAVE = :LLAVE ' );
      {$IFDEF DELPHIXE3_UP}
      Status('Ejecutando ' + Trim(TADOQuery(FDataSet).SQL.Text));
      {$ELSE}
      Status('Ejecutando ' + Trim(TOEQuery(FDataSet).SQL.Text));
      {$ENDIF}
      ParamAsFloat( FDataSet, 'CB_SALARIO', K_SALARIO );
      ParamAsInteger( FDataSet, 'LLAVE', K_LLAVE);
      Ejecuta(FDataSet);
      FreeAndNil(FDataSet);
      Status('  -> Ok'); Status('');

      // Actualizar Kardex
      FDataSet := CreateQuery( GetSQLScript( Q_RECALCULA_KARDEX ) );
      {$IFDEF DELPHIXE3_UP}
      Status('Ejecutando ' + Trim(TADOQuery(FDataSet).SQL.Text));
      {$ELSE}
      Status('Ejecutando ' + Trim(TOEQuery(FDataSet).SQL.Text));
      {$ENDIF}
      ParamAsInteger( FDataSet, 'EMPLEADO', K_CODIGO );
      Ejecuta( FDataSet );
      FreeAndNil(FDataSet);
      Status('  -> Ok'); Status('');

      TerminaTransaccion( True );

      // Mostrar nuevo salario
      Status('Nuevo salario');
      FDataSet := CreateQuery( 'select CB_SALARIO from COLABORA where CB_CODIGO = :CB_CODIGO' );
      ParamAsInteger( FDataSet, 'CB_CODIGO', K_CODIGO);
      {$IFDEF DELPHIXE3_UP}
      Status('  => ' + Trim(TADOQuery(FDataSet).SQL.Text));
      {$ELSE}
      Status('  => ' + Trim(TOEQuery(FDataSet).SQL.Text));
      {$ENDIF}
      FDataSet.Open;
      Status('  -> Ok'); Status('');
      MostrarDatos(FDataSet);
      FreeAndNil(FDataSet);

    except
      on Error: Exception do begin
          RollBackTransaccion;
          raise
      end;
    end;
  finally
    FreeAndNil( FDataSet );
  end;
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
end;

// C�digo tomado de DServerCatalogos.pPas
//function TdmServerCatalogos.CopiaValuacion(Empresa, Parametros: OleVariant; var ErrorCount: Integer): Integer;
procedure TSprint3.Escenario_Procedure_Call;
var
  Parametros: TZetaParams;
  oCopiar: TZetaCursor;
  Result : Integer;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
  ConectaDemo;
  Tabla := 'VAL_PTO';

  Parametros := TZetaParams.Create;
  Parametros.AddInteger('Folio', 2);
  Parametros.AddString('Puesto', '009');
  Parametros.AddDate('Fecha', Today);
  Parametros.AddInteger('Usuario', 1);

  with Fixture.oZetaProvider do begin
    //EmpresaActiva := Empresa;
    ExecSQL( EmpresaActiva, 'delete from VAL_PTO where VP_FOLIO > 4');

    AsignaParamList( Parametros.VarValues );
    oCopiar:= CreateQuery( Q_VAL_COPIA_PUESTO ); // <-----------------------------------------------
    oZCursor := CreateQuery( 'select * from VAL_PTO order by VP_FOLIO' );
    oZCursor.Open;
    MostrarDatos(oZCursor);
    oZCursor.Close;
    try
      Status('Copiando Valuaci�n con folio "2"');
      EmpiezaTransaccion;
      try
        ParamAsInteger( oCopiar, 'VP_FOLIO' , ParamList.ParamByName('Folio').AsInteger   );
           ParamAsChar( oCopiar, 'PU_CODIGO', ParamList.ParamByName('Puesto').AsString, ZetaCommonClasses.K_ANCHO_CODIGO );
           ParamAsDate( oCopiar, 'VP_FECHA' , ParamList.ParamByName('Fecha').AsDate      ) ;
        ParamAsInteger( oCopiar, 'US_CODIGO', ParamList.ParamByName('Usuario').AsInteger );
           ParamSalida( oCopiar, 'NewFolio');
        Ejecuta( oCopiar );
        TerminaTransaccion( True );
        oZCursor.Open;
        Status('  -> Ok'); Status('');
        MostrarDatos(oZCursor);
      except
        on Error: Exception do begin
          RollBackTransaccion;
          if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then begin
            DatabaseError( Format('� Valuaci�n ya Registrada con el Puesto: %s y Fecha: %s !',
                                   [ ParamList.ParamByName('Puesto').AsString ,
                                     FechaCorta( ParamList.ParamByName('Fecha').AsDate )  ] ) );
          end;
          raise;
        end;
      end;

      Result := GetParametro( oCopiar, 'NewFolio').Value;
      Status(Format('Valuaci�n ha sido copiado con el nuevo folio "%d"', [Result]));
    finally
      FreeAndNil( oCopiar );
      FreeAndNil( oZCursor );
      FreeAndNil(Parametros);
    end;
  end;
  //SetComplete;
end;

procedure TSprint3.Escenario_Procedure_SelectEscalarNumero;
const
  K_SQL = 'select CB_SALARIO = DBO.SP_KARDEX_CB_SALARIO( :Fecha, :Empleado )';
var
  FDataSet: TZetaCursor;
  iEmpleado: Integer;
  dFecha: TDate;
begin
  iEmpleado := 3;
  dFecha := Today;

  ConectaDemo;
  Tabla := '';
  with Fixture.oZetaProvider do try
    FDataSet := CreateQuery( K_SQL );
    with FDataSet do begin
      ParamAsInteger(FDataSet, 'Empleado', iEmpleado);
      ParamAsDate(FDataSet, 'Fecha', dFecha);
      Status('Ejecutando ' + K_SQL);
      Open;
      Status('  -> Ok');
      if IsEmpty then
        Status('No se ha encontrado CB_SALARIO del empleado ' + IntToStr(iEmpleado))
      else
        Status(Format('CB_SALARIO del empleado %d = %f', [iEmpleado, FieldByName('CB_SALARIO').AsFloat]));
      Close;
    end
  finally
    FreeAndNil(FDataSet)
  end;
end;

// Codigo tomado de MTS\DServerCalcNomina.pas
// function TdmServerCalcNomina.RecalcularTarjetasDataset( Dataset: TDataset): OleVariant;
procedure TSprint3.Escenario_Procedure_SelectEscalarTexto;
const
  K_SQL = 'select CB_TURNO from SP_FECHA_KARDEX( :Fecha, :Empleado )';
var
  FLeeTurno: TZetaCursor;
  iEmpleado: Integer;
  dFecha: TDate;
begin
  iEmpleado := 3;
  dFecha := Today;

  ConectaDemo;
  Tabla := '';
  with Fixture.oZetaProvider do try
    FLeeTurno := CreateQuery( K_SQL );
    with FLeeTurno do begin
      ParamAsInteger(FLeeTurno, 'Empleado', iEmpleado);
      ParamAsDate(FLeeTurno, 'Fecha', dFecha);
      Status('Ejecutando ' + K_SQL);
      Open;
      Status('  -> Ok');
      if IsEmpty then
        Status('No se ha encontrado CB_TURNO del empleado ' + IntToStr(iEmpleado))
      else
        Status(Format('CB_TURNO del empleado %d = "%s"', [iEmpleado, FieldByName('CB_TURNO').AsString]));
      Close;
    end
  finally
    FreeAndNil(FLeeTurno)
  end;
end;

procedure TSprint3.Escenario_Procedure_ExecuteProcedureFormat;
const
  K_CODIGO = 3;
  K_LLAVE = 992;
  K_SALARIO = 4999.9;
  K_MOSTRAR_SALARIO  = 'select CB_SALARIO from COLABORA where CB_CODIGO = %d';
  K_MODIFICA_SALARIO = 'UPDATE KARDEX set CB_SALARIO = %f where LLAVE = %d';
  K_RECALCULA_KARDEX = 'EXECUTE PROCEDURE RECALCULA_KARDEX( %d )';
var
  FDataSet: TDataSet;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
  ConectaDemo;
  Tabla := 'COLABORA';
  with Fixture.oZetaProvider, Fixture do try
    try
      MostrarQuery(Format( K_MOSTRAR_SALARIO, [ K_CODIGO ] ));

      EmpiezaTransaccion;
      // Modificar el salario

      FDataSet := CreateQuery( Format( K_MODIFICA_SALARIO, [K_SALARIO, K_LLAVE] ) );
      {$IFDEF DELPHIXE3_UP}
      Status('Ejecutando ' + Trim(TADOQuery(FDataSet).SQL.Text));
      {$ELSE}
      Status('Ejecutando ' + Trim(TOEQuery(FDataSet).SQL.Text));
      {$ENDIF}
      Ejecuta(FDataSet);
      FreeAndNil(FDataSet);
      Status('  -> Ok'); Status('');

      // Actualizar Kardex
      FDataSet := CreateQuery( Format( K_RECALCULA_KARDEX, [K_CODIGO] ) );
      {$IFDEF DELPHIXE3_UP}
      Status('Ejecutando ' + Trim(TADOQuery(FDataSet).SQL.Text));
      {$ELSE}
      Status('Ejecutando ' + Trim(TOEQuery(FDataSet).SQL.Text));
      {$ENDIF}
      Ejecuta( FDataSet );
      FreeAndNil(FDataSet);
      Status('  -> Ok'); Status('');

      TerminaTransaccion( True );
      MostrarQuery(Format( K_MOSTRAR_SALARIO, [ K_CODIGO ] ));

    except
      on Error: Exception do begin
          RollBackTransaccion;
          raise
      end;
    end;
  finally
    FreeAndNil( FDataSet );
  end;
end;

procedure TSprint3.PollError( const sDatos, sError: string);
begin
  {with cdsLog do begin
    try
      Append;
      FieldByName('POLL_DATA').AsString := Copy(sDatos, 1, K_ANCHO_POLL_DATA);
      FieldByName('POLL_ERR').AsString := Copy(sError, 1, K_ANCHO_POLL_ERROR);
      Post;
    except
      on Error: Exception do begin
        Cancel;
      end;
    end;
  end;}
end;

procedure TSprint3.PollException(const sDatos: string; Error: Exception);
begin
  PollError(sDatos, Error.Message);
end;

procedure TSprint3.Escenario_InsercionDirecta;
var
  i: integer;
  FDataSet: TDataSet;
  sDatos: string;
  dFecha: TDateTime;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
  ConectaComparte;
  Tabla := 'POLL';

  MostrarQuery('select * from POLL');

  Status('Insertando datos');
  with Fixture.oZetaProvider do try
    FDataset := CreateQuery( K_POLL_ESCRIBE );
    dFecha := Date();
    for I := Ord('A') to Ord('Z') do try
      ParamAsVarChar(FDataSet, 'PO_LINX'   , 'PO_LINX-' + Chr(I), K_ANCHO_LINX);
      ParamAsChar   (FDataSet, 'PO_EMPRESA', '1', K_ANCHO_EMPRESA);
      ParamAsInteger(FDataSet, 'PO_NUMERO' , 3);
      ParamAsChar   (FDataSet, 'PO_LETRA'  , Char(I), K_ANCHO_LETRA);
      ParamAsDate   (FDataSet, 'PO_FECHA'  , Int(dFecha));
      ParamAsChar   (FDataSet, 'PO_HORA'   , FormatDateTime('nnss', dFecha), K_ANCHO_HORA);
      Ejecuta(FDataSet);
    except
      on Error: Exception do begin
        sDatos := Format(K_POST_ERROR, ['1', 3, 'A', FormatDateTime('dd/mmm/yyyy', dFecha),
          FormatDateTime('nnss', dFecha)]);
        if DZetaServerProvider.PK_Violation(Error) then
          PollError(sDatos, 'Checada Ya Existe')
        else
          PollException(sDatos, Error);
      end;
    end;
    Status('  -> Ok'); Status('');
  finally
    FreeAndNil(FDataset)
  end;
  MostrarQuery('select * from POLL');
end;

// C�digo tomado de DServerCatalogos.pPas
//function TdmServerCatalogos.CopiaValuacion(Empresa, Parametros: OleVariant; var ErrorCount: Integer): Integer;
procedure TSprint3.Escenario_RollBackTransaction;
var
  Parametros: TZetaParams;
  oCopiar: TZetaCursor;
  Result : Integer;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');
  ConectaDemo;
  Tabla := 'VAL_PTO';

  Parametros := TZetaParams.Create;
  Parametros.AddInteger('Folio',2);
  Parametros.AddString('Puesto', '010');
  Parametros.AddDate('Fecha', Today);
  Parametros.AddInteger('Usuario', 1);

  with Fixture.oZetaProvider do begin
    //EmpresaActiva := Empresa;
    AsignaParamList( Parametros.VarValues );
    oCopiar:= CreateQuery( Q_VAL_COPIA_PUESTO ); // <-----------------------------------------------
    oZCursor := CreateQuery( 'select * from VAL_PTO order by VP_FOLIO' );

    Status('Datos antes de copiar Valuacion');
    oZCursor.Open; MostrarDatos(oZCursor); oZCursor.Close;

    try
      Status('Copiando Valuaci�n con folio "2"');
      Status('  => ' + Q_VAL_COPIA_PUESTO);
      EmpiezaTransaccion;
      try
        ParamAsInteger( oCopiar, 'VP_FOLIO' , ParamList.ParamByName('Folio').AsInteger   );
           ParamAsChar( oCopiar, 'PU_CODIGO', ParamList.ParamByName('Puesto').AsString, ZetaCommonClasses.K_ANCHO_CODIGO );
           ParamAsDate( oCopiar, 'VP_FECHA' , ParamList.ParamByName('Fecha').AsDate      ) ;
        ParamAsInteger( oCopiar, 'US_CODIGO', ParamList.ParamByName('Usuario').AsInteger );
           ParamSalida( oCopiar, 'NewFolio');
        Ejecuta( oCopiar );
        Status('  -> Ok');
        Result := GetParametro( oCopiar, 'NewFolio').Value;
        Status(Format('Valuaci�n ha sido copiado con el nuevo folio "%d"', [Result]));
        Status('');

        Status('Datos despues de copiar Valuacion');
        oZCursor.Open; MostrarDatos(oZCursor); oZCursor.Close;
        Status('');

        Status('Ejecutando RollBackTransaccion()');
        RollBackTransaccion;
        Status('  -> Ok'); Status('');

        Status('Datos despues de Ejecutar RollBackTransaccion()');
        oZCursor.Open; MostrarDatos(oZCursor); oZCursor.Close;
      except
        on Error: Exception do begin
          Status('  -> Error, Ejecutando RollBackTransaccion()');
          RollBackTransaccion;
          Status('  -> Ok'); Status('');

          if ( Pos( 'UNIQUE INDEX', UpperCase( Error.Message ) ) > 0 ) then begin
            DatabaseError( Format('� Valuaci�n ya Registrada con el Puesto: %s y Fecha: %s !',
                                   [ ParamList.ParamByName('Puesto').AsString ,
                                     FechaCorta( ParamList.ParamByName('Fecha').AsDate )  ] ) );
          end;
          raise;
        end;
      end;

    finally
      FreeAndNil( oCopiar );
      FreeAndNil( oZCursor );
      FreeAndNil(Parametros);
    end;
  end;
  //SetComplete;
end;

procedure TSprint3.Escenario_UpdateProcessLog;
begin
  Check(RestaurarCatalogo, 'No se restaur� el Catalogo');

  ConectaDemo;
  Tabla := 'PROCESO';

  with Fixture.oZetaProvider do try
    Status('Datos antes de OpenProcess()');
    oZCursor := CreateQuery( 'select * from PROCESO' );
    oZCursor.Open; MostrarDatos(oZCursor); oZCursor.Close;
    try
      Status('Ejecutando oZetaProvider.OpenProcess()');
      if Fixture.oZetaProvider.OpenProcess( prNoCalculaCosteo, 10, 'oZetaProvider.OpenProcess()') then begin
        Status('  -> Ok'); Status('');
        Status('Ejecutando oZetaProvider.CloseProcess()');
        CloseProcess;
        Status('  -> Ok'); Status('');
      end else
        Status('  -> oZetaProvider.OpenProcess() ha fallado');
    except
      on e: Exception do begin
        Status(e.Message);
        raise
      end;
    end;
  finally
    // Mostrar procesos
    Status('Datos despues de OpenProcess()');
    oZCursor.Open; MostrarDatos(oZCursor); oZCursor.Close;
    FreeAndNil(oZCursor);
  end;
end;

procedure TSprint3.Escenario_Procedure_ParamsAs;
const
  K_SCRIPT = 'CREATE PROCEDURE SP_DUNIT_SETGETPARAMS( ' + sLineBreak +
             '  @InParamInteger     Int,' + sLineBreak +
             '  @InParamFloat       float,' + sLineBreak +
             '  @InParamDateTime    datetime,' + sLineBreak +
             '  @InParamChar10      Char(10),' + sLineBreak +
             '  @InParamVarChar256  VarChar(256),' + sLineBreak +
             '  @InParamBoolean     Char(1),' + sLineBreak +
             '  @OutParamInteger    Int          output,' + sLineBreak +
             '  @OutParamFloat      float        output,' + sLineBreak +
             '  @OutParamDateTime   datetime     output,' + sLineBreak +
             '  @OutParamChar10     Char(10)     output,' + sLineBreak +
             '  @OutParamVarChar256 VarChar(256) output,' + sLineBreak +
             '  @OutParamBoolean    Char(1)      output)   ' + sLineBreak +
             'AS  ' + sLineBreak +
             'BEGIN  ' + sLineBreak +
             '  SET NOCOUNT ON;' + sLineBreak +
             '  set @OutParamInteger    = @InParamInteger' + sLineBreak +
             '  set @OutParamFloat      = @InParamFloat' + sLineBreak +
             '  set @OutParamDateTime   = @InParamDateTime' + sLineBreak +
             '  set @OutParamChar10     = @InParamChar10' + sLineBreak +
             '  set @OutParamVarChar256 = @InParamVarChar256' + sLineBreak +
             '  set @OutParamBoolean    = @InParamBoolean' + sLineBreak +
             '  return 1  ' + sLineBreak +
             'END';
var
  oSP: TZetaCursor;
begin
  ConectaDemo;
  Tabla := '';
  with Fixture.oZetaProvider do begin
    //Eliminar SP
    EjecutaAndFree('if object_id(''SP_DUNIT_SETGETPARAMS'') is not null drop procedure SP_DUNIT_SETGETPARAMS');
    //Crear SP
    Status('Ejecutando: '); Status(K_SCRIPT);

    EmpiezaTransaccion;
    try
      ExecSQL(  EmpresaActiva, K_SCRIPT ) ;
      TerminaTransaccion( TRUE )
    except
      on Error : Exception do
      begin
          TerminaTransaccion( FALSE );
          raise;
      end;

    end;

    Status('  -> Ok');
    Status('');
  end;

  with Fixture.oZetaProvider do try
    oSP := CreateQuery('execute procedure SP_DUNIT_SETGETPARAMS(' +
                         ':@InParamInteger,  :@InParamFloat,  :@InParamDateTime,  :@InParamChar10,  :@InParamVarChar256,  :@InParamBoolean, ' +
                         ':@OutParamInteger, :@OutParamFloat, :@OutParamDateTime, :@OutParamChar10, :@OutParamVarChar256, :@OutParamBoolean)');

    ParamAsInteger (oSP, '@InParamInteger'   , 10);
    ParamAsFloat   (oSP, '@InParamFloat'     , 10.0123456789);
    ParamAsDateTime(oSP, '@InParamDateTime'  , Now);
    ParamAsChar    (oSP, '@InParamChar10'    , 'Char10'   , 10);
    ParamAsVarChar (oSP, '@InParamVarChar256', 'VarChar(256)', 0);
    ParamAsBoolean (oSP, '@InParamBoolean'   , True);

    ParamSalida(oSP, '@OutParamInteger');
    ParamSalida(oSP, '@OutParamFloat');
    ParamSalida(oSP, '@OutParamDateTime');
    ParamSalida(oSP, '@OutParamChar10');
    ParamSalida(oSP, '@OutParamVarChar256');
    ParamSalida(oSP, '@OutParamBoolean');
    MostrarParametros( oSP );

    Status('Ejecutando: execute procedure SP_DUNIT_SETGETPARAMS()');
    Ejecuta(oSP);
    Status('  -> Ok'); Status('');

    MostrarParametros( oSP );
  finally
    FreeAndNil(oSP)
  end;
end;

// Coodigo extraido de DataModules\dCatalogos.pas
//   procedure TdmCatalogos.cdsPuestosAlEnviarDatos(Sender: TObject);
procedure TSprint3.Escenario_GrabaTabla_Insercion;

  procedure MostrarBitacora;
  begin
    Status('Bitacora:');
    Tabla := 'BITACORA';
    FreeAndNil(oZCursor);
    oZCursor := Fixture.oZetaProvider.CreateQuery ('select * from Bitacora');
    oZCursor.Open;
    MostrarCampos(oZCursor);
    MostrarDatos(oZCursor);
  end;

  procedure AplicaCambios(oDS: TZetaLookupDataSet);
  var
    ErrorCount: Integer;
  begin
    ErrorCount := 0;
    with oDS do begin
      if State in [dsEdit, dsInsert] then
        Post;
      if (ChangeCount > 0) then begin
        if Reconcile(Fixture.GrabaCatalogo(Fixture.oZetaProvider.EmpresaActiva, Tag, Delta, ErrorCount)) then begin
          ;
        end;
      end;
    end;
  end;

const
  K_CODIGO1   = 2003;
  K_CODIGO2   = 2013;
  K_ORIGINAL  = 'PUESTO_Original';
  K_INSERTAR  = 'PUESTO_Insertar';
  K_MODIFICAR = 'PUESTO_Modificar';
  K_ELIMINAR  = 'PUESTO_Eliminar';
var
  cdsPuestos: TZetaLookupDataSet;
  oCDS: TZetaLookupDataSet;

  procedure VerDatos(sArchivo: string);
  begin
    oZCursor.Open;
    ClonarDS(oZCursor, oCDS);
    CrearXML(oCDS, sArchivo);
    oZCursor.Close;
  end;

begin
  ConectaDemo;
  MostrarBitacora;
  Tabla := 'PUESTO';

  cdsPuestos := TZetaLookupDataSet.Create(nil);
  cdsPuestos.Tag := Ord(ePuestos);
  oCDS := TZetaLookupDataSet.Create(nil);
  Fixture.SetTablaInfo(ePuestos);
  with Fixture.oZetaProvider do try
    EjecutaAndFree(Format('delete from PUESTO where PU_CODIGO in (%d, %d)', [K_CODIGO1, K_CODIGO2]));
    oZCursor := CreateQuery('select * from PUESTO');
    VerDatos(XMLTemp(K_ORIGINAL));

    cdsPuestos.Data := GetTabla(EmpresaActiva);

    with cdsPuestos do begin
      Status(Format('Insertando en la tabla PUESTO el registro con PU_CODIGO = %d', [K_CODIGO1]));
      Append;
      FieldByName('PU_CODIGO').AsInteger := K_CODIGO1;
      AplicaCambios(cdsPuestos);
      Status(' -> Ok');
      VerDatos(XMLTemp(K_INSERTAR));
      CompararXML(XMLCase(K_INSERTAR), XMLTemp(K_INSERTAR));

      Status(Format('Modificando en la tabla PUESTO el registro con PU_CODIGO = %d a PU_CODIGO = %d', [K_CODIGO1, K_CODIGO2]));
      if Locate('PU_CODIGO', K_CODIGO1, []) then begin
        Edit;
        FieldByName('PU_CODIGO').AsInteger := K_CODIGO2;
        AplicaCambios(cdsPuestos);
        Status(' -> Ok');
      end else
        Status(Format(' -> PU_CODIGO = %d no existe', [K_CODIGO1]));
      VerDatos(XMLTemp(K_MODIFICAR));
      CompararXML(XMLCase(K_MODIFICAR), XMLTemp(K_MODIFICAR));

      Status(Format('Eliminando de la tabla PUESTO el registro con PU_CODIGO = %d', [K_CODIGO2]));
      if Locate('PU_CODIGO', K_CODIGO2, []) then begin
        Delete;
        AplicaCambios(cdsPuestos);
        Status(' -> Ok');
      end else
        Status(Format(' -> PU_CODIGO = %d no existe', [K_CODIGO2]));
      VerDatos(XMLTemp(K_ELIMINAR));
      CompararXML(XMLCase(K_MODIFICAR), XMLTemp(K_MODIFICAR));

      MostrarBitacora;
    end;
  finally
    FreeAndNil(oZCursor);
    FreeAndNil(oCDS);
    FreeAndNil(cdsPuestos)
  end;

end;

procedure TSprint3.Escenario_Blancos_Finales;
var
  {$IFDEF DELPHIXE3_UP}
  oOEQuery : TZADOQuery;
  {$ELSE}
  oOEQuery : TOEQuery;
  {$ENDIF}
begin
  ConectaDemo;
  Tabla := 'PUESTO_Espacios';

  with Fixture.oZetaProvider do try
    Fixture.SetTablaInfo(ePuestos);

    oZClientDS := TZetaClientDataSet.Create(nil);
    Status(Format('Obtener datos con conexi�n del sistema (%s)', [oZClientDS.ClassName]));
    StopWatch.Start;
    oZClientDS.Data := GetTabla(EmpresaActiva);
    StopWatch.Stop;
    Status('  -> Ok ' + StopWatch.Elapsed); Status('');
    MostrarCampos(oZClientDS);
    MostrarDatos(oZClientDS);
    CrearXML(oZClientDS, XMLTemp);
    //CompararXML(XMLCase, XMLTemp);

    {$IFDEF DELPHIXE3_UP}
    oZCursor := CreateQuery(qryMaster.CommandText); oZCursor.Open;
    {$ELSE}
    oZCursor := CreateQuery(qryMaster.SQL.Text); oZCursor.Open;
    {$ENDIF}
    Status(Format('Obtener datos con conexi�n del sistema (%s)', [oZCursor.ClassName]));
    Status('  -> Ok'); Status('');
    MostrarCampos(oZCursor);
    MostrarDatos(oZCursor);

    {$IFDEF DELPHIXE3_UP}
    oOEQuery := TZADOQuery.Create(nil);
    oOEQuery.SQL.Text := qryMaster.CommandText;
    oOEQuery.Connection := Fixture.oZetaProvider.adoEmpresa;
    {$ELSE}
    oOEQuery := TOEQuery.Create(nil);
    oOEQuery.SQL.Text := qryMaster.SQL.Text;
    oOEQuery.hDbc := Fixture.oZetaProvider.HdbcEmpresa;
    {$ENDIF}
    oOEQuery.Open;
    Status(Format('Obtener datos sin conexi�n del sistema (%s)', [oOEQuery.ClassName]));
    Status('  -> Ok'); Status('');
    MostrarCampos(oOEQuery);
    MostrarDatos(oOEQuery);

    {$IFDEF DELPHIXE3_UP}
    oADOQuery := oSQLServer.CreateQuery(qryMaster.CommandText);
    {$ELSE}
    oADOQuery := oSQLServer.CreateQuery(qryMaster.SQL.Text);
    {$ENDIF}
    Status(Format('Obtener datos sin conexi�n del sistema (%s)', [oADOQuery.ClassName]));
    Status('  -> Ok'); Status('');
    MostrarCampos(oADOQuery);
    MostrarDatos(oADOQuery);

    Check(CompararXML(XMLCase, XMLTemp), 'El contenido XML no es igual');

  finally
    FreeAndNil(oZCursor);
    FreeAndNil(oOEQuery);
    FreeAndNil(oADOQuery);
    FreeAndNil(oZClientDS);
  end;

end;

procedure TSprint3.Escenario_PenalizacionPorTrimRight;
const
  K_VECES = 1000;
var
  aMarca: array[1..K_VECES] of Int64;
  iSuma, iMayor, iMenor, iPromedio: array [1..2] of TLargeInteger;
  iFilas, iColumnas, I2: LongWord;
  I: Byte;
  sSuma: string;
begin
  ConectaDemo;
  Tabla := 'PUESTO';
  oZClientDS := TZetaClientDataset.Create(nil);
  StatusWnd := CreateStatusWindow('Espere por favor, procesando...');
  with Fixture.oZetaProvider do try
    iFilas := 0;
    iColumnas := 0;
    for I := 1 to 2 do begin
      if I = 1 then
        Status(Format('Ejecutando %d lecturas de tabla PUESTO CON TrimRight()', [K_VECES]))
      else begin
        Status(Format('Ejecutando %d lecturas de tabla PUESTO SIN TrimRight()', [K_VECES]));
        prvUnico.OnGetData := nil;
      end;
      //Ejecutar las lecturas
      sSuma := '  ->';
      iSuma[I] := 0;
      iMayor[I] := 0;
      iMenor[I] := 0;
      for I2 := 1 to K_VECES do begin
        // La primer lectura tarda mas, omitida para ser consistentes en los tiempos para el
        // segundo ciclo sin TrimRight()
        if (I2 = 1) then begin
          oZClientDS.Data := OpenSQL(EmpresaActiva, 'select * from PUESTO order by PU_CODIGO', True);
        end;

        StopWatch.Start;
        oZClientDS.Data := OpenSQL(EmpresaActiva, 'select * from PUESTO order by PU_CODIGO', True);
        StopWatch.Stop;

        Inc(iFilas, oZClientDS.RecordCount);
        aMarca[I2] := StopWatch.ElapsedMilliseconds;
        if I2 = 1 then begin
          Status('');
          iColumnas := MostrarCampos(oZClientDS);
          MostrarDatos(oZClientDS);
          iMayor[I] := aMarca[I2];
          iMenor[I] := aMarca[I2];
        end else begin
          if aMarca[I2] > iMayor[I] then iMayor[I] := aMarca[I2];
          if aMarca[I2] < iMenor[I] then iMenor[I] := aMarca[I2];
        end;
        Inc(iSuma[I], StopWatch.ElapsedMilliseconds);
        sSuma := sSuma + Format('%3d=%s', [I2, StopWatch.ElapsedAsString(aMarca[I2], True)]);
        if I2 <> K_VECES then
          if (I2 mod 10 = 0) then
            sSuma := sSuma + sLineBreak + '  ->'
          else
            sSuma := sSuma + Chr(9);
      end;
      Status('  -> Ok');
      Status('');
      if I = 1 then
        Status('Estad�sticas CON TrimRight()')
      else
        Status('Estad�sticas SIN TrimRight()');
      iPromedio[I] := Trunc(iSuma[I] / K_VECES);
      Status('Lecturas individuales:');
      Status(sSuma);
      with StopWatch do begin
        Status(Format('  Total.....: %s', [ElapsedAsString(iSuma[I]    , True)]));
        Status(Format('  Promedio..: %s', [ElapsedAsString(iPromedio[I], True)]));
        Status(Format('  Mayor.....: %s', [ElapsedAsString(iMayor[I]   , True)]));
        Status(Format('  Menor.....: %s', [ElapsedAsString(iMenor[I]   , True)]));
        Status(Format('  Filas.....: %s', [FormatFloat(',0', iFilas   )]));
        Status(Format('  Columnas..: %s', [FormatFloat(',0', iColumnas)]));
      end;
      Status('');
    end;
    Status(StringOfChar('-', 120));
    Status('Resultados finales');
    with StopWatch do begin
      Status('  Concepto..: '#9'Trim()'#9'AsIs'#9'Incremento'#9'Penalizaci�n');
      Status(Format('  Total.....: '#9'%s'#9'%s'#9'%s'#9'%f%%', [ElapsedAsString(iSuma[1]    , True), ElapsedAsString(iSuma[2]    , True), ElapsedAsString(iSuma[1]     - iSuma[2]    , True), (100 * iSuma[1]     / iSuma[2]    ) - 100]));
      Status(Format('  Promedio..: '#9'%s'#9'%s'#9'%s'#9'%f%%', [ElapsedAsString(iPromedio[1], True), ElapsedAsString(iPromedio[2], True), ElapsedAsString(iPromedio[1] - iPromedio[2], True), (100 * iPromedio[1] / iPromedio[2]) - 100]));
      Status(Format('  Mayor.....: '#9'%s'#9'%s'#9'%s'#9'%f%%', [ElapsedAsString(iMayor[1]   , True), ElapsedAsString(iMayor[2]   , True), ElapsedAsString(iMayor[1]    - iMayor[2]   , True), (100 * iMayor[1]    / iMayor[2]   ) - 100]));
      Status(Format('  Menor.....: '#9'%s'#9'%s'#9'%s'#9'%f%%', [ElapsedAsString(iMenor[1]   , True), ElapsedAsString(iMenor[2]   , True), ElapsedAsString(iMenor[1]    - iMenor[2]   , True), (100 * iMenor[1]    / iMenor[2]   ) - 100]));
      Status('');
      Status(Format('  Filas.....: '#9'%s', [FormatFloat(',0', iFilas            )]));
      Status(Format('  Columnas..: '#9'%s', [FormatFloat(',0', iColumnas * iFilas)]));
    end;
  finally
    FreeAndNil(oZClientDS);
  end;
  RemoveStatusWindow(StatusWnd);
end;

procedure TSprint3.Escenario_IssueColabora;
const
  K_BORRA: string = 'delete from COLABORA where CB_CODIGO = 999';
  K_SQL  : string = 'select PRETTYNAME as PrettyName, CB_CODIGO, CB_APE_PAT, CB_APE_MAT, CB_NOMBRES from COLABORA where (CB_CODIGO = 999)';
begin
  ConectaDemo;
  Tabla := 'COLABORA';
  oZClientDS := TZetaClientDataSet.Create(nil);
  with Fixture.oZetaProvider do try
    // Eliminar registro
    Status('Ejecutando ' + K_BORRA); EjecutaAndFree(K_BORRA); Status('  -> Ok'); Status('');

    //Insertar nuevo registro
    oZClientDS.Data := OpenSQL(EmpresaActiva, K_SQL, True);
    MostrarCampos(oZClientDS);
    MostrarDatos(oZClientDS);
    Status('Insertando con ' + oZClientDS.ClassName);
    with oZClientDS do begin
      Append;
      FieldByName('CB_CODIGO').AsInteger := 999;
      FieldByName('CB_APE_PAT').AsString := 'CB_APE_PAT999';
      FieldByName('CB_APE_MAT').AsString := 'CB_APE_MAT999';
      FieldByName('CB_NOMBRES').AsString := 'CB_NOMBRE999';
      Post
    end;
    Status('  -> Ok'); ; Status('');
    MostrarDatos(oZClientDS);
    oZClientDS.Close;

    //Leer el registro
    oADOQuery := oSQLServer.CreateQuery(K_SQL);
    Status(Format('Obtener datos sin conexi�n del sistema (%s)', [oADOQuery.ClassName]));
    Status('  -> Ok'); Status('');
    MostrarCampos(oADOQuery);
    MostrarDatos(oADOQuery);

  finally
    FreeAndNil(oZClientDS);
    FreeAndNil(oADOQuery)
  end;
end;

procedure TSprint3.Escenario_Transacciones;
begin
  ConectaDemo;
  Fixture.oZetaProvider.EmpiezaTransaccion;
  Fixture.oZetaProvider.EmpiezaTransaccion;
  Fixture.oZetaProvider.EmpiezaTransaccion;

  Fixture.oZetaProvider.TerminaTransaccion(True);
  Fixture.oZetaProvider.TerminaTransaccion(True);
  Fixture.oZetaProvider.TerminaTransaccion(True);



end;


initialization
  RegisterTests(TSprint3.SuiteName, [TSprint3.Suite]);

end.