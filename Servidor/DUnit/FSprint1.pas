unit FSprint1;

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Sistema............: TRESS
// Prop�sito..........: M�dulo para pruebas unitarias del M�dulo ServerCatalogos, Sprint 1
// Desarrollador......: Ricardo Carrillo Morales, 5/Ago/2013
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//  TAREAS DEL SPRINT 1
//__________________________________________________________________________________________________
//  1. Crear el ambiente por Setup. Restauraci�n de BD, Datos de conexi�n usando un mismo
//     componente.
//  2. Generar el ambiente cero, Tener BD tipo con Parche 435.
//  3. Integrar escenario de conexi�n al Setup()
//      Nota: Las tareas anteriores las ejecuta la clase auxiliar TSQLServer
//  4. Assert de creaci�n de objeto. Se confirma revisando la creaci�n revisando que se haya creado
//     el objeto y los objetos que se crean de inicio: TablaInfo, DetailInfo, FRegistry
//      Nota: FRegistry tuvo que ser clonado para reducir la dependencia del registro de windows y
//            leer del archivo ini creado por la clase TSQLServer
//  5. Assert de property Comparte. Regresa un arreglo con los datos para conectarse a la BD de
//     COMPARTE - internamente utiliza el objeto FRegistry
//  6. Assert de conectar a BD de COMPARTE
//  7. Assert de conectar a Datos de una Empresa
//  8. Assert de GetTabla(Empresa).Consulta - Verificar RowCount
//  9. Assert de GetTabla(Empresa).Consulta - Verificar contenido por exportaci�n a XML (Dataset to
//     XML)
// 10. Assert de Consulta Verificar RowCount y contenidos para cat�logos de Comparte
// 11. Validaci�n y ejecuci�n del escenario GetMasterDetail( Empresa )
// 12. Prueba de escenario de GetMasterDetail( Empresa ) - Verificar RowCount y contenido de la
//     tabla principal
// 13. Prueba de escenario de GetMasterDetail( Empresa ) - Verificar RowCount y contenido de la
//     tabla detalle
//__________________________________________________________________________________________________

interface

uses
  TestFramework, FSprint0, SysUtils, DB,
  // Auxiliares
  SQLServer,
  // Sistema TRESS
  DServerCatalogos, ZetaClientDataSet;

type

  TSprint1 = class(TSprint0)

  private
    {private declarations}
  public
    {public declarations}
  published
    procedure Escenario_Conexion;
    procedure Escenario_GetTabla_DEMO_CONCEPTO;
    procedure Escenario_GetTabla_COMPARTE_USUARIO;
    procedure Escenario_GetTabla_DEMO_CONCEPTO_FILTRO;
    procedure Escenario_GetTabla_DEMO_IMAGEN;
    procedure Escenario_GetTabla_DEMO_COLABORA;
    procedure Escenario_GetTabla_DEMO_NOMINA;
    procedure Escenario_MasterDetail;
  end;

implementation //===================================================================================

procedure TSprint1.Escenario_Conexion;
begin
  ConectaCOMPARTE;
  ConectaDEMO;
end;

procedure TSprint1.Escenario_GetTabla_DEMO_CONCEPTO;
begin
  ConectaDEMO;
  Tabla := 'CONCEPTO';
  ValidarRecordCountYXMLs(eConceptos, '', 132);
end;

procedure TSprint1.Escenario_GetTabla_COMPARTE_USUARIO;
begin
  ConectaCOMPARTE;
  Tabla := 'USUARIO';
  ValidarRecordCountYXMLs(eUsuario, '', 3);
end;

procedure TSprint1.Escenario_GetTabla_DEMO_CONCEPTO_FILTRO;
begin
  ConectaDEMO;
  Tabla := 'CONCEPTO';
  ValidarRecordCountYXMLs(eConceptos, Format( '( CO_NUMERO = %d )', [ 1 ] ), 1);
end;

procedure TSprint1.Escenario_GetTabla_DEMO_IMAGEN;
begin
  ConectaDEMO;
  Tabla := 'IMAGEN';
  ValidarRecordCountYXMLs(eEmpFoto, '(CB_CODIGO <=10) ', 10);
end;

procedure TSprint1.Escenario_GetTabla_DEMO_COLABORA;
begin
  ConectaDEMO;
  Tabla := 'COLABORA';
  ValidarRecordCountYXMLs(eDatosEmpleado, '(CB_CODIGO <=10)', 10);
end;

procedure TSprint1.Escenario_GetTabla_DEMO_NOMINA;
begin
  ConectaDEMO;
  Tabla := 'NOMINA';
  ValidarRecordCountYXMLs(fnDatosNomina, '(PE_YEAR=2012 and PE_TIPO=1 and PE_NUMERO=1)', 52);
end;

procedure TSprint1.Escenario_MasterDetail;
begin
  ConectaDEMO;
  Tabla := 'Maestro.PRESTACIONES';
  oZClientDSMaster := TZetaClientDataSet.Create( nil );
  try
    oZClientDSMaster.Data := Fixture.GetCatalogo( ePrestaciones, Fixture.oZetaProvider.EmpresaActiva );
    MostrarCampos(oZClientDSMaster);
    MostrarDatos(oZClientDSMaster);
    ValidarRecordCount(oZClientDSMaster, 1);
    CrearXML(oZClientDSMaster, XMLTemp);
    Check(CompararXML( XMLCase , XMLTemp), 'El contenido XML no es igual');

    Tabla := 'Detalle.PRESTACIONES';
    try
      oZClientDSDetail := TZetaClientDataSet.Create( nil );
      oZClientDSDetail.DataSetField := TDataSetField( oZClientDSMaster.FieldByName('qryDetail') );
      oZClientDSDetail.Conectar;
      // Workaround: CrearXML no exporta a XML estructura ni datos del DataSet si no se copia el Data
      oZClientDS := TZetaClientDataSet.Create( nil );
      oZClientDS.Data := oZClientDSDetail.Data;
      MostrarCampos(oZClientDS);
      MostrarDatos(oZClientDS);
      ValidarRecordCount(oZClientDS, 7);
      CrearXML(oZClientDS, XMLTemp);
      Check(CompararXML( XMLCase , XMLTemp), 'El contenido XML no es igual');
    finally

    end;
  finally
    FreeAndNil(oZClientDSMaster);
    FreeAndNil(oZClientDSDetail);
    FreeAndNil(oZClientDS);
  end;
end;

initialization
  RegisterTests(TSprint1.SuiteName, [TSprint1.Suite]);

end.
