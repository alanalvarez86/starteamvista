unit ZetaRegistryServer;

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Sistema............: TRESS
// Prop�sito..........: Versi�n personalizada de ZetaRegistryServer para pruebas unitarias del
// M�dulo ServerCatalogos
// Desarrollador......: Ricardo Carrillo Morales, 5/Ago/2013
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

interface

uses
  SysUtils, Classes, ADODB;

const
  // Declarado el mas nuevo primero
  SQLNCLIs: array[0..2] of string = ('SQLNCLI11', 'SQLNCLI10', 'SQLNCLI');

type

  TZetaRegistryServer = class(TObject)

  private
    { Private declarations }
    oIni     : TStringList;
    FCanWrite: Boolean;
    FSQLNCLI : string;
    function GetAliasComparte: String;
    function GetBinarios: String;
    function GetPassword: String;
    function GetPasswordRaw: String;
    function GetUserName: String;
    function GetDatabase: String;
    function GetTipoLogin: Integer;
    function GetChecksum: String;
    function GetChecksumRaw: String;
    function GetSQLNCLIProvider: String;
    procedure SetAliasComparte(const Value: String);
    procedure SetBinarios(const Value: String);
    procedure SetPassword(const Value: String);
    procedure SetPasswordRaw(const Value: String);
    procedure SetUserName(const Value: String);
    procedure SetDatabase(const Value: String);
    procedure SetTipoLogin(const Value: Integer);
    procedure SetChecksum(const Value: String);
    procedure SetChecksumRaw(const Value: String);
    procedure SetSQLNCLIProvider(const Value: String);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(const lReadWrite: Boolean = TRUE); virtual;
    destructor Destroy; override;
    property CanWrite       : Boolean read FCanWrite;
    property AliasComparte  : String  read GetAliasComparte   write SetAliasComparte;
    property Binarios       : String  read GetBinarios        write SetBinarios;
    property Password       : String  read GetPassword        write SetPassword;
    property PasswordRaw    : String  read GetPasswordRaw     write SetPasswordRaw;
    property UserName       : String  read GetUserName        write SetUserName;
    property Database       : string  read GetDatabase        write SetDatabase;
    property TipoLogin      : Integer read GetTipoLogin       write SetTipoLogin;
    property Checksum       : String  read GetChecksum        write SetChecksum;
    property ChecksumRaw    : String  read GetChecksumRaw     write SetChecksumRaw;
    property SQLNCLIProvider: String  read GetSQLNCLIProvider write SetSQLNCLIProvider;
    function IsValid        : Boolean;
    function IsValidAlias   : Boolean;
  end;

implementation
// ===================================================================================

uses
  ZetaServerTools, ZetaCommonTools;

{ ************ TZetaRegistryServer *********** }

constructor TZetaRegistryServer.Create(const lReadWrite: Boolean);
begin
  oIni := TStringList.Create;
  try
    oIni.LoadFromFile(ChangeFileExt(ParamStr(0), '.ini'));
  except

  end
end;

destructor TZetaRegistryServer.Destroy;
begin
  oIni.Free
end;

function TZetaRegistryServer.IsValid: Boolean;
begin
  Result := TRUE
end;

function TZetaRegistryServer.IsValidAlias: Boolean;
begin
  Result := TRUE
end;

function TZetaRegistryServer.GetAliasComparte: String;
begin
  Result := 'ComparteODBC'
end;

function TZetaRegistryServer.GetBinarios: String;
begin
  Result := 'C:\Program Files (x86)\Grupo Tress\Servidor\'
end;

function TZetaRegistryServer.GetDatabase: String;
begin
  Result := oIni.Values['Servidor'] + '.COMPARTE_UT_435'
end;

function TZetaRegistryServer.GetUserName: String;
begin
  Result := oIni.Values['Usuario']
end;

function TZetaRegistryServer.GetPassword: String;
begin
  Result := oIni.Values['Contrasena'];
end;

function TZetaRegistryServer.GetPasswordRaw: String;
begin
  Result := Encrypt(oIni.Values['Contrasena']);
end;

function TZetaRegistryServer.GetTipoLogin: Integer;
begin
  Result := 0
end;

procedure TZetaRegistryServer.SetAliasComparte(const Value: String);
begin;
end;

procedure TZetaRegistryServer.SetBinarios(const Value: String);
begin;
end;

procedure TZetaRegistryServer.SetDatabase(const Value: String);
begin;
end;

procedure TZetaRegistryServer.SetUserName(const Value: String);
begin;
end;

procedure TZetaRegistryServer.SetPassword(const Value: String);
begin;
end;

procedure TZetaRegistryServer.SetPasswordRaw(const Value: String);
begin;
end;

procedure TZetaRegistryServer.SetTipoLogin(const Value: Integer);
begin;
end;

function TZetaRegistryServer.GetChecksum: String;
begin
  Result := GetDatabase
end;

procedure TZetaRegistryServer.SetChecksum(const Value: String);
begin;
end;

function TZetaRegistryServer.GetChecksumRaw: String;
begin
  Result := Encrypt(GetDatabase)
end;

procedure TZetaRegistryServer.SetChecksumRaw(const Value: String);
begin;
end;

function TZetaRegistryServer.GetSQLNCLIProvider: String;
var
  Providers  : TStringList;
  iCount     : Integer;
  sClientProv: string;
begin
  // Recuperar el cliente nativo prederminado
  Result := FSQLNCLI;
  if Result <> '' then Exit;

  // Obtener la lista de proveedores instalados
  Providers := TStringList.Create;
  GetProviderNames(Providers);

  // Buscar los cliente preferido de TRESS
  for iCount := Low(SQLNCLIs) to High(SQLNCLIs) do begin
    sClientProv := SQLNCLIs[iCount];
    if Providers.IndexOf(sClientProv) <> -1 then begin
      Result := sClientProv;
      Break;
    end;
  end;

  if Result = '' then
    // No hay cliente nativo conocido, buscar el mas reciente de los instalados
    for iCount := Providers.Count - 1 downto 0 do begin
      sClientProv := Providers[iCount];
      if Pos('SQLNCLI', sClientProv) = 1 then begin
        Result := sClientProv;
        Break;
      end;
    end;
  FreeAndNil(Providers);

  if Result = '' then
    // ToDo: No se encontr� cliente nativo, que hay qu� hacer? Excepci�n? Selecci�n del usuario?
  else
    FSQLNCLI := Result
end;

procedure TZetaRegistryServer.SetSQLNCLIProvider(const Value: String);
begin
  FSQLNCLI := Value
end;

end.
