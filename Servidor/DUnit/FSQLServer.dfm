object dlgConexionSQL: TdlgConexionSQL
  Left = 507
  Top = 138
  Width = 465
  Height = 279
  BorderIcons = [biSystemMenu]
  Caption = 'Par'#225'metros de conexi'#243'n'
  Color = clBtnFace
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    449
    241)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 433
    Height = 105
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Servidor SQL '
    TabOrder = 0
    DesignSize = (
      433
      105)
    object Label1: TLabel
      Left = 16
      Top = 28
      Width = 39
      Height = 13
      Caption = 'Servidor'
    end
    object Label2: TLabel
      Left = 16
      Top = 52
      Width = 36
      Height = 13
      Caption = 'Usuario'
    end
    object Label3: TLabel
      Left = 16
      Top = 76
      Width = 54
      Height = 13
      Caption = 'Contrase'#241'a'
    end
    object edtServidor: TEdit
      Left = 80
      Top = 24
      Width = 345
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object edtUsuario: TEdit
      Left = 80
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object edtContrasena: TEdit
      Left = 80
      Top = 72
      Width = 121
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 120
    Width = 433
    Height = 81
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Archivos de Respaldo '
    TabOrder = 1
    DesignSize = (
      433
      81)
    object Label4: TLabel
      Left = 16
      Top = 28
      Width = 45
      Height = 13
      Caption = 'Comparte'
    end
    object Label5: TLabel
      Left = 16
      Top = 52
      Width = 28
      Height = 13
      Caption = 'Datos'
    end
    object edtComparte: TEdit
      Left = 80
      Top = 24
      Width = 321
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object btnComparte: TButton
      Left = 405
      Top = 24
      Width = 21
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 1
      OnClick = btnComparteClick
    end
    object edtDatos: TEdit
      Left = 80
      Top = 48
      Width = 321
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 2
    end
    object btnDatos: TButton
      Left = 405
      Top = 48
      Width = 21
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 3
      OnClick = btnDatosClick
    end
  end
  object btnAceptar: TButton
    Left = 288
    Top = 208
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Aceptar'
    Default = True
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object Button4: TButton
    Left = 368
    Top = 208
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancelar'
    ModalResult = 2
    TabOrder = 3
  end
  object OpenDialog: TOpenDialog
    Filter = 'Respaldos|*.bak|Todos los archivos|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 232
    Top = 56
  end
end
