unit StopWatch;
// Fuente: http://delphi.about.com/od/windowsshellapi/a/delphi-high-performance-timer-tstopwatch.htm
interface

uses
  Windows, SysUtils, DateUtils;

type
  TStopWatch = class
    function ElapsedAsString(ElapsedMilliseconds: TLargeInteger;
      ShortFormat: Boolean = False): string;
  private
    fFrequency: TLargeInteger;
    fIsRunning: Boolean;
    fIsHighResolution: Boolean;
    fStartCount, fStopCount: TLargeInteger;
    procedure SetTickStamp(var lInt: TLargeInteger);
    function GetElapsedTicks: TLargeInteger;
    function GetElapsedMilliseconds: TLargeInteger;
    function GetElapsed: string;
  public
    constructor Create(const startOnCreate: Boolean = False);
    procedure Start;
    procedure Stop;
    property IsHighResolution: Boolean read fIsHighResolution;
    property ElapsedTicks: TLargeInteger read GetElapsedTicks;
    property ElapsedMilliseconds: TLargeInteger read GetElapsedMilliseconds;
    property Elapsed: string read GetElapsed;
    property IsRunning: Boolean read fIsRunning;
  end;

implementation

constructor TStopWatch.Create(const startOnCreate: Boolean = False);
begin
  inherited Create;
  fIsRunning := False;
  fIsHighResolution := QueryPerformanceFrequency(fFrequency);
  if not fIsHighResolution then
    fFrequency := MSecsPerSec;

  if startOnCreate then
    Start;
end;

function TStopWatch.GetElapsedTicks: TLargeInteger;
begin
  result := fStopCount - fStartCount;
end;

procedure TStopWatch.SetTickStamp(var lInt: TLargeInteger);
begin
  if fIsHighResolution then
    QueryPerformanceCounter(lInt)
  else
    lInt := MilliSecondOf(Now);
end;

function TStopWatch.GetElapsed: string;
begin
  Result := ElapsedAsString(ElapsedMilliseconds);
end;

function TStopWatch.GetElapsedMilliseconds: TLargeInteger;
begin
  result := (MSecsPerSec * (fStopCount - fStartCount)) div fFrequency;
end;

procedure TStopWatch.Start;
begin
  SetTickStamp(fStartCount);
  fIsRunning := true;
end;

procedure TStopWatch.Stop;
begin
  SetTickStamp(fStopCount);
  fIsRunning := False;
end;

function TStopWatch.ElapsedAsString(ElapsedMilliseconds: TLargeInteger;
  ShortFormat: Boolean = False): string;
var
  dt: TDateTime;
  h, m, s, z: Word;
  Sign: string;
begin
  dt := ElapsedMilliseconds / MSecsPerSec / SecsPerDay;
  if dt < 0 then begin
    Sign := '-';
  end else
    Sign := '';
  if ShortFormat then begin
    Result := '';
    DecodeTime(dt, h, m, s, z);
    if h <> 0 then Result := Result + Format('%d', [h]) + 'h';
    if m <> 0 then Result := Result + Format('%d', [m]) + 'm';
    if s <> 0 then Result := Result + Format('%d', [s]) + 's';
    if z <> 0 then Result := Result + Format('%d', [z]) + 'ms';
  end else
    Result := FormatDateTime('hh:nn:ss.zzz', Frac(dt));
  if Trunc(dt) > 0 then
    Result := Format('%dd%s', [Trunc(dt), Result]);
  Result := Sign + Result
end;

end.
