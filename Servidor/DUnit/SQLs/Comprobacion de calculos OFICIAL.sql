-- 1 prueba por montos 
declare @Periodo int 
declare @Tipo int 
declare @Year int 

set @Periodo = 43
set @Tipo = 1  -- semanal 
set @Year = 2009

if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'MONTOS_NOMINA_OFICIAL')
    drop table MONTOS_NOMINA_OFICIAL;
    
select MN_YEAR = @Year, MN_PERIODO = @Periodo, MN_TIPO = @Tipo, Neto= sum( NO_NETO ), Deducciones= sum( NO_DEDUCCI ), Percepciones=sum( NO_PERCEPC) 
into   MONTOS_NOMINA_OFICIAL 
from   NOMINA 
where  PE_YEAR = @Year and PE_TIPO = @Tipo  and PE_NUMERO = @Periodo
  
--pruebas por conceptos calculados 
if exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'MONTOS_CONCEPTOS_OFICIAL')
    drop table MONTOS_CONCEPTOS_OFICIAL;
    
select   CO_NUMERO, Deducciones= sum( MO_DEDUCCI ), Percepciones=sum( MO_PERCEPC)  
into     MONTOS_CONCEPTOS_OFICIAL 
from     MOVIMIEN 
where    PE_YEAR = @Year and PE_TIPO = @Tipo  and PE_NUMERO = @Periodo 
group by CO_NUMERO

/*select *, (case when ADO.CO_NUMERO Is null then 'No existe' 
                when ODBC.Deducciones <> ADO.Deducciones then 'Fail Deducciones' 
                when ODBC.Percepciones <> ADO.Percepciones then 'Fail Percepciones'
                else 'OK' 
                end ) as Validacion  from MONTOS_CONCEPTOS_OFICIAL ODBC
left outer join  MONTOS_CONCEPTOS_ADO ADO on ODBC.CO_NUMERO = ADO.CO_NUMERO */
