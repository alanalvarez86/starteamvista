BACKUP DATABASE 
  [HYUNDAIOCT_20101029_435] 
TO  
  DISK = N'D:\Grupo Tress\Hyundai_Zero_State_435.bak' 
WITH 
  NOFORMAT, 
  NOINIT,  
  NAME = N'HYUNDAIOCT_20101029_435-Full Database Backup', 
  SKIP, 
  NOREWIND, 
  NOUNLOAD, 
  COMPRESSION,  
  STATS = 10
GO
