USE [DEMO_TRESS_UT_435]
GO
/****** Object:  StoredProcedure [dbo].[SP_DUNIT_SETGETPARAMS]    Script Date: 09/02/2013 17:54:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_DUNIT_SETGETPARAMS]
	@InParamInteger     Integer,
	@InParamFloat       float,
	@InParamDateTime    datetime,
	@InParamChar10      Char(10),
	@InParamVarChar256  VarChar(256), 
	@InParamVarCharMAX  VarChar(MAX),
	@OutParamInteger    Integer output,
	@OutParamFloat      float output,
	@OutParamDateTime   datetime output,
	@OutParamChar10     Char(10) output,
	@OutParamVarChar256 VarChar(256) output, 
	@OutParamVarCharMAX VarChar(MAX) output	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	set @OutParamInteger    = @InParamInteger
	set       @OutParamFloat      = @InParamFloat
	set       @OutParamDateTime   = @InParamDateTime
	set       @OutParamChar10     = @InParamChar10
	set       @OutParamVarChar256 = @InParamVarChar256
	set       @OutParamVarCharMAX = @InParamVarCharMAX
	return 1
END
