use [DEMO_TRESS_UT_435]
declare @ParamInteger    as Integer
declare @ParamFloat      as float 
declare @ParamDateTime   as datetime
declare @ParamChar10     as Char(10)
declare @ParamVarChar256 as VarChar(256)
declare @ParamVarCharMAX as VarChar(MAX)
declare @fecha as datetime
set @fecha = CONVERT (datetime, SYSDATETIME())
execute dbo.SP_DUNIT_SETGETPARAMS 
       10, 
       10.5, 
       @fecha, 
       'char10', 
       'Soy varchar(256)',
       
       'Soy varchar(MAX)',
       @ParamInteger OUTPUT,
       @ParamFloat OUTPUT,
       @ParamDateTime OUTPUT,
       @ParamChar10 OUTPUT,
       @ParamVarChar256 OUTPUT,
       @ParamVarCharMax OUTPUT


print @ParamInteger 
print @ParamFloat 
print @ParamDateTime 
print @ParamChar10 
print @ParamVarChar256 
print @ParamVarCharMax 
go
