unit FSprint0;

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Sistema............: TRESS
// Prop�sito..........: Clase base para pruebas unitarias del M�dulo ServerCatalogos
// Desarrollador......: Ricardo Carrillo Morales, 5/Ago/2013
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

interface

{$INCLUDE JEDI.INC}

uses
  TestFramework, Classes, SysUtils, Variants, Windows, DBClient, DB, ADODB, XMLDoc, XMLIntf,
  {$IFNDEF DELPHIXE3_UP} ODSI, {$ENDIF}
  // Soporte adicional
  SQLServer, StopWatch,
  // Sistema TRESS
  DServerCatalogos, ZetaServerTools, DZetaServerProvider, ZetaServerDataSet, ZetaClientDataSet,
  ZetaCommonClasses, ZetaCommonTools;

type

  TAligment = (alLeft, alRight);

  TFormato = record
    Cadena: string;
    Longitud: Word;
    Alineado: TAligment;
    Relleno: Char;
  end;

  TFormatos = array of TFormato;

  TSprint0 = class(TTestCase)

  private
    { private declarations }
    sTabla: string;
    FFixture: TdmServerCatalogos;
    FStopWatch: TStopWatch;
    aEmpresa: OleVariant;
    procedure SetTabla(Value: string);
    function GetArrayEmpresa(sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, sCM_NIVEL0, sCM_CODIGO: string)
      : OleVariant;
  protected
    Q_VAL_COPIA_PUESTO: string;
    oSQLServer      : TSQLServer;
    oADOQuery       : TADOQuery; // Debe ser TADOQuery para traer los datos tal como los avienta ADO
    oServerDS       : TServerDataSet;
    oZClientDS      : TZetaClientDataSet;
    oZClientDSMaster: TZetaClientDataSet;
    oZClientDSDetail: TZetaClientDataSet;
    oZCursor        : TZetaCursor;
    StatusWnd       : HWND;
    procedure ConectaDEMO;
    procedure ConectaCOMPARTE;
    procedure ConectaInterno;
    function  XMLCase(sArchivo: string = ''): string;
    function  XMLTemp(sArchivo: string = ''): string;
    procedure ClonarDS(oDS: TDataSet; oCDS: TClientDataSet);
    procedure CrearXML(oDS: TDataSet; sArchivo: string);
    function  CompararXML(sXML1, sXML2: string): Boolean;
    function  MostrarCampos(oDS: TDataSet): Integer;
    function  MostrarParametros(oDS: TDataSet): Integer;
    function  MostrarDatos(oDS: TDataSet): Integer;
    procedure MostrarQuery(sSQL: string);
    procedure ValidarRecordCountYXMLs(const eCualCatalogo: eTipoCatalogo; sFiltro: string;
      iRegistros: Integer);
    procedure ValidarRecordCount(oDS: TDataSet; iRegistros: Integer);
    function  RestaurarCatalogo: Boolean;
  public
    { public declarations }
    class function Suite: ITestSuite; override;
    class function SuiteName: string;
    procedure Setup; override;
    procedure TearDown; override;
    property Tabla: string read sTabla write SetTabla;
    property Fixture: TdmServerCatalogos read FFixture;
    property SQLServer: TSQLServer read oSQLServer;
    property StopWatch: TStopWatch read FStopWatch;
  end;

implementation

// ===================================================================================

const
  K_INDICE = 0;
  K_CLASE  = 1;
  K_NOMBRE = 2;
  K_TIPO   = 3;
  K_FIJO   = 4;
  K_TAMANO = 5;
  K_VALOR  = 6;

class function TSprint0.Suite: ITestSuite;
begin
  Result := TTestSuite.Create(Self);
end;

class function TSprint0.SuiteName: string;
begin
  Result := 'Pruebas de Provider ADO 32 y 64 bits';
end;

procedure TSprint0.SetTabla(Value: string);
begin
  sTabla := Value;
end;

function TSprint0.GetArrayEmpresa(sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, sCM_NIVEL0,
  sCM_CODIGO: string): OleVariant;
// ZetaCommonClasses:
// POSICIONES DEL ARREGLO dmCliente.Empresa
// P_ALIAS = 0;
// P_DATABASE = 0;
// P_USER_NAME = 1;
// P_PASSWORD = 2;
// P_USUARIO = 3;
// P_NIVEL_0 = 4;
// P_CODIGO = 5;
// P_GRUPO = 6;
// P_APPID = 7;

begin
  Result := VarArrayOf([sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, 0, sCM_NIVEL0, sCM_CODIGO,
      D_GRUPO_SIN_RESTRICCION, 0]);
end;

procedure TSprint0.Setup;
begin
  inherited;
  Q_VAL_COPIA_PUESTO :=
    '{CALL VAL_COPIA_PUESTO( :VP_FOLIO, :PU_CODIGO, :VP_FECHA, :US_CODIGO, :NewFolio)}';
  oSQLServer := TSQLServer.Create;
  FFixture := nil;
  if not oSQLServer.Open(False) then
    FreeAndNil(oSQLServer);
  Check(Assigned(oSQLServer), 'No se pudo conectar a servidor SQL ');
  FFixture   := TdmServerCatalogos.Create;
  FStopWatch := TStopWatch.Create();
end;

procedure TSprint0.TearDown;
begin
  FreeAndNil(FStopWatch);
  FreeAndNil(FFixture);
  FreeAndNil(oSQLServer);
  inherited
end;

// ..................................................................................................
function Formateado(aFormato: array of TFormato): string;
var
  I: Integer;
begin
  Result := '';
  for I := Low(aFormato) to High(aFormato) do
    if aFormato[I].Longitud > 0 then
      with aFormato[I] do
        case Alineado of
          alLeft:
            Result := Result + PadRCar(Cadena, Longitud, Relleno) + #9;
          alRight:
            Result := Result + PadLCar(Cadena, Longitud, Relleno) + #9
        end
    else
      Result := Result + aFormato[I].Cadena + #9;
end;

function XMLEscapeEncode(const XMLText: string): string;
const
  EscapeCodes: array [0 .. 6] of array [0 .. 1] of string = (('"', '&quot;'), ('''', '&apos;'),
    ('<', '&lt;'), ('>', '&gt;'), ('&', '&amp;'), (Chr(13), '&#013;'), (Chr(10), '&#010;'));
var
  I: Integer;
begin
  Result := XMLText;
  for I := Low(EscapeCodes) to High(EscapeCodes) do
    Result := StringReplace(Result, EscapeCodes[I, 0], EscapeCodes[I, 1], [rfReplaceAll])
end;

procedure TSprint0.ConectaDEMO;
begin
 // oSQLServer.Catalog := 'DEMO_TRESS_UT_435';
  aEmpresa := GetArrayEmpresa( 'utsqlserver.DEMO_TRESS_UT_435', oSQLServer.UserName,
    Encrypt(oSQLServer.Password), '', 'DEMO');
  ConectaInterno;
end;

procedure TSprint0.ConectaCOMPARTE;
begin
  aEmpresa := FFixture.oZetaProvider.Comparte;
  ConectaInterno;
end;

procedure TSprint0.ConectaInterno;
var
  lConecto: Boolean;
begin
  Check(Assigned(FFixture), 'No se ha podido establecer una conexi�n al servidor SQL ' +
      oSQLServer.DataSource);
  Status('Conectando a ' + aEmpresa[0]);
  try
    FFixture.oZetaProvider.EmpresaActiva := aEmpresa;
    oSQLServer.DataSource := aEmpresa[0];
    lConecto := TRUE;
    Status('  -> Ok');
  except
    on Error: Exception do
      lConecto := False;
  end;
  Check(lConecto, aEmpresa[0] + '  -> No se estableci� la conexi�n');
  Status('');
end;

function TSprint0.XMLCase(sArchivo: string = ''): string;
begin
  if sArchivo = '' then
    sArchivo := sTabla;
  Result := Format('%sxml_unitcases\%s.%s.%s.xml', [ExtractFilePath(ParamStr(0)), Name,
      oSQLServer.Catalog, sTabla]);
end;

function TSprint0.XMLTemp(sArchivo: string = ''): string;
begin
  if sArchivo = '' then
    sArchivo := sTabla;
  Result := Format('%sxml_temp\%s.%s.%s.xml', [ExtractFilePath(ParamStr(0)), Name,
      oSQLServer.Catalog, sTabla]);
end;

procedure TSprint0.ClonarDS(oDS: TDataSet; oCDS: TClientDataSet);
var
  I: Integer;
begin
  with oCDS do begin
    Close;
    FieldDefs.Assign(oDS.FieldDefs);
    CreateDataSet;
    LogChanges := False;
    Open;
  end;
  oDS.First;
  while not oDS.Eof do begin
    oCDS.Insert;
    for I := 0 to oCDS.FieldCount - 1 do begin
      if oCDS.Fields[I].ReadOnly then
        oCDS.Fields[I].ReadOnly := False;
      oCDS.Fields[I].Assign(oDS.Fields[I]);
    end;
    oCDS.Post;
    oDS.Next;
  end;
  oCDS.First
end;

procedure TSprint0.CrearXML(oDS: TDataSet; sArchivo: string);
begin
  Status(Format('Creando %s', [sArchivo]));
  if oDS.InheritsFrom(TClientDataSet) then
    TClientDataSet(oDS).SaveToFile(sArchivo, dfXMLUTF8)
  else
    raise Exception.Create(oDS.ClassName + ' no tiene un metodo que exporte a XML');
  if FileExists(sArchivo) then
    Status('  -> OK')
  else
    Status('  -> No se cre� ' + sArchivo);
  Status('');
end;

function TSprint0.CompararXML(sXML1, sXML2: string): Boolean;

  function NormalizarTipo(sTipo: string): string;
  begin
    Result := sTipo;
    if Pos(sTipo, 'fixed fixedFMT r8') <> 0 then
      Result := 'fixed';
  end;

  function BuscarCampo(sCampo: string; oNodos: IXMLNode): IXMLNode;
  var
    I: Integer;
  begin
    Result := nil;
    for I := 0 to oNodos.ChildNodes.Count - 1 do
      if sCampo = oNodos.ChildNodes[I].Attributes['attrname'] then begin
        Result := oNodos.ChildNodes[I];
        Break
      end;
  end;

var
  oXMLDoc1, oXMLDoc2: IXMLDocument;
  oFields1, oFields2: IXMLNode;
  oField1, oField2: IXMLNode;
  oRows1, oDataRow1, oData1: IXMLNode;
  oRows2, oDataRow2, oData2: IXMLNode;
  I, j: Integer;
  s1, s2: string;
  t1, t2: string;
  f1, f2: real;
  lOk, lExiste: Boolean;
begin
  Result := False;
  Status('Comparando XML');
  Status('  =>' + sXML1);
  Status('  =>' + sXML2);

  oXMLDoc1 := TXMLDocument.Create(nil);
  oXMLDoc1.LoadFromFile(sXML1);
  oXMLDoc2 := TXMLDocument.Create(nil);
  oXMLDoc2.LoadFromFile(sXML2);

  for I := 0 to oXMLDoc1.ChildNodes.Count - 1 do begin
    oFields1 := oXMLDoc1.ChildNodes[I].ChildNodes.FindNode('METADATA');
    if Assigned(oFields1) then
      Break
  end;

  for I := 0 to oXMLDoc2.ChildNodes.Count - 1 do begin
    oFields2 := oXMLDoc2.ChildNodes[I].ChildNodes.FindNode('METADATA');
    if Assigned(oFields2) then
      Break
  end;

  // Comparar la seccion <METADATA> nodo por nodo
  Status('  -> Secci�n <METADATA>');
  lOk := False;
  for I := 0 to oFields1.ChildNodes[0].ChildNodes.Count - 1 do begin
    oField1 := oFields1.ChildNodes[0].ChildNodes[I];
    s1 := oField1.AttributeNodes.FindNode('attrname').Text;
    lExiste := False;
    for j := 0 to oFields2.ChildNodes[0].ChildNodes.Count - 1 do begin
      oField2 := oFields2.ChildNodes[0].ChildNodes[j];
      s2 := oField2.AttributeNodes.FindNode('attrname').Text;
      lExiste := (AnsiUpperCase(s1) = AnsiUpperCase(s2));
      if lExiste then
        Break
    end;
    lOk := lExiste;
    if lOk then begin
      t1 := NormalizarTipo(oField1.AttributeNodes.FindNode('fieldtype').Text);
      t2 := NormalizarTipo(oField2.AttributeNodes.FindNode('fieldtype').Text);
      lOk := UpperCase(t1) = UpperCase(t2);
      if not lOk then begin
        Status(Format
            ('    -> El campo %s es de tipo diferente en la estructura de referencia.', [s1]));
        Break
      end
    end
    else begin
      Status(Format('    -> El campo %s no existe en la estructura de referencia.', [s1]));
      Break
    end;
  end;

  if lOk then begin
    Status('    -> Ok');
    Status('  -> Secci�n <ROWDATA>');
    for I := 0 to oXMLDoc1.ChildNodes.Count - 1 do begin
      oRows1 := oXMLDoc1.ChildNodes[I].ChildNodes.FindNode('ROWDATA');
      if Assigned(oRows1) then
        Break
    end;

    for I := 0 to oXMLDoc2.ChildNodes.Count - 1 do begin
      oRows2 := oXMLDoc2.ChildNodes[I].ChildNodes.FindNode('ROWDATA');
      if Assigned(oRows2) then
        Break
    end;

    if oRows1.ChildNodes.Count <> oRows2.ChildNodes.Count then
      Status(Format('    -> La Secci�n ROWDATA es diferente en ambos archivos: %d y %d registro(s)',
          [oRows1.ChildNodes.Count, oRows2.ChildNodes.Count]))
    else // Recorrer todos los registros del primer Dataset
      for I := 0 to oRows1.ChildNodes.Count - 1 do begin
        oDataRow1 := oRows1.ChildNodes[I];
        oDataRow2 := oRows2.ChildNodes[I];
        // Recorrer todas las columnas del registro
        for j := 0 to oDataRow1.AttributeNodes.Count - 1 do begin
          oData1 := oDataRow1.AttributeNodes[j];
          // Buscar el campo en el segundo dataset, no importa el orden en el <ROW>
          oData2 := oDataRow2.AttributeNodes.FindNode(oData1.NodeName);
          oField1 := BuscarCampo(oData1.NodeName, oFields1.ChildNodes[0]);
          Result := Assigned(oData2);
          if Result then
            with oField1.AttributeNodes.FindNode('fieldtype') do begin
              s1 := NormalizarTipo(Text);
              s2 := Text;
              if s1 = 'fixed' then begin
                f1 := StrToFloat(oData1.Text);
                f2 := StrToFloat(oData2.Text);
                Result := (f1 = f2);
              end
              else
                Result := (oData1.Text) = (oData2.Text);
            end;
          if not Result then begin
            Status(Format('    -> Contenido diferente en registro #%d:', [I + 1]));
            Status(Format('       %s: %s="%s"', [sXML1, oData1.NodeName, oData1.Text]));
            Status(Format('       %s: %s="%s"', [sXML2, oData2.NodeName, oData2.Text]));
            Break
          end;
        end;
        if not Result then
          Break;
      end;
    if lOk then
      Status('    -> Ok');
  end;

  oXMLDoc1 := nil;
  oXMLDoc2 := nil;
  Status('');
end;

function TSprint0.MostrarCampos(oDS: TDataSet): Integer;
var
  Formato: array [0 .. 5] of TFormato;

  function MostrarCamposInterno(oDS: TDataSet; const Nivel: string): Integer;
  var
    I, iMax: Integer;
    oCampo: TField;
  begin
    Result := oDS.FieldCount;
    // Buscar MAX(FieldName)
    iMax := 0;
    for I := 0 to oDS.FieldCount - 1 do
      with oDS.Fields[I] do
        if Length(FieldName) > iMax then
          iMax := Length(FieldName);
    Formato[K_NOMBRE].Longitud := iMax;

    for I := 0 to oDS.FieldCount - 1 do begin
      oCampo := oDS.Fields[I];
      with oCampo do begin
        Formato[K_INDICE].Cadena := Format(Nivel, [I + 1]);
        Formato[K_CLASE].Cadena := ClassName;
        Formato[K_NOMBRE].Cadena := FieldName;
        Formato[K_TIPO].Cadena := FieldTypeNames[DataType];
        if (oCampo is TStringField) and TStringField(oCampo).FixedChar then
          Formato[K_FIJO].Cadena := 'SI'
        else
          Formato[K_FIJO].Cadena := '';
        Formato[K_TAMANO].Cadena := Format('%4d', [DataSize]);
        if DataType = ftFloat then
          Formato[K_TAMANO].Cadena :=
            Format('%s.%d', [Formato[K_TAMANO].Cadena, TFloatField(oCampo).Precision]);
        Status(Formateado(Formato));
        if DataType = ftDataSet then
          MostrarCamposInterno(TDataSetField(oCampo).NestedDataSet,
            Formato[K_INDICE].Cadena + '.%d');
      end;
    end;
  end;

begin
  with Formato[K_INDICE] do begin
    Cadena := 'Index';
    Longitud := 5;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_CLASE] do begin
    Cadena := 'FieldClass';
    Longitud := 20;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_NOMBRE] do begin
    Cadena := 'FieldName';
    Longitud := 0;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_TIPO] do begin
    Cadena := 'DataType';
    Longitud := 15;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_FIJO] do begin
    Cadena := 'FixedChar';
    Longitud := 9;
    Alineado := alLeft;
    Relleno := '-';
  end;

  with Formato[K_TAMANO] do begin
    Cadena := 'Size';
    Longitud := 8;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  Status(Format('LISTADO DE CAMPOS DE %s "%s"', [oDS.ClassName, sTabla]));
  Status('');
  Status(Formateado(Formato));
  Status(StringOfChar('-', 120));

  Result := MostrarCamposInterno(oDS, '%d');

  Status(StringOfChar('-', 120));
  Status(Format(' -> Ok %d Campo(s)', [oDS.FieldCount]));
  Status('');
end;

function TSprint0.MostrarParametros(oDS: TDataSet): Integer;
var
  Formato: array [0 .. 6] of TFormato;
  I, iMax: Integer;
  oParam: TParameter;
begin
  with Formato[K_INDICE] do begin
    Cadena := 'Index';
    Longitud := 5;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_CLASE] do begin
    Cadena := 'Class';
    Longitud := 20;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_NOMBRE] do begin
    Cadena := 'ParamName';
    Longitud := 0;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_TIPO] do begin
    Cadena := 'DataType';
    Longitud := 15;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_FIJO] do begin
    Cadena := 'FixedChar';
    Longitud := 9;
    Alineado := alLeft;
    Relleno := '-';
  end;

  with Formato[K_TAMANO] do begin
    Cadena := 'Size';
    Longitud := 8;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  with Formato[K_VALOR] do begin
    Cadena := 'Valor';
    Longitud := 0;
    Alineado := alLeft;
    Relleno := ' ';
  end;

  Status('LISTA DE PARAMETROS');
  Status(StringOfChar('-', 120));
  Status(Formateado(Formato));
  Status(StringOfChar('-', 120));
  {$IFDEF DELPHIXE3_UP}
  with TADOQuery(oDS).Parameters do begin
  {$ELSE}
  with TOEQuery(oDS).Params do begin
    {$ENDIF}
    Result := Count;
    // Buscar Max(Name)
    iMax := 0;
    for I := 0 to Count - 1 do
      if Length(Items[I].Name) > iMax then
        iMax := Length(Items[I].Name);
    Formato[K_NOMBRE].Longitud := iMax;

    for I := 0 to Count - 1 do begin
      oParam := FFixture.oZetaProvider.GetParametro(oDS, Items[I].Name);
      with oParam do begin
        Formato[K_INDICE].Cadena := IntToStr(I + 1);
        Formato[K_CLASE].Cadena := ClassName;
        Formato[K_NOMBRE].Cadena := Name;
        Formato[K_TIPO].Cadena := FieldTypeNames[DataType];
        Formato[K_FIJO].Cadena := '????';
        Formato[K_TAMANO].Cadena := Format('%4d', [Size]);
        if Precision > 0 then
          Formato[K_TAMANO].Cadena := Formato[K_TAMANO].Cadena + '.' + IntToStr(Precision);
        case VarType(Value) of
          varNull:
            Formato[K_VALOR].Cadena := '(varNull)';
          varEmpty:
            Formato[K_VALOR].Cadena := '(varEmpty)';
          else
            case DataType of
              ftInteger:
                Formato[K_VALOR].Cadena := IntToStr(AsInteger);
              ftFloat:
                Formato[K_VALOR].Cadena := FloatToStr(AsFloat);
              ftDateTime:
                Formato[K_VALOR].Cadena := DateTimeToStr(AsDateTime);
              else
                Formato[K_VALOR].Cadena :=
                  Format('"%s"', [ { AsString } FFixture.oZetaProvider.GetParametro(oDS,
                      Name).AsString]);
            end;
        end;
        Status(Formateado(Formato));
      end;
    end;
    Status(StringOfChar('-', 120));
    Status(Format(' -> Ok %d Par�metro(s)', [Count]));
  end;
  Status('');
end;

function TSprint0.MostrarDatos(oDS: TDataSet): Integer;

  function MostrarDatosInterno(oDS: TDataSet; const sIndentado: string;
    const sTitulo: string = ''): Integer;
  var
    I, iRecs: Integer;
    sRenglon: string;
  begin
    if sTitulo <> '' then
      Status(sIndentado + ' ' + sTitulo);
    iRecs := 0;
    oDS.First;
    while not oDS.Eof do begin
      Inc(iRecs);
      sRenglon := Format('%s%d'#9, [sIndentado, iRecs]);
      for I := 0 to oDS.FieldCount - 1 do
        with oDS.Fields[I] do
          if DataType = ftDataSet then begin
            Status(sRenglon);
            sRenglon := sIndentado + #9;
            MostrarDatosInterno(TDataSetField(oDS.Fields[I]).NestedDataSet, sIndentado + #9,
              FieldName)
          end
          else begin
            if (I > 0) and (I < oDS.FieldCount) then
              sRenglon := sRenglon + ', ';
            sRenglon := sRenglon + Format('%s="%s"', [FieldName, XMLEscapeEncode(AsString)])
          end;
      Status(sRenglon);
      oDS.Next;
    end;
    Result := iRecs;
    Status(Format('%s  -> Ok %d registro(s)', [sIndentado, iRecs]));
    oDS.First;
  end;

begin
  Status(Format('CONTENIDO DE DATASET %s "%s"', [oDS.ClassName, sTabla]));
  Status('');
  Status('NUM'#9'CONTENIDO');
  Status(StringOfChar('-', 120));
  Result := MostrarDatosInterno(oDS, '');
  Status('');
end;

procedure TSprint0.MostrarQuery(sSQL: string);
var
  FDataSet: TDataSet;
begin
  try
    FDataSet := Fixture.oZetaProvider.CreateQuery(sSQL);
    FDataSet.Open;
    MostrarCampos(FDataSet);
    MostrarDatos(FDataSet);
  finally
    FreeAndNil(FDataSet);
  end;
end;

procedure TSprint0.ValidarRecordCountYXMLs(const eCualCatalogo: eTipoCatalogo; sFiltro: string;
  iRegistros: Integer);
var
  oZCDS: TZetaClientDataSet;
begin
  // Obtener datos con conexi�n del sistema
  try
    oZCDS := TZetaClientDataSet.Create(nil);
    FFixture.SetTablaInfo(eCualCatalogo);
    if sFiltro <> '' then
      FFixture.oZetaProvider.TablaInfo.Filtro := sFiltro;
    with FFixture.oZetaProvider do
      oZCDS.Data := GetTabla(EmpresaActiva);
    MostrarCampos(oZCDS);
    // MostrarDatos(oZCDS);
    ValidarRecordCount(oZCDS, iRegistros);
    CrearXML(oZCDS, XMLTemp);
  finally
    FreeAndNil(oZCDS);
  end;
  Check(CompararXML(XMLCase, XMLTemp), 'El contenido XML no es igual');
end;

procedure TSprint0.ValidarRecordCount(oDS: TDataSet; iRegistros: Integer);
begin
  Status(Format('Verificando la cantidad esperada de registros: %d', [iRegistros]));
  Check(oDS.RecordCount = iRegistros, Format('  -> Error, se encontraron %d ', [iRegistros]));
  Status('  -> Ok');
  Status('');
end;

function TSprint0.RestaurarCatalogo: Boolean;
begin
  Status('Restaurando Bases de Datos en ' + oSQLServer.SQLServer);
  FreeAndNil(FFixture);
  Result := oSQLServer.RestoreCatalog;
  if Result then begin
    Status('  -> Ok');
    FFixture := TdmServerCatalogos.Create;
  end else
    Status('  -> Error');
  Status('');
end;

end.
