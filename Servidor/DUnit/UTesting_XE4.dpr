program UTesting_XE4;

uses
  MidasLib,
  Forms,
  GUITestRunner,
  FSprint0 in 'FSprint0.pas',
  FSprint1 in 'FSprint1.pas',
  FSprint2 in 'FSprint2.pas',
  FSprint3 in 'FSprint3.pas',
  ZetaRegistryServer in 'ZetaRegistryServer.pas',
  DServerCatalogos in 'DServerCatalogos.pas',
  DZetaServerProvider in '..\ADO.64\DZetaServerProvider.pas' {dmZetaServerProvider: TDataModule},
  StopWatch in 'StopWatch.pas';

{$R *.res}

begin
  Application.Initialize;
  TGUITestRunner.RunRegisteredTests;
end.
