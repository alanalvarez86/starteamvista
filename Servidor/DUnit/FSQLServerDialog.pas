unit FSQLServerDialog;

interface

{$INCLUDE JEDI.INC}

uses
  SysUtils, FileCtrl, Forms, Dialogs, StdCtrls, Controls, Classes;

type
  // Forma modal para pedir parámetros de conexión SQL Server
  TSQLServerDialog = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtDataSource: TEdit;
    Label2: TLabel;
    edtUsername: TEdit;
    Label3: TLabel;
    edtPassword: TEdit;
    GroupBox2: TGroupBox;
    chkShared: TCheckBox;
    edtSharedBackup: TEdit;
    btnSearchShared: TButton;
    edtDataBackup: TEdit;
    btnSearchData: TButton;
    btnOk: TButton;
    btnCancel: TButton;
    OpenDialog: TOpenDialog;
    chkRestoreNow: TCheckBox;
    edtFolder: TEdit;
    Label4: TLabel;
    btnOpenFolder: TButton;
    chkData: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure btnSearchSharedClick(Sender: TObject);
    procedure btnSearchDataClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnOpenFolderClick(Sender: TObject);
    procedure chkSharedClick(Sender: TObject);
    procedure chkDataClick(Sender: TObject);
  private
    { Private declarations }
    sIni: string;
    oParams: TStringList;
    function GetDataSource: string;
    function GetUsername: string;
    function GetPassword: string;
    function GetRestore: Boolean;
    function GetSharedBackup: string;
    function GetDataBackup: string;
  public
    { Public declarations }
    property DataSource  : string  read GetDataSource;
    property Username    : string  read GetUsername;
    property Password    : string  read GetPassword;
    property Restore     : Boolean read GetRestore;
    property SharedBackup: string  read GetSharedBackup;
    property DataBackup  : string  read GetDataBackup;
  end;

var
  dlgConexionADO: TSQLServerDialog;

implementation

{$R *.dfm}

// -----------------------------------------------------------------------------
procedure TSQLServerDialog.FormCreate(Sender: TObject);

  procedure SetDefault(ValueName: string);
  begin
    if oParams.Values[ValueName] = '' then
      oParams.Values[ValueName] := BoolToStr(False)
  end;
begin
  oParams := TStringList.Create;
  sIni := ChangeFileExt(Application.ExeName, '.ini');
  if FileExists(sIni) then begin
    oParams.LoadFromFile(sIni);
    SetDefault('Restaurar');
    SetDefault('RestaurarComparte');
    SetDefault('RestaurarDatos');

    edtDataSource.Text := oParams.Values['Servidor'];
    edtUsername.Text   := oParams.Values['Usuario'];
    edtPassword.Text   := oParams.Values['Contrasena'];
    chkRestoreNow.Checked := StrToBool(oParams.Values['Restaurar']);
    chkShared.Checked     := StrToBool(oParams.Values['RestaurarComparte']);
    chkData.Checked       := StrToBool(oParams.Values['RestaurarDatos']);
    edtSharedBackup.Text := oParams.Values['Comparte'];
    edtDataBackup.Text   := oParams.Values['Datos'];
    edtFolder.Text       := oParams.Values['Carpeta'];
  end
end;

procedure TSQLServerDialog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ;
end;

procedure TSQLServerDialog.FormDestroy(Sender: TObject);
begin
  oParams.Free
end;

function TSQLServerDialog.GetDataSource: string;
begin
  Result := edtDataSource.Text
end;

function TSQLServerDialog.GetUsername: string;
begin
  Result := edtUsername.Text
end;

function TSQLServerDialog.GetPassword: string;
begin
  Result := edtPassword.Text
end;

function TSQLServerDialog.GetRestore: Boolean;
begin
  Result := chkRestoreNow.Checked
end;

function TSQLServerDialog.GetSharedBackup: string;
begin
  Result := edtSharedBackup.Text
end;

function TSQLServerDialog.GetDataBackup: string;
begin
  Result := edtDataBackup.Text
end;

procedure TSQLServerDialog.btnSearchSharedClick(Sender: TObject);
begin
  OpenDialog.InitialDir := ExtractFileDir(edtSharedBackup.Text);
  OpenDialog.FileName := ExtractFilename(edtSharedBackup.Text);
  if OpenDialog.Execute then
    edtSharedBackup.Text := OpenDialog.FileName
end;

procedure TSQLServerDialog.chkSharedClick(Sender: TObject);
begin
  edtSharedBackup.Enabled := TCheckBox(Sender).Checked;
  btnSearchShared.Enabled := TCheckBox(Sender).Checked;
end;

procedure TSQLServerDialog.chkDataClick(Sender: TObject);
begin
  edtDataBackup.Enabled := TCheckBox(Sender).Checked;
  btnSearchData.Enabled := TCheckBox(Sender).Checked;
end;

procedure TSQLServerDialog.btnOpenFolderClick(Sender: TObject);
var
  FDir: string;
begin
  FDir := edtFolder.Text;
  {$IFDEF DELPHIXE3_UP}
  if SelectDirectory('', ExtractFileDrive(FDir), FDir, [sdNewUI, sdNewFolder]) then
  {$ELSE}
  if SelectDirectory('', ExtractFileDrive(FDir), FDir) then
  {$ENDIF}
    edtFolder.Text := FDir
end;

procedure TSQLServerDialog.btnSearchDataClick(Sender: TObject);
begin
  OpenDialog.InitialDir := ExtractFileDir(edtDataBackup.Text);
  OpenDialog.FileName := ExtractFilename(edtDataBackup.Text);
  if OpenDialog.Execute then
    edtDataBackup.Text := OpenDialog.FileName
end;

procedure TSQLServerDialog.btnOkClick(Sender: TObject);
begin
  oParams.Values['Servidor']          := edtDataSource.Text;
  oParams.Values['Usuario']           := edtUsername.Text;
  oParams.Values['Contrasena']        := edtPassword.Text;
  oParams.Values['Restaurar']         := BoolToStr(chkRestoreNow.Checked);
  oParams.Values['RestaurarComparte'] := BoolToStr(chkShared.Checked);
  oParams.Values['RestaurarDatos']    := BoolToStr(chkData.Checked);
  oParams.Values['Comparte']          := edtSharedBackup.Text;
  oParams.Values['Datos']             := edtDataBackup.Text;
  oParams.Values['Carpeta']           := edtFolder.Text;
  try
    oParams.SaveToFile(sIni);
  finally
    ;
  end;
  ModalResult := mrOk
end;

end.
