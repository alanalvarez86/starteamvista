unit SQLServer;

interface

uses
  SysUtils, Dialogs, Classes, Windows, Forms, Controls, ActiveX, ADODB, DB, Provider;

type
  TSQLOptionSet = (eRestore, eRestoreShared, eRestoreData);
  TSQLOptions   = set of TSQLOptionSet;

  // Clase para SQL Server
  TSQLServer = class(TObject)
    function Open(ShowDialog: Boolean): Boolean;
    function Close: Boolean;
    function RestoreCatalog: Boolean;
    function DeleteCatalog: Boolean;
    procedure SaveToXML(Filename, SQLSentences: string);
    function CreateQuery(Sentences: string): TADOQuery;
    procedure SetupQuery(ADOSQL: TADOQuery);
  private
    StatusWnd     : HWND;
    fADOConn      : TADOConnection;
    FSQLNCLI      : string;
    fParams       : string;
    fSQLServer    : string;
    fInstance     : string;
    fCatalog      : string;
    fUserName     : string;
    fPassword     : string;
    fSharedBackup : string;
    fDataBackup   : string;
    fRestoreFolder: string;
    fConnected    : Boolean;
    fProvider     : TDataSetProvider;
    fOptions      : TSQLOptions;
    function GetSQLNCLIProvider: String;
    procedure SetSQLNCLIProvider(AValue: String);
    function GetDataSource: string;
    procedure SetDataSource(const AValue: string);
    function GetSharedCatalog: string;
    function GetDataCatalog: string;
    function SetupConnection: Boolean;
    function LoadFromFile: Boolean;
    function GetIniFileName: string;
  public
    constructor Create;
    destructor Destroy; override;
    property Connection     : TADOConnection   read fADOConn;
    property DataSource     : string           read GetDataSource      write SetDataSource;
    property SQLServer      : string           read fSQLServer;
    property SQLNCLIProvider: String           read GetSQLNCLIProvider write SetSQLNCLIProvider;
    property Instance       : string           read fInstance          write fInstance;
    property Catalog        : string           read fCatalog;
    property UserName       : string           read fUserName;
    property Password       : string           read fPassword;
    property Connected      : Boolean          read fConnected;
    property Provider       : TDataSetProvider read fProvider;
    property SharedCatalog  : string           read GetSharedCatalog;
    property DataCatalog    : string           read GetDataCatalog;
    property IniFileName    : string           read GetIniFileName;
    property Options        : TSQLOptions      read fOptions           write fOptions;
  end;

  function CreateStatusWindow(const Text: string): HWND;
  procedure RemoveStatusWindow(StatusWindow: HWND);

implementation //===================================================================================

uses
  FSQLServerDialog;

var
  oSQLServer: TSQLServer;

function CreateStatusWindow(const Text: string): HWND;
var
  FormWidth,
  FormHeight: integer;
begin
  FormWidth := 500;
  FormHeight := 100;
  result := CreateWindow('STATIC',
                         PChar(Text),
                         WS_OVERLAPPED or WS_POPUPWINDOW or WS_THICKFRAME or SS_CENTER or SS_CENTERIMAGE,
                         (Screen.Width - FormWidth) div 2,
                         (Screen.Height - FormHeight) div 2,
                         FormWidth,
                         FormHeight,
                         Application.Handle,
                         0,
                         HInstance,
                         nil);
  ShowWindow(result, SW_SHOWNORMAL);
  UpdateWindow(result);
end;

procedure RemoveStatusWindow(StatusWindow: HWND);
begin
  DestroyWindow(StatusWindow);
  Application.ProcessMessages;
end;

constructor TSQLServer.Create;
begin
  inherited;
  CoInitialize(nil);
  fADOConn       := nil;
  FSQLNCLI       := '';
  fParams        := '';
  fSQLServer     := '';
  fInstance      := '';
  fUserName      := '';
  fPassword      := '';
  fOptions       := [];
  fSharedBackup  := '';
  fDataBackup    := '';
  fRestoreFolder := '';
  fConnected   := False;
  fProvider    := TDataSetProvider.Create(nil);
end;

destructor TSQLServer.Destroy;
begin
  FreeAndNil(fProvider);
  inherited;
end;

function TSQLServer.GetSQLNCLIProvider: string;
var
  Providers  : TStringList;
  iCount     : Integer;
  sClientProv: string;
begin
  // Recuperar el cliente nativo prederminado
  Result := FSQLNCLI;
  if Result <> '' then Exit;

  // Obtener la lista de proveedores instalados
  Providers := TStringList.Create;
  GetProviderNames(Providers);

  // Buscar el cliente nativo m�s reciente
  for iCount := Providers.Count - 1 downto 0 do begin
    sClientProv := Providers[iCount];
    if Pos('SQLNCLI', sClientProv) = 1 then begin
      Result := sClientProv;
      Break;
    end;
  end;
  FreeAndNil(Providers);

  if Result = '' then
    // ToDo: No se encontr� cliente nativo, que hay qu� hacer? Excepci�n? Selecci�n del usuario?
  else
    FSQLNCLI := Result
end;

procedure TSQLServer.SetSQLNCLIProvider(AValue: String);
begin
  FSQLNCLI := AValue
end;

function TSQLServer.GetDataSource: string;
begin
  Result := fSQLServer;
  if fInstance <> '' then
    Result := Result + '\' + fInstance;
end;

// Syntaxis de AValue: <SQLServer>[[\Instance][.Catalog]]
procedure TSQLServer.SetDataSource(const AValue: string);
  function ExtractWord(var Sentence: string; const Separator: string): string;
  var
    i: Integer;
  begin
    i := Pos(Separator, Sentence);
    if i < 1 then i := Length(Sentence) + 1;
    Result := Copy(Sentence, 1, i - 1);
    Delete(Sentence, 1, i);
  end;
var
  NewServer: string;
begin
  NewServer := AValue;
  fSQLServer := ExtractWord(NewServer, '\');
  if ( NewServer = '' )  then
  begin
    NewServer := AValue;
    fSQLServer := ExtractWord(NewServer, '.');
    fCatalog   := NewServer;
    fInstance := '';
  end
  else
  begin
    fInstance  := ExtractWord(NewServer, '.');
    fCatalog   := NewServer;
  end;
end;

function TSQLServer.GetSharedCatalog: string;
begin
  Result := 'COMPARTE_UT_435'
end;

function TSQLServer.GetDataCatalog: string;
begin
  Result := 'DEMO_TRESS_UT_435'
end;

function TSQLServer.SetupConnection: Boolean;
begin
  fConnected := False;
  fParams := Format('Provider=%s;Data Source=%s;User ID=%s;Password=%s;' +
                    'MultipleActiveResultSets=true;Pooling=true;DataTypeCompatibility=80',
                    [GetSQLNCLIProvider, GetDataSource, fUserName, fPassword]);
  if fCatalog <> '' then
    fParams := Format('%s;Initial Catalog=%s', [fParams, fCatalog]);

  Screen.Cursor := crHourGlass;
  StatusWnd := CreateStatusWindow('Espere por favor, estableciendo conexi�n con ' + GetDataSource);
  fADOConn := TADOConnection.Create(nil);
  fADOConn.ConnectionString := fParams;
  fADOConn.LoginPrompt := False;
  try
    fADOConn.Open;
    fConnected := fADOConn.Connected;
  except
    on e:Exception do begin
      FreeAndNil(fADOConn);
      MessageDlg(Format('Ocurri� un error al conectarse a %s: %sConnectionString = "%s"',
                        [GetDataSource,  e.Message + sLineBreak, fParams]), mtError, [mbOk], 0)
    end;
  end;
  RemoveStatusWindow(StatusWnd);
  Screen.Cursor := crDefault;
  Result := fConnected;
end;

function TSQLServer.Open(ShowDialog: Boolean): Boolean;
var
  oSQLServerDialog: TSQLServerDialog;
begin
  FreeAndNil(fADOConn);
  fConnected := False;
  Result := True;
  if ShowDialog or (not FileExists(IniFileName)) then begin
    oSQLServerDialog := TSQLServerDialog.Create(nil);
    Result := (oSQLServerDialog.ShowModal = mrOk);
    if Result then begin
      //ToDo: Qu� hacer?
    end;
    FreeAndNil(oSQLServerDialog);
  end;
  if Result then Result := LoadFromFile and SetupConnection
end;

function TSQLServer.Close: Boolean;
begin
  Result := True;
  fConnected := False;
end;

function TSQLServer.RestoreCatalog: Boolean;
var
  Sentences, OldCatalog: string;
begin
  Result := False;
  OldCatalog := fCatalog;
  fCatalog := 'MASTER';
  if not SetupConnection then begin
    fCatalog := OldCatalog;
    Exit;
  end;

  try
    if eRestoreShared in Options then begin
      StatusWnd := CreateStatusWindow('Espere por favor, restaurando cat�logo ' + SharedCatalog);
      // Botar a los usuarios, si hay
      Sentences := 'alter database [' + SharedCatalog + '] set single_user with rollback immediate';
      fADOConn.Execute(Sentences);
      // Restaurar respaldo COMPARTE
      Sentences := Format('restore database [%s] from disk = N%s with file = 1, move N%s to N%s, move N%s to N%s, nounload, replace',
                          [SharedCatalog, QuotedStr(fSharedBackup),
                           QuotedStr('COMPARTE')    , QuotedStr(fRestoreFolder + SharedCatalog + '.mdf'),
                           QuotedStr('COMPARTE_log'), QuotedStr(fRestoreFolder + SharedCatalog + '.ldf')]);
      fADOConn.Execute(Sentences);
      RemoveStatusWindow(StatusWnd);
    end;

    if eRestoreData in Options then begin
      StatusWnd := CreateStatusWindow('Espere por favor, restaurando cat�logo ' + DataCatalog);
      // Botar a los usuarios, si hay
      Sentences := 'alter database ' + DataCatalog + ' set single_user with rollback immediate';
      fADOConn.Execute(Sentences);
      // Restaurar respaldo DEMO_TRESS
      Sentences := Format('restore database [%s] from disk = N%s with file = 1, move N%s to N%s, move N%s to N%s, nounload, replace',
                          [DataCatalog, QuotedStr(fDataBackup),
                           QuotedStr('Datos_Corp_Data'), QuotedStr(fRestoreFolder + DataCatalog + '.mdf'),
                           QuotedStr('Datos_Corp_Log') , QuotedStr(fRestoreFolder + DataCatalog + '.ldf')]);
      fADOConn.Execute(Sentences);
      // Bonus Point: Configurar rutas en COMPARTE
      Sentences := Format('update %s.dbo.COMPANY set CM_DATOS = %s where CM_CONTROL = %s',
                          [SharedCatalog, QuotedStr(DataSource + '.' + DataCatalog), QuotedStr('3DATOS')]);
      fADOConn.Execute(Sentences);
      RemoveStatusWindow(StatusWnd);
    end;
    Result := True;
  except
    on e:Exception do begin
      RemoveStatusWindow(StatusWnd);
      MessageDlg(Format('Ocurri� un error al restaurar al servidor SQL: %sSentencia:%s',
                        [sLineBreak + e.Message + sLineBreak + sLineBreak, sLineBreak + Sentences]),
                 mtError, [mbOk], 0);
    end;
  end;
  fCatalog := OldCatalog;
  FreeAndNil(fADOConn);
end;

function TSQLServer.DeleteCatalog: Boolean;
begin
  Result := False;
  if not SetupConnection then Exit;

  Screen.Cursor := crHourGlass;
  StatusWnd := CreateStatusWindow('Espere por favor, eliminando cat�logo ' + Catalog);
  try
    fADOConn.Execute('drop database ' + SharedCatalog);
    fADOConn.Execute('drop database ' + DataCatalog);
    Result := True;
  except
    on e:Exception do
      MessageDlg('Ocurri� un error al borrar las BDs del servidor SQL:' + sLineBreak + e.Message, mtError, [mbOk], 0);
  end;
  FreeAndNil(fADOConn);
  RemoveStatusWindow(StatusWnd);
  Screen.Cursor := crDefault;
end;

procedure TSQLServer.SaveToXML(Filename, SQLSentences: string);
var
  oQuery: TADOQuery;
begin
  if not SetupConnection then Exit;

  oQuery := CreateQuery(SQLSentences);
  try
    oQuery.SaveToFile('Query.' + Filename, pfXML);
  except
    on e:Exception do
      MessageDlg('Ocurri� un error al exportar usando conexi�n alterna:' + sLineBreak + e.Message, mtError, [mbOk], 0);
  end;
  FreeAndNil(oQuery);
  FreeAndNil(fADOConn);
end;

function TSQLServer.CreateQuery(Sentences: string): TADOQuery;
begin
  if not SetupConnection then Exit;
  Result := TADOQuery.Create(nil);
  fProvider.DataSet := Result;
  with Result do try
    SetupQuery(Result);
    SQL.Text := Sentences;
    Open;
  except
    on e:Exception do begin
      FreeAndNil(Result);
      MessageDlg('Ocurri� un error al crear ADOQuery:' + sLineBreak + e.Message, mtError, [mbOk], 0);
    end;
  end;
  FreeAndNil(fADOConn);
end;

procedure TSQLServer.SetupQuery(ADOSQL: TADOQuery);
begin
  ADOSQL.ConnectionString := fParams;
end;

function TSQLServer.LoadFromFile: Boolean;
var
  oParams   : TStringList;
  NewOptions: TSQLOptions;
begin
  Result      := False;
  NewOptions := [];
  if FileExists(IniFileName) then begin
    oParams    := TStringList.Create;
    fConnected := False;
    oParams.LoadFromFile(IniFileName);
    SetDataSource(oParams.Values['Servidor']);
    fUserName    := oParams.Values['Usuario'];
    fPassword    := oParams.Values['Contrasena'];
    if StrToBool(oParams.Values['Restaurar']) then
      Include(NewOptions, eRestore);
    if StrToBool(oParams.Values['RestaurarComparte']) then
      Include(NewOptions, eRestoreShared);
    if StrToBool(oParams.Values['RestaurarDatos'])
      then Include(NewOptions, eRestoreData);
    fOptions := NewOptions;
    fSharedBackup := oParams.Values['Comparte'];
    fDataBackup   := oParams.Values['Datos'];
    FreeAndNil(oParams);
    Result := True
  end;
end;

function TSQLServer.GetIniFileName: string;
begin
  Result := ChangeFileExt(Application.ExeName, '.ini')
end;

initialization //===================================================================================
  oSQLServer := TSQLServer.Create;
  if oSQLServer.Open(True) then begin
    if eRestore in oSQLServer.Options then
      oSQLServer.RestoreCatalog
  end else begin
    FreeAndNil(oSQLServer);
    Halt;
  end

end.