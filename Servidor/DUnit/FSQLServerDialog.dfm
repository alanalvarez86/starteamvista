object SQLServerDialog: TSQLServerDialog
  Left = 507
  Top = 138
  BorderIcons = [biSystemMenu]
  Caption = 'Par'#225'metros de conexi'#243'n'
  ClientHeight = 252
  ClientWidth = 483
  Color = clBtnFace
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    483
    252)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 467
    Height = 81
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Servidor SQL '
    TabOrder = 0
    DesignSize = (
      467
      81)
    object Label1: TLabel
      Left = 16
      Top = 28
      Width = 40
      Height = 13
      Caption = 'Servidor'
    end
    object Label2: TLabel
      Left = 16
      Top = 55
      Width = 36
      Height = 13
      Caption = 'Usuario'
    end
    object Label3: TLabel
      Left = 224
      Top = 55
      Width = 56
      Height = 13
      Caption = 'Contrase'#241'a'
    end
    object edtDataSource: TEdit
      Left = 80
      Top = 24
      Width = 379
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object edtUsername: TEdit
      Left = 80
      Top = 51
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object edtPassword: TEdit
      Left = 288
      Top = 51
      Width = 121
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 95
    Width = 467
    Height = 113
    Anchors = [akLeft, akTop, akRight]
    Caption = '  '
    TabOrder = 1
    DesignSize = (
      467
      113)
    object Label4: TLabel
      Left = 32
      Top = 82
      Width = 77
      Height = 13
      Caption = 'Carpeta destino'
    end
    object chkShared: TCheckBox
      Left = 16
      Top = 28
      Width = 113
      Height = 13
      Caption = 'BAK de [Comparte]'
      Checked = True
      State = cbChecked
      TabOrder = 7
      OnClick = chkSharedClick
    end
    object edtSharedBackup: TEdit
      Left = 135
      Top = 24
      Width = 300
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
    end
    object btnSearchShared: TButton
      Left = 439
      Top = 24
      Width = 21
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 2
      OnClick = btnSearchSharedClick
    end
    object edtDataBackup: TEdit
      Left = 135
      Top = 51
      Width = 300
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 3
    end
    object btnSearchData: TButton
      Left = 439
      Top = 51
      Width = 21
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 4
      OnClick = btnSearchDataClick
    end
    object chkRestoreNow: TCheckBox
      Left = 13
      Top = -2
      Width = 164
      Height = 17
      Caption = 'Restaurar respaldos al iniciar'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object edtFolder: TEdit
      Left = 135
      Top = 78
      Width = 300
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 5
    end
    object btnOpenFolder: TButton
      Left = 438
      Top = 78
      Width = 21
      Height = 21
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 6
      OnClick = btnOpenFolderClick
    end
    object chkData: TCheckBox
      Left = 16
      Top = 55
      Width = 113
      Height = 13
      Caption = 'BAK de [Datos]'
      Checked = True
      State = cbChecked
      TabOrder = 8
      OnClick = chkDataClick
    end
  end
  object btnOk: TButton
    Left = 319
    Top = 214
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Aceptar'
    Default = True
    TabOrder = 2
    OnClick = btnOkClick
  end
  object btnCancel: TButton
    Left = 399
    Top = 214
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancelar'
    ModalResult = 2
    TabOrder = 3
  end
  object OpenDialog: TOpenDialog
    Filter = 'Respaldos|*.bak|Todos los archivos|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 232
    Top = 56
  end
end
