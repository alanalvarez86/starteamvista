unit ZCreator;

interface

{$define CUENTA_EMPLEADOS}
{$define VALIDAEMPLEADOSGLOBAL}

uses Windows, Messages, SysUtils, Classes,
     {$ifdef CUENTA_EMPLEADOS}
     FAutoServer,
     {$ifdef VALIDAEMPLEADOSGLOBAL}
     ZetaLicenseClasses,
     {$endif}
     {$endif}
     ZBaseCreator,
     ZetaCommonClasses,
     ZetaCommonLists,
     DSalMin,
     DPrimasIMSS,
     DTablasISPT,
     DPrestaciones,
     DQueries,
     DValUma,
     DZetaServerProvider;

type
  TRastreador = class( TObject )
  private
    { Private declarations }
    FRastro: TStrings;
    function GetRastroText: String;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Rastro: String read GetRastroText;
    procedure AgregaTexto(const sTexto: String);
    procedure RastreoBegin;
    procedure RastreoCargaLista( Lista: TStrings );
    procedure RastreoEspacio;
    procedure RastreoFactor( const sTexto: String; const rFactor: TTasa );
    procedure RastreoFecha(const sTexto: String; const dValue: TDate);
    procedure RastreoHeader( const sTitulo: String );
    procedure RastreoSeparadorPag( const sTitulo: String );
    procedure RastreoPesos( const sTexto: String; const rMonto: TPesos );
    procedure RastreoPesosOpc( const sTexto: String; const rMonto: TPesos; const lOpcional: Boolean );
    procedure RastreoFloat( const sTexto: string; const rMonto: TTasa; const iDecimales : Integer = 2 );
    procedure RastreoMsg( const sTexto: String );
    procedure RastreoTasa( const sTexto: String; const rTasa: TTasa );
    procedure RastreoTexto( const sTexto, sValor: String );
    procedure RastreoTextoLibre( const sTexto : String ); overload;
    procedure RastreoNoVacio( const sTexto : String );
    procedure RastreoTextoLibre( const sTexto : String; var iCurrentPos: Integer ); overload;
    procedure RastreoInsertaTexto( const sTexto : String; const iRenglon: Integer );
  end;

  TZetaCreator = class(TZetaBaseCreator)
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FPrestaciones: TdmPrestaciones;
    FSalarioIntegrado: TSalarioIntegrado;
    FSalMin: TdmSalmin;
    FValUma: TdmValUma;
    FTablasISPT: TdmTablasISPT;
    FPrimasIMSS: TdmPrimasIMSS;
    FRitmos: TRitmos;
    FQueries: TCommonQueries;
    FPromediando: Boolean;
    FRastreador: TRastreador;
    FRastreadorNomina: TRastreador;
    FPolizaPrimero: Boolean;
    FPolizaCuadre: TPesos;
    function GetRastreando: Boolean;
    procedure DesPreparaPrestaciones;
    procedure DesPreparaPrimasIMSS;
    procedure DesPreparaQueries;
    procedure DesPreparaRitmos;
    procedure DesPreparaSalarioIntegrado;
    procedure DesPreparaSalMin;
    procedure DesPreparaValUma;
    procedure DesPreparaTablasISPT;
    function GetRastreandoNomina: Boolean;
    function GetRastreador: TRastreador;
  public
    { Public declarations }
    property dmPrestaciones: TdmPrestaciones read FPrestaciones;
    property dmSalMin: TdmSalmin read FSalMin;
    property dmValUma: TdmValUma read FValUma;
    property dmTablasISPT: TdmTablasISPT read FTablasISPT;
    property dmPrimasIMSS: TdmPrimasIMSS read FPrimasIMSS;
    property Queries: TCommonQueries read FQueries;
    property Rastreador: TRastreador read GetRastreador;
    property Rastreando: Boolean read GetRastreando;
    property RastreandoNomina: Boolean read GetRastreandoNomina;
    property Ritmos: TRitmos read FRitmos;
    property SalarioIntegrado: TSalarioIntegrado read FSalarioIntegrado;
    property Promediando: Boolean read FPromediando write FPromediando;
    property PolizaPrimero: Boolean read FPolizaPrimero write FPolizaPrimero;
    property PolizaCuadre: TPesos read FPolizaCuadre write FPolizaCuadre;
    procedure RegistraFunciones( Funciones: TipoFunciones );override;
    procedure PreparaSalMin;
    procedure PreparaValUma;
    procedure PreparaTablasISPT;
    procedure PreparaPrimasImss;
    procedure PreparaPrestaciones;
    procedure PreparaSalarioIntegrado;
    procedure PreparaQueries;
    procedure PreparaRitmos;
    procedure PreparaRastreo;
    procedure DesPreparaRastreo;
    procedure PreparaRastreoNomina;
    procedure DesPreparaRastreoNomina;
    {$ifdef CUENTA_EMPLEADOS}
    function ValidaLimiteEmpleados( oAutoServer: TAutoServer; operacion : eEvaluaOperacion;  var sMensajeAdvertencia, sMensajeError: String): Boolean;
    {$else}
    function GetMaxEmpleadosActivos( const dInicio: TDate ): Integer;
    {$endif}
  end;

var
  ZetaCreator: TZetaCreator;

implementation

uses ZetaCommonTools,
     {$ifdef CUENTA_EMPLEADOS}
     ZetaLicenseMgr,
     {$endif}
     ZFuncsGenerales,
     ZFuncsNomina,
     ZFuncsReporte,
     ZFuncsReporteNomina,
     ZFuncsPoliza,
     ZFuncsTress,
     ZFuncsISPT,
     ZFuncsIMSS,
     ZFuncsAnuales,
     ZFuncsCafe,
     ZFuncsMedico,
     ZFuncsAguinaldo,
     ZFuncsFonacot;

{$R *.DFM}

{ *********** TRastreador ********** }

constructor TRastreador.Create;
begin
     FRastro := TStringList.Create;
end;

destructor TRastreador.Destroy;
begin
     FRastro.Free;
     inherited Destroy;
end;

function TRastreador.GetRastroText: String;
begin
     Result:= FRastro.Text;
end;

procedure TRastreador.RastreoBegin;
begin
     FRastro.Clear;
end;

procedure TRastreador.RastreoCargaLista(Lista: TStrings);
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             Assign( FRastro );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TRastreador.RastreoEspacio;
begin
     RastreoTexto( '', '' );
end;

procedure TRastreador.RastreoFactor(const sTexto: String; const rFactor: TTasa);
begin
     RastreoTexto( sTexto, Padl( Format( '%12.4n', [ rFactor ] ), 17 ));
end;

procedure TRastreador.RastreoFecha(const sTexto: String; const dValue: TDate);
begin
     RastreoTexto( sTexto, Padl( FormatDateTime( 'dd/mmm/yyyy', dValue ), 17 ) );
end;

procedure TRastreador.RastreoHeader(const sTitulo: String);
const
     K_SEPARADOR_RASTREO = '======================================================================';
begin
     FRastro.Add( K_SEPARADOR_RASTREO );
     RastreoTexto( sTitulo, '' );
end;

procedure TRastreador.RastreoSeparadorPag(const sTitulo: String);
const
     K_SEPARADOR_PAGINA = '______________________________________________________________________________________';
begin
     RastreoTexto( sTitulo, '' );
     FRastro.Add( K_SEPARADOR_PAGINA );
end;

procedure TRastreador.RastreoMsg(const sTexto: String);
begin
     RastreoTexto( sTexto, '' );
end;

procedure TRastreador.RastreoPesos(const sTexto: String; const rMonto: TPesos);
begin
     RastreoPesosOpc( sTexto, rMonto, False );
end;

procedure TRastreador.RastreoPesosOpc(const sTexto: String; const rMonto: TPesos; const lOpcional: Boolean);
begin
     if not lOpcional or ( rMonto <> 0 ) then
        RastreoTexto( sTexto, Padl( Format( '%12.2n', [ rMonto ] ), 15 ));
end;

procedure TRastreador.RastreoFloat( const sTexto: string; const rMonto: TTasa; const iDecimales : Integer );
begin
     RastreoTexto( sTexto, Padl( Format( '%12.'+IntToStr(iDecimales)+'n', [ rMonto ] ), 17 ) );
end;

procedure TRastreador.RastreoTasa(const sTexto: String; const rTasa: TTasa);
begin
     RastreoTexto( sTexto, Padl( Format( '%12.4n', [ rTasa ] ), 17 ) + '%' );
end;

procedure TRastreador.RastreoTexto(const sTexto, sValor: String);
begin
     RastreoTextoLibre( ZetaCommonTools.PadL( sTexto, 50 ) + sValor );
end;

procedure TRastreador.RastreoTextoLibre( const sTexto : String; var iCurrentPos: Integer );
begin
     RastreoTextoLibre( sTexto );
     iCurrentPos:= FRastro.Count;
end;

procedure TRastreador.RastreoTextoLibre(const sTexto: String);
begin
     FRastro.Add( Copy( sTexto, 1, 80 ));
end;

procedure TRastreador.RastreoNoVacio( const sTexto : String );
begin
     if StrLleno( sTexto ) then
      RastreoTextoLibre(sTexto);
end;

procedure TRastreador.RastreoInsertaTexto(const sTexto: String; const iRenglon: Integer );
begin
     FRastro.Insert( iRenglon, sTexto );
end;

procedure TRastreador.AgregaTexto( const sTexto: String );
var
   pValor, pStart: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
   sValor: String;
begin
     pValor := Pointer( sTexto );
     if ( pValor <> nil ) then
     begin
          while ( pValor^ <> #0 ) do
          begin
               pStart := pValor;
               while not ( pValor^ in [ #0, #10, #13 ] ) do
               begin
                    Inc( pValor );
               end;
               SetString( sValor, pStart, ( pValor - pStart ) );
               FRastro.Add( sValor );
               if ( pValor^ = #13 ) then
                  Inc( pValor );
               if ( pValor^ = #10 ) then
                  Inc( pValor );
          end;
     end;
end;

{ ************** TZetaCreator **************** }

procedure TZetaCreator.RegistraFunciones( Funciones: TipoFunciones );
begin
     inherited;
     if (efComunes in Funciones) then
        Funciones := Funciones + [efGenerales,efTress,efISPT,efIMSS,efMedico,efFonacot];

     if efGenerales in Funciones then
        ZFuncsGenerales.RegistraFunciones( FunctionLibrary );
     if efTress in Funciones then
        ZFuncsTress.RegistraFunciones( FunctionLibrary );
     if efReglasPrestamos in Funciones then
        ZFuncsTress.RegistraFuncionesPrestamo( FunctionLibrary );
     if efISPT in Funciones then
        ZFuncsISPT.RegistraFunciones( FunctionLibrary );
     if efIMSS in Funciones then
        ZFuncsIMSS.RegistraFunciones( FunctionLibrary );
     if efFonacot in Funciones then
        ZFuncsFonacot.RegistraFunciones( FunctionLibrary );
     if efAnuales in Funciones then
        ZFuncsAnuales.RegistraFunciones( FunctionLibrary );
     if efNomina in Funciones then
        ZFuncsNomina.RegistraFunciones( FunctionLibrary );
     if efReporte in Funciones then
     begin
        ZFuncsReporte.RegistraFunciones( FunctionLibrary );
        ZFuncsReporteNomina.RegistraFunciones( FunctionLibrary );
     end;
     if efReporteBalanza in Funciones then
     begin
        ZFuncsReporteNomina.RegistraFuncionesBalanza( FunctionLibrary );
     end;
     if efPoliza in Funciones then
        ZFuncsPoliza.RegistraFunciones( FunctionLibrary );
     if efPolizaGral in Funciones then
        ZFuncsPoliza.RegistraFuncionesGeneral( FunctionLibrary );
     if efCafe in Funciones then
        ZFuncsCafe.RegistraFunciones( FunctionLibrary );
     if efMedico in Funciones then
        ZFuncsMedico.RegistraFunciones( FunctionLibrary );
     if efAguinaldo in Funciones then
        ZFuncsAguinaldo.RegistraFunciones( FunctionLibrary );
end;

procedure TZetaCreator.DataModuleDestroy(Sender: TObject);
begin
     DesPreparaRastreo;
     DesPreparaRastreoNomina;
     DesPreparaRitmos;
     DesPreparaQueries;
     DesPreparaSalarioIntegrado;
     DesPreparaPrestaciones;
     DesPreparaPrimasIMSS;
     DesPreparaSalMin;
     DesPreparaValUma;
     DesPreparaTablasISPT;
     if Assigned( oZetaProvider ) then
        oZetaProvider.LiberaEmpleadosConAjuste;
     inherited;
end;

procedure TZetaCreator.PreparaPrestaciones;
begin
     DesPreparaPrestaciones;
     if not Assigned( FPrestaciones ) then
        FPrestaciones := TdmPrestaciones.Create( oZetaProvider );
end;

procedure TZetaCreator.PreparaSalarioIntegrado;
begin
     if not Assigned( FSalarioIntegrado ) then
        FSalarioIntegrado := TSalarioIntegrado.Create( Self );
end;

procedure TZetaCreator.PreparaSalMin;
begin
     DesPreparaSalMin;
     if not Assigned( FSalMin ) then
        FSalMin := TdmSalMin.Create( oZetaProvider );
end;

procedure TZetaCreator.PreparaValUma;
begin
     DesPreparaValUma;
     if not Assigned( FValUma ) then
        FValUma := TdmValUma.Create( oZetaProvider );
end;

procedure TZetaCreator.PreparaTablasISPT;
begin
     DesPreparaTablasISPT;
     if not Assigned( FTablasISPT ) then
        FTablasISPT := TdmTablasISPT.Create( oZetaProvider );
end;

procedure TZetaCreator.PreparaPrimasImss;
begin
     DesPreparaPrimasIMSS;
     if not Assigned( FPrimasIMSS ) then
        FPrimasIMSS := TdmPrimasIMSS.Create( oZetaProvider );
end;

procedure TZetaCreator.PreparaQueries;
begin
     if not Assigned( FQueries ) then
        FQueries := TCommonQueries.Create( oZetaProvider );
end;

procedure TZetaCreator.PreparaRitmos;
begin
     if not Assigned( FRitmos ) then
     begin
          FRitmos := TRitmos.Create( oZetaProvider );
          if Assigned( FQueries ) then
             FRitmos.Queries := FQueries;
     end;
end;

procedure TZetaCreator.PreparaRastreo;
begin
     if not Assigned( FRastreador ) then
     begin
          FRastreador := TRastreador.Create;
     end;
end;

procedure TZetaCreator.DesPreparaSalMin;
begin
     FreeAndNil( FSalmin );
end;

procedure TZetaCreator.DesPreparaValUma;
begin
     FreeAndNil( FValUma );
end;

procedure TZetaCreator.DesPreparaTablasISPT;
begin
     FreeAndNil( FTablasIspt );
end;

procedure TZetaCreator.DesPreparaPrimasIMSS;
begin
     FreeAndNil( FPrimasImss );
end;


procedure TZetaCreator.DesPreparaPrestaciones;
begin
     FreeAndNil( FPrestaciones );
     FPrestaciones := NIL;
end;

procedure TZetaCreator.DesPreparaSalarioIntegrado;
begin
     FreeAndNil( FSalarioIntegrado );
end;

procedure TZetaCreator.DesPreparaQueries;
begin
     FreeAndNil( FQueries );
end;

procedure TZetaCreator.DesPreparaRitmos;
begin
     FreeAndNil( FRitmos );
end;

procedure TZetaCreator.DesPreparaRastreo;
begin
     FreeAndNil( FRastreador );
end;

function TZetaCreator.GetRastreando: Boolean;
begin
     Result := ( FRastreador <> nil );
end;

procedure TZetaCreator.DesPreparaRastreoNomina;
begin
     FreeAndNil( FRastreadorNomina );
end;

function TZetaCreator.GetRastreandoNomina: Boolean;
begin
     Result:= ( FRastreadorNomina <> nil );
end;

procedure TZetaCreator.PreparaRastreoNomina;
begin
     if not Assigned( FRastreadorNomina ) then
     begin
          FRastreadorNomina := TRastreador.Create;
     end;
end;

function TZetaCreator.GetRastreador: TRastreador;
begin
     if Rastreando then
       Result:= FRastreador
     else if RastreandoNomina then
        Result:= FRastreadorNomina
     else
         Result:= nil;
end;

{ Validar L�mite de Empleados }

{$ifdef CUENTA_EMPLEADOS}
{$ifdef VALIDAEMPLEADOSGLOBAL}
function TZetaCreator.ValidaLimiteEmpleados( oAutoServer: TAutoServer;  operacion : eEvaluaOperacion; var sMensajeAdvertencia,  sMensajeError: String): Boolean;
const
     K_MSG_FORMAT = 'Intentando Proceso Para %d Empleados' + CR_LF +
                    'Se Excedi� El L�mite De %d Licencias De Empleados Asignadas A Esta Empresa' + CR_LF +
                    'El Sentinel # %d Tiene %d Licencias Totales';


var
   FManager: TLicenseMgr;
   FGlobalLicense : TGlobalLicenseValues;
   evaluacion : eEvaluaTotalEmpleados;
   iMaximo, iEmpleados, iAsignados, iSentinel : Integer;
   tipoCompany : eTipoCompany;
begin
     sMensajeAdvertencia := VACIO;
     sMensajeError := VACIO;

     with oAutoServer do
     begin
          iMaximo := Empleados;
          iSentinel := NumeroSerie;
     end;

     FManager := TLicenseMgr.Create( oZetaProvider );
     tipoCompany := FManager.GetTipoValidoCompany;
     FGlobalLicense := TGlobalLicenseValues.Create( oAutoServer );
     FGlobalLicense.MostrarAdvertencia := Fmanager.GetGlobalMostrarAdvertencia(); // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
     try
        if FGlobalLicense.DebeAdvertir or FGlobalLicense.DebeValidar then
        begin
            //Prueba de concepto sin guardar en Clave
            with FManager do
            begin
                 AutoServer := oAutoServer;
            end;
            FGlobalLicense.TotalGlobal :=  oAutoServer.EmpleadosConteo;
            FGlobalLicense.TipoCompany :=  tipoCompany;
            evaluacion := FGlobalLicense.EvaluarEmpleadosMensaje(operacion,  sMensajeAdvertencia , sMensajeError);
            Result := not ( evaluacion >= evRebasaRestringe );
        end
        else
        begin
            //Procesa como antes
            with FManager do
            begin
                 AutoServer := oAutoServer;
                 iEmpleados := EmpleadosGet;
                 iAsignados := AsignadosGet;
                 Result := ( iEmpleados <= ZetaCommonTools.iMin( iAsignados, iMaximo ) );
            end;

            if Result then
               sMensajeError := VACIO
            else
                sMensajeError := Format( K_MSG_FORMAT, [ iEmpleados, iAsignados, iSentinel, iMaximo ] );
        end;
     finally
            FreeAndNil( FManager );
     end;



end;
{$else}
function TZetaCreator.ValidaLimiteEmpleados( oAutoServer: TAutoServer; var sMensaje: String): Boolean;
const
     K_MSG_FORMAT = 'Intentando Proceso Para %d Empleados' + CR_LF +
                    'Se Excedi� El L�mite De %d Licencias De Empleados Asignadas A Esta Empresa' + CR_LF +
                    'El Sentinel # %d Tiene %d Licencias Totales';
var
   FManager: TLicenseMgr;
   iMaximo, iEmpleados, iAsignados, iSentinel: Integer;
begin
     with oAutoServer do
     begin
          iMaximo := Empleados;
          iSentinel := NumeroSerie;
     end;
     FManager := TLicenseMgr.Create( oZetaProvider );
     try
        with FManager do
        begin
             AutoServer := oAutoServer;
             iEmpleados := EmpleadosGet;
             iAsignados := AsignadosGet;
             Result := ( iEmpleados <= ZetaCommonTools.iMin( iAsignados, iMaximo ) );
        end;
     finally
            FreeAndNil( FManager );
     end;
     if Result then
        sMensaje := VACIO
     else
         sMensaje := Format( K_MSG_FORMAT, [ iEmpleados, iAsignados, iSentinel, iMaximo ] );

end;
{$endif}
{$else}
function TZetaCreator.GetMaxEmpleadosActivos( const dInicio:TDate ): Integer;
const
{$ifdef CUENTA_USANDO_FECHA}
{$ifdef INTERBASE}
       K_QRY_GET_MAXEMPLEADOS = 'select COUNT(*) as CUANTOS from COLABORA '+
                                'where ( ( select RESULTADO from SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';
{$endif}
{$ifdef MSSQL}
       K_QRY_GET_MAXEMPLEADOS = 'select COUNT(*) as CUANTOS from COLABORA '+
                                'where ( ( select DBO.SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';
{$endif}
{$else}
        K_QRY_GET_MAXEMPLEADOS = 'select COUNT(*) as CUANTOS from COLABORA where ( CB_ACTIVO = %s )';
{$endif}
var
   FCursor: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FCursor := CreateQuery;
          try
             {$ifdef CUENTA_USANDO_FECHA}
                     AbreQueryScript( FCursor, Format( K_QRY_GET_MAXEMPLEADOS, [ DatetoStrSQLC( dInicio ) ] ));
             {$else}
                     AbreQueryScript( FCursor, Format( K_QRY_GET_MAXEMPLEADOS, [ EntreComillas( K_GLOBAL_SI ) ] ));
             {$endif}
             Result := FCursor.FieldByName('CUANTOS').AsInteger;
          finally
                 FreeAndNil(FCursor);
          end;
     end;
end;
{$endif}

end.
