unit ZetaSQL;

{ ***** ESPECIAL PARA INTERBASE *******}

interface

uses SysUtils;

type
  eTipoSQL = ( tsqlFloat, tsqlEntero, tsqlFecha, tsqlTexto, tsqlBlob, tsqlBooleano );

const
     {Constantes para construccion de QUERYS}
     K_LOJ = ' LEFT OUTER JOIN ';
     K_JOIN = ' JOIN ';
     K_LOJON = ' ON ';
     K_SELECTFIELD = ' SELECT ';
     K_SELECTALL  = ' SELECT * ';
     K_FROM       = ' FROM ';
     K_WHERE      = ' WHERE ';
     K_ORDER      = ' ORDER BY ';
     K_GROUP      = ' GROUP BY ';
     K_UNION      = 'UNION';
     K_COMA       = ' , ';
     K_COMILLA    = '"';
     K_ABREP      = ' ( ';
     K_CIERRAP    = ' ) ';
     K_AND        = ' AND ';
     K_OR         = ' OR ';
     K_PUNTO      = '.';
     K_IGUAL      = ' = ';
     K_MAYORQUE   = ' > ';
     K_MENORQUE   = ' < ';
     K_MAYORI     = ' >= ';
     K_MENORI     = ' <= ';
     K_VACIO      = '';
     //K_BOOLTRUE   = 'S';
     //K_BOOLFALSE  = 'N';
     K_ASCENDENTE = ' ASC ';
     K_DESCENDENTE= ' DESC ';

//function  ConcatFiltros( const sFiltro, sExpresion: String ): String;
function  LeftOuterJoin( const sTablaHijo, sCampoHijo, sTablaPadre, sCampoPadre : String;
                         const lPrimero : Boolean;
                         const lEsInnerJoin : Boolean = FALSE ) : String;
function  GetCastFormula( const sFormula : String; eTipo : eTipoSQL ) : String;
function  ExpresionAlias( const sExpresion, sAlias : String ) : String;
function  PreparaFormulaSQL( const sFormula : String ) : String;
function  ConvierteFechas( sTempo : String ) : String;

implementation

uses ZetaCommonTools, Controls;

{ ESTA FUNCION ESTA EN ZETACOMMMONTOOLS
function ConcatFiltros( const sFiltro, sExpresion: String ): String;
begin
     Result := ConcatString( sFiltro, sExpresion, K_AND );
end;}

{CV: 3-Dic-2001
Se agreg� la propiedad lEsInnerJoin, para que el join del Padre contra el hijo se haga
con JOIN enves de con LEFT OUTER JOIN. Est propiedad se asigna al agregar la relacion
y se hace en dmEntidadesTress, las unicas RELACIONES que ponen en TRUE esta propiedad
son ACUMULA-COLABORA y ACUMULA-CONCENPTO.
La propiedad InnerJoin tambien tienen implementacion en
las unidades ZetaEntidad.Pas DEntidadesTress.pas y ZetaSql.pas}

function  LeftOuterJoin( const sTablaHijo, sCampoHijo, sTablaPadre, sCampoPadre : String;
                         const lPrimero : Boolean;
                         const lEsInnerJoin : Boolean = FALSE ) : String;
begin
     Result := '';
     // Solo en el primer par, va el nombre de la tabla padre
     // En los demas pares va el AND
     if ( lPrimero ) then
     begin
          if lEsInnerJoin then Result := K_JOIN
          else Result := K_LOJ;

          Result := Result + sTablaPadre + K_LOJON;
     end
     else
         Result := K_AND;

     // LEFT OUTER JOIN PADRE ON PADRE.CAMPO1 = HIJO.CAMPO1
     //                      AND PADRE.CAMPO2 = HIJO.CAMPO2
     Result := Result +
               sTablaPadre + K_PUNTO + sCampoPadre + K_IGUAL +
               sTablaHijo  + K_PUNTO + sCampoHijo;
end;

function GetCastFormula( const sFormula : string; eTipo : eTipoSQL ) : string;
begin
     {$IFDEF INTERBASE}
     case eTipo of
          tsqlFloat  : Result := 'CAST( ( ' + sFormula + ' ) as NUMERIC(15,5) )';
          tsqlEntero : Result := 'CAST( ( ' + sFormula + ' ) as INTEGER )';
          tsqlFecha  : Result := 'CAST( ( ' + sFormula + ' ) as DATE )';
          tsqlTexto, tsqlBooleano : Result := '( ' + sFormula + ' )';
          else Result := sFormula;
     end;
     {$endif}

     {$IFDEF MSSQL}
     if eTipo in [ tsqlFloat, tsqlEntero, tsqlFecha, tsqlTexto, tsqlBooleano ] then
        Result := '( ' + sFormula + ' )'
     else Result := sFormula;
     {$endif}
end;

function  ExpresionAlias( const sExpresion, sAlias : String ) : String;
begin
     Result := sExpresion + ' ' + sAlias;
end;

function ConvierteFechas( sTempo : String ) : String;
var
   sFecha : String;
   nPos, nInicio, nFinal : Integer;
   cInicio : Char;
   lConvierte : Boolean;
   dFecha : TDate;


begin
     Result := '';
     nPos := POS( '/', sTempo );
     while ( nPos > 0 ) do
     begin
          lConvierte := FALSE;
          nFinal := 0;
          if ( sTempo[ nPos-3 ] = '''' ) or ( sTempo[ nPos-3 ] = '"' ) then
	      nInicio := nPos-3
          else if ( sTempo[ nPos-2 ] = '''' ) or ( sTempo[ nPos-2 ] = '"' ) then
              nInicio := nPos-2
          else
              nInicio := 0;

          if ( nInicio > 0 ) then
          begin
             cInicio := sTempo[ nInicio ];
             nFinal  := POS( cInicio, Copy( sTempo, nInicio+1, MAXINT ));
             if ( nFinal > 0 ) then
             begin
                  sFecha := Copy( sTempo, nInicio+1, nFinal-1 );
                  lConvierte := EsFechaValida( sFecha, dFecha );
                  if not lConvierte then
                         raise Exception.Create( EntreComillas( sFecha ) + ' No es una fecha v�lida');
             end;
          end;
          if ( lConvierte ) then
          begin
             Result := Result + Copy( sTempo, 1, nInicio-1 ) + '''' +
                              DateToStrSQL( dFecha ) + '''';
             nPos := nInicio+nFinal;
          end
          else
               Result := Result + Copy( sTempo, 1, nPos );

          sTempo := Copy( sTempo, nPos+1, MAXINT );
          nPos := POS( '/', sTempo );
     end;
     Result := Result + sTempo;
end;


function  PreparaFormulaSQL( const sFormula : String ) : String;
begin
     Result := GetCastFormula( ConvierteFechas( sFormula ), tsqlTexto );
end;




end.
