unit FServerBase;

interface

uses
  IdHTTPWebBrokerBridge, IdContext, SysUtils, StdCtrls, ComCtrls, ScktComp, DServerBase, ZetaASCIIFile, Data.DB, Classes,
  Datasnap.DBClient, cxMemo;

type
  TCafeLog = class(TASCIILog)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure WriteTexto(const sTexto: String); override;
  end;

  TServerBase = class(TDataModule)
    dsUsuarios: TDataSource;
    UserDataSet: TClientDataSet;
    UserDataSetAddress: TStringField;
    UserDataSetActivity: TStringField;
    UserDataSetDate: TDateTimeField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FExceptionMessage: String;
    FPuerto          : Word;
    FHayPuerto       : Boolean;
    FSocket          : TIdHTTPWebBrokerBridge;
    FDataModule      : TdmServerBase;
    FLog             : TCafeLog;
    FConnected       : Boolean;
    // FMemo            : TMemo;
    FMemo            : TcxMemo;
    FStatusPanel     : TStatusPanel;
    FPortPanel       : TStatusPanel;
    FirstRun         : Boolean;
    FRegistryKey: string;
    procedure SetConnected(const Value: Boolean);
    function GetPortNumber: Word;
    procedure SetPortNumber(const Value: Word);
    procedure UpdateUserDataSet(const sAddress: String; const lConnected: Boolean);
    procedure SetPortPanel(const Value: TStatusPanel);
    procedure SetRegistryKey(const Value: string);
  protected
    { Protected declarations }
    property  SOAPServer: TIdHTTPWebBrokerBridge read FSocket write FSocket;
    procedure LogInit(AFile: string = '');
    procedure LogWrite;
    procedure ClientConnect(AContext: TIdContext);
    procedure ClientDisconnect(AContext: TIdContext);
    procedure ShowStatus(const sMensaje: String);
  public
    { Public declarations }
    property Connected       : Boolean read FConnected write SetConnected;
    property ExceptionMessage: String read FExceptionMessage;
    property MainDataModule  : TdmServerBase read FDataModule write FDataModule;
    property Log             : TCafeLog read FLog;
    // property Memo            : TMemo read FMemo write FMemo;
    property Memo            : TcxMemo read FMemo write FMemo;
    property PortNumber      : Word read GetPortNumber write SetPortNumber;
    property PortPanel       : TStatusPanel read FPortPanel write SetPortPanel;
    property RegistryKey     : string read FRegistryKey write SetRegistryKey;
    property StatusPanel     : TStatusPanel read FStatusPanel write FStatusPanel;
    procedure LogIt(const sMensaje: String);
    procedure LogMessage(const sMensaje: String);
    procedure AppException(Sender: TObject; E: Exception);
    procedure LogExcepcion(const sMensaje: String; e: Exception);
    procedure ServerConnect(const lEnabled: Boolean);
    function GetGlobal(const sIdent, sDefault: string): string; overload;
    function GetGlobal(const sIdent: string; iDefault: Integer): Integer; overload;
    function GetGlobal(const sIdent: string; const dDefault: TDateTime): TDateTime; overload;
    function SetGlobal(const sIdent, sValue: string): Boolean; overload;
    function SetGlobal(const sIdent: string; iValue: Integer): Boolean; overload;
    function SetGlobal(const sIdent: string; const dValue: TDateTime): Boolean; overload;
  end;

var
  ServerBase: TServerBase;

implementation

uses
  Registry, Windows, Messages, ZetaCommonTools, ZetaCommonClasses, ZetaMessages, CafeteraConsts, CafeteraUtils, IdAssignedNumbers,
  Vcl.Controls;

{$R *.DFM}
{ ***** TCafeLog **** }

procedure TCafeLog.WriteTexto(const sTexto: String);
begin
    inherited WriteTexto(sTexto);
    // Flush(FBuffer);
end;

{ ******* TServerBase ********* }

procedure TServerBase.DataModuleCreate(Sender: TObject);
begin
  FirstRun   := True;
  FHayPuerto := False;
  FLog := TCafeLog.Create;
  //LogInit; // Se inicializa el nombre del archivo en el SetRegistryKey()
  ZetaCommonTools.Environment;
  FConnected := False;

  FSocket := nil;

  UserDataSet.CreateDataSet;
  UserDataSet.LogChanges := False;
  UserDataSet.Open;

end;

procedure TServerBase.DataModuleDestroy(Sender: TObject);
begin
  LogWrite;
  FreeAndNil(FLog);
  FreeAndNil(FSocket);
end;

function TServerBase.GetPortNumber: Word;
begin
  if Assigned(FSocket) then
    Result := FSocket.DefaultPort
  else
    Result := 0//K_CAFETERA_PUERTO
end;

procedure TServerBase.SetPortNumber(const Value: Word);
begin
  FPuerto    := Value;
  FHayPuerto := True;
  if Assigned(FPortPanel) then
    FPortPanel.Text := Format('Puerto %d', [FPuerto]);
end;

procedure TServerBase.SetPortPanel(const Value: TStatusPanel);
begin
  FPortPanel := Value;
  if Assigned(FPortPanel) then
    FPortPanel.Text := Format('Puerto %d', [FPuerto]);
end;

procedure TServerBase.SetRegistryKey(const Value: string);
begin
  FRegistryKey := '\Software\Grupo Tress\' + Value;
  LogInit(Value)
end;

procedure TServerBase.ServerConnect(const lEnabled: Boolean);
begin
  if not FHayPuerto then
    Exit;
  if Assigned(FSocket) and FSocket.Active and not lEnabled then begin
    FSocket.Active := False;
    LogIt('Servidor Desconectado');
  end;
  FreeAndNil(FSocket);
  try
    if lEnabled then begin
      // Crear la conexion para recibir peticiones de los clientes
      FSocket              := TIdHTTPWebBrokerBridge.Create(Self);
      FSocket.DefaultPort  := FPuerto;
      FSocket.OnConnect    := ClientConnect;
      FSocket.OnDisconnect := ClientDisconnect;
      FSocket.Active       := lEnabled;
      if FirstRun then
        LogIt(Format('Servidor Conectado en puerto %d', [FSocket.DefaultPort]))
      else
        LogIt(Format('Servidor Reconectado en puerto %d', [FSocket.DefaultPort]));
      FirstRun := False;
    end;
  except
    on e: Exception do begin
      FreeAndNil(FSocket);
      LogExcepcion(Format('O.o %s Puerto=%d', [ClassName + '.ServerConnect()', FPuerto]), e);
    end;
  end;
  FConnected := Assigned(FSocket) and FSocket.Active;
end;

function TServerBase.GetGlobal(const sIdent, sDefault: string): string;
var
  sValor: string;
begin
  if GetRegistryString(RegistryKey, sIdent, sValor) then
    Result := sValor
  else
    Result := sDefault
end;

function TServerBase.GetGlobal(const sIdent: string; iDefault: Integer): Integer;
var
  sValor: string;
begin
  if GetRegistryString(RegistryKey, sIdent, sValor) then
    Result := StrToIntDef(sValor, iDefault)
  else
    Result := iDefault
end;

function TServerBase.GetGlobal(const sIdent: string; const dDefault: TDateTime): TDateTime;
var
  dValor: TDateTime;
begin
  if GetRegistryDateTime(RegistryKey, sIdent, dValor) then
    Result := dValor
  else
    Result := dDefault
end;

function TServerBase.SetGlobal(const sIdent, sValue: string): Boolean;
begin
  Result := False;
  with TRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey(RegistryKey, True) then begin
        WriteString(sIdent, sValue);
        Result := True;
      end
    finally
      Free;
    end;
  end;
end;

function TServerBase.SetGlobal(const sIdent: string; iValue: Integer): Boolean;
begin
  Result := False;
  with TRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey(RegistryKey, True) then begin
        WriteString(sIdent, IntToStr(iValue));
        Result := True;
      end
    finally
      Free;
    end;
  end;
end;

function TServerBase.SetGlobal(const sIdent: String; const dValue: TDateTime): Boolean;
begin
  Result := False;
  with TRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey(RegistryKey, True) then begin
        WriteDateTime(sIdent, dValue);
        Result := True;
      end
    finally
      Free;
    end;
  end;
end;

procedure TServerBase.SetConnected(const Value: Boolean);
begin
  ServerConnect(Value);
  { Notifica al datamodule que las empresas ser�n reconectadas }
  if Assigned(FDataModule) then
    FDataModule.Connected := FConnected;
end;

procedure TServerBase.LogIt(const sMensaje: String);
begin
  ShowStatus(sMensaje);
  LogMessage(sMensaje);
end;

procedure TServerBase.LogMessage(const sMensaje: String);
var
  sTexto: string;
begin
  sTexto := Log.GetTimeStamp  + ': ' +
            //Format('(PID=%d) ', [GetCurrentProcessId]) +
            sMensaje;
  Log.WriteTexto(sTexto);

  if not Assigned(FMemo) then
    Exit;
  with FMemo.Lines do begin
    BeginUpdate;
    try
      Add(sTexto);
      // SendMessage(FMemo.Handle, EM_LINESCROLL, 0, FMemo.Lines.Count);
      FMemo.InnerControl.Perform(EM_LINESCROLL, 0,  FMemo.Lines.Count);
    finally
      EndUpdate;
    end;
  end;
end;

procedure TServerBase.AppException(Sender: TObject; E: Exception);
begin
  if E is ESocketError then begin
    FExceptionMessage := ESocketError(ExceptObject).Message;
    LogMessage(FExceptionMessage);
  end else begin
    if E is EDataBaseError then
      FExceptionMessage := EDataBaseError(E).Message
    else
      FExceptionMessage := E.Message;
    if (Sender <> nil) then begin
      if (Sender is TCustomWinSocket) then
        LogMessage(FExceptionMessage)
      else
        LogMessage(FExceptionMessage);
    end;
  end;
end;

procedure TServerBase.LogExcepcion(const sMensaje: String; e: Exception);
begin
  ShowStatus(sMensaje);
  LogMessage(sMensaje + ' ' + ExceptionAsString(e));
end;

procedure TServerBase.LogInit(AFile: string = '');
var
  K_LOG: String;
  sLog : String;

  procedure SwapFiles(const sSource, sResult: String);
  begin
    if FileExists(sResult) then
      SysUtils.DeleteFile(sResult);
    RenameFile(sSource, sResult);
  end;

begin
  if AFile = '' then
    K_LOG := ChangeFileExt(ExtractFileName(ParamStr(0)), '%s.log')
  else
    K_LOG := AFile + '%s.log';
  sLog  := ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['']));
  {$IFDEF ANTES}
  SwapFiles(ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['2'])), ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['3'])));
  SwapFiles(ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['1'])), ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['2'])));
  SwapFiles(sLog, ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['1'])));
  {$ELSE}
  if FileExists(sLog) and (Trunc(FileDateToDateTime(FileAge(sLog))) <> Date) then begin
    SwapFiles(ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['2'])),
      ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['3'])));
    SwapFiles(ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['1'])),
      ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['2'])));
    SwapFiles(sLog, ZetaMessages.SetFileNameDefaultPath(Format(K_LOG, ['1'])));
  end;
  {$ENDIF}
  with FLog do begin
    Init(sLog);
  end;
end;

procedure TServerBase.LogWrite;
begin
  with FLog do begin
    Close;
  end;
end;

procedure TServerBase.ShowStatus(const sMensaje: String);
begin
  if Assigned(FStatusPanel) then begin
    FStatusPanel.Text := sMensaje;
  end;
end;

procedure TServerBase.UpdateUserDataSet(const sAddress: String; const lConnected: Boolean);
var iConexion : Integer;
begin
  Exit; // Se anula el registro de cada conexion, ya que es por peticion, no tiene sentido llenar bitacora
  iConexion := K_CAFETERA_FUERA_LINEA_TEXT;

  if lConnected then
     iConexion := K_CAFETERA_EN_LINEA_TEXT;

  LogIt(Format('Cliente %s %s', [sAddress, ESTADO_CONEXION[iConexion]]));
  with UserDataSet do
    try
      Append;
      FieldByName('ADDRESS').AsString  := sAddress;
      FieldByName('ACTIVITY').AsString := ESTADO_CONEXION[iConexion];
      FieldByName('DATE').AsDateTime   := Now;
      Post;
    except
      on E: Exception do
        AppException(nil, E)
    end;
end;

{ ******* Eventos del Server Socket ******* }

procedure TServerBase.ClientConnect(AContext: TIdContext);
begin
  UpdateUserDataSet(AContext.Binding.IP, True);
end;

procedure TServerBase.ClientDisconnect(AContext: TIdContext);
begin
  UpdateUserDataSet(AContext.Binding.IP, False);
end;

end.

