unit DServerBase;

interface

uses
  Windows, Classes, DB;

type
  TdmServerBase = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FConnected: Boolean;
    FLock: TRTLCriticalSection;
  protected
    { Protected declarations }
    function GetQuery: TDataset; virtual; abstract;
    function GetScript(const iScript: Integer): String; virtual;
  public
    { Public declarations }
    property Connected: Boolean read FConnected write FConnected;
    property Query: TDataset read GetQuery;
    procedure Connect; virtual; abstract;
    procedure ConnectQuery(const sSQL: String); virtual; abstract;
    procedure Disconnect; virtual; abstract;
    procedure IniciarZonaCritica;
    procedure TerminarZonaCritica;
  end;

var
  dmServerBase: TdmServerBase;

implementation

uses
  ZetaCommonClasses;

{$R *.DFM}
{ TdmServerBase }

procedure TdmServerBase.DataModuleCreate(Sender: TObject);
begin
  InitializeCriticalSection(FLock);
end;

procedure TdmServerBase.DataModuleDestroy(Sender: TObject);
begin
  DeleteCriticalSection(FLock);
end;

function TdmServerBase.GetScript(const iScript: Integer): String;
begin
  Result := VACIO;
end;

procedure TdmServerBase.IniciarZonaCritica;
begin
  EnterCriticalSection(FLock);
end;

procedure TdmServerBase.TerminarZonaCritica;
begin
  LeaveCriticalSection(FLock);
end;

end.
