unit DSuperReporte;
interface
{.$define BOSE} //<-- ESTA ES LA DIRECTIVA PARA EL PROYECTO DE BOSE
{$include DEFINES.INC}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Math, DB, Provider, DBClient, ADODB,
  ZSuperSQL,
  ZAgenteSQLClient,
  ZAgenteSQL,
  ZetaQRExpr,
  ZCreator,
  ZetaServerDataSet,
  ZetaCommonLists,
  ZetaCommonClasses,
  DEntidades,
  DZetaServerProvider,
  ZEvaluador;

const
    K_ANCHO_TEXTO = 100;  { Ancho default para textos con Ancho = 0 }
    K_MAX_GRUPOS  = 5; {m�ximo de grupos permitidos para ASCII's}
    K_TOTAL_NAME  = 'TOTAL_';
{$ifdef BOSE}
    K_GRUPO_NAME  = 'GRUPO_';
    K_CUANTOS_NAME  = 'NTOTAL_';
{$else}
{$endif}
type
  TNotifyEvalua = procedure( DataSet : TZetaCursor; oColumna : TSQLColumna; oCampo : TField ) of object;
  TTotaliza = class(TObject)
  private
    FCampo : TField;
    FPosTotal  : Integer;
    FCriterio : eTipoOperacionCampo;
    FSuma : Array[0..K_MAX_GRUPOS] of TPesos;
    FNumero : Array[0..K_MAX_GRUPOS] of Integer;
    FTipoFormula : eTipoGlobal;
    FEvaluador : TZetaEvaluador;
    FConstante : TPesos;
    FNumTotGrupos : integer;
    FSuperReporte : TDataModule;
    function Resultado(const nCorte: integer) : TPesos;
//    function BooleanoVerdadero : Boolean;
    function ValorNumero : TPesos;
    procedure SetMaxGrupos( const nMaxGrupos : Integer );
  public
    constructor Create( oDataSet : TDataset; const iTotal : Integer; oColumna : TSQLColumna; Evaluador : TZetaEvaluador; oSuperReporte : TDataModule );
    procedure Inicializa(const nCorte: integer);
    procedure Acumula(const nCorte: integer);
    procedure AsignaResultado( oDataSet : TDataset;const nCorte: integer );
    property  MaxGrupos : Integer read FNumTotGrupos write SetMaxGrupos;
  end;
  TSubTotal = class(TObject)
  private
    FValores : array[ 0..K_MAX_SUB_TOT ] of TPesos;
    FCuantos : Integer;
    FAcumulado : Integer;
  public
    constructor Create;
    procedure Almacena( nPos : Integer; nValor : TPesos );
    property  Cuantos : Integer read FCuantos write FCuantos;
    property  Acumulados : Integer read FAcumulado write FAcumulado;
  end;
  TColumnaGrupo = class(TObject)
  private
    FCampo : TField;
    FValor : Variant;
    procedure LeeCampo;
  end;
  TColumnaAggregate = class(TObject)
  private
    FCampo : TField;
    FEvaluador : TZetaEvaluador;
    FValor : Variant;
    FBanda : Integer;
    procedure CalculaCampo;
    procedure LeeCampo;
  end;
  TdmSuperReporte = class(TDataModule)
    DataSetReporte: TServerDataSet;
    DataSetTotales: TServerDataSet;
    DatasetSubtotales: TServerDataSet;
    procedure dmSuperReporteCreate(Sender: TObject);
    procedure dmSuperReporteDestroy(Sender: TObject);
  private
    { Private declarations }
    FAgente : TSQLAgente;
    FEvaluadores : TList;
    FEvaluadoresFiltro : TList;
    FListaAggregates : TList;
    FHayAggregates : Boolean;
    FListaGrupos : TList;
    FCorteGrupo : Integer;
    FQuery : TZetaCursor;
    FTotales : Boolean;
    FTotalesGrupos : Boolean;
    FOnEvaluaColumna : TNotifyEvalua;
    FParametros: Variant;
    FTotalizando : Boolean;
    FOptimizaTotales : Boolean;
    FListaTotales : TList;
    FNumTotales : Integer;
    FNumTotalesGrupos : Integer;
    FNumGrupos : Integer;
    FListaSubTot : TList;
    FPointerSubTot : Integer;
    FSubTotal : TSubTotal;
    FGranTotal : TSubTotal;
    FCuantos : Integer;
    FCuantosSubTot : Integer;
    FListaAlmacenan : TList;
    procedure CreaCampos;
    procedure LimpiaEvaluadores;
    procedure CreaEvaluadores;
    procedure EjecutaQuery;
    procedure AgregaRegistros;
    function  CumpleFiltroCliente : Boolean;
    function  TotalizaDataSet : Boolean;
    function  oZetaProvider : TdmZetaServerProvider;
    function  oZetaCreator : TZetaCreator;
    procedure AddAggregate( oCampo : TField; oEvaluador : TZetaEvaluador; const iBanda : Integer );
    procedure AddGrupo(oCampo: TField);
    function  CorteGrupo: Boolean;
    procedure LeeGrupos;
    procedure ResetAggregates;
    function  ErrorGeneral(const sError: String) : Integer;
    function HayTotales: Boolean;
    procedure CopiaCampos;
    procedure CreaListaTotales;
    procedure AgregaRegistroTotales;
    procedure CopiaRegistro;
    procedure AcumulaTotales;
    {$ifdef BOSE_TEMP}
    //cv: POSTERIORMENTE SE UTILIZARA ESTE METODO, SE DEJA EN ESTA DIRECTIVA PARA QUE NO MARQUE EL WARNING
    procedure AcumulaSubTotales;
    {$else}
    {$endif}

    procedure AsignaResultado(const nCorte: integer; const lFinal : Boolean);
    procedure CopiaAggregates;
    procedure LeeAggregates;
    procedure PrimeraPasada;
    procedure AgregaSubTotal;
    procedure AsignaSubTotal(nCorte: Integer);
    procedure AsignaTotales;
    procedure SetMaxGrupos(const nGrupos: Integer);
    procedure InitCuantosSubTot;
    procedure RevisaCuantosSubTot;
    procedure RegresaSubTot;
    procedure AdelantaSubTot;
    procedure AumentaCuantos;
    procedure AlmacenaTotales(const nCorte, nGrupo: Integer);
    procedure PreparaListaAlmacenan;
  public
    { Public declarations }
    property Parametros: Variant read FParametros write FParametros;
    function Construye( Agente : TSQLAgente; const lTotales : Boolean = FALSE; const lTotalesGrupos : Boolean = FALSE ) : Boolean;
    function GetResult( iResult : Integer ) : TPesos;
    function GetSubTotal( const iResult, iCorte : Integer ) : TPesos;
    function GetCuantosSubTotal( const iCorte : Integer ) : Integer;
    function GetReporte : Variant;
    function  Prepara( Agente : TSQLAgente; const lTotales : Boolean = FALSE ) : Boolean;
    function  AbreDataSet : Boolean;
    procedure CierraDataSet;
    procedure SetParamValue( const sNombre : String; const Tipo : eTipoGlobal; const Valor : Variant );
    property  OnEvaluaColumna : TNotifyEvalua read FOnEvaluaColumna write FOnEvaluaColumna;
    property  QueryReporte : TZetaCursor read FQuery;
    property  Agente : TSQLAgente read FAgente write FAgente;
  end;

implementation

uses ZetaSQLBroker,
     ZetaCommonTools;

const K_BOOLEANO_SI = 'SI';
      K_BOOLEANO_NO = 'NO';

{$R *.DFM}

procedure TdmSuperReporte.dmSuperReporteCreate(Sender: TObject);
{$IFDEF SERVERCAFE}
var
componenteGuid : TGUID;
sNAme : string;
{$ENDIF}
begin
{$IFDEF SERVERCAFE}
     CreateGUID( componenteGuid );
     sName := StrTransAll(  GUIDToString(componenteGuid), '{', '' ) ;
     sName := StrTransAll(  sName, '}', '' ) ;
     sName := StrTransAll(  sName, '-', '' ) ;
     Self.Name := 'dmSuperReporte'+sName;
{$ENDIF}

     FEvaluadores  := TList.Create;
     FEvaluadoresFiltro := TList.Create;
     FListaAggregates := TList.Create;
     FListaGrupos  := TList.Create;
     FQuery        := oZetaProvider.CreateQuery;
     FTotales      := FALSE;
     FTotalesGrupos := FALSE;
     FListaTotales := TList.Create;
     FListaSubTot  := TList.Create;
     FListaAlmacenan := TList.Create;
     // oZetaCreator.SuperReporte := SELF;
end;

function TdmSuperReporte.oZetaCreator: TZetaCreator;
begin
  Result := TZetaCreator(Owner);
end;

function TdmSuperReporte.oZetaProvider: TdmZetaServerProvider;
begin
  if Assigned(Owner) then
    Result := oZetaCreator.oZetaProvider // Linea original
  else
     Result := TdmZetaServerProvider(Owner); //Linea nueva
end;

procedure TdmSuperReporte.dmSuperReporteDestroy(Sender: TObject);
begin
     LimpiaEvaluadores;
     FEvaluadores.Free;
     FEvaluadoresFiltro.Free;
     FListaAggregates.Free;
     FListaGrupos.Free;
     FListaTotales.Free;
     FListaSubTot.Free;
     FListaAlmacenan.Free;
end;

function TdmSuperReporte.ErrorGeneral(const sError: String) : Integer;
begin
     Result := FAgente.ListaErrores.Add( sError );
end;

procedure TdmSuperReporte.SetMaxGrupos(const nGrupos : Integer );
var
    i : Integer;
begin
    with FListaTotales do
        for i := Count-1 downto 0 do
            TTotaliza( Items[ i ] ).MaxGrupos := nGrupos;
end;

procedure TdmSuperReporte.PreparaListaAlmacenan;
var
    i : Integer;
    oColumna : TSQLColumna;
begin
    FListaAlmacenan.Clear;
    with FAgente do
        for i := 0 to NumColumnas-1 do
        begin
            oColumna := GetColumna( i );
            // S�lo almacena los Totales de aquellas columnas que
            // 1) Hay sido referenciadas en SubTotal( 'x' )
            // 2) Tengan un Totalizador (Criterio <> Ninguno)
            if ( oColumna.LugarSubTot >=0 ) and ( oColumna.Totalizador >= 0 ) then
                FListaAlmacenan.Add( oColumna );
        end;
end;

function TdmSuperReporte.AbreDataSet: Boolean;
begin
     EjecutaQuery;
     Result := NOT FQuery.Eof;
     if Result then
     begin
          CreaEvaluadores;
          {CV: 23-nov-2001:
           En el caso de que el AgregaRegistros no agregue ningun registro, debido a que
           se tiene algun filtro especial (OnFilterRecord siempre regresa FALSE), el
           DatasetReporte se est� quEdando vacio, y no me esta marcando ningun error.
           Ademas de que el Resultado de la funcion siempre me regresa TRUE}

          if ( FTotales or FAgente.SubTotales ) then
          begin
            CopiaCampos;
            {$ifdef BOSE}
            if FAgente.SubTotales then
               DataSetSubTotales.InitTempDataset;
            {$else}
            {$endif}
            CreaListaTotales;
            DataSetTotales.CreateTempDataSet;
            {$ifdef BOSE}
            if FAgente.SubTotales then
            begin
                 //if FNumGrupos = 0 then
                 //   raise Exception.Create( 'No se permite usar la funci�n SubTotal() si no existe por lo menos un grupo' );

                 if FNumGrupos > 5 then
                    raise Exception.Create( 'S�lo se permiten hasta 5 agrupaciones cuando se utiliza la funci�n SubTotal()' );

                 if FNumGrupos <> 0 then
                    DataSetSubTotales.CreateTempDataSet;
            end;
            {$else}
            {$endif}
          end;

          {$ifdef CAROLINA}
          DataSetSubTotales.SaveToFile('d:\temp\subt.cds');
          {$endif}

          if ( FAgente.SubTotales ) then
          begin
               SetMaxGrupos( FNumGrupos );
               PreparaListaAlmacenan;
               PrimeraPasada;
          end;

          SetMaxGrupos( FNumTotalesGrupos );
          AgregaRegistros;

          DataSetReporte.First;
     end
     else ErrorGeneral( K_NoHayRegistros );

     //FQuery.Close;    No se debe cerrar ya que hay procesos que lo reutilizan: Comparativo
end;


function TdmSuperReporte.CumpleFiltroCliente : Boolean;
var
   i : Integer;
begin
     Result := TRUE;
     with FAgente, FEvaluadoresFiltro do
       for i := 0 to Count-1 do
         with TZetaEvaluador( Items[ i ] ).Value do
         begin
           if ( Kind = resBool ) then
               Result := booResult
           else
               // Expresi�n regresa un tipo diferente
               Result := FALSE;
           // Si ya es falso, No tiene caso que siga evaluando
           if ( not Result ) then Exit;
         end;
end;

procedure TdmSuperReporte.AgregaSubTotal;
begin
    FSubTotal := TSubTotal.Create;
    FPointerSubTot := FListaSubTot.Add( FSubTotal );
    FSubTotal.Acumulados := FCuantos;
    FSubTotal.Cuantos    := FCuantosSubTot;
    FCuantosSubTot       := 0;
end;

procedure TdmSuperReporte.AlmacenaTotales(const nCorte, nGrupo : Integer );
var
    i : integer;
    {$ifdef BOSE}
    j : Integer;
    {$endif}
    oColumna : TSQLColumna;
    oTotaliza : TTotaliza;
    {$ifdef BOSE}
    sField,sGrupo,sTituloGrupo: string;
    {$else}
    {$endif}
begin
    if ( FListaAlmacenan.Count > 0 ) then
    begin
      FTotalizando := TRUE;

      {$ifdef BOSE}
      sGrupo := '';
      if ( FNumGrupos > 0 ) then
         with FListaGrupos do
              for i := 0 to FNumGrupos-1 do
                  with TColumnaGrupo( Items[ i ] ) do
                  begin
                       sGrupo := sGrupo +';' + String( FValor );
                       sTituloGrupo := sTituloGrupo + ';' + K_GRUPO_NAME + IntToStr(i);
                  end;
      sGrupo := Copy(sGrupo, 2, length(sGrupo));
      sTituloGrupo := Copy(sTituloGrupo, 2, length(sTituloGrupo));
      {$else}
      {$endif}
      with FListaAlmacenan do
        for i := 0 to Count-1 do
        begin
            oColumna := Items[ i ];
            oTotaliza := TTotaliza( FListaTotales[ oColumna.Totalizador ] );
            FSubTotal.Almacena( oColumna.LugarSubTot, oTotaliza.Resultado( nGrupo ));

            {$ifdef BOSE}
            //Llega a estar Cerrado cuando el reporte NO tiene Grupos y Se usa la funcion Total()
            if DataSetSubTotales.Active then
            begin
                 with DataSetSubTotales do
                 begin
                      Edit;
                      for j:= 0 to FNumGrupos -1 do
                      begin
                           sField := K_TOTAL_NAME + IntToStr(oColumna.PosCampo) +'_'+ IntToStr(j);
                           FieldByName( sField ).AsFloat := oTotaliza.FSuma[j+1];
                           sField := K_CUANTOS_NAME + IntToStr(j);
                           FieldByName( sField ).AsFloat := oTotaliza.FNumero[j+1];
                      end;
                      Post;
                 end;
            end;
            {$else}
            {$endif}
            {$ifdef CAROLINA}
            DataSetSubTotales.SaveToFile('d:\temp\subtotales.xml');
            {$endif}
            // Si es el �ltimo corte, no inicializa para dejar los
            // valores intactos y puedan ser almacenados en GranTotal
            if ( nCorte > 0 ) then
                oTotaliza.Inicializa( nCorte );
        end;
      FTotalizando := FALSE;
    end;
end;

procedure TdmSuperReporte.AsignaSubTotal(nCorte : Integer);
begin
     AgregaSubTotal;
     // S�lo estamos almacenando el Corte m�s Interno
     AlmacenaTotales( nCorte, FNumGrupos );
end;

procedure TdmSuperReporte.AsignaTotales;
begin
    // Agrega un ULTIMO SubTotal, que contiene el GranTotal
    FCuantosSubTot := FCuantos;
    AgregaSubTotal;
    // Almacena el Total-Total
    AlmacenaTotales( 0, 0 );
    // Guarda el Apuntador al ULTIMO SubTotal
    FGranTotal := FSubTotal;
end;

procedure TdmSuperReporte.PrimeraPasada;
var
   i : Integer;
   oCampo : TField;
   lGrupos : Boolean;
   lHayCorte : Boolean;
   lPrimero  : Boolean;
   oColumna: TSQLColumna;

   {$ifdef BOSE}
   procedure AgregaCodigoGrupos;
    var i: integer;
   begin
        if ( FNumGrupos > 0 ) then
        begin
           with FListaGrupos do
           begin
                DatasetSubTotales.Append;
                for i := 0 to FNumGrupos-1 do
                begin
                     with TColumnaGrupo( Items[ i ] ) do
                     begin
                          DatasetSubTotales.FieldByName(K_GRUPO_NAME+IntToStr(i)).AsString := FCampo.Value;
                     end;
                 end;
                 DatasetSubTotales.Post;
             end;
        end;
   end;
   {$else}
   {$endif}
begin
     lPrimero := TRUE;
     lGrupos := ( FNumGrupos > 0 ) and FHayAggregates;
     DataSetReporte.EmptyDataSet;
     DataSetReporte.IndexFieldNames := '';
     DataSetReporte.Append;  // El unico registro de Detalle
     InitCuantosSubTot;

     with FQuery do
     begin
       // No se necesita: First;
       while not Eof do
       begin
          if ( not FAgente.FiltroCliente ) or ( CumpleFiltroCliente ) then
          begin
            with FAgente do
              for i := 0 to NumColumnas-1 do
              begin
                oColumna := GetColumna( i ) ;
                with oColumna do
                  if not EsConstante then
                  begin
                       oCampo := DataSetReporte.Fields[ PosCampo ];
                       if ( DeDiccionario ) then
                          oCampo.Value := FieldByName( Formula ).Value
                       else if ( oCampo.DataType = ftBlob ) then
                          oCampo.AsString := ''  // No se necesita
                       // Si la f�rmula contiene SubTotal(), no tiene
                       // caso evaluarla en la primera pasada
                       else if ( UsaSubTotal ) then
                           oCampo.Value := 0
                       else
                           with TZetaEvaluador( FEvaluadores[ i ] ).Value do
                             case Kind of
                               resInt   : oCampo.AsInteger := intResult;
                               resDouble: if ( resFecha ) then oCampo.AsDateTime := dblResult
                                          else oCampo.AsFloat   := dblResult;
                               resString: oCampo.AsString := strResult;
                               resBool  : if ( booResult ) then oCampo.AsString := K_BOOLEANO_SI
                                          else oCampo.AsString := K_BOOLEANO_NO;
                               else
                                  DataBaseError( DesarmaErrorEvaluador(  strResult ).ErrCompleto );
                             end;
                  end;
                end;
            // En el primer Registro no se hace Corte de Grupo
            if ( lPrimero ) then
            begin
                lPrimero  := FALSE;
                lHayCorte := FALSE;
                FCorteGrupo := 0;

                LeeGrupos;
                {$ifdef BOSE}
                AgregaCodigoGrupos;
                {$else}
                {$endif}
            end
            else
                lHayCorte := CorteGrupo;

            // Si hay Corte de Grupo, se hace RESET a Aggregates
            // y se vuelven a calcular
            if ( lGrupos ) and ( lHayCorte ) then
                ResetAggregates;

            // En este punto, ya est� listo el registro Detalle
            // Se puede pasar a Totales.
            if ( lHayCorte ) then
            begin
                // FCorteGrupo := iMin(FCorteGrupo,FNumTotalesGrupos);
                AsignaSubTotal(FCorteGrupo);
                FCorteGrupo := 0;
                {$ifdef BOSE}
                //Agregar los c�rtes de los grupos
                AgregaCodigoGrupos;
                {$else}
                {$endif}
            end;

            {$ifdef CAROLINA}
            DataSetSubTotales.SaveToFile('d:\temp\subtotales.xml');
            {$endif}

            if FHayAggregates then
              LeeAggregates;
            {$ifdef CAROLINA}
            DataSetSubTotales.SaveToFile('d:\temp\subtotales.xml');
            {$endif}
            AcumulaTotales;
            {$ifdef CAROLINA}
            DataSetSubTotales.SaveToFile('d:\temp\subtotales.xml');
            {$endif}

            if ( lHayCorte ) then
                LeeGrupos;

            // Contador de # de registros que cumplen filtro
            AumentaCuantos;
          end;
          Next;
       end;
     end;

     // Totaliza el �ltimo registro
     AsignaSubTotal(0);
     // Guarda los GranTotales en otro SubTotal
     AsignaTotales;
     DataSetReporte.Cancel;
            {$ifdef CAROLINA}
            DataSetSubTotales.SaveToFile('d:\temp\subtotales.xml');
            {$endif}

     //AcumulaSubTotales;
            {$ifdef CAROLINA}
            DataSetSubTotales.SaveToFile('d:\temp\subtotales.xml');
            {$endif}

     // Deja listo para la SegundaPasada
     with FQuery do
     begin
          Close;
          Open;
     end;
end;

procedure TdmSuperReporte.InitCuantosSubTot;
begin
     FPointerSubTot := -1;
     FCuantos := 0;         // Contador de registros
     FCuantosSubTot := 0;         // Cuantos en SubTotal de Pointer
     FSubTotal := NIL;
end;

procedure TdmSuperReporte.AumentaCuantos;
begin
    Inc( FCuantos );        // Cuantos en Total
    Inc( FCuantosSubTot );  // Cuantos en ese Grupo
end;

procedure TdmSuperReporte.AdelantaSubTot;
begin
    // Puede darse el caso que no esten sincronizadas
    // la primera pasada con la segunda, por
    // cambios de los datos en el inter.
    Inc( FPointerSubTot );
    try
        FSubTotal := FListaSubTot[ FPointerSubTot ];
        FCuantosSubTot := FSubTotal.Acumulados;
    except
    end;
end;

procedure TdmSuperReporte.RegresaSubTot;
begin
    Dec( FPointerSubTot );
    try
        FSubTotal := FListaSubTot[ FPointerSubTot ];
        FCuantosSubTot := FSubTotal.Acumulados;
    except
    end;
end;

procedure TdmSuperReporte.RevisaCuantosSubTot;
begin
    Inc( FCuantos );
    if ( FCuantos > FCuantosSubTot ) then
        AdelantaSubTot;
end;

procedure TdmSuperReporte.AgregaRegistros;
var
   i : Integer;
   oCampo : TField;
   lGrupos : Boolean;
   lOptimiza : Boolean;
   lHayCorte : Boolean;
   lPrimero  : Boolean;
   oColumna : TSQLColumna;

begin  // AgregaRegistros
     lPrimero := TRUE;
     lGrupos := ( FNumGrupos > 0 ) and FHayAggregates;
     DataSetReporte.EmptyDataSet;
     DataSetReporte.IndexFieldNames := '';
     {$ifdef BOSE}
     if FAgente.SubTotales then
     begin
          if DataSetSubtotales.Active then
             DataSetSubtotales.First;
          FOptimizaTotales := FALSE;
     end;
     {$else}
     {$endif}
     lOptimiza := FTotales and FOptimizaTotales;
     // Cuando se hace un reporte SoloTotales optimizado,
     // No es necesario hacer Append/Post en DataSet reporte
     // Solo se hace un append fuera del ciclo y se reutiliza el mismo
     // registro de detalle. Solo se agregan registros a DataSetTotales
     if ( lOptimiza ) then
        DataSetReporte.Append;  // El unico registro de Detalle

     if FAgente.SubTotales then
        InitCuantosSubTot;

     with FQuery do
     begin
       // No se necesita: First;
       while not Eof do
       begin
          if ( not FAgente.FiltroCliente ) or ( CumpleFiltroCliente ) then
          begin
            if ( not lOptimiza ) then
                DataSetReporte.Append;


            // Si el # de registros que van es mayor que los amparados
            // por el 'Cuantos' del SubTotal activo, cambia de Subtotal
            // antes de evaluar las expresiones.
            if ( FAgente.SubTotales ) then
                RevisaCuantosSubTot;

            with FAgente do
              for i := 0 to NumColumnas-1 do
              begin
                oColumna := GetColumna( i );
                with oColumna do
                  if not EsConstante
                     {$ifdef BOSE}
                     AND not UsaSubTotal
                     {$endif}
                     then
                  begin
                       if RastreaColumnaCalculada then ColumnaCalculada:=i;

                       oCampo := DataSetReporte.Fields[ PosCampo ];
                       if ( DeDiccionario ) then
                          oCampo.Value := FieldByName( Formula ).Value
                       else if ( oCampo.DataType = ftBlob ) then
                       begin
                          if Assigned( FOnEvaluaColumna ) then
                              FOnEvaluaColumna( FQuery, GetColumna( i ), oCampo )
                          else
                              oCampo.AsString := FieldByName( Formula ).AsString
                       end
{
                       // Si la f�rmula contiene SubTotal(), no tiene
                       // caso evaluarla en la primera pasada
                       else if ( SubTotal ) then
                           oCampo.Value := 0
}
                       else
                           with TZetaEvaluador( FEvaluadores[ i ] ).Value do
                             case Kind of
                               resInt   : oCampo.AsInteger := intResult;
                               resDouble: if ( resFecha ) then oCampo.AsDateTime := dblResult
                                          else oCampo.AsFloat   := dblResult;
                               resString: oCampo.AsString := strResult;
                               resBool  : if ( booResult ) then oCampo.AsString := K_BOOLEANO_SI
                                          else oCampo.AsString := K_BOOLEANO_NO;
                               else
                                  DataBaseError( DesarmaErrorEvaluador(  strResult ).ErrCompleto );
                             end;
                  end;
               end;
            // En el primer Registro no se hace Corte de Grupo
            if ( lPrimero ) then
            begin
                lPrimero  := FALSE;
                lHayCorte := FALSE;
                FCorteGrupo := 0;

                if ( lOptimiza ) then
                    AgregaRegistroTotales;  // El primer registro de Totales

                LeeGrupos;
            end
            else
                lHayCorte := CorteGrupo;

            // Si hay Corte de Grupo, se hace RESET a Aggregates
            // y se vuelven a calcular
            if ( lGrupos ) and ( lHayCorte ) then
                ResetAggregates;

            if ( not lOptimiza ) then
                DataSetReporte.Post;

            // En este punto, ya est� listo el registro Detalle
            // Se puede pasar a Totales.
            if ( lOptimiza ) then
            begin
              if ( lHayCorte ) then
              begin
                  FCorteGrupo := iMin(FCorteGrupo,FNumTotalesGrupos);
                  AsignaResultado(FCorteGrupo, FALSE);
                  AgregaRegistroTotales;
                  FCorteGrupo := 0;

              end;
              if FHayAggregates then
                LeeAggregates;
              AcumulaTotales;
            end;

            if ( lHayCorte ) then
            begin
                 LeeGrupos;
                 {$ifdef BOSE}
                 if FAgente.SubTotales then
                    DatasetSubtotales.Next;
                 {$else}
                 {$endif}
            end;

            {$ifdef BOSE}
            if FAgente.SubTotales then
            begin
                 //Se evaluan las columnas que usan SubTotales;
                 DataSetReporte.Edit;
                 with FAgente do
                   for i := 0 to NumColumnas-1 do
                   begin
                     oColumna := GetColumna( i );
                     with oColumna do
                       if UsaSubTotal then
                       begin
                            if RastreaColumnaCalculada then ColumnaCalculada:=i;

                            oCampo := DataSetReporte.Fields[ PosCampo ];
                                with TZetaEvaluador( FEvaluadores[ i ] ).Value do
                                  case Kind of
                                    resInt   : oCampo.AsInteger := intResult;
                                    resDouble: if ( resFecha ) then oCampo.AsDateTime := dblResult
                                               else oCampo.AsFloat   := dblResult;
                                    resString: oCampo.AsString := strResult;
                                    resBool  : if ( booResult ) then oCampo.AsString := K_BOOLEANO_SI
                                               else oCampo.AsString := K_BOOLEANO_NO;
                                    else
                                       DataBaseError( DesarmaErrorEvaluador(  strResult ).ErrCompleto );
                                  end;
                       end;
                    end;
                    DataSetReporte.Post;
            end;
            {$else}
            {$endif}
          end;
          Next;
       end;
     end;
     if ( lOptimiza ) then
     begin
        AsignaResultado(0, TRUE);     // Totaliza el �ltimo registro
        DataSetReporte.Cancel;
     end;
end;

procedure TdmSuperReporte.CierraDataSet;
begin
     FQuery.Close;
end;

function TdmSuperReporte.Construye( Agente: TSQLAgente; const lTotales : Boolean; const lTotalesGrupos : Boolean ): Boolean;
begin
     try
        FTotalizando   := FALSE;
        FTotalesGrupos := lTotalesGrupos;
        Prepara( Agente, lTotales );
        // La primera pasada supone orden del Query y no del ClientDataSet
        if ( FAgente.SubTotales ) and ( FAgente.OrdenCliente ) then
            raise Exception.Create( 'No se permiten funciones de Totales en Reportes ordenados con F�rmulas' );
        Result := AbreDataSet;
        if Result then
        begin
             // Si ya se hizo en la primera pasada, no es necesario totalizar
             if ( FTotales ) and ( not FOptimizaTotales ) then
             begin
                 Agente.OrdenaDataSet( DataSetReporte );
                 TotalizaDataSet;
             end;
        end;
        // Libera Memoria en versi�n Profesional
        LimpiaEvaluadores;
     except
        on E : Exception do
        begin
             if DataSetReporte.Active then
                DataSetReporte.EmptyDataSet;
             ErrorGeneral( E.Message );
             Result := FALSE;
        end;
     end;
end;

procedure TdmSuperReporte.CreaCampos;
var
   i : Integer;
   sFieldName : String;
begin
     with DataSetReporte, FAgente do
     begin
       InitTempDataset;
       for i := 0 to NumColumnas-1 do
       begin
         with GetColumna( i ) do
           if not EsConstante
              {$ifdef BOSE}
              or SubTotales
              {$endif}
              then
           begin
             if Length( Alias ) = 0 then
                sFieldName := 'COLUMNA' + IntToStr( i )
             else
                 sFieldName := Alias;

             case TipoFormula of
               tgBooleano : AddStringField( sFieldName, 2 );  { SI/NO }
               tgFloat    : AddFloatField( sFieldName );
               tgNumero   : AddIntegerField( sFieldName );
               tgFecha    : AddDateField( sFieldName );
               tgTexto    : if ( Ancho = 0 ) then
                            begin
                                 if (Pos(UnaComilla, Formula)=1) OR
                                    (Pos(Comilla, Formula)=1) then
                                    AddStringField( sFieldName, Max(Length(Formula)-1,K_ANCHO_TEXTO))
                                 else AddStringField( sFieldName, K_ANCHO_TEXTO )
                            end
                            else
                              AddStringField( sFieldName, Ancho );
               tgMemo     : AddBlobField( sFieldName, 0 );
               else       DatabaseError( 'No se permiten Tipos Autom�ticos' );
             end; { case }
             PosCampo := FieldDefs.Count-1;
           end; { if not EsConstante }
       end;  { for }
       CreateTempDataset;
     end;
end;

procedure TdmSuperReporte.CreaEvaluadores;
var
   i : Integer;
   oEvaluador : TZetaEvaluador;
begin
     LimpiaEvaluadores;

     with FAgente do
     begin
       // Evaluadores de Columnas
       for i := 0 to NumColumnas-1 do
       begin
         with GetColumna( i ) do
         begin
            if ( not DeDiccionario ) then
            begin
                 oEvaluador := TZetaEvaluador.Create(oZetaCreator);
                 oEvaluador.Entidad := FAgente.Entidad;
                 oEvaluador.AgregaDataSet( FQuery );
                 oEvaluador.Prepare( Formula );
                 if ( EsConstante ) then
                 begin
                    ValorConstante := ZEvaluador.ResToString( oEvaluador.Value );
                    FreeAndNil( oEvaluador );
                 end
                 else if ( oEvaluador.IsAggreg ) then
                    AddAggregate( DataSetReporte.Fields[ PosCampo ], oEvaluador, Banda );
            end
            else
                // Se agregan NILs para conservar las posiciones
                // del arreglo paralelo a las GetColumna( i )
                oEvaluador := NIL;
            FEvaluadores.Add( oEvaluador );

            if ( GroupExp ) then
                AddGrupo( DataSetReporte.Fields[ PosCampo ] );
         end;
       end;

       // Evaluadores de Filtros
       // Un poco diferente.
       // SOLO se agregan los filtros de Cliente. NO es necesario el arreglo paralelo
       if FiltroCliente then
         for i := 0 to NumFiltros-1 do
           with GetFiltro( i ) do
           begin
              if ( not DeDiccionario ) then
              begin
                   oEvaluador := TZetaEvaluador.Create(oZetaCreator);
                   oEvaluador.Entidad := FAgente.Entidad;
                   oEvaluador.AgregaDataSet( FQuery );
                   oEvaluador.Prepare( Formula );
                   FEvaluadoresFiltro.Add( oEvaluador );
              end;
           end;

     end;
end;


procedure TdmSuperReporte.EjecutaQuery;
begin
     FQuery.Open;
end;

function TdmSuperReporte.GetReporte: Variant;
begin
    if FTotales then
        Result := DataSetTotales.Data
    else
        Result := DataSetReporte.Data;
end;

function TdmSuperReporte.GetResult(iResult: Integer): TPesos;
begin
     try
        if FTotalizando then
            Result := DataSetTotales.FieldByName( K_TOTAL_NAME + IntToStr( iResult-1 )).AsFloat
        else
          with FAgente.GetColumna( iResult-1 ) do
            if EsConstante then
                Result := StrToFloat( ValorConstante )
            else
                Result := DataSetReporte.Fields[ PosCampo ].AsFloat;
     except
        Result := 0;
     end;
end;

procedure TdmSuperReporte.LimpiaEvaluadores;

  procedure LimpiaLista( oLista : TList );
  var
     i : Integer;
     oEvaluador : TZetaEvaluador;
  begin
     with oLista do
     begin
          for i := Count-1 downto 0 do
          begin
              oEvaluador := TZetaEvaluador( Items[ i ] );
              // Las columnas de diccionario no tienen Evaluador
              if ( oEvaluador <> NIL ) then
                 oEvaluador.Free;
          end;
          Clear;
     end;
  end;

  procedure LimpiaAggregates;
  var
    i : Integer;
  begin
      with FListaAggregates do
      begin
        for i := Count-1 downto 0 do
            TColumnaAggregate( Items[ i ] ).Free;
        Clear;
      end;

      FHayAggregates := FALSE;
  end;

  procedure LimpiaGrupos;
  var
    i : Integer;
  begin
      with FListaGrupos do
      begin
        for i := Count-1 downto 0 do
            TColumnaGrupo( Items[ i ] ).Free;
        Clear;
      end;

      FNumGrupos := 0;
  end;

  procedure LimpiaTotales;
  var
    i : Integer;
  begin
      with FListaTotales do
      begin
        for i := Count-1 downto 0 do
            TTotaliza( Items[ i ] ).Free;
        Clear;
      end;
      FNumTotales := 0;
      FNumTotalesGrupos := 0;
  end;

  procedure LimpiaSubTotales;
  var
    i : Integer;
  begin
      FPointerSubTot := -1;
      FSubTotal := NIL;
      with FListaSubTot do
      begin
        for i := Count-1 downto 0 do
            TSubTotal( Items[ i ] ).Free;
        Clear;
      end;
  end;

begin  // LimpiaEvaluadores
       LimpiaLista( FEvaluadores );
       LimpiaLista( FEvaluadoresFiltro );
       LimpiaAggregates;
       LimpiaGrupos;
       LimpiaTotales;
       LimpiaSubTotales;
       FListaAlmacenan.Clear;   // No es due�a de los objetos
end;

function TdmSuperReporte.HayTotales : Boolean;
var
    i : Integer;
begin
    Result := FALSE;
    with FAgente do
        for i := 0 to NumColumnas-1 do
            if ( GetColumna( i ).Totalizacion <> ocNinguno ) then
            begin
                Result := TRUE;     // A la primera que encuentra, interrumpe la b�squeda
                Exit;
            end;
end;

function TdmSuperReporte.Prepara(Agente: TSQLAgente; const lTotales : Boolean ): Boolean;
begin
     FTotales := lTotales;
     FAgente := Agente;
     //SubTotal, descomentar
     FOptimizaTotales := not FAgente.OrdenCliente;
     if ( lTotales ) and not HayTotales then
       DatabaseError( 'No hay Columnas por Totalizar' );
     CreaCampos;
     oZetaProvider.PreparaQuery( FQuery, FAgente.SQL.Text );
     Result := TRUE;
end;



procedure TdmSuperReporte.SetParamValue(const sNombre: String;
  const Tipo : eTipoGlobal; const Valor: Variant);
begin
     // eTipoGlobal =(tgBooleano,tgFloat,tgNumero,tgFecha,tgTexto,tgMemo,tgAutomatico);
     with oZetaProvider do
       case Tipo of
         tgBooleano : ParamAsBoolean( FQuery, sNombre, Valor );
         tgFloat    : ParamAsFloat(   FQuery, sNombre, Valor );
         tgNumero   : ParamAsInteger( FQuery, sNombre, Valor );
         tgFecha    : ParamAsDate(    FQuery, sNombre, Valor );
         tgMemo     : ParamAsBlob(    FQuery, sNombre, Valor );
         else
                      ParamAsString(  FQuery, sNombre, Valor );
       end;
end;

procedure TdmSuperReporte.CopiaCampos;
begin
  with DataSetTotales do
  begin
      InitTempDataSet;
      FieldDefs.Assign( DataSetReporte.FieldDefs );
  end;
end;

procedure TdmSuperReporte.CreaListaTotales;
var
    i, nFieldCount, nColumnas : Integer;
    {$ifdef BOSE}
    j:integer;
    {$endif}
    oColumna : TSQLColumna;
    sTotal : String;

begin
    //nColumnas := DataSetReporte.FieldCount; //codigo original
    //CV:
    nFieldCount :=  DataSetReporte.FieldCount;
    nColumnas := FAgente.NumColumnas;
    if FTotalesGrupos then FNumTotalesGrupos := FNumGrupos
    else FNumTotalesGrupos := 0;
    FNumTotales := 0;
    with FAgente do
    begin
        {$ifdef BOSE}
        for i := 0 to FNumGrupos-1 do
        begin
             DataSetSubTotales.AddStringField(K_GRUPO_NAME + IntToStr(i) ,30);
             DataSetSubTotales.AddFloatField( K_CUANTOS_NAME + IntToStr( i ) );
        end;
        {$else}
        {$endif}
        for i := 0 to nColumnas-1 do
        begin
            oColumna  := GetColumna( i );
            if ( oColumna.Totalizacion <> ocNinguno ) then
            begin
                sTotal := K_TOTAL_NAME + IntToStr( i );
                with DataSetTotales do
                  if ( oColumna.TipoFormula = tgFecha ) and ( oColumna.Totalizacion in [ ocPromedio, ocMin, ocMax ] ) then
                  begin
                    AddDateField( sTotal );
                    {$ifdef BOSE}
                    for j := 0 to FNumGrupos-1 do
                    begin
                         if oColumna.LugarSubTot>=0 then
                            DataSetSubTotales.AddDateField( sTotal + '_' + IntToStr( j ) );
                    end;
                    {$else}
                    {$endif}
                  end
                  else
                  begin
                       AddFloatField( sTotal );
                       {$ifdef BOSE}
                       for j := 0 to FNumGrupos-1 do
                       begin
                            if oColumna.LugarSubTot>=0 then
                               DataSetSubTotales.AddFloatField( sTotal + '_' + IntToStr( j ));
                       end;
                       {$else}
                       {$endif}
                  end;
                oColumna.Totalizador := FListaTotales.Add( TTotaliza.Create( DataSetReporte, nFieldCount + FNumTotales, oColumna, FEvaluadores[ i ], self ));
                Inc( FNumTotales );
            end;
        end;
    end;
end;


procedure TdmSuperReporte.AgregaRegistroTotales;
begin
    DataSetTotales.Append;
    CopiaRegistro;
end;

procedure TdmSuperReporte.CopiaRegistro;
var
    i : Integer;
begin
    with FAgente do
      for i := DataSetReporte.FieldCount-1 downto 0 do
        DataSetTotales.Fields[ i ].Value := DataSetReporte.Fields[ i ].Value;
end;

procedure TdmSuperReporte.AcumulaTotales;
var
    i : Integer;
begin
    for i := FNumTotales-1 downto 0 do
      TTotaliza( FListaTotales.Items[ i ] ).Acumula(FCorteGrupo);
end;

{$ifdef BOSE_TEMP}
procedure TdmSuperReporte.AcumulaSubTotales;
 var sField, sGrupo,sGrupoActual{, sGrupoName}: string;
     i, {j,} k, iGrupos: integer;
     oColumna: TSQLColumna;
     rTotal: TPesos;
     //lYaHayGrupoActual: Boolean;
begin
     if FAgente.SubTotales then
     begin
          iGrupos := fNumGrupos-1;
          //Solo quiero acumular cuando tengo 3 o mas grupos.
          //Cuando tengo 1 grupo no necesito acumular los datos.
          //Tampoco se necesita acumular el ultimo grupo.
          while iGrupos >= 1 do
          begin
               with DatasetSubTotales do
               begin
                    Last;
                    repeat
                         sGrupo := VACIO;
                         for k := 0 to iGrupos -1 do
                             sGrupo := sGrupo +';' + FieldByName( K_GRUPO_NAME + IntToStr(k) ).AsString;

                         Prior;
                         sGrupoActual:= VACIO;
                         for k := 0 to iGrupos - 1 do
                             sGrupoActual := sGrupoActual +';' + FieldByName( K_GRUPO_NAME + IntToStr(k) ).AsString;
                         Next;

                         for i:= 0 to FListaAlmacenan.Count - 1 do
                         begin
                              oColumna := FListaAlmacenan.Items[i];
                              sField := K_TOTAL_NAME + IntToStr( oColumna.PosCampo ) + '_' + IntToStr( iGrupos - 1 ) ;

                              rTotal := FieldByName( sField ).AsFloat;

                              Prior;

                              if sGrupo = sGrupoActual then
                              begin
                                   Edit;
                                   FieldByName( sField ).AsFloat := rTotal;
                                   Post;
                                   {$ifdef CAROLINA}
                                   DataSetSubTotales.SaveToFile('d:\temp\subt.cds');
                                   {$endif}
                              end;
                              if (i < FListaAlmacenan.Count) then
                                  Next;
                         end;
                    until RecNo > 0 ;
               end;
               Dec(iGrupos);
          end;

     end;
end;
{$else}
{$endif}
procedure TdmSuperReporte.LeeAggregates;
var
    i : Integer;
begin
    with FListaAggregates do
      for i := 0 to Count-1 do
        with TColumnaAggregate( Items[ i ] ) do
            LeeCampo;
end;

procedure TdmSuperReporte.CopiaAggregates;
var
    i : Integer;
begin
    with FListaAggregates do
      for i := 0 to Count-1 do
        with TColumnaAggregate( Items[ i ] ) do
            DataSetTotales.Fields[ FCampo.Index ].Value := FValor;
end;

procedure TdmSuperReporte.AsignaResultado(const nCorte: integer; const lFinal : Boolean);
var
    j,i : Integer;
begin
     FTotalizando := TRUE;
     // Para determinar que hubo Corte de Grupo se tiene que haber
     // calculado el primer registro del siguiente grupo.
     // Si hay funciones 'SobreTotales' que usan 'SubTotal', se
     // tiene que posicionar el apuntador de 'SubTotales' en el
     // grupo anterior, que es el que se va a AsignarResultado.
     // Si ya se acabaron los registros del Query 'lFinal',
     // ya no es necesario regresarlo pues se encuentra en el �ltimo
     if ( not lFinal ) and ( FAgente.SubTotales ) then
        RegresaSubTot;
     for j:=FNumTotalesGrupos downto nCorte do
     begin
          if DataSetTotales.State = dsBrowse then
             DataSetTotales.Append;

          for i := 0 to FNumTotales-1 do
              TTotaliza( FListaTotales.Items[ i ] ).AsignaResultado( DataSetTotales, j );

          if FHayAggregates then
             CopiaAggregates;

          DataSetTotales.Post;
     end;
     if ( not lFinal ) and ( FAgente.SubTotales ) then
        AdelantaSubTot; // Deja el apuntador en el grupo nuevo
     FTotalizando := FALSE;
end;

function TdmSuperReporte.TotalizaDataSet: Boolean;
begin  // TotalizaDataSet;

    if ( FNumTotales > 0 ) then
    begin
      AgregaRegistroTotales; // El primer registro
      LeeGrupos;
      if FAgente.SubTotales then
        InitCuantosSubTot;
      with DataSetReporte do
      while not Eof do
      begin
          if FAgente.SubTotales then
            RevisaCuantosSubTot;

          if CorteGrupo then
          begin
               FCorteGrupo := iMin(FCorteGrupo,FNumTotalesGrupos);
               AsignaResultado(FCorteGrupo, FALSE);
               AgregaRegistroTotales;
               LeeGrupos;
               FCorteGrupo := 0;
          end;

          if FHayAggregates then
            LeeAggregates;

          AcumulaTotales;
          Next;
      end;
      AsignaResultado(0, TRUE);
      Result := TRUE;
    end
    else
        Result := FALSE;
end;

{ TTotaliza }

function TTotaliza.ValorNumero: TPesos;
begin
    if ( FCampo = NIL ) then
        Result := FConstante
    else
        Result := FCampo.AsFloat;
end;

{
function TTotaliza.BooleanoVerdadero : Boolean;
begin
    if ( FCampo = NIL ) then
        Result := ( FConstante > 0 )
    else
        Result := ( FCampo.AsString = K_BOOLEANO_SI );

end;
}

procedure TTotaliza.Acumula(const nCorte:integer);
var
    i : integer;
begin
     for i := nCorte to FNumTotGrupos do
     begin
          try
             //if ( FTipoFormula <> tgBooleano ) or BooleanoVerdadero then
             Inc( FNumero[i] );

            case FCriterio of
                 {ocCuantos : //if ( FTipoFormula <> tgBooleano ) or BooleanoVerdadero then
                             Inc( FNumero[i] );}
                 ocSuma    : FSuma[i] := FSuma[i] + ValorNumero;
                 ocPromedio: begin
//                                 Inc( FNumero[i] );
                                 FSuma[i] := FSuma[i] + ValorNumero;
                             end;
                 ocMin     : if ValorNumero < FSuma[i] then
                                 FSuma[i] := ValorNumero;
                 ocMax     : if ValorNumero > FSuma[i] then
                                 FSuma[i] := ValorNumero;
                 ocRepite  : FSuma[i] := ValorNumero;
            end;
          except
              // Por si quieren totalizar campos que no tienen un AsFloat v�lido
              FSuma[nCorte]   := 0;
              FNumero[nCorte] := 0;
          end;
     end;
end;

procedure TTotaliza.AsignaResultado(oDataSet: TDataset;const nCorte: integer);
begin
    oDataSet.Fields[ FPosTotal ].AsFloat := Resultado(nCorte);
    Inicializa(nCorte);
end;

constructor TTotaliza.Create(oDataset : TDataset; const iTotal : Integer; oColumna : TSQLColumna; Evaluador : TZetaEvaluador; oSuperReporte : TDataModule );
begin
    with oColumna do
    begin
        if EsConstante then
        begin
            if ( TipoFormula = tgBooleano ) then
            begin
                if ValorConstante = K_BOOLEANO_SI then
                    FConstante := 1
                else
                    FConstante := 0;
            end
            else
              try
                    FConstante := StrToCurr( ValorConstante );
              except
                    FConstante := 0;
              end;
        end
        else
            FCampo := oDataset.Fields[ PosCampo ];

        FPosTotal    := iTotal;
        FCriterio    := Totalizacion;
        FTipoFormula := TipoFormula;
        FEvaluador   := Evaluador;
        FSuperReporte := oSuperReporte;
        // FNumTotGrupos   := nGrupos;
        // Limpia( 0, K_MAX_GRUPOS );
    end;
end;


procedure TTotaliza.SetMaxGrupos( const nMaxGrupos : Integer );
begin
    FNumTotGrupos := nMaxGrupos;
    Inicializa( 0 );
end;

procedure TTotaliza.Inicializa(const nCorte: integer);
var
    i : Integer;
begin
     for i := nCorte to FNumTotGrupos do
     begin
          FNumero[i] := 0;
          if ( FCriterio = ocMin ) then
             FSuma[i] := IntPower( 10, 10 )
          else if ( FCriterio = ocMax ) then
              FSuma[i] := -IntPower( 10, 10 )
          else
              FSuma[i] := 0;
     end;
end;


function TTotaliza.Resultado(const nCorte: integer): TPesos;
begin
    case FCriterio of
        ocCuantos : Result := FNumero[nCorte];
        ocPromedio: if ( FNumero[nCorte] > 0 ) then
                        Result := FSuma[nCorte] / FNumero[nCorte]
                    else
                        Result := 0;
        ocSobreTotales :
        begin
             if FEvaluador = NIL then Result := 0
             else
                 with FEvaluador.Value do
                      case Kind of
                        resInt   : Result := intResult;
                        resDouble: Result := dblResult;
                        resString: Result := 0;
                        resBool  : if ( booResult ) then Result := 1
                                   else Result := 0;
                        else
                           Result := 99999;
                         // DataBaseError( DesarmaErrorEvaluador(  strResult ).ErrCompleto );
                      end;
        end
        else
            Result := FSuma[nCorte];
    end;
end;



procedure TdmSuperReporte.AddAggregate(oCampo: TField;
  oEvaluador: TZetaEvaluador; const iBanda : Integer);
var
    oColumnaAgg : TColumnaAggregate;
begin
    oColumnaAgg := TColumnaAggregate.Create;
    oColumnaAgg.FCampo     := oCampo;
    oColumnaAgg.FEvaluador := oEvaluador;
    oColumnaAgg.FBanda     := iBanda;
    FListaAggregates.Add( oColumnaAgg );
    FHayAggregates := TRUE;
end;

procedure TdmSuperReporte.AddGrupo(oCampo : TField);
var
    oColumnaGrupo : TColumnaGrupo;
begin
    oColumnaGrupo := TColumnaGrupo.Create;
    oColumnaGrupo.FCampo := oCampo;
    FListaGrupos.Add( oColumnaGrupo );
    FNumGrupos := FListaGrupos.Count;
end;

function TdmSuperReporte.CorteGrupo : Boolean;
var
    i : integer;
    oGrupo : TColumnaGrupo;
begin
    Result := FALSE;
    FCorteGrupo := 0;   // Grupo de Empresa
    if ( FNumGrupos > 0 ) then
       with FListaGrupos do
       begin
            for i := 0 to FNumGrupos-1 do
            begin
                 oGrupo := TColumnaGrupo( Items[ i ] ) ;
                 with oGrupo do
                 begin
                    if FCampo.Value <> FValor then
                    begin
                        Result := TRUE;
                        FCorteGrupo := i + 1;  // Los grupos se numeran de 1..n
                        break;
                    end;
                end;
            end;
       end;
end;

procedure TdmSuperReporte.LeeGrupos;
var
    i : Integer;
begin
    with FListaGrupos do
      for i := 0 to FNumGrupos-1 do
        TColumnaGrupo( Items[ i ] ).LeeCampo;
end;

procedure TdmSuperReporte.ResetAggregates;
var
    i : Integer;
begin
    if FHayAggregates then
      with FListaAggregates do
        for i := 0 to Count-1 do
          with TColumnaAggregate( Items[ i ] ) do
            if ( FBanda < 0 ) or ( FBanda >= FCorteGrupo ) then
            begin
              FEvaluador.Reset;
              CalculaCampo;
            end;
end;

{$ifdef BOSE}
function TdmSuperReporte.GetSubTotal( const iResult, iCorte: Integer): TPesos;
var
    nPos, i : Integer;
    oColumna : TSQLColumna;
    sField , sFiltro, sGrupo: string;
    oBookMark : TBookmark;
begin
    try
        oColumna := FAgente.GetColumna( iResult-1 );
        // Por si llaman SubTot( 'x' ) donde 'x' no existe
        if ( oColumna = NIL ) then
            nPos := -1
        else
            nPos := oColumna.Totalizador;
        if ( nPos < 0 ) then
            Result := 0
        else if iCorte = 0 then
            Result := FGranTotal.FValores[ oColumna.LugarSubTot ]
        else
        begin
             if ( oColumna.Totalizacion in [ocCuantos{, ocPromedio}] ) then
             begin
                  //Primero se obtiene el numero de registro del SubTotal Solicitado.
                  { ** }
                  Result := GetCuantosSubTotal( iCorte );
                  { ** }
             end
             else //Totalizacion Suma, Minimo, Maximo
             begin
                  sField := K_TOTAL_NAME + IntToStr(oColumna.PosCampo) +'_'+ IntToStr(iCorte-1);

                  with DataSetSubTotales do
                  begin
                       oBookMark := GetBookmark;
                       try
                          sFiltro := VACIO;
                          for i:= 0 to iCorte-1 do
                          begin
                               sGrupo := K_GRUPO_NAME + IntToStr(i);
                               sFiltro := ConcatFiltros( sFiltro, sGrupo + '=' + EntreComillas( DataSetSubTotales.FieldByName(sGrupo).AsString ) )
                          end;
                          Filter := sFiltro;
                          Filtered := sFiltro <> '';
                          Last;

                          if ( FindField( sField ) <> NIL ) then
                               Result := FieldByName( sField ).AsFloat
                          else
                              Result := 0;
                       finally
                              Filtered := FALSE;
                              GotoBookMark( oBookMark );
                              FreeBookMark( oBookMark );
                       end;

                       {** Totalizacion para el Promedio **}
                       if ( oColumna.Totalizacion = ocPromedio ) then
                       begin
                            Result := Result / GetCuantosSubTotal( iCorte );
                       end;
                  end;
             end;
        end;
    except
        Result := 0;
    end;
end;

{$else}
{.$ifdef ANTES -CV: error funcion subtotal}
function TdmSuperReporte.GetSubTotal( const iResult, iCorte: Integer): TPesos;
var
    nPos : Integer;
    oColumna : TSQLColumna;
    //oTotaliza : TTotaliza;
begin
    try
        oColumna := FAgente.GetColumna( iResult-1 );
        // Por si llaman SubTot( 'x' ) donde 'x' no existe
        if ( oColumna = NIL ) then
            nPos := -1
        else
            nPos := oColumna.LugarSubTot;
        if ( nPos < 0 ) then
            Result := 0
        else if iCorte = 0 then
            Result := FGranTotal.FValores[ nPos ]
        else
            Result := FSubTotal.FValores[ nPos ]
    except
        Result := 0;
    end;
end;

{$endif}

{$ifdef BOSE}
function TdmSuperReporte.GetCuantosSubTotal( const iCorte: Integer): Integer;
 var
    sGrupo, sField: string;
    bPosicion : TBookMark;
    j: integer;
begin
    if ( iCorte = 0 ) then
        Result := FGranTotal.Cuantos
    else
    begin
         sField := K_CUANTOS_NAME + IntToStr(iCorte-1);
         with DataSetSubTotales do
         begin
              if ( FindField( sField ) <> NIL ) then
              begin
                   bPosicion := GetBookMark;
                   try
                      for j:= 0 to iCorte -1 do
                      begin
                           with FListaGrupos do
                                with TColumnaGrupo( Items[ j ] ) do
                                    begin
                                         sGrupo := String( FValor );
                                    end;
                           Filter :=  ConcatFiltros( Filter, K_GRUPO_NAME + IntToStr(j)+ '='+ EntreComillas(sGrupo) );
                      end;
                      Filtered := TRUE;
                      Last;
                      Result := Trunc( FieldByName(sField).AsFloat );

                      Filter := VACIO;
                      Filtered:= FALSE;

                      GotoBookMark( bPosicion );

                   finally
                          FreeBookMark( bPosicion );
                   end;
              end
              else
                  Result := 0;
         end;
    end;
end;
{$else}
function TdmSuperReporte.GetCuantosSubTotal( const iCorte: Integer): Integer;
begin
    if ( iCorte = 0 ) then
        Result := FGranTotal.Cuantos
    else
        Result := FSubTotal.Cuantos;
end;
{$endif}
{ TColumnaAggregate }

procedure TColumnaAggregate.CalculaCampo;
begin
   with FEvaluador.Value do
     case Kind of
        resInt   : FCampo.AsInteger := intResult;
        resDouble: if ( resFecha ) then FCampo.AsDateTime := dblResult
                   else FCampo.AsFloat   := dblResult;
        resString: FCampo.AsString := strResult;
        resBool  : if ( booResult ) then FCampo.AsString := K_BOOLEANO_SI
                   else FCampo.AsString := K_BOOLEANO_NO;
     end;
end;

procedure TColumnaAggregate.LeeCampo;
begin
   // Guarda el ULTIMO Valor Calculado para usarlo en SOLO TOTALES
    FValor := FCampo.Value;
end;

{ TColumnaGrupo }

procedure TColumnaGrupo.LeeCampo;
begin
    FValor := FCampo.Value;
end;

{ TSubTotal }


procedure TSubTotal.Almacena(nPos : Integer; nValor : TPesos);
begin
    FValores[ nPos ] := nValor;
end;

constructor TSubTotal.Create;
var
    i : Integer;
begin
    for i := 0 to K_MAX_SUB_TOT do
            FValores[ i ] := 0;
    FCuantos := 0;
    FAcumulado := 0;
end;




end.
