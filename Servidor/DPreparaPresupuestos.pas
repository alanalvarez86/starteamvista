unit DPreparaPresupuestos;

interface

uses
  System.SysUtils, System.Classes, ZetaCommonClasses, DZetaServerProvider, ZetaCommonTools,
  DBClient, Data.Win.ADODB, ZetaServerDataSet, Data.DB, Variants, ZetaRegistryServer, ZetaServerTools;

type
  TShowProgress = function( const sMensaje: String ): Boolean of object;
  TdmPreparaPresupuestos = class(TDataModule)
    cdsTabla: TServerDataSet;
  protected
    function GetServer: WideString; safecall;
    function GetSQLUserName: WideString; safecall;
    function GetSQLUserPassword: WideString;
  private
    { Private declarations }
    function BuildEmpresa(DataSet: TZetaCursor): OleVariant;
    function GetScript( const iScript: Integer ): String;
    function GetNuevoProvider (sUbicacion: WideString; iQuery: Integer; sUsuario: WideString;
                                               sPassword: WideString; oZetaProvider: TdmZetaServerProvider): TdmZetaServerProvider;
    procedure LeerTablaDesdeBD (tabla, sBaseDatosBaseCodigo: String; oZetaProvider: TdmZetaServerProvider; bEmpleados: Boolean = FALSE);
    procedure procesaTablaPresupuestos (tabla, sBaseDatosBaseCodigo: String;
                                        oZetaProvider: TdmZetaServerProvider; bEmpleados: Boolean = FALSE);
    procedure LeeCatTabPresupuestos(const sBaseDatosBaseCodigo: string;  lConservarConfNomina: boolean;
        oZetaProvider: TdmZetaServerProvider);
    procedure LeeTabPresupuestosEmpleados(const sBaseDatosBaseCodigo: string;
        oZetaProvider: TdmZetaServerProvider);
    procedure MigrarKARFIJA (sBaseDatosBaseCodigo: String; oProvider: TdmZetaServerProvider);

  public
    { Public declarations }
    function PreparacionPresupuestos(iYear : Integer; dFechaEmpAlta : TDateTime ;
    dFechaEmpBaja : TDateTime; lNominaMensual, lConservarConfNomina: Boolean;
    oProvider: TdmZetaServerProvider; sDatabaseBase, sDatabaseDestino: String): Boolean;
    function OptimizacionPresupuestos(Parametros: OleVariant; oProvider: TdmZetaServerProvider): Boolean;
    function PresupuestosCatalogos(Parametros: OleVariant; oZetaProvider: TdmZetaServerProvider): WordBool;
    function PresupuestosEmpleados(Parametros: OleVariant; oZetaProvider: TdmZetaServerProvider; sBaseDatosBaseNombre: String): WordBool;
  end;

var
  dmPreparaPresupuestos: TdmPreparaPresupuestos;

  aCatTabPresupuestos: array[ 0..60 ] of String =
       ('RSOCIAL', 'RPATRON', 'NUMERICA', 'T_ART_80',
        'ART_80', 'GLOBAL', 'CONCEPTO', 'TURNO',
        'CLASIFI', 'CONTRATO', 'ENTIDAD', 'FESTIVO',
        'HORARIO', 'LEY_IMSS', 'MOT_BAJA', 'NIVEL1',
        'NIVEL2', 'NIVEL3', 'NIVEL4', 'NIVEL5',
        'NIVEL6', 'NIVEL7', 'NIVEL8', 'NIVEL9',
        'NOMPARAM', 'OTRASPER', 'SSOCIAL', 'PRESTACI',
        'TPRESTA', 'REGLAPREST', 'PRESTAXREG', 'PRIESGO',
        'PUESTO', 'QUERYS', 'RIESGO', 'SAL_MIN',
        'TCAMBIO', 'TKARDEX', 'TPERIODO', 'TALLA',
        'TOOL', 'MOT_TOOL', 'PTOTOOLS', 'PTOFIJAS',
        'PLAZA', 'R_CLASIFI', 'R_MODULO', 'R_ENTIDAD',
        'R_ATRIBUTO', 'R_RELACION', 'R_CLAS_ENT', 'R_FILTRO',
        'R_DEFAULT', 'R_ORDEN', 'R_MOD_ENT', 'R_LISTAVAL',
        'R_VALOR', 'REPORTE', 'CAMPOREP', 'TAHORRO',
        'VALOR_UMA');

  aTabPresupuestosColabora: array[ 0..7 ] of String =
       ('COLABORA', 'ACUMULA', 'AHORRO', 'ACAR_ABO',
       'PRESTAMO', 'PCAR_ABO', 'VACACION', 'KAR_TOOL');

  aTabPresupuestosFiltrosColabora: array[ 0..5 ] of String =
       ('(CB_ACTIVO=''S'' and CB_FEC_ING<''%s'') or (CB_ACTIVO=''N'' and CB_FEC_BAJ>= ''%s'')',
       'AC_YEAR = %d',
       'AH_STATUS = 0',
       'CB_CODIGO in (SELECT A.CB_CODIGO FROM %0:s.dbo.AHORRO A '+
                                            'WHERE A.CB_CODIGO = %0:s.dbo.ACAR_ABO.CB_CODIGO AND ' +
                                            '      A.AH_TIPO = %0:s.dbo.ACAR_ABO.AH_TIPO AND '+
                                            '      A.AH_STATUS = 0 )',
       'PR_STATUS = 0',
       'CB_CODIGO in (SELECT A.CB_CODIGO FROM %0:s.dbo.PRESTAMO A '+
                                             'WHERE A.CB_CODIGO = %0:s.dbo.PCAR_ABO.CB_CODIGO AND '+
                                             'A.PR_TIPO = %0:s.dbo.PCAR_ABO.PR_TIPO AND '+
                                             'A.PR_REFEREN = %0:s.dbo.PCAR_ABO.PR_REFEREN AND '+
                                             'A.PR_STATUS = 0 )');

const Q_GET_BASES_DATOS_SERVER = 1;

implementation

uses MotorPatchUtils;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

function TdmPreparaPresupuestos.GetServer: WideString;
var
    FRegistry: TZetaRegistryServer;
begin
      try
        FRegistry := TZetaRegistryServer.Create ();
        Result :=  FRegistry.ServerDB;
      finally
        FreeAndNil (FRegistry);
      end;
end;

function TdmPreparaPresupuestos.GetSQLUserName: WideString;
var
   FRegistry: TZetaRegistryServer;
begin
      try
        FRegistry := TZetaRegistryServer.Create(FALSE);
        Result :=  FRegistry.UserName;
      finally
        FreeAndNil (FRegistry);
      end;
end;

function TdmPreparaPresupuestos.GetSQLUserPassword: WideString;
var
   FRegistry: TZetaRegistryServer;
begin
      try
        FRegistry := TZetaRegistryServer.Create(FALSE);
        Result :=  FRegistry.PasswordRaw;
      finally
        FreeAndNil (FRegistry);
      end;
end;

function TdmPreparaPresupuestos.BuildEmpresa(DataSet: TZetaCursor): OleVariant;
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  0,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;

function TdmPreparaPresupuestos.GetScript( const iScript: Integer ): String;
begin
  case iScript of
      Q_GET_BASES_DATOS_SERVER: Result := 'select DB_DATOS AS CM_DATOS, '' '' AS CM_NOMBRE, '' '' AS CM_ALIAS, DB_USRNAME AS CM_USRNAME, DB_PASSWRD AS CM_PASSWRD, '' '' AS CM_NIVEL0,  ''DATOS'' AS CM_CODIGO '+
                                                ' from DB_INFO WHERE DB_CODIGO = ''%s'' ';
     else
         Result := '';
     end;
end;

function TdmPreparaPresupuestos.PreparacionPresupuestos(iYear : Integer;
dFechaEmpAlta : TDateTime ; dFechaEmpBaja : TDateTime; lNominaMensual, lConservarConfNomina: Boolean;
oProvider: TdmZetaServerProvider; sDatabaseBase, sDatabaseDestino: String): Boolean;
Const K_TAM_INICIAL = 50;
      K_PORCENTAJE_INC = 0.20;
      K_PORCENTAJE_LOG = 0.25;
      Q_PRESUP_MIGRA_CATALOGOS_DELETE = 'EXECUTE SPP_MIGRACIONPRESUPUESTO_DELETE ''%s''';
begin
     with oProvider do
     begin
        ExecSQL (EmpresaActiva, Format( Q_PRESUP_MIGRA_CATALOGOS_DELETE, [zBoolToStr( lConservarConfNomina )] ));
        MigrarKARFIJA (sDatabaseBase, oProvider);
     end;
     Result := TRUE;
end;

procedure TdmPreparaPresupuestos.MigrarKARFIJA (sBaseDatosBaseCodigo: String; oProvider: TdmZetaServerProvider);
const K_TABLA_ORIGEN = 'KAR_FIJA';
      K_TABLA_DESTINO = 'KAR_FIJA2';
var iAdvertencias, iErrores: Integer;
begin
    iAdvertencias := 0;
    iErrores := 0;
    LeerTablaDesdeBD(K_TABLA_ORIGEN, sBaseDatosBaseCodigo, oProvider, FALSE);
    GrabaDataSet (cdsTabla, K_TABLA_DESTINO, VACIO, oProvider, iAdvertencias, iErrores);
end;

procedure TdmPreparaPresupuestos.LeeCatTabPresupuestos(const sBaseDatosBaseCodigo: string; lConservarConfNomina: boolean;
                      oZetaProvider: TdmZetaServerProvider);
var
   i: Integer;
begin
    // Recorrer array de aCatTabPresupuestos y procesar
    for i := ( Low( aCatTabPresupuestos ) ) to High( aCatTabPresupuestos ) do
    begin
        // Si la tabla es CONCEPTO o NOMPARAM y se pidi� conservar la configuraci�n de n�mina,
        // entonces est�s tablas no se procesan.
        if ((aCatTabPresupuestos[i] = 'CONCEPTO') or (aCatTabPresupuestos[i] = 'NOMPARAM')) and (not lConservarConfNomina)
          OR
          ((aCatTabPresupuestos[i] <> 'CONCEPTO') and (aCatTabPresupuestos[i] <> 'NOMPARAM'))
        then
            procesaTablaPresupuestos (aCatTabPresupuestos[i], sBaseDatosBaseCodigo, oZetaProvider);
    end;
end;

procedure TdmPreparaPresupuestos.LeeTabPresupuestosEmpleados(const sBaseDatosBaseCodigo: string;
                      oZetaProvider: TdmZetaServerProvider);
var
   i: Integer;
begin
    // Recorrer array de aTabPresupuestosColabora y procesar
    for i := ( Low( aTabPresupuestosColabora ) ) to High( aTabPresupuestosColabora ) do
    begin
      procesaTablaPresupuestos (aTabPresupuestosColabora[i], sBaseDatosBaseCodigo, oZetaProvider, TRUE);
    end;
end;

procedure TdmPreparaPresupuestos.procesaTablaPresupuestos (tabla, sBaseDatosBaseCodigo: String;
                                                            oZetaProvider: TdmZetaServerProvider; bEmpleados: Boolean = FALSE);
var iAdvertencias, iErrores: Integer;
begin
    iAdvertencias := 0;
    iErrores := 0;
    LeerTablaDesdeBD(tabla, sBaseDatosBaseCodigo, oZetaProvider, bEmpleados);
    GrabaDataSet (cdsTabla, tabla, VACIO, oZetaProvider, iAdvertencias, iErrores, FALSE, FALSE, 'PRETTYNAME');
end;

procedure TdmPreparaPresupuestos.LeerTablaDesdeBD (tabla, sBaseDatosBaseCodigo: String;
                                oZetaProvider: TdmZetaServerProvider; bEmpleados: Boolean = FALSE);
var
  oZetaServerProviderCompany: TdmZetaServerProvider;
  sWhere, sWhereComun: String;
begin
  try
      sWhere := '';
      sWhereComun := '';
      oZetaServerProviderCompany := GetNuevoProvider(sBaseDatosBaseCodigo, Q_GET_BASES_DATOS_SERVER, '', '', oZetaProvider);

      with oZetaServerProviderCompany do
      begin
          cdsTabla.InitTempDataset;
          if bEmpleados then
          begin

              sWhereComun := Format (' WHERE CB_CODIGO IN (SELECT CB_CODIGO FROM %s.dbo.COLABORA ' +
                ' WHERE (CB_ACTIVO=''S'' and CB_FEC_ING<''%s'') or (CB_ACTIVO=''N'' and CB_FEC_BAJ>= ''%s''))',
                [oZetaProvider.ParamList.ParamByName('BaseDatosBaseNombre').AsString,
                DateToStrSQL (oZetaProvider.ParamList.ParamByName('FechaAlta').AsDate),
                DateToStrSQL (oZetaProvider.ParamList.ParamByName('FechaBaja').AsDate)]);


              if Tabla = 'COLABORA' then
                // '(CB_ACTIVO=''S'' and CB_FEC_ING<''%s'') or (CB_ACTIVO=''N'' and CB_FEC_BAJ>= ''%s'')'
                sWhere := ' WHERE ' + Format (aTabPresupuestosFiltrosColabora[0],
                  [DateToStrSQL (oZetaProvider.ParamList.ParamByName('FechaAlta').AsDate),
                  DateToStrSQL (oZetaProvider.ParamList.ParamByName('FechaBaja').AsDate)]);

              if Tabla = 'ACUMULA' then
                // 'AC_YEAR = %d'
                sWhere := ' AND ' + Format (aTabPresupuestosFiltrosColabora[1],
                  [oZetaProvider.ParamList.ParamByName('anioPresupuestar').AsInteger]);

              if Tabla = 'AHORRO' then
                // 'AH_STATUS = 0'
                sWhere := ' AND ' + aTabPresupuestosFiltrosColabora[2];

              if Tabla = 'ACAR_ABO' then
                { 'CB_CODIGO in (SELECT A.CB_CODIGO FROM %0:s.dbo.AHORRO A '+
                                                'WHERE A.CB_CODIGO = %0:s.dbo.ACAR_ABO.CB_CODIGO AND ' +
                                                '      A.AH_TIPO = %0:s.dbo.ACAR_ABO.AH_TIPO AND '+
                                                '      A.AH_STATUS = 0 )',}
                sWhere := ' AND ' + Format (aTabPresupuestosFiltrosColabora[3],
                    [oZetaProvider.ParamList.ParamByName('BaseDatosBaseNombre').AsString]);

              if Tabla = 'PRESTAMO' then
                //'PR_STATUS = 0'
                sWhere := ' AND ' + aTabPresupuestosFiltrosColabora[4];

              if Tabla = 'PCAR_ABO' then
              { 'CB_CODIGO in (SELECT A.CB_CODIGO FROM %0:s.dbo.PRESTAMO A '+
                                                 'WHERE A.CB_CODIGO = %0:s.dbo.PCAR_ABO.CB_CODIGO AND '+
                                                 'A.PR_TIPO = %0:s.dbo.PCAR_ABO.PR_TIPO AND '+
                                                 'A.PR_REFEREN = %0:s.dbo.PCAR_ABO.PR_REFEREN AND '+
                                                 'A.PR_STATUS = 0 )'}
                sWhere := ' AND ' + Format (aTabPresupuestosFiltrosColabora[5],
                    [oZetaProvider.ParamList.ParamByName('BaseDatosBaseNombre').AsString]);


              if Tabla <> 'COLABORA' then
                sWhere := sWhereComun + sWhere;

              cdsTabla.Data := OpenSQL(EmpresaActiva, Format ('SELECT * FROM %s ' + sWhere + ' ORDER BY CB_CODIGO', [tabla]), TRUE);
          end
          else
          begin
            cdsTabla.Data := OpenSQL(EmpresaActiva, Format ('SELECT * FROM %s ', [tabla]), TRUE);
          end;

      end;
  finally
      FreeAndNil (oZetaServerProviderCompany);
  end;
end;

function TdmPreparaPresupuestos.GetNuevoProvider (sUbicacion: WideString; iQuery: Integer; sUsuario: WideString;
                                            sPassword: WideString; oZetaProvider: TdmZetaServerProvider): TdmZetaServerProvider;
var
  dsBaseDatos: TZetaCursor;
  oZetaProviderCompany: TdmZetaServerProvider;
  empresaActivaTMP: Variant;
begin
      Result := nil;
      with oZetaProvider do
      begin
          try
            // Asignar empresaActivaTMP
            empresaActivaTMP := EmpresaActiva;
            EmpresaActiva := Comparte;

            dsBaseDatos := CreateQuery(Format(GetScript(iQuery), [sUbicacion, sUsuario, sPassword]));
            dsBaseDatos.Active := True;

            if not dsBaseDatos.Eof then
            begin
                  oZetaProviderCompany := TdmZetaServerProvider.Create(Nil);
                  oZetaProviderCompany.EmpresaActiva := BuildEmpresa(dsBaseDatos);
                  Result := oZetaProviderCompany;
            end;

            dsBaseDatos.Active := False;

            // Reasignar empresa activa
            EmpresaActiva := empresaActivaTMP;

          finally
               FreeAndNil(dsBaseDatos);
          end;
      end;
end;

function TdmPreparaPresupuestos.PresupuestosCatalogos(Parametros: OleVariant; oZetaProvider: TdmZetaServerProvider): WordBool;
var
  sBaseDatosBaseCodigo: String;
  lConservarConfNomina: Boolean;
begin
     with oZetaProvider do
     begin
        AsignaParamList( Parametros );
        with ParamList do
        begin
             sBaseDatosBaseCodigo := ParamByName( 'BaseDatosBase' ).AsString;
             lConservarConfNomina := ParamByName ('ConservarConfNomina').AsBoolean;
        end;
        LeeCatTabPresupuestos(sBaseDatosBaseCodigo, lConservarConfNomina, oZetaProvider)
     end;

     Result:= TRUE;
end;

function TdmPreparaPresupuestos.PresupuestosEmpleados(Parametros: OleVariant; oZetaProvider: TdmZetaServerProvider; sBaseDatosBaseNombre: String): WordBool;
var
  sBaseDatosBaseCodigo: String;
begin
     with oZetaProvider do
     begin
        AsignaParamList( Parametros );
        with ParamList do
        begin
             // C�digo en DB_INFO de la Base de Datos origen.
             sBaseDatosBaseCodigo := ParamByName( 'BaseDatosBase' ).AsString;
             // Nombre en el servidor para la Base de Datos origen.
             AddString('BaseDatosBaseNombre', sBaseDatosBaseNombre);
        end;
        LeeTabPresupuestosEmpleados(sBaseDatosBaseCodigo, oZetaProvider)
     end;
     Result:= TRUE;
end;

function TdmPreparaPresupuestos.OptimizacionPresupuestos(Parametros: OleVariant; oProvider: TdmZetaServerProvider): Boolean;
const Q_PRESUP_MIGRA_OPTIMIZA = 'EXECUTE SPP_MigracionOptimiza ''%s'' ';
begin
     with oProvider do
     begin
          AsignaParamList( Parametros );
          ExecSQL (EmpresaActiva, Format( Q_PRESUP_MIGRA_OPTIMIZA,
              [zBoolToStr( ParamList.ParamByName('NominaMensual').AsBoolean)] ));
     end;
     Result := TRUE;
end;

end.
