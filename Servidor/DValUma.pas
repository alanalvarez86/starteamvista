unit DValUma;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
     ZetaCommonClasses,
     ZetaCommonTools,
     DZetaServerProvider;

type
  TdmValUma = class(TDataModule)
    procedure dmValUmaCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FQueryValUma: TZetaCursor;
    FInicial: TDate;
    FFinal: TDate;
    FValorDiario: TPesos;
    FValorMensual: TPesos;
    FValorAnual: TPesos;
    FFacDescInfonavit: TPesos;
    function oZetaProvider: TdmZetaServerProvider;
    procedure PreparaQuery;
    procedure CacheValUma( const dReferencia: TDate );
  public
    { Public declarations }
    function  GetValUma( const dReferencia: TDate; const iTipo: integer ): TPesos;
    function  GetVUDefaultDiario( const dReferencia: TDate ): TPesos;
    function  GetFactorDescuento( const dReferencia: TDate ): TPesos;
  end;

implementation

const
     K_QUERY_VAL_UMA = 'select UM_FEC_INI, UM_VALOR, UM_FDESC from VALOR_UMA order by UM_FEC_INI desc';

{$R *.DFM}

{ TdmValUma }

procedure TdmValUma.dmValUmaCreate(Sender: TObject);
begin
     FInicial := 0;
     FFinal := 0;
end;

procedure TdmValUma.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FQueryValUma );
end;

function TdmValUma.oZetaProvider: TdmZetaServerProvider;
begin
     Result := TdmZetaServerProvider( Owner );
end;

function TdmValUma.GetValUma(const dReferencia: TDate; const iTipo: integer): TPesos;
begin
      // Si la fecha de referencia se sale de los rangos del cach�,  se tiene que hacer el query de nuevo.
      if ( dReferencia < FInicial ) or ( dReferencia > FFinal ) then
         CacheValUma( dReferencia );
      if ( iTipo > 0 ) then
      begin
           case iTipo of
                K_T_MENSUAL:      Result := FValorMensual;
                K_T_ANUAL:        Result := FValorAnual;
                K_T_FDESC:        Result := FFacDescInfonavit;
           else
               Result :=          FValorDiario;
           end;
      end
      else
          Result := FValorDiario;
end;

function TdmValUma.GetVUDefaultDiario(const dReferencia: TDate): TPesos;
begin
     Result := GetValUma( dReferencia, K_T_DIARIO );
end;

function TdmValUma.GetFactorDescuento(const dReferencia: TDate): TPesos;
begin
     Result := GetValUma( dReferencia, K_T_FDESC );
     {if Result = 0.0 then
     begin
          Result := GetVUDefaultDiario (dReferencia);
     end;}
end;

procedure TdmValUma.PreparaQuery;
begin
     if ( FQueryValUma = NIL ) then
     begin
          FQueryValUma := oZetaProvider.CreateQuery( K_QUERY_VAL_UMA );
     end;
end;

procedure TdmValUma.CacheValUma( const dReferencia: TDate );
const
     K_VU_FEC_INI = 0;
     K_VU_DIARIO  = 1;
     K_VU_FDESC   = 2;
var
   lEncontro: Boolean;
   dValorMensual, dValorAnual :  Double;
begin
     PreparaQuery;
     with FQueryValUma do
     begin
	      Active := True;
	      lEncontro := False;
	      FFinal := 2100 * 365;	// A�o 2100
	      { Recordar que el query viene en orden descendente; }
          { El primer registro del query es el de la �ltima fecha }
	      { Generalmente, en el primer registro se encuentra }
          while not Eof do
          begin
               FInicial := Fields[ K_VU_FEC_INI ].AsDateTime;
               if ( FInicial <= dReferencia ) then
               begin
	                lEncontro := True;
                    Break;
               end;
               FFinal := FInicial - 1;
               Next;
          end;
          if lEncontro then
          begin
               FValorDiario  := Fields[ K_VU_DIARIO ].AsFloat;
               FFacDescInfonavit  := Fields[ K_VU_FDESC ].AsFloat;
               dValorMensual := Redondea( FValorDiario * 30.4 );
               dValorAnual   := Redondea( dValorMensual * 12  );

               FValorMensual:= dValorMensual;
               FValorAnual:= dValorAnual;
          end
          else
          begin
               FInicial := 0;
               FValorDiario := 0;
               FValorMensual := 0;
               FValorAnual := 0;
               FFacDescInfonavit := 0;
          end;
          Active := False;
     end;
end;

end.
