unit DSuperGlobal;


interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Db, DBClient,
  Variants,
  ZetaCommonLists, ZetaServerDataSet, DZetaServerProvider;

type
  TGrabaGlobalEvent = procedure(iCodigo: Integer; sDelta: String) of object;

  TdmSuperGlobal = class(TObject)
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FOnGrabarGlobal: TGrabaGlobalEvent;
    FErrores: Integer;
    FActualizaDiccion: Boolean;
    procedure Init;
    procedure SetProvider(Provider: TdmZetaServerProvider);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property ActualizaDiccion: Boolean read FActualizaDiccion write FActualizaDiccion;
    property Errores: Integer read FErrores write FErrores;
    property OnGrabarGlobal: TGrabaGlobalEvent read FOnGrabarGlobal write FOnGrabarGlobal;
    property Provider: TdmZetaServerProvider read oZetaProvider write SetProvider;
    function ProcesaLectura: OleVariant;
    function ProcesaEscritura(const oDelta: OleVariant): OleVariant;
    function ProcesaEscrituraDescripcionesGV(const oDelta: OleVariant): OleVariant;
    function ProcesaLecturaGV : OleVariant;
  end;

const
  {$IFDEF TRESS}
  K_QUERY_GET_GLOBAL =
    'select GL_CODIGO, GL_FORMULA from GLOBAL where (GL_CODIGO < %d) order by GL_CODIGO';
  {$ELSE}
  K_QUERY_GET_GLOBAL = 'select GL_CODIGO, GL_FORMULA from GLOBAL order by GL_CODIGO';
  {$ENDIF}
  K_QUERY_GET_DESCRIP_GV =  'select GL_CODIGO, GL_DESCRIP from GLOBAL where GL_CODIGO IN (47,48,49,56,57,58,59,60,84,85,86,87,88) order by GL_CODIGO';
  K_DICCIONARIO   = 'execute procedure SP_REFRESH_DICCION';
  K_QUERY_UPDATE  = 'update GLOBAL set GL_FORMULA = :Formula where ( GL_CODIGO = :Codigo )';
  K_QUERY_INSERT  = 'insert into GLOBAL ( GL_CODIGO, GL_FORMULA ) values ( :Codigo, :Formula )';
  K_QUERY_SELECT  = 'select GL_DESCRIP, GL_FORMULA from GLOBAL where GL_CODIGO= :Codigo';
  K_ANCHO_FORMULA = 255;

implementation

uses
  ZGlobalTress, ZetaCommonTools, ZetaServerTools, ZetaCommonClasses;

{ TdmSuperGlobal }

constructor TdmSuperGlobal.Create;
begin
  Init;
end;

destructor TdmSuperGlobal.Destroy;
begin
  inherited Destroy;
end;

procedure TdmSuperGlobal.Init;
begin
  Errores := 0;
  ActualizaDiccion := FALSE;
end;

procedure TdmSuperGlobal.SetProvider(Provider: TdmZetaServerProvider);
begin
  oZetaProvider := Provider;
end;

{ *************** METODOS Y FUNCIONES PUBLICOS ******************** }


function TdmSuperGlobal.ProcesaLecturaGV: OleVariant;
var
  i, j, iCodigo: Integer;
  sDescripcion: String;
  FGlobal: TZetaCursor;
begin
{$IFDEF TRESS}
  if Assigned(oZetaProvider) then begin
    FGlobal := oZetaProvider.CreateQuery;
    try
      with oZetaProvider do begin
        Result := VarArrayCreate([0, K_TOT_DESCRIPCIONES_GV, 0, 2],varVariant);
        { Inicializa con 'NIL' }
        for i := 0 to K_TOT_DESCRIPCIONES_GV do
            for j := 0 to 2 do
             begin
               Result[i,j] := K_TOKEN_NIL;
             end;
        AbreQueryScript(FGlobal,K_QUERY_GET_DESCRIP_GV);
        i:= 0;
        with FGlobal do begin
          while not Eof do begin
              iCodigo := Fields[0].AsInteger;
              sDescripcion :=  Fields[1].AsString;
              Result[i,0] := iCodigo;
              Result[i,1] := sDescripcion ;
          inc(i);
          Next;
          end;
          Active := FALSE;
        end;
      end;
    finally
      FreeAndNil(FGlobal);
    end;
  end;
{$ENDIF}
end;

function TdmSuperGlobal.ProcesaLectura: OleVariant;
var
  i, iCodigo: Integer;
  FGlobal: TZetaCursor;
begin
  if Assigned(oZetaProvider) then begin
    FGlobal := oZetaProvider.CreateQuery;
    try
      with oZetaProvider do begin
        Result := VarArrayCreate([0, K_TOT_GLOBALES], varOleStr);
        { Inicializa con 'NIL' }
        for i := 0 to K_TOT_GLOBALES do
             Result[i] := K_TOKEN_NIL;
        {$IFDEF TRESS}
        AbreQueryScript(FGlobal, Format(K_QUERY_GET_GLOBAL, [K_GLOBAL_DECANUAL_PRIMER_CAMPO]));
        {$ELSE}
        AbreQueryScript(FGlobal, K_QUERY_GET_GLOBAL);
        {$ENDIF}
        with FGlobal do begin
          while not Eof do begin
            iCodigo := ZGlobalTress.GlobalToPos(Fields[0].AsInteger);
            if (iCodigo >= 1) and (iCodigo <= K_TOT_GLOBALES) then
              Result[iCodigo] := Fields[1].AsString;
            Next;
          end;
          Active := FALSE;
        end;
      end;
    finally
      FreeAndNil(FGlobal);
    end;
  end;
end;

function TdmSuperGlobal.ProcesaEscrituraDescripcionesGV(const oDelta: OleVariant): OleVariant;
const
  K_QUERY_UPDATE_DESCRIP_GV = 'update GLOBAL set GL_Descrip = :Descripcion where ( GL_CODIGO = :Codigo )';
var
  i, j, iCodigo: Integer;
  sDescripcion : string;
  FUpdate: TZetaCursor;
  function EjecutaQueryGlobal(oQuery: TZetaCursor; const iCodigo: Integer; const sDescripcion: String): Boolean;
  begin
    with oZetaProvider do begin
      ParamAsInteger(oQuery, 'Codigo', iCodigo);
      ParamAsVarChar(oQuery, 'Descripcion', sDescripcion, K_ANCHO_FORMULA);
      { Para que el resultado sea TRUE, tiene que existir el Registro }
      { Si no existe, entonces se tiene que agregar; }
      Result := (Ejecuta(oQuery) = 1);
    end;
  end;
  procedure GrabaGlobal(const iCodigo: Integer; const sDescripcion: String);
  begin
    with oZetaProvider do begin
      EmpiezaTransaccion;
      try
        if not EjecutaQueryGlobal(FUpdate, iCodigo, sDescripcion) then begin
          { Si no existe el registro, NO lo Agrega, a direrenciad el metodo que graba el valor del global }
        end;
        TerminaTransaccion(True);
      except
        on Error: Exception do begin
          TerminaTransaccion(FALSE);
          raise;
        end;
      end;
    end;
  end;
begin
{$IFDEF TRESS}
  Result := Null;
  if Assigned(oZetaProvider) then begin
    FUpdate := oZetaProvider.CreateQuery(K_QUERY_UPDATE_DESCRIP_GV );
    try
      for i := 0 to K_TOT_DESCRIPCIONES_GV do begin
        if (oDelta[i,1] <> K_TOKEN_NIL) then begin
          iCodigo := oDelta[i,0];
          sDescripcion :=  oDelta[i,1];
          GrabaGlobal(iCodigo, sDescripcion );
        end;
      end;
    finally
       FreeAndNil( FUpdate );
    end;
  end;
{$ENDIF}
end;

function TdmSuperGlobal.ProcesaEscritura(const oDelta: OleVariant): OleVariant;
var
  i, iCodigo: Integer;
  FUpdate, FInsert, FSelect: TZetaCursor;

  function EjecutaQueryGlobal(oQuery: TZetaCursor; const iCodigo: Integer; const sFormula: String): Boolean;
  begin
    with oZetaProvider do begin
      ParamAsInteger(oQuery, 'Codigo', iCodigo);
      ParamAsVarChar(oQuery, 'Formula', sFormula, K_ANCHO_FORMULA);
      { Para que el resultado sea TRUE, tiene que existir el Registro }
      { Si no existe, entonces se tiene que agregar; }
      Result := (Ejecuta(oQuery) = 1);
    end;
  end;

  procedure GrabaGlobal(const iCodigo: Integer; const sFormula: String);
  begin
    with oZetaProvider do begin
      EmpiezaTransaccion;
      try
        if not EjecutaQueryGlobal(FUpdate, iCodigo, sFormula) then begin
          { Si no existe el registro, lo Agrega }
          { Como no es muy comun que suceda esto, no se crea el Query }
          { sino hasta que se necesita }
          if (FInsert = nil) then begin
            FInsert := CreateQuery(K_QUERY_INSERT);
          end;
          EjecutaQueryGlobal(FInsert, iCodigo, sFormula);
        end;
        TerminaTransaccion(True);
      except
        on Error: Exception do begin
          TerminaTransaccion(FALSE);
          raise;
        end;
      end;
    end;
  end;

  function GetCambiosGlobalBitacora(sGlobalAnt, sGlobalNuevo: String): String;
  var
    i, j, iTope, iPtr, iValue, iValueNew: Integer;
    lAnterior, lNuevo: Boolean;
    sBitacora, sValue, sValueNew: String;
  begin
    // Quiere decir que el global no existe
    if StrVacio(sGlobalAnt) then begin
      for i := 1 to Length(sGlobalNuevo) do
        sGlobalAnt := sGlobalAnt + 'F';
    end;

    sBitacora := VACIO;
    iTope := High(TBit);
    i := 1;
    while i < Length(sGlobalAnt) do begin
      sValue := '$' + sGlobalAnt[i];
      sValueNew := '$' + sGlobalNuevo[i];
      Inc(i);
      if i <= Length(sGlobalAnt) then begin
        sValue := sValue + sGlobalAnt[i];
        sValueNew := sValueNew + sGlobalNuevo[i];
        Inc(i);
      end else begin
        sValue := sValue + 'F';
        sValueNew := sValueNew + 'F';
      end;

      iValue := StrToInt(sValue);
      iValueNew := StrToInt(sValueNew);

      for j := 0 to iTope do begin
        lAnterior := BitSet(iValue, j);
        lNuevo := BitSet(iValueNew, j);
        if (lAnterior <> lNuevo) then begin
          iPtr := j + ((Trunc(i / 2) - 1) * (iTope + 1));
          if StrLleno(sBitacora) then
            sBitacora := sBitacora + CR_LF;
          sBitacora := sBitacora + ObtieneElemento(lfClaseBitacora, iPtr + 1) + ' ' + ' De:' + '"' +
            BooltoSiNo(lAnterior) + '"' + ' A:' + '"' + BooltoSiNo(lNuevo) + '"';
        end;
      end;
    end;
    Result := sBitacora;
  end;

  procedure EscribeBitacora(const iCodigo: Integer; const sGlobalNuevo: string);
  var
    sGlobalDescrip, sGlobalAnterior, sDescripcion: string;
  begin
    with oZetaProvider do begin
      ParamAsInteger(FSelect, 'Codigo', iCodigo);
      with FSelect do begin
        Active := True;
        sGlobalDescrip := FieldByName('GL_DESCRIP').AsString;
        sGlobalAnterior := FieldByName('GL_FORMULA').AsString;
        Active := FALSE;
      end;
      if (iCodigo = K_GLOBAL_GRABA_BITACORA) then
        sDescripcion := GetCambiosGlobalBitacora(sGlobalAnterior, sGlobalNuevo)
      else
        sDescripcion := Format('De: "%s"' + CR_LF + 'A: "%s"', [sGlobalAnterior, sGlobalNuevo]);
      EscribeBitacora(tbNormal, clbGlobales, iCodigo, NullDateTime, GetDescripcionGlobal(iCodigo),
        sDescripcion);
    end;
  end;

begin
  Result := Null;
  if Assigned(oZetaProvider) then begin
    FUpdate := oZetaProvider.CreateQuery(K_QUERY_UPDATE);
    FSelect := oZetaProvider.CreateQuery(K_QUERY_SELECT);
    FInsert := nil;
    try
      for i := 0 to K_TOT_GLOBALES do begin
        if (oDelta[i] <> K_TOKEN_NIL) then begin
          iCodigo := ZGlobalTress.PosToGlobal(i);
          EscribeBitacora(iCodigo, oDelta[i]);
          GrabaGlobal(iCodigo, oDelta[i]);
          if Assigned(FOnGrabarGlobal) then
            FOnGrabarGlobal(iCodigo, oDelta[i]);
        end;
      end;
    finally
       FreeAndNil( FSelect );
       FreeAndNil( FUpdate );
       FreeAndNil( FInsert );
    end;
    { Actualiza los Datos de Diccionario }
    if ActualizaDiccion then begin
      with oZetaProvider do try
        EmpiezaTransaccion;
        try
          FUpdate := CreateQuery(K_DICCIONARIO);
          Ejecuta(FUpdate);
          TerminaTransaccion(True);
        except
          on Error: Exception do begin
            TerminaTransaccion(FALSE);
            raise;
          end;
        end;
      finally
        FreeAndNil( FUpdate );
      end;
    end;
  end;
end;

end.
