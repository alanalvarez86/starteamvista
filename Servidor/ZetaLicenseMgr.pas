unit ZetaLicenseMgr;

interface

{$define DEBUGSENTINEL}
{$define VALIDAEMPLEADOSGLOBAL}
{!$define TEST}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
     Variants,
     FAutoServer,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaLicenseClasses,
     DZetaServerProvider,
     Contnrs;

type
  TDatosUltimosProcesos = record
    CalculoIntegradosCuantos: Integer;
    CalculoIntegradosUltimo: TDate;
    AfectarNominaCuantos: Integer;
    AfectarNominaUltimo: TDate;
    AjusteRetFonacotCuantos: Integer;
    AjusteRetFonacotUltimo: TDate;
    CalcularPagoFonacotCuantos: Integer;
    CalcularPagoFonacotUltimo: TDate;
    ConciliacionFonacotCuantos: Integer;
    ConciliacionFonacotUltimo: TDate;
    LabCalcularTiemposCuantos: Integer;
    LabCalcularTiemposUltimo: TDate;
    ReportesEmailCuantos: Integer;
    ReportesEmailUltimo: TDate;
  end;

  TDatosUltimosRegistros = record
    CursosEntrenaCuantos: Integer;
    CafeteriaCuantos: Integer;
    CafeteriaUltimo: TDate;
    ResguardoCuantos: Integer;
    ResguardoUltimo: TDate;
    SMedicosCuantos : Integer;
    SMedicosUltimo : TDate;
    PlanCarreraCuantos : Integer;
    PlanCarreraUltimo : TDate;
    EvaluacionCuantos : Integer;
    EvaluacionUltimo : TDate;
  end;

  TSQLServerVersion = record
    Version : string;
    Nivel : string;
    Edicion : string;
  end;

  TDatosSeleccion = record
    BDCuantos : integer;
    RequisicionCuantos : integer;
    RequisicionUltimo : TDate;
  end;

  TDatosVisitantes = record
    BDCuantos : integer;
    LibroCuantos : integer;
    LibroUltimo : TDate;
  end;

  TDatosModulosComparte = record
    UsuarioAutomatico : integer;
    MisDatosCuantos : integer;
    MisDatosUltimo : TDate;
    KioscoCuantos : integer;
    KioscoUltimo : TDate;
    WorkflowCuantos : integer;
    WorkflowUltimo : TDate;
  end;

  // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
  TDatosUsuarioEmail = class( TObject )
     public
     { Public declarations }
     UsCodigo : Integer;
     UsEmail : String;
     UsFormato : Integer;
     UsActivo : String;
     UsNombre : String;
     UsCorto : String;
     constructor Create;
     destructor Destroy; override;
  end;

  TLicenseMgr = class(TObject)
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FAutoServer: TAutoServer;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    FGlobalLicense : TGlobalLicenseValues;
    FEmpleadosConteo : integer;
{$endif}
{$ifdef TRESSCFG}
    FDatosModulosComparte : TDatosModulosComparte;
{$endif}
    procedure SetAutoServer( value : TAutoServer );
    function GetCheckSum(const iValue: Integer): Integer;
    function GetDomainName(const iLicencias, iSentinel: Integer): String;
    function GetDomainNameWildCard: String;
    {
    function Decode(const sValue: String): Integer;
    }
    function Encode(const iValue: Integer): String;
    function EmpleadosCuenta(const sScript: String ): Integer;
    function GetUltima(const sScript: String ): TDate;

{$ifdef TRESSCFG}
    procedure ObtenerConteoRegistros( sTabla, sCampoFecha : string ; var Cuantos : integer; var Ultimo : TDateTime; iMesesAtras : integer = 0 ) ;
{$ifdef MSSQL}
    procedure ObtenerConteoRegistrosComparte( sTabla, sCampoFecha, sCampoTexto, sLikeTexto : string ; var Cuantos : integer; var Ultimo : TDateTime ; iMesesAtras : integer = 0) ;
{$endif}
    function GetGlobalStringBD( const iGlobal : Integer ) : String;
    function GetGlobalIntegerBD(const iGlobal: Integer): Integer;
    function GetFormulaConcepto(const iConcepto: Integer): String;
{$endif}

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( Provider: TdmZetaServerProvider );
    destructor Destroy; override;
    property AutoServer: TAutoServer read FAutoServer write SetAutoServer;
    {$ifdef VALIDAEMPLEADOSGLOBAL}
    property EmpleadosConteo : integer read FEmpleadosConteo write FEmpleadosConteo;
    {$endif}
    function AsignadosGet: Integer;
    function AsignadosSet(const iEmpleados: Integer): Boolean;
    function DesempleadosGet: Integer;
    function EmpleadosGet: Integer;
    function UltimaAlta: TDate;
    function UltimaBaja: TDate;
    function UltimaTarjeta: TDate;
  {$ifdef TRESSCFG}
    function DuracionNomina(iMesesAtras : integer) : Double;
    function GetCompanyXMLStr (sCM_CTRL_RL: String): string;
    function GetInfoUsoTressStr (sCM_CTRL_RL: String): string;
    function GetRazonesSocialesXMLStr (sCM_CTRL_RL: String) : string;
    function GetInfoUsoTress(sCM_CTRL_RL: String;  const lTodas: Boolean ): string;
    function GetNominaXMLStr (sCM_CTRL_RL: String) : string;
    function GetPeriodosXMLStr : string;
    function GetAsistenciaXMLStr : string;
    function GetFonacotXMLStr : string;
    function GetUsoModulosXMLStr : string;
    function GetDatabaseSize( sCM_DATOS : string ) : Double;
 {$endif}

    {$ifdef TRESSCFG}
    function UltimaNomina: TDatosPeriodo;
    function UltimosProcesos: TDatosUltimosProcesos;
    function UltimosRegistros: TDatosUltimosRegistros;

    function RegistrosSeleccion: TDatosSeleccion;
    function RegistrosVisitantes: TDatosVisitantes;
    function RegistrosModulosComparte: TDatosModulosComparte;

    function GetSQLServerVersion: TSQLServerVersion;
    function GetUltimasNominasXML: string;
    function GetChecadasAsistenciaXML(iDiasAtras : integer) : string;

    {$endif}
{$ifdef DEBUGSENTINEL}
    procedure AutoGetData(Sender: TObject; var StringData: String);
    procedure AutoSetData(Sender: TObject; var StringData: String);
    procedure DeleteData(Sender: TObject);
{$endif}
{$ifdef VALIDAEMPLEADOSGLOBAL}
//    eTipoCompany = ( tc3Datos, tcRecluta, tcVisitas, tcOtro, tc3Prueba, tcPresupuesto );
    procedure AutoOnGetDataAdditional( Sender: TObject );
    function GetTipoValidoCompany :  eTipoCompany;
    function EmpleadosGetGlobal : Integer;
    function GetGlobalMostrarAdvertencia: Boolean; // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
    // US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
    function GetGlobalServidorCorreos: String;
    function GetGlobalPuertoSMTP: Integer;
    function GetGlobalAutentificacionCorreo: Integer;
    function GetGlobalUserID: String;
    function GetGlobalEmailPSWD: String;
    function GetGlobalRecibirAdvertenciaPorEmail: Boolean;
    function GetGlobalGrupoUsuariosEmail: Integer;
    function GetUsuariosEmailSeguridad: TObjectList;
{$endif}

  end;

implementation

uses ZetaCommonTools,
     ZGlobalTress,
     ZetaServerTools;

const
     K_GLOBALES = 10;
     K_GLOBAL = 8;
     K_TOPE = 100000;
     K_RANDOM = 1000;

     K_DIAS_3DT_HISTORIA = 90;
     K_MESES_3DT_HISTORIA = 3;

     K_GLOBAL_READ = 'select GL_FORMULA from GLOBAL where ( GL_CODIGO = :Codigo )';
     K_GLOBAL_DELETE = 'delete from GLOBAL where ( GL_CODIGO = :Codigo )';
     K_GLOBAL_WRITE = 'insert into GLOBAL( GL_CODIGO, GL_FORMULA ) values ( :GL_CODIGO, :GL_FORMULA )';


     K_CONCEPTO_READ = 'select CO_FORMULA from CONCEPTO where ( CO_NUMERO = :CO_NUMERO )';
     K_CONCEPTOS_READ = 'select CO_NUMERO, CO_FORMULA from CONCEPTO where ( CO_NUMERO in ( %s ) )';
     K_CONCEPTOS_ACTIVOS_READ = 'select CO_NUMERO, CO_DESCRIP, CO_G_IMSS, CO_G_ISPT, CO_FORMULA from CONCEPTO where CO_ACTIVO = ''S'' and CO_NUMERO < 1000';


     K_EMPLEADOS_COUNT = 'select COUNT(*) as CUANTOS from COLABORA where ( CB_ACTIVO = %s )';
     K_DESEMPLEADOS_COUNT = 'select COUNT(*) as CUANTOS from COLABORA where ( CB_ACTIVO <> %s )';
     K_ULTIMA_ALTA = 'select MAX( CB_FEC_ING ) as ULTIMA from COLABORA where ( CB_ACTIVO = %s )';
     K_ULTIMA_BAJA = 'select MAX( CB_FEC_BAJ ) as ULTIMA from COLABORA where ( CB_ACTIVO <> %s )';
     K_ULTIMA_TARJETA = 'select MAX( AU_FECHA ) as ULTIMA from AUSENCIA';
     K_PERIODO_MAXIMOS = 'select MAX( PE_YEAR ) as PE_YEAR, PE_TIPO, TP_DESCRIP, TP_DIAS_EV, MAX( PE_NUMERO ) as PE_NUMERO, COUNT(*) CANTIDAD  from PERIODO left join TPERIODO on PERIODO.PE_TIPO = TPERIODO.TP_TIPO where '+
                          '( PE_STATUS >= %d ) and '+
                          '( PE_NUMERO < %d ) and ' +
                          '( PE_FEC_INI <= ''%s'' ) '+
                          'group by PE_TIPO,TP_DESCRIP,TP_DIAS_EV';


     K_PERIODO_MAXIMO = 'SELECT MAX(PE_NUMERO) as PE_NUMERO from PERIODO where PE_YEAR = %d and PE_TIPO = %d and '+
                          '( PE_STATUS >= %d ) and '+
                          '( PE_NUMERO < %d ) and ' +
                          '( PE_FEC_INI <= ''%s'' )';

     K_PERIODO_FECHA = 'select PE_FEC_INI from PERIODO where ( PE_YEAR = :Year ) and ( PE_TIPO = :Tipo ) and ( PE_NUMERO = :Numero )';
     K_PERIODO_INFO = 'select TP_DESCRIP, PE_FEC_INI, PE_NUM_EMP  from PERIODO left join TPERIODO on PERIODO.PE_TIPO = TPERIODO.TP_TIPO where ( PE_YEAR = :Year ) and ( PE_TIPO = :Tipo ) and ( PE_NUMERO = :Numero )';
     K_ULTIMOS_PROCESOS = 'select PC_PROC_ID, COUNT(*) as CUANTOS, MAX( PC_FEC_INI ) as FECHA from PROCESO where '+
                          '( PC_PROC_ID in ( %d, %d, %d, %d, %d  ) ) and '+
                          '( PC_FEC_INI between ''%s'' and ''%s'' ) '+
                          'group by PC_PROC_ID order by PC_PROC_ID';



     K_ULTIMOS_PROCESOS_REPORTES_EMAIL  = 'select COUNT(*) as CUANTOS, MAX( PC_FEC_INI ) as FECHA from PROCESO where  '+
                                               '( PC_PROC_ID = %d ) and '+
                                               '( PC_FEC_INI between ''%s'' and ''%s'' ) and ( US_CODIGO=0  or US_CODIGO = %d )';

     K_ULTIMOS_PROCESOS_CONCILIACION_FONACOT = 'select COUNT(*) as CUANTOS, MAX( PC_FEC_INI ) as FECHA from PROCESO where PC_PARAMS like ''%%Conciliaci�n de Fonacot%%'' and ' +
                                               '( PC_PROC_ID = %d ) and '+
                                               '( PC_FEC_INI between ''%s'' and ''%s'' ) ';



     K_FECHA = 'yyyy-mm-dd';


     {$ifdef MSSQL}
     K_CALCULO_NOMINA_DURA_MIL = 'select  ( SUM( cast ( DATEDIFF( second,  cast(PC_FEC_INI+'' ''+PC_HOR_INI as datetime),'+
                                                'cast(PC_FEC_FIN +'' ''+ PC_HOR_FIN as datetime)) as float ) ) / '+
                                           'SUM( cast( PROCESO.PC_PASO as float ))) *1000.00  as DURA_MIL  from PROCESO '+
                                 'where PC_PROC_ID = 17 and US_CANCELA =0 and PC_PASO > 0 and PC_FEC_INI between %s and %s ';
     {$else}
     K_CALCULO_NOMINA_DURA_MIL = 'select  ( SUM( cast(PC_FEC_FIN ||'' ''|| PC_HOR_FIN as date) -  cast(PC_FEC_INI ||'' ''|| PC_HOR_INI as date) ) * 24 * 60 * 60 / SUM(PC_PASO) ) * 1000.00  as DURA_MIL '+
                                 'from PROCESO where PC_PROC_ID = 17 and US_CANCELA =0 and PC_PASO > 0 and PC_FEC_INI between %s and %s ';
     {$endif}

     K_CHECADAS_ASISTENCIA = 'select CH_RELOJ, COUNT(*) as CANTIDAD,  MAX(AU_FECHA) as FECHA  from CHECADAS  where  CH_TIPO in (1,2) and AU_FECHA >= ''%s'' group by CH_RELOJ';


     K_PROCESOS_POR_PERIODO = 'select PE_FEC_INI, PC_HOR_INI, PC_FEC_FIN, PC_HOR_FIN from PROCESO where PC_PROC_ID = 12 and US_CANCELA =0  and PC_ERROR = ''N'' and PC_PARAMS like ''%s'' ';

     K_DOMAIN_NAME = 'TEXT_%s%s';
     K_DOMAIN_OFFSET = 1;
     {$ifdef INTERBASE}
     K_DOMAIN_READ = 'select RDB$FIELD_LENGTH CUANTOS from RDB$FIELDS where '+
                     '( ( RDB$SYSTEM_FLAG is NULL ) or ( RDB$SYSTEM_FLAG = 0 ) ) and '+
                     '( RDB$FIELD_NAME = ''%s'' )';
     K_DOMAIN_READ_ALL = 'select RDB$FIELD_NAME DOMINIO, RDB$FIELD_LENGTH CUANTOS from RDB$FIELDS where '+
                         '( ( RDB$SYSTEM_FLAG is NULL ) or ( RDB$SYSTEM_FLAG = 0 ) ) and '+
                         '( RDB$FIELD_NAME like ''%s'' )';
     K_DOMAIN_DROP = 'drop domain %s';
     K_DOMAIN_ADD = 'create domain %s VARCHAR( %d )';
     {$endif}
     {$ifdef MSSQL}
     K_DOMAIN_READ = 'select CHARACTER_MAXIMUM_LENGTH CUANTOS from INFORMATION_SCHEMA.DOMAINS '+
                     'where ( DOMAIN_NAME = ''%s'' )';
     K_DOMAIN_READ_ALL = 'select DOMAIN_NAME DOMINIO, CHARACTER_MAXIMUM_LENGTH CUANTOS from INFORMATION_SCHEMA.DOMAINS '+
                         'where ( DOMAIN_NAME like ''%s'' )';
     K_DOMAIN_DROP = 'exec SP_DROPTYPE %s';
     K_DOMAIN_ADD = 'exec SP_ADDTYPE %s, ''VARCHAR( %d )''';


     {$ifdef ANTES}K_OBTIENE_DB_SIZE = 'select str(convert(dec(15),sum(size))* 8192/ 1048576,10,2) as DB_SIZE from sysfiles';{$endif}
     K_OBTIENE_DB_SIZE = 'select str(convert(float,sum(size*1.0))* 8192.0/ 1048576.0,30,2) as DB_SIZE from sysfiles';


     K_OBTIENE_DB_VERSION  = 'select CAST( SERVERPROPERTY(''productversion'') as varchar(256)) as VERSION, CAST(SERVERPROPERTY (''productlevel'') as varchar(256)) as LEVEL, CAST(SERVERPROPERTY (''edition'') as varchar(256)) as EDITION';

     {$endif}
     K_ANCHO_FORMULA = 255;
     K_CODE_TOP = $FFFF;
     K_HEX_LOWER_CAR = 'G';
     K_HEX_UPPER_CAR = 'L';
     K_NUM_UPPER_CAR = 'Z';
     K_LOWER_CAR = 'A';
     K_LOWER_NUM = '0';
{$ifdef DEBUGSENTINEL}
     Q_VA_LEER_CLAVES = 'select CL_TEXTO from CLAVES where ( CL_NUMERO = %d )';
     Q_VA_BORRA_CLAVES = 'delete from CLAVES where ( CL_NUMERO = %d )';
     Q_VA_INSERTA_CLAVES = 'insert into CLAVES ( CL_NUMERO, CL_TEXTO ) ' +
                           'values ( %d, ''%s'' )';
{$endif}



     Q_COMPANYS_COUNT = 'select COUNT(*) CUANTOS '+
                                 'from COMPANY '+
                                 'where ( %s ) and ( CM_CTRL_RL = ''3DATOS'' or CM_CTRL_RL = '''' )';

     Q_DBS_COUNT  = 'select COUNT(*) CUANTOS '+
                                 'from DB_INFO '+
                                 'where ( %s ) and ( DB_CTRL_RL = ''3DATOS'' or DB_CTRL_RL = '''' )';

     Q_COMPANYS_LIST  = 'select CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS, CM_CODIGO '+
                                'from COMPANY '+
                                'where ( %s )  and ( CM_CTRL_RL = ''3DATOS'' or CM_CTRL_RL = '''' ) ';

     Q_DBS_LIST  = 'select DB_DESCRIP, DB_USRNAME, DB_PASSWRD, DB_NIVEL0='''', DB_DATOS, DB_CODIGO '+
                                'from DB_INFO '+
                                'where ( %s )  and ( DB_CTRL_RL = ''3DATOS'' or DB_CTRL_RL = '''' ) ';

     Q_GET_TIPO_REAL_COMPANY  = 'select CM_CONTROL, CM_CTRL_RL, CM_CHKSUM, CM_DATOS '+
                                'from COMPANY '+
                                'where CM_CODIGO = ''%s'' ';

     K_RAZONES_SOCIALES_READ = 'select RS_NOMBRE as Nombre, RS_RFC as RFC ,RS_CONTID as ContID, ' +
                         ' ( select count(*) from COLABORA CB ' +
                              ' join RPATRON RP on RP.TB_CODIGO = CB.CB_PATRON ' +
                              ' where CB.CB_ACTIVO = ''S'' and  RP.RS_CODIGO = RSOCIAL.RS_CODIGO ) Empleados, ' +
                        ' ( select count(*) from NOMINA NOM ' +
                              ' join RPATRON RP on RP.TB_CODIGO = NOM.CB_PATRON ' +
                              ' join PERIODO PE on NOM.PE_TIPO = PE.PE_TIPO and NOM.PE_NUMERO = PE.PE_NUMERO and NOM.PE_YEAR = PE.PE_YEAR ' +
                              ' where RP.RS_CODIGO = RSOCIAL.RS_CODIGO and NOM.NO_TIMBRO > 0 and PE.PE_FEC_PAG >=  ''2014-01-01'' ) Timbres ' +
                              ' from RSOCIAL';
     K_ANALITICA_POBLAR = 'EXEC ANALITICA_POBLAR';

{ TLicenseMgr }

constructor TLicenseMgr.Create( Provider: TdmZetaServerProvider );
begin
     oZetaProvider := Provider;
     FGlobalLicense := TGlobalLicenseValues.Create( nil );
end;

destructor TLicenseMgr.Destroy;
begin
     FreeAndNil( FGlobalLicense );
     inherited Destroy;
end;

{ Codificar / Decodificar }

function TLicenseMgr.Encode( const iValue: Integer ): String;
var
   sHex: String;
   i: Integer;
   cValue: Char;
begin
     Result := '';
     sHex := Trim( IntToHex( ( K_CODE_TOP - iValue ), 4 ) );
     for i := 1 to Length( sHex ) do
     begin
          cValue := sHex[ i ];
          if ( cValue in [ '0'..'9' ] ) then
          begin
               cValue := Chr( Ord( K_NUM_UPPER_CAR ) - ( Ord( cValue ) - Ord( K_LOWER_NUM ) ) );
          end
          else
          begin
               cValue := Chr( Ord( K_HEX_LOWER_CAR ) + ( Ord( cValue ) - Ord( K_LOWER_CAR ) ) );
          end;
          Result := Result + cValue;
     end;
end;

{
function TLicenseMgr.Decode( const sValue: String ): Integer;
var
   i: Integer;
   cValue: Char;
   sHex: String;
begin
     sHex := '$';
     for i := 1 to Length( sValue ) do
     begin
          cValue := sValue[ i ];
          if ( cValue in [ K_HEX_LOWER_CAR..K_HEX_UPPER_CAR ] ) then
          begin
               cValue := Chr( Ord( K_LOWER_CAR ) + ( Ord( cValue ) - Ord( K_HEX_LOWER_CAR ) ) )
          end
          else
          begin
               cValue := Chr( Ord( K_LOWER_NUM ) + ( Ord( K_NUM_UPPER_CAR ) - ( Ord( cValue ) ) ) );
          end;
          sHex := sHex + cValue;
     end;
     Result := K_CODE_TOP - StrToIntDef( sHex, 0 );
end;
}

function TLicenseMgr.GetCheckSum( const iValue: Integer ): Integer;
const
     K_CHECKSUM = 100;
begin
     Result := iValue div K_CHECKSUM;
end;

function TLicenseMgr.GetDomainName( const iLicencias, iSentinel: Integer ): String;
begin
     Result := Format( K_DOMAIN_NAME, [ Encode( iLicencias ), Encode( iSentinel ) ] );
end;

function TLicenseMgr.GetDomainNameWildCard: String;
begin
     Result := Format( K_DOMAIN_NAME, [ '%', '' ] );
end;

{ Escribir Asignacion de Licencias de Empleados }
{$ifdef VALIDA_EMPLEADOS}
function TLicenseMgr.AsignadosGet: Integer;
var
   i, iCheckSum: Integer;
   sValor: String;
   FQuery: TZetaCursor;
begin
     Result := 0;
     with oZetaProvider do
     begin
          FQuery := CreateQuery( K_GLOBAL_READ );
          try
             for i := 1 to K_GLOBALES do
             begin
                  ParamAsInteger( FQuery, 'Codigo', -i );
                  with FQuery do
                  begin
                       Active := True;
                       sValor := FieldByName( 'GL_FORMULA' ).AsString;
                       Active := False;
                  end;
                  if ( i = K_GLOBAL ) then
                  begin
                       Result := StrToIntDef( '$' + sValor, 0 );
                       if ( Result > 0 ) then
                          Result := ( K_TOPE - Result );
                  end;
             end;
             PreparaQuery( FQuery, Format( K_DOMAIN_READ, [ GetDomainName( Result, AutoServer.NumeroSerie ) ] ) );
             with FQuery do
             begin
                  Active := True;
                  if IsEmpty then
                  begin
                       iCheckSum := -1;
                  end
                  else
                  begin
                       iCheckSum := ( FieldByName( 'CUANTOS' ).AsInteger - K_DOMAIN_OFFSET );
                  end;
                  Active := False;
             end;
             if ( GetCheckSum( Result ) <> iCheckSum ) then
                Result := 0;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;
{$else}
function TLicenseMgr.AsignadosGet: Integer;
begin
     Result := AutoServer.Empleados;
end;
{$endif}

function TLicenseMgr.EmpleadosCuenta( const sScript: String ): Integer;
var
   FQuery: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FQuery := CreateQuery( Format( sScript, [ ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) ] ) );
          try
             with FQuery do
             begin
                  Active := True;
                  Result := FieldByName( 'CUANTOS' ).AsInteger;
                  Active := False;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;

function TLicenseMgr.GetUltima( const sScript: String ): TDate;
var
   FQuery: TZetaCursor;
begin
     with oZetaProvider do
     begin
          FQuery := CreateQuery( Format( sScript, [ ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) ] ) );
          try
             with FQuery do
             begin
                  Active := True;
                  Result := FieldByName( 'ULTIMA' ).AsDateTime;
                  Active := False;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;

{$ifdef TRESSCFG}
function TLicenseMgr.GetInfoUsoTress( sCM_CTRL_RL: String; const lTodas: Boolean ): string;
var
   SQL: String;
   FQuery: TZetaCursor;
begin
     SQL :=  'select (select AnaliticaID, AnaliticaModulo, AnaliticaHelpContext, AnaliticaCodigo, AnaliticaDescripcion, AnaliticaBool, AnaliticaNumero,'+
     'replace(convert(varchar, AnaliticaFecha, 111), ''/'',''-'') as AnaliticaFecha,'+
     ' replace(convert(varchar, AnaliticaFechaUpd, 111),''/'',''-'') as AnaliticaFechaUpd'+
     ' from T_Analitica Item FOR XML AUTO,  TYPE, ROOT(''AnaliticaTress'')) as Resultado ';
     with oZetaProvider do
     begin
          FQuery := CreateQuery( SQL);
          try
             with FQuery do
             begin
                  Active := True;
                  Result := FieldByName( 'Resultado' ).AsString;
                  Active := False;
             end;
             FreeAndNil( FQuery );
          Except
                 FreeAndNil( FQuery );
          end;
     end;
end;
{$endif}

function TLicenseMgr.EmpleadosGet: Integer;
begin
     Result := EmpleadosCuenta( K_EMPLEADOS_COUNT );
end;

function TLicenseMgr.DesempleadosGet: Integer;
begin
     Result := EmpleadosCuenta( K_DESEMPLEADOS_COUNT );
end;

function TLicenseMgr.UltimaTarjeta: TDate;
begin
     Result := GetUltima( K_ULTIMA_TARJETA );
end;

function TLicenseMgr.UltimaAlta: TDate;
begin
     Result := GetUltima( Format( K_ULTIMA_ALTA, [ ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) ] ) );
end;

function TLicenseMgr.UltimaBaja: TDate;
begin
     Result := GetUltima( Format( K_ULTIMA_BAJA, [ ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) ] ) );
end;

{$ifdef TRESSCFG}
function TLicenseMgr.UltimosProcesos: TDatosUltimosProcesos;
var
   FProcesos: TZetaCursor;
   dInicio, dFinal: TDate;
   i: Integer;


   procedure ObtenerProcesosEmail;
   var
      iUsuarioAutomatico : integer;
   begin
     with oZetaProvider do
     begin
          iUsuarioAutomatico := FDatosModulosComparte.UsuarioAutomatico;
          FProcesos := CreateQuery( Format (  K_ULTIMOS_PROCESOS_REPORTES_EMAIL, [ ord(prReporteador),  DateToStrSQL( dInicio ), DateToStrSQL( dFinal ) , iUsuarioAutomatico] ) );
     end;
     try
        with FProcesos do
        begin
             Active := True;
             while not Eof do
             begin
                  with Result do
                  begin
                       ReportesEmailCuantos := FieldByName( 'CUANTOS' ).AsInteger;
                       ReportesEmailUltimo := FieldByName( 'FECHA' ).AsDateTime;
                  end;
                  Next;
             end;
             Active := False;
        end;
     finally
            FreeAndNil( FProcesos );
     end;
   end;

   procedure ObtenerProcesosConciliadorFonacot;
   begin
     with oZetaProvider do
     begin
          FProcesos := CreateQuery( Format (  K_ULTIMOS_PROCESOS_CONCILIACION_FONACOT, [ ord(prRHImportarAhorrosPrestamos),  DateToStrSQL( dInicio ), DateToStrSQL( dFinal ) ] ) );
     end;
     try
        with FProcesos do
        begin
             Active := True;
             while not Eof do
             begin
                  with Result do
                  begin
                       ConciliacionFonacotCuantos := FieldByName( 'CUANTOS' ).AsInteger;
                       ConciliacionFonacotUltimo := FieldByName( 'FECHA' ).AsDateTime;
                  end;
                  Next;
             end;
             Active := False;
        end;
     finally
            FreeAndNil( FProcesos );
     end;
   end;

begin
     with Result do
     begin
          CalculoIntegradosCuantos := 0;
          CalculoIntegradosUltimo := 0;
          AfectarNominaCuantos := 0;
          AfectarNominaUltimo :=0;
     end;
     dFinal := Date;
     dInicio := dFinal;
     for i := 1 to 3 do
     begin
          dInicio := LastMonth( dInicio );
     end;

     ObtenerProcesosEmail;
     ObtenerProcesosConciliadorFonacot;

     with oZetaProvider do
     begin
          FProcesos := CreateQuery( Format( K_ULTIMOS_PROCESOS, [ Ord( prRHSalarioIntegrado ), Ord( prNOAfectar ),  Ord( prNOAjusteRetFonacot  ), Ord( prNOCalcularPagoFonacot  ),
          Ord( prLabCalcularTiempos  ), DateToStrSQL( dInicio ), DateToStrSQL( dFinal ) ] ) );
     end;
     try
        with FProcesos do
        begin
             Active := True;
             while not Eof do
             begin
                  with Result do
                  begin
                       case Procesos( FieldByName( 'PC_PROC_ID' ).AsInteger ) of
                            prRHSalarioIntegrado:
                            begin
                                 CalculoIntegradosCuantos := FieldByName( 'CUANTOS' ).AsInteger;
                                 CalculoIntegradosUltimo := FieldByName( 'FECHA' ).AsDateTime;
                            end;
                            prNOAfectar:
                            begin
                                 AfectarNominaCuantos := FieldByName( 'CUANTOS' ).AsInteger;
                                 AfectarNominaUltimo := FieldByName( 'FECHA' ).AsDateTime;
                            end;
                            prNOAjusteRetFonacot:
                            begin
                                 AjusteRetFonacotCuantos := FieldByName( 'CUANTOS' ).AsInteger;
                                 AjusteRetFonacotUltimo := FieldByName( 'FECHA' ).AsDateTime;
                            end;
                            prNOCalcularPagoFonacot:
                            begin
                                 CalcularPagoFonacotCuantos := FieldByName( 'CUANTOS' ).AsInteger;
                                 CalcularPagoFonacotUltimo := FieldByName( 'FECHA' ).AsDateTime;
                            end;
                            prLabCalcularTiempos:
                            begin
                                 LabCalcularTiemposCuantos := FieldByName( 'CUANTOS' ).AsInteger;
                                 LabCalcularTiemposUltimo := FieldByName( 'FECHA' ).AsDateTime;
                            end;
                       end;

                  end;
                  Next;
             end;
             Active := False;
        end;
     finally
            FreeAndNil( FProcesos );
     end;



end;

procedure TLicenseMgr.ObtenerConteoRegistros( sTabla, sCampoFecha : string ; var Cuantos : integer; var Ultimo : TDateTime ; iMesesAtras : integer = 0) ;
   const
       K_ULTIMOS_REGISTROS_CON_FECHA  = 'select COUNT(*) as CUANTOS, MAX(%s) as FECHA from %s';
       K_ULTIMOS_REGISTROS_CON_FECHA_DIAS  = 'select COUNT(*) as CUANTOS, MAX(%0:s) as FECHA from %1:s  where %0:s between %2:s and %3:s';
       K_ULTIMOS_REGISTROS_SIN_FECHA = 'select COUNT(*) as CUANTOS from %s';
   var
      FRegistros: TZetaCursor;
      dFinal, dInicio : TDate;
      i : integer;
   begin
     with oZetaProvider do
     begin
          if strLleno( sCampoFecha ) then
          begin
             if ( iMesesAtras > 0 ) then
             begin
                dFinal := Date;
                dInicio := dFinal;
                for i := 1 to iMesesAtras do
                begin
                     dInicio := LastMonth( dInicio );
                end;
                FRegistros := CreateQuery( Format (  K_ULTIMOS_REGISTROS_CON_FECHA_DIAS, [ sCampoFecha, sTabla, DateToStrSQLC(dInicio), DateToStrSQLC(dFinal) ] ) )
             end
             else
                 FRegistros := CreateQuery( Format (  K_ULTIMOS_REGISTROS_CON_FECHA, [ sCampoFecha, sTabla ] ) )
          end
          else
              FRegistros := CreateQuery( Format (  K_ULTIMOS_REGISTROS_SIN_FECHA, [ sTabla ] ) );

     end;
     try
        try
            with FRegistros do
            begin
                 Active := True;
                 while not Eof do
                 begin
                      Cuantos := FieldByName( 'CUANTOS' ).AsInteger;
                      if strLleno( sCampoFecha ) then
                         Ultimo := FieldByName( 'FECHA' ).AsDateTime;
                      Next;
                 end;
                 Active := False;
            end;
        except
            on Error: Exception do
            begin
                Cuantos := 0;
                Ultimo := 0;
            end;
        end;

     finally
            FreeAndNil( FRegistros );
     end;
   end;


function TLicenseMgr.UltimosRegistros: TDatosUltimosRegistros;
var
   iCuantos : integer;
   dFecha : TDateTime;
begin

    ObtenerConteoRegistros( 'ENTRENA', VACIO, iCuantos, dFecha );
    Result.CursosEntrenaCuantos := iCuantos;

    ObtenerConteoRegistros( 'CAF_COME', 'CF_FECHA', iCuantos, dFecha, K_MESES_3DT_HISTORIA );
    Result.CafeteriaCuantos := iCuantos;
    Result.CafeteriaUltimo := dFecha;

    ObtenerConteoRegistros( 'KAR_TOOL', 'KT_FEC_INI', iCuantos, dFecha, K_MESES_3DT_HISTORIA );
    Result.ResguardoCuantos := iCuantos;
    Result.ResguardoUltimo := dFecha;

    ObtenerConteoRegistros( 'CONSULTA', 'CN_FECHA', iCuantos, dFecha, K_MESES_3DT_HISTORIA );
    Result.SMedicosCuantos := iCuantos;
    Result.SMedicosUltimo := dFecha;

{$ifdef MSSQL}
    ObtenerConteoRegistros( 'EMP_PLAN', 'EP_FEC_MOD', iCuantos, dFecha, K_MESES_3DT_HISTORIA );
    Result.PlanCarreraCuantos := iCuantos;
    Result.PlanCarreraUltimo := dFecha;

    ObtenerConteoRegistros( 'EVALUA', 'EV_FEC_INI', iCuantos, dFecha, K_MESES_3DT_HISTORIA );
    Result.EvaluacionCuantos := iCuantos;
    Result.EvaluacionUltimo := dFecha;
{$else}
    iCuantos := 0;
    dFecha := 0;
    Result.PlanCarreraCuantos := iCuantos;
    Result.PlanCarreraUltimo := dFecha;
    Result.EvaluacionCuantos := iCuantos;
    Result.EvaluacionUltimo := dFecha;
{$endif}

end;

function TLicenseMgr.RegistrosSeleccion: TDatosSeleccion;
var
   iCuantos : integer;
   dFecha : TDateTime;
begin
    ObtenerConteoRegistros( 'REQUIERE', 'RQ_FEC_INI', iCuantos, dFecha );
    Result.RequisicionCuantos := iCuantos;
    Result.RequisicionUltimo := dFecha;
end;

function TLicenseMgr.RegistrosVisitantes: TDatosVisitantes;
var
   iCuantos : integer;
   dFecha : TDateTime;
begin
    ObtenerConteoRegistros( 'LIBRO', 'LI_ENT_FEC', iCuantos, dFecha, K_DIAS_3DT_HISTORIA );
    Result.LibroCuantos := iCuantos;
    Result.LibroUltimo := dFecha;
end;


{$ifdef MSSQL}
procedure TLicenseMgr.ObtenerConteoRegistrosComparte( sTabla, sCampoFecha, sCampoTexto, sLikeTexto : string ; var Cuantos : integer; var Ultimo : TDateTime ; iMesesAtras : integer = 0) ;
   const
       K_ULTIMOS_REGISTROS_CON_FECHA  = 'select COUNT(*) as CUANTOS, MAX(%s) as FECHA from %s where 1=1 ';
       K_ULTIMOS_REGISTROS_CON_FECHA_DIAS  = 'select COUNT(*) as CUANTOS, MAX(%0:s) as FECHA from %1:s  where %0:s between %2:s and %3:s ';
       K_ULTIMOS_REGISTROS_SIN_FECHA = 'select COUNT(*) as CUANTOS from %s where 1=1 ';
   var
      FRegistros: TZetaCursor;
      oEmpresa : OleVariant;
      sQuery : string;
      lOK : Boolean;
      dInicio, dFinal : TDate;
      i : integer;
   begin
     lOk := FALSE;
     with oZetaProvider do
     begin
          oEmpresa := EmpresaActiva;
          EmpresaActiva := Comparte;
          if strLleno( sCampoFecha ) then
          begin
             if ( iMesesAtras > 0 ) then
             begin
                dFinal := Date;
                dInicio := dFinal;
                for i := 1 to iMesesAtras do
                begin
                     dInicio := LastMonth( dInicio );

                end;
                sQuery := Format (  K_ULTIMOS_REGISTROS_CON_FECHA_DIAS, [ sCampoFecha, sTabla, DateToStrSQLC(dInicio), DateToStrSQLC(dFinal) ] );
             end
             else
                 sQuery :=  Format (  K_ULTIMOS_REGISTROS_CON_FECHA, [ sCampoFecha, sTabla ] )
          end
          else
              sQuery :=  Format (  K_ULTIMOS_REGISTROS_SIN_FECHA, [ sTabla ] );

          if strLleno( sCampoTexto ) and strLleno( sLikeTexto ) then
             sQuery := Format( '%s and (%s like ''%%%s%%'')', [ sQuery, sCampoTexto, sLikeTexto ]);

          try
             FRegistros := CreateQuery( sQuery);
             lOK := True;
          except
             on Error: Exception do
             begin
                 Cuantos := 0;
                 Ultimo := 0;
             end;
          end;
     end;

     if lOk then
     begin
        try
           try
               with FRegistros do
               begin
                    Active := True;
                    while not Eof do
                    begin
                         Cuantos := FieldByName( 'CUANTOS' ).AsInteger;
                         if strLleno( sCampoFecha ) then
                            Ultimo := FieldByName( 'FECHA' ).AsDateTime;
                         Next;
                    end;
                    Active := False;
               end;
           except
               on Error: Exception do
               begin
                   Cuantos := 0;
                   Ultimo := 0;
               end;
           end;

        finally
               FreeAndNil( FRegistros );
        end;
     end;

     with oZetaProvider do
     begin
          if not  VarIsNull(oEmpresa) then
             EmpresaActiva := oEmpresa;
     end;
end;
{$endif}



function TLicenseMgr.RegistrosModulosComparte: TDatosModulosComparte;
var
   iCuantos : integer;
   dFecha : TDateTime;

   function ObtenerUsuarioAutomatico : integer;
   const
        K_GET_USUARIO_AUTOMATICO = 'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
   var
      oEmpresaActiva : OleVariant;
      FUsuarioAut: TZetaCursor;
   begin
     with oZetaProvider do
     begin
          oEmpresaActiva := EmpresaActiva;
          EmpresaActiva := Comparte;
          Result := 0;

          try
            FUsuarioAut := CreateQuery( Format (  K_GET_USUARIO_AUTOMATICO, [11]) );
            with FUsuarioAut do
            begin
                Active := True;
                if not IsEmpty then
                    Result :=  StrToIntDef( FieldByName( 'CL_TEXTO' ).AsString, 0 );
                Active := False;
            end;
          except
              on Error: Exception do
              begin
                 Result :=0;
              end;
          end;

          if not VarIsNull( oEmpresaActiva ) then
             EmpresaActiva := oEmpresaActiva;
     end;
   end;

begin

    Result.UsuarioAutomatico := ObtenerUsuarioAutomatico;
{$ifdef MSSQL}
    ObtenerConteoRegistrosComparte( 'BITKIOSCO', 'BI_FECHA', '', '', iCuantos, dFecha, K_MESES_3DT_HISTORIA );
    Result.KioscoCuantos :=  iCuantos;
    Result.KioscoUltimo := dFecha;

    ObtenerConteoRegistrosComparte( 'BITKIOSCO', 'BI_FECHA', 'BI_TEXTO', 'Mis Datos', iCuantos, dFecha, K_MESES_3DT_HISTORIA );
    Result.MisDatosCuantos := iCuantos;
    Result.MisDatosUltimo := dFecha;

    ObtenerConteoRegistrosComparte( 'WPROCESO', 'WP_FEC_INI', '', '', iCuantos, dFecha, K_MESES_3DT_HISTORIA );
    Result.WorkflowCuantos := iCuantos;
    Result.WorkflowUltimo := dFecha;
{$else}
    iCuantos := 0;
    dFecha := 0;
    Result.KioscoCuantos :=  iCuantos;
    Result.KioscoUltimo := dFecha;
    Result.MisDatosCuantos := iCuantos;
    Result.MisDatosUltimo := dFecha;
    Result.WorkflowCuantos := iCuantos;
    Result.WorkflowUltimo := dFecha;
{$endif}
    Self.FDatosModulosComparte := Result;
end;




function TLicenseMgr.GetSQLServerVersion: TSQLServerVersion;
{$ifdef MSSQL}
var
   FConsultaVer: TZetaCursor;
begin
  with oZetaProvider do
  begin
       FConsultaVer := CreateQuery( K_OBTIENE_DB_VERSION );
       try
          try
              with FConsultaVer do
              begin
                   Active := TRUE;
                   while not Eof do
                   begin
                        with Result do
                        begin
                             Version := FieldByName( 'VERSION' ).AsString;
                             Nivel := FieldByName( 'LEVEL' ).AsString    ;
                             Edicion := FieldByName( 'EDITION' ).AsString;
                        end;
                        Next;
                   end;
                  Active := FALSE;
              end;
          except
              on Error: Exception do
              begin
                 Result.Version := VACIO;
                 Result.Nivel := VACIO;
                 Result.Edicion := VACIO
              end;
          end;

       finally
              FreeAndNil( FConsultaVer );
       end;
  end;
{$else}
begin
  Result.Version := VACIO;
  Result.Nivel := VACIO;
  Result.Edicion := VACIO;
{$endif}
end;


{$endif}

{$ifdef TRESSCFG}
function TLicenseMgr.UltimaNomina: TDatosPeriodo;
var
   FPeriodos, FPeriodoMax, FFecha: TZetaCursor;
   iYear, iNumero: Integer;
   eTipo: eTipoPeriodo;
   dInicio: TDate;
begin
     with Result do
     begin
          Year := 1980;
          Tipo := Ord(tpDiario);
          Numero := 0;
          Inicio := NullDateTime;
     end;
     dInicio := NullDateTime;
     with oZetaProvider do
     begin
          //FPeriodos := CreateQuery( Format( K_PERIODO_MAXIMOS, [ Ord( spAfectadaTotal ), K_LIMITE_NOM_NORMAL, DateToStrSQL( Now ) ] ) );
          InitGlobales;
          FPeriodos := CreateQuery( Format( K_PERIODO_MAXIMOS, [ Ord( spAfectadaTotal ), GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ), DateToStrSQL( Now ) ] ) );
          try

             FFecha := CreateQuery( K_PERIODO_FECHA );

             try
                with FPeriodos do
                begin
                     Active := True;
                     while not Eof do
                     begin
                          iYear := FieldByName( 'PE_YEAR' ).AsInteger;
                          eTipo := eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger );

                          try
                             FPeriodoMax := CreateQuery( Format( K_PERIODO_MAXIMO, [iYear, Ord(eTipo), Ord( spAfectadaTotal ), GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ), DateToStrSQL( Now ) ] ) );
                             FPeriodoMax.Active := True;
                             iNumero := FPeriodoMax.FieldByName( 'PE_NUMERO' ).AsInteger;
                             FPeriodoMax.Active := False;
                          finally
                             FreeAndnil( FPeriodoMax );
                          end;


                          ParamAsInteger( FFecha, 'Year', iYear );
                          ParamAsInteger( FFecha, 'Tipo', Ord( eTipo ) );
                          ParamAsInteger( FFecha, 'Numero', iNumero );
                          with FFecha do
                          begin
                               Active := True;
                               if ( dInicio < FieldByName( 'PE_FEC_INI' ).AsDateTime  ) then
                               begin
                                    dInicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
                                    with Result do
                                    begin
                                         Year := iYear;
                                         Tipo := eTipo;
                                         Numero := iNumero;
                                         Inicio := dInicio;
                                    end;
                                    {
                                    Result := Format( '%s ( %s )', [ ZetaCommonTools.GetPeriodoInfo( iYear, iNumero, eTipo ), ZetaCommonTools.FechaCorta( dInicio ) ] );
                                    }
                               end;
                               Active := False;
                          end;
                          Next;
                     end;
                     Active := False;
                end;
             finally
                    FreeAndNil( FFecha );
             end;
          finally
                 FreeAndNil( FPeriodos );
          end;
     end;
end;


function TLicenseMgr.GetUltimasNominasXML: string;
var
   FPeriodos, FPeriodoInfo, FPeriodoMax: TZetaCursor;
   iYear, iNumero, iCantidad, iEmpleados: Integer;
   eTipo: eTipoPeriodo;
   sDescPeriodo, sFormulaOrdinarios : string;
   slXML : TStringList;
begin
     slXML := TStringList.Create;

     slXML.Add('<Periodos>');
     with oZetaProvider do
     begin
          //FPeriodos := CreateQuery( Format( K_PERIODO_MAXIMOS, [ Ord( spAfectadaTotal ), K_LIMITE_NOM_NORMAL, DateToStrSQL( Now ) ] ) );
          InitGlobales;
          FPeriodos := CreateQuery( Format( K_PERIODO_MAXIMOS, [ Ord( spAfectadaTotal ), GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ), DateToStrSQL( Now ) ] ) );
          try
          //select PE_FEC_INI, PE_NUM_EMP, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET from PERIODO where ( PE_YEAR = :Year ) and ( PE_TIPO = :Tipo ) and ( PE_NUMERO = :Numero )
             FPeriodoInfo := CreateQuery( K_PERIODO_INFO );
             try
                with FPeriodos do
                begin
                     Active := True;
                     while not Eof do
                     begin
                          iYear := FieldByName( 'PE_YEAR' ).AsInteger;
                          eTipo := eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger );

                          try
                             FPeriodoMax := CreateQuery( Format( K_PERIODO_MAXIMO, [iYear, Ord(eTipo), Ord( spAfectadaTotal ), GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ), DateToStrSQL( Now ) ] ) );
                             FPeriodoMax.Active := True;
                             iNumero := FPeriodoMax.FieldByName( 'PE_NUMERO' ).AsInteger;
                             FPeriodoMax.Active := False;
                          finally
                             FreeAndnil( FPeriodoMax );
                          end;


                          sDescPeriodo := FieldByName( 'TP_DESCRIP' ).AsString;
                          sFormulaOrdinarios := FieldByName( 'TP_DIAS_EV' ).AsString;
                          iCantidad := FieldByName( 'CANTIDAD' ).AsInteger;

                          ParamAsInteger( FPeriodoInfo, 'Year', iYear );
                          ParamAsInteger( FPeriodoInfo, 'Tipo', Ord( eTipo ) );
                          ParamAsInteger( FPeriodoInfo, 'Numero', iNumero );
                          with FPeriodoInfo do
                          begin
                               Active := True;
                               iEmpleados :=  FPeriodoInfo.FieldByName('PE_NUM_EMP').AsInteger;
                               slXML.Add( Format( '<Periodo Tipo="%d" Cantidad="%d"  Year="%d" Numero="%d" Empleados="%d" >',[Ord(eTipo), iCantidad, iYear, iNumero, iEmpleados ]) );
                               slXML.Add( Format( '<FormulaOrdinarios><![CDATA[%s]]></FormulaOrdinarios>',[sFormulaOrdinarios]) )  ;
                               slXML.Add(         '</Periodo>' );
                               Active := False;
                          end;
                          Next;
                     end;
                     Active := False;
                end;
             finally
                    FreeAndNil( FPeriodoInfo );
             end;
          finally
                 FreeAndNil( FPeriodos );
          end;
     end;
     slXML.Add('</Periodos>');

     Result := slXML.Text;

     FreeAndNil( slXML );
end;



function TLicenseMgr.GetChecadasAsistenciaXML( iDiasAtras : integer ): string;
var
   FChecadasGroup : TZetaCursor;
   sReloj : string;
   iCantidad : integer;
   dFecha : TDateTime;
   slXML : TStringList;
begin
     slXML := TStringList.Create;

     slXML.Add('<Checadas>');

     with oZetaProvider do
     begin
          FChecadasGroup := CreateQuery( Format( K_CHECADAS_ASISTENCIA, [ DateToStrSQL( Date - iDiasAtras ) ] ) );
          try
             with FChecadasGroup do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       sReloj := FieldByName( 'CH_RELOJ' ).AsString;
                       iCantidad := FieldByName( 'CANTIDAD' ).AsInteger;
                       dFecha := FieldByName( 'FECHA' ).AsDateTime;
                       slXML.Add( Format( '<Reloj Id="%s"  Cantidad="%d" Fecha="%s"/>',[sReloj, iCantidad,  FormatDateTime( K_FECHA, dFecha ) ]) );
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FChecadasGroup );
          end;
     end;
     slXML.Add('</Checadas>');

     Result := slXML.Text;

     FreeAndNil( slXML );
end;

{$endif}

{ Escribir Asignacion de Licencias de Empleados }

function TLicenseMgr.AsignadosSet(const iEmpleados: Integer): Boolean;
var
   i, iValor, iCheckSum: Integer;
   sValor: String;
   FBorrar, FQuery: TZetaCursor;
   FLista: TStrings;
begin
     Result := False;
     iCheckSum := -1;
     Randomize;
     with oZetaProvider do
     begin
          FQuery := CreateQuery( K_GLOBAL_WRITE );
          try
             FBorrar := CreateQuery( K_GLOBAL_DELETE );
             try
                EmpiezaTransaccion;
                try
                   for i := 1 to K_GLOBALES do
                   begin
                        if ( i = K_GLOBAL ) then
                        begin
                             iValor := iEmpleados;
                             iCheckSum := GetCheckSum( iValor );
                        end
                        else
                        begin
                             { Dummys }
                             iValor := Trunc( ( K_TOPE / K_RANDOM ) ) * Random( K_RANDOM );
                        end;
                        sValor := IntToHex( K_TOPE - iValor, 7 );
                        ParamAsInteger( FBorrar, 'Codigo', -i );
                        Ejecuta( FBorrar );
                        ParamAsInteger( FQuery, 'GL_CODIGO', -i );
                        ParamAsVarChar( FQuery, 'GL_FORMULA', sValor, K_ANCHO_FORMULA );
                        Ejecuta( FQuery );
                   end;
                except
                      on Error: Exception do
                      begin
                           TerminaTransaccion( False );
                           raise;
                      end;
                end;
                { Escribir el CheckSum }
                if ( iCheckSum >= 0 ) then
                begin
                     { Primero Hay Que Borrar Todos Los Dominios }
                     FLista := TStringList.Create;
                     try
                        PreparaQuery( FQuery, Format( K_DOMAIN_READ_ALL, [ GetDomainNameWildCard ] ) );
                        with FQuery do
                        begin
                             Active := True;
                             while not Eof do
                             begin
                                  FLista.Add( FieldByName( 'DOMINIO' ).AsString );
                                  Next;
                             end;
                             Active := False;
                        end;
                        with FLista do
                        begin
                             for i := 0 to ( Count - 1 ) do
                             begin
                                  EmpiezaTransaccion;
                                  try
                                     PreparaQuery( FBorrar, Format( K_DOMAIN_DROP, [ Strings[ i ] ] ) );
                                     Ejecuta( FBorrar );
                                     TerminaTransaccion( True );
                                  except
                                        on Error: Exception do
                                        begin
                                             TerminaTransaccion( False );
                                             raise;
                                        end;
                                  end;
                             end;
                        end;
                     finally
                            FreeAndNil( FLista );
                     end;
                     EmpiezaTransaccion;
                     try
                        { Luego Hay que Guardar el CheckSum en el Domain }
                        PreparaQuery( FQuery, Format( K_DOMAIN_ADD, [ GetDomainName( iEmpleados, AutoServer.NumeroSerie ), ( iCheckSum + K_DOMAIN_OFFSET ) ] ) );
                        Ejecuta( FQuery );
                        TerminaTransaccion( True );
                        Result := True;
                     except
                           on Error: Exception do
                           begin
                                TerminaTransaccion( False );
                                raise;
                           end;
                     end;
                end;
             finally
                    FreeAndNil( FBorrar );
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;

{$ifdef DEBUGSENTINEL}
procedure TLicenseMgr.AutoGetData( Sender: TObject; var StringData: String );
var
   oEmpresaActiva : Variant;
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          oEmpresaActiva := EmpresaActiva;
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( Format( Q_VA_LEER_CLAVES, [ CLAVE_AUTORIZACION ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  if EOF then
                     StringData := ''
                  else
                      StringData :=  FieldByName( 'CL_TEXTO' ).AsString;
                  Active := False;
             end;
          finally
                 if not VarIsNull( oEmpresaActiva ) then
                    EmpresaActiva := oEmpresaActiva;
                 FreeAndNil( FDataset );
          end;
     end;
end;

procedure TLicenseMgr.AutoSetData( Sender: TObject; var StringData: String );
var
   oEmpresaActiva : Variant;
   {$ifdef TEST}
   iEmpleadosGlobal : integer;
   {$endif}
begin
     with oZetaProvider do
     begin
          oEmpresaActiva := EmpresaActiva;
          EmpresaActiva := Comparte;
          {$ifdef TESTXX}
          iEmpleadosGlobal := EmpleadosGetGlobal;
          {$endif}
          try
             EmpiezaTransaccion;
             try
                ExecSQL( Comparte, Format( Q_VA_BORRA_CLAVES, [ CLAVE_AUTORIZACION ] ) );
                ExecSQL( Comparte, Format( Q_VA_INSERTA_CLAVES, [ CLAVE_AUTORIZACION, StringData ] ) );

             {$ifdef TESTXX}
                ExecSQL( Comparte, Format( Q_VA_BORRA_CLAVES, [  1234  ] ) );
                ExecSQL( Comparte, Format( Q_VA_INSERTA_CLAVES, [ 1234, Format('%d',[iEmpleadosGlobal]) ] ) );
             {$endif}

                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
//                 if ( oEmpresaActiva <> NULL ) then
                 if not VarIsNull( oEmpresaActiva ) then
                    EmpresaActiva := oEmpresaActiva;
          end;
     end;
end;

procedure TLicenseMgr.DeleteData(Sender: TObject);
var
   oEmpresaActiva : Variant;
begin
     with oZetaProvider do
     begin
          oEmpresaActiva := EmpresaActiva;
          EmpresaActiva := Comparte;
          try
             EmpiezaTransaccion;
             try
                ExecSQL( Comparte, Format( Q_VA_BORRA_CLAVES, [ CLAVE_AUTORIZACION ] ) );
                TerminaTransaccion( True );
             except
                   on Error: Exception do
                   begin
                        RollBackTransaccion;
                        raise;
                   end;
             end;
          finally
                 if not VarIsNull( oEmpresaActiva ) then
                    EmpresaActiva := oEmpresaActiva;
          end;
     end;
end;
{$endif}

{$ifdef VALIDAEMPLEADOSGLOBAL}
function TLicenseMgr.EmpleadosGetGlobal: Integer;
const
     K_ANCHO_EMPRESA = 10;
     K_ANCHO_PROBLEMA = 80;
var
   iEmpleado, i, iLow, iHigh, iEmpresas, iCantidadEmpleados     {$ifdef MSSQL}, iTimeOut {$endif}: Integer;
   oLista, oEmpresa: OleVariant;
   sBDActual : string;
   FDataSet : TZetaCursor;
   oProvider: TdmZetaServerProvider;
   {$ifdef TEST}
   FLog: TStrings;
   {$endif}
   FBBDDRevisadas : TStringList;

   procedure InitLog;
   begin
   {$ifdef TEST}
        FLog := TStringList.Create;
   {$endif}
   end;

   procedure CloseLog;
   begin
   {$ifdef TEST}
        FLog.SaveToFile('C:\TEMP\Contabilizacion_tmp.txt');
        FreeAndNil( FLog );
   {$endif}
   end;

   procedure WriteLog( const iEmpleado: Integer; const sMensaje, sError: String );
   begin
   {$ifdef TEST}
        FLog.Add( Format( '%s: %s', [ sMensaje, sError ] ) );
        if Assigned( oProvider ) then
        begin
             oProvider.EscribeBitacora( tbError,
                                        clbNumTarjeta,
                                        iEmpleado,
                                        Date,
                                        sMensaje,
                                        sError );
        end;
   {$endif}
   end;

   procedure WriteException( const iEmpleado: Integer; const sMensaje: String; Error: Exception );
   begin
        WriteLog( iEmpleado, sMensaje, Error.Message );
   end;


   function BuildEmpresaDBInfo(DataSet: TZetaCursor): OleVariant;
     { Es una copia de DBaseCliente.BuildEmpresa }
     {
     Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

     P_ALIAS = 0;
     P_DATABASE = 0;
     P_USER_NAME = 1;
     P_PASSWORD = 2;
     P_USUARIO = 3;
     P_NIVEL_0 = 4;
     P_CODIGO = 5;
     }
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'DB_DATOS' ).AsString,
                                  FieldByName( 'DB_USRNAME' ).AsString,
                                  FieldByName( 'DB_PASSWRD' ).AsString,
                                  0,
                                  FieldByName( 'DB_NIVEL0' ).AsString,
                                  FieldByName( 'DB_CODIGO' ).AsString ] );
     end;
end;

   procedure ObtieneListaEmpresas;
   var
     oEmpresaActiva : Variant;
   begin
        oEmpresaActiva :=  Self.oZetaProvider.EmpresaActiva;
        with  Self.oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             FDataSet := CreateQuery( Format(  Q_DBS_COUNT , [ GetTipoCompanyBD( Ord( tc3Datos ) ) ] ) );
             try
                with FDataSet do
                begin
                     Active := True;
                     iEmpresas := Fields[ 0 ].AsInteger;
                     Active := False;
                end;
             finally
                    FreeAndNil( FDataset );
             end;
             if ( iEmpresas = 0 ) then
                ZetaCommonTools.SetOLEVariantToNull( oLista )
             else
             begin
                  oLista := VarArrayCreate( [ 1, iEmpresas, 1, 2 ], varVariant );
                  VarArrayLock( oLista );
                  try
                     iEmpresas := 1;
                     FDataset := CreateQuery( Format( Q_DBS_LIST , [ GetTipoCompanyBD( Ord( tc3Datos ) ) ] ) );
                     try
                        with FDataSet do
                        begin
                             Active := True;
                             while not Eof do
                             begin
                                  oLista[ iEmpresas, 1 ] := FieldByName( 'DB_DESCRIP' ).AsString;
                                  oLista[ iEmpresas, 2 ] := BuildEmpresaDBInfo( FDataset );
                                  Inc( iEmpresas );
                                  Next;
                             end;
                             Active := False;
                        end;
                     finally
                            FreeAndNil( FDataSet );
                     end;
                  finally
                         VarArrayUnlock( oLista );
                  end;
             end;
             EmpresaActiva :=  oEmpresaActiva;
        end;
   end;


   function ContabilizarEmpleados : integer ;
   begin
        { Se buscan los Empleados de la Empresa }
        Result := 0;
        FDataSet := nil;

        try
           FDataSet := oProvider.CreateQuery( Format(  K_EMPLEADOS_COUNT, [ ZetaCommonTools.EntreComillas( K_GLOBAL_SI ) ] ) );

            try
          with FDataSet do
           begin
                try
                   try
                      Active := True;
                      while not EOF do
                      begin
                           Result := FieldByName( 'CUANTOS' ).AsInteger;
                           Next;
                      end;
                   except
                         on Error: Exception do
                         begin
                              WriteException( iEmpleado, 'Error Al Contabilizar Empleados', Error );
                         end;
                   end;
                finally
                       Active := False;
                end;
           end;
        finally
               FreeAndNil( FDataset );
        end;


        except
              on Error: Exception do
              begin
                   WriteException( iEmpleado, 'Error Al Contabilizar Empleados', Error );
              end;
        end;


   end;

   { ** US #18732 Bitacora de uso de Sistema TRESS - Versi�n 2018 ** }
   procedure TressAnalitica;
   var FQuery: TZetaCursor;
   begin
        try
           with oProvider do
           begin
                FQuery := CreateQuery (K_ANALITICA_POBLAR);
                try
                   // Ejecuci�n de SP principal para Tress Anal�tica (T_Analitica)
                   EmpiezaTransaccion;
                   try
                      Ejecuta( FQuery );
                      TerminaTransaccion( True );
                      // Result := True;
                   except
                         on Error: Exception do
                         begin
                              TerminaTransaccion( False );
                              WriteException( iEmpleado, 'Error al generar bit�cora de uso de Sistema TRESS', Error );
                              // raise;
                         end;
                   end;
                finally
                       FreeAndNil (FQuery);
                end;
           end;
        except
              on Error: Exception do
              begin
                   WriteException( iEmpleado, 'Error al generar bit�cora de uso de Sistema TRESS', Error );
              end;
        end;
   end;

   procedure InitQuerys;
   begin
   end;

   procedure FreeQuerys;
   begin
   end;

begin

     FBBDDRevisadas := TStringList.Create;
     Result := 0;
     {$ifdef MSSQL}
     iTimeOut := 15;
     {$endif}

     {$ifdef TEST}
     InitLog;
     with FLog do
     begin
          Add( Format( '**** Inicio de Actualizaci�n de Tarjetas: %s ****', [ FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) ] ) );
          Add( '' );
     end;
     {$endif}

     try
          { Se llena el OleVariant oLista }
          try
               ObtieneListaEmpresas;
          except
               on Error: Exception do
               begin
                    WriteException( 0, 'Error Al Leer Lista De Empresas', Error );
                    ZetaCommonTools.SetOLEVariantToNull( oLista )
               end;
          end;


          { Se Verifica que se tengan empresas con Digito }
          if not VarIsNull( oLista ) then
          begin
               iLow := VarArrayLowBound( oLista, 1 );
               iHigh := VarArrayHighBound( oLista, 1 );
               InitQuerys;
               sBDActual := VACIO;
               try
                    for i := iLow to iHigh do
                    begin
                         iEmpleado := 0;
                         oEmpresa := oLista[ i, 2 ];
                         sBDActual := oEmpresa[0];
                         sBDActual := UpperCase( sBDActual );

                         if ( FBBDDRevisadas.IndexOf( sBDActual ) < 0 ) then
                         begin
                            oProvider := TdmZetaServerProvider.Create( nil );
                            try
                                 try
                                 {$ifdef MSSQLx}
                                      iTimeOut := oProvider.HdbcEmpresa.LoginTimeOut;
                                      oProvider.HdbcEmpresa.LoginTimeOut := 5;
                                 {$endif}
                                      oProvider.EmpresaActiva := oEmpresa;
                                      iCantidadEmpleados := ContabilizarEmpleados;

                                      { ** US #18732 Bitacora de uso de Sistema TRESS - Versi�n 2018 ** }
                                      TressAnalitica;

                                      {$ifdef TEST}
                                      with FLog do
                                      begin
                                           Add( Format( 'Comparte Timeout=%d',[oProvider.HdbcComparte.LoginTimeOut] ) );
                                           Add( Format( 'Empresa Timeout=%d,  Empleados=%d',[oProvider.HdbcEmpresa.LoginTimeOut, iCantidadEmpleados] ) );
                                           Add( Format( '--- Revisi�n de Licencia de Empleados De La Empresa: %s ---', [ oProvider.CodigoEmpresaActiva ] ) );
                                           Add('');
                                      end;
                                      {$endif}

                                      Result := Result + iCantidadEmpleados;                                    { M�todo para Actualizar los Empleados de Cada Empresa }
                                      FBBDDRevisadas.Add( sBDActual );
                                 finally
                                   {$ifdef MSSQLx}
                                        oProvider.HdbcEmpresa.LoginTimeOut := iTimeOut;
                                   {$endif}
                                        FreeAndNil( oProvider );
                                 end;
                            except                                                           //Si falla el EmpresaActiva escribe error en log pero contin�a
                                 on Error: Exception do                                      //Escribe error en log
                                      WriteException( 0, '--- Error Al Contablizar Empleados --- ', Error );
                            end;


                         end;
                    end;
               finally
                      FreeQuerys;                                                              //Termina thread en OK
               end;
          end;
     except
           on Error: Exception do
           begin
                FreeAndNil( oProvider );
                WriteException( 0, 'Error Al Actualizar Tarjetas', Error );
           end;
     end;

    {$ifdef TEST}
     with FLog do
     begin
          Add( '' );
          Add( Format( '**** Fin de Actualizaci�n de Tarjetas:    %s ****', [ FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) ] ) );
     end;
     CloseLog;
     {$endif}

     Self.EmpleadosConteo := Result;

end;

// US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
function TLicenseMgr.GetGlobalMostrarAdvertencia: Boolean;
const
 K_GET_MOSTRAR_ADVERTENCIA =
   'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
var
 oEmpresaActiva: OleVariant;
 FMostrarAdvertencia: TZetaCursor;
begin
 with oZetaProvider do
 begin
  oEmpresaActiva := EmpresaActiva;
  EmpresaActiva := Comparte;
  Result := True;
  try
   FMostrarAdvertencia := CreateQuery(Format(K_GET_MOSTRAR_ADVERTENCIA, [12]));
   try
    with FMostrarAdvertencia do
    begin
     Active := True;
     if not IsEmpty then
      Result := StrToBoolDef(FieldByName('CL_TEXTO').AsString, True);
     Active := False;
    end;
   finally
    FreeAndNil(FMostrarAdvertencia);
   end;
  except
   on Error: Exception do
   begin
    Result := True;
   end;
  end;

  if not VarIsNull(oEmpresaActiva) then
   EmpresaActiva := oEmpresaActiva;
 end;
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TLicenseMgr.GetGlobalServidorCorreos: String;
const
 K_GET_GLOBAL_CONFIG =
   'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
var
 oEmpresaActiva: OleVariant;
 FObtenerGlobalConfEmail: TZetaCursor;
begin
 with oZetaProvider do
 begin
  oEmpresaActiva := EmpresaActiva;
  EmpresaActiva := Comparte;
  Result := VACIO;
  try
   FObtenerGlobalConfEmail := CreateQuery(Format(K_GET_GLOBAL_CONFIG, [13]));
   try
    with FObtenerGlobalConfEmail do
    begin
     Active := True;
     if not IsEmpty then
      Result := FieldByName('CL_TEXTO').AsString;
     Active := False;
    end;
   finally
    FreeAndNil(FObtenerGlobalConfEmail);
   end;
  except
   on Error: Exception do
   begin
    Result := VACIO;
   end;
  end;

  if not VarIsNull(oEmpresaActiva) then
   EmpresaActiva := oEmpresaActiva;
 end;
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TLicenseMgr.GetGlobalPuertoSMTP: Integer;
const
 K_GET_GLOBAL_CONFIG =
   'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
 K_PUERTO_DEFAULT = 25;
var
 oEmpresaActiva: OleVariant;
 FObtenerGlobalConfEmail: TZetaCursor;
begin
 with oZetaProvider do
 begin
  oEmpresaActiva := EmpresaActiva;
  EmpresaActiva := Comparte;
  Result := K_PUERTO_DEFAULT;
  try
   FObtenerGlobalConfEmail := CreateQuery(Format(K_GET_GLOBAL_CONFIG, [14]));
   try
    with FObtenerGlobalConfEmail do
    begin
     Active := True;
     if not IsEmpty then
      Result := StrToIntDef( FieldByName('CL_TEXTO').AsString, K_PUERTO_DEFAULT );
     Active := False;
    end;
   finally
    FreeAndNil(FObtenerGlobalConfEmail);
   end;
  except
   on Error: Exception do
   begin
    Result := K_PUERTO_DEFAULT;
   end;
  end;

  if not VarIsNull(oEmpresaActiva) then
   EmpresaActiva := oEmpresaActiva;
 end;
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TLicenseMgr.GetGlobalAutentificacionCorreo: Integer;
const
 K_GET_GLOBAL_CONFIG =
   'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
 K_AUTH_DEFAULT = 0;
var
 oEmpresaActiva: OleVariant;
 FObtenerGlobalConfEmail: TZetaCursor;
begin
 with oZetaProvider do
 begin
  oEmpresaActiva := EmpresaActiva;
  EmpresaActiva := Comparte;
  Result := K_AUTH_DEFAULT;
  try
   FObtenerGlobalConfEmail := CreateQuery(Format(K_GET_GLOBAL_CONFIG, [15]));
   try
    with FObtenerGlobalConfEmail do
    begin
     Active := True;
     if not IsEmpty then
      Result := StrToIntDef( FieldByName('CL_TEXTO').AsString, K_AUTH_DEFAULT );
     Active := False;
    end;
   finally
    FreeAndNil(FObtenerGlobalConfEmail);
   end;
  except
   on Error: Exception do
   begin
    Result := K_AUTH_DEFAULT;
   end;
  end;

  if not VarIsNull(oEmpresaActiva) then
   EmpresaActiva := oEmpresaActiva;
 end;
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TLicenseMgr.GetGlobalUserID: String;
const
 K_GET_GLOBAL_CONFIG =
   'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
var
 oEmpresaActiva: OleVariant;
 FObtenerGlobalConfEmail: TZetaCursor;
begin
 with oZetaProvider do
 begin
  oEmpresaActiva := EmpresaActiva;
  EmpresaActiva := Comparte;
  Result := VACIO;
  try
   FObtenerGlobalConfEmail := CreateQuery(Format(K_GET_GLOBAL_CONFIG, [16]));
   try
    with FObtenerGlobalConfEmail do
    begin
     Active := True;
     if not IsEmpty then
      Result := FieldByName('CL_TEXTO').AsString;
     Active := False;
    end;
   finally
    FreeAndNil(FObtenerGlobalConfEmail);
   end;
  except
   on Error: Exception do
   begin
    Result := VACIO;
   end;
  end;

  if not VarIsNull(oEmpresaActiva) then
   EmpresaActiva := oEmpresaActiva;
 end;
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TLicenseMgr.GetGlobalEmailPSWD: String;
const
 K_GET_GLOBAL_CONFIG =
   'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
var
 oEmpresaActiva: OleVariant;
 FObtenerGlobalConfEmail: TZetaCursor;
begin
 with oZetaProvider do
 begin
  oEmpresaActiva := EmpresaActiva;
  EmpresaActiva := Comparte;
  Result := VACIO;
  try
   FObtenerGlobalConfEmail := CreateQuery(Format(K_GET_GLOBAL_CONFIG, [17]));
   try
    with FObtenerGlobalConfEmail do
    begin
     Active := True;
     if not IsEmpty then
      Result := FieldByName('CL_TEXTO').AsString;
     Active := False;
    end;
   finally
    FreeAndNil(FObtenerGlobalConfEmail);
   end;
  except
   on Error: Exception do
   begin
    Result := VACIO;
   end;
  end;

  if not VarIsNull(oEmpresaActiva) then
   EmpresaActiva := oEmpresaActiva;
 end;
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TLicenseMgr.GetGlobalRecibirAdvertenciaPorEmail: Boolean;
const
 K_GET_GLOBAL_CONFIG =
   'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
var
 oEmpresaActiva: OleVariant;
 FObtenerGlobalConfEmail: TZetaCursor;
begin
 with oZetaProvider do
 begin
  oEmpresaActiva := EmpresaActiva;
  EmpresaActiva := Comparte;
  Result := True;
  try
   FObtenerGlobalConfEmail := CreateQuery(Format(K_GET_GLOBAL_CONFIG, [18]));
   try
    with FObtenerGlobalConfEmail do
    begin
     Active := True;
     if not IsEmpty then
      Result := StrToBoolDef(FieldByName('CL_TEXTO').AsString, True);
     Active := False;
    end;
   finally
    FreeAndNil(FObtenerGlobalConfEmail);
   end;
  except
   on Error: Exception do
   begin
    Result := True;
   end;
  end;

  if not VarIsNull(oEmpresaActiva) then
   EmpresaActiva := oEmpresaActiva;
 end;
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TLicenseMgr.GetGlobalGrupoUsuariosEmail: Integer;
const
 K_GET_GLOBAL_CONFIG =
   'select CL_TEXTO from CLAVES where CL_NUMERO = %d ';
 K_GRUPO_DEFAULT = 0;
var
 oEmpresaActiva: OleVariant;
 FObtenerGlobalConfEmail: TZetaCursor;
begin
 with oZetaProvider do
 begin
  oEmpresaActiva := EmpresaActiva;
  EmpresaActiva := Comparte;
  Result := K_GRUPO_DEFAULT;
  try
   FObtenerGlobalConfEmail := CreateQuery(Format(K_GET_GLOBAL_CONFIG, [19]));
   try
    with FObtenerGlobalConfEmail do
    begin
     Active := True;
     if not IsEmpty then
      Result := StrToIntDef( FieldByName('CL_TEXTO').AsString, K_GRUPO_DEFAULT );
     Active := False;
    end;
   finally
    FreeAndNil(FObtenerGlobalConfEmail);
   end;
  except
   on Error: Exception do
   begin
    Result := K_GRUPO_DEFAULT;
   end;
  end;

  if not VarIsNull(oEmpresaActiva) then
   EmpresaActiva := oEmpresaActiva;
 end;
end;

// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
function TLicenseMgr.GetUsuariosEmailSeguridad: TObjectList;
const
     K_USUARIOS_INFO = 'SELECT US_CODIGO, US_EMAIL, US_FORMATO, US_ACTIVO, US_NOMBRE, US_CORTO  FROM USUARIO WHERE US_ACTIVO = ''S'' AND ( GR_CODIGO = :GRUPO_DEFAULT  OR GR_CODIGO = :GRUPO_ADICIONAL )  AND US_EMAIL <> ''''';
     K_GRUPO_DEFAULT = 1;
var
   oEmpresaActiva: OleVariant;
   FUsuariosEmailSQL: TZetaCursor;
   DatosUsuarioEmail : TDatosUsuarioEmail;
   ListaDatosUsuarioEmail: TObjectList;
begin
     ListaDatosUsuarioEmail := TObjectList.Create(true);
     with oZetaProvider do
     begin
          oEmpresaActiva := EmpresaActiva;
          EmpresaActiva := Comparte;
          try
             FUsuariosEmailSQL := CreateQuery( K_USUARIOS_INFO );
             with FUsuariosEmailSQL do
             begin
                  ParamAsInteger( FUsuariosEmailSQL, 'GRUPO_DEFAULT', K_GRUPO_DEFAULT );
                  ParamAsInteger( FUsuariosEmailSQL, 'GRUPO_ADICIONAL',  GetGlobalGrupoUsuariosEmail );
                  Active := True;
                  while not Eof do
                  begin
                       DatosUsuarioEmail := TDatosUsuarioEmail.Create;
                       DatosUsuarioEmail.UsCodigo := FUsuariosEmailSQL.FieldByName('US_CODIGO').AsInteger;
                       DatosUsuarioEmail.UsEmail := FUsuariosEmailSQL.FieldByName('US_EMAIL').AsString;
                       DatosUsuarioEmail.UsFormato := FUsuariosEmailSQL.FieldByName('US_FORMATO').AsInteger;
                       DatosUsuarioEmail.UsActivo := FUsuariosEmailSQL.FieldByName('US_ACTIVO').AsString;
                       DatosUsuarioEmail.UsNombre := FUsuariosEmailSQL.FieldByName('US_NOMBRE').AsString;
                       DatosUsuarioEmail.UsCorto := FUsuariosEmailSQL.FieldByName('US_CORTO').AsString;
                       ListaDatosUsuarioEmail.Add(DatosUsuarioEmail);
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FUsuariosEmailSQL );
          end;
     end;
     Result := ListaDatosUsuarioEmail;
end;
{$ENDIF}

procedure TLicenseMgr.SetAutoServer(value: TAutoServer);
begin
    FAutoServer := value;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    FGlobalLicense.AutoServer :=  value;
{$endif}
end;

function TLicenseMgr.GetTipoValidoCompany : eTipoCompany;
var
   FDataset: TZetaCursor;
   oEmpresaActiva : Variant;
   sCodigoEmpresa: string;
   sCM_CONTROL, sCM_CTRL_RL, sCM_CHKSUM, sCM_DATOS : string;
   sCHECK_Company, sCHECK_ControlReal, sCHECK_Control, sCHECK_Datos : string;
   dFecha : TDate;

begin
      oEmpresaActiva :=  Self.oZetaProvider.EmpresaActiva;
      sCodigoEmpresa := Self.oZetaProvider.CodigoEmpresaActiva;
      with  Self.oZetaProvider do
      begin
           EmpresaActiva := Comparte;
           FDataSet := CreateQuery( Format(  Q_GET_TIPO_REAL_COMPANY , [ sCodigoEmpresa ] ) );
           try
              with FDataSet do
              begin
                   Active := True;
                   sCM_CONTROL := FieldByName('CM_CONTROL').AsString;
                   sCM_CTRL_RL := FieldByName('CM_CTRL_RL').AsString;
                   sCM_CHKSUM  := FieldByName('CM_CHKSUM').AsString;
                   sCM_DATOS   := FieldByName('CM_DATOS').AsString;
                   Active := False;
              end;
           finally
                  FreeAndNil( FDataset );
           end;
           EmpresaActiva := oEmpresaActiva;
      end;

      Result := GetTipoCompanyEnum( sCM_CONTROL ) ;

      if ( Result in [tc3Datos, tcPresupuesto] ) then
      begin
            GetCompanyDataFromChecksum( sCM_CHKSUM, sCHECK_Company, sCHECK_ControlReal, sCHECK_Control, sCHECK_Datos, dFecha );

            if ( sCM_CTRL_RL = sCHECK_ControlReal ) and ( sCM_DATOS = sCHECK_Datos ) then
            begin
                 Result :=  GetTipoCompanyEnum( sCM_CTRL_RL );
            end
            else
            begin
                 Result := tc3Prueba;
            end;
      end;
end;

procedure TLicenseMgr.AutoOnGetDataAdditional(Sender: TObject);
begin
     TAutoServer( Sender ).EmpleadosConteo := FEmpleadosConteo;
end;


{$ifdef TRESSCFG}
function TLicenseMgr.GetPeriodosXMLStr: string;
begin
Result := GetUltimasNominasXML;
end;

function TLicenseMgr.GetRazonesSocialesXMLStr (sCM_CTRL_RL: String): string;
var
  FQuery: TZetaCursor;
  sRazonSocialtag : String;
begin
    Result := '';
    if ObtieneElemento (lfTipoCompany, ord (tc3Prueba)) <> sCM_CTRL_RL then
    begin
       Result := '<RazonesSociales>';
       with oZetaProvider do
          begin
             FQuery := CreateQuery( K_RAZONES_SOCIALES_READ  );
             try
                 with FQuery do
                 begin
                      Active := True;
                      while not Eof do
                      begin
                          sRazonSocialtag :=  Format( '<RazonSocial Nombre="%s" RFC="%s" ContID="%d" Empleados="%d" Timbres="%d"></RazonSocial>',
                                  [ FieldByName( 'NOMBRE' ).AsString,
                                    FieldByName( 'RFC' ).AsString,
                                    FieldByName( 'CONTID' ).AsInteger,
                                    FieldByName( 'EMPLEADOS' ).AsInteger,
                                    FieldByName( 'TIMBRES' ).AsInteger] ) ;

                          Result := Result + sRazonSocialtag + CR_LF;
                          Next;
                      end;
                      Active := False;
                 end;
             except
                  on Error: Exception do
                  begin
                      // Result := VACIO;
                  end;
             end;
          end;
        Result := Result + '</RazonesSociales>';
    end;
end;

function TLicenseMgr.GetNominaXMLStr (sCM_CTRL_RL: String): string;
var
   sConceptos, sFormula : string;
   iConcepto : integer;

   function GetConceptoISPT : string;
   begin
        Result := VACIO;
        iConcepto :=  GetGlobalIntegerBD( K_GLOBAL_ISPT );
        if ( iConcepto > 0 ) then
           sFormula := GetFormulaConcepto( iConcepto );

        Result := '<ISPT>' + CR_LF;
        Result := Result + Format( '<Concepto No="%d"><Formula><![CDATA[%s]]></Formula></Concepto>', [iConcepto, sFormula] );
        Result := Result + '</ISPT>' + CR_LF;
   end;

   function GetInfonavitAporta : string;
   var
    FQuery: TZetaCursor;
   begin
        Result := '<INFONAVITAporta>' + CR_LF;
        sConceptos :=  GetGlobalStringBD( K_GLOBAL_CONCEPTOS_INFONAVIT );

        if StrLleno( sConceptos ) then
        begin
            with oZetaProvider do
            begin
               FQuery := CreateQuery( Format( K_CONCEPTOS_READ, [sConceptos]) );
               try
                   with FQuery do
                   begin
                        Active := True;
                        while not Eof do
                        begin
                            Result := Result + Format( '<Concepto No="%d"><Formula><![CDATA[%s]]></Formula></Concepto>', [ FieldByName( 'CO_NUMERO' ).AsInteger,  FieldByName( 'CO_FORMULA' ).AsString] )+ CR_LF;
                            Next;
                        end;
                        Active := False;
                   end;
               except
                    on Error: Exception do
                    begin
                        // Result := VACIO;
                    end;
               end;
            end;
        end;
        Result := Result + '</INFONAVITAporta>' + CR_LF
   end;

   function SNtoXMLBool( const sValue : string ) : string;
   begin
        if ( sValue = 'S' ) then
           Result := 'true'
        else
            Result := 'false';
   end;

   function GetConceptosActivos : string;
   var
    FQuery: TZetaCursor;
    sConceptotag : String;
   begin
     {   lConceptos := TStringList.Create;
        lConceptos.Add( '<Conceptos>' );}
        Result := '<Conceptos>';
         with oZetaProvider do
            begin
               FQuery := CreateQuery( K_CONCEPTOS_ACTIVOS_READ  );
               try
                   with FQuery do
                   begin
                        Active := True;
                        while not Eof do
                        begin
                            sConceptotag :=  Format( '<Concepto No="%d" IMSS="%s" ISPT="%s" ><Descripcion><![CDATA[%s]]></Descripcion><Formula><![CDATA[%s]]></Formula></Concepto>',
                                    [ FieldByName( 'CO_NUMERO' ).AsInteger,
                                      SNtoXMLBool( FieldByName( 'CO_G_IMSS' ).AsString ),
                                      SNtoXMLBool( FieldByName( 'CO_G_ISPT' ).AsString ),
                                      FieldByName( 'CO_DESCRIP' ).AsString,
                                      FieldByName( 'CO_FORMULA' ).AsString] ) ;

                            Result := Result + sConceptoTag + CR_LF;
                            Next;
                        end;
                        Active := False;
                   end;
               except
                    on Error: Exception do
                    begin
                        // Result := VACIO;
                    end;
               end;
            end;
        {lConceptos.Add( '</Conceptos>' );
        Result := lConceptos.Text;}
        Result := Result + '</Conceptos>';
   end;

   function GetInfonavitProv : string;
   begin
        Result := VACIO;
        iConcepto :=  oZetaProvider.GetGlobalInteger( K_GLOBAL_PROVISION_INFONAVIT );
        if ( iConcepto > 0 ) then
           sFormula := GetFormulaConcepto( iConcepto );
        Result := '<INFONAVITProv>' + CR_LF;
        Result := Result + Format( '<Concepto No="%d"><![CDATA[%s]]></Concepto>', [iConcepto, sFormula] );
        Result := Result + '</INFONAVITProv>' + CR_LF;
   end;


   function GetConceptos (sCM_CTRL_RL: String) : string;
   begin
   {$ifdef ANTES}
        Result := Result + GetConceptoISPT + CR_LF;
        Result := Result + GetInfonavitAporta + CR_LF;
        Result := Result + GetInfonavitProv + CR_LF;
   {$endif}

        if ObtieneElemento (lfTipoCompany, ord (tc3Prueba)) <> sCM_CTRL_RL then
          Result := GetConceptosActivos;
   end;

begin
Result := '<Nomina>'+CR_LF+ GetConceptos (sCM_CTRL_RL) + GetPeriodosXMLStr  +'</Nomina>'+CR_LF;
end;


function TLicenseMgr.GetAsistenciaXMLStr: string;
begin
Result := '<Asistencia>'+CR_LF+ GetChecadasAsistenciaXML(K_DIAS_3DT_HISTORIA) +  '</Asistencia>' +CR_LF;
end;


function TLicenseMgr.GetFonacotXMLStr: string;
begin
Result := '<Fonacot/>'+CR_LF
end;



function TLicenseMgr.GetUsoModulosXMLStr: string;
begin
Result := '<UsoModulos/>'+CR_LF;

end;

function TLicenseMgr.GetCompanyXMLStr (sCM_CTRL_RL: String): string;
begin     
     Result := GetRazonesSocialesXMLStr (sCM_CTRL_RL);     
end;

{$ifdef TRESSCFG}
function TLicenseMgr.GetInfoUsoTressStr (sCM_CTRL_RL: String): string;
begin
     Result := GetInfoUsoTress(sCM_CTRL_RL, false );
end;
{$endif}

function TLicenseMgr.GetGlobalIntegerBD(const iGlobal: Integer): integer;
begin
     Result := StrToIntDef(  GetGlobalStringBD( iGlobal ), 0 ) ;
end;

function TLicenseMgr.GetGlobalStringBD(const iGlobal: Integer): String;
var
  FQuery: TZetaCursor;
begin
   with oZetaProvider do
   begin
      FQuery := CreateQuery( K_GLOBAL_READ );
      try
          ParamAsInteger( FQuery, 'Codigo', iGlobal );
          with FQuery do
          begin
               Active := True;
               Result := FieldByName( 'GL_FORMULA' ).AsString;
               Active := False;
          end;
      except
           on Error: Exception do
           begin
                Result := VACIO;
           end;
      end;
   end;
end;


function TLicenseMgr.GetFormulaConcepto(const iConcepto: Integer): String;
var
  FQuery: TZetaCursor;
begin
   with oZetaProvider do
   begin
      FQuery := CreateQuery( K_CONCEPTO_READ );
      try
          ParamAsInteger( FQuery, 'CO_NUMERO', iConcepto);
          with FQuery do
          begin
               Active := True;
               Result := FieldByName( 'CO_FORMULA' ).AsString;
               Active := False;
          end;
      except
           on Error: Exception do
           begin
                Result := VACIO;
           end;
      end;
   end;
end;


function TLicenseMgr.DuracionNomina(iMesesAtras : integer): Double;
var
   FQuery: TZetaCursor;
   sQuery : string;
   dFinal, dInicio : TDate;
   i : integer;
begin
     with oZetaProvider do
     begin
          dFinal := Date;
          dInicio := dFinal;

          if ( iMesesAtras <= 0 ) then
             dInicio := 0
          else
             for i := 1 to iMesesAtras do
             begin
                  dInicio := LastMonth( dInicio );
             end;

          sQuery := Format( K_CALCULO_NOMINA_DURA_MIL, [  DateToStrSQLC( dInicio ),  DateToStrSQLC( dFinal ) ] );
          FQuery := CreateQuery( sQuery );
          try
             try
                with FQuery do
                begin
                     Active := True;
                     Result := FieldByName( 'DURA_MIL' ).AsFloat;
                     Active := False;
                end;
             except
                on Error: Exception do
                begin
                     Result := -1.00;
                end;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;


function TLicenseMgr.GetDatabaseSize( sCM_DATOS : string ) : Double;
{$ifdef MSSQL}
var
  FQuery : TZetaCursor;
{$endif}
{$ifdef INTERBASE}
var
 sServer, sPath : string;
 size : Int64;
 function GetFileSize(fileName : wideString) : Int64;
 var
   sr : TSearchRec;
 begin
   if FindFirst(fileName, faAnyFile, sr ) = 0 then
      result := Int64(sr.FindData.nFileSizeHigh) shl Int64(32) + Int64(sr.FindData.nFileSizeLow)
   else
      result := -1;

   FindClose(sr) ;
 end;

 procedure GetServerAndGDBFile( const sValue: String; var sServer, sPath : string);
var
   iPos: Integer;
begin
     iPos := Pos( ':', sValue );
     if ( iPos > 0 ) then
     begin
          sServer := Copy( sValue, 1, ( iPos - 1 ) );
          sPath := Copy( sValue, ( iPos + 1 ), ( Length( sValue ) - iPos ) );
     end
     else
     begin
          sServer := '';
          sPath := sValue;
     end;
end;
{$endif}
begin
{$ifdef INTERBASE}
   try
      GetServerAndGDBFile( sCM_DATOS, sServer, sPath );
      size := GetFileSize(sPath);
      Result := size / 1048576.00;
   except
       on Error: Exception do
       begin
            Result := -1;
       end;
   end;
{$endif}
{$ifdef MSSQL}
   with oZetaProvider do
   begin
        FQuery := CreateQuery( K_OBTIENE_DB_SIZE );
        try
           try
               with FQuery do
               begin
                    Active := True;
                    Result := FieldByName( 'DB_SIZE' ).AsFloat;
                    Active := False;
               end;
               except
               on Error: Exception do
               begin
                    Result := -1.00;
               end;
           end;
        finally

               FreeAndNil( FQuery );
        end;
   end;
{$endif}
end;

{$endif}

{ TDatosUsuarioEmail }
// US #11820 Advertir uso de licencia al grupo de administradores y grupo en especifico por Correo
constructor TDatosUsuarioEmail.Create;
begin
     inherited Create;
     UsCodigo := 0;
     UsEmail := VACIO;
     UsFormato := 0;
     UsActivo := VACIO;
     UsNombre := VACIO;
     UsCorto := VACIO;
end;

destructor TDatosUsuarioEmail.Destroy;
begin
      inherited Destroy;
end;



end.
