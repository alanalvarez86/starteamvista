unit DSalMin;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
     ZetaCommonClasses,
     DZetaServerProvider;

type
  TdmSalMin = class(TDataModule)
    procedure dmSalMinCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FQuerySalMin: TZetaCursor;
    FInicial: TDate;
    FFinal: TDate;
    FZonaA: TPesos;
    FZonaB: TPesos;
    FZonaC: TPesos;
    function oZetaProvider: TdmZetaServerProvider;
    procedure PreparaQuery;
    procedure CacheSalMin( const dReferencia: TDate );
  public
    { Public declarations }
    function  GetSalMin( const dReferencia: TDate; const sZona: String ): TPesos;
    function  GetSMGDF( const dReferencia: TDate ): TPesos;
  end;

implementation

const
     K_QUERY_SAL_MIN = 'select SM_FEC_INI, SM_ZONA_A, SM_ZONA_B, SM_ZONA_C '+
                       'from SAL_MIN order by SM_FEC_INI desc';

{$R *.DFM}

{ TdmSalMin }

procedure TdmSalMin.dmSalMinCreate(Sender: TObject);
begin
     FInicial := 0;
     FFinal := 0;
end;

procedure TdmSalMin.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FQuerySalMin );
end;

function TdmSalMin.oZetaProvider: TdmZetaServerProvider;
begin
     Result := TdmZetaServerProvider( Owner );
end;

function TdmSalMin.GetSalMin(const dReferencia: TDate; const sZona: String): TPesos;
begin
      // Si la fecha de referencia se sale de los rangos del cache,  se tiene que hacer el query de nuevo.
      if ( dReferencia < FInicial ) or ( dReferencia > FFinal ) then
         CacheSalMin( dReferencia );
      if ( Length( sZona ) > 0 ) then
      begin
           case sZona[ 1 ] of
                K_T_ZONA_B: Result := FZonaB;
                K_T_ZONA_C: Result := FZonaC;
           else
               Result := FZonaA;
           end;
      end
      else
          Result := FZonaA;
end;

function TdmSalMin.GetSMGDF(const dReferencia: TDate): TPesos;
begin
     Result := GetSalMin( dReferencia, K_T_ZONA_A );
end;

procedure TdmSalMin.PreparaQuery;
begin
     if ( FQuerySalMin = NIL ) then
     begin
          FQuerySalMin := oZetaProvider.CreateQuery( K_QUERY_SAL_MIN );
     end;
end;

procedure TdmSalMin.CacheSalMin( const dReferencia: TDate );
const
     K_SM_FEC_INI = 0;
     K_SM_ZONA_A = 1;
     K_SM_ZONA_B = 2;
     K_SM_ZONA_C = 3;
var
   lEncontro: Boolean;
begin
     PreparaQuery;
     with FQuerySalMin do
     begin
	      Active := True;
	      lEncontro := False;
	      FFinal := 2100 * 365;	// A�o 2100
	      { Recordar que el query viene en orden descendente; }
          { El primer registro del query es el de la �ltima fecha }
	      { Generalmente, en el primer registro se encuentra }
          while not Eof do
          begin
               FInicial := Fields[ K_SM_FEC_INI ].AsDateTime;
               if ( FInicial <= dReferencia ) then
               begin
	                lEncontro := True;
                    Break;
               end;
               FFinal := FInicial - 1;
               Next;
          end;
          if lEncontro then
          begin
               FZonaA := Fields[ K_SM_ZONA_A ].AsFloat;
               FZonaB := Fields[ K_SM_ZONA_B ].AsFloat;
               FZonaC := Fields[ K_SM_ZONA_C ].AsFloat;
          end
          else
          begin
   	           FInicial := 0;
   	           FZonaA := 0;
   	           FZonaB := 0;
   	           FZonaC := 0;
          end;
          Active := False;
     end;
end;

end.
