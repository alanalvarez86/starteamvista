unit ZAgenteSQLClient;

interface

uses Classes, SysUtils,  
     ZetaTipoEntidad,
     Variants,
     ZetaCommonLists, DBClient;
	 
{$define XMLREPORT}
const
     K_STATUS_OK = -1;
type
  { Respuesta del SuperSQL para cada objeto que se solicita }
  eSQLStatus = Integer;

  { Cada una de las COLUMNAS que se solicitan al SuperSQL }
  TSQLColumna = class(TObject)
  private
    FDeDiccionario: Boolean;
    FFormula: String;
    FTipoFormula : eTipoGlobal;
    FStatus: eSQLStatus;
    FTipoEntidad : TipoEntidad;
    FAncho : Integer;
    FAlias : String;
    FTotalizacion : eTipoOperacionCampo;
    FGroupExp : Boolean;
    FBanda : Integer;
    FPosCampo : Integer;
    FValorConstante: String;
    FMsgError : string;
    FSubTotal : Boolean;
    FTotalizador : Integer;
    FLugarSubTot : Integer;
  public
    property Formula : String read FFormula write FFormula;
    property DeDiccionario : Boolean read FDeDiccionario write FDeDiccionario;
    property TipoFormula : eTipoGlobal read FTipoFormula write FTipoFormula;
    property Status : eSQLStatus read FStatus write FStatus;
    property Entidad : TipoEntidad read FTipoEntidad write FTipoEntidad;
    property Ancho : Integer read FAncho write FAncho;
    property Alias : String read FAlias write FAlias;
    property Totalizacion : eTipoOperacionCampo read FTotalizacion write FTotalizacion;
    property GroupExp : Boolean read FGroupExp write FGroupExp;
    property Banda : Integer read FBanda write FBanda;
    property PosCampo : Integer read FPosCampo write FPosCampo;
    property ValorConstante : String read FValorConstante write FValorConstante;
    property MsgError : String read FMsgError write FMsgError;
    property Totalizador : Integer read FTotalizador write FTotalizador;
    property UsaSubTotal : Boolean read FSubTotal write FSubTotal;
    property LugarSubTot : Integer read FLugarSubTot write FLugarSubTot;
    function EsConstante : Boolean;
  end;

  { Cada uno de los FILTROS que se solicitan al SuperSQL }
  TSQLFiltro = class(TObject)
  private
    FDeDiccionario: Boolean;
    FFormula: String;
    FStatus: eSQLStatus;
    FTipoEntidad: TipoEntidad;
  public
    property Formula : String read FFormula write FFormula;
    property DeDiccionario : Boolean read FDeDiccionario write FDeDiccionario;
    property Status : eSQLStatus read FStatus write FStatus;
    property Entidad : TipoEntidad read FTipoEntidad write FTipoEntidad;
  end;

  { Cada uno de los CAMPOS de ORDEN que se solicitan al SuperSQL }
  TSQLOrden = class(TObject)
  private
    FDeDiccionario: Boolean;
    FFormula : String;
    FAscendente: Boolean;
    FStatus: eSQLStatus;
    FTipoEntidad : TipoEntidad;
    FPosColumna : Integer;
  public
    property Formula : String read FFormula write FFormula;
    property DeDiccionario : Boolean read FDeDiccionario write FDeDiccionario;
    property Ascendente : Boolean read FAscendente write FAscendente;
    property Status : eSQLStatus read FStatus write FStatus;
    property Entidad : TipoEntidad read FTipoEntidad write FTipoEntidad;
    property PosColumna : Integer read FPosColumna write FPosColumna;
  end;
  TSQLGrupo = class(TSQLOrden);

  { Lista General de Objetos SQL }
  TSQLLista = class( TList )
  private
    procedure FreeAll;
  public
    procedure  Clear; override;
  end;

  { Lista de CAMPOS solicitados al SuperSQL }
  TSQLListaColumnas = class(TSQLLista)
  private
  public
    function AgregaColumna( const sFormula : String; const lDiccion : Boolean;
                            const eTipoEntidad : TipoEntidad;
                            const eTipoFormula : eTipoGlobal;
                            const iAncho : Integer;
                            const sAlias : String;
                            const eTotal :eTipoOperacionCampo;
                            const lGroupExp : Boolean;
                            const iBanda : Integer  ) : Integer;
  end;

  { Lista de FILTROS solicitados al SuperSQL }
  TSQLListaFiltros = class(TSQLLista)
  private
  public
    function AgregaFiltro(  const sFormula : String; const lDiccion : Boolean;
                         const eTipoEntidad : TipoEntidad ) : Integer;
  end;

  { Lista de ORDEN solicitados al SuperSQL }
  TSQLListaOrdenes = class(TSQLLista)
  private
  public
    function AgregaOrden(  const sFormula : String; const lAscendente : Boolean;
                           const eTipoEntidad : TipoEntidad; const lDiccion : Boolean ) : Integer;

  end;

  { Lista de Listas para enviar a Super SQL }
  TSQLAgenteClient = class(TObject)
  private
    FEntidad       : TipoEntidad;
    FListaFiltros  : TSQLListaFiltros;
    FListaOrdenes  : TSQLListaOrdenes;
    FListaGrupos   : TSQLListaOrdenes;
    FSQL           : TStringList;
    FListaErrores  : TStringList;
    FParametros: OleVariant;
    FFiltroCliente : Boolean;
    FOrdenCliente  : Boolean;
    FOptimizar: Boolean;
    FSubTotales : Boolean;
    FListaSubTotales : TStringList;
    FRastreaColumnaCalculada: Boolean;
    FColumnaCalculada: integer;
    function GetNumColumnas: Integer;
    function GetNumFiltros: Integer;
    function GetNumOrdenes: Integer;
    function GetNumGrupos : Integer;
    function GetNumErrores: Integer;
    {$ifdef XMLREPORT}
    procedure ColumnasToTextFile( Datos: TStrings );
    procedure FiltrosToTextFile( Datos: TStrings );
    procedure OrdenGrupoToTextFile( Datos: TStrings; Lista: TSQLListaOrdenes; const sTag: String );
    procedure OrdenesToTextFile( Datos: TStrings );
    procedure GruposToTextFile( Datos: TStrings );
    procedure ParametrosToTextFile( Datos: TStrings );
    {$endif}
    function ColumnasToVariant: OleVariant;
    procedure VariantToColumnas(oColumnas: OleVariant);
    function FiltrosToVariant: OleVariant;
    procedure VariantToFiltros(oFiltros: OleVariant);
    function OrdenGrupoToVariant( oLista : TSQLListaOrdenes ) : OleVariant;
    procedure VariantToOrdenGrupo( oLista : TSQLListaOrdenes; aValores: OleVariant );
    function OrdenesToVariant: OleVariant;
    procedure VariantToOrdenes(oOrdenes: OleVariant);
    function GruposToVariant: OleVariant;
    procedure VariantToGrupos(oGrupos: OleVariant);
  protected
    FListaColumnas : TSQLListaColumnas;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    { Para agregar }
    property Optimizar : Boolean read FOptimizar write FOptimizar;
    function AgregaColumna( sFormula : String; const lDiccion : Boolean;
                         const eTipoEntidad : TipoEntidad;
                         const eTipoFormula : eTipoGlobal;
                         const iAncho : Integer;
                         const sAlias : String = '';
                         const eTotal : eTipoOperacionCampo = ocAutomatico;
                         const lGroupExp : Boolean = FALSE;
                         const iBanda : Integer = -1 ) : Integer;virtual;
    function AgregaFiltro(  const sFormula : String; const lDiccion : Boolean;
                         const eTipoEntidad : TipoEntidad ) : Integer;
    function AgregaOrden(   const sFormula : String; const lAscendente : Boolean;
                            const eTipoEntidad : TipoEntidad;
                            const lDiccion : Boolean = TRUE ) : Integer;
    function AgregaGrupo(   const sFormula : String; const lAscendente : Boolean;
                            const eTipoEntidad : TipoEntidad;
                            const lDiccion : Boolean = TRUE ) : Integer;
    function AgregaColumnaOrden(const sFormula : String; const eTipoEntidad : TipoEntidad ) : Integer;

    { Para consultar resultados }
    function GetColumna( const iPos : Integer ) : TSQLColumna;
    function GetFiltro( const iPos : Integer ) : TSQLFiltro;
    function GetOrden( const iPos : Integer ) : TSQLOrden;
    function GetGrupo( const iPos : Integer ) : TSQLGrupo;
    function GetError( const iPos : eSQLStatus ) : String;
    { Para Poder enviarlos y Regresarlo del Servidor}
    function AgenteToVariant : OleVariant;
    procedure VariantToAgente( oAgente : OleVariant );

    procedure AsignaOrdenes(oAgente: TSQLAgenteClient);
    procedure AsignaFiltros( oAgente : TSQLAgenteClient );
    procedure OrdenaDataset( oDataset : TClientDataSet );
    {$ifdef XMLREPORT}
    procedure AgenteToTextFile( const sFileName: String ); overload;
    procedure AgenteToTextFile( Datos: TStrings ); overload;
    {$endif}
    { Propiedades }
    property Entidad : TipoEntidad read FEntidad write FEntidad;
    property SQL : TStringList read FSQL;
    property NumColumnas : Integer read GetNumColumnas;
    property NumFiltros : Integer read GetNumFiltros;
    property NumOrdenes : Integer read GetNumOrdenes;
    property NumGrupos  : Integer read GetNumGrupos;
    property NumErrores: Integer read GetNumErrores;
    property ListaErrores : TStringList read FListaErrores;
    property Parametros : OleVariant read FParametros write FParametros;
    property FiltroCliente : Boolean read FFiltroCliente write FFiltroCliente;
    property OrdenCliente  : Boolean read FOrdenCliente  write FOrdenCliente;
    property SubTotales : Boolean read FSubTotales;
    property ListaSubTotales : TStringList read FListaSubTotales;
    property ColumnaCalculada: integer read FColumnaCalculada write FColumnaCalculada;
    property RastreaColumnaCalculada: Boolean read FRastreaColumnaCalculada write FRastreaColumnaCalculada;
    procedure AgregaSubTotales( sLista : String );
  end;


implementation

uses ZetaCommonTools;

{ TSQLLista }

const K_MAX_ELEM = 13;

      K_ENTIDAD = 1;
      K_NUM_COLUMNAS  = 2;
      K_NUM_FILTROS = 3;
      K_NUM_ORDENES = 4;
      K_ERRORES = 5;
      K_SQL = 6;
      K_LISTA_COLUMNAS = 7;
      K_LISTA_FILTROS = 8;
      K_LISTA_ORDENES = 9;
      K_LISTA_GRUPOS  = 10;
      K_PARAMETROS    = 11;
      K_FILTRO_CLIENTE = 12;
      K_ORDEN_CLIENTE  = 13;

procedure TSQLLista.FreeAll;
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
         TObject( Items[ i ] ).Free;
end;

procedure TSQLLista.Clear;
begin
     FreeAll;
     inherited Clear;
end;


{ TSQLListaColumnas }

function TSQLListaColumnas.AgregaColumna(const sFormula: String;
  const lDiccion: Boolean; const eTipoEntidad : TipoEntidad;
  const eTipoFormula: eTipoGlobal; const iAncho : Integer; const sAlias : String;
  const eTotal : eTipoOperacionCampo;
  const lGroupExp : Boolean; const iBanda : Integer ): Integer;
var
   oColumna : TSQLColumna;
begin
     oColumna := TSQLColumna.Create;
     with oColumna do
     begin
          Formula := sFormula;
          DeDiccionario := lDiccion;
          TipoFormula := eTipoFormula;
          Entidad := eTipoEntidad;
          Status := K_STATUS_OK;
          Ancho  := iAncho;
          Alias  := sAlias;
          Totalizacion := eTotal;
          GroupExp := lGroupExp;
          Banda := iBanda;
          PosCampo := 0;    // Indica que no es Constante
          Totalizador := -1;
          UsaSubTotal := FALSE;
          LugarSubTot := -1;
     end;
     Result := Add( oColumna );
end;

{ TSQLListaFiltros }

function TSQLListaFiltros.AgregaFiltro(const sFormula: String;
  const lDiccion: Boolean; const eTipoEntidad: TipoEntidad): Integer;
var
   oFiltro : TSQLFiltro;
begin
     oFiltro := TSQLFiltro.Create;
     with oFiltro do
     begin
          Formula := sFormula;
          DeDiccionario := lDiccion;
          Entidad := eTipoEntidad;
          Status := K_STATUS_OK;
     end;
     Result := Add( oFiltro );
end;

{ TSQLListaOrdenes }

function TSQLListaOrdenes.AgregaOrden(const sFormula: String;
  const lAscendente: Boolean; const eTipoEntidad : TipoEntidad; const lDiccion : Boolean ) : Integer;

var
   oOrden : TSQLOrden;
begin
     oOrden := TSQLOrden.Create;
     with oOrden do
     begin
          Formula := sFormula;
          DeDiccionario := lDiccion;
          Ascendente := lAscendente;
          Entidad := eTipoEntidad;
          Status := K_STATUS_OK;
     end;
     Result := Add( oOrden );
end;

{ TSQLAgenteClient }


function TSQLAgenteClient.AgregaColumna(sFormula: String;
  const lDiccion: Boolean; const eTipoEntidad : TipoEntidad;
  const eTipoFormula: eTipoGlobal; const iAncho : Integer; const sAlias : String;
  const eTotal : eTipoOperacionCampo;
  const lGroupExp : Boolean; const iBanda : Integer ): Integer;
begin
     Result := FListaColumnas.AgregaColumna( sFormula, lDiccion, eTipoEntidad, eTipoFormula, iAncho, sAlias, eTotal, lGroupExp, iBanda );
end;

function TSQLAgenteClient.AgregaFiltro(const sFormula: String;
  const lDiccion: Boolean; const eTipoEntidad: TipoEntidad): Integer;
begin
     if StrLleno( sFormula ) then
        Result := FListaFiltros.AgregaFiltro( sFormula, lDiccion, eTipoEntidad )
     else
         Result := -1;
end;

function TSQLAgenteClient.AgregaOrden(const sFormula: String;
  const lAscendente: Boolean; const eTipoEntidad : TipoEntidad; const lDiccion : Boolean ) : Integer;

begin
     Result := FListaOrdenes.AgregaOrden( sFormula, lAscendente, eTipoEntidad, lDiccion );
end;

function TSQLAgenteClient.AgregaGrupo(const sFormula: String;
  const lAscendente: Boolean; const eTipoEntidad : TipoEntidad; const lDiccion : Boolean ) : Integer;

begin
     Result := FListaGrupos.AgregaOrden( sFormula, lAscendente, eTipoEntidad, lDiccion );
end;


procedure TSQLAgenteClient.Clear;
begin
     FListaColumnas.Clear;
     FListaFiltros.Clear;
     FListaOrdenes.Clear;
     FListaGrupos.Clear;
     FSQL.Clear;
     FListaErrores.Clear;
     FListaSubTotales.Clear;
     FSubTotales := FALSE;
end;

constructor TSQLAgenteClient.Create;
begin
     FListaColumnas   := TSQLListaColumnas.Create;
     FListaFiltros    := TSQLListaFiltros.Create;
     FListaOrdenes    := TSQLListaOrdenes.Create;
     FListaGrupos     := TSQLListaOrdenes.Create;
     FSQL             := TStringList.Create;
     FListaErrores    := TStringList.Create;
     FListaSubTotales := TStringList.Create;
     FFiltroCliente   := FALSE;
     FOrdenCliente    := FALSE;
     FOptimizar       := FALSE;
     FSubTotales      := FALSE;
     FRastreaColumnaCalculada := FALSE;
end;

destructor TSQLAgenteClient.Destroy;
begin
     FListaColumnas.Free;
     FListaFiltros.Free;
     FListaOrdenes.Free;
     FListaGrupos.Free;
     FSQL.Free;
     FListaErrores.Free;
     FListaSubTotales.Free;
end;

function TSQLAgenteClient.GetColumna(const iPos: Integer): TSQLColumna;
begin
     with FListaColumnas do
          if ( iPos >= 0 ) and ( iPos < Count ) then
             Result := TSQLColumna( Items[ iPos ] )
          else
              Result := NIL;
end;

function TSQLAgenteClient.GetFiltro(const iPos: Integer): TSQLFiltro;
begin
     with FListaFiltros do
          if ( iPos >= 0 ) and ( iPos < Count ) then
             Result := TSQLFiltro( Items[ iPos ] )
          else
              Result := NIL;
end;

function TSQLAgenteClient.GetNumColumnas: Integer;
begin
     Result := FListaColumnas.Count;
end;

function TSQLAgenteClient.GetNumFiltros: Integer;
begin
     Result := FListaFiltros.Count;
end;

function TSQLAgenteClient.GetNumOrdenes: Integer;
begin
     Result := FListaOrdenes.Count;
end;

function TSQLAgenteClient.GetNumGrupos: Integer;
begin
     Result := FListaGrupos.Count;
end;


function TSQLAgenteClient.GetOrden(const iPos: Integer): TSQLOrden;
begin
     with FListaOrdenes do
          if ( iPos >= 0 ) and ( iPos < Count ) then
             Result := TSQLOrden( Items[ iPos ] )
          else
              Result := NIL;
end;

function TSQLAgenteClient.GetGrupo(const iPos: Integer): TSQLGrupo;
begin
     with FListaGrupos do
          if ( iPos >= 0 ) and ( iPos < Count ) then
             Result := TSQLGrupo( Items[ iPos ] )
          else
              Result := NIL;
end;


function TSQLAgenteClient.ColumnasToVariant: OleVariant;
var
   oColumna: OleVariant;
   i, iCount: integer;
begin
     ZetaCommonTools.SetOleVariantToNull( Result );
     iCount := FListaColumnas.Count;
     if iCount > 0 then
     begin
          Result := VarArrayCreate([1, iCount ], varVariant);
          for i := 0 to iCount-1 do
          begin
               oColumna := VarArrayCreate([1, 12], varVariant);
               with TSQLColumna(FListaColumnas[i]) do
               begin
                    oColumna[1]:= DeDiccionario;
                    oColumna[2]:= Formula;
                    oColumna[3]:= TipoFormula;
                    oColumna[4]:= Ord(Status);
                    oColumna[5]:= Ord(Entidad);
                    oColumna[6]:= Ancho;
                    oColumna[7]:= Alias;
                    oColumna[8]:= Ord(Totalizacion);
                    oColumna[9]:= GroupExp;
                    oColumna[10]:= Banda;
                    oColumna[11]:= PosCampo;
                    oColumna[12]:= ValorConstante;
               end;
               Result[i+1] := oColumna;
          end;
     end;
end;

function TSQLAgenteClient.FiltrosToVariant: OleVariant;
var
   oColumna: OleVariant;
   i, iCount: integer;
begin
     ZetaCommonTools.SetOleVariantToNull( Result );
     iCount := FListaFiltros.Count;
     if iCount > 0 then
     begin
          Result := VarArrayCreate([1, iCount ], varVariant);
          for i := 0 to iCount -1 do
          begin
               oColumna := VarArrayCreate([1, 5 ], varVariant);
               with TSQLFiltro(FListaFiltros[i]) do
               begin
                    oColumna[1]:= DeDiccionario;
                    oColumna[2]:= Formula;
                    oColumna[3]:= Ord(Status);
                    oColumna[4]:= Ord(Entidad);
               end;
               Result[i+1] := oColumna;
          end;
     end;
end;

function TSQLAgenteClient.GruposToVariant: OleVariant;
begin
    Result := OrdenGrupoToVariant( FListaGrupos );
end;

function TSQLAgenteClient.OrdenesToVariant: OleVariant;
begin
    Result := OrdenGrupoToVariant( FListaOrdenes );
end;

function TSQLAgenteClient.OrdenGrupoToVariant( oLista : TSQLListaOrdenes ) : OleVariant;
var
   oColumna: OleVariant;
   i, iCount: integer;
begin
     ZetaCommonTools.SetOleVariantToNull( Result );
     iCount := oLista.Count;
     if iCount > 0 then
     begin
          Result := VarArrayCreate([1, iCount ], varVariant);
          for i := 0 to iCount - 1 do
          begin
               oColumna := VarArrayCreate([1, 6 ], varVariant);
               with TSQLOrden(oLista[i]) do
               begin
                    oColumna[1]:= Formula;
                    oColumna[2]:= Ord(Status);
                    oColumna[3]:= Ord(Entidad);
                    oColumna[4]:= Ascendente;
                    oColumna[5]:= DeDiccionario;
                    oColumna[6]:= PosColumna;
               end;
               Result[i+1] := oColumna;
          end;
     end;
end;

procedure TSQLAgenteClient.VariantToColumnas( oColumnas : OleVariant );
var i, iLow, iHigh, nPos : integer;
begin
     FListaColumnas.Clear;
     if not (VarIsNULL(oColumnas) OR VarIsEmpty(oColumnas)) then
     begin
          iLow := VarArrayLowBound( oColumnas,1 );
          iHigh := VarArrayHighBound( oColumnas, 1 );
          if iLow + iHigh > 0 then
             for i := iLow to iHigh do
             begin
                  nPos := AgregaColumna( oColumnas[i][2],
                                 oColumnas[i][1],
                                 TipoEntidad(oColumnas[i][5]),
                                 oColumnas[i][3],
                                 oColumnas[i][6],
                                 oColumnas[i][7],
                                 eTipoOperacionCampo( oColumnas[i][8] ),
                                 oColumnas[i][9],
                                 oColumnas[i][10] );
                  with TSQLColumna( FListaColumnas.Items[ nPos ] ) do
                  begin
                    PosCampo       := oColumnas[ i ][11];
                    ValorConstante := oColumnas[ i ][12];
                  end;
             end;
     end;
end;

procedure TSQLAgenteClient.VariantToFiltros( oFiltros : OleVariant );
 var i, iLow, iHigh : integer;
begin
     FListaFiltros.Clear;
     if not (VarIsNULL(oFiltros) OR VarIsEmpty(oFiltros)) then
     begin
          iLow := VarArrayLowBound( oFiltros,1 );
          iHigh := VarArrayHighBound( oFiltros, 1 );
          if iLow + iHigh > 0 then
             for i := iLow to iHigh do
                 AgregaFiltro( oFiltros[i][2],
                               oFiltros[i][1],
                               TipoEntidad(oFiltros[i][4]));
     end;
end;


procedure TSQLAgenteClient.VariantToGrupos(oGrupos: OleVariant);
begin
    VariantToOrdenGrupo( FListaGrupos, oGrupos );
end;

procedure TSQLAgenteClient.VariantToOrdenes(oOrdenes: OleVariant);
begin
    VariantToOrdenGrupo( FListaOrdenes, oOrdenes );
end;

procedure TSQLAgenteClient.VariantToOrdenGrupo( oLista : TSQLListaOrdenes; aValores : OleVariant );
 var i, iPos, iLow, iHigh : integer;
begin
     oLista.Clear;
     if not (VarIsNULL(aValores) OR VarIsEmpty(aValores) ) then
     begin
          iLow := VarArrayLowBound( aValores,1 );
          iHigh := VarArrayHighBound( aValores, 1 );
          if iLow + iHigh > 0 then
          for i := VarArrayLowBound( aValores,1 ) to VarArrayHighBound( aValores, 1 ) do
          begin
               iPos:= AgregaOrden( aValores[i][1],
                                   aValores[i][4],
                                   TipoEntidad(aValores[i][3]),
                                   aValores[i][5] );
               GetOrden(iPos).PosColumna := aValores[i][6];
          end;
     end;
end;

procedure TSQLAgenteClient.VariantToAgente(oAgente: OleVariant);
begin
     FEntidad := TipoEntidad(oAgente[K_ENTIDAD]);
     FListaErrores.Text := oAgente[K_ERRORES];
     FSQL.Text := oAgente[K_SQL];
     VariantToColumnas( oAgente[K_LISTA_COLUMNAS] );
     VariantToFiltros( oAgente[K_LISTA_FILTROS] );
     VariantToOrdenes( oAgente[K_LISTA_ORDENES] );
     VariantToGrupos( oAgente[K_LISTA_GRUPOS] );
     Parametros := oAgente[K_PARAMETROS];
     FiltroCliente := oAgente[K_FILTRO_CLIENTE];
     OrdenCliente := oAgente[K_ORDEN_CLIENTE];
end;

function TSQLAgenteClient.AgenteToVariant: OleVariant;
begin
     Result := VarArrayCreate([1, K_MAX_ELEM], varVariant);
     Result[K_ENTIDAD] := Ord(FEntidad);
     Result[K_NUM_COLUMNAS] := GetNumColumnas;
     Result[K_NUM_FILTROS] := GetNumFiltros;
     Result[K_NUM_ORDENES] := GetNumOrdenes;
     Result[K_ERRORES] := FListaErrores.Text;
     Result[K_SQL] := FSQL.Text;
     Result[K_LISTA_COLUMNAS] := ColumnasToVariant;
     Result[K_LISTA_FILTROS] := FiltrosToVariant;
     Result[K_LISTA_ORDENES] := OrdenesToVariant;
     Result[K_LISTA_GRUPOS]  := GruposToVariant;
     Result[K_PARAMETROS] := Parametros;
     Result[K_FILTRO_CLIENTE] := FiltroCliente;
     Result[K_ORDEN_CLIENTE] := OrdenCliente;
end;

function TSQLAgenteClient.GetError(const iPos: eSQLStatus): String;
begin
     with FListaErrores do
          if ( iPos > K_STATUS_OK ) and ( iPos < Count ) then
             Result := Strings[ iPos ]
          else
              Result := '';
end;

function TSQLAgenteClient.GetNumErrores : Integer;
begin
     Result := FListaErrores.Count;
end;




procedure TSQLAgenteClient.AsignaFiltros(oAgente: TSQLAgenteClient);
 var i : integer;
begin
     for i:=0 to oAgente.NumFiltros - 1 do
         AgregaFiltro( oAgente.GetFiltro(i).Formula,
                       oAgente.GetFiltro(i).DeDiccionario,
                       oAgente.GetFiltro(i).Entidad );
end;
procedure TSQLAgenteClient.AsignaOrdenes(oAgente: TSQLAgenteClient);
 var i : integer;
begin
     for i:=0 to oAgente.NumOrdenes - 1 do
         AgregaOrden( oAgente.GetOrden(i).Formula,
                      oAgente.GetOrden(i).Ascendente,
                      oAgente.GetOrden(i).Entidad,
                      oAgente.GetOrden(i).DeDiccionario );
end;

function TSQLColumna.EsConstante: Boolean;
begin
    Result := ( FPosCampo < 0 );
end;


procedure TSQLAgenteClient.OrdenaDataset(oDataset: TClientDataSet);
var
   i, iNumOrdenes : Integer;
   oOrden : TSQLOrden;
   sColumna, sIndex : String;
begin
    // PENDIENTE: Ascendente/Descendente
    sIndex := '';
    if OrdenCliente then
    begin
        iNumOrdenes := NumOrdenes;
        Dec( iNumOrdenes );
        for i := 0 to iNumOrdenes do
        begin
            oOrden := GetOrden( i );
            sColumna := 'COLUMNA' + IntToStr( oOrden.PosColumna );
            if ( i = 0 ) then
                sIndex := sColumna
            else
                sIndex := sIndex + ';' + sColumna;
        end;
    end;

    oDataSet.IndexFieldNames := sIndex;
    oDataSet.First;
end;

function TSQLAgenteClient.AgregaColumnaOrden(const sFormula: String; const eTipoEntidad: TipoEntidad): Integer;
var
    i, iNumColumnas : Integer;
begin
    Result := -1;
    iNumColumnas := NumColumnas;
    Dec( iNumColumnas );
    for i := 0 to iNumColumnas do
        if GetColumna( i ).Formula = sFormula then
        begin
            Result := i;
            Break;
        end;

    if ( Result < 0 ) then
        Result := AgregaColumna( sFormula, FALSE, eTipoEntidad, tgAutomatico, 0 );
end;

procedure TSQLAgenteClient.AgregaSubTotales(sLista: String);
var
    oListaTempo : TStringList;
    i : Integer;
    sSubTotal : String;
begin
    // Agrega los #'s de Columna "x" referenciados por SubTotal(x)
    // La 'sLista' es una lista de #'s separadas por "," (Ej. 5,7,14
    // Elimina repetidos
    oListaTempo := TStringList.Create;
    try
        oListaTempo.CommaText := sLista;
        for i := 0 to oListaTempo.Count-1 do
        begin
            sSubTotal := oListaTempo[ i ];
            if ( FListaSubTotales.IndexOf( sSubTotal )) < 0 then
            begin
                FListaSubTotales.Add( sSubTotal );
                FSubTotales := TRUE;
            end;
        end;
    finally
        oListaTempo.Free;
    end;
end;

{$ifdef XMLREPORT}
procedure TSQLAgenteClient.ColumnasToTextFile( Datos: TStrings );
var
   i, iCount: integer;
begin
     iCount := FListaColumnas.Count;
     if iCount > 0 then
     begin
          with Datos do
          begin
               Add( '' );
               Add( Format( '============ %s =============', [ 'Columnas' ] ) );
               for i := 0 to iCount - 1 do
               begin
                    with TSQLColumna(FListaColumnas[i]) do
                    begin
                         Add( Format( '-------- Columna # %3.3d ------------', [ i ] ) );
                         Add( Format( 'DeDiccionario = %s', [ BoolToStr( DeDiccionario ) ] ) );
                         Add( Format( 'Formula = %s', [ Formula ] ) );
                         Add( Format( 'TipoFormula = %d', [ Ord( TipoFormula ) ] ) );
                         Add( Format( 'Status = %d', [ Ord( Status ) ] ) );
                         Add( Format( 'Entidad = %d', [ Ord( Entidad ) ] ) );
                         Add( Format( 'Ancho = %d', [ Ancho ] ) );
                         Add( Format( 'Alias = %s', [ Alias ] ) );
                         Add( Format( 'Totalizacion = %d', [ Ord( Totalizacion ) ] ) );
                         Add( Format( 'GroupExp = %s', [ BoolToStr( GroupExp ) ] ) );
                         Add( Format( 'Banda = %d', [ Banda ] ) );
                         Add( Format( 'PosCampo = %d', [ PosCampo ] ) );
                         Add( Format( 'ValorConstante = %s', [ ValorConstante ] ) );
                    end;
               end;
               Add( Format( '========= %s =========', [ 'Fin de Columnas' ] ) );
          end;
     end;
end;

procedure TSQLAgenteClient.FiltrosToTextFile( Datos: TStrings );
var
   i, iCount: integer;
begin
     iCount := FListaFiltros.Count;
     if iCount > 0 then
     begin
          with Datos do
          begin
               Add( '' );
               Add( Format( '============ %s =============', [ 'Filtros' ] ) );
               for i := 0 to iCount -1 do
               begin
                    with TSQLFiltro(FListaFiltros[i]) do
                    begin
                         Add( Format( '-------- Filtro # %3.3d ------------', [ i ] ) );
                         Add( Format( 'DeDiccionario = %s', [ BoolToStr( DeDiccionario ) ] ) );
                         Add( Format( 'Formula = %s', [ Formula ] ) );
                         Add( Format( 'Status = %d', [ Ord( Status ) ] ) );
                         Add( Format( 'Entidad = %d', [ Ord( Entidad ) ] ) );
                    end;
               end;
               Add( Format( '========= %s =========', [ 'Fin de Filtros' ] ) );
          end;
     end;
end;

procedure TSQLAgenteClient.OrdenGrupoToTextFile( Datos: TStrings; Lista: TSQLListaOrdenes; const sTag: String );
var
   i, iCount: integer;
begin
     iCount := Lista.Count;
     if iCount > 0 then
     begin
          with Datos do
          begin
               Add( '' );
               Add( Format( '============ %s =============', [ sTag ] ) );
               for i := 0 to iCount - 1 do
               begin
                    with TSQLOrden(Lista[i]) do
                    begin
                         Add( Format( '-------- Elemento # %3.3d ------------', [ i ] ) );
                         Add( Format( 'DeDiccionario = %s', [ BoolToStr( DeDiccionario ) ] ) );
                         Add( Format( 'Formula = %s', [ Formula ] ) );
                         Add( Format( 'Status = %d', [ Ord( Status ) ] ) );
                         Add( Format( 'Entidad = %d', [ Ord( Entidad ) ] ) );
                         Add( Format( 'PosColumna = %d', [ PosColumna ] ) );
                         Add( Format( 'Ascendente = %s', [ BoolToStr( Ascendente ) ] ) );
                    end;
               end;
               Add( Format( '========= Fin de %s =========', [ sTag ] ) );
          end;
     end;
end;

procedure TSQLAgenteClient.OrdenesToTextFile( Datos: TStrings );
begin
     OrdenGrupoToTextFile( Datos, FListaOrdenes, 'Ordenes' );
end;

procedure TSQLAgenteClient.GruposToTextFile( Datos: TStrings );
begin
     OrdenGrupoToTextFile( Datos, FListaGrupos, 'Grupos' );
end;

procedure TSQLAgenteClient.ParametrosToTextFile( Datos: TStrings );
const
     P_TITULO = 1;
     P_FORMULA = 2;
     P_TIPO = 3;
var
   i, iLow, iHigh: Integer;
begin
     with Datos do
     begin
          Add( '' );
          Add( Format( '============ %s =============', [ 'Parámetros del Reporte' ] ) );
          iLow := VarArrayLowBound( Self.Parametros, 1 );
          iHigh := VarArrayHighBound( Self.Parametros, 1 );
          for i := iLow to iHigh do
          begin
               if VarIsArray( Self.Parametros[ i ] ) then
               begin
                    Add( Format( '-------- Parámetro # %3.3d ------------', [ i ] ) );
                    Add( Format( 'Título  = %s', [ Self.Parametros[ i ][ P_TITULO ] ] ) );
                    Add( Format( 'Formula = %s', [ Self.Parametros[ i ][ P_FORMULA ] ] ) );
                    Add( Format( 'Tipo    = %d', [ Ord( eTipoGlobal( Self.Parametros[ i ][ P_TIPO ] ) ) ] ) );
                    {
                    }
               end;
          end;
          Add( Format( '========= %s =========', [ 'Fin de Parámetros del Reporte' ] ) );
     end;
end;

procedure TSQLAgenteClient.AgenteToTextFile( Datos: TStrings );
begin
     with Datos do
     begin
          Add( Format( 'K_ENTIDAD = %d', [ Ord(Self.FEntidad) ] ) );
          Add( Format( 'K_NUM_COLUMNAS = %d', [ Self.GetNumColumnas ] ) );
          Add( Format( 'K_NUM_FILTROS = %d', [ Self.GetNumFiltros ] ) );
          Add( Format( 'K_NUM_ORDENES = %d', [ Self.GetNumOrdenes ] ) );
          Add( Format( 'K_ERRORES = %s', [ Self.FListaErrores.Text ] ) );
          Add( Format( 'K_SQL = %s', [ Self.FSQL.Text ] ) );
          Add( Format( 'K_FILTRO_CLIENTE = %s', [ BoolToStr( Self.FiltroCliente ) ] ) );
          Add( Format( 'K_ORDEN_CLIENTE = %s', [ BoolToStr( Self.OrdenCliente ) ] ) );
          ColumnasToTextFile( Datos );
          FiltrosToTextFile( Datos );
          OrdenesToTextFile( Datos );
          GruposToTextFile( Datos );
          ParametrosToTextFile( Datos );
     end;
end;

procedure TSQLAgenteClient.AgenteToTextFile( const sFileName: String );
var
   FDatos: TStrings;
begin
     FDatos := TStringList.Create;
     try
        AgenteToTextFile( FDatos );     
        with FDatos do
        begin
             SaveToFile( sFileName );
        end;
     finally
            FreeAndNil( FDatos );
     end;
end;
{$endif}
end.
