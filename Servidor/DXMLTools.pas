unit DXMLTools;

interface

uses SysUtils, Classes, Controls, DB, XMLDom, XMLIntf, MSXMLDom, XMLDoc, XSLProd,
     ComObj, Variants,
     ZetaCommonClasses,
     ZetaCommonLists;

type
  TZetaXMLNode = IXMLNode;
  TdmXMLTools = class(TDataModule)
    XMLDocument: TXMLDocument;
    XSLPageProducer: TXSLPageProducer;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FErrores: TStrings;
    FWarnings: TStrings;
    FCurrentNode: TZetaXMLNode;
    FXML: TStrings;
    FXMLVersion: String;
    FXMLEncoding: String;
    FXMLStandAlone: Boolean;
    function GetXML: TStrings;
    function GetXMLAsText: String;
    function GetXMLBuildErrors: Boolean;
    function GetXMLBuildWarnings: Boolean;
    function GetXMLSetupTag: String;
    function GUID2XML(const Value: TGUID): String;
    procedure Clear;
    procedure SetXMLAsText(const Value: String);
    procedure WriteErrorLog;
    procedure WriteWarningLog;
  protected
    { Protected declarations }

    property CurrentNode: TZetaXMLNode read FCurrentNode;
    procedure SetCurrentNode( Node: TZetaXMLNode );
  public
    { Public declarations }
    property XML: TStrings read GetXML;
    property XMLAsText: String read GetXMLAsText write SetXMLAsText;
    property XMLBuildErrors: Boolean read GetXMLBuildErrors;
    property XMLBuildWarnings: Boolean read GetXMLBuildWarnings;
    property XMLVersion: String read FXMLVersion write FXMLVersion;
    property XMLEncoding: String read FXMLEncoding write FXMLEncoding;
    property XMLStandAlone: Boolean read FXMLStandAlone write FXMLStandAlone;
    property XMLSetupTag: String read GetXMLSetupTag;
    function DateXML( const dReferencia: TDate; const lForDisplay: Boolean = false ): String;
    function DateTimeXML( const dReferencia: TDateTime; const lForDisplay: Boolean = false ): String;
    function FechaCortaXML( const dReferencia: TDate; const lForDisplay: Boolean = false ): String;
    function FloatXML(const rValue: Extended): String;
    function IntegerXML(const iValue: Integer): String;
    function PesosXML(const rValue: TPesos): String;
    function TimeXML( const dReferencia: TTime; const lForDisplay: Boolean = false ): String;
    function StringToDate(const sFecha: String; const dDefault: TDate): TDate;
    function GetEmpresa: OleVariant;
    function GetFirstChild(Node: TZetaXMLNode): TZetaXMLNode;
    function GetFieldInfo(Campo: TField; const lForDisplay: Boolean = false ): String;
    function GetFieldType( Campo: TField ): eTipoGlobal;
    function GetNextSibling(Node: TZetaXMLNode): TZetaXMLNode;
    function GetNode( const sTagName: String; Padre: TZetaXMLNode ): TZetaXMLNode; overload;
    function GetNode( const sTagName: String ): TZetaXMLNode; overload;
    function HasAttributes(Node: TZetaXMLNode): Boolean;
    function HasChildren( Node: TZetaXMLNode ): Boolean;
    function AttributeAsBoolean(Node: TZetaXMLNode; const sAttributeName: String; const lDefault: Boolean): Boolean;
    function AttributeAsDate(Node: TZetaXMLNode; const sAttributeName: String; const dDefault: TDate): TDate;
    function AttributeAsDateTime(Node: TZetaXMLNode; const sAttributeName: String; const dDefault: TDateTime): TDateTime;
    function AttributeAsFloat(Node: TZetaXMLNode; const sAttributeName: String; const rDefault: Extended): Extended;
    function AttributeAsGUID(Node: TZetaXMLNode; const sAttributeName: String; const gDefault: TGUID): TGUID;
    function AttributeAsInteger(Node: TZetaXMLNode; const sAttributeName: String; const iDefault: Integer): Integer;
    function AttributeAsString(Node: TZetaXMLNode; const sAttributeName, sDefault: String): String;
    function TagAsBoolean(Node: TZetaXMLNode; const lDefault: Boolean): Boolean;
    function TagAsDate(Node: TZetaXMLNode; const dDefault: TDate): TDate;
    function TagAsDateTime(Node: TZetaXMLNode; const dDefault: TDateTime): TDateTime;
    function TagAsFloat(Node: TZetaXMLNode; const rDefault: Extended): Extended;
    function TagAsGUID(Node: TZetaXMLNode; const gDefault: TGUID): TGUID;
    function TagAsInteger(Node: TZetaXMLNode; const iDefault: Integer): Integer;
    function TagAsString(Node: TZetaXMLNode; const sDefault: String): String;
    function WriteStartElement(const sTagName: String): TZetaXMLNode;
    function WriteEndElement: TZetaXMLNode;
    function WriteValueBoolean( const sTagName: String; const lValue: Boolean ): TZetaXMLNode;
    function WriteValueDate( const sTagName: String; const dValue: TDate ): TZetaXMLNode;
    function WriteValueDateTime( const sTagName: String; const dValue: TDateTime ): TZetaXMLNode;
    function WriteValueFloat( const sTagName: String; const rValue: Extended ): TZetaXMLNode;
    function WriteValueGUID( const sTagName: String; const gValue: TGUID ): TZetaXMLNode;
    function WriteValueInteger( const sTagName: String; const iValue: Integer ): TZetaXMLNode;
    function WriteValuePesos( const sTagName: String; const rValue: TPesos ): TZetaXMLNode;
    function WriteValueString( const sTagName, sValue: String): TZetaXMLNode;
    function WriteValueTime( const sTagName: String; const dValue: TTime ): TZetaXMLNode;
    function XMLTransform(const sXML, sXSLFile, sXMLFile: String; var sOutput: String): Boolean; overload;
    function XMLTransform(const sXML, sXSLFile: String; var sOutput: String): Boolean; overload;
    procedure WriteAttributeDate(const sAtributo: String; const dValue: TDate; Nodo: TZetaXMLNode); overload;
    procedure WriteAttributeDate(const sAtributo: String; const dValue: TDate ); overload;
    procedure WriteAttributeDateTime(const sAtributo: String; const dValue: TDateTime; Nodo: TZetaXMLNode); overload;
    procedure WriteAttributeDateTime(const sAtributo: String; const dValue: TDateTime ); overload;
    procedure WriteAttributeTime(const sAtributo: String; const dValue: TTime; Nodo: TZetaXMLNode); overload;
    procedure WriteAttributeTime(const sAtributo: String; const dValue: TTime ); overload;
    procedure WriteAttributeBoolean(const sAtributo: String; const lValue: Boolean; Nodo: TZetaXMLNode); overload;
    procedure WriteAttributeBoolean(const sAtributo: String; const lValue: Boolean ); overload;
    procedure WriteAttributeFloat(const sAtributo: String; const rValue: Extended; Nodo: TZetaXMLNode); overload;
    procedure WriteAttributeFloat(const sAtributo: String; const rValue: Extended ); overload;
    procedure WriteAttributePesos(const sAtributo: String; const rValue: TPesos; Nodo: TZetaXMLNode); overload;
    procedure WriteAttributePesos(const sAtributo: String; const rValue: TPesos ); overload;
    procedure WriteAttributeInteger(const sAtributo: String; const iValue: Integer; Nodo: TZetaXMLNode); overload;
    procedure WriteAttributeInteger(const sAtributo: String; const iValue: Integer ); overload;
    procedure WriteAttributeString(const sAtributo, sValue: String; Nodo: TZetaXMLNode); overload;
    procedure WriteAttributeString(const sAtributo, sValue: String); overload;
    procedure WriteBoolean( const lValue: Boolean; Nodo: TZetaXMLNode ); overload;
    procedure WriteBoolean( const lValue: Boolean ); overload;
    procedure WriteDate( const dValue: TDate; Nodo: TZetaXMLNode ); overload;
    procedure WriteDate( const dValue: TDate ); overload;
    procedure WriteDateTime( const dValue: TDateTime; Nodo: TZetaXMLNode ); overload;
    procedure WriteDateTime( const dValue: TDateTime ); overload;
    procedure WriteFloat( const rValue: Extended; Nodo: TZetaXMLNode ); overload;
    procedure WriteFloat( const rValue: Extended ); overload;
    procedure WritePesos( const rValue: TPesos; Nodo: TZetaXMLNode ); overload;
    procedure WritePesos( const rValue: TPesos ); overload;
    procedure WriteGUID(const gValue: TGUID; Nodo: TZetaXMLNode); overload;
    procedure WriteGUID(const gValue: TGUID); overload;
    procedure WriteInteger( const iValue: Integer; Nodo: TZetaXMLNode ); overload;
    procedure WriteInteger( const iValue: Integer ); overload;
    procedure WriteString( const sValue: String; Nodo: TZetaXMLNode); overload;
    procedure WriteString( const sValue: String); overload;
    procedure WriteTime( const dValue: TTime; Nodo: TZetaXMLNode ); overload;
    procedure WriteTime( const dValue: TTime ); overload;
    procedure XMLBuildDataRow( Cursor: TDataset; const sTag: String; const lForDisplay: Boolean = false; const lCerrarTag: Boolean = true );
    procedure XMLBuildDataSet( Cursor: TDataset; const sTag: String; const iRecCuantos: Integer = 0; const iRecInicial: Integer = 1; const lForDisplay: Boolean = false );
    procedure XMLInit( const sTagName, sValue: String ); overload;
    procedure XMLInit( const sTagName: String ); overload;
    procedure XMLEnd;
    procedure XMLError(const sError: String);
    procedure XMLWarning(const sWarning: String);
    procedure XMLToDataSet( Datos: TDataset; const sTag: String );
  end;

{var
  dmXMLTools: TdmXMLTools;}

function ExcepcionAsXML( Error: Exception ): string;
function ErrorAsXML( Error: String ): string;

implementation

uses ZetaCommonTools;

{$R *.dfm}

const
     K_ERRORES = 'ERRORES';
     K_ERROR = 'ERROR';
     K_WARNINGS = 'WARNINGS';
     K_WARNING = 'WARNING';
     K_ROWS_TAG = 'ROWS';
     K_CUANTOS_TAG = 'CUANTOS';
     K_ROW_TAG = 'ROW';
     K_PAR_TAG = 'PAR';
     K_LABELS_TAG = 'LABELS';
     K_DATA_TYPE_TAG = 'DATATYPE';
     K_DATA_LENGTH_TAG = 'DATALENGTH';
     K_XML_SETUP_VERSION = 'xml version';
     aJustificacion: array[ TAlignment ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'LEFT', 'RIGHT', 'CENTER' );

function ExcepcionAsXML( Error: Exception ): string;
begin
     Result := ErrorAsXML( Error.Message );
end;

function ErrorAsXML( Error: String ): string;
begin
     Result := Format( '<DATOS><ERRORES><ERROR>%s</ERROR></ERRORES></DATOS>', [ Error ] );
end;

{ ***** TdmXMLTools ******** }

procedure TdmXMLTools.DataModuleCreate(Sender: TObject);
const
     ENCODING_ISO_8859_1 = 'iso-8859-1';
     ENCODING_UTF_8 = 'UTF-8';
begin
     FXMLVersion := '1.0';
     FXMLEncoding := ENCODING_ISO_8859_1;
     FXMLStandAlone := True;
     FXML := TStringList.Create;
     FErrores := TStringList.Create;
     FWarnings := TStringList.Create;
     FCurrentNode := nil;
     with XMLDocument do
     begin
          Options := [ doNodeAutoCreate,  doAutoPrefix, doNamespaceDecl ];
          {$ifdef RDDAPP}
          Options := Options + [doNodeAutoIndent];
          {$endif}
          ParseOptions := [];
     end;
end;

procedure TdmXMLTools.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FWarnings );
     FreeAndNil( FErrores );
     FreeAndNil( FXML );
end;

procedure TdmXMLTools.Clear;
begin
     FXML.Clear;
     FErrores.Clear;
     FWarnings.Clear;
end;

function TdmXMLTools.GetNode( const sTagName: String; Padre: TZetaXMLNode ): TZetaXMLNode;
begin
     with XMLDocument do
     begin
          if Active then
             Result := Padre.ChildNodes.FindNode( sTagName )
          else
              Result := nil;
     end;
end;

function TdmXMLTools.GetNode( const sTagName: String ): TZetaXMLNode;
begin
     with XMLDocument do
     begin
          Result := GetNode( sTagName, DocumentElement );
     end;
end;

function TdmXMLTools.GetXML: TStrings;
begin
     Result := FXML;
end;

function TdmXMLTools.GetXMLAsText: String;
begin
     Result := FXML.Text;
end;

procedure TdmXMLTools.SetXMLAsText(const Value: String);
const
     K_XML_SETUP_VERSION = 'xml version';
begin
     with XMLDocument do
     begin
          Active := False;
          if ( Pos( K_XML_SETUP_VERSION, Value ) > 0 ) then
             LoadFromXML( Value )
          else
              LoadFromXML( XMLSetupTag + Value );
          Active := True;
          {
          FCurrentNode := XMLDocument.ChildNodes.First;
          }
          FCurrentNode := DocumentElement;
     end;
end;

function TdmXMLTools.GetXMLSetupTag: String;
const
     SETUP_TAG = '<?%s="%s" encoding="%s" standalone="%s"?>';
     aStandAlone: array[ False..True ] of String = ( 'no', 'yes' );
begin
     Result := Format( SETUP_TAG, [ K_XML_SETUP_VERSION, XMLVersion, XMLEncoding, aStandAlone[ XMLStandAlone ] ] );
end;

function TdmXMLTools.HasChildren( Node: TZetaXMLNode ): Boolean;
begin
     Result := Assigned( Node ) and Node.HasChildNodes;
end;

function TdmXMLTools.HasAttributes( Node: TZetaXMLNode ): Boolean;
begin
     Result := Assigned( Node ) and ( Node.AttributeNodes.Count > 0 )
end;

function TdmXMLTools.GetEmpresa: OleVariant;
const
     K_EMPRESA: WideString = 'EMPRESA';
     K_DATOS: WideString = 'DATOS';
     K_USUARIO: WideString = 'USERNAME';
     K_CLAVE: WideString = 'PASSWORD';
     K_USCODIGO: WideString = 'USUARIO';
     K_NIVEL0: WideString = 'NIVEL0';
     K_CODIGO: WideString = 'CODIGO';
var
   Empresa: TZetaXMLNode;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5;
}
begin
     Empresa := GetNode( K_EMPRESA );
     if Assigned( Empresa ) then
     begin
          Result := VarArrayOf( [ AttributeAsString( Empresa, K_DATOS, VACIO ),
                                  AttributeAsString( Empresa, K_USUARIO, VACIO ),
                                  AttributeAsString( Empresa, K_CLAVE, VACIO ),
                                  AttributeAsInteger( Empresa, K_USCODIGO, 0 ),
                                  AttributeAsString( Empresa, K_NIVEL0, VACIO ),
                                  TagAsString( Empresa, VACIO ) ] );
     end;
end;

function TdmXMLTools.GetFirstChild( Node: TZetaXMLNode ): TZetaXMLNode;
begin
     Result := Node.ChildNodes.First;
end;

function TdmXMLTools.GetNextSibling( Node: TZetaXMLNode ): TZetaXMLNode;
begin
     Result := Node.NextSibling;
end;

function TdmXMLTools.AttributeAsBoolean( Node: TZetaXMLNode; const sAttributeName: String; const lDefault: Boolean ): Boolean;
begin
     Result := TagAsBoolean( Node.AttributeNodes.Nodes[ sAttributeName ], lDefault );
end;

function TdmXMLTools.AttributeAsDate( Node: TZetaXMLNode; const sAttributeName: String; const dDefault: TDate ): TDate;
begin
     Result := TagAsDate( Node.AttributeNodes.Nodes[ sAttributeName ], dDefault );
end;

function TdmXMLTools.AttributeAsDateTime( Node: TZetaXMLNode; const sAttributeName: String; const dDefault: TDateTime ): TDateTime;
begin
     Result := TagAsDateTime( Node.AttributeNodes.Nodes[ sAttributeName ], dDefault );
end;

function TdmXMLTools.AttributeAsFloat( Node: TZetaXMLNode; const sAttributeName: String; const rDefault: Extended ): Extended;
begin
     Result := TagAsFloat( Node.AttributeNodes.Nodes[ sAttributeName ], rDefault );
end;

function TdmXMLTools.AttributeAsGUID( Node: TZetaXMLNode; const sAttributeName: String; const gDefault: TGUID ): TGUID;
begin
     Result := TagAsGUID( Node.AttributeNodes.Nodes[ sAttributeName ], gDefault );
end;

function TdmXMLTools.AttributeAsInteger( Node: TZetaXMLNode; const sAttributeName: String; const iDefault: Integer ): Integer;
begin
     Result := TagAsInteger( Node.AttributeNodes.Nodes[ sAttributeName ], iDefault );
end;

function TdmXMLTools.AttributeAsString( Node: TZetaXMLNode; const sAttributeName, sDefault: String ): String;
begin
     Result := TagAsString( Node.AttributeNodes.Nodes[ sAttributeName ], sDefault );
end;

function TdmXMLTools.TagAsBoolean( Node: TZetaXMLNode; const lDefault: Boolean ): Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( TagAsString( Node, ZetaCommonTools.zBoolToStr( lDefault ) ) );
end;

function TdmXMLTools.TagAsDate( Node: TZetaXMLNode; const dDefault: TDate ): TDate;
begin
     Result := StringToDate( TagAsString( Node, '' ), dDefault );
end;

function TdmXMLTools.TagAsDateTime( Node: TZetaXMLNode; const dDefault: TDateTime ): TDateTime;
begin
     Result := StringToDate( TagAsString( Node, '' ), dDefault );
end;

function TdmXMLTools.TagAsFloat( Node: TZetaXMLNode; const rDefault: Extended ): Extended;
begin
     Result := ZetaCommonTools.StrToReal( TagAsString( Node, FloatToStr( rDefault ) ) );
end;

function TdmXMLTools.TagAsGUID( Node: TZetaXMLNode; const gDefault: TGUID ): TGUID;
begin
     Result := ComObj.StringToGUID( TagAsString( Node, ComObj.GUIDToString( gDefault ) ) );
end;

function TdmXMLTools.TagAsInteger( Node: TZetaXMLNode; const iDefault: Integer ): Integer;
begin
     Result := StrToIntDef( TagAsString( Node, IntegerXML( iDefault ) ), 0 );
end;

function TdmXMLTools.TagAsString( Node: TZetaXMLNode; const sDefault: String ): String;
begin
     if Assigned( Node ) then
     begin
          Result := Node.Text;
          if ZetaCommonTools.StrVacio( Result ) then
              Result := sDefault;
     end
     else
         Result := sDefault;
end;

function TdmXMLTools.FechaCortaXML( const dReferencia: TDate; const lForDisplay: Boolean = false ): String;
begin
     Result := DateXML( dReferencia, lForDisplay );
end;

function TdmXMLTools.DateXML( const dReferencia: TDate; const lForDisplay: Boolean = false ): String;
const
     aFormato: array[ False..True ] of String = ( 'yyyy/mm/dd', 'dd/mmm/yyyy' );
begin
     if ( dReferencia <= 0 ) then
        Result := VACIO
     else
         Result := FormatDateTime( aFormato[ lForDisplay ], dReferencia );
end;

function TdmXMLTools.DateTimeXML( const dReferencia: TDateTime; const lForDisplay: Boolean = false ): String;
const
     aFormato: array[ False..True ] of String = ( 'yyyy/mm/dd hh:nn', 'dd/mmm/yyyy hh:nn' );
begin
     if ( dReferencia <= 0 ) then
        Result := VACIO
     else
         Result := FormatDateTime( aFormato[ lForDisplay ], dReferencia );
end;

function TdmXMLTools.TimeXML( const dReferencia: TTime; const lForDisplay: Boolean = false ): String;
const
     aFormato: array[ False..True ] of String = ( 'hh:nn', 'hh:nn AM/PM' );
begin
     if ( dReferencia <= 0 ) then
        Result := VACIO
     else
         Result := FormatDateTime( aFormato[ lForDisplay ], dReferencia );
end;

function TdmXMLTools.StringToDate( const sFecha: String; const dDefault: TDate ): TDate;
var
   sValor: String;
begin
     Result := dDefault;
     if ZetaCommonTools.StrLleno( sFecha ) then
     begin
          try
             sValor := StringReplace( sFecha, '/', '', [ rfReplaceAll ] );
             Result := CodificaFecha( StrToIntDef( Copy( sValor, 1, 4 ), 0 ),
                                      StrToIntDef( Copy( sValor, 5, 2 ), 0 ),
                                      StrToIntDef( Copy( sValor, 7, 2 ), 0 ) );
          except
          end;
     end;
end; 

function TdmXMLTools.GUID2XML( const Value: TGUID ): String;
const
     K_CARACTERES = 2;
begin
     Result := ComObj.GUIDToString( Value );
     Result := Copy( Result, K_CARACTERES, ( Length( Result ) - K_CARACTERES ) );
end;

function TdmXMLTools.IntegerXML( const iValue: Integer ): String;
begin
     Result := IntToStr( iValue );
end;

function TdmXMLTools.FloatXML( const rValue: Extended ): String;
begin
     Result := FormatFloat( '#.######', rValue )
end;

function TdmXMLTools.PesosXML( const rValue: TPesos ): String;
begin
     Result := FormatFloat( '#.00', rValue )
end;

function TdmXMLTools.GetFieldInfo( Campo: TField; const lForDisplay: Boolean = false ): String;
var
   sTexto: string;
begin
     with Campo do
     begin
          sTexto := DisplayText;
          // Si se tiene un formateo por OnGetText, se deja este
          if ( not ( DataType in [ ftBlob, ftMemo ] ) ) and ( sTexto <> AsString ) then
             Result := sTexto
          else
          begin
               case DataType of
                    ftSmallint, ftInteger, ftWord: Result := IntegerXML( AsInteger );
                    ftString: Result := AsString;
                    ftBoolean: Result := ZetaCommonTools.zBoolToStr( AsBoolean );
                    ftFloat: Result := FloatXML( AsFloat );
                    ftCurrency: Result := PesosXML( AsFloat );
                    ftDate: Result := DateXML( AsDateTime, lForDisplay );
                    ftDateTime: Result := DateTimeXML( AsDateTime, lForDisplay );
                    ftTime: Result := TimeXML( AsDateTime, lForDisplay );
               else
                   Result := AsString;
               end;
          end;
     end;
end;

function TdmXMLTools.GetFieldType( Campo: TField ): eTipoGlobal;
begin
     with Campo do
     begin
          case DataType of
               ftSmallint, ftInteger, ftWord: Result := tgNumero;
               ftString: Result := tgTexto;
               ftBoolean: Result := tgTexto;
               ftFloat: Result := tgFloat;
               ftCurrency: Result := tgFloat;
               ftDate: Result := tgFecha;
               ftDateTime: Result := tgFecha;
               ftTime: Result := tgFecha;
          else
              Result := tgAutomatico;
          end;
     end;
end;

function TdmXMLTools.WriteStartElement( const sTagName: String ): TZetaXMLNode;
begin
     FCurrentNode := FCurrentNode.AddChild( sTagName, -1 );
     Result := FCurrentNode;
end;

procedure TdmXMLTools.WriteBoolean( const lValue: Boolean; Nodo: TZetaXMLNode );
begin
     WriteString( ZetaCommonTools.zBoolToStr( lValue ), Nodo );
end;

procedure TdmXMLTools.WriteBoolean( const lValue: Boolean );
begin
     WriteBoolean( lValue, CurrentNode );
end;

procedure TdmXMLTools.WriteDate( const dValue: TDate; Nodo: TZetaXMLNode );
begin
     WriteString( DateXML( dValue ), Nodo );
end;

procedure TdmXMLTools.WriteDate( const dValue: TDate );
begin
     WriteDate( dValue, CurrentNode );
end;

procedure TdmXMLTools.WriteDateTime( const dValue: TDateTime; Nodo: TZetaXMLNode );
begin
     WriteString( FormatDateTime( 'dd/mm/yyyy hh:nn', dValue ), Nodo );
end;

procedure TdmXMLTools.WriteDateTime( const dValue: TDateTime );
begin
     WriteDate( dValue, CurrentNode );
end;

procedure TdmXMLTools.WriteFloat(const rValue: Extended; Nodo: TZetaXMLNode);
begin
     WriteString( FloatXML( rValue ), Nodo );
end;

procedure TdmXMLTools.WriteFloat(const rValue: Extended);
begin
     WriteFloat( rValue, CurrentNode );
end;

procedure TdmXMLTools.WritePesos(const rValue: TPesos; Nodo: TZetaXMLNode);
begin
     WriteString( PesosXML( rValue ), Nodo );
end;

procedure TdmXMLTools.WritePesos(const rValue: TPesos);
begin
     WritePesos( rValue, CurrentNode );
end;

procedure TdmXMLTools.WriteGUID(const gValue: TGUID; Nodo: TZetaXMLNode);
begin
     WriteString( GUID2XML( gValue ), Nodo );
end;

procedure TdmXMLTools.WriteGUID(const gValue: TGUID);
begin
     WriteGUID( gValue, CurrentNode );
end;

procedure TdmXMLTools.WriteInteger(const iValue: Integer; Nodo: TZetaXMLNode);
begin
     WriteString( IntegerXML( iValue ), Nodo );
end;

procedure TdmXMLTools.WriteInteger(const iValue: Integer);
begin
     WriteInteger( iValue, CurrentNode );
end;

procedure TdmXMLTools.WriteString( const sValue: String; Nodo: TZetaXMLNode );
begin
     with Nodo do
     begin
          Text := sValue;
     end;
end;

procedure TdmXMLTools.WriteString( const sValue: String );
begin
     WriteString( sValue, CurrentNode );
end;

procedure TdmXMLTools.WriteTime( const dValue: TTime; Nodo: TZetaXMLNode );
begin
     WriteString( TimeXML( dValue ), Nodo );
end;

procedure TdmXMLTools.WriteTime( const dValue: TTime );
begin
     WriteDate( dValue, CurrentNode );
end;

function TdmXMLTools.WriteValueBoolean(const sTagName: String; const lValue: Boolean): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WriteBoolean( lValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteValueDate(const sTagName: String; const dValue: TDate): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WriteDate( dValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteValueDateTime(const sTagName: String; const dValue: TDateTime): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WriteDateTime( dValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteValueFloat(const sTagName: String; const rValue: Extended): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WriteFloat( rValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteValuePesos(const sTagName: String; const rValue: TPesos): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WritePesos( rValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteValueGUID( const sTagName: String; const gValue: TGUID ): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WriteGUID( gValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteValueInteger(const sTagName: String; const iValue: Integer): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WriteInteger( iValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteValueString(const sTagName, sValue: String): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WriteString( sValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteValueTime(const sTagName: String; const dValue: TTime): TZetaXMLNode;
begin
     Result := WriteStartElement( sTagName );
     WriteTime( dValue, Result );
     WriteEndElement;
end;

function TdmXMLTools.WriteEndElement: TZetaXMLNode;
begin
     FCurrentNode := CurrentNode.ParentNode;
end;

function TdmXMLTools.GetXMLBuildErrors: Boolean;
begin
     Result := ( FErrores.Count > 0 );
end;

function TdmXMLTools.GetXMLBuildWarnings: Boolean;
begin
     Result := ( FWarnings.Count > 0 );
end;

procedure TdmXMLTools.XMLError( const sError: String );
begin
     FErrores.Add( sError );
end;

procedure TdmXMLTools.XMLWarning( const sWarning: String );
begin
     FWarnings.Add( sWarning );
end;

procedure TdmXMLTools.XMLInit( const sTagName, sValue: String );
begin
     Clear;
     {
     XMLAsText := Format( '<%0:s>%1:s</%0:s>', [ sTagName, sValue ] );
     }
     XMLAsText := Format( '%0:s<%1:s>%2:s</%1:s>', [ GetXMLSetupTag, sTagName, sValue ] );
     {
     with XMLDocument do
     begin
          Version := XMLVersion;
          Encoding := XMLEncoding;
          StandAlone := aStandAlone[ XMLStandAlone ];
     end;
     }
end;

procedure TdmXMLTools.XMLInit( const sTagName: String );
begin
     XMLInit( sTagName, VACIO );
end;

procedure TdmXMLTools.XMLBuildDataRow( Cursor: TDataset; const sTag: String; const lForDisplay: Boolean = false; const lCerrarTag: Boolean = true );
var
   i: Integer;
   Campo: TField;
begin
     with Cursor do
     begin
          First;
          while not EOF do
          begin
               WriteStartElement( sTag );
               try
                  for i := 0 to ( FieldCount - 1 ) do
                  begin
                       Campo := Fields[ i ];
                       WriteAttributeString( Campo.FieldName, GetFieldInfo( Campo, lForDisplay ) );
                  end;
               finally
                      if lCerrarTag then
                         WriteEndElement;
               end;
               Next;
          end;
     end;
end;

procedure TdmXMLTools.XMLBuildDataSet( Cursor: TDataset; const sTag: String; const iRecCuantos: Integer = 0; const iRecInicial: Integer = 1; const lForDisplay: Boolean = false );
const
     aRowIsEven: array[ False..True ] of String = ( '0', '1' );
var
   i, iRecTope, iRecNo, iCuantos: Integer;
   lEvenRow: Boolean;
   Nodo: TZetaXMLNode;
begin
     WriteStartElement( sTag );
     try
        with Cursor do
        begin
             { Agrega Labels }
             WriteStartElement( K_LABELS_TAG );
             try
                for i := 0 to ( FieldCount - 1 ) do
                begin
                     with Fields[ i ] do
                     begin
                          WriteAttributeString( FieldName, DisplayLabel );
                     end;
                end;
             finally
                    WriteEndElement;
             end;
             { Agrega Tipos de datos }
             WriteStartElement( K_DATA_TYPE_TAG );
             try
                for i := 0 to ( FieldCount - 1 ) do
                begin
                     with Fields[ i ] do
                     begin
                          WriteAttributeInteger( FieldName, Ord( GetFieldType( Fields[ i ] ) ) );
                     end;
                end;
             finally
                    WriteEndElement;
             end;
             { Agrega longitud de los datos }
             WriteStartElement( K_DATA_LENGTH_TAG );
             try
                for i := 0 to ( FieldCount - 1 ) do
                begin
                     with Fields[ i ] do
                     begin
                          WriteAttributeInteger( FieldName, Fields[ i ].Size );
                     end;
                end;
             finally
                    WriteEndElement;
             end;
             { Agrega Registros }
             iRecTope := iRecInicial + iRecCuantos - 1;
             iRecNo := 0;
             iCuantos := 0;
             lEvenRow := False;
             Nodo := WriteStartElement( K_ROWS_TAG );
             try
                First;
                while not Eof do
                begin
                     Inc( iRecNo );
                     if ( ( iRecCuantos = 0 ) or ( ( iRecno >= iRecInicial ) and ( iRecno <= iRecTope ) ) ) then
                     begin
                          { Agrega Registro }
                          WriteStartElement( K_ROW_TAG );
                          try
                             WriteAttributeString( K_PAR_TAG, aRowIsEven[ lEvenRow ] );
                             for i := 0 to ( FieldCount - 1 ) do
                             begin
                                  WriteAttributeString( Fields[ i ].FieldName, GetFieldInfo( Fields[ i ], lForDisplay ) );
                             end;
                          finally
                                 WriteEndElement;
                          end;
                          lEvenRow := not lEvenRow;
                          Inc( iCuantos );
                     end;
                     Next;
                end;
             finally
                    WriteEndElement;
             end;
             WriteValueInteger( K_CUANTOS_TAG, iCuantos );
        end;
     finally
            WriteEndElement;
     end;
end;

procedure TdmXMLTools.WriteErrorLog;
var
   iPtr: Integer;
begin
     with FErrores do
     begin
          if ( Count > 0 ) then
          begin
               WriteStartElement( K_ERRORES );
               for iPtr := 0 to ( Count - 1 ) do
               begin
                    WriteStartElement( K_ERROR );
                    WriteString( Strings[ iPtr ] );
                    WriteEndElement;
               end;
               WriteEndElement;
          end;
     end;
end;

procedure TdmXMLTools.WriteWarningLog;
var
   iPtr: Integer;
begin
     with FWarnings do
     begin
          if ( Count > 0 ) then
          begin
               WriteStartElement( K_WARNINGS );
	       for iPtr := 0 to ( Count - 1 ) do
               begin
                    WriteStartElement( K_WARNING );
                    WriteString( Strings[ iPtr ] );
                    WriteEndElement;
               end;
               WriteEndElement;
          end;
     end;
end;

procedure TdmXMLTools.XMLEnd;
begin
     WriteErrorLog;
     WriteWarningLog;
     { Finalizar El Tag Principal Del Documento }
     WriteEndElement;
     with XMLDocument do
     begin
          Self.FXML.Assign( XML );
          Active := False;
     end;
end;

function TdmXMLTools.XMLTransform(const sXML, sXSLFile, sXMLFile: String; var sOutput: String): Boolean;
begin
     Result := False;
     if ZetaCommonTools.StrLleno( sXSLFile ) then
     begin
          if SysUtils.FileExists( sXSLFile ) then
          begin
               sOutput := VACIO;
               try
                  XMLAsText := sXML;
                  if ZetaCommonTools.StrLleno( sXMLFile ) then
                  begin
                       with XMLDocument do
                       begin
                            SaveToFile( sXMLFile );
                       end;
                  end;
                  with XSLPageProducer do
                  begin
                       Active := False;
                       FileName := sXSLFile;
                       sOutput := Content;
                  end;
                  Result := True;
               except
                     on Error: Exception do
                     begin
                          sOutput := Error.Message;
                     end;
               end;
          end
          else
          begin
               sOutput := Format( 'Archivo %s No Existe', [ sXSLFile ] );
          end;
     end
     else
     begin
          sOutput := 'Archivo XSL No Fu� Especificado';
     end;
     with XMLDocument do
     begin
          Active := False;
     end;
     with XSLPageProducer do
     begin
          Active := False;
     end;
end;

function TdmXMLTools.XMLTransform( const sXML, sXSLFile: String; var sOutput: String ): Boolean;
begin
     Result := XMLTransform( sXML, sXSLFile, VACIO, sOutput );
end;

procedure TdmXMLTools.XMLToDataSet(Datos: TDataset; const sTag: String );
begin
     with Datos do
     begin
     end;
end;

procedure TdmXMLTools.WriteAttributeString( const sAtributo, sValue: String; Nodo: TZetaXMLNode );
begin
     with Nodo do
     begin
          SetAttribute( sAtributo, sValue );
     end;
end;

procedure TdmXMLTools.WriteAttributeString( const sAtributo, sValue: String );
begin
     WriteAttributeString( sAtributo, sValue, CurrentNode );
end;

procedure TdmXMLTools.WriteAttributeBoolean(const sAtributo: String; const lValue: Boolean; Nodo: TZetaXMLNode);
begin
     WriteAttributeString( sAtributo, ZetaCommonTools.zBoolToStr( lValue ), Nodo );
end;

procedure TdmXMLTools.WriteAttributeBoolean(const sAtributo: String; const lValue: Boolean);
begin
     WriteAttributeBoolean( sAtributo, lValue, CurrentNode );
end;

procedure TdmXMLTools.WriteAttributeDate(const sAtributo: String; const dValue: TDate; Nodo: TZetaXMLNode);
begin
     WriteAttributeString( sAtributo, DateXML( dValue ), Nodo );
end;

procedure TdmXMLTools.WriteAttributeDate(const sAtributo: String; const dValue: TDate);
begin
     WriteAttributeDate( sAtributo, dValue, CurrentNode );
end;

procedure TdmXMLTools.WriteAttributeDateTime(const sAtributo: String; const dValue: TDateTime; Nodo: TZetaXMLNode);
begin
     WriteAttributeString( sAtributo, DateTimeXML( dValue ), Nodo );
end;

procedure TdmXMLTools.WriteAttributeDateTime(const sAtributo: String; const dValue: TDateTime);
begin
     WriteAttributeDateTime( sAtributo, dValue, CurrentNode );
end;

procedure TdmXMLTools.WriteAttributeFloat(const sAtributo: String; const rValue: Extended; Nodo: TZetaXMLNode);
begin
     WriteAttributeString( sAtributo, FloatXML( rValue ), Nodo );
end;

procedure TdmXMLTools.WriteAttributeFloat(const sAtributo: String; const rValue: Extended);
begin
     WriteAttributeFloat( sAtributo, rValue, CurrentNode );
end;

procedure TdmXMLTools.WriteAttributePesos(const sAtributo: String; const rValue: TPesos; Nodo: TZetaXMLNode);
begin
     WriteAttributeString( sAtributo, PesosXML( rValue ), Nodo );
end;

procedure TdmXMLTools.WriteAttributePesos(const sAtributo: String; const rValue: TPesos);
begin
     WriteAttributePesos( sAtributo, rValue, CurrentNode );
end;

procedure TdmXMLTools.WriteAttributeInteger(const sAtributo: String; const iValue: Integer; Nodo: TZetaXMLNode);
begin
     WriteAttributeString( sAtributo, IntegerXML( iValue ), Nodo );
end;

procedure TdmXMLTools.WriteAttributeInteger(const sAtributo: String; const iValue: Integer);
begin
     WriteAttributeInteger( sAtributo, iValue, CurrentNode );
end;

procedure TdmXMLTools.WriteAttributeTime(const sAtributo: String; const dValue: TTime; Nodo: TZetaXMLNode);
begin
     WriteAttributeString( sAtributo, TimeXML( dValue ), Nodo );
end;

procedure TdmXMLTools.WriteAttributeTime(const sAtributo: String; const dValue: TTime);
begin
     WriteAttributeTime( sAtributo, dValue, CurrentNode );
end;

procedure TdmXMLTools.SetCurrentNode(Node: TZetaXMLNode);
begin
     FCurrentNode := Node;
end;

end.
