unit ZBaseCreator;

interface
{$INCLUDE DEFINES.INC}
uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
{$ifdef RDD}
     DEntidades,
{$else}
     DEntidadesTress,
{$endif}
     DExisteCampo,
     DZetaServerProvider,
     ZetaQRExpr,
     ZetaCommonClasses,
     ZetaCommonLists;

type
  TipoFunciones = set of eFunciones;

  TZetaBaseCreator = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FLibrary: TQRFunctionLibrary;
    {$ifdef RDD}
    FEntidadesTress: TdmEntidades;
    {$else}
    FEntidadesTress: TdmEntidadesTress;
    {$endif}
    FSuperReporte: TDataModule;
    FExisteCampo : TdmExisteCampo;
    function GetProvider: TdmZetaServerProvider;
    procedure DesPreparaExisteCampo;
  public
    { Public declarations }
    property oZetaProvider: TdmZetaServerProvider read GetProvider;
    {$ifdef RDD}
    property dmEntidadesTress: TdmEntidades read FEntidadesTress;
    {$else}
    property dmEntidadesTress: TdmEntidadesTress read FEntidadesTress;
    {$endif}

    property dmExisteCampo: TdmExisteCampo read FExisteCampo;
    property FunctionLibrary: TQRFunctionLibrary read FLibrary;
    property SuperReporte : TDataModule read FSuperReporte write FSuperReporte;

    procedure PreparaEntidades;
    procedure PreparaExisteCampo;
    procedure RegistraFunciones( Funciones: TipoFunciones );virtual;
end;

implementation

uses
     ZFuncsGenerales,
     ZFuncsReporte,
     ZetaCommonTools;

{$R *.DFM}

{ ************** TZetaCreator **************** }

procedure TZetaBaseCreator.DataModuleCreate(Sender: TObject);
begin
     FLibrary := TQRFunctionLibrary.Create;
end;

procedure TZetaBaseCreator.DataModuleDestroy(Sender: TObject);
begin
     DesPreparaExisteCampo;
     FreeAndNil( FLibrary );
end;

function TZetaBaseCreator.GetProvider: TdmZetaServerProvider;
begin
     Result := TdmZetaServerProvider( Owner );
end;

{ ************** Evaluador **************** }

procedure TZetaBaseCreator.RegistraFunciones( Funciones: TipoFunciones );
begin
     FunctionLibrary.EntryList.Clear;
end;

procedure TZetaBaseCreator.PreparaExisteCampo;
begin
     if not Assigned( FExisteCampo ) then
        FExisteCampo := TdmExisteCampo.Create( Self );
end;

procedure TZetaBaseCreator.PreparaEntidades;
begin
     if not Assigned( FEntidadesTress ) then
     {$ifdef RDD}
        FEntidadesTress := TdmEntidades.Create( oZetaProvider );
     {$else}
        FEntidadesTress := TdmEntidadesTress.Create( oZetaProvider );
     {$endif}
end;

procedure TZetaBaseCreator.DesPreparaExisteCampo;
begin
     FreeAndNil( FExisteCampo );
end;

end.
