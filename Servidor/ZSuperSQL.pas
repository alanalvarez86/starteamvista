unit ZSuperSQL;

interface

{$DEFINE MULTIPLES_ENTIDADES}
{$INCLUDE DEFINES.INC}
uses Classes, SysUtils,Forms,{$ifndef VER130}Variants,{$endif}
     ZetaEntidad,
     ZEvaluador,
     ZAgenteSQLClient,
     ZAgenteSQL,
     ZCreator,
     ZetaSQL,
     ZetaTipoEntidad,
     ZetaCommonLists,
     {$ifdef RDD}
     DEntidades,
     {$else}
     DEntidadesTress,
     {$endif}
     DZetaServerProvider;
const
    K_MAX_SUB_TOT = 30; {m�ximo de campos subjetos a ser SubTot( x )}

type
  TSuperSQL = class(TObject)
  private
    // Entidad principal
    FCreator: TZetaCreator;
    FAgente: TSQLAgente;
    FEvaluador: TZetaEvaluador;
    // Listas privadas
    FListaSelect : TStringList;
    FListaAlias  : TStringList;
    FListaDuplicados : TStringList;
    FListaRequeridos : TStringList;
    FListaJoins : TStringList;
    FListaWhere : TStringList;
    FListaOrderBy : TStringList;
    FListaGroupBy : TStringList;

    // Agrega elementos al estatuto SQL
    procedure ConstruyeQuery;
    procedure LimpiaListas;
    procedure ConstruyeSelect;
    procedure ConstruyeWhere;
    procedure ConstruyeOrderBy;
    procedure ConstruyeGroupBy;
    procedure ConstruyeFrom;
    // Valida sintaxis y obtiene campos requeridos
    procedure ValidaRequeridos;
    procedure ValidaColumnas;
    procedure MarcaSubTotales;
    procedure ValidaFiltros;
    procedure ValidaOrdenes;
    procedure ValidaOrdenesCliente;
    procedure ValidaGrupos;
    procedure ValidaTablas;
    procedure EvaluaColumnas;
    function  AgregaColumna( const sColumna, sAlias : String; const eTipo : TipoEntidad ) : String;
    procedure AgregaWhere( const sExpresion : String; const eTipo : TipoEntidad );
    procedure IncluyeRequeridos( var sFormula : String );
//  function  GetEntidad( oLista : TStringList; const iPos : Integer ) : TipoEntidad;

    function  ErrorGeneral( const sError : String ) : Integer;
    function  HayErrores : Boolean;

    function  oZetaProvider : TdmZetaServerProvider;
    {$ifdef RDD}
    function  dmEntidadesTress : TdmEntidades;
    {$else}
    function  dmEntidadesTress : TdmEntidadesTress;
    {$endif}

  public
    constructor Create( oCreator : TZetaCreator );
    destructor  Destroy; override;
    function    Construye( Agente : TSQLAgente; oCalcNomina : TObject = NIL ) : Boolean;
    function    EvaluaParams( Agente : TSQLAgente ) : Boolean;
  end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses;

const K_COLUMNA_SQL = 'COLUMNA';

function AgregaConjunto( Lista : TStringList; s : String ) : Integer;
begin
     with Lista do
     begin
          Result := IndexOf( s );
          if ( Result < 0 ) then
             Result := Add( s );
     end;
end;

function AgregaConjuntoObjeto( Lista : TStringList; s : String; o : TObject ) : Integer;
begin
     with Lista do
     begin
          Result := IndexOf( s );
          if ( Result < 0 ) then
             Result := AddObject( s, o );
     end;
end;

{ ********* TSuperSQL ********** }

constructor TSuperSQL.Create( oCreator: TZetaCreator );
begin
     FCreator := oCreator;
     FCreator.PreparaEntidades;
     FEvaluador := TZetaEvaluador.Create( FCreator );
     FListaSelect := TStringList.Create;
     FListaAlias  := TStringList.Create;
     FListaDuplicados := TStringList.Create;
     FListaRequeridos := TStringList.Create;
     FListaJoins := TStringList.Create;
     FListaWhere := TStringList.Create;
     FListaOrderBy := TStringList.Create;
     FListaGroupBy := TStringList.Create;
end;

function TSuperSQL.oZetaProvider: TdmZetaServerProvider;
begin
     Result := FCreator.oZetaProvider;
end;

{$ifdef RDD}
function TSuperSQL.dmEntidadesTress: TdmEntidades;
begin
     Result := FCreator.dmEntidadesTress;
end;
{$else}
function TSuperSQL.dmEntidadesTress: TdmEntidadesTress;
begin
     Result := FCreator.dmEntidadesTress;
end;
{$endif}


destructor TSuperSQL.Destroy;
begin
     FEvaluador.Free;
     FListaSelect.Free;
     FListaAlias.Free;
     FListaDuplicados.Free;
     FListaRequeridos.Free;
     FListaJoins.Free;
     FListaWhere.Free;
     FListaOrderBy.Free;
     FListaGroupBy.Free;
     inherited;
end;

procedure TSuperSQL.ConstruyeSelect;
var
   iNumColumnas : Integer;
   sColumna : String;
   i : Integer;
begin
     // Agrega alias COLUMNAi a todas las columnas de DICCION
     // Se necesita para distinguir NIVEL1.TB_ELEMENT de EDOCIVIL.TB_ELEMENT
     with FListaSelect do
       for i := 0 to Count-1 do
          // Strings[i] := ExpresionAlias( Strings[i], K_COLUMNA_SQL + IntToStr( i ));
          Strings[i] := ExpresionAlias( Strings[i], FListaAlias[ i ] );

     // Pasa todos los campos requeridos por evaluador a la lista de Select
     // tanto requeridos por Columnas como por funciones de Filtros
     with FListaRequeridos do
       for i := 0 to Count-1 do
           FListaSelect.Add( LimpiaFormula( Strings[i] ));

     iNumColumnas := FListaSelect.Count;
     if ( iNumColumnas > 0 ) then
     begin
         Dec( iNumColumnas );
         FAgente.SQL.Add( K_SELECTFIELD );
         for i := 0 to iNumColumnas do
         begin
              sColumna := FListaSelect.Strings[ i ];
              if ( i < iNumColumnas ) then
                 sColumna := sColumna + K_COMA;
              FAgente.SQL.Add( sColumna );
         end;
     end
     else
         ErrorGeneral( 'No hay Columnas por agregar ' );
end;

procedure TSuperSQL.ConstruyeWhere;
var
   i, iNumFiltros : Integer;
   oFiltro : TSQLFiltro;
   sFiltro : String;
   lHuboWhere : Boolean;
begin
     FAgente.FiltroCliente := FALSE;
     lHuboWhere := FALSE;
     iNumFiltros := FAgente.NumFiltros;
     if ( iNumFiltros > 0 ) then
     begin
         Dec( iNumFiltros );
         for i := 0 to iNumFiltros do
         begin
              oFiltro := FAgente.GetFiltro( i );
              if ( oFiltro.DeDiccionario ) then
              begin
                  sFiltro := K_ABREP + oFiltro.Formula + K_CIERRAP;
                  if ( not lHuboWhere ) then
                  begin
                       FAgente.SQL.Add( K_WHERE );
                       lHuboWhere := TRUE;
                  end
                  else
                      sFiltro := K_AND + sFiltro;

                  FAgente.SQL.Add( sFiltro );
              end
              else
                  FAgente.FiltroCliente := TRUE;
         end;
     end;
end;

procedure TSuperSQL.ConstruyeOrderBy;
var
   i, iNumOrdenes : Integer;
   oOrden : TSQLOrden;
   sOrden : String;
begin
     if not FAgente.OrdenCliente then
     begin
       // Se requieren de dos pasadas para eliminar duplicados.
       // El ODBC Express marca error con ORDER BY CB_CODIGO, CB_CODIGO
       FListaWhere.Clear;
       iNumOrdenes := FAgente.NumOrdenes;
       if ( iNumOrdenes > 0 ) then
       begin
           Dec( iNumOrdenes );
           for i := 0 to iNumOrdenes do
           begin
                oOrden := FAgente.GetOrden( i );
                sOrden := oOrden.Formula;
                if ( not oOrden.Ascendente ) then
                   sOrden := sOrden + K_DESCENDENTE;
                // Para detectar duplicados
                AgregaConjunto( FListaWhere, sOrden );
           end;
       end;

       iNumOrdenes := FListaWhere.Count;
       if ( iNumOrdenes > 0 ) then
       begin
           Dec( iNumOrdenes );
           FAgente.SQL.Add( K_ORDER );
           for i := 0 to iNumOrdenes do
           begin
                sOrden := FListaWhere.Strings[ i ];
                if ( i < iNumOrdenes ) then
                   sOrden := sOrden + K_COMA;
                FAgente.SQL.Add( sOrden );
           end;
       end;

     end;
end;

procedure TSuperSQL.ConstruyeGroupBy;
var
   i, iNumGrupos : Integer;
   oGrupo : TSQLGrupo;
   sGrupo : String;
begin
     iNumGrupos := FAgente.NumGrupos;
     if ( iNumGrupos > 0 ) then
     begin
         Dec( iNumGrupos );
         FAgente.SQL.Add( K_GROUP );
         for i := 0 to iNumGrupos do
         begin
              oGrupo := FAgente.GetGrupo( i );
              sGrupo := oGrupo.Formula;
              if ( i < iNumGrupos ) then
                 sGrupo := sGrupo + K_COMA;
              FAgente.SQL.Add( sGrupo );
         end;
     end;
end;


procedure TSuperSQL.ConstruyeFrom;
var
   i : Integer;
begin
     FAgente.SQL.Add( K_FROM );
     FAgente.SQL.Add( dmEntidadesTress.NombreEntidad( FAgente.Entidad ));
     with FListaJoins do
       for i := 0 to Count-1 do
           FAgente.SQL.Add( Strings[ i ] );
end;

function TSuperSQL.Construye(Agente: TSQLAgente; oCalcNomina : TObject ): Boolean;
begin
     FAgente := Agente;
     FEvaluador.Entidad := FAgente.Entidad;
     FEvaluador.CalcNomina := oCalcNomina;
     try
        LimpiaListas;
        ValidaRequeridos;
        if ( not HayErrores ) then
               ConstruyeQuery;
        Result := not HayErrores;
     finally
            FEvaluador.CalcNomina := NIL;
     end;
end;

function TSuperSQL.EvaluaParams( Agente: TSQLAgente): Boolean;
begin
     FAgente := Agente;
     FEvaluador.Entidad := enNinguno;
     LimpiaListas;
     EvaluaColumnas;
     Result := not HayErrores;
end;


procedure TSuperSQL.ConstruyeQuery;
begin
     ConstruyeSelect;
     ConstruyeFrom;
     ConstruyeWhere;
     ConstruyeGroupBy;
     ConstruyeOrderBy;
end;

function TSuperSQL.ErrorGeneral(const sError: String) : Integer;
begin
     // Temporal
     Result := FAgente.ListaErrores.Add( sError );
end;


function TSuperSQL.HayErrores: Boolean;
begin
     Result := ( FAgente.ListaErrores.Count > 0 );
end;

procedure TSuperSQL.IncluyeRequeridos( var sFormula : String );
var
   i, iPos : Integer;
   sRequerido, sColumna : String;
begin
     with FEvaluador.ListaRequeridos do
       for i := 0 to Count-1 do
       begin
            sRequerido := Strings[ i ];
            if EsEquivalente( sRequerido ) then
            begin
                 sColumna := AgregaColumna( LimpiaFormula( sRequerido ), '', TipoEntidad( Objects[ i ] ));
                 sFormula := StrTransVar( sFormula, ZEvaluador.Tokeniza( i ), sColumna );
            end
            else
            begin
                // Si viene con espacio, es que tiene un ALIAS. Ej. ACUMULADOS de ISPT
                // y en ese caso se agrega a la lista de requeridos
                // de lo contrario es un requerido normal, en formato "TABLA.CAMPO"
                iPos := Pos( ' ', sRequerido );
                if ( iPos > 0 ) then
                    AgregaConjuntoObjeto( FListaRequeridos, sRequerido, Objects[i] )
                else
                begin
                    // Esto permite detectar si ya hubo una Columna anterior id�ntica,
                    // que NO viene de "Requeridos" sino de un AgregaColumna del programador,
                    // y en su caso, evitar duplicarla. ODBCExpress marca error si
                    // hay 2 columnas con el mismo ALIAS
                    iPos := Pos( '.', sRequerido );
                    AgregaColumna( sRequerido, Copy( sRequerido, iPos+1, MAXINT ), TipoEntidad( Objects[i] ));
                end;
            end;
       end;
end;

procedure TSuperSQL.LimpiaListas;
begin
     FListaSelect.Clear;
     FListaAlias.Clear;
     FListaDuplicados.Clear;
     FListaRequeridos.Clear;
     FListaJoins.Clear;
     FListaWhere.Clear;
     FListaOrderBy.Clear;
     FListaGroupBy.Clear;
end;

procedure TSuperSQL.MarcaSubTotales;
var
    i : Integer;
    nPos : Integer;
    oColumna : TSQLColumna;
    nLugar : Integer;
begin
    // Por cada una de las columnas "x" que fueron referenciadas
    // en funciones SubTotal( "x" )
    nLugar := -1;
    with FAgente.ListaSubTotales do
        for i := 0 to Count-1 do
        begin
            nPos := StrToIntDef( Strings[ i ], 0 );
            // nPos = '0' se usa para indicar que hay 'NTOTAL()' o 'NSUBTOTAL()'
            if ( nPos > 0 ) then
            begin
                Inc( nLugar );
                // Hay un l�mite en posiciones del Arreglo de SubTotales
                if ( nLugar <= K_MAX_SUB_TOT ) then
                begin
                    // Adem�s, nPos est� recorrida por 1
                    // El usuario le llama '1' a la columna '0'
                    oColumna := FAgente.GetColumna( nPos-1 );
                    if ( oColumna <> NIL ) then
                        oColumna.LugarSubTot := nLugar;
                end
                else
                    ErrorGeneral( 'Se excedi� el l�mite de SubTotales' );
            end;
        end;
end;

procedure TSuperSQL.ValidaColumnas;

    procedure SetCriterio( oColumna : TSQLColumna );
    begin
         with oColumna do
           if ( Totalizacion = ocAutomatico ) then
               case TipoFormula of
                    tgNumero,tgFloat    : Totalizacion := ocSuma;
                    tgBooleano : Totalizacion := ocCuantos;
                   else
                       Totalizacion := ocNinguno;
               end;
    end;

var
   i, iNumColumnas : Integer;
   oColumna : TSQLColumna;
   sTransformada : String;
   eTipo : eTipoGlobal;
   //CV
   lParametro : Boolean;
   oEntidad : TipoEntidad;
   iTipo : integer;
   lConstante : Boolean;
begin
     iNumColumnas := FAgente.NumColumnas;
     Dec( iNumColumnas );
     for i := 0 to iNumColumnas do
     begin
          oColumna := FAgente.GetColumna( i );
          with oColumna do
          begin
            // Si es de diccionario, no tiene que evaluarse
            // Se agrega directamente a la lista de Select
            // La Formula Transformada es el alias de la columna
            //CV
            if DeDiccionario then
            begin
                 if oZetaProvider.VerConfidencial OR
                    ( oColumna.Formula = '1' ) OR {CV: Defecto 915: Si la formula es '1' no es necesario buscar si el campo es confidencial, de antemano sabemos q no lo es}
                    (FEvaluador.dmExisteCampo.ExisteCampo( oEntidad, Formula, sTransformada, iTipo, lParametro )) then
                    Formula := AgregaColumna( Formula, Alias, Entidad )
                 //PENDIENTE CV else UN Error????
            end
            else if EsFormulaSQL( Formula ) then
            begin
                // Las F�rmulas de SQL inician con '@'
                if ( TipoFormula = tgAutomatico ) then
                    TipoFormula := tgFloat;
                //CV se le agrega un alias
                Formula := AgregaColumna( PreparaFormulaSQL( LimpiaFormula( Formula )), Alias, enNinguno )
            end
            // Si no es de diccionario, se valida que sea correcta y se buscan
            // sus campos requeridos
            else if FEvaluador.GetRequiere( Formula, eTipo, sTransformada, lConstante ) then
            begin
                 // Si es correcta, se transforma y se agregan los requeridos
                 // a la lista de Select
                IncluyeRequeridos( sTransformada );
                Formula := sTransformada;
                TipoFormula := eTipo;
                Status := K_STATUS_OK;
                if ( lConstante ) AND FAgente.Optimizar then
                    PosCampo := -1;     // Indica que Es Constante

                // Si la f�rmula incluye una llamada a funci�n SubTotal()
                // entonces, marca la columna como SubTotal y
                // a nivel agente se marca como que hay por lo menos
                // 1 columna de SubTotal
                if ( FEvaluador.ListaSubTotal > '' ) then
                begin
                    UsaSubTotal := TRUE;
                    FAgente.AgregaSubTotales( FEvaluador.ListaSubTotal );
                end;
            end
            else
            begin
                 // Si no es correcta, se agrega el mensaje de error
                Formula := sTransformada;
                if ( Length( Alias ) > 0 ) then
                    sTransformada := Alias + CR_LF + sTransformada;
                Status  := ErrorGeneral( MsgError + CR_LF +sTransformada );
            end;
            SetCriterio(oColumna);
          end;
     end;
end;

procedure TSuperSQL.ValidaFiltros;
var
   i, iNumFiltros : Integer;
   oFiltro : TSQLFiltro;
   sTransformada, sSQL : String;
   eTipo : eTipoGlobal;
   lConstante : Boolean;
begin
     iNumFiltros := FAgente.NumFiltros;
     Dec( iNumFiltros );
     for i := 0 to iNumFiltros do
     begin
          oFiltro := FAgente.GetFiltro( i );
          with oFiltro do
            // Si es de diccionario, no tiene que evaluarse
            if DeDiccionario then
                AgregaWhere( Formula, Entidad )
            else if EsFormulaSQL( Formula ) then
            begin
                // Las F�rmulas de SQL inician con '@'
                Formula := PreparaFormulaSQL( LimpiaFormula( Formula ));
                AgregaWhere( Formula, enNinguno );
                DeDiccionario := TRUE;
            end
            // Si no es de diccionario, se valida que sea correcta y se buscan
            // sus campos requeridos
            else if FEvaluador.GetRequiere( Formula, eTipo, sTransformada, lConstante ) then
            begin
                 // Los FILTROS tienen que ser BOOLEANOS
                 // Si es correcta, se transforma y se agregan los requeridos
                 // a la lista de Select
                 if ( eTipo = tgBooleano ) then
                 begin
                      // Investiga si la f�rmula tiene su equivalente en SQL, para enviar al servidor
                      IncluyeRequeridos( sTransformada );
                      if ( FEvaluador.NoHayRequeridos ) and FEvaluador.SQLValido( Formula, sSQL ) then
                      begin
                           Formula := sSQL;
                           DeDiccionario := TRUE;
                      end
                      else
                          Formula := sTransformada;

                      // Si es SQL Valido, regresar Formula VACIA
                      // de lo contrario, regresar la Transformada
                      AgregaWhere( Formula, enNinguno );
                 end
                 else
                 begin
                      Formula := 'Filtro debe ser tipo LOGICO';
                      Status  := ErrorGeneral( Formula );
                 end;
            end
            else
            begin
                 // Si no es correcta, se agrega el mensaje de error
                Formula := sTransformada;
                sTransformada := 'Error en Filtro' + CR_LF + sTransformada;
                Status  := ErrorGeneral( sTransformada );
            end;
     end;
end;

procedure TSuperSQL.ValidaOrdenes;
var
   i, iNumOrdenes : Integer;
   oOrden : TSQLOrden;
   oEntidad: TipoEntidad;
begin
     if not FAgente.OrdenCliente then
     begin
       iNumOrdenes := FAgente.NumOrdenes;
       Dec( iNumOrdenes );
       for i := 0 to iNumOrdenes do
       begin
            oOrden := FAgente.GetOrden( i );
            with oOrden do
            begin
              // Si no es de diccionario, determina la entidad
              if Entidad = enNinguno then
                  oEntidad := dmEntidadesTress.GetTipoEntidad( Copy( Formula, 1, Pos( '.', Formula )-1) )
              else oEntidad := Entidad;
              AgregaConjuntoObjeto( FListaOrderBy, Formula, TObject( oEntidad ));
            end;
       end;
     end;
end;

procedure TSuperSQL.ValidaGrupos;
var
   i, iNumGrupos : Integer;
   oGrupo : TSQLGrupo;
   oEntidad: TipoEntidad;
begin
     iNumGrupos := FAgente.NumGrupos;
     Dec( iNumGrupos );
     for i := 0 to iNumGrupos do
     begin
          oGrupo := FAgente.GetGrupo( i );
          with oGrupo do
          begin
            // Si no es de diccionario, determina la entidad
            if Entidad = enNinguno then
                oEntidad := dmEntidadesTress.GetTipoEntidad( Copy( Formula, 1, Pos( '.', Formula )-1) )
            else oEntidad := Entidad;
            AgregaConjuntoObjeto( FListaGroupBy, Formula, TObject( oEntidad ));
          end;
     end;
end;


procedure TSuperSQL.ValidaOrdenesCliente;
var
   i, iNumOrdenes : Integer;
   oOrden : TSQLOrden;
begin
     FAgente.OrdenCliente := FALSE;
     iNumOrdenes := FAgente.NumOrdenes;
     Dec( iNumOrdenes );
     // Investiga si por lo menos hay UNA FORMULA de Orden
     for i := 0 to iNumOrdenes do
     begin
          oOrden := FAgente.GetOrden( i );
          if not oOrden.DeDiccionario then
          begin
            FAgente.OrdenCliente := TRUE;
            break;
          end;
     end;
     // Si por lo menos hay UNA FORMULA, Convierte
     // los ORDER BY a SELECT para luego ordenar el ClientDataSet
     if FAgente.OrdenCliente then
     begin
         for i := 0 to iNumOrdenes do
         begin
            oOrden := FAgente.GetOrden( i );
            with oOrden do
                // DeDiccionario? tgAutomatico?
                // PosColumna := FAgente.AgregaColumna( Formula, FALSE, Entidad, tgAutomatico, 0 );
                PosColumna := FAgente.AgregaColumnaOrden( Formula, Entidad );
         end;
     end;
end;

procedure TSuperSQL.ValidaRequeridos;
begin
     ValidaOrdenesCliente;
     ValidaColumnas;
     MarcaSubTotales;
     ValidaFiltros;
     ValidaOrdenes;
     ValidaGrupos;
     ValidaTablas;
end;

procedure TSuperSQL.EvaluaColumnas;
var
   i, iNumColumnas : Integer;
   oColumna : TSQLColumna;
   oTipo : eTipoGlobal;
   sCalculada : String;
begin
     iNumColumnas := FAgente.NumColumnas;
     Dec( iNumColumnas );
     for i := 0 to iNumColumnas do
     begin
          oColumna := FAgente.GetColumna( i );
          with oColumna do
          begin
            if FEvaluador.CalculaString( Formula, oTipo, sCalculada ) then
            begin
                Formula     := sCalculada;
                TipoFormula := oTipo;
            end
            else
            begin
                if Length( Alias ) > 0 then
                    sCalculada := Alias + CR_LF + sCalculada;
                Status := ErrorGeneral( sCalculada );
            end;
          end;
     end;
end;

procedure TSuperSQL.ValidaTablas;
var
   {$ifdef MULTIPLES_ENTIDADES}
   TablasConJoin : Array of TipoEntidad;
   {$else}
   TablasConJoin : set of TipoEntidad;
   {$endif}
   FListaBusca : TStringList;

    procedure BuscaJoins( oLista : TStringList; const sLista : String );
    var
       i, j : Integer;
       EntidadColumna : TipoEntidad;
    begin
       with oLista do
         for i := 0 to Count-1 do
         begin
              EntidadColumna := TipoEntidad( Objects[ i ] );

              {$ifdef MULTIPLES_ENTIDADES}
              if not Dentro( EntidadColumna, TablasConJoin ) then
              {$else}
              if not ( EntidadColumna in TablasConJoin ) then
              {$endif}
              begin
                   FListaBusca.Clear;
                   if dmEntidadesTress.BuscaJoins( FAgente.Entidad, EntidadColumna, FListaBusca ) then
                   begin
                       for j := 0 to FListaBusca.Count-1 do
                       begin
                            // Agrega estatuto JOIN a las lista de Joins, sin duplicados
                            AgregaConjunto( FListaJoins, FListaBusca.Strings[ j ] );
                            // Incluye en el conjunto para evitar b�squedas futuras
                            {$ifdef MULTIPLES_ENTIDADES}
                            //TablasConJoin
                            SetLength( TablasConJoin, Length(TablasConJoin) + 1 );
                            TablasConJoin[High(TablasConJoin)] :=  TipoEntidad( FListaBusca.Objects[ j ] );
                            {$else}
                            TablasConJoin := TablasConJoin + [ TipoEntidad( FListaBusca.Objects[ j ] ) ];
                            {$endif}
                       end;
                   end
                   else
                       ErrorGeneral( sLista + ': No se puede relacionar el campo ' + Strings[ i ] );
              end;
         end;
    end;

begin  // ValidaTablas
     // Llevar el conjunto de las tablas con las que ya se hizo el Join
     // Si son campos de la tabla principal, no tiene caso que BusqueJoins
     // Si son comandos de SQL teclados por el usuario, llevan enNinguno y no hacen Join
     {$ifdef MULTIPLES_ENTIDADES}
     SetLength(TablasConJoin, 2);
     TablasConJoin[0] := FAgente.Entidad;
     TablasConJoin[1] := enNinguno;
     {$else}
     TablasConJoin := [FAgente.Entidad, enNinguno];
     {$endif}

     FListaBusca := TStringList.Create;
     // Joins generados por Columnas, Requeridos y Filtros
     BuscaJoins( FListaSelect, 'Columnas' );
     BuscaJoins( FListaWhere, 'Filtros' );
     BuscaJoins( FListaOrderBy, 'Orden/Grupos' );
     BuscaJoins( FListaGroupBy, 'Agrupado' );
     BuscaJoins( FListaRequeridos, 'Requeridos' );
     BuscaJoins( FListaDuplicados, 'Duplicados' );
     //
     FListaBusca.Free;
end;

function TSuperSQL.AgregaColumna(const sColumna, sAlias : String; const eTipo : TipoEntidad): String;
var
   iPos : Integer;
   lExiste : Boolean;
begin
     iPos := FListaSelect.IndexOf( sColumna );
     if ( iPos < 0 ) then       // No hay uno igual
        lExiste := FALSE
     else if ( sAlias = '' ) then   // Encontr� uno, alias no importa
        lExiste := TRUE
     else if ( FListaAlias.Strings[ iPos ] = sAlias ) then  // El que encontr�, tiene mismo alias
        lExiste := TRUE
     else
        lExiste := FALSE;

     if ( not lExiste ) then // Busca otro que tenga el mismo ALIAS aunque f�rmula distinta
     begin                   // Ej. COLABORA.CB_CODIGO as CB_CODIGO, NOMINA.CB_CODIGO as CB_CODIGO
                             // Dado que es Join debe ser el mismo.
                             // Se llaman por FieldByName( 'CB_CODIGO' )
        iPos := FListaAlias.IndexOf( sAlias );
        lExiste := ( iPos >= 0 );
        // Se agrega a una lista de Duplicados para Asegurarnos que
        // se haga el Join con la entidad.
        // Ej. Por si tenemos NOMINA.CB_CODIGO y COLABORA.CB_CODIGO, asegura join a NOMINA y COLABORA.
        if ( lExiste ) then
            AgregaConjuntoObjeto( FListaDuplicados, sColumna, TObject( eTipo ));
     end;
{
     else                           // Ver si hay otro igual, pero con el mismo alias
     begin                          // Ej. CB_CODIGO COLUMNA0 y CB_CODIGO CB_CODIGO
        lExiste := FALSE;
        Inc( iPos );
        with FListaSelect do
            while ( iPos < Count ) do
            begin
                lExiste := ( FListaSelect.Strings[ iPos ] = sColumna ) and
                           ( FListaAlias.Strings[ iPos ] = sAlias );
                if ( lExiste ) then
                    break
                else
                    Inc( iPos );
            end;
     end;
}

     // Si ya existe, Regresa el alias con el que se va a identificar la columna
     if ( lExiste ) then
         Result := FListaAlias[ iPos ]
     else
     begin
          // Si no existe, la agrega a la lista de requeridos.
          // Se puede dar el caso de 'CB_ZONA_GE' Y 'ZCB_ZONA_GE'
          // Si el alias es VACIO, no importa el alias Anterior. Evita repetir campos
          FListaSelect.AddObject( sColumna, TObject( eTipo ));
          iPos := FListaSelect.Count - 1;
          if Length( sAlias ) = 0 then
             Result := K_COLUMNA_SQL + IntToStr( iPos )
          else
              Result := sAlias;
          FListaAlias.Add( Result );
     end;
end;

procedure TSuperSQL.AgregaWhere(const sExpresion: String; const eTipo : TipoEntidad);
begin
     AgregaConjuntoObjeto( FListaWhere, sExpresion, TObject( eTipo ));
end;

{
function TSuperSQL.GetEntidad( oLista : TStringList; const iPos : Integer ) : TipoEntidad;
var
   sCampo : String;
   iPunto : Integer;
begin
     sCampo := oLista.Strings[ iPos ];
     if ZEvaluador.TieneAlias( sCampo ) then
         // Los Par�metros de Nomina, que llevan alias, tienen la entidad en el Objects
         // No quise poner la constante 'enNomina' para permitir flexibilidad a futuro
         Result := TipoEntidad( oLista.Objects[ iPos ] )
     else
     begin
         iPunto := Pos( K_PUNTO, sCampo );
         if ( iPunto = 0 ) then
            Result := FAgente.Entidad
         else
             Result := dmEntidadesTress.GetTipoEntidad( Copy( sCampo, 1, iPunto-1 ));
     end;
end;
}

end.
