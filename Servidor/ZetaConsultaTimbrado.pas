unit ZetaConsultaTimbrado;

interface

uses
    SysUtils,
    Variants,
    DZetaServerProvider,
    ZetaCommonClasses,
    Contnrs,
    Classes;
type
  {*** US 13038: Notificacion Timbrado  ***}
  TDatosListadoEmpresas = class( TObject )
     public
     { Public declarations }
     CM_CODIGO  : String;
     CM_NOMBRE  : String;
     CM_PASSWRD : String;
     CM_USRNAME : String;
     CM_DATOS   : String;
     constructor Create;
     destructor Destroy; override;
  end;

  TDatosListadoContactos = class( TObject )
     public
     { Public declarations }
     US_CODIGO  : String;
     US_NOMBRE  : String;
     US_EMAIL   : String;
     constructor Create;
     destructor Destroy; override;
  end;

  TConsultaTimbrado = class(TObject)
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    protected
    { Protected declarations }
    public
    { Public declarations }
    constructor Create( Provider: TdmZetaServerProvider );
    destructor Destroy; override;
    function GetNotificacionTimbradoListaContacto( CM_CODIGO: String ): TObjectList;
    function GetNotificacionTimbradoListaEmpresas: TObjectList;
    function ObtenerEmpresa( CM_CODIGO: String ): Variant;
    function GetNominasPendientes( CM_CODIGO: String; oProvider: TdmZetaServerProvider; var sMensajeError: String ): String;
    function EmpleadosNotificacionesTimbradoPendiente( CM_CODIGO: String; var sMensajeError: String ): String;
end;
implementation

uses
    ZetaCommonTools;


{ TConsultaTimbrado }

constructor TConsultaTimbrado.Create(Provider: TdmZetaServerProvider);
begin
     oZetaProvider := Provider;
end;

destructor TConsultaTimbrado.Destroy;
begin
     inherited Destroy;
end;

function TConsultaTimbrado.GetNotificacionTimbradoListaContacto(CM_CODIGO: String): TObjectList;
const
     K_LISTADO_CONTACTOS = 'select US_CODIGO, US_NOMBRE, US_EMAIL from FN_NotificacionTimbrado_GetListaContacto(:CM_CODIGO)';
var
   oEmpresaActiva: OleVariant;
   FListadoContactosSQL: TZetaCursor;
   DatosListadoContactos : TDatosListadoContactos;
   ListaContactos: TObjectList;
begin
     ListaContactos := TObjectList.Create(true);
     with oZetaProvider do
     begin
          oEmpresaActiva := EmpresaActiva;
          EmpresaActiva := Comparte;
          try
             FListadoContactosSQL := CreateQuery( K_LISTADO_CONTACTOS );
             with FListadoContactosSQL do
             begin
                  ParamAsString( FListadoContactosSQL, 'CM_CODIGO', CM_CODIGO );
                  Active := True;
                  while not Eof do
                  begin
                       DatosListadoContactos := TDatosListadoContactos.Create;
                       DatosListadoContactos.US_CODIGO := FListadoContactosSQL.FieldByName('US_CODIGO').AsString;
                       DatosListadoContactos.US_NOMBRE := FListadoContactosSQL.FieldByName('US_NOMBRE').AsString;
                       DatosListadoContactos.US_EMAIL := FListadoContactosSQL.FieldByName('US_EMAIL').AsString;
                       ListaContactos.Add(DatosListadoContactos);
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FListadoContactosSQL );
          end;
     end;
     Result := ListaContactos;
end;

function TConsultaTimbrado.GetNotificacionTimbradoListaEmpresas: TObjectList;
const
     K_LISTADO_EMPRESA = 'select CM_CODIGO, CM_NOMBRE, CM_PASSWRD, CM_USRNAME, CM_DATOS from FN_NotificacionTimbrado_GetListaEmpresas()';
var
   oEmpresaActiva: OleVariant;
   FListadoEmpresasSQL: TZetaCursor;
   DatosListadoEmpresas : TDatosListadoEmpresas;
   ListaEmpresas: TObjectList;
begin
     ListaEmpresas := TObjectList.Create(true);
     with oZetaProvider do
     begin
          oEmpresaActiva := EmpresaActiva;
          EmpresaActiva := Comparte;
          try
             FListadoEmpresasSQL := CreateQuery( K_LISTADO_EMPRESA );
             with FListadoEmpresasSQL do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       DatosListadoEmpresas := TDatosListadoEmpresas.Create;
                       DatosListadoEmpresas.CM_CODIGO := FListadoEmpresasSQL.FieldByName('CM_CODIGO').AsString;
                       DatosListadoEmpresas.CM_NOMBRE := FListadoEmpresasSQL.FieldByName('CM_NOMBRE').AsString;
                       DatosListadoEmpresas.CM_PASSWRD := FListadoEmpresasSQL.FieldByName('CM_PASSWRD').AsString;
                       DatosListadoEmpresas.CM_USRNAME := FListadoEmpresasSQL.FieldByName('CM_USRNAME').AsString;
                       DatosListadoEmpresas.CM_DATOS  := FListadoEmpresasSQL.FieldByName('CM_DATOS').AsString;
                       ListaEmpresas.Add(DatosListadoEmpresas);
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FListadoEmpresasSQL );
          end;
     end;
     Result := ListaEmpresas;
end;

{ *** Obtener las nominas pendientes a timbrar ***}

function BuildEmpresaDBInfo(DataSet: TZetaCursor): OleVariant;
     { Es una copia de DBaseCliente.BuildEmpresa }
     {
     Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

     P_ALIAS = 0;
     P_DATABASE = 0;
     P_USER_NAME = 1;
     P_PASSWORD = 2;
     P_USUARIO = 3;
     P_NIVEL_0 = 4;
     P_CODIGO = 5;
     }
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  0,
                                  VACIO,{FieldByName( 'CM_NIVEL0' ).AsString,}
                                  FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;

function TConsultaTimbrado.ObtenerEmpresa( CM_CODIGO: String ): Variant;
const
     K_DBS_EMPRESA = 'select CM_CODIGO, CM_NOMBRE, CM_PASSWRD, CM_USRNAME, CM_DATOS from COMPANY where CM_CODIGO=:CM_CODIGO';
var
  oEmpresaActiva : Variant;
  oEmpresaNotificacion: OleVariant;
  FDataSet : TZetaCursor;
  iEmpresas: Integer;
begin
     oEmpresaActiva :=  Self.oZetaProvider.EmpresaActiva;
     with  Self.oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery( K_DBS_EMPRESA );
          ParamAsString( FDataset, 'CM_CODIGO', CM_CODIGO );
          with FDataSet do
          begin
               Active := True;
               iEmpresas := FDataset.RecordCount;
               Active := False;
          end;
          if ( iEmpresas = 0 ) then
             ZetaCommonTools.SetOLEVariantToNull( oEmpresaNotificacion )
          else
          begin
               oEmpresaNotificacion := VarArrayCreate( [ 1, iEmpresas, 1, 2 ], varVariant );
               VarArrayLock( oEmpresaNotificacion );
               try
                  iEmpresas := 1;
                  try
                     with FDataSet do
                     begin
                          Active := True;
                          oEmpresaNotificacion[ iEmpresas, 1 ] := FieldByName( 'CM_NOMBRE' ).AsString;
                          oEmpresaNotificacion[ iEmpresas, 2 ] := BuildEmpresaDBInfo( FDataset );
                          Active := False;
                     end;
                  finally
                         FreeAndNil( FDataSet );
                  end;
               finally
                      VarArrayUnlock( oEmpresaNotificacion );
               end;
          end;
          EmpresaActiva :=  oEmpresaActiva;
     end;
     Result := oEmpresaNotificacion;
end;


function TConsultaTimbrado.GetNominasPendientes( CM_CODIGO: String; oProvider: TdmZetaServerProvider; var sMensajeError: String ): String;
const
     K_EMPLEADOS_NOMINA_PENDIENTES = 'select * from FN_NotificacionTimbrado_GetNominasPendientes( :CM_CODIGO, :FECHASISTEMA )';
     K_STYLE_TABLE = 'style ="border: 1px dotted;border-collapse:collapse;border-color:#D0CECE;margin: 0 auto;font-family: Trebuchet MS;font-size: 12pt"';
     K_STYLE_TD    = 'style="border: 1px dotted;border-color:#D0CECE;padding:0 2px;padding:0 5.4pt;"';
     K_STYLE_TH    = 'style="border: 1px dotted;border-color:#D0CECE;padding:0 2px;padding:0 5.4pt;"';
var
   oNominasPendientes: TZetaCursor;
   sTablaEncabezado: String;
   sTablaBody: String;
   i, iFinal, iFinalAux: Integer;
   sTabla: String;

   function TablaHeader( sNombreColuma: String; i: Integer ): String;
   begin
        Result := '<th ' + K_STYLE_TH + '><strong>' + sNombreColuma + '</strong></th>';
   end;

   function TablaBody( sValor: String; i, iFinal, iFinalAux: Integer ): String;
   begin
        Result := '<td ' + K_STYLE_TD + '>' + sValor + '</td>';
   end;

begin
     oNominasPendientes := nil;
     sTablaEncabezado := VACIO;
     sTablaBody := VACIO;
     sTabla := VACIO;
     iFinalAux := 0;
     try
        try
        with oProvider do
        begin
             oNominasPendientes := CreateQuery( K_EMPLEADOS_NOMINA_PENDIENTES );
             ParamAsString( oNominasPendientes, 'CM_CODIGO', CM_CODIGO );
             ParamAsDate( oNominasPendientes, 'FECHASISTEMA', Trunc( Now ) );
             with oNominasPendientes do
             begin
                  Active := True;
                  iFinal := oNominasPendientes.RecordCount - 1;
                  if not Eof then
                  begin
                       if Fields.Count > 0 then
                       begin
                            sTabla := '<table ' + K_STYLE_TABLE + ' >' + CR_LF + '<tr>';
                            sTablaEncabezado := CR_LF;
                            for i:=0 to Fields.Count -1 do
                            begin
                                 sTablaEncabezado := sTablaEncabezado + TablaHeader( Fields[i].FieldName, i );
                            end;
                            sTablaEncabezado := sTablaEncabezado + CR_LF;
                            sTabla := sTabla + sTablaEncabezado + CR_LF + '</tr>' + CR_LF;
                       end;
                  end;
                  if not StrVacio( sTabla ) then
                     sTabla := sTabla + CR_LF;
                  while not Eof do
                  begin
                       if Fields.Count > 0 then
                       begin
                            sTablaBody := sTablaBody + CR_LF + '<tr>' + CR_LF;
                            for i:=0 to Fields.Count -1 do
                            begin
                                 sTablaBody := sTablaBody + TablaBody( Fields[i].AsString, i, iFinal, iFinalAux );
                            end;
                            sTablaBody := sTablaBody + CR_LF + '</tr>';
                       end;
                       Inc( iFinalAux );
                       Next;
                  end;
                  if not StrVacio( sTabla ) then
                     sTabla := sTabla + sTablaBody + CR_LF + '</table>';
                  Active := False;
             end;
             Result := sTabla;
        end;
        finally
               FreeAndNil( oNominasPendientes );
        end;
     except
           on Error: Exception do
           begin
                sMensajeError := 'Error al obtener nominas pendientes: ' + CM_CODIGO + ' ' + Error.Message;
                Result := VACIO;
           end;
     end;
end;

function TConsultaTimbrado.EmpleadosNotificacionesTimbradoPendiente( CM_CODIGO: String; var sMensajeError: String ): String;
var
   oEmpresa: OleVariant;
   oProvider: TdmZetaServerProvider;
   oLista: OleVariant;
begin
     Result := VACIO;
     try
          { Se llena el OleVariant oLista }
          try
             oLista := ObtenerEmpresa( CM_CODIGO );
          except
               on Error: Exception do
               begin
                    sMensajeError := 'Error empresa: ' + CM_CODIGO + ' ' + Error.Message;
                    ZetaCommonTools.SetOLEVariantToNull( oLista );
                    Result := VACIO;
               end;
          end;
          if not VarIsNull( oLista ) then
          begin
               oEmpresa := oLista[ 1, 2 ];
               oProvider := TdmZetaServerProvider.Create( nil );
               try
                    try
                       oProvider.EmpresaActiva := oEmpresa;
                       Result := GetNominasPendientes( CM_CODIGO, oProvider, sMensajeError );
                    finally
                    begin
                         FreeAndNil( oProvider );
                    end;
                    end;
               except                                                           //Si falla el EmpresaActiva escribe error en log pero contin�a
                    on Error: Exception do                                      //Escribe error en log
                    begin
                         sMensajeError := 'Error al obtener nominas pendientes: ' + CM_CODIGO + ' ' + Error.Message;
                         Result := VACIO;
                    end;
               end;
          end;
     except
           on Error: Exception do
           begin
                FreeAndNil( oProvider );
                sMensajeError := 'Error al obtener nominas pendientes: ' + CM_CODIGO + ' ' + Error.Message;
                Result := VACIO;
           end;
     end;
end;

constructor TDatosListadoEmpresas.Create;
begin
     inherited Create;
     CM_CODIGO  := VACIO;
     CM_NOMBRE  := VACIO;
     CM_PASSWRD := VACIO;
     CM_USRNAME := VACIO;
     CM_DATOS   := VACIO;
end;

destructor TDatosListadoEmpresas.Destroy;
begin
      inherited Destroy;
end;

constructor TDatosListadoContactos.Create;
begin
     inherited Create;
     US_CODIGO := VACIO;
     US_NOMBRE := VACIO;
     US_EMAIL  := VACIO;
end;

destructor TDatosListadoContactos.Destroy;
begin
      inherited Destroy;
end;

end.
