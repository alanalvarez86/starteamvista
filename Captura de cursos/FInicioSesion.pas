unit FInicioSesion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Buttons, StdCtrls, ExtCtrls,StrUtils,
     ZBaseDlgModal, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, dxSkinsDefaultPainters,
     dxSkinWhiteprint, cxButtons, ImgList, DB, DBClient, ZetaClientDataSet,
  ZetaKeyLookup, Vcl.ComCtrls, Vcl.Mask, ZetaNumero, dxBarBuiltInMenu,
  cxControls, cxPC, ZetaKeyLookup_DevEx, ZetaKeyCombo, ZetaDBTextBox;

type
  TInicioSesion = class(TZetaDlgModal_DevEx)
    PanelRelleno: TPanel;
    pcConfiguracion: TcxPageControl;
    imgListTipoDispositivo: TcxImageList;
    gbHabilitar: TGroupBox;
    GroupBox4: TGroupBox;
    lblCurso: TLabel;
    lblMAestro: TLabel;
    lblHoras: TLabel;
    lblMaestroIndividual: TLabel;
    zCursos: TZetaKeyLookup_DevEx;
    rbIndividual: TRadioButton;
    edtRevision: TEdit;
    HorasCurso: TZetaNumero;
    zMaestro: TZetaKeyLookup_DevEx;
    GroupBox6: TGroupBox;
    lblCursos: TLabel;
    lblRevisionMultiple: TLabel;
    lblHorasMultiple: TLabel;
    lblMaestroMultiple: TLabel;
    btnMultiplesCursos: TcxButton;
    rbMultiple: TRadioButton;
    edtRevisionMultiple: TEdit;
    HorasCursoMultiple: TZetaNumero;
    zMaestroMultiple: TZetaKeyLookup_DevEx;
    rbCursos: TRadioButton;
    GroupBox1: TGroupBox;
    lblGrupos: TLabel;
    rbGrupos: TRadioButton;
    zGrupo: TZetaKeyLookup_DevEx;
    GroupBox2: TGroupBox;
    rbAsistencia: TRadioButton;
    rbAprobados: TRadioButton;
    rbInscritos: TRadioButton;
    zCursoMultiple: TZetaKeyLookup_DevEx;
    cxBotonAgregar: TcxButton;
    cxButtonBorrar: TcxButton;
    cbDatosGlobales: TCheckBox;
    lbCursos: TListBox;
    lblListaCurso: TLabel;
    procedure OK_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rbIndividualClick(Sender: TObject);
    procedure rbMultipleClick(Sender: TObject);
    procedure zCursosValidKey(Sender: TObject);
    procedure rbGruposClick(Sender: TObject);
    procedure rbCursosClick(Sender: TObject);
    procedure cxBotonAgregarClick(Sender: TObject);
    procedure cxButtonBorrarClick(Sender: TObject);
    procedure zCursoMultipleValidKey(Sender: TObject);
    procedure cbDatosGlobalesClick(Sender: TObject);
  private
    { Private declarations }
    procedure getDatos;
    procedure setDatos;
    procedure ActivaIndividual;
    procedure ActivaMultiple;
    procedure ActivaGrupos;
    procedure ActivaCursos;
    function formaCadenaCursoMultiple: string;
    procedure setStrToInit;
    procedure cambiaDatosGlobales;
    procedure separarCursosMultiples;
  public
    { Public declarations }

  end;

var
  InicioSesion: TInicioSesion;
  iCursoIndex: Integer;
  lCursosMultiple: tstringlist;
  sCursosMultiple, sMaestroMultiple ,sRevisionMultiple: string;
  rHorasMultiple: currency;

implementation

uses IniFiles,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerTools,
     DConsultas,
     DCliente,
     DCatalogos,
     DReportes,
     DRecursos,
     DInterfase,
     Variants,
     ZetaDialogo,
     ZetaBusqueda_DevEx;

{$R *.DFM}

{ TConfiguraBDSAP }


procedure TInicioSesion.cbDatosGlobalesClick(Sender: TObject);
begin
  inherited;
  cambiaDatosGlobales;
end;

procedure TInicioSesion.cambiaDatosGlobales;
begin
  inherited;
  zMaestroMultiple.Enabled := cbDatosGlobales.Checked;
  HorasCursoMultiple.Enabled := cbDatosGlobales.Checked;
  edtRevisionMultiple.Enabled := cbDatosGlobales.Checked;
  zCursoMultiple.Llave := VACIO;
  edtRevisionMultiple.Text := VACIO;
  zMaestroMultiple.Llave := VACIO;
  HorasCursoMultiple.Valor := 0;
  //lbCursos.Clear;
  //lCursosMultiple.Clear;
end;

procedure TInicioSesion.cxBotonAgregarClick(Sender: TObject);
var
  sCursos: String;
  i: integer;
  bFlagRepetido: Boolean;
  strArray: TArray<System.string>;
  charArray: Array[0..0] of Char;
begin
  inherited;
  bFlagRepetido := FALSE;
  if not ( ZCursoMultiple.Llave = VACIO ) then
  begin
    sCursos := formaCadenaCursoMultiple;
    charArray[0] := '|';
    for i := 0 to lbCursos.Items.Count - 1 do
    begin
      strArray := lCursosMultiple[i].Split(charArray);
      if (strArray[0] = zCursoMultiple.Llave) then
      begin
        bFlagRepetido := TRUE;
        Break;
      end;
    end;
    if bFlagRepetido then
    begin
      ZetaDialogo.ZError( 'Captura de cursos', 'Este Curso ya fue agregado.', 0 );
    end
    else
    begin
      lbCursos.AddItem( zCursoMultiple.Llave + ' - ' + zCursoMultiple.Descripcion,nil);
      lCursosMultiple.Add(sCursos);
    end;
  end;
  sCursos := VACIO;
  zCursoMultiple.Llave := VACIO;
  if not cbDatosGlobales.Checked then
  begin
    edtRevisionMultiple.Text := VACIO;
    zMaestroMultiple.Llave := VACIO;
    HorasCursoMultiple.Valor := 0;
  end;
end;

procedure TInicioSesion.cxButtonBorrarClick(Sender: TObject);
begin
  inherited;
  if lbCursos.Items.Count <= 0 then
  begin
    ZetaDialogo.ZError( 'Captura de cursos', 'Agregue por lo menos un Curso', 0 );
  end
  else
  begin
    if lbCursos.ItemIndex < 0 then
    begin
      ZetaDialogo.ZError( 'Captura de cursos', 'Seleccione el elemento a borrar de la lista de Cursos de la sesi�n.', 0 );
    end
    else
    begin
      zCursoMultiple.Llave := VACIO;
      edtRevisionMultiple.Text := VACIO;
      HorasCursoMultiple.Valor := 0;
      zMaestroMultiple.Llave := VACIO;
      lCursosMultiple.Delete(lbCursos.ItemIndex);
      lbCursos.DeleteSelected;
    end;
  end;
end;

function TInicioSesion.formaCadenaCursoMultiple: string;
var
   sCadPipe:string;
begin
    sCadPipe := ConcatString( sCadPipe, zCursoMultiple.Llave, '|' );
    sCadPipe := ConcatString( sCadPipe, IfThen(sRevisionMultiple = VACIO, '-',sRevisionMultiple), '|');
    sCadPipe := ConcatString( sCadPipe, FloatToStr(rHorasMultiple), '|');
    sCadPipe := ConcatString( sCadPipe, IfThen(sMaestroMultiple = VACIO, '-',sMaestroMultiple), '|');
    Result := sCadPipe;
end;

procedure TInicioSesion.separarCursosMultiples;
var
    strArray1, strArray2 : TArray<System.string>;
    charArray1, charArray2  : Array[0..0] of Char;
    sDescrip : String;
    i : integer;
begin
    charArray1[0] := ',';
    charArray2[0] := '|';
    strArray1 := sCursosMultiple.Split(charArray1);
    for i := 0 to Length(strArray1) - 1 do
    begin
      lCursosMultiple.Add(strArray1[i]);
      strArray2 := strArray1[i].Split(charArray2);
      lbCursos.AddItem(strArray2[0] + ' - ' + dmInterfase.GetDescripcion(strArray2[0]) ,nil);
    end;
end;

procedure TInicioSesion.FormCreate(Sender: TObject);
var
   sFiltroCta:String;
begin
     inherited;
     dmCatalogos.cdsCursos.Conectar;
     dmCatalogos.cdsMaestros.Conectar;
     dmInterfase.cdsSesionesCursos.Conectar;
     with zCursos do
     begin
          LookupDataset := dmCatalogos.cdsCursos;
          Filtro := VACIO;
     end;
     with zCursoMultiple do
     begin
          LookupDataset := dmCatalogos.cdsCursos;
          Filtro := VACIO;
     end;
     with zMaestro do
     begin
          LookupDataset := dmCatalogos.cdsMaestros;
          Filtro := VACIO;
     end;
     with zMaestroMultiple do
     begin
          LookupDataset := dmCatalogos.cdsMaestros;
          Filtro := VACIO;
     end;
     with zGrupo do
     begin
          LookupDataset := dmInterfase.cdsSesionesCursos;
          Filtro := VACIO;
     end;
end;

procedure TInicioSesion.FormShow(Sender: TObject);
begin
     inherited;
     lCursosMultiple := TStringList.Create;
     dmCatalogos.cdsTiposPoliza.Refrescar;
     getDatos;
     cambiaDatosGlobales;
     if not (sCursosMultiple = VACIO) then
        separarCursosMultiples;
end;

procedure TInicioSesion.OK_DevExClick(Sender: TObject);
var
fCampoVacio: Boolean;
begin
     inherited;
     fCampoVacio := FALSE;
     if rbIndividual.Checked then
     begin
        sCursosMultiple := VACIO;
        if not strLleno(zCursos.Llave) then
        begin
            fCampoVacio := TRUE;
            ZetaDialogo.ZError( 'Captura de cursos', 'El Campo de Curso no debe quedar vac�o.', 0 );
        end;
     end;
     if rbMultiple.Checked then
     begin
        if lbCursos.Items.Count <= 0 then
        begin
            fCampoVacio := TRUE;
            ZetaDialogo.ZError( 'Captura de cursos', 'Se debe de agregar por lo menos un Curso.', 0 );
        end
        else
        begin
            setStrToInit;
        end;
     end;
     if rbGrupos.Checked then
     begin
        sCursosMultiple := VACIO;
        if not strLleno(zGrupo.Llave) then
        begin
            fCampoVacio := TRUE;
            ZetaDialogo.ZError( 'Captura de cursos', 'El Campo de Grupo no debe quedar vac�o.', 0 );
        end;
     end;
     if (not fCampoVacio) then
     begin
        ModalResult := mrOk;
        setDatos;
     end;
end;

procedure TInicioSesion.rbIndividualClick(Sender: TObject);
begin
     inherited;
     ActivaIndividual;
end;

procedure TInicioSesion.rbMultipleClick(Sender: TObject);
begin
     inherited;
     ActivaMultiple;
end;

procedure TInicioSesion.rbGruposClick(Sender: TObject);
begin
  inherited;
  ActivaGrupos;
end;

procedure TInicioSesion.rbCursosClick(Sender: TObject);
begin
  inherited;
  ActivaCursos;
end;

procedure TInicioSesion.ActivaCursos;
begin
  inherited;
  rbIndividual.Enabled := rbCursos.Checked;
  rbIndividual.Checked := rbCursos.Checked;
  rbMultiple.Enabled := rbCursos.Checked;
  ActivaIndividual;
end;

procedure TInicioSesion.ActivaGrupos;
begin
    if Self.rbGrupos.Checked then
    begin
      lblGrupos.Enabled := rbGrupos.Checked;
      rbInscritos.Enabled := rbGrupos.Checked;
      rbAprobados.Enabled := rbGrupos.Checked;
      rbAsistencia.Enabled := rbGrupos.Checked;
      if not rbAsistencia.Checked and not rbAprobados.Checked and not rbInscritos.Checked then
          rbInscritos.Checked := rbGrupos.Checked;
      zGrupo.Enabled := rbGrupos.Checked;
      rbCursos.Checked := Not rbGrupos.Checked;
      lblCursos.Enabled := Not rbGrupos.Checked;
      lblListaCurso.Enabled := Not rbGrupos.Checked;
      zCursoMultiple.Enabled := Not rbGrupos.Checked;
      lbCursos.Enabled := Not rbGrupos.Checked;
      cxBotonAgregar.Enabled := Not rbGrupos.Checked;
      cxButtonBorrar.Enabled := Not rbGrupos.Checked;
      cbDatosGlobales.Enabled := Not rbGrupos.Checked;
      edtRevisionMultiple.Enabled := Not rbGrupos.Checked;
      zMaestroMultiple.Enabled := Not rbGrupos.Checked;
      HorasCursoMultiple.Enabled := Not rbGrupos.Checked;
      rbMultiple.Checked := Not rbGrupos.Checked;
      rbMultiple.Enabled := Not rbGrupos.Checked;
      lblCurso.Enabled := Not rbGrupos.Checked;
      zCursos.Enabled := Not rbGrupos.Checked;
      lblMaestro.Enabled := Not rbGrupos.Checked;
      zMaestro.Enabled := Not rbGrupos.Checked;
      edtRevision.Enabled := Not rbGrupos.Checked;
      lblHoras.Enabled := Not rbGrupos.Checked;
      HorasCurso.Enabled := Not rbGrupos.Checked;
      rbIndividual.Checked := Not rbGrupos.Checked;
      rbIndividual.Enabled := Not rbGrupos.Checked;
      lblMaestroIndividual.Enabled := Not rbGrupos.Checked;
      lblRevisionMultiple.Enabled := Not rbGrupos.Checked;
      lblHorasMultiple.Enabled := Not rbGrupos.Checked;
      lblMaestroMultiple.Enabled := Not rbGrupos.Checked;
      if rbGrupos.Checked then
      begin
        cambiaDatosGlobales;
        zCursos.Llave := VACIO;
        zMaestro.Llave := VACIO;
        edtRevision.text := VACIO;
        HorasCurso.Valor := 0;
      end;
    end;
end;

procedure TInicioSesion.ActivaIndividual;
begin
    if Self.rbindividual.Checked then
    begin
       rbCursos.Enabled := rbIndividual.Checked;
       lblCurso.Enabled := rbIndividual.Checked;
       zCursos.Enabled := rbIndividual.Checked;
       edtRevision.Enabled := rbIndividual.Checked;
       lblMaestro.Enabled := rbIndividual.Checked;
       zMaestro.Enabled := rbIndividual.Checked;
       lblHoras.Enabled := rbIndividual.Checked;
       HorasCurso.Enabled := rbIndividual.Checked;
       lblMaestroIndividual.Enabled := rbIndividual.Checked;
       lblCursos.Enabled := Not rbIndividual.Checked;
       lblListaCurso.Enabled := Not rbIndividual.Checked;
       zCursoMultiple.Enabled := Not rbIndividual.Checked;
       lbCursos.Enabled := Not rbIndividual.Checked;
       cxBotonAgregar.Enabled := Not rbIndividual.Checked;
       cxButtonBorrar.Enabled := Not rbIndividual.Checked;
       cbDatosGlobales.Enabled := Not rbIndividual.Checked;
       edtRevisionMultiple.Enabled := Not rbIndividual.Checked;
       zMaestroMultiple.Enabled := Not rbIndividual.Checked;
       HorasCursoMultiple.Enabled := Not rbIndividual.Checked;
       rbMultiple.Checked := Not rbIndividual.Checked;
       rbGrupos.Checked := Not rbIndividual.Checked;
       lblGrupos.Enabled := Not rbIndividual.Checked;
       rbInscritos.Checked := Not rbIndividual.Checked;
       rbAprobados.Checked := Not rbIndividual.Checked;
       rbAsistencia.Checked := Not rbIndividual.Checked;
       rbInscritos.Enabled := Not rbIndividual.Checked;
       rbAprobados.Enabled := Not rbIndividual.Checked;
       rbAsistencia.Enabled := Not rbIndividual.Checked;
       zGrupo.Enabled := Not rbIndividual.Checked;
       lblRevisionMultiple.Enabled := Not rbIndividual.Checked;
       lblHorasMultiple.Enabled := Not rbIndividual.Checked;
       lblMaestroMultiple.Enabled := Not rbIndividual.Checked;
       if rbIndividual.Checked then
       begin
            cambiaDatosGlobales;
            zGrupo.Llave := VACIO;
       end;
    end;
end;

procedure TInicioSesion.ActivaMultiple;
begin
    if Self.rbMultiple.Checked then
    begin;
       rbCursos.Enabled := rbMultiple.Checked;
       lblCursos.Enabled := rbMultiple.Checked;
       lblListaCurso.Enabled := rbMultiple.Checked;
       zCursoMultiple.Enabled := rbMultiple.Checked;
       lbCursos.Enabled := rbMultiple.Checked;
       cxBotonAgregar.Enabled := rbMultiple.Checked;
       cxButtonBorrar.Enabled := rbMultiple.Checked;
       cbDatosGlobales.Enabled := rbMultiple.Checked;
       edtRevisionMultiple.Enabled := rbMultiple.Checked;
       zMaestroMultiple.Enabled := rbMultiple.Checked;
       HorasCursoMultiple.Enabled := rbMultiple.Checked;
       lblRevisionMultiple.Enabled := rbMultiple.Checked;
       lblHorasMultiple.Enabled := rbMultiple.Checked;
       lblMaestroMultiple.Enabled := rbMultiple.Checked;
       cambiaDatosGlobales;
       lblCurso.Enabled := Not rbMultiple.Checked;
       zCursos.Enabled := Not rbMultiple.Checked;
       lblMaestro.Enabled := Not rbMultiple.Checked;
       zMaestro.Enabled := Not rbMultiple.Checked;
       edtRevision.Enabled := Not rbMultiple.Checked;
       lblHoras.Enabled := Not rbMultiple.Checked;
       HorasCurso.Enabled := Not rbMultiple.Checked;
       lblMaestroIndividual.Enabled := Not rbMultiple.Checked;
       rbIndividual.Checked := Not rbMultiple.Checked;
       rbGrupos.Checked := Not rbMultiple.Checked;
       lblGrupos.Enabled := Not rbMultiple.Checked;
       rbInscritos.Checked := Not rbMultiple.Checked;
       rbAprobados.Checked := Not rbMultiple.Checked;
       rbAsistencia.Checked := Not rbMultiple.Checked;
       rbInscritos.Enabled := Not rbMultiple.Checked;
       rbAprobados.Enabled := Not rbMultiple.Checked;
       rbAsistencia.Enabled := Not rbMultiple.Checked;
       zGrupo.Enabled := Not rbMultiple.Checked;
       if rbMultiple.Checked then
       begin
            zCursos.Llave := VACIO;
            zMaestro.Llave := VACIO;
            edtRevision.text := VACIO;
            HorasCurso.Valor := 0;
            zGrupo.Llave := VACIO;
       end;
    end;
end;

procedure TInicioSesion.getDatos;
var
   appINI : TIniFile;
begin
     appINI := TIniFile.Create( ChangeFileExt( Application.ExeName, '.ini' ) ) ;
     try
        Self.rbCursos.Checked := zStrToBool( appINI.ReadString( 'DATOS', 'CURSOS', K_GLOBAL_SI ) );

        Self.rbindividual.Checked := zStrToBool( appINI.ReadString( 'DATOS', 'INDIVIDUAL', K_GLOBAL_SI ) );
        Self.zCursos.Llave := appINI.ReadString( 'DATOS', 'CURSO', VACIO );
        Self.edtRevision.Text := appINI.ReadString( 'DATOS', 'REVISION', VACIO );
        Self.zMaestro.Llave := appINI.ReadString( 'DATOS', 'MAESTRO', VACIO );
        Self.HorasCurso.Valor := appINI.ReadFloat( 'DATOS', 'HORAS', 0.0 );

        Self.rbMultiple.Checked := zStrToBool( appINI.ReadString( 'DATOS', 'MULTIPLE', K_GLOBAL_NO ) );
        sCursosMultiple := appINI.ReadString( 'DATOS', 'CURSOMULTIPLE', VACIO );
        Self.edtRevisionMultiple.Text := appINI.ReadString( 'DATOS', 'REVISIONMULTIPLE', VACIO );
        Self.zMaestroMultiple.Llave := appINI.ReadString( 'DATOS', 'MAESTROMULTIPLE', VACIO );
        Self.HorasCursoMultiple.Valor := appINI.ReadFloat( 'DATOS', 'HORASMULTIPLE', 0.0 );
        Self.cbDatosGlobales.Checked := zStrToBool( appINI.ReadString( 'DATOS', 'VALORESGLOBALES', K_GLOBAL_NO ) );

        Self.rbGrupos.Checked := zStrToBool( appINI.ReadString( 'DATOS', 'GRUPOS', K_GLOBAL_NO ) );
        Self.zGrupo.Llave := appINI.ReadString( 'DATOS', 'GRUPO', VACIO );
        Self.rbAsistencia.Checked := zStrToBool( appINI.ReadString( 'DATOS', 'ASISTENCIA', K_GLOBAL_NO ) );
        Self.rbAprobados.Checked := zStrToBool( appINI.ReadString( 'DATOS', 'APROBADOS', K_GLOBAL_NO ) );
        Self.rbInscritos.Checked := zStrToBool( appINI.ReadString( 'DATOS','INSCRITOS', K_GLOBAL_NO ) );

        ActivaIndividual;
        ActivaMultiple;
        ActivaGrupos;

    finally
           appINI.Free;
    end;
end;

procedure TInicioSesion.setDatos;
var
   appINI : TIniFile;
begin
     appINI := TIniFile.Create( ChangeFileExt( Application.ExeName,'.ini' ) );
     try
        appINI.WriteString( 'DATOS', 'CURSOS', zBoolToStr( Self.rbCursos.Checked ) );
        appINI.WriteString( 'DATOS', 'CURSO', Self.zCursos.Llave );
        appINI.WriteString( 'DATOS', 'REVISION', Self.edtRevision.Text );
        appINI.WriteString( 'DATOS', 'MAESTRO', Self.zMaestro.Llave );
        appINI.WriteFloat( 'DATOS', 'HORAS', Self.HorasCurso.Valor );
        appINI.WriteString( 'DATOS', 'CURSOMULTIPLE', sCursosMultiple );
        appINI.WriteString( 'DATOS', 'INDIVIDUAL', zBoolToStr( Self.rbindividual.Checked ) );
        appINI.WriteString( 'DATOS', 'MULTIPLE', zBoolToStr( Self.rbMultiple.Checked ) );
        appINI.WriteString( 'DATOS', 'REVISIONMULTIPLE', Self.edtRevisionMultiple.Text );
        appINI.WriteString( 'DATOS', 'MAESTROMULTIPLE', Self.zMaestroMultiple.Llave );
        appINI.WriteFloat( 'DATOS', 'HORASMULTIPLE', Self.HorasCursoMultiple.Valor );
        appINI.WriteString( 'DATOS', 'VALORESGLOBALES', zBoolToStr( Self.cbDatosGlobales.Checked ) );
        appINI.WriteString( 'DATOS', 'GRUPOS', zBoolToStr( Self.rbGrupos.Checked ) );
        appINI.WriteString( 'DATOS', 'GRUPO', Self.zGrupo.Llave );
        appINI.WriteString( 'DATOS', 'ASISTENCIA', zBoolToStr( Self.rbAsistencia.Checked ) );
        appINI.WriteString( 'DATOS', 'APROBADOS', zBoolToStr( Self.rbAprobados.Checked ) );
        appINI.WriteString( 'DATOS', 'INSCRITOS', zBoolToStr( Self.rbInscritos.Checked ) );
    finally
           appINI.Free;
    end;
end;

procedure TInicioSesion.zCursoMultipleValidKey(Sender: TObject);
begin
  inherited;
  with dmCatalogos.cdsCursos do
     begin
          sMaestroMultiple := FieldByName( 'MA_CODIGO' ).AsString;
          rHorasMultiple := FieldByName( 'CU_HORAS' ).AsFloat;
          sRevisionMultiple := FieldByName( 'CU_REVISIO' ).AsString;
     end;
end;

procedure TInicioSesion.zCursosValidKey(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsCursos do
     begin
          //edtMaestro.Text := VACIO;
          //if dmCatalogos.cdsMaestros.Locate( 'MA_CODIGO', FieldByName( 'MA_CODIGO' ).AsString, [] ) then
             //edtMaestro.Text := dmCatalogos.cdsMaestros.FieldByName( 'MA_NOMBRE' ).AsString;
          zMaestro.Llave := FieldByName( 'MA_CODIGO' ).AsString;
          HorasCurso.Valor := FieldByName( 'CU_HORAS' ).AsFloat;
          edtRevision.Text := FieldByName( 'CU_REVISIO' ).AsString;
     end;
end;

procedure TInicioSesion.setStrToInit;
var
  i: Integer;
begin
  inherited;
    sCursosMultiple := VACIO;
    for i := 0 to lCursosMultiple.Count-1 do
         sCursosMultiple := ConcatString( sCursosMultiple, lCursosMultiple[i], ',' );
  lCursosMultiple.Free;
end;

end.
