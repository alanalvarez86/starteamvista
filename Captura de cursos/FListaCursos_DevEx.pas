unit FListaCursos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, checklst, Buttons, ExtCtrls ,ZetaClientDataSet, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxButtons, ImgList, cxControls, cxContainer, cxEdit, cxCheckListBox;

type
  TListaCursos_DevEx = class(TForm)
    PanelInferior: TPanel;
    cxImageList24_PanelBotones: TcxImageList;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    PrendeBtn_DevEx: TcxButton;
    ApagaBtn_DevEx: TcxButton;
    Lista_DevEx: TcxCheckListBox;
    procedure PrendeBtnClick(Sender: TObject);
    procedure ApagaBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure PrendeBtn_DevExClick(Sender: TObject);
    procedure ApagaBtn_DevExClick(Sender: TObject);
  private
    FCurso: String;
    FListaTemp: TStringList;
    procedure PrendeApaga( lState: Boolean );
    procedure AgregaListaCursos;
    procedure FiltraCursos;
    function GetDataSet: TZetaClientDataSet;

  public
    property Curso: String read FCurso write FCurso;
  end;

function SetListaCursos: Boolean;

var
  ListaCursos_DevEx: TListaCursos_DevEx;

implementation

uses dCatalogos, ZetaDialogo, DInterfase;

function SetListaCursos: Boolean;
begin
     if ( ListaCursos_DevEx = nil ) then
        ListaCursos_DevEx:= TListaCursos_DevEx.Create( Application );
     try
        with ListaCursos_DevEx do
        begin
             //Curso := sCurso;
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     finally
        FreeAndNil( ListaCursos_DevEx );
     end;
end;

{$R *.DFM}

procedure TListaCursos_DevEx.FormCreate(Sender: TObject);
begin
     FListaTemp:= TStringList.Create;
     //HelpContext := H80815_Supervision_de_usuarios;
end;

procedure TListaCursos_DevEx.FormShow(Sender: TObject);
begin
     //dmInterfase.CargaListaCursos( Lista_DevEx.Items );
end;

procedure TListaCursos_DevEx.FormDestroy(Sender: TObject);
begin
     FListaTemp.Free;
end;

procedure TListaCursos_DevEx.AgregaListaCursos;
var
   i: Integer;
   tmpLista: TStringList;
   sCodigo : String;
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter     := Delimiter;
   ListOfStrings.DelimitedText := Str;
end;
begin
     FiltraCursos;
     tmpLista := Tstringlist.create();

     for i := 0 to Lista_DevEx.Items.Count - 1 do
         if Lista_DevEx.Items[ i ] .Checked then
         begin
              //DevEx (by am): Separamos la cadena para obtener el codigo
             Split('=',Lista_DevEx.Items[i].Text,tmpLista);
             sCodigo := trim(tmpLista[0]);
             dmInterfase.GuardaListaCursos( sCodigo );
         end;
end;

procedure TListaCursos_DevEx.PrendeBtnClick(Sender: TObject);
begin
     PrendeApaga( True );
end;

procedure TListaCursos_DevEx.ApagaBtnClick(Sender: TObject);
begin
     PrendeApaga( False );
end;

procedure TListaCursos_DevEx.PrendeApaga( lState: Boolean );
var
   i: Integer;
begin
     with Lista_DevEx do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Items[ i ].Checked := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

function TListaCursos_DevEx.GetDataSet:TZetaClientDataSet;
begin
     Result := dmCatalogos.cdsCursos;
end;

procedure TListaCursos_DevEx.FiltraCursos;
var
   iPos: Integer;
begin
     with GetDataSet, Lista_DevEx.Items do
     begin
          BeginUpdate;
          First;
          while not EOF do
          begin
               iPos := IndexOf( FieldByName( 'CU_CODIGO' ).AsString + '=' +
                                FieldByName( 'CU_NOMBRE' ).AsString );
               if ( iPos >= 0 ) then
                  Lista_DevEx.Items[iPos].Checked := FALSE;
               Next;
          end;
          EndUpdate;
     end;
end;

procedure TListaCursos_DevEx.OK_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     with GetDataSet  do
     begin
          DisableControls;
          try
             with Screen do
             begin
                  oCursor := Cursor;
                  Cursor := crHourglass;
                  try
                     AgregaListaCursos;
                     Enviar;
                  finally
                     Cursor := oCursor;
                  end;
             end;
             if ( ChangeCount > 0 ) then     // Algunos no se pudieron agregar
             begin
                  CancelUpdates;
                  ZError( self.Caption, 'No se Agregaron Todos los cursos Seleccionados!', 0 );
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TListaCursos_DevEx.PrendeBtn_DevExClick(Sender: TObject);
begin
     PrendeApaga( True );
end;

procedure TListaCursos_DevEx.ApagaBtn_DevExClick(Sender: TObject);
begin
      PrendeApaga( False );
end;

end.
