unit FTressShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell_DevEx,  ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, FileCTRL, ZBaseConsulta, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxStatusBar,
  dxRibbonStatusBar, cxClasses, dxSkinsForm, dxRibbonSkins,
  dxRibbonCustomizationForm, System.Actions, cxStyles, dxRibbon,
  dxBarBuiltInMenu, cxContainer, cxEdit, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient,
  IdSMTPBase, IdSMTP, dxBar, cxGroupBox, cxButtons, cxTextEdit, cxMemo, cxPC,
  cxShellBrowserDialog, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridLevel, cxGridCustomView,
  cxGridCustomLayoutView, cxGridCardView, cxGrid, ZetaCXGrid, cxGridDBTableView,
  ZetaKeyLookup_DevEx, ZetaDBTextBox, dxGDIPlusClasses, cxImage, SFE, SZCodeBaseX,
  imageenview, ieview, FToolsImageEn;

type
  TTressShell = class(TBaseImportaShell_DevEx)
    dxBarLargeButton1: TdxBarLargeButton;
    _P_Configurar_Conceptos: TAction;
    BrowseDir: TcxShellBrowserDialog;
    DataSource: TDataSource;
    imgListTipoDispositivo: TcxImageList;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel2: TPanel;
    editNumero: TEdit;
    Label1: TLabel;
    Label7: TLabel;
    Panel4: TPanel;
    cxGridCursos: TcxGrid;
    cxGridCursosDBBandedTableView1: TcxGridDBBandedTableView;
    RESULTADO: TcxGridDBBandedColumn;
    cxGridCursosLevel1: TcxGridLevel;
    Panel7: TPanel;
    Panel8: TPanel;
    btnReportes: TcxButton;
    Panel9: TPanel;
    TimerHora: TTimer;
    lblFecha: TLabel;
    lblHora: TLabel;
    lblTurno: TLabel;
    Label3: TLabel;
    lblPuesto: TLabel;
    lblNombre: TLabel;
    dsCursos: TDataSource;
    Splitter1: TSplitter;
    imgTazaFondo: TcxImage;
    gridChecadas_DevEx: TcxGrid;
    gridChecadas_DevExDBTableView: TcxGridDBTableView;
    Empleado: TcxGridDBColumn;
    Nombre: TcxGridDBColumn;
    gridChecadas_DevExLevel: TcxGridLevel;
    cxBSesion: TcxButton;
    lblMensaje: TLabel;
    TimerSensorBiometrico: TTimer;
    FOTO: TImageEnView;
    bCapturaManual: TcxButton;
    TimerEntradaManual: TTimer;
    iListBtnManual: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure btnSalidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure editNumeroKeyPress(Sender: TObject; var Key: Char);
    procedure btnReportesClick(Sender: TObject);
    procedure TimerHoraTimer(Sender: TObject);
    procedure cxGridCursosResize(Sender: TObject);
    procedure cxGridCursosDBBandedTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cxBSesionClick(Sender: TObject);
    procedure TimerSensorBiometricoTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bCapturaManualClick(Sender: TObject);
    procedure TimerEntradaManualTimer(Sender: TObject);
  private
    { Private declarations }
    lModoBatch: Boolean;
    FDrivers   : boolean;
    FAbrioSFE  : boolean;
    function LeeArchivo: Boolean;
    function SincronizaHuellas: Boolean;
    function getDatosConfiguracion(sLlave: String): String;
    function getDatosReporte(sLlave: String): integer;
    function getTipoLector: integer;
    function getArchivoHuellas: string;
    procedure switchWorking(bWorking : Boolean);
    function CaptureFinger() : Integer;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
    procedure CargaListaCursos;
    procedure borrarEmpleadoInfo;
    procedure DetieneLector;
    procedure InicializaSensorBiometrico;
    procedure InicializaBiometrico;
    procedure PrintError(error: Integer);
    function GetBioMessage(error: Integer) : String;
    procedure ProcesarChecada;
    procedure ObtieneChecadaProx(idNumeroHex: String);
    procedure DeshabilitaControles;
    function GetMostrarFoto: Boolean;
    function getLetraCredencial: String;
    function getDigitoEmpresa: String;
    procedure DetieneEntradaManual;

  protected
    { Protected declarations }
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure abrirConfigurar;
  end;

var
  TressShell: TTressShell;
  iContadorAnalitica: integer;

const
     K_PARAM_ARCHIVO         = 2;
     K_PARAM_TIPO_ARCHIVO    = 3;

implementation

uses IniFiles,
     DCliente,
     DGlobal,
     DSistema,
     DCatalogos,
     DTablas,
     DRecursos,
     DConsultas,
     DProcesos,
     DInterfase,
     DAsistencia,
     DNomina,
     DReportes,
     ZAsciiTools,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     FCalendario,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     ZetaDialogo,
     ZetaServerTools,
     ZGridModeTools,
     ZetaBusqueda_DevEx,
     FConfigurar,
     FInicioSesion,
     FAutoClasses, ZAccesosMgr, ZAccesosTress;

{$R *.dfm}

var
  gTemplate         : array[0..1403] of Integer; //351*4 = 1404
  gImageBytes       : array[0..65535] of Byte; // 256 * 256
  addrOfTemplate    : Pbyte; //Direccion de memoria del template
  addrOfImageBytes  : Pbyte; //Direccion de memoria de la imagen a mostrar
  gHintStopPos      : Integer;
  gWorking          : Boolean; //Bandera de lector en procesos
  iDedo             : Integer;
  iSeg              : Integer; //entrada manual

const
  FINGER_BITMAP_WIDTH   : Integer = 256; //Ancho de la imagen de la huella
  FINGER_BITMAP_HEIGHT  : Integer = 256; //Alto de la imagen de la huella
  ColorNColor           : Integer = 3;

procedure TTressShell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dmCliente.AnaliticaContabilizarUso('Versi�n 2018', 10, iContadorAnalitica);
  if ( getTipoLector = 2 ) then
    DetieneLector;
end;

procedure TTressShell.FormCreate(Sender: TObject);
begin
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmNomina   := TdmNomina.Create( Self );
     dmAsistencia := TdmAsistencia.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmInterfase := TdmInterfase.Create( Self );
     inherited;
     self.WindowState := wsMaximized;
     DataSource.DataSet := dmInterfase.cdsHistorial;
     dsCursos.Dataset := dmInterfase.cdsListaCursos;
     cxGridCursosDBBandedTableView1.DataController.DataModeController.GridMode:= True;
     FDrivers  := LoadDLL;
     FAbrioSFE := FALSE;
     iContadorAnalitica := 0;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
  	  DataSource.DataSet := nil;
     dsCursos.Dataset :=  nil;
     FreeAndNil( dmInterfase );
     FreeAndNil( dmReportes );
     FreeAndNil( dmAsistencia );
     FreeAndNil( dmNomina );
     FreeAndNil( dmProcesos );
     FreeAndNil( dmRecursos );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmTablas );
     FreeAndNil( dmCatalogos );
     FreeAndNil( dmSistema );
     FreeAndNil( Global );
     inherited;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     gridChecadas_DevExDBTableView.DataController.DataSource := DataSource;
     gridChecadas_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
     gridChecadas_DevExDBTableView.OptionsView.GroupByBox := False;
     gridChecadas_DevExDBTableView.FilterBox.Visible := fvNever;
     gridChecadas_DevExDBTableView.FilterBox.CustomizeDialog := TRUE;
     cxGridCursosDBBandedTableView1.DataController.DataSource := dsCursos;
     ActiveControl := editNumero;
end;

function TTressShell.getArchivoHuellas: string;
var
   appINI : TIniFile;
   sRuta : String;
begin
     sRuta := ExtractFilePath( Application.ExeName );
     sRuta := VerificaDir( sRuta );
     sRuta := sRuta + 'Configuracion.ini';
     appINI := TIniFile.Create( sRuta ) ;
     try
        Result := appINI.ReadString( 'CONFIGURACION', 'RUTAHUELLA', VACIO);
     finally
            appINI.Free;
     end;
end;

function TTressShell.getDatosConfiguracion(sLlave: String): String;
var
   appINI : TIniFile;
begin
     appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
     try
        Result := appINI.ReadString('DATOS', sLlave, VACIO);
     finally
            appINI.Free;
     end;
end;

function TTressShell.getDatosReporte(sLlave: String): integer;
var
   appINI : TIniFile;
   sRuta : String;
begin
     sRuta := ExtractFilePath( Application.ExeName );
     sRuta := VerificaDir( sRuta );
     sRuta := sRuta + 'Configuracion.ini';
     appINI := TIniFile.Create( sRuta );
     try
        Result := appINI.ReadInteger('CONFIGURACION', sLlave, 0);
     finally
            appINI.Free;
     end;
end;

procedure TTressShell.DoOpenAll;
var
    sMsg : String;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosAsistencia;
             InitActivosEmpleado;
             InitArrayTPeriodo;
             InitActivosPeriodo;
        end;
        if not dmCliente.EsModoDemo then
        begin
            if dmCliente.ModuloAutorizadoConsulta( okCursos , sMsg ) then
            begin
                if ZAccesosMgr.RevisaCualquiera(D_EMP_EXP_CURSOS_TOMADOS) or ZAccesosMgr.RevisaCualquiera(D_CAT_CAPA_SESIONES) then
                begin
                    TimerSensorBiometrico.Enabled := False;
                    switchWorking( False );
                    if ( getTipoLector = 2 ) then
                    begin
                         InicializaBiometrico;
                         if ZetaDialogo.zWarningYesNoConfirm( 'Captura de cursos', '�Desea actualizar informaci�n de biom�trico?', 0, mbNo ) then
                         begin
                              gHintStopPos := 0;
                              if FDrivers then
                              begin
                                   FAbrioSFE := LeeArchivo;
                                   if ( FAbrioSFE ) then
                                      SincronizaHuellas;
                              end;
                         end;
                         if FDrivers then
                         begin
                            if LeeArchivo then
                            begin
                                TimerSensorBiometrico.Enabled := True;
                            end;
                         end;
                    end;
                    CargaListaCursos;
                    InitBitacora;
                    CargaSistemaActivos;
                end
                else
                begin
                    DeshabilitaControles;
                    ZetaDialogo.ZError('Captura de Cursos', 'No cuenta con derechos de acceso.',0);
                end;
            end
            else
            begin
                DeshabilitaControles;
                ZetaDialogo.ZError('Captura de Cursos', 'El modulo de cursos no esta autorizado.',0);
            end;
        end
        else
        begin
            DeshabilitaControles;
            ZetaDialogo.ZError( 'Captura de Cursos', 'El Sistema TRESS esta en modo DEMO.', 0 );
        end;
     except
        on Error : Exception do
        begin
             DeshabilitaControles;
             ReportaErrorDialogo( 'Error al conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message );
             DoCloseAll;
        end;
     end;
     lblFecha.caption := FormatDateTime('dddd dd ''de'' mmmm ''de'' yyyy',Date);
     lblHora.caption := 'a las ' + TimeToStr(Time);
     if EditNumero.CanFocus then
        EditNumero.SetFocus;
     EditNumero.SelectAll;
end;

procedure TTressShell.DeshabilitaControles;
begin
    editNumero.Enabled := FALSE;
    cxBSesion.Enabled := FALSE;
    btnReportes.Enabled := FALSE;
    bCapturaManual.Enabled := FALSE;
end;

procedure TTressShell.CargaListaCursos;
 var
    oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        dmInterfase.CargaListaCursos;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmConsultas );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.editNumeroKeyPress(Sender: TObject; var Key: Char);
var
   lCurso: Boolean;
   iValue, iCode: Integer;
begin
     inherited;
     lCurso := FALSE;
     if ( key=#13 ) then
     begin
         if bCapturaManual.Down then
         begin
          if( Length( EditNumero.Text ) <= 9 ) then
          begin
            val( EditNumero.Text, iValue, iCode );
            if iCode = 0 then
            begin
              EditNumero.Text := getDigitoEmpresa + StringOfChar('0',9 - Length(EditNumero.Text)) + EditNumero.Text + getLetraCredencial;
              ProcesarChecada;
              DetieneEntradaManual;
              if ( getTipoLector = 2 ) then
                  TimerSensorBiometrico.Enabled := TRUE;
            end
            else
            begin
              ZetaDialogo.ZError( 'Captura de cursos', 'Informaci�n del registro del empleado invalida.', 0 );
              DetieneEntradaManual;
              if ( getTipoLector = 2 ) then
                  TimerSensorBiometrico.Enabled := TRUE;
            end;
          end
          else
          begin
              ZetaDialogo.ZError( 'Captura de cursos', 'Informaci�n del registro del empleado invalida.', 0 );
              DetieneEntradaManual;
              if ( getTipoLector = 2 ) then
                  TimerSensorBiometrico.Enabled := TRUE;
          end;
         end
         else
         begin
             if EditNumero.Text = 'CURSO' then
             begin
                DetieneLector;
                EditNumero.Clear;
                abrirConfigurar;
                lCurso := TRUE;
             end
             else
             begin
                if ( getTipoLector = 1 ) then  //Proximidad
                    ObtieneChecadaProx(EditNumero.Text);
                ProcesarChecada;
             end;
         end;
         if not lCurso then
         begin
            EditNumero.Text := VACIO;
            if EditNumero.CanFocus then
              EditNumero.SetFocus;
            EditNumero.SelectAll;
         end;
     end;
end;

procedure TTressShell.ProcesarChecada;
var
   sTexto, sCursos, sGrupo: string;
begin
    if ( getTipoLector = 2 ) then
    begin
        if ( FDrivers ) then
        begin
            TimerSensorBiometrico.Enabled := False;
            switchWorking( False );
        end;
    end;

    if ( Length( Trim( EditNumero.Text ) ) >= 3 ) then
    begin
         if ( dmInterfase.ConectaEmpresa( Trim( Copy( EditNumero.Text, 1, 1 ) ) ) ) then
         begin
              sTexto := Trim( Copy( EditNumero.Text, 2, Length( Trim(EditNumero.Text) ) - 2 ) );
         end
         else
         begin
            ZetaDialogo.ZError( 'Captura de cursos', 'Empresa de empleado no existe en Sistema TRESS.', 0 );
            sTexto := VACIO;
         end;
    end
    else
    begin
         ZetaDialogo.ZError( 'Captura de cursos', 'Informaci�n del registro del empleado invalida.', 0 );
         sTexto := VACIO;
    end;
    if strLleno( sTexto ) then
    begin
         with dmInterfase do
         begin
              if zStrToBool( getDatosConfiguracion( 'INDIVIDUAL' ) ) then
              begin
                   sCursos := getDatosConfiguracion( 'CURSO' );
                   if strLleno( sCursos ) then
                   begin
                        if (GrabaCursoTomado( StrToInt( sTexto ), dmCliente.FechaDefault, sCursos, 'I' )) then
                        begin
                            lblNombre.Caption := NombreEmpleado;
                            lblTurno.Caption := TurnoEmpleado;
                            lblPuesto.Caption := PuestoEmpleado;
                            if getMostrarFoto then
                              FToolsImageEn.AsignaBlobAImagen( FOTO , cdsHistCursos, 'IM_BLOB' );
                            iContadorAnalitica := iContadorAnalitica + 1;
                        end
                        else
                        begin
                            borrarEmpleadoInfo;
                        end;
                   end
                   else
                   begin
                        borrarEmpleadoInfo
                   end;
              end;
              if zStrToBool( getDatosConfiguracion( 'MULTIPLE' ) ) then
              begin
                   sCursos := getDatosConfiguracion( 'CURSOMULTIPLE' );
                   if strLleno( sCursos ) then
                   begin
                        if (GrabaCursoTomado( StrToInt( sTexto ), dmCliente.FechaDefault, sCursos, 'M' )) then
                        begin
                            lblNombre.Caption := NombreEmpleado;
                            lblTurno.Caption := TurnoEmpleado;
                            lblPuesto.Caption := PuestoEmpleado;
                            if getMostrarFoto then
                              FToolsImageEn.AsignaBlobAImagen( FOTO , cdsHistCursos, 'IM_BLOB' );
                            iContadorAnalitica := iContadorAnalitica + 1;
                        end
                        else
                        begin
                            borrarEmpleadoInfo;
                        end;
                   end
                   else
                   begin
                        borrarEmpleadoInfo;
                   end;
              end;
              if zStrToBool( getDatosConfiguracion( 'GRUPOS' ) ) then
              begin
                   sGrupo := getDatosConfiguracion( 'GRUPO' );
                   if strLleno( sGrupo ) then
                   begin
                        if (GrabaCursoTomado( StrToInt( sTexto ), dmCliente.FechaDefault, sGrupo, 'G' )) then
                        begin
                          lblNombre.Caption := NombreEmpleado;
                          lblTurno.Caption := TurnoEmpleado;
                          lblPuesto.Caption := PuestoEmpleado;
                          if getMostrarFoto then
                            FToolsImageEn.AsignaBlobAImagen( FOTO , cdsHistCursos, 'IM_BLOB' );
                          iContadorAnalitica := iContadorAnalitica + 1;
                        end
                        else
                        begin
                          borrarEmpleadoInfo;
                        end;
                   end
                   else
                   begin
                        borrarEmpleadoInfo;
                   end;
              end;
         end;
    end;
    if ( getTipoLector = 2 ) then
    begin
        if ( FDrivers ) then
        begin
            TimerSensorBiometrico.Enabled := False;
            switchWorking( False );
        end;
    end;
    if iContadorAnalitica >= 50 then
    begin
      dmCliente.AnaliticaContabilizarUso('Versi�n 2018', 10, iContadorAnalitica);
      iContadorAnalitica := 0;
    end;
end;


procedure TTressShell.bCapturaManualClick(Sender: TObject);
begin
  inherited;
    if (getDigitoEmpresa = VACIO) or (getLetraCredencial = VACIO) then
    begin
      DetieneLector;
      bCapturaManual.Down := FALSE;
      ZetaDialogo.ZError( 'Captura de cursos', 'No han sido configurados los valores de Letra de credencial y/o D�gito de empresa. Ingrese a la configuraci�n de la estaci�n', 0 );
      if ( getTipoLector = 2 ) then
          TimerSensorBiometrico.Enabled := TRUE;
    end
    else
    begin
      bCapturaManual.Down := TRUE;
      bCapturaManual.OptionsImage.ImageIndex := 1;
      iSeg := 5;
      DetieneLector;
      EditNumero.SetFocus;
      TimerEntradaManual.Enabled := TRUE;
    end;
end;

procedure TTressShell.borrarEmpleadoInfo;
begin
    lblNombre.Caption := VACIO;
    lblTurno.Caption := VACIO;
    lblPuesto.Caption := VACIO;
    FOTO.Blank;
end;

procedure TTressShell.DoProcesar;
begin
     { No Implementado }
end;

procedure TTressShell.CargaSistemaActivos;
begin
     RefrescaSistemaActivos;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     // No Implementar
end;

procedure TTressShell.ArchivoSeekClick(Sender: TObject);
begin
     // No Implementar
end;

procedure TTressShell.btnReportesClick(Sender: TObject);
var
   sCursos: string;
   iReporte: Integer;
begin
     inherited;
     DetieneLector;
     DetieneEntradaManual;
     if not gridChecadas_DevExDBTableView.DataController.DataSource.DataSet.IsEmpty then
     begin
         if zStrToBool( getDatosConfiguracion( 'INDIVIDUAL' ) ) then
         begin
              sCursos := getDatosConfiguracion( 'CURSO' );
         end;
         if zStrToBool( getDatosConfiguracion( 'MULTIPLE' )) then
         begin
              sCursos := getDatosConfiguracion( 'CURSOMULTIPLE' );
              sCursos := StrTransAll( sCursos, ',', ''',''' );
              sCursos := ansiUpperCase( sCursos );
         end;
         if zStrToBool( getDatosConfiguracion( 'GRUPOS' ) ) and zStrToBool( getDatosConfiguracion( 'APROBADOS' ) ) then
         begin
              sCursos := dmInterfase.cdsListaCursos.FieldByName( 'CU_CODIGO' ).AsString;
         end;
         if zStrToBool( getDatosConfiguracion( 'GRUPOS' ) ) and ( zStrToBool( getDatosConfiguracion( 'ASISTENCIA' ) ) or zStrToBool( getDatosConfiguracion( 'INSCRITOS' ) ) ) then
         begin
              sCursos := getDatosConfiguracion( 'GRUPO' );
         end;
         if StrLleno( sCursos ) then
         begin
            iReporte := getDatosReporte( 'REPORTE' );
            if iReporte <> 0 then
            begin
              if zStrToBool( getDatosConfiguracion( 'GRUPOS' ) ) and ( zStrToBool( getDatosConfiguracion( 'ASISTENCIA' ) ) or zStrToBool( getDatosConfiguracion( 'INSCRITOS' ) ) ) then
              begin
                   dmReportes.ImprimeUnaForma( Format( '( CUR_ASIS.SE_FOLIO = %s )', [ EntreComillas( sCursos ) ] ), iReporte );
              end
              else
              begin
                   if zStrToBool( getDatosConfiguracion( 'GRUPOS' ) ) and zStrToBool( getDatosConfiguracion( 'APROBADOS' )) then
                   begin
                      dmReportes.ImprimeUnaForma( Format( '( KARCURSO.CU_CODIGO in ( %s ) ) and ( KARCURSO.KC_FEC_TOM = %s )', [ EntreComillas( sCursos ), EntreComillas( dmInterfase.FechaIniGrupo ) ] ), iReporte );
                   end
                   else
                   begin
                      dmReportes.ImprimeUnaForma( Format( '( KARCURSO.CU_CODIGO in ( %s ) ) and ( KARCURSO.KC_FEC_TOM = %s )', [ EntreComillas( sCursos ), EntreComillas( FechaToStr( dmCliente.FechaDefault ) ) ] ), iReporte );
                   end;
              end;
            end
            else
            begin
                ZetaDialogo.ZError( 'Captura de cursos', 'El reporte no ha sido configurado.', 0 );
            end;
         end;
     end
     else
     begin
        ZetaDialogo.ZError( 'Captura de cursos',  'No hay empleados para generar reporte.', 0 );
     end;
     if ( getTipoLector = 2 ) then
     begin
        if ( FDrivers ) then
        begin
            TimerSensorBiometrico.Enabled := False;
        end;
     end;
end;

procedure TTressShell.btnSalidaClick(Sender: TObject);
begin
     Close;
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     // No Implementar
end;

procedure TTressShell._A_SalirSistemaExecute(Sender: TObject);
begin
  inherited;

end;

{ Procedimientos y Funciones Publicos del Shell de Tress }
procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

function TTressShell.SincronizaHuellas: Boolean;
var
   nRet, iId, iDedo, iValor, iEmpleado: Integer;
   iContador: Extended;
   sFilename, sRuta: string;
   ss: TStringStream;
   ms: TMemoryStream;
   oCursor: TCursor;

begin
     iId := 0;
     iDedo := 0;
     iContador := 0;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     sRuta := ExtractFilePath( getArchivoHuellas );
     sRuta := VerificaDir( sRuta );
     try
        if dmInterfase.AdquiereHuellas then
        begin
             with dmInterfase.cdsHuellasCurso do
             begin
                  First;
                  ms := TMemoryStream.Create;
                  while not EOF do
                  begin
                       Application.ProcessMessages();
                       try
                          iId := FieldByName( 'ID_NUMERO' ).AsInteger;
                          iDedo := FieldByName( 'HU_INDICE' ).AsInteger;
                          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;

                          nRet := SFE.CheckFingerNum(iId, iDedo);
                          if ( nRet <> 0 ) then
                          begin
                               ss := TStringStream.Create( FieldByName( 'HUELLA' ).AsString );
                               ms.Position := 0;
                               SZDecodeBase64( ss, ms );

                               sFilename := sRuta + Format( '%s_%s.tem', [ IntToStr( iId ), IntToStr( iDedo ) ] );
                               ms.SaveToFile( sFilename );

                               nRet := SFE.LoadTemplateFromFile( PWideChar( sFilename ), addrOfTemplate );

                               if ( nRet < 0 ) then
                               begin
                                    ZetaDialogo.ZError( 'Captura de cursos', Format( 'Error con plantilla, Empleado: %d Id: %d Indice: %d', [ iEmpleado, iId, iDedo ] ), 0 );
                               end
                               else
                               begin
                                    nRet := SFE.TemplateEnroll( iId , iDedo, 0, addrOfTemplate );
                                    if ( ( nRet < 0 ) and ( nRet <> -102 ) ) then
                                    begin
                                         ZetaDialogo.ZError( 'Captura de cursos', Format( 'Error al enrolar, Empleado: %d Id: %d Indice: %d', [ iEmpleado, iId, iDedo ] ), 0 );
                                    end;
                               end;
                               DeleteFile( sFilename );
                          end;
                       except
                             on Error: Exception do
                             begin
                                  ZetaDialogo.ZError( 'Captura de cursos', Format( 'Error proceso de obtener plantillas, Empleado: %d Id: %d Indice: %d', [ iEmpleado, iId, iDedo ] ), 0 );
                             end;
                       end;
                       Next;
                  end;
             end;
        end
        else
        begin
             ZetaDialogo.ZError( 'Captura de cursos', 'No se tienen plantillas por enrolar', 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     ms.Clear;
end;

procedure TTressShell.switchWorking(bWorking: Boolean);
begin
     gWorking := bWorking;
end;

procedure TTressShell.TimerEntradaManualTimer(Sender: TObject);
begin
  inherited;
  if bCapturaManual.Down = TRUE then
  begin
    lblMensaje.Caption := 'La captura manual estar� habilitada por los siguientes ' + IntToStr( iSeg ) + ' segundos. ';
    iSeg := iSeg - 1;
    if iSeg = -1 then
    begin
      DetieneEntradaManual;
    end;
  end;
end;

procedure TTressShell.DetieneEntradaManual;
begin
  inherited;
  TimerEntradaManual.Enabled := FALSE;
  bCapturaManual.Down := FALSE;
  bCapturaManual.OptionsImage.ImageIndex := 0;
  iSeg := 5;
  lblMensaje.Caption := VACIO;
end;

procedure TTressShell.TimerHoraTimer(Sender: TObject);
begin
     inherited;
     lblHora.caption := 'a las ' + TimeToStr(Time);
end;

//Proximidad
procedure TTressShell.ObtieneChecadaProx(idNumeroHex: String);
begin
    inherited;
    editNumero.Text := dmInterfase.GetChecadaEmpleadoProx(idNumeroHex);
end;

// BIOMETRICO

procedure TTressShell.TimerSensorBiometricoTimer(Sender: TObject);
begin
     if ( getTipoLector = 2 ) and ( bCapturaManual.Down = FALSE )then
     begin
          TimerSensorBiometrico.Enabled := FALSE;
          switchWorking( False );
          InicializaSensorBiometrico;
          CargaListaCursos;
          if EditNumero.CanFocus then
              EditNumero.SetFocus;
          EditNumero.SelectAll;
          if not (editNumero.Text = VACIO) then
              ProcesarChecada;
          EditNumero.Text := VACIO;
          TimerSensorBiometrico.Enabled := TRUE;
     end;
end;

// BIOMETRICO
procedure TTressShell.InicializaSensorBiometrico;
var
   nRet, userId, fingerNum: Integer;

   procedure ObtieneLectura;
   label
        stopWorking;
   begin
        if ( LeeArchivo ) then
        begin
             try
                switchWorking(True);
                while gWorking do
                begin
                     Application.ProcessMessages();
                     lblMensaje.Caption := 'Puede poner su huella en el lector';
                     userId := 0;

                     nRet := CaptureFinger;

                     if nRet <> 1 then
                        goto stopWorking;

                     nRet := SFE.Identify(userId, fingerNum);
                     if ( nRet < 0 ) then
                     begin
                          lblMensaje.Caption := VACIO;
                          PrintError(nRet);
                          TimerSensorBiometrico.Enabled := FALSE;
                          switchWorking( False );
                          ZetaDialogo.ZError( 'Captura de cursos', 'Empleado no identificado.', 0 );
                     end
                     else
                     begin
                          TimerSensorBiometrico.Enabled := FALSE;
                          lblMensaje.Caption := VACIO;
                          editNumero.Text := dmInterfase.GetChecadaEmpleado( userId ) + char(13);
                          goto stopWorking;
                     end;
                end;
stopWorking:
                switchWorking( False );
             except
                   on Error: Exception do
                   begin
                        lblMensaje.Caption := VACIO;
                        switchWorking(False);
                   end;
             end;
        end;
   end;

begin
     ObtieneLectura;
     {if ( userId > 0 ) then
     begin
          edtNumero.Tesxt := IntToStr( userId );
          //ProcesaChecada;
     end;}
end;

function TTressShell.CaptureFinger() : Integer;
var
  nRet, nCapArea, nMaxCapArea, nResult : Integer;
begin
     try
        Result := 0;
        nResult := 1;
        nMaxCapArea := 0;

        while True do
        begin
             try
                Application.ProcessMessages();
                if Not gWorking then
                begin
                     nResult := 0;
                     break;
                end;

                if ( FDrivers ) then
                   nRet := SFE.Capture();
                if nRet < 0 then
                begin
                     nResult := 0;
                     break;
                end;
                Application.ProcessMessages();
                if ( FDrivers ) then
                   nCapArea := SFE.IsFinger();
                if nCapArea >= 0 then
                begin
                     if nCapArea < nMaxCapArea + 2 then break;
                     if nCapArea > nMaxCapArea then nMaxCapArea := nCapArea;
                     if nCapArea > 45 then break;
                end;
                Application.ProcessMessages();
             except
                   Result := 0;
                   Exit;
             end;
        end;
        Application.ProcessMessages();

        if nResult = 1 then
        begin
             if ( FDrivers ) then
                nRet := SFE.GetImage(addrOfImageBytes);
             if nRet < 0 then
             begin
                  PrintError(nRet);
                  nResult := 0;
             end
        end;
        Application.ProcessMessages();
        CaptureFinger := nResult;
     except
           on E : Exception do
           begin
                DetieneLector;
                InicializaBiometrico;
                Result := 0;
           end;
     end;
end;

procedure TTressShell.InicializaBiometrico;
begin
     {$ifdef ANTES}
     addrOfImageBytes := Integer(addr(gImageBytes));
     addrOfTemplate := Integer(addr(gTemplate));
     {$else}
     addrOfImageBytes := addr( gImageBytes );
     addrOfTemplate := addr( gTemplate );
     {$endif}
end;

procedure TTressShell.DetieneLector;
begin
     if ( FDrivers ) then
        SFE.Close();
     switchWorking( False );
     TimerSensorBiometrico.Enabled := False;
     lblMensaje.Caption := VACIO;
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.cxBSesionClick(Sender: TObject);
begin
  inherited;
     DetieneLector;
     DetieneEntradaManual;
     with TInicioSesion.Create( Application ) do
     begin
          ShowModal;
          if ModalResult = mrOK then
             dmInterfase.cdsHistorial.EmptyDataset;
          borrarEmpleadoInfo;
          if (getTipoLector = 2) then
          begin
             if FDrivers then
             begin
                if LeeArchivo then
                begin
                    InicializaBiometrico;
                    TimerSensorBiometrico.Enabled := True;
                end;
             end;
          end
          else
          begin
              DetieneLector;
          end;
     end;
     lblMensaje.Caption := VACIO;
     CargaListaCursos;
     if EditNumero.CanFocus then
        EditNumero.SetFocus;
     EditNumero.SelectAll;
end;

procedure TTressShell.abrirConfigurar;
begin
     inherited;
     DetieneLector;
     DetieneEntradaManual;
     with TConfigurar.Create( Application ) do
     begin
          ShowModal;
          if ModalResult = mrOK then
          begin
              FOTO.Blank;
              if (getTipoLector = 2 ) then
              begin
                if FDrivers then
                begin
                  if LeeArchivo then
                  begin
                    InicializaBiometrico;
                    TimerSensorBiometrico.Enabled := True;
                  end;
                end;
              end
              else
              begin
                DetieneLector;
              end;
          end;
     end;
     lblMensaje.Caption := VACIO;
     CargaListaCursos;
     if EditNumero.CanFocus then
        EditNumero.SetFocus;
     EditNumero.SelectAll;
end;

procedure TTressShell.cxGridCursosDBBandedTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     inherited;
     if  (AViewInfo.GetNamePath = 'CU_NOMBRE') then
     begin
          with cxGridCursosDBBandedTableView1.DataController do
          begin
               ACanvas.Font.Color := RGB(194, 0, 0);
          end;
     end;
end;

procedure TTressShell.cxGridCursosResize(Sender: TObject);
begin
     inherited;
     cxGridCursosDBBandedTableView1.Bands[0].Width := cxGridCursos.Width - 3;
end;


procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

function TTressShell.getLetraCredencial: String;
var
    appINI : TIniFile;
    sRuta : String;
begin
    sRuta := ExtractFilePath( Application.ExeName );
    sRuta := VerificaDir( sRuta );
    sRuta := sRuta + 'Configuracion.ini';
    appINI := TIniFile.Create( sRuta );
    try
        Result := appINI.ReadString( 'CONFIGURACION', 'LETRA', VACIO );
    finally
        appINI.Free;
    end;
end;

function TTressShell.getDigitoEmpresa: String;
var
    appINI : TIniFile;
    sRuta : String;
begin
    sRuta := ExtractFilePath( Application.ExeName );
    sRuta := VerificaDir( sRuta );
    sRuta := sRuta + 'Configuracion.ini';
    appINI := TIniFile.Create( sRuta );
    try
        Result := appINI.ReadString( 'CONFIGURACION', 'DIGITO', VACIO );
    finally
        appINI.Free;
    end;
end;


function TTressShell.getMostrarFoto: Boolean;
var
    appINI : TIniFile;
    sRuta : String;
begin
    sRuta := ExtractFilePath( Application.ExeName );
    sRuta := VerificaDir( sRuta );
    sRuta := sRuta + 'Configuracion.ini';
    appINI := TIniFile.Create( sRuta );
    try
      Result := zStrToBool( appINI.ReadString( 'CONFIGURACION', 'MUESTRAFOTO', K_GLOBAL_NO ) );
    finally
      appINI.Free;
    end;
end;

function TTressShell.getTipoLector: integer;
var
   appINI : TIniFile;
   sRuta : String;
begin
     sRuta := ExtractFilePath( Application.ExeName );
     sRuta := VerificaDir( sRuta );
     sRuta := sRuta + 'Configuracion.ini';
     appINI := TIniFile.Create( sRuta ) ;
     try
        Result := appINI.ReadInteger( 'CONFIGURACION', 'TIPOGAFETE', 0 );
     finally
            appINI.Free;
     end;
end;

function TTressShell.LeeArchivo: Boolean;
var
   nRet : Integer;
   sArchivoHuellas: string;
begin
     nRet := 0;
     sArchivoHuellas := getArchivoHuellas;
     try
        if FileExists( sArchivoHuellas ) then
        begin
           //DeleteFile( sArchivoHuellas );
          if ( FDrivers ) then
             nRet := SFE.Open( PWideChar( sArchivoHuellas ), K_TIPO_BIOMETRICO, 0 );
          if ( nRet <> 0 ) then
          begin
               ZetaDialogo.ZError( 'Captura de cursos', Format( 'Error al abrir archivo %s', [ sArchivoHuellas ] ), 0 );
               Result := False;
          end
          else
          begin
              Result := True;
          end;
        end
        else
        begin
            lblMensaje.Caption := 'No existe archivo de huellas';
            Result := False;
        end;
     except
           on Error: Exception do
           begin
                //DeleteFile( sArchivoHuellas );
                Result := False;
           end;
     end;
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     // No Implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     // No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     // No Implementar
end;

procedure TTressShell.PrintError(error: Integer);
begin
     lblMensaje.Caption := GetBioMessage(error);
end;

function TTressShell.GetBioMessage(error: Integer) : String;
begin
    case error of
        0: Result := 'Exito!';
        -1: Result := 'Error de imagen de huella!';
        -2: Result := 'Huella inv�lida!';
        -3: Result := 'Error de n�mero biom�trico!';
        -4: Result := 'Error de archivo de dispositivo!';
        -6: Result := 'Error de almacenamiento de dispositivo!';
        -7: Result := 'Error de sensor de dispositivo!';
        -8: Result := 'Orden de enrolamiento incorrecto!';
        -9: Result := 'No puede analizar las tres capturas, intente de nuevo!';
        -11: Result := 'La imagen no es un dedo!';
        -100: Result := 'No hay dispositivo biom�trico!';
        -101: Result := 'No puede abrir archivo de dispositivo local!';
        -102: Result := 'Ya ha enrolado esa huella!';
        -103: Result := 'Error de identificaci�n de huella!';
        -104: Result := 'Error de verificaci�n de huella!';
        -105: Result := 'No puede abrir la imagen de huella!';
        -106: Result := 'No puede crear imagen de huella!';
        -107: Result := 'No puede abrir huella!';
        -108: Result := 'No puede crear huella!';
        else Result := 'Error desconocido! (Numero =' + IntToStr(error) + ')';
    end;
end;

end.
