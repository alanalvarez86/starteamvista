unit FGridDiasHoras_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls, StdCtrls, Mask,
     ZetaFecha, ZetaDBGrid, ZetaSmartLists, ZBaseGridEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, cxLabel, cxBarEditItem;
type
  TGridDiasHoras_DevEx = class(TBaseGridEdicion_DevEx)
    ZFecha: TZetaDBFecha;
    cbOperacion: TdxBarCombo;
    cxBarEditItem1: TcxBarEditItem;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridColExit(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
    procedure BuscaMotivo;
  protected
    { Protected declarations }
    function PosicionaSiguienteColumna: Integer; override;
    procedure Buscar; override;
    procedure Connect; override;
    procedure EscribirCambios; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  GridDiasHoras_DevEx: TGridDiasHoras_DevEx;

implementation

uses DNomina,
     DCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx,
     FTipoExcepcion_DevEx;

{$R *.DFM}

{ TGridDiasHoras }

procedure TGridDiasHoras_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33211_Dias_horas;
     cbOperacion.ItemIndex := dmNomina.OperacionConflicto; // Default Anterior
end;

procedure TGridDiasHoras_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmNomina do
     begin
          cdsExcepciones.Refrescar;
          DataSource.DataSet:= cdsExcepciones;
     end;
end;

procedure TGridDiasHoras_DevEx.Buscar;
begin
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado
          else
              if ( FieldName = 'FA_MOTIVO' ) then
                 BuscaMotivo;
     end;
end;

procedure TGridDiasHoras_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmNomina.cdsExcepciones do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridDiasHoras_DevEx.BuscaMotivo;
var
   sNewDia_Hor: String;
   iNewTipo: Integer;
begin
     sNewDia_Hor := '';
     iNewTipo := 0;
     with dmNomina.cdsExcepciones do
     begin
          if ShowTipoExcepcion( FieldByName( 'FA_DIA_HOR' ).AsString,
                                FieldByName( 'FA_MOTIVO' ).AsInteger,
                                sNewDia_Hor,
                                iNewTipo ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'FA_DIA_HOR' ).AsString := sNewDia_hor;
               FieldByName( 'FA_MOTIVO' ).AsInteger := iNewTipo;
          end;
{          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_HORA ) then
             ZetaDBGrid.SelectedField := FieldByName( 'FA_HORAS' )
          else }
             ZetaDBGrid.SelectedField := FieldByName( 'FA_FEC_INI' );
     end;
end;

function TGridDiasHoras_DevEx.PosicionaSiguienteColumna: Integer;
const
     K_COLUMNA_DIAS = 4;
     K_COLUMNA_HORA = 5;
begin
     if ( ZetaDBGrid.SelectedField.FieldName = 'FA_FEC_INI' ) and
        ( dmNomina.cdsExcepciones.FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_HORA ) then
        Result := K_COLUMNA_HORA
     else if ( ZetaDBGrid.SelectedField.FieldName = 'FA_DIAS' ) and
        ( dmNomina.cdsExcepciones.FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
        Result := PrimerColumna
     else
        Result := inherited PosicionaSiguienteColumna;
end;

procedure TGridDiasHoras_DevEx.KeyPress(var Key: Char);

procedure CambiaTipo( const sDiaHora: String; const iTipo: Integer );
begin
     with dmNomina.cdsExcepciones do
     begin
          if ( sDiaHora <> FieldByName( 'FA_DIA_HOR' ).AsString ) or
             ( iTipo <> FieldByName( 'FA_MOTIVO' ).AsInteger ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'FA_DIA_HOR' ).AsString := sDiaHora;
               FieldByName( 'FA_MOTIVO' ).AsInteger := iTipo;
          end;
          Key := #0;
          ZetaDBGrid.SetFocus;
{          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_HORA ) then
             ZetaDBGrid.SelectedField := FieldByName( 'FA_HORAS' )
          else }
              ZetaDBGrid.SelectedField := FieldByName( 'FA_FEC_INI' );
     end;
end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'FA_FEC_INI' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else
                   if ( ZetaDBGrid.SelectedField.FieldName = 'FA_MOTIVO' ) then
                   begin
                        case Key of
                             'O', 'o': CambiaTipo( K_TIPO_HORA, Ord( mfhOrdinarias ) );
                             'E', 'e': CambiaTipo( K_TIPO_HORA, Ord( mfhExtras ) );
                             'R', 'r': CambiaTipo( K_TIPO_HORA, Ord( mfhRetardo ) );
                             'L', 'l': CambiaTipo( K_TIPO_HORA, Ord( mfhAdicionales ) );
                             'D', 'd': CambiaTipo( K_TIPO_HORA, Ord( mfhDomingo ) );
                             '2'     : CambiaTipo( K_TIPO_HORA, Ord( mfhDobles ) );
                             '3'     : CambiaTipo( K_TIPO_HORA, Ord( mfhTriples ) );
                             'S', 's': CambiaTipo( K_TIPO_HORA, Ord( mfhFestivo ) );
                             'T', 't': CambiaTipo( K_TIPO_HORA, Ord( mfhDescanso ) );
                             'U', 'u': CambiaTipo( K_TIPO_HORA, Ord( mfhVacaciones ) );
                             'X', 'x': CambiaTipo( K_TIPO_HORA, Ord( mfhConGoce ) );
                             'Y', 'y': CambiaTipo( K_TIPO_HORA, Ord( mfhSinGoce ) );
                             'F', 'f': CambiaTipo( K_TIPO_DIA, Ord( mfdInjustificada ) );
                             'V', 'v': CambiaTipo( K_TIPO_DIA, Ord( mfdVacaciones ) );
                             'G', 'g': CambiaTipo( K_TIPO_DIA, Ord( mfdAguinaldo ) );
                             'N', 'n': CambiaTipo( K_TIPO_DIA, Ord( mfdNoTrabajados ) );
                             'A', 'a': CambiaTipo( K_TIPO_DIA, Ord( mfdAsistencia ) );
                             'Q', 'q': CambiaTipo( K_TIPO_DIA, Ord( mfdRetardo ) );
                             'M', 'm': CambiaTipo( K_TIPO_DIA, Ord( mfdIMSS ) );
                             'W', 'w': CambiaTipo( K_TIPO_DIA, Ord( mfdEM ) );
                             'H', 'h': CambiaTipo( K_TIPO_DIA, Ord( mfdAjuste ) );
                             'P', 'p': CambiaTipo( K_TIPO_DIA, Ord( mfdPrimaVacacional ) );
                        end;
                        Key := #0;
                   end;
          end;
     end
     else
         if ( ActiveControl = zFecha ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SetFocus;
                   if ( dmNomina.cdsExcepciones.FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_HORA ) then
                      SelectedField := dmNomina.cdsExcepciones.FieldByName( 'FA_HORAS' )
                   else
                      SelectedField := dmNomina.cdsExcepciones.FieldByName( 'FA_DIAS' );
              end;
         end;
end;

procedure TGridDiasHoras_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'FA_FEC_INI' ) then
          begin
               with zFecha do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    Width:= Column.Width + 2;
                    Visible := True;
               end;
          end;
     end;
end;

procedure TGridDiasHoras_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'FA_FEC_INI' ) and
             ( zFecha.Visible ) then
          begin
               zFecha.Visible := False;
          end;
     end;
end;

procedure TGridDiasHoras_DevEx.EscribirCambios;
begin
     dmNomina.OperacionConflicto := cbOperacion.ItemIndex;
     inherited EscribirCambios;
end;


end.
