inherited GridPagoRecibos_DevEx: TGridPagoRecibos_DevEx
  Left = 210
  Top = 88
  Caption = 'Registro de Recibos Pagados'
  ClientHeight = 313
  ClientWidth = 472
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 277
    Width = 472
    inherited OK_DevEx: TcxButton
      Left = 308
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 387
      Top = 5
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 472
    inherited ValorActivo2: TPanel
      Width = 146
      inherited textoValorActivo2: TLabel
        Width = 140
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 472
    Height = 230
    OnColExit = ZetaDBGridColExit
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 220
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'NO_FEC_PAG'
        Title.Caption = 'Pagado'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_USR_PAG'
        ReadOnly = True
        Title.Caption = 'Captur'#243
        Width = 65
        Visible = True
      end>
  end
  object ZFecha: TZetaDBFecha [4]
    Left = 280
    Top = 112
    Width = 105
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 4
    Text = '14/Oct/99'
    Valor = 36447.000000000000000000
    Visible = False
    DataField = 'NO_FEC_PAG'
    DataSource = DataSource
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
  end
end
