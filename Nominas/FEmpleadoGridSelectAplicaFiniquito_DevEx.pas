unit FEmpleadoGridSelectAplicaFiniquito_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
      StdCtrls,ExtCtrls, Db, ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
      cxLookAndFeelPainters, Menus, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
      dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
      cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
      cxGridTableView, cxGridDBTableView, ImgList, cxGridLevel, cxClasses,
      cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpleadoGridSelectAplicaFiniquito_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpleadoGridSelectAplicaFiniquito_DevEx: TEmpleadoGridSelectAplicaFiniquito_DevEx;

implementation

{$R *.DFM}

end.
