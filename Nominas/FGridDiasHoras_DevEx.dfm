inherited GridDiasHoras_DevEx: TGridDiasHoras_DevEx
  Left = 709
  Top = 179
  Caption = 'Excepciones de D'#237'as y Horas'
  ClientHeight = 307
  ClientWidth = 618
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 271
    Width = 618
    inherited OK_DevEx: TcxButton
      Left = 452
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 532
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 618
    inherited ValorActivo2: TPanel
      Width = 292
      inherited textoValorActivo2: TLabel
        Width = 286
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 618
    Height = 224
    OnColExit = ZetaDBGridColExit
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 210
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FA_MOTIVO'
        Title.Caption = 'Tipo'
        Width = 140
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'FA_FEC_INI'
        Title.Caption = 'Fecha'
        Width = 100
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'FA_DIAS'
        Title.Caption = 'D'#237'as'
        Width = 40
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'FA_HORAS'
        Title.Caption = 'Horas'
        Width = 40
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'Z_ERROR'
        Title.Caption = 'Error'
        Title.Color = clRed
        Visible = False
      end>
  end
  object ZFecha: TZetaDBFecha [4]
    Left = 408
    Top = 96
    Width = 105
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 4
    Text = '14/Oct/99'
    Valor = 36447.000000000000000000
    Visible = False
    DataField = 'FA_FEC_INI'
    DataSource = DataSource
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
    inherited BarSuperior: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarControlContainerItem_DBNavigator'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_AgregarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BorrarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ModificarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BuscarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_ImprimirBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ImprimirFormaBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ExportarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_CortarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_CopiarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_PegarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_UndoBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 139
          Visible = True
          ItemName = 'cbOperacion'
        end>
    end
    object cbOperacion: TdxBarCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      ShowEditor = False
      Items.Strings = (
        'Reportar Error'
        'Dejar Anteriores'
        'Sumar D'#237'as/Horas'
        'Sustituir D'#237'as/Horas')
      ItemIndex = -1
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Si ya Existe:'
      Category = 0
      Hint = 'Si ya Existe:'
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
  end
end
