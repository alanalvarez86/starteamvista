unit FNomTotales_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, StdCtrls, ExtCtrls, DBCtrls, Mask,
     {$ifndef VER130}Variants,{$endif}     
     ZBaseConsulta,
     ZetaDBTextBox, ZetaKeyCombo, FTressShell, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     cxGroupBox, cxTextEdit, cxMemo, cxDBEdit;

type
  TNomTotales_DevEx = class(TBaseConsulta)
    Panel2: TPanel;
    pnlNiveles: TPanel;
    Niveles: TZetaKeyCombo;
    Label22: TLabel;
    Panel3: TPanel;
    GroupBox2: TcxGroupBox;
    PE_LOG: TcxDBMemo;
    GroupBox3: TcxGroupBox;
    Label5: TLabel;
    PE_POS_MES: TZetaDBTextBox;
    Label15: TLabel;
    PE_PER_MES: TZetaDBTextBox;
    Label6: TLabel;
    PE_PER_TOT: TZetaDBTextBox;
    Label1: TLabel;
    Label16: TLabel;
    PE_NUMERO: TZetaDBTextBox;
    ZetaDBTextBox1: TZetaDBTextBox;
    GroupBox4: TcxGroupBox;
    Label19: TLabel;
    PE_DIAS: TZetaDBTextBox;
    Label20: TLabel;
    PE_DIAS_AC: TZetaDBTextBox;
    Label2: TLabel;
    PE_DIA_MES: TZetaDBTextBox;
    GroupBox1: TcxGroupBox;
    Label11: TLabel;
    PE_NUM_EMP: TZetaDBTextBox;
    Label12: TLabel;
    PE_TOT_PER: TZetaDBTextBox;
    Label13: TLabel;
    PE_TOT_DED: TZetaDBTextBox;
    Label14: TLabel;
    PE_TOT_NET: TZetaDBTextBox;
    GroupBox5: TcxGroupBox;
    PE_AHORRO: TDBCheckBox;
    PE_PRESTAM: TDBCheckBox;
    PE_SOLO_EX: TDBCheckBox;
    PE_INC_BAJ: TDBCheckBox;
    Panel1: TPanel;
    PE_FEC_PAG: TZetaDBTextBox;
    Label18: TLabel;
    Label3: TLabel;
    PE_DESCRIP: TZetaDBTextBox;
    Label10: TLabel;
    PE_FEC_MOD: TZetaDBTextBox;
    Label9: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label4: TLabel;
    PE_STATUS: TZetaDBTextBox;
    Label7: TLabel;
    PE_FEC_FIN: TZetaDBTextBox;
    Label8: TLabel;
    PE_FEC_INI: TZetaDBTextBox;
    UsoLbl: TLabel;
    PE_USO: TZetaDBTextBox;
    Label17: TLabel;
    PE_ASI_INI: TZetaDBTextBox;
    Label21: TLabel;
    PE_ASI_FIN: TZetaDBTextBox;
    StatusTimbLbl: TLabel;
    PE_TIMBRO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure NivelesChange(Sender: TObject);
  private
    procedure FillComboNiveles;

    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  NomTotales_DevEx: TNomTotales_DevEx;

implementation

uses DNomina, DCatalogos, DSistema, DCliente,
     ZetaCommonLists, ZetaCommonClasses, ZAccesosTress, ZetaClientDataSet;

{$R *.DFM}

{ TNomTotales }

procedure TNomTotales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo2 := stPeriodo;
     HelpContext := H30311_Totales_nomina;
     {$ifdef PRESUPUESTOS}

             //DevEx(by Angie): Indicamos que no sean visibles los componentes de timbrado y reasignamos la posici�n de todos los dem�s.
             //DevEx(by Angie): Cambios en Panel1

             Panel1.Height := 111;

             StatusTimbLbl.Visible := False;
             UsoLbl.Parent := Panel1;
             UsoLbl.Top := 71;

             PE_TIMBRO.Visible := False;
             PE_USO.Parent := Panel1;
             PE_USO.Top := 69;

             Label3.Parent := Panel1;
             Label3.Top := 91;

             PE_DESCRIP.Parent := Panel1;
             PE_DESCRIP.Top := 89;

             //DevEx(by Angie): Cambios en GroupBoxes

             GroupBox1.Top := 113;
             GroupBox5.Top := 113;
             GroupBox4.Top := 217;
             GroupBox3.Top := 217;
             GroupBox2.Top := 297;

     {$endif}
end;

procedure TNomTotales_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos.cdsPeriodo do
     begin
          if ( not Active ) or IsEmpty or ( FieldByName( 'PE_TIPO' ).AsInteger <> Ord( dmCliente.PeriodoTipo ) ) then
             Refrescar;                     // Para obligar a cambiar la lista de periodos a los activos
     end;

     with dmSistema do
     begin
          cdsNivel0.Conectar;
          FillComboNiveles;
          if Niveles.Items.Count = 1 then
          begin
               pnlNiveles.Visible := False;
               dmNomina.NivelConfidencialidad := VACIO
          end
          else
              if dmCliente.Confidencialidad = VACIO then
              begin
                   Niveles.ItemIndex := 0;
                   dmNomina.NivelConfidencialidad := VACIO;
              end
              else //Tiene un nivel de confidencialidad
              begin
                   Niveles.ItemIndex  := Niveles.Items.IndexOfName( dmCliente.ConfidencialidadDefault );
                   dmNomina.NivelConfidencialidad := dmCliente.ConfidencialidadDefault;
                   Niveles.Enabled := False;
              end;

     end;

     with dmNomina do
     begin
          cdsTotales.Conectar;
          DataSource.DataSet := cdsTotales;
     end;


end;

procedure TNomTotales_DevEx.FillComboNiveles;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          Niveles.Items.Clear;
          Niveles.Items.Add('< Todos >');
          Niveles.Items.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               Niveles.Items.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
          Niveles.ItemIndex := 0;
     end;
end;

procedure TNomTotales_DevEx.Refresh;
begin
     with dmNomina do
     begin
          cdsTotales.Refrescar;
     end;
end;

function TNomTotales_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
var
   OldDerechos : Integer;
begin
     OldDerechos := IndexDerechos;
     try
        IndexDerechos := D_CAT_NOMINA_PERIODOS;          // Toma Derechos de Catalogo de Periodos
        Result := inherited PuedeAgregar( sMensaje );
     finally
        IndexDerechos := OldDerechos;
     end;
end;

procedure TNomTotales_DevEx.Agregar;
begin
     dmCatalogos.cdsPeriodo.Agregar;
end;

function TNomTotales_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
var
   OldDerechos : Integer;
begin
     OldDerechos := IndexDerechos;
     try
        IndexDerechos := D_CAT_NOMINA_PERIODOS;          // Toma Derechos de Catalogo de Periodos
        Result := inherited PuedeBorrar( sMensaje );
     finally
        IndexDerechos := OldDerechos;
     end;
end;

procedure TNomTotales_DevEx.Borrar;
begin
     dmNomina.BorrarNominaActiva ( True );
     // PENDIENTE: hay que decidir si se usa BaseEdicion para el di�logo de borrado.
     // PENDIENTE: decidir la nueva manera de borrar.
end;

function TNomTotales_DevEx.PuedeModificar(var sMensaje: String): Boolean;
var
   OldDerechos : Integer;
begin
     OldDerechos := IndexDerechos;
     try
        IndexDerechos := D_CAT_NOMINA_PERIODOS;          // Toma Derechos de Catalogo de Periodos
        Result := inherited PuedeModificar( sMensaje );
     finally
        IndexDerechos := OldDerechos;
     end;
end;

procedure TNomTotales_DevEx.Modificar;
begin
     with dmCatalogos.cdsPeriodo do
     begin
          Locate( 'PE_NUMERO;PE_TIPO;PE_YEAR',
                  VarArrayOf( [ dmNomina.cdsTotales.FieldByName( 'PE_NUMERO' ).AsInteger,
                                dmNomina.cdsTotales.FieldByName( 'PE_TIPO' ).AsInteger,
                                dmNomina.cdsTotales.FieldByName( 'PE_YEAR' ).AsInteger ] ),
                  [ loCaseInsensitive ] );

          dmCatalogos.ModificaEdicionTotalesNomina(True);
     end;
     // PENDIENTE: investigar si hay alguna manera de condicionar el Refresca, ya que
     // en este momento ChangeCount vale 0
     dmNomina.cdsTotales.Refrescar;
end;

procedure TNomTotales_DevEx.NivelesChange(Sender: TObject);
begin
     inherited;
     with dmNomina do
     begin
          with Niveles.Items do
          begin
               NivelConfidencialidad := Names[  Niveles.ItemIndex ];
               cdsTotales.Refrescar;
          end;
     end;
end;

end.
