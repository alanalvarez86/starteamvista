unit FNomExcepciones_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, Db, DBGrids, ExtCtrls,StdCtrls, Buttons,
     ZBaseConsulta,
     ZetaDBGrid, Mask, ZetaFecha, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, Menus, cxTextEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxButtons, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;
type
  TNomExcepciones_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    BtnModificarTodos: TcxButton;
    ZetaDBGridDBTableView1CB_CODIGO: TcxGridDBColumn;
    ZetaDBGridDBTableView1PRETTYNAME: TcxGridDBColumn;
    ZetaDBGridDBTableView1FA_FEC_INI: TcxGridDBColumn;
    ZetaDBGridDBTableView1FA_MOTIVO: TcxGridDBColumn;
    ZetaDBGridDBTableView1fa_dias: TcxGridDBColumn;
    ZetaDBGridDBTableView1fa_horas: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure BtnModificarTodosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeModificarTodos(var sMensaje: String): Boolean;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  NomExcepciones_DevEx: TNomExcepciones_DevEx;

implementation

uses DNomina,
     DCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo, FTressShell;

{$R *.DFM}

{ TNomExcepciones }

procedure TNomExcepciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H30321_Dias_horas;
     TipoValorActivo2 := stPeriodo;
end;

procedure TNomExcepciones_DevEx.Connect;
begin
     with dmNomina do
     begin
          cdsExcepciones.Conectar;
          DataSource.DataSet:= cdsExcepciones;
     end;
end;

procedure TNomExcepciones_DevEx.Refresh;
begin
     with dmNomina do
     begin
          cdsExcepciones.Refrescar;
     end;
end;

procedure TNomExcepciones_DevEx.Agregar;
begin
     dmNomina.cdsExcepciones.Agregar;
end;

procedure TNomExcepciones_DevEx.Borrar;
begin
     dmNomina.cdsExcepciones.Borrar;
end;

procedure TNomExcepciones_DevEx.Modificar;
begin
     dmNomina.cdsExcepciones.Modificar;
end;

procedure TNomExcepciones_DevEx.BtnModificarTodosClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificarTodos( sMensaje ) then
        dmNomina.EditarGridDiasHoras( False )
     else
        zInformation( Caption, sMensaje, 0 );
end;

function TNomExcepciones_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Agregar', True );
     end;
     if Result then
     begin
          if not ( ZetaDbGridDBTableView.DataController.DataSet.Eof ) then
          begin
               Result := dmNomina.ValidaStatusEmpleado( ZetaDbGridDBTableView.DataController.DataSet.FieldByName('CB_CODIGO').Value , sMensaje );
          end;
     end;
end;

function TNomExcepciones_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Borrar', True );
     end;
     if Result then
     begin
          if not ( ZetaDbGridDBTableView.DataController.DataSet.Eof ) then
          begin
               Result := dmNomina.ValidaStatusEmpleado( ZetaDbGridDBTableView.DataController.DataSet.FieldByName('CB_CODIGO').Value , sMensaje );
          end;
     end;
end;

function TNomExcepciones_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar', True );
     end;
     if Result then
     begin
          if not ( ZetaDbGridDBTableView.DataController.DataSet.Eof ) then
          begin
               Result := dmNomina.ValidaStatusEmpleado( ZetaDbGridDBTableView.DataController.DataSet.FieldByName('CB_CODIGO').Value , sMensaje );
          end;
     end;
end;

function TNomExcepciones_DevEx.PuedeModificarTodos(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar', True );
     end;
end;

procedure TNomExcepciones_DevEx.FormShow(Sender: TObject);
begin
   CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'', SkCount );
  inherited;
  
end;

end.
