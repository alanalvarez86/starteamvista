unit FMontoRepetido_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ZetaDBTextBox, ZetaCommonLists, Mask, ZetaNumero,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxButtons, dxGDIPlusClasses;

type

  TMontoRepetido_DevEx = class(TForm)
    Mensaje: TLabel;
    Image1: TImage;
    IgnoreBtn: TcxButton;
    ReplaceBtn: TcxButton;
    SumarBtn: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    EmpleadoLb: TLabel;
    ConceptoLb: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    AnteriorNu: TZetaNumero;
    NuevoNu: TZetaNumero;
    Label6: TLabel;
    ReferenciaLb: TLabel;
    lblMensaje: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    function ShowDlgMontoRepetido( const iEmpleado, iConcepto: Integer;
             const sReferencia: String; const Anterior, Nuevo: Real ): eOperacionConflicto;

var
  MontoRepetido_DevEx: TMontoRepetido_DevEx;


implementation

uses
    DNomina,
    ZAccesosMgr,
    ZAccesosTress,
    ZetaCommonClasses,
    DCliente;

{$R *.DFM}

procedure TMontoRepetido_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext:= 0;
end;

function ShowDlgMontoRepetido( const iEmpleado, iConcepto: Integer;
         const sReferencia: String; const Anterior, Nuevo: Real ): eOperacionConflicto;
var
   PasoLimite:Boolean;
begin

     if ( MontoRepetido_DevEx = nil ) then
     begin
          MontoRepetido_DevEx := TMontoRepetido_DevEx.Create( Application.MainForm ); // Se destruye al Salir del Programa //
     end;

     with MontoRepetido_DevEx do
     begin
          EmpleadoLb.Caption:= IntToStr(iEmpleado);
          ConceptoLb.Caption:= IntToStr(iConcepto);
          ReferenciaLb.Caption:= sReferencia;
          AnteriorNu.Valor:= Anterior;
          NuevoNu.Valor:= Nuevo;

          PasoLimite := dmNomina.PasaLimiteMonto(iConcepto, Anterior + Nuevo );
          SumarBtn.Enabled := (not PasoLimite ) or Revisa( D_NOM_EXCEPCIONES_LIM_MONTO) or  (dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION) ;

          if (PasoLimite)then
             lblMensaje.Caption := 'Al sumar el resultado no esta en el l�mite permitido para el concepto'
          else
              lblMensaje.Caption := '';
          ShowModal;
          case ModalResult of
               mrOk:  Result:= ocSustituir;
               mrYes: Result:= ocSumar;
          else Result:= ocIgnorar;
          end;
     end;
end;

procedure TMontoRepetido_DevEx.FormShow(Sender: TObject);
begin
     IgnoreBtn.SetFocus;
end;

end.
