unit FSimulaMontosGlobal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, ComCtrls, StdCtrls, ZetaKeyCombo,
  Buttons,ZetaCommonClasses,ZetaCommonLists, Grids, DBGrids, ZetaDBGrid,
  DBCtrls, ZetaDBTextBox, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxTextEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxPCdxBarPopupMenu, cxContainer, cxGroupBox, cxPC, Menus,
  cxButtons, dxBarBuiltInMenu;

type
  TSimulaMontosGlobal_DevEx = class(TBaseConsulta)
    Panel: TPanel;
    Label1: TLabel;
    FiltroStatus: TZetaKeyCombo;
    PageControl1: TcxPageControl;
    Panel1: TPanel;
    TabSheet1: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    BtnLiquidacion: TcxButton;
    bbAprobarFiniquito: TcxButton;
    BtnAplicarFiniquito: TcxButton;
    bbBorrarFiniquito: TcxButton;
    dsMontos: TDataSource;
    PageControl2: TcxPageControl;
    TabSheet3: TcxTabSheet;
    GroupBox4: TcxGroupBox;
    Label2: TLabel;
    NO_PERCEPC: TZetaDBTextBox;
    Label3: TLabel;
    NO_DEDUCCI: TZetaDBTextBox;
    NO_NETO: TZetaDBTextBox;
    Label4: TLabel;
    TabSheet4: TcxTabSheet;
    GroupBox1: TcxGroupBox;
    Label6: TLabel;
    NO_PER_MEN: TZetaDBTextBox;
    Label9: TLabel;
    NO_PER_CAL: TZetaDBTextBox;
    Label5: TLabel;
    NO_TOT_PRE: TZetaDBTextBox;
    GroupBox2: TcxGroupBox;
    Label7: TLabel;
    NO_X_ISPT: TZetaDBTextBox;
    Label10: TLabel;
    NO_X_CAL: TZetaDBTextBox;
    Label8: TLabel;
    NO_X_MENS: TZetaDBTextBox;
    Label11: TLabel;
    NO_IMP_CAL: TZetaDBTextBox;
    TabDias: TcxTabSheet;
    Label17: TLabel;
    Label18: TLabel;
    Label22: TLabel;
    Label24: TLabel;
    NO_DIAS: TZetaDBTextBox;
    NO_DIAS_AS: TZetaDBTextBox;
    NO_DIAS_NT: TZetaDBTextBox;
    NO_DIAS_SU: TZetaDBTextBox;
    Label15: TLabel;
    Label23: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    NO_DIAS_IN: TZetaDBTextBox;
    NO_DIAS_SG: TZetaDBTextBox;
    NO_DIAS_CG: TZetaDBTextBox;
    NO_DIAS_OT: TZetaDBTextBox;
    Label29: TLabel;
    Label30: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    NO_DIAS_AJ: TZetaDBTextBox;
    NO_DIAS_RE: TZetaDBTextBox;
    NO_DIAS_SS: TZetaDBTextBox;
    NO_DIAS_EM: TZetaDBTextBox;
    Label16: TLabel;
    Label31: TLabel;
    Label38: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    NO_DIAS_FJ: TZetaDBTextBox;
    NO_DIAS_FV: TZetaDBTextBox;
    NO_DIAS_VA: TZetaDBTextBox;
    NO_DIAS_AG: TZetaDBTextBox;
    TabHoras: TcxTabSheet;
    Label35: TLabel;
    NO_HORAS: TZetaDBTextBox;
    Label36: TLabel;
    NO_Hora_PD: TZetaDBTextBox;
    NO_DES_TRA: TZetaDBTextBox;
    Label37: TLabel;
    NO_EXTRAS: TZetaDBTextBox;
    Label39: TLabel;
    Label40: TLabel;
    NO_TRIPLES: TZetaDBTextBox;
    Label41: TLabel;
    NO_FES_TRA: TZetaDBTextBox;
    NO_VAC_TRA: TZetaDBTextBox;
    Label42: TLabel;
    NO_DOBLES: TZetaDBTextBox;
    Label43: TLabel;
    Label44: TLabel;
    NO_TARDES: TZetaDBTextBox;
    Label45: TLabel;
    NO_HORA_CG: TZetaDBTextBox;
    NO_HORA_SG: TZetaDBTextBox;
    Label46: TLabel;
    NO_ADICION: TZetaDBTextBox;
    Label47: TLabel;
    Label48: TLabel;
    NO_FES_PAG: TZetaDBTextBox;
    Splitter1: TSplitter;
    dsMovimientos: TDataSource;
    bbCalcularNomina: TcxButton;
    Label12: TLabel;
    PER_ISN_SUM: TZetaDBTextBox;
    ZetaCXGrid1DBTableView: TcxGridDBTableView;
    ZetaCXGrid1Level1: TcxGridLevel;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid2DBTableView: TcxGridDBTableView;
    ZetaCXGrid2Level1: TcxGridLevel;
    ZetaCXGrid2: TZetaCXGrid;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_ACTIVO: TcxGridDBColumn;
    CB_FEC_ANT: TcxGridDBColumn;
    CB_SALARIO: TcxGridDBColumn;
    NO_PERCEPC_GRID: TcxGridDBColumn;
    NO_DEDUCCI_GRID: TcxGridDBColumn;
    NO_NETO_GRID: TcxGridDBColumn;
    TP_NOMBRE: TcxGridDBColumn;
    NO_STATUS: TcxGridDBColumn;
    NO_APROBA: TcxGridDBColumn;
    NO_FEC_LIQ: TcxGridDBColumn;
    co_numero: TcxGridDBColumn;
    CO_DESCRIP: TcxGridDBColumn;
    PERCEPCIONES: TcxGridDBColumn;
    DEDUCCIONES: TcxGridDBColumn;
    CO_TIPO: TcxGridDBColumn;
    IMPUESTOS_ISPT: TcxGridDBColumn;
    IMPUESTOS_CAL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FiltroStatusChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGrid1DblClick(Sender: TObject);
    procedure BtnLiquidacionClick(Sender: TObject);
    procedure bbAprobarFiniquitoClick(Sender: TObject);
    procedure BtnAplicarFiniquitoClick(Sender: TObject);
    procedure bbBorrarFiniquitoClick(Sender: TObject);
    procedure bbCalcularNominaClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure ZetaCXGrid2DBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaCXGrid1DBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaCXGrid1DBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ZetaCXGrid2DBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ZetaCXGrid1DBTableViewDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure ZetaCXGrid2DBTableViewDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure ZetaCXGrid1DBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaCXGrid2DBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
  private
    AColumn: TcxGridDBColumn;
    //FiltroActual :string;
    procedure AplicarFiltro;
    {procedure ActivaFiltro;
    procedure DesactivaFiltro;}
    procedure SetBotones;
    { Private declarations }
  public
    { Public declarations }
    procedure LlenaListas( oLista: TStrings; oLFijas: ListasFijas );
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Modificar; override;
    procedure Connect;override;
    procedure Refresh;override;
    procedure Agregar; override;
    procedure Borrar;override;
    procedure CreaColumaSumatoria(Columna:TcxGridDBColumn;
              TipoFormato: Integer ; TextoSumatoria: String ;
              FooterKind : tcxSummaryKind; GridTableView : TcxGridDBTableView );
    procedure ApplyMinWidth( ZetaDBGridDBTableView: TcxGridDBTableView);

  end;


var
  SimulaMontosGlobal_DevEx: TSimulaMontosGlobal_DevEx;
const
     {***Formastos para las sumatorias***}
     K_SIN_TIPO = 0;
     K_TIPO_MONEDA = 1;

     K_ESPACIO = ' ';
     K_FORMATO_MONEDA = ',0.00;-,0.00'; 
     K_FORMATO_DEFAULT ='0';

implementation

uses
     dGlobal,ZAccesosMgr,ZAccesosTress,ZGlobalTress,dNomina,DCliente, FTressShell,ZetaDespConsulta,
     FWizNomSimLiquidacionGlobal_DevEx,
     ZetaDialogo, ZcxWizardBasico, FGridAprobarFiniquitos_DevEx, ZBaseGridEdicion_DevEx,
     FWizNomSimAplicarFinGlobal_DevEx, FWizNomCalcularSimulacion_DevEx, DProcesos, ZetaSystemWorking,
     ZetaClientDataSet, ZGridModeTools;

{$R *.dfm}

procedure TSimulaMontosGlobal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
     ZetaCXGrid1DBTableView.DataController.DataModeController.GridMode:= True;
     ZetaCXGrid1DBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
	   ZetaCXGrid2DBTableView.DataController.DataModeController.GridMode:= True;
     ZetaCXGrid2DBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     {***}
     HelpContext := HHHHH_Simulacion_Finiquitos_Global;
     with FiltroStatus do
     begin
          LlenaListas( Lista, lfStatusSimFiniquitos );
          //Lista.Insert(0,'Todos');
          ItemIndex := 0;
     end;
end;

procedure TSimulaMontosGlobal_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth(ZetaCXGrid1DBTableView);
     ApplyMinWidth(ZetaCXGrid2DBTableView);                              //DevEx
     inherited;
     FiltroStatus.ItemIndex := 0;

     AplicarFiltro;
     PageControl1.ActivePageIndex  := 0;
     PageControl2.ActivePageIndex  := 0;
     SetBotones;

     {***CONFIGURACION GRID***}
     {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
     //Desactiva la opcion de filtrar por columna
     //ZetaCXGrid1DBTableView.OptionsCustomize.ColumnFiltering := False;
     //ZetaCXGrid2DBTableView.OptionsCustomize.ColumnFiltering := False;
     //Desactiva la posibilidad de agrupar
     ZetaCXGrid1DBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaCXGrid2DBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     ZetaCXGrid1DBTableView.OptionsView.GroupByBox := True;
     ZetaCXGrid2DBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     ZetaCXGrid1DBTableView.FilterBox.Visible := fvNever;
     ZetaCXGrid2DBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaCXGrid1DBTableView.FilterBox.CustomizeDialog := False;
     ZetaCXGrid2DBTableView.FilterBox.CustomizeDialog := False;

     //DevEx
     {***(@am): Se comenta la sumatoria porque no trabaja en GridMode***}
     //CreaColumaSumatoria( ZetaCXGrid1DBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount, ZetaCXGrid1DBTableView);
     ZetaCXGrid1DBTableView.ApplyBestFit();
     ZetaCXGrid2DBTableView.ApplyBestFit();


     //ZGridModeTools.CreaColumnaSumatoria( ZetaCXGrid2DBTableView.Columns[0], 0, 'Total:', SkCount );
     ZGridModeTools.CreaColumnaSumatoria( ZetaCXGrid1DBTableView.Columns[0], 0, 'Total:', SkCount );
end;

procedure TSimulaMontosGlobal_DevEx.SetBotones;
begin
     BtnLiquidacion.Enabled := CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_LIQUID_GLOBAL );
     bbBorrarFiniquito.Enabled := ( ( not DataSource.DataSet.IsEmpty ) and ( CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_BORRAR_GLOBAL) ) );
     BtnAplicarFiniquito.Enabled := ( ( not DataSource.DataSet.IsEmpty) and ( CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_APLICA_GLOBAL) )) ;
     bbAprobarFiniquito.Enabled := ( ( not DataSource.DataSet.IsEmpty ) and ( Global.GetGlobalBooleano ( K_GLOBAL_SIM_FINIQ_APROBACION ) ) and ( CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_APROBA_GLOBAL) ) );
     bbCalcularNomina.Enabled := not DataSource.DataSet.IsEmpty;
end;

procedure TSimulaMontosGlobal_DevEx.Connect;
begin
     inherited;

     with dmNomina do
     begin
          FiltroSimulacion := -1;
          DataSource.DataSet := cdsSimulaGlobales;
          dsMovimientos.DataSet := cdsSimGlobalesMovimien;
          dsMontos.DataSet := cdsSimGlobalTotales;

          cdsSimulaGlobales.Conectar;
          cdsSimGlobalTotales.Conectar;
     end;
     
end;

function TSimulaMontosGlobal_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_LIQUID_GLOBAL  );
     sMensaje := 'No tiene permisos para Agregar';
end;

function TSimulaMontosGlobal_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_BORRAR_GLOBAL );
     sMensaje := 'No tiene permisos para Borrar ';
end;

function TSimulaMontosGlobal_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := CheckDerecho(D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_CONSULTA );
     sMensaje := 'No tiene derechos para Consultar la Simulaci�n de Finiquitos Por Empleado';
end;

function TSimulaMontosGlobal_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin  //Correci�n de Defecto 1584: Imprimir el grid de Simulacion de finiquitos se requerie el derecho de Aplicar Finiquitos
     Result := CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_IMPRESION_GLOBAL );
     sMensaje := 'No tiene derechos para Imprimir la Simulaci�n de Finiquitos Por Empleado';
end;

procedure TSimulaMontosGlobal_DevEx.Modificar;
begin
     inherited;
     if not DataSource.DataSet.IsEmpty  then
     begin
          with dmNomina do
          begin
               dmCliente.SetEmpleadoNumero( cdsSimulaGlobales.FieldByName('CB_CODIGO').AsInteger);
               dmCliente.PeriodoTipo := eTipoPeriodo( cdsSimulaGlobales.FieldByName('PE_TIPO').AsInteger);
               TressShell.RefrescaEmpleado;
          end;
          TressShell.AbreFormaSimulaciones(1);//Abre la forma de Simulacion Individual
     end;

end;

procedure TSimulaMontosGlobal_DevEx.LlenaListas( oLista: TStrings; oLFijas: ListasFijas );
var
   eValueStatus: eStatusSimFiniquitos;
   i,j: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
          with FTemp do
          //with oLista do
          begin
               BeginUpdate;
               try
                  if dmNomina.DerechoSoloAplicarGlobal then
                  begin
                       AddObject( ObtieneElemento( oLFijas, Ord( ssfAprobada ) ), TObject( Ord( ssfAprobada ) ) );
                       AddObject( ObtieneElemento( oLFijas, Ord( ssfLiquidada ) ), TObject( Ord( ssfLiquidada ) ) );
                  end
                  else
                  begin
                       for eValueStatus := ( Low( eStatusSimFiniquitos ) ) to High( eStatusSimFiniquitos ) do
                       begin
                            AddObject( ObtieneElemento( oLFijas, Ord( eValueStatus ) ), TObject( Ord( eValueStatus ) ) );
                       end;
                  end;
               finally
                      EndUpdate;
               end;
          end;
          with oLista do
          begin
               BeginUpdate;
               try
                  Clear;
                  if dmNomina.DerechoSoloAplicarGlobal then j := 1
                  else j :=0;
                  
                  for i := 0 to ( FTemp.Count - j ) do
                  begin
                       if ( i = 0 ) and (NOT dmNomina.DerechoSoloAplicarGlobal) then
                           Add( '0=Todos' )
                       else
                       begin
                            if dmNomina.DerechoSoloAplicarGlobal then
                               AddObject( IntToStr( ( Integer( FTemp.Objects[ i ] ) ) + 1 ) + '=' + FTemp.Strings[ i ],FTemp.Objects[ i ] )
                            else
                                AddObject( IntToStr( ( Integer( FTemp.Objects[ i - 1 ] ) ) + 1 ) + '=' + FTemp.Strings[ i - 1 ],FTemp.Objects[ i - 1 ] );
                       end;
                  end;
               finally
                      EndUpdate;
               end;
          end;  
     finally
            FreeAndNil( FTemp );
     end;
end;

procedure TSimulaMontosGlobal_DevEx.AplicarFiltro;
const
     K_FILTRO = 'NO_APROBA = %d';
var
     sFiltro: string;
begin
     with dmNomina.cdsSimulaGlobales, FiltroStatus do
     begin
          if NOT dmNomina.DerechoSoloAplicarGlobal then
          begin
               if ItemIndex  = 0 then
               begin
                    Filtered := False;
               end
               else
               begin
                    Filtered := False;
                    case ItemIndex  of
                         3:
                         begin
                              sFiltro := Format (K_FILTRO + ' AND NO_STATUS>=4 AND CB_ACTIVO =''N''',[ ItemIndex -1 ]);
                         end
                         else
                         begin
                              sFiltro := Format (K_FILTRO,[ ItemIndex - 1 ]);
                         end;
                    end;
                    Filter := sFiltro;
                    Filtered := True;
               end;
          end
          else
          begin
               sFiltro := Format (K_FILTRO + ' AND NO_STATUS>=4 AND CB_ACTIVO =''N''',[ ItemIndex +1 ]);
               {
               case ItemIndex  of
                    1:
                    begin
                         sFiltro := Format (K_FILTRO + ' AND NO_STATUS>=4 AND CB_ACTIVO =''N''',[ ItemIndex +1 ]);
                    end
                    else
                    begin
                         sFiltro := Format (K_FILTRO,[ ItemIndex +1 ]);
                    end;
               end;          }
               Filter := sFiltro;
               Filtered := True;
          end;
     end;
end;

procedure TSimulaMontosGlobal_DevEx.FiltroStatusChange(Sender: TObject);
begin
     inherited;
     with FiltroStatus do
          if dmNomina.DerechoSoloAplicarGlobal then
             dmNomina.FiltroSimulacion := ItemIndex + 1
          else
              dmNomina.FiltroSimulacion := ItemIndex -1;
     Refresh;
end;



procedure TSimulaMontosGlobal_DevEx.Refresh;
begin
     inherited;
     with dmNomina do
     begin
          cdsSimulaGlobales.Refrescar;
          cdsSimGlobalTotales.Refrescar;
          cdsSimGlobalesMovimien.Refrescar;
     end;
     AplicarFiltro;
     ZetaCXGrid2DBTableView.ApplyBestFit();
     
end;

procedure TSimulaMontosGlobal_DevEx.ZetaCXGrid1DBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TSimulaMontosGlobal_DevEx.ZetaCXGrid1DBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
  inherited;
ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaCXGrid1DBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaCXGrid1DBTableView, AItemIndex, AValueList );
end;

procedure TSimulaMontosGlobal_DevEx.ZetaCXGrid1DBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  ZetaCXGrid1.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
end;

procedure TSimulaMontosGlobal_DevEx.ZetaCXGrid1DBTableViewDataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
  inherited;
     if ZetaCXGrid1DBTableView.DataController.IsGridMode and ZetaCXGrid1DBTableView.OptionsView.Footer then      // Hay columnas totalizadas
        ZGridModeTools.AsignaValorColumnaSummary( ASender );
end;

procedure TSimulaMontosGlobal_DevEx.ZetaCXGrid2DBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
   {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TSimulaMontosGlobal_DevEx.ZetaCXGrid2DBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
  inherited;
ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaCXGrid2DBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaCXGrid2DBTableView, AItemIndex, AValueList );
end;

procedure TSimulaMontosGlobal_DevEx.ZetaCXGrid2DBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
   {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  ZetaCXGrid2.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
end;

procedure TSimulaMontosGlobal_DevEx.ZetaCXGrid2DBTableViewDataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
  inherited;
     if ZetaCXGrid2DBTableView.DataController.IsGridMode and ZetaCXGrid2DBTableView.OptionsView.Footer then      // Hay columnas totalizadas
        ZGridModeTools.AsignaValorColumnaSummary( ASender );
end;

procedure TSimulaMontosGlobal_DevEx.ZetaDBGrid1DblClick(Sender: TObject);
begin
     inherited;
     //Cargar la forma
     DoEdit;
end;

procedure TSimulaMontosGlobal_DevEx.BtnLiquidacionClick(Sender: TObject);
begin
     inherited;
     if ZcxWizardBasico.ShowWizard(TLiquidacionGlobal_DevEx) then
        //Refresh;
end;

procedure TSimulaMontosGlobal_DevEx.bbAprobarFiniquitoClick(Sender: TObject);
begin
     inherited;
     with dmNomina.cdsGridAprobados do
     begin
          Refrescar;
          if RecordCount > 0 then
          begin
             ZBaseGridEdicion_DevEx.ShowGridEdicion( GridAprobarFiniquitos_DevEx, TGridAprobarFiniquitos_DevEx, False );

          end
          else
              ZInformation(Self.Caption,'No existen Finiquitos a Aprobar',0);
     end;
     Refresh;
end;

procedure TSimulaMontosGlobal_DevEx.BtnAplicarFiniquitoClick(Sender: TObject);
begin
     inherited;
     if not dmNomina.cdsSimulaGlobales.IsEmpty  then
     begin
          ZcxWizardBasico.ShowWizard(TWizNomSimAplicarFinGlobal_DevEx);
          Refresh;
     end
     else
         ZInformation(Self.Caption,'No existen Finiquitos a Aplicar',0);
end;

procedure TSimulaMontosGlobal_DevEx.bbBorrarFiniquitoClick(Sender: TObject);
begin
     inherited;
     if ZConfirm(Self.Caption,'� Desea Borrar Todas las Simulaciones Globales ?',0,mbYes) then
     begin
          dmNomina.BorrarFiniquitosGlobales;
          Refresh;
     end;
     ZetaCXGrid1DBTableView.ApplyBestFit();
     ZetaCXGrid2DBTableView.ApplyBestFit();
end;

procedure TSimulaMontosGlobal_DevEx.bbCalcularNominaClick(Sender: TObject);
begin
     {if ZetaDialogo.ZConfirm( Caption, 'Se calcular� la n�mina de todos los empleados con Simulaci�n Global.' + CR_LF + '�Desea continuar?',
                              0,  mbNo) then
     begin
          dmProcesos.CalcularSimulacionGlobal( Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
          TressShell.ReconectaMenu;
     end;}
     ZcxWizardBasico.ShowWizard(TWizNomCalcularSimulacion_DevEx) ;
end;


{procedure TSimulaMontosGlobal.DesactivaFiltro;
begin
     with dmNomina.cdsSimulaGlobales do
     begin
          FiltroActual := Filter;
          Filtered := False;
     end;
end;

procedure TSimulaMontosGlobal.ActivaFiltro;
begin
     with dmNomina.cdsSimulaGlobales do
     begin
          Filter := FiltroActual;
          Filtered := True;
     end;
end;}


procedure TSimulaMontosGlobal_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     SetBotones;
end;

procedure TSimulaMontosGlobal_DevEx.Agregar;
begin
     inherited;
     BtnLiquidacionClick(Self); 
end;

procedure TSimulaMontosGlobal_DevEx.Borrar;
begin
     inherited;
     if ZConfirm(Self.Caption,Format('� Desea Borra la Simulaci�n del Empleado #%d ?',
        [Datasource.DataSet.FieldByname('CB_CODIGO').AsInteger]),0,mbYes) then
     begin
          dmNomina.BorrarFiniquitoGlobal;
     end;
     ZetaCXGrid1DBTableView.ApplyBestFit();
     ZetaCXGrid2DBTableView.ApplyBestFit();
end;

procedure TSimulaMontosGlobal_DevEx.CreaColumaSumatoria(Columna:TcxGridDBColumn;
          TipoFormato: Integer ; TextoSumatoria: String ;
          FooterKind : tcxSummaryKind; GridTableView : TcxGridDBTableView );

procedure SeleccionTexto;
const
     K_VACIO = '';
     {***Formastos para las sumatorias***}
     K_SIN_TIPO = 0;
     K_TIPO_MONEDA = 1;

     K_ESPACIO = ' ';
     K_FORMATO_MONEDA = ',0.00;-,0.00'; 
     K_FORMATO_DEFAULT ='0';
begin
     if TextoSumatoria = K_VACIO then
     begin
          case FooterKind of
               skCount: TextoSumatoria:= 'Registros: ';
               skSum: TextoSumatoria:= 'Total: ';
               skAverage: TextoSumatoria:= 'Promedio: ';
               skMax: TextoSumatoria:= ' M�ximo: ';
               skMin:  TextoSumatoria:= ' M�nimo: ';
          end;
     end;
end;
begin
if Columna.Visible then
begin
     //Si la banda no esta visible se muestra.
     if GridTableView.OptionsView.Footer = false then
        GridTableView.OptionsView.Footer := true;
      with GridTableView.DataController.Summary do
      begin
           BeginUpdate;
      try
         with FooterSummaryItems.Add as TcxGridDBTableSummaryItem do
         begin
              Column := Columna;
              Kind := FooterKind;
               {***Se aplica un texto Default si el parametro de texto se mando vacio.**}
              SeleccionTexto;
              {***Tipo de formato que se desee aplicar**}
              //Al agregar el tipo de formato declarando la constante en esta unidad;
                 case TipoFormato of
                      K_TIPO_MONEDA : Format := TextoSumatoria + K_ESPACIO + K_FORMATO_MONEDA;
                      K_SIN_TIPO : Format := TextoSumatoria + K_ESPACIO + K_FORMATO_DEFAULT;
                 end;
         end;
      finally
             EndUpdate;
      end;
      end;
end;
end;

procedure TSimulaMontosGlobal_DevEx.ApplyMinWidth( ZetaDBGridDBTableView: TcxGridDBTableView);
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;


end.
