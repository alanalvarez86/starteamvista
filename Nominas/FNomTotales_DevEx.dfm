inherited NomTotales_DevEx: TNomTotales_DevEx
  Left = 663
  Top = 251
  Caption = 'Totales de N'#243'mina'
  ClientHeight = 484
  ClientWidth = 662
  ExplicitWidth = 662
  ExplicitHeight = 484
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel [0]
    Left = 0
    Top = 19
    Width = 662
    Height = 465
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object pnlNiveles: TPanel
      Left = 0
      Top = 0
      Width = 662
      Height = 29
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label22: TLabel
        Left = 16
        Top = 7
        Width = 122
        Height = 13
        Caption = 'Nivel de confidencialidad:'
      end
      object Niveles: TZetaKeyCombo
        Left = 144
        Top = 4
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        OnChange = NivelesChange
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 29
      Width = 662
      Height = 412
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox2: TcxGroupBox
        Left = 1
        Top = 317
        Caption = ' Valores Usados '
        TabOrder = 0
        Height = 89
        Width = 580
        object PE_LOG: TcxDBMemo
          Left = 10
          Top = 18
          DataBinding.DataField = 'PE_LOG'
          DataBinding.DataSource = DataSource
          Properties.ReadOnly = True
          TabOrder = 0
          Height = 63
          Width = 560
        end
      end
      object GroupBox3: TcxGroupBox
        Left = 293
        Top = 237
        Caption = ' Acumulados '
        TabOrder = 1
        Height = 80
        Width = 288
        object Label5: TLabel
          Left = 36
          Top = 34
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo en el Mes:'
        end
        object PE_POS_MES: TZetaDBTextBox
          Left = 133
          Top = 32
          Width = 35
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_POS_MES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_POS_MES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label15: TLabel
          Left = 174
          Top = 34
          Width = 15
          Height = 13
          Alignment = taRightJustify
          Caption = 'de:'
        end
        object PE_PER_MES: TZetaDBTextBox
          Left = 195
          Top = 32
          Width = 35
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_PER_MES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_PER_MES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label6: TLabel
          Left = 37
          Top = 53
          Width = 89
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo en el A'#241'o:'
        end
        object PE_PER_TOT: TZetaDBTextBox
          Left = 195
          Top = 51
          Width = 35
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_PER_TOT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_PER_TOT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label1: TLabel
          Left = 73
          Top = 15
          Width = 53
          Height = 13
          Alignment = taRightJustify
          Caption = 'Acumula a:'
        end
        object Label16: TLabel
          Left = 174
          Top = 53
          Width = 15
          Height = 13
          Alignment = taRightJustify
          Caption = 'de:'
        end
        object PE_NUMERO: TZetaDBTextBox
          Left = 133
          Top = 51
          Width = 35
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_NUMERO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_NUMERO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object ZetaDBTextBox1: TZetaDBTextBox
          Left = 133
          Top = 13
          Width = 97
          Height = 17
          AutoSize = False
          Caption = 'ZetaDBTextBox1'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_MES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox4: TcxGroupBox
        Left = 1
        Top = 237
        Caption = ' D'#237'as '
        TabOrder = 2
        Height = 80
        Width = 288
        object Label19: TLabel
          Left = 46
          Top = 16
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Esta N'#243'mina:'
        end
        object PE_DIAS: TZetaDBTextBox
          Left = 112
          Top = 14
          Width = 35
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_DIAS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_DIAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label20: TLabel
          Left = 10
          Top = 34
          Width = 99
          Height = 13
          Alignment = taRightJustify
          Caption = 'Acumulados en Mes:'
        end
        object PE_DIAS_AC: TZetaDBTextBox
          Left = 112
          Top = 32
          Width = 35
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_DIAS_AC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_DIAS_AC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label2: TLabel
          Left = 155
          Top = 34
          Width = 15
          Height = 13
          Alignment = taRightJustify
          Caption = 'de:'
        end
        object PE_DIA_MES: TZetaDBTextBox
          Left = 177
          Top = 32
          Width = 35
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_DIA_MES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_DIA_MES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox1: TcxGroupBox
        Left = 1
        Top = 133
        Caption = ' Totales '
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 3
        Height = 104
        Width = 288
        object Label11: TLabel
          Left = 52
          Top = 73
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Empleados:'
        end
        object PE_NUM_EMP: TZetaDBTextBox
          Left = 111
          Top = 71
          Width = 85
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_NUM_EMP'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_NUM_EMP'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label12: TLabel
          Left = 39
          Top = 16
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Percepciones:'
        end
        object PE_TOT_PER: TZetaDBTextBox
          Left = 111
          Top = 14
          Width = 85
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_TOT_PER'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_TOT_PER'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label13: TLabel
          Left = 41
          Top = 36
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Deducciones:'
        end
        object PE_TOT_DED: TZetaDBTextBox
          Left = 111
          Top = 34
          Width = 85
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_TOT_DED'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_TOT_DED'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label14: TLabel
          Left = 41
          Top = 54
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Neto a Pagar:'
        end
        object PE_TOT_NET: TZetaDBTextBox
          Left = 111
          Top = 52
          Width = 85
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'PE_TOT_NET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_TOT_NET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox5: TcxGroupBox
        Left = 293
        Top = 133
        Caption = ' Opciones '
        TabOrder = 4
        Height = 104
        Width = 288
        object PE_AHORRO: TDBCheckBox
          Left = 89
          Top = 17
          Width = 122
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Descontar Ahorros:'
          DataField = 'PE_AHORRO'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object PE_PRESTAM: TDBCheckBox
          Left = 76
          Top = 33
          Width = 135
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Descontar Pr'#233'stamos:'
          DataField = 'PE_PRESTAM'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object PE_SOLO_EX: TDBCheckBox
          Left = 92
          Top = 49
          Width = 119
          Height = 17
          Alignment = taLeftJustify
          Caption = 'S'#243'lo Excepciones:'
          DataField = 'PE_SOLO_EX'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object PE_INC_BAJ: TDBCheckBox
          Left = 120
          Top = 65
          Width = 91
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Incluir Bajas:'
          DataField = 'PE_INC_BAJ'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 3
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object Panel1: TPanel
        Left = 1
        Top = 2
        Width = 580
        Height = 131
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 5
        object PE_TIMBRO: TZetaDBTextBox
          Left = 97
          Top = 69
          Width = 195
          Height = 17
          AutoSize = False
          Caption = 'PE_TIMBRO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_TIMBRO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object PE_FEC_PAG: TZetaDBTextBox
          Left = 412
          Top = 9
          Width = 78
          Height = 17
          AutoSize = False
          Caption = 'PE_FEC_PAG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_FEC_PAG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label18: TLabel
          Left = 334
          Top = 11
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha de Pago:'
        end
        object Label3: TLabel
          Left = 34
          Top = 111
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descripci'#243'n:'
        end
        object PE_DESCRIP: TZetaDBTextBox
          Left = 97
          Top = 109
          Width = 248
          Height = 17
          AutoSize = False
          Caption = 'PE_DESCRIP'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_DESCRIP'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label10: TLabel
          Left = 355
          Top = 31
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modificada:'
        end
        object PE_FEC_MOD: TZetaDBTextBox
          Left = 412
          Top = 29
          Width = 78
          Height = 17
          AutoSize = False
          Caption = 'PE_FEC_MOD'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_FEC_MOD'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 367
          Top = 51
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modific'#243':'
        end
        object US_CODIGO: TZetaDBTextBox
          Left = 412
          Top = 49
          Width = 157
          Height = 17
          AutoSize = False
          Caption = 'US_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_CODIGO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 60
          Top = 51
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
        end
        object PE_STATUS: TZetaDBTextBox
          Left = 97
          Top = 49
          Width = 195
          Height = 17
          AutoSize = False
          Caption = 'PE_STATUS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_STATUS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label7: TLabel
          Left = 188
          Top = 11
          Width = 11
          Height = 13
          Alignment = taRightJustify
          Caption = 'al:'
        end
        object PE_FEC_FIN: TZetaDBTextBox
          Left = 214
          Top = 9
          Width = 78
          Height = 17
          AutoSize = False
          Caption = 'PE_FEC_FIN'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_FEC_FIN'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label8: TLabel
          Left = 33
          Top = 11
          Width = 60
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo Del:'
        end
        object PE_FEC_INI: TZetaDBTextBox
          Left = 97
          Top = 9
          Width = 78
          Height = 17
          AutoSize = False
          Caption = 'PE_FEC_INI'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_FEC_INI'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object UsoLbl: TLabel
          Left = 17
          Top = 91
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Uso de N'#243'mina:'
        end
        object PE_USO: TZetaDBTextBox
          Left = 97
          Top = 89
          Width = 248
          Height = 17
          AutoSize = False
          Caption = 'PE_USO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_USO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label17: TLabel
          Left = 23
          Top = 31
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'Asistencia Del:'
        end
        object PE_ASI_INI: TZetaDBTextBox
          Left = 97
          Top = 29
          Width = 78
          Height = 17
          AutoSize = False
          Caption = 'PE_ASI_INI'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_ASI_INI'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label21: TLabel
          Left = 188
          Top = 31
          Width = 11
          Height = 13
          Alignment = taRightJustify
          Caption = 'al:'
        end
        object PE_ASI_FIN: TZetaDBTextBox
          Left = 214
          Top = 29
          Width = 78
          Height = 17
          AutoSize = False
          Caption = 'PE_ASI_FIN'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_ASI_FIN'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object StatusTimbLbl: TLabel
          Left = 13
          Top = 71
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status Timbrado:'
        end
      end
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 662
    ExplicitWidth = 662
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 336
      ExplicitLeft = 326
      ExplicitWidth = 336
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 330
        ExplicitLeft = 3
      end
    end
  end
  inherited DataSource: TDataSource
    OnDataChange = nil
    Left = 528
    Top = 184
  end
end
