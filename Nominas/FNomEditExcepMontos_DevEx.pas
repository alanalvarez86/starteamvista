unit FNomEditExcepMontos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Buttons, DBCtrls, StdCtrls, ExtCtrls, Mask,
     {$ifndef VER130}Variants,{$endif}
     ZetaDBTextBox,
     ZetaNumero,
     ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;
type
  TNomEditExcepMontos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    EmpleadoLbl: TLabel;
    Panel2: TPanel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    MontoLbl: TLabel;
    Label6: TLabel;
    ReferenciaLbl: TLabel;
    MO_ACTIVO: TDBCheckBox;
    GroupBox1: TGroupBox;
    ExentaLbl: TLabel;
    MO_IMP_CALLbl: TLabel;
    MO_PER_CAL: TDBCheckBox;
    MO_X_ISPT: TZetaDBNumero;
    MO_IMP_CAL: TZetaDBNumero;
    MO_MONTO: TZetaDBNumero;
    MO_REFEREN: TDBEdit;
    CO_NUMERO: TZetaDBKeyLookup_DevEx;
    US_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure MO_ACTIVOClick(Sender: TObject);
    procedure MO_PER_CALClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    procedure ShowImpuestos( const lState: Boolean );
    procedure ShowControls( const lState: Boolean );
    procedure ShowIndividual( const lState: Boolean );
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure EscribirCambios; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  NomEditExcepMontos_DevEx: TNomEditExcepMontos_DevEx;

implementation

uses DNomina,
     DCatalogos,
     DCliente,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

procedure TNomEditExcepMontos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_NOM_EXCEP_MONTOS;
     FirstControl := CB_CODIGO;
     TipoValorActivo1 := stPeriodo;
     HelpContext := H33221_Excepciones_montos;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     CO_NUMERO.LookupDataset := dmCatalogos.cdsConceptos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TNomEditExcepMontos_DevEx.Connect;
begin
     with dmNomina do
     begin
          dmCatalogos.cdsConceptos.Conectar;
          dmCliente.cdsEmpleadoLookup.conectar;
          cdsExcepMontos.Conectar;
          DataSource.DataSet := cdsExcepMontos;
     end;
end;

function TNomEditExcepMontos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Agregar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
end;

function TNomEditExcepMontos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Borrar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
end;

function TNomEditExcepMontos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
end;

procedure TNomEditExcepMontos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( dmNomina.cdsExcepMontos.State = dsBrowse ) then
     begin
          ShowControls( MO_ACTIVO.Checked );
          ShowImpuestos( MO_ACTIVO.Checked );
     end;
end;

procedure TNomEditExcepMontos_DevEx.HabilitaControles;
begin
     CB_CODIGO.Enabled := Inserting;
     if Inserting then
        FirstControl := CB_CODIGO
     else
         FirstControl := CO_NUMERO;
     inherited;
end;

procedure TNomEditExcepMontos_DevEx.ShowControls( const lState: Boolean );
begin
     ShowImpuestos( lState );
     ReferenciaLbl.Enabled := lState;
     MO_REFEREN.Enabled := lState;
end;

procedure TNomEditExcepMontos_DevEx.ShowImpuestos( const lState: Boolean );
begin
     ExentaLbl.Enabled := lState;
     MO_X_ISPT.Enabled := lState;
     MO_PER_CAL.Enabled := lState;
     if lState then
        ShowIndividual( MO_PER_CAL.Checked )
     else
         ShowIndividual( lState );
end;

procedure TNomEditExcepMontos_DevEx.ShowIndividual( const lState: Boolean );
begin
     MO_IMP_CAL.Enabled := lState;
     MO_IMP_CALLbl.Enabled := lState;
end;

procedure TNomEditExcepMontos_DevEx.MO_ACTIVOClick(Sender: TObject);
begin
     inherited;
     if CO_NUMERO.LookUpDataSet.Active then
        ShowControls( MO_ACTIVO.Checked );
end;

procedure TNomEditExcepMontos_DevEx.MO_PER_CALClick(Sender: TObject);
begin
     inherited;
     ShowIndividual( MO_PER_CAL.Checked );
end;

procedure TNomEditExcepMontos_DevEx.EscribirCambios;
begin
     dmNomina.OperacionConflicto := Ord( ocReportar );
     inherited EscribirCambios;
end;

procedure TNomEditExcepMontos_DevEx.OKClick(Sender: TObject);
var
   iEmpleado, iConcepto: Integer;
   sReferencia: String;
begin
     with dmNomina do
     begin
          ConflictoMontos:= FALSE;

          inherited;

          if ConflictoMontos then
             with cdsExcepMontos do
             begin
                  iEmpleado:= FieldByName( 'CB_CODIGO' ).AsInteger;
                  iConcepto:= FieldByName( 'CO_NUMERO' ).AsInteger;
                  sReferencia:= FieldByName( 'MO_REFEREN' ).AsString;
                  Refrescar;
                  Locate( 'CB_CODIGO;CO_NUMERO;MO_REFEREN',VarArrayOf([ iEmpleado, iConcepto, sReferencia]),[]);
             end;
     end;
end;

procedure TNomEditExcepMontos_DevEx.OK_DevExClick(Sender: TObject);
var
   iEmpleado, iConcepto: Integer;
   sReferencia: String;
begin
     with dmNomina do
     begin
          ConflictoMontos:= FALSE;

          inherited;

          if ConflictoMontos then
             with cdsExcepMontos do
             begin
                  iEmpleado:= FieldByName( 'CB_CODIGO' ).AsInteger;
                  iConcepto:= FieldByName( 'CO_NUMERO' ).AsInteger;
                  sReferencia:= FieldByName( 'MO_REFEREN' ).AsString;
                  Refrescar;
                  Locate( 'CB_CODIGO;CO_NUMERO;MO_REFEREN',VarArrayOf([ iEmpleado, iConcepto, sReferencia]),[]);
             end;
     end;
end;

procedure TNomEditExcepMontos_DevEx.SetEditarSoloActivos;
begin
     CO_NUMERO.EditarSoloActivos := TRUE;
end;

end.

