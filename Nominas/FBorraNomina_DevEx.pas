unit FBorraNomina_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, ZBaseDlgModal_DevEx,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
     dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons,
     cxControls, cxContainer, cxEdit, cxGroupBox, cxRadioGroup,
     dxGDIPlusClasses;

type
  eBorraNomina_DevEx = ( bnInit, bnBorrar, bnCancelar );
  TBorraNominaActiva_DevEx = class(TZetaDlgModal_DevEx)
    Image1: TImage;
    Borrar: TcxRadioGroup;
    Mensaje: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FAccion: eBorraNomina_DevEx;
  public
    { Public declarations }
    property Accion: eBorraNomina_DevEx read FAccion;
  end;

function ShowDlgBorraNomina( const sMsg: String ): eBorraNomina_DevEx;

var
  BorraNominaActiva_DevEx: TBorraNominaActiva_DevEx;

implementation

uses ZetaCommonClasses,
     FTressShell;

{$R *.DFM}

function ShowDlgBorraNomina( const sMsg: String ): eBorraNomina_DevEx;
begin
     if ( BorraNominaActiva_DevEx = nil ) then
     begin
          BorraNominaActiva_DevEx := TBorraNominaActiva_DevEx.Create( Application ); // Se destruye al Salir del Programa //
     end;
     with BorraNominaActiva_DevEx do
     begin
          Mensaje.Caption := sMsg;
          ShowModal;
          Result := Accion;
     end;
end;

{ ******** TBorraNominaActiva ********** }

procedure TBorraNominaActiva_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H30332_Borrar_nomina_activa;
end;

procedure TBorraNominaActiva_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with Borrar do
     begin
          ItemIndex := 0;
          SetFocus;
     end;
     FAccion := bnCancelar;
end;

procedure TBorraNominaActiva_DevEx.BorrarClick(Sender: TObject);
begin
     inherited;
     OK_DevEx.Enabled := ( Borrar.ItemIndex = 0 ) or ( Borrar.ItemIndex = 1 );
end;

procedure TBorraNominaActiva_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     case Borrar.ItemIndex of
          0: FAccion := bnInit;
          1: FAccion := bnBorrar;
     else
         FAccion := bnCancelar;
     end;
     Close;
end;

end.


