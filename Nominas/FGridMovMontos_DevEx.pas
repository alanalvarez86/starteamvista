unit FGridMovMontos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion_DevEx, StdCtrls, Db, Grids, DBGrids, ZetaDBGrid, DBCtrls,
  ExtCtrls, ZetaMessages, ZetaSmartLists,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TGridMovMontos_DevEx = class(TBaseGridEdicion_DevEx)
    PanelEmpleado: TPanel;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    cbOperacion: TdxBarCombo;
    Label1: TdxBarStatic;
    procedure FormCreate(Sender: TObject);
    //procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure CB_CODIGOValidKey(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaConcepto;
    procedure SetColumnaCorrecta;
    procedure CambioEmpleado;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    { Protected declarations }
    function PosicionaSiguienteColumna: Integer; override;
    function PuedeModificar: Boolean; override;
    procedure Borrar; override;
    procedure Buscar; override;
    procedure Connect; override;
    procedure EscribirCambios; override;
    procedure CancelarCambios; override;
    procedure HabilitaControles; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    procedure ConectaMontos; virtual;
  public
    { Public declarations }
  end;

const
     K_COLUMNA_CONCEPT = 0;
     K_COLUMNA_PERCEPC = 2;
     K_COLUMNA_DEDUCCI = 3;
     K_COLUMNA_REFEREN = 4;

var
  GridMovMontos_DevEx: TGridMovMontos_DevEx;

implementation

uses dNomina, dCliente, dCatalogos, ZetaDialogo, ZetaClientDataSet,
     ZetaCommonLists, ZetaCommonClasses, ZetaCommonTools, ZAccesosTress;

{$R *.DFM}

procedure TGridMovMontos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H30314_Montos_NominaModifica;
     IndexDerechos := D_NOM_DATOS_MONTOS;
//     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     cbOperacion.ItemIndex := dmNomina.OperacionConflicto; // Default Anterior
     if ( self.Monitor.Height > 480 ) then    // Resolución mayor a 640 * 480
        self.Height := Trunc( self.Monitor.Height * 0.80 );
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
end;

procedure TGridMovMontos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetColumnaCorrecta;
end;

procedure TGridMovMontos_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     ConectaMontos;
     with dmCliente do
     begin
          dmNomina.EmpleadoGrid := Empleado;
          CB_CODIGO.SetLlaveDescripcion( IntToStr( Empleado ), GetDatosEmpleadoActivo.Nombre );  // Evita que se Haga el DoLookup del Control
     end;
end;

procedure TGridMovMontos_DevEx.ConectaMontos;
begin
     with dmNomina do
     begin
          cdsMovMontos.Conectar;
          DataSource.DataSet:= cdsMovMontos;
     end;
end;

procedure TGridMovMontos_DevEx.HabilitaControles;
begin
     inherited;
     CB_CODIGO.Enabled := not self.Editing;
end;

procedure TGridMovMontos_DevEx.Borrar;
begin
     inherited;
     SetColumnaCorrecta;
end;

procedure TGridMovMontos_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CO_NUMERO' ) then
             BuscaConcepto;
     end;
end;

procedure TGridMovMontos_DevEx.BuscaConcepto;
var
   sKey, sDescription: String;
begin
     if dmCatalogos.cdsConceptos.Search_DevEx( 'CO_NUMERO < 1000', sKey, sDescription ) then
     begin
          with DataSource.DataSet do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CO_NUMERO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridMovMontos_DevEx.EscribirCambios;
begin
     dmNomina.OperacionConflicto := cbOperacion.ItemIndex;
     inherited EscribirCambios;
end;

procedure TGridMovMontos_DevEx.CancelarCambios;
begin
     inherited CancelarCambios;

     with DataSource.DataSet do
          if not IsEmpty then
             First;

     SetColumnaCorrecta;
end;

function TGridMovMontos_DevEx.PosicionaSiguienteColumna: Integer;
begin
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'MO_DEDUCCI' ) then
              Result := K_COLUMNA_CONCEPT
          else if ( FieldName = 'MO_PERCEPC' ) and ( not dmNomina.ConceptoEsDeduccion( DataSource.DataSet ) ) then
              Result := K_COLUMNA_CONCEPT
          else
              Result := inherited PosicionaSiguienteColumna;
     end;
end;

function TGridMovMontos_DevEx.PuedeModificar: Boolean;
begin
     Result := inherited PuedeModificar;
     if Result then
        with DataSource do
             if ( DataSet <> nil ) and ( DataSet.IsEmpty ) and ( not PuedeAgregar ) then
                Result := FALSE;
end;

procedure TGridMovMontos_DevEx.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
               if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) then
                  SeleccionaSiguienteRenglon;
               SetColumnaCorrecta;
          end
          else
               if ( Key = Chr( VK_ESCAPE ) ) then
               begin
                    SeleccionaPrimerColumna;
                    SetColumnaCorrecta;
               end;
     end
     else if ActiveControl.TabStop then
          inherited KeyPress( Key );
end;

procedure TGridMovMontos_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) and
             ( StrVacio( DataSource.DataSet.FieldByName( 'CO_NUMERO' ).AsString ) ) then
          begin
               if OK_DevEx.Enabled then
                  OK_DevEx.SetFocus;
               if not OK_DevEx.Enabled then
                  CB_CODIGO.SetFocus;
          end
          else
          begin
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
               if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) then
                  SeleccionaSiguienteRenglon;
               SetColumnaCorrecta;
          end;
     end;
end;

procedure TGridMovMontos_DevEx.SetColumnaCorrecta;
begin
     with ZetaDBGrid, dmNomina do
     begin
          if ( SelectedField.FieldName = 'CO_NUMERO' ) and
             ( not ( DataSource.DataSet.IsEmpty ) ) and
             ( not ( DataSource.DataSet.State = dsInsert ) ) then
             SelectedIndex := K_COLUMNA_PERCEPC;
          if ( SelectedField.FieldName = 'MO_PERCEPC' ) and
             ( ConceptoEsDeduccion( DataSource.DataSet ) ) then
             SelectedIndex := K_COLUMNA_DEDUCCI
          else if ( SelectedField.FieldName = 'MO_DEDUCCI' ) and
             ( not ConceptoEsDeduccion( DataSource.DataSet ) ) then
             SelectedIndex := K_COLUMNA_REFEREN;
     end;
end;

{
procedure TGridMovMontos.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CO_NUMERO' ) then
        with ZetaDBGrid, dmNomina do
        begin
             Columns[K_COLUMNA_PERCEPC].ReadOnly := ( ConceptoEsDeduccion( cdsMovMontos ) );
             Columns[K_COLUMNA_DEDUCCI].ReadOnly := ( not Columns[K_COLUMNA_PERCEPC].ReadOnly );
        end;
end;
}

procedure TGridMovMontos_DevEx.CB_CODIGOValidKey(Sender: TObject);
var
   sMensaje: String;
begin
     inherited;
     with dmNomina do
     begin
          if ( CB_CODIGO.Valor <> EmpleadoGrid ) then
          begin
               if BajaNominaAnterior( dmCliente.cdsEmpleadoLookUp , sMensaje ) then
               begin
                    ZetaDialogo.zInformation( Caption, sMensaje, 0 );
                    CB_CODIGO.Valor := EmpleadoGrid;
                    CB_CODIGO.SetFocus;
               end
               else if TipoNominaDiferente( dmCliente.cdsEmpleadoLookUp , sMensaje ) then
               begin
                    ZetaDialogo.zInformation( Caption, sMensaje, 0 );
                    CB_CODIGO.Valor := EmpleadoGrid;
                    CB_CODIGO.SetFocus;
               end
               else
               begin
                    EmpleadoGrid:= CB_CODIGO.Valor;
                    CambioEmpleado;
                    ZetaDBGrid.SetFocus;
               end;
          end;
     end;
end;

procedure TGridMovMontos_DevEx.CambioEmpleado;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        TZetaClientDataSet( DataSource.DataSet ).Refrescar;
        DataSource.AutoEdit := PuedeModificar;
        SetColumnaCorrecta;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TGridMovMontos_DevEx.OK_DevExClick(Sender: TObject);
begin
     with dmNomina do
     begin
          ConflictoMontos:= FALSE;

          EscribirCambios;

          if ConflictoMontos then              // Por si hubo Conflicto en Montos, se Refresca
             TZetaClientDataSet( DataSource.DataSet ).Refrescar;
     end;

     DataSourceStateChange( self );

     if CB_CODIGO.Enabled then
        CB_CODIGO.SetFocus;
end;

procedure TGridMovMontos_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     if CB_CODIGO.Enabled then
        CB_CODIGO.SetFocus;
end;

end.


