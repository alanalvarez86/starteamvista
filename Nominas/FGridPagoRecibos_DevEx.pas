unit FGridPagoRecibos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion_DevEx, Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls,
  StdCtrls, Mask, ZetaFecha, ZetaDBGrid, ZetaSmartLists,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TGridPagoRecibos_DevEx = class(TBaseGridEdicion_DevEx)
    ZFecha: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    procedure BuscaEmpleado;
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
  end;

var
  GridPagoRecibos_DevEx: TGridPagoRecibos_DevEx;

implementation

uses DCliente, DNomina, ZetaCommonClasses, ZetaCommonLists, ZetaBuscaEmpleado_DevEx;

{$R *.DFM}

procedure TGridPagoRecibos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1:= stPeriodo;
     HelpContext := H30336_Pago_de_recibos;
end;

procedure TGridPagoRecibos_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmNomina do
     begin
          cdsPagoRecibos.Refrescar;
          DataSource.DataSet:= cdsPagoRecibos;
     end;
end;

procedure TGridPagoRecibos_DevEx.FormShow(Sender: TObject);
const
     COLUMNA_FECHA = 2;
begin
     inherited;
     with dmNomina, ZetaDBGrid do
     begin
          if MarcarRecibosPagados then
          begin
               Columns[COLUMNA_FECHA].ReadOnly:= False;
               Columns[COLUMNA_FECHA].Color:= clWindow;
               Columns[COLUMNA_FECHA].Font.Color:= clWindowText;
               ZFecha.Enabled:= True;
          end
          else
          begin
               Columns[COLUMNA_FECHA].ReadOnly:= True;
               Columns[COLUMNA_FECHA].Color:= clInfoBk;
               Columns[COLUMNA_FECHA].Font.Color:= clInfoText;
               ZFecha.Enabled:= False;
          end;
     end;
end;

procedure TGridPagoRecibos_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado;
     end;
end;

procedure TGridPagoRecibos_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmNomina.cdsPagoRecibos do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

procedure TGridPagoRecibos_DevEx.KeyPress(var Key: Char);
begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> chr(9) ) and ( key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'NO_FEC_PAG' ) then
               begin
                    if ZFecha.Enabled then
                    begin
                         zFecha.SetFocus;
                         SendMessage(zFecha.Handle, WM_Char, word(Key), 0);
                         Key:= #0;
                    end;
               end;
          end;
     end
     else if ( ActiveControl = zFecha ) and ( ( Key = chr( VK_RETURN ) ) or ( Key = chr( 9 ) ) ) then
     begin
          Key := #0;
          with ZetaDBGrid do
          begin
               SetFocus;
               ZetaDBGrid.SelectedIndex:= PrimerColumna;
               SeleccionaSiguienteRenglon;
          end;
     end;
end;

procedure TGridPagoRecibos_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     if (gdFocused in State) then
        if ( Column.FieldName = 'NO_FEC_PAG' ) then
           with zFecha do
           begin
                if Enabled then
                begin
                     Left := Rect.Left + ZetaDBGrid.Left;
                     Top := Rect.Top + ZetaDBGrid.Top;
                     Width:= Column.Width + 2;
                     Visible := True;
                end;
           end;
end;

procedure TGridPagoRecibos_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) and ( SelectedField.FieldName = 'NO_FEC_PAG' ) and
             ( zFecha.Visible ) then
             zFecha.Visible:= False;
     end;
end;


end.
