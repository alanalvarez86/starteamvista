unit FNomPolizas_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TNomPolizas_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Public declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function  PuedeModificar(var sMensaje: String ): Boolean; override;
  end;

var
  NomPolizas_DevEx: TNomPolizas_DevEx;

implementation

uses DNomina,
     dCatalogos,
     dSistema,
     ZetaCommonLists,
     ZetaCommonClasses;


{$R *.dfm}

procedure TNomPolizas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stPeriodo;
     HelpContext := H_Nom_Datos_Polizas;

end;

procedure TNomPolizas_DevEx.Connect;
begin
     dmCatalogos.cdsTiposPoliza.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmNomina do
     begin
          cdsPolizas.Conectar;
          DataSource.DataSet := cdsPolizas;
     end;
end;

procedure TNomPolizas_DevEx.Refresh;
begin
     dmNomina.cdsPolizas.Refrescar;
end;

procedure TNomPolizas_DevEx.Agregar;
begin
     inherited;
     dmNomina.cdsPolizas.Agregar;
end;

procedure TNomPolizas_DevEx.Borrar;
begin
     inherited;
     dmNomina.cdsPolizas.Borrar;
end;

procedure TNomPolizas_DevEx.Modificar;
begin
     inherited;
     dmNomina.cdsPolizas.Modificar;
end;

function TNomPolizas_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se puede modificar el encabezado de la p�liza';
     Result := False;
end;

procedure TNomPolizas_DevEx.FormShow(Sender: TObject);
begin
      CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'', SkCount );
      inherited;
      ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
      ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
end;

end.
