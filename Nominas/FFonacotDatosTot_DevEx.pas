unit FFonacotDatosTot_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, StdCtrls, ExtCtrls, ComCtrls,
     ZetaDBTextBox, DBCtrls, ZBaseGridLecturaImss_DevEx,
     cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
     dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
     cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList,
     ImgList, ZetaStateComboBox, cxGridLevel, cxClasses, cxGridCustomView,
     cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
     ZetaCXGrid, cxContainer, cxButtons, cxGroupBox, cxTextEdit, cxMemo,
     cxDBEdit, System.Actions, dxBarBuiltInMenu, cxPC,
     Variants, cxCheckBox, cxLabel, DBGrids, ZBaseGridLecturaFonacot_DevEx, FTressShell;

type
  TFonacotDatosTot_DevEx = class(TBaseGridLecturaFonacot_DevEx)
    DataSourceDetalle: TDataSource;
    pcTotales: TcxPageControl;
    tsResumen: TcxTabSheet;
    GroupBox3: TcxGroupBox;
    Label20: TLabel;
    FT_RETENC: TZetaDBTextBox;
    Label19: TLabel;
    FT_AJUSTE: TZetaDBTextBox;
    FT_CUANTOS: TZetaDBTextBox;
    Label5: TLabel;
    FT_TAJUST: TZetaDBTextBox;
    Label24: TLabel;
    Label18: TLabel;
    FT_INCAPA: TZetaDBTextBox;
    Label34: TLabel;
    Label35: TLabel;
    FT_EMPLEAD: TZetaDBTextBox;
    FT_BAJAS: TZetaDBTextBox;
    Label1: TLabel;
    FT_STATUS: TZetaDBTextBox;
    Label2: TLabel;
    FT_NOMINA: TZetaDBTextBox;
    Label3: TLabel;
    FC_PORCENTAJE: TZetaDBTextBox;
    Label4: TLabel;
    FT_DETALLE: TcxDBMemo;
    tsDetalleDiferencias: TcxTabSheet;
    btnCierraCalculo: TcxButton;
    btnAbreCalculo: TcxButton;
    grFonacotDiferencias: TZetaCXGrid;
    grFonacotDiferenciasGridDBTableView: TcxGridDBTableView;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    PR_REFEREN: TcxGridDBColumn;
    FC_NOMINA: TcxGridDBColumn;
    PF_PAGO: TcxGridDBColumn;
    FE_IN_DESC: TcxGridDBColumn;
    DIFERENCIA: TcxGridDBColumn;
    grFonacotDiferenciasGridLevel: TcxGridLevel;
    DataSourceDiferencias: TDataSource;
    plDetalle: TPanel;
    cbSoloDiferencias: TcxCheckBox;
    gbDiferencias: TcxGroupBox;
    Label8: TLabel;
    tbBajaIMSS: TZetaDBTextBox;
    Label9: TLabel;
    tbIncapacidad: TZetaDBTextBox;
    Label12: TLabel;
    tbOtro: TZetaDBTextBox;
    Label11: TLabel;
    tbTotal: TZetaDBTextBox;
    FC_AJUSTE: TcxGridDBColumn;
    Label6: TLabel;
    FT_DIFERENCIAS: TZetaDBTextBox;
    cxGroupBox1: TcxGroupBox;
    Label7: TLabel;
    PagoDirecto: TZetaDBTextBox;
    Label10: TLabel;
    ConfirmacionFonacot: TZetaDBTextBox;
    Label13: TLabel;
    AusenteEnCedula: TZetaDBTextBox;
    Label14: TLabel;
    CanceladoOtro: TZetaDBTextBox;
    PrestamosCancelados: TZetaDBTextBox;
    Label15: TLabel;
    SubTotal: TZetaDBTextBox;
    Label16: TLabel;
    PR_FON_MOT: TcxGridDBColumn;
    DataSourceViejo: TDataSource;
    Label17: TLabel;
    tbPagoMas: TZetaDBTextBox;
    Label22: TLabel;
    tbPagoMenos: TZetaDBTextBox;
    Label25: TLabel;
    FT_TIPO_CEDULA: TZetaTextBox;
    tsResumen2017: TcxTabSheet;
    cxGroupBox2: TcxGroupBox;
    Label21: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    Label23: TLabel;
    ZetaDBTextBox2: TZetaDBTextBox;
    ZetaDBTextBox3: TZetaDBTextBox;
    Label26: TLabel;
    ZetaDBTextBox4: TZetaDBTextBox;
    Label27: TLabel;
    Label28: TLabel;
    ZetaDBTextBox5: TZetaDBTextBox;
    Label29: TLabel;
    Label30: TLabel;
    ZetaDBTextBox6: TZetaDBTextBox;
    ZetaDBTextBox7: TZetaDBTextBox;
    Label31: TLabel;
    ZetaDBTextBox8: TZetaDBTextBox;
    Label32: TLabel;
    ZetaDBTextBox9: TZetaDBTextBox;
    Label33: TLabel;
    ZetaDBTextBox10: TZetaDBTextBox;
    Label36: TLabel;
    cxDBMemo1: TcxDBMemo;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    PR_STATUS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure btnCierraAbreCalculoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbSoloDiferenciasClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure grFonacotDiferenciasGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure grFonacotDiferenciasGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure grFonacotDiferenciasGridDBTableViewColumnHeaderClick(
      Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure grFonacotDiferenciasGridDBTableViewDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure IMSSPatronCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure IMSSMesCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure IMSSAnioCBLookUp(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Borrar; override;
    procedure Imprimir; override;
    procedure Exportar; override;
    procedure ConnectGrid; override;
    procedure ApplyMinWidth;
    procedure PintaPorcentaje_Tipo_Cedula;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  end;

var
  FonacotDatosTot_DevEx: TFonacotDatosTot_DevEx;

implementation

uses DNomina,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZImprimeGrid,
     ZAccesosMgr,
     ZetaDialogo,
     ZGridModeTools,
     ZetaClientDataSet,
     ZetaCommonTools,
     DCliente,
     DGlobal,
     ZGlobalTress;

{$R *.DFM}

const
     K_DERECHO_IMPRIMIR_PROPIO = 4;
     K_DERECHO_BORRAR_PROPIO = 2;
     K_DERECHO_CERRAR_CALCULO = 8;
     K_DERECHO_ABRIR_CALCULO = 16;

procedure TFonacotDatosTot_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( grFonacotDiferenciasGridDBTableView );
end;

procedure TFonacotDatosTot_DevEx.FormCreate(Sender: TObject);
var lNuevoEsquemaFonacot: Boolean;
begin
     inherited;
     TipoValorActivo2 := stRazonSocial;
     HelpContext := H_NOM_FONACOT_TOTALES;
     grFonacotDiferenciasGridDBTableView.DataController.DataModeController.GridMode:= True;

     lNuevoEsquemaFonacot := Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA);
     tsResumen2017.TabVisible := not lNuevoEsquemaFonacot;
     tsResumen.TabVisible := lNuevoEsquemaFonacot;
     tsDetalleDiferencias.TabVisible := lNuevoEsquemaFonacot;

     if lNuevoEsquemaFonacot then
        pcTotales.ActivePage := tsResumen
     else
         pcTotales.ActivePage := tsResumen2017;
end;

procedure TFonacotDatosTot_DevEx.FormShow(Sender: TObject);
begin

     try
         if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
         begin
             inherited;
             with self.IMSSPatronCB do
             begin
                  DropDownCount := iMin( Items.Count, 12 );   // M�ximo muestra 12 sin que aparezca el Scroll
             end;

             btnCierraCalculo.Enabled:= CheckDerechos( K_DERECHO_CERRAR_CALCULO );
             btnAbreCalculo.Enabled:= CheckDerechos( K_DERECHO_ABRIR_CALCULO );

             cbSoloDiferencias.Checked := dmNomina.FonacotSoloDiferencias;

             // ApplyMinWidth;
             CreaColumaSumatoria( grFonacotDiferenciasGridDBTableView.GetColumnByFieldName('CB_CODIGO'), 0 , '', skCount);

             //Activa el filtrado por DataSet
             if grFonacotDiferenciasGridDBTableView.DataController.IsGridMode then
                grFonacotDiferenciasGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
             // Activa la posibilidad de agrupar
             grFonacotDiferenciasGridDBTableView.OptionsCustomize.ColumnGrouping := True;
             // Muestra la caja de agrupamiento
             grFonacotDiferenciasGridDBTableView.OptionsView.GroupByBox := True;
             //Para que nunca muestre el filterbox inferior
             grFonacotDiferenciasGridDBTableView.FilterBox.Visible := fvNever;
             //Para que no aparezca el Custom Dialog
             grFonacotDiferenciasGridDBTableView.FilterBox.CustomizeDialog := False;
             //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
             grFonacotDiferenciasGridDBTableView.ApplyBestFit();

             Connect;
             PintaPorcentaje_Tipo_Cedula;

             dmNomina.TodasLasRazones := TRUE;
             IMSSPatronCB.ItemIndex := 0;
         end;
     Except
     end;
end;

procedure TFonacotDatosTot_DevEx.grFonacotDiferenciasGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     if grFonacotDiferenciasGridDBTableView.DataController.IsGridMode then
     begin
          inherited;
          self.AColumn := TcxGridDBColumn(AColumn);
     end;
end;


procedure TFonacotDatosTot_DevEx.grFonacotDiferenciasGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
   oField: TField;
   sFieldName: String;
   cdsClone: TZetaClientDataSet;
   oListaLookup: TStringList;

   procedure StoreLookupValues;
   var
      ValorAnterior, ValorActual: String;
   begin
        sFieldName := oField.KeyFields;
        oListaLookup := TStringList.Create;
        cdsClone := TZetaClientDataSet.Create(self);
        try
           with cdsClone do
           begin
                CloneCursor( TZetaClientDataSet( grFonacotDiferenciasGridDBTableView.DataController.DataSet ), TRUE, TRUE );
                IndexFieldNames := sFieldName;
                ValorAnterior := ZetaCommonClasses.VACIO;
                First;
                while ( not EOF ) do
                begin
                     ValorActual := FieldByName( sFieldName ).AsString;
                     if ( ValorActual <> ValorAnterior ) then
                     begin
                          oListaLookup.Add( Format( '%s=%s', [ TZetaLookupDataSet( oField.LookupDataSet ).GetDescripcion( ValorActual ), ValorActual ] ) );
                          ValorAnterior := ValorActual;
                     end;
                     Next;
                end;
                Close;
                oListaLookup.Sort;
           end;
        finally
               grFonacotDiferenciasGridDBTableView.Columns[ AItemIndex ].Tag := Integer( oListaLookup );
               cdsClone.Free;
        end;
   end;

   procedure LlenaLookupValues;
   var
      i: Integer;
   begin
        if ( grFonacotDiferenciasGridDBTableView.Columns[ AItemIndex ].Tag = 0 ) then
           StoreLookupValues;
        if ( grFonacotDiferenciasGridDBTableView.Columns[ AItemIndex ].Tag > 0 ) then
        begin
             try
                oListaLookup := TStringList( grFonacotDiferenciasGridDBTableView.Columns[ AItemIndex ].Tag );
                for i := 0 to oListaLookup.Count - 1 do
                begin
                     AValueList.Add( fviValue, oListaLookup.ValueFromIndex[i], oListaLookup.Names[i], TRUE );
                end;
             except
                   //Abort;   // excepci�n silenciosa - No continua con el llenado
             end;
        end;
   end;

begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if grFonacotDiferenciasGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( grFonacotDiferenciasGridDBTableView, AItemIndex, AValueList );
end;

procedure TFonacotDatosTot_DevEx.grFonacotDiferenciasGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
     if grFonacotDiferenciasGridDBTableView.DataController.IsGridMode then
     begin
          inherited;
          ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
     end;
end;
procedure TFonacotDatosTot_DevEx.grFonacotDiferenciasGridDBTableViewDataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
     inherited;
     if grFonacotDiferenciasGridDBTableView.DataController.IsGridMode and grFonacotDiferenciasGridDBTableView.OptionsView.Footer then      // Hay columnas totalizadas
        ZGridModeTools.AsignaValorColumnaSummary( ASender );
end;

procedure TFonacotDatosTot_DevEx.Connect;
begin
     if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
     begin
           with dmNomina do
           begin
                cdsFonTot.Conectar;
                cdsFonTotRs.Conectar;

                DataSource.DataSet:= cdsFonTotRs;
                DataSourceViejo.DataSet :=  cdsFonTot;

                cdsFonTotDetalle.Conectar;
                DataSourceDetalle.DataSet := cdsFonTotDetalle;
                cdsFonTotDiferencias.Conectar;
                DataSourceDiferencias.DataSet := cdsFonTotDiferencias;

                if dmNomina.TodasLasRazones then
                   IMSSPatronCB.ItemIndex := 0;
                // Refresh;
           end;
     end
     else
     begin
          ZInformation( 'Totales de Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_TIPO_PRESTAMO, 0 )

     end;
end;

procedure TFonacotDatosTot_DevEx.Refresh;
begin
     dmNomina.cdsFonTot.Refrescar;
     dmNomina.cdsFonTotRs.Refrescar;
     dmNomina.cdsFonTotDetalle.Refrescar;
     dmNomina.cdsFonTotDiferencias.Refrescar;
     // ApplyMinWidth;
     CreaColumaSumatoria( grFonacotDiferenciasGridDBTableView.GetColumnByFieldName('CB_CODIGO'), 0 , '', skCount);
     grFonacotDiferenciasGridDBTableView.ApplyBestFit();
     PintaPorcentaje_Tipo_Cedula;

     if dmNomina.TodasLasRazones then
        IMSSPatronCB.ItemIndex := 0;
end;

procedure TFonacotDatosTot_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     if grFonacotDiferenciasGridDBTableView.DataController.IsGridMode then
     begin
          inherited;
          self.AColumn := TcxGridDBColumn(AColumn);
     end;
end;


procedure TFonacotDatosTot_DevEx.Borrar;
begin
     dmNomina.cdsFonTot.Borrar;
end;

function TFonacotDatosTot_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar A Los Totales De Fonacot';
end;

function TFonacotDatosTot_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Pueden Modificar Los Totales De Fonacot';
end;

procedure TFonacotDatosTot_DevEx.btnCierraAbreCalculoClick(Sender: TObject);
begin
     inherited;
     dmNomina.AbreCierraCalculo( eSubeBaja( TcxButton( Sender ).Tag ) );
     Refresh;
end;

procedure TFonacotDatosTot_DevEx.cbSoloDiferenciasClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        dmNomina.FonacotSoloDiferencias := cbSoloDiferencias.Checked;
        dmNomina.cdsFonTotDetalle.Refrescar;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TFonacotDatosTot_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result:= CheckDerechos(K_DERECHO_IMPRIMIR_PROPIO);
     if not Result then
        sMensaje:='No tiene Permiso para Imprimir Registros';
end;

function TFonacotDatosTot_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= CheckDerechos(K_DERECHO_BORRAR_PROPIO);
     if not Result then
        sMensaje:='No tiene Permiso para Borrar Registros';
end;

{***(@am): Se detecto un error en la impresion de esta forma, porque se hered� de una clase base con grid cuando en
           realidad no lo necesita. El metodo de impresion en la base valida la existencia de un grid (el cual si esta
           presente aunque no se utilice), para no tener que hacer otra clase base que incluya el panel de Valores activos
           de IMSS solo se sobreescribe el metodo Print para que no realice la validacion del grid. ***}

procedure TFonacotDatosTot_DevEx.Imprimir;
begin
     if zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

procedure TFonacotDatosTot_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with grFonacotDiferenciasGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TFonacotDatosTot_DevEx.ConnectGrid;
begin
     inherited;
     try
         if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
         begin
             if grFonacotDiferenciasGridDBTableView.DataController.IsGridMode then
                grFonacotDiferenciasGridDBTableView.DataController.Filter.Active := TRUE;

             PintaPorcentaje_Tipo_Cedula;

             if dmNomina.TodasLasRazones then
                IMSSPatronCB.ItemIndex := 0;
         end;
     Except
     end;
end;

procedure TFonacotDatosTot_DevEx.Exportar;
 var
    lExporta: Boolean;
    i: integer;
    Valor : Variant;
begin
     lExporta:= FALSE;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TcxGridDBTableView ) then
          begin
               lExporta:= TRUE;
               if zConfirm( 'Exportar...', '� Desea exportar a excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    if (Components[ i ] is TDBGrid) then
                       ZImprimeGrid.ExportarGrid( TDBGrid( Components[ i ] ),
                       TDBGrid( Components[ i ] ).DataSource.DataSet, Caption, 'IM',
                       Valor[0],Valor[1],Valor[2],Valor[3])
                    else
                    begin
                         ZImprimeGrid.ExportarGrid(  ( grFonacotDiferenciasGridDBTableView ),
                                                     grFonacotDiferenciasGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM',
                                                     Valor[0],Valor[1],Valor[2],Valor[3]);
                    end;
               end;
               Break;
          end;
     end;
end;

procedure TFonacotDatosTot_DevEx.PintaPorcentaje_Tipo_Cedula;
Const K_SIN_CEDULA = -1;
var fPorcentaje :  Double;
    iTipoCedula: Integer;
begin
     with DataSource.DataSet do
     begin
     fPorcentaje := StrToFloat ( copy( FieldByName('FT_PORCENTAJE').AsString, 1, length( FieldByName('FT_PORCENTAJE').AsString )-1) );
     if fPorcentaje <= 90 then
        FC_PORCENTAJE.Font.Color := clRed
     else
         FC_PORCENTAJE.Font.Color := clWindowText;
     end;

     // Considerar si existe registro de c�dula para todas las razones sociales antes
     // de buscar por la raz�n social seleccionada.
     with DataSource.DataSet do
     begin
          iTipoCedula := dmNomina.FonacotTipoCedula(IMSSAnioCB.ValorEntero, IMSSMesCB.Indice, VACIO);
          if iTipoCedula  = K_SIN_CEDULA then
             iTipoCedula := dmNomina.FonacotTipoCedula(IMSSAnioCB.ValorEntero, IMSSMesCB.Indice, IMSSPatronCB.Llave);
          if (iTipoCedula = ord (etacPorTrabajador)) then
             FT_CUANTOS.Caption := 'No aplica';
     end;

     if iTipoCedula < 0 then
        FT_TIPO_CEDULA.Caption := VACIO
     else
         FT_TIPO_CEDULA.Caption := ObtieneElemento (lfTipoArchivoCedula, iTipoCedula);
end;

procedure TFonacotDatosTot_DevEx.IMSSAnioCBLookUp(Sender: TObject;
  var lOk: Boolean);
begin
     // inherited;
     dmCliente.IMSSYear := IMSSAnioCB.ValorEntero;
     if IMSSPatronCB.ItemIndex > 0 then
     begin
          dmNomina.TodasLasRazones := FALSE;
          // inherited
          TressSHell.RefrescaIMSSActivoPublico;
     end
     else
     begin
         // dmNomina.cdsFonTotRs.Refrescar;
         dmNomina.TodasLasRazones := TRUE;

         dmNomina.cdsFonTot.Refrescar;
         dmNomina.cdsFonTotRs.Refrescar;
         dmNomina.cdsFonTotDiferencias.Refrescar;
         dmNomina.cdsFonTotDetalle.Refrescar;
         PintaPorcentaje_Tipo_Cedula;

         IMSSPatronCB.ItemIndex := 0;
     end;
end;

procedure TFonacotDatosTot_DevEx.IMSSMesCBLookUp(Sender: TObject;
  var lOk: Boolean);
begin
     // inherited;
     dmCliente.IMSSMes := IMSSMesCB.Indice;
     if IMSSPatronCB.ItemIndex > 0 then
     begin
          dmNomina.TodasLasRazones := FALSE;
          // inherited
          TressSHell.RefrescaIMSSActivoPublico;
     end
     else
     begin
         // dmNomina.cdsFonTotRs.Refrescar;
         dmNomina.TodasLasRazones := TRUE;

         dmNomina.cdsFonTot.Refrescar;
         dmNomina.cdsFonTotRs.Refrescar;
         dmNomina.cdsFonTotDiferencias.Refrescar;
         dmNomina.cdsFonTotDetalle.Refrescar;
         PintaPorcentaje_Tipo_Cedula;

         IMSSPatronCB.ItemIndex := 0;
     end;

end;

procedure TFonacotDatosTot_DevEx.IMSSPatronCBLookUp(Sender: TObject;
  var lOk: Boolean);
begin
     // inherited;
     if IMSSPatronCB.ItemIndex > 0 then
     begin
          dmNomina.TodasLasRazones := FALSE;
          inherited
     end
     else
     begin
         // dmNomina.cdsFonTotRs.Refrescar;
         dmNomina.TodasLasRazones := TRUE;

         dmNomina.cdsFonTot.Refrescar;
         dmNomina.cdsFonTotRs.Refrescar;
         dmNomina.cdsFonTotDiferencias.Refrescar;
         dmNomina.cdsFonTotDetalle.Refrescar;
         PintaPorcentaje_Tipo_Cedula;

         IMSSPatronCB.ItemIndex := 0;
     end;
end;

end.
