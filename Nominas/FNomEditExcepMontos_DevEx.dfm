inherited NomEditExcepMontos_DevEx: TNomEditExcepMontos_DevEx
  Left = 226
  Top = 152
  Caption = 'Excepciones de Montos'
  ClientHeight = 324
  ClientWidth = 521
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 288
    Width = 521
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 356
      Top = 4
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 435
      Top = 4
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 521
    TabOrder = 0
    inherited Splitter: TSplitter
      Left = 300
      Width = 2
    end
    inherited ValorActivo1: TPanel
      Width = 300
      inherited textoValorActivo1: TLabel
        Width = 294
      end
    end
    inherited ValorActivo2: TPanel
      Left = 302
      Width = 219
      inherited textoValorActivo2: TLabel
        Width = 213
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 47
    Width = 521
    Height = 34
    Align = alTop
    TabOrder = 1
    object EmpleadoLbl: TLabel
      Left = 53
      Top = 9
      Width = 50
      Height = 13
      Caption = '&Empleado:'
    end
    object CB_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 110
      Top = 6
      Width = 359
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      DataField = 'CB_CODIGO'
      DataSource = DataSource
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 81
    Width = 521
    Height = 207
    Align = alClient
    TabOrder = 2
    object Label1: TLabel
      Left = 58
      Top = 11
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = '&Concepto:'
    end
    object MontoLbl: TLabel
      Left = 74
      Top = 33
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Monto:'
    end
    object Label6: TLabel
      Left = 59
      Top = 183
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modific'#243':'
    end
    object ReferenciaLbl: TLabel
      Left = 5
      Top = 56
      Width = 102
      Height = 13
      Alignment = taRightJustify
      Caption = '&Referencia Pr'#233'stamo:'
    end
    object US_CODIGO: TZetaDBTextBox
      Left = 109
      Top = 181
      Width = 104
      Height = 17
      AutoSize = False
      Caption = 'US_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object MO_ACTIVO: TDBCheckBox
      Left = 72
      Top = 77
      Width = 50
      Height = 17
      Alignment = taLeftJustify
      Caption = '&Activo:'
      DataField = 'MO_ACTIVO'
      DataSource = DataSource
      TabOrder = 3
      ValueChecked = 'S'
      ValueUnchecked = 'N'
      OnClick = MO_ACTIVOClick
    end
    object GroupBox1: TGroupBox
      Left = 19
      Top = 95
      Width = 194
      Height = 82
      Caption = ' Impuestos '
      TabOrder = 4
      object ExentaLbl: TLabel
        Left = 24
        Top = 20
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Parte E&xenta:'
      end
      object MO_IMP_CALLbl: TLabel
        Left = 13
        Top = 60
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'I&SPT Individual:'
      end
      object MO_PER_CAL: TDBCheckBox
        Left = 37
        Top = 38
        Width = 66
        Height = 17
        Alignment = taLeftJustify
        Caption = '&Individual:'
        DataField = 'MO_PER_CAL'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = MO_PER_CALClick
      end
      object MO_X_ISPT: TZetaDBNumero
        Left = 90
        Top = 16
        Width = 92
        Height = 21
        Mascara = mnPesos
        TabOrder = 0
        Text = '0.00'
        DataField = 'MO_X_ISPT'
        DataSource = DataSource
      end
      object MO_IMP_CAL: TZetaDBNumero
        Left = 90
        Top = 56
        Width = 92
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        DataField = 'MO_IMP_CAL'
        DataSource = DataSource
      end
    end
    object MO_MONTO: TZetaDBNumero
      Left = 109
      Top = 29
      Width = 92
      Height = 21
      Mascara = mnPesos
      TabOrder = 1
      Text = '0.00'
      DataField = 'MO_MONTO'
      DataSource = DataSource
    end
    object MO_REFEREN: TDBEdit
      Left = 109
      Top = 52
      Width = 92
      Height = 21
      DataField = 'MO_REFEREN'
      DataSource = DataSource
      TabOrder = 2
    end
    object CO_NUMERO: TZetaDBKeyLookup_DevEx
      Left = 109
      Top = 6
      Width = 265
      Height = 21
      Filtro = 'CO_NUMERO < 1000'
      LookupDataset = dmCatalogos.cdsConceptos
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 30
      DataField = 'CO_NUMERO'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 425
    Top = 4
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
