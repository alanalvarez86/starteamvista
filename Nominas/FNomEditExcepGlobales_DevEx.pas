unit FNomEditExcepGlobales_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZetaDBTextBox,
     ZetaNumero,
     ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TNomEditExcepGlobales_DevEx = class(TBaseEdicion_DevEx)
    CO_NUMERO: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    UsarLbl: TLabel;
    Usar: TDBRadioGroup;
    MG_MONTOLbl: TLabel;
    MG_MONTO: TZetaDBNumero;
    Label6: TLabel;
    US_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UsarClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    //FGrid: Boolean; //Si se mando llamar del Grid.
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  NomEditExcepGlobales_DevEx: TNomEditExcepGlobales_DevEx;

implementation

uses DNomina,
     DCatalogos,
     ZetaCommonLists,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

{ TNomEditExcepGlobales }

procedure TNomEditExcepGlobales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_NOM_EXCEP_GLOBALES;
     TipoValorActivo1 := stPeriodo;
     FirstControl := CO_NUMERO;
     HelpContext := H30323_Excepciones_globales;
     CO_NUMERO.LookupDataset := dmCatalogos.cdsConceptos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TNomEditExcepGlobales_DevEx.FormShow(Sender: TObject);
begin
     inherited;
{     if Inserting then
        FGrid := False
     else
         FGrid := True; }
end;

procedure TNomEditExcepGlobales_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     with dmNomina do
     begin
          cdsExcepGlobales.Conectar;
          DataSource.DataSet := cdsExcepGlobales;
     end;
end;

function TNomEditExcepGlobales_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Agregar' );
     end;
end;

function TNomEditExcepGlobales_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Borrar' );
     end;
end;

function TNomEditExcepGlobales_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar' );
     end;
end;

procedure TNomEditExcepGlobales_DevEx.HabilitaControles;
begin
     inherited;
     if Inserting then
     begin
          Usar.ItemIndex := 0;
          MG_MONTO.Text := '0.00';
     end
     else
     begin
          if zStrToBool( dmNomina.cdsExcepGlobales.FieldByName( 'MG_FIJO' ).AsString ) then
             Usar.ItemIndex := 0
          else
              Usar.ItemIndex := 1;
     end;
     UsarClick(Self);
end;

procedure TNomEditExcepGlobales_DevEx.UsarClick(Sender: TObject);
begin
     inherited;
     MG_MONTOlbl.Enabled := ( Usar.ItemIndex = 0 );
     MG_MONTO.Enabled := MG_MONTOLbl.Enabled;;
end;

procedure TNomEditExcepGlobales_DevEx.OKClick(Sender: TObject);
begin
     inherited;
{     if FGrid then
        Close
     else
     begin
          dmNomina.cdsExcepGlobales.Insert;
          HabilitaControles;
     end; }
end;

procedure TNomEditExcepGlobales_DevEx.SetEditarSoloActivos;
begin
     CO_NUMERO.EditarSoloActivos := TRUE;
end;

end.


