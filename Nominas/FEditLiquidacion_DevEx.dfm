inherited EditLiquidacion_DevEx: TEditLiquidacion_DevEx
  Left = 328
  Top = 228
  ActiveControl = FechaBaja
  Caption = 'Liquidaci'#243'n de Empleado'
  ClientHeight = 410
  ClientWidth = 469
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 374
    Width = 469
    TabOrder = 2
    DesignSize = (
      469
      36)
    inherited OK_DevEx: TcxButton
      Left = 305
      Top = 5
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 384
      Top = 5
      Cancel = True
    end
  end
  object PanelIdentifica: TPanel [1]
    Left = 0
    Top = 0
    Width = 469
    Height = 19
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter: TSplitter
      Left = 241
      Top = 0
      Width = 2
      Height = 19
    end
    object ValorActivo1: TPanel
      Left = 0
      Top = 0
      Width = 241
      Height = 19
      Align = alLeft
      Alignment = taLeftJustify
      BorderWidth = 8
      Caption = 'ValorActivo1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object ValorActivo2: TPanel
      Left = 243
      Top = 0
      Width = 226
      Height = 19
      Align = alClient
      Alignment = taRightJustify
      BorderWidth = 8
      Caption = 'ValorActivo2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object PageControl: TcxPageControl [2]
    Left = 0
    Top = 19
    Width = 469
    Height = 355
    Align = alClient
    TabOrder = 1
    TabStop = False
    Properties.ActivePage = tsLiquidacion
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 351
    ClientRectLeft = 4
    ClientRectRight = 465
    ClientRectTop = 24
    object tsLiquidacion: TcxTabSheet
      Caption = 'Liquidaci'#243'n'
      object Label6: TLabel
        Left = 78
        Top = 305
        Width = 74
        Height = 13
        Caption = 'Obser&vaciones:'
        FocusControl = NO_OBSERVA
        Transparent = True
      end
      object Label16: TLabel
        Left = 258
        Top = 8
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha de Baja:'
        FocusControl = FechaBaja
      end
      object GBHoras: TcxGroupBox
        Left = 14
        Top = 61
        Caption = ' Horas Pendientes '
        TabOrder = 2
        Height = 232
        Width = 138
        object Label13: TLabel
          Left = 34
          Top = 21
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ordi&narias:'
          FocusControl = NO_HORAS
        end
        object Label15: TLabel
          Left = 48
          Top = 44
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = '&Dobles:'
          FocusControl = NO_DOBLES
        end
        object Label5: TLabel
          Left = 50
          Top = 67
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = '&Triples:'
          FocusControl = NO_TRIPLES
        end
        object Label1: TLabel
          Left = 27
          Top = 113
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = '&Adicionales:'
          FocusControl = NO_ADICION
        end
        object Label2: TLabel
          Left = 6
          Top = 136
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prima Do&minical:'
          FocusControl = NO_HORA_PD
        end
        object Label7: TLabel
          Left = 48
          Top = 90
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ta&rdes:'
          FocusControl = NO_TARDES
        end
        object NO_HORAS: TZetaDBNumero
          Left = 86
          Top = 17
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 0
          Text = '0.00'
          DataField = 'NO_HORAS'
          DataSource = DataSource
        end
        object NO_DOBLES: TZetaDBNumero
          Left = 86
          Top = 40
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 1
          Text = '0.00'
          DataField = 'NO_DOBLES'
          DataSource = DataSource
        end
        object NO_TRIPLES: TZetaDBNumero
          Left = 86
          Top = 63
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 2
          Text = '0.00'
          DataField = 'NO_TRIPLES'
          DataSource = DataSource
        end
        object NO_TARDES: TZetaDBNumero
          Left = 86
          Top = 86
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 3
          Text = '0.00'
          DataField = 'NO_TARDES'
          DataSource = DataSource
        end
        object NO_ADICION: TZetaDBNumero
          Left = 86
          Top = 109
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 4
          Text = '0.00'
          DataField = 'NO_ADICION'
          DataSource = DataSource
        end
        object NO_HORA_PD: TZetaDBNumero
          Left = 86
          Top = 132
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 5
          Text = '0.00'
          DataField = 'NO_HORA_PD'
          DataSource = DataSource
        end
      end
      object GBVacaciones: TcxGroupBox
        Left = 158
        Top = 61
        Caption = ' Vacaciones '
        TabOrder = 3
        Height = 85
        Width = 297
        object Label3: TLabel
          Left = 53
          Top = 56
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = '&Saldo:'
          FocusControl = NO_DIAS_VA
        end
        object Label10: TLabel
          Left = 6
          Top = 18
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'os Anteriores:'
        end
        object Label11: TLabel
          Left = 28
          Top = 36
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o Actual:'
        end
        object lblIgual: TLabel
          Left = 152
          Top = 36
          Width = 6
          Height = 13
          Caption = '='
        end
        object Label9: TLabel
          Left = 6
          Top = 36
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = '(+)'
        end
        object Label14: TLabel
          Left = 6
          Top = 56
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = '(=)'
        end
        object VA_ANTERIORES: TZetaDBTextBox
          Left = 85
          Top = 16
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'VA_ANTERIORES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VA_ANTES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object VA_ACTUAL: TZetaDBTextBox
          Left = 85
          Top = 34
          Width = 65
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'VA_ACTUAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VA_ACTUAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object VA_FORMULA: TZetaDBTextBox
          Left = 162
          Top = 34
          Width = 127
          Height = 17
          AutoSize = False
          Caption = 'VA_FORMULA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VA_FORMULA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label25: TLabel
          Left = 167
          Top = 56
          Width = 54
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prima Vac.:'
          FocusControl = NO_DIAS_PV
        end
        object NO_DIAS_VA: TZetaDBNumero
          Left = 85
          Top = 52
          Width = 65
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 0
          Text = '0.00'
          DataField = 'NO_DIAS_VA'
          DataSource = DataSource
        end
        object NO_DIAS_PV: TZetaDBNumero
          Left = 224
          Top = 52
          Width = 65
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 1
          Text = '0.00'
          DataField = 'NO_DIAS_PV'
          DataSource = DataSource
        end
      end
      object GBAguinaldo: TcxGroupBox
        Left = 158
        Top = 147
        Caption = ' Aguinaldo '
        TabOrder = 4
        Height = 146
        Width = 297
        object Label4: TLabel
          Left = 170
          Top = 101
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = '&Proporcional:'
          FocusControl = NO_DIAS_AG
        end
        object Label8: TLabel
          Left = 160
          Top = 120
          Width = 6
          Height = 13
          Caption = '='
        end
        object AG_FORMULA: TZetaDBTextBox
          Left = 10
          Top = 118
          Width = 145
          Height = 17
          AutoSize = False
          Caption = 'AG_FORMULA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AG_FORMULA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object NO_ANT_AG: TZetaDBTextBox
          Left = 170
          Top = 10
          Width = 73
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'NO_ANT_AG'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_ANT_AG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblDiasPagados: TLabel
          Left = 81
          Top = 12
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'as ya Pagados:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label20: TLabel
          Left = 135
          Top = 48
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Faltas:'
        end
        object Label21: TLabel
          Left = 93
          Top = 66
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'Incapacidades:'
        end
        object NO_DIAS_FI: TZetaDBTextBox
          Left = 170
          Top = 46
          Width = 73
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'NO_DIAS_FI'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_DIAS_FI'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object NO_DIAS_IN: TZetaDBTextBox
          Left = 170
          Top = 64
          Width = 73
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'NO_DIAS_IN'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_DIAS_IN'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label17: TLabel
          Left = 95
          Top = 30
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'as en Curso:'
        end
        object NO_DIAS_CU: TZetaDBTextBox
          Left = 170
          Top = 28
          Width = 73
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'NO_DIAS_CU'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_DIAS_CU'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label19: TLabel
          Left = 73
          Top = 84
          Width = 93
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'as Considerados:'
        end
        object Label22: TLabel
          Left = 51
          Top = 48
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '(-)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 51
          Top = 66
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '(-)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label24: TLabel
          Left = 51
          Top = 84
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = '(=)'
        end
        object NO_DIAS_CO: TZetaDBTextBox
          Left = 170
          Top = 82
          Width = 73
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'NO_DIAS_CO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_DIAS_CO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object NO_DIAS_AG: TZetaDBNumero
          Left = 170
          Top = 116
          Width = 73
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 0
          Text = '0.00'
          DataField = 'NO_DIAS_AG'
          DataSource = DataSource
        end
      end
      object NO_LIQUIDA: TDBRadioGroup
        Left = 14
        Top = 23
        Width = 439
        Height = 39
        Caption = ' Tipo '
        Columns = 2
        DataField = 'NO_LIQUIDA'
        DataSource = DataSource
        Items.Strings = (
          '&Liquidaci'#243'n'
          '&Indemnizaci'#243'n')
        TabOrder = 1
        TabStop = True
        Values.Strings = (
          '0'
          '1')
        OnClick = NO_LIQUIDAClick
      end
      object NO_OBSERVA: TZetaDBEdit
        Left = 158
        Top = 301
        Width = 297
        Height = 21
        TabOrder = 5
        Text = 'NO_OBSERVA'
        DataField = 'NO_OBSERVA'
        DataSource = DataSource
      end
      object FechaBaja: TZetaFecha
        Left = 337
        Top = 3
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '26/Sep/00'
        Valor = 36795.000000000000000000
        OnValidDate = FechaBajaValidDate
      end
    end
    object tsClasificacion: TcxTabSheet
      Caption = 'Antig'#252'edad'
      ImageIndex = 1
      object CB_FEC_INGlbl: TLabel
        Left = 63
        Top = 89
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ingreso:'
      end
      object CB_PUESTOlbl: TLabel
        Left = 65
        Top = 146
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object CB_HORARIOlbl: TLabel
        Left = 70
        Top = 165
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_FEC_ING: TZetaDBTextBox
        Left = 104
        Top = 87
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'CB_FEC_ING'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_FEC_ING'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label18: TLabel
        Left = 12
        Top = 108
        Width = 89
        Height = 13
        Caption = 'Antig'#252'edad desde:'
      end
      object CB_FEC_ANT: TZetaDBTextBox
        Left = 104
        Top = 106
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'CB_FEC_ANT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_FEC_ANT'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZAntiguedad: TZetaTextBox
        Left = 188
        Top = 106
        Width = 151
        Height = 17
        AutoSize = False
        Caption = 'ZAntiguedad'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object CB_PUESTO: TZetaDBTextBox
        Left = 104
        Top = 144
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'CB_PUESTO'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_PUESTO'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_TURNO: TZetaDBTextBox
        Left = 104
        Top = 163
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'CB_TURNO'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_TURNO'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LBaja: TLabel
        Left = 44
        Top = 127
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Baja:'
      end
      object CB_FEC_BAJ: TZetaDBTextBox
        Left = 104
        Top = 125
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'CB_FEC_BAJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_FEC_BAJ'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object TU_DESCRIP: TZetaTextBox
        Left = 188
        Top = 163
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL1'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object PU_DESCRIP: TZetaTextBox
        Left = 188
        Top = 144
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL1'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
    end
    object tsArea: TcxTabSheet
      Caption = 'Niveles'
      ImageIndex = 2
      object CB_NIVEL1lbl: TLabel
        Left = 122
        Top = 49
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 122
        Top = 68
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 122
        Top = 88
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 122
        Top = 107
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 122
        Top = 127
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 122
        Top = 146
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 122
        Top = 165
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 122
        Top = 185
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 122
        Top = 204
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object ZNIVEL1: TZetaTextBox
        Left = 224
        Top = 47
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL1'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL2: TZetaTextBox
        Left = 224
        Top = 66
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL2'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL3: TZetaTextBox
        Left = 224
        Top = 86
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL3'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL4: TZetaTextBox
        Left = 224
        Top = 105
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL4'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL5: TZetaTextBox
        Left = 224
        Top = 125
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL5'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL6: TZetaTextBox
        Left = 224
        Top = 144
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL6'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL7: TZetaTextBox
        Left = 224
        Top = 163
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL7'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL8: TZetaTextBox
        Left = 224
        Top = 183
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL8'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL9: TZetaTextBox
        Left = 224
        Top = 202
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL9'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL5: TZetaDBTextBox
        Left = 161
        Top = 125
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL5'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL5'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL2: TZetaDBTextBox
        Left = 161
        Top = 66
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL2'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL2'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL3: TZetaDBTextBox
        Left = 161
        Top = 86
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL3'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL3'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL4: TZetaDBTextBox
        Left = 161
        Top = 105
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL4'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL4'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL1: TZetaDBTextBox
        Left = 161
        Top = 47
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL1'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL1'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL6: TZetaDBTextBox
        Left = 161
        Top = 144
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL6'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL6'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL7: TZetaDBTextBox
        Left = 161
        Top = 163
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL7'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL7'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL8: TZetaDBTextBox
        Left = 161
        Top = 183
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL8'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL8'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL9: TZetaDBTextBox
        Left = 161
        Top = 202
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL9'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL9'
        DataSource = dsEmpleado
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL10lbl: TLabel
        Left = 116
        Top = 224
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL10: TZetaDBTextBox
        Left = 161
        Top = 222
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL10'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL10'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL10: TZetaTextBox
        Left = 224
        Top = 222
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL10'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 116
        Top = 244
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL11: TZetaDBTextBox
        Left = 161
        Top = 242
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL11'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL11'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL11: TZetaTextBox
        Left = 224
        Top = 242
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL11'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 116
        Top = 264
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL12: TZetaDBTextBox
        Left = 161
        Top = 262
        Width = 60
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL12'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
        DataField = 'CB_NIVEL12'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL12: TZetaTextBox
        Left = 224
        Top = 262
        Width = 230
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL12'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource: TDataSource
    Left = 157
    Top = 42
  end
  object dsEmpleado: TDataSource
    Left = 172
    Top = 51
  end
end
