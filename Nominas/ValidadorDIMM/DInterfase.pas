unit DInterfase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet, ZetaCommonLists,
  {$ifndef VER130}
  Variants,
  {$endif}
  {$ifdef DOS_CAPAS}
  DServerCatalogos,
  DServerAnualNomina,
  DServerConsultas,
  {$else}
  Catalogos_TLB,
  DAnualNomina_TLB,
  Consultas_TLB,
  {$endif}
  ZetaCommonClasses,
  ZetaServerDataSet,
  ZetaClientTools,
  IniFiles,
  DExcelAppender,
  ZetaDialogo,
  Excel2000,
  ExcelXP;

type
    eTipoConfiConcepto = ( tcConfiPercepcion, tcConfiDeduccion, tcConfiMensual, tcConfiPTU, tcConfiAguinaldo, tcConfiPrimaVacacional,
                           tcConfiPagoxSepara, tcConfiTopeNegativo, tcConfiGuardaPagoxSepara, tcNegativosExcluidos );
    TdmInterfase = class(TDataModule)
    cdsConceptos: TZetaLookupDataSet;
    cdsDataSet: TZetaClientDataSet;
    cdsTotalesEmpleados: TServerDataSet;
    cdsEmpleados: TZetaClientDataSet;
    cdsSalMin: TZetaClientDataSet;
    cdsNegativos: TZetaClientDataSet;
    cdsErrores: TServerDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsConceptosAlAdquirirDatos(Sender: TObject);
    procedure cdsConceptosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEmpleadosAlAdquirirDatos(Sender: TObject);
    procedure cdsSalMinAlAdquirirDatos(Sender: TObject);
    procedure cdsNegativosAlAdquirirDatos(Sender: TObject);

    private
    { Private declarations }

    FConfiguracionIni : TIniFile;
    FFechaSalMin: TDate;
    FParamYear: Integer;
    FArchivoBitacora: String;

    {$ifdef DOS_CAPAS}

    function GetServerConsultas: TdmServerConsultas;
    function GetServerCatalogos : TdmServerCatalogos;
    function GetServerAnualNomina: TdmServerAnualNomina;
    property ServerCatalogo :TdmServerCatalogos read GetServerCatalogos;
    property ServerAnualNomina :TdmServerAnualNomina read GetServerAnualNomina;
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
    {$else}

    FServidor : IdmServerCatalogosDisp;
    FServidorAnualNomina: IdmServerAnualNominaDisp;
    FServerConsultas: IdmServerConsultasDisp;

    function GetServerCatalogos : IdmServerCatalogosDisp ;
    function GetServerAnualNomina: IdmServerAnualNominaDisp;
    function GetServerConsultas: IdmServerConsultasDisp;

    property ServerCatalogo : IdmServerCatalogosDisp read GetServerCatalogos;
    property ServerAnualNomina :IdmServerAnualNominaDisp read GetServerAnualNomina;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    {$endif}
    function GetDirectorioIni: String;
    procedure Recalcula( Parametros: TZetaParams; const lActualiza: Boolean );
    procedure PreparaCalculo;
    procedure ActualizaTotales( Parametros: TZetaParams );
    procedure ExportaBitacora;
    procedure GetPercepcionesNegativas( const sListaPercep: String );
    public
      { Public declarations }
      FConfiConceptos: array[ eTipoConfiConcepto ] of String;
      property FechaSalMin: TDate read FFechaSalMin write FFechaSalMin;
      property ArchivoBitacora: String read FArchivoBitacora write FArchivoBitacora;
      function GetPathBitacora: String;
      procedure CargaConceptosTipo;
      procedure ActualizaArchivoIni;
      procedure Procesa( const lActualiza: Boolean; Parametros: TZetaParams );
    end;

    TdmExcelDeclara = class( TExcelAppender )
    private
           {private}
           procedure InsertFormatoCelda( iColumna, iMaximo: Integer );
    protected
        function SetHojaEncabezado(oListaCampos: TStrings; const iRow, iCol, iMaximo: Integer; const lFormatCelda: Boolean): Boolean; override;
        procedure AgregaSheets; override;

    public

    end;



var
  dmInterfase: TdmInterfase;

const
     { Constantes archivo ini}
     K_ARCHIVO_INI = 'VALIDADORDIMM.INI';
     K_CONFIGURACION = 'CONFIGURACION';
     K_LISTA_PERCEP = 'PERCEPCIONES';
     K_LISTA_DEDUCC = 'DEDUCCIONES';
     K_LISTA_MENSUAL = 'MENSUAL';
     K_LISTA_PTU = 'PTU';
     K_LISTA_AGUINALDO = 'AGUINALDO';
     K_LISTA_PRIMA_VACA = 'PRIMA';
     K_LISTA_T_PAGO_SEPARA = 'TOMA_PAGO_SEPARA';
     K_LISTA_G_PAGO_SEPARA = 'GUARDA_PAGO_SEPARA';
     K_NEGATIVOS_EXCLUIDOS = 'EXCLUIR_NEGATIVOS';
     K_ARCHIVO_BIT = 'BITACORA';

     {Constantes defaults}
     {K_DEFAULT_AQUINALDO = '9';
     K_DEFAULT_PRIMAVACA = '6';
     K_DEFAULT_PTU = '14'; }
     K_TOLERANCIA_DEFAULT = 1.00;
     K_NOMBRE_BITACORA = 'BitValidadorDIMM.xls';


implementation

uses DCliente,
     ZReconcile,
     ZetaCommonTools,
     FTressShell;


{$R *.dfm}

procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     FConfiguracionIni := TIniFile.Create( GetDirectorioIni );

end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil(FConfiguracionIni);
end;

{$ifdef DOS_CAPAS}
function TdmInterfase.GetServerCatalogos: TdmServerCatalogos;
begin
     Result:= DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmInterfase.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result:= IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
{$endif}

{$ifdef DOS_CAPAS}
function TdmInterfase.GetServerAnualNomina: TdmServerAnualNomina;
begin
     Result:= DCliente.dmCliente.ServerAnualNomina;
end;
{$else}
function TdmInterfase.GetServerAnualNomina: IdmServerAnualNominaDisp;
begin
     Result:= IdmServerAnualNominaDisp( dmCliente.CreaServidor( CLASS_dmServerAnualNomina, FServidorAnualNomina ) );
end;
{$endif}

{$ifdef DOS_CAPAS}
function TdmInterfase.GetServerConsultas: TdmServerConsultas;
begin
     Result:= DCliente.dmCliente.ServerConsultas;
end;
{$else}
function TdmInterfase.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result:= IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;
{$endif}


procedure TdmInterfase.cdsConceptosAlAdquirirDatos(Sender: TObject);
begin
     cdsConceptos.Data := ServerCatalogo.GetConceptos( dmCliente.Empresa );
end;

procedure TdmInterfase.cdsConceptosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights:= False;
end;

function TdmInterfase.GetDirectorioIni : String;
begin
     Result := ExtractFilePath( Application.ExeName ) + K_ARCHIVO_INI;
end;

procedure TdmInterfase.Procesa( const lActualiza: Boolean; Parametros: TZetaParams );
begin
     Recalcula( Parametros, lActualiza );
     ExportaBitacora;
    // ActualizaArchivoIni;
end;

procedure TdmInterfase.CargaConceptosTipo;
var
   sSeccion: String;
begin
     { Defaults tomados de CONCEPTO.DB }
     sSeccion:= dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION;

     with FConfiguracionIni do
     begin
          FConfiConceptos[ tcConfiAguinaldo ] := ReadString( sSeccion, K_LISTA_AGUINALDO, VACIO );
          FConfiConceptos[ tcConfiPrimaVacacional ] := ReadString( sSeccion, K_LISTA_PRIMA_VACA, VACIO );
          FConfiConceptos[ tcConfiPTU ] :=  ReadString( sSeccion, K_LISTA_PTU, VACIO );
          FConfiConceptos[ tcConfiPagoxSepara ]:= ReadString( sSeccion, K_LISTA_T_PAGO_SEPARA, VACIO );
          FConfiConceptos[ tcConfiGuardaPagoxSepara ]:= ReadString( sSeccion, K_LISTA_G_PAGO_SEPARA, VACIO );
          FConfiConceptos[ tcNegativosExcluidos ]:= ReadString( sSeccion, K_NEGATIVOS_EXCLUIDOS, VACIO );


          if ( SectionExists( sSeccion ) ) then
          begin
               FConfiConceptos[ tcConfiPercepcion ] := ReadString( sSeccion, K_LISTA_PERCEP, VACIO );
               //FConfiConceptos[ tcConfiDeduccion ] := ReadString( sSeccion, K_LISTA_DEDUCC, VACIO );
               FConfiConceptos[ tcConfiMensual ] :=  ReadString( sSeccion, K_LISTA_MENSUAL, VACIO );
          end
          else
          begin
               FConfiConceptos[ tcConfiPercepcion ] := VACIO;
               FConfiConceptos[ tcConfiMensual ] := VACIO;
               with cdsConceptos do
               begin
                    Conectar;
                    Last;
                    while not BOF do
                    begin
                         { AP: Se requieren todos los conceptos activos y no activos
                         if ( zStrToBool( FieldByName('CO_ACTIVO').AsString ) ) then }
                        // begin
                         case eTipoConcepto ( FieldByName('CO_TIPO').AsInteger ) of
                              coPercepcion:
                              begin
                                   FConfiConceptos[ tcConfiPercepcion ] := ConcatString( FieldByName('CO_NUMERO').AsString, FConfiConceptos[ tcConfiPercepcion ], ',' );
                                   if zStrToBool( FieldByName('CO_MENSUAL').AsString ) then
                                   begin
                                        FConfiConceptos[ tcConfiMensual ] := ConcatString( FieldByName('CO_NUMERO').AsString, FConfiConceptos[ tcConfiMensual ], ',' );
                                   end;
                              end;
                              {  AP: Se elimina de la configuración las deducciones
                              coDeduccion:
                              begin
                                   FConfiConceptos[ tcConfiDeduccion ] := ConcatString( FieldByName('CO_NUMERO').AsString, FConfiConceptos[ tcConfiDeduccion ], ',' );
                              end;}
                         end;
                       //  end;
                         Prior;
                    end;
               end;
          end;
     end;
end;


procedure TdmInterfase.ActualizaArchivoIni;
begin
    {Actualiza archivo INI }
    with FConfiguracionIni do
    begin
         WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_LISTA_PERCEP, FConfiConceptos[ tcConfiPercepcion ] );
         //WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_LISTA_DEDUCC, FConfiConceptos[ tcConfiDeduccion ] );
         WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_LISTA_MENSUAL, FConfiConceptos[ tcConfiMensual ] );
         WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_LISTA_PTU, FConfiConceptos[ tcConfiPTU ] );
         WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_LISTA_AGUINALDO, FConfiConceptos[ tcConfiAguinaldo ] );
         WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_LISTA_PRIMA_VACA, FConfiConceptos[ tcConfiPrimaVacacional ] );
         WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_LISTA_T_PAGO_SEPARA, FConfiConceptos[ tcConfiPagoxSepara ] );
         WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_LISTA_G_PAGO_SEPARA, FConfiConceptos[ tcConfiGuardaPagoxSepara ] );
         WriteString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_NEGATIVOS_EXCLUIDOS, FConfiConceptos[ tcNegativosExcluidos ] );
    end;
end;

procedure TdmInterfase.PreparaCalculo;
begin
     with cdsTotalesEmpleados do
     begin
          InitTempDataset;
          AddIntegerField('EMPLEADO');
          AddStringField( 'NOMBRE', K_ANCHO_DESCRIPCION );
          AddFloatField( 'TOT_PER' );
          AddFloatField( 'C_TOT_PER' );
          {AddFloatField( 'TOT_DED' );
          AddFloatField( 'C_TOT_DED' ); }
          AddFloatField('EXENTO_INDIVIDUAL');
          AddFloatField( 'C_EXENTO_INDIVIDUAL' );
          AddFloatField('EXENTO_MENSUAL');
          AddFloatField( 'C_EXENTO_MENSUAL' );
          AddFloatField('EXENTO_ISPT');
          AddFloatField( 'C_EXENTO_ISPT' );
         { AddFloatField( 'TOT_MENS' );
          AddFloatField( 'C_TOT_MENS' );   }
          AddFloatField( 'PAGOS_X_SEPARA');
          AddFloatField( 'C_PAGOS_X_SEPARA' );
          CreateTempDataset;
     end;

     cdsEmpleados.Refrescar;
     cdsSalMin.Refrescar;

end;

procedure TdmInterfase.Recalcula( Parametros: TZetaParams; const lActualiza: Boolean );
var
   rSalarioMinimo: TPesos;
   {$ifndef DOS_CAPAS}
   oServer: IdmServerAnualNominaDisp;
   {$else}
   oServer: TdmServerAnualNomina;
   {$endif}

    function GetSalarioMinimo( const sZona: String): Double;
    begin
         Result:= 0;
         with cdsSalMin do
         begin
              if ( sZona = K_T_ZONA_A ) then
                 Result:= FieldByName('SM_ZONA_A').AsFloat
              else if ( sZona = K_T_ZONA_B ) then
                  Result:= FieldByName('SM_ZONA_B').AsFloat
              else if ( sZona = K_T_ZONA_C ) then
                   Result:= FieldByName('SM_ZONA_C').AsFloat;
         end;
    end;

    function MontosDiferentes( rValorTress, rValorCalculado: Double; const sConceptos: String ): Boolean;
    begin
         Result:= StrLleno( sConceptos ) and not PesosIgualesP( rValorTress, rValorCalculado, Parametros.ParamByName('Tolerancia').AsFloat );
    end;

    function HayMontosDiferentes: Boolean;
    begin
         with cdsDataSet, Parametros do
         begin
              Result:= ( MontosDiferentes( FieldByName('TOT_PER').AsFloat, FieldByName('C_TOT_PER').AsFloat, ParamByName('CoPercepciones').AsString ) )  or
                       //( MontosDiferentes( FieldByName('TOT_DED').AsFloat, FieldByName('C_TOT_DED').AsFloat, ParamByName('CoDeducciones').AsString ) ) or
                       //( MontosDiferentes( FieldByName('TOT_MENS').AsFloat, FieldByName('C_TOT_MENS').AsFloat, ParamByName('CoPerMens').AsString ) ) or
                       ( MontosDiferentes( FieldByName('EXENTO_INDIVIDUAL').AsFloat, FieldByName('C_EXENTO_INDIVIDUAL').AsFloat,
                                            ParamByName('CoPTU').AsString + ParamByName('CoAguinaldo').AsString + ParamByName('CoPrimaVaca').AsString ) ) or
                       ( MontosDiferentes( FieldByName('EXENTO_MENSUAL').AsFloat, FieldByName('C_EXENTO_MENSUAL').AsFloat,
                                            ParamByName('CoPTU').AsString + ParamByName('CoAguinaldo').AsString + ParamByName('CoPrimaVaca').AsString ) ) or
                       ( MontosDiferentes( FieldByName('EXENTO_ISPT').AsFloat, FieldByName('C_EXENTO_ISPT').AsFloat,
                                            ParamByName('CoPTU').AsString + ParamByName('CoAguinaldo').AsString + ParamByName('CoPrimaVaca').AsString ) ) or
                       ( MontosDiferentes( FieldByName('PAGO_X_SEPARA').AsFloat, FieldByName('C_PAGO_X_SEPARA').AsFloat, ParamByName('TomaPagoSepara').AsString ) ) ;

         end;
    end;

    function GetPrimerConcepto( const sConceptos: String ): Integer;
    var
       iPos: Integer;
    begin
         { Toma solamente la configuración del primer concepto para la configuración}
         iPos := Pos(',', sConceptos);
         if ( iPos > 0 ) then
              Result := StrToIntDef( Copy( sConceptos, 0, ( iPos - 1 ) ), 0 )
         else
              {Un elemento solamente}
             Result:= StrToIntDef( sConceptos, 0 );

    end;

    procedure SumaExento( const sListaConceptos: String; rValorCTope: Double );
    begin
         with cdsDataSet do
         begin
              if cdsConceptos.Locate('CO_NUMERO',GetPrimerConcepto( sListaConceptos ),[]) then
              begin
                   if not ( State in [dsEdit, dsInsert] ) then
                      Edit;
                   if ( StrLleno( cdsConceptos.FieldByName('CO_IMP_CAL').AsString ) ) then
                      FieldByName('C_EXENTO_INDIVIDUAL').AsFloat:= FieldByName('C_EXENTO_INDIVIDUAL').AsFloat + rValorCTope
                   else if zStrToBool( cdsConceptos.FieldByName('CO_MENSUAL').AsString ) then
                      FieldByName('C_EXENTO_MENSUAL').AsFloat:= FieldByName('C_EXENTO_MENSUAL').AsFloat + rValorCTope
                   else
                       FieldByName('C_EXENTO_ISPT').AsFloat:= FieldByName('C_EXENTO_ISPT').AsFloat + rValorCTope;

                       
                   Post;
              end;
         end;
    end;

begin
     { Recalcula }
     FParamYear:= Parametros.ParamByName('Year').AsInteger;
     //FPrimerFechaYear:= FirstDayOfYear( Parametros.ParamByName('Year').AsInteger );
     PreparaCalculo;

     with TressShell do
     begin
          RecalculoStart( cdsEmpleados.RecordCount + 10, 'Recalculando:' );
          cdsEmpleados.First;
          {$ifndef DOS_CAPAS}
          dmCliente.CreateFixedServer( CLASS_dmServerAnualNomina, oServer );
          {$else}
          oServer:= ServerAnualNomina;
          {$endif}
          try
             while ( not cdsEmpleados.Eof and RecalculoStep( cdsEmpleados.FieldByName('CB_CODIGO').AsInteger ) )do
             begin
                  Parametros.AddInteger( 'Empleado', cdsEmpleados.FieldByName('CB_CODIGO').AsInteger );
                  cdsDataSet.Data:= oServer.RecalculaTotPerGetLista( dmCliente.Empresa, Parametros.VarValues );

                  if not ( cdsDataSet.IsEmpty ) then
                  begin
                       with cdsTotalesEmpleados do
                       begin
                            rSalarioMinimo:= GetSalarioMinimo( cdsEmpleados.FieldByName('CB_ZONA_GE').AsString );

                            with Parametros do
                            begin
                                 SumaExento(ParamByName('CoPTU').AsString, rMax( rMin( cdsDataSet.FieldByName('C_PTU').AsFloat, 15 * rSalarioMinimo ), 0 ) );
                                 SumaExento(ParamByName('CoAguinaldo').AsString, rMax( rMin( cdsDataSet.FieldByName('C_AGUINALDO').AsFloat, 30 * rSalarioMinimo ), 0 ) );
                                 SumaExento(ParamByName('CoPrimaVaca').AsString, rMax( rMin( cdsDataSet.FieldByName('C_PRIMA_VACA').AsFloat, 15 * rSalarioMinimo ), 0 ) );
                            end;

                            if ( HayMontosDiferentes ) then
                            begin
                                 Append;
                                 FieldByName('EMPLEADO').AsInteger:= cdsDataSet.FieldByName('CB_CODIGO').AsInteger;
                                 FieldByName('NOMBRE').AsString:= cdsDataSet.FieldByName('PrettyName').AsString;
                                 FieldByName( 'TOT_PER' ).AsFloat:= cdsDataSet.FieldByName('TOT_PER').AsFloat;
                                 FieldByName( 'C_TOT_PER' ).AsFloat:= cdsDataSet.FieldByName('C_TOT_PER').AsFloat;
                               {  FieldByName( 'TOT_DED' ).AsFloat:= cdsDataSet.FieldByName('TOT_DED').AsFloat;
                                 FieldByName( 'C_TOT_DED' ).AsFloat:= cdsDataSet.FieldByName('C_TOT_DED').AsFloat;
                                 FieldByName( 'TOT_MENS' ).AsFloat:= cdsDataSet.FieldByName('TOT_MENS').AsFloat;
                                 FieldByName( 'C_TOT_MENS' ).AsFloat:= cdsDataSet.FieldByName('C_TOT_MENS').AsFloat; }

                                 FieldByName('EXENTO_INDIVIDUAL').AsFloat:= cdsDataSet.FieldByName('EXENTO_INDIVIDUAL').AsFloat;
                                 FieldByName('C_EXENTO_INDIVIDUAL').AsFloat:= rMax( cdsDataSet.FieldByName('C_EXENTO_INDIVIDUAL').AsFloat, 0 );

                                 FieldByName('EXENTO_MENSUAL').AsFloat:= cdsDataSet.FieldByName('EXENTO_MENSUAL').AsFloat;
                                 FieldByName('C_EXENTO_MENSUAL').AsFloat:= rMax( cdsDataSet.FieldByName('C_EXENTO_MENSUAL').AsFloat, 0 );

                                 FieldByName('EXENTO_ISPT').AsFloat:= cdsDataSet.FieldByName('EXENTO_ISPT').AsFloat;
                                 FieldByName('C_EXENTO_ISPT').AsFloat:= rMax( cdsDataSet.FieldByName('C_EXENTO_ISPT').AsFloat, 0 );

                                 FieldByName('PAGOS_X_SEPARA').AsFloat:= cdsDataSet.FieldByName('PAGO_X_SEPARA').AsFloat;
                                 FieldByName('C_PAGOS_X_SEPARA').AsFloat:= cdsDataSet.FieldByName('C_PAGO_X_SEPARA').AsFloat;
                                 Post;
                            end;
                       end;
                  end;
                  cdsEmpleados.Next;
             end;
             GetPercepcionesNegativas ( Parametros.ParamByName('CoPercepciones').AsString );
             if lActualiza then
             begin
                  RecalculoStep( 0, 'Actualizando Totales' );
                  ActualizaTotales ( Parametros );
             end;

             RecalculoEnd;
          finally
                 {$ifndef DOS_CAPAS}
                 dmCliente.DestroyFixedServer(oServer);
                 {$endif}
          end;
     end;

end;

procedure TdmInterfase.ActualizaTotales( Parametros: TZetaParams );
begin
     {Actualiza }
     ServerAnualNomina.ActualizaTotPerLista( dmCliente.Empresa, cdsTotalesEmpleados.Data, Parametros.VarValues  );
end;


function TdmInterfase.GetPathBitacora: String;
begin
     Result:= FConfiguracionIni.ReadString( dmCliente.Empresa[P_CODIGO] + '-' + K_CONFIGURACION, K_ARCHIVO_BIT, ExtractFilePath( Application.ExeName ) +
                                                                                                                dmCliente.Empresa[P_CODIGO]+
                                                                                                                K_NOMBRE_BITACORA );
end;

procedure TdmInterfase.cdsEmpleadosAlAdquirirDatos(Sender: TObject);
const
     {$ifdef INTERBASE}
           QRY_EMPLEADOS = 'select CB_CODIGO,('+K_PRETTYNAME+')as PRETTYNAME, CB_FEC_BAJ, CB_FEC_ING, CB_ZONA_GE from COLABORA ' +
                           'where ( CB_ACTIVO = ''S'' ) or ( CB_FEC_BAJ >= %s )';
     {$endif}
     {$ifdef MSSQL}
           QRY_EMPLEADOS = 'select CB_CODIGO,PRETTYNAME,CB_FEC_BAJ, CB_FEC_ING, CB_ZONA_GE from COLABORA '+
                           'where ( CB_ACTIVO = ''S'' ) or ( CB_FEC_BAJ >= %s )';
     {$endif}
begin
     cdsEmpleados.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( QRY_EMPLEADOS,[ EntreComillas( FechaAsStr( FirstDayOfYear( FParamYear ) ) )] ) );
end;

procedure TdmInterfase.cdsSalMinAlAdquirirDatos(Sender: TObject);
const
     QRY_SAL_MIN = 'select SM_FEC_INI, SM_ZONA_A, SM_ZONA_B, SM_ZONA_C from SAL_MIN where  SM_FEC_INI <= %s  order by SM_FEC_INI desc';
begin
     cdsSalMin.Data:= ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( QRY_SAL_MIN, [ EntreComillas( FechaAsStr( FFechaSalMin ) )]) );
end;

procedure TdmInterfase.ExportaBitacora;
 var
    oExcelAppender: TdmExcelDeclara;
    oListaCampos: TStrings;
    miworkbook: _WorkBook;



    procedure AgregaListaDiferencias;
    var
       i: Integer;

       procedure AgregaTressCalculado;
       begin
            with oListaCampos do
            begin
                 Add( 'Tress' );
                 Add( 'Calculado' );
            end;
       end;

    begin
         with oListaCampos do
         begin
              Clear;
              Add('Número');
              Add('Nombre');

              for i:= 0 to 4 do
              begin
                   AgregaTressCalculado;
              end;
         end;
    end;

    procedure AgregaListaPercepNegativas;
    begin
         with oListaCampos do
         begin
              Clear;
              Add('Empleado');
              Add('Concepto');
              Add('Monto');
         end;
    end;

begin
     oExcelAppender := TdmExcelDeclara.Create( Self );
     oListaCampos:= TStringList.Create;
     try
        with oExcelAppender do
        begin
             if not ( FileExists( ArchivoBitacora ) ) then
             begin
                  with ExcelApp do
                  begin
                       miworkbook := Workbooks.Add(xlWBATWorksheet,0);
                       miworkbook._SaveAs(ArchivoBitacora, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, xlNoChange, EmptyParam, EmptyParam, EmptyParam, EmptyParam, 0);
                  end;

             end;

             if not ( cdsTotalesEmpleados.IsEmpty ) then
             begin
                  ExcelApp.DisplayAlerts[0]:= FALSE;
                  try
                     if AbreArchivoBase( ArchivoBitacora ) then
                     begin
                          try
                             AgregaListaPercepNegativas;
                             if not VaciaDSWorkSheet(cdsNegativos, 'Percepciones Negativas', 1, 1, oListaCampos ) then
                                ZError('Error',GetErrorMessage,0);
                          except on Error: Exception do
                                 zError( 'Error', 'No fue posible generar reporte en Excel de Percepciones Negativas' + Error.Message, 0);
                          end;

                          try
                             AgregaListaDiferencias;
                             if not VaciaDSWorkSheet(cdsTotalesEmpleados, 'Diferencias', 4, 1, oListaCampos, TRUE ) then
                                ZError('Error',GetErrorMessage,0);
                          except on Error: Exception do
                                 zError( 'Error', ' No fue posible generar reporte en Excel de Diferencias' + Error.Message, 0);
                          end;

                          //

                          if ZetaDialogo.zConfirm( '¡ Proceso Terminado !', '¿ Desea Ver La Bitácora Del Proceso ?', 0, mbOk ) then
                          begin
                               RefrescarWorkBook;
                               ShowExcelApp;
                          end
                          else
                          begin
                               ExcelApp.DisplayAlerts[0]:= FALSE;
                               ExcelApp.ActiveWorkBook.Close(True,ArchivoBitacora,EmptyParam,0);
                               ExcelApp.Quit;
                          end;

                     end
                     else
                         ZError('Error',GetErrorMessage,0);
                     //end
                  finally
                         ExcelApp.DisplayAlerts[0]:= TRUE;
                  end
             end
             else
                 ZetaDialogo.zInformation( ' Validador DIMM ', '¡ No se encontraron diferencias que reportar ! ' , 0 );
        end;

     finally
            FreeAndNil( oListaCampos );
            FreeAndNil( oExcelAppender );
     end;

end;



function TdmExcelDeclara.SetHojaEncabezado(oListaCampos: TStrings; const iRow, iCol, iMaximo: Integer; const lFormatCelda: Boolean): Boolean;

   procedure EscribeEncabezadoColumna( iColumna: Integer; const sTexto: String );
   begin
        with ExcelSheet.Cells do
        begin
             Item[ iRow - 2, iColumna ].Value:= sTexto;
        end;

        if lFormatCelda then
           InsertFormatoCelda( iColumna + 1, iMaximo + 5 );
   end;

begin
     Result:= True;
     if ( ExcelSheet.Name = 'Diferencias' ) then
     begin
          try
             EscribeEncabezadoColumna( 3, 'Percepciones (1000)' );
             //EscribeEncabezadoColumna( 5, 'Deducciones' );
             EscribeEncabezadoColumna( 5, 'Exento ISPT Individual (1009)' );
             EscribeEncabezadoColumna( 7, 'Exento Mensual (1006)' );
             EscribeEncabezadoColumna( 9, 'Exento ISPT (1003)');
            // EscribeEncabezadoColumna( 13, 'Percep. Mensuales');
             EscribeEncabezadoColumna( 11,'Pagos x Separación');

             inherited SetHojaEncabezado( oListaCampos, iRow - 1, iCol, iMaximo, lFormatCelda);

             except
                   on Error: Exception do
                   begin
                        FErrorMessage := Error.Message;
                        Result := False;
                   end;
             end;
     end
     else inherited SetHojaEncabezado (oListaCampos, iRow, iCol, iMaximo, lFormatCelda);
end;

procedure TdmExcelDeclara.AgregaSheets;
var
   miWorkSheet: _Worksheet;

   function ExisteSheet( const sNombre: String ): Boolean;
   var
      i: Integer;
   begin
        Result:= False;
        with ExcelApp do
        begin
             for i:= 1 to Sheets.Count  do
             begin
                  miWorkSheet:= Sheets.Item[i] as _Worksheet;
                  Result:= ( miWorkSheet.Name = sNombre );
                  if Result then
                     Exit;
             end;
        end;
   end;

begin
     with ExcelApp do
     begin
          if not ExisteSheet( 'Percepciones Negativas' ) then
          begin
               miWorkSheet:= Sheets.Add( EmptyParam,EmptyParam,1,xlWorksheet,0 ) as  _Worksheet;
               miWorkSheet.Name:= 'Percepciones Negativas';
          end;

          if not ExisteSheet( 'Diferencias' ) then
          begin
               miWorkSheet:= Sheets.Add( EmptyParam,EmptyParam,1,xlWorksheet,0 ) as _Worksheet;
               miWorkSheet.Name:= 'Diferencias';
          end;
     end;
end;

procedure TdmExcelDeclara.InsertFormatoCelda( iColumna, iMaximo: Integer );
var
   Formato: FormatCondition;
   sAddress: String;
   Rango: ExcelRange;
begin

     with ExcelSheet.Cells do
     begin
          if ( iColumna > 2 ) and ( ( iColumna mod 2 ) = 0 )  then
          begin
               {Rango:= Range[ Item[ iRenglon, iColumna ], Item[ iRenglon, iColumna ] ].EntireColumn; }
               Rango:= Range[ Item[ 5, iColumna ], Item[ iMaximo, iColumna ] ];
               if ( Rango.FormatConditions.Count = 0 ) then
               begin
                    sAddress:= '=' + '$' + Cells.Item[ 1, iColumna - 1 ].Address[False, False, xlA1,FALSE,EmptyParam];

                    Formato:= Rango.FormatConditions.Add(xlCellValue, xlNotEqual, sAddress ,EmptyParam);
                    Formato.Font.Color:= clRed;
               end;
          end;
     end;
end;


procedure TdmInterfase.GetPercepcionesNegativas( const sListaPercep: String );
const
     QRY_NEGATIVOS = 'select CB_CODIGO, CO_NUMERO, ( AC_MES_01 + AC_MES_02 + AC_MES_03 + AC_MES_04 + '+
                     'AC_MES_05 + AC_MES_06 + AC_MES_07 + AC_MES_08 + AC_MES_09 + AC_MES_10 + ' +
                     'AC_MES_11 + AC_MES_12 + AC_MES_13 ) AC_SUMA from ACUMULA where AC_YEAR = %d and ( AC_MES_01 + AC_MES_02 + AC_MES_03 + AC_MES_04 + ' +
                     'AC_MES_05 + AC_MES_06 + AC_MES_07 + AC_MES_08 + AC_MES_09 + AC_MES_10 + ' +
                     'AC_MES_11 + AC_MES_12 + AC_MES_13 ) < 0 and '+
                     '( CO_NUMERO in ( %s ) ) order by CB_CODIGO';
begin
     if StrLleno( sListaPercep ) then
        cdsNegativos.Data:= ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( QRY_NEGATIVOS, [ FParamYear, sListaPercep ] ) );
end;

procedure TdmInterfase.cdsNegativosAlAdquirirDatos(Sender: TObject);
const
     QRY_NEGATIVOS = 'select CB_CODIGO, CO_NUMERO, AC_ANUAL from ACUMULA where AC_YEAR = %d and AC_ANUAL < 0 order by CB_CODIGO';
begin
     cdsNegativos.Data:= ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( QRY_NEGATIVOS, [ FParamYear ] ) );
end;

end.
