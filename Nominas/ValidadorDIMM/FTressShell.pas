unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ZetaKeyLookup, StdCtrls, Buttons, ComCtrls, Mask,
  ZetaNumero, ZBaseShell, ZBaseConsulta, ZetaTipoEntidad, ActnList, ImgList, ZetaCommonClasses,
  ZetaFecha, Menus, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, dxRibbonSkins,
  dxSkinsdxRibbonPainter, cxStyles, cxClasses, dxSkinsForm, dxRibbon,
  dxStatusBar, dxRibbonStatusBar;

type
  TTressShell = class(TBaseShell)
    GBYear: TGroupBox;
    lblYear: TLabel;
    Year: TZetaNumero;
    GBConceptos: TGroupBox;
    lbPercepcion: TLabel;
    lbMensual: TLabel;
    lbPTU: TLabel;
    lbAguinaldo: TLabel;
    lbPrimaVaca: TLabel;
    btnBuscaPercep: TZetaKeyLookup;
    btnBuscaMensual: TZetaKeyLookup;
    btnBuscaPTU: TZetaKeyLookup;
    btnBuscaAguinaldo: TZetaKeyLookup;
    Panel1: TPanel;
    lbBitacora: TLabel;
    BuscaBitacora: TSpeedButton;
    lbTolerancia: TLabel;
    PathBitacora: TEdit;
    eTolerancia: TZetaNumero;
    PanelBotones: TPanel;
    btnRecalcula: TBitBtn;
    Salir: TBitBtn;
    ArbolImages: TImageList;
    ActionList: TActionList;
    _A_OtraEmpresa: TAction;
    _A_Servidor: TAction;
    _A_SalirSistema: TAction;
    _P_Procesar: TAction;
    _P_Cancelar: TAction;
    _H_AcercaDe: TAction;
    _Per_GenerarPoliza: TAction;
    ePercepcion: TMemo;
    eMensual: TEdit;
    btnBuscaPrima: TZetaKeyLookup;
    ePTU: TEdit;
    eAguinaldo: TEdit;
    ePrimaVacacional: TEdit;
    ToparNegativos: TCheckBox;
    lbConceptosTopar: TLabel;
    btnBuscaConceptosTope: TZetaKeyLookup;
    eConceptosTope: TEdit;
    btnRevisa: TBitBtn;
    DialogoBitacora: TOpenDialog;
    Panel2: TPanel;
    ProgressBar: TProgressBar;
    FechaSalarioMinimo: TZetaFecha;
    lblFechaSalMin: TLabel;
    BtnDetener: TBitBtn;
    lbAvance: TLabel;
    lbAccion: TLabel;
    GroupBox1: TGroupBox;
    lbSumaPagos: TLabel;
    TomaPagoSepara: TZetaKeyLookup;
    lbGuardarEn: TLabel;
    GuardaPagoSepara: TZetaKeyLookup;
    ShellMenu: TMainMenu;
    Ayuda: TMenuItem;
    MenuItem5: TMenuItem;
    AyudaSeparador2: TMenuItem;
    MenuItem6: TMenuItem;
    _H_Contendio: TAction;
    Archivo: TMenuItem;
    Salir1: TMenuItem;
    _A_ClaveAcceso: TAction;
    N1: TMenuItem;
    ClavedeAcceso1: TMenuItem;
    Servidor1: TMenuItem;
    N2: TMenuItem;
    OtraEmpresa1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnBuscaConceptoValidLookup(Sender: TObject);
    procedure btnRevisaRecalculaClick(Sender: TObject);
    procedure ToparNegativosClick(Sender: TObject);
    procedure BuscaBitacoraClick(Sender: TObject);
    procedure BtnDetenerClick(Sender: TObject);
    procedure TomaPagoSeparaValidLookup(Sender: TObject);
    procedure _H_ContendioExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_ClaveAccesoExecute(Sender: TObject);
  private
    { Private declarations }
    FParameterList: TZetaParams;
    FCancelaRecalculo: Boolean;
    procedure CargaConceptosDefault;
    procedure DescargaConceptosDefault;
    procedure ActivaControles( const lProcesando: Boolean );
    procedure HablitidaControlesTope( const lHabilitar: Boolean );
    function CanContine: Boolean;
  protected
     procedure DoOpenAll; override;
     procedure DoCloseAll; override;
  public
    { Public declarations }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
    procedure RecalculoEnd;
    procedure RecalculoStart(const iMaxSteps: Integer; const sMessage: String);
    function RecalculoStep(const iEmpleado: Integer; const sMensaje: String = VACIO ): Boolean;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
  end;

var
  TressShell: TTressShell;

implementation

uses dCliente,
     dGlobal,
     DDiccionario,
     DTablas
     {$ifdef DOS_CAPAS}
     ,DZetaServerProvider
     ,ZetaSQLBroker
     {$endif}
     ,DInterfase
     ,ZetaClientTools
     ,ZetaDialogo
     ,ZetaCommonTools,
     ZetaAcercaDe,
     ZetaRegistryCliente,
     FAutoClasses,
     ZAccesosMgr,
     ZAccesosTress;

{$R *.dfm}

procedure TTressShell.FormCreate(Sender: TObject);

     procedure EscondeHintsCtrlF;
     begin
          { Debido a que no se implement� la funcionalidad de Ctrl-F, se esconden los hints }
           TSpeedButton(btnBuscaPercep.Controls[1]).ShowHint:= FALSE;
           //TSpeedButton(btnBuscaDeduccion.Controls[1]).ShowHint:= FALSE;
           TSpeedButton(btnBuscaMensual.Controls[1]).ShowHint:= FALSE;
           TSpeedButton(btnBuscaPTU.Controls[1]).ShowHint:= FALSE;
           TSpeedButton(btnBuscaAguinaldo.Controls[1]).ShowHint:= FALSE;
           TSpeedButton(btnBuscaPrima.Controls[1]).ShowHint:= FALSE;
           TSpeedButton(btnBuscaConceptosTope.Controls[1]).ShowHint:= FALSE;
     end;

begin
      {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmCliente := TdmCliente.Create( Self );
     dmInterfase:= TdmInterfase.Create(Self);
     FParameterList := TZetaParams.Create;
     inherited;
     ZAccesosMgr.SetValidacion( TRUE );
     EscondeHintsCtrlF;
     HelpContext := 1000;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     FParameterList.Free;
     dmInterfase.Free;
     dmCliente.Free;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     with dmInterfase do
     begin
          btnBuscaPercep.LookupDataSet:= cdsConceptos;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          DescargaConceptosDefault;
          dmInterfase.ActualizaArchivoIni;
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmInterfase );
//          ZetaClientTools.CierraDatasets( dmCliente );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

procedure TTressShell.DoOpenAll;
var
   lConDerechos: Boolean;
   lAutorizacion: Boolean;
begin
     try
        inherited DoOpenAll;
        {***US 11757: Proteccion HTTP SkinController = True
        Evita que el backgound de algunos componentes de la aplicacion
        sea de color blanco
        ***}
        DevEx_SkinController.NativeStyle := TRUE;
        with dmCliente do
        begin
             InitActivosSistema;
             { AP: Se cambio para que tomara el a�o anterior }
             Year.Valor:= YearDefault - 1;
             FechaSalarioMinimo.Valor:= LastDayOfYear(YearDefault - 1);
        end;

        GuardaPagoSepara.Enabled:= StrLleno(TomaPagoSepara.Llave);
        lbGuardarEn.Enabled:= GuardaPagoSepara.Enabled;

        with dmInterfase do
        begin
             cdsConceptos.Conectar;
             CargaConceptosDefault;
             PathBitacora.Text:= GetPathBitacora;
        end;
      except
         on Error : Exception do
         begin
              ZetaDialogo.ZError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
              DoCloseAll;
         end;
      end;

      lConDerechos:= Revisa( D_NOM_ANUAL_COMPARA_ISPT );
      with Autorizacion do
           lAutorizacion:= not EsDemo and OkModulo( okValidador );


      if ( not lAutorizacion ) then
      begin
           ZetaDialogo.ZInformation( 'ValidadorDIMM', 'El M�dulo de ValidadorDIMM no est� Autorizado, consulte a su Distribuidor',  0 );
      end
      else if not lConDerechos then
      begin
           ZetaDialogo.ZError( Caption, 'No se tiene derechos sobre rec�lculo de ValidadorDIMM' , 0 );
      end;

      ActivaControles( not lConDerechos or not lAutorizacion );

end;

procedure TTressShell.CargaConceptosDefault;
begin
     with dmInterfase do
     begin
          CargaConceptosTipo;

          ePercepcion.Text:= FConfiConceptos[tcConfiPercepcion];
          {eDeduccion.Text:= FConfiConceptos[tcConfiDeduccion]; }
          eMensual.Text:= FConfiConceptos[tcConfiMensual];
          ePTU.Text:= FConfiConceptos[tcConfiPTU];
          eAguinaldo.Text:= FConfiConceptos[tcConfiAguinaldo];
          ePrimaVacacional.Text:= FConfiConceptos[tcConfiPrimaVacacional];
          TomaPagoSepara.Llave:= FConfiConceptos[tcConfiPagoxSepara];
          GuardaPagoSepara.Llave:= FConfiConceptos[tcConfiGuardaPagoxSepara];
          eConceptosTope.Text:= FConfiConceptos[tcNegativosExcluidos];
          ToparNegativos.Checked:= StrLleno( eConceptosTope.Text );
     end;
end;

procedure  TTressShell.DescargaConceptosDefault;
begin
     with dmInterfase do
     begin
          FConfiConceptos[tcConfiPercepcion]:= ePercepcion.Text;
          {FConfiConceptos[tcConfiDeduccion]:= eDeduccion.Text; }
          FConfiConceptos[tcConfiMensual]:= eMensual.Text;
          FConfiConceptos[tcConfiPTU]:= ePTU.Text;
          FConfiConceptos[tcConfiAguinaldo]:= eAguinaldo.Text;
          FConfiConceptos[tcConfiPrimaVacacional]:= ePrimaVacacional.Text;
          FConfiConceptos[tcConfiPagoxSepara]:= TomaPagoSepara.Llave;
          FConfiConceptos[tcConfiGuardaPagoxSepara]:= GuardaPagoSepara.Llave;
          FConfiConceptos[tcNegativosExcluidos]:= eConceptosTope.Text;
     end;
end;

procedure TTressShell.btnBuscaConceptoValidLookup(Sender: TObject);

    procedure ConcatConcepto( oEdit: TObject );
    begin
         with TEdit( oEdit ) do
              Text:= ConcatString( Text, TZetaKeyLookup(Sender).Llave, ',' );
    end;

begin
     inherited;
     with TZetaKeyLookup(Sender) do
     begin
          case eTipoConfiConcepto( Tag ) of
                tcConfiPercepcion: ConcatConcepto( ePercepcion );
                //tcConfiDeduccion: ConcatConcepto( eDeduccion );
                tcConfiMensual: ConcatConcepto( eMensual );
                tcConfiPTU: ConcatConcepto( ePTU );
                tcConfiAguinaldo: ConcatConcepto( eAguinaldo );
                tcConfiPrimaVacacional: ConcatConcepto( ePrimaVacacional );
                tcConfiTopeNegativo: ConcatConcepto( eConceptosTope );
          end;
     end;
end;

procedure TTressShell.btnRevisaRecalculaClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     if CanContine then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor:= crHourglass;
          try
             FCancelaRecalculo:= FALSE;

             btnDetener.Visible:= TRUE;
             btnRecalcula.Visible:= FALSE;

             ActivaControles( TRUE );

             with FParameterList do
             begin
                  AddInteger( 'Year', Year.ValorEntero );
                  AddString( 'CoPercepciones', ePercepcion.Text );
                  AddString( 'CoDeducciones', '' );
                  AddString( 'CoPerMens', eMensual.Text );
                  AddString( 'CoPTU', ePTU.Text );
                  AddString( 'CoAguinaldo', eAguinaldo.Text);
                  AddString( 'CoPrimaVaca', ePrimaVacacional.Text );
                  AddString( 'GuardaPagoSepara', GuardaPagoSepara.Llave );
                  AddString( 'TomaPagoSepara', TomaPagoSepara.Llave );
                  AddFloat( 'Tolerancia', eTolerancia.Valor );
                  AddBoolean( 'ToparNegativos', ToparNegativos.Checked );
                  AddString( 'CoTope', eConceptosTope.Text );
             end;
             DescargaConceptosDefault;

             with dmInterfase do
             begin
                  FechaSalMin:= FechaSalarioMinimo.Valor;
                  ArchivoBitacora:= PathBitacora.Text;
                  Procesa( IntToBool( TBitBtn( Sender ).Tag ), FParameterList );
             end


          finally
                 FCancelaRecalculo:= TRUE;

                 ActivaControles(FALSE);
                 btnDetener.Visible:= False;
                 btnRecalcula.Visible:= True;
                 Screen.Cursor:= oCursor;
          end;
     end;
end;


procedure TTressShell.ToparNegativosClick(Sender: TObject);
begin
     inherited;
     HablitidaControlesTope( ToparNegativos.Checked );

end;

procedure TTressShell.BuscaBitacoraClick(Sender: TObject);
begin
     inherited;
     PathBitacora.Text := ZetaClientTools.AbreDialogo( DialogoBitacora, PathBitacora.Text, 'xsl' );
end;


procedure TTressShell.ActivaControles(const lProcesando: Boolean );
var
   lEnabledControl, lOk: Boolean;
   i,c: Integer;
begin
     with dmCliente do
     begin
          lEnabledControl:= EmpresaAbierta and ( not lProcesando );

          for i:= 0 to TressShell.ControlCount - 1 do
          begin
               if ( Controls[i].ClassType = TGroupBox ) or ( Controls[i].ClassType = TPanel ) then
               begin
                    for c:= 0 to ( TCustomControl( Controls[i] ).ControlCount - 1 ) do
                    begin
                         with TCustomControl(Controls[i]).Controls[c] do
                         begin
                              lOk := ( ( ClassType = TZetaNumero ) or
                              ( ClassType = TMemo ) or ( ClassType = TBitBtn ) or
                              ( ClassType = TCheckBox ) or ( ClassType = TSpeedButton ) or
                              ( ClassType = TZetaKeyLookup ) or
                              ( ClassType = TEdit )   or
                              ( ClassType = TZetaFecha )  or
                              ( ClassType = TGroupBox ) ) and
                              ( Name <> 'Salir' ) and
                              ( Name <> 'BtnDetener');


                              Enabled:= ( lEnabledControl ) or ( not lOk );
                         end;
                    end;
               end;
          end;

          HablitidaControlesTope( lEnabledControl and ToparNegativos.Checked );
     end;
end;

procedure TTressShell.HablitidaControlesTope(const lHabilitar: Boolean);
begin
     lbConceptosTopar.Enabled:= lHabilitar;
     eConceptosTope.Enabled:= lHabilitar;
     btnBuscaConceptosTope.Enabled:= lHabilitar;
end;

procedure TTressShell.RecalculoStart(const iMaxSteps: Integer; const sMessage: String);
begin
     with ProgressBar do
     begin
          Position := 0;
          Max := iMaxSteps;
          Step := 1;
          //Width := Conciliar.Left - Left - 5;
          Visible := True;
          lbAvance.Visible:= Visible;
          lbAccion.Visible:= Visible;
          lbAccion.Caption:= sMessage;
     end;
     Application.ProcessMessages;
end;

function TTressShell.RecalculoStep( const iEmpleado: Integer; const sMensaje: String ): Boolean;
begin
     with ProgressBar do
     begin
          StepIt;
          if ( StrVacio(sMensaje) ) then
             lbAvance.Caption:= Format( 'Avance al %4.1n %s Empleado: %d', [( 100 * Position / Max ),'%',
                                                                           iEmpleado ] )
          else
              lbAvance.Caption:= sMensaje;
     end;
     Result := NOT FCancelaRecalculo;
     Application.ProcessMessages;
end;

procedure TTressShell.RecalculoEnd;
begin
     with ProgressBar do
     begin
          Position := Max;
          Visible := False;
          lbAvance.Visible:= Visible;
          lbAccion.Visible:= Visible;
     end;
     Application.ProcessMessages;
end;

procedure TTressShell.BtnDetenerClick(Sender: TObject);
begin
     inherited;
     FCancelaRecalculo:= TRUE;
end;

function TTressShell.CanContine: Boolean;
const
     K_ERROR_LISTA_CONCEPTOS = 'La Lista de Conceptos no es v�lida';

     function ValidaListaConceptos( const oControl: TWinControl ): Boolean;
     begin
          Result:= ValidaListaNumeros( TEdit( oControl ).Text );
          if not Result then
          begin
               ZetaDialogo.ZError( Caption, K_ERROR_LISTA_CONCEPTOS, 0 );
               oControl.SetFocus;
          end;
     end;

     function YearValido: Boolean;
     begin
          Result:= ( Year.Valor <> 0  );
          if not Result then
          begin
               ZetaDialogo.ZError( Caption, 'A�o Inv�lido', 0 );
               Year.SetFocus;
          end;
     end;

begin
     Result:= ( YearValido ) and ( ( TheYear( FechaSalarioMinimo.Valor ) = Year.Valor ) or ( ZetaDialogo.ZWarningConfirm('Advertencia', 'El a�o y la fecha del salar�o m�nimo no coinciden, � Desea Continuar con el '+
                                                                                                  ' proceso ?',0, mbCancel ) ) );

     if Result then
     begin
          Result:= ValidaListaConceptos( ePercepcion ) and
                   {ValidaListaConceptos( eDeduccion ) and}
                   ValidaListaConceptos( eMensual ) and
                   ValidaListaConceptos( eAguinaldo ) and
                   ValidaListaConceptos( ePrimaVacacional) and
                   ValidaListaConceptos( ePTU ) and
                   ( not eConceptosTope.Enabled or ValidaListaConceptos( eConceptosTope ) ) ;
     end;

     if Result then
     begin
          if StrLleno( TomaPagoSepara.Llave ) then
          begin
               Result:= StrLleno( GuardaPagoSepara.Llave );
               if not Result then
               begin
                    ZetaDialogo.ZError( Caption, 'Especificar concepto de referencia y almacenamiento de suma de pagos por separaci�n', 0 );
                    TomaPagoSepara.SetFocus;
               end;
          end;
     end;
end;

procedure TTressShell.TomaPagoSeparaValidLookup(Sender: TObject);
begin
     inherited;
     GuardaPagoSepara.Enabled:= StrLleno(TomaPagoSepara.Llave);
     lbGuardarEn.Enabled:= GuardaPagoSepara.Enabled;
end;


function TTressShell.FormaActiva: TBaseConsulta;
begin
      Result:= Nil;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo,iPeriodoBorrado: Integer);
begin
     //
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
      //
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
    //
end;

procedure TTressShell.ChangeTimerInfo;
begin
     //
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     //
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     //
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     //
end;

procedure TTressShell.RefrescaEmpleado;
begin
     //
end;

procedure TTressShell.RefrescaIMSS;
begin
      //
end;



procedure TTressShell.ReinitPeriodo;
begin
     //
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
      //
end;

procedure TTressShell.SetBloqueo;
begin
      //
end;


procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     //
end;


procedure TTressShell._Per_GenerarPolizaExecute(Sender: TObject);
begin
     {No hace nada}
end;



procedure TTressShell._H_ContendioExecute(Sender: TObject);
begin
     inherited;
     Application.HelpCommand( HELP_FINDER, 0 );
end;

procedure TTressShell._H_AcercaDeExecute(Sender: TObject);
begin
     inherited;
     try
        ZAcercaDe := TZAcercaDe.Create( Self );
        ZAcercaDe.ShowModal;
     finally
            ZAcercaDe.Free;
     end;
end;

procedure TTressShell._A_SalirSistemaExecute(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TTressShell._A_OtraEmpresaExecute(Sender: TObject);
begin
     AbreEmpresa( False );
end;

procedure TTressShell._A_ServidorExecute(Sender: TObject);
begin
     CambiaServidor;
end;

procedure TTressShell._A_ClaveAccesoExecute(Sender: TObject);
begin
     CambiaClaveUsuario;
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
    //No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     //No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     //No Implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     //No Implementar
end;

end.
