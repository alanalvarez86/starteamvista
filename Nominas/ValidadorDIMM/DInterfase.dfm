object dmInterfase: TdmInterfase
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 151
  Top = 231
  Height = 416
  Width = 557
  object cdsConceptos: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsConceptosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    LookupActivoField = 'CO_ACTIVO'
    OnGetRights = cdsConceptosGetRights
    Left = 56
    Top = 24
  end
  object cdsDataSet: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 24
  end
  object cdsTotalesEmpleados: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 64
    Top = 96
  end
  object cdsEmpleados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpleadosAlAdquirirDatos
    Left = 160
    Top = 96
  end
  object cdsSalMin: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsSalMinAlAdquirirDatos
    Left = 56
    Top = 176
  end
  object cdsNegativos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsNegativosAlAdquirirDatos
    Left = 128
    Top = 176
  end
  object cdsErrores: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 200
    Top = 176
  end
end
