program ValidadorDIMM;

uses
  Forms,
  ComObj,
  ActiveX,
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  DInterfase in 'DInterfase.pas' {dmInterfase: TDataModule},
  FTressShell in 'FTressShell.pas' {TressShell};

{$R *.RES}
{$R WindowsXP.res}
{procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     //MuestraSplash;
     Application.Title := 'Validador DIMM';
     Application.HelpFile := 'ValidadorDIMM.chm';
     Application.CreateForm(TTressShell, TressShell);
     with TressShell do
        begin
             if Login( False ) then
             begin
                  //CierraSplash;
                  Show;
                  Update;
                  BeforeRun;
                  Application.Run;
             end
             else
             begin
                  //CierraSplash;
                  Free;
             end;
        end;
end.
