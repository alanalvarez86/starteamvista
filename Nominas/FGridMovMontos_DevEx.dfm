inherited GridMovMontos_DevEx: TGridMovMontos_DevEx
  Left = 305
  Top = 275
  Caption = 'Montos'
  ClientHeight = 333
  ClientWidth = 575
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 297
    Width = 575
    DesignSize = (
      575
      36)
    inherited OK_DevEx: TcxButton
      Left = 411
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 490
      Cancel = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 575
    inherited ValorActivo2: TPanel
      Width = 249
      inherited textoValorActivo2: TLabel
        Width = 243
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 77
    Width = 575
    Height = 220
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    TabOrder = 4
    Columns = <
      item
        Expanded = False
        FieldName = 'CO_NUMERO'
        Title.Alignment = taCenter
        Title.Caption = '#'
        Width = 40
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'DESCRIPCION'
        Title.Caption = 'Concepto'
        Width = 215
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'MO_PERCEPC'
        Title.Alignment = taRightJustify
        Title.Caption = 'Percepciones'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'MO_DEDUCCI'
        Title.Alignment = taRightJustify
        Title.Caption = 'Deducciones'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'MO_REFEREN'
        Title.Caption = 'Referencia'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        ReadOnly = True
        Title.Caption = 'Modific'#243
        Width = 45
        Visible = True
      end>
  end
  object PanelEmpleado: TPanel [4]
    Left = 0
    Top = 47
    Width = 575
    Height = 30
    Align = alTop
    TabOrder = 2
    object EmpleadoLbl: TLabel
      Left = 13
      Top = 8
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = '&Empleado:'
      FocusControl = CB_CODIGO
    end
    object CB_CODIGO: TZetaKeyLookup_DevEx
      Left = 66
      Top = 5
      Width = 356
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      OnValidKey = CB_CODIGOValidKey
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 436
    Top = 57
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
    inherited BarSuperior: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarControlContainerItem_DBNavigator'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_AgregarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BorrarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ModificarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BuscarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_ImprimirBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ImprimirFormaBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ExportarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_CortarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_CopiarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_PegarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_UndoBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'Label1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 123
          Visible = True
          ItemName = 'cbOperacion'
        end>
    end
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Enabled = False
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
    object cbOperacion: TdxBarCombo
      Category = 0
      Visible = ivAlways
      Width = 150
      Items.Strings = (
        'Reportar Error'
        'Dejar Anteriores'
        'Sumar Monto'
        'Sustituir Monto')
      ItemIndex = -1
    end
    object Label1: TdxBarStatic
      Align = iaRight
      Caption = 'Si ya existen:'
      Category = 0
      Hint = 'Si ya existen:'
      Visible = ivAlways
      Alignment = taRightJustify
    end
  end
end
