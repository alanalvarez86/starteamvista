unit FWizCalculoConfPago;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, Vcl.Menus, Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Vcl.StdCtrls,
  cxTextEdit, cxButtons, cxProgressBar, ZetaKeyCombo, Vcl.Mask, ZetaNumero,
  cxDropDownEdit, ZetaCXStateComboBox, cxMaskEdit, cxSpinEdit;

type
  TWizCalculoConfPago = class(TcxBaseWizard)
    gbArchivoCedula: TcxGroupBox;
    cxLabel1: TcxLabel;
    btnCedulaFonacot: TcxButton;
    ArchivoCedulaFonacot: TcxTextEdit;
    lblRazonSocialConciliar: TcxLabel;
    varRazonSocialConciliar: TZetaKeyLookup_DevEx;
    DialogoCedula: TOpenDialog;
    PanelProgressBar: TPanel;
    ProgressBar: TcxProgressBar;
    Label11: TcxLabel;
    Label12: TcxLabel;
    txtAnio: TcxSpinEdit;
    zcboMes: TcxStateComboBox;
    procedure btnCedulaFonacotClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);

  private
    { Private declarations }
    procedure HabilitaFiltroRazonSocial;
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizCalculoConfPago: TWizCalculoConfPago;

implementation

{$R *.dfm}

uses
    ZetaCommonTools, ZetaDialogo, FTressShell, DCliente,
    ZetaClientTools, ZetaCommonLists,
    ZetaCommonClasses, DConcilia;


procedure TWizCalculoConfPago.btnCedulaFonacotClick(Sender: TObject);
begin
     inherited;
     ArchivoCedulaFonacot.Text := AbreDialogo( DialogoCedula, ArchivoCedulaFonacot.Text,'csv' );
end;



procedure TWizCalculoConfPago.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     dmCliente.CancelaConciliacion := lOk;
end;

procedure TWizCalculoConfPago.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                 if ( StrVacio (ArchivoCedulaFonacot.Text) ) then
                 begin
                      ZetaDialogo.ZError( 'Error al calcular confronta', 'El archivo de c�dula de pago est� vac�o ', 0 );
                      CanMove := False;
                 end;
            end;
     end;

end;

function TWizCalculoConfPago.EjecutarWizard: Boolean;
begin
     TressShell.SetProgressbar( Self.ProgressBar);
     TressShell.CalcularConfronta( varRazonSocialConciliar.Llave, ArchivoCedulaFonacot.Text, txtAnio.Value, zcboMes.Indice );
end;

procedure TWizCalculoConfPago.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_CALCULAR_CONFRONTA;

     with dmCliente.cdsRSocial do
     begin
          Conectar;
     end;
     HabilitaFiltroRazonSocial;
     varRazonSocialConciliar.LookupDataset := dmCliente.cdsRSocial;

     ArchivoCedulaFonacot.Text := dmCliente.CedulaUltimaConfronta;
     if ( varRazonSocialConciliar.Visible ) then
        varRazonSocialConciliar.Llave := dmCliente.RazonSocialUltimaConfronta;

     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizCalculoConfPago.FormShow(Sender: TObject);
begin
     inherited;
     txtAnio.Value := dmCliente.CedulaAnioConciliacion;//Anio de conciliacion sugerido
     zcboMes.Indice  := dmCliente.CedulaMesConciliacion;//Mes de conciliacion sugerido
     Advertencia.Caption := 'Se revisar� el c�lculo de Fonacot realizado en sistema TRESS para cotejarlo con la c�dula seleccionada.';
end;

procedure TWizCalculoConfPago.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'CedulaPago  ', ArchivoCedulaFonacot.Text );
          AddInteger( 'A�o' , txtAnio.Value );
          AddString( 'Mes', ObtieneElemento( lfMeses, zcboMes.Indice - 1 ));
          AddString( 'RazonSocial', varRazonSocialConciliar.Llave );

     end;
     with Descripciones do
     begin
          AddString( 'C�dula de pago', ArchivoCedulaFonacot.Text );
          AddInteger( 'A�o' , txtAnio.Value );
          AddString( 'Mes', ObtieneElemento( lfMeses, zcboMes.Indice - 1));
          if ( varRazonSocialConciliar.Visible ) then
            AddString( 'Raz�n social', varRazonSocialConciliar.Llave );

     end;
     inherited;

end;


procedure TWizCalculoConfPago.HabilitaFiltroRazonSocial;
var
   lHabilitarFiltro : Boolean;
begin
     lHabilitarFiltro := False;
     with dmCliente.cdsRSocial do
     begin
          lHabilitarFiltro := (RecordCount > 1)
     end;

     varRazonSocialConciliar.LookupDataset := dmCliente.cdsRSocial;
     varRazonSocialConciliar.Visible := lHabilitarFiltro;
     varRazonSocialConciliar.Enabled := lHabilitarFiltro;
     lblRazonSocialConciliar.Visible := lHabilitarFiltro;
     lblRazonSocialConciliar.Enabled := lHabilitarFiltro;

     if not lHabilitarFiltro then
     begin
          varRazonSocialConciliar.Llave := VACIO;
     end;

end;



end.
