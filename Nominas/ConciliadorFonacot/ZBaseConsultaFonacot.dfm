inherited BaseConsultaFonacot: TBaseConsultaFonacot
  Left = 0
  Top = 0
  Caption = 'Cr'#233'ditos activos que no est'#225'n en c'#233'dula'
  ClientHeight = 366
  ClientWidth = 992
  OldCreateOrder = False
  OnKeyUp = FormKeyUp
  ExplicitWidth = 992
  ExplicitHeight = 366
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 992
    ExplicitWidth = 992
    inherited ImgNoRecord: TImage
      ExplicitLeft = -3
    end
    inherited Slider: TSplitter
      Left = 616
      Width = 0
      ExplicitLeft = 616
      ExplicitWidth = 0
    end
    inherited ValorActivo1: TPanel
      Width = 600
      ExplicitWidth = 600
      inherited textoValorActivo1: TLabel
        ParentShowHint = False
        ShowHint = True
      end
    end
    inherited ValorActivo2: TPanel
      Left = 616
      Width = 376
      ExplicitLeft = 616
      ExplicitWidth = 376
      inherited textoValorActivo2: TLabel
        Left = 290
        ParentShowHint = False
        ShowHint = True
        ExplicitLeft = 290
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Width = 992
    Height = 306
    ExplicitTop = 60
    ExplicitWidth = 992
    ExplicitHeight = 306
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OnCustomDrawCell = ZetaDBGridDBTableViewCustomDrawCell
    end
  end
  object PanelFiltros: TPanel [2]
    Left = 0
    Top = 19
    Width = 992
    Height = 41
    Align = alTop
    TabOrder = 2
    object EmpleadoConcilia: TZetaKeyLookup_DevEx
      Left = 78
      Top = 6
      Width = 375
      Height = 21
      Hint = 'B'#250'squeda de empleados'
      Align = alCustom
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = EmpleadoConciliaValidKey
    end
    object EmpleadoLbl: TcxLabel
      Left = 18
      Top = 6
      Caption = 'Empleado:'
      Transparent = True
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object ActionList1: TActionList
    Left = 416
    Top = 128
    object BuscarEmpleado: TAction
      Caption = 'BuscarEmpleado'
      Hint = 'Buscar empleado'
      ShortCut = 16454
      OnExecute = BuscarEmpleadoExecute
    end
  end
end
