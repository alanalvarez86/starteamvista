unit FWizNuevosCreditosGridSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, DBClient;

type
  TWizNuevosCreditosGridSelect = class(TBasicoGridSelect_DevEx)

    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WizNuevosCreditosGridSelect: TWizNuevosCreditosGridSelect;

implementation

{$R *.dfm}

procedure TWizNuevosCreditosGridSelect.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     TClientDataSet(DataSource.DataSet ).CancelUpdates;
end;

end.
