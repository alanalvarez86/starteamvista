unit FWizActualizaDatosEmp;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZcxBaseWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, cxContainer, cxEdit,
  ZCXBaseWizardFiltro, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxTextEdit, DBClient, Vcl.Mask, ZetaNumero, TressMorado2013, ZetaEdit,
  cxRadioGroup, cxMemo, Vcl.ExtCtrls, ZetaKeyLookup_DevEx;

type
  TWizActualizaDatosEmp = class(TBaseCXWizardFiltro)
    DialogoBitacora: TOpenDialog;
    Parametros2: TdxWizardControlPage;
    Parametros3: TdxWizardControlPage;
    GroupBox2: TGroupBox;
    chkPlazoAumenta: TCheckBox;
    chkPlazoDisminuye: TCheckBox;
    ChkCambioRetencion: TCheckBox;
    GroupBox3: TGroupBox;
    LblDiferencias: TLabel;
    nMontos: TZetaNumero;
    ChkRecalcularSaldo: TCheckBox;
    chkRemoverFormula: TCheckBox;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    txtActulizaPrestamosPath: TcxTextEdit;
    BuscaBitacoraFONACOT: TcxButton;
    procedure BuscaBitacoraFONACOTClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure chkCambioRetencionClick(Sender: TObject);
  private
    { Private declarations }
    DataSet: TClientDataSet;
    FFiltroPlazoAumentaConciliacion: Boolean;
    FFiltroPlazoDisminuyeConciliacion: Boolean;
    FFiltroCambioRetencionConciliacion: Boolean;
    FFiltroDiferenciaMontosConciliacion: Currency;
    //FCanceladoWizard: Boolean;
    procedure SetFiltrosDiferencias;
    procedure AplicarFiltroDataSet;
    procedure ActualizarFiltrosDataSet(lFiltroPlazoAumentaConciliacion, lFiltroPlazoDisminuyeConciliacion, lFiltroCambioRetencionConciliacion: Boolean; dDiferenciaMontos: Currency );
    procedure ChecarFiltros( lActualizaFiltro: boolean = TRUE );
    procedure ActualizarCheckBox;
    procedure ActualizarGlobalFiltros;
    procedure FiltrosSugeridosConsultaDataSet;
    procedure CancelarCambiosDataSet;
    function FiltrosCambiaron: Boolean;
    procedure HabilitarDiferencias( lHabilitar: Boolean );
  protected

      function EjecutarWizard: Boolean;override;
      procedure CargaParametros;override;
      function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

  function ShowWizard( aDataSet: TClientDataSet): Boolean;

var
  WizActualizaDatosEmp: TWizActualizaDatosEmp;

implementation

{$R *.dfm}

uses
    DConcilia, ZetaClientTools, ZetaCommonTools,
    ZetaCommonClasses, ZetaDialogo, ZBasicoSelectGrid_DevEx, ZBaseSelectGrid_DevEx,
    FWizActualizaDatosEmpGridSelect, ZetaClientDataSet, DCliente, DCatalogos;

//Parametro: aDataset
function ShowWizard( aDataSet: TClientDataSet): Boolean;
begin
     with TWizActualizaDatosEmp.Create( Application ) do
     begin
          try
             DataSet := aDataSet;
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;

function TWizActualizaDatosEmp.EjecutarWizard: Boolean;
begin
     with dmConcilia do
     begin
          LogInit( txtActulizaPrestamosPath.Text ,'Inicio Actualizaci�n de Pr�stamos' );
          {***US 12872: Conciliador Fonacot - El proceso que actualiza datos de cr�ditos no respeta la opci�n que se marca y actualiza todas las diferencias***}
          ActualizarCreditos(chkRemoverFormula.Checked, chkPlazoAumenta.Checked, chkPlazoDisminuye.Checked, chkCambioRetencion.Checked, chkRecalcularSaldo.Checked );
          LogEnd;
          FiltrosSugeridosConsultaDataSet;
          //graba datos en registry
          try
             SetPathBitacora ( K_ACTUALIZA_PRESTAMOS, txtActulizaPrestamosPath.Text );
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zInformation( '', 'No cuenta con los derechos necesarios en los registros locales (Registry) '
                                                + ' para configurar la aplicaci�n en su computadora, contacte al Administrador del Sistema.', 0 );
                end;
          end;
     end;
end;

procedure TWizActualizaDatosEmp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     //if not( FCanceladoWizard ) then
     //   CancelarCambiosDataSet;
     
     FreeAndNil(dmConcilia.cdsCreditosDiferenciasTemp);
     //FCanceladoWizard := FALSE;
end;

procedure TWizActualizaDatosEmp.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_ACTUALIZA_DATOS_CREDITOS;
     Parametros.PageIndex := 0;
     Parametros2.PageIndex := 1;
     Parametros3.PageIndex := 2;
     FiltrosCondiciones.PageIndex := 3;
     Ejecucion.PageIndex := 4;
     SetFiltrosDiferencias;
     HabilitarDiferencias( chkCambioRetencion.Checked );
end;

procedure TWizActualizaDatosEmp.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Se actualizar�n los pr�stamos de Fonacot de Sistema Tress con la informaci�n de los cr�ditos de la c�dula.';
     TClientDataSet( DataSet ).LogChanges := True;
     with dmConcilia, Application do
     begin
          txtActulizaPrestamosPath.Text:= GetPathBitacora( K_ACTUALIZA_PRESTAMOS );
     end;
end;

procedure TWizActualizaDatosEmp.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     if ( lok ) then
        CancelarCambiosDataSet;
end;

procedure TWizActualizaDatosEmp.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros3 ) then
           begin
                AplicarFiltroDataSet;
                if ( StrVacio( txtActulizaPrestamosPath.Text) ) then
                begin
                     ZetaDialogo.ZError( 'Error al actualizar datos', 'El directorio de la bit�cora no puede quedar vac�o', 0 );
                     CanMove := False;
                end
                else if ( DataSet.IsEmpty ) then
                begin
                     ZetaDialogo.ZError( 'Error al actualizar datos', 'Lista de empleados a actualizar est� vac�a', 0 );
                     CanMove := False;
                end
                else
                begin
                     //si hay registros a procesar, abrir el grid de empleados para filtrar
                     { CanMove := ZBasicoSelectGrid_DevEx.GridSelectBasico(
                                 TZetaClientDataSet( DataSet ), TWizActualizaDatosEmpGridSelect, FALSE );}

                end;
                 //si no hay registros a procesar, despues de filtrarlos por el grid de empleados, no avanzar
                if ( DataSet.IsEmpty and CanMove) then
                begin
                     ZetaDialogo.ZError( 'Error al actualizar datos', 'Lista de empleados a actualizar est� vac�a', 0 );
                     CanMove := False;
                     //cancelar los cambios realizados por el grid de empleados, cuando se eliminaron TODOS los registros a procesar
                     TClientDataSet( DataSet).CancelUpdates;
                end;
           end;

           if Adelante then
           begin
                if ( WizardControl.ActivePage = FiltrosCondiciones ) then
                begin
                     SeleccionarClick( Sender );
                     CanMove := Verificacion;
                     // if CanMove then
                     //      DataSet.Data := dmConcilia.cdsCreditosDiferenciasTemp.Data;
                end;
           end
     end;
end;

procedure TWizActualizaDatosEmp.BuscaBitacoraFONACOTClick(Sender: TObject);
begin
     inherited;
     txtActulizaPrestamosPath.Text := ZetaClientTools.AbreDialogo( DialogoBitacora , txtActulizaPrestamosPath.Text, 'log' );
end;

procedure TWizActualizaDatosEmp.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          AddString( 'Plazos que aumentaron',  BoolToSiNo( chkPlazoAumenta.Checked ) );
          AddString( 'Plazos que disminuyeron',  BoolToSiNo( chkPlazoDisminuye.Checked ) );
          AddString( 'Cambio en ret. mensual',  BoolToSiNo( chkCambioRetencion.Checked ) );
          if chkCambioRetencion.Checked then
          begin
               AddFloat(  'Diferencias mayores a', nMontos.Valor );
               AddString( 'Recalcular saldo',  BoolToSiNo( ChkRecalcularSaldo.Checked ) );
          end;
          AddString( 'Remover f�rmula pr�stamo',  BoolToSiNo( chkRemoverFormula.Checked ) );
          AddString( 'Guardar bit�cora en', txtActulizaPrestamosPath.Text );
          if ( DataSet <> nil ) and ( DataSet.Active ) and ( Wizard.EsPaginaActual( Ejecucion )) then
             AddInteger ( 'Cantidad de empleados', DataSet.RecordCount);
     end;
end;

procedure TWizActualizaDatosEmp.SetFiltrosDiferencias;
begin
     ActualizarCheckBox;
     ActualizarGlobalFiltros;
end;

procedure TWizActualizaDatosEmp.AplicarFiltroDataSet;
begin
     if ( chkPlazoAumenta.Checked <> dmCliente.FiltroPlazoAumentaConciliacion ) then
          ChecarFiltros
     else if ( chkPlazoDisminuye.Checked <> dmCliente.FiltroPlazoDisminuyeConciliacion ) then
          ChecarFiltros
     else if ( chkCambioRetencion.Checked <> dmCliente.FiltroCambioRetencionConciliacion ) then
          ChecarFiltros
     else if ( nMontos.Valor <> dmCliente.FiltroDiferenciaMontosConciliacion ) then
          ChecarFiltros;
end;

function TWizActualizaDatosEmp.FiltrosCambiaron: Boolean;
begin
     Result := False;
     if ( FFiltroPlazoAumentaConciliacion <> dmCliente.FiltroPlazoAumentaConciliacion ) then
          Result := True
     else if ( FFiltroPlazoDisminuyeConciliacion <> dmCliente.FiltroPlazoDisminuyeConciliacion ) then
          Result := True
     else if ( FFiltroCambioRetencionConciliacion <> dmCliente.FiltroCambioRetencionConciliacion ) then
          Result := True
     else if ( FFiltroDiferenciaMontosConciliacion <> dmCliente.FiltroDiferenciaMontosConciliacion ) then
          Result := True;
end;


procedure TWizActualizaDatosEmp.HabilitarDiferencias( lHabilitar: Boolean );
begin
     LblDiferencias.Enabled := lHabilitar;
     nMontos.Enabled := lHabilitar;
     ChkRecalcularSaldo.Enabled := lHabilitar;
end;

procedure TWizActualizaDatosEmp.ChecarFiltros( lActualizaFiltro: boolean = TRUE );
const
     K_Filtro_1 = 'CPlazo > TressPlazo';
     K_Filtro_2 = 'TressPlazo > CPlazo';
     K_Filtro_3 = 'CRetencion <> TressRetencion';
     K_Sin_Filt = 'Posicion = -1';
var
   sFiltro, sFiltroDiferencia:string;
   function GetDiferencia( sFiltroDiferencia: String ): String;
   begin
        if StrLleno( sFiltroDiferencia ) then
        sFiltroDiferencia := Parentesis( sFiltroDiferencia );
        sFiltroDiferencia := ConcatFiltros( sFiltroDiferencia, Parentesis( 'DIFERENCIA>' +  FloatToStr(nMontos.Valor) ) );
        Result := sFiltroDiferencia;
   end;
begin
     sFiltro := K_Sin_Filt;
     if chkPlazoAumenta.Checked then
     begin
          sFiltro := ConcatString( sFiltro,K_Filtro_1 ,' OR ');
     end;
     if chkPlazoDisminuye.Checked then
     begin
          sFiltro := ConcatString( sFiltro,K_Filtro_2 ,' OR ');
     end;
     HabilitarDiferencias( chkCambioRetencion.Checked );
     if chkCambioRetencion.Checked then
     begin
          sFiltroDiferencia := Parentesis( GetDiferencia( K_Filtro_3 ) );
          sFiltro := ConcatString( sFiltro, sFiltroDiferencia,' OR ');
     end;
     if lActualizaFiltro then
        ActualizarFiltrosDataSet(chkPlazoAumenta.Checked,chkPlazoDisminuye.Checked,chkCambioRetencion.Checked, nMontos.Valor );
     TClientDataSet( DataSet ).CancelUpdates; //Resetear DataSet para poder aplicar el Filtrado Nuevo

     dmConcilia.cdsCreditosDiferenciasTemp := TZetaClientDataSet.Create( Self );
     dmConcilia.cdsCreditosDiferenciasTemp.Data := DataSet.Data;
     with DataSet do
     begin
          Filtered := FALSE;
          Filter := sFiltro;
          Filtered := StrLleno(sFiltro);
     end;

     with dmConcilia.cdsCreditosDiferenciasTemp do
     begin
          Filtered := FALSE;
          Filter := sFiltro;
          Filtered := StrLleno(sFiltro);
     end;
end;

procedure TWizActualizaDatosEmp.chkCambioRetencionClick(Sender: TObject);
begin
     inherited;
     HabilitarDiferencias( chkCambioRetencion.Checked );
end;

procedure TWizActualizaDatosEmp.ActualizarFiltrosDataSet(lFiltroPlazoAumentaConciliacion, lFiltroPlazoDisminuyeConciliacion, lFiltroCambioRetencionConciliacion: Boolean; dDiferenciaMontos: Currency );
begin
     dmCliente.FiltroPlazoAumentaConciliacion    := lFiltroPlazoAumentaConciliacion;
     dmCliente.FiltroPlazoDisminuyeConciliacion  := lFiltroPlazoDisminuyeConciliacion;
     dmCliente.FiltroCambioRetencionConciliacion := lFiltroCambioRetencionConciliacion;
     dmCliente.FiltroDiferenciaMontosConciliacion := dDiferenciaMontos;
end;

procedure TWizActualizaDatosEmp.FiltrosSugeridosConsultaDataSet;
begin
     if FiltrosCambiaron then //Validar Si los Filtros Sugeridos no Cambiaron
     begin
          ActualizarFiltrosDataSet(FFiltroPlazoAumentaConciliacion, FFiltroPlazoDisminuyeConciliacion, FFiltroCambioRetencionConciliacion, FFiltroDiferenciaMontosConciliacion );//Aplicar los Filtros por Defualt
          ActualizarCheckBox;
          ChecarFiltros(FALSE);
     end;
end;

procedure TWizActualizaDatosEmp.ActualizarCheckBox;
begin
     chkPlazoAumenta.Checked    := dmCliente.FiltroPlazoAumentaConciliacion;
     chkPlazoDisminuye.Checked  := dmCliente.FiltroPlazoDisminuyeConciliacion;
     chkCambioRetencion.Checked := dmCliente.FiltroCambioRetencionConciliacion;
     nMontos.Valor := dmCliente.FiltroDiferenciaMontosConciliacion;
end;

procedure TWizActualizaDatosEmp.ActualizarGlobalFiltros;
begin
     FFiltroPlazoAumentaConciliacion    := dmCliente.FiltroPlazoAumentaConciliacion;
     FFiltroPlazoDisminuyeConciliacion  := dmCliente.FiltroPlazoDisminuyeConciliacion;
     FFiltroCambioRetencionConciliacion := dmCliente.FiltroCambioRetencionConciliacion;
     FFiltroDiferenciaMontosConciliacion := dmCliente.FiltroDiferenciaMontosConciliacion;
end;

procedure TWizActualizaDatosEmp.CancelarCambiosDataSet;
begin
     TClientDataSet(DataSet).CancelUpdates;
     //FCanceladoWizard := TRUE;
     FiltrosSugeridosConsultaDataSet;
end;

procedure TWizActualizaDatosEmp.CargaListaVerificacion;
var
  iEmpleado, iTempEmpleado: Integer;
  bEncontrado : Boolean;
begin
     dmConcilia.GetEmpFonacotGetLista( ParameterList );
     if dmConcilia.cdsCreditosDiferenciasTemp = NIL then
        dmConcilia.cdsCreditosDiferenciasTemp := TZetaClientDataSet.Create( Self )
     else if ( NOT dmConcilia.cdsCreditosDiferenciasTemp.isEmpty ) then
        dmConcilia.cdsCreditosDiferenciasTemp.EmptyDataSet;

     dmConcilia.cdsCreditosDiferenciasTemp.Data := DataSet.Data;
     with dmConcilia.cdsCreditosDiferenciasTemp do
     begin
          First;
          while( not Eof ) do
          begin
                bEncontrado := False;
                iEmpleado := FieldByName('NEmpleado').AsInteger;
                with dmConcilia.cdsDataLista do
                begin
                    First;
                    while ( not Eof ) do
                    begin
                          iTempEmpleado := dmConcilia.cdsDataLista.FieldByName('CB_CODIGO').AsInteger;
                          if iEmpleado = iTempEmpleado then
                          begin
                              bEncontrado := True;
                              break;
                          end
                          else
                              Next;
                    end;
                end;

                if bEncontrado = false then
                    Delete
                else
                    Next;
          end;
          MergeChangeLog;
          First;
     end;
end;

function TWizActualizaDatosEmp.Verificar: Boolean;
begin
     WizActualizaDatosEmpGridSelect := TWizActualizaDatosEmpGridSelect.Create( Self );
     try
          //Self.Hide;
          with WizActualizaDatosEmpGridSelect do
             begin

                  Dataset := TZetaClientDataSet( dmConcilia.cdsCreditosDiferenciasTemp );

                  if Dataset.RecordCount = 0 then
                  begin
                      ZetaDialogo.zInformation( '� Atenci�n !', 'La Lista a verificar est� vac�a', 0 );
                      Result := False;
                  end
                  else
                  begin
                      ShowModal;
                      Result := ( Modalidad = mrOk );
                  end;


                  if Result = False  then
                  begin
                       Verificacion := False
                  end;

             end;
          finally
             FreeAndNil( WizActualizaDatosEmpGridSelect );
     end;
end;

end.

