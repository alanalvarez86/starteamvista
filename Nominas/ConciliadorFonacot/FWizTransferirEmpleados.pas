unit FWizTransferirEmpleados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, cxContainer, cxEdit, Vcl.Buttons, Vcl.StdCtrls, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, Vcl.Mask, ZetaFecha, Vcl.Menus, cxButtons,
  DBClient, cxTextEdit;

type
  TWizTransferirEmpleados = class(TcxBaseWizard)
    DialogoBitacora: TOpenDialog;
    Label4: TLabel;
    ZFechaKardex: TZetaFecha;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    txtPathImportKardex: TcxTextEdit;
    BuscaBitacoraFONACOT: TcxButton;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure BuscaBitacoraFONACOTClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
      DataSet: TClientDataSet;
  protected
      function EjecutarWizard: Boolean;override;
      procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizTransferirEmpleados: TWizTransferirEmpleados;
  function ShowWizard( aDataSet: TClientDataSet): Boolean;overload;

implementation

{$R *.dfm}

uses
    DCliente, DConcilia, ZetaClientTools, ZetaClientDataSet,
    ZetaDialogo, ZBasicoSelectGrid_DevEx, ZetaCommonTools,
    FWizTransferirEmpleadosGridSelect, ZetaCommonClasses;

function TWizTransferirEmpleados.EjecutarWizard: Boolean;
begin
       with dmConcilia do
       begin
            LogInit( txtPathImportKardex.Text ,'Inicio Importar Kardex');
            TransferirEmpleadosFonacot(ZFechaKardex.Valor);
            LogEnd;
            //graba datos en registry
            try
               SetPathBitacora ( K_EMPLEADOS_NUEVOS_FONACOT, txtPathImportKardex.Text  );
            except
                  on Error: Exception do
                  begin
                       ZetaDialogo.zInformation( '', 'No cuenta con los derechos necesarios en los registros locales (Registry) '
                                                + ' para configurar la aplicaci�n en su computadora, contacte al Administrador del Sistema.', 0 );
                  end;
            end;
       end;
end;


procedure TWizTransferirEmpleados.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_NUEVOS_EMPLEADOS_CREDITOS;
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;

     ZFechaKardex.Valor := Date;
end;

procedure TWizTransferirEmpleados.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Se importar� el n�mero de Fonacot en el cat�logo de empleados en la ventana Datos > Otros > Fonacot';
     TClientDataSet( DataSet ).LogChanges := True;
     with dmConcilia, Application do
     begin
          txtPathImportKardex.Text:= GetPathBitacora( K_EMPLEADOS_NUEVOS_FONACOT );
     end;

end;


procedure TWizTransferirEmpleados.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if ( lok ) then
       TClientDataSet(DataSet).CancelUpdates;
end;

procedure TWizTransferirEmpleados.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                 if ( StrVacio( txtPathImportKardex.Text) ) then
                 begin
                      ZetaDialogo.ZError( 'Error al transferir empleados', 'El directorio de la bit�cora no puede quedar vac�o', 0 );
                      CanMove := False;
                 end
                 else if ( DataSet.IsEmpty ) then
                 begin
                      ZetaDialogo.ZError( 'Error al transferir empleados', 'Lista de empleados a importar est� vac�a', 0 );
                      CanMove := False;
                 end
                 else
                 begin
                      //si hay registros a procesar, abrir el grid de empleados para filtrar
                       CanMove := ZBasicoSelectGrid_DevEx.GridSelectBasico(
                                  TZetaClientDataSet( DataSet ), TWizTransferirEmpleadosGridSelect, FALSE );
                 end;
                 //si no hay registros a procesar, despues de filtrarlos por el grid de empleados, no avanzar
                 if ( DataSet.IsEmpty ) then
                 begin
                      ZetaDialogo.ZError( 'Error al transferir empleados', 'Lista de empleados a importar est� vac�a', 0 );
                      CanMove := False;
                      //cancelar los cambios realizados por el grid de empleados, cuando se eliminaron TODOS los registros a procesar
                      TClientDataSet( DataSet).CancelUpdates;
                 end;
            end;
     end;
end;

procedure TWizTransferirEmpleados.BuscaBitacoraFONACOTClick(Sender: TObject);
begin
     inherited;
     txtPathImportKardex.Text := ZetaClientTools.AbreDialogo( DialogoBitacora, txtPathImportKardex.Text, 'log' );
end;

procedure TWizTransferirEmpleados.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'FechaMovimiento', ZFechaKardex.Texto );
          AddString( 'RutaImportarKardex', txtPathImportKardex.Text );
     end;

     with Descripciones do
     begin
          AddString( 'Fecha de movimiento', ZFechaKardex.Texto );
          AddString( 'Guardar bit�cora en', txtPathImportKardex.Text );
          if ( DataSet <> nil ) and ( DataSet.Active ) and ( Wizard.EsPaginaActual( Ejecucion )) then
             AddInteger ( 'Cantidad de empleados', DataSet.RecordCount);
     end;
     inherited;

end;


//Parametro: aDataset
function ShowWizard( aDataSet: TClientDataSet): Boolean;
begin
     with TWizTransferirEmpleados.Create( Application ) do
     begin
          try
             DataSet := aDataSet;
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;

end.
