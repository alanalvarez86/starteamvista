unit DDiccionario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseDiccionario,
     ZetaCommonLists,
     ZetaClientDataSet;
     {,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists}

type
  TdmDiccionario = class(TdmBaseDiccionario)
  private
    { Private declarations }
  protected

  public
    { Public declarations }
    //procedure CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings );
    procedure CambioGlobales; override;
    procedure SetLookupNames; override;
    function ChecaDerechoConfidencial: Boolean;override;
    function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation
{$R *.DFM}

{procedure TdmDiccionario.CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings );
var
   oCampo: TObjetoString;
begin
     oLista.Clear;
     GetListaDatosTablas( oEntidad, lTodos );
     with cdsBuscaPorTabla do
     begin
          while NOT EOF do
          begin
               oCampo := TObjetoString.Create;
               oCampo.Campo := FieldByName( 'DI_NOMBRE' ).AsString;
               oLista.AddObject( FieldByName( 'DI_TITULO' ).AsString, oCampo );
               Next;
          end;
     end;
end;
}

procedure TdmDiccionario.CambioGlobales;
begin
end;
function TdmDiccionario.ChecaDerechoConfidencial: Boolean;
begin
     Result := FALSE;
end;

function TdmDiccionario.GetDerechosClasifi(  eClasifi: eClasifiReporte): Integer;
begin
     Result := 0;
end;

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     //dmSeleccion.SetLookupNames;
end;  

end.
