unit DCatalogos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
{$else}
     Catalogos_TLB,
{$endif}
  ZetaCommonClasses,
  ZetaCommonLists,
  ZetaClientDataSet,
  ZetaKeyLookup_DevEx,
  Variants;
type
  TdmCatalogos = class(TDataModule)
    cdsPuestos: TZetaLookupDataSet;
    cdsTurnos: TZetaLookupDataSet;
    cdsClasifi: TZetaLookupDataSet;
    cdsConceptos: TZetaLookupDataSet;
    cdsCondiciones: TZetaLookupDataSet;
    cdsNomParamLookUp: TZetaLookupDataSet;
    cdsConceptosLookUp: TZetaLookupDataSet;
    cdsHorarios: TZetaLookupDataSet;
    cdsNomParam: TZetaLookupDataSet;
    cdsPeriodo: TZetaLookupDataSet;
    cdsMaestros: TZetaLookupDataSet;
    cdsCursos: TZetaLookupDataSet;
    cdsTPeriodos: TZetaLookupDataSet;
    cdsCertificaciones: TZetaLookupDataSet;
    cdsEstablecimientos: TZetaLookupDataSet;
    cdsCompetencias: TZetaLookupDataSet;
    cdsCompNiveles: TZetaLookupDataSet;
    cdsCompCursos: TZetaClientDataSet;
    cdsCompRevisiones: TZetaClientDataSet;
    cdsCatPerfiles: TZetaLookupDataSet;
    cdsRevPerfiles: TZetaClientDataSet;
    cdsMatPerfilComps: TZetaClientDataSet;
    procedure cdsPuestosAlAdquirirDatos(Sender: TObject);
    procedure cdsTurnosAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiAlAdquirirDatos(Sender: TObject);
    procedure cdsConceptosAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsConceptosLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsHorariosAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamAlAdquirirDatos(Sender: TObject);
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodoAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodoLookupDescription(Sender: TZetaLookupDataSet; var sDescription: String);
    procedure cdsPeriodoLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsPeriodoLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsSuperLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsMaestrosAlAdquirirDatos(Sender: TObject);
    procedure cdsCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsTPeriodosAlAdquirirDatos(Sender: TObject);
    procedure cdsCertificacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsEstablecimientosAlAdquirirDatos(Sender: TObject);
    procedure cdsCompetenciasAlAdquirirDatos(Sender: TObject);
    procedure cdsCatPerfilesAlAdquirirDatos(Sender: TObject);
    procedure cdsMatPerfilCompsAlAdquirirDatos(Sender: TObject);

  private
    { Private declarations }
    FDuracion_Curso: TDiasHoras;
    FOperacion : eOperacionConflicto;
{$ifdef DOS_CAPAS}
    function GetServerCatalogos: TdmServerCatalogos;
    property ServerCatalogo: TdmServerCatalogos read GetServerCatalogos;
{$else}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
{$endif}
  public
    { Public declarations }
    property Duracion_Curso: TDiasHoras read FDuracion_Curso write FDuracion_Curso;
    property Operacion: eOperacionConflicto read FOperacion write FOperacion;

    procedure GetDatosPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo );
    function GrabaSesiones ( const DeltaSesion, DeltaAprobados: OleVariant; var ResultAprobados: OleVariant; var iFolio, ErrorCount, ErrorKCCount : Integer ): OleVariant;
    //procedure AplicaFiltroStatusPeriodo(DBLookup: TZetaKeyLookup;Aplicar:Boolean ); overload;
    //DevEx (by am): Se cobrecarga el metodo para que siga funcionando con el nuevo componente del Lookup
    procedure AplicaFiltroStatusPeriodo(DBLookup: TZetaKeyLookup_DevEx;Aplicar:Boolean ); overload;
    function GetUltimaRevisionPerfil:TDateTime;
    function GetUltimaRevisionCompetencia:TDateTime;
    function GetNivelRequeridoCompetencia(Competencia: string;var Descripcion:string): Integer;
  end;

var
  dmCatalogos: TdmCatalogos;

implementation

uses DCliente,
     DGlobal,
     ZGlobalTress,
     ZetaCommonTools,
     //ZetaBuscaPeriodo,
     ZetaBuscaPeriodo_DevEx,
     ZAccesosMgr,
     ZAccesosTress,
     FToolsCatalogos, FTressShell, ZReconcile;
//     FCalendarioRitmo;

{$R *.DFM}

{ TdmCatalogos }

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
{$endif}

procedure TdmCatalogos.DataModuleCreate(Sender: TObject);
begin
     FDuracion_Curso:= 0;
end;

procedure TdmCatalogos.cdsSuperLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmCatalogos.cdsPuestosAlAdquirirDatos(Sender: TObject);
begin
     cdsPuestos.Data := ServerCatalogo.GetPuestos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsTurnosAlAdquirirDatos(Sender: TObject);
begin
     cdsTurnos.Data := ServerCatalogo.GetTurnos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsClasifiAlAdquirirDatos(Sender: TObject);
begin
     cdsClasifi.Data := ServerCatalogo.GetClasifi( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsHorariosAlAdquirirDatos(Sender: TObject);
begin
     cdsHorarios.Data := ServerCatalogo.GetHorarios( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsNomParamAlAdquirirDatos(Sender: TObject);
begin
     cdsNomParam.Data := ServerCatalogo.GetNomParam( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
begin
     //cdsNomParamLookUp.Data := ServerCatalogo.GetNomParam( dmCliente.Empresa );
     with cdsNomParam do
     begin
          Conectar;
          cdsNomParamLookUp.Data := Data;
     end;
end;

procedure TdmCatalogos.cdsConceptosAlAdquirirDatos(Sender: TObject);
begin
     cdsConceptos.Data := ServerCatalogo.GetConceptos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsConceptosLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsConceptos do
     begin
          Conectar;
          cdsConceptosLookUp.Data := Data;
     end;
end;

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

{ cdsPeriodo }

procedure TdmCatalogos.cdsPeriodoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          //GetDatosPeriodo( YearDefault, PeriodoTipo );
     end;
end;

procedure TdmCatalogos.GetDatosPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo );
begin
     with cdsPeriodo do
     begin
          Data := ServerCatalogo.GetPeriodos( dmCliente.Empresa, iYear, Ord(TipoPeriodo) );
          ResetDataChange;
     end;
end;

procedure TdmCatalogos.cdsPeriodoLookupDescription(Sender: TZetaLookupDataSet;
          var sDescription: String);
begin
     with Sender do
     begin
          sDescription := 'De ' + FechaCorta( FieldByName( 'PE_FEC_INI' ).AsDateTime ) +
                          ' a ' + FechaCorta( FieldByName( 'PE_FEC_FIN' ).AsDateTime );
     end;
end;

procedure TdmCatalogos.cdsPeriodoLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean;
          const sFilter, sKey: String; var sDescription: String);
begin
     with cdsPeriodo do
     begin
          Filtered := False;
          Filter := sFilter;
          Filtered := True;
          lOk := Locate( 'PE_NUMERO', StrToIntDef( sKey, 0 ), [] );
          if lOk then
          begin
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmCatalogos.cdsPeriodoLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean;
          const sFilter: String; var sKey, sDescription: String);
begin
     //lOk := ZetaBuscaPeriodo.BuscaPeriodoDialogo( Sender, sFilter, sKey, sDescription );
     lOk := ZetaBuscaPeriodo_DevEx.BuscaPeriodoDialogo( Sender, sFilter, sKey, sDescription ); //DevEx: Invoca a la nueva forma de ZetaBuscaPeriodo_DevEx
end;


{cdsCursos}

procedure TdmCatalogos.cdsCursosAlAdquirirDatos(Sender: TObject);
begin
     cdsCursos.Data:= ServerCatalogo.GetVistaCursos( dmCliente.Empresa );
end;

{cdsMaestros}

procedure TdmCatalogos.cdsMaestrosAlAdquirirDatos(Sender: TObject);
begin
     cdsMaestros.Data := ServerCatalogo.GetMaestros( dmCliente.Empresa );
end;

function TdmCatalogos.GrabaSesiones ( const DeltaSesion, DeltaAprobados: OleVariant; var ResultAprobados: OleVariant; var iFolio, ErrorCount, ErrorKCCount : Integer ): OleVariant;
begin
     Result := ServerCatalogo.GrabaSesiones( dmCliente.Empresa, DeltaSesion, DeltaAprobados, iFolio, Ord( ocReportar ), ErrorCount, ErrorKCCount, ResultAprobados );
end;

{cdsTPeriodos}

procedure TdmCatalogos.cdsTPeriodosAlAdquirirDatos(Sender: TObject);
begin
     cdsTPeriodos.Data := ServerCatalogo.GetTPeriodos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCertificacionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCertificaciones.Data := ServerCatalogo.GetCertificaciones(dmCliente.Empresa);  
end;

procedure TdmCatalogos.cdsEstablecimientosAlAdquirirDatos(Sender: TObject);
begin
     cdsEstablecimientos.Data := ServerCatalogo.GetEstablecimientos( dmCliente.Empresa );
end;

{
procedure TdmCatalogos.AplicaFiltroStatusPeriodo(DBLookup: TZetaKeyLookup;Aplicar:Boolean);
begin
      if Aplicar then
      begin
           //EZ: En Supervisores siempre aplica el filtro
           DBLookup.Filtro := Format('PE_STATUS < %d',[Ord(spAfectadaParcial)]);
           with cdsPeriodo do
           begin
                Filtered := False;
                Filter :=  DBLookup.Filtro;
                Filtered := True;
           end;
      end
      else
      begin
           //DBLookup.Filtro := VACIO;
           with cdsPeriodo do
           begin
                Filtered := False;
           end;
      end;

end;
}
//DevEx (by am): Se sobrecargo el metodo para que funcione en los nuevos componentes de Lookup

procedure TdmCatalogos.AplicaFiltroStatusPeriodo(DBLookup: TZetaKeyLookup_DevEx;Aplicar:Boolean);
begin
      if Aplicar then
      begin
           //EZ: En Supervisores siempre aplica el filtro
           DBLookup.Filtro := Format('PE_STATUS < %d',[Ord(spAfectadaParcial)]);
           with cdsPeriodo do
           begin
                Filtered := False;
                Filter :=  DBLookup.Filtro;
                Filtered := True;
           end;
      end
      else
      begin
           //DBLookup.Filtro := VACIO;
           with cdsPeriodo do
           begin
                Filtered := False;
           end;
      end;

end;
procedure TdmCatalogos.cdsCompetenciasAlAdquirirDatos(Sender: TObject);
VAR
   oRevisiones,oCursos,oNiveles :OleVariant;
begin
     cdsCompetencias.Data := ServerCatalogo.GetCompetencias(dmCliente.Empresa,oRevisiones,oNiveles,oCursos);
     cdsCompRevisiones.Data := oRevisiones;
     cdsCompNiveles.Data := oNiveles;
     cdsCompCursos.Data := oCursos;
end;

procedure TdmCatalogos.cdsCatPerfilesAlAdquirirDatos(Sender: TObject);
VAR
   oRevisiones,oPtoGpoCompeten:OleVariant;
begin
     cdsCatPerfiles.Data := ServerCatalogo.GetCatPerfiles(dmCliente.Empresa,VACIO,oRevisiones,oPtoGpoCompeten);
     cdsRevPerfiles.Data := oRevisiones;
     //cdsGpoCompPuesto.Data  := oPtoGpoCompeten;
end;

function TdmCatalogos.GetUltimaRevisionCompetencia: TDateTime;
begin
     Result := NullDateTime;
     with cdsCompRevisiones  do
     begin
          Filtered := False;
          Filter := Format(' CC_CODIGO = ''%s''',[cdsCompetencias.FieldByName('CC_CODIGO').AsString]);
          Filtered := True;
          //OrdenarPor(cdsCompRevisiones,'RC_FEC_INI');
          if RecordCount > 0 then
             Result := FieldByName('RC_FEC_INI').AsDateTime;
     end;
end;

function TdmCatalogos.GetUltimaRevisionPerfil: TDateTime;
begin
      Result := NullDateTime;
     with cdsRevPerfiles do
     begin
          Filtered := False;
          Filter := Format(' CP_CODIGO = ''%s''',[cdsCatPerfiles.FieldByName('CP_CODIGO').AsString]);
          Filtered := True;
         //OrdenarPor(cdsRevPerfiles,'RP_FEC_INI');
          if RecordCount > 0 then
             Result := FieldByName('RP_FEC_INI').AsDateTime;
     end;
end;


function TdmCatalogos.GetNivelRequeridoCompetencia(Competencia:string;var Descripcion:string):Integer;
var
   GpoComp:string;
   Nivel:Integer;
begin
     cdsMatPerfilComps.Conectar;
     GpoComp := cdsCatPerfiles.FieldByName('CP_CODIGO').AsString;
     With cdsMatPerfilComps do
     begin
          Locate('CC_CODIGO;CP_CODIGO',VarArrayOf([Competencia,GpoComp]),[]);
          Nivel := FieldByName('NC_NIVEL').AsInteger;
     end;

     with cdsCompNiveles do
     begin
          Locate('CC_CODIGO;NC_NIVEL',VarArrayOf([Competencia,Nivel]),[]);
          Descripcion := FieldByName('NC_DESCRIP').AsString;
     end;
     Result := Nivel;
end;

procedure TdmCatalogos.cdsMatPerfilCompsAlAdquirirDatos(Sender: TObject);
begin
     cdsMatPerfilComps.Data := ServerCatalogo.GetMatrizPerfilComps(dmCliente.Empresa );
end;

end.

