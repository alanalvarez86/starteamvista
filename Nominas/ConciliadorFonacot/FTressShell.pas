 unit FTressShell;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBasicoNavBarShell, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsdxNavBarPainter, dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, System.Actions, Vcl.ActnList, cxLocalization, dxBar,
  cxStyles, cxClasses, dxSkinsForm, Vcl.ExtCtrls, Vcl.ImgList, dxRibbon,
  dxNavBar, Vcl.StdCtrls, cxButtons, dxStatusBar, dxRibbonStatusBar, cxPC,
  cxContainer, cxEdit, cxTextEdit, cxLabel, ZetaKeyLookup_DevEx, Data.DB,
  HtmlHelpViewer, cxProgressBar,
  FAutoClasses, dxBarBuiltInMenu, dxRibbonCustomizationForm, Vcl.ComCtrls;

type
  TTressShell = class(TBasicoNavBarShell)

	ImageList: TImageList;
    TabFonacot: TdxRibbonTab;
    FActualiza_NuevosEmpleadosCreditosbtn: TdxBarLargeButton;
    FActualiza_ImportarNuevosCreditosbtn: TdxBarLargeButton;
    FActualiza_ActualizaDatosCreditosbtn: TdxBarLargeButton;
    FConfronta_GenerarCedulabtn: TdxBarLargeButton;
    FConciliacion_ConciliarCreditosbtn: TdxBarLargeButton;
    FConfronta_CalcularConfrontabtn: TdxBarLargeButton;
    F_ConciliarCreditos: TAction;
    F_CalcularConfronta: TAction;
    F_NuevosEmpleadosCreditos: TAction;
    F_ActualizaDatosCreditos: TAction;
    F_GenerarCedula: TAction;
    F_ImportarNuevosCreditos: TAction;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConciliarProceso( sRazonSocial: String; sRuta: String; iAnio: Integer; iMes: Integer; Bajas: Boolean; FechaBajas: TDate);
    procedure CalcularConfronta( sRazonSocial: String; sRuta: String ; iAnio: Integer; iMes: Integer);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure F_ConciliarCreditosExecute(Sender: TObject);
    procedure ClientAreaPG_DevExChange(Sender: TObject);
    procedure F_NuevosEmpleadosCreditosExecute(Sender: TObject);
    procedure F_ActualizaDatosCreditosExecute(Sender: TObject);
    procedure F_CalcularConfrontaExecute(Sender: TObject);
    procedure F_GenerarCedulaExecute(Sender: TObject);
    procedure F_ImportarNuevosCreditosExecute(Sender: TObject);
  private
    { Private declarations }
     ProgressBar: TcxProgressBar;
     function EsFormaFONACOT( Clase: TClass ): Boolean;

  protected
    procedure CargaTraducciones; override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure HabilitarBotonesRibbon( Habilitar: Boolean; RealizoConciliacion: Boolean; RealizoConfronta: Boolean );
    procedure VacioPanelConciliacionShell;//Conciliador FONACOT - Mejoras Limpiar fecha de conciliacion en la la barra de status del Shell
  public
    { Public declarations }
   procedure ConciliarEnd;
   procedure ConciliarStart(const iMaxSteps: Integer);
   function ConciliarStep: Boolean;
   procedure LLenaOpcionesGrupos; override;
   procedure SetProgressbar( BarraProgreso: TcxProgressBar);
   procedure SetFechaConciliacionShell(const iAnio, iMes: Integer);//Conciliador FONACOT - Mejoras Mostrar Fecha Consiliacion en la barra de status del Shell
  end;

 const
     K_RELLENO     = 10;
     K_CARDINAL_ESPANOL = 2058;
     K_PANEL_CONCILIACION = 1;//Conciliador FONACOT - Mejoras Posicion Panel Shell
var
  TressShell: TTressShell;

implementation

uses
    ZetaClientTools,
    DConcilia,
    DCliente,
    {$ifdef DOS_CAPAS}
    DZetaServerProvider,
    ZetaSQLBroker,
    {$endif}
    ZetaCommonTools,
    ZetaCommonLists,
    ZetaCommonClasses,
    ZGlobalTress,
    ZetaDialogo,
    ZBaseThreads, DBClient,DGlobal,DDiccionario,
    ZImprimeGrid,ZetaBuscaEmpleado_DevEx,
    ZetaRegistryCliente, ZAccesosMgr, ZAccesosTress,
    ZcxWizardBasico,
    FWizConciliaCreditos,
    ZetaDespConsulta,
    FCreditosDiferencias,
    FCreditosNuevos,
    FNuevosEmpleadosFonacot,
    FWizTransferirEmpleados,
    FWizActualizaDatosEmp,
    FWizCalculoConfPago,
    FWizGeneraCedPago,
    FWizNuevosCreditos;


{$R *.dfm}


procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmCliente := TdmCliente.Create( Self );
     dmConcilia := TdmConcilia.Create( Self );
     dmDiccionario := TdmDiccionario.Create( self );

     Global := TdmGlobal.Create;
     inherited;

     FHelpGlosario := Application.HelpFile;
     HelpContext := H_CONCIL_COMENZAR;
     dmCliente.ConciliacionTerminada := False;
     dmCliente.ConfrontaTerminada := False;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin

     inherited;
     FreeAndNil( Global );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmCliente );
     FreeAndNil( dmConcilia );
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     CargaTraducciones;

end;

procedure TTressShell.F_ActualizaDatosCreditosExecute(Sender: TObject);
begin
     inherited;
     if ( dmConcilia.cdsCreditosDiferencias.IsEmpty ) then
            ZetaDialogo.zInformation( 'Actualiza cr�ditos', 'No se encontraron registros a procesar', 0 )
     else
     begin
          with dmCliente do
          begin
               FWizActualizaDatosEmp.ShowWizard(  TClientDataSet( dmConcilia.cdsCreditosDiferencias ) );
          end;
     end;
end;

procedure TTressShell.F_CalcularConfrontaExecute(Sender: TObject);
begin
     inherited;
     ZcxWizardBasico.ShowWizard( TWizCalculoConfPago );
end;

procedure TTressShell.F_ConciliarCreditosExecute(Sender: TObject);
begin
    inherited;
    ZcxWizardBasico.ShowWizard( TWizConciliaCreditos );
end;

procedure TTressShell.F_GenerarCedulaExecute(Sender: TObject);
begin
     inherited;
     if ( dmConcilia.cdsConfrontacion.IsEmpty ) then
        ZetaDialogo.zInformation( 'Generar c�dula', 'No se encontraron registros a procesar', 0 )
     else
     begin
          ZcxWizardBasico.ShowWizard( TWizGeneraCedPago );
     end;

end;

procedure TTressShell.F_ImportarNuevosCreditosExecute(Sender: TObject);
begin
     inherited;
     if ( dmConcilia.cdsCreditosNuevos.IsEmpty ) then
        ZetaDialogo.zInformation( 'Importar nuevos cr�ditos', 'No se encontraron registros a procesar', 0 )
     else
     begin
          with dmCliente do
          begin
               FWizNuevosCreditos.ShowWizard(  TClientDataSet( dmConcilia.cdsCreditosNuevos ) );
          end;
     end;
end;

procedure TTressShell.F_NuevosEmpleadosCreditosExecute(Sender: TObject);
begin
     inherited;
     if ( dmConcilia.cdsEmpleaFonacot.IsEmpty ) then
        ZetaDialogo.zInformation( 'Nuevos empleados', 'No se encontraron registros a procesar', 0 )
     else
     begin
          with dmCliente do
          begin
               dmConcilia.cdsEmpleaFonacot.MergeChangeLog;
               FWizTransferirEmpleados.ShowWizard(  TClientDataSet( dmConcilia.cdsEmpleaFonacot) );
          end;
     end;
end;


procedure TTressShell.DoOpenAll;
begin
     inherited DoOpenAll;

     SetArbolitosLength(12);
     CreaNavBar;
     LLenaOpcionesGrupos;
     with dmCliente.cdsRSocial do
     begin
          Conectar;
     end;
     dmCliente.ConciliacionTerminada := False;
     dmCliente.ConfrontaTerminada := False;
     HabilitarBotonesRibbon(False, False, False);
     dmCliente.CedulaAnioConciliacion := TheYear( Date );//Mejoras FONACOT - Anio Sugerido
     dmCliente.CedulaMesConciliacion := TheMonth( Date );//Mejoras FONACOT - Mes Sugerido
     dmCliente.FiltroPlazoAumentaConciliacion := FALSE;//Resetear Filtro
     dmCliente.FiltroPlazoDisminuyeConciliacion := FALSE;//Resetear Filtro
     dmCliente.FiltroCambioRetencionConciliacion := FALSE;//Resetear Filtro
     VacioPanelConciliacionShell;//Resetear Fecha Conciliacion Shell
     Global.Conectar;

     //carga valores activos


     if Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA) then
     begin
          ZetaDialogo.zInformation( 'Conciliador Fonacot', 'No es posible ejecutar el Conciliador Fonacot '
                                    + CR_LF +
                                    'cuando los pr�stamos se manejan por c�dula mensual.', 0 );
          Application.Terminate;
     end;

     try
        dmCliente.RazonSocialUltimaConciliacion := ClientRegistry.RazonSocialConciliacion;
        dmCliente.CedulaUltimaConciliacion := ClientRegistry.RutaUltimaCedula;
        dmCliente.RazonSocialUltimaConfronta := ClientRegistry.RazonSocialConfronta;
        dmCliente.CedulaUltimaConfronta := ClientRegistry.RutaUltimaCedulaConfronta;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zInformation( '', 'No cuenta con los derechos necesarios en los registros locales (Registry) '
                                                            + ' para configurar la aplicaci�n en su computadora, contacte al Administrador del Sistema.', 0 );
           end;
     end;
end;


procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
     inherited;
     _V_Cerrar.Execute;
end;


procedure TTressShell.ConciliarProceso( sRazonSocial: String; sRuta: String; iAnio: Integer; iMes: Integer; Bajas: Boolean; FechaBajas: TDate);
begin
     if ( not dmCliente.EsModoDemo ) then
     begin
          HabilitarBotonesRibbon(True, True, False);
          dmCliente.CancelaConciliacion := FALSE;
          dmCliente.ConciliacionTerminada := False;
          CierraFormaTodas;
          try
             with dmConcilia do
             begin
                  begin
                       RazonSocial := sRazonSocial;
                       PathDatos := sRuta;
                       ConciliarFonacot(iAnio, iMes, Bajas, FechaBajas);
                       if ( not dmCliente.CancelaConciliacion ) then
                       begin
                             ZetaDialogo.ZInformation('Proceso', 'Proceso terminado', 0 );

                             if ( not ArchivoInvalido )then
                             begin
                                  //graba datos en registry
                                  try
                                      dmConcilia.SetUltimaCedula( sRuta );
                                      ClientRegistry.RazonSocialConciliacion := sRazonSocial;
                                  except
                                        on Error: Exception do
                                        begin
                                             ZetaDialogo.zInformation( '', 'No cuenta con los derechos necesarios en los registros locales (Registry) '
                                                                  + ' para configurar la aplicaci�n en su computadora, contacte al Administrador del Sistema.', 0 );
                                        end;
                                  end;
                                  dmCliente.ConciliacionTerminada := True;
                                  dmCliente.CedulaUltimaConciliacion := sRuta;
                                  dmCliente.RazonSocialUltimaConciliacion := sRazonSocial;
                                  dmCliente.CedulaAnioConciliacion := iAnio;
                                  dmCliente.CedulaMesConciliacion := iMes;
                                  dmCliente.FiltroPlazoAumentaConciliacion := TRUE;
                                  dmCliente.FiltroPlazoDisminuyeConciliacion := TRUE;
                                  dmCliente.FiltroCambioRetencionConciliacion := TRUE;
                                  SetFechaConciliacionShell( iAnio, iMes );//Conciliador FONACOT - Mejoras
                                  if ( ExistenErroresCedula )then
                                  begin
                                        AbreFormaConsulta( efcErroresCedula );
                                  end
                                  else
                                        AbreFormaConsulta( efcResumenEmpleadosConCredito );
                                  end
                             else
                             begin
                                  ZetaDialogo.ZError('Leyendo c�dula','El archivo de c�dula de Fonacot es inv�lido � est� vac�o',0);
                             end;
                       end
                       else
                           dmConcilia.LimpiarDataSetsConciliacion;
                  end;
             end;
          finally
                 dmCliente.CancelaConciliacion := FALSE;
          end;
     end;

end;


procedure TTressShell.CalcularConfronta( sRazonSocial: String; sRuta: String ; iAnio: Integer; iMes: Integer);
begin
     if ( not dmCliente.EsModoDemo ) then
     begin
          HabilitarBotonesRibbon(True, False, True);

          dmCliente.CancelaConciliacion := FALSE;
          dmCliente.ConfrontaTerminada := False;
          CierraFormaTodas;
          try
             with dmConcilia do
             begin
                  RazonSocial := sRazonSocial;
                  PathDatos := sRuta;
                  Confrontar( iAnio , iMes  );
                  if ( not dmCliente.CancelaConciliacion ) then
                  begin
                       ZetaDialogo.ZInformation('Proceso', 'Proceso terminado', 0 );

                       if ( not ArchivoInvalido )then
                       begin
                            //graba datos en registry
                            try
                                ClientRegistry.RutaUltimaCedulaConfronta := sRuta;
                                ClientRegistry.RazonSocialConfronta := sRazonSocial;
                            except
                                  on Error: Exception do
                                  begin
                                        ZetaDialogo.zInformation( '', 'No cuenta con los derechos necesarios en los registros locales (Registry) '
                                                          + ' para configurar la aplicaci�n en su computadora, contacte al Administrador del Sistema.', 0 );
                                  end;
                            end;
                            dmCliente.ConfrontaTerminada := True;
                            dmCliente.CedulaUltimaConfronta := sRuta;
                            dmCliente.RazonSocialUltimaConfronta := sRazonSocial;

                            AbreFormaConsulta( efcCedulaPago );
                            dmCliente.MesConfronta := iMes;
                       end
                       else
                       begin
                             ZetaDialogo.ZError('Leyendo c�dula','El archivo de c�dula de Fonacot es inv�lido � est� vac�o',0);
                       end;
                  end
                  else
                       dmConcilia.cdsConfrontacion.Active := False;
             end;
          finally
                 dmCliente.CancelaConciliacion := FALSE;
          end;
     end;

end;

procedure TTressShell.ConciliarStart( const iMaxSteps: Integer );
begin
     with ProgressBar do
     begin
          Position := 0;
          Properties.Max := iMaxSteps;
     end;
     Application.ProcessMessages;
end;

function TTressShell.ConciliarStep: Boolean;
begin
     Result := True;
     with ProgressBar do
     begin
          Position := Position + 1;
     end;
     Result := NOT dmCliente.CancelaConciliacion;
     Application.ProcessMessages;
end;

procedure TTressShell.ConciliarEnd;
begin
     with ProgressBar do
     begin
          Position := Properties.Max;
     end;
     Application.ProcessMessages;

end;


procedure TTressShell.HabilitarBotonesRibbon( Habilitar: Boolean; RealizoConciliacion: Boolean; RealizoConfronta: Boolean );
var
   HabilitarProceso: Boolean;
   Autorizado: Boolean;
begin

     HabilitarProceso := False;
     Autorizado := False;

     Autorizado := dmCliente.ModuloAutorizado( okNomina );
     HabilitarProceso := Autorizado;

     //inicializa botones
     if ( not Habilitar) then
     begin
          FActualiza_NuevosEmpleadosCreditosbtn.Enabled := False;
          FActualiza_ImportarNuevosCreditosbtn.Enabled := False;
          FActualiza_ActualizaDatosCreditosbtn.Enabled := False;
          FConfronta_GenerarCedulabtn.Enabled := False;
     end;

     FConciliacion_ConciliarCreditosbtn.Enabled :=  HabilitarProceso;
     if ( Habilitar and RealizoConciliacion ) then
     begin
          FActualiza_NuevosEmpleadosCreditosbtn.Enabled := HabilitarProceso
                                                      and dmCliente.ChecaAccesoImportarKardex;
          FActualiza_ImportarNuevosCreditosbtn.Enabled := HabilitarProceso
                                                     and dmCliente.ChecaAccesoImportarPrestamo;
          FActualiza_ActualizaDatosCreditosbtn.Enabled := HabilitarProceso
                                                     and dmCliente.ChecaAccesoImportarPrestamo;
     end;
     FConfronta_CalcularConfrontabtn.Enabled := HabilitarProceso;
     if ( Habilitar and RealizoConfronta ) then
     begin
          FConfronta_GenerarCedulabtn.Enabled := HabilitarProceso;
     end;

end;

procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := K_CARDINAL_ESPANOL;  // Cardinal para Espanol (Mexico)

end;


procedure TTressShell.LLenaOpcionesGrupos;
var
   iIndice : Integer;

begin
     if ( dmCliente.EmpresaAbierta ) then
     begin
          //recorre los grupos del navbar, le asigna su imagen por su nombre
          for iIndice := 0 to (DevEx_NavBar.Groups.Count - 1)  do
          begin
               DevEx_NavBar.Groups[iIndice].LargeImageIndex := iIndice;
               DevEx_NavBar.Groups[iIndice].SmallImageIndex := iIndice;
               //para que use las imagenes grandes
               DevEx_NavBar.Groups[iIndice].UseSmallImages := False;
          end;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          CierraDatasets(dmConcilia);
          CierraDatasets(dmDiccionario);
          dmCliente.cdsRSocial.Active := False;
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.SetProgressbar( BarraProgreso: TcxProgressBar);
begin
     Self.ProgressBar := BarraProgreso;
end;

procedure TTressShell.ClientAreaPG_DevExChange(Sender: TObject);
var iControles: integer;
begin
     inherited;
     //activar el tab de fonacot
     if ( ClientAreaPG_DevEx.ActivePage <> nil ) then
     begin
          for  iControles:= 0 to (ClientAreaPG_DevEx.ActivePage.ControlCount - 1 )  do
          begin
               if EsFormaFONACOT( ClientAreaPG_DevEx.ActivePage.Controls[iControles].ClassType ) then
               begin
                    DevEx_ShellRibbon.ActiveTab := TabFonacot;
               end;
          end;
     end;
end;


function TTressShell.EsFormaFONACOT( Clase: TClass ): Boolean;
begin
     Result := False;
     if ( ( Clase = TCreditosDiferencias ) or  ( Clase = TNuevosEmpleadosFonacot ) or ( Clase = TCreditosNuevos ) ) then
        Result:= True
     else
         Result := False;
end;

procedure TTressShell.SetFechaConciliacionShell(const iAnio, iMes: Integer);//Conciliador FONACOT - Mejoras
begin
     with dmConcilia do
     begin
          Anio := iAnio;
          Mes := iMes;
          StatusBarMsg( GetDescripcionFechaConciliacion, K_PANEL_CONCILIACION );
          Application.ProcessMessages;
     end;
end;

procedure TTressShell.VacioPanelConciliacionShell;
begin
      StatusBarMsg( VACIO, K_PANEL_CONCILIACION );
end;


end.
