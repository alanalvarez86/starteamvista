unit FWizConciliaCreditos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.Menus,
  Vcl.ExtCtrls, Vcl.StdCtrls, cxTextEdit, cxButtons,
  ZetaKeyLookup_DevEx, cxProgressBar, cxDropDownEdit,
  ZetaCXStateComboBox, cxMaskEdit, cxSpinEdit, Vcl.Mask,
  ZetaFecha, cxCheckBox, TressMorado2013;

type
  TWizConciliaCreditos = class(TcxBaseWizard)
    gbArchivoCedula: TcxGroupBox;
    cxLabel1: TcxLabel;
    btnCedulaFonacot: TcxButton;
    ArchivoCedulaFonacot: TcxTextEdit;
    DialogoCedula: TOpenDialog;
    lblRazonSocialConciliar: TLabel;
    varRazonSocialConciliar: TZetaKeyLookup_DevEx;
    PanelProgressBar: TPanel;
    ProgressBar: TcxProgressBar;
    cxLabel2: TcxLabel;
    zcboMes: TcxStateComboBox;
    cxLabel3: TcxLabel;
    txtAnio: TcxSpinEdit;
    cbBajas: TcxCheckBox;
    zdbFecha: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure btnCedulaFonacotClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);

  private
    { Private declarations }
    procedure HabilitaFiltroRazonSocial;
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizConciliaCreditos: TWizConciliaCreditos;

implementation

uses
    DCliente, ZetaClientTools, ZetaCommonClasses,
    ZetaDialogo, FTressShell, ZetaCommonTools, ZetaCommonLists;

{$R *.dfm}

procedure TWizConciliaCreditos.btnCedulaFonacotClick(Sender: TObject);
begin
    inherited;
    ArchivoCedulaFonacot.Text := AbreDialogo( DialogoCedula, ArchivoCedulaFonacot.Text,'csv' );
end;

procedure TWizConciliaCreditos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_CONCILIAR_CREDITOS;

     with dmCliente.cdsRSocial do
     begin
          Conectar;
     end;
     HabilitaFiltroRazonSocial;
     varRazonSocialConciliar.LookupDataset := dmCliente.cdsRSocial;

     //lee valor de la conciliacion mas reciente
     ArchivoCedulaFonacot.Text := dmCliente.CedulaUltimaConciliacion;
     if ( varRazonSocialConciliar.Visible ) then
          varRazonSocialConciliar.Llave := dmCliente.RazonSocialUltimaConciliacion;

     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;


end;

procedure TWizConciliaCreditos.FormShow(Sender: TObject);
begin
     inherited;
     txtAnio.Value := dmCliente.CedulaAnioConciliacion;//Anio de conciliacion sugerido
     zcboMes.Indice  := dmCliente.CedulaMesConciliacion;//Mes de conciliacion sugerido
     zdbFecha.Valor := StrAsDate(zcboMes.Indice.ToString + '/' + '01' + '/' + IntToStr(txtAnio.Value));
     Advertencia.Caption :=  'Al aplicar este proceso se comparar� la informaci�n que se tiene en Sistema TRESS contra la que se tiene en la c�dula de fonacot, lo cual arrojar� diferencias si hubiese.';
end;

procedure TWizConciliaCreditos.HabilitaFiltroRazonSocial;
var
   lHabilitarFiltro : Boolean;
begin
     lHabilitarFiltro := False;
     with dmCliente.cdsRSocial do
     begin
          lHabilitarFiltro := (RecordCount > 1)
     end;

     varRazonSocialConciliar.LookupDataset := dmCliente.cdsRSocial;
     varRazonSocialConciliar.Visible := lHabilitarFiltro;
     varRazonSocialConciliar.Enabled := lHabilitarFiltro;
     lblRazonSocialConciliar.Visible := lHabilitarFiltro;
     lblRazonSocialConciliar.Enabled := lHabilitarFiltro;

     if not lHabilitarFiltro then
     begin
          varRazonSocialConciliar.Llave := VACIO;
     end;

end;

procedure TWizConciliaCreditos.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     dmCliente.CancelaConciliacion := lOk;
end;

procedure TWizConciliaCreditos.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                 if ( StrVacio (ArchivoCedulaFonacot.Text) ) then
                 begin
                      ZetaDialogo.ZError( 'Error al conciliar cr�ditos', 'El archivo de c�dula de Fonacot est� vac�o ', 0 );
                      CanMove := False;
                 end;
            end;
     end;
end;

procedure TWizConciliaCreditos.CargaParametros;
begin

     with ParameterList do
     begin
          AddString( 'Mes', ObtieneElemento( lfMeses, zcboMes.Indice - 1 ));
          AddInteger( 'A�o' , txtAnio.Value );
          AddString( 'ArchivoCedula', ArchivoCedulaFonacot.Text );
          AddString( 'RazonSocial', varRazonSocialConciliar.Llave );
          if cbBajas.Checked then
          begin
            AddBoolean( 'Bajas' , True );
            AddDate( 'FechaBajas', zdbFecha.Valor );
          end
          else
            AddBoolean( 'Bajas' , False );
     end;

     Descripciones.Clear;
     with Descripciones do
     begin
          AddString( 'Mes', ObtieneElemento( lfMeses, zcboMes.Indice - 1));
          AddInteger( 'A�o' , txtAnio.Value );
          AddString( 'Archivo c�dula', ArchivoCedulaFonacot.Text );
          if ( varRazonSocialConciliar.Visible ) then
            AddString( 'Raz�n social', varRazonSocialConciliar.Llave );

          if cbBajas.Checked then
          begin
            AddString( 'Incluir bajas' , 'Si' );
            AddString( 'Bajas posteriores a', zdbFecha.ValorAsText );
          end
          else
            AddString( 'Incluir bajas' , 'No');
     end;
     inherited;

end;

function TWizConciliaCreditos.EjecutarWizard: Boolean;
begin
     TressShell.SetProgressbar( Self.ProgressBar);
     TressShell.ConciliarProceso( varRazonSocialConciliar.Llave, ArchivoCedulaFonacot.Text, txtAnio.Value, zcboMes.Indice, cbBajas.Checked, zdbFecha.Valor );
end;

end.
