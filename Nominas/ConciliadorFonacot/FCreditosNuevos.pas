unit FCreditosNuevos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseConsulta, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  ZBaseConsultaFonacot, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxContainer, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxLabel,
  ZetaKeyLookup_DevEx, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxCheckBox;

type
  TCreditosNuevos = class(TBaseConsultaFonacot)
    Posicion: TcxGridDBColumn;
    NFonacot: TcxGridDBColumn;
    NEmpleado: TcxGridDBColumn;
    NomCompleto: TcxGridDBColumn;
    StatusEmp: TcxGridDBColumn;
    RFC: TcxGridDBColumn;
    NCredito: TcxGridDBColumn;
    Reubicado: TcxGridDBColumn;
    MMensual: TcxGridDBColumn;
    Pagos: TcxGridDBColumn;
    MontoTotal: TcxGridDBColumn;
    Meses: TcxGridDBColumn;
    chkMostrarCreditoNuevo: TcxCheckBox;
    chkMostarCreditoConCuota: TcxCheckBox;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AplicarFiltros(Sender: TObject);
    procedure InicializarFiltros;
  private
    { Private declarations }

  protected
     procedure Connect; override;

  public
    { Public declarations }
  end;

var
  CreditosNuevos: TCreditosNuevos;

implementation

{$R *.dfm}

uses
    ZetaCommonClasses, DConcilia, ZetaCommonTools, DCliente;

procedure TCreditosNuevos.Connect;
begin
     inherited;
     DataSource.DataSet := dmConcilia.cdsCreditosNuevos;
end;

procedure TCreditosNuevos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_CREDITOS_NUEVOS;
     if ( dmCliente.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontraron cr�ditos nuevos'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';
     InicializarFiltros;
end;

procedure TCreditosNuevos.FormShow(Sender: TObject);
begin
    inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('Posicion'), K_SIN_TIPO , '', skCount);
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('MontoTotal'),K_TIPO_MONEDA , '', skSum);
     ZetaDBGridDBTableView.ApplyBestFit();
end;


procedure TCreditosNuevos.AplicarFiltros(Sender: TObject);
const
     K_Filtro_1 = 'Meses = 0';
     K_Filtro_2 = 'Meses > 0 ';
     K_Sin_Filt = 'Meses = -1';
var
   sFiltro:string;
begin
     sFiltro := K_Sin_Filt;
     if chkMostrarCreditoNuevo.Checked then
     begin
           sFiltro := ConcatString( sFiltro,K_Filtro_1 ,' OR ');
     end;
     if chkMostarCreditoConCuota.Checked then
     begin
          sFiltro := ConcatString( sFiltro,K_Filtro_2 ,' OR ');
     end;

     with dmConcilia.cdsCreditosNuevos do
     begin
          Filtered := FALSE;
          Filter := sFiltro;
          Filtered := StrLleno(sFiltro);
     end;

end;

procedure TCreditosNuevos.InicializarFiltros;
begin
     chkMostrarCreditoNuevo.OnClick(Self);
end;


end.
