unit FResumenEmpleadosConCredito;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseConsulta, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  ZBaseConsultaFonacot, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxContainer, ZetaDBTextBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxLabel, ZetaKeyLookup_DevEx, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TResumenEmpleadosConCredito = class(TBaseConsultaFonacot)
    NFonacot: TcxGridDBColumn;
    NEmpleado: TcxGridDBColumn;
    NomCompleto: TcxGridDBColumn;
    Status: TcxGridDBColumn;
    RFC: TcxGridDBColumn;
    CredTress: TcxGridDBColumn;
    CredCedulas: TcxGridDBColumn;
    PanelResumen: TPanel;
    Label5: TLabel;
    ztxtCredTress: TZetaTextBox;
    Label6: TLabel;
    ztxtCredCedula: TZetaTextBox;
    chkMasTress: TCheckBox;
    chkMasCedula: TCheckBox;
    chkIguales: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ChecarFiltros(Sender: TObject);
    procedure InicializarFiltros;
  private
    { Private declarations }
  protected
     procedure Connect; override;

  public
    { Public declarations }
  end;

var
  ResumenEmpleadosConCredito: TResumenEmpleadosConCredito;

implementation

{$R *.dfm}

uses
    ZetaCommonClasses, DConcilia, ZetaCommonTools, ZetaCommonLists,
    DCliente;


procedure TResumenEmpleadosConCredito.Connect;
begin
     inherited;
     DataSource.DataSet := dmConcilia.cdsEmpCreditos;
end;


procedure TResumenEmpleadosConCredito.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_RESUMEN_EMPLEADOS;
     if ( dmCliente.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontraron diferencias'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';
     InicializarFiltros;
end;

procedure TResumenEmpleadosConCredito.FormShow(Sender: TObject);
begin
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('NFonacot'), K_SIN_TIPO , '', skCount );
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CredTress'), K_SIN_TIPO , '', skSum );
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CredCedulas'), K_SIN_TIPO , '', skSum );
     ZetaDBGridDBTableView.ApplyBestFit();
     if ( dmCliente.ConciliacionTerminada ) then
     begin
          ztxtCredTress.Caption  := IntToStr( dmConcilia.TotalCredTress );
          ztxtCredCedula.Caption  := IntToStr( dmConcilia.TotalCredCed );
     end;
end;



procedure TResumenEmpleadosConCredito.ChecarFiltros(Sender: TObject);
const
     K_Filtro_1 = 'CredTress > CredCedulas';
     K_Filtro_2 = 'CredTress < CredCedulas';
     K_Filtro_3 = 'CredTress = CredCedulas';
     K_Sin_Filt = 'CredTress = -1';
var
   sFiltro:string;
begin
     sFiltro := K_Sin_Filt;
     if chkMasTress.Checked then
     begin
          sFiltro := ConcatString( sFiltro,K_Filtro_1 ,' OR ');
     end;
     if chkMasCedula .Checked then
     begin
          sFiltro := ConcatString( sFiltro,K_Filtro_2 ,' OR ');
     end;
     if chkIguales.Checked then
     begin
          sFiltro := ConcatString( sFiltro,K_Filtro_3 ,' OR ');
     end;

     with dmConcilia.cdsEmpCreditos do
     begin
          Filtered := FALSE;
          Filter := sFiltro;
          Filtered := StrLleno(sFiltro);
     end;

end;

procedure TResumenEmpleadosConCredito.InicializarFiltros;//Mejoras de FONACOT - Default Filtros
begin
     chkMasTress.OnClick(Self);
end;

end.
