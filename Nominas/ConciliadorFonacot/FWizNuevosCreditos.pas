unit FWizNuevosCreditos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.Menus,
  cxCheckBox, Vcl.StdCtrls, cxButtons, cxTextEdit, Vcl.Mask, ZetaFecha,
  DBClient;

type
  TWizNuevosCreditos = class(TcxBaseWizard)
    zFechaInicial: TZetaFecha;
    Label9: TcxLabel;
    GroupBox1: TcxGroupBox;
    Label3: TcxLabel;
    txtPathImportPresta: TcxTextEdit;
    BuscaBitacoraPrestamos: TcxButton;
    DialogoBitacora: TOpenDialog;
    ckConsideraCuotas: TcxCheckBox;
    procedure BuscaBitacoraPrestamosClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
      DataSet: TClientDataSet;
  protected
      function EjecutarWizard: Boolean;override;
      procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
   WizNuevosCreditos: TWizNuevosCreditos;
   function ShowWizard( aDataSet: TClientDataSet): Boolean;overload;

implementation

{$R *.dfm}


uses
    ZetaClientTools, DConcilia, ZetaCommonTools, ZetaDialogo,
    FWizNuevosCreditosGridSelect, ZBasicoSelectGrid_DevEx,
    ZetaClientDataSet, ZetaCommonClasses;

function TWizNuevosCreditos.EjecutarWizard: Boolean;
begin
     with dmConcilia do
     begin
          LogInit( txtPathImportPresta.Text ,'Inicio Importar Pr�stamo' );
          ImportarNuevosCreditos(zFechaInicial.Valor, ckConsideraCuotas.Checked );
          LogEnd;
          //graba datos en registry
          try
             SetPathBitacora( K_CREDITOS_NUEVOS, txtPathImportPresta.Text );
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zInformation( '', 'No cuenta con los derechos necesarios en los registros locales (Registry) '
                                                + ' para configurar la aplicaci�n en su computadora, contacte al Administrador del Sistema.', 0 );
                end;
          end;
     end;
end;

procedure TWizNuevosCreditos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_IMPORTAR_NUEVOS_CREDITOS;
     ZFechaInicial.Valor := Date;
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizNuevosCreditos.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Al aplicar el proceso se importar�n los nuevos cr�ditos Fonacot al expediente de pr�stamos de los empleados.';
     TClientDataSet( DataSet ).LogChanges := True;
     with dmConcilia, Application do
     begin
           txtPathImportPresta.Text:= GetPathBitacora( K_CREDITOS_NUEVOS );
     end;
end;

procedure TWizNuevosCreditos.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if ( lok ) then
       TClientDataSet(DataSet).CancelUpdates;
end;

procedure TWizNuevosCreditos.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                 if ( StrVacio( txtPathImportPresta.Text) ) then
                 begin
                      ZetaDialogo.ZError( 'Error al importar cr�ditos', 'El directorio de la bit�cora no puede quedar vac�o', 0 );
                      CanMove := False;
                 end
                 else if ( DataSet.IsEmpty ) then
                 begin
                      ZetaDialogo.ZError( 'Error al importar cr�ditos', 'Lista de nuevos cr�ditos est� vac�a', 0 );
                      CanMove := False;
                 end
                 else
                 begin
                      //si hay registros a procesar, abrir el grid de empleados para filtrar
                       CanMove := ZBasicoSelectGrid_DevEx.GridSelectBasico(
                                  TZetaClientDataSet( DataSet ), TWizNuevosCreditosGridSelect, FALSE );
                        //si no hay registros a procesar, despues de filtrarlos por el grid de empleados, no avanzar
                       if ( DataSet.IsEmpty ) then
                       begin
                            ZetaDialogo.ZError( 'Error al importar cr�ditos', 'Lista de nuevos cr�ditos est� vac�a', 0 );
                            CanMove := False;
                            //cancelar los cambios realizados por el grid de empleados, cuando se eliminaron TODOS los registros a procesar
                            TClientDataSet( DataSet).CancelUpdates;
                       end;
                 end;

            end;
     end;
end;

procedure TWizNuevosCreditos.BuscaBitacoraPrestamosClick(Sender: TObject);
begin
     inherited;
     txtPathImportPresta.Text := ZetaClientTools.AbreDialogo( DialogoBitacora , txtPathImportPresta.Text, 'log' );
end;

procedure TWizNuevosCreditos.CargaParametros;
begin
     with Descripciones do
     begin
          AddDate( 'Fecha inicial' , zFechaInicial.Valor );
          AddString( 'Considerar cuotas pagadas',  BoolToSiNo( ckConsideraCuotas.Checked ) );
          AddString( 'Guardar bit�cora en', txtPathImportPresta.Text );
          if ( DataSet <> nil ) and ( DataSet.Active ) and ( Wizard.EsPaginaActual( Ejecucion )) then
             AddInteger ( 'Cantidad de cr�ditos', DataSet.RecordCount);
     end;
     inherited;

end;


 //Parametro: aDataset
function ShowWizard( aDataSet: TClientDataSet): Boolean;
begin
     with TWizNuevosCreditos.Create( Application ) do
     begin
          try
             DataSet := aDataSet;
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;


end.
