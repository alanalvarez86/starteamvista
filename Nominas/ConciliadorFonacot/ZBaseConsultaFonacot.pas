unit ZBaseConsultaFonacot;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls, ZBaseConsulta,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus, System.Actions,
  Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  cxContainer, cxLabel, ZetaKeyLookup_DevEx;

type
  TBaseConsultaFonacot = class(TBaseGridLectura_DevEx)
    PanelFiltros: TPanel;
    EmpleadoConcilia: TZetaKeyLookup_DevEx;
    EmpleadoLbl: TcxLabel;
    ActionList1: TActionList;
    BuscarEmpleado: TAction;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EmpleadoConciliaValidKey(Sender: TObject);
    procedure BuscarEmpleadoExecute(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
      procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);

  private
    { Private declarations }

  protected
    { Protected declarations }
     function PuedeAgregar( var sMensaje: String ): Boolean;override;
     function PuedeBorrar( var sMensaje: String ): Boolean;override;
     function PuedeModificar( var sMensaje: String ): Boolean;override;
     function PuedeImprimir( var sMensaje: String ): Boolean;override;
     procedure Refresh;override;
     function PosicionaEmpleado( const iEmpleado: Integer ) : Boolean; virtual;
     procedure Connect; override;
  public
    { Public declarations }
  end;

const
     K_SIN_TIPO = K_SIN_TIPO;
     K_TIPO_MONEDA = K_TIPO_MONEDA;
var
  BaseConsultaFonacot: TBaseConsultaFonacot;

implementation

{$R *.dfm}

uses
    ZetaCommonLists,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaDialogo,
    DCliente,
    ZetaBuscaEmpleado_DevEx, FTressShell;



procedure TBaseConsultaFonacot.BuscarEmpleadoExecute(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     Self := TBaseConsultaFonacot(TressShell.FormaActiva);
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          EmpleadoConcilia.Llave := sKey;
          EmpleadoConciliaValidKey(EmpleadoConcilia);
     end;
end;

procedure TBaseConsultaFonacot.EmpleadoConciliaValidKey(Sender: TObject);
begin
     inherited;
     with TZetaKeyLookup_DevEx(Sender) do
     begin
          if ( Valor = 0 ) or  not ( PosicionaEmpleado( Valor )  ) then
          begin
               Llave:= VACIO;
          end;
     end;
end;

procedure TBaseConsultaFonacot.FormCreate(Sender: TObject);
begin

      TipoValorActivo1 := stCedula;
      TipoValorActivo2 := stArchivo;
      inherited;
      InitValoresActivos;
      EmpleadoConcilia.LookupDataSet:= dmCliente.cdsEmpleadoLookup;
      ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
end;

procedure TBaseConsultaFonacot.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     inherited;
     case Key of
          VK_RETURN : ZetaDBGrid.SetFocus;
     end;
end;

procedure TBaseConsultaFonacot.FormShow(Sender: TObject);
begin
     inherited;
     ApplyMinWidth;
     PanelIdentifica.Visible := True;
     ValorActivo1.Visible := dmCliente.ConciliacionTerminada;
     ValorActivo2.Visible := dmCliente.ConciliacionTerminada;
     Slider.Visible := True;

     if ( StrVacio( textoValorActivo2.Caption ) ) then
     begin
          ValorActivo2.Visible := False;
          ValorActivo1.Align := alClient;
     end;
     textoValorActivo1.Hint := textoValorActivo1.Caption;
     textoValorActivo2.Hint := 'Raz�n Social: ' + dmCliente.RazonSocialUltimaConciliacion + ': ' + dmCliente.cdsRSocial.GetDescripcion(dmCliente.CedulaUltimaConciliacion);

end;


function TBaseConsultaFonacot.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar en el conciliador Fonacot' ;
end;

function TBaseConsultaFonacot.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar en el conciliador Fonacot' ;
end;

function TBaseConsultaFonacot.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar en el conciliador Fonacot' ;
end;

function TBaseConsultaFonacot.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TBaseConsultaFonacot.Refresh;
begin

end;

procedure TBaseConsultaFonacot.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     inherited;
     if AViewInfo.GridRecord.Selected then
        ACanvas.Brush.Color := clInfoBk;
end;

function TBaseConsultaFonacot.PosicionaEmpleado( const iEmpleado: Integer ): Boolean;
begin
     if ( DataSource.DataSet <> nil ) and ( DataSource.DataSet.Active ) then
     begin
          Result:= ( DataSource.DataSet.Locate('NEmpleado', iEmpleado , [] ) );
          if not Result then
          begin
               zWarning( '� Atenci�n !', '� Empleado no encontrado !', 0, mbOK );
               EmpleadoConcilia.SetFocus;
          end;
     end;
end;


procedure TBaseConsultaFonacot.Connect;
begin
     inherited;
     dmCliente.SetValorRutaCedula( K_TIPO_CONCILIACION );
     dmCliente.SetValorRazonSocial( K_TIPO_CONCILIACION );

end;

end.
