unit ZetaDespConsulta;

interface

uses
    ZBaseConsulta, ZAccesosTress;

type
  eFormaConsulta = ( efcNinguna
                     , efcResumenEmpleadosConCredito
                     , efcNuevosEmpleadosFonacot
                     , efcCreditosNuevos
                     , efcCreditosDiferencias
                     , efcCreditosActivosNoCedula
                     , efcCreditosSaldadosActivos
                     , efcErroresCedula
                     , efcCedulaPago
                     );
    TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;


  function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;


implementation

uses
    FResumenEmpleadosConCredito,
    FNuevosEmpleadosFonacot,
    FCreditosNuevos,
    FCreditosDiferencias,
    FCreditosActivosNoCedula,
    FCreditosSaldadosActivos,
    FErroresCedula,
    FCedulaPago;


function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin

     case Forma of
          efcResumenEmpleadosConCredito: Result := Consulta( D_EMPLEADOS, TResumenEmpleadosConCredito );
          efcNuevosEmpleadosFonacot: Result := Consulta( D_EMPLEADOS, TNuevosEmpleadosFonacot );
          efcCreditosNuevos: Result := Consulta( D_EMPLEADOS, TCreditosNuevos );
          efcCreditosDiferencias: Result := Consulta( D_EMPLEADOS, TCreditosDiferencias );
          efcCreditosActivosNoCedula: Result := Consulta( D_EMPLEADOS, TCreditosActivosNoCedula );
          efcCreditosSaldadosActivos: Result := Consulta( D_EMPLEADOS, TCreditosSaldadosActivos );
          efcErroresCedula: Result := Consulta( D_EMPLEADOS, TErroresCedula );
          efcCedulaPago: Result := Consulta( D_NOMINA, TCedulaPago );

          else
              Result := Consulta( 0, nil );
     end;
end;

end.
