inherited dmCliente: TdmCliente
  object cdsAscii: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 208
    Top = 72
    object cdsAsciiRenglon: TStringField
      FieldName = 'Renglon'
      Size = 2048
    end
    object cdsAsciiDIFERENCIA: TCurrencyField
      FieldName = 'DIFERENCIA'
    end
  end
  object cdsRSocial: TZetaLookupDataSet
    Tag = 49
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsRSocialAlAdquirirDatos
    LookupName = 'Cat'#225'logo de razones sociales'
    LookupDescriptionField = 'RS_NOMBRE'
    LookupKeyField = 'RS_CODIGO'
    OnGetRights = cdsRSocialGetRights
    Left = 288
    Top = 72
  end
end
