unit FCreditosDiferencias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseConsultaFonacot, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls, ZBaseConsulta,ZetaCommonClasses,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxContainer,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxLabel, ZetaKeyLookup_DevEx,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxButtons, Vcl.Mask, ZetaNumero, TressMorado2013;

type
  TCreditosDiferencias = class(TBaseConsultaFonacot)
    Posicion: TcxGridDBColumn;
    NFonacot: TcxGridDBColumn;
    NEmpleado: TcxGridDBColumn;
    NomCompleto: TcxGridDBColumn;
    StatusEmp: TcxGridDBColumn;
    NCredito: TcxGridDBColumn;
    CRetencion: TcxGridDBColumn;
    CPlazo: TcxGridDBColumn;
    TressRetencion: TcxGridDBColumn;
    TressPlazo: TcxGridDBColumn;
    chkPlazoAumenta: TCheckBox;
    chkPlazoDisminuye: TCheckBox;
    chkCambioRetencion: TCheckBox;
    Label11: TLabel;
    nMontos: TZetaNumero;
    BtnFiltro1: TcxButton;
    DiferenciaCredito: TcxGridDBColumn;
    DiferenciaPlazo: TcxGridDBColumn;
    Reubicado: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ChecarFiltros(Sender: TObject);
    procedure InicializarFiltros;
    procedure SetFiltrosDiferencias;
    procedure CPlazoCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure TressPlazoCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure TressRetencionCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure CRetencionCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure CambiarPropiedadesTextoGrid(var ACanvas: TcxCanvas);
    procedure ObtenerValoresComparar(var iTressPlazo, iCPlazo: Integer; var dTressRetencion, dCRetencion: TPesos; var  AViewInfo: TcxGridTableDataCellViewInfo);
    procedure nMontosExit(Sender: TObject);
    procedure BtnFiltro1Click(Sender: TObject);
    procedure ZetaDBGridDBTableViewFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    { Private declarations }
    procedure EnfocarEmpleado;
  protected
     procedure Connect; override;

  public
    { Public declarations }
  end;

var
  CreditosDiferencias: TCreditosDiferencias;

implementation

{$R *.dfm}

uses
    DConcilia, DCliente, ZetaCommonTools;



procedure TCreditosDiferencias.Connect;
begin
     inherited;
     DataSource.DataSet := dmConcilia.cdsCreditosDiferencias;
end;

procedure TCreditosDiferencias.CPlazoCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
          AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   iTressPlazo: Integer;
   iCPlazo: Integer;
   dTressRetencion: TPesos;
   dCRetencion: TPesos;
begin
     ObtenerValoresComparar(iTressPlazo, iCPlazo, dTressRetencion, dCRetencion, AViewInfo);
     if iCPlazo <> iTressPlazo then
        CambiarPropiedadesTextoGrid( ACanvas );
end;

procedure TCreditosDiferencias.CRetencionCustomDrawCell( Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
          AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   iTressPlazo: Integer;
   iCPlazo: Integer;
   dTressRetencion: TPesos;
   dCRetencion: TPesos;
begin
     ObtenerValoresComparar(iTressPlazo, iCPlazo, dTressRetencion, dCRetencion, AViewInfo);
     if dCRetencion <> dTressRetencion then
        CambiarPropiedadesTextoGrid( ACanvas );
end;

procedure TCreditosDiferencias.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_CREDITOS_DIFERENCIAS;
     if ( dmCliente.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n cr�dito con diferencias'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';
     InicializarFiltros;
end;

procedure TCreditosDiferencias.FormShow(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.ApplyBestFit();
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('Posicion'), K_SIN_TIPO , '', skCount );
     EnfocarEmpleado;
end;

procedure TCreditosDiferencias.ChecarFiltros(Sender: TObject);
const
     K_Filtro_1 = 'CPlazo > TressPlazo';
     K_Filtro_2 = 'TressPlazo > CPlazo';
     K_Filtro_3 = 'CRetencion <> TressRetencion';
     K_Sin_Filt = 'Posicion = -1';

     procedure HabilitarDiferencias( lHabilitar: Boolean );
     begin
          Label11.Enabled := lHabilitar;
          nMontos.Enabled := lHabilitar;
          BtnFiltro1.Enabled := lHabilitar;
     end;

     function GetDiferencia( sFiltroDiferencia: String ): String;
     begin
          if StrLleno( sFiltroDiferencia ) then
          sFiltroDiferencia := Parentesis( sFiltroDiferencia );
          sFiltroDiferencia := ConcatFiltros( sFiltroDiferencia, Parentesis( 'DIFERENCIA>' +  FloatToStr(nMontos.Valor) ) );
          Result := sFiltroDiferencia;
     end;
var
   sFiltro, sFiltroDiferencia:string;
begin
     sFiltro := K_Sin_Filt;
     if chkPlazoAumenta.Checked then
     begin
          sFiltro := ConcatString( sFiltro,K_Filtro_1 ,' OR ');
     end;
     if chkPlazoDisminuye.Checked then
     begin
          sFiltro := ConcatString( sFiltro,K_Filtro_2 ,' OR ');
     end;
     HabilitarDiferencias( chkCambioRetencion.Checked );
     if chkCambioRetencion.Checked then
     begin
          sFiltroDiferencia := Parentesis( GetDiferencia( K_Filtro_3 ) );
          sFiltro := ConcatString( sFiltro, sFiltroDiferencia,' OR ');
     end;
     SetFiltrosDiferencias;
     with dmConcilia.cdsCreditosDiferencias do
     begin
          Filtered := FALSE;
          Filter := sFiltro;
          Filtered := StrLleno(sFiltro);
     end;
end;

procedure TCreditosDiferencias.InicializarFiltros;//Mejoras de FONACOT - Default Filtros
begin
     chkPlazoAumenta.OnClick(Self);
     SetFiltrosDiferencias;
end;

procedure TCreditosDiferencias.nMontosExit(Sender: TObject);
begin
     inherited;
     ChecarFiltros( Self );
end;

procedure TCreditosDiferencias.SetFiltrosDiferencias;//Mejoras de FONACOT - Default Valor Activc Filtros
begin
     dmCliente.FiltroPlazoAumentaConciliacion    := chkPlazoAumenta.Checked;
     dmCliente.FiltroPlazoDisminuyeConciliacion  := chkPlazoDisminuye.Checked;
     dmCliente.FiltroCambioRetencionConciliacion := chkCambioRetencion.Checked;
     dmCliente.FiltroDiferenciaMontosConciliacion := nMontos.Valor;
end;

procedure TCreditosDiferencias.TressPlazoCustomDrawCell( Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
          AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   iTressPlazo: Integer;
   iCPlazo: Integer;
   dTressRetencion: TPesos;
   dCRetencion: TPesos;
begin
     ObtenerValoresComparar(iTressPlazo, iCPlazo, dTressRetencion, dCRetencion, AViewInfo);
     if iCPlazo <> iTressPlazo then
        CambiarPropiedadesTextoGrid( ACanvas );
end;

procedure TCreditosDiferencias.TressRetencionCustomDrawCell( Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
          AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   iTressPlazo: Integer;
   iCPlazo: Integer;
   dTressRetencion: TPesos;
   dCRetencion: TPesos;
begin
     ObtenerValoresComparar(iTressPlazo, iCPlazo, dTressRetencion, dCRetencion, AViewInfo);
     if dCRetencion <> dTressRetencion  then
        CambiarPropiedadesTextoGrid( ACanvas );
end;

procedure TCreditosDiferencias.ZetaDBGridDBTableViewFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
  EnfocarEmpleado;
end;

procedure TCreditosDiferencias.BtnFiltro1Click(Sender: TObject);
begin
     inherited;
     //with GridActivo do
     with ZetaDBGridDBTableView do
     begin
          if Visible and Enabled then
             SetFocus;
     end;
end;

procedure TCreditosDiferencias.CambiarPropiedadesTextoGrid(var ACanvas: TcxCanvas);
begin
     with ACanvas do
     begin
          Font.Style := [fsBold];
          Font.Color := clRed;
     end;
end;

procedure TCreditosDiferencias.ObtenerValoresComparar(var iTressPlazo, iCPlazo: Integer; var dTressRetencion, dCRetencion: TPesos;
          var AViewInfo: TcxGridTableDataCellViewInfo);
begin
     iTressPlazo := AViewInfo.GridRecord.Values[TressPlazo.Index];
     iCPlazo := AViewInfo.GridRecord.Values[CPlazo.Index];
     dTressRetencion := AViewInfo.GridRecord.Values[TressRetencion.Index];
     dCRetencion := AViewInfo.GridRecord.Values[CRetencion.Index];
end;

procedure TCreditosDiferencias.EnfocarEmpleado;
begin
    if ZetaDBGridDBTableView.DataController.FocusedRecordIndex > -1 then
        dmCliente.SetEmpleadoNumero(ZetaDBGridDBTableView.DataController.Values[ZetaDBGridDBTableView.DataController.FocusedRecordIndex, 2 ])
    else
        begin
            if ( dmConcilia.cdsCreditosDiferencias.State <> dsInactive ) and
               ( dmConcilia.cdsCreditosDiferencias.RecordCount > 0 ) then
             begin
                 with dmConcilia.cdsCreditosDiferencias do
                 begin
                      First;
                      dmCliente.SetEmpleadoNumero( FieldByName('NEmpleado').AsInteger);
                 end;
             end;
        end;
end;

end.
