unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerConsultas,
     DServerGlobal,
     DServerRecursos,
     {$else}
     Catalogos_TLB,
     Consultas_TLB,
     Global_TLB,
     Recursos_TLB,
     {$endif}
     DBaseCliente,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaClientDataSet;

type
  TdmCliente = class( TBaseCliente )
    cdsAscii: TZetaClientDataSet;
    cdsAsciiRenglon: TStringField;
    cdsRSocial: TZetaLookupDataSet;
    cdsAsciiDIFERENCIA: TCurrencyField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsRSocialAlAdquirirDatos(Sender: TObject);
    procedure cdsRSocialGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
  private
    { Private declarations }
    FDatosPeriodo: TDatosPeriodo;
    FEmpleado:Integer;
    {$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerGlobal: TdmServerGlobal;
    FServerRecursos:TdmServerRecursos;
    {$else}
    FServerCatalogos: IdmServerCatalogosDisp;
    FServerConsultas: IdmServerConsultasDisp;
    FServerGlobal: IdmServerGlobalDisp;
    FserverRecursos:IdmServerRecursosDisp;

    FConfrontaTerminada : Boolean;
    FConciliacionTerminada : Boolean;
    FCancelaConciliacion : Boolean;
    FMesConfronta : Integer;

    FValorRutaCedula: String;
    FValorRazonSocial: String;

    FRazonSocialUltimaConciliacion : String;
    FRazonSocialUltimaConfronta : String;
    FCedulaUltimaConciliacion : String;
    FCedulaUltimaConfronta : String;
    FCedulaAnioConciliacion: Integer;//Mejoras Fonnacot - Valor Activo Anio de Conciliacion
    FCedulaMesConciliacion: Integer;//Mejoras Fonnacot - Valor Activo Mes de Conciliacion
    FFiltroPlazoAumentaConciliacion : Boolean;//Mejoras Fonnacot - Valor Activo Filtros Creditos diferencias
    FFiltroPlazoDisminuyeConciliacion : Boolean;//Mejoras Fonnacot - Valor Activo Filtros Creditos diferencias
    FFiltroCambioRetencionConciliacion : Boolean;//Mejoras Fonnacot - Valor Activo Filtros Creditos diferencias
    FFiltroDiferenciaMontosConciliacion : Currency;//Mejoras Fonnacot - Valor Activo Filtros Creditos diferencias

    function GetServerCatalogos: IdmServerCatalogosDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerGlobal: IdmServerGlobalDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    {$endif}
    function ImportarCedulasFonacotGetASCII( ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant;
    function GetLista( ZetaDataset: TZetaClientDataset ): OleVariant;


    function GetIMSSPatron: String;
    function GetIMSSYear: Integer;
    function GetIMSSMes: Integer;
    function GetIMSSTipo: eTipoLiqIMSS;
  protected
    { Protected declarations }
    function GetValorActivoStr( const eTipo: TipoEstado ): String;override;

  public
    { Public declarations }
    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;

    {$ifdef DOS_CAPAS}
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerRecursos: TdmServerRecursos read FServerRecursos;

    {$else}
    property ServerCatalogos: IdmServerCatalogosDisp read GetServerCatalogos;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerGlobal: IdmServerGlobalDisp read GetServerGlobal;
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    {$endif}

    property GetDatosPeriodoActivo: TDatosPeriodo read FDatosPeriodo;
    property Empleado :Integer read FEmpleado;

    property ConfrontaTerminada: Boolean read FConfrontaTerminada write FConfrontaTerminada;
    property ConciliacionTerminada: Boolean read FConciliacionTerminada write FConciliacionTerminada;
    property CancelaConciliacion: Boolean read FCancelaConciliacion write FCancelaConciliacion;
    property MesConfronta: Integer read FMesConfronta write FMesConfronta;

    property ValorRutaCedula: String read FValorRutaCedula;
    property ValorRazonSocial: String read FValorRazonSocial;

    property RazonSocialUltimaConciliacion: String read FRazonSocialUltimaConciliacion write FRazonSocialUltimaConciliacion;
    property RazonSocialUltimaConfronta: String read FRazonSocialUltimaConfronta write FRazonSocialUltimaConfronta;
    property CedulaUltimaConciliacion: String read FCedulaUltimaConciliacion write FCedulaUltimaConciliacion;
    property CedulaUltimaConfronta: String read FCedulaUltimaConfronta write FCedulaUltimaConfronta;
    property CedulaAnioConciliacion: Integer read FCedulaAnioConciliacion write FCedulaAnioConciliacion;
    property CedulaMesConciliacion: Integer read FCedulaMesConciliacion write FCedulaMesConciliacion;
    property FiltroPlazoAumentaConciliacion: Boolean read FFiltroPlazoAumentaConciliacion write FFiltroPlazoAumentaConciliacion;
    property FiltroPlazoDisminuyeConciliacion: Boolean read FFiltroPlazoDisminuyeConciliacion write FFiltroPlazoDisminuyeConciliacion;
    property FiltroCambioRetencionConciliacion: Boolean read FFiltroCambioRetencionConciliacion write FFiltroCambioRetencionConciliacion;
    property FiltroDiferenciaMontosConciliacion: Currency read FFiltroDiferenciaMontosConciliacion write FFiltroDiferenciaMontosConciliacion;

    function GetSQLData( const sSQL: String ): OleVariant;
    function CargaCedulaFonacot( sPath:string ):Olevariant;
    function GetPrestamosImportar:OleVariant;
    function ChecaAccesoImportarKardex: Boolean;
    function ChecaAccesoImportarPrestamo: Boolean;

    procedure SetValorRutaCedula( TipoConciliacion: Integer);
    procedure SetValorRazonSocial( TipoConciliacion: Integer);
    procedure CargaActivosIMSS(Parametros: TZetaParams);
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema(Parametros: TZetaParams);

    procedure SetEmpleadoNumero( const Value: TNumEmp );
  end;

const
     K_TIPO_CONCILIACION = 0;
     K_TIPO_CONFRONTA = 1;

var
  dmCliente: TdmCliente;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZAsciiTools,
     ZAccesosTress,ZAccesosMgr,
     DSuperASCII,
     ZetaRegistryCliente;

{$R *.DFM}



{ ********* TdmCliente ********** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( self );
     FServerRecursos := TdmServerRecursos.Create( self );
     {$endif}
     inherited;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerRecursos );
     {$endif}
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( CreaServidor( CLASS_dmServerCatalogos, FServerCatalogos ) );
end;

function TdmCliente.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;

function TdmCliente.GetServerGlobal: IdmServerGlobalDisp;
begin
     Result := IdmServerGlobalDisp( CreaServidor( CLASS_dmServerGlobal, FServerGlobal ) );
end;
function TdmCliente.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result := IdmServerRecursosDisp( CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;
{$endif}

{ ******** Llamadas a Servidores ********** }

function TdmCliente.GetSQLData(const sSQL: String): OleVariant;
begin
     Result := ServerConsultas.GetQueryGralTodos( Empresa, sSQL );
end;

function TdmCliente.CargaCedulaFonacot( sPath:string ):Olevariant;
var
   iErrorCount :Integer;
begin
     FillcdsASCII( cdsASCII, sPath );
     Result := cdsAscii.Data;
     with cdsASCII do
     begin
          if ( RecordCount > 0 )then
          begin
               Active := True;
               //remover el renglon de los encabezados
               First;
               Delete;
               MergeChangeLog;
               Result := ImportarCedulasFonacotGetASCII( GetLista( cdsASCII ), iErrorCount );
               Active := False;
          end;
     end;
end;

function TdmCliente.ImportarCedulasFonacotGetASCII( ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant;
var
   oSuperASCII:TdmSuperASCII;
begin

     oSuperASCII := TdmSuperASCII.Create( Self );
     try
        with oSuperASCII do
        begin
             Formato := faASCIIDel;
             FormatoImpFecha := ifDDMMYYs;
             UsaCommaText := False;
             AgregaColumna( 'N_FONACOT', tgTexto,K_ANCHO_DESCRIPCION , True );
             AgregaColumna( 'RFC', tgTexto,13 );
             AgregaColumna( 'NOM_COMPLETO', tgTexto,90);
             AgregaColumna( 'N_CREDITO', tgTexto, K_ANCHO_REFERENCIA,True );
             AgregaColumna( 'RET_MENS', tgFloat,K_ANCHO_PESOS ,True );
             AgregaColumna( 'N_EMPLEADO', tgTexto, K_ANCHO_PESOS );
             AgregaColumna( 'PLAZO', tgNumero, 3 ,True);
             AgregaColumna( 'MESES', tgNumero, 3 ,True);
             AgregaColumna( 'RET_REAL', tgFloat,K_ANCHO_PESOS ,True );
             AgregaColumna( 'INCIDENCIA', tgTexto,1 );
             AgregaColumna( 'FECHA_INI',tgTexto,10 );
             AgregaColumna( 'FECHA_FIN',tgTexto,10 );
             AgregaColumna( 'REUBICADO', tgTexto, 1 );
             AgregaColumna( 'DIFERENCIA', tgFloat, K_ANCHO_PESOS, False );
             Result := Procesa( ListaASCII );
             ErrorCount := Errores;
        end;
     finally
            oSuperASCII.Free;
     end;

end;

function TdmCliente.GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
begin
     with ZetaDataset do
     begin
          MergeChangeLog;  // Elimina Registros Borrados //
          Result := Data;
     end;
end;

function TdmCliente.GetPrestamosImportar: OleVariant;
var
   oSuperASCII:TdmSuperASCII;
begin
     oSuperASCII := TdmSuperASCII.Create( Self ); 
     try
        with oSuperASCII do
        begin
             Formato := faASCIIDel;
             FormatoImpFecha := ifDDMMYYs;
             AgregaColumna( 'CB_CODIGO', tgNumero, K_ANCHO_NUMEROEMPLEADO, True );
             AgregaColumna( 'PR_TIPO', tgTexto, K_ANCHO_CODIGO1, True);
             AgregaColumna( 'PR_REFEREN', tgTexto, K_ANCHO_REFERENCIA, True);
             AgregaColumna( 'PR_FECHA', tgFecha, K_ANCHO_FECHA, True);
             AgregaColumna( 'PR_MONTO', tgFloat, K_ANCHO_PESOS, True);
             AgregaColumna( 'PR_SALDO_I', tgFloat, K_ANCHO_PESOS);
             AgregaColumna( 'PR_FORMULA', tgTexto, K_ANCHO_FORMULA );
             AgregaColumna( 'PR_SUB_CTA', tgTexto, K_ANCHO_DESCRIPCION );
             AgregaColumna( 'PR_MONTO_S', tgFloat, K_ANCHO_PESOS);
             AgregaColumna( 'PR_TASA', tgFloat, K_ANCHO_TASA );
             AgregaColumna( 'PR_MESES', tgNumero, K_ANCHO_STATUS );
             AgregaColumna( 'PR_INTERES', tgFloat, K_ANCHO_PESOS);
             AgregaColumna( 'PR_PAGOS', tgNumero, K_ANCHO_STATUS );
             AgregaColumna( 'PR_PAG_PER', tgFloat, K_ANCHO_PESOS);
             Result := cdsResultado.Data;
        end;
     finally
            oSuperASCII.Free;
     end;
end;

function TdmCliente.ChecaAccesoImportarKardex :Boolean ;
begin
     Result := Revisa(D_EMP_PROC_IMPORTA_KARDEX);
end;

function TdmCliente.ChecaAccesoImportarPrestamo :Boolean ;
begin
     Result := Revisa( D_EMP_PROC_IMP_AHORROS_PRESTAMOS );
end;


procedure TdmCliente.cdsRSocialAlAdquirirDatos(Sender: TObject);
begin
  inherited;
  cdsRSocial.Data:= ServerCatalogos.GetRSocial(dmCliente.Empresa);
end;

procedure TdmCliente.cdsRSocialGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;


function TdmCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
      case eTipo of
          stCedula: Result := ValorRutaCedula;
          stArchivo: Result := ValorRazonSocial;
      end;
end;


procedure TdmCliente.SetValorRutaCedula( TipoConciliacion: Integer);
begin
     case TipoConciliacion of
          0: FValorRutaCedula  := 'C�dula: ' + CedulaUltimaConciliacion;
          1: FValorRutaCedula := 'C�dula: ' + CedulaUltimaConfronta;
      end;
end;


procedure TdmCliente.SetValorRazonSocial( TipoConciliacion: Integer);
begin
      case TipoConciliacion of
          0: if StrLleno ( RazonSocialUltimaConciliacion ) then
                FValorRazonSocial := 'Raz�n Social: ' + cdsRSocial.GetDescripcion( RazonSocialUltimaConciliacion )
             else
                 FValorRazonSocial := VACIO;

          1: if StrLleno ( RazonSocialUltimaConfronta ) then
                FValorRazonSocial := 'Raz�n Social: ' + cdsRSocial.GetDescripcion( RazonSocialUltimaConfronta )
             else
                 FValorRazonSocial := VACIO;
      end;
end;

procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', IMSSPatron );
          AddInteger( 'IMSSYear', IMSSYear );
          AddInteger( 'IMSSMes', IMSSMes );
          AddInteger( 'IMSSTipo', Ord( IMSSTipo ) );
     end;
end;


procedure TdmCliente.CargaActivosSistema(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddDate('FechaAsistencia', Date );
          AddDate('FechaDefault', FechaDefault );
          AddInteger('YearDefault', YearDefault );
          AddInteger( 'EmpleadoActivo', Empleado );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddInteger( 'Year', YearDefault );
          //AddInteger( 'Tipo', Ord( PeriodoTipo ) );
          //AddInteger( 'Numero', PeriodoNumero );
     end;
end;


function TdmCliente.GetIMSSPatron: String;
begin
     Result := VACIO;          // PENDIENTE : Se Requiere Consultar el Catalogo ???
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     //Result := TheYear( FFechaSupervisor );
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     //Result := TheMonth( FFechaSupervisor );
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := tlOrdinaria;
end;

procedure TdmCliente.SetEmpleadoNumero(const Value: TNumEmp);
begin
     FEmpleado := Value;
end;

end.



