unit FCreditosActivosNoCedula;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseConsultaFonacot, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, Vcl.StdCtrls, Vcl.ExtCtrls,
  cxContainer, cxLabel, ZetaKeyLookup_DevEx;

type
  TCreditosActivosNoCedula = class(TBaseConsultaFonacot)
    NEmpleado: TcxGridDBColumn;
    NomCompleto: TcxGridDBColumn;
    StatusEmp: TcxGridDBColumn;
    NFonacot: TcxGridDBColumn;
    NCredito: TcxGridDBColumn;
    MPrestado: TcxGridDBColumn;
    Saldo: TcxGridDBColumn;
    Observa: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  CreditosActivosNoCedula: TCreditosActivosNoCedula;

implementation

{$R *.dfm}


uses
    ZetaCommonClasses, DConcilia, DCliente;

procedure TCreditosActivosNoCedula.FormCreate(Sender: TObject);
begin
   inherited;
     HelpContext := H_CONCIL_FONACOT_CREDITOS_ACTIVOS_NO_CEDULA ;
     if ( dmCliente.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n cr�dito que no est� en la c�dula'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';
end;

procedure TCreditosActivosNoCedula.FormShow(Sender: TObject);
begin
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('NEmpleado'), K_SIN_TIPO , '', skCount);
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('MPrestado'), K_TIPO_MONEDA , 'Total prestado: ', skSum);
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('Saldo'), K_TIPO_MONEDA , 'Saldo total: ', skSum);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TCreditosActivosNoCedula.Connect;
begin
     inherited;
     DataSource.DataSet := dmConcilia.cdsCreditosActivos;
end;



end.
