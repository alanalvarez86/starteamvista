unit FWizTransferirEmpleadosGridSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, cxTextEdit, DBClient;

type
  TWizTransferirEmpleadosGridSelect = class(TBasicoGridSelect_DevEx)
    btnBuscarEmp_DevEx: TcxButton;

      procedure NEmpleadoCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure btnBuscarEmp_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewFocusedItemChanged(
      Sender: TcxCustomGridTableView; APrevFocusedItem,
      AFocusedItem: TcxCustomGridTableItem);
    procedure ExcluirClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WizTransferirEmpleadosGridSelect: TWizTransferirEmpleadosGridSelect;

implementation

{$R *.dfm}

uses
    ZetaBuscaEmpleado_DevEx, ZetaCommonClasses, DConcilia;

procedure TWizTransferirEmpleadosGridSelect.btnBuscarEmp_DevExClick(
  Sender: TObject);
var
   sKey,sNombre:string;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo(VACIO,sKey,sNombre)then
     begin
          with dmConcilia.cdsEmpleaFonacot do
          begin
               Edit;
               FieldByName('NEmpleado').AsInteger := StrToInt(sKey);
               FieldByName('NomTress').AsString := sNombre;
               Post;
          end;
     end;
end;

procedure TWizTransferirEmpleadosGridSelect.Cancelar_DevExClick(
  Sender: TObject);
begin
     inherited;
     TClientDataSet(DataSource.DataSet ).CancelUpdates;
end;

procedure TWizTransferirEmpleadosGridSelect.ExcluirClick(Sender: TObject);
begin
     inherited;
     if ZetaDBGridDBTableView.DataController.DataSource.DataSet.RecordCount = 0  then
        btnBuscarEmp_DevEx.Visible := False;

end;

procedure TWizTransferirEmpleadosGridSelect.NEmpleadoCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     inherited;
     if AViewInfo.GridRecord.Focused and AViewInfo.Item.Focused then
     begin
           with btnBuscarEmp_DevEx do
           begin
                Left := AViewInfo.RealBounds.Left + AViewInfo.RealBounds.Width - Width;
                Top := AViewInfo.RealBounds.Top + ZetaDBGrid.Top;
                Visible := True;
           end;
     end;
end;


procedure TWizTransferirEmpleadosGridSelect.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var iTermina: String;
begin
     inherited;
     iTermina := AViewInfo.GridRecord.DisplayTexts[6];

     if iTermina = 'Baja' then
        ACanvas.Font.Color := clRed;

end;

procedure TWizTransferirEmpleadosGridSelect.ZetaDBGridDBTableViewFocusedItemChanged(
  Sender: TcxCustomGridTableView; APrevFocusedItem,
  AFocusedItem: TcxCustomGridTableItem);
begin
     inherited;
     if ( AFocusedItem <> nil ) then
     begin
          if ( AFocusedItem.Name <> 'NEmpleado' ) and ( btnBuscarEmp_DevEx.Visible ) then
            btnBuscarEmp_DevEx.Visible:= False;
     end;
end;

end.
