program ConciliadorFonacot;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  DConcilia in 'DConcilia.pas' {dmConcilia: TDataModule},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DDiccionario in 'DDiccionario.pas',
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  DBaseCliente in '..\..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBasicoNavBarShell in '..\..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  ZetaDespConsulta in 'ZetaDespConsulta.pas',
  ZArbolTress in 'ZArbolTress.pas',
  ZcxWizardBasico in '..\..\Tools\ZcxWizardBasico.pas' {CXWizardBasico},
  ZBaseConsulta in '..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZcxBaseWizard in '..\..\Tools\ZcxBaseWizard.pas' {cxBaseWizard},
  ZBaseGridLectura_DevEx in '..\..\Tools\ZBaseGridLectura_DevEx.pas' {BaseGridLectura_DevEx},
  ZBaseConsultaFonacot in 'ZBaseConsultaFonacot.pas' {BaseConsultaFonacot},
  ZBaseDlgModal_DevEx in '..\..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  ZBasicoSelectGrid_DevEx in '..\..\Tools\ZBasicoSelectGrid_DevEx.pas' {BasicoGridSelect_DevEx},
  DCatalogos in 'DCatalogos.pas' {dmCatalogos: TDataModule};

{$R *.res}
{$R WindowsXP.res}
{$R ..\..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;

  Application.Title := 'Conciliador Fonacot';
  Application.HelpFile := 'Conciliador FONACOT.chm';
  Application.CreateForm(TTressShell, TressShell);
  Application.CreateForm(TdmCatalogos, dmCatalogos);
  with TressShell do
  begin
       if Login( True ) then
       begin
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end;
       Free;
  end;
end.

