unit FNomEditDatosAsist_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, StdCtrls, Mask, ComCtrls,
     ZBaseAsisEdicion_DevEx,
     ZetaKeyCombo, ZetaKeyLookup_DevEx, ZetaNumero, ZetaDBTextBox,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
     cxControls, dxSkinsdxBarPainter, dxBarExtItems,
     dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
     dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     cxContainer, cxEdit, cxGroupBox;

type
  TNomEditDatosAsist_DevEx = class(TBaseAsisEdicion_DevEx)
    PageControl: TcxPageControl;
    HorasTab: TcxTabSheet;
    GroupBox1: TcxGroupBox;
    Label20: TLabel;
    AU_FECHA: TZetaDBTextBox;
    Label6: TLabel;
    AU_POSICIO: TZetaDBTextBox;
    US_CODIGO: TZetaDBTextBox;
    Label22: TLabel;
    GroupBox2: TcxGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label16: TLabel;
    AU_EXTRAS: TZetaDBTextBox;
    Label5: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label7: TLabel;
    AU_HORASCK: TZetaDBTextBox;
    AU_PER_CG: TZetaDBNumero;
    AU_PER_SG: TZetaDBNumero;
    AU_DES_TRA: TZetaDBNumero;
    AU_TARDES: TZetaDBNumero;
    AU_TRIPLES: TZetaDBNumero;
    AU_HORAS: TZetaDBNumero;
    AU_DOBLES: TZetaDBNumero;
    TabSheet1: TcxTabSheet;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    AU_STATUS: TZetaDBKeyCombo;
    AU_TIPODIA: TZetaDBKeyCombo;
    AU_TIPO: TZetaDBKeyLookup_DevEx;
    HO_CODIGO: TZetaDBKeyLookup_DevEx;
    TabSheet3: TcxTabSheet;
    CB_NIVEL1Lbl: TLabel;
    CB_NIVEL2Lbl: TLabel;
    CB_NIVEL3Lbl: TLabel;
    CB_NIVEL4Lbl: TLabel;
    CB_NIVEL5Lbl: TLabel;
    CB_NIVEL6Lbl: TLabel;
    CB_NIVEL7Lbl: TLabel;
    CB_NIVEL8Lbl: TLabel;
    CB_NIVEL9Lbl: TLabel;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    bbMostrarCalendario: TcxButton;
    CB_NIVEL10Lbl: TLabel;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11Lbl: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12Lbl: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    procedure SetCamposNivel;
    {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$IFEND}
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure EscribirCambios; override;
    procedure DoCancelChanges; override;
  public
    { Public declarations }
  end;

var
  NomEditDatosAsist_DevEx: TNomEditDatosAsist_DevEx;

implementation

uses DCatalogos,
     DTablas,
     DNomina,
     DGlobal,
     ZAccesosTress,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TNomEditDatosAsist_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     {$endif}
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     IndexDerechos := D_ASIS_DATOS_PRENOMINA;
     HelpContext := H22121_edicion_prenomina; //H22122_Modificar_renglon;
     FirstControl := AU_HORAS;
     SetCamposNivel;

     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_PUESTO.LookupDataset := dmCatalogos.cdsPuestos;
     CB_CLASIFI.LookupDataset := dmCatalogos.cdsClasifi;
     AU_TIPO.LookupDataset := dmTablas.cdsIncidencias;
     HO_CODIGO.LookupDataset := dmCatalogos.cdsHorarios;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     {$ifdef ACS}{OP: 5.Ago.08}
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
     {$endif}

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TNomEditDatosAsist_DevEx.FormShow(Sender: TObject);
begin
     PageControl.ActivePage := HorasTab;
     inherited;
     {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$IFEND}
end;

procedure TNomEditDatosAsist_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsHorarios.Conectar;
          cdsTurnos.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
     end;
     with dmTablas do
     begin
          cdsIncidencias.Conectar;
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}{OP: 5.Ago.08}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmNomina do
     begin
          cdsDatosAsist.Conectar;
          DataSource.DataSet := cdsMovDatosAsist;
     end;
end;

procedure TNomEditDatosAsist_DevEx.Agregar;
begin
     { No debe Hacer nada }
end;

procedure TNomEditDatosAsist_DevEx.Borrar;
begin
     { No debe Hacer nada }
end;

procedure TNomEditDatosAsist_DevEx.EscribirCambios;
begin
     ClientDataset.Post;
end;

procedure TNomEditDatosAsist_DevEx.DoCancelChanges;
begin
     ClientDataset.Cancel;        // Si se deja la herencia cancela todos los post que se hayan hecho
end;

procedure TNomEditDatosAsist_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}{OP: 5.Ago.08}       
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

procedure TNomEditDatosAsist_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
  inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;

procedure TNomEditDatosAsist_DevEx.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmNomina.cdsMovDatosAsist do
               bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
end;


{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TNomEditDatosAsist_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     Self.Width:= K_WIDTH_SMALLFORM;
     for I := 0 to TabSheet3.ControlCount - 1 do
     begin
          if TabSheet3.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               TabSheet3.Controls[I].Left := K_WIDTH_SMALL_LEFT - (TabSheet3.Controls[I].Width+2);
          end;
          if TabSheet3.Controls[I].ClassNameIs( 'TZetaDbKeyLookup_DevEx' ) then
          begin
               if ( TabSheet3.Controls[I].Visible ) then
               begin
                    TabSheet3.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( TabSheet3.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabSheet3.Controls[I].Left := K_WIDTH_SMALL_LEFT;
               end;
          end;
     end;
end;
{$IFEND}

procedure TNomEditDatosAsist_DevEx.SetEditarSoloActivos;
begin
     HO_CODIGO.EditarSoloActivos := TRUE;
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;

     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
end;

end.


