inherited NomDatosAsist_DevEx: TNomDatosAsist_DevEx
  Left = 331
  Top = 195
  Caption = 'Pre-N'#243'mina'
  ClientHeight = 292
  ClientWidth = 814
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 814
    inherited Slider: TSplitter
      Left = 308
    end
    inherited ValorActivo1: TPanel
      Width = 292
      inherited textoValorActivo1: TLabel
        Width = 286
      end
    end
    inherited ValorActivo2: TPanel
      Left = 311
      Width = 503
      inherited textoValorActivo2: TLabel
        Width = 497
      end
    end
  end
  object PageControl1: TcxPageControl [1]
    Left = 0
    Top = 19
    Width = 814
    Height = 114
    Align = alTop
    TabOrder = 2
    Properties.ActivePage = TabSheet1
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    ClientRectBottom = 86
    ClientRectLeft = 2
    ClientRectRight = 812
    ClientRectTop = 2
    object TabSheet1: TcxTabSheet
      Caption = 'D'#237'as'
      object Label17: TLabel
        Left = 19
        Top = 4
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarios:'
      end
      object NO_DIAS: TZetaDBTextBox
        Left = 71
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label18: TLabel
        Left = 18
        Top = 22
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_DIAS_AS: TZetaDBTextBox
        Left = 71
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label19: TLabel
        Left = 339
        Top = 4
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas injustificadas:'
      end
      object NO_DIAS_FI: TZetaDBTextBox
        Left = 436
        Top = 2
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label20: TLabel
        Left = 347
        Top = 22
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas justificadas:'
      end
      object NO_DIAS_FJ: TZetaDBTextBox
        Left = 436
        Top = 20
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 138
        Top = 4
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Incapacidad:'
      end
      object NO_DIAS_IN: TZetaDBTextBox
        Left = 204
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_IN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_IN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 0
        Top = 40
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'No trabajados:'
      end
      object NO_DIAS_NT: TZetaDBTextBox
        Left = 71
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_NT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_NT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label23: TLabel
        Left = 117
        Top = 22
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso sin goce:'
      end
      object NO_DIAS_SG: TZetaDBTextBox
        Left = 204
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label24: TLabel
        Left = 10
        Top = 58
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Suspensi'#243'n:'
      end
      object NO_DIAS_SU: TZetaDBTextBox
        Left = 71
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SU'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SU'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label32: TLabel
        Left = 475
        Top = 4
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aguinaldo/pagar:'
      end
      object NO_DIAS_AG: TZetaDBTextBox
        Left = 560
        Top = 2
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_VA: TZetaDBTextBox
        Left = 436
        Top = 56
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_VA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_VA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label31: TLabel
        Left = 335
        Top = 58
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaciones a pagar:'
      end
      object NO_DIAS_RE: TZetaDBTextBox
        Left = 293
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_RE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_RE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label30: TLabel
        Left = 245
        Top = 22
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Retardos:'
      end
      object NO_DIAS_AJ: TZetaDBTextBox
        Left = 293
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label29: TLabel
        Left = 259
        Top = 4
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ajuste:'
      end
      object NO_DIAS_OT: TZetaDBTextBox
        Left = 204
        Top = 55
        Width = 38
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_OT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_OT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label28: TLabel
        Left = 129
        Top = 58
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otros permisos:'
      end
      object NO_DIAS_CG: TZetaDBTextBox
        Left = 204
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label26: TLabel
        Left = 258
        Top = 58
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'E Y M:'
      end
      object NO_DIAS_EM: TZetaDBTextBox
        Left = 293
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_EM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_EM'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label27: TLabel
        Left = 113
        Top = 40
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso con goce:'
      end
      object NO_DIAS_SS: TZetaDBTextBox
        Left = 293
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label25: TLabel
        Left = 261
        Top = 40
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'IMSS:'
      end
      object Label37: TLabel
        Left = 482
        Top = 22
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subsidio incap.:'
      end
      object NO_DIAS_SI: TZetaDBTextBox
        Left = 560
        Top = 20
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label40: TLabel
        Left = 344
        Top = 40
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas vacaciones:'
      end
      object NO_DIAS_FV: TZetaDBTextBox
        Left = 436
        Top = 38
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FV'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FV'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Calcular: TcxButton
        Left = 609
        Top = 39
        Width = 90
        Height = 34
        Hint = 'Calcular Pre-N'#243'mina'
        Caption = 'Calcular'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = CalcularClick
        OptionsImage.Glyph.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          20000000000000100000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF9B939CFFDEDBDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE6E3E6FFA199A2FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFCECACEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFDCD9DDFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
          8CFFA8A1A9FFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFFFFFFFFFBEB8BFFFE2DFE2FFFBFBFBFFFDFDFDFFE4E1
          E4FFC2BCC2FFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE9E7E9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD9D5D9FF9A919AFF9A919AFF9A91
          9AFF9A919AFFDCD9DDFFFFFFFFFFFFFFFFFFAAA3ABFFF6F5F6FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFE9E7E9FF9A919AFF9A919AFF9A919AFF9A919AFFCCC8
          CCFFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D5D9FFAEA7AEFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFC2BCC2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFDEDBDFFFA8A1A9FFA8A1A9FFA8A1
          A9FFD5D2D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7FFC8C4C9FFFDFD
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFE4E1E4FFA8A1A9FFA8A1A9FFA8A1A9FFD3D0
          D4FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFFEDEBEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E1E4FF8B818CFFD0CC
          D0FFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFFEBE9EBFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBFFA49DA5FFC5C0C6FFF8F7
          F8FFFFFFFFFFFFFFFFFFFFFFFFFFF8F7F8FF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFFD5D2D6FFFFFFFFFFFFFFFFFFFFFFFFFFB5AEB5FFC5C0C6FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E1E4FF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFF4F3F4FFE2DFE2FFE2DFE2FFE2DF
          E2FFE2DFE2FFFFFFFFFFFFFFFFFFC7C2C7FFB1ABB2FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFBEB8BFFFE2DFE2FFE2DFE2FFE2DFE2FFF0EF
          F1FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFE9E7E9FFC5C0C6FFC5C0C6FFC5C0
          C6FFC5C0C6FFD9D5D9FFDEDBDFFFA39BA3FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFE6E3E6FFC5C0C6FFC5C0C6FFC5C0C6FFC5C0C6FFE2DF
          E2FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFC3BEC4FFF4F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFDCD9DDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFFFFFFFFFAFA9B0FFD3D0D4FFEFEDEFFFE7E5E8FFD0CC
          D0FFA8A1A9FFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
          8CFFA8A1A9FFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC5C0
          C6FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFE9E7E9FFC5C0C6FFC5C0C6FFC5C0
          C6FFC5C0C6FFC5C0C6FFFFFFFFFFD3D0D4FFC5C0C6FFC5C0C6FFC5C0C6FFC5C0
          C6FFD3D0D4FFFFFFFFFFC5C0C6FFC5C0C6FFC5C0C6FFC5C0C6FFC5C0C6FFE2DF
          E2FFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFD3D0D4FFA8A1A9FFA8A1
          A9FFA8A1A9FFA8A1A9FFA8A1A9FFF4F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF4F3F4FFA8A1A9FFA8A1A9FFA8A1A9FFA8A1A9FFA8A1A9FFC8C4C9FFFFFF
          FFFFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFFB1AB
          B2FFB7B0B7FFB1ABB2FF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF0EFF1FF8B818CFFA199A2FFB7B0B7FFB7B0B7FF908791FFB7B0B7FFFFFF
          FFFFFFFFFFFFFFFFFFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFDCD9DDFFFFFFFFFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFFF0EF
          F1FFFFFFFFFFF0EFF1FF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF0EFF1FF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFF9A919AFFB7B0B7FFFFFF
          FFFFFFFFFFFFFFFFFFFFE7E5E8FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFAAA3ABFFFBFBFBFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFFF0EF
          F1FFFFFFFFFFF0EFF1FF8B818CFFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF0EFF1FF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFF9A919AFFB7B0B7FFFFFF
          FFFFFFFFFFFFFDFDFDFFB7B0B7FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF948B95FFB5AEB5FFB7B0B7FFA199A2FF8B818CFFEDEB
          EDFFFFFFFFFFEDEBEDFF8B818CFFB1ABB2FFB7B0B7FFB7B0B7FFB7B0B7FFB7B0
          B7FFB1ABB2FF8B818CFFC2BCC2FFFFFFFFFFFFFFFFFF988F99FF9B939CFFB7B0
          B7FFB7B0B7FF968D97FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAFA9
          B0FFE9E7E9FFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF988F99FFE4E1E4FFD0CCD0FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 1
        OptionsImage.Spacing = 6
      end
      object bbtnModificarAsis: TcxButton
        Left = 609
        Top = 2
        Width = 90
        Height = 34
        Hint = 'Modificar Asistencia de tarjetas del periodo'
        Caption = 'Modificar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = bbtnModificarAsisClick
        OptionsImage.Glyph.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          200000000000001000000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B3E9FF00AEE8FF00A9E6FF00ABE7FF00ADE8FF00B0
          E8FF00B0E8FF00AEE8FF00AFE8FF00AFE8FF00AFE8FF00AFE8FF00B0E8FF00B0
          E8FF00B1E9FF00B1E9FF00B1E9FF00B1E9FF00B1E9FF00AFE8FF00AEE8FF00AE
          E8FF00B1E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B3E9FF00AEE8FF0FB6EAFF53CBF0FF53CBF0FF53CBF0FF53CB
          F0FF53CBF0FF53CBF0FF53CBF0FF53CBF0FF53CBF0FF53CBF0FF53CBF0FF53CB
          F0FF53CBF0FF53CBF0FF53CBF0FF53CBF0FF53CBF0FF53CBF0FF53CBF0FF53CB
          F0FF13B8EBFF00AFE8FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF0CB2E9FFC6EEFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD8F4FCFF19B7EAFF00B1E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00ADE8FF3BC5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF44C8F0FF00ADE7FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00ADE7FF48C7EFFFFFFFFFFFFFFFFFFFD7F3FBFF46C7EFFF44C4
          EEFF44C6EFFF42C0EDFFA0E2F7FFD1F1FCFF44C4EEFF44C5EEFF44C6EFFF43C3
          EEFFC1ECF9FFB1E7F8FF43C1EDFF44C6EFFF44C5EEFF44C6EFFFC9EFFAFFFFFF
          FFFFFFFFFFFF46C6EFFF00ACE7FF00B2E9FF00B2E9FF00B2E9FF00B1E8FE00B2
          E9FF00B2E9FF00ADE7FF44C6EFFFFFFFFFFFFFFFFFFFC1EDF9FF00AAE6FF00A5
          E5FF00A9E6FF009FE4FF6DD3F3FFBAEAF9FF00A5E5FF00A7E5FF00A8E6FF00A3
          E5FFA0E2F6FF87DAF5FF00A1E4FF00A8E6FF00A7E5FF00A8E6FFACE6F8FFFFFF
          FFFFFFFFFFFF44C6EFFF00ADE7FF00B2E9FF00B2E9FF00B2E9FF00B1E8FE00B2
          E9FF00B2E9FF00ADE7FF44C6EFFFFFFFFFFFFFFFFFFFC1EDF9FF00AAE6FF00A5
          E5FF00A9E6FF009FE4FF6DD3F3FFBAEAF9FF00A5E5FF00A7E5FF00A8E6FF00A3
          E5FFA0E2F6FF87DAF5FF00A1E4FF00A8E6FF00A7E5FF00A8E6FFACE6F8FFFFFF
          FFFFFFFFFFFF44C6EFFF00AEE8FF00B2E9FF00B2E9FF00B1E8FE00B2E9FF00B2
          E9FF00B2E9FF00ADE7FF44C6EFFFFFFFFFFFFFFFFFFFDAF4FBFF55CCF0FF53C9
          EFFF53CBF0FF51C5EFFFA7E5F8FFD5F2FCFF53C9EFFF53CAEFFF53CAF0FF52C7
          EFFFC6EEFAFFB7E9F9FF52C6EFFF53CAF0FF53CAEFFF53CAF0FFCDF0FBFFFFFF
          FFFFFFFFFFFF44C6EFFF00AEE8FF00B2E9FF00B2E9FF00B1E8FE00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFFBFEFEFFEFFAFDFFEEFA
          FDFFEFFAFDFFEBF9FEFFF7FCFFFFFBFDFFFFEEFAFDFFEEFAFDFFEEFAFDFFEDF9
          FDFFF9FEFFFFF9FDFFFFECF9FEFFEFFAFDFFEEFAFDFFEFFAFDFFFBFDFEFFFFFF
          FFFFFFFFFFFF44C6EFFF00AEE8FF00B2E9FF00B2E9FF00B1E8FE00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFCBF0FAFF10B7EAFF0DB3
          E9FF0DB6EAFF0DAEE8FF84DAF5FFC4EDFAFF0DB3E9FF0DB4E9FF0DB5EAFF0DB1
          E9FFAFE6F7FF99E0F6FF0DAFE8FF0DB5EAFF0DB4E9FF0DB5EAFFB9EAF9FFFFFF
          FFFFFFFFFFFF44C6EFFF00AEE8FF00B2E9FF00B2E9FF00B1E8FE00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFC6EEFAFF00B0E8FF00AC
          E7FF00AFE8FF00A5E6FF78D6F4FFBEEBFAFF00ACE7FF00ADE7FF00AEE8FF00AA
          E7FFA6E4F7FF92DEF6FF00A8E6FF00AEE8FF00ADE7FF00AEE8FFB2E8F9FFFFFF
          FFFFFFFFFFFF44C6EFFF00AEE8FF00B2E9FF00B2E9FF00B1E8FE00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFC1EDF9FF00AAE6FF00A5
          E5FF00A9E6FF009FE4FF6DD3F3FFBAEAF9FF00A5E5FF00A6E5FF00A8E6FF00A3
          E5FF9FE2F6FF98DFF6FF00A3E5FF00AAE7FF00AAE7FF00B3E9FFBFECFAFFFFFF
          FFFFFFFFFFFF44C6EFFF00AFE8FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFF2FBFEFFC3EDFAFFC2EC
          F9FFC2EDFAFFC1EBF9FFE0F6FCFFF0FAFEFFC2ECF9FFC2ECF9FFC2EDFAFFC2EC
          F9FFEFFAFEFF87DBF5FF8FDBF4FFB2E8F8FF75D4F3FF00AAE7FF99E0F7FFFFFF
          FFFFFFFFFFFF43C6EFFF00B0E8FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFECFAFDFFA9E5F7FFA6E4
          F7FFA8E5F7FFA3E2F6FFD2F2FBFFEAF8FDFFA6E4F7FFA6E4F7FFA7E5F7FFA5E3
          F7FFE4F7FDFF91DEF6FFFBFFFEFFFFFFFFFF85DBF5FF00A1E4FF00ADE8FFA8E6
          F8FFFFFFFFFF49C7F0FF00B0E8FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFC2EDF9FF00ABE7FF00A6
          E5FF00A9E7FF009FE4FF6FD4F3FFBAEAF9FF00A6E5FF00A7E5FF00A8E7FF00A4
          E5FF97E0F6FF94DFF5FFAEE7F8FF74D4F3FF00A0E4FF4AC8EFFF36C1EDFF00A6
          E6FFA4E5F7FF58CCF1FF00B0E8FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFC5EEFAFF00AFE8FF00AA
          E7FF00AEE8FF00A4E6FF75D6F3FFBDEBFAFF00AAE7FF00ABE7FF00ACE8FF00A8
          E7FFA6E4F8FF8BDCF5FF08ADE8FF00A5E5FF54C8EFFFFFFFFFFFE9F9FDFF09B3
          E9FF00AAE7FF10B7EAFF00B1E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFD0F1FBFF25BEECFF1EBB
          EBFF22BDECFF14B5EAFF8FDDF5FFC9EEFAFF1EBAEBFF20BBECFF21BCECFF1AB9
          EBFFBAEAF9FF75D5F3FF009EE3FF76D7F3FFFFFFFFFFFFFFFFFFFFFFFFFFC9EF
          FAFF02AFE8FF00ACE7FF04B5E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFFDFEFFFFF9FDFEFFFBFE
          FFFFFCFEFFFFF7FEFFFFFFFFFFFFFFFFFFFFF4FCFEFFF5FCFEFFF5FCFEFFF3FC
          FEFFFFFFFFFFB3E8F8FF00AAE6FF59CCF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFAAE6F8FF00ABE7FF00AFE8FF01B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF44C6EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF60D1F2FF00A2E4FF63CFF1FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF85DBF5FF00A9E7FF01B1E8FF01B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00AFE8FF45C6EFFFFFFFFFFFFFFFFFFFFFFFFFFFB3E8F8FF56C9
          F0FF46C7EFFF43C6EFFF4EC6EFFFB4E9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB4E8F8FF59C9F0FF41C5EEFF07B2E8FF00A7E6FF92DFF6FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF76D6F3FF00ACE7FF00B0E8FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B0E8FF49C7EFFFFFFFFFFFFFFFFFFFFFFFFFFF74D5F3FF00A3
          E5FF55CCF0FF65D0F2FF0AB1E9FF6FD3F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF72D4F2FF00A9E6FF64CFF1FF89DCF5FF20BDECFF00ABE7FFABE7F8FFFFFF
          FFFFFFFFFFFFFFFFFFFF96DFF6FF00A9E6FF00AFE8FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B1E8FF26BEECFFFFFFFFFFFFFFFFFFFFFFFFFF7ED9F4FF00AE
          E8FFFFFFFFFFFFFFFFFF58CEF2FF6ED4F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF77D7F3FF1ABBECFFFFFFFFFFFFFFFFFF5ACDF1FF00A9E6FF08B1E9FFE0F6
          FCFFFFFFFFFF6BD2F2FF0AB4EAFF9CE0F6FF37C3EEFF00B1E8FE00B2E9FF00B2
          E9FF00B2E9FF00B1E9FF00ADE8FF68D1F2FFE2F7FCFFF0FAFEFF71D4F2FF01AF
          E8FFFFFFFFFFFFFFFFFF61D0F2FF64D0F1FFF0FAFEFFF0FAFEFFF0FAFEFFF0FA
          FEFF6CD2F2FF1EBCECFFFFFFFFFFFFFFFFFF50CAF0FF00AEE8FF00ADE7FF2EC0
          EDFF5DCFF1FF22BDECFFD2F1FBFFFFFFFFFFB9EAF9FF00ADE5FB00B2E9FF00B2
          E9FF00B2E9FF00B3E9FF00B1E8FF00ADE8FF0BB5EAFF0FB7EAFF07B2E9FF04B1
          E9FFC9EFFBFFFFFFFFFF44C6EFFF03B0E8FF09B5EAFF0CB6EAFF04B3E9FF05B4
          E9FF07B1E9FF10B5EAFFE4F7FDFFFBFEFFFF27BEECFF00ADE7FF01B3E9FF00B0
          E8FF00AEE7FFE3F6FCFFFFFFFFFFFFFFFFFFB3E8F8FF00B1E8FE00B1E8FE00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B1E9FF00AFE8FF00AEE8FF00B0E9FF00B0
          E9FF12B7EBFF2BBFECFF00B1E9FF00B2E9FF00B2E9FF00B1E9FF00B1E9FF00B1
          E9FF00B0E9FF00B0E8FF1EBBECFF2DC0EDFF00AFE8FF00B2E9FF00B1E9FF01B3
          E9FF00B1E9FF48C7F0FFC2ECFAFFA1E3F7FF1EBBECFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B1E9FF00B1E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B1E9FF01B2E9FF00B1E9FF00B1E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B1E8FF02B3EAFF02B3E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B1E8FE00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B1E7FD}
        OptionsImage.Margin = 1
        OptionsImage.Spacing = 6
      end
    end
    object TabSheet2: TcxTabSheet
      Caption = 'Horas'
      object Label4: TLabel
        Left = 26
        Top = 6
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarias:'
      end
      object NO_HORAS: TZetaDBTextBox
        Left = 80
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label10: TLabel
        Left = 161
        Top = 42
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima dominical:'
      end
      object NO_HORA_PD: TZetaDBTextBox
        Left = 242
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_PD'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_PD'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DES_TRA: TZetaDBTextBox
        Left = 408
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label11: TLabel
        Left = 304
        Top = 24
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descanso trabajado:'
      end
      object NO_EXTRAS: TZetaDBTextBox
        Left = 80
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_EXTRAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_EXTRAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 44
        Top = 24
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extras:'
      end
      object Label6: TLabel
        Left = 40
        Top = 42
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dobles:'
      end
      object NO_TRIPLES: TZetaDBTextBox
        Left = 80
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TRIPLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TRIPLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 318
        Top = 42
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo trabajado:'
      end
      object NO_FES_TRA: TZetaDBTextBox
        Left = 408
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_VAC_TRA: TZetaDBTextBox
        Left = 408
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_VAC_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_VAC_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label13: TLabel
        Left = 307
        Top = 60
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaci'#243'n trabajada:'
      end
      object NO_DOBLES: TZetaDBTextBox
        Left = 80
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DOBLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DOBLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 40
        Top = 60
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Triples:'
      end
      object Label8: TLabel
        Left = 201
        Top = 6
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tardes:'
      end
      object NO_TARDES: TZetaDBTextBox
        Left = 242
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TARDES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TARDES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label14: TLabel
        Left = 462
        Top = 6
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso con goce:'
      end
      object NO_HORA_CG: TZetaDBTextBox
        Left = 556
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_HORA_SG: TZetaDBTextBox
        Left = 556
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label15: TLabel
        Left = 467
        Top = 24
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso sin goce:'
      end
      object NO_ADICION: TZetaDBTextBox
        Left = 408
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_ADICION'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_ADICION'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 346
        Top = 6
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Adicionales:'
      end
      object Label36: TLabel
        Left = 474
        Top = 42
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo pagado:'
      end
      object NO_FES_PAG: TZetaDBTextBox
        Left = 556
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_PAG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_PAG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label38: TLabel
        Left = 138
        Top = 60
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima dominical total:'
      end
      object NO_HORAPDT: TZetaDBTextBox
        Left = 242
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAPDT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAPDT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label39: TLabel
        Left = 168
        Top = 24
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'No trabajadas:'
      end
      object NO_HORASNT: TZetaDBTextBox
        Left = 242
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORASNT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORASNT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object TabSheet4: TcxTabSheet
      Caption = 'Turno'
      object Label1: TLabel
        Left = 52
        Top = 4
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_TURNO: TZetaDBTextBox
        Left = 86
        Top = 2
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'CB_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Turno: TZetaDBTextBox
        Left = 155
        Top = 2
        Width = 217
        Height = 17
        AutoSize = False
        Caption = 'Turno'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Turno'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 57
        Top = 23
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as:'
      end
      object Label3: TLabel
        Left = 42
        Top = 43
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Jornada:'
      end
      object NO_D_TURNO: TZetaDBTextBox
        Left = 86
        Top = 22
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_D_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_D_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_JORNADA: TZetaDBTextBox
        Left = 86
        Top = 42
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_JORNADA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_JORNADA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object bbMostrarCalendario: TcxButton
        Left = 374
        Top = 0
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = bbMostrarCalendarioClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end
    end
    object TabSheet3: TcxTabSheet
      Caption = 'Generales'
      object Label34: TLabel
        Left = 35
        Top = 46
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 82
        Top = 44
        Width = 239
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label35: TLabel
        Left = 27
        Top = 67
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_USER_RJ: TZetaDBTextBox
        Left = 82
        Top = 65
        Width = 239
        Height = 17
        AutoSize = False
        Caption = 'NO_USER_RJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_USER_RJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_OBSERVA: TZetaDBTextBox
        Left = 82
        Top = 23
        Width = 239
        Height = 17
        AutoSize = False
        Caption = 'NO_OBSERVA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_OBSERVA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label16: TLabel
        Left = 4
        Top = 25
        Width = 74
        Height = 13
        Caption = 'Observaciones:'
      end
      object Label33: TLabel
        Left = 45
        Top = 4
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object STATUSNOM: TZetaDBTextBox
        Left = 82
        Top = 2
        Width = 141
        Height = 17
        AutoSize = False
        Caption = 'STATUSNOM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object gbAutorizacion: TcxGroupBox
        Left = 403
        Top = 1
        Caption = ' Autorizaci'#243'n de Pre-N'#243'mina '
        TabOrder = 0
        Height = 83
        Width = 246
        object Label41: TLabel
          Left = 22
          Top = 20
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Usuario:'
        end
        object NO_SUP_OK: TZetaDBTextBox
          Left = 62
          Top = 19
          Width = 140
          Height = 17
          AutoSize = False
          Caption = 'NO_SUP_OK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_DES_OK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label42: TLabel
          Left = 28
          Top = 39
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object NO_FEC_OK: TZetaDBTextBox
          Left = 62
          Top = 38
          Width = 140
          Height = 17
          AutoSize = False
          Caption = 'NO_FEC_OK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_FEC_OK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label43: TLabel
          Left = 35
          Top = 58
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object NO_HOR_OK: TZetaDBTextBox
          Left = 62
          Top = 57
          Width = 140
          Height = 17
          AutoSize = False
          Caption = 'NO_HOR_OK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_HOR_OK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 133
    Width = 814
    Height = 159
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = dsAusencia
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object AU_POSICIO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'AU_POSICIO'
        FooterAlignmentHorz = taRightJustify
        HeaderAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_FECHA: TcxGridDBColumn
        Caption = 'Fecha y D'#237'a'
        DataBinding.FieldName = 'AU_FECHA'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object CHECADA1: TcxGridDBColumn
        Caption = 'Ent 1'
        DataBinding.FieldName = 'CHECADA1'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object CHECADA2: TcxGridDBColumn
        Caption = 'Sal 1'
        DataBinding.FieldName = 'CHECADA2'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object CHECADA3: TcxGridDBColumn
        Caption = 'Ent 2'
        DataBinding.FieldName = 'CHECADA3'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object CHECADA4: TcxGridDBColumn
        Caption = 'Sal 2'
        DataBinding.FieldName = 'CHECADA4'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_HORAS: TcxGridDBColumn
        Caption = 'Ordinarias'
        DataBinding.FieldName = 'AU_HORAS'
        Width = 64
      end
      object AU_NUM_EXT: TcxGridDBColumn
        Caption = 'Hrs. Extras'
        DataBinding.FieldName = 'AU_NUM_EXT'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_EXTRAS: TcxGridDBColumn
        Caption = 'Extras Pago'
        DataBinding.FieldName = 'AU_EXTRAS'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_PER_CG: TcxGridDBColumn
        Caption = 'Permiso CG'
        DataBinding.FieldName = 'AU_PER_CG'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_PER_SG: TcxGridDBColumn
        Caption = 'Permiso SG'
        DataBinding.FieldName = 'AU_PER_SG'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_DES_TRA: TcxGridDBColumn
        Caption = 'Des. Trab.'
        DataBinding.FieldName = 'AU_DES_TRA'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_TARDES: TcxGridDBColumn
        Caption = 'Tardes'
        DataBinding.FieldName = 'AU_TARDES'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_STATUS: TcxGridDBColumn
        Caption = 'D'#237'a'
        DataBinding.FieldName = 'AU_STATUS'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
      object AU_TIPODIA: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AU_TIPODIA'
        FooterAlignmentHorz = taRightJustify
      end
      object AU_TIPO: TcxGridDBColumn
        Caption = 'Incidencia'
        DataBinding.FieldName = 'AU_TIPO'
        FooterAlignmentHorz = taRightJustify
      end
      object HO_CODIGO: TcxGridDBColumn
        Caption = 'Horario'
        DataBinding.FieldName = 'HO_CODIGO'
        FooterAlignmentHorz = taRightJustify
      end
      object US_CODIGO_GRID: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_CODIGO'
        FooterAlignmentHorz = taRightJustify
        Width = 64
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 96
    Top = 184
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsAusencia: TDataSource
    OnDataChange = dsAusenciaDataChange
    Left = 56
    Top = 176
  end
end
