inherited GridPrenomina_DevEx: TGridPrenomina_DevEx
  Left = 521
  Top = 435
  Caption = 'Pre-N'#243'mina'
  ClientHeight = 378
  ClientWidth = 599
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 342
    Width = 599
    inherited OK_DevEx: TcxButton
      Left = 435
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 514
    end
    object BBModificar: TcxButton
      Left = 8
      Top = 6
      Width = 121
      Height = 25
      Hint = 'Modificar Rengl'#243'n'
      Caption = '&Modificar Rengl'#243'n'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BBModificarClick
      OptionsImage.Glyph.Data = {
        7A080000424D7A08000000000000360000002800000017000000170000000100
        20000000000044080000C40E0000C40E0000000000000000000000B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF97E0F6FF97E0F6FF2CBFEDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2
        F7FFFFFFFFFFFFFFFFFFB7E9F9FF44C7EFFF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFF
        FFFFFBFEFFFF9FE2F7FF18B9EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFAFE7F8FF24BD
        ECFF00B2E9FF0CB6EAFF68D1F2FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF10B7EAFF00B2E9FF14B8EBFF4CC9
        F0FFFFFFFFFFFFFFFFFF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFDFF5FCFFFFFFFFFFFFFF
        FFFFFFFFFFFFEBF9FDFF10B7EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FFA3E3F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF8BDCF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF1CBAEBFFF7FDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFBFEFFFF28BEECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF80D9F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFB7E9F9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF08B4EAFFE3F7FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E9
        F9FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF5CCEF1FFFFFFFFFFFFFFFFFFD7F3FCFF48C8EFFF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FFB7E9F9FF6CD3F2FF04B3E9FF00B2E9FF1CBAEBFFA3E3F7FF8BDC
        F5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF08B4EAFF80D9F4FFF7FDFEFFFFFFFFFFA3E3F7FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF40C5EFFFE3F7FDFFFFFFFFFFF7FDFEFF7CD7F4FF08B4EAFF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF18B9
        EBFFBFECF9FFA7E4F7FF1CBAEBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      OptionsImage.Margin = 1
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 599
    inherited Splitter: TSplitter
      Left = 339
    end
    object ImgNoRecord: TImage [1]
      Left = 0
      Top = 0
      Width = 16
      Height = 19
      Hint = 'No Existe Informaci'#243'n'
      Align = alLeft
      ParentShowHint = False
      Picture.Data = {
        07544269746D617042010000424D420100000000000076000000280000001100
        0000110000000100040000000000CC0000000000000000000000100000001000
        000000000000000080000080000000808000800000008000800080800000C0C0
        C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DD0000000000
        000DD0000000D999FFFFFFFF9990D0000000D999900000099990D0000000D099
        999FF99999F0D0000000D0F99999999990F0D0000000D0FFF999999FFFF0D000
        0000D0F00099990000F0D0000000D0FFF999999FFFF0D0000000D0F999999999
        90F0D0000000D099999FF99999F0D0000000D999900000099990D0000000D999
        FFFFFFFF9990D0000000D99000000000099DD0000000DDDDDDDDDDDDDDDDD000
        0000DDDDDDDDDDDDDDDDD0000000}
      ShowHint = True
      Transparent = True
      Visible = False
    end
    inherited ValorActivo1: TPanel
      Left = 16
    end
    inherited ValorActivo2: TPanel
      Left = 342
      Width = 257
      inherited textoValorActivo2: TLabel
        Width = 251
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 187
    Width = 599
    Height = 155
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    TabOrder = 5
    OnDblClick = ZetaDBGridDblClick
    OnTitleClick = ZetaDBGridTitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'AU_POSICIO'
        ReadOnly = True
        Title.Alignment = taRightJustify
        Title.Caption = '#'
        Width = 20
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_FECHA'
        ReadOnly = True
        Title.Caption = 'Fecha'
        Width = 125
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_HORAS'
        Title.Alignment = taRightJustify
        Title.Caption = 'Ordinarias'
        Width = 57
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_DOBLES'
        Title.Alignment = taRightJustify
        Title.Caption = 'Dobles'
        Width = 39
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_TRIPLES'
        Title.Alignment = taRightJustify
        Title.Caption = 'Triples'
        Width = 36
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_PER_CG'
        Title.Alignment = taRightJustify
        Title.Caption = 'Permiso CG'
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_PER_SG'
        Title.Alignment = taRightJustify
        Title.Caption = 'Permiso SG'
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_DES_TRA'
        Title.Alignment = taRightJustify
        Title.Caption = 'Des. Tra.'
        Width = 55
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_TARDES'
        Title.Alignment = taRightJustify
        Title.Caption = 'Tardes'
        Width = 43
        Visible = True
      end
      item
        Alignment = taRightJustify
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'US_CODIGO'
        ReadOnly = True
        Title.Alignment = taRightJustify
        Title.Caption = 'Modific'#243
        Width = 48
        Visible = True
      end>
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 4
  end
  object PageControl1: TcxPageControl [4]
    Left = 0
    Top = 77
    Width = 599
    Height = 110
    Align = alTop
    TabOrder = 2
    Properties.ActivePage = TabSheet1
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    ClientRectBottom = 86
    ClientRectLeft = 4
    ClientRectRight = 595
    ClientRectTop = 4
    object TabSheet1: TcxTabSheet
      Caption = 'D'#237'as'
      object Label17: TLabel
        Left = 19
        Top = 4
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarios:'
      end
      object NO_DIAS: TZetaDBTextBox
        Left = 71
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label18: TLabel
        Left = 18
        Top = 22
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_DIAS_AS: TZetaDBTextBox
        Left = 71
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AS'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label19: TLabel
        Left = 338
        Top = 4
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas injustificadas:'
      end
      object NO_DIAS_FI: TZetaDBTextBox
        Left = 434
        Top = 2
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FI'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label20: TLabel
        Left = 345
        Top = 22
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas justificadas:'
      end
      object NO_DIAS_FJ: TZetaDBTextBox
        Left = 434
        Top = 20
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FJ'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 138
        Top = 4
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Incapacidad:'
      end
      object NO_DIAS_IN: TZetaDBTextBox
        Left = 203
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_IN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_IN'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 0
        Top = 40
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'No trabajados:'
      end
      object NO_DIAS_NT: TZetaDBTextBox
        Left = 71
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_NT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_NT'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label23: TLabel
        Left = 117
        Top = 22
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso sin goce:'
      end
      object NO_DIAS_SG: TZetaDBTextBox
        Left = 203
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SG'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label24: TLabel
        Left = 10
        Top = 58
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Suspensi'#243'n:'
      end
      object NO_DIAS_SU: TZetaDBTextBox
        Left = 71
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SU'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SU'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label32: TLabel
        Left = 472
        Top = 4
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aguinaldo/pagar:'
      end
      object NO_DIAS_AG: TZetaDBTextBox
        Left = 556
        Top = 2
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AG'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_VA: TZetaDBTextBox
        Left = 434
        Top = 56
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_VA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_VA'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label31: TLabel
        Left = 334
        Top = 58
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaciones a pagar:'
      end
      object NO_DIAS_RE: TZetaDBTextBox
        Left = 292
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_RE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_RE'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label30: TLabel
        Left = 244
        Top = 22
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Retardos:'
      end
      object NO_DIAS_AJ: TZetaDBTextBox
        Left = 292
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AJ'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label29: TLabel
        Left = 258
        Top = 4
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ajuste:'
      end
      object NO_DIAS_OT: TZetaDBTextBox
        Left = 203
        Top = 55
        Width = 38
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_OT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_OT'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label28: TLabel
        Left = 129
        Top = 58
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otros permisos:'
      end
      object NO_DIAS_CG: TZetaDBTextBox
        Left = 203
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_CG'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label26: TLabel
        Left = 257
        Top = 58
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'E Y M:'
      end
      object NO_DIAS_EM: TZetaDBTextBox
        Left = 292
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_EM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_EM'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label27: TLabel
        Left = 113
        Top = 40
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso con goce:'
      end
      object NO_DIAS_SS: TZetaDBTextBox
        Left = 292
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SS'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label25: TLabel
        Left = 260
        Top = 40
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'IMSS:'
      end
      object Label37: TLabel
        Left = 479
        Top = 22
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subsidio incap.:'
      end
      object NO_DIAS_SI: TZetaDBTextBox
        Left = 556
        Top = 20
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SI'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label40: TLabel
        Left = 342
        Top = 40
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas vacaciones:'
      end
      object NO_DIAS_FV: TZetaDBTextBox
        Left = 434
        Top = 38
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FV'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FV'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object TabSheet2: TcxTabSheet
      Caption = 'Horas'
      object Label4: TLabel
        Left = 7
        Top = 6
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarias:'
      end
      object NO_HORAS: TZetaDBTextBox
        Left = 61
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAS'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label10: TLabel
        Left = 142
        Top = 42
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima dominical:'
      end
      object NO_HORA_PD: TZetaDBTextBox
        Left = 223
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_PD'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_PD'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DES_TRA: TZetaDBTextBox
        Left = 394
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DES_TRA'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label11: TLabel
        Left = 290
        Top = 24
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descanso trabajado:'
      end
      object NO_EXTRAS: TZetaDBTextBox
        Left = 61
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_EXTRAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_EXTRAS'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 25
        Top = 24
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extras:'
      end
      object Label6: TLabel
        Left = 21
        Top = 42
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dobles:'
      end
      object NO_TRIPLES: TZetaDBTextBox
        Left = 61
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TRIPLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TRIPLES'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 304
        Top = 42
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo trabajado:'
      end
      object NO_FES_TRA: TZetaDBTextBox
        Left = 394
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_TRA'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_VAC_TRA: TZetaDBTextBox
        Left = 394
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_VAC_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_VAC_TRA'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label13: TLabel
        Left = 293
        Top = 60
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaci'#243'n trabajada:'
      end
      object NO_DOBLES: TZetaDBTextBox
        Left = 61
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DOBLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DOBLES'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 21
        Top = 60
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Triples:'
      end
      object Label8: TLabel
        Left = 182
        Top = 6
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tardes:'
      end
      object NO_TARDES: TZetaDBTextBox
        Left = 223
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TARDES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TARDES'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label14: TLabel
        Left = 452
        Top = 6
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso con goce:'
      end
      object NO_HORA_CG: TZetaDBTextBox
        Left = 546
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_CG'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_HORA_SG: TZetaDBTextBox
        Left = 546
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_SG'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label15: TLabel
        Left = 457
        Top = 24
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso sin goce:'
      end
      object NO_ADICION: TZetaDBTextBox
        Left = 394
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_ADICION'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_ADICION'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 332
        Top = 6
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Adicionales:'
      end
      object Label36: TLabel
        Left = 464
        Top = 42
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo pagado:'
      end
      object NO_FES_PAG: TZetaDBTextBox
        Left = 546
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_PAG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_PAG'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label38: TLabel
        Left = 149
        Top = 24
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'No trabajadas:'
      end
      object NO_HORASNT: TZetaDBTextBox
        Left = 223
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORASNT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORASNT'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label39: TLabel
        Left = 119
        Top = 60
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima dominical total:'
      end
      object NO_HORAPDT: TZetaDBTextBox
        Left = 223
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAPDT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAPDT'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object TabSheet4: TcxTabSheet
      Caption = 'Turno'
      object Label1: TLabel
        Left = 52
        Top = 4
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_TURNO: TZetaDBTextBox
        Left = 86
        Top = 2
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'CB_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TURNO'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 57
        Top = 23
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as:'
      end
      object Label3: TLabel
        Left = 42
        Top = 43
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Jornada:'
      end
      object NO_D_TURNO: TZetaDBTextBox
        Left = 86
        Top = 21
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_D_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_D_TURNO'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_JORNADA: TZetaDBTextBox
        Left = 86
        Top = 41
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_JORNADA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_JORNADA'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object TURNO: TZetaDBTextBox
        Left = 153
        Top = 2
        Width = 217
        Height = 17
        AutoSize = False
        Caption = 'TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TURNO'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object bbMostrarCalendario: TcxButton
        Left = 373
        Top = 0
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = bbMostrarCalendarioClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
      end
    end
    object TabSheet3: TcxTabSheet
      Caption = 'Generales'
      object Label34: TLabel
        Left = 34
        Top = 44
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 81
        Top = 42
        Width = 138
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label35: TLabel
        Left = 26
        Top = 62
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_USER_RJ: TZetaDBTextBox
        Left = 81
        Top = 60
        Width = 138
        Height = 17
        AutoSize = False
        Caption = 'NO_USER_RJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_USER_RJ'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_OBSERVA: TZetaDBTextBox
        Left = 82
        Top = 24
        Width = 279
        Height = 17
        AutoSize = False
        Caption = 'NO_OBSERVA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_OBSERVA'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label16: TLabel
        Left = 4
        Top = 26
        Width = 74
        Height = 13
        Caption = 'Observaciones:'
      end
      object Label33: TLabel
        Left = 45
        Top = 8
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object STATUSNOM: TZetaDBTextBox
        Left = 82
        Top = 6
        Width = 137
        Height = 17
        AutoSize = False
        Caption = 'STATUSNOM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_STATUS'
        DataSource = dsNomina
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  object PanelEmpleado: TPanel [5]
    Left = 0
    Top = 47
    Width = 599
    Height = 30
    Align = alTop
    TabOrder = 3
    object EmpleadoLbl: TLabel
      Left = 13
      Top = 8
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = '&Empleado:'
      FocusControl = CB_CODIGO
    end
    object CB_CODIGO: TZetaKeyLookup_DevEx
      Left = 66
      Top = 5
      Width = 425
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      OnValidKey = CB_CODIGOValidKey
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 556
    Top = 153
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Enabled = False
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Enabled = False
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Enabled = False
      Visible = ivNever
    end
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Enabled = False
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Enabled = False
      Visible = ivNever
    end
  end
  object dsNomina: TDataSource
    OnDataChange = dsNominaDataChange
    Left = 544
    Top = 155
  end
end
