unit FNomExcepGlobales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxTextEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TNomExcepGlobales_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  NomExcepGlobales_DevEx: TNomExcepGlobales_DevEx;

implementation

uses DNomina, ZetaCommonLists,ZetaCommonClasses, dCatalogos;

{$R *.DFM}

{ TNomExcepGlobales }

procedure TNomExcepGlobales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo2 := stPeriodo;
     HelpContext:= H30323_Excepciones_globales;
end;

procedure TNomExcepGlobales_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     with dmNomina do
     begin
          cdsExcepGlobales.Conectar;
          DataSource.DataSet:= cdsExcepGlobales;
     end;
end;

procedure TNomExcepGlobales_DevEx.Refresh;
begin
     with dmNomina do
     begin
          cdsExcepGlobales.Refrescar;
     end;
end;

procedure TNomExcepGlobales_DevEx.Agregar;
begin
     dmNomina.cdsExcepGlobales.Agregar;
end;

procedure TNomExcepGlobales_DevEx.Borrar;
begin
     dmNomina.cdsExcepGlobales.Borrar;
end;

procedure TNomExcepGlobales_DevEx.Modificar;
begin
     dmNomina.cdsExcepGlobales.Modificar;
end;

function TNomExcepGlobales_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Agregar' );
     end;
end;

function TNomExcepGlobales_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Borrar' );
     end;
end;

function TNomExcepGlobales_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar' );
     end;
end;

procedure TNomExcepGlobales_DevEx.FormShow(Sender: TObject);
begin
   CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'', SkCount );
  inherited;
    ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := true;
    ZetaDBGridDBTableView.OptionsView.GroupByBox := true;

end;

end.
