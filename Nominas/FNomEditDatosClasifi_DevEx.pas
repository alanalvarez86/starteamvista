unit FNomEditDatosClasifi_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, StdCtrls, ZetaFecha,
  ZetaNumero, ZetaKeyCombo, Mask, ZetaDBTextBox, ComCtrls,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxContainer, cxEdit,
  cxGroupBox, ZetaKeyLookup_DevEx, cxPC, cxCheckBox, dxBarBuiltInMenu;

type
  TNomEditDatosClasifi_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    ClasificaTab: TcxTabSheet;
    Panel2: TPanel;
    Label5: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    CB_ZONA_GE: TDBComboBox;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_PATRON: TZetaDBKeyLookup_DevEx;
    NO_FUERA: TDBCheckBox;
    Panel1: TPanel;
    Label4: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label22: TLabel;
    Label33: TLabel;
    Label7: TLabel;
    NO_OBSERVA: TDBEdit;
    NivelesTab: TcxTabSheet;
    CB_NIVEL9Lbl: TLabel;
    CB_NIVEL8Lbl: TLabel;
    CB_NIVEL7Lbl: TLabel;
    CB_NIVEL6Lbl: TLabel;
    CB_NIVEL5Lbl: TLabel;
    CB_NIVEL4Lbl: TLabel;
    CB_NIVEL3Lbl: TLabel;
    CB_NIVEL2Lbl: TLabel;
    CB_NIVEL1Lbl: TLabel;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    PagosFolios: TcxTabSheet;
    GroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    NO_FOLIO_1: TZetaDBNumero;
    NO_FOLIO_2: TZetaDBNumero;
    NO_FOLIO_3: TZetaDBNumero;
    NO_FOLIO_4: TZetaDBNumero;
    NO_FOLIO_5: TZetaDBNumero;
    GroupBox2: TcxGroupBox;
    Label13: TLabel;
    CBPagado: TcxCheckBox;
    NO_FEC_PAG: TZetaDBFecha;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    NO_USR_PAG: TZetaDBTextBox;
    NO_STATUS: TZetaDBTextBox;
    NO_LIQUIDA: TZetaDBTextBox;
    bbMostrarCalendario: TcxButton;
    ChkDepositoBanca: TCheckBox;
    CB_NIVEL10Lbl: TLabel;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11Lbl: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12Lbl: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    Label14: TLabel;
    NO_PAGO: TZetaDBKeyCombo;
    Label15: TLabel;
    CB_TABLASS: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure CBPagadoClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure ChkDepositoBancaClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
    procedure FormShow(Sender: TObject);
  private
    FConnect : Boolean;
    procedure PonerModoEdicion;
    procedure SetCamposNivel;
    procedure SetControlsCheckBoxes;
    {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$IFEND}
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  NomEditDatosClasifi_DevEx: TNomEditDatosClasifi_DevEx;

implementation

uses DTablas,
     DCatalogos,
     DGlobal,
     DSistema,
     DNomina,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZGlobalTress,
     ZAccesosTress,
     DCliente;

{$R *.DFM}

{ TNomEditDatosClasifi }

procedure TNomEditDatosClasifi_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     {$endif}
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     HelpContext:= H30315_Clasificacion;
     IndexDerechos := D_NOM_DATOS_CLASIFI;
     SetCamposNivel;

     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_PUESTO.LookupDataset := dmCatalogos.cdsPuestos;
     CB_CLASIFI.LookupDataset := dmCatalogos.cdsClasifi;
     CB_PATRON.LookupDataset := dmCatalogos.cdsRPatron;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     {$ifdef ACS}{OP: 5.Ago.08}
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
     {$endif}

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;

end;

procedure TNomEditDatosClasifi_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$IFEND}
end;

procedure TNomEditDatosClasifi_DevEx.Connect;
begin
     with dmTablas do
     begin
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}{OP: 5.Ago.08}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmCatalogos do
     begin
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
     end;
     dmSistema.cdsUsuarios.Conectar;
     with dmNomina do
     begin
          cdsDatosClasifi.Conectar;
          DataSource.DataSet:= cdsDatosClasifi;
     end;
     SetControlsCheckBoxes;
end;

procedure TNomEditDatosClasifi_DevEx.SetControlsCheckBoxes;
begin
     FConnect := TRUE;
     CBPagado.Checked := ( NO_FEC_PAG.Valor > 0 );
     with ChkDepositoBanca do
     begin
          Checked := ZetaCommonTools.StrLleno( dmNomina.cdsDatosClasifi.FieldbyName( 'CB_BAN_ELE' ).AsString );
          Enabled := ( ChkDepositoBanca.checked ) or ZetaCommonTools.StrLleno( dmCliente.cdsEmpleado.FieldbyName( 'CB_BAN_ELE' ).AsString );
     end;
     FConnect := FALSE;
end;

procedure TNomEditDatosClasifi_DevEx.PonerModoEdicion;
begin
     with dmNomina.cdsDatosClasifi do
     begin
          if NOT FConnect AND ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TNomEditDatosClasifi_DevEx.CBPagadoClick(Sender: TObject);
begin
     inherited;
     PonerModoEdicion;
     NO_FEC_PAG.Enabled := CBPagado.Checked;
     if ( NO_FEC_PAG.Enabled ) AND ( NO_FEC_PAG.Valor = 0 ) then
     begin
          NO_FEC_PAG.Valor := Date;
     end;
end;

procedure TNomEditDatosClasifi_DevEx.OK_DevExClick(Sender: TObject);
begin
     if NOT CBPagado.Checked then NO_FEC_PAG.Valor := 0;
     inherited;
end;

procedure TNomEditDatosClasifi_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     SetControlsCheckBoxes;
end;

procedure TNomEditDatosClasifi_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}{OP: 5.Ago.08}    
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

procedure TNomEditDatosClasifi_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;

procedure TNomEditDatosClasifi_DevEx.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmNomina.cdsDatosClasifi do
          begin
               bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
          end;
     end;
end;

procedure TNomEditDatosClasifi_DevEx.ChkDepositoBancaClick(Sender: TObject);
var
   sBanca: String;
begin
     inherited;
     if NOT FConnect then
     begin
          PonerModoEdicion;
          if ChkDepositoBanca.Checked then
          begin
               sBanca := dmCliente.cdsEmpleado.FieldbyName('CB_BAN_ELE').AsString
          end
          else
          begin
               sBanca := VACIO;
          end;
          dmNomina.cdsDatosClasifi.FieldByName( 'CB_BAN_ELE' ).AsString := sBanca;

     end;
end;

procedure TNomEditDatosClasifi_DevEx.SetEditarSoloActivos;
begin
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;
     CB_PATRON.EditarSoloActivos := TRUE;
     CB_TABLASS.EditarSoloActivos := TRUE;
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TNomEditDatosClasifi_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     Self.Width:= K_WIDTH_SMALLFORM;
     for I := 0 to NivelesTab.ControlCount - 1 do
     begin
          if NivelesTab.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               NivelesTab.Controls[I].Left := K_WIDTH_SMALL_LEFT - (NivelesTab.Controls[I].Width+2);
          end;
          if NivelesTab.Controls[I].ClassNameIs( 'TZetaDbKeyLookup_DevEx' ) then
          begin
               if ( NivelesTab.Controls[I].Visible ) then
               begin
                    NivelesTab.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( NivelesTab.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    NivelesTab.Controls[I].Left := K_WIDTH_SMALL_LEFT;
               end;
          end;
     end;
end;
{$IFEND}

end.


