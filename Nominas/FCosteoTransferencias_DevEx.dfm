inherited CosteoTransferencias_DevEx: TCosteoTransferencias_DevEx
  Left = 263
  Top = 153
  Caption = 'Transferencias'
  ClientHeight = 505
  ClientWidth = 979
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 979
    inherited Slider: TSplitter
      Left = 257
    end
    inherited ValorActivo1: TPanel
      Width = 241
      inherited textoValorActivo1: TLabel
        Width = 235
      end
    end
    inherited ValorActivo2: TPanel
      Left = 260
      Width = 719
      inherited textoValorActivo2: TLabel
        Width = 713
      end
    end
  end
  object pnFiltros: TPanel [1]
    Left = 0
    Top = 19
    Width = 979
    Height = 94
    Align = alTop
    TabOrder = 2
    object lblFecIni: TLabel
      Left = 50
      Top = 13
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha inicial:'
    end
    object lblFecFin: TLabel
      Left = 57
      Top = 37
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha final:'
    end
    object Label1: TLabel
      Left = 24
      Top = 109
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Transferido desde:'
    end
    object lblMaestro: TLabel
      Left = 47
      Top = 133
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Transferido a:'
    end
    object Label2: TLabel
      Left = 77
      Top = 157
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Motivo:'
    end
    object Label3: TLabel
      Left = 88
      Top = 187
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object Label4: TLabel
      Left = 72
      Top = 222
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Captura:'
    end
    object zFechaIni: TZetaFecha
      Left = 115
      Top = 8
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '27/ene/04'
      Valor = 38013.000000000000000000
      OnChange = btnFiltrarClick
    end
    object zFechaFin: TZetaFecha
      Left = 115
      Top = 32
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '27/ene/04'
      Valor = 38013.000000000000000000
      OnChange = btnFiltrarClick
    end
    object CCOrigen: TZetaKeyLookup_DevEx
      Left = 115
      Top = 105
      Width = 350
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 2
      TabStop = True
      WidthLlave = 70
    end
    object CCDestino: TZetaKeyLookup_DevEx
      Left = 115
      Top = 129
      Width = 350
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 3
      TabStop = True
      WidthLlave = 70
    end
    object btnFiltrar: TBitBtn
      Left = 478
      Top = 108
      Width = 97
      Height = 46
      Caption = 'Filtrar'
      TabOrder = 8
      OnClick = btnFiltrarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
        7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
        7799000BFB077777777777700077777777007777777777777700777777777777
        7777777777777777700077777777777770007777777777777777}
    end
    object TR_MOTIVO: TZetaKeyLookup_DevEx
      Left = 115
      Top = 153
      Width = 350
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 4
      TabStop = True
      WidthLlave = 70
    end
    object rgTipo: TRadioGroup
      Left = 115
      Top = 177
      Width = 350
      Height = 33
      Columns = 3
      Items.Strings = (
        'Todos'
        'Ordinarias'
        'Extras')
      TabOrder = 5
    end
    object cbSoloNegativos: TCheckBox
      Left = 115
      Top = 60
      Width = 246
      Height = 17
      Caption = 'Mostrar Transferencias mayores a la Asistencia'
      TabOrder = 7
      OnClick = btnFiltrarClick
    end
    object rgGlobales: TRadioGroup
      Left = 115
      Top = 212
      Width = 350
      Height = 33
      Columns = 3
      Items.Strings = (
        'Todas'
        'Individuales'
        'Globales')
      TabOrder = 6
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 113
    Width = 979
    Height = 392
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object AU_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AU_FECHA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Grouping = False
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre Completo'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Grouping = False
        Width = 64
      end
      object CC_ORIGEN: TcxGridDBColumn
        Caption = 'Origen'
        DataBinding.FieldName = 'CC_ORIGEN'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object CC_CODIGO: TcxGridDBColumn
        Caption = 'Destino'
        DataBinding.FieldName = 'CC_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object TR_HORAS: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'TR_HORAS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Options.Grouping = False
        Width = 64
      end
      object ASIS_HORAS: TcxGridDBColumn
        Caption = 'Asistencia'
        DataBinding.FieldName = 'ASIS_HORAS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Options.Grouping = False
        Width = 64
      end
      object TR_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TR_TIPO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object GROID_TR_MOTIVO: TcxGridDBColumn
        Caption = 'Motivo'
        DataBinding.FieldName = 'TR_MOTIVO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
      object TR_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'TR_STATUS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Grouping = False
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Transfiere'
        DataBinding.FieldName = 'US_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Grouping = False
        Width = 64
      end
      object TR_APRUEBA: TcxGridDBColumn
        Caption = 'Recibe'
        DataBinding.FieldName = 'TR_APRUEBA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Grouping = False
        Width = 64
      end
      object TR_GLOBAL: TcxGridDBColumn
        Caption = #191'Global?'
        DataBinding.FieldName = 'TR_GLOBAL'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 64
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 696
    Top = 5
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 1049088
  end
  inherited ActionList: TActionList
    Left = 474
    Top = 16
  end
  inherited PopupMenu1: TPopupMenu
    Left = 440
    Top = 16
  end
end
