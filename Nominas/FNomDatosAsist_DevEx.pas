unit FNomDatosAsist_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, StdCtrls, DBGrids, ExtCtrls, ComCtrls,
     ZetaDBTextBox,
     Mask, ZetaFecha, ZBaseShell, FTressShell,
     ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
     cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
     cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
     Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
     cxGrid, ZetaCXGrid, cxPCdxBarPopupMenu, cxPC, cxContainer, cxGroupBox,
     cxButtons;

type
  TNomDatosAsist_DevEx = class(TBaseGridLectura_DevEx)
    dsAusencia: TDataSource;
    PageControl1: TcxPageControl;
    TabSheet1: TcxTabSheet;
    Label17: TLabel;
    NO_DIAS: TZetaDBTextBox;
    Label18: TLabel;
    NO_DIAS_AS: TZetaDBTextBox;
    Label19: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    Label20: TLabel;
    NO_DIAS_FJ: TZetaDBTextBox;
    Label21: TLabel;
    NO_DIAS_IN: TZetaDBTextBox;
    Label22: TLabel;
    NO_DIAS_NT: TZetaDBTextBox;
    Label23: TLabel;
    NO_DIAS_SG: TZetaDBTextBox;
    Label24: TLabel;
    NO_DIAS_SU: TZetaDBTextBox;
    Label32: TLabel;
    NO_DIAS_AG: TZetaDBTextBox;
    NO_DIAS_VA: TZetaDBTextBox;
    Label31: TLabel;
    NO_DIAS_RE: TZetaDBTextBox;
    Label30: TLabel;
    NO_DIAS_AJ: TZetaDBTextBox;
    Label29: TLabel;
    NO_DIAS_OT: TZetaDBTextBox;
    Label28: TLabel;
    NO_DIAS_CG: TZetaDBTextBox;
    Label26: TLabel;
    NO_DIAS_EM: TZetaDBTextBox;
    Label27: TLabel;
    NO_DIAS_SS: TZetaDBTextBox;
    Label25: TLabel;
    TabSheet2: TcxTabSheet;
    Label4: TLabel;
    NO_HORAS: TZetaDBTextBox;
    Label10: TLabel;
    NO_HORA_PD: TZetaDBTextBox;
    NO_DES_TRA: TZetaDBTextBox;
    Label11: TLabel;
    NO_EXTRAS: TZetaDBTextBox;
    Label5: TLabel;
    Label6: TLabel;
    NO_TRIPLES: TZetaDBTextBox;
    Label12: TLabel;
    NO_FES_TRA: TZetaDBTextBox;
    NO_VAC_TRA: TZetaDBTextBox;
    Label13: TLabel;
    NO_DOBLES: TZetaDBTextBox;
    Label7: TLabel;
    Label8: TLabel;
    NO_TARDES: TZetaDBTextBox;
    Label14: TLabel;
    NO_HORA_CG: TZetaDBTextBox;
    NO_HORA_SG: TZetaDBTextBox;
    Label15: TLabel;
    NO_ADICION: TZetaDBTextBox;
    Label9: TLabel;
    Label36: TLabel;
    NO_FES_PAG: TZetaDBTextBox;
    TabSheet4: TcxTabSheet;
    Label1: TLabel;
    CB_TURNO: TZetaDBTextBox;
    Turno: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    NO_D_TURNO: TZetaDBTextBox;
    NO_JORNADA: TZetaDBTextBox;
    TabSheet3: TcxTabSheet;
    Label34: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label35: TLabel;
    NO_USER_RJ: TZetaDBTextBox;
    NO_OBSERVA: TZetaDBTextBox;
    Label16: TLabel;
    Label33: TLabel;
    STATUSNOM: TZetaDBTextBox;
    Calcular: TcxButton;
    bbMostrarCalendario: TcxButton;
    Label37: TLabel;
    NO_DIAS_SI: TZetaDBTextBox;
    Label38: TLabel;
    NO_HORAPDT: TZetaDBTextBox;
    Label39: TLabel;
    NO_HORASNT: TZetaDBTextBox;
    Label40: TLabel;
    NO_DIAS_FV: TZetaDBTextBox;
    bbtnModificarAsis: TcxButton;
    gbAutorizacion: TcxGroupBox;
    Label41: TLabel;
    NO_SUP_OK: TZetaDBTextBox;
    Label42: TLabel;
    NO_FEC_OK: TZetaDBTextBox;
    Label43: TLabel;
    NO_HOR_OK: TZetaDBTextBox;
    AU_POSICIO: TcxGridDBColumn;
    AU_FECHA: TcxGridDBColumn;
    CHECADA1: TcxGridDBColumn;
    CHECADA2: TcxGridDBColumn;
    CHECADA3: TcxGridDBColumn;
    CHECADA4: TcxGridDBColumn;
    AU_HORAS: TcxGridDBColumn;
    AU_NUM_EXT: TcxGridDBColumn;
    AU_EXTRAS: TcxGridDBColumn;
    AU_PER_CG: TcxGridDBColumn;
    AU_PER_SG: TcxGridDBColumn;
    AU_DES_TRA: TcxGridDBColumn;
    AU_TARDES: TcxGridDBColumn;
    AU_STATUS: TcxGridDBColumn;
    AU_TIPODIA: TcxGridDBColumn;
    AU_TIPO: TcxGridDBColumn;
    HO_CODIGO: TcxGridDBColumn;
    US_CODIGO_GRID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure CalcularClick(Sender: TObject);
    procedure ZetaDBGrid1TitleClick(Column: TColumn);
    procedure dsAusenciaDataChange(Sender: TObject; Field: TField);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure bbtnModificarAsisClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function NoHayDatos: Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
    procedure ApplyMinWidth; override;
  public
    { Public declarations }
  end;

var
   NomDatosAsist_DevEx: TNomDatosAsist_DevEx;

implementation

uses DNomina,
     DSistema,
     DCatalogos,
     DProcesos,
     DGlobal,
     DCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaSystemWorking,
     ZGlobalTress,
     ZAccesosMgr,
     FGridAjusteTarjetaPeriodo_DevEx,
     ZBaseGridEdicion_DevEx,
     ZAccesosTress,
     ZetaDialogo,
     dAsistencia, dTablas;

{$R *.DFM}

{ TNomDatosAsist }
const
     aTitulo: array[ False..True ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( ' Pre-N�mina No Autorizada ', ' Pre-N�mina Autorizada ' );

procedure TNomDatosAsist_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H20212_Prenomina;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     Calcular.Enabled := CheckDerecho( D_ASIS_PROC_CALC_PRENOMINA, K_DERECHO_CONSULTA );
     bbtnModificarAsis.Enabled := CheckDerecho( D_ASIS_DATOS_TARJETA_DIARIA, K_DERECHO_CAMBIO );
     //Calcular.Enabled := RevisaCualquiera( D_ASIS_PROC_CALC_PRENOMINA );
end;

procedure TNomDatosAsist_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos do
     begin
          cdsHorarios.Conectar;
          cdsTurnos.Conectar;
     end;
     with dmNomina do
     begin
          cdsDatosAsist.Conectar;
          DataSource.DataSet := cdsDatosAsist;
          dsAusencia.DataSet := cdsMovDatosAsist;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNomDatosAsist_DevEx.Refresh;
begin
     dmNomina.cdsDatosAsist.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TNomDatosAsist_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar En Esta Pantalla';
end;

function TNomDatosAsist_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En Esta Pantalla';
end;

function TNomDatosAsist_DevEx.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar La Pren�mina' );
     end;
end;

procedure TNomDatosAsist_DevEx.Modificar;
begin
    // ZetaDBGrid1.DataSource := nil;
    ZetaDBGridDBTableView.DataController.DataSource := nil;  //DevEx
     try
        dmNomina.cdsDatosAsist.Modificar;
     finally
        //ZetaDBGrid1.DataSource := dsAusencia;
        ZetaDBGridDBTableView.DataController.DataSource := dsAusencia;     //DevEx
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNomDatosAsist_DevEx.CalcularClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        InitAnimation( 'Calculando Pre-N�mina' );
        dmProcesos.CalcularNominaEmpleado( FALSE );
        Refresh;
     finally
            EndAnimation;
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

function TNomDatosAsist_DevEx.NoHayDatos: Boolean;
begin
     Result := inherited NoHayDatos;
     if not Result then
     begin
          with dsAusencia do
          begin
               if ( Dataset = nil ) then
                  Result := False
               else
                   Result := Dataset.IsEmpty;
          end;
     end;
end;

procedure TNomDatosAsist_DevEx.ZetaDBGrid1TitleClick(Column: TColumn);
begin
     { No debe hacer nada }
end;

procedure TNomDatosAsist_DevEx.dsAusenciaDataChange(Sender: TObject; Field: TField);
begin
     { No se debe llamar inherited para que no se tome otro Dataset
     { como referencia para el m�todo NoData }
     //ImgNoRecord.Visible:= NoHayDatos;   //DevEx
end;

procedure TNomDatosAsist_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

procedure TNomDatosAsist_DevEx.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     inherited;
     with Datasource do
     begin
          if ( Dataset <> nil ) then
          begin
               with DataSet do
               begin
                    bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
                    gbAutorizacion.Caption := aTitulo[ ( FieldByName( 'NO_SUP_OK' ).AsInteger <> 0 ) ];
               end;
          end;
     end;
end;

{
procedure TNomDatosAsist.GetFechaLimite;
begin
     Global.Conectar;
     FechaLimite.Caption := FechaCorta( Global.GetGlobalDate( K_GLOBAL_FECHA_LIMITE ) );
end;
}

procedure TNomDatosAsist_DevEx.bbtnModificarAsisClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ValidaTarjetaStatusDlg( dmCliente.GetDatosPeriodoActivo.Status) then
     begin
          with dmAsistencia.cdsGridTarjetasPeriodo do
          begin
               dmTablas.cdsIncidencias.Conectar;
               Refrescar;

               if not IsEmpty then
                  ZBaseGridEdicion_DevEx.ShowGridEdicion( GridAjusteTarjetasPeriodo_DevEx, TGridAjusteTarjetasPeriodo_DevEx,False )
               else
                   ZInformation('Pren�mina','La Lista de Tarjetas del Per�odo Esta Vac�a ',0);
          end;

     end;
end;

procedure TNomDatosAsist_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
end;

procedure TNomDatosAsist_DevEx.ApplyMinWidth;
var
   i: Integer;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption);
          end;
     end;

end;

end.


