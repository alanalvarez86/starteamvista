inherited SimulaMontosGlobal_DevEx: TSimulaMontosGlobal_DevEx
  Left = 304
  Top = 234
  Caption = 'Simulaci'#243'n de Finiquitos Global'
  ClientHeight = 470
  ClientWidth = 1101
  OnShow = FormShow
  ExplicitWidth = 1101
  ExplicitHeight = 470
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1101
    TabOrder = 3
    ExplicitWidth = 1101
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 842
      ExplicitWidth = 842
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 836
        ExplicitLeft = 3
      end
    end
  end
  object Panel: TPanel [1]
    Left = 0
    Top = 19
    Width = 1101
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 35
      Top = 11
      Width = 103
      Height = 13
      Caption = 'Mostrar Simulaciones:'
    end
    object FiltroStatus: TZetaKeyCombo
      Left = 141
      Top = 9
      Width = 145
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      OnChange = FiltroStatusChange
      ListaFija = lfStatusSimFiniquitos
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  object PageControl1: TcxPageControl [2]
    Left = 0
    Top = 60
    Width = 1101
    Height = 366
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = TabSheet2
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    ClientRectBottom = 345
    ClientRectLeft = 1
    ClientRectRight = 1100
    ClientRectTop = 1
    object TabSheet1: TcxTabSheet
      Caption = 'Empleados'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ZetaCXGrid1: TZetaCXGrid
        Left = 0
        Top = 0
        Width = 1099
        Height = 344
        Align = alClient
        TabOrder = 0
        object ZetaCXGrid1DBTableView: TcxGridDBTableView
          OnDblClick = ZetaDBGrid1DblClick
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          FilterBox.Visible = fvNever
          DataController.DataSource = DataSource
          DataController.Filter.OnGetValueList = ZetaCXGrid1DBTableViewDataControllerFilterGetValueList
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.Summary.OnAfterSummary = ZetaCXGrid1DBTableViewDataControllerSummaryAfterSummary
          DataController.OnSortingChanged = ZetaCXGrid1DBTableViewDataControllerSortingChanged
          OptionsBehavior.FocusCellOnTab = True
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideSelection = True
          OptionsView.Indicator = True
          OnColumnHeaderClick = ZetaCXGrid1DBTableViewColumnHeaderClick
          object CB_CODIGO: TcxGridDBColumn
            Caption = 'Empleado'
            DataBinding.FieldName = 'CB_CODIGO'
            Options.Grouping = False
            Width = 64
          end
          object PRETTYNAME: TcxGridDBColumn
            Caption = 'Nombre'
            DataBinding.FieldName = 'PRETTYNAME'
            Options.Grouping = False
            Width = 64
          end
          object CB_ACTIVO: TcxGridDBColumn
            Caption = 'Activo'
            DataBinding.FieldName = 'CB_ACTIVO'
            Width = 64
          end
          object CB_FEC_ANT: TcxGridDBColumn
            Caption = 'Antig'#252'edad'
            DataBinding.FieldName = 'CB_FEC_ANT'
            Options.Grouping = False
            Width = 64
          end
          object CB_SALARIO: TcxGridDBColumn
            Caption = 'Salario Diario'
            DataBinding.FieldName = 'CB_SALARIO'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.ReadOnly = False
            Options.Grouping = False
            Width = 64
          end
          object NO_PERCEPC_GRID: TcxGridDBColumn
            Caption = 'Percepciones'
            DataBinding.FieldName = 'NO_PERCEPC'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.ReadOnly = False
            Options.Grouping = False
            Width = 64
          end
          object NO_DEDUCCI_GRID: TcxGridDBColumn
            Caption = 'Deducciones'
            DataBinding.FieldName = 'NO_DEDUCCI'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.ReadOnly = False
            Options.Grouping = False
            Width = 64
          end
          object NO_NETO_GRID: TcxGridDBColumn
            Caption = 'Monto'
            DataBinding.FieldName = 'NO_NETO'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.ReadOnly = False
            Options.Grouping = False
            Width = 64
          end
          object TP_NOMBRE: TcxGridDBColumn
            Caption = 'N'#243'mina'
            DataBinding.FieldName = 'TP_NOMBRE'
            Width = 64
          end
          object NO_STATUS: TcxGridDBColumn
            Caption = 'Status'
            DataBinding.FieldName = 'NO_STATUS'
            Width = 64
          end
          object NO_APROBA: TcxGridDBColumn
            Caption = 'Simulaci'#243'n'
            DataBinding.FieldName = 'NO_APROBA'
            Options.Grouping = False
            Width = 64
          end
          object NO_FEC_LIQ: TcxGridDBColumn
            Caption = 'Fecha Liquidaci'#243'n'
            DataBinding.FieldName = 'NO_FEC_LIQ'
            Options.Grouping = False
            Width = 99
          end
        end
        object ZetaCXGrid1Level1: TcxGridLevel
          GridView = ZetaCXGrid1DBTableView
        end
      end
    end
    object TabSheet2: TcxTabSheet
      Caption = 'Totales por Concepto'
      ImageIndex = 1
      object Splitter1: TSplitter
        Left = 0
        Top = 142
        Width = 1099
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 1093
      end
      object PageControl2: TcxPageControl
        Left = 0
        Top = 0
        Width = 1099
        Height = 142
        Align = alTop
        TabOrder = 0
        Properties.ActivePage = TabSheet3
        Properties.CustomButtons.Buttons = <>
        Properties.TabPosition = tpBottom
        ClientRectBottom = 121
        ClientRectLeft = 1
        ClientRectRight = 1098
        ClientRectTop = 1
        object TabSheet3: TcxTabSheet
          Caption = 'Totales'
          object GroupBox4: TcxGroupBox
            Left = 0
            Top = 0
            Align = alLeft
            Caption = ' Totales '
            TabOrder = 0
            Height = 120
            Width = 265
            object Label2: TLabel
              Left = 16
              Top = 20
              Width = 68
              Height = 13
              Alignment = taRightJustify
              Caption = 'Percepciones:'
            end
            object NO_PERCEPC: TZetaDBTextBox
              Left = 87
              Top = 18
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_PERCEPC'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'PERCEPCIONES'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object Label3: TLabel
              Left = 18
              Top = 39
              Width = 66
              Height = 13
              Alignment = taRightJustify
              Caption = 'Deducciones:'
            end
            object NO_DEDUCCI: TZetaDBTextBox
              Left = 87
              Top = 37
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_DEDUCCI'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'DEDUCCIONES'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object NO_NETO: TZetaDBTextBox
              Left = 87
              Top = 56
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_NETO'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'NETO'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object Label4: TLabel
              Left = 18
              Top = 58
              Width = 66
              Height = 13
              Alignment = taRightJustify
              Caption = 'Neto a Pagar:'
            end
          end
        end
        object TabSheet4: TcxTabSheet
          Caption = 'Impuestos'
          object GroupBox1: TcxGroupBox
            Left = 261
            Top = 0
            Align = alLeft
            Caption = ' Percepciones '
            TabOrder = 0
            Height = 120
            Width = 236
            object Label6: TLabel
              Left = 77
              Top = 22
              Width = 54
              Height = 13
              Alignment = taRightJustify
              Caption = 'Mensuales:'
            end
            object NO_PER_MEN: TZetaDBTextBox
              Left = 134
              Top = 20
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_PER_MEN'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'PER_MEN_SUM'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object Label9: TLabel
              Left = 15
              Top = 40
              Width = 116
              Height = 13
              Alignment = taRightJustify
              Caption = 'Con Impuesto Individual:'
            end
            object NO_PER_CAL: TZetaDBTextBox
              Left = 134
              Top = 38
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_PER_CAL'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'PER_CAL_SUM'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object Label5: TLabel
              Left = 40
              Top = 59
              Width = 91
              Height = 13
              Alignment = taRightJustify
              Caption = 'Total Prestaciones:'
            end
            object NO_TOT_PRE: TZetaDBTextBox
              Left = 134
              Top = 57
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_TOT_PRE'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'TOT_PRE_SUM'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object Label12: TLabel
              Left = 68
              Top = 78
              Width = 63
              Height = 13
              Alignment = taRightJustify
              Caption = 'Total de ISN:'
            end
            object PER_ISN_SUM: TZetaDBTextBox
              Left = 134
              Top = 76
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'PER_ISN_SUM'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'PER_ISN_SUM'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
          end
          object GroupBox2: TcxGroupBox
            Left = 0
            Top = 0
            Align = alLeft
            Caption = ' Impuestos '
            TabOrder = 1
            Height = 120
            Width = 261
            object Label7: TLabel
              Left = 31
              Top = 38
              Width = 117
              Height = 13
              Alignment = taRightJustify
              Caption = 'Exento ISPT Mensuales:'
            end
            object NO_X_ISPT: TZetaDBTextBox
              Left = 151
              Top = 18
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_X_ISPT'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'ISPT_SUM'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object Label10: TLabel
              Left = 62
              Top = 74
              Width = 86
              Height = 13
              Alignment = taRightJustify
              Caption = 'ISPT Individuales:'
            end
            object NO_X_CAL: TZetaDBTextBox
              Left = 151
              Top = 54
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_X_CAL'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'CAL_SUM'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object Label8: TLabel
              Left = 26
              Top = 56
              Width = 122
              Height = 13
              Alignment = taRightJustify
              Caption = 'Exento ISPT Individuales:'
            end
            object NO_X_MENS: TZetaDBTextBox
              Left = 151
              Top = 36
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_X_MENS'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'MENS_SUM'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
            object Label11: TLabel
              Left = 85
              Top = 20
              Width = 63
              Height = 13
              Alignment = taRightJustify
              Caption = 'Total Exento:'
            end
            object NO_IMP_CAL: TZetaDBTextBox
              Left = 151
              Top = 72
              Width = 85
              Height = 17
              Alignment = taRightJustify
              AutoSize = False
              Caption = 'NO_IMP_CAL'
              ShowAccelChar = False
              Brush.Color = clBtnFace
              Border = True
              DataField = 'IMP_CAL_SUM'
              DataSource = dsMontos
              FormatFloat = '%14.2n'
              FormatCurrency = '%m'
            end
          end
        end
        object TabDias: TcxTabSheet
          Caption = 'D'#237'as'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label17: TLabel
            Left = 27
            Top = 12
            Width = 50
            Height = 13
            Alignment = taRightJustify
            Caption = 'Ordinarios:'
          end
          object Label18: TLabel
            Left = 26
            Top = 30
            Width = 51
            Height = 13
            Alignment = taRightJustify
            Caption = 'Asistencia:'
          end
          object Label22: TLabel
            Left = 4
            Top = 48
            Width = 73
            Height = 13
            Alignment = taRightJustify
            Caption = 'No Trabajados:'
          end
          object Label24: TLabel
            Left = 19
            Top = 66
            Width = 58
            Height = 13
            Alignment = taRightJustify
            Caption = 'Suspensi'#243'n:'
          end
          object NO_DIAS: TZetaDBTextBox
            Left = 80
            Top = 10
            Width = 58
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_AS: TZetaDBTextBox
            Left = 80
            Top = 28
            Width = 58
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_AS'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_AS_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_NT: TZetaDBTextBox
            Left = 80
            Top = 46
            Width = 58
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_NT'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_NT_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_SU: TZetaDBTextBox
            Left = 80
            Top = 64
            Width = 58
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_SU'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_SU_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label15: TLabel
            Left = 174
            Top = 12
            Width = 62
            Height = 13
            Alignment = taRightJustify
            Caption = 'Incapacidad:'
          end
          object Label23: TLabel
            Left = 149
            Top = 30
            Width = 87
            Height = 13
            Alignment = taRightJustify
            Caption = 'Permiso Sin Goce:'
          end
          object Label27: TLabel
            Left = 145
            Top = 48
            Width = 91
            Height = 13
            Alignment = taRightJustify
            Caption = 'Permiso Con Goce:'
          end
          object Label28: TLabel
            Left = 163
            Top = 66
            Width = 73
            Height = 13
            Alignment = taRightJustify
            Caption = 'Otros Permisos:'
          end
          object NO_DIAS_IN: TZetaDBTextBox
            Left = 240
            Top = 10
            Width = 58
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_IN'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_INI_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_SG: TZetaDBTextBox
            Left = 240
            Top = 28
            Width = 58
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_SG'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_SG_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_CG: TZetaDBTextBox
            Left = 240
            Top = 46
            Width = 58
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_CG'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_CG_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_OT: TZetaDBTextBox
            Left = 240
            Top = 64
            Width = 58
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_OT'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_OT_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label29: TLabel
            Left = 321
            Top = 12
            Width = 32
            Height = 13
            Alignment = taRightJustify
            Caption = 'Ajuste:'
          end
          object Label30: TLabel
            Left = 307
            Top = 30
            Width = 46
            Height = 13
            Alignment = taRightJustify
            Caption = 'Retardos:'
          end
          object Label25: TLabel
            Left = 324
            Top = 48
            Width = 29
            Height = 13
            Alignment = taRightJustify
            Caption = 'IMSS:'
          end
          object Label26: TLabel
            Left = 321
            Top = 66
            Width = 32
            Height = 13
            Alignment = taRightJustify
            Caption = 'E Y M:'
          end
          object NO_DIAS_AJ: TZetaDBTextBox
            Left = 358
            Top = 10
            Width = 59
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_AJ'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_AJ_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_RE: TZetaDBTextBox
            Left = 358
            Top = 28
            Width = 59
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_RE'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_RE_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_SS: TZetaDBTextBox
            Left = 358
            Top = 46
            Width = 59
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_SS'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_SS_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_EM: TZetaDBTextBox
            Left = 358
            Top = 64
            Width = 59
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_EM'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_EM_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label16: TLabel
            Left = 436
            Top = 12
            Width = 95
            Height = 13
            Alignment = taRightJustify
            Caption = 'Faltas Injustificadas:'
          end
          object Label31: TLabel
            Left = 442
            Top = 30
            Width = 89
            Height = 13
            Alignment = taRightJustify
            Caption = 'Faltas Justificadas:'
          end
          object Label38: TLabel
            Left = 423
            Top = 48
            Width = 108
            Height = 13
            Alignment = taRightJustify
            Caption = 'Faltas por Vacaciones:'
          end
          object Label32: TLabel
            Left = 432
            Top = 66
            Width = 99
            Height = 13
            Alignment = taRightJustify
            Caption = 'Vacaciones a Pagar:'
          end
          object Label34: TLabel
            Left = 441
            Top = 84
            Width = 90
            Height = 13
            Alignment = taRightJustify
            Caption = 'Aguinaldo a Pagar:'
          end
          object NO_DIAS_FI: TZetaDBTextBox
            Left = 538
            Top = 10
            Width = 63
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_FI'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_FI_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_FJ: TZetaDBTextBox
            Left = 538
            Top = 28
            Width = 63
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_FJ'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_FJ_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_FV: TZetaDBTextBox
            Left = 538
            Top = 46
            Width = 63
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_FV'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_FV_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_VA: TZetaDBTextBox
            Left = 538
            Top = 64
            Width = 63
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_VA'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_VA_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DIAS_AG: TZetaDBTextBox
            Left = 538
            Top = 82
            Width = 63
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DIAS_AG'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DIAS_AG_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
        object TabHoras: TcxTabSheet
          Caption = 'Horas'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label35: TLabel
            Left = 0
            Top = 12
            Width = 50
            Height = 13
            Alignment = taRightJustify
            Caption = 'Ordinarias:'
          end
          object NO_HORAS: TZetaDBTextBox
            Left = 54
            Top = 10
            Width = 53
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_HORAS'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'HORAS_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label36: TLabel
            Left = 111
            Top = 48
            Width = 78
            Height = 13
            Alignment = taRightJustify
            Caption = 'Prima Dominical:'
          end
          object NO_Hora_PD: TZetaDBTextBox
            Left = 194
            Top = 46
            Width = 53
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_Hora_PD'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'HORA_PD_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_DES_TRA: TZetaDBTextBox
            Left = 373
            Top = 10
            Width = 55
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DES_TRA'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DES_TRA_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label37: TLabel
            Left = 265
            Top = 12
            Width = 102
            Height = 13
            Alignment = taRightJustify
            Caption = 'Descanso Trabajado:'
          end
          object NO_EXTRAS: TZetaDBTextBox
            Left = 54
            Top = 28
            Width = 53
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_EXTRAS'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'EXTRAS_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label39: TLabel
            Left = 18
            Top = 30
            Width = 32
            Height = 13
            Alignment = taRightJustify
            Caption = 'Extras:'
          end
          object Label40: TLabel
            Left = 14
            Top = 48
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Dobles:'
          end
          object NO_TRIPLES: TZetaDBTextBox
            Left = 54
            Top = 64
            Width = 53
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_TRIPLES'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'TRIPLES_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label41: TLabel
            Left = 279
            Top = 30
            Width = 88
            Height = 13
            Alignment = taRightJustify
            Caption = 'Festivo Trabajado:'
          end
          object NO_FES_TRA: TZetaDBTextBox
            Left = 373
            Top = 28
            Width = 55
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_FES_TRA'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'FES_TRA_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_VAC_TRA: TZetaDBTextBox
            Left = 373
            Top = 46
            Width = 55
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_VAC_TRA'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'VAC_TRA_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label42: TLabel
            Left = 252
            Top = 48
            Width = 115
            Height = 13
            Alignment = taRightJustify
            Caption = 'Vacaciones Trabajadas:'
          end
          object NO_DOBLES: TZetaDBTextBox
            Left = 54
            Top = 46
            Width = 53
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_DOBLES'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'DOBLES_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label43: TLabel
            Left = 14
            Top = 66
            Width = 34
            Height = 13
            Alignment = taRightJustify
            Caption = 'Triples:'
          end
          object Label44: TLabel
            Left = 153
            Top = 12
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tardes:'
          end
          object NO_TARDES: TZetaDBTextBox
            Left = 194
            Top = 10
            Width = 53
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_TARDES'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'TARDES_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label45: TLabel
            Left = 431
            Top = 12
            Width = 91
            Height = 13
            Alignment = taRightJustify
            Caption = 'Permiso Con Goce:'
          end
          object NO_HORA_CG: TZetaDBTextBox
            Left = 528
            Top = 10
            Width = 55
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_HORA_CG'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'HORA_CG_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object NO_HORA_SG: TZetaDBTextBox
            Left = 528
            Top = 28
            Width = 55
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_HORA_SG'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'HORA_SG_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label46: TLabel
            Left = 435
            Top = 30
            Width = 87
            Height = 13
            Alignment = taRightJustify
            Caption = 'Permiso Sin Goce:'
          end
          object NO_ADICION: TZetaDBTextBox
            Left = 194
            Top = 28
            Width = 53
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_ADICION'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'ADICION_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label47: TLabel
            Left = 132
            Top = 30
            Width = 57
            Height = 13
            Alignment = taRightJustify
            Caption = 'Adicionales:'
          end
          object Label48: TLabel
            Left = 445
            Top = 48
            Width = 77
            Height = 13
            Alignment = taRightJustify
            Caption = 'Festivo Pagado:'
          end
          object NO_FES_PAG: TZetaDBTextBox
            Left = 528
            Top = 46
            Width = 55
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'NO_FES_PAG'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'FES_PAG_SUM'
            DataSource = dsMontos
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
      end
      object ZetaCXGrid2: TZetaCXGrid
        Left = 0
        Top = 148
        Width = 1099
        Height = 196
        Align = alClient
        TabOrder = 1
        object ZetaCXGrid2DBTableView: TcxGridDBTableView
          OnDblClick = ZetaDBGrid1DblClick
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          FilterBox.Visible = fvNever
          DataController.DataSource = dsMovimientos
          DataController.Filter.OnGetValueList = ZetaCXGrid2DBTableViewDataControllerFilterGetValueList
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.Summary.OnAfterSummary = ZetaCXGrid2DBTableViewDataControllerSummaryAfterSummary
          DataController.OnSortingChanged = ZetaCXGrid2DBTableViewDataControllerSortingChanged
          OptionsBehavior.FocusCellOnTab = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideSelection = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          OnColumnHeaderClick = ZetaCXGrid2DBTableViewColumnHeaderClick
          object co_numero: TcxGridDBColumn
            Caption = 'Concepto'
            DataBinding.FieldName = 'co_numero'
            Width = 64
          end
          object CO_DESCRIP: TcxGridDBColumn
            Caption = 'Descripci'#243'n'
            DataBinding.FieldName = 'CO_DESCRIP'
            Width = 64
          end
          object PERCEPCIONES: TcxGridDBColumn
            Caption = 'Percepciones'
            DataBinding.FieldName = 'PERCEPCIONES'
            Width = 64
          end
          object DEDUCCIONES: TcxGridDBColumn
            Caption = 'Deducciones'
            DataBinding.FieldName = 'DEDUCCIONES'
            Width = 64
          end
          object CO_TIPO: TcxGridDBColumn
            Caption = 'Tipo'
            DataBinding.FieldName = 'CO_TIPO'
            Width = 64
          end
          object IMPUESTOS_ISPT: TcxGridDBColumn
            Caption = 'Exento'
            DataBinding.FieldName = 'IMPUESTOS_ISPT'
          end
          object IMPUESTOS_CAL: TcxGridDBColumn
            Caption = 'ISPT Individual'
            DataBinding.FieldName = 'IMPUESTOS_CAL'
            Width = 64
          end
        end
        object ZetaCXGrid2Level1: TcxGridLevel
          GridView = ZetaCXGrid2DBTableView
        end
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 426
    Width = 1101
    Height = 44
    Align = alBottom
    TabOrder = 2
    object BtnLiquidacion: TcxButton
      Left = 8
      Top = 8
      Width = 162
      Height = 26
      Hint = 'Liquidaci'#243'n Global'
      Caption = 'Liquidaci'#243'n Global'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFD9F5E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFE9F9F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFE
        FCFFBAEDD5FF9AE5C1FF9AE5C1FFBAEDD5FFF7FDFAFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFDBF6E9FFFFFFFFFFFFFFFFFFFFFFFFFFE1F7ECFF71DA
        A7FFA5E8C8FFC8F1DDFFC8F1DDFFA5E8C8FF6EDAA6FFD9F5E7FFFFFFFFFFFFFF
        FFFFFFFFFFFFF1FBF7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFD0F3E2FFFFFFFFFFFFFFFFFFF7FDFAFF71DAA7FFDEF7
        EBFFFAFEFCFFA8E9C9FF97E4BFFFFAFEFCFFE9F9F1FF71DAA7FFEFFBF5FFFFFF
        FFFFFFFFFFFFE6F9F0FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFBDEED6FFFFFFFFFFFFFFFFFFADEACCFFB2EBD0FFFFFF
        FFFF9AE5C1FF6BD9A4FF81DFB1FF81DFB1FFFFFFFFFFC8F1DDFFA0E6C4FFFFFF
        FFFFFFFFFFFFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFB2EBD0FFFFFFFFFFFFFFFFFF81DFB1FFE9F9F1FFFFFF
        FFFFD9F5E7FFCBF2DFFF97E4BFFF58D498FFFFFFFFFFFAFEFCFF76DCABFFFFFF
        FFFFFFFFFFFFC8F1DDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFA8E9C9FFFFFFFFFFFFFFFFFF6EDAA6FFF7FDFAFFFFFF
        FFFFE1F7ECFF6EDAA6FF69D8A2FFC5F0DBFFFFFFFFFFFFFFFFFF76DCABFFFFFF
        FFFFFFFFFFFFBDEED6FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF92E3BCFFFFFFFFFFFFFFFFFF97E4BFFFCEF2E1FFFFFF
        FFFF92E3BCFF8CE2B8FFB8EDD3FFECFAF3FFFFFFFFFFE6F9F0FF84DFB3FFFFFF
        FFFFFFFFFFFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF87E0B5FFFFFFFFFFFFFFFFFFD6F4E6FF7CDDAEFFFCFE
        FDFFB5ECD1FF66D8A1FF5ED69BFFA0E6C4FFFFFFFFFF8FE2BAFFC5F0DBFFFFFF
        FFFFFFFFFFFF9DE6C2FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFFFFFFA5E8C8FF8AE1
        B7FFF1FBF7FFD6F4E6FFC8F1DDFFF4FCF8FF9DE6C2FF95E4BDFFFFFFFFFFFFFF
        FFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF69D8A2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5F0
        DBFF74DBA9FF74DBA9FF74DBA9FF6EDAA6FFB8EDD3FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF7FDEB0FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF5ED69BFFFCFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF7FDFAFFF4FCF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF87E0B5FFA8E9C9FFA8E9C9FFCBF2DFFFF7FD
        FAFFA8E9C9FFA8E9C9FFA8E9C9FFA8E9C9FFF1FBF7FFD0F3E2FFA8E9C9FFA8E9
        C9FF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFE9F9
        F1FF50D293FF50D293FF50D293FF50D293FFD3F4E4FF92E3BCFF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFF4FC
        F8FF6EDAA6FF66D8A1FF66D8A1FF6EDAA6FFECFAF3FF92E3BCFF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF69D8A2FFDEF7
        EBFFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE4F8EEFF74DBA9FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtnLiquidacionClick
    end
    object bbAprobarFiniquito: TcxButton
      Left = 372
      Top = 8
      Width = 162
      Height = 26
      Hint = 'Aprobar Finiquitos '
      Caption = 'Aprobar Finiquitos '
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8
        A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF8CE2B8FFE9F9
        F1FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFF4FCF8FFFFFF
        FFFFADEACCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF53D395FFD6F4E6FFFFFFFFFFFFFF
        FFFFFCFEFDFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFD9F5E7FF53D395FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF79DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF5BD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
        D6FFFFFFFFFFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF50D293FF55D3
        96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF8AE1B7FFE6F9F0FF6BD9A4FF50D293FF50D293FF50D2
        93FF60D69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF53D395FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF66D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF69D8A2FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF6BD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF53D395FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF79DDACFFF7FDFAFFFFFFFFFFB8EDD3FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF71DAA7FFEFFBF5FFFFFFFFFFA0E6
        C4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFE1F7ECFFFFFF
        FFFF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF58D498FFBDEE
        D6FFFCFEFDFF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF81DFB1FF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = bbAprobarFiniquitoClick
    end
    object BtnAplicarFiniquito: TcxButton
      Left = 554
      Top = 8
      Width = 162
      Height = 26
      Hint = 'Aplicar Finiquitos a N'#243'mina'
      Caption = 'Aplicar Finiquitos a N'#243'mina'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF76DCABFFFCFEFDFFA5E8C8FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF66D8A1FFA2E7C6FFA8E9C9FFA8E9C9FFA8E9C9FFA8E9C9FFA8E9
        C9FFA8E9C9FF5ED69BFF63D79FFF9DE6C2FFDBF6E9FFFFFFFFFFEFFBF5FF9DE6
        C2FF6BD9A4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFD9F5E7FFA2E7C6FF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
        BCFF7FDEB0FF9AE5C1FFFAFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFCFEFDFFAAE9CAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF63D79FFFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FF9DE6
        C2FF6EDAA6FFFFFFFFFFFFFFFFFFFAFEFCFFDBF6E9FFFFFFFFFFEFFBF5FFE4F8
        EEFFFFFFFFFFFFFFFFFFA2E7C6FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF53D395FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF5BD5
        9AFFBAEDD5FFFFFFFFFFFFFFFFFF8AE1B7FF92E3BCFFFFFFFFFFBDEED6FF5BD5
        9AFFFAFEFCFFFFFFFFFFECFAF3FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF63D79FFFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FF95E4
        BDFF95E4BDFFDEF7EBFFB8EDD3FF50D293FF92E3BCFFFFFFFFFFBDEED6FF50D2
        93FFDEF7EBFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFFD6F4E6FFADEA
        CCFFFFFFFFFFFFFFFFFFDBF6E9FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEE
        D6FFBAEDD5FF60D69DFF71DAA7FFB5ECD1FFE6F9F0FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF7FDFAFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF92E3BCFF9DE6C2FFFCFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FC
        F8FFB2EBD0FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9F9
        F1FF63D79FFFFFFFFFFFFFFFFFFFFAFEFCFFE4F8EEFFFFFFFFFFD6F4E6FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFFCFEFDFF7FDEB0FF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF6BD9
        A4FF8CE2B8FFFFFFFFFFFFFFFFFF92E3BCFF92E3BCFFFFFFFFFFBDEED6FF50D2
        93FF74DBA9FF79DDACFF53D395FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFBAEDD5FFD6F4E6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FF9DE6
        C2FFA2E7C6FFFFFFFFFFFFFFFFFF9DE6C2FF92E3BCFFFFFFFFFFBDEED6FF7CDD
        AEFFFFFFFFFFFFFFFFFFADEACCFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF71DAA7FF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF71DA
        A7FF60D69DFFF4FCF8FFFFFFFFFFFAFEFCFFDEF7EBFFFFFFFFFFF7FDFAFFF4FC
        F8FFFFFFFFFFFCFEFDFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF79DDACFFE6F9F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF1FBF7FF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF74DBA9FFCEF2E1FFFFFFFFFFEFFBF5FF8AE1
        B7FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF6BD9A4FFE9F9F1FF95E4BDFF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtnAplicarFiniquitoClick
    end
    object bbBorrarFiniquito: TcxButton
      Left = 736
      Top = 8
      Width = 162
      Height = 26
      Hint = 'Borrar Finiquitos '
      Caption = 'Borrar Finiquitos '
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000004858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFE8EAF9FFA4AC
        E6FFC6CBEFFFFFFFFFFFFFFFFFFFD1D5F2FFA4ACE6FFDDE0F5FFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF8D97DFFF8D97DFFF8D97DFFF8D97
        DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97
        DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = bbBorrarFiniquitoClick
    end
    object bbCalcularNomina: TcxButton
      Left = 190
      Top = 8
      Width = 162
      Height = 26
      Hint = 'Calcular N'#243'mina'
      Caption = 'Calcular N'#243'mina'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF988F99FFE2DFE2FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E5E8FFA199
        A2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFC8C4C9FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5D2
        D6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFCAC6CBFFDCD9
        DDFFFFFFFFFFD3D0D4FFD2CED2FFFFFFFFFFE4E1E4FFC8C4C9FFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFACA5ACFFC8C4
        C9FFFFFFFFFFBAB4BBFFACA5ACFFFFFFFFFFC8C4C9FF9D959EFFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFB1ABB2FFCAC6
        CBFFFFFFFFFFBEB8BFFFB8B2B9FFFFFFFFFFD2CED2FFACA5ACFFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFCAC6CBFFDCD9
        DDFFFFFFFFFFD3D0D4FFD2CED2FFFFFFFFFFE4E1E4FFC8C4C9FFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFF8F7F8FFFBFB
        FBFFFFFFFFFFFAF9FAFFF8F7F8FFFFFFFFFFFBFBFBFFF6F5F6FFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFA199A2FFBCB6
        BDFFFFFFFFFFAFA9B0FFA199A2FFFFFFFFFFBEB8BFFF928993FFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFE9E7E9FFEFED
        EFFFFFFFFFFFEFEDEFFFE9E7E9FFFFFFFFFFF4F3F4FFE4E1E4FFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFF6F5F6FFF0EF
        F1FFF0EFF1FFF0EFF1FFF0EFF1FFF0EFF1FFF0EFF1FFF4F3F4FFFFFFFFFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFF8F7F8FF8F8590FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFEDEBEDFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFF0EFF1FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFF8F7F8FF8F8590FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFEDEBEDFFE2DF
        E2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFFFFFFF8F7F8FFF0EF
        F1FFF0EFF1FFF0EFF1FFF0EFF1FFF0EFF1FFF0EFF1FFF6F5F6FFFFFFFFFFD0CC
        D0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFD3D0D4FFFBFBFBFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFD9D5D9FF9B93
        9CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = bbCalcularNominaClick
    end
  end
  inherited DataSource: TDataSource
    Left = 904
    Top = 16
  end
  object dsMontos: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 872
    Top = 16
  end
  object dsMovimientos: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 840
    Top = 16
  end
end
