unit FGridSimulacionMontos_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, FGridMovMontos_DevEx, DB, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, DBCtrls, ExtCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, ZetaKeyLookup_DevEx, cxNavigator, cxDBNavigator, cxButtons;

type
  TGridSimulacionMontos_DevEx = class(TGridMovMontos_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure HabilitaControles; override;
    procedure ConectaMontos; override;
  public
    { Public declarations }
  end;

var
  GridSimulacionMontos_DevEx: TGridSimulacionMontos_DevEx;

implementation

uses dNomina,
     ZetaCommonLists;
     
{$R *.dfm}

procedure TGridSimulacionMontos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 0; // PENDIENTE
     IndexDerechos := 0; // PENDIENTE
     TipoValorActivo2 := stNinguno;
end;

procedure TGridSimulacionMontos_DevEx.ConectaMontos;
begin
     with dmNomina do
     begin
          cdsSimulaMontos.Conectar;
          DataSource.DataSet:= cdsSimulaMontos;
     end;
end;

procedure TGridSimulacionMontos_DevEx.HabilitaControles;
begin
     inherited;
     CB_CODIGO.Enabled := FALSE;  // No se puede cambiar de empleado
end;

end.
