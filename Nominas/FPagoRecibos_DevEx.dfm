inherited PagoRecibos_DevEx: TPagoRecibos_DevEx
  Caption = 'Pago de Recibos'
  ClientHeight = 160
  ClientWidth = 197
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 124
    Width = 197
    inherited OK_DevEx: TcxButton
      Left = 14
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 102
      Top = 5
      Cancel = True
    end
  end
  object RGMarcar: TcxRadioGroup [1]
    Left = 8
    Top = 32
    Caption = ' Marcar Recibos como: '
    Properties.Items = <
      item
        Caption = 'Pagados'
        Value = '0'
      end
      item
        Caption = 'No Pagados'
        Value = '1'
      end>
    ItemIndex = 0
    TabOrder = 1
    Height = 81
    Width = 185
  end
  object ValorActivo1: TPanel [2]
    Left = 0
    Top = 0
    Width = 197
    Height = 19
    Align = alTop
    Alignment = taLeftJustify
    BorderWidth = 8
    Caption = 'ValorActivo1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 144
  end
end
