inherited EmpleadoGridSelectAplicaFiniquito_DevEx: TEmpleadoGridSelectAplicaFiniquito_DevEx
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Selecciona los empleados a aplicar finiquito'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    inherited Cancelar_DevEx: TcxButton
      Cancel = True
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnHorzSizing = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 65
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Width = 247
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
