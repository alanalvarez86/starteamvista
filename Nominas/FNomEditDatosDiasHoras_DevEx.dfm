inherited NomEditDatosDiasHoras_DevEx: TNomEditDatosDiasHoras_DevEx
  Left = 208
  Top = 160
  Caption = 'Excepciones de D'#237'as / Horas'
  ClientHeight = 229
  ClientWidth = 450
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 193
    Width = 450
    TabOrder = 5
    inherited OK_DevEx: TcxButton
      Left = 286
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 365
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 450
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 124
      inherited textoValorActivo2: TLabel
        Width = 118
      end
    end
  end
  object FA_DIA_HOR: TDBRadioGroup [3]
    Left = 0
    Top = 47
    Width = 450
    Height = 40
    Align = alTop
    Caption = ' Tipo '
    Columns = 2
    DataField = 'FA_DIA_HOR'
    DataSource = DataSource
    Items.Strings = (
      'D'#237'as'
      'Horas')
    TabOrder = 1
    Values.Strings = (
      'D'
      'H')
    OnChange = FA_DIA_HORChange
  end
  object GBDias: TcxGroupBox [4]
    Left = 0
    Top = 87
    Align = alLeft
    TabOrder = 2
    Height = 106
    Width = 225
    object LMotDias: TLabel
      Left = 17
      Top = 20
      Width = 35
      Height = 13
      Caption = '&Motivo:'
      FocusControl = FA_MOTIVO1
    end
    object LFecha: TLabel
      Left = 19
      Top = 43
      Width = 33
      Height = 13
      Caption = '&Fecha:'
      FocusControl = FA_FEC_INI
    end
    object LDias: TLabel
      Left = 26
      Top = 65
      Width = 26
      Height = 13
      Caption = '&D'#237'as:'
      FocusControl = FA_DIAS
    end
    object FA_MOTIVO1: TZetaDBKeyCombo
      Left = 55
      Top = 16
      Width = 161
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
      OnChange = FA_MOTIVO1Change
      ListaFija = lfMotivoFaltaDias
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = True
      EsconderVacios = False
      DataField = 'FA_MOTIVO'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object FA_FEC_INI: TZetaDBFecha
      Left = 55
      Top = 38
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '10/Dec/97'
      Valor = 35774.000000000000000000
      DataField = 'FA_FEC_INI'
      DataSource = DataSource
    end
    object FA_DIAS: TZetaDBNumero
      Left = 55
      Top = 61
      Width = 40
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 2
      Text = '0.00'
      DataField = 'FA_DIAS'
      DataSource = DataSource
    end
  end
  object GBHoras: TcxGroupBox [5]
    Left = 225
    Top = 87
    Align = alClient
    TabOrder = 3
    Height = 106
    Width = 225
    object LMotHoras: TLabel
      Left = 17
      Top = 20
      Width = 35
      Height = 13
      Caption = 'Moti&vo:'
      FocusControl = FA_MOTIVO2
    end
    object LHoras: TLabel
      Left = 21
      Top = 65
      Width = 31
      Height = 13
      Caption = '&Horas:'
      FocusControl = FA_HORAS
    end
    object Label1: TLabel
      Left = 103
      Top = 65
      Width = 6
      Height = 13
      Caption = '='
    end
    object Label2: TLabel
      Left = 164
      Top = 65
      Width = 37
      Height = 13
      Caption = 'Min&utos'
      FocusControl = HorasMin
    end
    object Label3: TLabel
      Left = 19
      Top = 43
      Width = 33
      Height = 13
      Caption = '&Fecha:'
      FocusControl = FA_FEC_INI2
    end
    object FA_MOTIVO2: TZetaDBKeyCombo
      Left = 55
      Top = 16
      Width = 161
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
      ListaFija = lfMotivoFaltaHoras
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = True
      EsconderVacios = False
      DataField = 'FA_MOTIVO'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object FA_HORAS: TZetaDBNumero
      Left = 55
      Top = 61
      Width = 40
      Height = 21
      Mascara = mnHoras
      TabOrder = 2
      Text = '0.00'
      OnChange = FA_HORASChange
      DataField = 'FA_HORAS'
      DataSource = DataSource
    end
    object HorasMin: TZetaNumero
      Left = 119
      Top = 61
      Width = 40
      Height = 21
      Mascara = mnEmpleado
      MaxLength = 4
      TabOrder = 3
      OnEnter = HorasMinEnter
      OnExit = HorasMinExit
    end
    object FA_FEC_INI2: TZetaDBFecha
      Left = 55
      Top = 38
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '10/Dec/97'
      Valor = 35774.000000000000000000
      DataField = 'FA_FEC_INI'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 372
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
