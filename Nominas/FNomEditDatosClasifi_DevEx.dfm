inherited NomEditDatosClasifi_DevEx: TNomEditDatosClasifi_DevEx
  Left = 697
  Top = 317
  Caption = 'Clasificaci'#243'n'
  ClientHeight = 392
  ClientWidth = 576
  OnCloseQuery = nil
  ExplicitWidth = 582
  ExplicitHeight = 421
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 356
    Width = 576
    ExplicitTop = 356
    ExplicitWidth = 576
    inherited OK_DevEx: TcxButton
      Left = 412
      ExplicitLeft = 412
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 491
      ExplicitLeft = 491
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 576
    ExplicitWidth = 576
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Width = 250
      ExplicitWidth = 250
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 244
        ExplicitLeft = 164
      end
    end
  end
  object PageControl: TcxPageControl [3]
    Left = 0
    Top = 50
    Width = 576
    Height = 306
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = ClasificaTab
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 304
    ClientRectLeft = 2
    ClientRectRight = 574
    ClientRectTop = 27
    object ClasificaTab: TcxTabSheet
      Caption = 'Clasificaci'#243'n'
      object Panel2: TPanel
        Left = 0
        Top = 57
        Width = 572
        Height = 220
        Align = alClient
        TabOrder = 1
        object Label5: TLabel
          Left = 64
          Top = 10
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Zona Geogr'#225'fica:'
        end
        object Label10: TLabel
          Left = 116
          Top = 53
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Turno:'
        end
        object Label11: TLabel
          Left = 111
          Top = 75
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Puesto:'
        end
        object Label12: TLabel
          Left = 85
          Top = 97
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clasificaci'#243'n:'
        end
        object Label2: TLabel
          Left = 79
          Top = 119
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Reg. Patronal:'
        end
        object Label14: TLabel
          Left = 65
          Top = 183
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'M'#233'todo de Pago:'
        end
        object Label15: TLabel
          Left = 38
          Top = 31
          Width = 109
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tabla de Prestaciones:'
        end
        object bbMostrarCalendario: TcxButton
          Left = 454
          Top = 49
          Width = 21
          Height = 21
          Hint = 'Mostrar Calendario del Turno'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
            BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
            BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
            8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
            DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
            FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
            F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
            8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
            7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
            CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
            F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
            EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
            BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
            ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
            FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
            BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
            E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
            9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
            E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          OnClick = bbMostrarCalendarioClick
        end
        object CB_ZONA_GE: TDBComboBox
          Left = 154
          Top = 5
          Width = 145
          Height = 21
          DataField = 'CB_ZONA_GE'
          DataSource = DataSource
          Items.Strings = (
            'A'
            'B'
            'C')
          TabOrder = 0
        end
        object CB_TURNO: TZetaDBKeyLookup_DevEx
          Left = 154
          Top = 49
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsTurnos
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_TURNO'
          DataSource = DataSource
        end
        object CB_PUESTO: TZetaDBKeyLookup_DevEx
          Left = 154
          Top = 71
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsPuestos
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_PUESTO'
          DataSource = DataSource
        end
        object CB_CLASIFI: TZetaDBKeyLookup_DevEx
          Left = 154
          Top = 93
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsClasifi
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 3
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_CLASIFI'
          DataSource = DataSource
        end
        object CB_PATRON: TZetaDBKeyLookup_DevEx
          Left = 154
          Top = 115
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 4
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_PATRON'
          DataSource = DataSource
        end
        object NO_FUERA: TDBCheckBox
          Left = 70
          Top = 137
          Width = 97
          Height = 18
          Alignment = taLeftJustify
          Caption = 'Pago por Fuera:'
          DataField = 'NO_FUERA'
          DataSource = DataSource
          TabOrder = 5
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object ChkDepositoBanca: TCheckBox
          Left = 11
          Top = 159
          Width = 156
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Dep'#243'sito Banca Electr'#243'nica:'
          TabOrder = 6
          OnClick = ChkDepositoBancaClick
        end
        object NO_PAGO: TZetaDBKeyCombo
          Left = 154
          Top = 179
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 8
          ListaFija = lfMetodoPagoSAT
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'NO_PAGO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CB_TABLASS: TZetaDBKeyLookup_DevEx
          Left = 154
          Top = 27
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsSSocial
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 9
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_TABLASS'
          DataSource = DataSource
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 572
        Height = 57
        Align = alTop
        TabOrder = 0
        object Label4: TLabel
          Left = 15
          Top = 30
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Observaciones:'
        end
        object US_CODIGO: TZetaDBTextBox
          Left = 380
          Top = 8
          Width = 177
          Height = 17
          AutoSize = False
          Caption = 'US_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_CODIGO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label22: TLabel
          Left = 332
          Top = 10
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modific'#243':'
        end
        object Label33: TLabel
          Left = 56
          Top = 10
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
        end
        object Label7: TLabel
          Left = 370
          Top = 31
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo Liquidaci'#243'n:'
        end
        object NO_STATUS: TZetaDBTextBox
          Left = 93
          Top = 8
          Width = 145
          Height = 17
          AutoSize = False
          Caption = 'NO_STATUS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_STATUS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object NO_LIQUIDA: TZetaDBTextBox
          Left = 456
          Top = 29
          Width = 101
          Height = 17
          AutoSize = False
          Caption = 'NO_LIQUIDA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_LIQUIDA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object NO_OBSERVA: TDBEdit
          Left = 92
          Top = 27
          Width = 253
          Height = 21
          DataField = 'NO_OBSERVA'
          DataSource = DataSource
          TabOrder = 0
        end
      end
    end
    object NivelesTab: TcxTabSheet
      Caption = 'Niveles'
      object CB_NIVEL9Lbl: TLabel
        Left = 131
        Top = 180
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9'
      end
      object CB_NIVEL8Lbl: TLabel
        Left = 131
        Top = 158
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8'
      end
      object CB_NIVEL7Lbl: TLabel
        Left = 131
        Top = 136
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7'
      end
      object CB_NIVEL6Lbl: TLabel
        Left = 131
        Top = 114
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6'
      end
      object CB_NIVEL5Lbl: TLabel
        Left = 131
        Top = 92
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5'
      end
      object CB_NIVEL4Lbl: TLabel
        Left = 131
        Top = 70
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4'
      end
      object CB_NIVEL3Lbl: TLabel
        Left = 131
        Top = 48
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3'
      end
      object CB_NIVEL2Lbl: TLabel
        Left = 131
        Top = 26
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2'
      end
      object CB_NIVEL1Lbl: TLabel
        Left = 131
        Top = 4
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1'
      end
      object CB_NIVEL10Lbl: TLabel
        Left = 125
        Top = 202
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10'
        Visible = False
      end
      object CB_NIVEL11Lbl: TLabel
        Left = 125
        Top = 224
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11'
        Visible = False
      end
      object CB_NIVEL12Lbl: TLabel
        Left = 125
        Top = 246
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12'
        Visible = False
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 0
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 44
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 66
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 88
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 110
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 132
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 154
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 176
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 22
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 198
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 220
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 170
        Top = 242
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL12'
      end
    end
    object PagosFolios: TcxTabSheet
      Caption = 'Pagos y Folios'
      object GroupBox1: TcxGroupBox
        Left = 160
        Top = 104
        Caption = 'Folios'
        TabOrder = 1
        Height = 97
        Width = 257
        object Label1: TLabel
          Left = 17
          Top = 20
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '1:'
          Transparent = True
        end
        object Label3: TLabel
          Left = 17
          Top = 42
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '2:'
          Transparent = True
        end
        object Label6: TLabel
          Left = 17
          Top = 65
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '3:'
          Transparent = True
        end
        object Label8: TLabel
          Left = 121
          Top = 20
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '4:'
          Transparent = True
        end
        object Label9: TLabel
          Left = 121
          Top = 42
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '5:'
          Transparent = True
        end
        object NO_FOLIO_1: TZetaDBNumero
          Left = 31
          Top = 16
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 0
          Text = ''
          DataField = 'NO_FOLIO_1'
          DataSource = DataSource
        end
        object NO_FOLIO_2: TZetaDBNumero
          Left = 31
          Top = 38
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 1
          Text = ''
          DataField = 'NO_FOLIO_2'
          DataSource = DataSource
        end
        object NO_FOLIO_3: TZetaDBNumero
          Left = 31
          Top = 61
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 2
          Text = ''
          DataField = 'NO_FOLIO_3'
          DataSource = DataSource
        end
        object NO_FOLIO_4: TZetaDBNumero
          Left = 135
          Top = 16
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 3
          Text = ''
          DataField = 'NO_FOLIO_4'
          DataSource = DataSource
        end
        object NO_FOLIO_5: TZetaDBNumero
          Left = 135
          Top = 38
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 4
          Text = ''
          DataField = 'NO_FOLIO_5'
          DataSource = DataSource
        end
      end
      object GroupBox2: TcxGroupBox
        Left = 160
        Top = 5
        Caption = 'Pagos'
        TabOrder = 0
        Height = 97
        Width = 257
        object Label13: TLabel
          Left = 26
          Top = 51
          Width = 39
          Height = 13
          Caption = 'Usuario:'
          Transparent = True
        end
        object NO_USR_PAG: TZetaDBTextBox
          Left = 70
          Top = 49
          Width = 160
          Height = 17
          AutoSize = False
          Caption = 'NO_USR_PAG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_USR_PAG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CBPagado: TcxCheckBox
          Left = 5
          Top = 28
          Caption = 'Pagado'
          Properties.Alignment = taLeftJustify
          TabOrder = 0
          Transparent = True
          OnClick = CBPagadoClick
          Width = 62
        end
        object NO_FEC_PAG: TZetaDBFecha
          Left = 70
          Top = 25
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '22/Feb/99'
          Valor = 36213.000000000000000000
          DataField = 'NO_FEC_PAG'
          DataSource = DataSource
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 420
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 22020168
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF98A1E2FFF6F7FDFFB7BEEBFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFB1B8
          E9FFF6F7FDFFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF8792DEFFDFE2F6FFA1A9E5FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8
          A1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF8CE2B8FFE9F9
          F1FF53D396FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF51D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF6FDAA7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF51D395FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF59D59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF61D79FFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4ED293FF53D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF8AE1B7FFE6F9F0FF69D9A4FF4ED293FF4ED293FF4ED2
          93FF5ED69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF51D395FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF64D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF67D8A2FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF69D9A4FFEFFBF5FFFFFFFFFFDEF7EBFF51D395FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF6FDAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF56D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF81DFB1FF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF5160CFFF6B78D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6E7BD7FF7482D9FF6370D4FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFF969FE2FF6370D4FF969FE2FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4656CCFF4656CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4656CCFF4656CCFF6E7BD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF6875D6FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF5463D0FF7482D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 296
    Top = 336
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 22020280
  end
end
