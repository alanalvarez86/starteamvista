inherited FonacotDatosTot_DevEx: TFonacotDatosTot_DevEx
  Left = 349
  Top = 198
  Caption = 'Totales de Fonacot'
  ClientHeight = 861
  ClientWidth = 1097
  ExplicitWidth = 1097
  ExplicitHeight = 861
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1097
    Height = 18
    ExplicitWidth = 1097
    ExplicitHeight = 18
    inherited ImgNoRecord: TImage
      Height = 18
      ExplicitHeight = 18
    end
    inherited Slider: TSplitter
      Left = 323
      Height = 18
      ExplicitLeft = 323
      ExplicitHeight = 18
    end
    inherited ValorActivo1: TPanel
      Width = 307
      Height = 18
      ExplicitWidth = 307
      ExplicitHeight = 18
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 771
      Height = 18
      ExplicitLeft = 326
      ExplicitWidth = 771
      ExplicitHeight = 18
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 765
        ExplicitLeft = 685
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Left = 10
    Top = 696
    Width = 297
    Height = 109
    Align = alNone
    Visible = False
    ExplicitLeft = 10
    ExplicitTop = 696
    ExplicitWidth = 297
    ExplicitHeight = 109
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = nil
    end
  end
  inherited IMSSPanel: TPanel
    Top = 18
    Width = 1097
    Height = 31
    ExplicitTop = 18
    ExplicitWidth = 1097
    ExplicitHeight = 31
    inherited IMSSMesLBL: TLabel
      Left = 398
      ExplicitLeft = 398
    end
    inherited IMSSYearLBL: TLabel
      Left = 511
      ExplicitLeft = 511
    end
    inherited IMSSPatronLBL: TLabel
      Left = 18
      ExplicitLeft = 18
    end
    inherited IMSSTipoLBL: TLabel
      Left = 617
      ParentFont = False
      Visible = False
      ExplicitLeft = 617
    end
    inherited IMSSMesCB: TStateComboBox
      Left = 423
      ExplicitLeft = 423
    end
    inherited IMSSAnioCB: TStateComboBox
      Left = 536
      ExplicitLeft = 536
    end
    inherited IMSSPatronCB: TStateComboBox
      Left = 92
      Width = 289
      ParentFont = False
      ExplicitLeft = 92
      ExplicitWidth = 289
    end
    inherited IMSSTipoCB: TStateComboBox
      Left = 644
      Visible = False
      ExplicitLeft = 644
    end
    inherited IMSSYearUpDown: TUpDown
      Left = 595
      ExplicitLeft = 595
    end
  end
  object pcTotales: TcxPageControl [3]
    Left = 0
    Top = 49
    Width = 1097
    Height = 812
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = tsDetalleDiferencias
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 810
    ClientRectLeft = 2
    ClientRectRight = 1095
    ClientRectTop = 27
    object tsResumen2017: TcxTabSheet
      Caption = 'Resumen'
      ImageIndex = 2
      object cxGroupBox2: TcxGroupBox
        Left = 32
        Top = 17
        Caption = ' Resumen Mensual '
        TabOrder = 0
        Height = 361
        Width = 377
        object Label21: TLabel
          Left = 15
          Top = 34
          Width = 117
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tot. Retenci'#243'n Mensual:'
        end
        object ZetaDBTextBox1: TZetaDBTextBox
          Left = 138
          Top = 34
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox1'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_RETENC'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label23: TLabel
          Left = 73
          Top = 97
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total Ajuste:'
        end
        object ZetaDBTextBox2: TZetaDBTextBox
          Left = 138
          Top = 97
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_AJUSTE'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object ZetaDBTextBox3: TZetaDBTextBox
          Left = 138
          Top = 118
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_CUANTOS'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label26: TLabel
          Left = 91
          Top = 118
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cr'#233'ditos:'
        end
        object ZetaDBTextBox4: TZetaDBTextBox
          Left = 138
          Top = 139
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_TAJUST'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label27: TLabel
          Left = 40
          Top = 139
          Width = 92
          Height = 13
          Alignment = taRightJustify
          Caption = 'T. Pr'#233'stamo Ajuste:'
        end
        object Label28: TLabel
          Left = 59
          Top = 160
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'Incapacidades:'
        end
        object ZetaDBTextBox5: TZetaDBTextBox
          Left = 138
          Top = 160
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox5'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_INCAPA'
          DataSource = DataSourceViejo
          FormatFloat = '%14.3n'
          FormatCurrency = '%m'
        end
        object Label29: TLabel
          Left = 77
          Top = 181
          Width = 55
          Height = 13
          Caption = 'Empleados:'
        end
        object Label30: TLabel
          Left = 103
          Top = 202
          Width = 29
          Height = 13
          Caption = 'Bajas:'
        end
        object ZetaDBTextBox6: TZetaDBTextBox
          Left = 138
          Top = 181
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox6'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_EMPLEAD'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object ZetaDBTextBox7: TZetaDBTextBox
          Left = 138
          Top = 202
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox7'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_BAJAS'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label31: TLabel
          Left = 47
          Top = 223
          Width = 85
          Height = 13
          Caption = 'Status de c'#225'lculo:'
        end
        object ZetaDBTextBox8: TZetaDBTextBox
          Left = 138
          Top = 223
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox8'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_STATUS'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label32: TLabel
          Left = 11
          Top = 55
          Width = 121
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tot. Retenci'#243'n x N'#243'mina:'
        end
        object ZetaDBTextBox9: TZetaDBTextBox
          Left = 138
          Top = 55
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox9'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_NOMINA'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label33: TLabel
          Left = 96
          Top = 245
          Width = 36
          Height = 13
          Caption = 'Detalle:'
        end
        object ZetaDBTextBox10: TZetaDBTextBox
          Left = 138
          Top = 76
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ZetaDBTextBox10'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_PORCENTAJE'
          DataSource = DataSourceViejo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label36: TLabel
          Left = 31
          Top = 76
          Width = 101
          Height = 13
          Caption = '% de pago a reportar:'
        end
        object cxDBMemo1: TcxDBMemo
          Left = 138
          Top = 245
          DataBinding.DataField = 'FT_DETALLE'
          DataBinding.DataSource = DataSource
          Properties.ReadOnly = True
          Properties.ScrollBars = ssVertical
          TabOrder = 0
          Height = 81
          Width = 215
        end
      end
      object cxButton1: TcxButton
        Left = 440
        Top = 17
        Width = 145
        Height = 26
        Hint = 'Cerrar c'#225'lculo Fonacot'
        Caption = 'Cerrar c'#225'lculo Fonacot'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF6A77D6FFE2E5F7FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBEDF9FF7C87
          DAFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFE8EAF9FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7
          FDFF5C6AD2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF6D7AD6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF848FDDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCBD0F1FF98A1E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF737FD8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF6875D5FF4858CCFFC6CBEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF7C87DAFF4E5DCEFFD1D5F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF9FAFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF6D7AD6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF7F8ADBFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFEBEDF9FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7
          FDFF4E5DCEFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF707DD7FFE2E5F7FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8EAF9FF7682
          D9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF7682D9FFEBEDF9FF5F6D
          D2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FFD7DAF4FF7682D9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF7682D9FFE8EAF9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFD1D5F2FF7682D9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF707DD7FFEEEFFAFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFD7DAF4FF7682D9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5160CEFFFFFFFFFF5F6D
          D2FF4858CCFF4858CCFF4858CCFF4858CCFF5160CEFFF6F7FDFF6875D5FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFD1D5F2FFCBD0
          F1FF5160CEFF4858CCFF4858CCFF4B5BCDFFBAC0ECFFE5E7F8FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFF1F2
          FBFFE5E7F8FFACB3E8FFACB3E8FFDFE2F6FFF6F7FDFF6D7AD6FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5665
          D0FFB4BBEAFFDFE2F6FFE2E5F7FFBDC3EDFF5F6DD2FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnCierraAbreCalculoClick
      end
      object cxButton2: TcxButton
        Tag = 1
        Left = 440
        Top = 49
        Width = 145
        Height = 26
        Hint = 'Abrir c'#225'lculo Fonacot'
        Caption = 'Abrir c'#225'lculo Fonacot'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF9D959EFFE9E7E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEDEFFFA49D
          A5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFEDEBEDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F5
          F6FF928993FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9D959EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFA69FA7FFCCC8CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF9A919AFFC5C0C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE2DFE2FF8F8590FFA39BA3FFF4F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA49DA5FF8B818CFF8B818CFFC8C4C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFC2BCC2FF8B818CFF8D838EFFE6E3E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE6E3E6FFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF988F99FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA69FA7FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFDEDBDFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F3
          F4FF908791FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFDCD9DDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFD3D0D4FF9B93
          9CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCECACEFFCCC8CCFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFF4F3F4FFA8A1A9FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFD5D2D6FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFFFFFFFFF9A919AFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFCAC6CBFFD3D0D4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFFDFDFDFFA8A1A9FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8F8590FFF6F5F6FFA49DA5FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFE0DDE0FFE6E3E6FF9289
          93FF8B818CFF8B818CFF8B818CFFCCC8CCFFE6E3E6FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFF4F3F4FFF2F1
          F2FFCAC6CBFFC8C4C9FFE6E3E6FFF6F5F6FF9D959EFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFCCC8
          CCFFE9E7E9FFEBE9EBFFCECACEFF948B95FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = btnCierraAbreCalculoClick
      end
    end
    object tsResumen: TcxTabSheet
      Caption = 'Resumen'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 0
      ParentFont = False
      object GroupBox3: TcxGroupBox
        Left = 16
        Top = 17
        Caption = ' Resumen Mensual '
        TabOrder = 0
        Height = 488
        Width = 425
        object Label20: TLabel
          Left = 74
          Top = 31
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pago requerido:'
        end
        object FT_RETENC: TZetaDBTextBox
          Left = 155
          Top = 31
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_RETENC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_RETENC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label19: TLabel
          Left = 91
          Top = 78
          Width = 58
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total ajuste:'
        end
        object FT_AJUSTE: TZetaDBTextBox
          Left = 155
          Top = 78
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_AJUSTE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_AJUSTE'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object FT_CUANTOS: TZetaDBTextBox
          Left = 155
          Top = 187
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_CUANTOS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_CUANTOS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label5: TLabel
          Left = 108
          Top = 187
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cr'#233'ditos:'
        end
        object FT_TAJUST: TZetaDBTextBox
          Left = 155
          Top = 298
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_TAJUST'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_TAJUST'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label24: TLabel
          Left = 48
          Top = 299
          Width = 101
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo pr'#233'stamo ajuste:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label18: TLabel
          Left = 12
          Top = 256
          Width = 137
          Height = 13
          Alignment = taRightJustify
          Caption = 'Empleados con incapacidad:'
        end
        object FT_INCAPA: TZetaDBTextBox
          Left = 155
          Top = 256
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_INCAPA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_INCAPA'
          DataSource = DataSource
          FormatFloat = '%14.3n'
          FormatCurrency = '%m'
        end
        object Label34: TLabel
          Left = 94
          Top = 210
          Width = 55
          Height = 13
          Caption = 'Empleados:'
        end
        object Label35: TLabel
          Left = 56
          Top = 233
          Width = 93
          Height = 13
          Caption = 'Empleados de baja:'
        end
        object FT_EMPLEAD: TZetaDBTextBox
          Left = 155
          Top = 210
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_EMPLEAD'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_EMPLEAD'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object FT_BAJAS: TZetaDBTextBox
          Left = 155
          Top = 233
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_BAJAS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_BAJAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label1: TLabel
          Left = 64
          Top = 147
          Width = 85
          Height = 13
          Caption = 'Status de c'#225'lculo:'
        end
        object FT_STATUS: TZetaDBTextBox
          Left = 155
          Top = 147
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_STATUS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_STATUS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label2: TLabel
          Left = 45
          Top = 55
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Retenci'#243'n de n'#243'mina:'
        end
        object FT_NOMINA: TZetaDBTextBox
          Left = 155
          Top = 55
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_NOMINA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_NOMINA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label3: TLabel
          Left = 113
          Top = 344
          Width = 36
          Height = 13
          Caption = 'Detalle:'
        end
        object FC_PORCENTAJE: TZetaDBTextBox
          Left = 155
          Top = 124
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FC_PORCENTAJE'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_PORCENTAJE'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 48
          Top = 124
          Width = 101
          Height = 13
          Caption = '% de pago a reportar:'
        end
        object Label6: TLabel
          Left = 93
          Top = 101
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'Diferencias:'
        end
        object FT_DIFERENCIAS: TZetaDBTextBox
          Left = 155
          Top = 101
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_DIFERENCIAS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'FT_DIFERENCIAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label25: TLabel
          Left = 108
          Top = 321
          Width = 41
          Height = 13
          Caption = 'Formato:'
        end
        object FT_TIPO_CEDULA: TZetaTextBox
          Left = 155
          Top = 321
          Width = 127
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'FT_TIPO_CEDULA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object FT_DETALLE: TcxDBMemo
          Left = 155
          Top = 344
          DataBinding.DataField = 'FT_DETALLE'
          DataBinding.DataSource = DataSource
          Properties.ReadOnly = True
          Properties.ScrollBars = ssVertical
          TabOrder = 0
          Height = 81
          Width = 215
        end
      end
      object btnCierraCalculo: TcxButton
        Left = 464
        Top = 66
        Width = 145
        Height = 26
        Hint = 'Cerrar c'#225'lculo Fonacot'
        Caption = 'Cerrar c'#225'lculo Fonacot'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF6A77D6FFE2E5F7FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBEDF9FF7C87
          DAFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFE8EAF9FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7
          FDFF5C6AD2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF6D7AD6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF848FDDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCBD0F1FF98A1E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF737FD8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF6875D5FF4858CCFFC6CBEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF7C87DAFF4E5DCEFFD1D5F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF7682D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF9FAFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF6D7AD6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF7F8ADBFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFEBEDF9FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7
          FDFF4E5DCEFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF707DD7FFE2E5F7FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8EAF9FF7682
          D9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF7682D9FFEBEDF9FF5F6D
          D2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FFD7DAF4FF7682D9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF7682D9FFE8EAF9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFD1D5F2FF7682D9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF707DD7FFEEEFFAFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFD7DAF4FF7682D9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5160CEFFFFFFFFFF5F6D
          D2FF4858CCFF4858CCFF4858CCFF4858CCFF5160CEFFF6F7FDFF6875D5FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFD1D5F2FFCBD0
          F1FF5160CEFF4858CCFF4858CCFF4B5BCDFFBAC0ECFFE5E7F8FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFF1F2
          FBFFE5E7F8FFACB3E8FFACB3E8FFDFE2F6FFF6F7FDFF6D7AD6FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5665
          D0FFB4BBEAFFDFE2F6FFE2E5F7FFBDC3EDFF5F6DD2FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnCierraAbreCalculoClick
      end
      object btnAbreCalculo: TcxButton
        Tag = 1
        Left = 464
        Top = 98
        Width = 145
        Height = 26
        Hint = 'Abrir c'#225'lculo Fonacot'
        Caption = 'Abrir c'#225'lculo Fonacot'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF9D959EFFE9E7E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEDEFFFA49D
          A5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFEDEBEDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F5
          F6FF928993FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9D959EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFA69FA7FFCCC8CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF9A919AFFC5C0C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE2DFE2FF8F8590FFA39BA3FFF4F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA49DA5FF8B818CFF8B818CFFC8C4C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFC2BCC2FF8B818CFF8D838EFFE6E3E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE6E3E6FFF0EFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF988F99FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA69FA7FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFDEDBDFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F3
          F4FF908791FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFDCD9DDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFD3D0D4FF9B93
          9CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCECACEFFCCC8CCFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFF4F3F4FFA8A1A9FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFD5D2D6FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFFFFFFFFF9A919AFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFCAC6CBFFD3D0D4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFFDFDFDFFA8A1A9FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8F8590FFF6F5F6FFA49DA5FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFE0DDE0FFE6E3E6FF9289
          93FF8B818CFF8B818CFF8B818CFFCCC8CCFFE6E3E6FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFF4F3F4FFF2F1
          F2FFCAC6CBFFC8C4C9FFE6E3E6FFF6F5F6FF9D959EFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFCCC8
          CCFFE9E7E9FFEBE9EBFFCECACEFF948B95FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = btnCierraAbreCalculoClick
      end
    end
    object tsDetalleDiferencias: TcxTabSheet
      Caption = 'Diferencias en montos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object grFonacotDiferencias: TZetaCXGrid
        Left = 0
        Top = 187
        Width = 1093
        Height = 596
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object grFonacotDiferenciasGridDBTableView: TcxGridDBTableView
          PopupMenu = PopupMenu1
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          DataController.DataSource = DataSourceDetalle
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.OnGetValueList = grFonacotDiferenciasGridDBTableViewDataControllerFilterGetValueList
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.Summary.OnAfterSummary = grFonacotDiferenciasGridDBTableViewDataControllerSummaryAfterSummary
          DataController.OnSortingChanged = grFonacotDiferenciasGridDBTableViewDataControllerSortingChanged
          Filtering.ColumnFilteredItemsList = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OnColumnHeaderClick = grFonacotDiferenciasGridDBTableViewColumnHeaderClick
          object CB_CODIGO: TcxGridDBColumn
            Caption = 'Empleado'
            DataBinding.FieldName = 'CB_CODIGO'
            MinWidth = 110
            Width = 110
          end
          object PRETTYNAME: TcxGridDBColumn
            Caption = 'Nombre'
            DataBinding.FieldName = 'PRETTYNAME'
            MinWidth = 300
            Width = 300
          end
          object PR_REFEREN: TcxGridDBColumn
            Caption = 'N'#250'mero de cr'#233'dito'
            DataBinding.FieldName = 'PR_REFEREN'
            MinWidth = 120
            Width = 120
          end
          object PF_PAGO: TcxGridDBColumn
            Caption = 'Monto en c'#233'dula'
            DataBinding.FieldName = 'PF_PAGO'
            MinWidth = 125
            Width = 125
          end
          object FC_NOMINA: TcxGridDBColumn
            Caption = 'Retenido en TRESS'
            DataBinding.FieldName = 'FC_NOMINA'
            MinWidth = 125
            Width = 125
          end
          object FC_AJUSTE: TcxGridDBColumn
            Caption = 'Ajuste'
            DataBinding.FieldName = 'FC_AJUSTE'
            MinWidth = 125
            Width = 125
          end
          object DIFERENCIA: TcxGridDBColumn
            Caption = 'Diferencia'
            DataBinding.FieldName = 'DIFERENCIA'
            MinWidth = 125
            Width = 125
          end
          object FE_IN_DESC: TcxGridDBColumn
            Caption = 'Incidencia'
            DataBinding.FieldName = 'FE_IN_DESC'
            MinWidth = 125
            Width = 125
          end
          object PR_STATUS: TcxGridDBColumn
            Caption = 'Estatus del pr'#233'stamo'
            DataBinding.FieldName = 'PR_STATUS'
            MinWidth = 125
            Width = 125
          end
          object PR_FON_MOT: TcxGridDBColumn
            Caption = 'Motivo'
            DataBinding.FieldName = 'PR_FON_MOT'
            MinWidth = 125
          end
        end
        object grFonacotDiferenciasGridLevel: TcxGridLevel
          GridView = grFonacotDiferenciasGridDBTableView
        end
      end
      object plDetalle: TPanel
        Left = 0
        Top = 0
        Width = 1093
        Height = 187
        Align = alTop
        TabOrder = 1
        DesignSize = (
          1093
          187)
        object cbSoloDiferencias: TcxCheckBox
          Left = 950
          Top = 154
          Anchors = [akRight, akBottom]
          Caption = 'Mostrar solo diferencias'
          State = cbsChecked
          TabOrder = 0
          Transparent = True
          OnClick = cbSoloDiferenciasClick
          Width = 135
        end
        object gbDiferencias: TcxGroupBox
          Left = 8
          Top = 6
          Caption = 'Diferencias'
          TabOrder = 1
          Height = 163
          Width = 249
          object Label8: TLabel
            Left = 89
            Top = 15
            Width = 29
            Height = 13
            Caption = 'Bajas:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object tbBajaIMSS: TZetaDBTextBox
            Left = 124
            Top = 14
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'tbBajaIMSS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'BajaIMSS'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label9: TLabel
            Left = 45
            Top = 34
            Width = 73
            Height = 13
            Caption = 'Incapacidades:'
          end
          object tbIncapacidad: TZetaDBTextBox
            Left = 124
            Top = 33
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'tbIncapacidad'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'Incapacidad'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label12: TLabel
            Left = 95
            Top = 110
            Width = 23
            Height = 13
            Caption = 'Otro:'
          end
          object tbOtro: TZetaDBTextBox
            Left = 124
            Top = 109
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'tbOtro'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'Otro'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label11: TLabel
            Left = 91
            Top = 129
            Width = 27
            Height = 13
            Caption = 'Total:'
          end
          object tbTotal: TZetaDBTextBox
            Left = 124
            Top = 128
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'tbTotal'
            DragMode = dmAutomatic
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'Total'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object PrestamosCancelados: TZetaDBTextBox
            Left = 124
            Top = 52
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'PrestamosCancelados'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'PrestamoCancelado'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label15: TLabel
            Left = 8
            Top = 53
            Width = 110
            Height = 13
            Caption = 'Pr'#233'stamos cancelados:'
          end
          object Label17: TLabel
            Left = 53
            Top = 72
            Width = 65
            Height = 13
            Caption = 'Pago de m'#225's:'
          end
          object tbPagoMas: TZetaDBTextBox
            Left = 124
            Top = 71
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'tbPagoMas'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'PagoMas'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label22: TLabel
            Left = 39
            Top = 91
            Width = 77
            Height = 13
            Caption = 'Pago de menos:'
          end
          object tbPagoMenos: TZetaDBTextBox
            Left = 124
            Top = 90
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'tbPagoMenos'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'PagoMenos'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
        object cxGroupBox1: TcxGroupBox
          Left = 273
          Top = 6
          Caption = 'Motivos de pr'#233'stamos cancelados'
          TabOrder = 2
          Height = 123
          Width = 264
          object Label7: TLabel
            Left = 15
            Top = 15
            Width = 120
            Height = 13
            Caption = 'Pago directo en Fonacot:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object PagoDirecto: TZetaDBTextBox
            Left = 141
            Top = 14
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'PagoDirecto'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'PagoDirecto'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label10: TLabel
            Left = 14
            Top = 34
            Width = 121
            Height = 13
            Caption = 'Confirmaci'#243'n de Fonacot:'
          end
          object ConfirmacionFonacot: TZetaDBTextBox
            Left = 141
            Top = 33
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'ConfirmacionFonacot'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'Confirmacion'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label13: TLabel
            Left = 43
            Top = 53
            Width = 92
            Height = 13
            Caption = 'Ausente en c'#233'dula:'
          end
          object AusenteEnCedula: TZetaDBTextBox
            Left = 141
            Top = 52
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'AusenteEnCedula'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'Ausente'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label14: TLabel
            Left = 112
            Top = 72
            Width = 23
            Height = 13
            Caption = 'Otro:'
          end
          object CanceladoOtro: TZetaDBTextBox
            Left = 141
            Top = 71
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CanceladoOtro'
            DragMode = dmAutomatic
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CanceladoOtro'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object SubTotal: TZetaDBTextBox
            Left = 141
            Top = 90
            Width = 111
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'SubTotal'
            DragMode = dmAutomatic
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'SubTotal'
            DataSource = DataSourceDiferencias
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label16: TLabel
            Left = 108
            Top = 91
            Width = 27
            Height = 13
            Caption = 'Total:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 864
    Top = 296
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 28312480
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001A140F7F5A4634BF3E3024A93E3024A95744
          32BD221B148B0000000000000000000000000000000000000000000000000000
          0000000000000000000000000012BF966FF5D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC09871F60000001300000000000000000000000000000000000000000000
          0000000000000000000000000013C09771F6D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC0966FF50000001200000000000000000000000000000000000000000000
          0000000000040000000000000015C59968F3D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC49869F300000012000000000000000D0000000000000000000000000505
          0659847992FA7474A4FF110E0E77785B3CCCD7AA7DFFD8AA7DFFD8AA7DFFD9AA
          7DFF7F6243D20303034B706F9AFA8D829BFF0605075C0000000000000000201E
          249E8B819CFF7776A3FF1411107D725637C7DCAD7BFFDDAD7BFFDDAD7BFFDEAE
          7BFF7B5F3FCF110F107B0C0C10781614188B2E2B34B200000000000000001E1B
          219A8B819CFF7D79A1FF0E0C0C720100002F18120D7B110D096E110D096E1712
          0D7A020101380705055A413F55CF5A5466DE232128A300000000000000001D1B
          21998B819CFF8C819CFF655F78E9100F127D0F0E107A1210138012101380100F
          117C0D0C0E75585261DB8A819CFF8B819CFF1D1B219900000000000000001D1B
          21998B819CFF8C819CFF8A819CFF847B9FFF847B9FFF847B9FFF847B9FFF847B
          9FFF837B9FFF89809DFF8C829CFF8B819CFF1D1B21990000000000000000201E
          249E8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B81
          9CFF8B819CFF8B819CFF8B819CFF8B819CFF201E249E00000000000000001817
          1C918B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B81
          9CFF8B819CFF8B819CFF8B819CFF8B819CFF19181D9300000000000000000000
          002819181D9325232AA61110138001010136010101380101013B0101013B0000
          00300303034C1B191E95201E249E1B191E950000002B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00041A4831B34ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF256445C70000001000000000000000000205
          034C4ED293FF040C0864040A0760040A0760040A0760040A0760040A0760040A
          0760040A0760040A0760040C086443B47EF3050F0A6C0000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000611
          0C7043B47EF34ED293FF4ED293FF43B47EF3000201380000000000000000040A
          07604ED293FF00000000000000000000000000000000000000000000000047BD
          85F74ED293FF4ED293FF4BC88DFB02070454000000000000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000004ED2
          93FF4ED293FF4BC88DFB0207045400000000000000000000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000004ED2
          93FF4BC88DFB0207045400000000000000000000000000000000000000000103
          02444ED293FF050F0A6C040A0760040A0760040A0760040A0760040C08644ED2
          93FF0309065C0000000000000000000000000000000000000000000000000000
          00000E271B9347BD85F74ED293FF4ED293FF4ED293FF4ED293FF4ED293FF0611
          0C70000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00004656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF2F3A87DF090B1980161C40AF4656CCFF4656CCFF1D24
          55BF090B1980252E6DCF4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF0000000000000000000000000000
          000003040A6003040A6003040A6003040A6003040A6003040A6003040A600304
          0A6003040A6003040A6003040A6003040A600000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF00000000000000004656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF00000000000000000000
          00000000000000000000000000004656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000004656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF0000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000080A5C00384BAF0002023C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000090C6000B2E9FF00B2E9FF005975CB00080A5C00000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000011177800B2E9FF00B2E9FF0083ACE7000E137000000008000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000161D800092BEEF00161D8000000010000000000000000C000102380000
          0000000000000000000000000000000000000000000000000000000000000000
          00000001023800000018000000000000000000242F97007DA4E300536FC70000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000080A5C004962BF00A8DCFB00B2E9FF00B2E9FF0008
          0A5C000000000000000000000000000000000000000000000000000000000000
          00000000000000161D80008AB6EB00B2E9FF00B2E9FF00B2E9FF00B2E9FF0083
          ACE70000000C0000000000000000000000000000000000000000000000000000
          0000000000000000002000A0D4F700B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00181F830000000000000000000000000000000000000000000000000000
          0000000000000000000000181F8300B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00A0D4F70000002400000000000000000000000000000000000000000000
          000000000000000000000000000C0083ACE700B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF003446AB00000000000000000000000000000000000000000000
          000000000000000000000000000000090C6000B2E9FF00B2E9FF00B2E9FF00B2
          E9FF004E67C30001013400000000000000000000000000000000000000000000
          000000000000000000000000000000000000005975CB00B2E9FF00779BDF0006
          09580000000000000000000000100000000C0000000000000000000000000000
          0000000000000000000000000000000000000002023C00131A7C000000080000
          000000000004000E13700092BEEF001015740000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000005
          0650007093DB00B2E9FF00B2E9FF000B0F680000000000000000000000000000
          0000000000000000000000000000000000000000000000000004004155B700B2
          E9FF00B2E9FF005975CB0002023C000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000090C600059
          75CB00080A5C0000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited ActionList: TActionList
    Left = 986
    Top = 96
  end
  inherited PopupMenu1: TPopupMenu
    Left = 864
    Top = 96
  end
  object DataSourceDetalle: TDataSource
    OnDataChange = DataSourceDataChange
    OnUpdateData = DataSourceUpdateData
    Left = 856
    Top = 200
  end
  object DataSourceDiferencias: TDataSource
    Left = 864
    Top = 144
  end
  object DataSourceViejo: TDataSource
    OnDataChange = DataSourceDataChange
    OnUpdateData = DataSourceUpdateData
    Left = 864
    Top = 360
  end
end
