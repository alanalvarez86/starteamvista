unit FSimulaMontos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, ExtCtrls, ComCtrls, DBCtrls,
     ZetaCommonClasses, ZetaDBTextBox, ZBaseGridLectura_DevEx,
     cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList, cxGridLevel,
     cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGrid, ZetaCXGrid, cxPCdxBarPopupMenu, cxContainer,
     cxGroupBox, cxPC, cxButtons;

type
  TSimulaMontos_DevEx = class(TBaseGridLectura_DevEx)
    PageControl: TcxPageControl;
    dsMovimiento: TDataSource;
    tsTotales: TcxTabSheet;
    TabSheet3: TcxTabSheet;
    Splitter1: TSplitter;
    GroupBox1: TcxGroupBox;
    Label6: TLabel;
    NO_PER_MEN: TZetaDBTextBox;
    Label9: TLabel;
    NO_PER_CAL: TZetaDBTextBox;
    Label4: TLabel;
    NO_TOT_PRE: TZetaDBTextBox;
    GroupBox2: TcxGroupBox;
    Label5: TLabel;
    NO_X_ISPT: TZetaDBTextBox;
    Label10: TLabel;
    NO_X_CAL: TZetaDBTextBox;
    Label7: TLabel;
    NO_X_MENS: TZetaDBTextBox;
    Label8: TLabel;
    NO_IMP_CAL: TZetaDBTextBox;
    GroupBox3: TcxGroupBox;
    Label13: TLabel;                                                                 
    CB_SALARIO: TZetaDBTextBox;
    Label19: TLabel;
    CB_SAL_INT: TZetaDBTextBox;
    TabSheet2: TcxTabSheet;
    Label33: TLabel;
    NO_STATUS: TZetaDBTextBox;
    US_CODIGO: TZetaDBTextBox;
    Label12: TLabel;
    Label11: TLabel;
    NO_OBSERVA: TZetaDBTextBox;
    GroupBox4: TcxGroupBox;
    Label1: TLabel;
    NO_PERCEPC: TZetaDBTextBox;
    Label2: TLabel;
    NO_DEDUCCI: TZetaDBTextBox;
    NO_NETO: TZetaDBTextBox;
    Label3: TLabel;
    Label20: TLabel;
    NO_FUERA: TDBCheckBox;
    Label21: TLabel;
    NO_LIQUIDA: TZetaDBTextBox;
    Calcular: TcxButton;
    TabHoras: TcxTabSheet;
    TabDias: TcxTabSheet;
    Label17: TLabel;
    Label18: TLabel;
    Label22: TLabel;
    Label24: TLabel;
    NO_DIAS: TZetaDBTextBox;
    NO_DIAS_AS: TZetaDBTextBox;
    NO_DIAS_NT: TZetaDBTextBox;
    NO_DIAS_SU: TZetaDBTextBox;
    Label14: TLabel;
    Label23: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    NO_DIAS_IN: TZetaDBTextBox;
    NO_DIAS_SG: TZetaDBTextBox;
    NO_DIAS_CG: TZetaDBTextBox;
    NO_DIAS_OT: TZetaDBTextBox;
    Label29: TLabel;
    Label30: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    NO_DIAS_AJ: TZetaDBTextBox;
    NO_DIAS_RE: TZetaDBTextBox;
    NO_DIAS_SS: TZetaDBTextBox;
    NO_DIAS_EM: TZetaDBTextBox;
    Label15: TLabel;
    Label16: TLabel;
    Label38: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    NO_DIAS_FJ: TZetaDBTextBox;
    NO_DIAS_FV: TZetaDBTextBox;
    NO_DIAS_VA: TZetaDBTextBox;
    NO_DIAS_AG: TZetaDBTextBox;
    Label34: TLabel;
    NO_HORAS: TZetaDBTextBox;
    Label35: TLabel;
    NO_Hora_PD: TZetaDBTextBox;
    NO_DES_TRA: TZetaDBTextBox;
    Label36: TLabel;
    NO_EXTRAS: TZetaDBTextBox;
    Label37: TLabel;
    Label39: TLabel;
    NO_TRIPLES: TZetaDBTextBox;
    Label40: TLabel;
    NO_FES_TRA: TZetaDBTextBox;
    NO_VAC_TRA: TZetaDBTextBox;
    Label41: TLabel;
    NO_DOBLES: TZetaDBTextBox;
    Label42: TLabel;
    Label43: TLabel;
    NO_TARDES: TZetaDBTextBox;
    Label44: TLabel;
    NO_HORA_CG: TZetaDBTextBox;
    NO_HORA_SG: TZetaDBTextBox;
    Label45: TLabel;
    NO_ADICION: TZetaDBTextBox;
    Label46: TLabel;
    Label47: TLabel;
    NO_FES_PAG: TZetaDBTextBox;
    BtnModificarTodos: TcxButton;
    PanelFiniquito: TPanel;
    BtnLiquidacion: TcxButton;
    BtnAplicarFiniquito: TcxButton;
    CalcularAVENT: TcxButton;
    Label48: TLabel;
    NO_APROBA: TZetaDBTextBox;
    bbMostrarTotales: TcxButton;
    bbAprobarFiniquito: TcxButton;
    bbBorrarFiniquito: TcxButton;
    Label49: TLabel;
    DBCheckBox1: TDBCheckBox;
    NO_PER_ISN: TZetaDBTextBox;
    lblTotalISN: TLabel;
    co_numero: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    MO_PERCEPC: TcxGridDBColumn;
    MO_DEDUCCI: TcxGridDBColumn;
    CO_TIPO: TcxGridDBColumn;
    MO_ACTIVO: TcxGridDBColumn;
    US_CODIGO_GRID: TcxGridDBColumn;
    MO_REFEREN: TcxGridDBColumn;
    MO_X_ISPT: TcxGridDBColumn;
    MO_PER_CAL: TcxGridDBColumn;
    mo_imp_cal: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure CalcularClick(Sender: TObject);
    procedure BtnModificarTodosClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure BtnLiquidacionClick(Sender: TObject);
    procedure BtnAplicarFiniquitoClick(Sender: TObject);
    procedure bbAprobarFiniquitoClick(Sender: TObject);
    procedure bbBorrarFiniquitoClick(Sender: TObject);
    procedure bbMostrarTotalesClick(Sender: TObject);
  private
    function ChecaStatus: Boolean;
    function VerificarNominaCalculada:Boolean;
    function NoHayDatosNomina: Boolean;
    procedure SetBotones;
    { Private declarations }
  protected
    { Protected declarations }
    function NoHayDatos: Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
    procedure CargaParametrosReporte(Parametros: TZetaParams);override;
  end;

var
  SimulaMontos_DevEx: TSimulaMontos_DevEx;

implementation

uses DNomina,
     DCatalogos,
     DCliente,
     DSistema,
     DProcesos,
     DGlobal,
     FTressShell,
     ZImprimeForma,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaTipoEntidad,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     ZetaSystemWorking,
     ZetaClientDataSet,
     ZetaDespConsulta,
     dRecursos;

{$R *.DFM}

{ TSimulaMontos }

procedure TSimulaMontos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= HHHHH_Simulacion_Finiquitos_Empleado;
     IndexDerechos := D_EMP_SIM_FINIQUITOS_EMPLEADO;
     //Verificar Global de AVENT
     Calcular.Visible := not Global.GetGlobalBooleano( K_GLOBAL_AVENT );
     CalcularAVENT.Visible := not Calcular.Visible;
     PageControl.ActivePage := tsTotales;
end;

procedure TSimulaMontos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     Calcular.Enabled := True; //CheckDerecho( D_EMP_SIM_FINIQUITOS_EMPLEADO, K_DERECHO_CAMBIO );
     if DataSource.DataSet.Active then
        bbMostrarTotales.Enabled := zStrToBool( DataSource.DataSet.FieldByName('NO_GLOBAL').AsString )
     else
         bbMostrarTotales.Enabled := FALSE;
     bbMostrarTotales.Visible := CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_CONSULTA);
     bbAprobarFiniquito.Enabled := ( ( CheckDerecho(D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_NIVEL0 ) ) and ( Global.GetGlobalBooleano ( K_GLOBAL_SIM_FINIQ_APROBACION ) ) );
     bbBorrarFiniquito.Enabled := ( CheckDerecho(D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_CONFIGURA ));
     SetBotones;
end;

procedure TSimulaMontos_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmNomina do
     begin
          cdsSimulaMontos.Conectar;
          DataSource.DataSet:= cdsSimulaTotales;
          dsMovimiento.DataSet:= cdsSimulaMontos;

          if DerechoSoloAplicarIndividual then
          begin
               with cdsSimulaTotales do
               begin
                    if (eStatusPeriodo(FieldByName('NO_STATUS').AsInteger)<spCalculadaTotal) OR
                       (eStatusSimFiniquitos(FieldByName('NO_APROBA').AsInteger)=ssfSinAprobar) OR
                            (dmCliente.cdsEmpleado.FieldByName( 'CB_ACTIVO' ).AsString ='S' )   then
                       begin
                            cdsSimulaMontos.Close ;
                            cdsSimulaTotales.Close;
                       end;
               end;
          end;
     end;
end;

procedure TSimulaMontos_DevEx.Refresh;
begin
     TZetaClientDataSet( dsMovimiento.DataSet ).Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSimulaMontos_DevEx.CalcularClick(Sender: TObject);
var
   oCursor: TCursor;
   sMensaje: string;
begin
     inherited;

     if ChecaStatus then
     begin
          if dmNomina.ExistePeriodoSimulacion( sMensaje, 'Calcular N�mina') then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  InitAnimation( 'Calculando N�mina' );
                  dmProcesos.CalcularSimulacionEmpleado( eTipoPeriodo( dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger ),
                                                         Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
               finally
                      EndAnimation;
                      DProcesos.SetDataChange(prNOCalcular);
                      TressShell.ReconectaMenu;
                      Screen.Cursor := oCursor;
               end;
          end
          else
              ZetaDialogo.ZError( Caption, sMensaje, 0);
     end;
end;

function TSimulaMontos_DevEx.ChecaStatus: Boolean;
var
   sMensaje :string;
const
     K_Mensaje = 'La Simulaci�n de Finiquito tiene status de %s. Se cambiar� a status %s' +CR_LF + '� Desea Continuar ?';
begin
     with dmNomina do
     begin
          if ( RequiereAprobacionRH ) then
          begin
               if ( StatusSimulacionActual <> ssfSinAprobar )then
               begin
                    sMensaje := Format(K_Mensaje,[ObtieneElemento(lfStatusSimFiniquitos,Ord(StatusSimulacionActual)),ObtieneElemento(lfStatusSimFiniquitos,Ord(ssfSinAprobar)) ]);
                    Result := ZConfirm(Self.Caption,sMensaje,0,mbNO );
               end
               else
                   Result := True;
          end
          else
          begin
               if ( StatusSimulacionActual = ssfLiquidada )then
               begin
                    sMensaje := Format(K_Mensaje,[ObtieneElemento(lfStatusSimFiniquitos,Ord(StatusSimulacionActual)),ObtieneElemento(lfStatusSimFiniquitos,Ord(ssfAprobada)) ]);
                    Result := ZConfirm(Self.Caption,sMensaje,0,mbNO );
               end
               else
                   Result := True;
          end;
     end;
end;

function TSimulaMontos_DevEx.NoHayDatos: Boolean;
begin
     Result := NoData( dsMovimiento );
end;

function TSimulaMontos_DevEx.NoHayDatosNomina: Boolean;
begin
     Result := NoData( DataSource );
end;


function TSimulaMontos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ExistePeriodoSimulacion( sMensaje, 'Agregar' );
     end;
end;

procedure TSimulaMontos_DevEx.Agregar;
begin
     if ChecaStatus then
     begin
          TZetaClientDataSet( dsMovimiento.DataSet ).Agregar;
     end;
end;

procedure TSimulaMontos_DevEx.Borrar;
begin
     if ChecaStatus then
     begin
          TZetaClientDataSet( dsMovimiento.DataSet ).Borrar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TSimulaMontos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ExistePeriodoSimulacion( sMensaje, 'Modificar' );
     end;
end;

procedure TSimulaMontos_DevEx.Modificar;
begin
     if ChecaStatus then
     begin
          TZetaClientDataSet( dsMovimiento.DataSet ).Modificar;
     end;
end;

procedure TSimulaMontos_DevEx.BtnModificarTodosClick(Sender: TObject);
var
   sMensaje: String;
begin
     if NoHayDatos then
         zInformation( Caption, 'No Hay Datos Para Modificar', 0 )
     else
     begin
          if PuedeModificar( sMensaje ) then
          begin
               ZetaDBGridDBTableView.DataController.DataSource := nil;
               try
                  dmNomina.EditarGridSimulacionMontos;
               finally
                  ZetaDBGridDBTableView.DataController.DataSource := dsMovimiento;
               end;
          end
          else
             zInformation( Caption, sMensaje, 0 );
     end;
end;

procedure TSimulaMontos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     { No se debe llamar inherited para que no se tome otro Dataset
     { como referencia para el m�todo NoData }
     //ImgNoRecord.Visible:= NoHayDatos; //DevEx(@am): No es necesario enla Nueva Imagen.
     SetBotones;
end;

procedure TSimulaMontos_DevEx.SetBotones;
begin
     bbBorrarFiniquito.Enabled := ( (not NoHayDatosNomina) and ( CheckDerecho( D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_CONFIGURA ) ) );
     BtnAplicarFiniquito.Enabled := ( ( not NoHayDatosNomina) and ( CheckDerecho( D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_BANCA ) ) );
     bbAprobarFiniquito.Enabled := ( (not NoHayDatosNomina) and  (Global.GetGlobalBooleano ( K_GLOBAL_SIM_FINIQ_APROBACION ) )and  ( CheckDerecho(D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_NIVEL0 ) ) );
     if DataSource.DataSet.Active then
        bbMostrarTotales.Enabled := zStrToBool( DataSource.DataSet.FieldByName('NO_GLOBAL').AsString )
     else
         bbMostrarTotales.Enabled := FALSE;
end;

procedure TSimulaMontos_DevEx.BtnLiquidacionClick(Sender: TObject);
var
   sMensaje: String;
begin
     if  CheckDerecho( D_EMP_SIM_FINIQUITOS_EMPLEADO, K_DERECHO_SIST_KARDEX ) then
     begin
          if dmNomina.ExistePeriodoSimulacion( sMensaje, 'Liquidar empleado' ) then
          begin{
               if (NOT DataSource.DataSet.IsEmpty)then
               begin
                    if ZConfirm(Self.Caption,'La Liquidaci�n Ya Existe. '+CR_LF+'� Desea Continuar ?',0,mbYes)then
                       dmNomina.SimulaLiquidacion;
               end
               else
                   dmNomina.SimulaLiquidacion;
                   }
               dmNomina.SimulaLiquidacion;
          end
          else
              zInformation( Caption, sMensaje, 0 );
     end
     else
         zInformation( Caption, 'No tiene permiso para liquidar empleados', 0 );

     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSimulaMontos_DevEx.BtnAplicarFiniquitoClick(Sender: TObject);
begin
     inherited;
     if CheckDerecho( D_EMP_SIM_FINIQUITOS_EMPLEADO, K_DERECHO_BANCA )  then
     begin
          with dmNomina do
          begin
               if ( ( ( Ord( StatusSimulacionActual ) <> Ord(ssfSinAprobar ) ) and ( RequiereAprobacionRH  ) ) or ( ( Ord( StatusSimulacionActual ) >= Ord(ssfAprobada ) ) and ( not RequiereAprobacionRH ) )  )then
               begin
                   if VerificarNominaCalculada then
                      AplicaFiniquito;
               end
               else
                   ZError(Self.Caption,'El finiquito no est� aprobado',0 );
          end;

     end
     else
         zInformation( Caption, 'No tiene permiso para aplicar finiquitos', 0 );
end;

procedure TSimulaMontos_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enMovimienBalanza, DataSource.Dataset );
end;

procedure TSimulaMontos_DevEx.CargaParametrosReporte(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddInteger( 'Year', dmCliente.YearDefault );
          AddInteger( 'Tipo', dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger );
          AddInteger( 'Numero', Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
     end;
end;
procedure TSimulaMontos_DevEx.bbAprobarFiniquitoClick(Sender: TObject);
const
     K_MENSAJE = 'La Simulaci�n de Finiquito con Status %s ser� Aprobada.'+CR_LF+'� Desea Continuar ?';
     K_MENSAJE_ERR2 = 'El empleado %s no est� dado de baja '+CR_LF+'No se puede aprobar la Simulaci�n de Finiquitos';
     K_MENSAJE_WR1 = 'El Finiquito ya esta Aprobado '+CR_LF+'� Desea Desaprobar el Finiquito ?';

begin
     inherited;
     if CheckDerecho(D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_NIVEL0 )  then
     begin
          if ( dmNomina.StatusSimulacionActual = ssfAprobada ) then
          begin
               if ZConfirm(sELF.Caption,K_MENSAJE_WR1,0,mbYes)then
               begin
                    dmNomina.ProcesaDesAprobacion
               end
          end
          else
          begin
               if ZConfirm(sELF.Caption,Format(K_MENSAJE,[ObtieneElemento(lfStatusSimFiniquitos,Ord(dmNomina.StatusSimulacionActual))]),0,mbYes)then
               begin
                    if VerificarNominaCalculada then
                    begin
                         if ( dmNomina.cdsSimulaTotales.FieldByName('NO_STATUS').AsInteger >= Ord(spCalculadaTotal ) )then
                         begin
                              with dmCliente.GetDatosEmpleadoActivo do
                              begin
                                   if Activo then
                                   begin
                                        if ( ZAccesosMgr.CheckDerecho( D_EMP_REG_BAJA, K_DERECHO_CONSULTA ) ) and
                                            ZetaDialogo.zConfirm( 'Aprobaci�n de Finiquito', '� Desea Efectuar la Baja de Este Empleado ?', 0, mbYes ) then
                                        begin
                                             dmRecursos.AgregaKardex( K_T_BAJA );
                                        end
                                  end;
                                  if dmCliente.GetDatosEmpleadoActivo.Activo then
                                  begin
                                       ZError(Self.Caption,Format(K_MENSAJE_ERR2,[dmCliente.GetEmpleadoDescripcion]),0 )
                                  end
                                  else
                                      dmNomina.ProcesaAprobacion;
                             end;
                         end;
                    end;
               end;
          end;
     end
     else
         zInformation( Caption, 'No tiene permiso para Aprobar Finiquito', 0 );
end;

procedure TSimulaMontos_DevEx.bbBorrarFiniquitoClick(Sender: TObject);
begin
     inherited;
     if ZWarningConfirm(Self.Caption,'� Desea borrar el Finiquito de Empleado ?',0,mbNo )then
     begin
          dmNomina.BorrarFiniquito;
          Refresh;
     end;
end;

procedure TSimulaMontos_DevEx.bbMostrarTotalesClick(Sender: TObject);
begin
     inherited;
     with dmNomina do
     begin
          cdsSimulaGlobales.Refrescar;
          cdsSimulaTotales.Refrescar;
          cdsSimulaGlobales.Locate('CB_CODIGO ',DataSource.DataSet.FieldByName('CB_CODIGO').AsInteger,[]);
     end;

     TressShell.AbreFormaSimulaciones(2);//Abre la forma de Simulaciones Globales

end;

function TSimulaMontos_DevEx.VerificarNominaCalculada:Boolean;
const
     K_MENSAJE_CAL = 'La n�mina del empleado no est� calculada '+CR_LF+'� Desea Calcularla ?';
begin
     Result :=( dmNomina.cdsSimulaTotales.FieldByName('NO_STATUS').AsInteger >= Ord(spCalculadaTotal ) );
     if not Result then
     begin
          Result := ZConfirm('Simulaci�n de Finiquitos',K_MENSAJE_CAL,0,mbYes);
          if Result then
          begin
               CalcularClick(Self);
          end
     end;
end;

end.
