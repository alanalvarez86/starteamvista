unit FFonacotDatosEmpleado_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, StdCtrls, ExtCtrls,
     ZetaDBTextBox, ZBaseGridLecturaImss_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, Menus, ActnList, ImgList, ComCtrls, ZetaStateComboBox,
     cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
     ZBaseGridLecturaFonacot_DevEx, System.Actions;

type
  TFonacotDatosEmpleado_DevEx = class(TBaseGridLecturaFonacot_DevEx)
    dsCre_Emp: TDataSource;
    Panel1: TPanel;
    FE_CUANTOS: TZetaDBTextBox;
    Label7: TLabel;
    FE_RETENC: TZetaDBTextBox;
    GroupBox1: TGroupBox;
    FE_INCIDE: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    FE_FECHA1: TZetaDBTextBox;
    FE_FECHA2: TZetaDBTextBox;
    Label4: TLabel;
    FE_AJUSTE: TZetaDBTextBox;
    Label5: TLabel;
    Label1: TLabel;
    FE_NOMINA: TZetaDBTextBox;
    Label6: TLabel;
    PR_REFEREN: TcxGridDBColumn;
    FC_RETENC: TcxGridDBColumn;
    FC_NOMINA: TcxGridDBColumn;
    FC_AJUSTE: TcxGridDBColumn;
    FC_MONTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  end;

var
  FonacotDatosEmpleado_DevEx: TFonacotDatosEmpleado_DevEx;

implementation

uses DNomina,
     DGlobal,
     ZGlobalTress,
     ZetaCommonLists,
     ZetaCommonClasses, ZBaseGridLectura_DevEx;

{$R *.DFM}

{ TIMSSHisMensual }

procedure TFonacotDatosEmpleado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
    // TipoValorActivo2 := stRazonSocial;
     HelpContext := H_FONACOT_DATOS_EMPLEADO;
end;

procedure TFonacotDatosEmpleado_DevEx.Connect;
const
    RETENCION_MENSUAL_GLOBALAPAGADO = 'Tot. Retenci�n Mensual:';
    RETENCION_NOMINA_GLOBALAPAGADO = 'Tot. Retenci�n x N�mina:';

    RETENCION_MENSUAL_GLOBALPRENDIDO = 'Pago Requerido:';
    RETENCION_NOMINA_GLOBALPRENDIDO = 'Retenci�n de N�mina:';

var
  iValorGlobalPrestamoFonacot : integer;
begin
      try
           iValorGlobalPrestamoFonacot := 0;
           iValorGlobalPrestamoFonacot := Global.GetGlobalInteger( K_GLOBAL_FONACOT_PRESTAMO_CONCILIA );

           case iValorGlobalPrestamoFonacot of
             0 :
             begin
                  label1.Caption := RETENCION_MENSUAL_GLOBALAPAGADO;
                  label6.Caption := RETENCION_NOMINA_GLOBALAPAGADO;
                  ZetaDBGridDBTableView.Columns[1].Caption := 'Retenci�n Mensual';
                  ZetaDBGridDBTableView.Columns[2].Caption := 'Descuento N�mina';
             end;
             1 :
             begin
                  label1.Caption := RETENCION_MENSUAL_GLOBALPRENDIDO;
                  label6.Caption := RETENCION_NOMINA_GLOBALPRENDIDO;
                  ZetaDBGridDBTableView.Columns[1].Caption := 'Pago Requerido';
                  ZetaDBGridDBTableView.Columns[2].Caption := 'Retenci�n de N�mina';
             end;
           end;

           with dmNomina do
           begin
                cdsFonEmp.Conectar;
                cdsFonCre.Conectar;
                DataSource.DataSet:= cdsFonEmp;
                dsCre_Emp.DataSet := cdsFonCre;
           end;

      Except
      End;
end;

procedure TFonacotDatosEmpleado_DevEx.Refresh;
begin
     with dmNomina do
     begin
          cdsFonEmp.Refrescar;
          cdsFonCre.Refrescar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TFonacotDatosEmpleado_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se puede agregar el historial de pagos Fonacot';
end;

function TFonacotDatosEmpleado_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se puede borrar el historial de pagos Fonacot';
end;

function TFonacotDatosEmpleado_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se puede modificar el historial de pagos Fonacot';
end;

procedure TFonacotDatosEmpleado_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited; 
end;

end.
