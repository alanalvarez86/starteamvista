inherited GridNomMontos_DevEx: TGridNomMontos_DevEx
  Left = 771
  Top = 153
  Caption = 'Excepciones de Montos'
  ClientHeight = 334
  ClientWidth = 602
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 298
    Width = 602
    inherited OK_DevEx: TcxButton
      Left = 436
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 516
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 602
    inherited ValorActivo2: TPanel
      Width = 276
      inherited textoValorActivo2: TLabel
        Width = 270
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 602
    Height = 251
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 192
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'CO_NUMERO'
        Title.Alignment = taCenter
        Title.Caption = '#'
        Width = 41
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_DESCRIP'
        ReadOnly = True
        Title.Caption = 'Concepto'
        Width = 161
        Visible = True
      end
      item
        Alignment = taRightJustify
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'MO_MONTO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Monto'
        Width = 63
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'MO_REFEREN'
        Title.Caption = 'Referencia'
        Width = 59
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        ReadOnly = True
        Title.Caption = 'Modific'#243
        Width = 45
        Visible = True
      end>
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 436
    Top = 113
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
    inherited BarSuperior: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarControlContainerItem_DBNavigator'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_AgregarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BorrarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ModificarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_BuscarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_ImprimirBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ImprimirFormaBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_ExportarBtn'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton_CortarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_CopiarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_PegarBtn'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_UndoBtn'
        end
        item
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 126
          Visible = True
          ItemName = 'CbOperacion'
        end>
    end
    object CbOperacion: TdxBarCombo
      Category = 0
      Hint = 'Accion'
      Visible = ivAlways
      ShowEditor = False
      Items.Strings = (
        'Reportar Error'
        'Dejar Anteriores'
        'Sumar Monto'
        'Sustituir Monto')
      ItemIndex = -1
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Si ya existen :'
      Category = 0
      Hint = 'Si ya existen :'
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
  end
end
