inherited NomExcepMontos_DevEx: TNomExcepMontos_DevEx
  Left = 188
  Top = 269
  Caption = 'Excepciones de Montos'
  ClientHeight = 312
  ClientWidth = 585
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 585
    inherited Slider: TSplitter
      Left = 421
    end
    inherited ValorActivo1: TPanel
      Width = 405
      inherited textoValorActivo1: TLabel
        Width = 399
      end
    end
    inherited ValorActivo2: TPanel
      Left = 424
      Width = 161
      inherited textoValorActivo2: TLabel
        Width = 155
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 55
    Width = 585
    Height = 257
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 70
        Options.Grouping = False
        Width = 70
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        MinWidth = 80
        Width = 164
      end
      object CO_NUMERO: TcxGridDBColumn
        Caption = 'Concepto'
        DataBinding.FieldName = 'CO_NUMERO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 80
        Options.Grouping = False
        Width = 80
      end
      object CO_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CO_DESCRIP'
        MinWidth = 100
        Options.Grouping = False
        Width = 100
      end
      object MO_PERCEPC: TcxGridDBColumn
        Caption = 'Percepciones'
        DataBinding.FieldName = 'MO_PERCEPC'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 100
        Options.Grouping = False
        Width = 100
      end
      object MO_DEDUCCI: TcxGridDBColumn
        Caption = 'Deducciones'
        DataBinding.FieldName = 'MO_DEDUCCI'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 100
        Options.Grouping = False
        Width = 100
      end
      object CO_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CO_TIPO'
        MinWidth = 50
      end
      object MO_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'MO_ACTIVO'
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_CODIGO'
        MinWidth = 80
        Options.Grouping = False
      end
      object MO_REFEREN: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'MO_REFEREN'
        MinWidth = 80
        Options.Grouping = False
        Width = 80
      end
      object MO_X_ISPT: TcxGridDBColumn
        Caption = 'Exento'
        DataBinding.FieldName = 'MO_X_ISPT'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 80
        Options.Grouping = False
      end
      object MO_PER_CAL: TcxGridDBColumn
        Caption = 'Individual'
        DataBinding.FieldName = 'MO_PER_CAL'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 80
        Options.Grouping = False
        Width = 100
      end
      object MO_IMP_CAL: TcxGridDBColumn
        Caption = 'ISPT Individual'
        DataBinding.FieldName = 'MO_IMP_CAL'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 100
        Options.Grouping = False
        Width = 100
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 585
    Height = 36
    Align = alTop
    TabOrder = 2
    object BtnModificarTodos: TcxButton
      Left = 4
      Top = 5
      Width = 113
      Height = 26
      Caption = 'Modificar Todos'
      TabOrder = 0
      OnClick = BtnModificarTodosClick
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000000B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFAFE7F8FF3CC4EEFF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFCBEF
        FBFF5CCEF1FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFE7F8
        FDFF70D4F3FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEFFAFEFF80D9F4FF10B7
        EAFF00B2E9FF0CB6EAFF38C3EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF38C3EEFF18B9EBFF00B2E9FF00B2
        E9FF97E0F6FFE3F7FDFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFBFEC
        F9FFFBFEFFFFFFFFFFFFFFFFFFFF5CCEF1FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEBF9FDFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F8FDFF0CB6EAFF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF20BCECFFF7FDFEFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DAF4FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FDFEFF24BDECFF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0CB6EAFFE7F8
        FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE6F8FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CF
        F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3EDFAFF34C2EDFF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FFCBEFFBFFFFFFFFFFDFF5FCFF58CDF1FF00B2E9FF00B2E9FF10B7EAFF0CB6
        EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF3CC4EEFF7CD7F4FF08B4EAFF00B2E9FF04B3E9FF70D4F3FFEFFAFEFF74D5
        F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF50CAF0FFDBF4FCFFFFFFFFFFFFFFFFFF68D1
        F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF04B3E9FFB7E9F9FFFFFFFFFFFFFFFFFFCBEFFBFF3CC4EEFF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF60CFF1FFCBEFFBFF5CCEF1FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      OptionsImage.Margin = 1
    end
  end
  inherited DataSource: TDataSource
    Left = 408
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end