unit FTestMyEMail;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons,
     ExtCtrls, ComCtrls, IniFiles, Mask,
  ZetaNumero, ZetaKeyCombo,
	{$IFDEF TRESS_DELPHIXE5_UP}
	DEmailService,
	{$ELSE}
	DEmailServiceD7,
	{$ENDIF} ZetaCommonLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxButtons;

type
  TEMailTest = class(TForm)
    ServidorLBL: TLabel;
    Servidor: TEdit;
    UsuarioLBL: TLabel;
    Usuario: TEdit;
    RemitenteLBL: TLabel;
    Remitente: TEdit;
    RemitenteDireccionLBL: TLabel;
    RemitenteDireccion: TEdit;
    DireccionEnvio: TLabel;
    Ejecutar: TcxButton;
    Destinatario: TEdit;
    Label1: TLabel;
    txtPassword: TEdit;
    Label2: TLabel;
    znPuerto: TZetaNumero;
    Autenticacion: TZetaKeyCombo;
    PrimerDiaLBL: TLabel;
    procedure EjecutarClick(Sender: TObject);
    procedure EmailAuthenticationFailed(var Handled: Boolean);
    procedure EmailConnectionFailed(Sender: TObject);
    procedure EmailConnectionRequired(var Handled: Boolean);
    procedure EmailFailure(Sender: TObject);
    procedure EmailInvalidHost(var Handled: Boolean);
    procedure EmailRecipientNotFound(Recipient: String);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function GetDestinatario: String;
    function GetFromAddress: String;
    function GetFromName: String;
    function GetHost: String;
    function GetUserID: String;
    function GetPassword: String;
    function GetPort: Integer;
    procedure SendEmail;
  public
    { Public declarations }
  end;

var
  EMailTest: TEMailTest;

implementation

uses ZetaDialogo;

{$R *.DFM}

{ ******** TEMailTest ************ }

function TEMailTest.GetDestinatario: String;
begin
     Result := Destinatario.Text;
end;

function TEMailTest.GetFromAddress: String;
begin
     Result := RemitenteDireccion.Text;
end;

function TEMailTest.GetFromName: String;
begin
     Result := Remitente.Text;
end;

function TEMailTest.GetHost: String;
begin
     Result := Servidor.Text;
end;

function TEMailTest.GetUserID: String;
begin
     Result := Usuario.Text;
end;

procedure TEMailTest.SendEmail;
var
   dmEmailServiceTest : TdmEmailService;
   sMensajeError : string;
begin
     if ( GetDestinatario <> '' ) then
     begin
          dmEmailServiceTest := TdmEmailService.Create(Self);
          dmEmailServiceTest.NewEmail;
          dmEmailServiceTest.NewBodyMessage;
          dmEmailServiceTest.AuthMethod := eAuthTressEmail( Autenticacion.ItemIndex );
          dmEmailServiceTest.MailServer := GetHost;
          dmEmailServiceTest.User := GetUserId;
          dmEmailServiceTest.Password := GetPassword;
          dmEmailServiceTest.Port := GetPort;
          dmEmailServiceTest.FromAddress := GetFromAddress;
          dmEmailServiceTest.FromName := GetFromName;
          dmEmailServiceTest.Subject := Format( 'Prueba de Mensaje %s', [ FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) ] );
          dmEmailServiceTest.ToAddress.Add( GetDestinatario );
          dmEmailServiceTest.MessageText.Add (Format( 'Prueba de Mensaje %s', [ FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) ] ));

          if dmEmailServiceTest.SendEmail( sMensajeError ) then
             ZetaDialogo.zInformation( '� Exito !', 'Mensaje Enviado Por E-mail', 0 )
          else
              ZetaDialogo.zError( '� Hubo Problemas !', Format( 'El Servidor %s de E-mail no se Pudo Conectar o Fue Imposible Enviar El Correo ( Detalle: %s ) ', [ GetHost , sMensajeError] ), 0 );
     end;
end;


procedure TEMailTest.EmailAuthenticationFailed(var Handled: Boolean);
begin
     ZetaDialogo.zError( '� Hubo Problemas !', 'E-mail Authentication Failed', 0 );
     Handled := True;
end;

procedure TEMailTest.EmailConnectionFailed(Sender: TObject);
begin
     ZetaDialogo.zError( '� Hubo Problemas !', 'E-mail Connection Failed', 0 );
end;

procedure TEMailTest.EmailConnectionRequired(var Handled: Boolean);
begin
     ZetaDialogo.zError( '� Hubo Problemas !', 'E-mail Connection Required', 0 );
     Handled := True;
end;

procedure TEMailTest.EmailFailure(Sender: TObject);
begin
     ZetaDialogo.zError( '� Hubo Problemas !', 'E-mail General Failure', 0 );
end;

procedure TEMailTest.EmailInvalidHost(var Handled: Boolean);
begin
     ZetaDialogo.zError( '� Hubo Problemas !', Format( 'Servidor Inv�lido: %s', [ GetHost ] ), 0 );
     Handled := True;
end;

procedure TEMailTest.EmailRecipientNotFound(Recipient: String);
begin
     ZetaDialogo.zError( '� Hubo Problemas !', Format( 'Destinatario No Encontrado: %s', [ Recipient ] ), 0 );
end;

procedure TEMailTest.EjecutarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           SendEMail;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TEMailTest.GetPassword: String;
begin
    Result := Self.txtPassword.Text;
end;

function TEMailTest.GetPort: Integer;
begin
     Result := Self.znPuerto.ValorEntero;
     if ( Result = 0 ) then
        Result := 25;
end;

procedure TEMailTest.FormShow(Sender: TObject);
begin
  with Autenticacion do
  begin
       if ItemIndex < 0 then
          ItemIndex := 0;
  end;
end;

end.
