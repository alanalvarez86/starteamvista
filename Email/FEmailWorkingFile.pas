unit FEmailWorkingFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaSystemWorking, ComCtrls, ExtCtrls, StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxGDIPlusClasses, cxImage;

type
  TEmailWorkingFile = class(TZSystemWorking)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmailWorkingFile: TEmailWorkingFile;

procedure InitAnimation( const sMensaje: String );
procedure EndAnimation;

implementation

{$R *.DFM}

procedure InitAnimation( const sMensaje: String );
begin
     EmailWorkingFile := TEmailWorkingFile.Create( Application );
     with EmailWorkingFile do
     begin
          SetMensaje( sMensaje );
          Show;
          Update;
     end;
end;

procedure EndAnimation;
begin
     if ( EmailWorkingFile <> nil ) then
     begin
          with EmailWorkingFile do
          begin
               Close;
               Free;
          end;
          EmailWorkingFile := nil;
     end;
end;

end.
