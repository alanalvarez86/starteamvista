program TressEmail;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Forms,
  SysUtils,
  ZetaClientTools,
  ZetaSplash,
  FReportesMailCFG in 'FReportesMailCFG.pas' {ReportesMailTest},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DReportesGenerador in '..\Datamodules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule};

{$R *.RES}
{$R WindowsXP.res}


{$R ..\Traducciones\Spanish.RES}
begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'Reportes E-mail';
     Application.HelpFile := 'TressEmail.chm';
     Application.CreateForm(TReportesMailTest, ReportesMailTest);
  if ( ( ParamCount > 1 ) ) then
     begin
          try
             ReportesMailTest.ProcesarBatch
          finally
                 FreeAndNil( ReportesMailTest );
          end
     end
     else
     begin
          with ReportesMailTest do
          begin
               if Login (False) then
               begin
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    Free;
               end;
          end;
     end;
end.
