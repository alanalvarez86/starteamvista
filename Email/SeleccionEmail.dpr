program SeleccionEmail;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  ZetaSplash,
  FReportesMailCFG in 'FReportesMailCFG.pas' {ReportesMailTest},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DReportesGenerador in '..\Datamodules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DEntidades in '..\Evaluador\DEntidades.pas' {dmEntidades: TDataModule},
  ZetaTipoEntidad in '..\Seleccion\ZetaTipoEntidad.pas',
  DEntidadesTress in '..\Seleccion\DEntidadesTress.pas' {dmEntidadesTress: TDataModule},
  ZReportModulo in '..\Seleccion\ZReportModulo.pas',
  ZGlobalTress in '..\Seleccion\ZGlobalTress.pas',
  DGlobal in '..\Seleccion\DGlobal.pas',
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  ZetaAdicionalMgr in '..\Seleccion\ZetaAdicionalMgr.pas',
  DDiccionario in 'Seleccion\DDiccionario.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseSeleccionDiccionario in '..\Seleccion\DBaseSeleccionDiccionario.pas' {dmBaseSeleccionDiccionario: TDataModule},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell};

{$R *.RES}
{$R WindowsXP.res}
{$R ..\Traducciones\Spanish.RES}
{procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}
begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'Reportes E-Mail';
     Application.HelpFile := 'TressEmail.chm';
  Application.CreateForm(TReportesMailTest, ReportesMailTest);
  if ( ParamCount > 0 ) then
     begin
          try
             ReportesMailTest.ProcesarBatch
          finally
                 FreeAndNil( ReportesMailTest );
          end
     end
     else
     begin
          {SplashScreen := TSplashScreen.Create( Application );
          with SplashScreen do
          begin
               Show;
               Update;
          end;}
          with ReportesMailTest do
          begin
               if Login( False ) then
               begin
                    //CierraSplash;
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    //CierraSplash;
                    Free;
               end;
          end;
     end;
end.
