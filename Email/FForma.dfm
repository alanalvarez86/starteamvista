object Forma: TForma
  Left = 15
  Top = 106
  Width = 943
  Height = 622
  VertScrollBar.Position = 29
  Caption = 'Forma'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 0
    Top = -29
    Width = 625
    Height = 81
    Caption = 'Generar Reporte'
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -64
    Font.Name = 'Rockwell'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 147
    Width = 809
    Height = 201
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Edit1: TEdit
    Left = 632
    Top = 83
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'DATOS'
  end
  object Edit2: TEdit
    Left = 632
    Top = 59
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '1930'
  end
  object DBGrid2: TDBGrid
    Left = 8
    Top = 355
    Width = 809
    Height = 201
    DataSource = DataSource2
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object BitBtn2: TBitBtn
    Left = 0
    Top = 59
    Width = 625
    Height = 81
    Caption = 'Generar Mail'
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -64
    Font.Name = 'Rockwell'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    OnClick = BitBtn2Click
  end
  object Button1: TButton
    Left = 632
    Top = -29
    Width = 265
    Height = 73
    Caption = 'Genera Un Reporte'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -48
    Font.Name = 'Onyx'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    OnClick = Button1Click
  end
  object BitBtn3: TBitBtn
    Left = 856
    Top = 187
    Width = 75
    Height = 25
    Caption = 'BitBtn3'
    TabOrder = 7
    OnClick = BitBtn3Click
  end
  object RichEdit1: TRichEdit
    Left = 56
    Top = 563
    Width = 481
    Height = 89
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -16
    Font.Name = 'News Gothic MT'
    Font.Style = [fsBold, fsItalic]
    Lines.Strings = (
      'Buenos Dias, tengo entendido que ustedes tienen un '
      'programita donde'
      'apuntan sus horas de trabajo'
      'durante el dia, si me podrian pasar por correo la informacion '
      'de esta'
      'semana para anotarlas en el reporte . Les agradesco de '
      'antemano su'
      'ayuda.'
      'Gracias. :o)')
    ParentFont = False
    TabOrder = 8
  end
  object DataSource1: TDataSource
    DataSet = dmReportesMail.cdsCampoRep
    Left = 16
    Top = 264
  end
  object DataSource2: TDataSource
    DataSet = dmReportesMail.cdsResultados
    Left = 8
    Top = 360
  end
end
