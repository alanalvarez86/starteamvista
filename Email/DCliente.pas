unit DCliente;

interface

{$DEFINE REPORTING}
{.$UNDEF REPORTING}
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
     ZetaClientDataSet,
     DBasicoCliente,
     {$ifdef DOS_CAPAS}
        {$ifdef TRESS}
           {$ifdef REPORTING}
           DServerReporting,
           {$else}
           DServerCalcNomina,
           {$endif}
        DServerSuper,
        DServerGlobal,
        DServerCatalogos,
        {$endif}
        {$ifdef SELECCION}
        DServerGlobalSeleccion,
        DServerSelReportes,
        DServerSeleccion,
        {$endif}
        {$ifdef VISITANTES}
        DServerGlobalVisitantes,
        DServerVisReportes,
        DServerVisitantes,
        {$endif}

     DServerReportes,
     DServerLogin,
     {$endif}
     ZetaCommonLists,
     ZetaCommonClasses;

{$ifdef REPORTING}
{$IFDEF TRESS}
  {$ifdef DOS_CAPAS}
  type
    TdmServerCalcNomina = TdmServerReporting;
 {$ENDIF}
{$endif} 
{$endif}

type
  TdmCliente = class(TBasicoCliente)
    cdsPeriodo: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    FEmpleado: integer;
    FPeriodoActivo :TDatosPeriodo;
    FGeneraListaEmpleados:Boolean;
    //FTipo: eTipoCompany;
    {$ifdef DOS_CAPAS}
        {$ifdef TRESS}
        FServerSuper: TdmServerSuper;
        {$endif}
        FServerGlobal: {$ifdef TRESS}TdmServerGlobal{$endif}{$ifdef SELECCION}TdmServerGlobalSeleccion{$endif}{$ifdef VISITANTES}TdmServerGlobalVisitantes{$endif};
        FServerReporteador: {$ifdef TRESS}TdmServerCalcNomina{$endif}{$ifdef SELECCION}TdmServerSelReportes{$endif}{$ifdef VISITANTES}TdmServerVisReportes{$endif};

    FServerCatalogos: {$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif}{$ifdef VISITANTES}TdmServerVisitantes{$endif};
    FServerReportes: TdmServerReportes;
    FServerLogin: TdmServerLogin;
    {$endif}
    function GetIMSSMes: Integer;
    function GetIMSSPatron: String;
    function GetIMSSTipo: eTipoLiqIMSS;
    function GetIMSSYear: Integer;
    {$ifdef TRESS}
    function GetPeriodoInicial(Parametros:TZetaParams): Boolean;
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo( Parametros: TZetaParams );
    procedure CargaActivosPeriodoLista(Parametros: TZetaParams);
    {$endif}
    procedure CargaActivosSistema( Parametros: TZetaParams );
  protected
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros;
    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;
    property Empleado: integer read FEmpleado write FEmpleado default 0;
    property PeriodoActivo: TDatosPeriodo read FPeriodoActivo  write FPeriodoActivo;
    property GeneraListaEmpleados: Boolean read FGeneraListaEmpleados  write FGeneraListaEmpleados default false;

    {$ifdef DOS_CAPAS}
        property ServerReporteador: {$ifdef TRESS}TdmServerCalcNomina{$endif}{$ifdef SELECCION}TdmServerSelReportes{$endif}{$ifdef VISITANTES}TdmServerVisReportes{$endif} read FServerReporteador;
        property ServerGlobal: {$ifdef TRESS}TdmServerGlobal{$endif}{$ifdef SELECCION}TdmServerGlobalSeleccion{$endif}{$ifdef VISITANTES}TdmServerGlobalVisitantes{$endif} read FServerGlobal;
        {$ifdef TRESS}
        property ServerSuper: TdmServerSuper read FServerSuper;
        {$endif}

    property ServerCatalogos: {$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif}{$ifdef VISITANTES}TdmServerVisitantes{$endif} read FServerCatalogos;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerLogin: TdmServerLogin read FServerLogin;
    {$endif}
    function BuscaCompany( const Codigo: String ): Boolean;
    function Empresa: OleVariant;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    //function ListaCompanies( Lista: TStrings ): Boolean;
    procedure CargaActivosTodos( Parametros: TZetaParams ); override;
    function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String; override;
  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses DDiccionario,
     ZetaRegistryCliente,
     ZetaCommonTools;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FParametros := TZetaParams.Create( Self );

     {$ifdef TRESS}
     TipoCompany := tc3Datos;
     {$endif}
     {$ifdef SELECCION}
     TipoCompany := tcRecluta;
     {$endif}
    {$ifdef VISITANTES}
     TipoCompany := tcVisitas;
     {$endif}

     {$ifdef DOS_CAPAS}
     
       {$ifdef TRESS}
       FServerSuper := TdmServerSuper.Create( Self );
       {$endif}

     FServerGlobal := {$ifdef TRESS}TdmServerGlobal{$endif}{$ifdef SELECCION}TdmServerGlobalSeleccion{$endif}{$ifdef VISITANTES}TdmServerGlobalVisitantes{$endif}.Create( Self );
     FServerReporteador := {$ifdef TRESS}TdmServerCalcNomina{$endif}{$ifdef SELECCION}TdmServerSelReportes{$endif}{$ifdef VISITANTES}TdmServerVisReportes{$endif}.Create( Self );
     FServerCatalogos := {$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif}{$ifdef VISITANTES}TdmServerVisitantes{$endif}.Create( Self );
     FServerReportes := TdmServerReportes.Create( Self );
     FServerLogin := TdmServerLogin.Create( Self );
     {$endif}

     {$IFDEF TRESS_DELPHIXE5_UP}
    // FPeriodoActivo := TDatosPeriodo.Create;
     {$ENDIF}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
         {$ifdef TRESS}
         FreeAndNil( FServerSuper );
         {$endif}
     FreeAndNil( FServerLogin );
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerReporteador );
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerGlobal );
     {$endif}

     {$IFDEF TRESS_DELPHIXE5_UP}
     //FreeAndNil(FPeriodoActivo);
     {$ENDIF}
end;

{$ifdef TRESS}
function TdmCliente.GetPeriodoInicial(Parametros:TZetaParams): Boolean;
 function GetParametro( const sName:string; const iValor: integer ):integer;
 begin
      if ( Parametros.FindParam(sName) <> NIL) then
      begin
           Result := Parametros.ParamByName(sName).AsInteger;
           if ( Result = -1 ) then
              Result := iValor;
      end
      else
          Result := iValor;
 end;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, GetParametro( 'YEAR', TheYear(Date) ), GetParametro( 'TIPO', ClientRegistry.TipoNomina )  );
          Result := not IsEmpty;
     end;
end;
{$endif}

{function TdmCliente.ListaCompanies( Lista: TStrings ): Boolean;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             with cdsCompany do
             begin
                  Data := Servidor.GetCompanys( Usuario, ord( FTipo ) );
                  while not Eof do
                  begin
                       Add( Format( '%s=%s', [ FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ) );
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
          Result := ( Count > 0 );
     end;
end;}

function TdmCliente.BuscaCompany( const Codigo: String ): Boolean;
begin
     cdsCompany.Data := Servidor.GetCompanys( 0, Ord(TipoCompany) );
     Result := cdsCompany.Locate( 'CM_CODIGO', Codigo, [] );
     if Result then
     begin
          ClientRegistry.Compania := Codigo;
     end;

end;

function TdmCliente.Empresa: OleVariant; {Creo que este metodo repite logica que ya se esta haciendo en DBaseCliente}
begin
     Result := BuildEmpresa( cdsCompany );
end;

procedure TdmCliente.CargaActivosSistema;
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          if GeneraListaEmpleados then
          	AddInteger( 'EmpleadoActivo', FEmpleado )
          else
          		AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', 'REP_EMAIL' );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosTodos( Parametros: TZetaParams );
begin
     {$ifdef TRESS}
     CargaActivosIMSS( Parametros );
     if FGeneraListaEmpleados then
       CargaActivosPeriodoLista (Parametros)
     else
     	 CargaActivosPeriodo( Parametros );
     {$endif}
     CargaActivosSistema( Parametros );
end;

{$ifdef TRESS}
procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', ImssPatron );
          AddInteger( 'IMSSYear', ImssYear );
          AddInteger( 'IMSSMes', ImssMes );
          AddInteger( 'IMSSTipo', Ord( ImssTipo ) );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with cdsPeriodo do
     begin
          if IsEmpty then
             GetPeriodoInicial(Parametros);
          with Parametros do
          begin
               AddInteger( 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
               AddInteger( 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
               AddInteger( 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
          end;
     end;
end;

procedure TdmCliente.CargaActivosPeriodoLista(Parametros: TZetaParams);
begin
     with cdsPeriodo do
     begin
          if IsEmpty then
             GetPeriodoInicial(Parametros);
          with Parametros do
          begin
               AddInteger( 'Year', FPeriodoActivo.Year );
               AddInteger( 'Tipo', Ord(FPeriodoActivo.Tipo ));
               AddInteger( 'Numero', FPeriodoActivo.Numero );
          end;
     end;
end;
{$endif}

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          with cdsPeriodo do
          begin
               if FGeneraListaEmpleados then
               begin
                    Year := FPeriodoActivo.Year;
               		Tipo := FPeriodoActivo.Tipo;
                    Numero := FPeriodoActivo.Numero;  
               end
               else
               begin
               	    Year := FieldByName( 'PE_YEAR' ).AsInteger;
                    Tipo := eTipoPeriodo(FieldByName( 'PE_TIPO' ).AsInteger);
                    Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               end;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;


function TdmCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := TheMonth(Date);
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := tlOrdinaria;
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := TheYear( Date );
end;

function TdmCliente.GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin
     Result := dmDiccionario.GetValorActivo(eValor);
end;

end.
