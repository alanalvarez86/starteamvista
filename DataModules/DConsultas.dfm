object dmConsultas: TdmConsultas
  OldCreateOrder = False
  Height = 479
  Width = 741
  object cdsQueryGeneral: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsQueryGeneralAlAdquirirDatos
    AlCrearCampos = cdsQueryGeneralAlCrearCampos
    Left = 40
    Top = 16
  end
  object cdsBitacora: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsBitacoraAfterOpen
    AlCrearCampos = cdsBitacoraAlCrearCampos
    AlModificar = cdsBitacoraAlModificar
    Left = 40
    Top = 64
  end
  object cdsLogDetail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsLogDetailAfterOpen
    AlModificar = cdsLogDetailAlModificar
    Left = 40
    Top = 161
  end
  object cdsProcesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsProcesosAfterOpen
    OnCalcFields = cdsProcesosCalcFields
    AlCrearCampos = cdsProcesosAlCrearCampos
    AlModificar = cdsProcesosAlModificar
    Left = 40
    Top = 113
  end
  object cdsTableroRolesInformacion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsTableroRolesInformacionAlAdquirirDatos
    AlBorrar = cdsTableroRolesInformacionAlBorrar
    Left = 40
    Top = 225
  end
  object cdsTREmpresa: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsTREmpresaAlAdquirirDatos
    Left = 40
    Top = 281
  end
  object cdsTRTablero: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsTRTableroAlAdquirirDatos
    Left = 48
    Top = 337
  end
end
