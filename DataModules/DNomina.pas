unit DNomina;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     {$ifdef TRESS_DELPHIXE5_UP}System.UITypes,{$endif}
     Db, ZetaClientDataSet, DBClient, Provider,
     {$ifndef VER130}Variants,{$endif}
{$IFDEF DOS_CAPAS}
     DServerNomina,
{$else}
     Nomina_TLB,
{$endif}
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZetaCommonLists;

{$define QUINCENALES}
{$define CAMBIO_TNOM}
{.$undefine QUINCENALES}

//Derechos para simulacion de finiquitos
const
     K_DERECHO_LIQUID_GLOBAL =  K_DERECHO_BAJA;
     K_DERECHO_APROBA_GLOBAL =  K_DERECHO_CAMBIO;
     K_DERECHO_APLICA_GLOBAL =  K_DERECHO_IMPRESION;
     K_DERECHO_BORRAR_GLOBAL =  K_DERECHO_SIST_KARDEX;
     K_DERECHO_IMPRESION_GLOBAL = K_DERECHO_ALTA;

type

 TDatosNominaEmpleado = record
    Empleado               : Integer;
    FecCambioTNomina       : TDate;
    EsCambioEnPeriodo      : Boolean;
    TNominaInicio          : eTipoPeriodo;
    TNominaFin             : eTipoPeriodo;
    FecNominaInicio           : TDate;
    FecNominaFin              : TDate;
  end;

  TdmNomina = class(TDataModule)
    cdsDatosClasifi: TZetaClientDataSet;
    cdsDatosAsist: TZetaClientDataSet;
    cdsTotales: TZetaClientDataSet;
    cdsMontos: TZetaClientDataSet;
    cdsExcepGlobales: TZetaClientDataSet;
    cdsExcepMontos: TZetaClientDataSet;
    cdsExcepciones: TZetaClientDataSet;
    cdsDatosNomina: TZetaClientDataSet;
    cdsMovDatosAsist: TZetaClientDataSet;
    cdsMovMontos: TZetaClientDataSet;
    cdsMovDiasHoras: TZetaClientDataSet;
    cdsPagoRecibos: TZetaClientDataSet;
    cdsLiquidacion: TZetaClientDataSet;
    cdsAhorros: TZetaClientDataSet;
    cdsPrestamos: TZetaClientDataSet;
    cdsErrores: TZetaClientDataSet;
    cdsPrestamosRef: TZetaClientDataSet;
    cdsPolizas: TZetaClientDataSet;
    cdsSimulaMontos: TZetaClientDataSet;
    cdsSimulaTotales: TZetaClientDataSet;
    cdsSimulaLiquidacion: TZetaClientDataSet;
    cdsFonTot: TZetaClientDataSet;
    cdsFonEmp: TZetaClientDataSet;
    cdsFoncre: TZetaClientDataSet;
    cdsSimulaGlobales: TZetaClientDataSet;
    cdsSimGlobalTotales: TZetaClientDataSet;
    cdsSimGlobalesMovimien: TZetaClientDataSet;
    cdsGridAprobados: TZetaClientDataSet;
    cdsDeclaracionesAnuales: TZetaClientDataSet;
    cdsFonTotDetalle: TZetaClientDataSet;
    cdsFonTotDiferencias: TZetaClientDataSet;
    cdsDetalleImportacion: TZetaClientDataSet;
    cdsFonTotRs: TZetaClientDataSet;
    cdsDatosClasifiAfectacion: TZetaClientDataSet;
    procedure cdsTotalesAfterOpen(DataSet: TDataSet);
    procedure cdsDatosNominaAfterOpen(DataSet: TDataSet);
    procedure cdsDatosClasifiAfterOpen(DataSet: TDataSet);
    procedure cdsDatosNominaAlAdquirirDatos(Sender: TObject);
    procedure dmNominaCreate(Sender: TObject);
    procedure cdsDatosAsistAfterOpen(DataSet: TDataSet);
    procedure cdsDatosAsistAlAdquirirDatos(Sender: TObject);
    procedure cdsExcepGlobalesAfterOpen(DataSet: TDataSet);
    procedure cdsMontosAlAdquirirDatos(Sender: TObject);
    procedure cdsDatosAsistAlModificar(Sender: TObject);
    procedure cdsDatosAsistAlEnviarDatos(Sender: TObject);
    procedure cdsDatosAsistAfterCancel(DataSet: TDataSet);
    procedure cdsMovDatosAsistAlCrearCampos(Sender: TObject);
    procedure cdsDatosAsistAlCrearCampos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsPagoRecibosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsExcepcionesReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsExcepMontosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsMovMontosReconcileError(DataSet: TClientDataSet;E: EReconcileError; UpdateKind: TUpdateKind;var Action: TReconcileAction);
    procedure cdsMovDiasHorasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsMovDatosAsistReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsExcepGlobalesReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsPagoRecibosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsExcepcionesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsExcepMontosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsMovMontosReconcileError(DataSet: TCustomClientDataSet;E: EReconcileError; UpdateKind: TUpdateKind;var Action: TReconcileAction);
    procedure cdsMovDiasHorasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsMovDatosAsistReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsExcepGlobalesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsMovDatosAsistBeforePost(DataSet: TDataSet);
    procedure cdsMovDiasHorasAlCrearCampos(Sender: TObject);
    procedure cdsMovDiasHorasAfterPost(DataSet: TDataSet);
    procedure cdsMovDiasHorasNewRecord(DataSet: TDataSet);
    procedure cdsMovDiasHorasBeforeOpen(DataSet: TDataSet);
    procedure cdsDatosNominaAlCrearCampos(Sender: TObject);
    procedure cdsMovDiasHorasAlAdquirirDatos(Sender: TObject);
    procedure cdsMovDiasHorasAlEnviarDatos(Sender: TObject);
    procedure cdsMovDiasHorasAlModificar(Sender: TObject);
    procedure cdsMovDiasHorasBeforePost(DataSet: TDataSet);
    procedure cdsMovMontosAlCrearCampos(Sender: TObject);
    procedure cdsMontosAlCrearCampos(Sender: TObject);
    procedure cdsMovMontosAlEnviarDatos(Sender: TObject);
    procedure cdsMovMontosAlAdquirirDatos(Sender: TObject);
    procedure cdsMovMontosAlModificar(Sender: TObject);
    procedure cdsMovMontosBeforePost(DataSet: TDataSet);
    procedure cdsMovMontosNewRecord(DataSet: TDataSet);
    procedure cdsMovMontosAfterPost(DataSet: TDataSet);
    procedure cdsMovMontosAfterOpen(DataSet: TDataSet);
    procedure cdsMovMontosCO_NUMEROChange(Sender: TField);
    procedure cdsMovMontosMO_PERCEPCChange(Sender: TField);
    procedure cdsMovMontosMO_DEDUCCIChange(Sender: TField);
    procedure cdsDatosClasifiAlCrearCampos(Sender: TObject);
    procedure cdsTotalesAlAdquirirDatos(Sender: TObject);
    procedure cdsExcepGlobalesAlAdquirirDatos(Sender: TObject);
    procedure cdsExcepcionesAlAdquirirDatos(Sender: TObject);
    procedure PE_MESGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure AU_FECHAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FA_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FA_DIA_HORChange(Sender: TField);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure HO_CODIGOChange(Sender: TField);
    procedure AU_HORASChange(Sender: TField);
    procedure AU_DES_TRAChange(sender: TField);
    procedure DoblesTriplesChange(Sender: TField);
    procedure FE_INCIDEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure NO_STATUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PE_STATUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PE_STATUSTIMBRADOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PR_FON_MOTGetText(Sender: TField; var Text: String; DisplayText: Boolean);

    

    // cdsExcepMontos
    procedure cdsExcepMontosAlAdquirirDatos(Sender: TObject);
    procedure cdsExcepMontosNewRecord(DataSet: TDataSet);
    procedure cdsExcepMontosBeforePost(DataSet: TDataSet);
    procedure cdsExcepMontosAfterEdit(DataSet: TDataSet);
    procedure cdsExcepMontosAlCrearCampos(Sender: TObject);
    procedure cdsExcepMontosAlEnviarDatos(Sender: TObject);
    procedure cdsExcepMontosCB_CODIGOChange(Sender: TField);
    procedure cdsExcepcionesCB_CODIGOChange(Sender: TField);
    procedure cdsPagoRecibosCB_CODIGOValidate(Sender: TField);
    procedure cdsPagoRecibosCB_CODIGOChange(Sender: TField);
    procedure cdsPagoRecibosNO_FEC_PAGGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsExcepMontosCO_NUMEROValidate(Sender: TField);
    procedure cdsExcepMontosCO_NUMEROChange(Sender: TField);
    procedure cdsExcepMontosMO_DEDUCCIGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsExcepMontosMO_PERCEPCGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsExcepMontosAfterPost(DataSet: TDataSet);
    procedure cdsExcepMontosAlModificar(Sender: TObject);
    procedure cdsDatosClasifiAlAdquirirDatos(Sender: TObject);
    procedure cdsDatosClasifiAfterEdit(DataSet: TDataSet);
    procedure cdsDatosClasifiAlModificar(Sender: TObject);
    procedure cdsDatosClasifiAlEnviarDatos(Sender: TObject);
    procedure cdsDatosClasifiAfterDelete(DataSet: TDataSet);
    procedure cdsExcepcionesAlModificar(Sender: TObject);
    procedure cdsExcepcionesAlCrearCampos(Sender: TObject);
    procedure cdsExcepcionesAlEnviarDatos(Sender: TObject);
    procedure cdsExcepGlobalesAlCrearCampos(Sender: TObject);
    procedure cdsExcepcionesNewRecord(DataSet: TDataSet);
    procedure cdsExcepcionesBeforePost(DataSet: TDataSet);
    procedure cdsExcepcionesBeforeOpen(DataSet: TDataSet);
    procedure cdsExcepGlobalesAlModificar(Sender: TObject);
    procedure cdsExcepGlobalesNewRecord(DataSet: TDataSet);
    procedure cdsExcepGlobalesAfterEdit(DataSet: TDataSet);
    procedure cdsExcepGlobalesAfterDelete(DataSet: TDataSet);
    procedure cdsExcepGlobalesAlEnviarDatos(Sender: TObject);
    procedure cdsExcepGlobalesBeforePost(DataSet: TDataSet);
    procedure cdsExcepMontosAlBorrar(Sender: TObject);
    procedure cdsExcepcionesAlBorrar(Sender: TObject);
    procedure cdsMovDiasHorasAlBorrar(Sender: TObject);
    procedure cdsPagoRecibosAlAdquirirDatos(Sender: TObject);
    procedure cdsPagoRecibosAlEnviarDatos(Sender: TObject);
    procedure cdsPagoRecibosBeforeOpen(DataSet: TDataSet);
    procedure cdsPagoRecibosAfterPost(DataSet: TDataSet);
    procedure cdsPagoRecibosAlCrearCampos(Sender: TObject);
    procedure cdsPagoRecibosBeforePost(DataSet: TDataSet);
    procedure cdsPagoRecibosNewRecord(DataSet: TDataSet);
    procedure cdsLiquidacionAlAdquirirDatos(Sender: TObject);
    procedure cdsLiquidacionAlEnviarDatos(Sender: TObject);
    procedure cdsLiquidacionAlModificar(Sender: TObject);
    procedure cdsDatosClasifiNewRecord(DataSet: TDataSet);
    procedure ExcepcionesCB_CODIGOValidate(Sender: TField);
    procedure cdsExcepMontosMO_REFERENValidate(Sender: TField);
    procedure ExcepDiasHorasAfterPost(DataSet: TDataSet);
    procedure cdsMovDatosAsistAfterEdit(DataSet: TDataSet);
    procedure MO_PERCEPCGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure MO_DEDUCCIGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsMovDatosAsistAfterOpen(DataSet: TDataSet);
    procedure cdsMovMontosAlBorrar(Sender: TObject);
    procedure cdsMovDatosAsistBeforeInsert(DataSet: TDataSet);
    procedure cdsPolizasAlAdquirirDatos(Sender: TObject);
    procedure cdsPolizasAlCrearCampos(Sender: TObject);
    procedure cdsPolizasAfterDelete(DataSet: TDataSet);
    procedure cdsPolizasAlEnviarDatos(Sender: TObject);
    procedure cdsPolizasAlAgregar(Sender: TObject);
    procedure cdsSimulaMontosAlAdquirirDatos(Sender: TObject);
    procedure cdsSimulaMontosAlEnviarDatos(Sender: TObject);
    procedure cdsSimulaMontosAlModificar(Sender: TObject);
    procedure cdsSimulaMontosBeforePost(DataSet: TDataSet);
    procedure cdsSimulaLiquidacionAlAdquirirDatos(Sender: TObject);
    procedure cdsSimulaLiquidacionAlEnviarDatos(Sender: TObject);
    procedure cdsSimulaLiquidacionAlModificar(Sender: TObject);
    procedure cdsFonTotAlAdquirirDatos(Sender: TObject);
    procedure cdsFonTotAlCrearCampos(Sender: TObject);
    procedure cdsFonTotAlEnviarDatos(Sender: TObject);
    procedure cdsFonTotAlBorrar(Sender: TObject);
    procedure cdsFonacotAdquirirDatos(Sender: TObject);
    procedure cdsFonEmpAlCrearCampos(Sender: TObject);
    procedure cdsFoncreAlCrearCampos(Sender: TObject);
    procedure cdsSimulaTotalesAlEnviarDatos(Sender: TObject);
    procedure cdsSimulaGlobalesAlAdquirirDatos(Sender: TObject);
    procedure cdsSimulaGlobalesAlCrearCampos(Sender: TObject);
    procedure CB_FEC_ANTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsSimGlobalTotalesAlAdquirirDatos(Sender: TObject);
    procedure cdsSimGlobalTotalesAlCrearCampos(Sender: TObject);
    procedure cdsSimGlobalesMovimienAlCrearCampos(Sender: TObject);
    procedure cdsGridAprobadosAlCrearCampos(Sender: TObject);
    procedure NO_APROBA_GridGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsGridAprobadosAlEnviarDatos(Sender: TObject);
    procedure cdsGridAprobadosAlAdquirirDatos(Sender: TObject);
    procedure ACTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsExcepGlobalCO_NUMEROValidate(Sender: TField);
    procedure cdsDeclaracionesAnualesAlAdquirirDatos(Sender: TObject);
    procedure cdsFoncreCalcFields(DataSet: TDataSet);
    procedure cdsFonTotCalcFields(DataSet: TDataSet);
    procedure cdsFonTotDetalleAlAdquirirDatos(Sender: TObject);
    procedure cdsFonTotDiferenciasAlAdquirirDatos(Sender: TObject);
    procedure cdsFonTotDiferenciasAlCrearCampos(Sender: TObject);
    procedure cdsFonTotDetalleAlCrearCampos(Sender: TObject);
    procedure cdsDetalleImportacionAlAdquirirDatos(Sender: TObject);
    procedure cdsFonTotRsAlAdquirirDatos(Sender: TObject);
    procedure cdsFonTotRsAlCrearCampos(Sender: TObject);
    procedure cdsFonTotDetalleAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FLastConcepto: Integer;
    FLastMonto: TPesos;
    FLastReferen: String;
    FDetalleImportacion : String;
    FSoloAltas, FEditandoGrid: Boolean;
    FMarcarRecibosPagados: Boolean;
    FOperacion: Integer;
    //FUltimoConcepto: Integer;
    //FUltimaDeduccion: TPesos;
    //FUltimaPercepcion: TPesos;
    FUltimaFecha: TDateTime;
    FUltimoTipo: String;  // (D/H) char(1)
    FUltimoTipoHoras: eMotivoFaltaHoras;
    FUltimoTipoDias: eMotivoFaltaDias;
    FUltimoHoras: TDiasHoras;
    FUltimoDias: TDiasHoras;
    FUltimoPagoRecibo: TDateTime;
    FConflictoMontos, FConflictoFaltas: Boolean;
    FFechaBajaLiquidacion : TDate;
    FRefrescaMovMontos : Boolean;
    FEmpleadoGrid: TNumEmp;
    FNivelConfidencialidad :string;
    FNominaEmpleado: TDatosNominaEmpleado;
    FFiltroSimulacion:Integer;
    FAplicandoBaja:Boolean;
    FFonacotSoloDiferencias: Boolean;
    FTodasLasRazones: Boolean;
{$ifdef DOS_CAPAS}
    function GetServerNomina: TdmServerNomina;
    property ServerNomina: TdmServerNomina read GetServerNomina;
{$else}
    FServidor: IdmServerNominaDisp;
    function GetServerNomina: IdmServerNominaDisp;
    property ServerNomina: IdmServerNominaDisp read GetServerNomina;
{$endif}

    function GetMsgBorrarPeriodo( const lActiva: Boolean ): String;
    procedure GrabaExcepcionesDiasHoras(Dataset: TZetaClientDataset);
    procedure GrabaExcepcionesMonto(Dataset: TZetaClientDataset);
    procedure GrabaExcepcionesMontoParams( Dataset: TZetaClientDataset; oParams: TZetaParams );
    procedure ValidaEmpleadoBaja( const iEmpleado: Integer );
    procedure ValidaFechaDiasHoras( DataSet: TDataSet);
    procedure ProcesaLiquidacion( oClientDataSet: TZetaClientDataSet );
    procedure GrabaLiquidacionParams( Parametros: TZetaParams; oClientDataset: TZetaClientDataSet);
    procedure GetDatosLiquidacion( Parametros: TZetaParams; oClientDataSet: TZetaClientDataSet );
    function GetMontoAnterior(const iEmpleado, iConcepto: Integer; const sReferencia: String): Currency;
    function GetDiaHoraAnterior(const iEmpleado, iMotivo: Integer; const dFecha: TDate; const sDiaHora: String): Currency;
    {$ifdef VER130}
    function OperaConflictoMontos(DataSet: TClientDataSet): eOperacionConflicto;
    function OperaConflictoDiasHoras(DataSet: TClientDataSet): eOperacionConflicto;
    {$else}
    function OperaConflictoMontos(DataSet: TCustomClientDataSet): eOperacionConflicto;
    function OperaConflictoDiasHoras(DataSet: TCustomClientDataSet): eOperacionConflicto;
    {$endif}
    function GetEmpleadoGrid: Integer;
    function GetReferenciaPrestamo( const iEmpleado: integer; const sTipo: string; var sReferencia: string ): Boolean;
    function ValidaReferenciaPrestamo( const iEmpleado: integer; const sTipo, sReferencia: string ): Boolean;
    procedure ConectaFonTot( const iMes, iYear: Integer);
    function GetTipoPeriodo(const iEmpleado: Integer; const dFecha: TDate ): eTipoPeriodo;
    function VerificaGrupoRegistroExcepciones(var sMensaje:string):Boolean;
  public
    { Publid declarations }
    property SoloAltas: Boolean read FSoloAltas write FSoloAltas;
    property MarcarRecibosPagados: Boolean read FMarcarRecibosPagados write FMarcarRecibosPagados;
    property OperacionConflicto: Integer read FOperacion write FOperacion;
    property ConflictoMontos: Boolean read FConflictoMontos write FConflictoMontos;
    property ConflictoFaltas: Boolean read FConflictoFaltas write FConflictoFaltas;
    property FechaBajaLiquidacion: TDate read FFechaBajaLiquidacion write FFechaBajaLiquidacion;
    property EmpleadoGrid: TNumEmp read FEmpleadoGrid write FEmpleadoGrid;
    property NivelConfidencialidad: string read FNivelConfidencialidad write FNivelConfidencialidad;
    property FiltroSimulacion:Integer read FFiltroSimulacion write FFiltroSimulacion;
    property FonacotSoloDiferencias: Boolean read FFonacotSoloDiferencias write FFonacotSoloDiferencias;
    property TodasLasRazones: Boolean read FTodasLasRazones write FTodasLasRazones;
    property DetalleImportacion : String read FDetalleImportacion write FDetalleImportacion;

    //US #17507: Es necesario que la nomina desafectadas de forma parcial se puedan calcular nuevamente para as� realizar los ajustes correspondientes
    function ValidaAfectada(var sMensaje: String; const lActiva: Boolean; const sOperacion: String; const bValidacionAfectadaParcial: Boolean = false): Boolean;
    function BajaNominaAnterior(oDataSet: TDataSet; var sMensaje: String): Boolean;
    //US #17507: Es necesario que la nomina desafectadas de forma parcial se puedan calcular nuevamente para as� realizar los ajustes correspondientes
    function ValidaStatusEmpleado(const iNumeroEmpleado: Integer; var sMensaje: String): Boolean;
    function ConceptoEsDeduccion( DataSet : TDataSet ) : Boolean;
    function TipoNominaDiferente( oDataSet: TDataSet; var sMensaje: String) : Boolean;
    function ExistePeriodoSimulacion( var sMensaje: String; const sOperacion: String): Boolean;
    procedure EditarGridMontos( const lSoloAltas: Boolean );
    procedure EditarGridDiasHoras( const lSoloAltas: Boolean );
    procedure EditarGridPagoRecibos;
    procedure EditarGridPrenomina;
    procedure EditarGridMovMontos;
    procedure EditarGridSimulacionMontos;
    procedure LiquidaEmpleadoActivo;
    procedure AgregaExcepGlobales;
    procedure BorrarNominaActiva( const lActiva: Boolean );
    procedure SimulaLiquidacion;
    procedure AplicaFiniquito;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure AbreCierraCalculo( const eAccion: eSubeBaja );
    function GetStatusFonacot( const iMes, iYear: Integer ): eStatusLiqImss;
    function GetStatusAnterior: eStatusSimFiniquitos;
    function RequiereAprobacionRH: Boolean;
    function StatusSimulacionActual :eStatusSimFiniquitos;
    procedure ProcesaAprobacion;
    procedure ProcesaDesAprobacion;

    procedure BorrarFiniquito;
    procedure BorrarFiniquitosGlobales;
    procedure BorrarFiniquitoGlobal;
    
    function HayConflictoLimites(iConcepto:Integer;Monto:TPesos;DataSet:TDataSet;var sMensaje:string;var lMensaje:Boolean):Boolean;
    function PasaLimiteMonto(iConcepto:Integer;Monto:TPesos):Boolean;
    function DerechoSoloAplicarGlobal: Boolean;
    function DerechoSoloAplicarIndividual: Boolean;
    property AplicandoBaja: Boolean read FAplicandoBaja write FAplicandoBaja;
    procedure GetDatosNominaClasifi( const iEmpleado,iYear,iPeriodo,iTipo:Integer );
    //US #17507: Es necesario que la nomina desafectadas de forma parcial se puedan calcular nuevamente para as� realizar los ajustes correspondientes
    procedure GetDatosNominaClasifiAfectacion( const iEmpleado,iYear,iPeriodo,iTipo:Integer );
    procedure AplicarLiquidacion;
    function FonacotTipoCedula(Anio, Mes: Integer; RazonSocial : String) : Integer;
    procedure ConectaFonTotRS( const iMes, iYear: Integer);
  end;

var
  dmNomina: TdmNomina;

implementation

uses DCliente,
     DCatalogos,
     DGlobal,
     DTablas,
     DSistema,
     DRecursos,
     ZBaseDlgModal_DevEx,
     ZBaseEdicion_DevEx,
     ZBaseGridEdicion_DevEx,
     ZReconcile,
     ZetaMsgDlg,
     ZetaCommonTools,
     ZetaClientTools,
     ZGlobalTress,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     FGridPrenomina_DevEx,
     FNomEditDatosDiasHoras_DevEx,
     FNomEditDatosMontos_DevEx,
     FNomEditDatosMontosSimulacion_DevEx,
     FNomEditExcepMontos_DevEx,
     FGridNomMontos_DevEx,
     FGridMovMontos_DevEx,
     FGridSimulacionMontos_DevEx,
     FNomEditDatosClasifi_DevEx,
     FNomEditExcepDiasHoras_DevEx,
     FNomEditExcepGlobales_DevEx,
     FGridDiasHoras_DevEx,
     FPagoRecibos_DevEx,
     FGridPagoRecibos_DevEx,
     FEditLiquidacion_DevEx,
     FEditSimulaLiquidacion_DevEx,
     FBorraNomina_DevEx,
     FMontoRepetido_DevEx,
     FFaltaRepetida_DevEx,
     {$ifdef INTERFAZ_ELECTROLUX}
      Dinterfase,
     {$Endif}
     {$ifdef LITTELFUSE_INTERFAZ}
      Dinterfase,
      {$endif}
     FTressShell, Math, ExtCtrls;


{$R *.DFM}

{ ********** TdmNomina ************* }

procedure TdmNomina.dmNominaCreate(Sender: TObject);
begin
     //FUltimoConcepto := 1;
     //FUltimaDeduccion := 0;
     //FUltimaPercepcion :=0;
     FLastConcepto := 0;
     FLastMonto := 0;
     FSoloAltas := FALSE;
     FEditandoGrid:= FALSE;
     FOperacion := Ord( ocReportar );
     with cdsPrestamosRef do
     begin
          IndexFieldNames := 'PR_TIPO;PR_REFEREN';
          Filter := Format( 'PR_STATUS = %d', [ ord(spActivo) ] );
          Filtered := TRUE;
     end;
     with FNominaEmpleado do
     begin
          Empleado:= 0;
          FecCambioTNomina:= NullDateTime;
          EsCambioEnPeriodo:= False;
          TNominaInicio:= Ord(tpDiario); 
          TNominaFin:= Ord(tpDiario);
          FecNominaInicio:= NullDateTime;
          FecNominaFin:= NullDateTime;
     end;
     FFiltroSimulacion := -1;
     FAplicandoBaja := False;
     FFonacotSoloDiferencias := TRUE;
     FTodasLasRazones := FALSE;
end;

{$ifdef DOS_CAPAS}

function TdmNomina.GetServerNomina: TdmServerNomina;
begin
     Result := DCliente.dmCliente.ServerNomina;
end;
{$else}
function TdmNomina.GetServerNomina: IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( dmCliente.CreaServidor( CLASS_dmServerNomina, FServidor ) );
end;
{$endif}

{$ifdef VER130}
procedure TdmNomina.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmNomina.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

procedure TdmNomina.ValidaFechaDiasHoras( Dataset: TDataset );
var
   dInicio, dfin: TDate;
begin
     with dmCliente.GetDatosPeriodoActivo do
     begin
          {$ifdef CAMBIO_TNOM}
          with FNominaEmpleado do
          begin
               dInicio:= FecNominaInicio;
               dFin:= FecNominaFin;
          end;
          {$else}

          {$ifdef QUINCENALES}
          dInicio := InicioAsis;
          dfin := FinAsis;
          {$else}
          dInicio := Inicio;
          dfin := Fin;
          {$endif}
          {$endif}
     end;
     with Dataset do
     begin
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
          begin
               case eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger ) of
                    mfdInjustificada:
                    begin
                         {$ifdef QUINCENALESFALSE}
                         with dmCliente.GetDatosPeriodoActivo do
                         begin
                         {$endif}
                              if ( FieldByName( 'FA_FEC_INI' ).AsDateTime < dInicio )  then
                                 DatabaseError( 'La Fecha De Inicio No Puede Ser Anterior Al Inicio De la N�mina' );
                              if ( ( FieldByName( 'FA_FEC_INI' ).AsDateTime + FieldByName( 'FA_DIAS' ).AsInteger - 1 ) > dFin ) then
                                 DataBaseError( 'Fecha + D�as No Puede Ser Superior Al Fin De la N�mina');
                         {$ifdef QUINCENALESFALSE}
                         end;
                         {$endif}
                    end;
                    mfdAjuste:
                    begin
                         {$ifdef QUINCENALESFALSE}
                         with dmCliente.GetDatosPeriodoActivo do
                         begin
                         {$endif}
                              if ( FieldByName( 'FA_FEC_INI' ).AsDateTime >= dInicio ) or
                                 ( ( FieldByName( 'FA_FEC_INI' ).AsDateTime + FieldByName( 'FA_DIAS' ).AsInteger - 1 ) >= dInicio ) then
                              begin
                                   DatabaseError( 'Fecha + D�as No Puede Quedar Dentro del Rango de Fechas de N�mina');
                              end;
                         {$ifdef QUINCENALESFALSE}
                         end;
                         {$endif}
                    end;
               end;
          end
          else       // Tipo = Horas
          begin
               {$ifdef QUINCENALESFALSE}
               with dmCliente.GetDatosPeriodoActivo do
               {$endif}
                      if ( FieldByName( 'FA_FEC_INI' ).AsDateTime < dInicio ) or
                       ( ( FieldByName( 'FA_FEC_INI' ).AsDateTime > dFin ) ) then
                       DatabaseError( 'Fecha Debe Quedar Dentro del Rango de Fechas de N�mina');
          end;
     end;
end;

function TdmNomina.BajaNominaAnterior(oDataSet: TDataSet; var sMensaje: String): Boolean;
{
const
     K_MENSAJE_BAJA = 'No Es Posible Capturar Excepciones Para Empleados Dados de Baja';
}
begin
     with oDataSet do
     begin
          Result := ZetaCommonTools.EsBajaNominaAnterior( zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ),
                                                          FieldByName( 'CB_NOMYEAR' ).AsInteger,
                                                          FieldByName( 'CB_NOMTIPO' ).AsInteger,
                                                          FieldByName( 'CB_NOMNUME' ).AsInteger,
                                                          Global.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ),
                                                          FieldByName( 'CB_FEC_BAJ' ).AsDateTime,
                                                          dmCliente.GetDatosPeriodoActivo,
                                                          TRUE, sMensaje );
{
          result := ( not zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) );
          if Result then
          begin
               if ( PeriodoNumero < K_LIMITE_NOM_NORMAL ) then    // Ordinaria
                  Result := ( not IncluyeBaja( FieldByName( 'CB_NOMYEAR' ).AsInteger,
                                               FieldByName( 'CB_NOMTIPO' ).AsInteger,
                                               FieldByName( 'CB_NOMNUME' ).AsInteger,
                                               FieldByName( 'CB_FEC_BAJ' ).AsDateTime,
                                               GetDatosPeriodoActivo ) )
               else                               // Especial
                  Result := ( not GetDatosPeriodoActivo.IncluyeBajas );
          end;
          if Result then
          begin
               sMensaje := K_MENSAJE_BAJA;
               if ( PeriodoNumero < K_LIMITE_NOM_NORMAL ) then
                  sMensaje := sMensaje + ' En Per�odos Anteriores';
          end;
}
     end;
end;

//US #17507: Es necesario que la nomina desafectadas de forma parcial se puedan calcular nuevamente para as� realizar los ajustes correspondientes
function TdmNomina.ValidaStatusEmpleado(const iNumeroEmpleado: Integer; var sMensaje: String): Boolean;
var
   Estado: eStatusPeriodo;
begin
     Result := True;
     with dmNomina do
     begin
          if ( iNumeroEmpleado > 0 ) then
          begin
               GetDatosNominaClasifiAfectacion(iNumeroEmpleado, dmCliente.GetDatosPeriodoActivo.Year, dmCliente.GetDatosPeriodoActivo.Numero, dmCliente.GetDatosPeriodoActivo.Tipo);
               if( not cdsDatosClasifiAfectacion.IsEmpty ) then
               begin
                    Estado := eStatusPeriodo( cdsDatosClasifiAfectacion.FieldByName( 'NO_STATUS' ).AsInteger );
                    if Estado in [ spAfectadaTotal ] then
                    begin
                         sMensaje := Format( 'No es posible realizar excepciones para el empleado %d: %s  ya est� afectado en el periodo.', [ iNumeroEmpleado, dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr(iNumeroEmpleado ) ) ] );
                         Result := False;
                    end;
               end;
          end;
     end;
end;

function TdmNomina.TipoNominaDiferente( oDataSet: TDataSet; var sMensaje: String) : Boolean;
var
   eTipo: eTipoPeriodo;
   oDatosPeriodo: TDatosPeriodo;

   procedure ValidaFechaCambioTNom;
   begin
        with FNominaEmpleado do
        begin
             if ( FecCambioTNomina > oDatosPeriodo.Fin ) then
             begin
                  FecCambioTNomina:= ServerNomina.GetFecCambioTNom( dmCliente.Empresa, FNominaEmpleado.Empleado, oDatosPeriodo.Fin );
             end;
        end;
   end;

   procedure SetValoresNomina;
   begin
        { Se inicializan los valores del empleado respecto a su posible cambio de tipo de n�mina }
        with FNominaEmpleado do
        begin
             Empleado:= oDataSet.FieldByName('CB_CODIGO').AsInteger;
             FecCambioTNomina:= oDataSet.FieldByName( 'CB_FEC_NOM' ).AsDateTime;

             TNominaFin:= eTipo;
             TNominaInicio:= eTipo;
             FecNominaInicio:= oDatosPeriodo.InicioAsis;
             FecNominaFin:= oDatosPeriodo.FinAsis;
             { Se valida que la fecha de cambio de tipo de n�mina no sea despues de la fecha final del periodo }
             ValidaFechaCambioTNom;

             EsCambioEnPeriodo:= ( EsCambioTNomPeriodo( FecCambioTNomina, oDatosPeriodo ) );

             if ( EsCambioEnPeriodo )  then
             begin
                  TNominaInicio:= GetTipoPeriodo( Empleado, oDatosPeriodo.Inicio );
                  if ( TNominaInicio <> TNominaFin ) then
                  begin
                       { En caso que se este calculando la n�mina a la que cambio }
                       if ( Result ) then
                       begin
                            FecNominaFin:= FecCambioTNomina - 1;
                       end
                       else
                           { En caso que NO se este calculando la n�mina a la que cambio }
                           FecNominaInicio:= FecCambioTNomina;
                  end;
             end;
        end;

   end;

begin
     with oDataSet do
     begin
          //eTipo := dmCatalogos.GetTipoNomina( FieldByName( 'CB_TURNO' ).AsString );
          oDatosPeriodo:= dmCliente.GetDatosPeriodoActivo;
          eTipo := eTipoPeriodo( oDataSet.FieldByName( 'CB_NOMINA' ).AsInteger );
          Result := ( eTipo <> dmCliente.PeriodoTipo );
          SetValoresNomina;

          if Result then
          begin
               with FNominaEmpleado do
               begin
                    if ( EsCambioEnPeriodo ) then
                       Result:= ( TNominaInicio <> dmCliente.PeriodoTipo )
               end;
          end;

     end;

     if Result then
        sMensaje := 'El Empleado pertenece a una N�mina ' + ObtieneElemento( lfTipoPeriodo, Ord( eTipo ) );
end;

function TdmNomina.GetTipoPeriodo(const iEmpleado: Integer; const dFecha: TDate ): eTipoPeriodo;
begin
     Result:= eTipoPeriodo( ServerNomina.GetTNomEmpleado( dmCliente.Empresa, iEmpleado, dFecha ) );
end;

procedure TdmNomina.ValidaEmpleadoBaja( const iEmpleado: Integer );
var
   sMensaje: String;
begin
     { Lo usa  cdsExcepciones y cdExcepMontos }
     with dmCliente do
     begin
          with cdsEmpleadoLookup do
          begin
               Conectar;
               if ( GetDescripcion( IntToStr( iEmpleado ) ) <> VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
               begin
                    if BajaNominaAnterior( cdsEmpleadoLookup, sMensaje ) then
                    begin
                         DataBaseError( sMensaje );
                    end;
                    if TipoNominaDiferente( cdsEmpleadoLookup, sMensaje ) then
                    begin
                         DataBaseError( sMensaje );
                    end;
               end
               else
                   DataBaseError( 'No Existe El Empleado #' + IntToStr( iEmpleado ) );
          end;
     end;
end;

function TdmNomina.ValidaAfectada(var sMensaje: String; const lActiva: Boolean; const sOperacion: String; const bValidacionAfectadaParcial: Boolean = false ): Boolean;
var
   Estado: eStatusPeriodo;
begin
     if lActiva then
        Estado := eStatusPeriodo( dmCliente.cdsPeriodo.FieldByName( 'PE_STATUS' ).AsInteger )
     else
         Estado := eStatusPeriodo( dmCatalogos.cdsPeriodo.FieldByName( 'PE_STATUS' ).AsInteger );
     //US #17507: Es necesario que la nomina desafectadas de forma parcial se puedan calcular nuevamente para as� realizar los ajustes correspondientes
     if( bValidacionAfectadaParcial ) then
     begin
          if ( ( Estado in [ spAfectadaTotal ] ) ) then
          begin
               sMensaje := ( 'N�mina Ya Fu� Afectada, No Se Puede ' + sOperacion );
               Result := False;
          end
          else
              Result := True;
     end
     else
     begin
          if( Estado in [ spAfectadaParcial, spAfectadaTotal ] ) then
          begin
               sMensaje := ( 'N�mina Ya Fu� Afectada, No Se Puede ' + sOperacion );
               Result := False;
          end
          else
              Result := True;
     end;
end;

procedure TdmNomina.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;

procedure TdmNomina.ExcepDiasHorasAfterPost(DataSet: TDataSet);
begin
     with TZetaClientDataset( Dataset ) do
     begin
          FUltimoTipo := FieldByName( 'FA_DIA_HOR' ).AsString;
          if ( FUltimoTipo = K_TIPO_HORA ) then
          begin
               FUltimoTipoHoras := eMotivoFaltaHoras( FieldByName( 'FA_MOTIVO' ).AsInteger );
               FUltimoHoras := FieldByName( 'FA_HORAS' ).AsFloat;
          end
          else
          begin
               FUltimoTipoDias := eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger );
               FUltimaFecha := FieldByName( 'FA_FEC_INI' ).AsDateTime;
               FUltimoDias := FieldByName( 'FA_DIAS' ).AsFloat;
          end;
     end;
     { PENDIENTE: falta calcular d�as/horas y agregar la n�mina que corresponda }
     { asimismo, modificar el status de la n�mina en cdsNomina }
end;

procedure TdmNomina.ExcepcionesCB_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
            {$ifnDef INTERFAZ_ELECTROLUX}{$ifndef LITTELFUSE_INTERFAZ}ValidaEmpleadoBaja( AsInteger ) {$endif}{$endif}
          else if FEditandoGrid then
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
     end;
end;

procedure TdmNomina.LiquidaEmpleadoActivo;
begin
     with dmCliente.GetDatosPeriodoActivo do
     begin
          if ( Tipo <> eTipoPeriodo(dmCliente.cdsEmpleado.FieldByName('CB_NOMINA').AsInteger) ) then
             DataBaseError( 'Es necesario que el Tipo de n�mina sea el mismo que el del Empleado')
          else if Status in [ spNueva, spPreNomina, spSinCalcular, spCalculadaParcial, spCalculadaTotal ] then
             ProcesaLiquidacion( cdsLiquidacion )
          else
              DataBaseError( 'No Se Permiten Liquidaciones En Per�odos Afectados Parcial o Totalmente');
     end;
end;

procedure TdmNomina.ProcesaLiquidacion( oClientDataSet: TZetaClientDataSet );
var
   oCursor: TCursor;
begin
     with dmCliente do
     begin
          with GetDatosEmpleadoActivo do
          begin
               if Activo and ( ZAccesosMgr.CheckDerecho( D_EMP_REG_BAJA, K_DERECHO_CONSULTA ) ) and
                  ZetaDialogo.zConfirm( 'Liquidaci�n de Empleado', '� Desea Efectuar la Baja de Este Empleado ?', 0, mbYes ) then
               begin
                    dmRecursos.AgregaKardex( K_T_BAJA );
               end;
          end;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with GetDatosEmpleadoActivo do
             begin
                  if not Activo then
                     FFechaBajaLiquidacion := Baja
                  else
                     FFechaBajaLiquidacion := dmCliente.FechaDefault;
             end;
             oClientDataSet.Modificar;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TdmNomina.AgregaExcepGlobales;
begin
     with cdsExcepGlobales do
     begin
          if not Active then
          begin
               dmCatalogos.cdsConceptos.Conectar;
               Conectar;
          end;
          Agregar;
     end;
end;

function TdmNomina.GetMsgBorrarPeriodo( const lActiva: Boolean ): String;
var
   iNumero, iEmpleados: Integer;
   eTipo: eTipoPeriodo;
   eStatus: eStatusPeriodo;
   eStatusTim : eStatusTimbrado;
   dInicio, dFin: TDate;

function GetPeriodoDescripcion: String;
begin
     Result := ZetaCommonLists.ObtieneElemento( lfTipoNomina, Ord( eTipo ) ) +
               ': ' +
               IntToStr( iNumero ) +
                 CR_LF + {acl. Para que no se encime cuando la nomina sea de 30 caracteres.}
               ' Del ' +
               ZetaCommonTools.FechaCorta( dInicio ) +
               ' Al '  +
               ZetaCommonTools.FechaCorta( dFin );
end;

begin
     if lActiva then // valida si se est� intentado borrar la n�mina activa alguna del cat. de per�odos
     begin
          with dmCliente do
          begin
               with GetDatosPeriodoActivo do
               begin
                    eTipo := Tipo;
                    iNumero := Numero;
                    dInicio := Inicio;
                    dFin := Fin;
                    eStatus := Status;
                    eStatusTim := StatusTimbrado;
               end;
               iEmpleados := GetPeriodoCampoEmpleados;
        end;
     end
     else
     begin
          with dmCatalogos.cdsPeriodo do
          begin
               eTipo := eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger );
               iNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
               dInicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               dFin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               eStatus := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               eStatusTim := eStatusTimbrado( FieldByName( 'PE_TIMBRO' ).AsInteger );
               iEmpleados := FieldByName( 'PE_NUM_EMP' ).AsInteger;
          end;
     end;
     Result := 'El Per�odo' +
               CR_LF +
               GetPeriodoDescripcion +
               CR_LF +
               'Contiene Percepciones y Deducciones' +
               CR_LF +
               'de la N�mina de ' + IntToStr( iEmpleados ) + ' Empleados' +
               CR_LF +
               'Status Actual: ' +
               ZetaCommonTools.GetDescripcionStatusPeriodo( eStatus, eStatusTim );
end;

procedure TdmNomina.BorrarNominaActiva( const lActiva: Boolean );
var
   oCursor: TCursor;
   lBorrar, lCancelar: Boolean;
   iYear, iNumero: Integer;
   eTipo: eTipoPeriodo;
   sMensaje: String;
begin
     lCancelar := False;
     lBorrar := False;
     if ValidaAfectada( sMensaje, lActiva, 'Borrar' ) then
     begin
          case FBorraNomina_DevEx.ShowDlgBorraNomina( GetMsgBorrarPeriodo( lActiva ) ) of
               FBorraNomina_DevEx.bnInit: lBorrar := True;
               FBorraNomina_DevEx.bnBorrar: lBorrar := False;
               FBorraNomina_DevEx.bnCancelar: lCancelar := True;
          end;

          if not lCancelar then
          begin
               with dmCliente do
               begin
                    if lActiva then // valida si se est� intentado borrar la n�mina activa alguna del cat. de per�odos
                    begin
                         iYear := YearDefault;
                         eTipo := PeriodoTipo;
                         iNumero := PeriodoNumero
                    end
                    else
                    begin
                         iYear := dmCatalogos.cdsPeriodo.FieldByName( 'PE_YEAR' ).AsInteger;
                         eTipo := eTipoPeriodo( dmCatalogos.cdsPeriodo.FieldByName( 'PE_TIPO' ).AsInteger );
                         iNumero := dmCatalogos.cdsPeriodo.FieldByName( 'PE_NUMERO' ).AsInteger
                    end;
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                       ServerNomina.BorraNomina( Empresa, Usuario, FechaDefault, iYear, Ord( eTipo ), iNumero, lBorrar,VACIO );
                       if not lBorrar then
                       begin
                            if lActiva then
                            begin
                                 dmCatalogos.cdsPeriodo.Active := FALSE;
                                 TressShell.BuscaNuevoPeriodo( Ord( eTipo), iNumero );
                            end
                            else
                            begin
                                 dmCatalogos.cdsPeriodo.Delete;
                                 if ( iYear = YearDefault ) and ( eTipo = PeriodoTipo ) and
                                    ( iNumero = PeriodoNumero ) then
                                      TressShell.BuscaNuevoPeriodo( Ord( eTipo), iNumero );
                            end;
                       end
                       else
                       begin
                            if ( iYear = YearDefault ) and ( eTipo = PeriodoTipo ) and
                               ( iNumero = PeriodoNumero ) then
                            begin
                                 dmCatalogos.cdsPeriodo.Active := FALSE;
                                 TressShell.SetDataChange( [enPeriodo] );
                            end
                            else
                                 dmCatalogos.GetDatosPeriodo( iYear, eTipo );
                       end;
                    finally
                           Screen.Cursor := oCursor;
                    end;
               end;
          end;
     end
     else
         ZetaDialogo.ZInformation( 'Borrar N�mina', sMensaje, 0 );
end;

procedure TdmNomina.GrabaExcepcionesMonto( Dataset: TZetaClientDataset );
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        dmCliente.CargaActivosPeriodo( Parametros );
        Parametros.AddBoolean('ExcederLimites',Revisa(D_NOM_EXCEPCIONES_LIM_MONTO ));
        GrabaExcepcionesMontoParams( Dataset, Parametros );
     finally
            Parametros.Free;
     end;
end;

procedure TdmNomina.GrabaExcepcionesMontoParams( Dataset: TZetaClientDataset; oParams: TZetaParams );
var
   ErrorCount: Integer;
   ErrorVar, ErrorData: OleVariant;
begin
     ErrorCount := 0;
     with Dataset do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Repeat
                     oParams.AddInteger( 'Operacion', FOperacion );
                     ErrorVar := ServerNomina.GrabaMovMontos( dmCliente.Empresa, oParams.VarValues, Delta, ErrorCount, ErrorData );
                     cdsErrores.Data := ErrorData;
               Until ( Reconciliar( ErrorVar ) );
               SoloAltas := FALSE;              // Para que al refrescar DataSets de Grids no interfiera el SoloAltas
               TressShell.SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
               FRefrescaMovMontos:= TRUE;
               //cdsSimulaMontos.Refrescar;
          end;
     end;
end;

function TdmNomina.GetMontoAnterior( const iEmpleado, iConcepto: Integer; const sReferencia: String ): Currency;
begin
     Result:= 0;
     with cdsErrores do
     begin
          if Locate( 'CB_CODIGO;CO_NUMERO;MO_REFEREN',VarArrayOf([ iEmpleado, iConcepto, sReferencia ] ), [] ) then
             Result := FieldByName( 'MO_MONTO' ).AsFloat;
     end;
end;

{$ifdef VER130}
function TdmNomina.OperaConflictoMontos( DataSet: TClientDataSet ): eOperacionConflicto;
var
   iEmpleado, iConcepto: Integer;
   sReferencia : String;
begin
     with DataSet do
     begin
          iEmpleado:= ValorCampo( FieldByName( 'CB_CODIGO' ) );
          iConcepto:= ValorCampo( FieldByName( 'CO_NUMERO' ) );
          sReferencia:= ValorCampo( FieldByName( 'MO_REFEREN' ) );
          Result := FMontoRepetido_DevEx.ShowDlgMontoRepetido( iEmpleado , iConcepto, sReferencia,
                                          GetMontoAnterior( iEmpleado, iConcepto, sReferencia ),
                                          ValorCampo( FieldByName( 'MO_PERCEPC' ) ) +
                                          ValorCampo( FieldByName( 'MO_DEDUCCI' ) ) );
     end;
end;
{$else}
function TdmNomina.OperaConflictoMontos( DataSet: TCustomClientDataSet ): eOperacionConflicto;
var
   iEmpleado, iConcepto: Integer;
   sReferencia : String;
begin
     with DataSet do
     begin
          iEmpleado:= ValorCampo( FieldByName( 'CB_CODIGO' ) );
          iConcepto:= ValorCampo( FieldByName( 'CO_NUMERO' ) );
          sReferencia:= ValorCampo( FieldByName( 'MO_REFEREN' ) );
          {$ifDef INTERFAZ_ELECTROLUX}
          Result :=  ocSustituir;
          {$else}
          {$ifdef LITTELFUSE_INTERFAZ}
          Result :=  ocSustituir;
          {$else}

          Result := FMontoRepetido_DevEx.ShowDlgMontoRepetido( iEmpleado , iConcepto, sReferencia,
                                          GetMontoAnterior( iEmpleado, iConcepto, sReferencia ),
                                          ValorCampo( FieldByName( 'MO_PERCEPC' ) ) +
                                          ValorCampo( FieldByName( 'MO_DEDUCCI' ) ) );
          {$endif}
          {$endif}
     end;
end;
{$endif}

procedure TdmNomina.GrabaExcepcionesDiasHoras( Dataset: TZetaClientDataset );
var
   ErrorCount: Integer;
   ErrorVar, ErrorData: OleVariant;
   Parametros: TZetaParams;
begin
     ErrorCount := 0;
     with Dataset do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Parametros := TZetaParams.Create;
               try
                  with dmCliente do
                  begin
                       CargaActivosTodos( Parametros );
                       Repeat
                             Parametros.AddInteger( 'Operacion', FOperacion );
                             ErrorVar := ServerNomina.GrabaFaltas( Empresa, Parametros.VarValues, Delta, ErrorCount, ErrorData );
                             cdsErrores.Data := ErrorData;
                       Until ( Reconciliar( ErrorVar ) );
                       SoloAltas := FALSE;              // Para que al refrescar DataSets de Grids no interfiera el SoloAltas
                       TressShell.SetDataChange( [ enPeriodo, enNomina, enFaltas ] );
                  end;
               finally
                      Parametros.Free;
               end;
          end;
     end;
end;

function TdmNomina.GetDiaHoraAnterior( const iEmpleado, iMotivo: Integer; const dFecha: TDate;
                                       const sDiaHora: String ): Currency;
begin
     Result:= 0;
     with cdsErrores do
     begin
          if Locate( 'CB_CODIGO;FA_FEC_INI;FA_DIA_HOR;FA_MOTIVO',VarArrayOf([ iEmpleado,
                     dFecha, sDiaHora, iMotivo ] ), [] ) then
             Result := FieldByName( 'FA_VALOR' ).AsFloat;
     end;
end;

{$ifdef VER130}
function TdmNomina.OperaConflictoDiasHoras(DataSet: TClientDataSet): eOperacionConflicto;
var
   iEmpleado, iMotivo : Integer;
   sDiaHora: String;
   dFecha: TDate;
begin
     with DataSet do
     begin
          iEmpleado:= ValorCampo( FieldByName( 'CB_CODIGO' ) );
          dFecha:= ValorCampo( FieldByName( 'FA_FEC_INI' ) );
          sDiaHora := ValorCampo( FieldByName( 'FA_DIA_HOR' ) );
          iMotivo := ValorCampo( FieldByName( 'FA_MOTIVO' ) );
          Result := FFaltaRepetida_DevEx.ShowDlgFaltaRepetida( iEmpleado, iMotivo, sDiaHora,
                                          ValorCampo( FieldByName( 'FA_HORAS' ) ) +
                                          ValorCampo( FieldByName( 'FA_DIAS' ) ),
                                          GetDiaHoraAnterior( iEmpleado, iMotivo, dFecha, sDiaHora ) );
     end;
end;
{$else}
function TdmNomina.OperaConflictoDiasHoras(DataSet: TCustomClientDataSet): eOperacionConflicto;
var
   iEmpleado, iMotivo : Integer;
   sDiaHora: String;
   dFecha: TDate;
begin
     with DataSet do
     begin
          iEmpleado:= ValorCampo( FieldByName( 'CB_CODIGO' ) );
          dFecha:= ValorCampo( FieldByName( 'FA_FEC_INI' ) );
          sDiaHora := ValorCampo( FieldByName( 'FA_DIA_HOR' ) );
          iMotivo := ValorCampo( FieldByName( 'FA_MOTIVO' ) );
          Result := FFaltaRepetida_DevEx.ShowDlgFaltaRepetida( iEmpleado, iMotivo, sDiaHora,
                                          ValorCampo( FieldByName( 'FA_HORAS' ) ) +
                                          ValorCampo( FieldByName( 'FA_DIAS' ) ),
                                          GetDiaHoraAnterior( iEmpleado, iMotivo, dFecha, sDiaHora ) );
     end;
end;
{$endif}

{
function TdmNomina.PuedeAgregarNomina( var sMensaje: String ): Boolean;
var
   Baja: TDatosPeriodo;
begin
     with dmRecursos do
     begin
          GetDatosEmpleadoActivo;
          with cdsDatosEmpleado do
          begin
               with Baja do
               begin
                    Year := FieldByName( 'CB_NOMYEAR' ).AsInteger;
                    Tipo := eTipoPeriodo( FieldByName( 'CB_NOMTIPO' ).AsInteger );
                    Numero := FieldByName( 'CB_NOMNUME' ).AsInteger;
               end;
               if ZetaCommonTools.RequiereBorrar( dmCliente.GetDatosPeriodoActivo,
                                                  Baja,
                                                  FieldByName( 'CB_FEC_ING' ).AsDateTime,
                                                  FieldByName( 'CB_FEC_BAJ' ).AsDateTime,
                                                  zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ),
                                                  eTipoPeriodo( FieldByName( 'TU_NOMINA' ).AsInteger ),
                                                  sMensaje ) then
               begin
                    Result := False
               end
               else
                   Result := True;
          end;
     end;
end;
}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmNomina.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmNomina.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPeriodo, Entidades ) or Dentro( enNomina , Entidades ) or (  Estado in [ stPeriodo, stEmpleado ] ) then
     {$else}
     if ( enPeriodo in Entidades ) or ( enNomina in Entidades ) or (  Estado in [ stPeriodo, stEmpleado ] ) then
     {$endif}
     begin
          if ( Estado <> stEmpleado ) then
              cdsTotales.SetDataChange;
          cdsDatosAsist.SetDataChange;
          cdsExcepciones.SetDataChange;
          cdsExcepMontos.SetDataChange;
          cdsExcepGlobales.SetDataChange;
          {
          cdsMontos.SetDataChange;
          }
          cdsMovMontos.SetDataChange;
          cdsSimulaMontos.SetDataChange;
          cdsDatosClasifi.SetDataChange;
          cdsSimulaGlobales.SetDataChange;
          cdsSimulaTotales.SetDataChange;
          cdsSimulaLiquidacion.SetDataChange;
          cdsSimGlobalTotales.SetDataChange;
          {
          cdsDatosNomina.SetDataChange;
          }
          cdsMovDiasHoras.SetDataChange;
          cdsPagoRecibos.SetDataChange;
     end;
     {
     if ( enNomina in Entidades ) then
     begin
          cdsTotales.SetDataChange;
          if ( Estado = stEmpleado ) then
          begin
               cdsDatosAsist.SetDataChange;
               cdsExcepciones.SetDataChange;
               cdsExcepMontos.SetDataChange;
               cdsExcepGlobales.SetDataChange;
               cdsMontos.SetDataChange;
               cdsMovMontos.SetDataChange;
               cdsDatosClasifi.SetDataChange;
               cdsDatosNomina.SetDataChange;
               cdsMovDiasHoras.SetDataChange;
               cdsPagoRecibos.SetDataChange;
          end;
     end;
     }
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enFaltas , Entidades ) then
     {$else}
     if ( enFaltas in Entidades ) then
     {$endif}
     begin
          cdsExcepciones.SetDataChange;
          {
          cdsDatosNomina.SetDataChange;
          }
          cdsMovDiasHoras.SetDataChange;
          cdsDatosAsist.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enMovimien , Entidades ) then
     {$else}
     if ( enMovimien in Entidades ) then
     {$endif}
     begin
          cdsExcepMontos.SetDataChange;
          {
          cdsMontos.SetDataChange;
          }
          cdsMovMontos.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enMovGral , Entidades ) then
     {$else}
     if ( enMovGral in Entidades ) then
     {$endif}
     begin
          cdsExcepGlobales.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enAusencia, Entidades ) then
     {$else}
     if ( enAusencia in Entidades ) then
     {$endif}
     begin
          cdsDatosAsist.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if ( ( Dentro( enPolHead, Entidades ) ) or ( Estado = stPeriodo  ) ) then
     {$else}
     if ( ( enPolHead in Entidades ) or  ( Estado = stPeriodo  ) ) then
     {$endif}
     begin
          cdsPolizas.SetDataChange;
     end;

       {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enFonTot , Entidades ) or ( Estado = stIMSS ) or ( Estado = stRazonSocial ) then
     {$else}
     if ( enFonTot in Entidades ) or ( Estado = stIMSS )  or ( Estado = stRazonSocial )  then
     {$endif}
     begin
          cdsFonTot.SetDataChange;
          cdsFonEmp.SetDataChange;
          cdsFonCre.SetDataChange;
          cdsFonTotDetalle.SetDataChange;
          cdsFonTotDiferencias.SetDataChange;
          cdsFonTotRs.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enFonEmp , Entidades ) or ( Estado = stEmpleado ) then
     {$else}
     if ( enFonEmp in Entidades ) or ( Estado = stEmpleado ) then
     {$endif}
     begin
          cdsFonEmp.SetDataChange;
          cdsFonCre.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enDeclaraAnualCierre , Entidades ) or Dentro( enDeclaraAnual , Entidades ) then
     {$else}
     if ( enDeclaraAnualCierre in Entidades ) or  ( enDeclaraAnual in Entidades ) then
     {$endif}
     begin
          cdsDeclaracionesAnuales.SetDataChange;
     end

end;

function TdmNomina.GetEmpleadoGrid: Integer;
begin
     if FEditandoGrid then
        Result := FEmpleadoGrid
     else
        Result := dmCliente.Empleado;
end;

{ *********** cdsDatosNomina *********** }

procedure TdmNomina.cdsDatosNominaAlAdquirirDatos(Sender: TObject);
begin
     // PENDIENTE: Para evitar anidamiento de Lookup.
     dmCatalogos.cdsTurnos.Conectar;
     {
     cdsMovDiasHoras.Conectar;
     }
     with dmCliente do
     begin
          cdsDatosNomina.Data := ServerNomina.GetDatosNomina( Empresa, Empleado, YearDefault, Ord( PeriodoTipo ), PeriodoNumero );
     end;
end;

procedure TdmNomina.cdsDatosNominaAfterOpen(DataSet: TDataSet);
begin
     { Pertence a otro master detail }
     {
     cdsDatosAsist.DataSetField := cdsDatosNomina.FieldByName('tqDatosAsist') As TDataSetField;
     }
     with cdsDatosNomina do
     begin
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmNomina.cdsDatosNominaAlCrearCampos(Sender: TObject);
begin
     with cdsDatosNomina do
     begin
          CreateSimpleLookup( dmCatalogos.cdsTurnos, 'TURNO', 'CB_TURNO' );
          FieldByName('NO_STATUS').OnGetText := NO_STATUSGetText;
          ListaFija( 'NO_PAGO', lfMetodoPagoSAT );
     end;
end;

{ ********** cdsMovDiasHoras ********** }

procedure TdmNomina.cdsMovDiasHorasAlAdquirirDatos(Sender: TObject);
var
   oTotales: OleVariant;
begin
     dmCatalogos.cdsTurnos.Conectar;
     with dmCliente do
     begin
          cdsMovDiasHoras.Data := ServerNomina.GetFaltas( Empresa, Empleado, YearDefault,
                                  Ord( PeriodoTipo ), PeriodoNumero, oTotales );
          cdsDatosNomina.Data := oTotales;
     end;
end;

procedure TdmNomina.cdsMovDiasHorasAlCrearCampos(Sender: TObject);
begin
     with cdsMovDiasHoras do
     begin
         MaskHoras( 'FA_HORAS' );
         MaskHoras( 'FA_DIAS' );
         MaskFecha( 'FA_FEC_INI' );
         FieldByName( 'CB_CODIGO' ).OnChange := cdsExcepcionesCB_CODIGOChange;
         FieldByName( 'FA_DIA_HOR' ).OnChange := FA_DIA_HORChange;
         with FieldByName( 'FA_MOTIVO' ) do
         begin
              OnGetText := FA_MOTIVOGetText;
              Alignment := taLeftJustify;
         end;
     end;
end;

procedure TdmNomina.FA_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     { Usado por cdsMovDiasHoras y cdsExcepciones }
     if ( Sender.Dataset.FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
        Text := ZetaCommonLists.ObtieneElemento( lfMotivoFaltaDias, Sender.AsInteger )
     else
         if ( Sender.Dataset.FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_HORA ) then
            Text := ZetaCommonLists.ObtieneElemento( lfMotivoFaltaHoras, Sender.AsInteger )
         else
             Text := '';
end;

procedure TdmNomina.FA_DIA_HORChange(Sender: TField);
begin
     with Sender.Dataset do
     begin
          if ( Sender.AsString = K_TIPO_HORA ) then
          begin
               if ( FieldByName( 'FA_MOTIVO' ).AsInteger = 0 ) then
                  FieldByName( 'FA_MOTIVO' ).AsInteger := Ord( FUltimoTipoHoras );
               if ( FieldByName( 'FA_HORAS' ).AsFloat = 0.0 ) then
                  FieldByName( 'FA_HORAS' ).AsFloat := FUltimoHoras;
               // FieldByName( 'FA_FEC_INI' ).AsDateTime := 0;
               // EZM: Pare evitar problema de FechasVacias = 1900
               // En excepciones de HORAS, la fecha es irrelevante
//               FieldByName( 'FA_FEC_INI' ).AsDateTime := dmCliente.GetDatosPeriodoActivo.Inicio;
               FieldByName( 'FA_DIAS' ).AsFloat := 0.0;
          end
          else
          begin
               if ( FieldByName( 'FA_MOTIVO' ).AsInteger = 0 ) then
                  FieldByName( 'FA_MOTIVO' ).AsInteger := Ord( FUltimoTipoDias );
               if ( FieldByName( 'FA_DIAS' ).AsFloat = 0.0 ) then
                  FieldByName( 'FA_DIAS' ).AsFloat := FUltimoDias;
               FieldByName( 'FA_HORAS' ).AsFloat := 0.0;
          end;
          if ( FieldByName( 'FA_FEC_INI' ).AsDateTime = 0 ) then
             FieldByName( 'FA_FEC_INI' ).AsDateTime := FUltimaFecha;
     end;
end;

procedure TdmNomina.cdsMovDiasHorasAfterPost(DataSet: TDataSet);
begin
     // Se va a encargar el metodo ExcepDiasHorasAfterPost
end;

procedure TdmNomina.cdsMovDiasHorasNewRecord(DataSet: TDataSet);
begin
     with cdsMovDiasHoras do
     begin
          with dmCliente do
          begin
               FieldByName( 'PE_YEAR' ).AsInteger := YearDefault;
               FieldByName( 'PE_TIPO' ).AsInteger := Ord( PeriodoTipo );
               FieldByName( 'PE_NUMERO' ).AsInteger := PeriodoNumero;
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
          end;
     end;
     // se va a encargar de poner los default el CB_CODIGOChange
     {
     with cdsMovDiasHoras do
     begin
          FieldByName( 'FA_DIA_HOR' ).AsString := dfUltimoTipo;
          FieldByName( 'FA_MOTIVO' ).AsInteger := dfUltimoMotivo;
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
          begin
               FieldByName( 'FA_FEC_INI' ).AsDateTime := dfUltimaFecha;
               FieldByName( 'FA_DIAS' ).AsFloat := dfDias;
          end
          else
          begin
               FieldByName( 'FA_FEC_INI' ).AsDateTime := 0;
               FieldByName( 'FA_HORAS' ).AsFloat := dfHoras;
          end;
     end;
     }
end;

procedure TdmNomina.cdsMovDiasHorasBeforeOpen(DataSet: TDataSet);
begin
     {
     dfUltimaFecha := dmCliente.GetDatosPeriodoActivo.Inicio;
     }
     {$ifdef QUINCENALES}
     FUltimaFecha := dmCliente.GetDatosPeriodoActivo.InicioAsis;
     {$else}
     FUltimaFecha := dmCliente.GetDatosPeriodoActivo.Inicio;
     {$endif}
     FUltimoTipo := K_TIPO_HORA;  // (D/H) char(1)
     FUltimoTipoHoras := mfhExtras;
     FUltimoTipoDias := mfdInjustificada;
     FUltimoHoras := 1.00;
     FUltimoDias := 1.00;
end;

procedure TdmNomina.cdsMovDiasHorasAlEnviarDatos(Sender: TObject);
begin
     GrabaExcepcionesDiasHoras( cdsMovDiasHoras );
     cdsMovDiasHoras.Refrescar;
end;

{$ifdef VER130}
procedure TdmNomina.cdsMovDiasHorasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          Opera := OperaConflictoDiasHoras( DataSet );
          if Opera <> ocIgnorar then
          begin
               FOperacion:= Ord( Opera );
               FConflictoFaltas:= TRUE;
               Action := raCorrect;
          end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$else}
procedure TdmNomina.cdsMovDiasHorasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          Opera := OperaConflictoDiasHoras( DataSet );
          if Opera <> ocIgnorar then
          begin
               FOperacion:= Ord( Opera );
               FConflictoFaltas:= TRUE;
               Action := raCorrect;
          end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$endif}

procedure TdmNomina.cdsMovDiasHorasAlBorrar(Sender: TObject);
begin
     with cdsMovDiasHoras do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
          begin
               Delete;
               Enviar;
          end;
     end;
end;

procedure TdmNomina.cdsMovDiasHorasAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( NomEditDatosDiasHoras_DevEx, TNomEditDatosDiasHoras_DevEx );
end;

procedure TdmNomina.cdsMovDiasHorasBeforePost(DataSet: TDataSet);
begin
     with cdsMovDiasHoras do
     begin
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
          begin
               if not ( eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger )
                  in [ mfdInjustificada,mfdAguinaldo,mfdAsistencia,mfdNoTrabajados,mfdRetardo,mfdAjuste,mfdIMSS,mfdEM ]) then
                  DatabaseError( 'No se Permite Capturar Motivo de Falta D�a de este Tipo' );
          end;
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
             FieldByName( 'FA_HORAS' ).AsFloat := 0
          else
             FieldByName( 'FA_DIAS' ).AsFloat := 0;
          if (FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) and
             ( eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger ) in [ mfdInjustificada, mfdAjuste ] ) and
             ( FieldByName( 'FA_FEC_INI' ).AsDateTime = 0 ) then
             DatabaseError( 'Fecha No Puede Quedar Vac�a Para Este Tipo de Falta' );
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) and
             ( FieldByName( 'FA_DIAS' ).AsFloat = 0 ) and
             ( eMotivoFaltaDias ( FieldByName( 'FA_MOTIVO' ).AsInteger ) in
             [ mfdInjustificada, mfdJustificada, mfdSinGoce, mfdConGoce,
               mfdSuspension, mfdOtrosPermisos, mfdVacaciones, mfdIncapacidad, mfdAguinaldo,
               mfdNoTrabajados, mfdRetardo, mfdAjuste, mfdPrimaVacacional ]) then
            DatabaseError( 'N�mero De D�as No Puede Ser Cero');
          { Se debe tener acceso a la ayuda para conocer el motivo del error }
          { Es Igual para todos los demas raise }
          { PENDIENTE: Carlos pregunta: �Porqu� no se valida que TODOS los tipos de hora no acepten ceros? }
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_HORA ) and
             ( FieldByName( 'FA_HORAS' ).AsFloat = 0 ) and
             ( eMotivoFaltaHoras ( FieldByName( 'FA_MOTIVO' ).AsInteger ) in [ mfhConGoce, mfhSinGoce ] ) then
             DatabaseError( 'N�mero De Horas No Puede Ser Cero');
          ValidaFechaDiasHoras( cdsMovDiasHoras );
     end;
end;

{ ************* cdsExcepciones ************ }

procedure TdmNomina.cdsExcepcionesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsExcepciones.Data:= ServerNomina.GetDatosExcepciones( Empresa, YearDefault, Ord( PeriodoTipo ), PeriodoNumero, FSoloAltas );
     end;
end;

procedure TdmNomina.cdsExcepcionesAlCrearCampos(Sender: TObject);
begin
     with cdsExcepciones do
     begin
         MaskHoras( 'FA_HORAS' );
         MaskHoras( 'FA_DIAS' );
         MaskFecha( 'FA_FEC_INI' );
         FieldByName( 'CB_CODIGO' ).OnValidate := ExcepcionesCB_CODIGOValidate;
         FieldByName( 'CB_CODIGO' ).OnChange := cdsExcepcionesCB_CODIGOChange;
         FieldByName( 'FA_DIA_HOR' ).OnChange := FA_DIA_HORChange;
         with FieldByName( 'FA_MOTIVO' ) do
         begin
              OnGetText := FA_MOTIVOGetText;
              Alignment := taLeftJustify;
         end;
     end;
end;

procedure TdmNomina.cdsExcepcionesBeforeOpen(DataSet: TDataSet);
begin
     {
     dfUltimaFecha:= dmCliente.GetDatosPeriodoActivo.Inicio;
     }
     {$ifdef QUINCENALES}
     FUltimaFecha := dmCliente.GetDatosPeriodoActivo.InicioAsis;
     {$else}
     FUltimaFecha := dmCliente.GetDatosPeriodoActivo.Inicio;
     {$endif}
     FUltimoTipo := K_TIPO_HORA;  // (D/H) char(1)
     FUltimoTipoHoras := mfhExtras;
     FUltimoTipoDias := mfdInjustificada;
     FUltimoHoras := 1.00;
     FUltimoDias := 1.00;
end;

procedure TdmNomina.cdsExcepcionesNewRecord(DataSet: TDataSet);
begin
     { PENDIENTE: AgregarNomina como en 1.3 }
     with cdsExcepciones do
     begin
          with dmCliente do
          begin
               FieldByName( 'PE_YEAR' ).AsInteger := YearDefault;
               FieldByName( 'PE_TIPO' ).AsInteger := Ord( PeriodoTipo );
               FieldByName( 'PE_NUMERO' ).AsInteger := PeriodoNumero;
          end;
     end;
end;

procedure TdmNomina.cdsExcepcionesAlBorrar(Sender: TObject);
begin
     with cdsExcepciones do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
          begin
               Delete;
               Enviar;
          end;
     end;
end;

procedure TdmNomina.cdsExcepcionesAlModificar(Sender: TObject);
begin
     with dmCliente do
     begin
          SetTipoLookupEmpleado( eLookEmpNominas );
          try
             ZBaseEdicion_DevEx.ShowFormaEdicion( NomEditExcepDiasHoras_DevEx, TNomEditExcepDiasHoras_DevEx );
          finally
             SetTipoLookupEmpleado( eLookEmpGeneral );
          end;
     end;
end;

procedure TdmNomina.cdsExcepcionesBeforePost(DataSet: TDataSet);
begin
     with cdsExcepciones do
     begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger = 0 ) then
          begin
               FieldByName( 'CB_CODIGO' ).FocusControl;
               DataBaseError( 'N�mero De Empleado No Puede Quedar Vac�o' );
          end;
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
          begin
               if not ( eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger )
                  in [ mfdInjustificada,mfdAguinaldo,mfdAsistencia,mfdNoTrabajados,mfdRetardo,mfdAjuste,mfdIMSS,mfdEM ]) then
                  DatabaseError( 'No se Permite Capturar Motivo de Falta D�a de este Tipo' );
          end;
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) then
             FieldByName( 'FA_HORAS' ).AsFloat := 0
          else
          begin
               FieldByName( 'FA_DIAS' ).AsFloat := 0;
               // FieldByName( 'FA_FEC_INI' ).AsDateTime:= 0;
               // EZM: Pare evitar problema de FechasVacias = 1900
               // En excepciones de HORAS, la fecha es irrelevante
               // FieldByName( 'FA_FEC_INI' ).AsDateTime := dmCliente.GetDatosPeriodoActivo.Inicio;
          end;
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) and
             ( eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger ) in [ mfdInjustificada, mfdAjuste ] ) and
             ( FieldByName( 'FA_FEC_INI' ).AsDateTime = 0 ) then
             DatabaseError( 'Fecha No Puede Quedar Vac�a Para Este Tipo de Falta' );
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_DIA ) and
             ( FieldByName( 'FA_DIAS' ).AsFloat = 0 ) and
             ( eMotivoFaltaDias( FieldByName( 'FA_MOTIVO' ).AsInteger ) in
             [ mfdInjustificada, mfdJustificada, mfdSinGoce, mfdConGoce,
               mfdSuspension, mfdOtrosPermisos, mfdVacaciones, mfdIncapacidad, mfdAguinaldo,
               mfdNoTrabajados, mfdRetardo, mfdAjuste, mfdPrimaVacacional ] ) then
            DatabaseError( 'N�mero De D�as No Puede Ser Cero');
          if ( FieldByName( 'FA_DIA_HOR' ).AsString = K_TIPO_HORA ) and
             ( FieldByName( 'FA_HORAS' ).AsFloat = 0 ) and
             ( eMotivoFaltaHoras ( FieldByName( 'FA_MOTIVO' ).AsInteger ) in [ mfhConGoce, mfhSinGoce ] ) then
             DatabaseError( 'N�mero De Horas No Puede Ser Cero');
          { PENDIENTE NOTA: las siguientes asignaciones se hacen }
          { en TdmNomina.AgregaDiaHora en 1.3 }
          with dmCliente do
          begin
               FieldByName( 'PE_YEAR' ).AsInteger := YearDefault;
               FieldByName( 'PE_TIPO' ).AsInteger := Ord( PeriodoTipo );
               FieldByName( 'PE_NUMERO' ).AsInteger := PeriodoNumero;
          end;

          if  ( State in [dsEdit] ) and ( FNominaEmpleado.Empleado <> FieldByName( 'CB_CODIGO' ).AsInteger ) then
          begin
               ValidaEmpleadoBaja( FieldByName( 'CB_CODIGO' ).AsInteger );
          end;

          ValidaFechaDiasHoras( cdsExcepciones );
     end;
end;

procedure TdmNomina.cdsExcepcionesAlEnviarDatos(Sender: TObject);
begin
     GrabaExcepcionesDiasHoras( cdsExcepciones );
end;

{$ifdef VER130}
procedure TdmNomina.cdsExcepcionesReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          Opera := OperaConflictoDiasHoras( DataSet );
          if Opera <> ocIgnorar then
             with DataSet do
             begin
                  Edit;
                  FieldByName( 'OPERACION' ).NewValue := Ord( Opera );
                  Post;
                  FConflictoFaltas:= TRUE;
                  Action := raCorrect;
             end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
{var
   sError: String;
begin
    if PK_Violation( E ) then
    begin
         with DataSet do
         begin
              sError := Format( 'La Excepci�n Ya Fu� Capturada' +
                                CR_LF +
                                'Empleado: %d' +
                                CR_LF +
                                'Fecha: %s',
                                [ FieldByName( 'CB_CODIGO' ).AsInteger,
                                  ZetaCommonTools.FechaCorta( FieldByName( 'FA_FEC_INI' ).AsDateTime ) ] );
         end;
    end
    else
        sError := GetErrorDescription( E );
    Action := cdsExcepciones.ReconciliaError( DataSet, UpdateKind, E, 'CB_CODIGO;FA_DIA_HOR;FA_FEC_INI;FA_MOTIVO', sError );
    }
end;
{$else}
procedure TdmNomina.cdsExcepcionesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          Opera := OperaConflictoDiasHoras( DataSet );
          if Opera <> ocIgnorar then
             with DataSet do
             begin
                  Edit;
                  FieldByName( 'OPERACION' ).NewValue := Ord( Opera );
                  Post;
                  FConflictoFaltas:= TRUE;
                  Action := raCorrect;
             end;
     end
     else if FK_VIOLATION( E ) then { FK_VIOLATION }
     begin
          MessageDlg( 'El tipo de n�mina es invalido.', mtError, [ mbOK ], 0 );
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
{var
   sError: String;
begin
    if PK_Violation( E ) then
    begin
         with DataSet do
         begin
              sError := Format( 'La Excepci�n Ya Fu� Capturada' +
                                CR_LF +
                                'Empleado: %d' +
                                CR_LF +
                                'Fecha: %s',
                                [ FieldByName( 'CB_CODIGO' ).AsInteger,
                                  ZetaCommonTools.FechaCorta( FieldByName( 'FA_FEC_INI' ).AsDateTime ) ] );
         end;
    end
    else
        sError := GetErrorDescription( E );
    Action := cdsExcepciones.ReconciliaError( DataSet, UpdateKind, E, 'CB_CODIGO;FA_DIA_HOR;FA_FEC_INI;FA_MOTIVO', sError );
    }
end;
{$endif}

procedure TdmNomina.cdsExcepcionesCB_CODIGOChange(Sender: TField);
var
   oCursor: TCursor;
begin
     with Sender.DataSet do
     begin
          if ( FindField( 'PRETTYNAME' ) <> nil ) then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourGlass;
               try
                  FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescripcion(Sender.AsString);
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
          // Asigna Valores Default
          if ZetaCommonTools.StrVacio( FieldByName( 'FA_DIA_HOR' ).AsString ) then
          begin
               if ( FUltimoTipo = K_TIPO_HORA ) then
                  FieldByName( 'FA_DIA_HOR' ).AsString := K_TIPO_HORA
               else
                   FieldByName( 'FA_DIA_HOR' ).AsString := K_TIPO_DIA;
          end;
     end;
end;

procedure TdmNomina.EditarGridDiasHoras( const lSoloAltas: Boolean);
begin
     SoloAltas := lSoloAltas;
     try
        dmCliente.SetLookupEmpleado( eLookEmpNominas );
        FEditandoGrid:= TRUE;
        ZbaseGridEdicion_DevEx.ShowGridEdicion( GridDiasHoras_DevEx, TGridDiasHoras_DevEx, lSoloAltas );
     finally
        FEditandoGrid:= FALSE;
        dmCliente.ResetLookupEmpleado;
     end;
     SoloAltas := False;
     // Para que muestre los registros que se agregaron
     // if ( lSoloAltas ) then
        cdsExcepciones.Refrescar;
end;

{ *********** cdsDatosClasifi *********** }

procedure TdmNomina.cdsDatosClasifiAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsDatosClasifi.Data:= ServerNomina.GetEmpleadoClasifi( Empresa, Empleado, YearDefault, Ord( PeriodoTipo ), PeriodoNumero );
     end;
end;

procedure TdmNomina.GetDatosNominaClasifi( const iEmpleado,iYear,iPeriodo,iTipo :Integer);
begin
     cdsDatosClasifi.Data:= ServerNomina.GetEmpleadoClasifi( dmCliente.Empresa, iEmpleado, iYear, iTipo, iPeriodo );
end;

procedure TdmNomina.GetDatosNominaClasifiAfectacion( const iEmpleado,iYear,iPeriodo,iTipo :Integer);
begin
     cdsDatosClasifiAfectacion.Data:= ServerNomina.GetEmpleadoClasifi( dmCliente.Empresa, iEmpleado, iYear, iTipo, iPeriodo );
end;

procedure TdmNomina.cdsDatosClasifiAfterOpen(DataSet: TDataSet);
begin
     with cdsDatosClasifi do
     begin
          //ListaFija( 'NO_STATUS', lfStatusPeriodo );
          FieldByName('NO_STATUS').OnGetText := NO_STATUSGetText;
          ListaFija( 'NO_LIQUIDA', lfLiqNomina );
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
          FieldByName( 'NO_USR_PAG' ).OnGetText := US_CODIGOGetText;
          ListaFija('NO_TIMBRO', lfStatusTimbrado);
     end;
end;

procedure TdmNomina.cdsDatosClasifiAlCrearCampos(Sender: TObject);
begin
     with cdsDatosClasifi do
     begin
{          with dmTablas do
          begin
               CreateSimpleLookUp( cdsNivel1, 'NIVEL1', 'CB_NIVEL1' );
               CreateSimpleLookUp( cdsNivel2, 'NIVEL2', 'CB_NIVEL2' );
               CreateSimpleLookUp( cdsNivel3, 'NIVEL3', 'CB_NIVEL3' );
               CreateSimpleLookUp( cdsNivel4, 'NIVEL4', 'CB_NIVEL4' );
               CreateSimpleLookUp( cdsNivel5, 'NIVEL5', 'CB_NIVEL5' );
               CreateSimpleLookUp( cdsNivel6, 'NIVEL6', 'CB_NIVEL6' );
               CreateSimpleLookUp( cdsNivel7, 'NIVEL7', 'CB_NIVEL7' );
               CreateSimpleLookUp( cdsNivel8, 'NIVEL8', 'CB_NIVEL8' );
               CreateSimpleLookUp( cdsNivel9, 'NIVEL9', 'CB_NIVEL9' );
          end; }
          with dmCatalogos do
          begin
               CreateSimpleLookUp( cdsRPatron, 'PATRON', 'CB_PATRON' );
               CreateSimpleLookUp( cdsClasifi, 'CLASIFI', 'CB_CLASIFI' );
               CreateSimpleLookUp( cdsPuestos, 'PUESTO', 'CB_PUESTO' );
               CreateSimpleLookUp( cdsTurnos, 'TURNO', 'CB_TURNO' );
               CreateSimpleLookUp( cdsSSocial, 'SSOCIAL', 'CB_TABLASS' );
          end;
         ListaFija( 'NO_PAGO', lfMetodoPagoSAT );
     end;
end;

procedure TdmNomina.cdsDatosClasifiAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsDatosClasifi do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile ( ServerNomina.GrabaEmpleadoClasifi( dmCliente.Empresa, Delta, ErrorCount ) );
               TressShell.SetDataChange( [ enPeriodo, enNomina ] );
          end;
     end;
end;

procedure TdmNomina.cdsDatosClasifiAfterDelete(DataSet: TDataSet);
begin
     cdsDatosClasifi.Enviar;
end;

procedure TdmNomina.cdsDatosClasifiAfterEdit(DataSet: TDataSet);
begin
     with cdsDatosClasifi do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'NO_USR_PAG' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmNomina.cdsDatosClasifiAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( NomEditDatosClasifi_DevEx, TNomEditDatosClasifi_DevEx );
end;

procedure TdmNomina.cdsDatosClasifiNewRecord(DataSet: TDataSet);
begin
     with cdsDatosClasifi do
     begin
          with dmCliente do
          begin
               FieldByName( 'PE_YEAR' ).AsInteger := YearDefault;
               FieldByName( 'PE_TIPO' ).AsInteger := Ord( PeriodoTipo );
               FieldByName( 'PE_NUMERO' ).AsInteger := PeriodoNumero;
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               with GetDatosPeriodoActivo do
               begin
                    with dmRecursos.GetDatosClasificacion( Empleado, Fin ) do
                    begin
                         FieldByName( 'CB_ZONA_GE' ).AsString := ZonaGeografica;
                         FieldByName( 'CB_PUESTO' ).AsString := Puesto;
                         FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
                         FieldByName( 'CB_TURNO' ).AsString := Turno;
                         FieldByName( 'CB_PATRON' ).AsString := Patron;
                         FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
                         FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
                         FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
                         FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
                         FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
                         FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
                         FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
                         FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
                         FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
                         {$ifdef ACS}
                         FieldByName( 'CB_NIVEL10' ).AsString := Nivel10;
                         FieldByName( 'CB_NIVEL11' ).AsString := Nivel11;
                         FieldByName( 'CB_NIVEL12' ).AsString := Nivel12;
                         {$endif}
                         FieldByName( 'NO_APROBA' ).AsInteger := Ord(ssfSinAprobar);
                         FieldByName( 'NO_GLOBAL' ).AsString := K_GLOBAL_NO;
                         // Tabla de prestaciones
                         FieldByName ('CB_TABLASS').AsString := TablaPrestaciones;
                    end;
                    with dmRecursos.GetDatosSalario( Empleado, Inicio, Fin ) do
                    begin
                         FieldByName( 'CB_SAL_INT' ).AsFloat := Integrado;
                         FieldByName( 'CB_SALARIO' ).AsFloat := Diario;
                    end;
                    with ZetaCommonTools.DatosJornadaFromVariant( ServerNomina.GetDatosJornada( dmCliente.Empresa, FieldByName( 'CB_TURNO' ).AsString, Inicio, Fin ) ) do
                    begin
                         FieldByName( 'NO_JORNADA' ).AsFloat := Jornada;
                         FieldByName( 'NO_D_TURNO' ).AsInteger := Dias;
                    end;
               end;
          end;
          FieldByName( 'NO_STATUS' ).AsInteger  := Ord( spNueva );
          // GLOBAL Simulacion de RH ...
          FieldByName( 'NO_FUERA' ).AsString  := K_GLOBAL_NO;
          {Datos de Clasificacion}
     end;
end;

{ ******* cdsTotales ********* }

procedure TdmNomina.cdsTotalesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsTotales.Data:= ServerNomina.GetDatosTotales( Empresa, YearDefault, Ord( PeriodoTipo ), PeriodoNumero,FNivelConfidencialidad );
     end;
end;

procedure TdmNomina.cdsTotalesAfterOpen(DataSet: TDataSet);
begin
     with cdsTotales do
     begin
  //        ListaFija('PE_STATUS', lfStatusPeriodo );
          FieldByName( 'PE_STATUS' ).OnGetText := PE_STATUSGetText;
          FieldByName( 'PE_TIMBRO' ).OnGetText := PE_STATUSTIMBRADOGetText;
          ListaFija( 'PE_USO', lfUsoPeriodo );
     end;
     with cdsTotales do
     begin
          FieldByName( 'PE_MES' ).OnGetText := PE_MESGetText;
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmNomina.PE_MESGetText( Sender: TField; var Text: String; DisplayText: Boolean);
var
   iMes: Integer;
begin
     iMes := Sender.AsInteger;
     case iMes of
          1..12: Text := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}LongMonthNames[ iMes ];
          13: Text := 'Mes 13'
     else
         Text := '< INDEFINIDO >';
     end;

end;

{ ************* cdsDatosAsist ****************** }

procedure TdmNomina.cdsDatosAsistAlAdquirirDatos(Sender: TObject);
var
   Tarjetas: OleVariant;
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo( Parametros );
             with Parametros do
             begin
                  AddInteger( 'Empleado', GetEmpleadoGrid );
                  with GetDatosPeriodoActivo do
                  begin
{$ifdef QUINCENALES}
                       AddDate( 'FechaIni', InicioAsis );
                       AddDate( 'FechaFin', FinAsis );
{$else}
                       AddDate( 'FechaIni', Inicio );
                       AddDate( 'FechaFin', Fin );
{$endif}
                  end;
             end;
             cdsDatosAsist.Data := ServerNomina.GetNomDatosAsist( Empresa, Parametros.VarValues, Tarjetas );
        end;
        with cdsMovDatosAsist do
        begin
             Data := Tarjetas
{$ifdef FALSE}
             if not cdsDatosAsist.IsEmpty then

             else
                 if Active then
                    EmptyDataSet;
{$endif}
        end;
     finally
            Parametros.Free;
     end;
end;

procedure TdmNomina.cdsDatosAsistAfterOpen(DataSet: TDataSet);
begin
     with cdsDatosAsist do
     begin
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
          FieldByName( 'NO_USER_RJ' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmNomina.cdsDatosAsistAlCrearCampos(Sender: TObject);
begin
     with cdsDatosAsist do
     begin
          CreateSimpleLookup( dmCatalogos.cdsTurnos, 'TURNO', 'CB_TURNO' );
          //ListaFija( 'NO_STATUS', lfStatusPeriodo );
          FieldByName('NO_STATUS').OnGetText := NO_STATUSGetText;
          MaskTime( 'NO_HOR_OK' );
          MaskFecha( 'NO_FEC_OK' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'NO_DES_OK', 'NO_SUP_OK' );
     end;
end;

procedure TdmNomina.cdsDatosAsistAlModificar(Sender: TObject);
begin
     //ZBaseEdicion.ShowFormaEdicion( EditPrenomina, TEditPrenomina );
     EditarGridPrenomina;
end;

{$ifdef VER130}
procedure TdmNomina.cdsMovDatosAsistReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     sError := GetErrorDescription( E );
     Action := cdsMovDatosAsist.ReconciliaError(DataSet,UpdateKind,E, 'AU_FECHA', sError );
end;
{$else}
procedure TdmNomina.cdsMovDatosAsistReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     sError := GetErrorDescription( E );
     Action := cdsMovDatosAsist.ReconciliaError(DataSet,UpdateKind,E, 'AU_FECHA', sError );
end;
{$endif}

//US 14933: El usuario requiere que los procesos de nomina sean funcionales con cualquier tipo de periodo (Parte1)
{$ifdef VER130}
procedure TdmNomina.cdsExcepGlobalesReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileMontoGlobalError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmNomina.cdsExcepGlobalesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileMontoGlobalError(Dataset, UpdateKind, E);
end;
{$endif}

procedure TdmNomina.cdsDatosAsistAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   Parametros: TZetaParams;
begin
     ErrorCount := 0;
     with cdsDatosAsist do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     with cdsMovDatosAsist do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Parametros := TZetaParams.Create;
               try
                  dmCliente.CargaActivosPeriodo( Parametros );
                  Reconciliar( ServerNomina.GrabaPrenomina( dmCliente.Empresa, Parametros.VarValues, Delta, ErrorCount ) );
                  if ( ChangeCount = 0 ) then
                  begin
                       { Refresca otros Dataset }
                       TressShell.SetDataChange( [ enPeriodo, enNomina, enAusencia ] );
                       { Refresca cdsDatosAsist }
                       cdsDatosAsist.Refrescar;
                  end
                  else
                  begin
                       cdsDatosAsist.Edit; // Padre, en modo de Edici�n
                  end;
               finally
                      Parametros.Free;
               end;
          end;
     end;
end;

procedure TdmNomina.cdsDatosAsistAfterCancel(DataSet: TDataSet);
begin
     cdsMovDatosAsist.CancelUpdates;
end;

procedure TdmNomina.EditarGridPrenomina;
begin
     FEditandoGrid:= TRUE;
     try
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridPrenomina_DevEx, TGridPrenomina_DevEx, FALSE );
        // Verifica si cambi� Empleado
        with dmCliente do
             if ( FEmpleadoGrid <> Empleado ) and ( SetEmpleadoNumero( FEmpleadoGrid ) ) then
                TressShell.CambiaEmpleadoActivos;
     finally
        FEditandoGrid:= FALSE;
     end;
end;

{ ********** cdsMovDatosAsist ************* }

procedure TdmNomina.cdsMovDatosAsistAlCrearCampos(Sender: TObject);
begin
     with cdsMovDatosAsist do
     begin
          FieldByName( 'AU_FECHA' ).OnGetText := AU_FECHAGetText;
          FieldByName( 'HO_CODIGO' ).OnChange := HO_CODIGOChange;
          FieldByName( 'AU_HORAS' ).OnChange  := AU_HORASChange;
          FieldByName( 'AU_DES_TRA' ).OnChange  := AU_DES_TRAChange;
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          MaskHoras( 'AU_HORAS' );
          MaskHoras( 'AU_EXTRAS' );
          MaskHoras( 'AU_NUM_EXT' );          
          MaskHoras( 'AU_DOBLES' );
          MaskHoras( 'AU_TRIPLES' );
          MaskHoras( 'AU_TARDES' );
          MaskHoras( 'AU_PER_CG' );
          MaskHoras( 'AU_PER_SG' );
          MaskHoras( 'AU_DES_TRA' );
          MaskTime( 'CHECADA1' );
          MaskTime( 'CHECADA2' );
          MaskTime( 'CHECADA3' );
          MaskTime( 'CHECADA4' );
     end;
end;

procedure TdmNomina.cdsMovDatosAsistAfterOpen(DataSet: TDataSet);
begin
     with cdsMovDatosAsist do
     begin
          FieldByName( 'AU_DOBLES' ).OnChange := DoblesTriplesChange;
          FieldByName( 'AU_TRIPLES' ).OnChange := DoblesTriplesChange;
     end;
end;

procedure TdmNomina.DoblesTriplesChange(Sender: TField);
begin
     with Sender.Dataset do
     begin
          FieldByName( 'AU_EXTRAS' ).AsFloat := FieldByName( 'AU_DOBLES' ).AsFloat + FieldByName( 'AU_TRIPLES' ).AsFloat;
     end;
end;

procedure TdmNomina.HO_CODIGOChange(Sender: TField);
begin
     with Sender.Dataset do
     begin
          FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmNomina.AU_HORASChange(Sender: TField);
const
     K_I_FALTA_INJUSTIFICADA = 'FI';
     K_I_RETARDO = 'RE';
var
   rJornada : Currency;
begin
     with Sender.Dataset do
     begin
          rJornada := dmCatalogos.GetJornadaHorario( FieldByName( 'HO_CODIGO' ).AsString );
          if ( eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger ) = auHabil ) and
             ( ZetaCommonTools.MiRound( Sender.AsFloat, 4 ) < ZetaCommonTools.MiRound( rJornada, 4 )  ) then
          begin
               if ( Sender.AsFloat <= 0 ) then
               begin
                    FieldByName( 'AU_TARDES' ).AsFloat := 0;
                    FieldByName( 'AU_TIPO' ).AsString  := K_I_FALTA_INJUSTIFICADA;
               end
               else
               begin
                    FieldByName( 'AU_TARDES' ).AsFloat := ( rJornada - Sender.AsFloat );
                    FieldByName( 'AU_TIPO' ).AsString  := K_I_RETARDO;
               end;
          end
          else
          begin
               FieldByName( 'AU_TARDES' ).AsFloat := 0;
               FieldByName( 'AU_TIPO' ).AsString  := VACIO;
          end;
     end;
end;

procedure TdmNomina.AU_DES_TRAChange(Sender: TField);
const
     K_I_FALTA_SS = 'FSS';
begin
     with Sender.Dataset do
     begin
          if ( eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger ) <> auHabil ) and
             ( FieldByName( 'AU_TIPO' ).AsString = K_I_FALTA_SS ) then
          begin
               FieldByName( 'AU_TIPO' ).AsString  := VACIO;
          end;
     end;
end;

procedure TdmNomina.AU_FECHAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.DataSet.IsEmpty then
        Text := ''
     else
         Text := Sender.AsString + ' -  ' + ObtieneElemento( lfDiasSemana, DayOfWeek( Sender.AsDateTime ) - 1 );
end;

procedure TdmNomina.cdsMovDatosAsistBeforeInsert(DataSet: TDataSet);
begin
     Abort;      // No se puede Agregar
end;

procedure TdmNomina.cdsMovDatosAsistBeforePost(DataSet: TDataSet);
var
   nHoras: Currency;
{$ifdef QUINCENALES}
   rJornada : Currency;
{$endif}
   sIncidencia, sIncidenciaGlobal: String;
begin
     with cdsMovDatosAsist do
     begin
          if dmCliente.PuedeCambiarTarjetaDlg( cdsMovDatosAsist ) then
          begin
               { US_CODIGO solo cambia cuando el registro es modificado desde la Prenomina }
               FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
               if ( eStatusAusencia( FieldByName( 'AU_STATUS' ).AsInteger ) = auHabil ) then
               begin
                    {
                    nHoras := FieldByName( 'AU_HORAS' ).AsFloat + FieldByName( 'AU_PER_CG' ).AsFloat + FieldByName( 'AU_PER_SG' ).AsFloat;
                    }
                    nHoras := FieldByName( 'AU_HORAS' ).AsFloat + FieldByName( 'AU_EXTRAS' ).AsFloat + FieldByName( 'AU_PER_SG' ).AsFloat;
                    sIncidencia := FieldByName( 'AU_TIPO' ).AsString;
                    sIncidenciaGlobal := Global.GetGlobalString( K_INCIDENCIA_FI );
                    if ( nHoras = 0 ) and ( StrVacio( sIncidencia )) then
                       FieldByName( 'AU_TIPO' ).AsString := sIncidenciaGlobal
                    else
                    begin
                         if ( nHoras > 0 ) and ( Trim( sIncidencia ) = Trim( sIncidenciaGlobal ) ) then
                            FieldByName( 'AU_TIPO' ).AsString := VACIO;
                    end;
{$ifdef QUINCENALES}
                    if ( eTipoDiaAusencia( FieldByName( 'AU_TIPODIA' ).AsInteger ) = daNormal ) and
                       ( nHoras > 0 ) then      // Solo acumula horas no trabajadas si el empleado asisti� a trabajar
                    begin
                         rJornada := dmCatalogos.GetJornadaHorario( FieldByName( 'HO_CODIGO' ).AsString );
                         FieldByName( 'AU_HORASNT' ).AsFloat := ZetaCommonTools.rMax( rJornada - FieldByName( 'AU_HORAS' ).AsFloat, 0 );
                    end
                    else
                         FieldByName( 'AU_HORASNT' ).AsFloat := 0;
{$endif}
               end;
          end
          else
          begin
               {CV: Si quiero cancelar el registro porque la Fecha NO se puede cambiar.
               Y no deja salir al usuario del registro hasta que lo cancele}
               Cancel;
               Abort;
          end;
     end;
end;

procedure TdmNomina.cdsMovDatosAsistAfterEdit(DataSet: TDataSet);
begin
     cdsDatosAsist.Edit;
end;

{ ********** cdsMontos ************ }

procedure TdmNomina.cdsMontosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsMontos.Data:= ServerNomina.GetMontos( Empresa, Empleado, YearDefault, Ord( PeriodoTipo ), PeriodoNumero );
     end;
end;

procedure TdmNomina.cdsMontosAlCrearCampos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          //ListaFija( 'NO_STATUS', lfStatusPeriodo );
          FieldByName('NO_STATUS').OnGetText := NO_STATUSGetText;
          ListaFija( 'NO_LIQUIDA', lfLiqNomina );
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
          ListaFija( 'NO_APROBA', lfStatusSimFiniquitos );
     end;
end;

{ ********** cdsMovMontos *********** }

procedure TdmNomina.cdsMovMontosAlAdquirirDatos(Sender: TObject);
var
   oTotales: OleVariant;
begin
     // PENDIENTE: Evita AV por recursividad
     // Ya se hace en la Forma de Consulta: dmCatalogos.cdsConceptos.Conectar;
     with dmCliente do
     begin
          cdsMovMontos.Data := ServerNomina.GetMovMontos( Empresa, GetEmpleadoGrid, YearDefault,
                                                          Ord( PeriodoTipo ), PeriodoNumero,
                                                          oTotales );
          cdsMontos.Data := oTotales;
     end;
end;

procedure TdmNomina.cdsMovMontosAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsTPresta.Conectar;
     dmCatalogos.cdsConceptos.Conectar;  
     with TZetaClientDataSet( Sender ) do
     begin
          with FieldByName( 'MO_PERCEPC' ) do
          begin
               OnGetText := MO_PERCEPCGetText;
               OnChange  := cdsMovMontosMO_PERCEPCChange;
          end;
          with FieldByName( 'MO_DEDUCCI' ) do
          begin
               OnGetText := MO_DEDUCCIGetText;
               OnChange  := cdsMovMontosMO_DEDUCCIChange;
          end;
          with FieldByName( 'CO_NUMERO' ) do
          begin
               OnValidate := cdsExcepMontosCO_NUMEROValidate;
               OnChange   := cdsMovMontosCO_NUMEROChange;
          end;
          FieldByName( 'MO_REFEREN' ).OnValidate:= cdsExcepMontosMO_REFERENValidate;
          MaskNumerico( 'MO_X_ISPT', '#,0.00;;#' );
          MaskNumerico( 'MO_IMP_CAL', '#,0.00;;#' );
          with dmCatalogos do
          begin
               CreateSimpleLookup( cdsConceptos, 'DESCRIPCION', 'CO_NUMERO' );
               CreateLookUp( cdsConceptos, 'CO_TIPO', 'CO_NUMERO', 'CO_NUMERO', 'CO_TIPO' );
          end;
          ListaFija( 'CO_TIPO', lfTipoConcepto );
     end;
end;

procedure TdmNomina.cdsMovMontosCO_NUMEROChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          if ConceptoEsDeduccion( Sender.DataSet ) then
          begin
{               with FieldByName( 'MO_DEDUCCI' ) do
                    if ( AsFloat = 0 ) then
                       AsFloat := FUltimaDeduccion; }
               FieldByName( 'MO_PERCEPC' ).AsFloat:= 0;
          end
          else
          begin
{               with FieldByName( 'MO_PERCEPC' ) do
                    if ( AsFloat = 0 ) then
                       AsFloat := FUltimaPercepcion; }
               FieldByName( 'MO_DEDUCCI' ).AsFloat:= 0;
          end;
     end;
end;

procedure TdmNomina.MO_DEDUCCIGetText(Sender: TField; var Text: String;
          DisplayText: Boolean);
begin
    if ConceptoEsDeduccion( Sender.DataSet ) then
        Text := FormatFloat( '#,0.00', Sender.AsFloat )
    else
        Text := '';
end;

procedure TdmNomina.cdsMovMontosMO_DEDUCCIChange(Sender: TField);
begin
     with Sender do
          if ( not ConceptoEsDeduccion( DataSet ) ) and ( AsFloat <> 0 ) then
             AsFloat := 0;
end;

procedure TdmNomina.MO_PERCEPCGetText(Sender: TField; var Text: String;
          DisplayText: Boolean);
begin
    if ConceptoEsDeduccion( Sender.DataSet ) then
        Text := ''
    else
        Text := FormatFloat( '#,0.00', Sender.AsFloat );
end;

procedure TdmNomina.cdsMovMontosMO_PERCEPCChange(Sender: TField);
begin
     with Sender do
          if ( ConceptoEsDeduccion( DataSet ) ) and ( AsFloat <> 0 ) then
             AsFloat := 0;
end;

function TdmNomina.ConceptoEsDeduccion(DataSet: TDataSet): Boolean;
begin
    Result := eTipoConcepto( DataSet.FieldByName( 'CO_TIPO' ).AsInteger ) in [ coDeduccion, coObligacion, coResultados ];
end;

procedure TdmNomina.cdsMovMontosAfterOpen(DataSet: TDataSet);
begin
     with TZetaClientDataSet( DataSet ) do
     begin
          MaskNumerico( 'US_CODIGO', '#,0;;#' );
     end;
end;

procedure TdmNomina.cdsMovMontosAlModificar(Sender: TObject);
begin
     FRefrescaMovMontos := FALSE;
     ZBaseEdicion_DevEx.ShowFormaEdicion( NomEditDatosMontos_DevEx, TNomEditDatosMontos_DevEx );
     if FRefrescaMovMontos then
        TressShell.SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
end;

procedure TdmNomina.cdsMovMontosAlBorrar(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Monto ?' ) then
          begin
               FRefrescaMovMontos := FALSE;
               Delete;
               Enviar;
               if FRefrescaMovMontos then
                  TressShell.SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
          end;
     end;
end;

procedure TdmNomina.cdsMovMontosAlEnviarDatos(Sender: TObject);
begin
     GrabaExcepcionesMonto( cdsMovMontos );
end;

procedure TdmNomina.cdsMovMontosNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'MO_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'MO_PER_CAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'MO_REFEREN' ).AsString := '';
          if FEditandoGrid then
             FieldByName( 'CB_CODIGO' ).AsInteger := FEmpleadoGrid
          else
             FieldByName( 'CB_CODIGO' ).AsInteger := dmCliente.Empleado;
          //FieldByName( 'MO_DEDUCCI' ).AsFloat := FUltimaDeduccion;
          //FieldByName( 'MO_PERCEPC' ).AsFloat := FUltimaPercepcion;
          //FieldByName( 'CO_NUMERO' ).AsInteger := FUltimoConcepto;
     end;
end;

function TdmNomina.PasaLimiteMonto(iConcepto:Integer;Monto:TPesos):Boolean;
begin
     with dmCatalogos.cdsConceptos do
     begin
          Result := ( ( zStrToBool( FieldByName( 'CO_VER_INF' ).AsString ) ) and ( Monto < FieldByName('CO_LIM_INF').AsFloat ) );
          if not Result then
          begin
               Result :=( ( zStrToBool( FieldByName('CO_VER_SUP').AsString ) ) and ( Monto > FieldByName('CO_LIM_SUP').AsFloat ) );
          end;
     end;
end;

function TdmNomina.VerificaGrupoRegistroExcepciones(var sMensaje:string):Boolean;
var
   GrupoActivo: Integer;
begin
     GrupoActivo :=  dmCliente.GetGrupoActivo;
     with dmCatalogos.cdsConceptos do
     begin
          Result := ( ( not( GrupoActivo = D_GRUPO_SIN_RESTRICCION ) ) and ( zStrToBool( FieldByName('CO_VER_ACC').AsString ) and ( not ( Pos( IntToStr( GrupoActivo ),FieldByName('CO_GPO_ACC').AsString ) > 0 ) ) ) );
          if Result then
               sMensaje :=  Format( 'No tiene permisos para capturar excepciones al concepto #%d - %s',[FieldByName('CO_NUMERO').AsInteger ,FieldByName('CO_DESCRIP').AsString ] );
     end;
end;

function TdmNomina.HayConflictoLimites(iConcepto:Integer;Monto:TPesos;DataSet:TDataSet;var sMensaje:string;var lMensaje:Boolean):Boolean;
begin
     lMensaje := Revisa( D_NOM_EXCEPCIONES_LIM_MONTO);
     Result := False;
     with dmCatalogos.cdsConceptos do
     begin
          if Locate ('CO_NUMERO',iConcepto,[])then
          begin
               Result := ( ( zStrToBool( FieldByName( 'CO_VER_INF' ).AsString ) ) and ( Monto < FieldByName('CO_LIM_INF').AsFloat ) );
               if Result then
               begin
                    if lMensaje then
                    begin
                         Result := not ZConfirm('L�mites de Monto',Format ( 'El monto m�nino permitido para el concepto #%d es: %s '+ CR_LF+'� Desea Registrar la Excepci�n ?',[iConcepto,FormatFloat('$#,###.00',FieldByName('CO_LIM_INF').AsFloat )] ),0,mbNo );
                    end
                    else
                         sMensaje := Format ( 'El monto m�nimo permitido para el concepto #%d es: %s',[iConcepto,FormatFloat('$#,###.00',FieldByName('CO_LIM_INF').AsFloat ) ] );
               end
               else
               begin
                    Result :=( ( zStrToBool( FieldByName('CO_VER_SUP').AsString ) ) and ( Monto > FieldByName('CO_LIM_SUP').AsFloat ) );
                    if Result then
                    begin
                         if lMensaje then
                         begin
                             Result := not ZConfirm('L�mites de Monto',Format ( 'El monto m�ximo permitido para el concepto #%d es: %s '+ CR_LF+'� Desea Registrar la Excepci�n ?',[iConcepto,FormatFloat('$#,###.00',FieldByName('CO_LIM_SUP').AsFloat )] ),0,mbNo );
                         end
                         else
                              sMensaje := Format ( 'El monto m�ximo permitido para el concepto #%d es: %s',[iConcepto,FormatFloat('$#,###.00',FieldByName('CO_LIM_SUP').AsFloat ) ] );
                    end;
               end;
          end;
     end;
end;


procedure TdmNomina.cdsMovMontosBeforePost(DataSet: TDataSet);
var
   sMensaje:string;
   lMensaje:Boolean;
begin
     with DataSet do
     begin
          if ( FieldByName( 'CO_NUMERO' ).AsInteger <= 0 ) then
             DataBaseError( 'El N�mero de Concepto debe Ser Mayor que Cero' );
          if HayConflictoLimites(FieldByName( 'CO_NUMERO' ).AsInteger,(FieldByName('MO_PERCEPC').AsFloat + FieldByName('MO_DEDUCCI').AsFloat),DataSet,sMensaje,lMensaje)then
              if lMensaje then
                  Abort
              else
                 DatabaseError(sMensaje);

          with dmCliente do
          begin
               with GetDatosPeriodoActivo do
               begin
                    FieldByName( 'PE_NUMERO' ).AsInteger := Numero;
                    FieldByName( 'PE_YEAR' ).AsInteger := Year;
                    FieldByName( 'PE_TIPO' ).AsInteger := Ord( Tipo );
               end;
               FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
          end;
     end;
end;

procedure TdmNomina.cdsMovMontosAfterPost(DataSet: TDataSet);
begin
     with cdsMovMontos do
     begin
          //FUltimoConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
{          if ( FieldByName( 'MO_DEDUCCI' ).AsFloat > 0 ) then
             FUltimaDeduccion := FieldByName( 'MO_DEDUCCI' ).AsFloat;
          if ( FieldByName( 'MO_PERCEPC' ).AsFloat > 0 ) then
             FUltimaPercepcion := FieldByName( 'MO_PERCEPC' ).AsFloat; }
     end;
end;

{$ifdef VER130}
procedure TdmNomina.cdsMovMontosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          Opera := OperaConflictoMontos( DataSet );
          if Opera <> ocIgnorar then
          begin
               FOperacion:= Ord( Opera );
               Action := raCorrect;
          end;
          FConflictoMontos:= TRUE;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$else}
procedure TdmNomina.cdsMovMontosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          Opera := OperaConflictoMontos( DataSet );
          if Opera <> ocIgnorar then
          begin
               FOperacion:= Ord( Opera );
               Action := raCorrect;
          end;
          FConflictoMontos:= TRUE;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$endif}

procedure TdmNomina.EditarGridMovMontos;
var
   sOldIndex: String;
begin
     FConflictoMontos := FALSE;
     with cdsMovMontos do
     begin
          sOldIndex := IndexFieldNames;
          FEditandoGrid:= TRUE;
          dmCliente.SetLookupEmpleado( eLookEmpNominas );
          try
             IndexFieldNames:= VACIO;         // No debe tener activo el Indice
             ZbaseGridEdicion_DevEx.ShowGridEdicion( GridMovMontos_DevEx, TGridMovMontos_DevEx, FALSE );
             // Verifica si cambi� Activo de Empleado
             with dmCliente do
                  if ( FEmpleadoGrid <> Empleado ) and ( SetEmpleadoNumero( FEmpleadoGrid ) ) then
                     TressShell.CambiaEmpleadoActivos;
             //Refrescar;
          finally
             IndexFieldNames := sOldIndex;
             FEditandoGrid:= FALSE;
             dmCliente.ResetLookupEmpleado;
          end;
     end;
end;

{ ************* cdsExcepMontos ************** }

procedure TdmNomina.cdsExcepMontosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsExcepMontos.Data:= ServerNomina.GetDatosExcepMontos( Empresa, YearDefault, Ord( PeriodoTipo ), PeriodoNumero, SoloAltas );
     end;
end;

procedure TdmNomina.cdsExcepMontosAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsTPresta.Conectar;
     with cdsExcepMontos do
     begin
          MaskPesos( 'MO_MONTO' );
          MaskPesos( 'MO_X_ISPT' );
          MaskPesos( 'MO_IMP_CAL' );
          ListaFija( 'CO_TIPO', lfTipoConcepto );
          FieldByName( 'CB_CODIGO' ).OnValidate:= ExcepcionesCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange:= cdsExcepMontosCB_CODIGOChange;
          FieldByName( 'CO_NUMERO' ).OnValidate:= cdsExcepMontosCO_NUMEROValidate;
          FieldByName( 'CO_NUMERO' ).OnChange:= cdsExcepMontosCO_NUMEROChange;
          FieldByName( 'MO_PERCEPC' ).OnGetText := cdsExcepMontosMO_PERCEPCGetText;
          FieldByName( 'MO_DEDUCCI' ).OnGetText := cdsExcepMontosMO_DEDUCCIGetText;
          FieldByName( 'MO_REFEREN' ).OnValidate:= cdsExcepMontosMO_REFERENValidate;
     end;
end;

procedure TdmNomina.cdsExcepMontosAlBorrar(Sender: TObject);
begin
     with cdsExcepMontos do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Esta Excepci�n ?' ) then
          begin
               Delete;
               Enviar;
          end;
     end;
end;

procedure TdmNomina.cdsExcepMontosAlModificar(Sender: TObject);
begin
     with dmCliente do
     begin
          SetTipoLookupEmpleado( eLookEmpNominas );
          try
             ZBaseEdicion_DevEx.ShowFormaEdicion( NomEditExcepMontos_DevEx, TNomEditExcepMontos_DevEx );
          finally
             SetTipoLookupEmpleado( eLookEmpGeneral );
          end;
     end;
end;

procedure TdmNomina.cdsExcepMontosNewRecord(DataSet: TDataSet);
begin
     with cdsExcepMontos do
     begin
          with dmCliente do
          begin
               FieldByName( 'PE_YEAR' ).AsInteger := YearDefault;
               FieldByName( 'PE_TIPO' ).AsInteger := Ord( PeriodoTipo );
               FieldByName( 'PE_NUMERO' ).AsInteger := PeriodoNumero;
               FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
          end;
          FieldByName( 'MO_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'MO_PER_CAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'MO_REFEREN' ).AsString := '';
          FieldByName( 'MO_PERCEPC' ).AsFloat := 0.0;
          FieldByName( 'MO_DEDUCCI' ).AsFloat := 0.0;
          FieldByName( 'MO_MONTO' ).AsFloat := 0.0;
     end;
end;

procedure TdmNomina.cdsExcepMontosBeforePost(DataSet: TDataSet);
var
   sMensaje:string;
   lMensaje:Boolean;
begin
     with cdsExcepMontos do
     begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger = 0 ) then
          begin
               FieldByName( 'CB_CODIGO' ).FocusControl;
               DataBaseError( 'N�mero De Empleado No Puede Quedar Vac�o' );
          end;
          if ( FieldByName( 'CO_NUMERO' ).AsInteger = 0 ) then
          begin
               FieldByName( 'CO_NUMERO' ).FocusControl;
               DataBaseError( 'N�mero De Concepto No Puede Quedar Vac�o' );
          end;
          // Asignar Percepcion o Deduccion
          if ( eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger ) in [ coDeduccion, coObligacion ] ) then
          begin
               FieldByName( 'MO_DEDUCCI' ).AsFloat := FieldByName( 'MO_MONTO' ).AsFloat;
               FieldByName( 'MO_PERCEPC' ).AsFloat := 0.0;
          end
          else
          begin
               FieldByName( 'MO_PERCEPC' ).AsFloat := FieldByName( 'MO_MONTO' ).AsFloat;
               FieldByName( 'MO_DEDUCCI' ).AsFloat := 0.0;
          end;
          if HayConflictoLimites(FieldByName( 'CO_NUMERO' ).AsInteger,FieldByName( 'MO_MONTO' ).AsFloat,DataSet,sMensaje,lMensaje)then
              if lMensaje then
                  Abort
              else
                 DatabaseError(sMensaje);
     end;
end;

procedure TdmNomina.cdsExcepMontosAfterEdit(DataSet: TDataSet);
begin
     with cdsExcepMontos do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmNomina.cdsExcepMontosAfterPost(DataSet: TDataSet);
begin
     with cdsExcepMontos do
     begin
          FLastConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
          FLastMonto := FieldByName( 'MO_MONTO' ).AsFloat;
          FLastReferen := FieldByName( 'MO_REFEREN' ).AsString;
     end;
end;

procedure TdmNomina.cdsExcepMontosAlEnviarDatos(Sender: TObject);
begin
     GrabaExcepcionesMonto( cdsExcepMontos );
end;

{$ifdef VER130}
procedure TdmNomina.cdsExcepMontosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          Opera := OperaConflictoMontos( DataSet );
          if Opera <> ocIgnorar then
             with DataSet do
             begin
                  Edit;
                  FieldByName( 'OPERACION' ).NewValue := Ord( Opera );
                  Post;
                  FConflictoMontos:= TRUE;
                  Action := raCorrect;
             end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$else}
procedure TdmNomina.cdsExcepMontosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          Opera := OperaConflictoMontos( DataSet );
          if Opera <> ocIgnorar then
             with DataSet do
             begin
                  //FOperacion:= Ord( Opera ); Ya no es necesario desde que elimino el metodo VerificaRegistroExcepciones Rev. 1.36
                  Edit;
                  FieldByName( 'OPERACION' ).NewValue := Ord( Opera );
                  if Opera <> ocIgnorar then
                  begin
                       Post;
                       FConflictoMontos:= TRUE;
                       Action := raCorrect;
                  end
                  else
                  begin
                       FConflictoMontos := False;
                       Action := raAbort;
                  end;

             end;
     end
     else if FK_VIOLATION( E ) then { FK_VIOLATION }
     {$ifndef INTERFAZ_ELECTROLUX}
     {$ifndef LITTELFUSE_INTERFAZ}
       MessageDlg( 'El tipo de n�mina es invalido.', mtError, [ mbOK ], 0 )
     {$else}
     {$else}
	      Dminterfase.EscribeErrorExterno( 'El tipo de n�mina es invalido.' )
     {$endif}
     {$endif}
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$endif}

procedure TdmNomina.cdsExcepMontosCB_CODIGOChange(Sender: TField);
{$ifndef INTERFAZ_ELECTROLUX}
{$ifndef LITTELFUSE_INTERFAZ}
var
   oCursor: TCursor;
   sReferencia: string;
{$endif}
{$endif}
begin
     {$ifndef INTERFAZ_ELECTROLUX}
     {$ifndef LITTELFUSE_INTERFAZ}
     with cdsExcepMontos do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;

          try
             if dmCliente.cdsEmpleadoLookup.Active = True then
                FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          finally
                 Screen.Cursor := oCursor;
          end;
          // Asigna Valores Default
          if ( FLastConcepto > 0  ) and ( FieldByName( 'CO_NUMERO' ).AsInteger = 0 ) then  // Es Registro Nuevo
          begin
               if dmTablas.cdsTPresta.Locate( 'TB_CONCEPT', FLastConcepto, [] ) then
               begin
                    if GetReferenciaPrestamo( Sender.AsInteger,
                                              dmTablas.cdsTPresta.FieldByName('TB_CODIGO').AsString,
                                              sReferencia ) then
                    begin
                         FieldByName( 'CO_NUMERO' ).AsInteger := FLastConcepto;
                         //FieldByName('MO_REFEREN').AsString := sReferencia;
                    end;
               end
               else
               begin
                    FieldByName( 'CO_NUMERO' ).AsInteger := FLastConcepto;
                    FieldByName( 'MO_REFEREN' ).AsString := FLastReferen;
               end;
               FieldByName( 'MO_MONTO' ).AsFloat := FLastMonto;
          end;
     end;
     {$endif}
     {$endif}
end;

procedure TdmNomina.cdsExcepMontosCO_NUMEROValidate(Sender: TField);
{$ifndef INTERFAZ_ELECTROLUX}
{$ifndef LITTELFUSE_INTERFAZ}
var
   sDescrip, sReferencia,sMensaje: String;
   lActivo, lUsarConfidencialidad : Boolean;
{$endif}
{$endif}
begin
{$ifndef INTERFAZ_ELECTROLUX}
{$ifndef LITTELFUSE_INTERFAZ}
     lActivo := True;
     lUsarConfidencialidad := True;
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
//               if ( dmCatalogos.cdsConceptos.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then
               if ( AsInteger >= 1000 ) then
                  DataBaseError( 'N�mero de Concepto Debe Ser Menor a 1000' )
               else if ( not dmCatalogos.cdsConceptos.LookupKey( IntToStr( AsInteger ), '', sDescrip, lActivo, lUsarConfidencialidad) ) then
                  DataBaseError( 'No Existe el Concepto de N�mina #' + IntToStr( Sender.AsInteger ) )
               else
               if ( not lActivo ) then
                  DataBaseError( 'No Est� activo el Concepto de N�mina #' + IntToStr( Sender.AsInteger ) )
               else
               if VerificaGrupoRegistroExcepciones(sMensaje)then
                    DataBaseError (sMensaje)
               else
               begin
                    if dmTablas.cdsTPresta.Locate( 'TB_CONCEPT', AsInteger, [] ) then
                    begin
                         if DataSet.FieldByName('CB_CODIGO').AsInteger > 0 then
                         begin
                              if GetReferenciaPrestamo( DataSet.FieldByName('CB_CODIGO').AsInteger,
                                                        dmTablas.cdsTPresta.FieldByName('TB_CODIGO').AsString,
                                                        sReferencia ) then
                              begin
                                   DataSet.FieldByName('MO_REFEREN').AsString := sReferencia;
                              end
                              else
                                  DataBaseError( Format( 'El Empleado no Contiene Ning�n Pr�stamo Asociado al Concepto %d', [AsInteger] ) );
                         end;
                    end;
               end;
          end
          else
              DataBaseError( 'N�mero de Concepto Debe Ser Mayor a Cero' );
     end;
{$endif}
{$endif}
end;

procedure TdmNomina.cdsExcepMontosCO_NUMEROChange(Sender: TField);
begin
     with cdsExcepMontos do
     begin
          FieldByName( 'CO_DESCRIP' ).AsString := dmCatalogos.cdsConceptos.FieldByName( 'CO_DESCRIP' ).AsString;
          FieldByName( 'CO_TIPO' ).AsInteger := dmCatalogos.cdsConceptos.FieldByName( 'CO_TIPO' ).AsInteger;
     end;
end;

procedure TdmNomina.cdsExcepMontosMO_PERCEPCGetText(Sender: TField; var Text: String;
          DisplayText: Boolean);
begin
    if ConceptoEsDeduccion( cdsExcepMontos ) then
        Text := ''
    else
        Text := FormatFloat( '#,0.00', Sender.AsFloat );
end;

procedure TdmNomina.cdsExcepMontosMO_DEDUCCIGetText(Sender: TField; var Text: String;
          DisplayText: Boolean);
begin
    if ConceptoEsDeduccion( cdsExcepMontos ) then
        Text := FormatFloat( '#,0.00', Sender.AsFloat )
    else
        Text := '';
end;

procedure TdmNomina.cdsExcepMontosMO_REFERENValidate(Sender: TField);
{$ifndef INTERFAZ_ELECTROLUX}
{$ifndef LITTELFUSE_INTERFAZ}
var
   sTipo: String;
{$endif}
{$endif}
begin
{$ifndef INTERFAZ_ELECTROLUX}
{$ifndef LITTELFUSE_INTERFAZ}
     with Sender do
     begin
          if ( Pos( ZetaCommonClasses.UnaComilla, AsString ) > 0 ) then
             DataBaseError( 'Referencia No Puede Incluir Comillas' )
          else if dmTablas.cdsTPresta.Locate( 'TB_CONCEPT', DataSet.FieldByName( 'CO_NUMERO' ).AsInteger, [] ) then
          begin
               if ( DataSet.FieldByName('CB_CODIGO').AsInteger > 0 ) then
               begin
                    sTipo := dmTablas.cdsTPresta.FieldByName('TB_CODIGO').AsString;
                    if NOT ValidaReferenciaPrestamo( DataSet.FieldByName('CB_CODIGO').AsInteger,
                                                     sTipo,
                                                     AsString ) then
                       DataBaseError(Format( 'El Empleado no Contiene Ning�n Pr�stamo Tipo "%s" con la Referencia "%s"',[sTipo,AsString] ) );
               end;
          end;
     end;
{$endif}
{$endif}
end;

procedure TdmNomina.EditarGridMontos( const lSoloAltas: Boolean );
begin
     SoloAltas := lSoloAltas;
     try
        dmCliente.SetLookupEmpleado( eLookEmpNominas );
        FEditandoGrid:= TRUE;
        ZbaseGridEdicion_DevEx.ShowGridEdicion( GridNomMontos_DevEx, TGridNomMontos_DevEx, lSoloAltas );
     finally
        FEditandoGrid:= FALSE;
        dmCliente.ResetLookupEmpleado;
     end;
     SoloAltas := False;
     // Para que muestre los registros que se agregaron
     //if lSoloAltas then
        cdsExcepMontos.Refrescar;
end;

{ ********** cdsExcepGlobales ********** }

procedure TdmNomina.cdsExcepGlobalesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsExcepGlobales.Data:= ServerNomina.GetDatosExcepGlobales( Empresa, YearDefault, Ord( PeriodoTipo ), PeriodoNumero );
     end;
end;

procedure TdmNomina.cdsExcepGlobalesAlCrearCampos(Sender: TObject);
begin
     with cdsExcepGlobales do
     begin
          CreateSimpleLookup( dmCatalogos.cdsConceptos, 'CO_DESCRIP', 'CO_NUMERO' );
          CreateLookUp( dmCatalogos.cdsConceptos, 'CO_TIPO', 'CO_NUMERO', 'CO_NUMERO', 'CO_TIPO' );
          FieldByName( 'CO_NUMERO' ).OnValidate:= cdsExcepGlobalCO_NUMEROValidate;
          ListaFija( 'CO_TIPO', lfTipoConcepto );
     end;
end;

procedure TdmNomina.cdsExcepGlobalCO_NUMEROValidate(Sender: TField);
var
   sMensaje:string;
begin
     if VerificaGrupoRegistroExcepciones(sMensaje) then
        DatabaseError(sMensaje); 
end;

procedure TdmNomina.cdsExcepGlobalesAfterOpen(DataSet: TDataSet);
begin
     with cdsExcepGlobales do
     begin
          MaskPesos( 'MG_MONTO' );
          ListaFija( 'CO_TIPO', lfTipoConcepto );
     end;
end;

procedure TdmNomina.cdsExcepGlobalesAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( NomEditExcepGlobales_DevEx, TNomEditExcepGlobales_DevEx );
end;

procedure TdmNomina.cdsExcepGlobalesNewRecord(DataSet: TDataSet);
begin
     with cdsExcepGlobales, dmCliente.GetDatosPeriodoActivo do
     begin
          FieldByName( 'PE_YEAR' ).AsInteger := Year;
          FieldByName( 'PE_TIPO' ).AsInteger := Ord(Tipo);
          FieldByName( 'PE_NUMERO' ).AsInteger := Numero;
          FieldByName( 'MG_FIJO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmNomina.cdsExcepGlobalesAfterEdit(DataSet: TDataSet);
begin
     cdsExcepGlobales.FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmNomina.cdsExcepGlobalesAfterDelete(DataSet: TDataSet);
begin
   cdsExcepGlobales.Enviar;
end;

procedure TdmNomina.cdsExcepGlobalesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsExcepGlobales do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerNomina.GrabaExcepGlobales( dmCliente.Empresa, Delta, ErrorCount ) );
               TressShell.SetDataChange( [ enPeriodo,enNomina,enMovGral ] );
          end;
     end;
end;

procedure TdmNomina.cdsExcepGlobalesBeforePost(DataSet: TDataSet);
var
   sMensaje:string;
   lMensaje:Boolean;
begin
     with cdsExcepGlobales  do
     begin
          if not ZetaCommonTools.zStrToBool( FieldByName( 'MG_FIJO' ).AsString ) then
             FieldByName( 'MG_MONTO' ).AsInteger := 0;

          if HayConflictoLimites(FieldByName( 'CO_NUMERO' ).AsInteger,FieldByName( 'MG_MONTO' ).AsFloat,DataSet,sMensaje,lMensaje)then
             if lMensaje then
                  Abort
             else
                 DatabaseError(sMensaje);
           if ( dmCliente.GetDatosPeriodoActivo.Tipo <> K_PERIODO_VACIO ) and ( dmCliente.GetDatosPeriodoActivo.Numero = 0 ) then
           begin
                ZetaDialogo.zInformation( 'Aviso', 'No se ha definido ning�n per�odo ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, Ord( dmCliente.GetDatosPeriodoActivo.Tipo ) ), 0 );
                Abort;
           end;
     end;
end;

{ cdsSimulaMontos }

function TdmNomina.DerechoSoloAplicarGlobal: Boolean;
begin
     Result := FALSE;
     if NOT CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_LIQUID_GLOBAL) then
     begin
          if Global.GetGlobalBooleano ( K_GLOBAL_SIM_FINIQ_APROBACION ) then
          begin
               if NOT CheckDerecho(D_EMP_SIM_FINIQUITOS_TOTALES,K_DERECHO_APROBA_GLOBAL) then
               begin
                    Result := TRUE;
               end
          end
          else
          begin
               Result := TRUE;
          end;
     end;
end;

function TdmNomina.DerechoSoloAplicarIndividual: Boolean;
begin
     Result := FALSE;
     if NOT CheckDerecho(D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_SIST_KARDEX) then
     begin
          if Global.GetGlobalBooleano( K_GLOBAL_SIM_FINIQ_APROBACION ) then
          begin
               if NOT CheckDerecho(D_EMP_SIM_FINIQUITOS_EMPLEADO,K_DERECHO_NIVEL0) then
               begin
                    Result := TRUE;
               end
          end
          else
          begin
               Result := TRUE;
          end;
     end;
end;

procedure TdmNomina.cdsSimulaMontosAlAdquirirDatos(Sender: TObject);
var
   oTotales: OleVariant;
//   lSoloAplicar: Boolean;
begin
     with dmCliente do
     begin
          cdsSimulaMontos.Data := ServerNomina.GetMovMontos( Empresa, Empleado, YearDefault,
                                                             cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger,
                                                             Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ),
                                                             oTotales );

          cdsSimulaTotales.Data := oTotales;

{          if DerechoSoloAplicarIndividual then
          begin
               with cdsSimulaTotales do
               begin
                    if (eStatusPeriodo(FieldByName('NO_STATUS').AsInteger)<spCalculadaTotal) OR
                            (dmCliente.cdsEmpleado.FieldByName( 'CB_ACTIVO' ).AsString ='S' )  then
                       begin
                            cdsSimulaMontos.Close ;
                            cdsSimulaTotales.Close;
                       end;
               end;
          end;  }
     end;
end;

procedure TdmNomina.cdsSimulaMontosBeforePost(DataSet: TDataSet);
begin
     cdsMovMontosBeforePost( DataSet );
     with DataSet do
     begin
          FieldByName( 'PE_NUMERO' ).AsInteger := Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );
          FieldByName( 'PE_TIPO' ).AsInteger := dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger ;
     end;
end;

procedure TdmNomina.cdsSimulaMontosAlEnviarDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with Parametros do
        begin
             AddInteger( 'Year', dmCliente.YearDefault );
             AddInteger( 'Tipo', dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger ) ;
             AddInteger( 'Numero', Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
             AddBoolean('ExcederLimites',Revisa(D_NOM_EXCEPCIONES_LIM_MONTO ));
        end;
        GrabaExcepcionesMontoParams( cdsSimulaMontos, Parametros );
     finally
            Parametros.Free;
     end;
end;

procedure TdmNomina.cdsSimulaMontosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( NomEditDatosMontosSimulacion_DevEx, TNomEditDatosMontosSimulacion_DevEx );
end;

procedure TdmNomina.EditarGridSimulacionMontos;
var
   sOldIndex: String;
begin
     FConflictoMontos := FALSE;
     with cdsSimulaMontos do
     begin
          sOldIndex := IndexFieldNames;
          FEditandoGrid:= TRUE;
          dmCliente.SetLookupEmpleado( eLookEmpNominas );
          try
             IndexFieldNames:= VACIO;         // No debe tener activo el Indice
             ZbaseGridEdicion_DevEx.ShowGridEdicion( GridSimulacionMontos_DevEx, TGridSimulacionMontos_DevEx, FALSE );
          finally
             IndexFieldNames := sOldIndex;
             FEditandoGrid:= FALSE;
             dmCliente.ResetLookupEmpleado;
          end;
     end;
end;

function TdmNomina.ExistePeriodoSimulacion( var sMensaje: String; const sOperacion: String): Boolean;
var
   iNumPeriodo: Integer;
   TipoPeriodo : eTipoPeriodo;
begin
     iNumPeriodo := Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );
     with dmCatalogos do
     begin
          TipoPeriodo := eTipoPeriodo( dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger );
          GetDatosPeriodo( dmCliente.YearDefault, TipoPeriodo );
          Result := cdsPeriodo.Locate( 'PE_NUMERO', iNumPeriodo, [] );
          if ( not Result ) then
             sMensaje := 'No se tiene definido el periodo para simulaci�n de finiquitos ' + CR_LF +
                         'para los periodos de tipo ' + ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, Ord( TipoPeriodo ) ) + ',' + CR_LF +
                         'no se puede ' + sOperacion;
     end;
end;

{ ************* cdsPagoRecibos ***************** }

procedure TdmNomina.cdsPagoRecibosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsPagoRecibos.Data := ServerNomina.GetPagoRecibos( Empresa, YearDefault, Ord( PeriodoTipo ), PeriodoNumero, FSoloAltas );
     end;
end;

procedure TdmNomina.cdsPagoRecibosAlCrearCampos(Sender: TObject);
begin
     with cdsPagoRecibos do
     begin
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsPagoRecibosCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsPagoRecibosCB_CODIGOChange;
          FieldByName( 'NO_FEC_PAG' ).OnGetText := cdsPagoRecibosNO_FEC_PAGGetText;
     end;
end;

procedure TdmNomina.cdsPagoRecibosBeforeOpen(DataSet: TDataSet);
begin
     FUltimoPagoRecibo := dmCliente.FechaDefault;
end;

procedure TdmNomina.cdsPagoRecibosNewRecord(DataSet: TDataSet);
begin
     with cdsPagoRecibos do
     begin
          with dmCliente do
          begin
               FieldByName( 'PE_YEAR' ).AsInteger := YearDefault;
               FieldByName( 'PE_TIPO' ).AsInteger := Ord( PeriodoTipo );
               FieldByName( 'PE_NUMERO' ).AsInteger := PeriodoNumero;
          end;
     end;
end;

procedure TdmNomina.cdsPagoRecibosBeforePost(DataSet: TDataSet);
begin
     with cdsPagoRecibos do
     begin
          FieldByName( 'NO_USR_PAG' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmNomina.cdsPagoRecibosAfterPost(DataSet: TDataSet);
begin
     FUltimoPagoRecibo := cdsPagoRecibos.FieldByName( 'NO_FEC_PAG' ).AsDateTime;
end;

procedure TdmNomina.cdsPagoRecibosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPagoRecibos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerNomina.GrabaPagoRecibos( dmCliente.Empresa, Delta, ErrorCount ) );
               TressShell.SetDataChange( [ enNomina ] );
          end;
     end;
end;

{$ifdef VER130}
procedure TdmNomina.cdsPagoRecibosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
    Action := cdsPagoRecibos.ReconciliaError(DataSet,UpdateKind,E,'CB_CODIGO', GetErrorDescription( E ));
end;
{$else}
procedure TdmNomina.cdsPagoRecibosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
    Action := cdsPagoRecibos.ReconciliaError(DataSet,UpdateKind,E,'CB_CODIGO', GetErrorDescription( E ));
end;
{$endif}


procedure TdmNomina.cdsPagoRecibosNO_FEC_PAGGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsDateTime = NullDateTime ) then
     begin
          if ( Sender.Dataset.FieldByName( 'CB_CODIGO' ).AsInteger > 0  )then
             Text := 'Sin Pagar'
          else
              Text:= '';
     end
     else
         Text := ZetaCommonTools.FechaCorta( Sender.AsDateTime );
end;

procedure TdmNomina.cdsPagoRecibosCB_CODIGOValidate(Sender: TField);
var
   FFechaRecibo: TDateTime;
   FUserRecibo: Integer;
   iEmpleado: TNumEmp;
   oCursor: TCursor;
begin
     iEmpleado := Sender.AsInteger;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmCliente do
        begin
             cdsEmpleadoLookup.Conectar;
             if ( cdsEmpleadoLookup.GetDescripcion( IntToStr( iEmpleado ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                DataBaseError( 'No Existe el Empleado #' + IntToStr( iEmpleado ) )
             else
             begin
                  if not ServerNomina.ValidaNominaEmpleado( Empresa,
                                                            iEmpleado,
                                                            YearDefault,
                                                            Ord( PeriodoTipo ),
                                                            PeriodoNumero,
                                                            FFechaRecibo,
                                                            FUserRecibo ) then
                  begin
                       DataBaseError( 'El Empleado #' + IntToStr( iEmpleado ) + ' No Tiene Registro de Nomina Para El Periodo Activo' );
                  end
                  else
                  begin
                       if FMarcarRecibosPagados then   // Registrar como pagados
                       begin
                            if ( FFechaRecibo > NullDateTime ) then   // Ya esta registrado como Pagado
                            begin
                                 if not ZetaMsgDlg.ConfirmaCambio( 'El Recibo del Empleado #' + IntToStr( iEmpleado ) +
                                                                    CR_LF +
                                                                    'Ya Fu� Pagado El ' + ZetaCommonTools.FechaCorta( FFechaRecibo ) +
                                                                    CR_LF +
                                                                    '� Desea Cambiar La Fecha ?' ) then
                                 begin
                                      Abort;
                                 end;
                            end;
                       end
                       else                            // Registrar como NO Pagados
                       begin
                            if ( FFechaRecibo = NullDateTime ) then
                            begin
                                 DataBaseError( 'El Recibo del Empleado #' + IntToStr( iEmpleado ) + ' Ya Est� Registrado Como NO Pagado' );
                            end;
                       end;
                  end;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmNomina.cdsPagoRecibosCB_CODIGOChange(Sender: TField);
var
   oCursor: TCursor;
begin
     with cdsPagoRecibos do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          finally
                 Screen.Cursor := oCursor;
          end;
          if FMarcarRecibosPagados then
             FieldByName( 'NO_FEC_PAG' ).AsDateTime := FUltimoPagoRecibo
          else
             FieldByName( 'NO_FEC_PAG' ).AsDateTime := 0;
          FieldByName( 'NO_USR_PAG' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmNomina.EditarGridPagoRecibos;
var
   PagoRecibosForma : TForm;
   //DevEx , permite accesar al indice del radiobutton de la forma clasica o la forma nueva
function GetIndiceForma(const iValor : Integer = 0;  bCambiarValor : Boolean = False): Boolean;
begin
     if (bCambiarValor) then
     begin
          TPagoRecibos_DevEx(PagoRecibosForma).RGMarcar.ItemIndex:= iValor;
          Result := False;
     end
     else
         Result := (TPagoRecibos_DevEx(PagoRecibosForma).RGMarcar.ItemIndex = 0 );
end;

begin
     try
        PagoRecibosForma := TPagoRecibos_DevEx.Create( Application );

        with PagoRecibosForma do
        begin
             if Global.GetGlobalBooleano( K_DEFAULT_RECIBOS_PAGADOS ) then
                GetIndiceForma(1, True)
             else
                GetIndiceForma(0, True);
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  FMarcarRecibosPagados := GetIndiceForma;
                  SoloAltas := True;
                  try
                     dmCliente.SetLookupEmpleado;
                     ZbaseGridEdicion_DevEx.ShowGridEdicion( GridPagoRecibos_DevEx, TGridPagoRecibos_DevEx, True );
                  finally
                     dmCliente.ResetLookupEmpleado;
                  end;
                  SoloAltas := False;
             end;
        end;
     finally
            PagoRecibosForma.Free;
     end;
end;

{ ************** cdsLiquidacion *************** }

procedure TdmNomina.cdsLiquidacionAlAdquirirDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        dmCliente.CargaActivosTodos( Parametros );
        GetDatosLiquidacion( Parametros, cdsLiquidacion );
     finally
            Parametros.Free;
     end;
end;

procedure TdmNomina.GetDatosLiquidacion( Parametros: TZetaParams; oClientDataSet: TZetaClientDataSet );
var
   Ahorros, Prestamos: OleVariant;
begin
     Parametros.AddDate( 'FechaBaja', FFechaBajaLiquidacion );
     with dmCliente do
     begin
          oClientDataSet.Data:= ServerNomina.GetLiquidacion( Empresa, Empleado, Parametros.VarValues, Ahorros, Prestamos, GetDatosEmpleadoActivo.Ingreso );
          cdsAhorros.Data := Ahorros;
          cdsPrestamos.Data := Prestamos;
     end;
     TressShell.SetDataChange( [ enPeriodo, enNomina ] );
end;

procedure TdmNomina.cdsLiquidacionAlModificar(Sender: TObject);
begin
     with cdsLiquidacion do
     begin
          Refrescar;
          if FieldByName( 'NO_EXISTE_LIQ' ).AsBoolean or
             ZetaDialogo.ZErrorConfirm( 'Liquidaci�n', 'El Empleado Ya Fu� Liquidado. � Desea Calcularle Otra Liquidaci�n ?', 0, mbNo ) then
          begin
               ZBaseDlgModal_DevEx.ShowDlgModal( EditLiquidacion_DevEx, TEditLiquidacion_DevEx );
          end;
     end;
end;

procedure TdmNomina.cdsLiquidacionAlEnviarDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo( Parametros );
             GrabaLiquidacionParams( Parametros, cdsLiquidacion );
        end;
     finally
            Parametros.Free;
     end;
end;

procedure TdmNomina.GrabaLiquidacionParams( Parametros: TZetaParams; oClientDataset: TZetaClientDataSet);

   function GetDescMonto( const rMonto: Currency ): String;
   begin
        if ( rMonto < 0 ) then
           Result := 'Devoluci�n: '
        else
            Result := 'Deducci�n: ';
        Result := Result + FormatFloat( '$#,0.00', Abs( rMonto ) );
   end;

begin
     with cdsAhorros do
     begin
          First;
          while not Eof do
          begin
               if ( eTipoAhorro( FieldByName( 'TB_LIQUIDA' ).AsInteger ) = taSaldarPreguntando ) then
               begin
                    Edit;
                    FieldByName( 'SALDAR' ).AsString := ZetaCommonTools.zBoolToStr( ZetaDialogo.ZConfirm( 'Saldar Ahorro',
                                                                                                         '�Desea Saldar el Ahorro : ' +
                                                                                                         CR_LF +
                                                                                                         '[ ' +
                                                                                                         FieldByName( 'AH_TIPO' ).AsString +
                                                                                                         '=' +
                                                                                                         FieldByName( 'TB_ELEMENT' ).AsString +
                                                                                                         ' ] ?' +
                                                                                                         CR_LF +
                                                                                                         GetDescMonto( FieldByName( 'AH_SALDO' ).AsFloat ) ,0, mbYes ) );
                    Post;
               end;
               Next;
          end;
          MergeChangeLog;
     end;
     with cdsPrestamos do
     begin
          First;
          while not Eof do
          begin
               if ( eTipoAhorro( FieldByName( 'TB_LIQUIDA' ).AsInteger ) = taSaldarPreguntando ) then
               begin
                    Edit;
                    FieldByName( 'SALDAR' ).AsString := ZetaCommonTools.zBoolToStr( ZetaDialogo.ZConfirm( 'Saldar Pr�stamo',
                                                                                                         '�Desea Saldar el Pr�stamo : ' +
                                                                                                         CR_LF +
                                                                                                         '[ ' +
                                                                                                         FieldByName( 'PR_TIPO' ).AsString +
                                                                                                         '=' +
                                                                                                         FieldByName( 'TB_ELEMENT' ).AsString +
                                                                                                         CR_LF +
                                                                                                         'Referencia: ' + FieldByName( 'PR_REFEREN' ).AsString +
                                                                                                         CR_LF +
                                                                                                         ' ] ?' +
                                                                                                         CR_LF +
                                                                                                         GetDescMonto( FieldByName( 'PR_SALDO' ).AsFloat ) ,0, mbYes ) );
                    Post;
               end;
               Next;
          end;
          MergeChangeLog;
     end;
     with oClientDataset do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          MergeChangeLog; { Se envian los datos, no el Delta }
          ServerNomina.GrabaLiquidacion( dmCliente.Empresa, Parametros.VarValues, Data, cdsAhorros.Data, cdsPrestamos.Data );
          TressShell.SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas ] );
     end;
end;

{ ************** cdsSimulaLiquidacion *************** }

procedure TdmNomina.cdsSimulaLiquidacionAlAdquirirDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        dmCliente.CargaActivosTodos( Parametros );
        with Parametros do
        begin
             AddInteger( 'Tipo', dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger );
             AddInteger( 'Numero', Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
        end;
        GetDatosLiquidacion( Parametros, cdsSimulaLiquidacion );
     finally
            Parametros.Free;
     end;
end;

procedure TdmNomina.cdsSimulaLiquidacionAlEnviarDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo( Parametros );
             with Parametros do
             begin
                  AddInteger( 'Tipo', dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger ) ;
                  AddInteger( 'Numero', Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
             end;
             GrabaLiquidacionParams( Parametros, cdsSimulaLiquidacion );
        end;
     finally
            Parametros.Free;
     end;
end;

procedure TdmNomina.cdsSimulaLiquidacionAlModificar(Sender: TObject);
begin
     with cdsSimulaLiquidacion do
     begin
          Refrescar;
          if FieldByName( 'NO_EXISTE_LIQ' ).AsBoolean or
             ZetaDialogo.ZErrorConfirm( 'Liquidaci�n', 'El Empleado Ya Fu� Liquidado. � Desea Calcularle Otra Liquidaci�n ?', 0, mbNo ) then
          begin
               ZBaseDlgModal_DevEx.ShowDlgModal( EditSimulaLiquidacion_DevEx, TEditSimulaLiquidacion_DevEx );
          end;
     end;
end;

procedure TdmNomina.SimulaLiquidacion;
begin
     ProcesaLiquidacion( cdsSimulaLiquidacion );
end;

{/**********************Prestamos************************************************/}
function TdmNomina.GetReferenciaPrestamo( const iEmpleado: integer; const sTipo: string; var sReferencia: string ): Boolean;
begin
     { Optimizar lectura de prestamos del empleado - Si ya est� el ClientDataSet no se manda refrescar }
     with cdsPrestamosRef do
     begin
          //if ( not Active ) or ( IsEmpty ) or ( FieldByName( 'CB_CODIGO' ).AsInteger <> iEmpleado ) then
          dmRecursos.GetPrestamos( cdsPrestamosRef, iEmpleado );
          Result := Locate( 'PR_TIPO', sTipo, [] );
          if Result then
             sReferencia := FieldByName('PR_REFEREN').AsString;
     end;
end;

function TdmNomina.ValidaReferenciaPrestamo( const iEmpleado: integer; const sTipo, sReferencia: string ): Boolean;
begin
     { Optimizar lectura de prestamos del empleado - Si ya est� el ClientDataSet no se manda refrescar }
     with cdsPrestamosRef do
     begin
          //if ( not Active ) or ( IsEmpty ) or ( FieldByName( 'CB_CODIGO' ).AsInteger <> iEmpleado ) then
          dmRecursos.GetPrestamos( cdsPrestamosRef, iEmpleado );
          Result := Locate( 'PR_TIPO;PR_REFEREN', VarArrayOf([sTipo,sReferencia]), [] );
     end;

end;

{*******************cdsPolizas*******************************}

procedure TdmNomina.cdsPolizasAlAdquirirDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     dmSistema.cdsUsuarios.Conectar;
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo(Parametros);
             cdsPolizas.Data:= ServerNomina.GetDatosPoliza( Empresa, Parametros.VarValues );
        end;
     finally
            Parametros.Free;
     end;
end;


procedure TdmNomina.cdsPolizasAlCrearCampos(Sender: TObject);
begin
     with cdsPolizas do
     begin
          MaskFecha('PH_FECHA');
          MaskTime( 'PH_HORA' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookup, 'US_DESCRIP', 'US_CODIGO' );
          ListaFija( 'PH_STATUS',lfStatusPoliza );
     end;
end;

procedure TdmNomina.cdsPolizasAfterDelete(DataSet: TDataSet);
begin
     cdsPolizas.Enviar;
end;

procedure TdmNomina.cdsPolizasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPolizas do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               with dmCliente do
               begin
                    Reconciliar( ServerNomina.GrabaDatosPoliza( Empresa,Delta,ErrorCount ) )
               end;
          end;
     end;
end;

procedure TdmNomina.cdsPolizasAlAgregar(Sender: TObject);
begin
    TressShell._Per_GenerarPolizaExecute(Self);
     //TressShell.ReconectaFormaActiva;
end;

procedure TdmNomina.AplicaFiniquito;
var
   Parametros: TZetaParams;
   sMensaje: String;
begin
     //Agregar la baja del empleado;
     //Esta llamada tambien revisa la entrega de herramientas.
     if ZetaDialogo.ZConfirm( 'Simulaci�n de Finiquitos', Format('El finiquito con status %s ser� copiado a la N�mina de Baja del Empleado.',[ObtieneElemento(lfStatusSimFiniquitos,Ord(StatusSimulacionActual))]) + CR_LF +
                                                          '�Desea continuar?', 0, mbNo ) then
     begin
          with dmCliente do
          begin
               with GetDatosEmpleadoActivo do
               begin
                    if Activo and ( ZAccesosMgr.CheckDerecho( D_EMP_REG_BAJA, K_DERECHO_CONSULTA ) ) and
                       ZetaDialogo.zConfirm( 'Liquidaci�n de Empleado', '� Desea Efectuar la Baja de Este Empleado ?', 0, mbYes ) then
                    begin
                         dmRecursos.AgregaKardex( K_T_BAJA );
                    end;
               end;
          end;
          with dmCliente do
          begin
               with GetDatosEmpleadoActivo do
               begin
                    if Activo then
                    begin
                         ZetaDialogo.ZError( 'Simulaci�n de Finiquitos', Format( 'El empleado %d:%s no esta dado de baja', [Empleado, cdsEmpleado.FieldByName('PRETTYNAME').AsString] ) +
                                                                        CR_LF + 'No se puede aplicar la Simulaci�n de Finiquitos', 0 );
                    end
                    else
                    begin
                         if ExistePeriodoSimulacion( sMensaje, 'Aplicar finiquito') then
                         begin
                              Parametros := TZetaParams.Create;
                              try
                                 with Parametros do
                                 begin
                                      //Nomina DE LA BAJA
                                      AddInteger( 'Year', cdsEmpleado.FieldByName('CB_NOMYEAR').AsInteger );
                                      AddInteger( 'Tipo', cdsEmpleado.FieldByName('CB_NOMTIPO').AsInteger );
                                      AddInteger( 'Numero' , cdsEmpleado.FieldByName('CB_NOMNUME').AsInteger );

                                      //NOMINA DE LA SIMULACION
                                      AddInteger( 'YearFiniquito', dmCliente.YearDefault );
                                      AddInteger( 'TipoFiniquito', dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger );
                                      AddInteger( 'NumeroFiniquito', Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
                                      AddInteger( 'Empleado', Empleado );
                                 end;
                                 ServerNomina.AplicaFiniquito( dmCliente.Empresa, Parametros.VarValues );
                                 TressShell.SetDataChange( [ enPeriodo, enNomina ] );
                              finally
                                     Parametros.Free;
                              end;
                         end
                         else
                             ZetaDialogo.ZError('Simulaci�n de Finiquitos', sMensaje,0 );
                    end;
               end;
          end;
     end;
end;

procedure TdmNomina.cdsFonTotAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          ConectaFonTot( ImssMes, ImssYear );
end;

procedure TdmNomina.cdsFonTotRsAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          ConectaFonTotRs( ImssMes, ImssYear );
end;

procedure TdmNomina.cdsFonTotRsAlCrearCampos(Sender: TObject);
begin
     with cdsFonTotRs do
     begin
          ListaFija( 'FT_STATUS', lfStatusLiqImss );
          CreateCalculated( 'FT_PORCENTAJE', ftString, 10 );
          CreateCalculated( 'FT_DIFERENCIAS', ftFloat, 0 );
     end;
end;

function TdmNomina.GetStatusFonacot( const iMes, iYear: Integer ): eStatusLiqImss;
var
   oDeltaAnterior: OleVariant;
begin
     with cdsFonTot do
     begin
          DisableControls;
          oDeltaAnterior:= Data;
          try
             ConectaFonTot( iMes, iYear );
             Result:= eStatusLiqImss( FieldByName('FT_STATUS').AsInteger );
             Data:= oDeltaAnterior;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmNomina.ConectaFonTot( const iMes, iYear: Integer );
begin
     with dmCliente, cdsFonTot do
     begin
          Data:= ServerNomina.GetTotalesFonacot( Empresa, iYear, iMes );

          if ( IsEmpty ) then
          begin
               Edit;
               FieldByName('FT_STATUS').AsInteger:= 0;
               MergeChangeLog;
          end;
     end;
end;

procedure TdmNomina.ConectaFonTotRS (const iMes, iYear: Integer);
begin
     with dmCliente, cdsFonTotRs do
     begin
          if not FTodasLasRazones then
             Data:= ServerNomina.GetTotalesFonacotRs( Empresa, iYear, iMes, RazonSocial )
          else
              Data:= ServerNomina.GetTotalesFonacotRs( Empresa, iYear, iMes, VACIO );

          if ( IsEmpty ) then
          begin
               Edit;
               FieldByName('FT_STATUS').AsInteger:= 0;
               MergeChangeLog;
          end;
     end;
end;

procedure TdmNomina.cdsFonTotAlCrearCampos(Sender: TObject);
begin
     with cdsFonTot do
     begin
          ListaFija( 'FT_STATUS', lfStatusLiqImss );
          CreateCalculated( 'FT_PORCENTAJE', ftString, 10 );
          CreateCalculated( 'FT_DIFERENCIAS', ftFloat, 0 );
     end;
end;

procedure TdmNomina.cdsFonTotCalcFields(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if (FieldByName( 'FT_RETENC' ).AsFloat > 0 ) and (FieldByName( 'FT_NOMINA' ).AsFloat > 0) then
          begin
               FieldByName( 'FT_PORCENTAJE' ).AsString := FloatToStr ( Redondea (((FieldByName( 'FT_NOMINA' ).AsFloat + FieldByName( 'FT_AJUSTE' ).AsFloat) * 100) / (FieldByName( 'FT_RETENC' ).AsFloat  ) ) ) + '%';
          end
          else
               FieldByName( 'FT_PORCENTAJE' ).AsString := '0.00%';


          FieldByName( 'FT_DIFERENCIAS' ).AsFloat := FieldByName( 'FT_RETENC' ).AsFloat - ( FieldByName( 'FT_NOMINA' ).AsFloat + FieldByName( 'FT_AJUSTE' ).AsFloat ); //FloatToStr ( FieldByName( 'FT_RETENC' ).AsFloat - ( FieldByName( 'FT_NOMINA' ).AsFloat + FieldByName( 'FT_AJUSTE' ).AsFloat ) );

     end;
end;

procedure TdmNomina.cdsFonTotDetalleAfterOpen(DataSet: TDataSet);
begin
     with cdsFonTotDetalle do
     begin
          FieldByName ('PR_FON_MOT').OnGetText := PR_FON_MOTGetText;
     end;
end;

procedure TdmNomina.PR_FON_MOTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsString = 'CanceladoOtro' ) then
     begin
          Text:= 'Otro';
     end
     else
         Text := Sender.AsString;
end;

procedure TdmNomina.cdsFonTotDetalleAlAdquirirDatos(Sender: TObject);
var sDiferencias: String;
begin
     sDiferencias := VACIO;
     if FonacotSoloDiferencias = TRUE then
        sDiferencias := ' AND DIFERENCIA <> 0 ';

     with dmCliente do
     begin
          if not FTodasLasRazones then
             cdsFonTotDetalle.Data := ServerNomina.FonacotDetallePagoCedula (Empresa, ImssYear, ImssMes, RazonSocial, sDiferencias)
          else
              cdsFonTotDetalle.Data := ServerNomina.FonacotDetallePagoCedula (Empresa, ImssYear, ImssMes, VACIO, sDiferencias);
     end;
end;

procedure TdmNomina.cdsFonTotDetalleAlCrearCampos(Sender: TObject);
begin
     with cdsFonTotDetalle do
     begin
          MaskPesos ('PF_PAGO');
          MaskPesos ('FC_NOMINA');
          MaskPesos ('FC_AJUSTE');
          MaskPesos ('DIFERENCIA');
          ListaFija( 'PR_STATUS', lfStatusPrestamo );
     end;
end;

procedure TdmNomina.cdsFonTotDiferenciasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          if not FTodasLasRazones then
             cdsFonTotDiferencias.Data := ServerNomina.FonacotDetalleDiferencias (Empresa, ImssYear, ImssMes, RazonSocial)
          else
              cdsFonTotDiferencias.Data := ServerNomina.FonacotDetalleDiferencias (Empresa, ImssYear, ImssMes, VACIO);
     end;
end;

procedure TdmNomina.cdsFonTotDiferenciasAlCrearCampos(Sender: TObject);
begin
     with cdsFonTotDiferencias do
     begin
          MaskPesos( 'Otro' );
          MaskPesos( 'BajaIMSS' );
          MaskPesos( 'Incapacidad' );
          MaskPesos( 'Otro' );
          MaskPesos( 'Total' );
     end;
end;

procedure TdmNomina.AbreCierraCalculo( const eAccion: eSubeBaja );
const
     K_MENSAJE = 'El status de c�lculo Fonacot debe ser igual a ';
     K_ENCABEZADO = '� Atenci�n !';
     K_CONFIRM = ' �  Desea %s el c�lculo de Fonacot ?';
     aConstAccion: array[ eSubeBaja ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('cerrar', 'abrir');
var
   eStatusFon: eStatusLiqImss;
begin
    if  ( ZConfirm(K_ENCABEZADO, Format( K_CONFIRM, [ aConstAccion[eAccion] ] ),0, mbNo  ) ) then
    begin
         with cdsFonTot do
         begin
              eStatusFon:= eStatusLiqImss( FieldByName('FT_STATUS').AsInteger);
              case( eAccion ) of
                    eSubir:
                    begin
                         if( eStatusFon = slCalculadaTotal ) then
                              Inc( eStatusFon )
                         else
                         begin
                              ZetaDialogo.ZInformation( K_ENCABEZADO, K_MENSAJE + ObtieneElemento(lfStatusLiqImss, Ord( slCalculadaTotal) ), 0 );
                         end;
                    end;
                    eBajar:
                    begin
                         if( eStatusFon = slAfectadaTotal  ) then
                             Dec( eStatusFon )
                         else
                         begin
                              ZetaDialogo.ZInformation( K_ENCABEZADO, K_MENSAJE + ObtieneElemento(lfStatusLiqImss, Ord(slAfectadaTotal) ), 0 );
                         end;
                    end;
              end;

              Edit;
              FieldByName('FT_STATUS').AsInteger := Integer( eStatusFon );
              Post;
              Enviar;
         end;
     end;

end;

procedure TdmNomina.cdsFonTotAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsFonTot do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerNomina.GrabaTotalesFonacot( dmCliente.Empresa, Delta, ErrorCount ) ) then
                  TressShell.SetDataChange( [enFonEmp] );
          end;
     end;
end;


procedure TdmNomina.cdsFonTotAlBorrar(Sender: TObject);
begin
     if ( GetStatusFonacot( dmCliente.ImssMes, dmCliente.ImssYear ) = slAfectadaTotal ) then
     begin
          ZetaDialogo.zError( '� Error !','El c�lculo de Fonacot se encuentra cerrado', 0 );
     end
     else
     begin
          with cdsFonTot do
          begin
               if ( RecordCount > 0 ) then       // Tiene Registro de c�lculo de fonacot
               begin
                    if FieldByName( 'FT_MONTH' ).AsInteger = 0 then
                       zInformation( 'Totales de Fonacot', 'No hay datos para borrar', 0 )
                    else
                    begin
                         if ZConfirm( '� Atenci�n !',
                                      '�Desea borrar completamente el C�lculo Fonacot de ' +
                                      MesConLetraMes( FieldByName( 'FT_MONTH' ).AsInteger ) + ' de ' +
                                      IntToStr (FieldByName( 'FT_YEAR' ).AsInteger) +
                                      '?', H_BORRAR_PAGO_FONACOT, mbNo ) then
                         begin
                              Delete;
                              Enviar;
                              TressShell.SetDataChange( [enFonTot] );
                         end;
                    end;
               end
               else
                   zInformation( '� Error !', 'No hay datos para borrar', 0 );
          end;
     end;
end;

procedure TdmNomina.cdsFonacotAdquirirDatos(Sender: TObject);
begin
     with dmCliente, TZetaClientDataSet( Sender ) do
     begin
          Data:= ServerNomina.GetDatosFonacot( Empresa, ImssYear, ImssMes, Empleado, Tag );
     end;
end;

procedure TdmNomina.cdsFonEmpAlCrearCampos(Sender: TObject);
begin
     with cdsFonEmp do
     begin
          MaskFecha('FE_FECHA1');
          MaskFecha('FE_FECHA2');
          FieldByName('FE_INCIDE').OnGetText:= FE_INCIDEGetText;
     end;
end;

procedure TdmNomina.FE_INCIDEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   sIncidencia: String[ 1 ];
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     sIncidencia:= AnsiString( Sender.AsString );
{$ELSE}
     sIncidencia:= Sender.AsString;
{$ENDIF}
     case  sIncidencia[1] of
           '0': Text:= 'Sin Incidencia';
           'B': Text:= 'Baja';
           'I': Text:= 'Incapacidad';
     end;
end;


procedure TdmNomina.cdsFoncreAlCrearCampos(Sender: TObject);
begin
     with cdsFonCre do
     begin
          MaskPesos('FC_NOMINA');
          MaskPesos('FC_RETENC');
          MaskPesos('FC_AJUSTE');
          MaskPesos('FC_MONTO');
          CreateCalculated( 'FC_MONTO', ftFloat, 0 );
          MaskPesos('FC_MONTO');
     end;
end;

procedure TdmNomina.cdsFoncreCalcFields(DataSet: TDataSet);
begin
          with Dataset do
     begin
          FieldByName( 'FC_MONTO' ).AsFloat := FieldByName('FC_AJUSTE' ).AsFloat +
                                                FieldByName( 'FC_NOMINA' ).AsFloat;
     end;
end;

function TdmNomina.GetStatusAnterior: eStatusSimFiniquitos;
begin
     if Global.GetGlobalBooleano(K_GLOBAL_SIM_FINIQ_APROBACION )then
     begin
          Result := ssfSinAprobar;
     end
     else
         Result := ssfAprobada;
end;

function TdmNomina.RequiereAprobacionRH: Boolean;
begin
     Result := Global.GetGlobalBooleano(K_GLOBAL_SIM_FINIQ_APROBACION );
end;

function TdmNomina.StatusSimulacionActual :eStatusSimFiniquitos ;
begin
     Result := eStatusSimFiniquitos(cdsSimulaTotales.FieldByName('NO_APROBA').AsInteger );
end;

procedure TdmNomina.ProcesaAprobacion;
var
   oCursor: TCursor;
begin
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with cdsSimulaTotales do
             begin
                  Edit;
                  FieldByName('NO_APROBA').AsInteger := Ord(ssfAprobada );
                  Post;
                  Enviar;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TdmNomina.ProcesaDesAprobacion;
var
   oCursor: TCursor;
begin
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with cdsSimulaTotales do
             begin
                  Edit;
                  FieldByName('NO_APROBA').AsInteger := Ord(ssfSinAprobar);
                  Post;
                  Enviar;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TdmNomina.cdsSimulaTotalesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsSimulaTotales do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerNomina.GrabaNomina( dmCliente.Empresa,Delta,ErrorCount ))then
                  TressShell.SetDataChange( [enNomina] );
          end;
     end;

end;



procedure TdmNomina.BorrarFiniquito;
begin
     with cdsSimulaTotales do
     begin
          if RecordCount > 0 then
          begin
               Delete;
               Enviar;
          end;
     end;
end;

procedure TdmNomina.cdsSimulaGlobalesAlAdquirirDatos(Sender: TObject);
var
   Parametros: TZetaParams;
//   lSoloPorAplicar: Boolean;
begin
     Parametros := TZetaParams.Create;
     with dmCliente do
     begin
          with Parametros do
          begin
               AddInteger('Year',YearDefault );
               //AddInteger('Tipo',Ord(PeriodoTipo));
               AddInteger('Numero',Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
          end;
          cdsSimulaGlobales.Data := ServerNomina.GetSimulacionesGlobales(dmCliente.Empresa,Parametros.VarValues);
     end;
end;

procedure TdmNomina.CB_FEC_ANTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText  then
            Text :=  Tiempo( Sender.AsDateTime, Date , etMeses );
end;

procedure TdmNomina.cdsSimulaGlobalesAlCrearCampos(Sender: TObject);
begin
     with cdsSimulaGlobales do
     begin
          ///ListaFija('NO_STATUS',lfStatusPeriodo);
          FieldByName('NO_STATUS').OnGetText := NO_STATUSGetText;
          ListaFija('NO_APROBA',lfStatusSimFiniquitos);
          FieldByName('CB_FEC_ANT').OnGetText := CB_FEC_ANTGetText;
          MaskPesos( 'NO_NETO' );
          MaskPesos( 'NO_DEDUCCI' );
          MaskPesos( 'NO_PERCEPC' );
          MaskPesos( 'CB_SALARIO' );
          FieldByName('CB_ACTIVO').OnGetText := ACTIVOGetText;

          MaskFecha('NO_FEC_LIQ');  //DevEx
     end;
end;

procedure TdmNomina.ACTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.Dataset.IsEmpty then
        Text := VACIO
     else
     begin
          Text := K_PROPPERCASE_NO;
          if( Sender.AsString = K_GLOBAL_SI )then
              Text := K_PROPPERCASE_SI;
     end;
end;

procedure TdmNomina.cdsSimGlobalTotalesAlAdquirirDatos(Sender: TObject);
var
   Parametros: TZetaParams;
   oMovimientos:OleVariant;
begin
     Parametros := TZetaParams.Create;
     with dmCliente do
     begin
          with Parametros do
          begin
               AddInteger('Year',YearDefault );
               AddInteger('Filtro',FFiltroSimulacion);
               AddInteger('Numero',Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
          end;
          cdsSimGlobalTotales.Data := ServerNomina.GetTotalesConceptos(dmCliente.Empresa,Parametros.VarValues,oMovimientos);
          cdsSimGlobalesMovimien.Data := oMovimientos;
     end;
end;

procedure TdmNomina.cdsSimGlobalTotalesAlCrearCampos(Sender: TObject);
begin
     with cdsSimGlobalTotales do
     begin
          MaskPesos('PERCEPCIONES');
          MaskPesos('DEDUCCIONES');
          MaskPesos('NETO');
          MaskPesos('ISPT_SUM');
          MaskPesos('MENS_SUM');
          MaskPesos('CAL_SUM');
          MaskPesos('IMP_CAL_SUM');
          MaskPesos('PER_MEN_SUM');
          MaskPesos('PER_CAL_SUM');
          MaskPesos('TOT_PRE_SUM');
          MaskPesos('PER_ISN_SUM');
     end;
end;

procedure TdmNomina.cdsSimGlobalesMovimienAlCrearCampos(Sender: TObject);
begin
     with cdsSimGlobalesMovimien do
     begin
          MaskPesos('PERCEPCIONES');
          MaskPesos('DEDUCCIONES');
          MaskPesos('IMPUESTOS_CAL');
          MaskPesos('IMPUESTOS_ISPT');
          ListaFija('CO_TIPO',lfTipoConcepto);
     end;
end;

procedure TdmNomina.cdsGridAprobadosAlCrearCampos(Sender: TObject);
begin

      with cdsGridAprobados do
      begin
           FieldByName('NO_APROBA').OnGetText := NO_APROBA_GridGetText;
      end;
end;


procedure TdmNomina.NO_APROBA_GridGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := VACIO;
end;


procedure TdmNomina.cdsGridAprobadosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsGridAprobados do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerNomina.GrabaNomina( dmCliente.Empresa,Delta,ErrorCount ))then
                  TressShell.SetDataChange( [enNomina] );
          end;
     end;
end;

procedure TdmNomina.cdsGridAprobadosAlAdquirirDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     with dmCliente do
     begin
          with Parametros do
          begin
               AddInteger('Year',YearDefault );
               AddInteger('Numero',Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) );
          end;
          cdsGridAprobados.Data := ServerNomina.GetSimulacionesAprobar(dmCliente.Empresa,Parametros.VarValues);
     end;

end;

procedure TdmNomina.BorrarFiniquitosGlobales;
var
   oCursor:TCursor;
   sFiltro: string;
begin
     sFiltro := Format (' NO_GLOBAL = ''%s'' ',[K_GLOBAL_SI ]);
     with cdsSimulaGlobales do
     begin
          First;
          DisableControls;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             while not eof do
             begin
                  with dmCliente do
                  begin
                       ServerNomina.BorraNomina(Empresa,Usuario,FechaDefault,FieldByName('PE_YEAR').AsInteger,FieldByName('PE_TIPO').AsInteger,FieldByName('PE_NUMERO').AsInteger,True,sFiltro);
                  end;
                  Next
             end;
          finally
                 EnableControls;
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TdmNomina.BorrarFiniquitoGlobal;
var
   sFiltro:string;
begin
     with cdsSimulaGlobales do
     begin
          sFiltro := Format (' NO_GLOBAL = ''%s'' and CB_CODIGO = %d ',[K_GLOBAL_SI,FieldByName('CB_CODIGO').AsInteger]);
          with dmCliente do
          begin
               ServerNomina.BorraNomina(Empresa,Usuario,FechaDefault,FieldByName('PE_YEAR').AsInteger,FieldByName('PE_TIPO').AsInteger,FieldByName('PE_NUMERO').AsInteger,True,sFiltro);
          end;
          Delete;
          Refrescar;
     end;

end;


procedure TdmNomina.cdsDeclaracionesAnualesAlAdquirirDatos(
  Sender: TObject);
begin
     cdsDeclaracionesAnuales.Data := ServerNomina.GetDeclaracion( dmCliente.Empresa ); 
end;

procedure TdmNomina.cdsDetalleImportacionAlAdquirirDatos(Sender: TObject);
var sDetalle : WideString;
begin
     with dmCliente do
     begin
          cdsDetalleImportacion.Data:= ServerNomina.GetDetalleImportacion( Empresa, sDetalle ) ;//widestring( FDetalleImportacion  ));
          FDetalleImportacion := sDetalle;
     end
end;

procedure TdmNomina.AplicarLiquidacion;
begin
     cdsLiquidacion.Refrescar;
     AplicandoBaja:= True;
     try
        ZBaseDlgModal_DevEx.ShowDlgModal( EditLiquidacion_DevEx, TEditLiquidacion_DevEx );
     finally
            AplicandoBaja:= False;
     end;
end;


procedure TdmNomina.NO_STATUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := ZetaCommonTools.GetDescripcionStatusPeriodo( eStatusPeriodo( Sender.AsInteger) , eStatusTimbrado(Sender.DataSet.FieldByName('NO_TIMBRO').AsInteger  ) ) ;
end;

procedure TdmNomina.PE_STATUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := ZetaCommonTools.GetDescripcionStatusPeriodo( eStatusPeriodo( Sender.AsInteger) , eStatusTimbrado(Sender.DataSet.FieldByName('PE_TIMBRO').AsInteger  ) ) ;
end;

procedure TdmNomina.PE_STATUSTIMBRADOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := ZetaCommonLists.ObtieneElemento( lfStatusTimbrado, (Sender.DataSet.FieldByName('PE_TIMBRO').AsInteger ));
end;

function TdmNomina.FonacotTipoCedula(Anio, Mes: Integer; RazonSocial : String) : Integer;
begin
      Result := ServerNomina.FonacotTipoCedula (dmCliente.Empresa, Anio, Mes, RazonSocial);
end;

end.



