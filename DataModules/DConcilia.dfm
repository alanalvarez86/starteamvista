object dmConcilia: TdmConcilia
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 404
  Width = 610
  object cdsPrestamos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 48
    Top = 40
  end
  object cdsCedulas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 224
    Top = 40
  end
  object cdsEmpleados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 40
  end
  object cdsEmpCreditos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 48
    Top = 120
    object cdsEmpCreditosNFonacot: TStringField
      FieldName = 'NFonacot'
      Size = 30
    end
    object cdsEmpCreditosNEmpleado: TIntegerField
      FieldName = 'NEmpleado'
    end
    object cdsEmpCreditosNomCompleto: TStringField
      FieldName = 'NomCompleto'
      Size = 80
    end
    object cdsEmpCreditosRFC: TStringField
      FieldName = 'RFC'
      Size = 13
    end
    object cdsEmpCreditosCredTress: TSmallintField
      FieldName = 'CredTress'
    end
    object cdsEmpCreditosCredCedulas: TSmallintField
      FieldName = 'CredCedulas'
    end
    object cdsEmpCreditosStatus: TStringField
      FieldName = 'Status'
      Size = 15
    end
  end
  object cdsCreditosActivos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 120
    object cdsCreditosActivosNumEmpleado: TIntegerField
      FieldName = 'NEmpleado'
    end
    object cdsCreditosActivosNomCompleto: TStringField
      FieldName = 'NomCompleto'
      Size = 80
    end
    object cdsCreditosActivosRFC: TStringField
      FieldName = 'NFonacot'
      Size = 30
    end
    object cdsCreditosActivosNCredito: TStringField
      FieldName = 'NCredito'
    end
    object cdsCreditosActivosMPrestado: TFloatField
      FieldName = 'MPrestado'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosActivosRMensual: TFloatField
      FieldName = 'RMensual'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosActivosSaldo: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosActivosStatus: TStringField
      FieldName = 'StatusEmp'
      Size = 15
    end
    object cdsCreditosActivosObserva: TStringField
      FieldName = 'Observa'
      Size = 100
    end
    object cdsCreditosActivosReubicado: TStringField
      FieldName = 'Reubicado'
    end
  end
  object cdsImportPrestamos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 256
    Top = 112
  end
  object cdsImportKardex: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 256
    Top = 184
  end
  object cdsEmpleaFonacot: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 184
    object cdsEmpleaFonacotNEmpleado: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'NEmpleado'
      OnChange = cdsEmpleaFonacotNEmpleadoChange
      OnValidate = cdsEmpleaFonacotNEmpleadoValidate
    end
    object cdsEmpleaFonacotNomCompleto: TStringField
      FieldName = 'NomCompleto'
      Size = 60
    end
    object cdsEmpleaFonacotNomTress: TStringField
      FieldName = 'NomTress'
      Size = 60
    end
    object cdsEmpleaFonacotRFC: TStringField
      FieldName = 'RFC'
      Size = 13
    end
    object cdsEmpleaFonacotNFonacot: TStringField
      Alignment = taRightJustify
      FieldName = 'NFonacot'
      Size = 30
    end
    object cdsEmpleaFonacotPosicion: TIntegerField
      FieldName = 'Posicion'
    end
    object cdsEmpleaFonacotStatus: TStringField
      FieldName = 'Status'
      Size = 30
    end
    object cdsEmpleaFonacotFechaBaja: TDateField
      FieldName = 'FechaBaja'
    end
    object cdsEmpleaFonacotReubicado: TStringField
      FieldName = 'Reubicado'
      Size = 1
    end
  end
  object cdsCreditosNuevos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 184
    object cdsCreditosNuevosNFonacot: TStringField
      FieldName = 'NFonacot'
      Size = 30
    end
    object cdsCreditosNuevosNEmpleado: TIntegerField
      FieldName = 'NEmpleado'
    end
    object cdsCreditosNuevosNomCompleto: TStringField
      FieldName = 'NomCompleto'
      Size = 60
    end
    object cdsCreditosNuevosRFC: TStringField
      FieldName = 'RFC'
      Size = 13
    end
    object cdsCreditosNuevosNCredito: TStringField
      FieldName = 'NCredito'
    end
    object cdsCreditosNuevosMMensual: TFloatField
      FieldName = 'MMensual'
      LookupResultField = '`'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosNuevosPagos: TFloatField
      FieldName = 'Pagos'
    end
    object cdsCreditosNuevosMeses: TIntegerField
      FieldName = 'Meses'
    end
    object cdsCreditosNuevosMontoTotal: TFloatField
      FieldName = 'MontoTotal'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosNuevosPosicion: TIntegerField
      FieldName = 'Posicion'
    end
    object cdsCreditosNuevosStatusEmp: TStringField
      FieldName = 'StatusEmp'
      Size = 15
    end
    object cdsCreditosNuevosReubicado: TStringField
      FieldName = 'Reubicado'
      OnGetText = cdsReubicadoGetText
      Size = 2
    end
  end
  object cdsAscii: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 240
    object cdsAsciiRenglon: TStringField
      FieldName = 'Renglon'
      Size = 2048
    end
    object cdsAsciiDiferencia: TCurrencyField
      FieldName = 'DIFERENCIA'
    end
  end
  object cdsCreditosNoActivos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 240
    object StringField3: TStringField
      FieldName = 'NCredito'
    end
    object cdsCreditosNoActivosFechaInicial: TDateField
      FieldName = 'FechaInicial'
    end
    object cdsCreditosNoActivosStatus: TStringField
      FieldName = 'Status'
      Size = 30
    end
    object FloatField1: TFloatField
      FieldName = 'MPrestado'
      DisplayFormat = '#,0.00'
    end
    object FloatField3: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '#,0.00'
    end
    object FloatField2: TFloatField
      FieldName = 'RMensual'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosNoActivosTotalPrestamo: TFloatField
      FieldName = 'TotalPrestamo'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosNoActivosPagos: TIntegerField
      FieldName = 'Pagos'
    end
    object cdsCreditosNoActivosPosicion: TIntegerField
      FieldName = 'Posicion'
    end
    object cdsCreditosNoActivosNEmpleado: TIntegerField
      FieldName = 'NEmpleado'
    end
    object cdsCreditosNoActivosNomCompleto: TStringField
      FieldName = 'NomCompleto'
      Size = 80
    end
    object cdsCreditosNoActivosNFonacot: TStringField
      FieldName = 'NFonacot'
      Size = 30
    end
    object cdsCreditosNoActivosStatusEmp: TStringField
      FieldName = 'StatusEmp'
      Size = 15
    end
    object cdsCreditosNoActivosSaldoAjuste: TCurrencyField
      FieldName = 'SaldoAjuste'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosNoActivosObserva: TStringField
      FieldName = 'Observa'
      Size = 100
    end
    object cdsCreditosNoActivosReubicado: TStringField
      FieldName = 'Reubicado'
    end
    object cdsCreditosNoActivosTressPlazo: TIntegerField
      FieldName = 'TressPlazo'
    end
    object cdsCreditosNoActivosTressCedula: TIntegerField
      FieldName = 'TressCedula'
    end
  end
  object cdsActualizaPrestamos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 256
    Top = 240
  end
  object cdsCreditosDiferencias: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 296
    object cdsCreditosDiferenciasPosicion: TIntegerField
      FieldName = 'Posicion'
    end
    object cdsCreditosDiferenciasNFonacot: TStringField
      FieldName = 'NFonacot'
      Size = 30
    end
    object cdsCreditosDiferenciasNEmpleado: TIntegerField
      FieldName = 'NEmpleado'
    end
    object cdsCreditosDiferenciasNomCompleto: TStringField
      FieldName = 'NomCompleto'
      Size = 60
    end
    object cdsCreditosDiferenciasStatusEmp: TStringField
      FieldName = 'StatusEmp'
      Size = 15
    end
    object cdsCreditosDiferenciasNCredito: TStringField
      FieldName = 'NCredito'
    end
    object cdsCreditosDiferenciasCRetencion: TFloatField
      FieldName = 'CRetencion'
      LookupResultField = '`'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasCPlazo: TIntegerField
      FieldName = 'CPlazo'
    end
    object cdsCreditosDiferenciasCMeses: TIntegerField
      FieldName = 'CMeses'
    end
    object cdsCreditosDiferenciasCMontoPres: TFloatField
      FieldName = 'CMontoPres'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasMTress: TFloatField
      FieldName = 'TressRetencion'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasTressPlazo: TIntegerField
      FieldName = 'TressPlazo'
    end
    object cdsCreditosDiferenciasTressMeses: TIntegerField
      FieldName = 'TressMeses'
    end
    object cdsCreditosDiferenciasTresMontoP: TFloatField
      FieldName = 'TressMontoP'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasPR_SALDO_I: TFloatField
      FieldName = 'PR_SALDO_I'
    end
    object cdsCreditosDiferenciasPR_MONTO: TFloatField
      FieldName = 'PR_MONTO'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasPR_FECHA: TDateField
      FieldName = 'PR_FECHA'
    end
    object cdsCreditosDiferenciasPR_FORMULA: TStringField
      FieldName = 'PR_FORMULA'
      Size = 255
    end
    object cdsCreditosDiferenciasPR_SUB_CTA: TStringField
      FieldName = 'PR_SUB_CTA'
      Size = 75
    end
    object cdsCreditosDiferenciasPR_MONTO_S: TFloatField
      FieldName = 'PR_MONTO_S'
    end
    object cdsCreditosDiferenciasPR_TASA: TFloatField
      FieldName = 'PR_TASA'
    end
    object cdsCreditosDiferenciasPR_INTERES: TFloatField
      FieldName = 'PR_INTERES'
    end
    object cdsCreditosDiferenciasActualizaMontoP: TBooleanField
      FieldName = 'ActualizaMontoP'
    end
    object cdsCreditosDiferenciasPR_OBSERVA: TStringField
      FieldName = 'PR_OBSERVA'
      Size = 100
    end
    object cdsCreditosDiferenciasDIFERENCIAS: TCurrencyField
      FieldName = 'DIFERENCIA'
    end
    object cdsCreditosDiferenciasDiferenciaCredito: TFloatField
      FieldName = 'DiferenciaCredito'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasDIFERENCIAPLAZO: TIntegerField
      FieldName = 'DIFERENCIAPLAZO'
    end
    object cdsCreditosDiferenciasReubicado: TStringField
      FieldName = 'Reubicado'
    end
  end
  object cdsRetCreditos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 424
    Top = 64
  end
  object cdsTotales: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 424
    Top = 192
  end
  object cdsConfrontacion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsConfrontacionAlCrearCampos
    Left = 424
    Top = 136
    object cdsConfrontacionNFonacot: TStringField
      FieldName = 'NFonacot'
      Size = 30
    end
    object cdsConfrontacionNEmpleado: TIntegerField
      FieldName = 'NEmpleado'
    end
    object cdsConfrontacionNomCompleto: TStringField
      FieldName = 'NomCompleto'
      Size = 60
    end
    object cdsConfrontacionStatusEmp: TStringField
      FieldName = 'StatusEmp'
      Size = 15
    end
    object cdsConfrontacionRFC: TStringField
      FieldName = 'RFC'
      Size = 13
    end
    object cdsConfrontacionNCredito: TStringField
      FieldName = 'NCredito'
      Size = 50
    end
    object cdsConfrontacionRetCedula: TFloatField
      FieldName = 'RetCedula'
      DisplayFormat = '#,0.00'
    end
    object cdsConfrontacionRetTress: TFloatField
      FieldName = 'RetTress'
      DisplayFormat = '#,0.00'
    end
    object cdsConfrontacionIncidencia: TStringField
      FieldName = 'Incidencia'
      Size = 2
    end
    object cdsConfrontacionFecInicio: TDateField
      FieldName = 'FecInicio'
      OnGetText = cdsConfrontacionFecInicioGetText
    end
    object cdsConfrontacionFecFin: TDateField
      FieldName = 'FecFin'
      OnGetText = cdsConfrontacionFecInicioGetText
    end
    object cdsConfrontacionPlazo: TIntegerField
      FieldName = 'Plazo'
    end
    object cdsConfrontacionMeses: TIntegerField
      FieldName = 'Meses'
    end
    object cdsConfrontacionSaldoAjus: TFloatField
      FieldName = 'SaldoAjus'
      DisplayFormat = '#,0.00'
    end
    object cdsConfrontacionStatusPrestamo: TIntegerField
      FieldName = 'StatusPrestamo'
    end
    object cdsConfrontacionObserva: TStringField
      FieldName = 'Observa'
      Size = 25
    end
    object cdsConfrontacionReubicado: TStringField
      DisplayWidth = 2
      FieldName = 'Reubicado'
      Size = 2
    end
    object cdsConfrontacionDIFERENCIA: TFloatField
      FieldName = 'DIFERENCIA'
    end
    object cdsConfrontacionDiferenciaCredito: TFloatField
      FieldName = 'DiferenciaCredito'
      DisplayFormat = '#,0.00'
    end
  end
  object cdsAuxiliar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 424
    Top = 248
  end
  object cdsEmpleadosFonacot: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 424
    Top = 296
  end
  object cdsPrestamosAltas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 528
    Top = 232
  end
  object cdsProcesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsProcesosAfterOpen
    OnCalcFields = cdsProcesosCalcFields
    AlCrearCampos = cdsProcesosAlCrearCampos
    AlModificar = cdsProcesosAlModificar
    Left = 304
    Top = 40
  end
  object cdsLogDetail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsLogDetailAfterOpen
    AlModificar = cdsLogDetailAlModificar
    Left = 384
    Top = 40
  end
  object cdsActualizaPCarAbo: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 264
    Top = 296
  end
  object cdsPrestaAjus: TZetaClientDataSet
    Tag = 5
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 296
  end
  object cdsPCarAbo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 512
    Top = 152
  end
  object cdsCreditosDiferenciasTemp: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 344
    object cdsCreditosDiferenciasTempPosicion: TIntegerField
      FieldName = 'Posicion'
    end
    object cdsCreditosDiferenciasTempNFonacot: TStringField
      FieldName = 'NFonacot'
      Size = 30
    end
    object cdsCreditosDiferenciasTempNEmpleado: TIntegerField
      FieldName = 'NEmpleado'
    end
    object cdsCreditosDiferenciasTempNomCompleto: TStringField
      FieldName = 'NomCompleto'
      Size = 60
    end
    object cdsCreditosDiferenciasTempNomStatusEmp: TStringField
      FieldName = 'StatusEmp'
      Size = 15
    end
    object cdsCreditosDiferenciasTempNCredito: TStringField
      FieldName = 'NCredito'
    end
    object cdsCreditosDiferenciasTempCRetencion: TFloatField
      FieldName = 'CRetencion'
      LookupResultField = '`'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasTempCPlazo: TIntegerField
      FieldName = 'CPlazo'
    end
    object cdsCreditosDiferenciasTempCMeses: TIntegerField
      FieldName = 'CMeses'
    end
    object cdsCreditosDiferenciasTempCMontoPres: TFloatField
      FieldName = 'CMontoPres'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasTempTressRetencion: TFloatField
      FieldName = 'TressRetencion'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasTempTressPlazo: TIntegerField
      FieldName = 'TressPlazo'
    end
    object cdsCreditosDiferenciasTempTressMeses: TIntegerField
      FieldName = 'TressMeses'
    end
    object cdsCreditosDiferenciasTempTressMontoP: TFloatField
      FieldName = 'TressMontoP'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasTempPR_SALDO_I: TFloatField
      FieldName = 'PR_SALDO_I'
    end
    object cdsCreditosDiferenciasTempPR_MONTO: TFloatField
      FieldName = 'PR_MONTO'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasTempPR_FECHA: TDateField
      FieldName = 'PR_FECHA'
    end
    object cdsCreditosDiferenciasTempPR_FORMULA: TStringField
      FieldName = 'PR_FORMULA'
      Size = 255
    end
    object cdsCreditosDiferenciasTempPR_SUB_CTA: TStringField
      FieldName = 'PR_SUB_CTA'
      Size = 75
    end
    object cdsCreditosDiferenciasTempPR_MONTO_S: TFloatField
      FieldName = 'PR_MONTO_S'
    end
    object cdsCreditosDiferenciasTempPR_TASA: TFloatField
      FieldName = 'PR_TASA'
    end
    object cdsCreditosDiferenciasTempPR_INTERES: TFloatField
      FieldName = 'PR_INTERES'
    end
    object cdsCreditosDiferenciasTempActualizaMontoP: TBooleanField
      FieldName = 'ActualizaMontoP'
    end
    object cdsCreditosDiferenciasTempPR_OBSERVA: TStringField
      FieldName = 'PR_OBSERVA'
      Size = 100
    end
    object cdsCreditosDiferenciasTempDIFERENCIA: TCurrencyField
      FieldName = 'DIFERENCIA'
    end
    object cdsCreditosDiferenciasTempDiferenciaCredito: TFloatField
      FieldName = 'DiferenciaCredito'
      DisplayFormat = '#,0.00'
    end
    object cdsCreditosDiferenciasTempDIFERENCIAPLAZO: TIntegerField
      FieldName = 'DIFERENCIAPLAZO'
    end
    object cdsCreditosDiferenciasTempReubicado: TStringField
      FieldName = 'Reubicado'
    end
  end
  object cdsDataLista: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 352
    Top = 272
  end
end
