object dmPresupuestos: TdmPresupuestos
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 442
  Top = 251
  Height = 479
  Width = 741
  object cdsSupuestosRH: TZetaClientDataSet
    Aggregates = <>
    Filtered = True
    IndexFieldNames = 'ES_CODIGO;SR_FECHA;SR_TIPO'
    Params = <>
    BeforePost = cdsSupuestosRHBeforePost
    OnCalcFields = cdsSupuestosRHCalcFields
    OnNewRecord = cdsSupuestosRHNewRecord
    OnReconcileError = cdsSupuestosRHReconcileError
    AlAdquirirDatos = cdsSupuestosRHAlAdquirirDatos
    AlEnviarDatos = cdsSupuestosRHAlEnviarDatos
    AlCrearCampos = cdsSupuestosRHAlCrearCampos
    AlBorrar = cdsSupuestosRHAlBorrar
    AlModificar = cdsSupuestosRHAlModificar
    Left = 40
    Top = 24
  end
  object cdsEventosAltas: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'EV_CODIGO'
    Params = <>
    BeforePost = cdsEventosAltasBeforePost
    AfterDelete = cdsEventosAltasAfterDelete
    OnNewRecord = cdsEventosAltasNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEventosAltasAlAdquirirDatos
    AlEnviarDatos = cdsEventosAltasAlEnviarDatos
    AlCrearCampos = cdsEventosAltasAlCrearCampos
    AlModificar = cdsEventosAltasAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Contrataciones'
    LookupDescriptionField = 'EV_DESCRIP'
    LookupKeyField = 'EV_CODIGO'
    OnGetRights = cdsEventosAltasGetRights
    Left = 128
    Top = 24
  end
  object cdsPeriodos: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'PE_MES;PE_TIPO;PE_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsPeriodosAlAdquirirDatos
    Left = 216
    Top = 24
  end
  object cdsContrataciones: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 88
  end
  object cdsImportarKardex: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 152
    Top = 88
  end
  object cdsLista: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 248
    Top = 88
  end
  object cdsEscenarios: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'ES_CODIGO'
    Params = <>
    OnReconcileError = cdsEscenariosReconcileError
    AlAdquirirDatos = cdsEscenariosAlAdquirirDatos
    AlEnviarDatos = cdsEscenariosAlEnviarDatos
    AlBorrar = cdsEscenariosAlBorrar
    AlModificar = cdsEscenariosAlModificar
    LookupName = 'Escenarios de Presupuesto'
    LookupDescriptionField = 'ES_ELEMENT'
    LookupKeyField = 'ES_CODIGO'
    Left = 40
    Top = 144
  end
end
