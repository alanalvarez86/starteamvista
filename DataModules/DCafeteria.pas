unit DCafeteria;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerCafeteria,
{$else}
     Cafeteria_TLB,
{$endif}
     ZetaClientDataSet,
     ZetaTipoEntidad,
     ZetaCommonLists,
     Variants;

{$define QUINCENALES}
{$define MULT_TIPO_COMIDA}  //Permitir multiples registros de cafeteria de diferente tipo en un mismo minuto
{.$undefine QUINCENALES}

type
  TdmCafeteria = class(TDataModule)
    cdsCafDiarias: TZetaClientDataSet;
    cdsCafPeriodo: TZetaClientDataSet;
    cdsCafInvita: TZetaClientDataSet;
    cdsComidas: TZetaClientDataSet;
    cdsCafPeriodoTotales: TZetaClientDataSet;
    cdsCasDiarias: TZetaClientDataSet;
    cdsCaseta: TZetaClientDataSet;
    procedure cdsCafDiariasAlAdquirirDatos(Sender: TObject);
    procedure cdsCafPeriodoAlAdquirirDatos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsComidasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsComidasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsCafInvitaAlAdquirirDatos(Sender: TObject);
    procedure cdsCafInvitaAlCrearCampos(Sender: TObject);
    procedure cdsCafInvitaAlModificar(Sender: TObject);
    procedure cdsCafDiariasAlModificar(Sender: TObject);
    procedure cdsCafInvitaAfterPost(DataSet: TDataSet);
    procedure cdsCafDiariasNewRecord(DataSet: TDataSet);
    procedure cdsCafDiariasBeforePost(DataSet: TDataSet);
    procedure cdsCafInvitaNewRecord(DataSet: TDataSet);
    procedure cdsCafInvitaBeforePost(DataSet: TDataSet);
    procedure cdsComidasAlAdquirirDatos(Sender: TObject);
    procedure cdsComidasAlCrearCampos(Sender: TObject);
    procedure cdsComidasNewRecord(DataSet: TDataSet);
    procedure cdsComidasAfterEdit(DataSet: TDataSet);
    procedure cdsComidasBeforePost(DataSet: TDataSet);
    procedure cdsComidasCB_CODIGOChange(Sender: TField);
    procedure cdsComidasCB_CODIGOValidate(Sender: TField);
    procedure cdsComidasCF_TIPOValidate(Sender: TField);
    procedure CF_HORASetText(Sender: TField; const Text: String);
    procedure CF_HORAValidate(Sender: TField);
    procedure cdsComidasAfterPost(DataSet: TDataSet);
    procedure cdsComidasBeforeOpen(DataSet: TDataSet);
    procedure cdsComidasAlEnviarDatos(Sender: TObject);
    procedure cdsCafDiariasAlCrearCampos(Sender: TObject);
    procedure cdsCafPeriodoAlCrearCampos(Sender: TObject);
    procedure cdsCasDiariasAlAdquirirDatos(Sender: TObject);
    procedure cdsCasDiariasAlCrearCampos(Sender: TObject);
    procedure cdsCasDiariasAlModificar(Sender: TObject);
    procedure cdsCasDiariasAfterPost(DataSet: TDataSet);
    procedure cdsCasDiariasBeforePost(DataSet: TDataSet);
    procedure cdsCasDiariasNewRecord(DataSet: TDataSet);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);{acl}
    procedure AL_ENTRADAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsCasetaAlAdquirirDatos(Sender: TObject);
    procedure cdsCasetaAlCrearCampos(Sender: TObject);
    procedure cdsCasetaAfterEdit(DataSet: TDataSet);
    procedure cdsCasetaAlEnviarDatos(Sender: TObject);
    procedure cdsCasetaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
              UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsCasetaBeforeOpen(DataSet: TDataSet);
    procedure cdsCasetaCB_CODIGOChange(Sender: TField);
    procedure cdsCasetaBeforePost(DataSet: TDataSet);
    procedure cdsCasetaAfterPost(DataSet: TDataSet);
    procedure cdsCasetaNewRecord(DataSet: TDataSet);
    procedure cdsCasetaCB_CODIGOValidate(Sender: TField);
    procedure cdsCafDiariasAlEnviarDatos(Sender: TObject);
    procedure cdsCafDiariasAfterDelete(DataSet: TDataSet);
    procedure cdsCafInvitaReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
  private
    { Private declarations}
    Ultimafecha: TDate;
    UltimaHora: String;
    UltimoTipo: String;
    UltimaComidas: Integer;
    FInvitador: Integer;
    FSoloAltas: Boolean;
{$ifdef DOS_CAPAS}
    function GetServerCafeteria: TdmServerCafeteria;
    property ServerCafeteria: TdmServerCafeteria read GetServerCafeteria;
{$else}
    FServidor: IdmServerCafeteriaDisp;
    function GetServerCafeteria: IdmServerCafeteriaDisp;
    property ServerCafeteria: IdmServerCafeteriaDisp read GetServerCafeteria;
{$endif}
  public
    { Public declarations }
    property Invitador: integer read FInvitador write FInvitador;
    function RegistrarCasetaPuntual( DataSet: TZetaClientDataSet; const sChecada: string ): String; {acl}
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure SetClaseComidas( const lComidas: Boolean );
    procedure EditarGridComidas( const lSoloAltas: Boolean);
    procedure EditarGridCaseta( const lSoloAltas: Boolean);//acl    
    function ObtieneHorario( const iEmpleado: Integer; const dFecha: TDate ): String;{acl}
    function GetHoraEntSalEmp( DataSet: TZetaClientDataSet; const iEmpleado: Integer; const dFecha: TDate; iEntrada: Integer ): String;{acl}
  end;

var
  dmCafeteria: TdmCafeteria;

implementation

uses DCliente,
     DCatalogos,
     DRecursos,
     ZReconcile,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZBaseEdicion_DevEx,
     ZBaseGridEdicion_DevEx,
     FEditCafDiarias_DevEx,
     FEditCafInvita_DevEx,
     FGridComidas_DevEx,
     FTressShell,
     FEditCasDiarias_DevEx,
     dSistema,
     dAsistencia,
     FGridCaseta_DevEx,
     ZetaDialogo;

{$R *.DFM}

{ TdmCafateria }

{$ifdef DOS_CAPAS}
function TdmCafeteria.GetServerCafeteria: TdmServerCafeteria;
begin
     Result := DCliente.dmCliente.ServerCafeteria;
end;
{$else}
function TdmCafeteria.GetServerCafeteria: IdmServerCafeteriaDisp;
begin
     Result := IdmServerCafeteriaDisp( dmCliente.CreaServidor(CLASS_dmServerCafeteria,FServidor ) );
end;
{$endif}

{$ifdef VER130}
procedure TdmCafeteria.cdsReconcileError( DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmCafeteria.cdsReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmCafeteria.NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmCafeteria.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     if ( Estado = stEmpleado ) or
        {$ifdef MULTIPLES_ENTIDADES}
        Dentro( enComida , Entidades )
        {$else}
        ( enComida in Entidades )
        {$endif} then
     begin
          cdsCafDiarias.SetDataChange;
          cdsCafPeriodo.SetDataChange;
     end;
     if ( Estado = stFecha ) then
     begin
          cdsCafDiarias.SetDataChange;
          cdsCafInvita.SetDataChange;
     end;
     if ( Estado = stPeriodo ) then
        cdsCafPeriodo.SetDataChange;
{acl}        
     if ( ( Estado = stEmpleado ) or ( Estado = stFecha )  ) or
        {$ifdef MULTIPLES_ENTIDADES}
        Dentro( enAccesLog , Entidades )
        {$else}
        ( enAccesLog in Entidades )
        {$endif}
      then
        cdsCasDiarias.SetDataChange;
end;

procedure TdmCafeteria.SetClaseComidas( const lComidas: Boolean );
begin
     with cdsComidas do
     begin
          try
             DisableControls;
             if State in [ dsEdit, dsInsert ] then
                Post;
             if ( RecordCount > 0 ) then
             begin
                  First;
                  while not Eof do
                  begin
                       Edit;
                       if lComidas then
                          FieldByName( 'CF_COMIDAS' ).AsInteger := FieldByName( 'COMIDAS' ).AsInteger
                       else
                           FieldByName( 'CF_EXTRAS' ).AsInteger := FieldByName( 'COMIDAS' ).AsInteger;
                       Post;
                       Next;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmCafeteria.EditarGridComidas( const lSoloAltas: Boolean );
begin
     FSoloAltas := lSoloAltas;
     try
        dmCliente.SetLookupEmpleado( eLookEmpComidas );
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridComidas_DevEx, TGridComidas_DevEx, lSoloAltas )
     finally
        dmCliente.ResetLookupEmpleado;
     end;
     FSoloAltas := False;
end;

{ ********* cdsCafDiarias ************ }

procedure TdmCafeteria.cdsCafDiariasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsCafDiarias.Data := ServerCafeteria.GetCafDiarias( dmCliente.Empresa, Empleado, FechaAsistencia );
     end;
end;

procedure TdmCafeteria.cdsCafDiariasAlCrearCampos(Sender: TObject);
begin
     with cdsCafDiarias do
     begin
          MaskTime( 'CF_HORA' );
     end;
end;

procedure TdmCafeteria.cdsCafDiariasAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCafDiarias_DevEx, TEditCafDiarias_DevEx )
end;

procedure TdmCafeteria.cdsCafDiariasNewRecord(DataSet: TDataSet);
begin
     with cdsCafDiarias do
     begin
          with dmCliente do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'CF_FECHA' ).AsDateTime := FechaAsistencia;
          end;
          FieldByName( 'CF_REG_EXT' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CF_TIPO' ).AsString := '1';
          FieldByName( 'CF_COMIDAS' ).AsInteger := 1;
          FieldByName( 'CF_HORA' ).AsString := '0000';
          FieldByName( 'CF_EXTRAS' ).AsInteger := 0;
     end;
end;

procedure TdmCafeteria.cdsCafDiariasBeforePost(DataSet: TDataSet);
begin
     with cdsCafDiarias do
     begin
          if ( FieldByName( 'CF_COMIDAS' ).AsInteger < 0 ) then
             DataBaseError( 'El N�mero de Comidas Debe Ser Mayor o Igual a 0' );
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;

          if ( ( FieldByName( 'CF_TIPO' ).AsInteger <= 0 ) or ( FieldByName( 'CF_TIPO' ).AsInteger >= 10 ) ) then
             DataBaseError( 'El Tipo de Comida debe ser un valor entre 1 a 9' );

          if ( StrVacio( FieldByName( 'CF_HORA' ).AsString )  ) then
          begin
               FieldByName( 'CF_HORA' ).FocusControl;
             	 DataBaseError( 'Hora no puede quedar vac�a' );
          end;

          if ( StrVacio( FieldByName( 'CL_CODIGO' ).AsString )  ) then
          begin
                FieldByName( 'CL_CODIGO' ).AsString := '0';
          end;
     end;
end;

procedure TdmCafeteria.cdsCafDiariasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCafDiarias do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCafeteria.GrabaCafDiarias( dmCliente.Empresa, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
               begin
                    // EZM: Como no hay valores activos de por medio y NO comparten el mismo
                    // ClientDataSet, es necesario desconectarlo para forzar a que se refresque
                    // cdsCafPeriodo.Active := False;
                    TressShell.SetDataChange( [ enComida ] );
               end;
          end;
     end;
end;

{ ********* cdsCafPeriodo ************ }

procedure TdmCafeteria.cdsCafPeriodoAlAdquirirDatos(Sender: TObject);
var
   oTotales: OleVariant;
begin
     with dmCliente do
     begin
          with GetDatosPeriodoActivo do
          begin
               {$ifdef QUINCENALES}
               cdsCafPeriodo.Data := ServerCafeteria.GetCafPeriodo( dmCliente.Empresa, Empleado, InicioAsis, FinAsis, oTotales );
               {$else}
               cdsCafPeriodo.Data := ServerCafeteria.GetCafPeriodo( dmCliente.Empresa, Empleado, Inicio, Fin, oTotales );
               {$endif}
          end;
          cdsCafPeriodoTotales.Data := oTotales;
     end;
end;

procedure TdmCafeteria.cdsCafPeriodoAlCrearCampos(Sender: TObject);
begin
     with cdsCafPeriodo do
     begin
          MaskFecha( 'CF_FECHA' );
          MaskTime( 'CF_HORA' );
     end;
end;

{ *********** cdsCafInvita ************ }

procedure TdmCafeteria.cdsCafInvitaAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsCafInvita.Data := ServerCafeteria.GetCafInvita( dmCliente.Empresa, Invitador, FechaAsistencia );
     end;
end;

procedure TdmCafeteria.cdsCafInvitaAlCrearCampos(Sender: TObject);
begin
     with cdsCafInvita do
     begin
        MaskTime( 'CF_HORA' );
        CreateSimpleLookup( dmCatalogos.cdsInvitadores, 'IV_DESCRIP', 'IV_CODIGO' );
     end;
end;

procedure TdmCafeteria.cdsCafInvitaAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCafInvita_DevEx, TEditCafInvita_DevEx )
end;

procedure TdmCafeteria.cdsCafInvitaAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
   sCodigo, sFecha, sHora, sTipo : String;
begin
     ErrorCount := 0;
     with cdsCafInvita do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile ( ServerCafeteria.GrabaCafInvita( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    with cdsCafInvita do
                    begin
                         sCodigo := cdsCafInvita.FieldByName('IV_CODIGO').AsString;
                         sFecha := cdsCafInvita.FieldByName('CF_FECHA').AsString;
                         sHora := cdsCafInvita.FieldByName('CF_HORA').AsString;
                         sTipo := cdsCafInvita.FieldByName('CF_TIPO').AsString;

                         Refrescar;
                         try
                            Locate('IV_CODIGO;CF_FECHA;CF_HORA;CF_TIPO', VarArrayOf( [ sCodigo, sFecha, sHora, sTipo ] ), []);
                         except on E: Exception do
                                Last;
                         end;

                    end;
               end;
          end;
     end;
end;

procedure TdmCafeteria.cdsCafInvitaNewRecord(DataSet: TDataSet);
begin
     with cdsCafInvita do
     begin
          FieldByName( 'IV_CODIGO' ).AsInteger := dmCatalogos.cdsInvitadores.FieldByName( 'IV_CODIGO' ).AsInteger;
          FieldByName( 'CF_FECHA' ).AsDateTime := dmCliente.FechaAsistencia;
          FieldByName( 'CF_TIPO' ).AsString := '1';
          FieldByName( 'CF_COMIDAS' ).AsInteger := 1;
          FieldByName( 'CF_REG_EXT' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCafeteria.cdsCafInvitaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError : String;
begin
     if ( FK_Violation(E) ) and ( UpdateKind = ukInsert ) then
     begin
          with DataSet do
          begin
               sError := 'No se pueden agregar Invitaciones si no hay Invitadores seleccionados.';
          end;

          ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
          case UpdateKind of
               ukInsert: Action := raCancel;
          else
              Action := raAbort;
          end;
     end
     else
         Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;

procedure TdmCafeteria.cdsCafInvitaBeforePost(DataSet: TDataSet);
begin
     with cdsCafInvita do
     begin
          if ( FieldByName( 'CF_COMIDAS' ).AsInteger < 0 ) then
             DataBaseError( 'El N�mero de Comidas Debe Ser Mayor a Igual a 0');
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

{ *************** cdsComidas ************** }

procedure TdmCafeteria.cdsComidasAlAdquirirDatos(Sender: TObject);
begin
     cdsComidas.Data := ServerCafeteria.GetComidas( dmCliente.Empresa, dmCliente.FechaAsistencia, FSoloAltas );
end;

procedure TdmCafeteria.cdsComidasAlCrearCampos(Sender: TObject);
begin
     with cdsComidas do
     begin
          MaskTime( 'CF_HORA' );
          MaskPesos( 'COMIDAS' );
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsComidasCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsComidasCB_CODIGOChange;
          FieldByName( 'CF_TIPO' ).OnValidate := cdsComidasCF_TIPOValidate;
          with FieldByName( 'CF_HORA' ) do
          begin
               OnSetText := CF_HORASetText;
               OnValidate := CF_HORAValidate;
          end;
     end;
end;

procedure TdmCafeteria.cdsComidasBeforeOpen(DataSet: TDataSet);
begin
     Ultimafecha := dmCliente.FechaDefault;
     UltimoTipo := '1';
     UltimaComidas := 1;
end;

procedure TdmCafeteria.cdsComidasNewRecord(DataSet: TDataSet);
begin
     with cdsComidas do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger:= dmCliente.Usuario;
          FieldByName( 'CF_REG_EXT' ).AsString:= K_GLOBAL_NO;
     end;
end;

procedure TdmCafeteria.cdsComidasAfterEdit(DataSet: TDataSet);
begin
     cdsComidas.FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmCafeteria.cdsComidasBeforePost(DataSet: TDataSet);
begin
     with cdsComidas do
     begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger = 0 ) then
          begin
               FieldByName( 'CB_CODIGO' ).FocusControl;
               DataBaseError( 'N�mero de Empleado No Puede Quedar Vac�o' );
          end;
          if ( FieldByName( 'CF_FECHA' ).AsDateTime = NullDateTime ) then
          begin
               FieldByName( 'CF_FECHA' ).FocusControl;
               DataBaseError( 'Fecha de Registro No Puede Quedar Vacia' );
          end;
          if ( FieldByName( 'COMIDAS' ).AsInteger <= 0 ) then
          begin
               FieldByName( 'COMIDAS' ).FocusControl;
               DataBaseError( 'Falta La Cantidad De Comidas' );
          end;
          if ( FieldByName( 'CF_TIPO' ).AsInteger = 0 ) then
          begin
               FieldByName( 'CF_TIPO' ).FocusControl;
               DataBaseError( 'Falta Tipo de Comida' );
          end;
     end;
end;

procedure TdmCafeteria.cdsComidasAfterPost(DataSet: TDataSet);
begin
     with cdsComidas do
     begin
          Ultimafecha := FieldByName( 'CF_FECHA' ).AsDateTime;
          UltimaHora := FieldByName( 'CF_HORA' ).AsString;
          UltimoTipo := FieldByName( 'CF_TIPO' ).AsString;
          UltimaComidas := FieldByName( 'COMIDAS' ).AsInteger;
     end;
end;

procedure TdmCafeteria.cdsComidasCB_CODIGOValidate(Sender: TField);
var
   iEmpleado : Integer;
   sNombre : String;
begin
     iEmpleado := Sender.AsInteger;
     with dmCliente.cdsEmpleadoLookup do
     begin
          if ( iEmpleado > 0 ) then
          begin
               sNombre := GetDescripcion( IntToStr( iEmpleado ) );
               if StrVacio( sNombre ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No Existe el Empleado #' + IntToStr( iEmpleado ) )
               else
               begin
                    if ( not zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) ) and
                       ( not dmRecursos.ShowAdvertenciaBaja( iEmpleado, sNombre, FieldByName( 'CB_FEC_BAJ' ).AsDateTime ) ) then
                       Abort;    // Raise silencioso por que ya se mostr� el dialogo de Advertencia
               end;
{
               else if ( not zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) ) then
                  DataBaseError( 'El Empleado ' + IntToStr( AsInteger ) + ': '+
                                 sNombre + CR_LF + ' ya fu� dado de Baja el '+
                                 FechaCorta( FieldByName( 'CB_FEC_BAJ' ).AsDateTime ) +
                                 CR_LF + 'No se Pueden Registrar Comidas' );
}
          end
          else
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
     end;
end;

procedure TdmCafeteria.cdsComidasCB_CODIGOChange(Sender: TField);
begin
     with cdsComidas do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          // Asigna Valores Default
          if ( FieldByName( 'CF_FECHA' ).AsDateTime = NullDateTime ) then
          begin
               FieldByName( 'CF_FECHA' ).AsDateTime := UltimaFecha;
               FieldByName( 'CF_HORA' ).AsString := UltimaHora;
               FieldByName( 'CF_TIPO' ).AsString := UltimoTipo;
               FieldByName( 'COMIDAS' ).AsInteger := UltimaComidas;
          end;
     end;
end;

procedure TdmCafeteria.cdsComidasCF_TIPOValidate(Sender: TField);
begin
     if ( Sender.AsInteger < 1 ) or ( Sender.AsInteger > 9 ) then
        DataBaseError( 'Tipo de Comidas Debe Ser Un Valor De 1 a 9' );
end;

procedure TdmCafeteria.CF_HORASetText(Sender: TField; const Text: String);
begin
     Sender.AsString := ConvierteHora( Text );
end;

procedure TdmCafeteria.CF_HORAValidate(Sender: TField);
var
   sHora: String;
begin
     sHora := Sender.AsString;
     if not ValidaHora( sHora, '48' ) then
        raise ERangeError.Create( 'Hora "' + Copy( sHora, 1, 2 ) + ':' + Copy( sHora, 3, 2 ) + '" Inv�lida' );
end;

procedure TdmCafeteria.cdsComidasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsComidas as TZetaClientDataSet do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerCafeteria.GrabaComidas( dmCliente.Empresa, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
                  TressShell.SetDataChange( [ enComida ] );
          end;
     end;
end;

{$ifdef VER130}
procedure TdmCafeteria.cdsComidasReconcileError(DataSet: TClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
    sError : String;
begin
    if PK_Violation( E ) then
      with DataSet do
        sError := Format( 'La comida ya fu� capturada.' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s' {$ifdef MULT_TIPO_COMIDA} + CR_LF + 'Tipo: %d' {$endif},
                    [ FieldByName( 'CB_CODIGO' ).AsInteger, FechaCorta( FieldByName( 'CF_FECHA' ).AsDateTime ) {$ifdef MULT_TIPO_COMIDA},FieldByName('CF_TIPO').AsString{$endif} ] )
    else
        sError := GetErrorDescription( E );
    //Action := cdsComidas.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;CF_FECHA;CF_HORA', sError );
    cdsComidas.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;CF_FECHA;CF_HORA', sError );
    Action := raCancel;   // Est� en el grid, para que marque todos los errores y al final cierre la forma
end;
{$else}
procedure TdmCafeteria.cdsComidasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
    sError : String;
begin
    if PK_Violation( E ) then
      with DataSet do
        sError := Format( 'La comida ya fu� capturada.' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s'{$ifdef MULT_TIPO_COMIDA} + CR_LF + 'Tipo: %d' {$endif},
                    [ FieldByName( 'CB_CODIGO' ).AsInteger, FechaCorta( FieldByName( 'CF_FECHA' ).AsDateTime ){$ifdef MULT_TIPO_COMIDA},FieldByName('CF_TIPO').AsInteger{$endif} ] )
    else
        sError := GetErrorDescription( E );
    //Action := cdsComidas.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;CF_FECHA;CF_HORA', sError );
    cdsComidas.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;CF_FECHA;CF_HORA', sError );
    Action := raCancel;   // Est� en el grid, para que marque todos los errores y al final cierre la forma
end;
{$endif}

              { *********DCafeteria manejara la logica de CASETA ************ }
{ *********cdsCasDiarias ************ acl}
procedure TdmCafeteria.cdsCasDiariasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsCasDiarias.Data := ServerCafeteria.GetCasDiarias( dmCliente.Empresa, Empleado, FechaAsistencia );
     end;
end;

procedure TdmCafeteria.cdsCasDiariasAlCrearCampos(Sender: TObject);
begin
     with cdsCasDiarias do
     begin
          MaskFecha( 'AL_FECHA' );
          MaskTime( 'AL_HORA' );
          MaskBool( 'AL_ENTRADA' );
          FieldByName( 'AL_ENTRADA' ).OnGetText := AL_ENTRADAGetText;
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmCafeteria.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := VACIO
     else
         Text := dmSistema.cdsUsuariosLookup.GetDescripcion( Sender.AsString );
end;

procedure TdmCafeteria.AL_ENTRADAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
const
     aTipoEntrada: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Salida', 'Entrada' );
begin
     if DisplayText and StrLleno( Sender.AsString ) then
     begin
          Text := aTipoEntrada[ zStrToBool ( Sender.AsString ) ];
     end
     else
         Text := Sender.AsString;
end;

procedure TdmCafeteria.cdsCasDiariasAlModificar(Sender: TObject);
begin
      ZBaseEdicion_DevEx.ShowFormaEdicion( EditCasDiarias_DevEx, TEditCasDiarias_DevEx )
end;

procedure TdmCafeteria.cdsCasDiariasAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCasDiarias do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCafeteria.GrabaCasDiarias( dmCliente.Empresa, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
               begin
                    TressShell.SetDataChange( [ enAccesLog ] );
               end; 
          end;
     end;
end;

procedure TdmCafeteria.cdsCasDiariasBeforePost(DataSet: TDataSet);
begin
     with cdsCasDiarias do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'AL_OK_MAN' ).AsString := K_GLOBAL_SI;
          if ( not StrLleno( FieldByName( 'AL_HORA' ).AsString ) ) then
          begin
               FieldByName( 'AL_HORA' ).FocusControl;
               DataBaseError( 'Hora de Registro No Puede Quedar Vacia' );
          end;
     end;
end;

procedure TdmCafeteria.cdsCasDiariasNewRecord(DataSet: TDataSet);
begin
     with cdsCasDiarias do
     begin
          with dmCliente do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'AL_FECHA' ).AsDateTime := FechaAsistencia;
          end;
          FieldByName( 'AL_ENTRADA' ).AsString := K_GLOBAL_SI;
     end;
end;


function TdmCafeteria.RegistrarCasetaPuntual( DataSet: TZetaClientDataSet; const sChecada: string ): String;
begin
     with DataSet do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName('AL_HORA').AsString := ConvierteHora(sChecada);
          Result := FieldByName('AL_HORA').AsString;          
     end;
end;

function TdmCafeteria.ObtieneHorario( const iEmpleado: Integer; const dFecha: TDate ): String;
var
   sturno : String;
begin
     sTurno:= dmRecursos.GetDatosClasificacion( iEmpleado, dFecha).Turno;
     Result:= dmAsistencia.GetStatusHorario( sTurno, dFecha ).Horario;
end;

function TdmCafeteria.GetHoraEntSalEmp( DataSet: TZetaClientDataSet;  const iEmpleado: Integer; const dFecha: TDate; iEntrada: Integer ): String;
var
   sChecada,sHorario: string;
begin
     sHorario:= ObtieneHorario( iEmpleado, dFecha );
     if ( dmAsistencia.GetChecadaPuntual(sHorario , iEntrada, sChecada )) then
         Result :=  RegistrarCasetaPuntual( DataSet, sChecada )
     else
         Zerror('Error en Turno: ', sChecada, 0);
end;

{ *********cdsCaseta ************ acl}
procedure TdmCafeteria.EditarGridCaseta( const lSoloAltas: Boolean );
begin
     FSoloAltas := lSoloAltas;
     try
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridCaseta_DevEx, TGridCaseta_DevEx, lSoloAltas );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
     FSoloAltas := False;
end;

procedure TdmCafeteria.cdsCasetaAlAdquirirDatos(Sender: TObject);
begin
    cdsCaseta.Data := ServerCafeteria.GetCaseta( dmCliente.Empresa, dmCliente.FechaAsistencia, FSoloAltas );
end;

procedure TdmCafeteria.cdsCasetaAlCrearCampos(Sender: TObject);
begin
     with cdsCaseta do
     begin
          MaskFecha( 'AL_FECHA' );
          MaskTime( 'AL_HORA' );
          MaskBool( 'AL_ENTRADA' );
          FieldByName( 'AL_ENTRADA' ).OnGetText := AL_ENTRADAGetText;
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsCasetaCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsCasetaCB_CODIGOChange;
          with FieldByName( 'AL_HORA' ) do
          begin
               OnSetText := CF_HORASetText;
               OnValidate := CF_HORAValidate;
          end;
     end;
end;

procedure TdmCafeteria.cdsCasetaCB_CODIGOValidate(Sender: TField);
var
   iEmpleado : Integer;
   sNombre : String;
begin
     iEmpleado := Sender.AsInteger;
     with dmCliente.cdsEmpleadoLookup do
     begin
          if ( iEmpleado > 0 ) then
          begin
               sNombre := GetDescripcion( IntToStr( iEmpleado ) );
               if StrVacio( sNombre ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No Existe el Empleado #' + IntToStr( iEmpleado ) )
          end
          else
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
     end;
end;

procedure TdmCafeteria.cdsCasetaAfterEdit(DataSet: TDataSet);
begin
     cdsCaseta.FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmCafeteria.cdsCasetaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCaseta do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerCafeteria.GrabaCaseta( dmCliente.Empresa, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
                  TressShell.SetDataChange( [ enAccesLog ] );
          end;
     end;
end;

procedure TdmCafeteria.cdsCasetaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
    sError : String;
begin
    if PK_Violation( E ) then
      with DataSet do
        sError := Format( 'Checada ya fu� capturada.' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s',
                    [ FieldByName( 'CB_CODIGO' ).AsInteger, FechaCorta( FieldByName( 'AL_FECHA' ).AsDateTime ) ] )
    else
        sError := GetErrorDescription( E );
    cdsCaseta.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;AL_FECHA;AL_HORA', sError );
    Action := raCancel;   // Est� en el grid, para que marque todos los errores y al final cierre la forma

end;

procedure TdmCafeteria.cdsCasetaBeforeOpen(DataSet: TDataSet);
begin
     Ultimafecha := dmCliente.FechaDefault;
end;

procedure TdmCafeteria.cdsCasetaCB_CODIGOChange(Sender: TField);
begin
     with cdsCaseta do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          // Asigna Valores Default
          if ( FieldByName( 'AL_FECHA' ).AsDateTime = NullDateTime ) then
          begin
               FieldByName( 'AL_FECHA' ).AsDateTime := UltimaFecha;
               FieldByName( 'AL_HORA' ).AsString := UltimaHora;
          end;
     end;
end;

procedure TdmCafeteria.cdsCasetaBeforePost(DataSet: TDataSet);
begin
     with cdsCaseta do
     begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger = 0 ) then
          begin
               FieldByName( 'CB_CODIGO' ).FocusControl;
               DataBaseError( 'N�mero de Empleado No Puede Quedar Vac�o' );
          end;
          if ( FieldByName( 'AL_FECHA' ).AsDateTime = NullDateTime ) then
          begin
               FieldByName( 'AL_FECHA' ).FocusControl;
               DataBaseError( 'Fecha de Registro No Puede Quedar Vacia' );
          end;
          if ( not StrLleno( FieldByName( 'AL_HORA' ).AsString ) ) then
          begin
               FieldByName( 'AL_HORA' ).FocusControl;
               DataBaseError( 'Hora de Registro No Puede Quedar Vacia' );
          end;
          if ( not StrLleno( FieldByName( 'AL_ENTRADA' ).AsString ) ) then
          begin
               FieldByName( 'AL_ENTRADA' ).FocusControl;
               DataBaseError( 'Tipo Checada No Puede Quedar Vacia' );
          end;          
     end;  
end;

procedure TdmCafeteria.cdsCasetaAfterPost(DataSet: TDataSet);
begin
     with cdsCaseta do
     begin
          Ultimafecha := FieldByName( 'AL_FECHA' ).AsDateTime;
          UltimaHora := FieldByName( 'AL_HORA' ).AsString;
    end;
end;

procedure TdmCafeteria.cdsCasetaNewRecord(DataSet: TDataSet);
begin
     with cdsCaseta do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger:= dmCliente.Usuario;
          FieldByName( 'AL_OK_MAN' ).AsString := K_GLOBAL_SI;
          FieldByName( 'AL_ENTRADA' ).AsString := K_GLOBAL_SI;          
     end;
end;



procedure TdmCafeteria.cdsCafDiariasAfterDelete(DataSet: TDataSet);
begin
     cdsCafDiarias.Enviar
end;

end.
