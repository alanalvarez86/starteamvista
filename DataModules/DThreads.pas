unit DThreads;

interface

uses Classes, SysUtils, Forms, Windows, ComObj, ActiveX,
     ZBaseThreads,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaClientTools,
{$ifndef VER130}
     Variants,
{$endif}
{$ifdef DOS_CAPAS}
     DServerIMSS,
     DServerSistema,
     DServerAsistencia,
     DServerNomina,
     DServerNominaTimbrado,
     DServerCalcNomina,
     DServerAnualNomina,
     DServerRecursos,
     DServerCafeteria;
{$else}
     IMSS_TLB,
     Asistencia_TLB,
     DAnualNomina_TLB,
     DCalcNomina_TLB,
     Nomina_TLB,
     TimbradoNomina_TLB,
     Recursos_TLB,
     Sistema_TLB,
     Cafeteria_TLB;
{$endif}


type
  { *** Threads de Asistencia *** }
  TAsistenciaThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerAsistencia;
    property Servidor: TdmServerAsistencia read GetServidor;
{$else}
    function GetServidor: IdmServerAsistenciaDisp;
    property Servidor: IdmServerAsistenciaDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;
  { *** Threads de IMSS *** }
  TIMSSThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerIMSS;
    property Servidor: TdmServerIMSS read GetServidor;
{$else}
    function GetServidor: IdmServerIMSSDisp;
    property Servidor: IdmServerIMSSDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;
  { *** Threads de N�mina *** }
  TNominaThread = class( TBaseThread )
  private
{$ifdef TIMBRADO}
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerNominaTimbrado;
    property Servidor: TdmServerNominaTimbrado read GetServidor;
{$else}
    function GetServidor: IdmServerNominaTimbradoDisp;
    property Servidor: IdmServerNominaTimbradoDisp read GetServidor;
{$endif}

{$else}
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerNomina;
    property Servidor: TdmServerNomina read GetServidor;
{$else}
    function GetServidor: IdmServerNominaDisp;
    property Servidor: IdmServerNominaDisp read GetServidor;
{$endif}
{$endif}
  protected
    function Procesar: OleVariant; override;
  end;
  { *** Threads de C�lculo de N�mina *** }
  TCalcNominaThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerCalcNomina;
    property Servidor: TdmServerCalcNomina read GetServidor;
{$else}
    function GetServidor: IdmServerCalcNominaDisp;
    property Servidor: IdmServerCalcNominaDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;
  { *** Threads de Procesos Anuales de N�mina *** }
  TAnualThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerAnualNomina;
    property Servidor: TdmServerAnualNomina read GetServidor;
{$else}
    function GetServidor: IdmServerAnualNominaDisp;
    property Servidor: IdmServerAnualNominaDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;
  { *** Threads de Recursos Humanos *** }
  TRecursosThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerRecursos;
    property Servidor: TdmServerRecursos read GetServidor;
{$else}
    function GetServidor: IdmServerRecursosDisp;
    property Servidor: IdmServerRecursosDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;
  { *** Threads de Sistema *** }
  TSistemaThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerSistema;
    property Servidor: TdmServerSistema read GetServidor;
{$else}
    function GetServidor: IdmServerSistemaDisp;
    property Servidor: IdmServerSistemaDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;
  { *** Threads de Cafeteria *** }
  TCafeteriaThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerCafeteria;
    property Servidor: TdmServerCafeteria read GetServidor;
{$else}
    function GetServidor: IdmServerCafeteriaDisp;
    property Servidor: IdmServerCafeteriaDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;

implementation

uses ZetaCommonTools,
     DCliente,
     DProcesos,
{$ifdef TIMBRADO}
     DInterfase, 
{$endif}
     DConsultas;


{ ******* TIMSSThread ******** }

{$ifdef DOS_CAPAS}
function TIMSSThread.GetServidor: TdmServerIMSS;
begin
     Result := DCliente.dmCliente.ServerIMSS;
end;

{$else}
function TIMSSThread.GetServidor: IdmServerIMSSDisp;
begin
     Result := IdmServerIMSSDisp( CreateServer( CLASS_dmServerIMSS ) );
end;
{$endif}

function TIMSSThread.Procesar: OleVariant;
var
   Arreglo, Empleados, Movimientos,Infonavit,Incapacidades: OleVariant;
begin
     case Proceso of
          prIMSSCalculoPagos   : Result := Servidor.CalcularPagosIMSS( Empresa, Parametros.VarValues );
          prIMSSCalculoRecargos: Result := Servidor.CalcularRecargosIMSS( Empresa, Parametros.VarValues );
          prIMSSTasaINFONAVIT  : Result := Servidor.CalcularTasaINFONAVIT( Empresa, Parametros.VarValues );
          prIMSSRevisarSUA     : Result := Servidor.RevisarSUA( Empresa, Parametros.VarValues );
          prIMSSExportarSUA:
          begin
               Result := Servidor.ExportarSUA( Empresa, Parametros.VarValues, Empleados, Movimientos,Infonavit, Incapacidades);
               Arreglo := VarArrayCreate( [ 0, 4 ], varVariant );
               Arreglo[ 0 ] := Parametros.VarValues;
               Arreglo[ 1 ] := Empleados;
               Arreglo[ 2 ] := Movimientos;
               Arreglo[ 3 ] := Infonavit;
               Arreglo[ 4 ] := Incapacidades;
               SetCargo( Arreglo );
          end;
          prIMSSCalculoPrima:
          begin
               Result := Servidor.CalcularPrimaRiesgo( Empresa, Parametros.VarValues, Empleados );
               Arreglo := VarArrayCreate( [ 0, 1 ], varVariant );
               Arreglo[ 0 ] := Parametros.VarValues;
               Arreglo[ 1 ] := Empleados;
               SetCargo( Arreglo );
          end;
          prIMSSAjusteRetInfonavit:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AjusteRetInfonavit( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AjusteRetInfonavitLista( Empresa, Employees, Parametros.VarValues );
          end;
          prIMSSValidacionMovimientosIDSE :
          begin
              Result := Servidor.ValidarMovimientosIDSE( Empresa, Employees, Parametros.VarValues );
          end;
     end;
end;

{ ******* TAsistenciaThread ******** }

{$ifdef DOS_CAPAS}
function TAsistenciaThread.GetServidor: TdmServerAsistencia;
begin
     Result := DCliente.dmCliente.ServerAsistencia;
end;
{$else}
function TAsistenciaThread.GetServidor: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( CreateServer( CLASS_dmServerAsistencia ) );
end;
{$endif}
function TAsistenciaThread.Procesar: OleVariant;
begin
     case Proceso of
          prASISExtrasPer:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AutorizarHoras( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AutorizarHorasLista( Empresa, Employees, Parametros.VarValues );
          end;
          prASISCorregirFechas: Result := Servidor.CorregirFechas( Empresa, Parametros.VarValues );
          prSISTBorrarTarjetas: Result := Servidor.BorrarTarjetas( Empresa, Parametros.VarValues );
          prSISTBorrarPOLL: Result := Servidor.BorrarPoll( Usuario, Parametros.VarValues );
          prASISIntercambioFestivo:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.IntercambioFestivo( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.IntercambioFestivoLista( Empresa, Employees, Parametros.VarValues );
          end;
          prASISCancelarExcepcionesFestivos:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CancelarExcepcionesFestivos( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CancelarExcepcionesFestivosLista( Empresa, Employees, Parametros.VarValues );
          end;
           prASISRegistrarExcepcionesFestivos:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.RegistrarExcepcionesFestivo( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.RegistrarExcepcionesFestivoLista( Empresa, Parametros.VarValues,Employees );
          end;
          prASIAjustIncapaCal:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AjustarIncapaCal( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.AjustarIncapaCalLista( Empresa, Employees, Parametros.VarValues );          
         end;
         prASISAutorizacionPreNomina:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AutorizacionPreNomina( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.AutorizacionPreNominaLista( Empresa, Employees,Parametros.VarValues  );
         end;
         prNoTransferenciasCosteo:
         begin
              if VarIsNull( Employees ) then
                 Result := Servidor.TransferenciasCosteo( Empresa, Parametros.VarValues )
              else
                  Result := Servidor.TransferenciasCosteoLista( Empresa, Employees, Parametros.VarValues );
         end;
         prASISImportacionClasificaciones:
         begin
              Result := Servidor.ImportarClasificacionesTemp( Empresa, Parametros.VarValues, Employees );
         end;
         prNoCalculaCosteo:
         begin
              Result := Servidor.CalculaCosteo( Empresa, Parametros.VarValues )
         end;
         prNoCancelaTransferencias:
         begin
              if VarIsNull( Employees ) then
                 Result := Servidor.CancelaTransferenciasCosteo( Empresa, Parametros.VarValues )
              else
                  Result := Servidor.CancelaTransferenciasCosteoLista( Empresa, Employees, Parametros.VarValues );
         end;
     end;
end;

{ ******* TNominaThread ******** }

{$ifdef TIMBRADO}
{$ifdef DOS_CAPAS}
function TNominaThread.GetServidor: TdmServerNominaTimbrado;
begin
    Result := dmInterfase.ServerNominaTimbrado; 
end;
{$else}
function TNominaThread.GetServidor: IdmServerNominaTimbradoDisp;
begin
     Result := IdmServerNominaTimbradoDisp( CreateServer( CLASS_dmServerNominaTimbrado ) );
end;
{$endif}

{$else}
{$ifdef DOS_CAPAS}
function TNominaThread.GetServidor: TdmServerNomina;
begin
     Result := DCliente.dmCliente.ServerNomina;
end;
{$else}
function TNominaThread.GetServidor: IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( CreateServer( CLASS_dmServerNomina ) );
end;
{$endif}
{$endif}
function TNominaThread.Procesar: OleVariant;
var
   Datos, Arreglo: OleVariant;
   VarValues: OleVariant;//US Conciliar
begin
     case Proceso of
          prNOAfectar: Result := Servidor.AfectarNomina( Empresa, Parametros.VarValues );
          prNODesafectar: Result := Servidor.DesafectarNomina( Empresa, Parametros.VarValues );
          prNOFoliarRecibos: Result := Servidor.FoliarRecibos( Empresa, Parametros.VarValues );
          prNODefinirPeriodos:
          begin
               Result := Servidor.DefinirPeriodos( Empresa, Parametros.VarValues );
               SetCargo( Parametros.ParamByName( 'Tipo' ).AsInteger );
          end;
          prNOPagosPorFuera:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.PagarPorFuera( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.PagarPorFueraLista( Empresa, Employees, Parametros.VarValues );
          end;
          prNOImportarMov: Result := Servidor.ImportarMovimientos( Empresa, Parametros.VarValues, Employees );
          prNOImportarAcumuladosRS: Result := Servidor.ImportarMovimientos( Empresa, Parametros.VarValues, Employees );
          prNOExportarMov:
          begin
               Result := Servidor.ExportarMovimientos( Empresa, Parametros.VarValues, Datos );
               Arreglo := VarArrayCreate( [ 0, 1 ], varVariant );
               Arreglo[ 0 ] := Parametros.VarValues;
               Arreglo[ 1 ] := Datos;
               SetCargo( Arreglo );
          end;
          prNOPolizaContable: Result := Servidor.Poliza( Empresa, Parametros.VarValues, Employees );
          prNOLiquidacion:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.LiquidacionGlobal( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.LiquidacionGlobalLista( Empresa, Employees, Parametros.VarValues );
          end;
          prNOCopiarNominas: Result := Servidor.CopiarNomina( Empresa, Parametros.VarValues );          
          prNOCalcularDiferencias: Result := Servidor.CalcularDiferencias( Empresa, Parametros.VarValues );
          prNOLimpiarAcum: Result := Servidor.LimpiarAcumulado( Empresa, Parametros.VarValues );
          prNORecalcAcum: Result := Servidor.RecalculoAcumulado( Empresa, Parametros.VarValues );
          {*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
          prNORecalcAhorros: Result := Servidor.RecalculoAcumulado( Empresa, Parametros.VarValues );
          {*** FIN ***}
          {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
          prNORecalcPrestamos:  Result := Servidor.RecalculoAcumulado( Empresa, Parametros.VarValues );
          {*** FIN ***}
          prNOPagoRecibos: Result := Servidor.ImportarPagoRecibosLista( Empresa, Employees, Parametros.VarValues );
          prNOReFoliarRecibos: Result := Servidor.ReFoliarRecibos( Empresa, Parametros.VarValues );
          prSISTBorrarNominas: Result := Servidor.BorrarPeriodos( Empresa, Parametros.VarValues );
          {$ifndef TIMBRADO}
          prSISTBorrarTimbradoNominas: Result := Servidor.BorrarTimbradoPeriodos( Empresa, Parametros.VarValues );
          {$endif}
          prNOAjusteRetFonacot:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AjusteRetFonacot( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AjusteRetFonacotLista( Empresa, Employees,  Parametros.VarValues );
          end;
          {$ifndef TIMBRADO}
          prNOAjusteRetFonacotCancelacion:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AjusteRetFonacotCancelacion( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AjusteRetFonacotCancelacionLista( Empresa, Employees,  Parametros.VarValues );
          end;
          {$endif}
          prNOCalcularPagoFonacot: Result:= Servidor.CalcularPagoFonacot( Empresa, Parametros.VarValues );
          prEmpAplicacionFiniGlobal:
          begin
               if VarIsNull( Employees ) then
                  Result:= Servidor.AplicacionGlobalFiniquitos( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AplicacionGlobalFiniquitosLista( Empresa, Employees,  Parametros.VarValues );
          end;
          prNoTimbrarNomina:
          begin
               {$ifdef TIMBRADO}
               if not VarIsNull( Employees ) then
                  Result := Servidor.TimbrarNominasLista( Empresa, Employees,  Parametros.VarValues )
               else {$endif}
                   Result := FALSE;


          end;

          prIniciarConciliacionTimbrado:
          begin
               {$ifdef TIMBRADO}
               VarValues := Parametros.VarValues;
               Servidor.IniciarConciliacionTimbradoPeriodos(VarValues);
               Result := VarValues;
               Parametros.VarValues := VarValues;
               {$endif}
          end;

          prAplicarConciliacionTimbrado:
          begin
               {$ifdef TIMBRADO}
               VarValues := Parametros.VarValues;
               Servidor.AplicarConciliacionTimbradoPeriodos(VarValues);
               Result := VarValues;
               Parametros.VarValues := VarValues;
               {$endif}
          end;
          {$ifndef TIMBRADO}
          prNOFonacotImportarCedulas: Result:= Servidor.ImportarCedulasFonacot (Empresa, Parametros.VarValues, Employees);

          // prNOFonacotGenerarCedula: Result:= Servidor.GenerarCedulaPagoFonacot (Empresa, Parametros.VarValues, Employees);
          prNOFonacotGenerarCedula:
          begin
               Result := Servidor.GenerarCedulaPagoFonacot( Empresa, Parametros.VarValues, Datos );
               Arreglo := VarArrayCreate( [ 0, 1 ], varVariant );
               Arreglo[ 0 ] := Parametros.VarValues;
               Arreglo[ 1 ] := Datos;
               SetCargo( Arreglo );
          end;

          prNOFonacotEliminarCedula: Result:= Servidor.EliminarCedulaFonacot (Empresa, Parametros.VarValues, Datos );
          {$endif}




    end;
end;

{ ******* TCalcNominaThread ******** }

{$ifdef DOS_CAPAS}
function TCalcNominaThread.GetServidor: TdmServerCalcNomina;
begin
     Result := DCliente.dmCliente.ServerCalcNomina;
end;
{$else}
function TCalcNominaThread.GetServidor: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( CreateServer( CLASS_dmServerCalcNomina ) );
end;
{$endif}
function TCalcNominaThread.Procesar: OleVariant;
var
   Datos, Arreglo: OleVariant;
begin
     case Proceso of
          prRHTranferencia: Result := Servidor.Transferencia( Empresa, Employees, Parametros.VarValues );
          prNOCalcular: Result := Servidor.CalculaNomina( Empresa, Parametros.VarValues );
          prASISCalculoPrenomina: Result := Servidor.CalculaPreNomina( Empresa, Parametros.VarValues );
          prASISProcesarTarjetas: Result := Servidor.ProcesarTarjetasPendientes( Empresa, Parametros.VarValues );
          prASISPollRelojes: Result := Servidor.Poll( Empresa, Parametros.VarValues, Employees );
          prASISRecalculoTarjetas: Result := Servidor.RecalcularTarjetas( Empresa, Parametros.VarValues );
          prASISRegistrosAut: Result := Servidor.RegistrosAutomaticos( Empresa, Parametros.VarValues );
          prNORastrearCalculo:
          begin
               Result := Servidor.RastreaConcepto( Empresa, Parametros.VarValues, Datos );
               Arreglo := VarArrayCreate( [ 0, 1 ], varVariant );
               Arreglo[ 0 ] := Parametros.VarValues;
               Arreglo[ 1 ] := Datos;
               SetCargo( Arreglo );
          end;
          prNOCalculaRetroactivos:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CalculaRetroactivo( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.CalculaRetroactivoLista( Empresa, Employees, Parametros.VarValues );
          end;
     end;
end;

{ ********** TAnualThread *********** }

{$ifdef DOS_CAPAS}
function TAnualThread.GetServidor: TdmServerAnualNomina;
begin
     Result := DCliente.dmCliente.ServerAnualNomina;
end;
{$else}
function TAnualThread.GetServidor: IdmServerAnualNominaDisp;
begin
     Result := IdmServerAnualNominaDisp( CreateServer( CLASS_dmServerAnualNomina ) );
end;
{$endif}
function TAnualThread.Procesar: OleVariant;
begin
     case Proceso of
          prSISTCierreAhorros: Result := Servidor.CerrarAhorros( Empresa, Parametros.VarValues );
          prSISTCierrePrestamos: Result := Servidor.CerrarPrestamos( Empresa, Parametros.VarValues );
          prNOCalculoAguinaldo: Result := Servidor.CalcularAguinaldo( Empresa, Parametros.VarValues );
          prNOPagoAguinaldo:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.PagarAguinaldo( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.PagarAguinaldoLista( Empresa, Employees, Parametros.VarValues );
          end;
          prNOCalculoPTU: Result := Servidor.CalcularPTU( Empresa, Parametros.VarValues );
          prNOPagoPTU:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.PTUPago( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.PTUPagoLista( Empresa, Employees, Parametros.VarValues );
          end;
          prNORepartoAhorro: Result := Servidor.CalcularRepartoAhorro( Empresa, Parametros.VarValues );
          prNOPagoRepartoAhorro:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.PagarAhorro( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.PagarAhorroLista( Empresa, Employees, Parametros.VarValues );
          end;
          prNOISPTAnual: Result := Servidor.ISPTAnual( Empresa, Parametros.VarValues );
          prNOISPTAnualPago:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.ISPTPago( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.ISPTPagoLista( Empresa, Employees, Parametros.VarValues );
          end;
          prNOCreditoSalario: Result := Servidor.DeclaraCredito( Empresa, Parametros.VarValues );
          prNOCreditoAplicado:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CreditoAplicado( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CreditoAplicadoLista( Empresa, Employees,  Parametros.VarValues );
          end;
          prNoDeclaracionAnual:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.DeclaracionAnual( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.DeclaracionAnualLista( Empresa, Employees,  Parametros.VarValues );
          end;
          prNoDeclaracionAnualCierre:
          begin
               Result := Servidor.DeclaracionAnualCierre( Empresa, Parametros.VarValues )
          end;
          prNOPrevioISR:
          begin
          	   Result := Servidor.PrevioISR( Empresa, Parametros.VarValues )
          end;
     end;
end;

{ ******* TRecursosThread ******** }

{$ifdef DOS_CAPAS}
function TRecursosThread.GetServidor: TdmServerRecursos;
begin
     Result := DCliente.dmCliente.ServerRecursos;
end;
{$else}
function TRecursosThread.GetServidor: IdmServerRecursosDisp;
begin
     Result := IdmServerRecursosDisp( CreateServer( CLASS_dmServerRecursos ) );
end;
{$endif}

function TRecursosThread.Procesar: OleVariant;
var
   Errores: WideString;
   Arreglo: OleVariant;
begin
     case Proceso of
          prRHAplicarTabulador:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AplicarTabulador( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AplicarTabuladorLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHPromediarVariables:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.PromediarVariables( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.PromediarVariablesLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHSalarioIntegrado:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.SalarioIntegrado( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.SalarioIntegradoLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHCambioSalario:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CambioSalario( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CambioSalarioLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHAplicarEventos:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.Eventos( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.EventosLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHImportarKardex: Result := Servidor.ImportarKardexLista( Empresa, Employees, Parametros.VarValues );
          prRHCancelarKardex:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CancelarKardex( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CancelarKardexLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHCancVacGlobales:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CancelarVacaciones( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CancelarVacacionesLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHVacacionesGlobales:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.Vacaciones( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.VacacionesLista( Empresa, Employees, Parametros.VarValues );
          end;
          prSISTBorrarBajas:
          begin
               Result := Servidor.BorrarBajas( Empresa, Parametros.VarValues );
          end;
          prRHCierreVacacionGlobal:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CierreGlobalVacaciones( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CierreGlobalVacacionesLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHEntregarHerramienta:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.EntregarHerramienta( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.EntregarHerramientaLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHRegresarHerramienta:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.RegresarHerramienta( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.RegresarHerramientaLista( Empresa, Employees, Parametros.VarValues );
          end;
          prSISTBorrarHerramienta:
          begin
               Result := Servidor.BorrarHerramientas( Empresa, Parametros.VarValues );
          end;
          prRHCursoTomado:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CursoTomado( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.CursoTomadoLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHImportarAltas: Result := Servidor.ImportarAltasLista( Empresa, Employees, Parametros.VarValues );
          prRHRenumeraEmpleados:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.Renumera( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.RenumeraLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHPermisoGlobal:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.PermisoGlobal( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.PermisoGlobalLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHBorrarCursoTomado:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.BorrarCursoTomado( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.BorrarCursoTomadoLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHImportarAsistenciaSesiones: Result := Servidor.ImportarAsistenciaSesionesLista( Empresa, Employees, Parametros.VarValues );
          prRHCancelarCierreVacaciones:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CancelaCierreVacaciones( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CancelaCierreVacacionesLista( Empresa, Employees, Parametros.VarValues );
          end;
          prRHCancelarPermisosGlobales:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CancelarPermisosGlobales( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.CancelarPermisosGlobalesLista ( Empresa, Parametros.VarValues,Employees );
          end;
          prRHImportarAhorrosPrestamos:  Result := Servidor.ImportarAhorrosPrestamos( Empresa, Parametros.VarValues, Employees );
          prRHRecalculaSaldosVacaciones:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.RecalculaSaldosVaca( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.RecalculaSaldosVacaLista ( Empresa, Employees, Parametros.VarValues );
          end;
          prRHCursoProgGlobal:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CursoProgGlobal( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.CursoProgGlobalLista( Empresa, Employees, Parametros.VarValues );          
         end;
          prRHCancelarCursoProgGlobal:
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.CancelarCursoProgGlobal( Empresa, Parametros.VarValues )
               else
                  Result := Servidor.CancelarCursoProgGlobalLista( Empresa, Employees, Parametros.VarValues );          
         end;

         // prRHFoliarCapacitaciones:
         // begin
              // Result := Servidor.FoliarCapacitaciones( Empresa, Parametros.VarValues );
         // end;
         
         prRHFoliarCapacitaciones:
         begin
              if VarIsNull( Employees ) then
                 Result := Servidor.FoliarCapacitacionesSTPS( Empresa, Parametros.VarValues )
              else
                  Result := Servidor.FoliarCapacitacionesSTPSLista( Empresa, Employees, Parametros.VarValues );
         end;
         prRHReiniciarCapacitacionesSTPS:
         begin
              if VarIsNull( Employees ) then
                 Result := Servidor.FoliarCapacitacionesSTPS( Empresa, Parametros.VarValues )
              else
                  Result := Servidor.FoliarCapacitacionesSTPSLista( Empresa, Employees, Parametros.VarValues );
         end; 
         prRHRevisarDatosSTPS :
         begin
            Result := Servidor.RevisarDatosSTPS ( Empresa, Parametros.VarValues, Errores );
            Arreglo := VarArrayCreate( [ 0, 1 ], varVariant );
            Arreglo[ 0 ] := Parametros.VarValues;
            Arreglo[ 1 ] := Errores;
            SetCargo( Arreglo );
         end;

         prRHImportarSGM:
         begin
              Result := Servidor.ImportarSGMLista( Empresa, Parametros.VarValues, Employees );
         end;
         prRHRenovacionSGM:
         begin
              Result := Servidor.RenovarSGMLista( Empresa, Parametros.VarValues, Employees );
         end;
         prRHBorrarSGM:
         begin
              Result := Servidor.BorrarSGMLista( Empresa, Parametros.VarValues, Employees );
         end;
         prRHImportarOrganigrama:
         begin
              Result := Servidor.ImportarOrganigramaLista( Empresa, Parametros.VarValues, Employees );
         end;
         prRHImportarImagenes:
         begin
              Result := Servidor.ImportarImagenesLista( Empresa, Employees, Parametros.VarValues);
         end;
         prEmpCambioTurnoMasivo:
         begin
              if VarIsNull( Employees ) then
                 Result := Servidor.CambioMasivoTurno( Empresa, Parametros.VarValues )
              else
                 Result := Servidor.CambioMasivoTurnoLista( Empresa, Employees, Parametros.VarValues );
         end;
     end;
end;

{ ******* TSistemaThread ******** }

{$ifdef DOS_CAPAS}
function TSistemaThread.GetServidor: TdmServerSistema;
begin
     Result := DCliente.dmCliente.ServerSistema;
end;
{$else}
function TSistemaThread.GetServidor: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( CreateServer( CLASS_dmServerSistema ) );
end;
{$endif}

function TSistemaThread.Procesar: OleVariant;
var
   Bitacora: OleVariant;
   VarValues: OleVariant;
   Datos, Arreglo: OleVariant;
begin
     case Proceso of
          prSISTDepurar: Result := Servidor.BorrarBitacora( Empresa, Parametros.VarValues );
          prSISTNumeroTarjeta:
          begin
               Result := Servidor.ActualizaNumeroTarjeta( Bitacora );
               SetCargo( Bitacora );
          end;
          prSistEnrolarUsuario :  Result := Servidor.EnrolamientoMasivo( Empresa,Parametros.VarValues);
          prSistImportarEnrolamientoMasivo  :  Result := Servidor.ImportarEnrolamientoMasivo( Empresa,Parametros.VarValues,Employees);
          {$ifndef DOS_CAPAS}
          prSISTDepuraBitBiometrico: Result := Servidor.DepuraBitacoraBiometrica( Parametros.VarValues ); // SYNERGY
          prSISTAsignaNumBiometricos: // SYNERGY
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AsignaNumBiometricos( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AsignaNumBiometricosLista( Empresa, Employees, Parametros.VarValues );
          end;
          prSISTAsignaGrupoTerminales: // SYNERGY
          begin
               if VarIsNull( Employees ) then
                  Result := Servidor.AsignaGrupoTerminales( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.AsignaGrupoTerminalesLista( Empresa, Employees, Parametros.VarValues );
          end;
          prSISTImportaTerminales: // SYNERGY
          begin
               Result := Servidor.ImportaTerminales( Parametros.VarValues, dmCliente.Usuario, Bitacora );
               SetCargo( Bitacora );               
          end;
          {$endif}

          prSISTPrepararPresupuestos:
          begin
               // Result := Servidor.PrepararPresupuestosWizard(Parametros.VarValues);
          end;

          prPatchCambioVersion:
          begin
               VarValues := Parametros.VarValues;
               Servidor.EjecutaMotorPatch(VarValues);
               Result := VarValues;
               Parametros.VarValues := VarValues;
          end;

          prSISTImportarTablas: Result := Servidor.ImportarTablas ( dmConsultas.BaseDatosImportacionXMLCSV, Parametros.VarValues );

          prSISTImportarTablasCSV: Result := Servidor.ImportarTablaCSVLista( dmConsultas.BaseDatosImportacionXMLCSV, Employees, Parametros.VarValues );
          
          prSISTExtraerChecadas:
          begin
               Result := Servidor.ExtraerChecadasBitTerm( dmCliente.Empresa, Parametros.VarValues, Datos );
               Arreglo := VarArrayCreate( [ 0, 1 ], varVariant );
               Arreglo[ 0 ] := Parametros.VarValues;
               Arreglo[ 1 ] := Datos;
               SetCargo( Arreglo );
          end;

          prSISTRecuperarChecadas://@DC
          begin
               Result := Servidor.GeneraReloj( dmCliente.Empresa, Parametros.VarValues, Datos );
               Arreglo := VarArrayCreate( [ 0, 1 ], varVariant );
               Arreglo[ 0 ] := Parametros.VarValues;
               Arreglo[ 1 ] := Datos;
               SetCargo( Arreglo );
          end;

          prSISTImportarCafeteria: Result := Servidor.CopiarConfiguracionCafeteria(dmCliente.Empresa, Parametros.VarValues );

          {$ifdef TRESS_DELPHIXE5_UP}
          prSISTAgregarTareaCalendarioReportes: //@GB
          begin
               Result := Servidor.GrabaTareaCalendario( dmCliente.Empresa, Parametros.VarValues, Datos );
          end;
          {$endif}
     end;
end;

{ ******* TCafeteriaThread ******** }

{$ifdef DOS_CAPAS}
function TCafeteriaThread.GetServidor: TdmServerCafeteria;
begin
     Result := DCliente.dmCliente.ServerCafeteria;
end;

{$else}
function TCafeteriaThread.GetServidor: IdmServerCafeteriaDisp;
begin
     Result := IdmServerCafeteriaDisp( CreateServer( CLASS_dmServerCafeteria ) );
end;
{$endif}

function TCafeteriaThread.Procesar: OleVariant;
begin
     case Proceso of
          prCAFEComidasGrupales   :
          begin
               if VarIsNull( Employees ) then
                   Result := Servidor.ComidasGrupales( Empresa, Parametros.VarValues )
               else
                   Result := Servidor.ComidasGrupalesLista( Empresa, Employees, Parametros.VarValues );
          end;
          prCAFECorregirFechas: Result := Servidor.CorregirFechasCafe( Empresa, Parametros.VarValues );
     end;
end;


end.
