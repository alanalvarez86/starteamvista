unit DBaseGlobal;

{.$define TEST_pe}  //Directiva para probar cuales programaciones especiales tiene el cliente.

interface

uses Forms, Controls, DB, Classes, StdCtrls,
     {$ifndef VER130}
     Variants,
     {$endif}
     ZetaCommonClasses,
     ZetaCommonLists,
     ZGlobalTress,
     ZToolsPE,
     {$ifdef TEST_pe}
     Dialogs,TypInfo,
     {$endif}
     ExtCtrls;


type
  TItemGlobal = record
    case Tipo: eTipoGlobal of
      tgBooleano: (booResult: Boolean);
      tgFloat   : (dblResult: Double );
      tgNumero  : (intResult: Longint );
      tgFecha   : (datResult: TDate );
      tgTexto   : (strLleno : Boolean );
  end;
  TdmBaseGlobal = class(TObject )
  private
    { Private declarations }
    FArregloGlobal: array[ 0 .. K_TOT_GLOBALES ] of TItemGlobal;
    FArregloStrings: array[ 0 .. K_TOT_GLOBALES ] of String; { No quise poner el string dentro del Record, porque tiene que ser de longitud fija y ocupa mucha memoria }
    FArregloDescripcionesGV : array[ 0 .. K_TOT_DESCRIPCIONES_GV, 0 .. 2 ] of String; { No quise poner el string dentro del Record, porque tiene que ser de longitud fija y ocupa mucha memoria }
    function GetArregloDescripcionesGV(Index1, Index2: Integer): string;
    function ComponenteValido( Componente: TComponent ): Boolean;
    procedure LeeParam( iCodigo: Integer; Valor: TParam );
    procedure EscribeParam( var aGlobalServer: Variant; iCodigo: Integer; Valor: TParam );
    procedure Cargar( Valores: TParams ); { Carga valores usados en capturas }
  protected
    function GetGlobales: Variant; virtual; abstract;
    procedure GrabaGlobales( const aGlobalServer: Variant; const lActualizaDiccion: Boolean;
              var ErrorCount: Integer ); virtual; abstract;

    function GetDescripcionesGV : Variant; virtual; abstract;
    procedure GrabaDescripcionesGV( const aGlobalServer: Variant; const lActualizaDiccion: Boolean;
              var ErrorCount: Integer ); virtual; abstract;

    function GetTipoGlobal( const i: integer ): eTipoGlobal;virtual;
    function GetTotalesGlobales: integer; virtual;
  public
    { Public declarations }
    property ResultArregloDescripcionesGV[Index1, Index2: Integer]: string read GetArregloDescripcionesGV;
    procedure Conectar;virtual;
    // Interfase con Formas de Edición de Globales
    procedure CargaControles(Parametros: TZetaParams; Forma: TCustomForm);
    procedure DescargaControles(Parametros: TZetaParams; Forma: TCustomForm; const lActualizaDiccion : Boolean = FALSE);
    procedure Descargar( Valores: TParams; const lActualizaDiccion: Boolean = FALSE );   { Descarga valores usados en capturas }
    procedure ConectarDescripcionesGV;
    // Obtener Globales por cada tipo, dada su constante
    function  GetGlobalBooleano( const iCodigo: Integer ): Boolean;
    function  GetGlobalString( const iCodigo: Integer ): String;
    function  GetGlobalInteger( const iCodigo: Integer ): Integer;
    function  GetGlobalReal( const iCodigo: Integer ): Real;
    function  GetGlobalDate( const iCodigo: Integer ): TDateTime;
    function  OK_ProyectoEspecial( const eProgEspecial: eProyectoEspecial ): Boolean;
    // Campos Adicionales
    function HayAdicionales: Boolean;
    function AdFecha( const Index: Integer ): String;
    function AdTexto( const Index: Integer ): String;
    function AdTabla( const Index: Integer ): String;
    function AdNumero( const Index: Integer ): String;
    function AdLogico( const Index: Integer ): String;
  end;

   function StrToReal( const sValor: String ): Extended;

implementation

uses {$ifndef TRESS_REPORTES}DDiccionario,{$endif}
     SysUtils,
     ZetaHora,
     ZetaNumero,
     ZetaEdit,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaKeyLookup,
     ZetaKeyLookup_DevEx,
     cxMemo,
     ZetaServerTools,
     ZetaCommonTools;

function StrToReal( const sValor: String ): Extended;
begin
     if ( Trim( sValor ) = '' ) then
        Result := 0
     else
     begin
          try
             Result := StrToFloat( Trim( sValor ) );
          except
                Result := 0;
          end;
     end;
end;

{ ************** TdmBaseGlobal *************** }
{
 GetGlobales recibe un arreglo ajustado al numero total de CONSTANTES Globales.
 Cuando NO hay un registro correspondiente a la Constante, en su lugar viene un 'NIL'.
 De lo contrario, en su lugar viene la formula (GLOBAL.GL_FORMULA ).
}

function TdmBaseGlobal.GetArregloDescripcionesGV(Index1, Index2: Integer): string;
begin
  Result := FArregloDescripcionesGV[Index1, Index2];
end;

function TdmBaseGlobal.GetTipoGlobal( const i: integer ): eTipoGlobal;
begin
     Result := ZGlobalTress.GetTipoGlobal( PosToGlobal( i ) );
end;

function TdmBaseGlobal.GetTotalesGlobales: integer;
begin
     Result := K_TOT_GLOBALES;
end;

procedure TdmBaseGlobal.Conectar;
var
   aGlobalServer: Variant;
   sFormula: String;
   i: Integer;
begin
//     aGlobalServer := Server.GetGlobales( dmCliente.Empresa );
     aGlobalServer := GetGlobales;
     for i := 1 to GetTotalesGlobales do
     begin
          FArregloStrings[ i ] := '';
          try
             sFormula := aGlobalServer[ i ];
          except
                sFormula := K_TOKEN_NIL;   // Si no existe el global en el arreglo regresa NIL
          end;
          with FArregloGlobal[ i ] do
          begin
               if ( sFormula = K_TOKEN_NIL ) then
                    sFormula := '';

               //Tipo := ZGlobalTress.GetTipoGlobal( PosToGlobal( i ) );
               Tipo := GetTipoGlobal( i );

               case Tipo of
                    tgBooleano : booResult := zStrToBool( sFormula );
                    tgFloat    : dblResult := StrToReal( sFormula );
                    tgNumero   : intResult := StrToIntDef( sFormula, 0 );
                    tgFecha    : datResult := StrToFecha( sFormula );
                    else
                    begin
                         FArregloStrings[ i ] := sFormula;
                         strLleno := Length( sFormula ) > 0;
                    end;
               end;
          end;
     end;
     {$ifndef TRESS_REPORTES}
     dmDiccionario.SetLookupNames;
     {$endif}
end;


procedure TdmBaseGlobal.ConectarDescripcionesGV ;
var
   aGlobalServer: Variant;
   sDescripcion: String;
   i, j, iCodigo: Integer;
begin
     aGlobalServer := GetDescripcionesGV ;
     sDescripcion := VACIO;
     j := 0;
     for i := 0 to K_TOT_DESCRIPCIONES_GV do
         begin
           FArregloDescripcionesGV[i,j] := K_TOKEN_NIL;
          try
             iCodigo := aGlobalServer[ i, 0 ];
             sDescripcion := aGlobalServer[ i, 1 ];
             FArregloDescripcionesGV[i,0] := IntToStr(iCodigo);
             FArregloDescripcionesGV[i,1] := sDescripcion;
          except
                  FArregloDescripcionesGV[i,0] := '';
                  FArregloDescripcionesGV[i,0] := '';
          end;
     end;
end;


function TdmBaseGlobal.GetGlobalBooleano(const iCodigo: Integer): Boolean;
begin
     Result := FArregloGlobal[ GlobalToPos( iCodigo ) ].booResult;
end;

function TdmBaseGlobal.GetGlobalDate(const iCodigo: Integer): TDateTime;
begin
     Result := FArregloGlobal[ GlobalToPos( iCodigo ) ].datResult;
end;

function TdmBaseGlobal.GetGlobalInteger(const iCodigo: Integer): Integer;
begin
     Result := FArregloGlobal[ GlobalToPos( iCodigo ) ].intResult;
end;

function TdmBaseGlobal.GetGlobalReal(const iCodigo: Integer): Real;
begin
     Result := FArregloGlobal[ GlobalToPos( iCodigo ) ].dblResult;
end;

function TdmBaseGlobal.GetGlobalString(const iCodigo: Integer): String;
begin
     Result := FArregloStrings[ GlobalToPos( iCodigo ) ];
end;

procedure TdmBaseGlobal.Cargar( Valores: TParams );
var
   i, iCodigo: Integer;
   Valor: TParam;
begin
     with Valores do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               Valor := Items[ i ];
               iCodigo := StrToIntDef( Valor.Name, 0 );
               if iCodigo >= 0 then
                  LeeParam( iCodigo, Valor );
          end;
     end;
end;

procedure TdmBaseGlobal.LeeParam( iCodigo: Integer; Valor: TParam );
begin
     iCodigo := GlobalToPos( iCodigo );
     with FArregloGlobal[ iCodigo ], Valor do
     begin
          case Tipo of
               tgBooleano:
               begin
                    DataType := ftBoolean;
                    AsBoolean := booResult;
               end;
               tgFloat:
               begin
                    DataType := ftFloat;
                    AsFloat := dblResult;
               end;
               tgNumero:
               begin
                    DataType := ftInteger;
                    AsInteger := intResult;
               end;
               tgFecha:
               begin
                    DataType := ftDateTime;
                    AsDateTime := datResult;
               end;
               else
               begin
                    DataType := ftString;
                    AsString := FArregloStrings[ iCodigo ];
               end;
          end;
     end;
end;

procedure TdmBaseGlobal.Descargar( Valores: TParams; const lActualizaDiccion : Boolean );
var
   i, iCodigo, ErrorCount: Integer;
   Valor: TParam;
   aGlobalServer: Variant;
begin
     with Valores do
     begin
          if ( Count > 0 ) then
          begin
               aGlobalServer := VarArrayCreate( [ 0, K_TOT_GLOBALES ], varOleStr );
               { Inicializa con 'NIL' }
               for i := 0 to K_TOT_GLOBALES do
                   aGlobalServer[ i ] := K_TOKEN_NIL;
               for i := 0 to ( Count - 1 ) do
               begin
                    Valor := Items[ i ];
                    iCodigo := StrToIntDef( Valor.Name, 0 );
                    if iCodigo >= 0 then
                       EscribeParam( aGlobalServer, iCodigo, Valor );
               end;
               { Enviar a Servidor la Lista de los Parámetros que cambiaron para que se graben en la tabla Global }
               GrabaGlobales( aGlobalServer, lActualizaDiccion, ErrorCount );
//               Server.GrabaGlobales( dmCliente.Empresa, aGlobalServer, lActualizaDiccion, ErrorCount );
               {$ifndef TRESS_REPORTES}
               if lActualizaDiccion then
                  dmDiccionario.CambioGlobales;
               {$endif}
               if ( ErrorCount > 0 ) then
                  DataBaseError( 'Error al Grabar en Tabla Global' );
          end;
     end;
end;

procedure TdmBaseGlobal.EscribeParam( var aGlobalServer: Variant; iCodigo: Integer; Valor: TParam );
var
   sFormula: String;
   lDiferentes: Boolean;
begin
     sFormula := Valor.AsString;
     iCodigo  := GlobalToPos( iCodigo );
     with FArregloGlobal[ iCodigo ], Valor do
        case Tipo of
             tgBooleano:
             begin
                  lDiferentes := ( AsBoolean <> booResult );
                  booResult := AsBoolean;
                  sFormula := zBoolToStr( AsBoolean );
             end;
             tgFloat:
             begin
                  lDiferentes := ( AsFloat <> dblResult );
                  dblResult := AsFloat;
                  sFormula := AsString;
             end;
             tgNumero:
             begin
                  lDiferentes := ( AsInteger <> intResult );
                  intResult := AsInteger;
                  sFormula := AsString;
             end;
             tgFecha:
             begin
                  lDiferentes := ( AsDateTime <> datResult );
                  datResult := AsDateTime;
                  sFormula := FechaToStr( AsDateTime );
             end;
             else
             begin
                  lDiferentes := ( AsString <> FArregloStrings[ iCodigo ] );
                  FArregloStrings[ iCodigo ] := AsString;
                  sFormula := AsString;
             end;
        end;

     if ( lDiferentes ) then
        aGlobalServer[ iCodigo ] := sFormula;
end;

{ *** Cargar /Descargar de controles para ZetaBaseGlobal y ZetaBaseWizard *** }

function TdmBaseGlobal.ComponenteValido( Componente: TComponent ): Boolean;
begin
     Result := ( Componente.Tag <> 0 ) and
               ( ( Componente is TCustomEdit ) or
                 ( Componente is TCheckBox ) or
                 ( Componente is TComboBox ) or
                 ( Componente is TZetaKeyCombo ) or
                 ( Componente is TZetaKeyLookup ) or
                 ( Componente is TZetaKeyLookup_DevEx ) or
                 ( Componente is TRadioButton ) or
                 ( Componente is TRadioGroup ) or
                 ( Componente is TcxMemo ) ) ; //***DevEx(by am): Se agrega el memo de DevExpress como componente valido.
end;

procedure TdmBaseGlobal.CargaControles( Parametros: TZetaParams; Forma: TCustomForm );
var
   i, j, iCodigo: Integer;
   Parametro: TParam;
   Componente: TComponent;

procedure CargaControl;
begin
     if ( Componente is TCheckBox ) then
        case Parametro.DataType of
             ftBoolean: TCheckBox( Componente ).Checked := Parametro.AsBoolean;
        end
     else
     if ( Componente is TZetaKeyCombo ) then
        case Parametro.DataType of
             ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: TZetaKeyCombo( Componente ).Llave := Parametro.AsString;
             ftSmallint, ftInteger, ftWord:
             begin
                  with TZetaKeyCombo( Componente ) do
                  begin
                       if ( EsconderVacios ) then
                       begin
                            LlaveEntero:= Parametro.AsInteger;
                       end
                       else
                           Valor := Parametro.AsInteger;
                  end;
             end;
        end
     else
     if ( Componente is TComboBox ) then
        case Parametro.DataType of
             ftSmallint, ftInteger, ftWord: TComboBox( Componente ).ItemIndex := Parametro.AsInteger;
        end
     else
     if ( Componente is TZetaKeyLookup) or (Componente is TZetaKeyLookup_DevEx) then
        if(componente is TzetaKeylookup) then
            case Parametro.DataType of
                ftSmallint, ftInteger, ftWord: TZetaKeyLookup( Componente ).Valor := Parametro.AsInteger;
                ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: TZetaKeyLookup( Componente ).Llave := Parametro.AsString;
            end
        else
            case Parametro.DataType of
                ftSmallint, ftInteger, ftWord: TZetaKeyLookup_DevEx( Componente ).Valor := Parametro.AsInteger;
                ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: TZetaKeyLookup_DevEx( Componente ).Llave := Parametro.AsString;
            end
     else
     if ( Componente is TZetaFecha ) then
        case Parametro.DataType of
             ftDateTime: TZetaFecha( Componente ).Valor := Parametro.AsDateTime;
        end
     else
     if ( Componente is TZetaHora ) then
        case Parametro.DataType of
             ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: TZetaHora( Componente ).Valor := Parametro.AsString;
        end
     else
     if ( Componente is TZetaNumero ) then
        case Parametro.DataType of
             ftSmallint, ftInteger, ftWord: TZetaNumero( Componente ).Valor := Parametro.AsInteger;
             ftFloat: TZetaNumero( Componente ).Valor := Parametro.AsFloat;
        end
     else
     if ( Componente is TCustomEdit ) then
        case Parametro.DataType of
             ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: TCustomEdit( Componente ).Text := Parametro.AsString;
             ftSmallint, ftInteger, ftWord: TCustomEdit( Componente ).Text := IntToStr( Parametro.AsInteger );
             ftFloat: TCustomEdit( Componente ).Text := FloatToStr( Parametro.AsFloat );
        end
     else
     {***DevEx(by am): Se agrega especificamente la clase de Memo pues no sabes que tantos componentes hereden de TcxCustomEdit para el arbol de clases de DevExpress
                       Si se necesitara utilizar algun otro componente que necesite ser cargado agregarlo a la condicion de la misma forma.***}
     if (Componente is TcxCustomMemo) then
        case Parametro.DataType of
             ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: (Componente as TcxMemo).Text := Parametro.AsString;
             ftSmallint, ftInteger, ftWord: (Componente as TcxMemo).Text := IntToStr( Parametro.AsInteger );
             ftFloat: (Componente as TcxMemo).Text := FloatToStr( Parametro.AsFloat );
        end
     else
     if ( Componente is TRadioButton ) then
        case Parametro.DataType of
             ftBoolean: TRadioButton( Componente ).Checked := Parametro.AsBoolean;
        end
     else
     if ( Componente is TRadioGroup ) then
         case Parametro.DataType of
              ftSmallint, ftInteger, ftWord: TRadioGroup( Componente ).ItemIndex := Parametro.AsInteger;
     end;

end;

begin
     with Forma do
     begin
          with Parametros do
          begin
               Clear;
               for i := 0 to ( ComponentCount - 1 ) do
               begin
                    if ComponenteValido( Components[ i ] ) then
                    begin
                         CreateParam( ftUnknown, IntToStr( Components[ i ].Tag ), ptInputOutput );
                    end;
               end;
          end;
     end;
     Cargar( Parametros );
     with Forma do
     begin
          with Parametros do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Parametro := Items[ i ];
                    iCodigo := StrAsInteger( Parametro.Name );
                    for j := 0 to ( ComponentCount - 1 ) do
                    begin
                         Componente := Components[ j ];
                         if ( Componente.Tag = iCodigo ) and ComponenteValido( Componente ) then
                            CargaControl;
                    end;
               end;
          end;
     end;
end;

procedure TdmBaseGlobal.DescargaControles( Parametros: TZetaParams; Forma: TCustomForm; const lActualizaDiccion : Boolean = FALSE );
var
   i, j, iCodigo: Integer;
   Parametro: TParam;
   Componente: TComponent;

procedure DescargaControl;
begin
     if ( Componente is TCheckBox ) then
        case Parametro.DataType of
             ftBoolean: Parametro.AsBoolean := TCheckBox( Componente ).Checked;
        end
     else
     if ( Componente is TZetaKeyCombo ) then
        case Parametro.DataType of
             ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: Parametro.AsString := TZetaKeyCombo( Componente ).Llave;
             ftSmallint, ftInteger, ftWord:
             begin
                  with TZetaKeyCombo( Componente ) do
                  begin
                       if ( EsconderVacios ) then
                       begin
                            Parametro.AsInteger := LlaveEntero;
                       end
                       else
                           Parametro.AsInteger := Valor;
                  end;
             end;

        end
     else
     if ( Componente is TComboBox ) then
        case Parametro.DataType of
             ftSmallint, ftInteger, ftWord: Parametro.AsInteger := TComboBox( Componente ).ItemIndex;
        end
     else
     if ( Componente is TZetaKeyLookup ) or (Componente is TzetaKeyLookup_DevEx ) then
        if ( Componente is TZetaKeyLookup ) then
            case Parametro.DataType of
                ftSmallint, ftInteger, ftWord: Parametro.AsInteger := TZetaKeyLookup( Componente ).Valor;
                ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: Parametro.AsString := TZetaKeyLookup( Componente ).Llave;
            end
        else
            case Parametro.DataType of
                ftSmallint, ftInteger, ftWord: Parametro.AsInteger := TZetaKeyLookup_DevEx( Componente ).Valor;
                ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: Parametro.AsString := TZetaKeyLookup_DevEx( Componente ).Llave;
            end
     else
     if ( Componente is TZetaFecha ) then
        case Parametro.DataType of
             ftDateTime: Parametro.AsDateTime := TZetaFecha( Componente ).Valor;
        end
     else
     if ( Componente is TZetaHora ) then
        case Parametro.DataType of
             ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: Parametro.AsString := TZetaHora( Componente ).Valor;
        end
     else
     if ( Componente is TZetaNumero ) then
        case Parametro.DataType of
             ftSmallint, ftInteger, ftWord: Parametro.AsInteger := TZetaNumero( Componente ).ValorEntero;
             ftFloat: Parametro.AsFloat := TZetaNumero( Componente ).Valor;
        end
     else
     if ( Componente is TCustomEdit ) then
        case Parametro.DataType of
             ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: Parametro.AsString := TCustomEdit( Componente ).Text;
             ftSmallint, ftInteger, ftWord: Parametro.AsInteger := StrAsInteger( TCustomEdit( Componente ).Text );
             ftFloat: Parametro.AsFloat := StrToReal( TCustomEdit( Componente ).Text );
        end
     //DevEx
     else
     if (Componente is TcxCustomMemo) then
        case Parametro.DataType of
             ftString {$IFDEF TRESS_DELPHIXE5_UP},ftWideString{$ENDIF}: Parametro.AsString := (Componente as TcxMemo).Text;
             ftSmallint, ftInteger, ftWord: Parametro.AsInteger :=  StrAsInteger( (Componente as TcxMemo).Text );
             ftFloat: Parametro.AsFloat := StrToReal( (Componente as TcxMemo).Text );
        end
     else
     if ( Componente is TRadioButton ) then
        case Parametro.DataType of
             ftBoolean: Parametro.AsBoolean := TRadioButton( Componente ).Checked;
        end
     else
     if ( Componente is TRadioGroup ) then
         case Parametro.DataType of
              ftSmallint, ftInteger, ftWord: Parametro.AsInteger:= TRadioGroup( Componente ).ItemIndex;
     end;
end;

begin
     with Forma do
     begin
          with Parametros do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Parametro := Items[ i ];
                    iCodigo := StrAsInteger( Parametro.Name );
                    for j := 0 to ( ComponentCount - 1 ) do
                    begin
                         Componente := Components[ j ];
                         if ( Componente.Tag = iCodigo ) and ComponenteValido( Componente ) then
                            DescargaControl;
                    end;
               end;
          end;
     end;
     Descargar( Parametros, lActualizaDiccion );
end;

{ *********************** Globales especializados TRESS **********************}

function TdmBaseGlobal.HayAdicionales: Boolean;
var
   iCodigo: Integer;
begin
     Result := FALSE;
     iCodigo := K_GLOBAL_PRIMER_ADICIONAL;
     while ( not Result ) and ( iCodigo <= K_GLOBAL_LAST_ADICIONAL ) do
     begin
          Result := Length( GetGlobalString( iCodigo )) > 0;
          Inc( iCodigo );
     end;
end;

function TdmBaseGlobal.AdFecha( const Index: Integer ): String;
begin
     Result := GetGlobalString( K_GLOBAL_FECHA_BASE + Index );
end;

function TdmBaseGlobal.AdLogico(const Index: Integer): String;
begin
     Result := GetGlobalString( K_GLOBAL_LOG_BASE + Index );
end;

function TdmBaseGlobal.AdNumero(const Index: Integer): String;
begin
     Result := GetGlobalString( K_GLOBAL_NUM_BASE + Index );
end;

function TdmBaseGlobal.AdTabla(const Index: Integer): String;
begin
     Result := Trim( GetGlobalString( K_GLOBAL_TAB_BASE + Index ) );
end;

function TdmBaseGlobal.AdTexto(const Index: Integer): String;
begin
     Result := GetGlobalString( K_GLOBAL_TEXTO_BASE + Index );
end;

{$ifdef TEST_pe}
function TdmBaseGlobal.OK_ProyectoEspecial( const eProgEspecial: eProyectoEspecial): Boolean;

 procedure Test( const sGlobal: string);
 var
     i: eProyectoEspecial;
     oLista: TStrings;

 begin
      {$ifdef TEST_pe}
       oLista := TStringList.Create;
       for i:= low(eProyectoEspecial) to High(eProyectoEspecial) do
       begin
            if ZetaCommonTools.ProgramacionEspecial( i ,sGlobal ) then
               oLista.Add( Format( '%s - %d [CON DERECHOS]' , [ TypInfo.GetEnumName( Typeinfo(eProyectoespecial), Ord(i) ), Ord(i)] ) )
            else
               oLista.Add( Format( '%s - %d [SIN DERECHOS]' , [ TypInfo.GetEnumName( Typeinfo(eProyectoespecial), Ord(i) ), Ord(i)] ) )
       end;
       oLista.SaveTofile('d:\temp\GlobalPE.txt');
       oLista.Free;
      {$endif}
 end;
 var
    sGlobal: string;

begin
     {$ifdef TEST_pe}
     Conectar;
     {$endif}
     sGlobal := GetGlobalString(K_GLOBAL_PROYECTOS_ESPECIALES);
     {$ifdef TEST_pe}
     Test(sGlobal);
     {$endif}

     Result := ZetaCommonTools.ProgramacionEspecial( eProgEspecial, sGlobal )
               {and dmCliente.OK_ProyectoEspecial( eProgEspecial ) <-- A futuro se valida tambien contra el sentinel}
               ;
end;
{$else}
function TdmBaseGlobal.OK_ProyectoEspecial( const eProgEspecial: eProyectoEspecial): Boolean;
begin
     {$ifdef TRESS}
     Result := ZetaCommonTools.ProgramacionEspecial( eProgEspecial, GetGlobalString(K_GLOBAL_PROYECTOS_ESPECIALES) )
               {and dmCliente.OK_ProyectoEspecial( eProgEspecial ) <-- A futuro se valida tambien contra el sentinel}
               ;
     {$else}
     Result := FALSE;
     {$endif}
end;
{$endif}

end.
