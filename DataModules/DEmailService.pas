unit DEmailService;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IniFiles, Registry,
  ZetaCommonLists, ZetaMessages, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP, IdMessage, IdEmailAddress,
  IdExplicitTLSClientServerBase, IdSMTPBase,
  IdAttachmentFile, IdAttachment;

type

  TdmEmailService = class(TDataModule)
    IdEmail: TIdSMTP;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure EmailRecipientNotFound(Recipient: String);
  private
    { Private declarations }
    FIniFile: TIniFile;
    FRegistry : TRegistry;
    FLista : TStrings;
    FSubType: eEmailType;
    FPuerto: integer;
    FTimeOut: integer;
    FMailServer: string;
    FReplyTo: string;
    FUser: string;
    FPassWord: string;
    FSubject: string;
    FFromName: string;
    FFromAddress: string;
    FMessageText: TStrings;
    FAttachments: TStrings;
    FToCarbonCopy: TStrings;
    FToBlindCarbonCopy: TStrings;
    FToAddress: TStrings;
    FErrorMsg: string;
    FAuthMethod : eAuthTressEmail;
    function GetFileName( sFileName: string ): string;
    function ConectMailServer(var sErrorMsg: string): Boolean;
    procedure WriteAddressSection( const sSeccion, sSubSeccion : string; oAddresses : TStrings );
    procedure ReadAddressSection( const sSeccion: string; oAddress: TStrings );
    procedure ReadAddressKey(const sKey, sSubKey: string; oAddress: TStrings);
    procedure WriteAddressKey(const sKey, sSubKey: string; oAddress: TStrings);
  public
    { Public declarations }
    property MailServer: string read FMailServer write FMailServer;
    property Port: integer read FPuerto write FPuerto default 25;
    property SubType: eEmailType read FSubType write FSubType;
    property User: string read FUser write FUser;
    property Password: string read FPassWord write FPassWord;
    property FromAddress: string read FFromAddress write FFromAddress;
    property FromName: string read FFromName write FFromName;
    property ToAddress: TStrings read FToAddress;
    property ToCarbonCopy: TStrings read FToCarbonCopy;
    property ToBlindCarbonCopy: TStrings read FToBlindCarbonCopy;
    property ReplyTo: string read FReplyTo write FReplyTo;
    property Subject: string read FSubject write FSubject;
    property MessageText: TStrings read FMessageText write FMessageText;
    property Attachments: TStrings read FAttachments;
    property TimeOut : integer read FTimeOut write FTimeOut;
    property ErrorMsg : string read FErrorMsg write FErrorMsg;
    property AuthMethod : eAuthTressEmail read FAuthMethod write FAuthMethod;
    function WriteToIniFile( const sFileName : string ) : Boolean;
    function ReadFromIniFile( const sFileName : string ) : Boolean;

    function WriteToRegistry( const sKeyName : string ) : Boolean;
    function ReadFromRegistry( const sKeyName : string ) : Boolean;

    {$ifdef KIOSCO2}
    function SendEmail( var sErrorMsg : string; const sArchivo: String ): Boolean;
    {$else}
    function SendEmail( var sErrorMsg : string ): Boolean;
    {$endif}

    function VerifyEmailAddresses( var sErrorMsg: string ): Boolean;
    function Validate( var sErrorMsg: string ): Boolean;
    procedure NewEmail;
    procedure NewBodyMessage;

  end;

 {$ifdef KIOSCO2}
   TSendEmailThread = class(TThread)
  private
    FArchivo: String;
    FEmail : TIdSMTP;
    FAttachList : TStrings;
    FPostMessage : TIdMessage;
    FHandle: HWND;
    FEmailService: TdmEmailService;
    procedure TerminoExito;
    procedure TerminoFalla;
    procedure BorraArchivos;
  protected
    procedure Execute; override;
  public
    constructor Create( oEmail: TIdSMTP; const sArchivo: String );
    destructor Destroy; override;
    property EmailService: TdmEmailService read FEmailService write FEmailService;
    property AttachList : TStrings read FAttachList write FAttachList;
    property PostMessage : TIdMessage read FPostMessage write FPostMessage;

  end;
  {$endif}

var
  dmEmailService: TdmEmailService;

implementation
uses ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

const
     K_SECCION = 'EmailParameters';
     K_SECCION_TO_ADDRESS = 'EmailToAddress';
     K_SECCION_CARBON_ADDRESS = 'EmailCarbonAddress';
     K_SECCION_BLIND_ADDRESS = 'EmailBlindAddress';


     K_TO_ADDRESS = 'ToAddress%d';
     K_CARBON_ADDRESS = 'CarbonAdresss%d';
     K_BLIND_ADDRESS = 'BlindAdress%d';

     K_MAILSERVER = 'Host';
     K_PORT = 'Port';
     K_SUBTYPE = 'Type';
     K_USER = 'UserId';
     K_PASSWORD = 'PassWord';
     K_FROMADDRESS = 'Remitente';
     K_FROMNAME = 'FromName';
     K_REPLYTO = 'ReplyTo';
     K_TIMEOUT = 'TimeOut';
     K_AUTH = 'Auth';

     K_DEF_MAILSERVER = '255.255.255.255';
     K_DEF_PORT = '25';
     K_DEF_SUBTYPE = '0';
     K_DEF_USER = 'Usuario';
     K_DEF_PASSWORD = 'PassWord';
     K_DEF_FROMADDRESS = 'usuario@dominio.com';
     K_DEF_FROMNAME = 'Nombre del Usuario';
     K_DEF_REPLYTO = 'usuario@dominio.com';
     K_DEF_TIMEOUT = '0';
     K_DEF_AUTH = '0';
     K_CORREO_INVALIDO = 'Direcci�n de Correo no encontrada o inv�lida: ';
     K_CORREOS_INVALIDO = 'Direcci�n(es) de Correo(s) no encontrada(s) o inv�lida(s): ';

     H_KEY_SOFTWARE = 'Software\Grupo Tress\TressWin\';


function TdmEmailService.GetFileName( sFileName: string ) : string;
begin
     if StrVacio( ExtractFileDir( sFileName ) ) then
        Result := VerificaDir( ExtractFileDir( Application.ExeName ) ) + sFileName
     else Result := sFileName;
end;

procedure TdmEmailService.ReadAddressSection( const sSeccion: string; oAddress: TStrings );
 var i : integer;
begin
     with FLista do
     begin
          Clear;
          FIniFile.ReadSectionValues( sSeccion, FLista );
          for i:= 0 to Count - 1 do
              oAddress.Add( Values[ Names[ i ] ] );
     end;
end;

procedure TdmEmailService.WriteAddressSection( const sSeccion, sSubSeccion : string; oAddresses : TStrings );
 var
    i: integer;
begin
     with FIniFile do
     begin
          EraseSection( sSeccion );
          for i:= 0 to oAddresses.Count - 1 do
              WriteString( sSeccion, Format( sSubSeccion, [ i ] ), oAddresses[ i ] );
     end;
end;


function TdmEmailService.ReadFromIniFile( const sFileName : string ) : Boolean;
begin
     NewEmail;

     FIniFile := TIniFile.Create( GetFileName( sFileName ) );
     try
        with FIniFile do
        begin
             MailServer  :=  ReadString( K_SECCION, K_MAILSERVER, K_DEF_MAILSERVER );
             Port        :=  StrToInt( ReadString( K_SECCION, K_PORT, K_DEF_PORT ) );
             SubType     :=  eEmailType( StrToInt( ReadString( K_SECCION, K_SUBTYPE, K_DEF_SUBTYPE ) ) );
             User        :=  ReadString( K_SECCION, K_USER, K_DEF_USER );
             PassWord    :=  ReadString( K_SECCION, K_PASSWORD, K_DEF_PASSWORD );
             FromAddress :=  ReadString( K_SECCION, K_FROMADDRESS, K_DEF_FROMADDRESS );
             FromName    :=  ReadString( K_SECCION, K_FROMNAME, K_DEF_FROMNAME );
             ReplyTo     :=  ReadString( K_SECCION, K_REPLYTO, K_DEF_REPLYTO );
             TimeOut     :=  StrToInt( ReadString( K_SECCION, K_TIMEOUT, K_DEF_TIMEOUT ) );
             AuthMethod   :=  eAuthTressEmail( StrToInt( ReadString( K_SECCION, K_AUTH, K_DEF_AUTH ) ) );

             ReadAddressSection( K_SECCION_TO_ADDRESS, FToAddress );
             ReadAddressSection( K_SECCION_CARBON_ADDRESS, FToCarbonCopy );
             ReadAddressSection( K_SECCION_BLIND_ADDRESS, FToBlindCarbonCopy );

        end;
        Result := TRUE;
     finally
            FreeAndNil( FIniFile );
     end;
end;

function TdmEmailService.WriteToIniFile( const sFileName : string ) : Boolean;
begin
     FIniFile := TIniFile.Create( GetFileName( sFileName ) );
     try
        with FIniFile do
        begin
             WriteString( K_SECCION, K_MAILSERVER, MailServer );
             WriteString( K_SECCION, K_PORT, IntToStr( Port ) );
             WriteString( K_SECCION, K_SUBTYPE, IntToStr( Ord( SubType ) ) );
             WriteString( K_SECCION, K_USER, User );
             WriteString( K_SECCION, K_PASSWORD, PassWord );
             WriteString( K_SECCION, K_FROMADDRESS, FromAddress );
             WriteString( K_SECCION, K_FROMNAME, FromName );
             WriteString( K_SECCION, K_REPLYTO, ReplyTo );
             WriteString( K_SECCION, K_TIMEOUT, IntToStr( TimeOut ) );
             WriteString( K_SECCION, K_AUTH, IntToStr( Ord( AuthMethod ) ) );

             WriteAddressSection( K_SECCION_TO_ADDRESS, K_TO_ADDRESS, FToAddress );
             WriteAddressSection( K_SECCION_CARBON_ADDRESS, K_CARBON_ADDRESS, FToCarbonCopy );
             WriteAddressSection( K_SECCION_BLIND_ADDRESS, K_BLIND_ADDRESS, FToBlindCarbonCopy );

        end;
        Result := TRUE;
     finally
            FreeAndNil( FIniFile );
     end;
end;

procedure TdmEmailService.WriteAddressKey( const sKey, sSubKey: string; oAddress : TStrings );
 var
    i: integer;
begin
     with FRegistry do
     begin
          if OpenKey( sKey, TRUE ) then
          begin
               for i:=0 to oAddress.Count - 1 do
                   WriteString( Format( sSubKey,[i] ), oAddress[ i ] );
          end;
          CloseKey;
     end;
end;

procedure TdmEmailService.ReadAddressKey( const sKey, sSubKey : string; oAddress : TStrings );
 var
    oKey: TRegKeyInfo;
    i : integer;
begin
     with FRegistry do
     begin
          if OpenKey( sKey, FALSE ) and GetKeyInfo( oKey ) then
          begin
               for i:= 0 to oKey.NumValues - 1 do
               begin
                    oAddress.Add( ReadString( Format(sSubKey, [i] )));
               end;
               CloseKey;
          end;
     end;
end;

function TdmEmailService.WriteToRegistry( const sKeyName : string ) : Boolean;
 var sKey : string;
begin
     sKey := H_KEY_SOFTWARE + sKeyName;
     
     if FRegistry = NIL then
        FRegistry := TRegistry.Create;

     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;

          Result := OpenKey( sKey, TRUE );
          if Result then
          begin
               WriteString( K_MAILSERVER, MailServer );
               WriteInteger( K_PORT, Port );
               Writeinteger( K_SUBTYPE, Ord( SubType ) );
               WriteString( K_USER, User );
               WriteString( K_PASSWORD, PassWord );
               WriteString( K_FROMADDRESS, FromAddress );
               WriteString( K_FROMNAME, FromName );
               WriteString( K_REPLYTO, ReplyTo );
               WriteInteger( K_TIMEOUT, TimeOut );
               WriteInteger( K_AUTH, Ord( AuthMethod ) );               
               CloseKey;

               sKey := sKey + '\';
               WriteAddressKey( sKey + K_SECCION_TO_ADDRESS, K_TO_ADDRESS, FToAddress );
               WriteAddressKey( sKey + K_SECCION_CARBON_ADDRESS, K_CARBON_ADDRESS, FToCarbonCopy );
               WriteAddressKey( sKey + K_SECCION_BLIND_ADDRESS, K_BLIND_ADDRESS, FToBlindCarbonCopy );
               CloseKey;
          end;
     end;
end;

function TdmEmailService.ReadFromRegistry( const sKeyName : string ) : Boolean;
 var sKey : string;
begin
     NewEMail;
     if FRegistry = NIL then
        FRegistry := TRegistry.Create;

     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;

          sKey := H_KEY_SOFTWARE + sKeyName;
          Result := OpenKey( sKey, FALSE );

          if Result then
          begin
               MailServer  := ReadString( K_MAILSERVER );
               Port        := ReadInteger( K_PORT );
               SubType     := eEmailType( ReadInteger( K_SUBTYPE ) );
               User        := ReadString( K_USER );
               PassWord    := ReadString( K_PASSWORD );
               FromAddress := ReadString( K_FROMADDRESS );
               FromName    := ReadString( K_FROMNAME );
               ReplyTo     := ReadString( K_REPLYTO );
               TimeOut     := ReadInteger( K_TIMEOUT );
               AuthMethod  := eAuthTressEmail( ReadInteger( K_AUTH ) );
               CloseKey;

               sKey := sKey + '\';
               ReadAddressKey( sKey + K_SECCION_TO_ADDRESS, K_TO_ADDRESS, FToAddress );
               ReadAddressKey( sKey + K_SECCION_CARBON_ADDRESS, K_CARBON_ADDRESS, FToCarbonCopy );
               ReadAddressKey( sKey + K_SECCION_BLIND_ADDRESS, K_BLIND_ADDRESS, FToBlindCarbonCopy );

          end;
          CloseKey;
     end;
end;

function TdmEmailService.ConectMailServer( var sErrorMsg : string ): Boolean;
begin
     Result := FALSE;

     with IdEmail do
     begin
          if Connected then
             DisConnect;

          Host := FMailServer;
          Username:= FUser;
          Port := FPuerto;
          Password := FPassWord;

             if AuthMethod = authLogin then
                  AuthType := satDefault // �satDefault = atLogin?
               else
                   AuthType := satNone;

          try
             Connect;
             Result := Connected;
          except
                On E:Exception do
                   sErrorMsg := E.Message;
          end;
     end;
end;

{$ifdef KIOSCO2}
//Metodo que se utiliza en el Kiosco.
function TdmEmailService.SendEmail( var sErrorMsg : string; const sArchivo: String ): Boolean;
var
   ProcesoEmail: TSendEmailThread;
   idtTextPart:TIdMessage;
   IdPostMessage : TIdMessage;
   i : integer;

  procedure AgregarBodyHTML;
  begin
      if FAttachments.Count > 0 then
      begin
         idtTextPart:=TIdMessage.Create(nil);
         idtTextPart.ContentType:='text/plain';
         idtTextPart.Body.Add('');

         idtTextPart:= TIdMessage.Create(nil);
         idtTextPart.ContentType := 'text/html';
         idtTextPart.Body := FMessageText;
      end
      else
      begin
        IdPostMessage.Body := FMessageText;
      end;
  end;

  procedure AgregarBodyTextPlain;
  begin
       IdPostMessage.Body :=  FMessageText;
  end;

begin
     Result := FALSE;

 with IdEmail do
     begin
          if not ConectMailServer( sErrorMsg ) then
          begin
               Host := FMailServer;
               Username := FUser;
               Password := FPassWord;
               Port := FPuerto;
               if AuthMethod = authLogin then
                  AuthType := satDefault
               else
                   AuthType := satNone;

               try
                  Connect;
               except
                     On E:Exception do
                        sErrorMsg := E.Message;
               end;
          end;

          if Connected then
          begin
               IdPostMessage  := TIdMessage.Create(Self);
               IdPostMessage.Clear;
               IdPostMessage.ClearBody;
               IdPostMessage.ClearHeader;
               IdPostMessage.Encoding := meMIME;

               case FSubType of
                    emtTexto: IdPostMessage.ContentType := 'text/plain';
                    emtHTML:
                    begin
                     if FAttachments.Count > 0 then
                        IdPostMessage.ContentType := 'Multipart/Alternative'
                     else
                        IdPostMessage.ContentType := 'text/html';
                    end;
               end;

               IdPostMessage.Recipients.Clear;
               IdPostMessage.CCList.Clear;
               IdPostMessage.BccList.Clear;
               IdPostMessage.Date := 0;
               IdPostMessage.From.Address := FFromAddress ;
               IdPostMessage.From.Name := FFromName;
               IdPostMessage.ReplyTo.EMailAddresses := FReplyTo;

               for i:=0 to FToAddress.Count-1 do
                 IdPostMessage.Recipients.Add.Address := FToAddress[i];

               for i:=0 to FToBlindCarbonCopy.Count-1 do
               begin
                   IdPostMessage.CCList.Add.Address := FToBlindCarbonCopy[i];
                   IdPostMessage.BccList.Add.Address := FToBlindCarbonCopy[i];
               end;

               IdPostMessage.Subject := FSubject;

               case FSubType of
                    emtTexto: AgregarBodyTextPlain;
                    emtHTML: AgregarBodyHTML;
               end;

               //IdPostMessage.Attachments.AddStrings( FAttachments );
               ProcesoEmail := TSendEmailThread.Create( IdEmail, sArchivo );
               ProcesoEmail.PostMessage := IdPostMessage;
               ProcesoEmail.AttachList := FAttachments;

               with ProcesoEmail do
               begin
                    EmailService := self;
                    Resume;
               end;
               Result := TRUE;
          end;
     end;
end;

{$ELSE}

function TdmEmailService.SendEmail( var sErrorMsg : string ): Boolean;
var
  idtTextPart:TIdMessage;
  IdPostMessage : TIdMessage;
  idtAttach : TIdAttachment;
  idtEmail : TIdEMailAddressItem;
  i : integer;

  procedure AgregarBodyHTML;
  begin
      if FAttachments.Count > 0 then
      begin
         idtTextPart:=TIdMessage.Create (nil);
         idtTextPart.ContentType:='text/plain';
         idtTextPart.Body.Add('');

         idtTextPart:= TIdMessage.Create (nil);
         idtTextPart.ContentType := 'text/html';
         idtTextPart.Body := FMessageText;
      end
      else
      begin
        IdPostMessage.Body := FMessageText;
      end;
  end;

  procedure AgregarBodyTextPlain;
  begin
       IdPostMessage.Body :=  FMessageText;
  end;

  procedure AgregarAttachments;
  var
     iAttach : integer;
  begin
       for iAttach := 0  to FAttachments.Count - 1 do
       begin
            if FileExists(FAttachments[iAttach]) then
               idtAttach := TIdAttachmentFile.Create(IdPostMessage.MessageParts, FAttachments[iAttach]);
       end;
  end;

begin
     FErrorMsg:= VACIO;
     Result := FALSE;

     with IdEmail do
     begin
          if not ConectMailServer( sErrorMsg ) then
          begin
               Host := FMailServer;
               Username := FUser;
               Password := FPassWord;
               Port := FPuerto;
               
                if AuthMethod = authLogin then
                  AuthType := satDefault
               else
                   AuthType := satNone;

               try
                  Connect;
               except
                     On E:Exception do
                        sErrorMsg := E.Message;
               end;
          end;

          if Connected then
          begin
               IdPostMessage  := TIdMessage.Create(Self);
               IdPostMessage.Clear;
               IdPostMessage.ClearBody;
               IdPostMessage.ClearHeader;
               IdPostMessage.Encoding := meMIME;

               case FSubType of
                    // emtTexto: IdPostMessage.ContentType := 'text/plain';
                    emtHTML:
                    begin
                     if FAttachments.Count > 0 then
                        IdPostMessage.ContentType := 'Multipart/Alternative'
                     else
                        IdPostMessage.ContentType := 'text/html';
                    end;
                    emtTextoError:
                    begin
                        IdPostMessage.ContentType := 'text/plain';
                        IdPostMessage.Charset := 'UTF-8';
                    end;
               end;

               IdPostMessage.Recipients.Clear;
               IdPostMessage.CCList.Clear;
               IdPostMessage.BccList.Clear;
               IdPostMessage.Date := 0;
               IdPostMessage.From.Address := FFromAddress ;
               IdPostMessage.From.Name := FFromName;
               IdPostMessage.ReplyTo.EMailAddresses := FReplyTo;

               //IdEmailAddress.TIdEMailAddressItem.
               for i:=0 to FToAddress.Count-1 do
               begin
                 if StrLLeno( FToAddress[i] ) then
                 begin
                      idtEmail := IdPostMessage.Recipients.Add;
                      idtEmail.Address := FToAddress[i];
                 end;

               end;

               if IdPostMessage.Recipients.Count > 0 then
               begin
                  for i:=0 to FToBlindCarbonCopy.Count-1 do
                  begin
                      IdPostMessage.CCList.Add.Address := FToBlindCarbonCopy[i];
                      IdPostMessage.BccList.Add.Address := FToBlindCarbonCopy[i];
                  end;

                  IdPostMessage.Subject := FSubject;

                  case FSubType of
                       emtTexto: AgregarBodyTextPlain;
                       emtHTML: AgregarBodyHTML;
                       emtTextoError: AgregarBodyTextPlain;
                  end;

                  AgregarAttachments;

                  try
                     Send( IdPostMessage );
                     Result := TRUE;
                  except
                        On E: Exception do
                        begin
                             if StrLleno(FErrorMsg) then
                                sErrorMsg := FErrorMsg
                             else
                                 sErrorMsg := E.Message;

                        end;
                  end;
               end;
          end;
     end;
end;

{$ENDIF}

function TdmEmailService.VerifyEmailAddresses( var sErrorMsg: string ): Boolean;
begin
     Result := TRUE;
end;

function TdmEmailService.Validate( var sErrorMsg: string ): Boolean;
begin
     Result := TRUE;
end;

procedure TdmEmailService.NewBodyMessage;
begin
     FToAddress.Clear;
     FToCarbonCopy.Clear;
     FToBlindCarbonCopy.Clear;

     Subject := VACIO;
     FMessageText.Clear;
     FAttachments.Clear;
end;

procedure TdmEmailService.NewEmail;
begin
     MailServer   := VACIO;
     Port         := 25;
     SubType      := emtTexto;
     User         := VACIO;
     PassWord     := VACIO;
     FromAddress  := VACIO;
     FromName     := VACIO;
     ReplyTo      := VACIO;
     TimeOut      := 0;
     AuthMethod   := authSinLogin;

     NewBodyMessage;
end;

procedure TdmEmailService.DataModuleCreate(Sender: TObject);
begin
     FMessageText := TStringList.Create;
     FToAddress := TStringList.Create;
     FToCarbonCopy := TStringList.Create;
     FToBlindCarbonCopy := TStringList.Create;
     FAttachments := TStringList.Create;
     FLista := TStringList.Create;
end;

procedure TdmEmailService.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FLista );
     FreeAndNil( FMessageText );
     FreeAndNil( FToAddress );
     FreeAndNil( FToCarbonCopy );
     FreeAndNil( FToBlindCarbonCopy );
     FreeAndNil( FAttachments );
     if Assigned( FRegistry ) then
        FreeAndNil( FRegistry );

end;

{$ifdef KIOSCO2}
{ TSendEmailThread }

procedure TSendEmailThread.BorraArchivos;
 var
    iPage : integer;
    sExt, sNombre, sArchivoTemp : string;
begin
     if FileExists( FArchivo ) then
     begin
          DeleteFile( FArchivo );
     end
     else
     begin
          iPage := 1;

          sExt := ExtractFileExt( FArchivo );
          sNombre := Copy( FArchivo, 1, Pos( sExt, FArchivo )-1 );
          sArchivoTemp := sNombre + IntToStr( iPage ) + sExt;

          while FileExists( sArchivoTemp ) do
          begin
               DeleteFile( sArchivoTemp );
               Inc( iPage );
               sArchivoTemp := sNombre + IntToStr( iPage ) + sExt;
          end;
     end;
end;

constructor TSendEmailThread.Create( oEmail: TIdSMTP; const sArchivo: String );
begin
     FArchivo:= sArchivo;
     FEmail:= oEmail;
     FreeOnTerminate := True;
     with Application do
     begin
          if Assigned( MainForm ) then
             FHandle := MainForm.Handle
          else
              FHandle := 0;
     end;
     inherited Create( True );
end;

procedure TSendEmailThread.Execute;

  procedure AgregarAttachments;
  var
     iAttach : integer;
     idtAttach : TIdAttachment;
  begin
       for iAttach := 0  to FAttachList.Count - 1 do
       begin
            if FileExists(FAttachList[iAttach]) then
               idtAttach := TIdAttachment.Create(FPostMessage.MessageParts)
            else

       end;
  end;

begin
        AgregarAttachments;

        try
           FEmail.Send( Self.FPostMessage );
           if StrLleno(EmailService.ErrorMsg) then
           begin
              EmailService.ErrorMsg := K_CORREOS_INVALIDO + EmailService.ErrorMsg;
              TerminoFalla;
           end
           else
               TerminoExito;
        except
              On E: Exception do
              begin
                    EmailService.ErrorMsg := E.Message;
                    TerminoFalla;
              end;
        end;

        BorraArchivos;
end;

procedure TSendEmailThread.TerminoExito;
begin
     if ( FHandle > 0 ) then
     begin
          Windows.PostMessage( FHandle, WM_WIZARD_END, 1, 0 );

     end;
end;

procedure TSendEmailThread.TerminoFalla;
begin
     if ( FHandle > 0 ) then
          Windows.PostMessage( FHandle, WM_WIZARD_ERROR, 1, 0 );
end;

destructor TSendEmailThread.Destroy;
begin
     inherited;
end;

{$endif}


procedure TdmEmailService.EmailRecipientNotFound(Recipient: String);
begin
     //Se queda con el ifdef en caso de que por error llegue a este codigo....
     {$ifndef KIOSCO2}
     FErrorMsg := K_CORREO_INVALIDO + Recipient;
     {$endif}
end;



end.
