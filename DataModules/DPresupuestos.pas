unit DPresupuestos;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  SysUtils, Classes, DB, DBClient, Controls,forms,
  {$ifndef VER130}Variants,{$endif}
{$IFDEF DOS_CAPAS}
     DServerPresupuestos,
     DServerRecursos,
     DServerNomina,
     DServerAnualNomina,
     DServerCalcNomina,
     DServerConsultas,
{$else}
     Consultas_TLB,
     DCalcNomina_TLB,
     DAnualNomina_TLB,
     Nomina_TLB,
     Recursos_TLB,
     Presupuestos_TLB,
{$endif}
     ZetaClientDataSet, ZetaCommonClasses, ZetaCommonLists, ZetaTipoEntidad,
  ZetaServerDataSet;

type
  TCallBackStart = procedure( Sender: TObject; const sMsg: String; const iMax: Integer ) of object;
  TCallBack = procedure( Sender: TObject; const sMsg: String; const iStep: Integer; var Continue: Boolean ) of object;
  TCallBackEnd = procedure( Sender: TObject; const sMsg: String ) of object;
  TdmPresupuestos = class(TDataModule)
    cdsSupuestosRH: TZetaClientDataSet;
    cdsEventosAltas: TZetaLookupDataSet;
    cdsPeriodos: TZetaClientDataSet;
    cdsContrataciones: TServerDataSet;
    cdsImportarKardex: TServerDataSet;
    cdsLista: TServerDataSet;
    // (JB) Escenarios de presupuestos T1060 CR1872
    cdsEscenarios: TZetaLookupDataSet;    
{$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$endif}
    procedure cdsSupuestosRHAlAdquirirDatos(Sender: TObject);
    procedure cdsSupuestosRHAlCrearCampos(Sender: TObject);
    procedure cdsSupuestosRHCalcFields(DataSet: TDataSet);
    procedure cdsSupuestosRHNewRecord(DataSet: TDataSet);
    procedure cdsSupuestosEV_CODIGOValidate(Sender: TField);
    procedure cdsSupuestosSR_TIPOChange(Sender: TField);
    procedure cdsSupuestosRHBeforePost(DataSet: TDataSet);
    procedure cdsSupuestosRHAlEnviarDatos(Sender: TObject);
{$ifdef VER130}
    procedure cdsSupuestosRHReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
    procedure cdsSupuestosRHReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$endif}
    procedure cdsSupuestosRHAlModificar(Sender: TObject);
    procedure cdsEventosAltasAlAdquirirDatos(Sender: TObject);
    procedure cdsEventosAltasAlModificar(Sender: TObject);
    procedure cdsEventosAltasAlCrearCampos(Sender: TObject);
    procedure cdsEventosAltasCB_CODIGOChange(Sender: TField);
    procedure cdsEventosAltasEV_PUESTOChange(Sender: TField);
    procedure cdsEventosAltasTabuladorChange(Sender: TField);
    procedure cdsEventosAltasNewRecord(DataSet: TDataSet);
    procedure cdsEventosAltasBeforePost(DataSet: TDataSet);
    procedure cdsEventosAltasAlEnviarDatos(Sender: TObject);
    procedure cdsEventosAltasAfterDelete(DataSet: TDataSet);
    procedure cdsPeriodosAlAdquirirDatos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsSupuestosRHAlBorrar(Sender: TObject);
    procedure cdsEventosAltasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    // (JB) Escenarios de presupuestos T1060 CR1872
    procedure cdsEscenariosAlAdquirirDatos(Sender: TObject);
    procedure cdsEscenariosAlBorrar(Sender: TObject);
    procedure cdsEscenariosAlEnviarDatos(Sender: TObject);
    procedure cdsEscenariosAlModificar(Sender: TObject);
    procedure cdsEscenariosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
  private
    { Private declarations }
    FCallBackStart: TCallBackStart;
    FCallBack: TCallBack;
    FCallBackEnd: TCallBackEnd;
    FCounter: Integer;
    FParametros: TZetaParams;
    FSimulando: Boolean;
    FEscenario: String;
    {$ifdef DOS_CAPAS}
    function GetServerPresupuestos: TdmServerPresupuestos;
    function GetServerRecursos : TdmServerRecursos;
    function GetServerNomina: TdmServerNomina;
    function GetServerAnualNomina: TdmServerAnualNomina;
    function GetServerCalcNomina: TdmServerCalcNomina;
    function GetServerConsultas: TdmServerConsultas;
    {$else}
    FServidor: IdmServerPresupuestosDisp;
    FServidorRecursos : IdmServerRecursosDisp;
    FServidorNomina: IdmServerNominaDisp;
    FServidorAnualNomina: IdmServerAnualNominaDisp;
    FServidorCalcNomina: IdmServerCalcNominaDisp;
    FServidorConsultas: IdmServerConsultasDisp;
    function GetServerPresupuestos: IdmServerPresupuestosDisp;
    function GetServerRecursos : IdmServerRecursosDisp ;
    function GetServerNomina: IdmServerNominaDisp;
    function GetServerAnualNomina: IdmServerAnualNominaDisp;
    function GetServerCalcNomina: IdmServerCalcNominaDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    {$endif}
    function OpenProcessSimulacion: Boolean;
    function CloseProcessSimulacion: OleVariant;
    function GetMaxSteps: Integer;
    function CanContinue( const sMsg: String = VACIO; const iStep: Integer = 1 ): Boolean;
    function DoCallBack( const sMsg: String; const iStep: Integer ): Boolean;
    function EsPeriodoACalcular(const PosPeriodo: eTipoPeriodoMes): Boolean;
    function GetNominaDescrip: String;
    function ValidaCodigoSupuesto( const TipoSupuesto: eTipoSupuestoRH; const sCodigo: String ): Boolean;
    procedure CargaParamsWizardFiltros(oParams: TZetaParams);
    procedure AsignaDatosClasificacion(DataSet: TZetaClientDataSet; const sPrefijo: string; const lRevisaCambio: Boolean);
    procedure SetDatosEmpleado(const iEmpleado: Integer);
    procedure SetDatosPuesto(const sPuesto: String);
    procedure SetDatosTabulador(const sClasif: String);
    procedure ConectaDataSets;
    procedure DesconectaDataSets;
    procedure LimpiarNominasAcumulados;
    procedure LimpiarSupuestosRH;
    procedure AplicarSupuestosRH;
    procedure AplicaSupuestoAlta( const dFecha: TDate; const sCodigo: String; const iQtyEmpleados: Integer );
    procedure CrearContratacionesDataSet;
    procedure CrearImportarKardexDataSet;
    procedure AgregarContrataciones(const sObservaciones: String);
    procedure AgregarContratacionesBajas(const sObservaciones: String);
    procedure AplicaSupuestoEvento( const dFecha: TDate; const sCodigo, sDescrip: String; const iQtyEmpleados: Integer );
    procedure CalcularSalarioIntegrado(const iMes: Integer);
    procedure CalcularPromedioVariables(const iMes: Integer);
    procedure CalcularAguinaldo(const oParams: TZetaParams);
    procedure CalcularPTU(const oParams: TZetaParams);
    procedure LiquidacionesGlobales;
    procedure CalcularNomina;
    procedure AfectarNomina;
    procedure CalcularCUBO;
    procedure AsignaPeriodoNomina(cdsDataSet: TServerDataset);
    procedure DepurarEventos(cdsDataSet: TServerDataset; const iQtyEmpleados: integer);
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerPresupuestos: TdmServerPresupuestos read GetServerPresupuestos;
    property ServerRecursos : TdmServerRecursos read GetServerRecursos;
    property ServerNomina: TdmServerNomina read GetServerNomina;
    property ServerAnualNomina: TdmServerAnualNomina read GetServerAnualNomina;
    property ServerCalcNomina: TdmServerCalcNomina  read GetServerCalcNomina;
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
    {$else}
    property ServerPresupuestos: IdmServerPresupuestosDisp read GetServerPresupuestos;
    property ServerRecursos : IdmServerRecursosDisp read GetServerRecursos;
    property ServerNomina: IdmServerNominaDisp read GetServerNomina;
    property ServerAnualNomina: IdmServerAnualNominaDisp  read GetServerAnualNomina;
    property ServerCalcNomina: IdmServerCalcNominaDisp read GetServerCalcNomina;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    {$endif}
  public
    { Public declarations }
    property OnCallBackStart: TCallBackStart read FCallBackStart write FCallBackStart;
    property OnCallBack: TCallBack read FCallBack write FCallBack;
    property OnCallBackEnd: TCallBackEnd read FCallBackEnd write FCallBackEnd;
    property EscenarioActivo: String read FEscenario write FEscenario;

    function SimularPresupuesto( Parametros, ParamsAguinaldo, ParamsPTU: TZetaParams ): Boolean;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure ShowWizardSimulacion;
	
	// (JB) Escenarios de presupuestos T1060 CR1872
    procedure RefrescarSupuestosRH( sEscenario : String );
    procedure CopiaSupuestos(const sLlaveEscenario, sLlaveNuevoEscenario : String );
  end;

const
     K_MENSAJE_PRESUPUESTO = 'Simulaci�n de Presupuesto';
var
  dmPresupuestos: TdmPresupuestos;

implementation

uses dCliente, dSistema, dCatalogos, dRecursos, dGlobal,
     ZetaCommonTools,
     ZetaDataSetTools,
     ZGlobalTress,
     ZBaseEdicion_DevEx,
     ZBaseGridEdicion_DevEx,
     ZetaDialogo,
     ZReconcile,
     ZcxWizardBasico,
     ZetaMsgDlg,
     FGridSupuestosRH_DevEx,
     FEditCatEventosAltas_DevEx,
     FWizPresupSimulacion_DevEx,
     FTressShell,
     ZAccesosMgr,
     ZAccesosTress,
     FGridEditEscenario_DevEx, DateUtils;

{$R *.dfm}

{$ifdef DOS_CAPAS}
function TdmPresupuestos.GetServerPresupuestos: TdmServerPresupuestos;
begin
     Result := DCliente.dmCliente.ServerPresupuestos;
end;

function TdmPresupuestos.GetServerRecursos: TdmServerRecursos;
begin
     Result := DCliente.dmCliente.ServerRecursos;
end;

function TdmPresupuestos.GetServerNomina: TdmServerNomina;
begin
     Result := DCliente.dmCliente.ServerNomina;
end;

function TdmPresupuestos.GetServerAnualNomina: TdmServerAnualNomina;
begin
     Result := DCliente.dmCliente.ServerAnualNomina;
end;

function TdmPresupuestos.GetServerCalcNomina: TdmServerCalcNomina;
begin
     Result := DCliente.dmCliente.ServerCalcNomina;
end;

function TdmPresupuestos.GetServerConsultas: TdmServerConsultas;
begin
     Result := DCliente.dmCliente.ServerConsultas;
end;

{$else}
function TdmPresupuestos.GetServerPresupuestos: IdmServerPresupuestosDisp;
begin
     Result := IdmServerPresupuestosDisp( dmCliente.CreaServidor( CLASS_dmServerPresupuestos, FServidor ) );
end;

function TdmPresupuestos.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidorRecursos ) );
end;

function TdmPresupuestos.GetServerNomina: IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( dmCliente.CreaServidor( CLASS_dmServerNomina, FServidorNomina ) );
end;

function TdmPresupuestos.GetServerAnualNomina: IdmServerAnualNominaDisp;
begin
     Result := IdmServerAnualNominaDisp( dmCliente.CreaServidor( CLASS_dmServerAnualNomina, FServidorAnualNomina ) );
end;

function TdmPresupuestos.GetServerCalcNomina: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( dmCliente.CreaServidor( CLASS_dmServerCalcNomina, FServidorCalcNomina ) );
end;

function TdmPresupuestos.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServidorConsultas ) );
end;

{$endif}

procedure TdmPresupuestos.DataModuleCreate(Sender: TObject);
begin
     Fescenario := VACIO;
     FSimulando := FALSE;
     FParametros:= nil;
     CrearContratacionesDataSet;
     CrearImportarKardexDataSet;
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmPresupuestos.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
begin
     if Dentro( enSupuestosRH, Entidades ) then
        cdsSupuestosRH.SetDataChange;
     if Dentro( enEventoAlta, Entidades ) then
        cdsEventosAltas.SetDataChange;
end;
{$else}
procedure TdmPresupuestos.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
begin
     if ( enSupuestosRH in Entidades ) then
        cdsSupuestosRH.SetDataChange;
     if ( enEventoAlta in Entidades ) then
        cdsEventosAltas.SetDataChange;
end;
{$endif}

{$ifdef VER130}
procedure TdmPresupuestos.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmPresupuestos.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

procedure TdmPresupuestos.CargaParamsWizardFiltros( oParams: TZetaParams );
begin
     with oParams do
     begin
          AddString( 'RangoLista', FParametros.ParamByName( 'RangoLista' ).AsString );   // Equivalente a Todos
          AddString( 'Condicion', FParametros.ParamByName( 'Condicion' ).AsString );
          AddString( 'Filtro', FParametros.ParamByName( 'Filtro' ).AsString );
     end;
     with dmCliente do
     begin
          CargaActivosIMSS( oParams );
          CargaActivosPeriodo( oParams );
          CargaActivosSistema( oParams );
     end;
end;

function TdmPresupuestos.GetNominaDescrip: String;
begin
     with dmCliente.GetDatosPeriodoActivo do
     begin
          Result := ZetaCommonTools.ShowNomina( Year, Ord( Tipo ), Numero );
     end;
end;

procedure TdmPresupuestos.CrearContratacionesDataSet;
var
   i: Integer;
begin
     with cdsContrataciones do
     begin
          InitTempDataset;

          AddIntegerField( 'CB_CODIGO' );
          // Identificaci�n
          AddStringField( 'CB_APE_PAT', K_ANCHO_DESCRIPCION ); // No puede quedar VACIO
          AddStringField( 'CB_APE_MAT', K_ANCHO_DESCRIPCION ); // No se requiere pero debe incluirse
          AddStringField( 'CB_NOMBRES', K_ANCHO_DESCRIPCION ); // No se requiere pero debe incluirse
          AddStringField( 'CB_SEXO', K_ANCHO_CODIGO1 );        // No puede quedar VACIO
          // Contrataci�n
          AddDateField( 'CB_FEC_ING' );
          AddDateField( 'CB_FEC_ANT' );
          AddDateField( 'CB_FEC_CON' );
          AddStringField( 'CB_CONTRAT', K_ANCHO_CODIGO1 );
          AddStringField( 'CB_PUESTO', K_ANCHO_CODIGO );
          AddStringField( 'CB_CLASIFI', K_ANCHO_CODIGO );
          AddStringField( 'CB_TURNO', K_ANCHO_CODIGO );
          AddStringField( 'CB_PATRON', K_ANCHO_CODIGO1 );
          // Niveles
          for i := 1 to K_GLOBAL_NIVEL_MAX do
              AddStringField( 'CB_NIVEL' + IntToStr( i ), K_ANCHO_CODIGO_NIVELES );
          // Salario
          AddStringField( 'CB_AUTOSAL', K_ANCHO_BOOLEANO );
          AddFloatField( 'CB_SALARIO' );
          AddFloatField( 'CB_PER_VAR' );
          AddFloatField( 'CB_FAC_INT' );
          AddFloatField( 'CB_SAL_INT' );
          AddStringField( 'CB_ZONA_GE', K_ANCHO_ZONAGEO );
          AddStringField( 'CB_TABLASS', K_ANCHO_CODIGO1 );
          AddStringField( 'COD_PERCEPS', 10 );
          // Otros
          AddStringField( 'CB_BAN_ELE', K_ANCHO_DESCRIPCION );
          AddStringField( 'CB_NIVEL0', K_ANCHO_CODIGO );
          // Vacaciones
          AddDateField( 'CB_DER_FEC' );
          AddFloatField( 'CB_DER_PAG' );
          AddFloatField( 'CB_DER_GOZ' );
          // Personales
          AddStringField( 'NOM_PADRE', K_ANCHO_DESCRIPCION );
          AddStringField( 'NOM_MADRE', K_ANCHO_DESCRIPCION );

          AddIntegerField( 'CB_PLAZA' );
          AddFloatField( 'CB_DER_PV');
          AddIntegerField( 'CB_NOMINA' );

          { AP(03/Oct/08): Defecto #1123: Alta de Empleados marca el error
          Fecha de Contrataci�n }
          AddDateField('CB_FEC_COV');

          AddIntegerField('CB_INFTIPO' );   //Lo valida, por eso se lleva
          AddStringField( 'CB_MUNICIP', K_ANCHO_CODIGO );//Lo valida, por eso se lleva
          AddStringField( 'CB_ESTADO', K_ANCHO_CODIGO );//Lo valida, por eso se lleva
          CreateTempDataset;
     end;
end;

procedure TdmPresupuestos.CrearImportarKardexDataSet;
begin
     with cdsImportarKardex do
     begin
          InitTempDataset;

          AddIntegerField( 'CB_CODIGO' );
          AddIntegerField( 'OPERACION' );
          AddDateField( 'FECHA' );
          AddStringField( 'CODIGO', 10 );
          AddStringField( 'ADICIONALES', K_ANCHO_OBSERVACIONES );
          AddStringField( 'COMENTARIO', K_ANCHO_OBSERVACIONES );

          CreateTempDataset;
     end;
end;

{ cdsEventosAltas }

procedure TdmPresupuestos.cdsEventosAltasAlAdquirirDatos(Sender: TObject);
begin
     cdsEventosAltas.Data := ServerPresupuestos.GetEventosAltas( dmCliente.Empresa );
end;

procedure TdmPresupuestos.cdsEventosAltasAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatEventosAltas_DevEx, TEditCatEventosAltas_DevEx );
end;

procedure TdmPresupuestos.cdsEventosAltasNewRecord(DataSet: TDataSet);
begin
     with cdsEventosAltas do
     begin
          FieldByName( 'EV_ACTIVO' ).AsString  := K_GLOBAL_SI;
          FieldByName( 'EV_NIVEL0' ).AsString  := dmCliente.ConfidencialidadDefault;
          FieldByName( 'EV_BAJA' ).AsString    := K_GLOBAL_NO;
          FieldByName( 'EV_BAN_ELE' ).AsString := K_GLOBAL_NO;
          FieldByName( 'EV_NOMINA' ).AsInteger := Ord( dmCliente.PeriodoTipo );

          with Global.GetEmpDefault do
          begin
               FieldByName( 'EV_AUTOSAL' ).AsString := ZetaCommonTools.zBoolToStr( AutoSal );
               FieldByName( 'EV_ZONA_GE' ).AsString := ZonaGeografica;
               FieldByName( 'EV_PATRON' ).AsString  := Patron;
               FieldByName( 'EV_SALARIO' ).AsFloat  := Salario;
               FieldByName( 'EV_CONTRAT' ).AsString := Contrato;
               FieldByName( 'EV_CLASIFI' ).AsString := Clasifi;
               FieldByName( 'EV_TURNO' ).AsString   := Turno;
               FieldByName( 'EV_TABLASS' ).AsString := TablaSS;
               FieldByName( 'EV_NIVEL1' ).AsString  := Nivel1;
               FieldByName( 'EV_NIVEL2' ).AsString  := Nivel2;
               FieldByName( 'EV_NIVEL3' ).AsString  := Nivel3;
               FieldByName( 'EV_NIVEL4' ).AsString  := Nivel4;
               FieldByName( 'EV_NIVEL5' ).AsString  := Nivel5;
               FieldByName( 'EV_NIVEL6' ).AsString  := Nivel6;
               FieldByName( 'EV_NIVEL7' ).AsString  := Nivel7;
               FieldByName( 'EV_NIVEL8' ).AsString  := Nivel8;
               FieldByName( 'EV_NIVEL9' ).AsString  := Nivel9;
               FieldByName( 'EV_PUESTO' ).AsString  := Puesto;   // Se asigna al final para que primero sugiera los defaults de globales
          end;
          if strVacio( FieldByName( 'EV_NIVEL0' ).AsString ) and dmSistema.HayNivel0 then      // Si se ocupa un valor de confidencialidad
             FieldByName( 'EV_NIVEL0' ).AsString := dmSistema.cdsNivel0.FieldByName( 'TB_CODIGO' ).AsString;
     end;
end;

procedure TdmPresupuestos.cdsEventosAltasAlCrearCampos(Sender: TObject);
begin
     with cdsEventosAltas do
     begin
          FieldByName( 'CB_CODIGO' ).OnChange := cdsEventosAltasCB_CODIGOChange;
          FieldByName( 'EV_PUESTO' ).OnChange := cdsEventosAltasEV_PUESTOChange;
          FieldByName( 'EV_CLASIFI' ).OnChange := cdsEventosAltasTabuladorChange;
          FieldByName( 'EV_AUTOSAL' ).OnChange := cdsEventosAltasTabuladorChange;
     end;
end;

procedure TdmPresupuestos.cdsEventosAltasBeforePost(DataSet: TDataSet);
var
   i : Integer;
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'EV_CODIGO' ).AsString ) then
             DataBaseError( 'El c�digo no puede quedar vac�o' );
          if ZetaCommonTools.strVacio( FieldByName( 'EV_NIVEL0' ).AsString ) and dmSistema.HayNivel0 then      // Si se ocupa un valor de confidencialidad
             DataBaseError( 'Nivel de confidencialidad no puede quedar vac�o' );
          if ( FieldByName( 'EV_SALARIO' ).AsFloat < 0 ) then
             DataBaseError( 'Salario no puede ser menor que cero' );
          if ZetaCommonTools.StrVacio( FieldByName( 'EV_PUESTO' ).AsString ) or ZetaCommonTools.StrVacio( FieldByName( 'EV_CLASIFI' ).AsString ) then
             DataBaseError( 'Puesto y/o clasificaci�n no pueden quedar vacios') ;
          if ZetaCommonTools.StrVacio( FieldByName( 'EV_TURNO' ).AsString ) then
             DataBaseError( 'Turno no puede quedar vac�o') ;
          if ZetaCommonTools.StrVacio( FieldByName( 'EV_TABLASS' ).AsString ) then
             DataBaseError( 'Tabla de prestaciones no puede quedar vac�a');
          if ZetaCommonTools.StrVacio( FieldByName( 'EV_CONTRAT' ).AsString ) then
             DataBaseError( 'Tipo de Contrato no puede quedar vac�o') ;
          for i := 1 to Global.NumNiveles do
          begin
               if ZetaCommonTools.StrVacio( FieldByName( 'EV_NIVEL' + IntToStr( i )).AsString ) then
                  DataBaseError( Global.NombreNivel( i ) + ' no puede quedar vac�o') ;
          end;
          if ZetaCommonTools.StrVacio( FieldByName( 'EV_PATRON').AsString ) then
             DataBaseError( 'Registro Patronal no puede quedar vac�o');
     end;
end;

procedure TdmPresupuestos.cdsEventosAltasAfterDelete(DataSet: TDataSet);
begin
     cdsEventosAltas.Enviar;
end;

procedure TdmPresupuestos.cdsEventosAltasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsEventosAltas do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPresupuestos.GrabaEventosAltas( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enEventoAlta ] );
               end;
          end;
     end;
end;

procedure TdmPresupuestos.cdsEventosAltasCB_CODIGOChange(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) and dmCliente.SetEmpleadoNumero( AsInteger ) then
          begin
               SetDatosEmpleado( AsInteger );
          end;
     end;
end;

procedure TdmPresupuestos.cdsEventosAltasEV_PUESTOChange(Sender: TField);
begin
     with Sender do
     begin
          if strLleno( AsString ) then
             SetDatosPuesto( AsString );
     end;
end;

procedure TdmPresupuestos.cdsEventosAltasTabuladorChange(Sender: TField);
begin
     with cdsEventosAltas do
     begin
          if strLleno( FieldByName( 'EV_CLASIFI' ).AsString ) and
             zStrToBool( FieldByName( 'EV_AUTOSAL' ).AsString ) then   // Tiene Tabulador
             SetDatosTabulador( FieldByName( 'EV_CLASIFI' ).AsString );
     end;
end;
procedure TdmPresupuestos.cdsEventosAltasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_PRESUP_CONTRATACIONES, iRight );
end;

procedure TdmPresupuestos.AsignaDatosClasificacion( DataSet: TZetaClientDataSet; const sPrefijo: string; const lRevisaCambio: Boolean );

   procedure RevisaCambio( Destino, Fuente: TField );
   begin
        if lRevisaCambio then
           ZetaDataSetTools.RevisaCambioCampo( Fuente, Destino )
        else
            Destino.AsString := Fuente.AsString;
   end;

begin
     with Dataset do
     begin
          RevisaCambio( cdsEventosAltas.FieldByName( 'EV_CLASIFI' ), FieldByName( sPrefijo + '_CLASIFI' ));  // Se asigna al principio para que despues se sobreescriba con lo del empleado
          RevisaCambio( cdsEventosAltas.FieldByName( 'EV_NIVEL0' ), FieldByName( sPrefijo + '_NIVEL0' ));
          RevisaCambio( cdsEventosAltas.FieldByName( 'EV_ZONA_GE' ), FieldByName( sPrefijo + '_ZONA_GE' ));
          RevisaCambio( cdsEventosAltas.FieldByName( 'EV_PATRON' ), FieldByName( sPrefijo + '_PATRON' ));
          RevisaCambio( cdsEventosAltas.FieldByName( 'EV_CONTRAT' ), FieldByName( sPrefijo + '_CONTRAT' ));
          RevisaCambio( cdsEventosAltas.FieldByName( 'EV_TURNO' ), FieldByName( sPrefijo + '_TURNO' ));
          RevisaCambio( cdsEventosAltas.FieldByName( 'EV_TABLASS' ), FieldByName( sPrefijo + '_TABLASS' ));

          if lRevisaCambio then
             ZetaDataSetTools.RevisaCambioCampo( FieldByName( sPrefijo + '_SALARIO' ), cdsEventosAltas.FieldByName( 'EV_SALARIO' ) )
          else
              cdsEventosAltas.FieldByName( 'EV_SALARIO' ).AsFloat  := FieldByName( sPrefijo + '_SALARIO' ).AsFloat;

     end;
end;

procedure TdmPresupuestos.SetDatosEmpleado( const iEmpleado: Integer );
var
   i : Integer;
begin
     with dmCliente.cdsEmpleado do
     begin
          cdsEventosAltas.FieldByName( 'EV_PUESTO' ).AsString := FieldByName( 'CB_PUESTO' ).AsString;   // Se asigna al principio para que despues se sobreescriba lo configurado en el puesto
          cdsEventosAltas.FieldByName( 'EV_AUTOSAL' ).AsString := FieldByName( 'CB_AUTOSAL' ).AsString; // Se asigna al principio para que despues se sobreescriba con lo del empleado
          cdsEventosAltas.FieldByName( 'EV_NOMINA' ).AsInteger := FieldByName( 'CB_NOMINA' ).AsInteger; 
          AsignaDatosClasificacion( dmCliente.cdsEmpleado, 'CB', FALSE );

          for i := 1 to K_GLOBAL_NIVEL_MAX do
              cdsEventosAltas.FieldByName( 'EV_NIVEL' + IntToStr(i) ).AsString := FieldByName( 'CB_NIVEL' + IntToStr(i) ).AsString;
     end;
     // Limpia Percepciones Fijas
     for i := 1 to 5 do
     begin
          cdsEventosAltas.FieldByName( 'EV_OTRAS_' + IntToStr(i) ).AsString := ZetaCommonClasses.VACIO;
     end;
     // Asigna Percepciones Fijas del Empleado activo
     with dmRecursos.cdsEmpPercepcion do
     begin
          Refrescar;                    //El Data viene ordenado por KF_FOLIO - no se ocupa ordenar
          i := 1;
          while ( not EOF ) and ( i <= 5 ) do
          begin
               cdsEventosAltas.FieldByName( 'EV_OTRAS_' + IntToStr(i) ).AsString := FieldByName( 'KF_CODIGO' ).AsString;
               Inc( i );
               Next;
          end;
     end;
end;

procedure TdmPresupuestos.SetDatosPuesto( const sPuesto: String );
var
   i : Integer;
begin
     with dmCatalogos.cdsPuestos do
     begin
          Conectar;
          if ( not IsEmpty ) then
          begin
               if ( sPuesto = FieldByName( 'PU_CODIGO' ).AsString ) or Locate( 'PU_CODIGO', sPuesto, [] ) then
               begin
                    AsignaDatosClasificacion( dmCatalogos.cdsPuestos, 'PU', TRUE );

                    ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_PER_VAR' ), cdsEventosAltas.FieldByName( 'EV_PER_VAR' ) );
                    // Multiples
                    if ( FieldByName( 'PU_AUTOSAL' ).AsString <> ZetaCommonClasses.K_USAR_GLOBAL ) then    // Si NO se quiere Usar Global de Empresa
                       ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_AUTOSAL' ), cdsEventosAltas.FieldByName( 'EV_AUTOSAL' ) );

                    for i := 1 to K_GLOBAL_NIVEL_MAX do
                        ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_NIVEL' + IntToStr(i) ), cdsEventosAltas.FieldByName( 'EV_NIVEL' + IntToStr(i) ) );
               end;
          end;
     end;
end;

procedure TdmPresupuestos.SetDatosTabulador( const sClasif: String );
var
   i : Integer;
begin
     with dmCatalogos.cdsClasifi do
     begin
          Conectar;
          if ( not IsEmpty ) then
          begin
               if ( sClasif = FieldByName( 'TB_CODIGO' ).AsString ) or Locate( 'TB_CODIGO', sClasif, [] ) then
               begin
                    cdsEventosAltas.FieldByName( 'EV_SALARIO' ).AsFloat := FieldByName( 'TB_SALARIO' ).AsFloat;
                    for i := 1 to 5 do
                    begin
                         cdsEventosAltas.FieldByName( 'EV_OTRAS_' + IntToStr( i ) ).AsString := FieldByName( 'TB_OP' + IntToStr( i ) ).AsString;
                    end;
               end;
          end;
     end;
end;

{ cdsSupuestosRH }

procedure TdmPresupuestos.cdsSupuestosRHAlAdquirirDatos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     cdsSupuestosRH.Data := ServerPresupuestos.GetSupuestosRH( dmCliente.Empresa );
end;

procedure TdmPresupuestos.cdsSupuestosRHAlCrearCampos(Sender: TObject);
begin
     with cdsSupuestosRH do
     begin
          MaskFecha( 'SR_FECHA' );
          ListaFija( 'SR_TIPO', lfTipoSupuestoRH );
          CreateCalculated( 'DESCRIPCION', ftString, K_ANCHO_DESCRIPCION );
          CreateCalculated( 'PRIORIDAD', ftString, 20 );
          CreateCalculated( 'ACTIVO', ftString, K_ANCHO_BOOLEANO );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          FieldByName( 'EV_CODIGO' ).OnValidate := cdsSupuestosEV_CODIGOValidate;
          FieldByName( 'SR_TIPO' ).OnChange := cdsSupuestosSR_TIPOChange;
     end;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TdmPresupuestos.RefrescarSupuestosRH( sEscenario : String );
begin
     if NOT FSimulando then
     begin
          with cdsSupuestosRH do
          begin
               Filtered := False;
               Filter := Format('ES_CODIGO = %s', [ EntreComillas( sEscenario ) ] );
               Filtered := True;
          end;
     end;
end;

procedure TdmPresupuestos.cdsSupuestosRHCalcFields(DataSet: TDataSet);
var
   lEsEventoKardex : Boolean;
   oLookupDataSet  : TZetaLookupDataSet;
begin
     DataSet.FieldByName( 'DESCRIPCION' ).AsString := VACIO;
     DataSet.FieldByName( 'ACTIVO' ).AsString := K_GLOBAL_SI;
     DataSet.FieldByName( 'PRIORIDAD' ).AsString := VACIO;
     if strLleno( DataSet.FieldByName( 'EV_CODIGO' ).AsString ) then
     begin
          lEsEventoKardex := ( eTipoSupuestoRH( DataSet.FieldByName( 'SR_TIPO' ).AsInteger ) = srEvento );
          if lEsEventoKardex then
             oLookupDataSet := dmCatalogos.cdsEventos
          else
              oLookupDataSet := self.cdsEventosAltas;
          with oLookupDataSet do
          begin
               Conectar;
               if Locate( 'EV_CODIGO', DataSet.FieldByName( 'EV_CODIGO' ).AsString, [ ] ) then
               begin
                    DataSet.FieldByName( 'DESCRIPCION' ).AsString := FieldByName( 'EV_DESCRIP' ).AsString;
                    DataSet.FieldByName( 'ACTIVO' ).AsString := FieldByName( 'EV_ACTIVO' ). AsString;
                    if lEsEventoKardex then
                       DataSet.FieldByName( 'PRIORIDAD' ).AsString := ObtieneElemento( lfNivelUsuario, FieldByName( 'EV_PRIORID' ).AsInteger );
               end;
          end;
     end;
     //Application.ProcessMessages;
end;

procedure TdmPresupuestos.cdsSupuestosRHAlModificar(Sender: TObject);
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridSupuestosRH_DevEx, TGridSupuestosRH_DevEx, FALSE );
     Application.ProcessMessages;
end;

procedure TdmPresupuestos.cdsSupuestosRHNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'SR_TIPO' ).AsInteger := Ord( srEventoAlta );
          FieldByName( 'SR_FECHA' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'SR_CUANTOS' ).AsInteger := 0;
          // (JB) Escenarios de presupuestos T1060 CR1872
          FieldByName( 'ES_CODIGO' ).AsString := cdsEscenarios.FieldByName('ES_CODIGO').AsString ;
     end;
end;

procedure TdmPresupuestos.cdsSupuestosRHBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if StrVacio( FieldByName('EV_CODIGO').AsString ) then
             DatabaseError( 'El c�digo del evento o contrataci�n no puede quedar vac�o' );
          if ( eTipoSupuestoRH( FieldByName( 'SR_TIPO' ).AsInteger ) = srEventoAlta ) and ( FieldByName('SR_CUANTOS').AsInteger <= 0 ) then
             DatabaseError( 'La cantidad de contrataciones debe ser mayor a cero' );
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          // (JB) Escenarios de presupuestos T1060 CR1872
          if StrVacio( FieldByName('ES_CODIGO').AsString ) then
               DatabaseError( 'El C�digo de Escenario no puede quedar Vac�o' );
    end;
end;

procedure TdmPresupuestos.cdsSupuestosRHAlBorrar(Sender: TObject);
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar �ste supuesto ?' ) then
     begin
          with cdsSupuestosRH do
          begin
               Delete;
               Enviar;
          end;
     end;
end;

procedure TdmPresupuestos.cdsSupuestosRHAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsSupuestosRH do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPresupuestos.GrabaSupuestosRH( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enSupuestosRH ] );
               end;
          end;
     end;
end;

{$ifdef VER130}
procedure TdmPresupuestos.cdsSupuestosRHReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
     Action := raCancel;
end;
{$else}
procedure TdmPresupuestos.cdsSupuestosRHReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
     Action := raCancel;
end;
{$endif}

function TdmPresupuestos.ValidaCodigoSupuesto( const TipoSupuesto: eTipoSupuestoRH; const sCodigo: String ): Boolean;
var
   sDescrip : String;
   oLookupDataSet : TZetaLookupDataSet;
begin
     if strLleno( sCodigo ) then
     begin
          if ( TipoSupuesto = srEvento ) then
             oLookupDataSet := dmCatalogos.cdsEventos
          else
              oLookupDataSet := self.cdsEventosAltas;
          Result := oLookupDataSet.LookupKey( sCodigo, Format( 'EV_ACTIVO=%s', [ EntreComillas( K_GLOBAL_SI ) ] ), sDescrip );
     end
     else
         Result := TRUE;
end;

procedure TdmPresupuestos.cdsSupuestosSR_TIPOChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          if ( not ( ValidaCodigoSupuesto( eTipoSupuestoRH( FieldByName( 'SR_TIPO' ).AsInteger ), FieldByName( 'EV_CODIGO' ).AsString ) ) ) then
             FieldByName( 'EV_CODIGO' ).AsString := VACIO;    // El c�digo anterior no existe valido para el nuevo tipo de evento
     end;
end;

procedure TdmPresupuestos.cdsSupuestosEV_CODIGOValidate(Sender: TField);
begin
     if ( not ( ValidaCodigoSupuesto( eTipoSupuestoRH( Sender.DataSet.FieldByName( 'SR_TIPO' ).AsInteger ), Sender.AsString ) ) ) then
     begin
          ZetaDialogo.ZError( '', 'C�digo no existe o no est� activo', 0 );
          Abort;
     end;
end;

{ cdsPeriodos }

procedure TdmPresupuestos.cdsPeriodosAlAdquirirDatos(Sender: TObject);
begin
     if ( FParametros = nil ) then
        db.DataBaseError( 'Falta iniciar par�metros del proceso' )
     else
         cdsPeriodos.Data := ServerPresupuestos.GetPeriodosSimulacion( dmCliente.Empresa, FParametros.VarValues );
end;

{ Simulacion de Presupuesto }

function TdmPresupuestos.CanContinue( const sMsg: String = VACIO; const iStep: Integer = 1 ): Boolean;
begin
     FCounter := FCounter + iStep;
     Result := self.DoCallBack( sMsg, FCounter );
end;

function TdmPresupuestos.DoCallBack( const sMsg: String; const iStep: Integer ): Boolean;
begin
     if Assigned( FCallBack ) then
        FCallBack( Self, sMsg, iStep, Result )
     else
         Result := True;
end;

procedure TdmPresupuestos.ConectaDataSets;
const
     K_FILTRO_FECHAS_RH = '( SR_FECHA >= %s ) and ( SR_FECHA <= %s )';
begin
     with dmCatalogos do
     begin
          cdsEventos.Conectar;
          cdsContratos.Conectar;
     end;
     cdsEventosAltas.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsSupuestosRH do
     begin
          Conectar;
          IndexFieldNames := 'ES_CODIGO;SR_FECHA;SR_TIPO';
          with FParametros do
          begin
               if ( not ParamByName( 'TodosSupuestosRH' ).AsBoolean ) then
               begin
                    Filter := Format( K_FILTRO_FECHAS_RH, [  EntreComillas( FechaAsStr( ParamByName( 'FechaRH1' ).AsDateTime ) ),
                                                             EntreComillas( FechaAsStr( ParamByName( 'FechaRH2' ).AsDateTime ) ) ] );
                    Filtered := TRUE;
               end;
          end;
     end;
     cdsPeriodos.Conectar;
end;

procedure TdmPresupuestos.DesconectaDataSets;
begin
     with cdsSupuestosRH do
     begin
          Filtered := FALSE;
          Filter := VACIO;
     end;
     cdsPeriodos.Active := FALSE;
end;

function TdmPresupuestos.GetMaxSteps: Integer;
var
   iQtyMeses : Integer;
begin
     with FParametros do
     begin
          iQtyMeses:= ( ParamByName( 'MesFinal' ).AsInteger - ParamByName( 'MesInicial' ).AsInteger + 1 );
     end;
     Result := 1 +   // Limpiar Supuestos de Kardex
               1 +   // Limpiar N�minas
               cdsSupuestosRH.RecordCount +    // Supuestos a Aplicar ( validar si Recordcount considera o no el filtro
               ( iQtyMeses * 2 ) +   // Calcular Integrados y Promedio de Variables cada mes
               ( cdsPeriodos.RecordCount * 3 ) +   // Periodos a Revisar Liquidacion, Calcular y Afectar
               1 ;   // Generar CUBO de Conteo
     { Los calculos de aguinaldo y PTU no reportar�n avance en progressbar }
end;

function TdmPresupuestos.OpenProcessSimulacion: Boolean;
begin
     // PENDIENTE: Grabar en PROCESO el inicio del proceso
     Result := TRUE;
     FCounter := 0;
     if Assigned( FCallBackStart ) then
        FCallBackStart( Self, 'Iniciando simulaci�n de presupuesto', GetMaxSteps );
end;

function TdmPresupuestos.CloseProcessSimulacion: OleVariant;
begin
     // PENDIENTE: Grabar en PROCESO el resultado y asignarlo a Result
     SetOLEVariantToNull( Result );
     if Assigned( FCallBackEnd ) then
        FCallBackEnd( Self, 'Termina simulaci�n de presupuesto' );
end;

function TdmPresupuestos.SimularPresupuesto(Parametros, ParamsAguinaldo, ParamsPTU: TZetaParams): Boolean;
var
   iLastYear : Integer;
   iLastMes : Integer;
   Resultado: OleVariant;
begin
     FParametros:= Parametros;
     iLastYear := dmCliente.YearDefault;
     dmCliente.YearDefault := FParametros.ParamByName( 'Year' ).AsInteger;
     ConectaDataSets;
     try
        FSimulando := TRUE;
        cdsSupuestosRH.DisableControls;
        if OpenProcessSimulacion then
        begin
             try
                LimpiarSupuestosRH;
                LimpiarNominasAcumulados;
                AplicarSupuestosRH;
                with cdsPeriodos do
                begin
                     First;
                     iLastMes := 0;
                     while ( not EOF ) and CanContinue( '', 0 ) do     // Barrido de Periodos
                     begin
                          dmCliente.PeriodoTipo := eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger );
                          dmCliente.SetPeriodoNumero( FieldByName( 'PE_NUMERO' ).AsInteger );
                          if ( FieldByName( 'PE_MES' ).AsInteger <> iLastMes ) then
                          begin
                               iLastMes := FieldByName( 'PE_MES' ).AsInteger;
                               if ( iLastMes <= 12 ) then         // Si es Mes13 no se calcula integrado y promedio de variables ( No hay fecha )
                               begin
                                    CalcularSalarioIntegrado( iLastMes );
                                    if ( dmCliente.YearDefault < K_YEAR_REFORMA ) or
                                       ( not ( ZetaCommonTools.EsBimestre( FieldByName( 'PE_MES' ).AsInteger ) ) ) then  // Al inicio del bimestre: 1, 3, 5, 7 ,9 ,11
                                       CalcularPromedioVariables( iLastMes );
                               end;
                          end;
                          with FParametros do
                          begin
                               if ParamByName( 'CalcularAguinaldo' ).AsBoolean and
                                  ( ParamByName( 'MesAguinaldo' ).AsInteger = iLastMes ) and
                                  EsPeriodoACalcular( eTipoPeriodoMes( ParamByName( 'PeriodoAguinaldo' ).AsInteger ) ) then
                                  CalcularAguinaldo( ParamsAguinaldo );
                               if ParamByName( 'CalcularPTU' ).AsBoolean and
                                  ( ParamByName( 'MesPTU' ).AsInteger = iLastMes ) and
                                  EsPeriodoACalcular( eTipoPeriodoMes( ParamByName( 'PeriodoPTU' ).AsInteger ) ) then
                                  CalcularPTU( ParamsPTU );
                          end;
                          LiquidacionesGlobales;
                          try
                             CalcularNomina;
                             AfectarNomina;
                          except
                                on Error: Exception do
                                begin
                                     if Pos( 'No Hay Empleados En La N�mina', Error.Message) = 0 then
                                        ZError('Simulaci�n de Presupuestos', Error.Message,0 );
                                end;
                          end;
                          Next;
                     end;
                end;
                CalcularCUBO;
             finally
                    Resultado := CloseProcessSimulacion;
             end;
        end;
     finally
            dmCliente.YearDefault := iLastYear;
            DesconectaDataSets;
            FSimulando := FALSE;
            TressShell.SetDataChange( [ enSupuestosRH ] );
            cdsSupuestosRH.EnableControls;
     end;
     Result := TRUE;
end;

procedure TdmPresupuestos.LimpiarSupuestosRH;
begin
     if CanContinue( 'Depurando supuestos de personal' ) then
     begin
          ServerPresupuestos.DepuraSupuestosRH( dmCliente.Empresa, FParametros.VarValues );
          TressShell.SetDataChange( [ enEmpleado, enKardex ] );
     end;
end;

procedure TdmPresupuestos.LimpiarNominasAcumulados;
begin
     if CanContinue( 'Depurando c�lculos anteriores de N�mina' ) then
     begin
          ServerPresupuestos.DepuraNominas( dmCliente.Empresa, FParametros.VarValues );
          TressShell.SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas, enAcumula, enAhorro, enPrestamo ] );
     end;
end;

procedure TdmPresupuestos.AplicarSupuestosRH;
var
   dFecha : TDate;
   eTipoSupuesto : eTipoSupuestoRH;
   sCodigo : String;
begin
     with cdsSupuestosRH do
     begin
          Filtered := FALSE;
          try

             First;
             while (not eof) AND (FieldByName( 'ES_CODIGO' ).AsString <> FParametros.ParamByName('ESCENARIO').AsString ) do
                   Next;

             dFecha := FieldByName( 'SR_FECHA' ).AsDateTime;
             eTipoSupuesto := eTipoSupuestoRH( FieldByName( 'SR_TIPO' ).AsInteger );
             sCodigo := FieldByName( 'EV_CODIGO' ).AsString;
             while ( not EOF ) and
                   (FieldByName( 'ES_CODIGO' ).AsString = FParametros.ParamByName('ESCENARIO').AsString ) and
                   CanContinue( Format( 'Aplicando %s: %s el %s', [ ObtieneElemento( lfTipoSupuestoRH, Ord( eTipoSupuesto ) ),
                                                                                    sCodigo, FechaCorta( dFecha ) ] ) ) do
             begin
                  if zStrToBool( FieldByName( 'ACTIVO' ).AsString ) then
                  begin
                       case eTipoSupuesto of
                            srEventoAlta : AplicaSupuestoAlta( dFecha, sCodigo, FieldByName ( 'SR_CUANTOS' ).AsInteger );
                            srEvento : AplicaSupuestoEvento( dFecha, sCodigo, FieldByName( 'DESCRIPCION' ).AsString, FieldByName ( 'SR_CUANTOS' ).AsInteger );
                       end;
                  end;
                  {
                  else
                      PENDIENTE: Mensaje para advertir que no se ejecut� por que no est� activo
                      }
                  Next;
                  dFecha  := FieldByName( 'SR_FECHA' ).AsDateTime;
                  eTipoSupuesto := eTipoSupuestoRH( FieldByName( 'SR_TIPO' ).AsInteger );
                  sCodigo := FieldByName( 'EV_CODIGO' ).AsString;
             end;
          finally
                 Filtered := TRUE;
          end;
     end;
end;

procedure TdmPresupuestos.AplicaSupuestoAlta( const dFecha: TDate; const sCodigo: String; const iQtyEmpleados: Integer );
var
   iSigEmpleado, iEmpleado: Integer;
   sTemp, sObservaciones : String;
   lAgregaBaja: Boolean;
   dFechaBaja: TDate;

   procedure AgregaContratacionInfo;
   var
      i: Integer;
   begin
        with cdsContrataciones do
        begin
             Append;
             FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
             FieldByName( 'CB_APE_PAT' ).AsString := sObservaciones;
             FieldByName( 'CB_SEXO' ).AsString := ZetaCommonLists.ObtieneElemento( lfSexo, Ord( esMasculino ) );
             FieldByName( 'CB_FEC_ING' ).AsDateTime := dFecha;
             FieldByName( 'CB_FEC_ANT' ).AsDateTime := dFecha;
             FieldByName( 'CB_FEC_CON' ).AsDateTime := dFecha;
             FieldByName( 'CB_CONTRAT' ).AsString := cdsEventosAltas.FieldByName( 'EV_CONTRAT' ).AsString;
             FieldByName( 'CB_PUESTO' ).AsString := cdsEventosAltas.FieldByName( 'EV_PUESTO' ).AsString;
             FieldByName( 'CB_CLASIFI' ).AsString := cdsEventosAltas.FieldByName( 'EV_CLASIFI' ).AsString;
             FieldByName( 'CB_TURNO' ).AsString := cdsEventosAltas.FieldByName( 'EV_TURNO' ).AsString;
             FieldByName( 'CB_PATRON' ).AsString := cdsEventosAltas.FieldByName( 'EV_PATRON' ).AsString;
             FieldByName( 'CB_NOMINA' ).AsInteger := cdsEventosAltas.FieldByName( 'EV_NOMINA' ).AsInteger;
             for i := 0 to K_GLOBAL_NIVEL_MAX do    // Se incluye el campo de Nivel0
                 FieldByName( 'CB_NIVEL' + IntToStr( i ) ).AsString := cdsEventosAltas.FieldByName( 'EV_NIVEL' + IntToStr( i ) ).AsString;
             FieldByName( 'CB_AUTOSAL' ).AsString := cdsEventosAltas.FieldByName( 'EV_AUTOSAL' ).AsString;
             FieldByName( 'CB_SALARIO' ).AsFloat := cdsEventosAltas.FieldByName( 'EV_SALARIO' ).AsFloat;
             FieldByName( 'CB_PER_VAR' ).AsFloat := cdsEventosAltas.FieldByName( 'EV_PER_VAR' ).AsFloat;
             FieldByName( 'CB_FAC_INT' ).AsFloat := 0;                            // Si van en cero se calcula el integrado normalmente
             FieldByName( 'CB_SAL_INT' ).AsFloat := 0;
             FieldByName( 'CB_ZONA_GE' ).AsString := cdsEventosAltas.FieldByName( 'EV_ZONA_GE' ).AsString;
             FieldByName( 'CB_TABLASS' ).AsString := cdsEventosAltas.FieldByName( 'EV_TABLASS' ).AsString;
             sTemp := VACIO;
             for i := 1 to 5 do
                 sTemp := ConcatString( sTemp, PadR( cdsEventosAltas.FieldByName( 'EV_OTRAS_' + IntToStr( i ) ).AsString, 2 ), VACIO );
             FieldByName( 'COD_PERCEPS' ).AsString := Trim( sTemp );
             if ZetaCommonTools.zStrToBool( cdsEventosAltas.FieldByName( 'EV_BAN_ELE' ).AsString ) then
                FieldByName( 'CB_BAN_ELE' ).AsString := sObservaciones;
             FieldByName( 'CB_DER_FEC' ).AsDateTime := NextDate( dFecha, Date );  // L�gica que se sigue en dServerRecursos
             FieldByName( 'CB_DER_PAG' ).AsFloat := 0;
             FieldByName( 'CB_DER_GOZ' ).AsFloat := 0;
             FieldByName( 'NOM_PADRE' ).AsString := VACIO;
             FieldByName( 'NOM_MADRE' ).AsString := VACIO;
             FieldByName( 'CB_MUNICIP' ).AsString := VACIO;
             FieldByName( 'CB_ESTADO' ).AsString := VACIO;
             Post;
        end;
   end;

   procedure IniciaDatosBaja;
   begin
        lAgregaBaja := ZetaCommonTools.zStrToBool( cdsEventosAltas.FieldByName( 'EV_BAJA' ).AsString );
        if lAgregaBaja then
        begin
             with dmCatalogos.cdsContratos do
             begin
                  lAgregaBaja := Locate( 'TB_CODIGO', cdsEventosAltas.FieldByName( 'EV_CONTRAT' ).AsString, [] ) and
                                 ( FieldByName( 'TB_DIAS' ).AsInteger > 0 );
                  if lAgregaBaja then
                     dFechaBaja := ( dFecha + FieldByName( 'TB_DIAS' ).AsInteger - 1 );
             end;
        end;
   end;

   procedure AgregaContratacionBaja;
   begin
        with cdsImportarKardex do
        begin
             Append;
             FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
             FieldByName( 'OPERACION' ).AsInteger := Ord( eoBaja );
             FieldByName( 'FECHA' ).AsDateTime := dFechaBaja;
             FieldByName( 'CODIGO' ).AsString := Global.GetGlobalString( K_GLOBAL_DEF_MOTIVO_BAJA );    // A Futuro es probable que se requiere especificar un motivo de baja para la simulaci�n
             FieldByName( 'ADICIONALES' ).AsString := FechaAsStr( dFechaBaja ) +                        // 25/06/2006   - Fecha de Baja de IMSS
                                                      PadL( IntToStr( dmCliente.YearDefault ), 4 ) +    // 2006         - A�o de Ultima N�mina
                                                      PadL( IntToStr( Ord( dmCliente.PeriodoTipo ) ), 1 ) +    // 1            - Tipo de Ultima N�mina
                                                      PadL( IntToStr( dmCliente.PeriodoNumero ), 3 );   //  37          - Periodo de Ultima N�mina
             FieldByName( 'COMENTARIO' ).AsString := sObservaciones;
             Post;
        end;
   end;

begin
     if cdsEventosAltas.Locate( 'EV_CODIGO', sCodigo, [] ) then
     begin
          with cdsContrataciones do
          begin
               if Active then
                  EmptyDataSet
               else
                   CrearContratacionesDataSet;
          end;


          with cdsImportarKardex do
          begin
               if Active then
                  EmptyDataSet
               else
                   CrearImportarKardexDataSet;
          end;

         { AP(03/Octubre/08: Defecto #1123
          cdsContrataciones.EmptyDataSet;
          cdsImportarKardex.EmptyDataSet;}

          IniciaDatosBaja;
          sObservaciones := 'Contrataci�n: ' + sCodigo;

          iSigEmpleado := dmRecursos.ProximoEmpleado;
          for iEmpleado := iSigEmpleado to iSigEmpleado + iQtyEmpleados - 1 do
          begin
               AgregaContratacionInfo;
               if lAgregaBaja then
                  AgregaContratacionBaja;
          end;

          // Disparar paletitas de Importar Altas y Kardex ( Bajas )
          AgregarContrataciones( sObservaciones );
          AgregarContratacionesBajas( sObservaciones );

     end;
end;

procedure TdmPresupuestos.AgregarContrataciones( const sObservaciones: String );
var
   oParams : TZetaParams;
begin
     oParams := TZetaParams.Create;
     try
        with oParams do
        begin
             AddInteger( 'Formato', Ord( faASCIIFijo ) );               // No afecta, pero debe incluirse
             AddInteger( 'FormatoImpFecha', Ord( ifDDMMYYYYs ) );       // No afecta, pero debe incluirse
             AddString( 'Archivo', VACIO );                             // No se requiere
             AddMemo( 'Observaciones', sObservaciones );
             AddBoolean( 'lRegKardex', FALSE );                         // No se agregar�n registros de Kardex
             AddDate( 'Fecha', NullDateTime );                          // No se requiere por que no se incluri�n registros de kardex
        end;
        ServerRecursos.ImportarAltasLista( dmCliente.Empresa, cdsContrataciones.Lista, oParams.VarValues );
        TressShell.SetDataChange( [ enEmpleado, enKardex, enAhorro ] );
     finally
            oParams.Free;
     end;
end;

procedure TdmPresupuestos.AgregarContratacionesBajas( const sObservaciones: String );
var
   oParams : TZetaParams;
begin
     if ( not cdsImportarKardex.IsEmpty ) then
     begin
          oParams := TZetaParams.Create;
          try
             with oParams do
             begin
                  AddInteger( 'Formato', Ord( faASCIIFijo ) );               // No afecta, pero debe incluirse
                  AddInteger( 'FormatoImpFecha', Ord( ifDDMMYYYYs ) );       // Debe coincidir con el formato en que se tranfiere fecha de baja de IMSS
                  AddString( 'Archivo', VACIO );                             // No se requiere
                  AddMemo( 'Observaciones', sObservaciones );
             end;
             cdsLista.Lista := ServerRecursos.ImportarKardexGetLista( dmCliente.Empresa, oParams.VarValues, cdsImportarKardex.Lista );
             AsignaPeriodoNomina( cdsLista );
             ServerRecursos.ImportarKardexLista( dmCliente.Empresa, cdsLista.Lista, oParams.VarValues );
             TressShell.SetDataChange( [ enEmpleado, enKardex, enAhorro ] );
          finally
                 oParams.Free;
          end;
     end;
end;

procedure TdmPresupuestos.AplicaSupuestoEvento( const dFecha: TDate; const sCodigo, sDescrip: String; const iQtyEmpleados: Integer );
var
   oParams : TZetaParams;
begin
     oParams := TZetaParams.Create;
     try
        CargaParamsWizardFiltros( oParams );
        with oParams do
        begin
             AddDate( 'FechaReferencia', dFecha );
             AddDate( 'FechaRegistro', dFecha );
             AddString( 'Descripcion', sDescrip );
             AddMemo( 'Observaciones', K_MENSAJE_PRESUPUESTO );
             AddString( 'ListaEventos', EntreComillas( sCodigo ) );   // Se aplicar� uno a la vez
        end;

        cdsLista.Lista := ServerRecursos.EventosGetLista( dmCliente.Empresa, oParams.VarValues );

        DepurarEventos( cdsLista, iQtyEmpleados );

        if ( cdsLista.FindField( 'TV_BAJA' ) <> NIL ) then
           AsignaPeriodoNomina( cdsLista );

        ServerRecursos.EventosLista( dmCliente.Empresa, cdsLista.Lista, oParams.VarValues );

        TressShell.SetDataChange( [ enEmpleado, enKardex, enAhorro ] );
     finally
            oParams.Free;
     end;
end;

procedure TdmPresupuestos.AsignaPeriodoNomina( cdsDataSet: TServerDataset );
var
   oDatasetPeriodos: TZetaClientDataSet;
   sOldIndex: String;
   dFecha: TDate;
   TipoPeriodo: eTipoPeriodo;
   iYear, iNumero : Integer;

   function LlenaDataSetPeriodos: Boolean;
   var
      dInicial, dFinal : TDate;
   begin
        with cdsDataSet do
        begin
             First;
             dInicial := FieldByName( 'TV_FECHA' ).AsDateTime;
             Last;
             dFinal := FieldByName( 'TV_FECHA' ).AsDateTime;
        end;
        oDatasetPeriodos.Data := ServerPresupuestos.GetPeriodosBaja( dmCliente.Empresa, dInicial, dFinal );
        Result := ( not oDataSetPeriodos.IsEmpty );
   end;

   function UbicaPeriodoPago: Boolean;
   begin
        with oDatasetPeriodos do
        begin
             // Si el periodo ya est� posicionado no es necesario ubicarlo ( Util para las bajas de contrataciones eventuales }
             Result := ( Ord( TipoPeriodo ) = FieldByName( 'PE_TIPO' ).AsInteger ) and
                       ( dFecha >= FieldByName('PE_FEC_INI').AsDateTime ) and
                       ( dFecha <= FieldByName('PE_FEC_FIN').AsDateTime );
             // Si no est� posicionado se ubica el Tipo y se barren los periodos
             if ( not Result ) then
             begin
                  if Locate( 'PE_TIPO', Ord( TipoPeriodo ), []) then       // Posiciona el Tipo de Periodo
                  begin
                       while ( NOT EOF ) and ( FieldByName( 'PE_TIPO' ).AsInteger = Ord( TipoPeriodo ) ) do
                       begin
                            Result := ( dFecha >= FieldByName('PE_FEC_INI').AsDateTime ) and ( dFecha <= FieldByName('PE_FEC_FIN').AsDateTime );
                            if Result then
                               Break;
                            Next;
                       end;
                  end
             end;
             // Si se encuentra ya est� posicionado
             if Result then
             begin
                  iYear   := FieldByName( 'PE_YEAR' ).AsInteger;
                  iNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
             end
        end;
   end;

begin
     with cdsDataSet do
     begin
          if ( not IsEmpty ) then
          begin
               //dmCatalogos.cdsTurnos.Conectar;
               sOldIndex := IndexFieldNames;
               oDatasetPeriodos := TZetaClientDataSet.Create( self );
               try
                  IndexFieldNames := 'TV_FECHA';
                  if LlenaDataSetPeriodos then
                  begin
                       First;
                       while ( not EOF ) do
                       begin
                            if zStrToBool( FieldByName( 'TV_BAJA' ).AsString ) then  // No a todos los empleados puede aplicar la baja
                            begin
                                 dFecha := FieldByName( 'TV_FECHA' ).AsDateTime;

                                 TipoPeriodo := eTipoPeriodo( FieldByName('TV_TIPONOM').AsInteger );

                                 if UbicaPeriodoPago then
                                 begin
                                      Edit;
                                      FieldByName( 'TV_NOMYEAR' ).AsInteger := iYear;
                                      FieldByName( 'TV_NOMTIPO' ).AsInteger := Ord( TipoPeriodo );
                                      FieldByName( 'TV_NOMNUME' ).AsInteger := iNumero;
                                      // La Fecha de IMSS del Evento debe hacerse igual a la fecha de baja ( Validaci�n de Extemporaneas )
                                      FieldByName( 'TV_FEC_BSS' ).AsDateTime := dFecha;
                                      Post;
                                 end;
                            end;

                            Next;
                       end;
                  end;
               finally
                      FreeAndNil( oDatasetPeriodos );
                      IndexFieldNames := sOldIndex;
               end;
          end;
     end;
end;

procedure TdmPresupuestos.DepurarEventos( cdsDataSet: TServerDataset; const iQtyEmpleados : integer );
var
   sOldIndex: String;
begin
     if ( iQtyEmpleados > 0 ) then
     begin
          with cdsDataset do
          begin
               sOldIndex := IndexFieldNames;
               try
                  IndexFieldNames := 'CB_FEC_ANT;CB_CODIGO';
                  First;

                  while ( RecordCount > iQtyEmpleados ) do
                  begin
                       Delete;
                  end;
               finally
                      IndexFieldNames := sOldIndex;
               end;
          end;
     end;
end;

procedure TdmPresupuestos.CalcularSalarioIntegrado( const iMes: Integer );
var
   oParams : TZetaParams;
begin
     if CanContinue( 'Salario Integrado de ' + ObtieneElemento( lfMeses, iMes - GetOffSet( lfMeses ) ) ) then
     begin
          oParams := TZetaParams.Create;
          try
             CargaParamsWizardFiltros( oParams );
             with oParams do
             begin
                  AddDate( 'Fecha', EncodeDate( FParametros.ParamByName( 'Year' ).AsInteger, iMes, 1 ) );  // Se calcula el primer dia del mes
                  AddString( 'Descripcion', 'Rec�lculo de Salarios Integrados' );
                  AddMemo( 'Observaciones', VACIO );
                  AddBoolean( 'Referencia', TRUE );
                  AddBoolean( 'NoIncluirIncapacitados', TRUE );
                  AddBoolean( 'AvisoIMSSFechaCambio', TRUE );
             end;
             ServerRecursos.SalarioIntegrado( dmCliente.Empresa, oParams.VarValues );
             TressShell.SetDataChange( [ enEmpleado, enKardex ] );
          finally
                 oParams.Free;
          end;
     end;
end;

procedure TdmPresupuestos.CalcularPromedioVariables( const iMes: Integer );
var
   oParams : TZetaParams;
   iYear : Integer;
begin
     if CanContinue( 'Promedio de Variables de ' + ObtieneElemento( lfMeses, iMes - GetOffSet( lfMeses ) ) ) then
     begin
          iYear := FParametros.ParamByName( 'Year' ).AsInteger;
          oParams := TZetaParams.Create;
          try
             CargaParamsWizardFiltros( oParams );
             with oParams do
             begin
                  if ( iMes = 1 ) then                    // Si se calcula en Enero
                  begin
                       AddInteger( 'Year', iYear - 1 );   // A�o anterior
                       AddInteger( 'Mes', NumeroBimestre( 12 ) );           // Diciembre
                  end
                  else
                  begin
                       AddInteger( 'Year', iYear );       // A�o anterior
                       AddInteger( 'Mes', NumeroBimestre( iMes - 1 ) );     // Mes Anterior
                  end;
                  AddString( 'Formula', Global.GetGlobalString( K_GLOBAL_DEF_PROM_DIAS ) );
                  AddString( 'Descripcion', 'Promedio de Percepciones Variables' );
                  AddDate( 'Fecha', EncodeDate( iYear, iMes, 1 ) );                 // Primer d�a del mes
                  AddDate( 'Fecha2', EncodeDate( iYear, iMes, 1 ) );                 // Primer d�a del mes
                  AddMemo( 'Observaciones', VACIO );
                  AddBoolean( 'NoIncluirIncapacitados', TRUE );
             end;
             ServerRecursos.PromediarVariables( dmCliente.Empresa, oParams.VarValues );
             TressShell.SetDataChange( [ enEmpleado, enKardex ] );
          finally
                 oParams.Free;
          end;
     end;
end;

function TdmPresupuestos.EsPeriodoACalcular( const PosPeriodo: eTipoPeriodoMes ): Boolean;
var
   iMes, iTipo : Integer;
begin
     case PosPeriodo of
          pmPrimero : Result := ( cdsPeriodos.FieldByName( 'PE_POS_MES' ).AsInteger = 1 );    // Primer periodo del mes
          pmUltimo :
          begin
               with cdsPeriodos do
               begin
                    iMes := FieldByName( 'PE_MES' ).AsInteger;
                    iTipo := FieldByName( 'PE_TIPO' ).AsInteger;
                    try
                       Next;
                       Result := EOF or ( iMes <> FieldByName( 'PE_MES' ).AsInteger ) or ( iTipo <> FieldByName( 'PE_TIPO' ).AsInteger );
                    finally
                           if ( not EOF ) then
                              Prior;
                    end;
               end;
          end;
     else
         Result := FALSE;
     end;
end;

procedure TdmPresupuestos.CalcularAguinaldo( const oParams: TZetaParams );
begin
     if CanContinue( 'Calculando Aguinaldo: ' + GetNominaDescrip, 0 ) then     // Pasa 0 para que no avance el Progress Bar
     begin
          CargaParamsWizardFiltros( oParams );
          ServerAnualNomina.CalcularAguinaldo( dmCliente.Empresa, oParams.VarValues );
          TressShell.SetDataChange( [ enAguinaldo ] );
          ServerAnualNomina.PagarAguinaldo( dmCliente.Empresa, oParams.VarValues );
          TressShell.SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas ] );
     end;
end;

procedure TdmPresupuestos.CalcularPTU( const oParams: TZetaParams );
begin
     if CanContinue( 'Calculando PTU: ' + GetNominaDescrip, 0 ) then           // Pasa 0 para que no avance el Progress Bar
     begin
          CargaParamsWizardFiltros( oParams );
          ServerAnualNomina.CalcularPTU( dmCliente.Empresa, oParams.VarValues );
          TressShell.SetDataChange( [ enRepartoPTU ] );
          ServerAnualNomina.PTUPago( dmCliente.Empresa, oParams.VarValues );
          TressShell.SetDataChange( [ enRepartoPTU, enPeriodo, enNomina, enMovimien ] );
     end;
end;

procedure TdmPresupuestos.LiquidacionesGlobales;
var
   oParams : TZetaParams;
begin
     if CanContinue( 'Calculando Liquidaciones: ' + GetNominaDescrip ) then
     begin
          oParams := TZetaParams.Create;
          try
             CargaParamsWizardFiltros( oParams );
             oParams.AddInteger( 'TipoLiquidacion', 0);
             oParams.AddString( 'Observaciones', VACIO);
             oParams.AddString('Simulacion',K_GLOBAL_NO);
             ServerNomina.LiquidacionGlobal( dmCliente.Empresa, oParams.VarValues );
             TressShell.SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas ] );
          finally
                 oParams.Free;
          end;
     end;
end;

procedure TdmPresupuestos.CalcularNomina;
var
   oParams : TZetaParams;
begin
     if CanContinue( 'Calculando N�mina: ' + GetNominaDescrip ) then
     begin
          oParams := TZetaParams.Create;
          try
             CargaParamsWizardFiltros( oParams );
             with oParams do
             begin
                  AddBoolean( 'MuestraTiempos', FALSE );
                  AddBoolean( 'MuestraScript', FALSE );
                  AddInteger( 'TipoCalculo', Ord( tcNueva ) );
             end;
             ServerCalcNomina.CalculaNomina( dmCliente.Empresa, oParams.VarValues );
             TressShell.SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
          finally
                 oParams.Free;
          end;
     end;
end;

procedure TdmPresupuestos.AfectarNomina;
var
   oParams : TZetaParams;
begin
     if CanContinue( 'Afectando N�mina: ' + GetNominaDescrip ) then
     begin
          oParams := TZetaParams.Create;
          try
             CargaParamsWizardFiltros( oParams );
             ServerNomina.AfectarNomina( dmCliente.Empresa, oParams.VarValues );
             TressShell.SetDataChange( [ enPeriodo, enNomina, enAcumula, enAhorro, enPrestamo ] );
          finally
                 oParams.Free;
          end;
     end;
end;

procedure TdmPresupuestos.CalcularCUBO;
begin
     if CanContinue( 'Calculando CUBO de Totales' ) then
     begin
          ServerPresupuestos.CalculaCubo( dmCliente.Empresa, FParametros.VarValues );
          TressShell.SetDataChange( [ enCuboPresup ] );
     end;
end;

procedure TdmPresupuestos.ShowWizardSimulacion;
begin
     ZcxWizardBasico.ShowWizard( TWizPresupSimulacion_DevEx );
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TdmPresupuestos.cdsEscenariosAlAdquirirDatos(Sender: TObject);
begin
     cdsEscenarios.Data := ServerPresupuestos.GetEscenarios( dmCliente.Empresa );
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TdmPresupuestos.cdsEscenariosAlBorrar(Sender: TObject);
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar �ste escenario ?' ) then
     begin
          with cdsEscenarios do
          begin
               Delete;
               Enviar;
          end;
     end;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TdmPresupuestos.cdsEscenariosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsEscenarios do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPresupuestos.GrabaEscenarios( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enSupuestosRH ] );
               end;
          end;
     end;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TdmPresupuestos.cdsEscenariosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( GridEditEscenario_DevEx, TGridEditEscenario_DevEx );
     Application.ProcessMessages;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TdmPresupuestos.cdsEscenariosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
     Action := raCancel;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TdmPresupuestos.CopiaSupuestos(const sLlaveEscenario, sLlaveNuevoEscenario : String );
var
     cdsTemporal : TZetaClientDataSet;
begin
     cdsTemporal := TZetaClientDataSet.Create( self );

     with cdsTemporal do
     begin
          Data := ServerPresupuestos.GetSupuestosRH( dmCliente.Empresa );
          Filter := Format( 'ES_CODIGO = %s', [ EntreComillas( sLlaveEscenario ) ] );
          Filtered := True;
          First;
          While Not Eof do
          begin
               //cdsSupuestosRH.Active := True;
               if not ( cdsSupuestosRH.State in [ dsEdit, dsInsert ] ) then
                   cdsSupuestosRH.Append;
               cdsSupuestosRH.FieldByName('SR_FECHA').AsDateTime := FieldByName('SR_FECHA').AsDateTime;
               cdsSupuestosRH.FieldByName('SR_TIPO').AsInteger := FieldByName('SR_TIPO').AsInteger;
               cdsSupuestosRH.FieldByName('SR_CUANTOS').AsInteger := FieldByName('SR_CUANTOS').AsInteger;
               cdsSupuestosRH.FieldByName('EV_CODIGO').AsString := FieldByName('EV_CODIGO').AsString;
               cdsSupuestosRH.FieldByName('US_CODIGO').AsInteger := FieldByName('US_CODIGO').AsInteger;
               cdsSupuestosRH.FieldByName('ES_CODIGO').AsString := sLlaveNuevoEscenario;
               cdsSupuestosRH.Post;
               Next;
          end;
     end;
end;

end.
