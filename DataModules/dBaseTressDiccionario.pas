unit dBaseTressDiccionario;

interface
{$INCLUDE DEFINES.INC}


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dbasediccionario, Db, DBClient, ZetaClientDataSet,
  ZetaCommonLists;

type
  TdmBaseTressDiccionario = class(TdmBaseDiccionario)
  private
    { Private declarations }
  protected
    function ChecaDerechoConfidencial: Boolean;override;
    function ValidaModulo( const eClasifi: eClasifiReporte ): Boolean;override;
  public
    { Public declarations }
    function GetValorActivo({$ifdef RDD}const eValor: eRDDValorActivo{$else}const sValor: string{$endif}): string;override;
    function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;
  end;

var
  dmBaseTressDiccionario: TdmBaseTressDiccionario;

implementation

uses
    DCliente,
    DGlobal,
    FAutoClasses,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZGlobalTress,
    ZAccesosMgr,
    ZAccesosTress,
    ZReportTools;

{$R *.DFM}

{ TdmBaseTressDiccionario }

function TdmBaseTressDiccionario.ChecaDerechoConfidencial: Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );
end;

function TdmBaseTressDiccionario.ValidaModulo(const eClasifi: eClasifiReporte): Boolean;
begin
     Result := TRUE;
end;

function TdmBaseTressDiccionario.GetValorActivo(  {$ifdef RDD}const eValor :eRDDValorActivo{$else}const sValor :string{$endif} ): string;
begin
     {$ifdef RDD}
     with dmCliente do
     begin
          case eValor  of
               vaEmpleado: Result := IntToStr( Empleado );
               vaNumeroNomina: Result := IntToStr( GetDatosPeriodoActivo.Numero );
               vaTipoNomina: Result := IntToStr( Ord( GetDatosPeriodoActivo.Tipo ) );
               vaYearNomina: Result := IntToStr( GetDatosPeriodoActivo.Year )
          else
              Result := VACIO;
          end;
     end;
     {$else}
     with dmCliente do
     begin
     {$ifndef TRESSCFG}
          if ( sValor = K_EMPLEADO ) then
             Result := IntToStr( Empleado )
          else
              if ( sValor = K_YEAR ) then
                 Result := IntToStr( GetDatosPeriodoActivo.Year )
              else
                  if ( sValor = K_TIPO ) then
                     Result := IntToStr( Ord( GetDatosPeriodoActivo.Tipo ) )
                  else
                      if ( sValor = K_NUMERO ) then
                         Result := IntToStr( GetDatosPeriodoActivo.Numero )
                      else
                          Result := sValor;
     {$endif}
      end;

     {$endif}

end;

function TdmBaseTressDiccionario.GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;
begin
     {$ifdef RDD}
     Result := inherited GetDerechosClasifi( eClasifi ); 
     {$else}
     case eClasifi of
          crEmpleados  : Result:= D_REPORTES_EMPLEADOS;
          crCursos     : Result:= D_REPORTES_CURSOS;
          crAsistencia : Result:= D_REPORTES_ASISTENCIA;
          crNominas    : Result:= D_REPORTES_NOMINA;
          crPagosIMSS  : Result:= D_REPORTES_IMSS;
          crConsultas  : Result:= D_REPORTES_CONSULTAS;
          crCatalogos  : Result:= D_REPORTES_CATALOGOS;
          crTablas     : Result:= D_REPORTES_TABLAS;
          crSupervisor : Result:= D_REPORTES_SUPERVISORES;
          crCafeteria  : Result:= D_REPORTES_CAFETERIA;
          crLabor      : Result:= D_REPORTES_LABOR;
          crMedico     : Result:= D_REPORTES_MEDICOS;
          crCarrera    : Result:= D_REPORTES_CARRERA;
          crMigracion  : Result:= D_REPORTES_MIGRACION;
          crKiosco     : Result:= D_REPORTES_KIOSCO;
          crAccesos    : Result:= D_REPORTES_ACCESOS;
          crEvaluacion : Result:= D_REPORTES_EVALUACION;
          crCajaAhorro : Result:= D_REPORTES_CAJAAHORRO;
          crEmailEmpleados : Result:= D_REPORTES_ENVIO_EMAIL;
          else Result:= 0;
     end;
     {$endif}

end;


end.
