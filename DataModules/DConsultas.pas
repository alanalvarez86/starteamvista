unit DConsultas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient, Variants, ZetaKeyCombo,
     ZetaClientDataSet,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonLists,
{$ifdef DOS_CAPAS}
     DServerConsultas;
{$else}
     Consultas_TLB;
{$endif}

type
  TdmConsultas = class(TDataModule)
    cdsQueryGeneral: TZetaClientDataSet;
    cdsBitacora: TZetaClientDataSet;
    cdsLogDetail: TZetaClientDataSet;
    cdsProcesos: TZetaClientDataSet;
    cdsTableroRolesInformacion: TZetaClientDataSet;
    cdsTREmpresa: TZetaClientDataSet;
    cdsTRTablero: TZetaClientDataSet;
    procedure cdsQueryGeneralAlAdquirirDatos(Sender: TObject);
    procedure cdsLogDetailAfterOpen(DataSet: TDataSet);
    procedure cdsBitacoraAfterOpen(DataSet: TDataSet);
    procedure cdsProcesosAlCrearCampos(Sender: TObject);
    procedure cdsProcesosAfterOpen(DataSet: TDataSet);
    procedure cdsProcesosCalcFields(DataSet: TDataSet);
    procedure cdsProcesosAlModificar(Sender: TObject);
    procedure UsuarioGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsBitacoraAlModificar(Sender: TObject);
    procedure cdsLogDetailAlModificar(Sender: TObject);
    procedure cdsQueryGeneralAlCrearCampos(Sender: TObject);
    procedure cdsBitacoraAlCrearCampos(Sender: TObject);
    procedure cdsTREmpresaAlAdquirirDatos(Sender: TObject);
    procedure cdsTRTableroAlAdquirirDatos(Sender: TObject);
    procedure cdsTableroRolesInformacionAlBorrar(Sender: TObject);
    procedure cdsTableroRolesInformacionAlAdquirirDatos(Sender: TObject);

  private
    { Private declarations }
    FSQLText: String;
    FBaseDatosImportacionXMLCSV: OleVariant;
    FTableroGlobal : Boolean;
    FTableroEmpresa : TStringList;
    FTableroIndicadores : TSTringList;
{$ifdef DOS_CAPAS}
    function GetServerConsultas: TdmServerConsultas;
{$else}
    FServidor: IdmServerConsultasDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
{$endif}
    procedure GetProcessText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ConfigProcIdField(Campo: TField);
    procedure PC_MAXIMOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FechaGetTextQryGeneral( Sender: TField; var Text: String; DisplayText: Boolean );
  public
    { Public declarations }
{$ifdef DOS_CAPAS}
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
{$else}
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
{$endif}
    property SQLText: String read FSQLText write FSQLText;
    property TableroGlobal: Boolean read FTableroGlobal write FTableroGlobal;
    property TableroEmpresa : TStringList read FTableroEmpresa write FTableroEmpresa;
    property TableroIndicadores : TStringList read FTableroIndicadores write FTableroIndicadores;
    property BaseDatosImportacionXMLCSV: OleVariant read FBaseDatosImportacionXMLCSV write FBaseDatosImportacionXMLCSV;
    function GetStatusProceso: eProcessStatus;
    function ProcesoEstaAbierto: Boolean;
    procedure CancelarProceso;   
    {$ifdef TRESS}
    procedure GetProcessLog(const iProceso: Integer; usarBDImportacionXMLCSV: Boolean = FALSE);
    procedure LeerUnProceso(const iFolio: Integer; usarBDImportacionXMLCSV: Boolean = FALSE);
    {$else}
    procedure GetProcessLog(const iProceso: Integer);
    procedure LeerUnProceso(const iFolio: Integer);
    {$endif}
    procedure LlenaListaProcesos(Lista: TStrings);
    procedure RefrescarBitacora( Parametros: TZetaParams );
    procedure RefrescarListaProcesos(const iStatus, iProceso, iUsuario: Integer; const dStart, dEnd: TDate);
    procedure GetRolTablerosConsulta(iSesionId, iTablero :Integer; sEmpresa, sPista, sRol: String);
    function GrabaTableroRoles(iTablero :Integer; sEmpresa, sRol, sActivo: String) : Integer;
    procedure LlenarLista(const Lista: TStringList; cdsDataSet : TDataSet);
  end;

var
  dmConsultas: TdmConsultas;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZetaWizardFeedBack_DevEx,
{$ifndef TRESSCFG}
     FEditBitacora_DevEx,
     FProcessLogShow_DevEx,
{$endif}
     DSistema,
     DCliente,
     FBaseEditBitacora_DevEx,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaMsgDlg;


{$R *.DFM}

function GetEstimatedTime( const dValue: TDateTime ): String;
var
   iDias, iHour, iMin, iSec, iMSec: Word;
begin
     if ( dValue = NullDateTime ) then
        Result := ''
     else
     begin
          DecodeTime( dValue, iHour, iMin, iSec, iMSec );
          iDias := Trunc( dValue );
          Result := '';
          Result := ZetaClientTools.AddValue( '', ' %d D�a', iDias );
          if ( iDias < 1 ) then
          begin
               Result := ZetaClientTools.AddValue( Result, ' %d Hora', iHour );
               if ( iHour < 1 ) then
               begin
                    Result := ZetaClientTools.AddValue( Result, ' %d Minuto', iMin );
                    if ( iMin < 1 ) then
                       Result := ZetaClientTools.AddValue( Result, ' %d Segundo', iSec );
               end;
          end;
     end;
end;

{ ************** TdmConsultas **************** }

{$ifdef DOS_CAPAS}
function TdmConsultas.GetServerConsultas: TdmServerConsultas;
begin
     Result := DCliente.dmCliente.ServerConsultas;
end;
{$else}
function TdmConsultas.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServidor ) );
end;
{$endif}

procedure TdmConsultas.UsuarioGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;

procedure TdmConsultas.GetProcessText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Sender.Dataset.IsEmpty then
             Text := ''
          else
              Text := ZetaClientTools.GetProcessName( Procesos( Sender.AsInteger ) );
     end
     else
         Text := Sender.AsString;
end;

procedure TdmConsultas.ConfigProcIdField( Campo: TField );
begin
     with Campo do
     begin
          OnGetText := GetProcessText;
          Alignment := taLeftJustify;
     end;
end;

procedure TdmConsultas.LlenaListaProcesos( Lista: TStrings );
var
   eValue: Procesos;
   i: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
        with FTemp do
        begin
             BeginUpdate;
             Sorted := True;
             for eValue := ( Low( Procesos ) ) to High( Procesos ) do
             begin
                  if((eValue) in aProcesosVersion) then
                  	AddObject( GetProcessName( eValue ), TObject( Ord( eValue ) ) );
             end;
             EndUpdate;
        end;
        with Lista do
        begin
             try
                BeginUpdate;
                Clear;
                with FTemp do
                begin
                     for i := 0 to ( Count - 1 ) do
                     begin
                          if ( i = 0 ) then
                              Lista.Add( '0=Todos' )
                          else
                              Lista.Add( IntToStr( Integer( Objects[ i ] ) ) + '=' + Strings[ i ] );
                     end;
                end;
             finally
                    EndUpdate;
             end;
        end;
     finally
            FTemp.Free;
     end;
end;

{ cdsQueryGeneral }

procedure TdmConsultas.cdsQueryGeneralAlAdquirirDatos(Sender: TObject);
procedure FechaGetText( Sender: TField; var Text: String; DisplayText: Boolean );
var
   dValue: TDate;
begin
     if DisplayText then
     begin
          {if IsEmpty then
             Text := ''
          else
          begin }
               dValue := StrToDate (Sender.AsString);
               if ( dValue <= 0 ) then
                  Text := ''
               else
                    Text := FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat, dValue );
         // end;
     end;
end;
var
   bDerecho:Boolean;
begin
     bDerecho :=
              {$IFDEF SELECCION}True{$ELSE}
              {$IFDEF VISITANTES}True{$ELSE}
              {$IFDEF INTERFAZ_TRESS}True{$ELSE}
              ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA ){$ENDIF}{$ENDIF}{$ENDIF};

{$ifdef TIMBRADO}
     cdsQueryGeneral.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa,FSQLText );
{$else}
     cdsQueryGeneral.Data := ServerConsultas.GetQueryGral( dmCliente.Empresa,FSQLText, bDerecho );
{$endif}
end;

{ cdsLogDetail }

{$ifdef TRESS}
procedure TdmConsultas.GetProcessLog(const iProceso: Integer; usarBDImportacionXMLCSV: Boolean = FALSE);
{$else}
procedure TdmConsultas.GetProcessLog(const iProceso: Integer);
{$endif}
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        {$ifdef TRESS}
        if not usarBDImportacionXMLCSV then
           cdsLogDetail.Data := ServerConsultas.GetProcessLog( dmCliente.Empresa, iProceso )
        else
            cdsLogDetail.Data := ServerConsultas.GetProcessLog( BaseDatosImportacionXMLCSV, iProceso );
        {$else}
        cdsLogDetail.Data := ServerConsultas.GetProcessLog( dmCliente.Empresa, iProceso )
        {$endif}
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmConsultas.cdsLogDetailAfterOpen(DataSet: TDataSet);
begin
     with cdsLogDetail do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_CLASE', lfClaseBitacora );
          MaskFecha( 'BI_FECHA' );
          MaskFecha( 'BI_FEC_MOV' );
     end;
end;

procedure TdmConsultas.cdsLogDetailAlModificar(Sender: TObject);
begin
{$ifndef TRESSCFG}
     if not cdsLogDetail.IsEmpty then
        FBaseEditBitacora_DevEx.ShowLogDetail( FormaBitacora_DevEx, TFormaBitacora_DevEx, cdsLogDetail );
{$endif}
end;

{ cdsBitacora }

procedure TdmConsultas.RefrescarBitacora(Parametros: TZetaParams);
begin
     cdsBitacora.Data := ServerConsultas.GetBitacora( dmCliente.Empresa, Parametros.VarValues );
end;

procedure TdmConsultas.cdsBitacoraAfterOpen(DataSet: TDataSet);
begin
      {***(@am): Por el cambio a gridMode, se mueve este codigo al evento AlCrearCampos para que al dar Refresh
                 se conserven las mascaras en las columnas indicadas***}
     {with cdsBitacora do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_CLASE', lfClaseBitacora );
          MaskFecha( 'BI_FECHA' );
          MaskFecha( 'BI_FEC_MOV' );
          ConfigProcIdField( FieldByName( 'BI_PROC_ID' ) );
     end; }
end;

procedure TdmConsultas.cdsBitacoraAlCrearCampos(Sender: TObject);
begin
    with cdsBitacora do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_CLASE', lfClaseBitacora );
          MaskFecha( 'BI_FECHA' );
          MaskFecha( 'BI_FEC_MOV' );
          ConfigProcIdField( FieldByName( 'BI_PROC_ID' ) );
     end;
end;

procedure TdmConsultas.cdsBitacoraAlModificar(Sender: TObject);
begin
{$ifndef TRESSCFG}
         FBaseEditBitacora_DevEx.ShowLogDetail( FormaBitacora_DevEx, TFormaBitacora_DevEx, cdsBitacora );
{$endif}
end;

{ cdsProcesos }

function TdmConsultas.ProcesoEstaAbierto: Boolean;
begin
     with cdsProcesos do
     begin
          Result := ( FieldByName( 'PC_ERROR' ).AsString = B_PROCESO_ABIERTO );
     end;
end;

function TdmConsultas.GetStatusProceso: eProcessStatus;
begin
     Result := ZetaClientTools.GetProcessStatus( cdsProcesos.FieldByName( 'PC_ERROR' ).AsString );
end;

procedure TdmConsultas.RefrescarListaProcesos( const iStatus, iProceso, iUsuario: Integer; const dStart, dEnd: TDate );
begin
     cdsProcesos.Data := ServerConsultas.GetProcessList( dmCliente.Empresa, iStatus, iProceso, iUsuario, dStart, dEnd );
end;

{$ifdef TRESS}
procedure TdmConsultas.LeerUnProceso( const iFolio: Integer; usarBDImportacionXMLCSV: Boolean = FALSE );
{$else}
procedure TdmConsultas.LeerUnProceso( const iFolio: Integer);
{$endif}
var
   oCursor: TCursor;
begin
     if ( iFolio > 0 ) then
     begin
          with cdsProcesos do
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := oCursor;
               try
                  {$ifdef TRESS}
                  if not usarBDImportacionXMLCSV then
                     Data := ServerConsultas.GetProcess( dmCliente.Empresa, iFolio )
                  else
                     Data := ServerConsultas.GetProcess( BaseDatosImportacionXMLCSV, iFolio )
                  {$else}   
                  Data := ServerConsultas.GetProcess( dmCliente.Empresa, iFolio )
                  {$endif}
               finally
                      Screen.Cursor := oCursor;
               end;
               if IsEmpty then
                  ZetaDialogo.ZError( '� Bit�cora Vac�a !', 'La Bit�cora Del Proceso # ' + IntToStr( iFolio ) + ' No Existe', 0 )
               else
                   Modificar;
          end;
     end;
end;

procedure TdmConsultas.cdsProcesosAlCrearCampos(Sender: TObject);
begin
     with cdsProcesos do
     begin
          CreateCalculated( 'PC_AVANCE', ftString, 10 );
          CreateCalculated( 'PC_TIEMPO', ftString, 40 );
          CreateCalculated( 'PC_FALTA', ftString, 20 );
          CreateCalculated( 'PC_STATUS', ftString, 10 );
          CreateCalculated( 'PC_INICIO', ftString, 30 );
          CreateCalculated( 'PC_FIN', ftString, 30 );
          CreateCalculated( 'PC_SPEED', ftString, 30 );
          CreateCalculated( 'PC_PARAM', ftString, 255 );

          // Desarrollo de US #12445: Cambios sobre la pantalla de Consulta de procesos.
          MaskFecha( 'PC_FEC_INI' );
          MaskFecha( 'PC_FEC_FIN' );
          ConfigProcIdField( FieldByName( 'PC_PROC_ID' ) );
          with FieldByName( 'US_CODIGO' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := UsuarioGetText;
          end;
          FieldByName( 'US_CANCELA' ).OnGetText := UsuarioGetText;
          FieldByName( 'PC_MAXIMO' ).OnGetText := PC_MAXIMOGetText;
          // Para que no salga Empleado '0'
          TNumericField( FieldByName( 'CB_CODIGO' ) ).DisplayFormat := '0;;#';
     end;
end;

procedure TdmConsultas.cdsProcesosAfterOpen(DataSet: TDataSet);
begin
     // Desarrollo de US #12445: Cambios sobre la pantalla de Consulta de procesos.
     { Por el cambio a gridMode, se mueve este codigo al evento AlCrearCampos para que al dar Refresh
                 se conserven las mascaras en las columnas indicadas }
     {with cdsProcesos do
     begin
          MaskFecha( 'PC_FEC_INI' );
          MaskFecha( 'PC_FEC_FIN' );
          ConfigProcIdField( FieldByName( 'PC_PROC_ID' ) );
          with FieldByName( 'US_CODIGO' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := UsuarioGetText;
          end;
          FieldByName( 'US_CANCELA' ).OnGetText := UsuarioGetText;
          FieldByName( 'PC_MAXIMO' ).OnGetText := PC_MAXIMOGetText;
          // Para que no salga Empleado '0'
          TNumericField( FieldByName( 'CB_CODIGO' ) ).DisplayFormat := '0;;#';
     end;}
end;

procedure TdmConsultas.cdsProcesosCalcFields(DataSet: TDataSet);
var
   rAvance: Extended;
   iMaximo: Integer;
   dInicial, dFinal, dValue: TDateTime;
   eStatus: eProcessStatus;
   i: integer;
   sTemp, sArchivo: string;

   function GetFechaHora( const dValue: TDateTime ): String;
   const
        K_SHOW_DATE_TIME = 'hh:nn:ss AM/PM dd/mmm/yyyy';
   begin
        if ( dValue = NullDateTime ) then
           Result := ''
        else
            Result := FormatDateTime( K_SHOW_DATE_TIME, dValue );
   end;

begin
     eStatus := GetStatusProceso;
     with Dataset do
     begin
          iMaximo := FieldByName( 'PC_MAXIMO' ).AsInteger;
          if ( eStatus = epsOK ) then
             rAvance := 1
          else
              rAvance := ZetaCommonTools.rMax( 0.01, FieldByName( 'PC_PASO' ).AsInteger / ZetaCommonTools.iMax( 1, iMaximo ) );
          dInicial := ZetaClientTools.AddDateTime( FieldByName( 'PC_FEC_INI' ).AsDateTime,
                                                   FieldByName( 'PC_HOR_INI' ).AsString );
          dFinal := ZetaClientTools.AddDateTime( FieldByName( 'PC_FEC_FIN' ).AsDateTime,
                                                 FieldByName( 'PC_HOR_FIN' ).AsString );
          dValue := ZetaClientTools.GetElapsed( dFinal, dInicial );
          FieldByName( 'PC_INICIO' ).AsString := GetFechaHora( dInicial );
          FieldByName( 'PC_FIN' ).AsString := GetFechaHora( dFinal );
          FieldByName( 'PC_AVANCE' ).AsString := FormatFloat( '#0.# %', 100 * rAvance );
          FieldByName( 'PC_TIEMPO' ).AsString := ZetaClientTools.GetDuration( dValue );
          with FieldByName( 'PC_FALTA' ) do
          begin
               if ( rAvance < 0.001 ) or ( eStatus <> epsEjecutando ) then
                  AsString := ''
               else if ( rAvance < 0.15 ) then  // Avance menor al 15%
                  AsString := '???'
               else
                  AsString := GetEstimatedTime( dValue * ( ( 1 / rAvance ) - 1 ) );
          end;
          with FieldByName( 'PC_STATUS' ) do
          begin
               case eStatus of
                    epsEjecutando: AsString := 'Abierto';
                    epsCancelado: AsString := 'Cancelado';
                    epsOK: AsString := 'OK';
               else
                   AsString := 'Con Error';
               end;
          end;
          FieldByName( 'PC_SPEED' ).AsString := ZetaWizardFeedBack_DevEx.GetProcessSpeed( iMaximo, dInicial, dFinal );

          //Campo de Par�metros del Proceso
          sArchivo := FieldByName( 'PC_PARAMS' ).AsString;
          if ( sArchivo <> VACIO ) then
          begin
               for i := 1 to ( Length( sArchivo ) ) do
               begin
                    if ( sArchivo[ i ] = ZetaCommonClasses.K_PIPE ) then
                    begin
                         FieldByName( 'PC_PARAM' ).ASString := FieldByName( 'PC_PARAM' ).ASString + sTemp + CR_LF;
                         sTemp := '';
                    end
                    else
                        sTemp := sTemp + sArchivo[ i ];
               end;
               FieldByName( 'PC_PARAM' ).ASString := FieldByName( 'PC_PARAM' ).ASString + sTemp;
          end;
     end;
end;

procedure TdmConsultas.cdsProcesosAlModificar(Sender: TObject);
begin
     with cdsProcesos do
     begin
          if not IsEmpty then
          begin
          {$ifndef TRESSCFG}
          {$ifndef WORKFLOWCFG}
          if ord(prNOFonacotImportarCedulas) = FieldByName( 'PC_PROC_ID' ).AsInteger then
             cdsLogDetail.IndexFieldNames := 'CB_CODIGO;BI_TEXTO'
          else
              cdsLogDetail.IndexFieldNames := VACIO;

                   FProcessLogShow_DevEx.GetBitacora( Procesos( FieldByName( 'PC_PROC_ID' ).AsInteger ),
                                            FieldByName( 'PC_NUMERO' ).AsInteger );
          {$endif}
          {$endif}
          end;
     end;
end;

procedure TdmConsultas.CancelarProceso;
const
     K_MENSAJE = '� Desea Cancelar El Proceso' + CR_LF + '%s' + CR_LF + 'N�mero %d ?';
var
   oCursor: TCursor;
   iFolio: Integer;
begin
     with cdsProcesos do
     begin
          iFolio :=  FieldByName( 'PC_NUMERO' ).AsInteger;
          if ZetaDialogo.zConfirm( 'Cancelar Proceso', Format( K_MENSAJE, [ GetProcessName( Procesos( FieldByName( 'PC_PROC_ID' ).AsInteger ) ), iFolio ] ), 0, mbNo ) then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  ServerConsultas.CancelaProceso( dmCliente.Empresa, iFolio );
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
     end;
end;

procedure TdmConsultas.PC_MAXIMOGetText(Sender: TField; var Text: String;  DisplayText: Boolean);
begin
    if Sender.AsInteger = 0 then
        Text := 'Todos'
    else
        Text := Sender.AsString;
end;
procedure TdmConsultas.FechaGetTextQryGeneral( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Sender.AsString <> VACIO then
             Text := Sender.AsString
          else
          begin
             Text := ''
          end;
     end;
end;

procedure TdmConsultas.cdsQueryGeneralAlCrearCampos(Sender: TObject);
var
   i: Integer;
begin
      for  i := 0 to cdsQueryGeneral.Fields.Count -1 do
      begin
        if  cdsQueryGeneral.Fields[i].DataType = ftDateTime then
        begin
              cdsQueryGeneral.Fields[i].OnGetText := FechaGetTextQryGeneral;
        end;
      end;
end;


procedure TdmConsultas.cdsTableroRolesInformacionAlAdquirirDatos(Sender: TObject);
begin
     //GetRolTablerosConsulta( 1, 0, '', '', ''); //Todos
end;

procedure TdmConsultas.cdsTableroRolesInformacionAlBorrar(Sender: TObject);
Const
     K_ADVERTENCIA = ' �Desea quitar el tablero %s ' + CR_LF +
                   ' al rol %s en la ' + CR_LF +  ' empresa %s ?';
var
     sAdvertencia : String;
begin
     inherited;
     with cdsTableroRolesInformacion do
     begin
          sAdvertencia := Format(K_ADVERTENCIA, [FieldByName('TableroNombre').AsString, FieldByName('RO_CODIGO').AsString, FieldByName('CM_NOMBRE').AsString]);
          if ( ZetaMsgDlg.ConfirmaCambio( sAdvertencia ) ) then
          begin
               cdsTRTablero.Locate('Descripcion', FieldByName('TableroNombre').AsString, []);
               GrabaTableroRoles( cdsTRTablero.FieldByName('Llave').AsInteger, FieldByName('CM_CODIGO').AsString, FieldByName('RO_CODIGO').AsString, K_GLOBAL_NO );
          end;
     end;
end;

procedure TdmConsultas.cdsTREmpresaAlAdquirirDatos(Sender: TObject);
var
   Parametros : TZetaParams;
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsTREmpresa do
     begin
          Parametros := TZetaParams.Create;
          try
             Parametros.AddInteger( 'SesionID', 1 );
             Parametros.AddString(  'Todos', zBoolToStr(TableroGlobal) );

             Data := ServerConsultas.GetRolEmpresas( dmCliente.Empresa, Parametros.VarValues, ErrorCount );
          finally
                 FreeAndNil( Parametros );
          end;
     end;
end;

procedure TdmConsultas.cdsTRTableroAlAdquirirDatos(Sender: TObject);
var
   Parametros : TZetaParams;
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsTRTablero do
     begin
          Parametros := TZetaParams.Create;
          try
             Parametros.AddInteger( 'SesionID', 1 );
             Parametros.AddString(  'Todos', zBoolToStr(TableroGlobal));
             Data := ServerConsultas.GetRolTableros( dmCliente.Empresa, Parametros.VarValues, ErrorCount );
          finally
                 FreeAndNil( Parametros );
          end;
     end;
end;

function TdmConsultas.GrabaTableroRoles(iTablero :Integer; sEmpresa, sRol, sActivo: String): Integer;
var
   ErrorCount: Integer;
   Parametros : TZetaParams;
begin
     ErrorCount := 0;
     Result := 1;
     try
        Parametros := TZetaParams.Create;
        Parametros.AddString( 'Empresa', sEmpresa );
        Parametros.AddInteger( 'Tablero', iTablero );
        Parametros.AddString( 'Rol', sRol );
        Parametros.AddString( 'Activo', sActivo );

        try
           ServerConsultas.GrabaRolTablero( dmCliente.Empresa, Parametros.VarValues, ErrorCount );
        except
           on Error: Exception do
           begin
                ZError('Tableros por roles de usuario', Error.Message, 0);
                Result := 0;
           end;
        end;

     finally
            FreeAndNil( Parametros );
     end;
end;

procedure TdmConsultas.GetRolTablerosConsulta(iSesionId, iTablero :Integer; sEmpresa, sPista, sRol: String);
var
   Parametros : TZetaParams;
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsTableroRolesInformacion do
     begin
          try
             if iTablero = -1 then
                iTablero := 0;

             Parametros := TZetaParams.Create;
             Parametros.AddInteger( 'SesionID', iSesionId );
             Parametros.AddString(  'Empresa', sEmpresa );
             Parametros.AddInteger( 'Tablero', iTablero );
             Parametros.AddString(  'Rol', sRol );
             Parametros.AddString( 'Pista', sPista );

             Data := ServerConsultas.GetRolTablerosConsulta( dmCliente.Empresa, Parametros.VarValues, ErrorCount );
          finally
                 FreeAndNil( Parametros );
          end;
     end;
end;


procedure TdmConsultas.LlenarLista(const Lista: TStringList; cdsDataSet : TDataSet);
begin
     //try
        with cdsDataSet do
        begin
             with Lista do
             begin
                  try
                     BeginUpdate;
                     Clear;
                     while not Eof do
                     begin
                          AddObject( FieldByName( 'Llave' ).AsString + '=' + FieldByName( 'Descripcion' ).AsString, TObject( 0 ) );
                          Next;
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
        end;
     //end;
end;

end.
