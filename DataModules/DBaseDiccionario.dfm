object dmBaseDiccionario: TdmBaseDiccionario
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 264
  Top = 240
  Height = 479
  Width = 741
  object cdsDiccion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsDiccionAlAdquirirDatos
    Left = 32
    Top = 24
  end
  object cdsBuscaPorCampo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 96
    Top = 88
  end
  object cdsBuscaPorTabla: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 96
    Top = 152
  end
  object cdsGlobal: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsGlobalAlAdquirirDatos
    Left = 96
    Top = 24
  end
  object cdsClasificaciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RC_ORDEN'
    Params = <>
    AlAdquirirDatos = cdsClasificacionesAlAdquirirDatos
    LookupName = 'Clasificaciones'
    LookupDescriptionField = 'RC_NOMBRE'
    LookupKeyField = 'RC_CODIGO'
    Left = 348
    Top = 24
  end
  object cdsTablasPorClasificacion: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CE_ORDEN'
    Params = <>
    AlAdquirirDatos = cdsTablasPorClasificacionAlAdquirirDatos
    Left = 348
    Top = 80
  end
  object cdsRelaciones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 348
    Top = 160
  end
  object cdsCamposPorTabla: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'AT_CAMPO'
    Params = <>
    AlCrearCampos = cdsCamposPorTablaAlCrearCampos
    Left = 348
    Top = 224
  end
  object cdsLookupGeneral: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsLookupGeneralAlAdquirirDatos
    Left = 348
    Top = 352
  end
  object cdsClasifiDerechos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsClasifiDerechosAlAdquirirDatos
    Left = 348
    Top = 280
  end
  object cdsDatosDefault: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 468
    Top = 80
  end
  object cdsListasFijas: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'LV_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsListasFijasAlAdquirirDatos
    LookupDescriptionField = 'LV_NOMBRE'
    LookupKeyField = 'LV_CODIGO'
    Left = 468
    Top = 160
  end
  object cdsModulos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'MO_ORDEN'
    Params = <>
    AlAdquirirDatos = cdsModulosAlAdquirirDatos
    LookupKeyField = 'MO_CODIGO'
    Left = 468
    Top = 24
  end
  object cdsListasFijasValores: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'LV_CODIGO;VL_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsListasFijasValoresAlAdquirirDatos
    Left = 468
    Top = 224
  end
  object cdsFunciones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsFuncionesAlAdquirirDatos
    Left = 216
    Top = 24
  end
  object cdsEntidades: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'EN_CODIGO'
    Params = <>
    BeforePost = cdsEntidadesBeforePost
    OnNewRecord = cdsEntidadesNewRecord
    AlAdquirirDatos = cdsEntidadesAlAdquirirDatos
    LookupKeyField = 'EN_CODIGO'
    Left = 580
    Top = 24
  end
  object cdsEntidadesLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'EN_CODIGO'
    Params = <>
    LookupName = 'Lista de Tablas'
    LookupDescriptionField = 'EN_TITULO'
    LookupKeyField = 'EN_CODIGO'
    Left = 580
    Top = 80
  end
  object cdsModulosLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'MO_ORDEN'
    Params = <>
    LookupName = 'M'#243'dulos'
    LookupDescriptionField = 'MO_NOMBRE'
    LookupKeyField = 'MO_CODIGO'
    Left = 588
    Top = 136
  end
  object cdsEntidadesPorModulo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEntidadesPorModuloAlAdquirirDatos
    Left = 468
    Top = 296
  end
end
