unit DPlanCarrera;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, Variants,
     {$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerLabor,
     DServerPlanCarrera,
     {$else}
     Catalogos_TLB,
     Labor_TLB,
     PlanCarrera_TLB,
     {$endif}
     ZBaseEdicion,
     ZBaseEdicion_DevEx,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TdmPlanCarrera = class(TDataModule)
    cdsTCompetencia: TZetaLookupDataSet;
    cdsCalificaciones: TZetaLookupDataSet;
    cdsDimensiones: TZetaLookupDataSet;
    cdsAcciones: TZetaLookupDataSet;
    cdsNiveles: TZetaLookupDataSet;
    cdsFamilias: TZetaLookupDataSet;
    cdsCompetencia: TZetaLookupDataSet;
    cdsCompetenciaAccion: TZetaClientDataSet;
    cdsCompetenciaCalifica: TZetaClientDataSet;
    cdsCompetenciaFamilia: TZetaClientDataSet;
    cdsCompetenciaMapa: TZetaClientDataSet;
    cdsCompetenciaPuesto: TZetaClientDataSet;
    cdsPuestos: TZetaLookupDataSet;
    cdsEditNiveles: TZetaClientDataSet;
    cdsNivelDimension: TZetaClientDataSet;
    cdsEditFamilia: TZetaClientDataSet;
    cdsEditPuesto: TZetaClientDataSet;
    cdsPuestoDimension: TZetaClientDataSet;
    cdsEditCompetencia: TZetaClientDataSet;
    cdsEditAcciones: TZetaClientDataSet;
    procedure CA_CODIGOValidate(Sender: TField);
    procedure ReadOnlyGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsCalificacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsCalificacionesAlModificar(Sender: TObject);
    procedure cdsCalificacionesAlEnviarDatos(Sender: TObject);
    procedure cdsCalificacionesAfterDelete(DataSet: TDataSet);
    procedure cdsCalificacionesBeforePost(DataSet: TDataSet);
    procedure cdsDimensionesAlAdquirirDatos(Sender: TObject);
    procedure cdsDimensionesAlModificar(Sender: TObject);
    procedure cdsDimensionesAlEnviarDatos(Sender: TObject);
    procedure cdsDimensionesAfterDelete(DataSet: TDataSet);
    procedure cdsDimensionesBeforePost(DataSet: TDataSet);
    procedure cdsAccionesAlAdquirirDatos(Sender: TObject);
    procedure cdsAccionesAlAgregar(Sender: TObject);
    procedure cdsAccionesAlCrearCampos(Sender: TObject);
    procedure cdsAccionesAfterDelete(DataSet: TDataSet);
    procedure cdsAccionesAlModificar(Sender: TObject);
    procedure cdsAccionesAlEnviarDatos(Sender: TObject);
    procedure cdsPuestosAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosAlCrearCampos(Sender: TObject);
    procedure cdsPuestosAlModificar(Sender: TObject);
    procedure cdsCompetenciaAlAdquirirDatos(Sender: TObject);
    procedure cdsCompetenciaAlAgregar(Sender: TObject);
    procedure cdsCompetenciaAlCrearCampos(Sender: TObject);
    procedure cdsCompetenciaAlModificar(Sender: TObject);
    procedure cdsCompetenciaAfterDelete(DataSet: TDataSet);
    procedure cdsCompetenciaAccionAfterInsert(DataSet: TDataSet);
    procedure cdsCompetenciaAccionNewRecord(DataSet: TDataSet);
    procedure cdsCompetenciaCalificaAfterInsert(DataSet: TDataSet);
    procedure cdsCompetenciaCalificaAlCrearCampos(Sender: TObject);
    procedure cdsCompetenciaCalificaNewRecord(DataSet: TDataSet);
    procedure cdsCompetenciaFamiliaAfterInsert(DataSet: TDataSet);
    procedure cdsCompetenciaFamiliaNewRecord(DataSet: TDataSet);
    procedure cdsCompetenciaMapaAlCrearCampos(Sender: TObject);
    procedure cdsCompetenciaMapaAfterOpen(DataSet: TDataSet);
    procedure cdsCompetenciaMapaNewRecord(DataSet: TDataSet);
    procedure cdsCompetenciaPuestoAfterInsert(DataSet: TDataSet);
    procedure cdsCompetenciaPuestoAlCrearCampos(Sender: TObject);
    procedure cdsCompetenciaPuestoNewRecord(DataSet: TDataSet);
    procedure cdsTCompetenciaAlAdquirirDatos(Sender: TObject);
    procedure cdsTCompetenciaAlCrearCampos(Sender: TObject);
    procedure cdsTCompetenciaAlModificar(Sender: TObject);
    procedure cdsTCompetenciaAlEnviarDatos(Sender: TObject);
    procedure cdsTCompetenciaNewRecord(DataSet: TDataSet);
    procedure cdsTCompetenciaAfterDelete(DataSet: TDataSet);
    procedure cdsTCompetenciaBeforePost(DataSet: TDataSet);
    procedure cdsEditNivelesAlAdquirirDatos(Sender: TObject);
    procedure cdsEditNivelesAlEnviarDatos(Sender: TObject);
    procedure cdsEditNivelesBeforeDelete(DataSet: TDataSet);
    procedure cdsNivelDimensionAfterInsert(DataSet: TDataSet);
    procedure cdsNivelDimensionNewRecord(DataSet: TDataSet);
    procedure cdsNivelDimensionAlCrearCampos(Sender: TObject);
    procedure cdsNivelesAlAdquirirDatos(Sender: TObject);
    procedure cdsNivelesAfterDelete(DataSet: TDataSet);
    procedure cdsNivelesAlAgregar(Sender: TObject);
    procedure cdsNivelesAlModificar(Sender: TObject);
    procedure cdsFamiliasAlAdquirirDatos(Sender: TObject);
    procedure cdsFamiliasAlModificar(Sender: TObject);
    procedure cdsFamiliasAlAgregar(Sender: TObject);
    procedure cdsFamiliasAfterDelete(DataSet: TDataSet);
    procedure cdsPuestoDimensionAlCrearCampos(Sender: TObject);
    procedure cdsPuestoDimensionNewRecord(DataSet: TDataSet);
    procedure cdsEditAccionesAlAdquirirDatos(Sender: TObject);
    procedure cdsEditAccionesAlAgregar(Sender: TObject);
    procedure cdsEditAccionesAlEnviarDatos(Sender: TObject);
    procedure cdsEditAccionesBeforeDelete(DataSet: TDataSet);
    procedure cdsEditAccionesBeforeInsert(DataSet: TDataSet);
    procedure cdsEditAccionesBeforePost(DataSet: TDataSet);
    procedure cdsEditCompetenciaAlEnviarDatos(Sender: TObject);
    procedure cdsEditCompetenciaAlAdquirirDatos(Sender: TObject);
    procedure cdsEditCompetenciaAlAgregar(Sender: TObject);
    procedure cdsEditCompetenciaBeforeInsert(DataSet: TDataSet);
    procedure cdsEditCompetenciaBeforeDelete(DataSet: TDataSet);
    procedure cdsEditCompetenciaBeforePost(DataSet: TDataSet);
    procedure cdsEditFamiliaAlAdquirirDatos(Sender: TObject);
    procedure cdsEditFamiliaAlEnviarDatos(Sender: TObject);
    procedure cdsEditFamiliaAlAgregar(Sender: TObject);
    procedure cdsEditFamiliaBeforeInsert(DataSet: TDataSet);
    procedure cdsEditFamiliaBeforePost(DataSet: TDataSet);
    procedure cdsEditFamiliaBeforeDelete(DataSet: TDataSet);
    procedure cdsEditNivelesAlAgregar(Sender: TObject);
    procedure cdsEditNivelesBeforeInsert(DataSet: TDataSet);
    procedure cdsEditNivelesBeforePost(DataSet: TDataSet);
    procedure cdsEditPuestoAlCrearCampos(Sender: TObject);
    procedure cdsEditPuestoAlAgregar(Sender: TObject);
    procedure cdsEditPuestoAlAdquirirDatos(Sender: TObject);
    procedure cdsEditPuestoAlEnviarDatos(Sender: TObject);
    procedure cdsEditPuestoBeforePost(DataSet: TDataSet);
    procedure cdsCompetenciaBeforeInsert(DataSet: TDataSet);
    procedure cdsFamiliasAlEnviarDatos(Sender: TObject);
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    function GetServerPlanCarrera: TdmServerPlanCarrera;
    property ServerPlanCarrera: TdmServerPlanCarrera read GetServerPlanCarrera;
    {$else}
    FServidor: IdmServerPlanCarreraDisp;
    function CambiaCampo( oCampo: TField ): Boolean;
    function GetServerPlanCarrera: IdmServerPlanCarreraDisp;
    property ServerPlanCarrera: IdmServerPlanCarreraDisp read GetServerPlanCarrera;
    {$endif}
    procedure CompetenciaMapaRenumera;
    procedure GetEditCompetencia( const sCompetencia: string );
    procedure GetEditPuestos( const sCodPuesto: string );
    procedure GetEditFamilia( const sFamilia: string );
    procedure GetEditNiveles( const sNivel: string );
    procedure GetEditAccion( const sAccion: string );
    procedure LlenaCampoMemo( DataSet: TDataSet; const sFieldName: String );
    procedure ValidaCodigoLleno( DataSet: TDataSet; const sCampo: String );
    procedure ValidaDescripcionLlena( DataSet: TDataSet; const sCampo: String ); overload;
    procedure ValidaDescripcionLlena( DataSet: TDataSet; const sCampo, sMensaje: String ); overload;
  public
    { Public declarations }
    function SubeBajaAcciones( const lSubir: Boolean ): Boolean;
    function YaExistia( Dataset: TDataset; const sKeyFieldName, sKeyFieldValue: String ): Boolean;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    function ValidaModuloPlanCarrera:Boolean;
  end;

var
  dmPlanCarrera: TdmPlanCarrera;

implementation

uses
    FTressShell,
    FEditTCompetencias_DevEx,
    FEditCalifica_DevEx,
    FEditDimensiones_DevEx,
    FEditAcciones_DevEx,
    FEditNiveles_DevEx,
    FEditFamilias_DevEx,
    FEditPuestos_DevEx,
    FEditCompetencias_DevEx,
    ZReconcile,
    ZetaCommonTools,
    DCliente,
    FAutoClasses, DBasicoCliente;

{$R *.DFM}

{ TdmPlanCarrera }

{$ifdef DOS_CAPAS}
function TdmPlanCarrera.GetServerPlanCarrera: TdmServerPlanCarrera;
begin
     Result := DCliente.dmCliente.ServerPlanCarrera;
end;
{$else}
function TdmPlanCarrera.GetServerPlanCarrera: IdmServerPlanCarreraDisp;
begin
     Result := IdmServerPlanCarreraDisp( dmCliente.CreaServidor( CLASS_dmServerPlanCarrera, FServidor ) );
end;
{$endif}

function TdmPlanCarrera.YaExistia( Dataset: TDataset; const sKeyFieldName, sKeyFieldValue: String ): Boolean;
begin
     with Dataset do
     begin
          if IsEmpty then
             Result := False
          else
          begin
               DisableControls;
               try
                  Result := Locate( sKeyFieldName, sKeyFieldValue, [] );
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.ReadOnlyGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

{$ifdef VER130}
procedure TdmPlanCarrera.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmPlanCarrera.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

procedure TdmPlanCarrera.NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enTCompetencia , Entidades ) then
     {$else}
     if ( enTCompetencia in Entidades ) then
     {$endif}
     begin
          cdsTcompetencia.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCalifica , Entidades ) then
     {$else}
     if ( enCalifica in Entidades ) then
     {$endif}
     begin
          cdsCalificaciones.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enDimensiones , Entidades ) then
     {$else}
     if ( enDimensiones in Entidades ) then
     {$endif}
     begin
          cdsDimensiones.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enAcciones , Entidades ) then
     {$else}
     if ( enAcciones in Entidades ) then
     {$endif}
     begin
          cdsAcciones.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enNivelesPuesto , Entidades ) then
     {$else}
     if ( enNivelesPuesto in Entidades ) then
     {$endif}
     begin
          cdsNiveles.SetDataChange;
          cdsEditNiveles.SetDataChange;
          cdsNivelDimension.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enFamiliasPuesto , Entidades ) then
     {$else}
     if ( enFamiliasPuesto in Entidades ) then
     {$endif}
     begin
          cdsFamilias.SetDataChange;
          cdsEditFamilia.SetDataChange;
          cdsCompetenciaFamilia.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPuesto , Entidades ) then
     {$else}
     if ( enPuesto in Entidades ) then
     {$endif}
     begin
          cdsPuestos.SetDataChange;
          cdsEditPuesto.SetDataChange;
          cdsCompetenciaPuesto.SetDataChange;
          cdsPuestoDimension.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCompetencias , Entidades ) then
     {$else}
     if ( enCompetencias in Entidades ) then
     {$endif}
     begin
          cdsCompetencia.SetDataChange;
          cdsCompetenciaMapa.SetDataChange;
          cdsCompetenciaCalifica.SetDataChange;
          cdsEditCompetencia.SetDatachange;
     end;
end;

procedure TdmPlanCarrera.ValidaCodigoLleno( DataSet: TDataSet; const sCampo: String );
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( sCampo ).AsString ) then
             DataBaseError( 'El c�digo no puede quedar vac�o' );
     end;
end;

procedure TdmPlanCarrera.ValidaDescripcionLlena( DataSet: TDataSet; const sCampo, sMensaje: String );
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( sCampo ).AsString ) then
             DataBaseError( sMensaje );
     end;
end;

procedure TdmPlanCarrera.ValidaDescripcionLlena( DataSet: TDataSet; const sCampo: String );
begin
     ValidaDescripcionLlena( Dataset, sCampo, 'La descripci�n no puede quedar vac�a' );
end;

procedure TdmPlanCarrera.LlenaCampoMemo( DataSet: TDataSet; const sFieldName: String );
const
     K_ESPACIO = ' ';
begin
     {
     Evita que la base de datos provoque un error cuando
     el campo se actualiza a un valor en blanco y el cambio
     al campo es el unico cambio al Dataset

     El error es:

     [Microsoft][ODBC SQL Server Drive][SQL Server]Incorrect syntax near the keyword WHERE

     }
     with Dataset do
     begin
          if StrVacio( FieldByName( sFieldName ).AsString ) then
              FieldByName( sFieldName ).AsString := K_ESPACIO;
     end;
end;

{ Es similar a ZetaServerTools.CambiaCampo }
function TdmPlanCarrera.CambiaCampo( oCampo: TField ): Boolean;
begin
     with oCampo do
     begin
          if IsNull then
             Result := False
          else
              if ( DataType = ftDateTime ) then
                 Result := ( VarAsType( OldValue, varDate ) <> NewValue )
              else
                  Result := ( OldValue <> NewValue );
     end;
end;

{ ******** cdsTCompetencia ********* }

procedure TdmPlanCarrera.cdsTCompetenciaAlAdquirirDatos(Sender: TObject);
begin
     cdsTCompetencia.Data := ServerPlanCarrera.GetCatCompetencias( dmCliente.Empresa );
end;

procedure TdmPlanCarrera.cdsTCompetenciaAlCrearCampos(Sender: TObject);
begin
     with cdsTCompetencia do
     begin
          ListaFija( 'TC_TIPO', lfTipoCompete );
     end;
end;

procedure TdmPlanCarrera.cdsTCompetenciaAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditTCompetencias_DevEx, TEditTCompetencias_DevEx );
end;

procedure TdmPlanCarrera.cdsTCompetenciaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsTCompetencia do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaCatCompetencias( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enTCompetencia ] );
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsTCompetenciaNewRecord(DataSet: TDataSet);
begin
     with cdsTCompetencia do
     begin
          FieldByName( 'TC_TIPO' ).Asinteger := Ord( etcHabilidad );
     end;
end;

procedure TdmPlanCarrera.cdsTCompetenciaAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaCatCompetencias( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enTCompetencia ] );
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsTCompetenciaBeforePost(DataSet: TDataSet);
begin
     ValidaCodigoLleno( cdsTCompetencia, 'TC_CODIGO' );
     ValidaDescripcionLlena( cdsTCompetencia, 'TC_DESCRIP' );
end;

{ ******* cdsCalificaciones ******* }

procedure TdmPlanCarrera.cdsCalificacionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCalificaciones.Data := ServerPlanCarrera.GetCatCalificaciones( dmCliente.Empresa );
end;

procedure TdmPlanCarrera.cdsCalificacionesAlModificar(Sender: TObject);
begin
    ZBaseEdicion_DevEx.ShowFormaEdicion( EditCalifica_DevEx, TEditCalifica_DevEx );
end;

procedure TdmPlanCarrera.cdsCalificacionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCalificaciones do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaCatCalificaciones( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enCalifica ] );
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsCalificacionesAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaCatCalificaciones( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enCalifica ] );
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsCalificacionesBeforePost(DataSet: TDataSet);
begin
     ValidaCodigoLleno( cdsCalificaciones, 'CA_CODIGO' );
     ValidaDescripcionLlena( cdsCalificaciones, 'CA_DESCRIP' );
end;

procedure TdmPlanCarrera.CA_CODIGOValidate(Sender: TField);
var
   sDescrip: String;
begin
     with Sender do
     begin
          if StrLleno( AsString ) then
          begin
               if not ( cdsCalificaciones.LookupKey( UpperCase( AsString ), '', sDescrip ) ) then
                  DataBaseError( 'No Existe Calificaci�n: ' + AsString );
          end
          else
              DataBaseError( 'La Calificaci�n NO Puede Quedar Vac�a' );
     end;
end;

{ ********* cdsDimensiones ******** }

procedure TdmPlanCarrera.cdsDimensionesAlAdquirirDatos(Sender: TObject);
begin
     cdsTCompetencia.Conectar;
     cdsDimensiones.Data := ServerPlanCarrera.GetCatDimensiones( dmCliente.Empresa );
end;

procedure TdmPlanCarrera.cdsDimensionesAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditDimensiones_DevEx, TEditDimensiones_DevEx );
end;

procedure TdmPlanCarrera.cdsDimensionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsDimensiones do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaCatDimensiones( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enDimensiones ] );
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsDimensionesAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaCatDimensiones( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enDimensiones ] );
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsDimensionesBeforePost(DataSet: TDataSet);
begin
     ValidaCodigoLleno( cdsDimensiones, 'DM_CODIGO' );
     ValidaDescripcionLlena( cdsDimensiones, 'DM_DESCRIP' );
end;

{ ******* cdsAcciones ******* }

procedure TdmPlanCarrera.cdsAccionesAlAdquirirDatos(Sender: TObject);
begin
     cdsAcciones.Data := ServerPlanCarrera.GetCatAcciones( dmCliente.Empresa );
end;

procedure TdmPlanCarrera.cdsAccionesAlCrearCampos(Sender: TObject);
begin
     with cdsAcciones do
     begin
          ListaFija( 'AN_CLASE', lfClaseAcciones );
          {
          with FieldByName( 'AN_CLASE' ) do
          begin
               OnGetText := AN_CLASEGetText;
          end;
          }
     end;
end;

procedure TdmPlanCarrera.cdsAccionesAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaCatAcciones( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enAcciones ] );
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsAccionesAlModificar(Sender: TObject);
var
   sCodigo: string;
begin
     sCodigo := cdsAcciones.FieldByName( 'AN_CODIGO' ).AsString;
     cdsEditAcciones.Refrescar;
     cdsAcciones.Refrescar;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditAcciones_DevEx, TEditAcciones_DevEx );
     cdsAcciones.Locate( 'AN_CODIGO', sCodigo, [ loPartialKey ] );

     //*ZBaseEdicion.ShowFormaEdicion( EditAcciones, TEditAcciones );
end;

procedure TdmPlanCarrera.cdsAccionesAlEnviarDatos(Sender: TObject);
{
var
   ErrorCount: Integer;
}
begin
     {
     ErrorCount := 0;
     with cdsAcciones do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaCatAcciones( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enAcciones ] );
               end;
          end;
     end;
     }
end;

procedure TdmPlanCarrera.cdsAccionesAlAgregar(Sender: TObject);
begin
     cdsEditAcciones.Agregar;
end;

{ ********* cdsNiveles ******** }

procedure TdmPlanCarrera.cdsNivelesAlAdquirirDatos(Sender: TObject);
begin
     cdsNiveles.Data := ServerPlanCarrera.GetCatNiveles( dmCliente.Empresa );
end;

procedure TdmPlanCarrera.cdsNivelesAlModificar(Sender: TObject);
var
   sCodigo: string;
begin
     cdsEditNiveles.Refrescar;
     sCodigo := cdsNiveles.FieldByName( 'NP_CODIGO' ).AsString;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditNiveles_DevEx, TEditNiveles_DevEx );
     cdsNiveles.Locate( 'NP_CODIGO', sCodigo, [ loPartialKey ] );
end;

procedure TdmPlanCarrera.cdsNivelesAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerPlanCarrera.GrabaNiveles( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enNivelesPuesto ] );
               end;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsNivelesAlAgregar(Sender: TObject);
begin
     cdsEditNiveles.Agregar;
end;

{ ******** cdsFamilias ******* }

procedure TdmPlanCarrera.cdsFamiliasAlAdquirirDatos(Sender: TObject);
begin
     cdsFamilias.Data := ServerPlanCarrera.GetCatFamilias( dmCliente.Empresa );
end;

procedure TdmPlanCarrera.cdsFamiliasAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerPlanCarrera.GrabaFamilias( dmCliente.Empresa, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmPlanCarrera.cdsFamiliasAlModificar(Sender: TObject);
var
   sCodigo: string;
begin
     cdsEditFamilia.Refrescar;
     sCodigo := cdsFamilias.FieldByName( 'FP_CODIGO' ).AsString;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditFamilias_DevEx, TEditFamilias_DevEx );
     cdsFamilias.Locate( 'FP_CODIGO', sCodigo, [ loPartialKey ] );
end;

procedure TdmPlanCarrera.cdsFamiliasAlAgregar(Sender: TObject);
begin
     cdsEditFamilia.Agregar;
end;

procedure TdmPlanCarrera.cdsFamiliasAlEnviarDatos(Sender: TObject);
begin
end;

{ ******* cdsPuestos ******* }

procedure TdmPlanCarrera.cdsPuestosAlAdquirirDatos(Sender: TObject);
begin
     cdsFamilias.Conectar;
     cdsNiveles.Conectar;
     cdsPuestos.Data := ServerPlanCarrera.GetCatPuestos( dmCliente.Empresa );
end;

procedure TdmPlanCarrera.cdsPuestosAlCrearCampos(Sender: TObject);
begin
     with cdsPuestos do
     begin
          CreateSimpleLookup( cdsFamilias, 'FP_DESCRIP', 'FP_CODIGO' );
          CreateSimpleLookup( cdsNiveles, 'NP_DESCRIP', 'NP_CODIGO' );
     end;
end;

procedure TdmPlanCarrera.cdsPuestosAlModificar(Sender: TObject);
var
   sCodigo: string;
begin
     cdsEditPuesto.Refrescar;
     sCodigo := cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditPuestos_DevEx, TEditPuestos_DevEx );
     cdsPuestos.Locate( 'PU_CODIGO', sCodigo, [ loPartialKey ] );
end;

{ ******** cdsCompetencia ********* }

procedure TdmPlanCarrera.cdsCompetenciaAlAdquirirDatos(Sender: TObject);
begin
     cdsTCompetencia.Conectar;
     cdsCompetencia.Data := ServerPlanCarrera.GetCatHabilidades( dmCliente.Empresa );
end;

procedure TdmPlanCarrera.cdsCompetenciaAlCrearCampos(Sender: TObject);
begin
     cdsCompetencia.CreateSimpleLookup( cdsTCompetencia, 'TC_DESCRIP', 'TC_CODIGO' );
end;

procedure TdmPlanCarrera.cdsCompetenciaAlModificar(Sender: TObject);
var
   sCodigo: string;
begin
     cdsEditCompetencia.Refrescar;
     sCodigo := cdsCompetencia.FieldByName( 'CM_CODIGO' ).AsString;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCompetencias_DevEx, TEditCompetencias_DevEx );
     cdsCompetencia.Locate( 'CM_CODIGO', sCodigo, [ loPartialKey ] );
end;

procedure TdmPlanCarrera.cdsCompetenciaAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerPlanCarrera.GrabaCompetencia( dmCliente.Empresa, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmPlanCarrera.cdsCompetenciaAlAgregar(Sender: TObject);
begin
     cdsEditCompetencia.Agregar;
end;

{ ******* cdsEditNiveles ********* }

procedure TdmPlanCarrera.cdsEditNivelesAlAdquirirDatos(Sender: TObject);
{
var
   oNivelDimension: OleVariant;
}
begin
     {
     cdsDimensiones.Conectar;
     cdsNiveles.Conectar;
     cdsEditNiveles.Data := ServerPlanCarrera.GetNiveles( dmCliente.Empresa, cdsNiveles.FieldByName( 'NP_CODIGO' ).AsString, oNivelDimension );
     cdsNivelDimension.Data := oNivelDimension;
     }
     GetEditNiveles( cdsNiveles.FieldByName( 'NP_CODIGO' ).AsString );
end;

procedure TdmPlanCarrera.GetEditNiveles( const sNivel: String );
var
   oNivelDimension: OleVariant;
begin
     cdsDimensiones.Conectar;
     cdsNiveles.Conectar;
     cdsEditNiveles.Data := ServerPlanCarrera.GetNiveles( dmCliente.Empresa, sNivel, oNivelDimension );
     cdsNivelDimension.Data := oNivelDimension;
end;

procedure TdmPlanCarrera.cdsEditNivelesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oNivelDim: OleVariant;
   iPos: String;
begin
     ErrorCount := 0;
     with cdsNivelDimension do
     begin
          PostData;
     end;
     with cdsEditNiveles do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) or ( cdsNivelDimension.ChangeCount > 0 ) then
          begin
               iPos := cdsEditNiveles.FieldByName('NP_CODIGO').asString;
               if ( Reconcile( ServerPlanCarrera.GrabaCatNiveles( dmCliente.Empresa, DeltaNull, cdsNivelDimension.DeltaNull, ErrorCount, oNivelDim ) ) )and
                  ( (  cdsNivelDimension.ChangeCount = 0 ) or cdsNivelDimension.Reconcile( oNivelDim ) ) then
               begin
                    TressShell.SetDataChange( [ enNivelesPuesto ] );
                    cdsEditNiveles.MergeChangeLog;
               end
               else
               begin
                     with cdsEditNiveles do
                     begin
                          if ( ChangeCount > 0 ) or ( cdsNivelDimension.ChangeCount > 0 ) then
                          begin
                               Edit;
                          end;
                     end;
               end;

               if ( cdsNiveles.recordcount >0 ) then
                  cdsNiveles.Locate('NP_CODIGO',iPos,[ loPartialKey ]);
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditNivelesBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsNiveles do
     begin
          if Reconcile( ServerPlanCarrera.BorraNivel( dmCliente.Empresa,
                        FieldByName( 'NP_CODIGO' ).AsString,
                        ErrorCount ) ) then
          begin
               TressShell.SetDataChange( [ enNivelesPuesto ] );
               cdsEditNiveles.MergeChangeLog;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditNivelesAlAgregar(Sender: TObject);
begin
     GetEditNiveles( VACIO );
     cdsEditNiveles.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditNiveles_DevEx, TEditNiveles_DevEx );
end;

procedure TdmPlanCarrera.cdsEditNivelesBeforeInsert(DataSet: TDataSet);
begin
     cdsCompetenciaBeforeInsert(DataSet);
     cdsNivelDimension.EmptyDataSet;
end;

procedure TdmPlanCarrera.cdsEditNivelesBeforePost(DataSet: TDataSet);
begin
     ValidaCodigoLleno( cdsEditNiveles, 'NP_CODIGO' );
     ValidaDescripcionLlena( cdsEditNiveles, 'NP_DESCRIP' );
end;

{ ******** cdsNivelDimension ******* }

procedure TdmPlanCarrera.cdsNivelDimensionAfterInsert(DataSet: TDataSet);
begin
     with cdsEditNiveles do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmPlanCarrera.cdsNivelDimensionNewRecord(DataSet: TDataSet);
begin
     {
     if ( cdsEditNiveles.State = dsBrowse ) then
          cdsEditNiveles.Edit;
     }
     with cdsNivelDimension do
     begin
          FieldByName( 'NP_CODIGO' ).AsString := cdsEditNiveles.FieldByName( 'NP_CODIGO' ).AsString;
     end;
end;

procedure TdmPlanCarrera.cdsNivelDimensionAlCrearCampos(Sender: TObject);
begin
     with cdsNivelDimension do
     begin
          CreateSimpleLookup( cdsDimensiones, 'DM_DESCRIP', 'DM_CODIGO' );
     end;
end;

{ ******** cdsEditFamilia ******** }

procedure TdmPlanCarrera.cdsEditFamiliaAlAdquirirDatos(Sender: TObject);
{
var
   oCompetenciaFamilia: OleVariant;
}
begin
     cdsCompetencia.Conectar;
     {
     cdsFamilias.Conectar;
     cdsEditFamilia.Data := ServerPlanCarrera.GetFamilia( dmCliente.Empresa, cdsFamilias.FieldByName( 'FP_CODIGO' ).AsString, oCompetenciaFamilia );
     cdsCompetenciaFamilia.Data := oCompetenciaFamilia;
     }
     GetEditFamilia( cdsFamilias.FieldByName( 'FP_CODIGO' ).AsString );
end;

procedure TdmPlanCarrera.GetEditFamilia( const sFamilia: string );
var
   oCompetenciaFamilia: OleVariant;
begin
     cdsFamilias.Conectar;
     cdsEditFamilia.Data := ServerPlanCarrera.GetFamilia( dmCliente.Empresa, sFamilia, oCompetenciaFamilia );
     cdsCompetenciaFamilia.Data := oCompetenciaFamilia;
end;

procedure TdmPlanCarrera.cdsEditFamiliaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oCompFam: OleVariant;
   iPos: String;
begin
     ErrorCount := 0;
     with cdsCompetenciaFamilia do
     begin
          PostData;
     end;
     with cdsEditFamilia do
     begin
          PostData;
          if ( ChangeCount > 0 ) or
             ( cdsCompetenciaFamilia.ChangeCount > 0 ) then
          begin
               iPos := cdsEditFamilia.FieldByName('FP_CODIGO').asString;
               if ( Reconcile( ServerPlanCarrera.GrabaCatFamilias( dmCliente.Empresa, DeltaNull, cdsCompetenciaFamilia.DeltaNull, ErrorCount, oCompFam ) ) ) and
                  ( (  cdsCompetenciaFamilia.ChangeCount = 0 ) or cdsCompetenciaFamilia.Reconcile( oCompFam ) ) then
               begin
                    TressShell.SetDataChange( [ enFamiliasPuesto ] );
                    cdsEditFamilia.MergeChangeLog;
               end
               else
               begin
                     with cdsEditFamilia do
                     begin
                          if ( ChangeCount > 0 ) or ( cdsCompetenciaFamilia.ChangeCount > 0 ) then
                          begin
                               Edit;
                          end;
                     end;
               end;

               if ( cdsEditFamilia.recordcount >0 ) then
                  cdsEditFamilia.Locate('FP_CODIGO',iPos,[ loPartialKey ]);
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditFamiliaBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsFamilias do
     begin
          if Reconcile( ServerPlanCarrera.BorraFamilia( dmCliente.Empresa,
                                                        FieldByName( 'FP_CODIGO' ).AsString,
                                                        ErrorCount ) ) then
          begin
               TressShell.SetDataChange( [ enFamiliasPuesto ] );
               cdsEditFamilia.MergeChangeLog;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditFamiliaAlAgregar(Sender: TObject);
begin
     GetEditFamilia( VACIO );
     cdsEditFamilia.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditFamilias_DevEx, TEditFamilias_DevEx );
end;

procedure TdmPlanCarrera.cdsEditFamiliaBeforeInsert(DataSet: TDataSet);
begin
     cdsCompetenciaBeforeInsert(DataSet);
     cdsCompetenciaFamilia.EmptyDataSet;
end;

procedure TdmPlanCarrera.cdsEditFamiliaBeforePost(DataSet: TDataSet);
begin
     ValidaCodigoLleno( cdsEditFamilia, 'FP_CODIGO' );
     ValidaDescripcionLlena( cdsEditFamilia, 'FP_DESCRIP' );
end;

{ ******** cdsCompetenciaFamilia ******** }

procedure TdmPlanCarrera.cdsCompetenciaFamiliaAfterInsert(DataSet: TDataSet);
begin
     if ( cdsEditFamilia.State = dsBrowse ) then
          cdsEditFamilia.Edit;
end;

procedure TdmPlanCarrera.cdsCompetenciaFamiliaNewRecord(DataSet: TDataSet);
begin
     {
     if ( cdsEditFamilia.State = dsBrowse ) then
          cdsEditFamilia.Edit;
     }
     with cdsCompetenciaFamilia do
     begin
          FieldByName( 'FP_CODIGO' ).AsString := cdsEditFamilia.fieldByName( 'FP_CODIGO' ).AsString;
     end;
end;

{ ******** cdsEditPuesto ******** }

procedure TdmPlanCarrera.cdsEditPuestoAlAdquirirDatos(Sender: TObject);
begin
     cdsPuestos.Conectar;
     GetEditPuestos( cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString );
end;

procedure TdmPlanCarrera.cdsEditPuestoAlCrearCampos(Sender: TObject);
begin
     with cdsEditPuesto do
     begin
          CreateSimpleLookup( cdsFamilias, 'FP_DESCRIP', 'FP_CODIGO' );
          CreateSimpleLookup( cdsNiveles, 'NP_DESCRIP', 'NP_CODIGO' );
     end;
end;

procedure TdmPlanCarrera.GetEditPuestos( const sCodPuesto: string );
var
   oCompetenciaPuesto, oPuestoDimension: OleVariant;
begin
     cdsDimensiones.Conectar;
     cdsCompetencia.Conectar;
     cdsFamilias.Conectar;
     cdsNiveles.Conectar;
     cdsEditPuesto.Data := ServerPlanCarrera.GetPuestos( dmCliente.Empresa, sCodPuesto, oCompetenciaPuesto, oPuestoDimension );
     cdsCompetenciaPuesto.Data := oCompetenciaPuesto;
     cdsPuestoDimension.Data := oPuestoDimension;
end;

procedure TdmPlanCarrera.cdsEditPuestoAlAgregar(Sender: TObject);
begin
     GetEditPuestos( VACIO );
     cdsEditPuesto.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditPuestos_DevEx, TEditPuestos_DevEx );
end;

procedure TdmPlanCarrera.cdsEditPuestoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oCompPto, oPtoDim: OleVariant;
   iPos: String;
begin
     ErrorCount := 0;
     with cdsCompetenciaPuesto do
     begin
          PostData;
     end;
     with cdsPuestoDimension do
     begin
          PostData;
     end;
     with cdsEditPuesto do
     begin
          PostData;
          if ( ChangeCount > 0 ) or
             ( cdsCompetenciaPuesto.ChangeCount > 0 ) or
             ( cdsPuestoDimension.ChangeCount > 0 ) then
          begin
                iPos := cdsEditPuesto.FieldByName('PU_CODIGO').asString;
                if ( Reconcile( ServerPlanCarrera.GrabaCatPuestos( dmCliente.Empresa, DeltaNull, cdsCompetenciaPuesto.DeltaNull, cdsPuestoDimension.DeltaNull, ErrorCount, oCompPto, oPtoDim ) ) )and
                   ( (  cdsCompetenciaPuesto.ChangeCount = 0 ) or cdsCompetenciaPuesto.Reconcile( oCompPto ) ) and
                   ( (  cdsPuestoDimension.ChangeCount = 0 ) or cdsPuestoDimension.Reconcile( oPtoDim ) ) then
               begin
                    TressShell.SetDataChange( [ enPuesto ] );
                    cdsEditPuesto.MergeChangeLog;
               end
               else
               begin
                     with cdsEditPuesto do
                     begin
                          if ( ChangeCount > 0 ) or ( cdsCompetenciaPuesto.ChangeCount > 0 ) or ( cdsPuestoDimension.ChangeCount > 0 ) then
                          begin
                               Edit;
                          end;
                     end;
               end;

               if ( cdsPuestos.recordcount >0 ) then
                  cdsPuestos.Locate('PU_CODIGO',iPos,[ loPartialKey ]);
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditPuestoBeforePost(DataSet: TDataSet);
begin
     with cdsEditPuesto do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'FP_CODIGO' ).AsString ) then
             DataBaseError( 'El c�digo de familia no puede quedar vac�o' );
          if ZetaCommonTools.StrVacio( FieldByName( 'NP_CODIGO' ).AsString ) then
             DataBaseError( 'El c�digo de nivel no puede quedar vac�o' );
     end;
     with cdsCompetenciaPuesto do
     begin
          while not EOF do
          begin
               if StrVacio( FieldByName( 'CA_CODIGO' ).AsString ) then
               begin
                    DataBaseError( 'La calificaci�n no puede quedar vac�a' );
                    Break;
               end;
               Next;
          end;
     end;
end;

{ ******** cdsEditCompetencia ******** }

procedure TdmPlanCarrera.cdsEditCompetenciaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oCompMapa, oCompCalifica: OleVariant;
   iPos : String;
begin
     ErrorCount := 0;
     with cdsCompetenciaMapa do
     begin
          DisableControls;
          try
             PostData;
          finally
                 EnableControls;
          end;
          //CompetenciaMapaRenumera;
     end;
     with cdsCompetenciaCalifica do
     begin
          PostData;
     end;
     with cdsEditCompetencia do
     begin
          PostData;
          if ( ChangeCount > 0 ) or
             ( cdsCompetenciaMapa.ChangeCount > 0 ) or
             ( cdsCompetenciaCalifica.ChangeCount > 0 ) then
          begin
               iPos := cdsEditCompetencia.FieldByName('CM_CODIGO').asString;
               if ( Reconcile( ServerPlanCarrera.GrabaCatCompetencia( dmCliente.Empresa, DeltaNull, cdsCompetenciaMapa.DeltaNull, cdsCompetenciaCalifica.DeltaNull, ErrorCount, oCompMapa, oCompCalifica ) ) )and
                  ( (  cdsCompetenciaMapa.ChangeCount = 0 ) or cdsCompetenciaMapa.Reconcile( oCompMapa ) ) and
                  ( (  cdsCompetenciaCalifica.ChangeCount = 0 ) or cdsCompetenciaCalifica.Reconcile( oCompCalifica ) ) then
               begin
                    TressShell.SetDataChange( [ enCompetencias ] );
                    cdsEditCompetencia.MergeChangeLog;
               end
               else
               begin
                     with cdsEditCompetencia do
                     begin
                          if ( ChangeCount > 0 ) or ( cdsCompetenciaMapa.ChangeCount > 0 ) or ( cdsCompetenciaCalifica.ChangeCount > 0 ) then
                          begin
                               Edit;
                          end;
                     end;
               end;

               if ( cdsCompetencia.recordcount >0 ) then
                  cdsCompetencia.Locate('CM_CODIGO',iPos,[ loPartialKey ]);
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditCompetenciaAlAdquirirDatos( Sender: TObject);
begin
     cdsCompetencia.Conectar;
     GetEditCompetencia( cdsCompetencia.FieldByName( 'CM_CODIGO' ).AsString );
end;

procedure TdmPlanCarrera.cdsEditCompetenciaAlAgregar(Sender: TObject);
begin
     GetEditCompetencia( VACIO );
     cdsEditCompetencia.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCompetencias_DevEx, TEditCompetencias_DevEx );
end;

procedure TdmPlanCarrera.GetEditCompetencia( const sCompetencia: string );
var
   oCompetenciaMapa, oCompetenciaCalifica: OleVariant;
begin
     cdsCalificaciones.Conectar;
     cdsAcciones.Conectar;
     cdsTCompetencia.Conectar;
     cdsEditCompetencia.Data := ServerPlanCarrera.GetCatCompetencia( dmCliente.Empresa, sCompetencia, oCompetenciaMapa, oCompetenciaCalifica );
     cdsCompetenciaMapa.Data := oCompetenciaMapa;
     cdsCompetenciaCalifica.Data := oCompetenciaCalifica;
end;

procedure TdmPlanCarrera.cdsEditCompetenciaBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsCompetencia do
     begin
          if Reconcile( ServerPlanCarrera.BorraCompetencia( dmCliente.Empresa,
                                                            FieldByName( 'CM_CODIGO' ).AsString,
                                                            ErrorCount ) ) then
          begin
               TressShell.SetDataChange( [ enCompetencias ] );
               cdsEditCompetencia.MergeChangeLog;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditCompetenciaBeforeInsert(DataSet: TDataSet);
begin
     cdsCompetenciaBeforeInsert(DataSet);
     cdsCompetenciaCalifica.EmptyDataSet;
     cdsCompetenciaMapa.EmptyDataSet;
end;

procedure TdmPlanCarrera.cdsEditCompetenciaBeforePost(DataSet: TDataSet);
begin
     ValidaCodigoLleno( cdsEditCompetencia, 'CM_CODIGO' );
     ValidaDescripcionLlena( cdsEditCompetencia, 'CM_DESCRIP' );
     with cdsEditCompetencia do
     begin
          if ( State = dsEdit ) and
             CambiaCampo( FieldByName( 'CM_CODIGO' ) ) and
             CambiaCampo( FieldByName( 'CM_DETALLE' ) ) then
          begin
               DataBaseError( 'El C�digo y la Descripci�n de Funciones no pueden ser modificados simult�neamente' );
          end;
          if ZetaCommonTools.StrVacio( FieldByName( 'TC_CODIGO' ).AsString ) then
             DataBaseError( 'El Tipo De Competencia No Puede Quedar Vac�o' );
     end;
     LlenaCampoMemo( cdsEditCompetencia, 'CM_DETALLE' );
end;

{ ****** cdsCompetenciaAccion ********* }

procedure TdmPlanCarrera.cdsCompetenciaAccionAfterInsert( DataSet: TDataSet);
begin
     with cdsEditAcciones do
     begin
          if ( State = dsBrowse ) then
               Edit;
     end;
end;

procedure TdmPlanCarrera.cdsCompetenciaAccionNewRecord(DataSet: TDataSet);
begin
     {
     with cdsEditAcciones do
     begin
          if ( State = dsBrowse ) then
               Edit;
     end;
     }
     with cdsCompetenciaAccion do
     begin
          FieldByName( 'AN_CODIGO' ).AsString := cdsEditAcciones.fieldByName( 'AN_CODIGO' ).AsString;
     end;
end;

{ ****** cdsCompetenciaCalifica ********* }

procedure TdmPlanCarrera.cdsCompetenciaCalificaAfterInsert(DataSet: TDataSet);
begin
     with cdsEditCompetencia do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmPlanCarrera.cdsCompetenciaCalificaNewRecord(DataSet: TDataSet);
begin
     {
     if ( cdsEditCompetencia.State = dsBrowse ) then
          cdsEditCompetencia.Edit;
     }
     with cdsCompetenciaCalifica do
     begin
          FieldByName( 'CM_CODIGO' ).AsString := cdsEditCompetencia.FieldByName( 'CM_CODIGO' ).AsString;
     end;
end;

procedure TdmPlanCarrera.cdsCompetenciaCalificaAlCrearCampos(Sender: TObject);
begin
     with cdsCompetenciaCalifica do
     begin
          CreateSimpleLookup( cdsCalificaciones, 'CA_DESCRIP', 'CA_CODIGO' );
     end;
end;

{ ******* cdsCompetenciaMapa ******** }

procedure TdmPlanCarrera.CompetenciaMapaRenumera;
var
   i: Integer;
   sIndexFieldNames: String;
begin
     with cdsCompetenciaMapa do
     begin
          DisableControls;
          try
             i := 1;
             sIndexFieldNames := IndexFieldNames;
             IndexFieldNames := '';
             First;
             while not Eof do
             begin
                  with FieldByName( 'CM_ORDEN' ) do
                  begin
                       if ( AsInteger <> i ) then
                       begin
                            Edit;
                            AsInteger := i;
                            Post;
                       end;
                       Next;
                       Inc( i );
                  end;
             end;
             IndexFieldNames := sIndexFieldNames;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsCompetenciaMapaAlCrearCampos(Sender: TObject);
begin
     with cdsCompetenciaMapa do
     begin
          CreateSimpleLookup( cdsAcciones, 'AN_NOMBRE', 'AN_CODIGO' );
     end;
end;

procedure TdmPlanCarrera.cdsCompetenciaMapaAfterOpen(DataSet: TDataSet);
begin
     CompetenciaMapaRenumera;
end;

procedure TdmPlanCarrera.cdsCompetenciaMapaNewRecord(DataSet: TDataSet);
begin
     {
     with cdsEditCompetencia do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
     }
     with cdsCompetenciaMapa do
     begin
          FieldByName( 'CM_CODIGO' ).AsString := cdsEditCompetencia.FieldByName( 'CM_CODIGO' ).AsString;
     end;
end;

function TdmPlanCarrera.SubeBajaAcciones( const lSubir: Boolean ): Boolean;
const
     K_TEMP_ORDEN = 10000;
var
   iOriginal, iVecino: Integer;

procedure CambiaOrden( const iAntes, iDespues: Integer );
begin
     with cdsCompetenciaMapa do
     begin
          if ( Locate( 'CM_ORDEN', iAntes, [] ) ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
               begin
                    Edit;
               end;
               FieldByName( 'CM_ORDEN' ).AsInteger := iDespues;
               Post;
          end
     end;
end;

begin
     Result := True;
     with cdsCompetenciaMapa do
     begin
          DisableControls;
          try
             if IsEmpty or ( lSubir and Bof ) or ( not lSubir and Eof ) then
             begin
                  Beep;
                  Result := False;
             end
             else
             begin
                  { Averigua el orden del registro actual }
                  iOriginal := FieldByName( 'CM_ORDEN' ).AsInteger;
                  { Se mueve al registro contiguo }
                  if lSubir then
                     Prior  { Cuando es subir, el contiguo es el de arriba }
                  else
                      Next; { Cuando es bajar, el contiguo es el de abajo }
                  { Averigua el orden del registro contiguo }
                  iVecino := FieldByName( 'CM_ORDEN' ).AsInteger;
                  { Asigna un orden temporal al registro original }
                  CambiaOrden( iOriginal, K_TEMP_ORDEN );
                  { Asigna el orden original al registro contiguo }
                  CambiaOrden( iVecino, iOriginal );
                  { Asigna el orden del registro contiguo al registro original }
                  CambiaOrden( K_TEMP_ORDEN, iVecino );
             end;
          finally
                 EnableControls;
          end;
     end;
end;

{ ******* cdsCompetenciaPuesto ******** }

procedure TdmPlanCarrera.cdsCompetenciaPuestoAlCrearCampos(Sender: TObject);
begin
     with cdsCompetenciaPuesto do
     begin
          CreateSimpleLookup( cdsCompetencia, 'CM_DESCRIP', 'CM_CODIGO' );
          with FieldByName( 'CA_CODIGO' ) do
          begin
               OnValidate := CA_CODIGOValidate;
               //OnChange := EmpTO_CODIGOChange; /*En Caso de que quieran la descripci�n de la Calificaci�n
          end;
     end;
end;

procedure TdmPlanCarrera.cdsCompetenciaPuestoAfterInsert(DataSet: TDataSet);
begin
     if ( cdsEditPuesto.State = dsBrowse ) then
          cdsEditPuesto.Edit;
end;

procedure TdmPlanCarrera.cdsCompetenciaPuestoNewRecord(DataSet: TDataSet);
begin
     {
     cdsCalificaciones.Conectar;
     if ( cdsEditPuesto.State = dsBrowse ) then
          cdsEditPuesto.Edit;
     }
     with cdsCompetenciaPuesto do
     begin
          FieldByName( 'PU_CODIGO' ).AsString := cdsEditPuesto.FieldByName( 'PU_CODIGO' ).AsString;
          //if( strLleno(cdsCalificaciones.FieldByName('CA_CODIGO').AsString)  )then
          if not ( cdsCalificaciones.IsEmpty )then
             FieldByName( 'CA_CODIGO' ).AsString := cdsCalificaciones.FieldByName('CA_CODIGO').AsString;
     end;
end;

{ ****** cdsPuestoDimension ******* }

procedure TdmPlanCarrera.cdsPuestoDimensionAlCrearCampos(Sender: TObject);
begin
     with cdsPuestoDimension do
     begin
          CreateSimpleLookup( cdsDimensiones, 'DM_DESCRIP', 'DM_CODIGO' );
     end;
end;

procedure TdmPlanCarrera.cdsPuestoDimensionNewRecord(DataSet: TDataSet);
begin
     {
     if ( cdsEditPuesto.State = dsBrowse ) then
          cdsEditPuesto.Edit;
     }
     with cdsPuestoDimension do
     begin
          FieldByName( 'PU_CODIGO' ).AsString := cdsEditPuesto.fieldByName( 'PU_CODIGO' ).AsString;
     end;
end;

{ ******** cdsEditAcciones ******** }

procedure TdmPlanCarrera.cdsEditAccionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCompetencia.Conectar;
     GetEditAccion( cdsAcciones.FieldByName( 'AN_CODIGO' ).AsString );
end;

procedure TdmPlanCarrera.GetEditAccion( const sAccion: string );
var
   oCompetenciaAccion: OleVariant;
begin
     cdsAcciones.Conectar;
     cdsEditAcciones.Data := ServerPlanCarrera.GetAccionesComp( dmCliente.Empresa, sAccion, oCompetenciaAccion );
     cdsCompetenciaAccion.Data := oCompetenciaAccion;
end;

procedure TdmPlanCarrera.cdsEditAccionesAlAgregar(Sender: TObject);
begin
     GetEditAccion( VACIO );
     cdsEditAcciones.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditAcciones_DevEx, TEditAcciones_DevEx );
end;

procedure TdmPlanCarrera.cdsEditAccionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oCompAcc: OleVariant;
   iPos : String;
begin
     ErrorCount := 0;
     with cdsCompetenciaAccion do
     begin
          PostData;
     end;
     with cdsEditAcciones do
     begin
          PostData;
          if ( ChangeCount > 0 ) or
             ( cdsCompetenciaAccion.ChangeCount > 0 ) then
          begin
               iPos := cdsEditAcciones.FieldByName('AN_CODIGO').asString;
               if ( Reconcile( ServerPlanCarrera.GrabaAccionesComp( dmCliente.Empresa, DeltaNull, cdsCompetenciaAccion.DeltaNull, ErrorCount, oCompAcc ) ) ) and
                  ( (  cdsCompetenciaAccion.ChangeCount = 0 ) or cdsCompetenciaAccion.Reconcile( oCompAcc ) ) then
               begin
                    TressShell.SetDataChange( [ enAcciones ] );
                    cdsEditAcciones.MergeChangeLog;
               end
               else
               begin
                     with cdsEditAcciones do
                     begin
                          if ( ChangeCount > 0 ) or ( cdsEditAcciones.ChangeCount > 0 ) then
                          begin
                               Edit;
                          end;
                     end;
               end;

               if ( cdsAcciones.recordcount >0 ) then
                  cdsAcciones.Locate('AN_CODIGO',iPos,[]);
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditAccionesBeforeDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsAcciones do
     begin
          if Reconcile( ServerPlanCarrera.BorraAccion( dmCliente.Empresa,
                                                        FieldByName( 'AN_CODIGO' ).AsString,
                                                        ErrorCount ) ) then
          begin
               TressShell.SetDataChange( [ enAcciones ] );
               cdsEditAcciones.MergeChangeLog;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsEditAccionesBeforeInsert(DataSet: TDataSet);
begin
     cdsCompetenciaBeforeInsert(DataSet);
     cdsCompetenciaAccion.EmptyDataSet;
end;

procedure TdmPlanCarrera.cdsEditAccionesBeforePost(DataSet: TDataSet);
begin
     ValidaCodigoLleno( cdsEditAcciones, 'AN_CODIGO' );
     ValidaDescripcionLlena( cdsEditAcciones, 'AN_NOMBRE', 'El nombre no puede quedar vac�o' );
     with cdsEditAcciones do
     begin
          if ( State = dsEdit ) and
             CambiaCampo( FieldByName( 'AN_CODIGO' ) ) and
             CambiaCampo( FieldByName( 'AN_DETALLE' ) ) then
          begin
               DataBaseError( 'El C�digo y la Descripci�n no pueden ser modificados simult�neamente' );
          end;
     end;
     LlenaCampoMemo( cdsEditAcciones, 'AN_DETALLE' );

end;

function TdmPlanCarrera.ValidaModuloPlanCarrera:Boolean;
begin
     Result := dmCliente.ModuloAutorizado(okPlanCarrera);
     If Result then
     begin
          if ( Autorizacion.EsDemo ) then
          begin
               DataBaseError('M�dulo de Plan de Carrera No Autorizado');
               Result := False;
          end;
     end;
end;

procedure TdmPlanCarrera.cdsCompetenciaBeforeInsert(DataSet: TDataSet);
begin
     if not ValidaModuloPlanCarrera then
        (TZetaClientDataSet(DataSet)).Cancel; 
end;

end.

