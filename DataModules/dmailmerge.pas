unit DMailMerge;

interface


uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    {$ifndef VER130}
    Variants, WordXP,
    {$endif}
    OleServer {$IFNDEF TRESS_DELPHIXE5_UP}, Word97 {$ENDIF};  //(@am): La unidad Word97 ya no existe en la instalacion de XE5

type
  TdmMailMerge = class(TDataModule)
    WordApp: TWordApplication;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Ejecuta( sDocFile, sDataFile : oleVariant);
    procedure Imprime( sDocFile, sDataFile : oleVariant);
  end;

var
  dmMailMerge: TdmMailMerge;

implementation

{$R *.DFM}

{ TdmMailMerge }


procedure TdmMailMerge.Ejecuta( sDocFile, sDataFile : oleVariant );
begin
    {***(@am): La unidad Word97 ya no existe en la instalacion de XE5.
               Por lo tanto se utiliza WordXP, el cual necesita mas parametros. ***}
     with WordApp do
     begin
          ConnectKind := ckRunningOrNew;    // Por si corren preliminar varias veces
          Connect;
          Visible := TRUE;                  // Para que usuario pueda ver preliminar
          Documents.Open( sDocFile, EmptyParam, EmptyParam, EmptyParam,
                          EmptyParam, EmptyParam, EmptyParam, EmptyParam,
                          EmptyParam, EmptyParam,
                          {$IFDEF TRESS_DELPHIXE5_UP}EmptyParam, EmptyParam, EmptyParam,EmptyParam,EmptyParam {$ENDIF});

          if ( ActiveDocument.ActiveWindow.View.type_ >= 7 ) then    // Lo abrió en modo wdReadingView - tipo no disponible en WordXP unit
             ActiveDocument.ActiveWindow.View.type_ := wdPrintView;  // Es el default en vista de impresión

          with ActiveDocument.MailMerge do
          begin
               OpenDataSource( sDataFile, EmptyParam, EmptyParam, EmptyParam,
                               EmptyParam, EmptyParam, EmptyParam, EmptyParam,
                               EmptyParam, EmptyParam, EmptyParam, EmptyParam,
                               EmptyParam, EmptyParam,
                               {$IFDEF TRESS_DELPHIXE5_UP}EmptyParam, EmptyParam {$ENDIF});
               ViewMailMergeFieldCodes := 0;   // Hace el 'merge' visible en pantalla
          end;
          Activate;                         // Pone 'Word' como aplicación activa arriba de la nuestra
          Disconnect;                       // Se desconecta de Word, mas no lo cierra
     end;
end;


procedure TdmMailMerge.Imprime( sDocFile, sDataFile : oleVariant );
 var
    lGrabaCambios: OleVariant;

begin
     {***(@am): La unidad Word97 ya no existe en la instalacion de XE5.
               Por lo tanto se utiliza WordXP, el cual necesita mas parametros. ***}
     with WordApp do
     begin
          ConnectKind := ckRunningOrNew;    // Por si corren preliminar varias veces
          Connect;
          Visible := TRUE;                  // Para que usuario pueda ver preliminar
          Documents.Open( sDocFile, EmptyParam, EmptyParam, EmptyParam,
                          EmptyParam, EmptyParam, EmptyParam, EmptyParam,
                          EmptyParam, EmptyParam,
                          {$IFDEF TRESS_DELPHIXE5_UP}EmptyParam, EmptyParam, EmptyParam,EmptyParam,EmptyParam {$ENDIF});
          with ActiveDocument.MailMerge do
          begin
               OpenDataSource( sDataFile, EmptyParam, EmptyParam, EmptyParam,
                               EmptyParam, EmptyParam, EmptyParam, EmptyParam,
                               EmptyParam, EmptyParam, EmptyParam, EmptyParam,
                               EmptyParam, EmptyParam,
                               {$IFDEF TRESS_DELPHIXE5_UP} EmptyParam, EmptyParam {$ENDIF});
               ViewMailMergeFieldCodes := 0;   // Hace el 'merge' visible en pantalla
          end;
          Activate;                         // Pone 'Word' como aplicación activa arriba de la nuestra
          PrintOut;

          Sleep(5000);

          lGrabaCambios := FALSE;
          Quit( lGrabaCambios );

     end;
end;

end.




