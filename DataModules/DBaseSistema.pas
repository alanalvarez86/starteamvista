unit DBaseSistema;

interface

{$DEFINE MULTIPLES_ENTIDADES}
{$DEFINE SISNOM_CLAVES}
{$INCLUDE DEFINES.INC}
{$ifdef TRESS_DELPHIXE5_UP}
{$DEFINE SUSCRIPCION}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, CheckLst,
     ZetaCommonLists,
     ZAccesosMgr,
     ZetaTipoEntidad,
     {$ifdef DOS_CAPAS}
     DServerSistema,
     {$else}
     Sistema_TLB,
     {$endif}
     {$ifndef VER130}
     Variants,
     {$endif}
     ZetaClientDataSet,
     ZetaServerDataSet,
     ComCtrls,
     ZetaCommonClasses,
     ZetaKeyCombo,
     ZetaClientTools;

type
  eArbolDerechos = (adTress,adClasifi,adEntidades,adAdicionales);
  TGetTextoBitacora = function ( Nodo: TTreeNode; const iDerecho: TDerechosAcceso): String of Object;
  TGrupoEmpresa = class( TObject )
  private
    { Private declarations }
    FGrupo: Integer;
    FGrupoName: String;
    FEmpresa: String;
    FEmpresaName: String;
  public
    { Public declarations }
    property Grupo: Integer read FGrupo write FGrupo;
    property GrupoName: String read FGrupoName write FGrupoName;
    property Empresa: String read FEmpresa write FEmpresa;
    property EmpresaName: String read FEmpresaName write FEmpresaName;
    function GetGroupDescription: String;
    function GetEmpresaDescription: String;
  end;
  TGruposEmpresas = class( TObject )
  private
    { Private declarations }
    FLista: TList;
    function GetItem( Index: Integer ): TGrupoEmpresa;
    procedure Delete(const Index: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Items[ Index: Integer ]: TGrupoEmpresa read GetItem;
    function Add: TGrupoEmpresa;
    function Count: Integer;
    procedure Clear;
  end;
  TdmBaseSistema = class(TDataModule)
    cdsEmpresas: TZetaLookupDataSet;
    cdsGrupos: TZetaLookupDataSet;
    cdsUsuarios: TZetaLookupDataSet;
    cdsImpresoras: TZetaClientDataSet;
    cdsAccesosBase: TZetaClientDataSet;
    cdsEmpresasLookUp: TZetaLookupDataSet;
    cdsSuscrip: TZetaClientDataSet;
    cdsCopiarAccesos: TZetaClientDataSet;
    cdsGruposLookup: TZetaLookupDataSet;
    cdsEmpresasAccesos: TZetaLookupDataSet;
    cdsDerechos: TServerDataSet;
    cdsUsuariosLookup: TZetaLookupDataSet;
    cdsClasifiRepEmp: TZetaLookupDataSet;
    cdsRoles: TZetaLookupDataSet;
    cdsUserRoles: TZetaLookupDataSet;
    cdsGruposTodos: TZetaLookupDataSet;
    cdsSistBaseDatos: TZetaLookupDataSet;
    cdsSistBaseDatosLookUp: TZetaLookupDataSet;
    cdsBasesDatos: TZetaClientDataSet;
    cdsSuscripUsuarioCalendario: TZetaClientDataSet;
    cdsUsuariosSeguridad: TZetaLookupDataSet;
    cdsUsuariosSeguridadLookup: TZetaLookupDataSet;

    cdsSistSolicitudEnvios: TZetaClientDataSet;
    cdsSistTareaCalendario: TZetaClientDataSet;
    cdsSistTareaUsuario: TZetaClientDataSet;
    cdsSistTareaRoles: TZetaClientDataSet;
    cdsSuscripCalendarioReportes: TZetaClientDataSet;
    cdsEnviosProgramadosEmpresa: TZetaClientDataSet;
    cdsSistTareaCalendarioLookup: TZetaLookupDataSet;
    cdsEmpresasBuildLookup: TZetaLookupDataSet;
    cdsSistEnvioEmail: TZetaClientDataSet;
    cdsBitacoraReportes: TZetaClientDataSet;




    procedure DataModuleCreate(Sender: TObject);
    procedure cdsEmpresasAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpresasAlModificar(Sender: TObject);
    procedure cdsGruposAlAdquirirDatos(Sender: TObject);
    procedure cdsGruposAlModificar(Sender: TObject);
    procedure cdsUsuariosAlAdquirirDatos(Sender: TObject);
    procedure cdsImpresorasAlAdquirirDatos(Sender: TObject);
    procedure cdsImpresorasAlModificar(Sender: TObject);
    procedure cdsUsuariosAlModificar(Sender: TObject);
    procedure cdsUsuariosAfterOpen(DataSet: TDataSet);
    procedure cdsUsuariosAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosAlCrearCampos(Sender: TObject);
    procedure cdsEmpresasAlEnviarDatos(Sender: TObject);
    procedure cdsEmpresasAfterDelete(DataSet: TDataSet);
    procedure cdsEmpresasBeforePost(DataSet: TDataSet);
    procedure cdsEmpresasNewRecord(DataSet: TDataSet);
    procedure cdsImpresorasAfterDelete(DataSet: TDataSet);
    procedure cdsImpresorasAlEnviarDatos(Sender: TObject);
    procedure cdsGruposAfterDelete(DataSet: TDataSet);
    procedure cdsGruposAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosAfterDelete(DataSet: TDataSet);
    procedure cdsUsuariosBeforePost(DataSet: TDataSet);
    procedure cdsUsuariosNewRecord(DataSet: TDataSet);
    procedure cdsAccesosBaseAlEnviarDatos(Sender: TObject);
    {$ifdef VER130}
    procedure GenericReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsUsuariosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure GenericReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsUsuariosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsEmpresasLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpresasBeforeInsert(DataSet: TDataSet);
    procedure cdsUsuariosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEmpresasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsGruposGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsSuscripAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripAlCrearCampos(Sender: TObject);
    procedure cdsSuscripAlEnviarDatos(Sender: TObject);
    procedure cdsSuscripNewRecord(DataSet: TDataSet);
    procedure cdsSuscripBeforePost(DataSet: TDataSet);
    procedure cdsGruposNewRecord(DataSet: TDataSet);
    procedure cdsGruposBeforePost(DataSet: TDataSet);
    procedure cdsGruposLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsGruposBeforeInsert(DataSet: TDataSet);
    procedure cdsEmpresasAccesosAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpresasAccesosAlCrearCampos(Sender: TObject);
    procedure cdsUsuariosLookupGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsGruposTodosAlAdquirirDatos(Sender: TObject);

    { ** cdsUsuariosSeguridad ** }
    procedure cdsUsuariosSeguridadAlAdquirirDatos(Sender: TObject);
    procedure cdsUsuariosSeguridadAlModificar(Sender: TObject);
    procedure cdsUsuariosSeguridadAfterOpen(DataSet: TDataSet);
    procedure cdsUsuariosSeguridadAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosSeguridadAlCrearCampos(Sender: TObject);
    procedure cdsUsuariosSeguridadAfterDelete(DataSet: TDataSet);
    procedure cdsUsuariosSeguridadBeforePost(DataSet: TDataSet);
    procedure cdsUsuariosSeguridadNewRecord(DataSet: TDataSet);
    {$ifdef VER130}
    procedure cdsUsuariosSeguridadReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsUsuariosSeguridadReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsUsuariosSeguridadGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsUsuariosSeguridadLookupGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsSistBaseDatosAlAdquirirDatos(Sender: TObject);
    procedure cdsSistBaseDatosAlCrearCampos(Sender: TObject);    
    procedure cdsSistBaseDatosDB_CODIGOValidate(Sender: TField);  
    procedure DB_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsSistBaseDatosAlModificar(Sender: TObject);
    procedure cdsSistBaseDatosAlEnviarDatos(Sender: TObject);
    procedure cdsSistBaseDatosAfterDelete(DataSet: TDataSet);
    procedure cdsSistBaseDatosBeforePost(DataSet: TDataSet);
    procedure cdsSistBaseDatosReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsSistBaseDatosNewRecord(DataSet: TDataSet);
    procedure cdsSistBaseDatosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsSistBaseDatosLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsSistBaseDatosLookUpLookupSearch(
      Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
      var sKey, sDescription: String);
    procedure cdsEmpresasLookUpGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsSuscripCalendarioReporteReconcileError(
      DataSet: TCustomClientDataSet; E: EReconcileError;
      UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsSuscripUsuarioCalendarioAlCrearCampos(Sender: TObject);
    procedure cdsSuscripUsuarioCalendarioAlAdquirirDatos(Sender: TObject);

    procedure cdsSistTareaCalendarioAfterCancel(DataSet: TDataSet);
    procedure cdsSistTareaCalendarioAfterDelete(DataSet: TDataSet);
    procedure cdsSistTareaCalendarioAlAdquirirDatos(Sender: TObject);
    procedure cdsSistTareaCalendarioAlCrearCampos(Sender: TObject);
    procedure cdsSistTareaUsuarioAlAdquirirDatos(Sender: TObject);
    procedure cdsSistTareaRolesAlAdquirirDatos(Sender: TObject);
    procedure cdsSistTareaRolesAlEnviarDatos(Sender: TObject);
    procedure cdsSistTareaUsuarioAlEnviarDatos(Sender: TObject);
    procedure cdsSistTareaCalendarioAlModificar(Sender: TObject);
    procedure cdsSistTareaCalendarioNewRecord(DataSet: TDataSet);
    procedure cdsSistTareaCalendarioAlEnviarDatos(Sender: TObject);
    procedure cdsSistTareaCalendarioLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripCalendarioReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripCalendarioReportesAlCrearCampos(Sender: TObject);
    procedure cdsEnviosProgramadosEmpresaAlAdquirirDatos(Sender: TObject);
    procedure cdsEnviosProgramadosEmpresaAlCrearCampos(Sender: TObject);
    procedure cdsEnviosProgramadosEmpresaAlEnviarDatos(Sender: TObject);
    procedure cdsEnviosProgramadosEmpresaNewRecord(DataSet: TDataSet);
    procedure cdsEnviosProgramadosEmpresaAfterDelete(DataSet: TDataSet);
    procedure cdsEnviosProgramadosEmpresaAfterCancel(DataSet: TDataSet);
    procedure cdsEnviosProgramadosEmpresaAlModificar(Sender: TObject);
    procedure CA_NX_FECGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure DescargaListaRolesCalendario( Lista: TStrings );
    procedure DescargaListaUsuariosCalendario( Lista: TStrings );
    procedure cdsSistSolicitudEnviosAlCrearCampos(Sender: TObject);
    procedure cdsSistSolicitudEnviosAlModificar(Sender: TObject);
    procedure cdsSistEnvioEmailAlAdquirirDatos(Sender: TObject);
    procedure cdsSistEnvioEmailAlCrearCampos(Sender: TObject);
    procedure cdsSistEnvioEmailAlModificar(Sender: TObject);
    procedure cdsBitacoraReportesAfterOpen(DataSet: TDataSet);
    procedure cdsBitacoraReportesAlCrearCampos(Sender: TObject);
    procedure cdsBitacoraReportesAlModificar(Sender: TObject);
    procedure cdsEmpresasBuildLookupAlAdquirirDatos(Sender: TObject);

  private
    { Private declarations }
    FTipoBitacora: Integer;
    FEmpleadoNumero: Integer;
    FFechaInicial: TDate;
    FUltimaFrecuencia: Integer;
    FFechaFinal: TDate;
    FUsuario: Integer;
    FSiguienteGrupo: integer;
    FMaxDigito: Char;
    FBloqueoSistema: Boolean;
    FArbolSeleccionado: eArbolDerechos;
    FCodigoCopiado: string ;
    FEditandoUsuarios:Boolean;
	{$ifdef SISNOM_CLAVES}
    FClaveEncriptada: string;
	{$endif}    
    FActualizarBD: Boolean;

    {Propiedades de Tareas Calendario - Envios Programados }
    FCodigoReporte: string;
    FNombreReporte: string;
    FTituloReporte: string;
    FFolioCalendario: integer;
    FEditarCalendario: Boolean;
    FEnvioProgramadoXEmpresa: Boolean;
    FTituloProceso : Boolean;

    function GetTitulo( Nodo: TTreeNode; var iPos: Integer): String;
    procedure CambiaUsuarioFechaSalida( Sender: TField );
    procedure ActualizaFechaPassWord(Sender: TField; const Text: String);
{$ifdef SISNOM}
    procedure ActualizaFechaPassWordSeguridad(Sender: TField; const Text: String);
{$endif}
    procedure SU_FRECUENGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CA_FRECGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ACCESOSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ConectaUsuarioSuscrip( const iUsuario: integer );

  protected
    { Protected declarations }
    FTextoBitacora: String;
    FcdsAccesos: TZetaClientDataSet;

{$ifdef DOS_CAPAS}
    function GetServerSistema: TdmServerSistema;
    property ServerSistema: TdmServerSistema read GetServerSistema;
{$else}
    FServidor: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
{$endif}
    function GetcdsAccesos : TZetaClientDataSet; virtual;
    function BuscaAccesos( const iPosition: Integer ): Boolean; virtual;
  public
    { Public declarations }
    property UltimaFrecuencia: Integer read FUltimaFrecuencia;
    property FechaInicial: TDate read FFechaInicial write FFechaInicial;
    property FechaFinal: TDate read FFechaFinal write FFechaFinal;
    property TipoBitacora: Integer  read FTipoBitacora write FTipoBitacora;
    property EmpleadoNumero: Integer read FEmpleadoNumero write FEmpleadoNumero;
    property Usuario: Integer read FUsuario write FUsuario;
    property BloqueoSistema: Boolean read FBloqueoSistema;
    property ArbolSeleccionado : eArbolDerechos read FArbolSeleccionado write FArbolSeleccionado ;
    property CodigoCopiado: string read FCodigoCopiado write FCodigoCopiado;
    property EditandoUsuarios: Boolean read FEditandoUsuarios write FEditandoUsuarios;
	{$ifdef SISNOM_CLAVES}
    property ClaveEncriptada: string read FClaveEncriptada write FClaveEncriptada;
	{$endif}
    property ActualizarBD: Boolean read FActualizarBD write FActualizarBD;

    //TAREAS CALENDARIO PARA REPORTE
    property CodigoReporte: string read FCodigoReporte write FCodigoReporte;
    property NombreReporte: string read FNombreReporte write FNombreReporte;
    property TituloReporte: string read FTituloReporte write FTituloReporte;
    property FolioCalendario: integer read FFolioCalendario write FFolioCalendario;
    property EditarCalendario: Boolean read FEditarCalendario write FEditarCalendario;
    property EnvioProgramadoXEmpresa: Boolean read FEnvioProgramadoXEmpresa write FEnvioProgramadoXEmpresa;
    property TituloProceso: Boolean read FTituloProceso write FTituloProceso;


    function BuscaDerecho( const iPosition: Integer ): TDerechosAcceso; virtual;
    function BuscaDerechoCopiado( const iPosition: Integer ): TDerechosAcceso;
    function CargarGruposAccesos( Lista: TGruposEmpresas; var iDefault: Integer ): Boolean;
    function UsuarioPosicionado: Integer;
    procedure ActivaUsuarios;
{$ifdef SISNOM}
    procedure ActivaUsuariosSeguridad;
    procedure PosicionaUsuarioActivoSeguridad;
    procedure SuspendeUsuariosSeguridad;
    procedure ModificaUsuariosSeguridad(const lNavegacion: Boolean );
{$endif}
    procedure AgregaEmpresa;
    procedure ConectaAccesos( var sGroupName, sCompanyName: String ); virtual;
    procedure CopyAccesos(iGrupo: Integer; sCompany: String);virtual;
    procedure DatasetDerechosCrear;
    procedure GrabaDerecho( const iPosition: Integer; const iDerecho: TDerechosAcceso; oNodo: TTreeNode; oTextoBitacora: TGetTextoBitacora);virtual;
    procedure PosicionaUsuarioActivo;
    procedure SuspendeUsuarios;
    procedure BorraSuscripciones(const iReporte: Integer );
    function AgregaSuscripciones( iUsuario, iReporte: Integer ): boolean;overload;
    function AgregaSuscripciones( iUsuario, iReporte, iFrecuencia: Integer; iHayDatos:Integer): boolean; overload;
    function PuedeModificarUsuarioPropio( DataSet : TDataSet ) : Boolean;

    procedure ConectarUsuariosLookup;

    procedure GetReporteSuscrip( const iReporte: Integer );
    procedure GrabaSuscrip;
    property cdsAccesos : TZetaClientDataSet read  GetcdsAccesos;
    function GetEmpresaAccesos: OleVariant;overload;
    function GetEmpresaAccesos( const sCompany: string ): OleVariant;overload;

    procedure GetListaClasifiEmpresa( oLista : TStrings ); virtual;
    procedure ObtieneDescripcionPadre( const iPadre: Integer; var sCodigo, sDescrip: String );
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);virtual;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);virtual;
    {$endif}
    function BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso; virtual;
    function BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso; virtual;
    function BuscaDerechoAdicionales(const iPosition: Integer): TDerechosAcceso;virtual;
    function BuscaDerechoCopiadoAdicionales(const iPosition: Integer ): TDerechosAcceso;virtual;
    function BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;virtual;
    function BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;virtual;

    procedure ModificaUsuarios(const lNavegacion: Boolean );
    procedure DescargaListaUserRoles( Lista: TStrings );virtual;

    procedure CargaListaRolesCalendario( Lista: TStrings );
    procedure CargaListaUsuariosCalendario( Lista: TStrings; Filtro : Boolean );

    procedure CargaListaRolesDefault( Lista: TStrings );virtual;
    procedure CargaListaRoles( Lista: TStrings );virtual;
    procedure BajaUsuario(const iEmpleado:Integer;const lQuitarUsuario:Boolean);virtual;
    procedure ConectaGrupoAdAdmin( oEmpresa: OleVariant );virtual;
    procedure ConectaAccesosAdicionales;virtual;
    procedure CopiaDerechosAdicionales;virtual;
    procedure LeeTodosGrupos( Combo: TZetaKeyCombo );
    function ValidarBDEmpleados(sCodigo, sBaseDatos: String; bWizEmpleados: Boolean = FALSE): WideString;
    function GetSQLUserName: String;
    function GetServer: String;
    procedure GetBasesDatos (CM_DATOS, CM_USRNAME, CM_PASSWRD: String);
    procedure GrabaBaseAdicionales;
    function  AgregarTareaCalendarioReportes(Parametros:TZetaParams): Boolean;

    {$ifdef SUSCRIPCION}
    function BuildEmpresa( const sEmpresa: String) :OleVariant;
    {$endif}
    procedure EliminarUsuariosRolesCalendario(iTipoOperacion, iFolio, iUsCodigo: integer; sRoCodigo: string; var iContadorRegistros: integer);
    procedure RefrescarSolicitudEnvios( Parametros: TZetaParams );
    function InsertarSolicitudEnvio(oDataSet: TZetaClientDataSet): String;
    function CancelarSolicitudEnvio : String;

    procedure RefrescarBitacoraReportes(Parametros: TZetaParams);
    procedure RefrescarBitacoraCorreos(Parametros: TZetaParams);


  end;

var
  dmBaseSistema: TdmBaseSistema;

const
     K_MODIFICAR_USUARIO_PROPIO = ZetaCommonClasses.K_DERECHO_SIST_KARDEX;

procedure CambiarAccesos;

implementation

uses ZetaCommonTools,
     {$ifdef SISNOM_CLAVES}
     ZetaServerTools,
     {$endif}
     ZetaDataSetTools,
     ZetaTipoEntidadTools,
     ZetaDialogo,
     ZAccesosTress,
     ZReconcile,
{$ifndef TRESSCFG}
     {$IFDEF TRESS_DELPHIXE5_UP}
     ZBaseEdicion_DevEx,
     FSistEditUsuarios_DevEx,
     FSistEditEmpresas_DevEx,
     FSistEditGrupos_DevEx,
     FEditImpresoras_DevEx,
     FSistEditAccesos_DevEx,
     FEditSistBaseDatos_DevEx,
     ZetaBusqueda_DevEx,
{$ifdef SISNOM}
     FSistEditUsuariosSeguridad,
{$endif}
     {$ELSE}
     ZBaseEdicion,
     FSistEditUsuarios,
     FSistEditEmpresas,
     FSistEditGrupos,
     FEditImpresoras,
     FSistEditAccesos,
     FEditSistBaseDatos,
     ZetaBusqueda,
     {$ENDIF}
     FTressShell,
{$endif}
     DCliente,
     DSistema
     {$ifdef SUSCRIPCION}
     ,DReportes
     ,ZcxWizardBasico,
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     {$IFNDEF RECLASIFICACION_PLAZAS}
     {$IFNDEF RDDAPP}
     FWizSistCalendarioReportes_DevEx,
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     FSistSolicitudEnviosLog_DevEx,
     FEditSistSolicitudReportes_DevEx,
     FEditBitacoraReportes_DevEx
     {$endif}
     ;

{$R *.DFM}

procedure CambiarAccesos;
var
   oCursor: TCursor;
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     if ( SistEditAccesos_DevEx = nil ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             SistEditAccesos_DevEx := TSistEditAccesos_DevEx.Create( Application );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
     with SistEditAccesos_DevEx do
     begin
          ShowModal;
     end;
{$ELSE}
     if ( SistEditAccesos = nil ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             SistEditAccesos := TSistEditAccesos.Create( Application );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
     with SistEditAccesos do
     begin
          ShowModal;
     end;
{$ENDIF}
end;

{ ****** TGrupoEmpresa ****** }

function TGrupoEmpresa.GetGroupDescription: String;
begin
     Result := Format( 'Grupo: %s', [ FGrupoName ] );
end;

function TGrupoEmpresa.GetEmpresaDescription: String;
begin
     Result := Format( '%s', [ FEmpresaName ] );
end;

{ ****** TGruposEmpresas ****** }

constructor TGruposEmpresas.Create;
begin
     FLista := TList.Create;
end;

destructor TGruposEmpresas.Destroy;
begin
     FreeAndNil( FLista );
     inherited Destroy;
end;

procedure TGruposEmpresas.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
         Delete( i );
end;

function TGruposEmpresas.Add: TGrupoEmpresa;
begin
     Result := TGrupoEmpresa.Create;
     FLista.Add( Result );
end;

procedure TGruposEmpresas.Delete(const Index: Integer);
begin
     Items[ Index ].Free;
     FLista.Delete( Index );
end;

function TGruposEmpresas.Count: Integer;
begin
     Result := FLista.Count;
end;

function TGruposEmpresas.GetItem(Index: Integer): TGrupoEmpresa;
begin
     Result := TGrupoEmpresa( FLista.Items[ Index ] );
end;

{ ********* TdmBaseSistema *********** }

procedure TdmBaseSistema.DataModuleCreate(Sender: TObject);
begin
     FUltimaFrecuencia := Ord( frDiario );
     FActualizarBD := FALSE;
end;

function TdmBaseSistema.GetcdsAccesos : TZetaClientDataSet;
begin
     Result := cdsAccesosBase;
end;

{$ifdef DOS_CAPAS}
function TdmBaseSistema.GetServerSistema: TdmServerSistema;
begin
     Result := DCliente.dmCliente.ServerSistema;
end;
{$else}
function TdmBaseSistema.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServidor ) );
end;
{$endif}

{$ifdef VER130}
procedure TdmBaseSistema.GenericReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmBaseSistema.GenericReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmBaseSistema.NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmBaseSistema.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enUsuarios , Entidades ) then
     {$else}
     if ( enUsuarios in Entidades ) then
     {$endif}
        cdsUsuarios.SetDataChange;
end;

{ *********** Empresas ************ }

procedure TdmBaseSistema.cdsEmpresasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          // cdsEmpresas.Data := ServerSistema.GetEmpresas( Ord( TipoCompany ), GetGrupoActivo );
          cdsEmpresas.Data := ServerSistema.GetEmpresasSistema( GetGrupoActivo );
          { Para que se refresque el Dataset con el que se escogen las empresas }
          cdsCompany.SetDataChange;
     end;
end;

procedure TdmBaseSistema.AgregaEmpresa;
begin
     with cdsEmpresas do
     begin
          Refrescar;
          Agregar;
     end;
end;

procedure TdmBaseSistema.cdsEmpresasAlModificar(Sender: TObject);
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     {$IFDEF DEVEX }
     ZBaseEdicion_DevEx.ShowFormaEdicion( SistEditEmpresas_DevEx, TSistEditEmpresas_DevEx );
     {$ENDIF}
{$ELSE}
     ZBaseEdicion.ShowFormaEdicion( SistEditEmpresas, TSistEditEmpresas );
{$ENDIF}
end;

procedure TdmBaseSistema.cdsEmpresasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer; 
   sCodigo, sCM_DATOS, sCM_CONTROL: String;
   bInsercion: Boolean;
   oDestino: Variant;
begin
     ErrorCount := 0;
     bInsercion := FALSE;
     with cdsEmpresas do
     begin
          if State in [ dsInsert ] then
             bInsercion := TRUE;

          if State in [ dsEdit, dsInsert ] then
             Post;

          sCodigo:= FieldByName('CM_CODIGO').AsString;
          
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerSistema.GrabaEmpresas( Delta, ErrorCount ) ) then
               begin
                    Refrescar;
                    Locate( 'CM_CODIGO', sCodigo, [] );
                    cdsEmpresasLookUp.Refrescar;
                    { Para que se refresque el Dataset con el que se escogen las empresas }
                    dmCliente.cdsCompany.SetDataChange;
                    TressShell.SetDataChange( [ enCompanys ] );
                    {
                     User Story #5285: Solicitar prender derecho a entidades al crear Empresas
                    }   
                    sCM_CONTROL := FieldByName('CM_CONTROL').AsString;
                    if  (ErrorCount = 0) and (bInsercion) and
                        ((sCM_CONTROL = '3DATOS') OR (sCM_CONTROL = '3PRESUP')) then
                    begin
                         if (ZetaDialogo.ZConfirm('Diccionario de datos',
                            '�Prender derechos de acceso a entidades?' + CR_LF +
                            'Se prender�n derechos de acceso a las entidades de la base de datos indicada' + CR_LF +
                            'para todos los grupos de usuarios del sistema.', 0, mbYes)) then
                         begin
                            sCM_DATOS:= FieldByName('CM_DATOS').AsString;
                            oDestino := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                       FieldByName( 'CM_USRNAME' ).AsString,
                                       ZetaServerTools.Encrypt (FieldByName( 'CM_PASSWRD' ).AsString),
                                       0,
                                       FieldByName( 'CM_NIVEL0' ).AsString,
                                       FieldByName( 'CM_CODIGO' ).AsString ] );
                            ServerSistema.PrendeDerechosEntidades( sCodigo, oDestino, FALSE, FALSE, ErrorCount );
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TdmBaseSistema.cdsEmpresasAfterDelete(DataSet: TDataSet);
begin
     cdsEmpresas.Enviar;
end;

procedure TdmBaseSistema.cdsEmpresasBeforePost(DataSet: TDataSet);
begin
     with cdsEmpresas do
     begin
          if StrVacio( FieldByName( 'CM_CODIGO' ).AsString ) then
             DatabaseError( 'El c�digo de la empresa no puede quedar vac�o' )
          else if StrVacio( FieldByName( 'DB_CODIGO' ).AsString ) then
          begin
             DatabaseError( 'Base de datos no puede quedar vac�o' );
          end
          else if ( FieldByName( 'CM_ACUMULA' ).AsString = FieldByName( 'CM_CODIGO' ).AsString ) then
             DataBaseError( 'C�digo de empresa acumuladora debe ser diferente al c�digo de empresa' );
     end;
end;

procedure TdmBaseSistema.cdsEmpresasBuildLookupAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          //cdsEmpresasPortal.Data := ServerSistema.GetEmpresas( Ord( TipoCompany ), K_GRUPO_ADMIN );
          cdsEmpresasBuildLookup.Data := dmCliente.GetEmpresas;
          { Para que se refresque el Dataset con el que se escogen las empresas }
          //cdsCompany.SetDataChange;
     end;
end;

procedure TdmBaseSistema.cdsEmpresasBeforeInsert(DataSet: TDataSet);
begin
     cdsEmpresasLookUp.Data := cdsEmpresas.Data;  // si se hace despu�s, regresa el Dataset a dsBrowse
     FMaxDigito := ZetaDataSetTools.GetNextCaracter( DataSet, 'CM_DIGITO' );
     if EsMinuscula( FMaxDigito ) then
        FMaxDigito := #0;               // La 'a' minuscula es mayor que 'Z' y el m�ximo valor es 'Z'
end;

procedure TdmBaseSistema.cdsEmpresasNewRecord(DataSet: TDataSet);
const
     K_SIN_RESTRICCION = -1;
begin
     with cdsEmpresas do
     begin
          FieldByName( 'CM_EMPATE' ).AsInteger := 0;
          FieldByName( 'CM_USACAFE' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CM_USACASE' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CM_CONTROL' ).AsString := ObtieneElemento( lfTipoCompany, Ord( dmCliente.TipoCompany ) );
          //
          FieldByName( 'CM_CTRL_RL' ).AsString := ObtieneElemento( lfTipoCompanyName, Ord( dmCliente.TipoCompany ) ); // ??
          // -----
          FieldByName( 'CM_DIGITO' ).AsString := FMaxDigito;
          FieldByName( 'CM_KCLASIFI').AsInteger:= K_SIN_RESTRICCION;
     end;
end;

procedure TdmBaseSistema.cdsEmpresasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_SIST_DATOS_EMPRESAS, iRight );
end;

procedure TdmBaseSistema.ACCESOSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := ZetaCommonTools.BoolToSiNo( Sender.AsInteger > 0 );
end;

{ cdsEmpresasLookup }

procedure TdmBaseSistema.cdsEmpresasLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsEmpresas do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque el traspaso de Data regresa el
          begin                          // dataset a dsBrowse
               Conectar;
               cdsEmpresasLookUp.Data := Data;
          end;
     end;
end;

{ cdsEmpresasAccesos }

procedure TdmBaseSistema.cdsEmpresasAccesosAlAdquirirDatos( Sender: TObject);
var
   Parametros:TZetaParams;
begin
     try
        with dmCliente do
        begin
             Parametros:= TZetaParams.Create;
             with Parametros do
             begin
                  AddInteger( 'TIPO_COMPANY', Ord( TipoCompany ) );
                  AddInteger( 'GR_CODIGO', cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger );
                  AddInteger( 'GR_CODIGO_ACTIVO', dmCliente.GetGrupoActivo );
                  //Derechos de la empresa a la que se accedio
                  AddString( 'ASIGNA_DERECHOS_OTROS_GRUPOS', zBoolToStr( ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, ZetaCommonClasses.K_DERECHO_SIST_KARDEX ) ) );
                  AddString( 'ASIGNA_DERECHOS_GRUPO_PROPIO', zBoolToStr( ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, ZetaCommonClasses.K_DERECHO_BANCA ) ) );
             end;
             if ( dmCliente.GetGrupoActivo <> D_GRUPO_SIN_RESTRICCION ) then
                cdsEmpresasAccesos.Data := ServerSistema.GetEmpresasAccesosGruposAcceso( Parametros.VarValues )
             else
                 cdsEmpresasAccesos.Data := ServerSistema.GetEmpresasAccesos( Ord( TipoCompany ), cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger );
        end;
     finally
            freeAndNil( Parametros );
     end;
end;

procedure TdmBaseSistema.cdsEmpresasAccesosAlCrearCampos(Sender: TObject);
begin
     with cdsEmpresasAccesos do
     begin
          with FieldByName( 'ACCESOS' ) do
          begin
               OnGetText := ACCESOSGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

{ ********* Grupos *********** }

procedure TdmBaseSistema.cdsGruposAlAdquirirDatos(Sender: TObject);
begin
     cdsGrupos.Data := ServerSistema.GetGrupos( dmCliente.GetGrupoActivo );
end;


procedure TdmBaseSistema.cdsGruposNewRecord(DataSet: TDataSet);
begin
     cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger := FSiguienteGrupo;
end;

procedure TdmBaseSistema.cdsGruposBeforeInsert(DataSet: TDataSet);
begin
     FSiguienteGrupo := ZetaDataSetTools.GetNextEntero( DataSet, 'GR_CODIGO', 1 );
     cdsGruposLookUp.Data := cdsGrupos.Data;  // si se hace despu�s, regresa el Dataset a dsBrowse
end;

procedure TdmBaseSistema.cdsGruposAlModificar(Sender: TObject);
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     ZBaseEdicion_DevEx.ShowFormaEdicion( SistEditGrupos_DevEx, TSistEditGrupos_DevEx );
{$ELSE}
     ZBaseEdicion.ShowFormaEdicion( SistEditGrupos, TSistEditGrupos )
{$ENDIF}
end;

procedure TdmBaseSistema.cdsGruposAfterDelete(DataSet: TDataSet);
begin
     cdsGrupos.Enviar
     // Cambiar a espa�ol el 'Cannot delete parent table because child table exists'
end;

procedure TdmBaseSistema.cdsGruposBeforePost(DataSet: TDataSet);
begin
     with cdsGrupos do
     begin
          if ( FieldByName( 'GR_CODIGO' ).AsInteger <= 0 ) then
          begin
               FieldByName( 'GR_CODIGO' ).FocusControl;
               DataBaseError( '� Falta Especificar el C�digo del Grupo !' );
          end  { Verificaci�n de que el C�digo del Grupo NO sea el Mismo que el Grupo al que esta subordinado }
          else if ( FieldByName( 'GR_CODIGO' ).AsInteger = FieldByName( 'GR_PADRE' ).AsInteger ) then
               begin
                    FieldByName( 'GR_PADRE' ).FocusControl;
                    DataBaseError( '� El C�digo Del Grupo No Puede Ser Igual Al C�digo del Grupo Subordinado !' );
               end { Verificaci�n de que el C�digo del Grupo Subordinado no esta Vacio }
               else if StrVacio ( FieldByName( 'GR_PADRE' ).AsString ) or ( FieldByName( 'GR_PADRE' ).AsInteger = 0 ) then
                    begin
                         FieldByName( 'GR_PADRE' ).FocusControl;
                         DataBaseError( '� El C�digo Del Grupo Subordinado No Puede Quedar Vacio !' );
                    end
     end;
end;

procedure TdmBaseSistema.cdsGruposAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsGrupos do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerSistema.GrabaGrupos( Delta, dmCliente.Usuario, ErrorCount) ) then
               begin
                    cdsGruposLookup.Refrescar;
               end;
          end;
     end;
end;

procedure TdmBaseSistema.cdsGruposGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, iRight );
end;

procedure TdmBaseSistema.cdsGruposLookupAlAdquirirDatos(Sender: TObject);
begin
     with cdsGrupos do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque el traspaso de Data regresa el
          begin                          // dataset a dsBrowse
               Conectar;
               cdsGruposLookUp.Data := Data;
          end;
     end;
end;

{ ************ Usuarios ********* }

procedure TdmBaseSistema.cdsUsuariosAlCrearCampos(Sender: TObject);
begin
     cdsGrupos.Conectar;
     cdsUsuarios.CreateSimpleLookup( cdsGrupos, 'GR_DESCRIP', 'GR_CODIGO' );
     cdsUsuarios.ListaFija('US_FORMATO', lfTipoFormato );
end;

procedure TdmBaseSistema.cdsUsuariosAlAdquirirDatos(Sender: TObject);
var
   iLongitud: Integer;
   lBlockSistema: WordBool;
begin
     with dmCliente do
     begin
          cdsUsuarios.Data := ServerSistema.GetUsuarios( iLongitud, GetGrupoActivo, lBlockSistema );
          SetPasswordLongitud( iLongitud );
          FBloqueoSistema:= lBlockSistema;
     end;
     cdsUsuariosLookUp.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBlockSistema );
end;

procedure TdmBaseSistema.SuspendeUsuarios;
begin
     with dmCliente do
          cdsUsuarios.Data := ServerSistema.SuspendeUsuarios( Usuario, GetGrupoActivo);
end;

procedure TdmBaseSistema.ActivaUsuarios;
begin
     with dmCliente do
          cdsUsuarios.Data := ServerSistema.ActivaUsuarios( Usuario, GetGrupoActivo );
end;

procedure TdmBaseSistema.cdsUsuariosAfterDelete(DataSet: TDataSet);
begin
     cdsUsuarios.Enviar;
end;

procedure TdmBaseSistema.cdsUsuariosBeforePost(DataSet: TDataSet);
var
   sClave, sMensaje	{$ifdef SISNOM_CLAVES}, sClaveDesencriptada{$endif}: String;
begin
     with cdsUsuarios do
     begin
          if ( FieldByName( 'US_CODIGO' ).AsInteger <= 0 ) then
             DataBaseError( 'N�mero De Usuario Debe Ser Mayor A 0' );
          if ZetaCommonTools.StrVacio( FieldByName( 'US_CORTO' ).AsString ) then
             DataBaseError( 'El Nombre Corto De Usuario No Puede Quedar Vac�o' );
          if ZetaCommonTools.StrVacio(FieldByName( 'US_NOMBRE' ).AsString ) then
             DataBaseError( 'El Nombre Del Usuario No Puede Quedar Vac�o' );
          if ( FieldByName( 'GR_CODIGO' ).AsInteger <= 0 ) then
             DataBaseError( 'El Grupo Del Usuario Debe Ser Mayor A 0' );

	{$ifdef SISNOM_CLAVES}
          sClaveDesencriptada := ZetaServerTools.Decrypt( FClaveEncriptada );
	{$endif}
          sClave := FieldByName( 'US_PASSWRD' ).AsString;

	{$ifdef SISNOM_CLAVES}
          if ( FClaveEncriptada <> sClave ) then
          begin
               if ( sClave <> sClaveDesencriptada ) then
               begin
	{$endif}
                    if ZetaCommonTools.StrVacio( sClave ) then
                       DataBaseError( 'La Clave De Acceso Del Usuario No Puede Quedar Vac�a' );
                    with dmCliente.DatosSeguridad do
                    begin
                         if ( PasswordLongitud > 0 ) and ( Length( sClave ) < PasswordLongitud ) then
                            DataBaseError( Format( 'Clave De Acceso Debe Tener Por Lo Menos %d Caracteres', [ PasswordLongitud ] ) );
                    end;
                    if not dmCliente.ValidaCaracteresPassword( sClave, sMensaje ) then
                       DataBaseError( sMensaje );

	{$ifdef SISNOM_CLAVES}
               end;
               FieldByName( 'US_PASSWRD' ).AsString := ZetaServerTools.Encrypt( sClave );
          end;
	{$endif}
          if strVacio( FieldByName( 'CM_CODIGO' ).AsString ) then
             FieldByName('CB_CODIGO').AsInteger := 0;
             
          if dmCliente.getDatosUsuarioActivo.LoginAD then
          begin
               cdsUsuariosLookup.Locate('US_CODIGO',FieldByName( 'US_CODIGO' ).AsInteger,[] );

               if ( (UpperCase( cdsUsuariosLookup.FieldByName('US_DOMAIN').AsString ) <> UpperCase( FieldByName( 'US_DOMAIN' ).AsString ) )and ( StrLleno(FieldByName( 'US_DOMAIN' ).AsString ) ) )then
               begin
                    if( cdsUsuariosLookup.Locate('US_DOMAIN',UpperCase( FieldByName( 'US_DOMAIN' ).AsString ),[] ) )then
                     DataBaseError( 'El Usuario del Dominio: '+UpperCase( FieldByName( 'US_DOMAIN' ).AsString )+' est� repetido' )
                    else
                        FieldByName( 'US_DOMAIN' ).AsString := UpperCase( FieldByName( 'US_DOMAIN' ).AsString );
               end;
          end
          else
              FieldByName( 'US_DOMAIN' ).AsString := UpperCase( FieldByName( 'US_DOMAIN' ).AsString );


          FieldByName( 'US_CORTO' ).AsString := UpperCase( FieldByName( 'US_CORTO' ).AsString );
     end;
end;

procedure TdmBaseSistema.cdsUsuariosNewRecord(DataSet: TDataSet);
begin
     with cdsUsuarios do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := ServerSistema.GetNuevoUsuario;
          FieldByName( 'US_CAMBIA' ).AsString := K_GLOBAL_SI;
          FieldByName( 'US_NIVEL' ).AsInteger := 1;
          FieldByName( 'US_BLOQUEA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'US_DENTRO' ).AsString := K_GLOBAL_NO;
          FieldByName( 'US_ACTIVO' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmBaseSistema.cdsUsuariosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iLongitud: Integer;
   lBlockSistema: WordBool;

   deltaBack : Olevariant;
begin
     ErrorCount := 0;

     with cdsUsuarios do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               deltaBack := Delta;
               if Reconcile( ServerSistema.GrabaUsuarios( Delta, dmCliente.Usuario, ErrorCount ) ) then
               begin
                  {$ifndef DOS_CAPAS}
                  {$ifdef TRESS}
                  if (dmCliente.EmpresaAbierta) and ( ErrorCount = 0 ) then
                  begin
                       ServerSistema.ActualizaUsuariosEmpresa( dmCliente.Empresa, deltaBack);
                  end;
                  {$endif}
                  {$endif}
                  cdsUsuariosLookUp.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBlockSistema );
               end;
          end;



     end;


     {
     Adem�s de cambiar los datos del usuario hay que
     cambiar el campo SUPER.US_CODIGO si USUARIO.US_CODIGO
     tuvo cambio � si el usuario fu� borrado
     }
end;

procedure TdmBaseSistema.CambiaUsuarioFechaSalida( Sender: TField );
begin
     with Sender do
     begin
          if ( Dataset.State in [ dsEdit, dsInsert ] ) and not ZetaCommonTools.zStrToBool( AsString ) then
             Dataset.FieldByName( 'US_FEC_OUT' ).AsDateTime := Now;
     end;
end;

procedure TdmBaseSistema.cdsUsuariosAfterOpen(DataSet: TDataSet);
begin
     with cdsUsuarios do
     begin
          FieldByName( 'US_DENTRO' ).OnChange := CambiaUsuarioFechaSalida;
          FieldByName( 'US_PASSWRD' ).OnSetText := ActualizaFechaPassWord;
     end;
end;

procedure TdmBaseSistema.ActualizaFechaPassWord(Sender: TField; const Text: String);
begin
     Sender.AsString := Text;
     cdsUsuarios.FieldByName( 'US_FEC_PWD' ).AsDateTime := Date;
end;

procedure TdmBaseSistema.ModificaUsuarios(const lNavegacion: Boolean );
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     if SistEditUsuarios_DevEx = NIL then
        SistEditUsuarios_DevEx := TSistEditUsuarios_DevEx.Create( Application );
     SistEditUsuarios_DevEx.Navegacion := lNavegacion;
     SistEditUsuarios_DevEx.ShowModal;
{$ELSE}
     if SistEditUsuarios = NIL then
        SistEditUsuarios := TSistEditUsuarios.Create( Application );
     SistEditUsuarios.Navegacion := lNavegacion;
     SistEditUsuarios.ShowModal;
{$ENDIF}
end;

procedure TdmBaseSistema.cdsUsuariosAlModificar(Sender: TObject);
begin
     ModificaUsuarios(TRUE);
end;

procedure TdmBaseSistema.PosicionaUsuarioActivo;
begin
     with cdsUsuarios do
     begin
          Conectar;
          // Posiciona 'cdsUsuarios' en Usuario Activo
          GetDescripcion( IntToStr( dmCliente.Usuario ) );
     end;
end;

function TdmBaseSistema.UsuarioPosicionado: Integer;
begin
     // No es lo mismo UsuarioActivo (el que di� Login) que
     // usuario posicionado (El registro activo de cdsUsuarios)
     Result := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
end;

procedure TdmBaseSistema.cdsUsuariosGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_SIST_DATOS_USUARIOS, iRight );
end;

{$ifdef VER130}
procedure TdmBaseSistema.cdsUsuariosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( pos( 'XAK1', E.Message ) > 0 ) then
        E.Message := '� Nombre De Usuario Ya Existe !';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmBaseSistema.cdsUsuariosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( pos( 'XAK1', E.Message ) > 0 ) then
        E.Message := '� Nombre De Usuario Ya Existe !';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{ ****** Impresoras *********** }

procedure TdmBaseSistema.cdsImpresorasAlAdquirirDatos(Sender: TObject);
begin
     cdsImpresoras.Data := ServerSistema.GetImpresoras;
end;


procedure TdmBaseSistema.cdsImpresorasAlModificar(Sender: TObject);
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditImpresoras_DevEx, TEditImpresoras_DevEx );
{$ELSE}
     ZBaseEdicion.ShowFormaEdicion( EditImpresoras, TEditImpresoras )
{$ENDIF}
end;

procedure TdmBaseSistema.cdsImpresorasAfterDelete(DataSet: TDataSet);
begin
     cdsImpresoras.Enviar;
end;

procedure TdmBaseSistema.cdsImpresorasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsImpresoras do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconcile( ServerSistema.GrabaImpresoras( Delta, ErrorCount ) );
    end;
end;

{ *********** Derechos de Acceso ************** }

procedure TdmBaseSistema.ConectaAccesos( var sGroupName, sCompanyName: String );
var
   iGrupo: Integer;
   sCompany: String;
begin
     FTextoBitacora:= VACIO;
     with cdsGrupos do
     begin
          iGrupo := FieldByName( 'GR_CODIGO' ).AsInteger;
          sGroupName := FieldByName( 'GR_DESCRIP' ).AsString;
     end;
     with cdsEmpresasAccesos do
     begin
          sCompany := FieldByName( 'CM_CODIGO' ).AsString;
          sCompanyName := FieldByName( 'CM_NOMBRE' ).AsString;
     end;
     cdsAccesos.Data := ServerSistema.GetAccesos( iGrupo, sCompany )
end;

{$ifdef SUSCRIPCION}
function TdmBaseSistema.BuildEmpresa(const sEmpresa: String): OleVariant;
begin
  with cdsEmpresasBuildLookup do
     begin
          Conectar;
          Locate( 'CM_CODIGO', sEmpresa, [loCaseInsensitive] );
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                          FieldByName( 'CM_USRNAME' ).AsString,
                          FieldByName( 'CM_PASSWRD' ).AsString,
                          Self.Usuario,
                          FieldByName( 'CM_NIVEL0' ).AsString,
                          FieldByName( 'CM_CODIGO' ).AsString, 1, 0 ] );
     end;
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     {$IFNDEF RDDAPP}
     dmReportes.cdsLookupReportesEmpresa.SetDataChange;
     dmReportes.cdsSuscripReportesLookup.SetDataChange;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
end;
{$endif}

function TdmBaseSistema.BuscaAccesos( const iPosition: Integer ): Boolean;
begin
     Result := cdsAccesos.Locate( 'GR_CODIGO;CM_CODIGO;AX_NUMERO', VarArrayOf( [ cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger, cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString, iPosition ] ), [] );
end;

function TdmBaseSistema.BuscaDerecho( const iPosition: Integer ): TDerechosAcceso;
begin
     if BuscaAccesos( iPosition ) then
        Result := cdsAccesos.FieldByName( 'AX_DERECHO' ).AsInteger
     else
         Result := K_SIN_DERECHOS;
end;

procedure TdmBaseSistema.CopyAccesos( iGrupo: Integer; sCompany: String );
begin
     with cdsCopiarAccesos do
     begin
          Data := ServerSistema.GetAccesos( iGrupo, sCompany );
          IndexFieldNames := 'AX_NUMERO';
     end;
end;

function TdmBaseSistema.BuscaDerechoCopiado( const iPosition: Integer ): TDerechosAcceso;
begin
     with cdsCopiarAccesos do
     begin
          if Locate( 'AX_NUMERO', VarArrayOf( [ iPosition ] ), [] ) then
             Result := FieldByName( 'AX_DERECHO' ).AsInteger
          else
              Result := K_SIN_DERECHOS;
     end;
end;

function TdmBaseSistema.GetTitulo( Nodo: TTreeNode; var iPos: Integer ): String;
var
   oPadre: TTreeNode;
begin
     Result := VACIO;
     oPadre := Nodo.Parent;
     while ( Assigned ( oPadre ) ) do
     begin
           Result := oPadre.Text + '-' + Result;
           oPadre := oPadre.Parent;
     end;
     iPos:= length(Result);
     Result:= Result + Nodo.Text;
end;

procedure TdmBaseSistema.GrabaDerecho( const iPosition: Integer; const iDerecho: TDerechosAcceso; oNodo: TTreeNode; oTextoBitacora: TGetTextoBitacora);
var
   sTextoBitacora: String;
   iPos: Integer;
begin
     with cdsAccesos do
     begin
          if BuscaAccesos( iPosition ) then
          begin
               if ( FieldByName( 'AX_DERECHO' ).AsInteger <> iDerecho ) then
               begin
                    sTextoBitacora:= GetTitulo(oNodo, iPos )+ CR_LF;
                    sTextoBitacora:= sTextoBitacora + Space(iPos) + 'Antes: '+ oTextoBitacora( oNodo, FieldByName('AX_DERECHO').AsInteger ) + CR_LF ;
                    Edit;
                    FieldByName( 'AX_DERECHO' ).AsInteger := iDerecho;
                    sTextoBitacora:= sTextoBitacora + Space(iPos)+ 'Nuevo: '+ oTextoBitacora( oNodo, iDerecho )+ CR_LF;
                    Post;
                    FTextoBitacora:= FTextoBitacora + sTextoBitacora;
               end;
          end
          else
          begin
               sTextoBitacora:= GetTitulo(oNodo, iPos)+ CR_LF;
               sTextoBitacora:= sTextoBitacora + Space(iPos) + 'Antes: Ninguno'+ CR_LF;
               Append;
               FieldByName( 'GR_CODIGO' ).AsInteger := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
               FieldByName( 'CM_CODIGO' ).AsString := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
               FieldByName( 'AX_NUMERO' ).AsInteger := iPosition;
               FieldByName( 'AX_DERECHO' ).AsInteger := iDerecho;
               sTextoBitacora:= sTextoBitacora + Space(iPos)+ 'Nuevo: '+ oTextoBitacora( oNodo, iDerecho)+ CR_LF;
               Post;
               FTextoBitacora:= FTextoBitacora + sTextoBitacora;
          end;
     end;
end;


procedure TdmBaseSistema.cdsAccesosBaseAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   sCodigo, sDescripcion: string;
   oParametros: TZetaParams;

begin
     ErrorCount := 0;
     with cdsAccesos do
     begin
          if State in [ dsEdit, dsInsert ] then
             PostData;
          if ( ChangeCount > 0 ) then
          begin
               oParametros:= TZetaParams.Create;
               try
                  sDescripcion:= 'Cambi� Accesos: Grupo: ' + cdsGrupos.FieldByName('GR_CODIGO').AsString + ', Empresa: ' + cdsEmpresasAccesos.FieldByName('CM_CODIGO').AsString;
                  with oParametros do
                  begin
                       AddString('Descripcion', sDescripcion);
                       {$IFDEF TRESS_DELPHIXE5_UP}
                       AddString('TextoBitacora',   FTextoBitacora );
                       {$ELSE}
                       AddBlob('TextoBitacora', FTextoBitacora);
                       {$ENDIF}
                       AddInteger('Usuario', dmCliente.Usuario);
                  end;

                  if Reconcile( ServerSistema.GrabaAccesos( Delta, oParametros.VarValues, ErrorCount ) ) then
                  begin
                       sCodigo := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                       cdsEmpresasAccesos.Refrescar;
                       cdsEmpresasAccesos.Locate( 'CM_CODIGO', sCodigo, [ loCaseInsensitive ] );
                  end;
               finally
                      FreeAndNil(oParametros);
               end;
          end;
     end;
     FTextoBitacora:= VACIO;
end;

{ ********* cdsSuscrip ********* }

procedure TdmBaseSistema.ConectaUsuarioSuscrip( const iUsuario: integer );
begin
     cdsSuscrip.Data := ServerSistema.GetUsuarioSuscrip( dmCliente.Empresa, iUsuario );
end;

procedure TdmBaseSistema.cdsSuscripAlAdquirirDatos(Sender: TObject);
begin
     ConectaUsuarioSuscrip( UsuarioPosicionado );
end;

procedure TdmBaseSistema.cdsSuscripAlCrearCampos(Sender: TObject);
begin
    with cdsSuscrip do
    begin
         with FieldByName( 'SU_FRECUEN' ) do
         begin
              OnGetText := SU_FRECUENGetText;
              Alignment := taLeftJustify;
         end;
    end;
end;

procedure TdmBaseSistema.CA_FRECGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
    if Sender.IsNull then
        Text := ''
    else
        Text := ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, Sender.AsInteger );
end;

procedure TdmBaseSistema.SU_FRECUENGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
    if Sender.IsNull then
        Text := ''
    else
        Text := ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, Sender.AsInteger );
end;

procedure TdmBaseSistema.cdsSuscripAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsSuscrip do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
            Reconcile( ServerSistema.GrabaUsuarioSuscrip( dmCliente.Empresa, Delta, ErrorCount, UsuarioPosicionado ) );
    end;
end;

procedure TdmBaseSistema.cdsSuscripNewRecord(DataSet: TDataSet);
begin
    with cdsSuscrip do
        FieldByName( 'SU_FRECUEN' ).AsInteger := FUltimaFrecuencia;
end;

procedure TdmBaseSistema.cdsSuscripUsuarioCalendarioAlAdquirirDatos(
  Sender: TObject);
begin
      cdsSuscripUsuarioCalendario.Data := ServerSistema.GetUsuarioSuscripCalendarioReporte( dmCliente.Empresa, UsuarioPosicionado );
end;

procedure TdmBaseSistema.cdsSuscripUsuarioCalendarioAlCrearCampos(
  Sender: TObject);
begin
    with cdsSuscripUsuarioCalendario do
    begin
         with FieldByName( 'CA_FREC' ) do
         begin
              OnGetText := CA_FRECGetText;
              Alignment := taLeftJustify;
         end;
    end;
end;

procedure TdmBaseSistema.cdsSuscripBeforePost(DataSet: TDataSet);
begin
    FUltimaFrecuencia := cdsSuscrip.FieldByName( 'SU_FRECUEN' ).AsInteger;
end;

procedure TdmBaseSistema.cdsSuscripCalendarioReporteReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
  var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

function TdmBaseSistema.AgregaSuscripciones( iUsuario, iReporte, iFrecuencia, iHayDatos: Integer  ): boolean;
begin
     with cdsSuscrip do
     begin
          if not( Locate('US_CODIGO', iUsuario,[] ) ) then
          begin
               Append;
               FieldByName('RE_CODIGO').AsInteger:= iReporte;
               FieldByName('US_CODIGO').AsInteger:= iUsuario;
               FieldByName('SU_FRECUEN').AsInteger:= iFrecuencia;
               FieldByName('SU_VACIO').AsInteger:= iHayDatos;
               Post;
          end;
          Result:= ( ChangeCount > 0 );
     end
end;
function TdmBaseSistema.AgregaSuscripciones( iUsuario, iReporte: Integer ): boolean;
begin
     Result:= AgregaSuscripciones( iUsuario, iReporte, FUltimaFrecuencia, Ord(enEnviar) );
end;

procedure TdmBaseSistema.BorraSuscripciones( const iReporte: Integer );
begin
     with cdsSuscrip do
     begin
          ConectaUsuarioSuscrip( dmCliente.Usuario );
          if Locate('RE_CODIGO',iReporte, [] ) then;
          begin
             Delete;
             Enviar;
          end;
     end;
end;

procedure TdmBaseSistema.GetReporteSuscrip( const iReporte: Integer );
begin
     cdsSuscrip.Data := ServerSistema.GetReporteSuscrip( dmCliente.Empresa, iReporte );
end;

procedure TdmBaseSistema.GrabaSuscrip;
begin
     cdsSuscrip.Enviar;
end;

function TdmBaseSistema.AgregarTareaCalendarioReportes(Parametros:TZetaParams): Boolean;
var
    oParametros: OleVariant;
    oDatos: OleVariant;
begin
     oParametros := Parametros.VarValues;
     try
          ServerSistema.GrabaTareaCalendario(dmCliente.Empresa, oParametros, oDatos);
          Result := True;
     Except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;

end;


{ ********* Copiar Derechos de Acceso ********* }

function TdmBaseSistema.CargarGruposAccesos( Lista: TGruposEmpresas; var iDefault: Integer ): Boolean;
var
   iGrupo: Integer;
   sCompany: String;
   Parametros:TZetaParams;
begin
     iDefault := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
     iGrupo := iDefault;
     sCompany := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
     Lista.Clear;
     with cdsCopiarAccesos do
     begin
          IndexName := '';
          with dmCliente do
          begin
               Parametros:= TZetaParams.Create;
               with Parametros do
               begin
                    AddInteger( 'TIPO_COMPANY', Ord( TipoCompany ) );
                    AddInteger( 'GR_CODIGO', iGrupo );
                    AddInteger( 'GR_CODIGO_ACTIVO', dmCliente.GetGrupoActivo );
                    //Derechos de la empresa a la que se accedio
                    AddString( 'ASIGNA_DERECHOS_OTROS_GRUPOS', zBoolToStr( ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, ZetaCommonClasses.K_DERECHO_SIST_KARDEX ) ) );
                    AddString( 'ASIGNA_DERECHOS_GRUPO_PROPIO', zBoolToStr( ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, ZetaCommonClasses.K_DERECHO_BANCA ) ) );
               end;
               if ( dmCliente.GetGrupoActivo <> D_GRUPO_SIN_RESTRICCION ) then
                  Data := ServerSistema.GetEmpresasAccesosGruposAcceso( Parametros.VarValues )
               else
                   Data := ServerSistema.GetGruposAccesos( iGrupo, sCompany, Ord( dmCliente.TipoCompany ) );
          end;
          while not Eof do
          begin
               with Lista do
               begin
                    with Add do
                    begin
                         Grupo := FieldByName( 'GR_CODIGO' ).AsInteger;
                         GrupoName := FieldByName( 'GR_DESCRIP' ).AsString;
                         Empresa := FieldByName( 'CM_CODIGO' ).AsString;
                         EmpresaName := FieldByName( 'CM_NOMBRE' ).AsString;
                    end;
               end;
               Next;
          end;
          Result := ( Lista.Count > 0 );
     end;
end;

{ ********* Imprimir  Derechos de Acceso ********* }

procedure TdmBaseSistema.DatasetDerechosCrear;
const
     K_MAX_BIT_RIGHT = $F;  { 0..15 }
var
   i: Integer;
begin
     with cdsDerechos do
     begin
          InitTempDataSet;
          AddIntegerField( 'RI_POS' );
          AddIntegerField( 'RI_NIVEL' );
          AddStringField( 'RI_NOMBRE', 255 );
          AddStringField( 'RI_COMENTA', 255 );
          for i := 0 to K_MAX_BIT_RIGHT do
          begin
               AddStringField( Format( 'RI_ITEM_%2.2d', [ i ] ), 1 );
          end;
          CreateTempDataset;
          LogChanges := False;
     end;
end;

function TdmBaseSistema.PuedeModificarUsuarioPropio( DataSet : TDataSet ) : Boolean;
begin
     Result := (dmCliente.Usuario <> Dataset.FieldByName( 'US_CODIGO' ).AsInteger) or
               ( (dmCliente.Usuario = Dataset.FieldByName( 'US_CODIGO' ).AsInteger) and
                 (ZAccesosMgr.CheckDerecho( D_SIST_DATOS_USUARIOS, K_MODIFICAR_USUARIO_PROPIO )) );
end;

function TdmBaseSistema.GetEmpresaAccesos: OleVariant;


begin
     with cdsEmpresasAccesos do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  Usuario,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString,
                                  cdsGrupos.FieldByName('GR_CODIGO').AsInteger ] );
     end;
end;

function TdmBaseSistema.GetEmpresaAccesos( const sCompany: string ): OleVariant;

 var sCompanyInicial: string;
begin

     with cdsEmpresasAccesos do
     begin
          sCompanyInicial := FieldByName('CM_CODIGO').AsString;
          try
             if Locate( 'CM_CODIGO', sCompany, [] ) then
                  Result := GetEmpresaAccesos
             else
             begin
                    raise Exception.Create( 'Error al buscar la Empresa ' + sCompany );
             end;
          finally
                 //Posicionarse otra vez en la empresa en la que estaba.
                 if NOT Locate( 'CM_CODIGO', sCompanyInicial, [] ) then
                    raise Exception.Create( 'Error al buscar la Empresa ' + sCompanyInicial );
          end
     end;
end;

procedure TdmBaseSistema.ObtieneDescripcionPadre( const iPadre: Integer; var sCodigo, sDescrip: String );
var
   oDataSetLookup: TZetaLookupDataSet;
begin
     sCodigo := VACIO;
     sDescrip := VACIO;
     if ( iPadre <> 0 ) then   // Si es administrador no tiene padre y regresa vacio
     begin
          oDataSetLookup := TZetaLookupDataSet.Create( self );
          try
             with oDataSetLookup do
             begin
                  Data := ServerSistema.GetGrupos( ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION );
                  if Locate( 'GR_CODIGO', iPadre, [] ) then
                  begin
                       sCodigo := IntToStr( iPadre );
                       sDescrip := FieldbyName( 'GR_DESCRIP' ).AsString;
                  end;
             end;
          finally
                 FreeAndNil( oDataSetLookup );
          end;
     end;
end;
{ Clasificaciones de reportes por empresa}

procedure TdmBaseSistema.GetListaClasifiEmpresa( oLista : TStrings );
begin
     {Aqui se conecta cdsClasifiRepEmp de acuerdo a la Empresa, se dejo en la base por compatibilidad }

end;

function TdmBaseSistema.BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

function TdmBaseSistema.BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

function TdmBaseSistema.BuscaDerechoAdicionales(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

function TdmBaseSistema.BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
     Result := 0;
end;

function TdmBaseSistema.BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
     Result := 0;
end;

function TdmBaseSistema.BuscaDerechoCopiadoAdicionales(const iPosition: Integer ): TDerechosAcceso;
begin
     Result := 0;
end;



procedure TdmBaseSistema.cdsUsuariosLookupGetRights( Sender: TZetaClientDataSet; const iRight: Integer;
  var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmBaseSistema.cdsUsuariosSeguridadLookupGetRights( Sender: TZetaClientDataSet; const iRight: Integer;
  var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmBaseSistema.DescargaListaUserRoles( Lista: TStrings );
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.CargaListaRolesDefault( Lista: TStrings );
begin
     // No hace nada en la base
end;


procedure TdmBaseSistema.CargaListaRoles( Lista: TStrings );
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.BajaUsuario(const iEmpleado:Integer;const lQuitarUsuario:Boolean);
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.ConectaGrupoAdAdmin;
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.ConectarUsuariosLookup;
begin
  cdsUsuarios.Conectar;
end;

procedure TdmBaseSistema.ConectaAccesosAdicionales;
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.CopiaDerechosAdicionales;
begin
end;

procedure TdmBaseSistema.cdsGruposTodosAlAdquirirDatos(Sender: TObject);
begin
     cdsGruposTodos.Data := ServerSistema.GetGrupos( ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION );
end;

procedure TdmBaseSistema.LeeTodosGrupos( Combo: TZetaKeyCombo);
begin
     with Combo do
     begin
          with cdsGruposTodos do
          begin
               Refrescar;
               First;
               while ( not EOF ) do
               begin
                    if strLleno( FieldByName( 'GR_DESCRIP' ).AsString ) then
                       Items.AddObject( FieldByName( 'GR_DESCRIP' ).AsString, TObject( FieldByName( 'GR_CODIGO' ).AsInteger ) );
                    Next;
               end;
          end;
          Sorted := True;
     end;
end;

{ ********** cdsUsuariosSeguridad **********}

procedure TdmBaseSistema.cdsUsuariosSeguridadAlCrearCampos(Sender: TObject);
begin
     cdsGrupos.Conectar;
     cdsUsuariosSeguridad.CreateSimpleLookup( cdsGrupos, 'GR_DESCRIP', 'GR_CODIGO' );
     cdsUsuariosSeguridad.ListaFija('US_FORMATO', lfTipoFormato );
end;

procedure TdmBaseSistema.cdsUsuariosSeguridadAlAdquirirDatos(Sender: TObject);
{$ifdef SISNOM}
var
   iLongitud: Integer;
   lBlockSistema: WordBool;
{$endif}
begin
     {$ifdef SISNOM}
     with dmCliente do
     begin
          // SISNOM.
          cdsUsuariosSeguridad.Data := ServerSistema.GetUsuariosSeguridad( iLongitud, GetGrupoActivo, lBlockSistema );
          
          SetPasswordLongitud( iLongitud );
          FBloqueoSistema:= lBlockSistema;
     end;
     // SISNOM.
     cdsUsuariosSeguridadLookUp.Data := ServerSistema.GetUsuariosSeguridad( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBlockSistema );
     {$endif}
end;

{$ifdef SISNOM}
procedure TdmBaseSistema.SuspendeUsuariosSeguridad;
begin
     with dmCliente do
          cdsUsuariosSeguridad.Data := ServerSistema.SuspendeUsuarios( Usuario, GetGrupoActivo);
end;

procedure TdmBaseSistema.ActivaUsuariosSeguridad;
begin
     with dmCliente do
          cdsUsuariosSeguridad.Data := ServerSistema.ActivaUsuarios( Usuario, GetGrupoActivo );
end;
{$endif}

procedure TdmBaseSistema.cdsUsuariosSeguridadAfterDelete(DataSet: TDataSet);
begin
     cdsUsuariosSeguridad.Enviar;
end;

procedure TdmBaseSistema.cdsUsuariosSeguridadBeforePost(DataSet: TDataSet);
var
   sClave, sMensaje	{$ifdef SISNOM_CLAVES}, sClaveDesencriptada{$endif}: String;
begin
     with cdsUsuariosSeguridad do
     begin
          if ( FieldByName( 'US_CODIGO' ).AsInteger <= 0 ) then
             DataBaseError( 'N�mero De Usuario Debe Ser Mayor A 0' );
          if ZetaCommonTools.StrVacio( FieldByName( 'US_CORTO' ).AsString ) then
             DataBaseError( 'El Nombre Corto De Usuario No Puede Quedar Vac�o' );
          if ZetaCommonTools.StrVacio(FieldByName( 'US_NOMBRE' ).AsString ) then
             DataBaseError( 'El Nombre Del Usuario No Puede Quedar Vac�o' );
          if ( FieldByName( 'GR_CODIGO' ).AsInteger <= 0 ) then
             DataBaseError( 'El Grupo Del Usuario Debe Ser Mayor A 0' );

	{$ifdef SISNOM_CLAVES}
          sClaveDesencriptada := ZetaServerTools.Decrypt( FClaveEncriptada );
	{$endif}
          sClave := FieldByName( 'US_PASSWRD' ).AsString;

	{$ifdef SISNOM_CLAVES}
          if ( FClaveEncriptada <> sClave ) then
          begin
               if ( sClave <> sClaveDesencriptada ) then
               begin
	{$endif}
                    if ZetaCommonTools.StrVacio( sClave ) then
                       DataBaseError( 'La Clave De Acceso Del Usuario No Puede Quedar Vac�a' );
                    with dmCliente.DatosSeguridad do
                    begin
                         if ( PasswordLongitud > 0 ) and ( Length( sClave ) < PasswordLongitud ) then
                            DataBaseError( Format( 'Clave De Acceso Debe Tener Por Lo Menos %d Caracteres', [ PasswordLongitud ] ) );
                    end;
                    if not dmCliente.ValidaCaracteresPassword( sClave, sMensaje ) then
                       DataBaseError( sMensaje );

	{$ifdef SISNOM_CLAVES}
               end;
               FieldByName( 'US_PASSWRD' ).AsString := ZetaServerTools.Encrypt( sClave );
          end;
	{$endif}
          if strVacio( FieldByName( 'CM_CODIGO' ).AsString ) then
             FieldByName('CB_CODIGO').AsInteger := 0;

          if dmCliente.getDatosUsuarioActivo.LoginAD then
          begin
               cdsUsuariosSeguridadLookup.Locate('US_CODIGO',FieldByName( 'US_CODIGO' ).AsInteger,[] );

               if ( (UpperCase( cdsUsuariosSeguridadLookup.FieldByName('US_DOMAIN').AsString ) <> UpperCase( FieldByName( 'US_DOMAIN' ).AsString ) )and ( StrLleno(FieldByName( 'US_DOMAIN' ).AsString ) ) )then
               begin
                    if( cdsUsuariosSeguridadLookup.Locate('US_DOMAIN',UpperCase( FieldByName( 'US_DOMAIN' ).AsString ),[] ) )then
                     DataBaseError( 'El Usuario del Dominio: '+UpperCase( FieldByName( 'US_DOMAIN' ).AsString )+' est� repetido' )
                    else
                        FieldByName( 'US_DOMAIN' ).AsString := UpperCase( FieldByName( 'US_DOMAIN' ).AsString );
               end;
          end
          else
              FieldByName( 'US_DOMAIN' ).AsString := UpperCase( FieldByName( 'US_DOMAIN' ).AsString );


          FieldByName( 'US_CORTO' ).AsString := UpperCase( FieldByName( 'US_CORTO' ).AsString );
     end;
end;

procedure TdmBaseSistema.cdsUsuariosSeguridadNewRecord(DataSet: TDataSet);
begin
     with cdsUsuariosSeguridad do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := ServerSistema.GetNuevoUsuario;
          FieldByName( 'US_CAMBIA' ).AsString := K_GLOBAL_SI;
          FieldByName( 'US_NIVEL' ).AsInteger := 1;
          FieldByName( 'US_BLOQUEA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'US_DENTRO' ).AsString := K_GLOBAL_NO;
          FieldByName( 'US_ACTIVO' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmBaseSistema.cdsUsuariosSeguridadAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iLongitud: Integer;
   lBlockSistema: WordBool;

   deltaBack : Olevariant;
begin
     ErrorCount := 0;

     with cdsUsuariosSeguridad do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               deltaBack := Delta;
               if Reconcile( ServerSistema.GrabaUsuarios( Delta, dmCliente.Usuario, ErrorCount ) ) then
               begin
                  {$ifndef DOS_CAPAS}
                  {$ifdef TRESS}
                  if (dmCliente.EmpresaAbierta) and ( ErrorCount = 0 ) then
                  begin
                       ServerSistema.ActualizaUsuariosEmpresa( dmCliente.Empresa, deltaBack);
                  end;
                  {$endif}
                  {$endif}
                  cdsUsuariosSeguridadLookUp.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBlockSistema );
               end;
          end;



     end;


     {
     Adem�s de cambiar los datos del usuario hay que
     cambiar el campo SUPER.US_CODIGO si USUARIO.US_CODIGO
     tuvo cambio � si el usuario fu� borrado
     }
end;

procedure TdmBaseSistema.cdsUsuariosSeguridadAfterOpen(DataSet: TDataSet);
begin
{$ifdef SISNOM}
     with cdsUsuariosSeguridad do
     begin
          FieldByName( 'US_DENTRO' ).OnChange := CambiaUsuarioFechaSalida;
          FieldByName( 'US_PASSWRD' ).OnSetText := ActualizaFechaPassWordSeguridad;
     end;
{$endif}
end;

{$ifdef SISNOM}
procedure TdmBaseSistema.ActualizaFechaPassWordSeguridad(Sender: TField; const Text: String);
begin
     Sender.AsString := Text;
     cdsUsuariosSeguridad.FieldByName( 'US_FEC_PWD' ).AsDateTime := Date;
end;

procedure TdmBaseSistema.ModificaUsuariosSeguridad(const lNavegacion: Boolean );
begin
     if SistEditUsuariosSeguridad = NIL then
        SistEditUsuariosSeguridad := TSistEditUsuariosSeguridad.Create( Application );
     SistEditUsuariosSeguridad.Navegacion := lNavegacion;

     SistEditUsuariosSeguridad.ShowModal;
end;

procedure TdmBaseSistema.PosicionaUsuarioActivoSeguridad;
begin
     with cdsUsuariosSeguridad do
     begin
          Conectar;
          // Posiciona 'cdsUsuariosSeguridad' en Usuario Activo
          GetDescripcion( IntToStr( dmCliente.Usuario ) );
     end;
end;
{$endif}

procedure TdmBaseSistema.cdsUsuariosSeguridadAlModificar(Sender: TObject);
begin
{$ifdef SISNOM}
     ModificaUsuariosSeguridad(TRUE);
{$endif}
end;

procedure TdmBaseSistema.cdsUsuariosSeguridadGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_SIST_DATOS_USUARIOS, iRight );
end;

{$ifdef VER130}
procedure TdmBaseSistema.cdsUsuariosSeguridadReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( pos( 'XAK1', E.Message ) > 0 ) then
        E.Message := '� Nombre De Usuario Ya Existe !';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmBaseSistema.cdsUsuariosSeguridadReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( pos( 'XAK1', E.Message ) > 0 ) then
        E.Message := '� Nombre De Usuario Ya Existe !';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}
procedure TdmBaseSistema.cdsSistBaseDatosAlAdquirirDatos(Sender: TObject);
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourGlass;
        {$ifdef SELECCION}
        cdsSistBaseDatos.Data := ServerSistema.ConsultaConfBD(ord (tcRecluta));
        {$else}
        {$ifdef VISITANTES}
        cdsSistBaseDatos.Data := ServerSistema.ConsultaConfBD(ord (tcVisitas));
        {$else}
        cdsSistBaseDatos.Data := ServerSistema.ConsultaConfBD(ord (tc3Datos));
        {$endif}
        {$endif}
        cdsSistBaseDatosLookUp.Data := cdsSistBaseDatos.Data;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmBaseSistema.cdsSistBaseDatosAlCrearCampos(Sender: TObject);
begin
     with cdsSistBaseDatos do
     begin
          FieldByName( 'DB_CODIGO' ).OnValidate := cdsSistBaseDatosDB_CODIGOValidate;
          with FieldByName( 'DB_TIPO' ) do
          begin
               OnGetText:= DB_TIPOGetText;
               Alignment:= taLeftJustify;
          end;
     end;
end;

procedure TdmBaseSistema.cdsSistBaseDatosDB_CODIGOValidate(Sender: TField);
const
     K_ESPACIO = ' ';
begin
     if Pos (K_ESPACIO, Sender.AsString) > 0 then
        DataBaseError( 'C�digo de base de datos no puede contener espacios');
end;

procedure TdmBaseSistema.DB_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          Text := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Sender.AsInteger);
     end;
end;

procedure TdmBaseSistema.cdsSistBaseDatosAlModificar(Sender: TObject);
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     ZBaseEdicion_DevEx.ShowFormaEdicion(EditSistBaseDatos_DevEx, TEditSistBaseDatos_DevEx);
{$ELSE}
     ZBaseEdicion.ShowFormaEdicion(EditSistBaseDatos, TEditSistBaseDatos);
{$ENDIF}
end;

procedure TdmBaseSistema.cdsSistBaseDatosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   sAdvertenciaValidarBDEmpleados : WideString;
begin
     ErrorCount := 0;
     FActualizarBD := FALSE;
     with cdsSistBaseDatos do
     begin
          // Validaci�n de base de datos de empleados.
          if  (FieldByName ('DB_TIPO').AsInteger = Ord (tc3Datos)) and ( State in [ dsEdit, dsInsert ]) then
          begin
             sAdvertenciaValidarBDEmpleados := ValidarBDEmpleados(FieldByName ('DB_CODIGO').AsString, FieldByName('DB_DATOS').AsString);
             if sAdvertenciaValidarBDEmpleados <> VACIO then
                ZWarning('Advertencia', sAdvertenciaValidarBDEmpleados, 0, mbOK);
          end;
          
          if State in [ dsEdit, dsInsert ] then
          begin
             PostData;
             FActualizarBD := TRUE;
          end;

          if ( ChangeCount > 0 ) then
          begin
               if not Reconcile( ServerSistema.GrabaBD( Delta, ErrorCount ) ) then
                    FActualizarBD := FALSE;
          end
          else
              FActualizarBD := FALSE;
     end;
end;

procedure TdmBaseSistema.cdsSistBaseDatosAfterDelete(DataSet: TDataSet);
begin
     cdsSistBaseDatos.Enviar;
end;

procedure TdmBaseSistema.cdsSistBaseDatosBeforePost(DataSet: TDataSet);
begin
     inherited;
     with DataSet do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'DB_CODIGO' ).AsString ) then
             DataBaseError( 'C�digo no puede quedar vac�o' )
          else if ZetaCommonTools.StrVacio( FieldByName( 'DB_DATOS' ).AsString ) then
             DataBaseError( 'Ubicaci�n no puede quedar vac�o' )
          else
          begin
              // Solo si cambi� el Password, se encripatar�
              if FieldByName ('DB_PASSWRD').NewValue <> FieldByName ('DB_PASSWRD').OldValue then
                 FieldByName ('DB_PASSWRD').AsString := ZetaServerTools.Encrypt( FieldByName ('DB_PASSWRD').AsString );
          end;
     end;
end;

procedure TdmBaseSistema.cdsSistBaseDatosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError : String;
begin
     // if FK_Violation(E)then
     if (  Pos( 'REFERENCE', UpperCase( E.Message )) > 0) then
             sError := Format('No se puede borrar la base de datos: %s. Se encuentra asignada a una o m�s empresas',
                    [ cdsSistBaseDatos.FieldByName('DB_CODIGO').AsString +' : '+ cdsSistBaseDatos.FieldByName('DB_DESCRIP').AsString])
     else if (  Pos( 'UNIQUE', UpperCase( E.Message )) > 0) then
             sError := Format('La ubicaci�n: %s ya se encuentra asignada a una base de datos',[ cdsSistBaseDatos.FieldByName('DB_DATOS').AsString])
     else if PK_Violation (E)then
             sError := Format('El c�digo de base de datos: %s ya se encuentra dado de alta',[ cdsSistBaseDatos.FieldByName('DB_CODIGO').AsString])
     else
         sError := E.Message;

     ZetaDialogo.ZWarning('Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 , mbOK);
     case UpdateKind of
          ukDelete: Action := raCancel;
     else
         Action := raAbort;
     end;
end;

procedure TdmBaseSistema.cdsSistBaseDatosNewRecord(DataSet: TDataSet);
begin
     with cdsSistBaseDatos do
     begin
          FieldByName( 'DB_USRNAME' ).AsString := GetSQLUserName;
          FieldByName( 'DB_CODIGO' ).AsString := VACIO;
          FieldByName( 'DB_USRDFLT' ).AsString := K_GLOBAL_SI;
          FieldByName( 'DB_ESPC' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmBaseSistema.cdsSistBaseDatosLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsSistBaseDatos do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque el traspaso de Data regresa el
          begin                          // dataset a dsBrowse
               Conectar;
               cdsSistBaseDatosLookUp.Data := Data;
          end;
     end;
end;

procedure TdmBaseSistema.cdsSistBaseDatosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_SIST_BASE_DATOS, iRight );
end;

procedure TdmBaseSistema.cdsSistBaseDatosLookUpLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
          var sKey, sDescription: String);
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     lOk := ZetaBusqueda_DevEx.ShowSearchForm( Sender, sFilter, sKey, sDescription, false );
{$ELSE}
     lOk := ZetaBusqueda.ShowSearchForm( Sender, sFilter, sKey, sDescription, false )
{$ENDIF}
end;

function TdmBaseSistema.ValidarBDEmpleados (sCodigo, sBaseDatos: String; bWizEmpleados: Boolean = FALSE): WideString;
         var sMensaje: WideString;
begin
     if bWizEmpleados then
     begin
        ServerSistema.ValidarBDEmpleados(sCodigo, sBaseDatos, sMensaje);
        Result := sMensaje;
     end
     else
     begin
         try
            ServerSistema.ValidarBDEmpleados(sCodigo, sBaseDatos, sMensaje);
            Result := sMensaje;
         except

               Result := VACIO;
         end;
     end;
end;

function TdmBaseSistema.GetSQLUserName: String;
begin
     try
        Result := ServerSistema.GetSQLUserName;
     except
           Result := '';
     end;
end;

function TdmBaseSistema.GetServer: String;
begin
    Result := ServerSistema.GetServer;
end;

procedure TdmBaseSistema.GetBasesDatos (CM_DATOS, CM_USRNAME, CM_PASSWRD: String);
begin
     cdsBasesDatos.Data := ServerSistema.GetBasesDatos (CM_DATOS, CM_USRNAME, CM_PASSWRD);
end;

{***DevEx (by am): Anteriormente este evento estaba ligado a cdsEmpresasLookUpGetRigths,
pero se detecoto que no tiene un evento AlModificar por lo tanto el boton de Modificar estaba visible
pero no hacia nada. Para corregir este error se le hizo su propio metodo a cdsEmpresasLook***}
procedure TdmBaseSistema.cdsEmpresasLookUpGetRights(
  Sender: TZetaClientDataSet; const iRight: Integer;
  var lHasRights: Boolean);
begin
      lHasRights := FALSE;
end;

procedure TdmBaseSistema.GrabaBaseAdicionales;
begin
     {$ifndef EVALUACION}     //DChavez  se agrego esta directiva por que evaluacion CFG usa un dsistema propio y no incluye este metodo.
       {$ifndef VISITANTES_MGR} //DChavez Correcion del bug #13431 Invalid object name 'GRUPO_AD' al querer editar derechos de acceso 
          ServerSistema.GrabaBaseAdicionales( GetEmpresaAccesos, cdsGrupos.FieldByName('GR_CODIGO').AsInteger);
       {$ENDIF}
     {$ENDIF}
end;


procedure TdmBaseSistema.cdsSistTareaCalendarioAfterCancel(DataSet: TDataSet);
begin
     {$ifdef SUSCRIPCION}
     inherited;
     cdsSistTareaCalendario.CancelUpdates;
     {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaCalendarioAfterDelete(DataSet: TDataSet);
begin
     {$ifdef SUSCRIPCION}
     cdsSistTareaCalendario.Enviar;
     {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaCalendarioAlAdquirirDatos(Sender: TObject);
begin
     {$ifdef SUSCRIPCION}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     cdsSistTareaCalendario.Data := ServerSistema.GetTareaCalendario( dmCliente.Empresa, dmCliente.GetGrupoActivo );
     {$IFNDEF RDDAPP}
     dmReportes.EmpresaReportes := BuildEmpresa(dmCliente.Compania);
     dmReportes.cdsLookupReportesEmpresa.Conectar;
     dmReportes.cdsSuscripReportesLookup.Conectar;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaCalendarioAlCrearCampos(Sender: TObject);
begin
     {$ifdef SUSCRIPCION}
     ConectarUsuariosLookup;
     cdsEmpresas.Conectar;
     with cdsSistTareaCalendario do
     begin
          ListaFija( 'CA_FREC', lfEmailFrecuencia );
          {FieldByName('CA_NX_FEC').EditMask := '!90:00;0';
          FieldByName('CA_ULT_FEC').EditMask := '!00:00;0';
          FieldByName('CA_FECHA').EditMask := '!90:00;0';}
          FieldByName('CA_NX_FEC').OnGetText := CA_NX_FECGetText;
          FieldByName('CA_ULT_FEC').OnGetText := CA_NX_FECGetText;
          FieldByName('CA_FECHA').OnGetText := CA_NX_FECGetText;
          MaskFecha('CA_CAPTURA');

          CreateSimpleLookup( cdsUsuariosLookUp, 'US_DESCRIP', 'CA_US_CHG' );
          CreateSimpleLookup( cdsEmpresas, 'CM_NOMBRE', 'CM_CODIGO' );
     end;
     {$endif}

end;

procedure TdmBaseSistema.CA_NX_FECGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var sFecha : String;
begin
     {$ifdef SUSCRIPCION}
     if DisplayText then
     begin
          DateTimeToString(sFecha, 'MM/dd/yyyy', Sender.AsDateTime);
          if sFecha = '12/30/1899' then
             Text := VACIO
          else
              Text := FormatDateTime ('dd/MMMMM/yyyy hh:nn:ss AM/PM', Sender. AsDateTime);
     end;
     {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaCalendarioAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     {$ifdef SUSCRIPCION}
     inherited;
     ErrorCount := 0;
     with cdsSistTareaCalendario do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerSistema.GrabaCalendario( Delta, ErrorCount ) );
          end;
     end;
     {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaCalendarioAlModificar(Sender: TObject);
begin
     {$ifdef SUSCRIPCION}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     {$IFNDEF RDDAPP}
     inherited;
     EnvioProgramadoXEmpresa := False;
     ZcxWizardBasico.ShowWizard(TWizSistCalendarioReportes_DevEx);
     cdsEnviosProgramadosEmpresa.Refrescar;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$endif}
end;



procedure TdmBaseSistema.cdsSistTareaCalendarioNewRecord(DataSet: TDataSet);
begin
     {$ifdef SUSCRIPCION}
     dmCliente.cdsCompany.Conectar;
     with cdsSistTareaCalendario do
     begin
          FieldByName( 'CA_FREC' ).AsInteger := ord(frSemanal);
          FieldByName( 'CA_HORA' ).AsString := FormatDateTime('hhnn', Now);
          FieldByName( 'CA_FECHA' ).AsDateTime := Date();
          FieldByName( 'CA_RECUR' ).AsInteger := 1;
          FieldByName( 'CA_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CM_CODIGO' ).AsString := dmCliente.cdsCompany.FieldByName( 'CM_CODIGO' ).AsString;
     end;
     {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaRolesAlAdquirirDatos(Sender: TObject);
begin
     {$ifdef SUSCRIPCION}
     inherited;
     if  EnvioProgramadoXEmpresa = true then
     begin
         cdsSistTareaRoles.Data := ServerSistema.GetTareaRoles( dmCliente.Empresa, cdsEnviosProgramadosEmpresa.FieldByName('CA_FOLIO').AsInteger );
     end
     else
     begin
         cdsSistTareaRoles.Data := ServerSistema.GetTareaRoles( dmCliente.Empresa, cdsSistTareaCalendario.FieldByName('CA_FOLIO').AsInteger );
     end;
     {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaRolesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     {$ifdef SUSCRIPCION}
     ErrorCount := 0;
     with cdsSistTareaRoles do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if  EnvioProgramadoXEmpresa = true then
               begin
                    Reconcile( ServerSistema.GrabaTareaRoles( Delta, ErrorCount, cdsEnviosProgramadosEmpresa.FieldByName('CA_FOLIO').AsInteger ) );
               end
               else
               begin
                    Reconcile( ServerSistema.GrabaTareaRoles( Delta, ErrorCount, cdsSistTareaCalendario.FieldByName('CA_FOLIO').AsInteger ) );
               end;
          end;
     end;
     {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaUsuarioAlAdquirirDatos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     inherited;

     if  EnvioProgramadoXEmpresa = true then
     begin
         cdsSistTareaUsuario.Data := ServerSistema.GetTareaUsuario( dmCliente.Empresa, cdsEnviosProgramadosEmpresa.FieldByName('CA_FOLIO').AsInteger );
     end
     else
     begin
         cdsSistTareaUsuario.Data := ServerSistema.GetTareaUsuario( dmCliente.Empresa, cdsSistTareaCalendario.FieldByName('CA_FOLIO').AsInteger );
     end;
 {$endif}
end;

procedure TdmBaseSistema.cdsSistTareaUsuarioAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
{$ifdef SUSCRIPCION}
     ErrorCount := 0;
     with cdsSistTareaUsuario do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if  EnvioProgramadoXEmpresa = true then
               begin
                    Reconcile( ServerSistema.GrabaTareaUsuario( Delta, ErrorCount, cdsEnviosProgramadosEmpresa.FieldByName('CA_FOLIO').AsInteger ) );
               end
               else
               begin
                    Reconcile( ServerSistema.GrabaTareaUsuario( Delta, ErrorCount, cdsSistTareaCalendario.FieldByName('CA_FOLIO').AsInteger ) );
               end;
          end;
     end;
 {$endif}
end;

procedure TdmBaseSistema.cdsSuscripCalendarioReportesAlAdquirirDatos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
  inherited;
          if StrVacio(CodigoReporte) then
             CodigoReporte := '0';

          cdsSuscripCalendarioReportes.Data := ServerSistema.GetSuscripcionesCalendarioReportes( dmCliente.Empresa, StrAsInteger(CodigoReporte) );
 {$endif}
end;

procedure TdmBaseSistema.cdsSuscripCalendarioReportesAlCrearCampos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
  inherited;
     with cdsSuscripCalendarioReportes do
     begin
          ListaFija( 'CA_FREC', lfEmailFrecuencia );
          MaskTime( 'CA_HORA' );
     end;
 {$endif}
end;



procedure TdmBaseSistema.DescargaListaRolesCalendario( Lista: TStrings );
var
   i, iUsuario, iFolio: Integer;
begin
{$ifdef SUSCRIPCION}
     iUsuario := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
     if  EnvioProgramadoXEmpresa = true then
     begin
          iFolio := cdsEnviosProgramadosEmpresa.FieldByName( 'CA_FOLIO' ).AsInteger;
     end
     else
     begin
          iFolio := cdsSistTareaCalendario.FieldByName( 'CA_FOLIO' ).AsInteger;
     end;

     cdsSistTareaRoles.EmptyDataset;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with cdsSistTareaRoles do
               begin
                    Append;
                    FieldByName( 'RO_CODIGO' ).AsString := Names[i];
                    FieldByName( 'CA_FOLIO' ).AsInteger := iFolio;
                    if Integer( Lista.Objects[ i ] ) > 0 then
                       FieldByName( 'RS_ACTIVO' ).AsString := K_GLOBAL_SI
                    else
                        FieldByName( 'RS_ACTIVO' ).AsString := K_GLOBAL_NO;

                    FieldByName( 'RS_VACIO' ).AsInteger := 0;
                    FieldByName( 'RS_US_CHG' ).AsInteger := iUsuario;
                    Post;
               end;
          end;
     end;

     cdsSistTareaRoles.Enviar;
  {$endif}
end;

procedure TdmBaseSistema.DescargaListaUsuariosCalendario( Lista: TStrings );
var
   i, iUsuario, iFolio: Integer;
begin
{$ifdef SUSCRIPCION}
     iUsuario := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
     if  EnvioProgramadoXEmpresa = true then
     begin
          iFolio := cdsEnviosProgramadosEmpresa.FieldByName( 'CA_FOLIO' ).AsInteger;
     end
     else
     begin
          iFolio := cdsSistTareaCalendario.FieldByName( 'CA_FOLIO' ).AsInteger;
     end;

     cdsSistTareaUsuario.EmptyDataset;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with cdsSistTareaUsuario do
               begin
                    Append;
                    FieldByName( 'US_CODIGO' ).AsString := Names[i];
                    FieldByName( 'CA_FOLIO' ).AsInteger := iFolio;
                    if Integer( Lista.Objects[ i ] ) > 0 then
                       FieldByName( 'RU_ACTIVO' ).AsString := K_GLOBAL_SI
                    else
                        FieldByName( 'RU_ACTIVO' ).AsString := K_GLOBAL_NO;

                    FieldByName( 'RU_VACIO' ).AsInteger := 0;
                    FieldByName( 'RU_US_CHG' ).AsInteger := iUsuario;
                    Post;
               end;
          end;
     end;

     cdsSistTareaUsuario.Enviar;
       {$endif}
end;



procedure TdmBaseSistema.cdsEnviosProgramadosEmpresaAlModificar(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     {$IFNDEF RDDAPP}
     inherited;
     EnvioProgramadoXEmpresa := True;
     TituloProceso := False;
     ZcxWizardBasico.ShowWizard(TWizSistCalendarioReportes_DevEx);
     cdsSistTareaCalendario.Refrescar;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$endif}
     {$endif}
end;

procedure TdmBaseSistema.cdsEnviosProgramadosEmpresaNewRecord(DataSet: TDataSet);
begin
{$ifdef SUSCRIPCION}
     dmCliente.cdsCompany.Conectar;
     with cdsEnviosProgramadosEmpresa do
     begin
          FieldByName( 'CA_FREC' ).AsInteger := ord(frSemanal);
          FieldByName( 'CA_HORA' ).AsString := FormatDateTime('hhnn', Now);
          FieldByName( 'CA_FECHA' ).AsDateTime := Date();
          FieldByName( 'CA_RECUR' ).AsInteger := 1;
          FieldByName( 'CA_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CM_CODIGO' ).AsString := dmCliente.cdsCompany.FieldByName( 'CM_CODIGO' ).AsString;
     end;
       {$endif}
end;



procedure TdmBaseSistema.cdsEnviosProgramadosEmpresaAfterCancel(DataSet: TDataSet);
begin
{$ifdef SUSCRIPCION}
  inherited;
     cdsEnviosProgramadosEmpresa.CancelUpdates;
       {$endif}
end;

procedure TdmBaseSistema.cdsEnviosProgramadosEmpresaAfterDelete(DataSet: TDataSet);
begin
{$ifdef SUSCRIPCION}
     cdsEnviosProgramadosEmpresa.Enviar;
{$endif}
end;

procedure TdmBaseSistema.cdsEnviosProgramadosEmpresaAlAdquirirDatos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     {$IFNDEF RDDAPP}
     cdsEnviosProgramadosEmpresa.Data := ServerSistema.GetTareaCalendarioEmpresa( dmCliente.Empresa, dmCliente.GetGrupoActivo );
     dmReportes.EmpresaReportes := BuildEmpresa(dmCliente.Compania);
     dmReportes.cdsLookupReportesEmpresa.Conectar;
     dmReportes.cdsSuscripReportesLookup.Conectar;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
end;

procedure TdmBaseSistema.cdsEnviosProgramadosEmpresaAlCrearCampos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
{$ifndef VISITANTES}
{$ifndef RDDAPP}
     ConectarUsuariosLookup;
     cdsEmpresas.Conectar;
     dmReportes.EmpresaReportes := BuildEmpresa(dmCliente.Compania);
     with cdsEnviosProgramadosEmpresa do
     begin
          ListaFija( 'CA_FREC', lfEmailFrecuencia );
          FieldByName('CA_NX_FEC').OnGetText := CA_NX_FECGetText;
          FieldByName('CA_ULT_FEC').OnGetText := CA_NX_FECGetText;
          FieldByName('CA_FECHA').OnGetText := CA_NX_FECGetText;
          MaskFecha('CA_CAPTURA');

          CreateSimpleLookup( cdsUsuariosLookUp, 'US_DESCRIP', 'CA_US_CHG' );
          CreateSimpleLookup( cdsEmpresas, 'CM_NOMBRE', 'CM_CODIGO' );
     end;
{$endif}
{$endif}
{$endif}
end;

procedure TdmBaseSistema.cdsEnviosProgramadosEmpresaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
{$ifdef SUSCRIPCION}
     inherited;
     ErrorCount := 0;
     with cdsEnviosProgramadosEmpresa do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerSistema.GrabaCalendario( Delta, ErrorCount ) );
          end;
     end;
{$endif}
end;



procedure TdmBaseSistema.cdsSistTareaCalendarioLookupAlAdquirirDatos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     inherited;
     cdsSistTareaCalendarioLookup.Data := ServerSistema.GetTareaCalendario( dmCliente.Empresa, 1 );
{$endif}
end;


procedure TdmBaseSistema.EliminarUsuariosRolesCalendario(iTipoOperacion, iFolio, iUsCodigo: integer; sRoCodigo: string; var iContadorRegistros: integer);
begin
{$ifdef SUSCRIPCION}
      iContadorRegistros := 0;
      if iTipoOperacion > 0 then
      begin
           ServerSistema.BorrarUsuarioRolCalendario( dmCliente.Empresa, iFolio, iUsCodigo, sRoCodigo, iTipoOperacion, iContadorRegistros );
      end;
{$endif}
end;


procedure TdmBaseSistema.RefrescarSolicitudEnvios(Parametros: TZetaParams);
begin
{$ifdef SUSCRIPCION}
     cdsSistTareaCalendarioLookup.Conectar;
     cdsSistSolicitudEnvios.Data := ServerSistema.GetSolicitudEnvio( dmCliente.Empresa, Parametros.VarValues );
{$endif}
end;

function TdmBaseSistema.InsertarSolicitudEnvio(oDataSet: TZetaClientDataSet) : String;
var sReporte, sMensaje : WideString;
begin
{$ifdef SUSCRIPCION}
     with oDataSet do
     begin
          ServerSistema.AgregarSolicitudEnvioProgramado( FieldByName ('CA_FOLIO').AsInteger, FieldByName ('CA_REPORT').AsInteger, sReporte, sMensaje );
          Result := sMensaje;
          cdsSistTareaCalendarioLookup.SetDataChange;
     end;
{$endif}
end;

function TdmBaseSistema.CancelarSolicitudEnvio : String;
var sMensaje : WideString;
begin
{$ifdef SUSCRIPCION}
     with cdsSistSolicitudEnvios do
     begin
          ServerSistema.CancelarEnvio(FieldByName('CAL_FOLIO').AsInteger, sMensaje);
          Result := sMensaje;
     end;
{$endif}
end;

procedure TdmBaseSistema.cdsSistSolicitudEnviosAlCrearCampos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     inherited;
     cdsSistTareaCalendarioLookup.Conectar;
     with cdsSistSolicitudEnvios do
     begin
          ListaFija( 'CAL_ESTATUS', lfSolicitudEnvios );
          FieldByName('CAL_INICIO').OnGetText := CA_NX_FECGetText;
          FieldByName('CAL_FIN').OnGetText := CA_NX_FECGetText;
          FieldByName('CAL_FECHA').OnGetText := CA_NX_FECGetText;
          CreateSimpleLookup( cdsSistTareaCalendarioLookup, 'CA_NOMBRE', 'CA_FOLIO' );
     end;
{$endif}
end;

procedure TdmBaseSistema.cdsSistSolicitudEnviosAlModificar(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     with cdsSistSolicitudEnvios do
     begin
          if not IsEmpty then
          begin
               FSistSolicitudEnviosLog_DevEx.ShowLog;
          end;
     end;
{$endif}
end;


procedure TdmBaseSistema.cdsSistEnvioEmailAlAdquirirDatos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     inherited;
     cdsSistEnvioEmail.Data := ServerSistema.GetSistEnvioEmail( dmCliente.Empresa );
{$endif}
end;

procedure TdmBaseSistema.cdsSistEnvioEmailAlCrearCampos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     inherited;
     cdsSistEnvioEmail.ListaFija('EE_STATUS', lfEVCorreoStatus);
     cdsSistEnvioEmail.ListaFija('EE_SUBTYPE', lfEmailType );
     cdsSistEnvioEmail.FieldByName('EE_FEC_IN').OnGetText := {EE_FEC_INGetText}CA_NX_FECGetText;
     cdsSistEnvioEmail.FieldByName('EE_FEC_OUT').OnGetText := CA_NX_FECGetText;
{$endif}
end;

procedure TdmBaseSistema.cdsSistEnvioEmailAlModificar(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     inherited;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditSistSolicitudReportes_DevEx, TEditSistSolicitudReportes_DevEx);
{$endif}
end;


{ cdsBitacoraReportes Inicio }
procedure TdmBaseSistema.cdsBitacoraReportesAfterOpen(DataSet: TDataSet);
begin
{$ifdef SUSCRIPCION}
     inherited;
     with cdsBitacoraReportes do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'CA_FREC', lfEmailFrecuencia );
          // ListaFija( 'BI_CLASE', lfClaseSistBitacora );
          MaskFecha( 'BI_FECHA' );
          // MaskFecha( 'BI_FEC_MOV' );
     end;
{$endif}
end;

procedure TdmBaseSistema.cdsBitacoraReportesAlCrearCampos(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     inherited;
     with cdsBitacoraReportes do
     begin
          MaskFecha( 'BI_FECHA' );
          ListaFija( 'CA_FREC', lfEmailFrecuencia );
         //  MaskFecha( 'BI_FEC_MOV' );
     end;
{$endif}
end;

procedure TdmBaseSistema.cdsBitacoraReportesAlModificar(Sender: TObject);
begin
{$ifdef SUSCRIPCION}
     FEditBitacoraReportes_DevEx.ShowLogDetail( EditBitacoraReportes_DevEx, TEditBitacoraReportes_DevEx, cdsBitacoraReportes );
{$endif}
end;

procedure TdmBaseSistema.RefrescarBitacoraReportes(Parametros: TZetaParams);
begin
{$ifdef SUSCRIPCION}
     cdsBitacoraReportes.Data := ServerSistema.GetBitacoraReportes( dmCliente.Empresa, Parametros.VarValues );
{$endif}
end;

procedure TdmBaseSistema.RefrescarBitacoraCorreos(Parametros: TZetaParams);
begin
{$ifdef SUSCRIPCION}
     cdsSistEnvioEmail.Data := ServerSistema.GetBitacoraCorreos( dmCliente.Empresa, Parametros.VarValues );
{$endif}
end;


procedure TdmBaseSistema.CargaListaRolesCalendario( Lista: TStrings );
begin
{$ifdef SUSCRIPCION}
     with cdsRoles do
     begin
          First;
          with Lista do
          begin
               BeginUpdate;
               Clear;
               while not Eof do
               begin
                    AddObject( FieldByName( 'RO_CODIGO' ).AsString+' = '+FieldByName( 'RO_NOMBRE' ).AsString , TObject( FieldByName( 'RO_NOMBRE' ).AsString ) );
                    Next;
               end;
          EndUpdate;
          end;
     end;
{$endif}
end;



procedure TdmBaseSistema.CargaListaUsuariosCalendario(Lista: TStrings; Filtro : Boolean);
begin
{$ifdef SUSCRIPCION}
     with cdsUsuarios do
     begin
          Filtered := False;
          Filter := Format( 'US_ACTIVO=%s', [ EntreComillas(K_GLOBAL_SI) ] );
          Filtered := Filtro;
          First;
          with Lista do
          begin
               BeginUpdate;
               Clear;
               while not Eof do
               begin
                    AddObject( FieldByName( 'US_CODIGO' ).AsString+' = '+FieldByName( 'US_NOMBRE' ).AsString  + ' ('+FieldByName( 'US_CORTO' ).AsString+ ')' , TObject( FieldByName( 'US_CODIGO' ).AsString ) );
                    Next;
               end;
          EndUpdate;
          end;
     end;
{$endif}
end;


end.

