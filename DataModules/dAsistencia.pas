unit DAsistencia;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,

{$IFDEF DOS_CAPAS}
     DServerAsistencia,DServerSuper,
{$else}
     Asistencia_TLB, Super_TLB,
{$endif}
{$ifndef VER130}
    Variants,
{$endif}
     {$ifdef TRESS_DELPHIXE5_UP}System.UITypes,{$endif}
     ZetaClientDataSet,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses;

const
     K_AUTORIZACIONES_OFFSET = 5;            // Para Compensar la diferencia entre eTipoChecadas y eAutorizaChecadas - CHECADAS.CH_TIPO
type
  TdmAsistencia = class(TDataModule)
    cdsAsisTarjeta: TZetaClientDataSet;
    cdsAutorizaciones: TZetaClientDataSet;
    cdsAsisCalendario: TZetaClientDataSet;
    cdsChecadas: TZetaClientDataSet;
    cdsHorarioTemp: TZetaClientDataSet;
    cdsErrores: TZetaClientDataSet;
    cdsAutoXAprobar: TZetaClientDataSet;
    cdsClasifiTemp: TZetaClientDataSet;
    cdsCalendarioEmpleado: TZetaClientDataSet;
    cdsGridTarjetasPeriodo: TZetaClientDataSet;
    cdsCosteoTransferencias: TZetaClientDataSet;
    cdsCosteoTarjetaLookup: TZetaClientDataSet;
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsGridTarjetasPeriodoReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsAutorizacionesReconcileError(DataSet: TClientDataSet;  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHorarioTempReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsGridTarjetasPeriodoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsAutorizacionesReconcileError(DataSet: TCustomClientDataSet;  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHorarioTempReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsAsisTarjetaAlAdquirirDatos(Sender: TObject);
    procedure cdsAutorizacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsAsisCalendarioAlAdquirirDatos(Sender: TObject);
    procedure dmAsistenciaCreate(Sender: TObject);
    procedure cdsAsisCalendarioAfterOpen(DataSet: TDataSet);
    procedure AU_FECHAChange(Sender: TField);
    procedure HO_CODIGOChange(Sender: TField);
    procedure cdsAsisTarjetaHO_CODIGOChange(Sender: TField);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CH_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CH_H_REALSetText(Sender: TField; const Text: String);
    procedure CH_H_REALChange(Sender: TField);
    procedure CHECADAChange(Sender: TField);
    procedure CH_H_REALValidate(Sender: TField);
    procedure CH_H_AJUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure DescansoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure AprobadasGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CH_RELOJGetText(Sender: TField; var Text: String; DisplayText: Boolean);{ACL150409}
    procedure CH_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);{ACL150409}
    procedure cdsAutorizacionesAlModificar(Sender: TObject);
    procedure cdsAutorizacionesNewRecord(DataSet: TDataSet);
    procedure cdsAutorizacionesBeforePost(DataSet: TDataSet);
    procedure cdsAutorizacionesAlCrearCampos(Sender: TObject);
    procedure cdsAsisCalendarioAlModificar(Sender: TObject);
    procedure cdsAsisCalendarioNewRecord(DataSet: TDataSet);
    procedure cdsChecadasAfterDelete(DataSet: TDataSet);
    procedure cdsAsisTarjetaAfterCancel(DataSet: TDataSet);
    procedure cdsAsisTarjetaAlCrearCampos(Sender: TObject);
    procedure cdsChecadasAlCrearCampos(Sender: TObject);
    procedure cdsAsisTarjetaAlModificar(Sender: TObject);
    procedure cdsAsisTarjetaNewRecord(DataSet: TDataSet);
    procedure cdsChecadasNewRecord(DataSet: TDataSet);
    procedure cdsAsisTarjetaAlEnviarDatos(Sender: TObject);
    procedure cdsAutorizacionesAlEnviarDatos(Sender: TObject);
    procedure cdsAutorizacionesBeforeOpen(DataSet: TDataSet);
    procedure cdsAutorizacionesAfterPost(DataSet: TDataSet);
//    procedure cdsAutorizacionesAfterEdit(DataSet: TDataSet);
    procedure cdsAutorizacionesCB_CODIGOChange(Sender: TField);
    procedure cdsAutorizacionesCB_CODIGOValidate(Sender: TField);
    procedure cdsAutorizacionesCH_RELOJChange(Sender: TField);
    procedure cdsAutorizacionesCH_RELOJValidate(Sender: TField);
    procedure cdsAutorizacionesCH_HOR_DESChange(Sender: TField);
    procedure cdsAutorizacionesCH_TIPOChange(Sender: TField);
    procedure cdsHorarioTempAlAdquirirDatos(Sender: TObject);
    procedure cdsHorarioTempAlCrearCampos(Sender: TObject);
    procedure cdsHorarioTempBeforePost(DataSet: TDataSet);
    procedure cdsHorarioTempAlEnviarDatos(Sender: TObject);
    procedure cdsHorarioTempHO_CODIGOChange(Sender: TField);
    procedure cdsHorarioTempHO_CODIGOValidate(Sender: TField);
    procedure cdsAutorizacionesAlBorrar(Sender: TObject);
    procedure cdsAsisCalendarioAlCrearCampos(Sender: TObject);
    procedure cdsAsisTarjetaAlBorrar(Sender: TObject);
    procedure cdsAsisTarjetaAfterOpen(DataSet: TDataSet);
    procedure cdsChecadasAfterInsert(DataSet: TDataSet);
    procedure cdsAsisTarjetaAfterInsert(DataSet: TDataSet);
    procedure cdsAsisCalendarioCalcFields(DataSet: TDataSet);
    procedure cdsAsisTarjetaCalcFields(DataSet: TDataSet);
    procedure cdsChecadasAlModificar(Sender: TObject);
    procedure cdsAsisCalendarioAlEnviarDatos(Sender: TObject);
    procedure cdsAsisCalendarioAfterDelete(DataSet: TDataSet);
    procedure cdsAsisTarjetaAlAgregar(Sender: TObject);
    procedure PuedeCambiarTarjetaBeforePost(DataSet: TDataSet);
    procedure cdsAutorizacionesBeforeEdit(DataSet: TDataSet);
    procedure cdsAutoXAprobarAlAdquirirDatos(Sender: TObject);
    procedure cdsAutoXAprobarAlCrearCampos(Sender: TObject);
    procedure cdsAutoXAprobarAlEnviarDatos(Sender: TObject);
    procedure cdsAutoXAprobarBeforePost(DataSet: TDataSet);
    procedure cdsClasifiTempAfterDelete(DataSet: TDataSet);
    procedure cdsClasifiTempAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiTempAlCrearCampos(Sender: TObject);
    procedure cdsClasifiTempAlEnviarDatos(Sender: TObject);
    procedure cdsClasifiTempAlModificar(Sender: TObject);
    procedure cdsClasifiTempBeforePost(DataSet: TDataSet);
    procedure cdsClasifiTempNewRecord(DataSet: TDataSet);
    procedure cdsClasifiTempCB_CODIGOChange(Sender: TField);
    procedure cdsGridTarjetasPeriodoAlAdquirirDatos(Sender: TObject);
    procedure cdsGridTarjetasPeriodoAlCrearCampos(Sender: TObject);
    procedure AU_FECHAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsMotAutoValidate(Sender: TField);
    procedure cdsHorarioValidate(Sender: TField);
    procedure ValidaLookup( const sValor,sFiltro : String ; oLookup: TZetaLookupDataSet  );
    procedure CH_H_REALValidar(Sender: TField);
    procedure cdsGridTarjetasPeriodoAlEnviarDatos(Sender: TObject);
    procedure cdsGridTarjetasPeriodoBeforePost(DataSet: TDataSet);
    procedure cdsChecadaManualValidate(Sender: TField);{ACL160409}
    procedure cdsMotChecadaValidate(Sender: TField);
    procedure cdsChecadasAfterPost(DataSet: TDataSet);
    procedure cdsMotChecadaChange(Sender: TField);
    procedure cdsGridTarjetasPeriodoBeforeOpen(DataSet: TDataSet);
    procedure cdsChecadasBeforePost(DataSet: TDataSet);
    procedure cdsChecadasAfterOpen(DataSet: TDataSet);
    procedure cdsChecadasAfterCancel(DataSet: TDataSet);
    procedure cdsCosteoTransferenciasAlCrearCampos(Sender: TObject);
    procedure cdsCosteoTransferenciasAlModificar(Sender: TObject);
    procedure cdsCosteoTransferenciasNewRecord(DataSet: TDataSet);
    procedure cdsCosteoTransferenciasAlEnviarDatos(Sender: TObject);
    procedure cdsCosteoTransferenciasBeforePost(DataSet: TDataSet);
    procedure cdsCosteoTransferenciasAfterPost(DataSet: TDataSet);
    procedure cdsCosteoTransferenciasAlBorrar(Sender: TObject);
    procedure cdsCosteoTransferenciasBeforeInsert(DataSet: TDataSet);
    procedure cdsCosteoTransferenciasAfterCancel(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsCosteoTransferenciasAlAgregar(Sender: TObject);
    procedure cdsCosteoTransferenciasCB_CODIGOValidate(Sender: TField);
    procedure cdsCosteoTransferenciasAU_FECHAValidate(Sender: TField);
    procedure TR_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure TR_STATUSChange(Sender: TField);

  private
    { Private declarations }
    FUltimaFecha: TDate;
    FUltimoTipo: eTipoChecadas;
    FUltimaHora: Double;
    FUltimoMotivo: String;
    FCalendarioInicio: TDate;
    FCalendarioFin: TDate;
    FHorarioInicio: TDate;
    FHorarioFin: TDate;
    FSoloAltas: Boolean;
    FCambioAutorizacion: Boolean;
    FOperacion: Integer;
    FChecadasBuffer: OleVariant;
    FChecadasHayBuffer: Boolean;
    FEmpleadoRegTarjeta: TNumEmp;
    FFechaRegTarjeta: TDate;
    FRegistrandoTarjetas: Boolean;
    FGrabandoTarjeta: Boolean;
    FAgregandoTarjeta: Boolean;
    FCambiaComboFestivo: Boolean;
    FHorario: String;
    FNewTarjeta: OleVariant;
    FNewChecadas: OleVariant;
    FFechaGridAuto: TDate;
    FUltimoMotivoCh: String;{ACL170409}
    FTransferUltimaFecha: TDate;
    FTransferUltimoTipo: eTipoHoraTransferenciaCosteo;
    FTransferUltimaHora: TDiasHoras;
    FTransferUltimoMotivo: string;
    FTransferUltimoCC: string;
    FAgregandoTransferencia: Boolean;
    FParametrosTransferencias : TZetaParams;
    FLastEmpleadoTransferencia: integer;

{$IFDEF DOS_CAPAS}
    function GetServerAsistencia: TdmServerAsistencia;
    property ServerAsistencia: TdmServerAsistencia read GetServerAsistencia;
    function GetServerSuper: TdmServerSuper;
    property ServerSuper: TdmServerSuper read GetServerSuper;
{$else}
    FServidor: IdmServerAsistenciaDisp;
    FServidorSuper: IdmServerSuperDisp;

    function GetServerAsistencia: IdmServerAsistenciaDisp ;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;

    function GetServerSuper: IdmServerSuperDisp ;
    property ServerSuper: IdmServerSuperDisp read GetServerSuper;
{$endif}
    function EnviarCalendario: Boolean;
    function GetDescTipo( const nTipo: Integer ): String;
{    function GetStatusHorario( const sTurno: String; const dReferencia: TDate ): TStatusHorario; }{acl} 
    procedure TarjetaNueva( Dataset: TDataset );
    procedure LlenaAusenciaNuevo( Dataset: TDataset );
    function PrenomAutorizada ( Error: EReconcileError ):Boolean;
    procedure AsignaCCOrigen; //Proyecto Transferencias-

  protected
    { Protected declarations }
    {$IFDEF DOS_CAPAS}
    FServerSuper: TdmServerSuper;
    {$endif}
  public
    { Public declarations }
    function GetStatusHorario( const sTurno: String; const dReferencia: TDate ): TStatusHorario;//acl
    property CambiaComboFestivo: boolean read FCambiaComboFestivo write FCambiaComboFestivo;
    property CambiaHorario: String read FHorario write FHorario;
    property CalFechaIni: TDate read FCalendarioInicio write FCalendarioInicio;
    property CalFechaFin: TDate read FCalendarioFin write FCalendarioFin;
    property SoloAltas: Boolean read FSoloAltas write FSoloAltas;
    property OperacionConflicto : Integer read FOperacion write FOperacion;
    property CambioAutorizacion: Boolean read FCambioAutorizacion write FCambioAutorizacion;
    property EmpleadoRegTarjeta: TNumEmp read FEmpleadoRegTarjeta write FEmpleadoRegTarjeta;
    property FechaRegTarjeta: TDate read FFechaRegTarjeta write FFechaRegTarjeta;
    property RegistrandoTarjetas: Boolean read FRegistrandoTarjetas write FRegistrandoTarjetas;
    property FechaGridAuto: TDate read FFechaGridAuto write FFechaGridAuto;
    property ParametrosTransferencias: TZetaParams read FParametrosTransferencias write FParametrosTransferencias; 
    function GetVacaDiasGozar( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
    function GetVacaFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const rGozo: TPesos; var rDiasRango: Double ): TDate;
    function TieneDerechoAprobar: boolean;
    function AprobarPrendido: boolean;
    procedure SetCalendarioInicio;
    procedure EditarGridAutorizaciones( const lSoloAltas: Boolean );
    procedure EditarGridHorarioTemp;
    procedure EditarGridAprobarAutorizaciones;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure RegistraTarjetas( const TipoRegistro: TTipoRegTarjeta );
    procedure AplicaFiltros(sFiltros: String);
    procedure MuestraCalendarioEmpleado;
    procedure MuestraCalendarioVacacion( const rDiasGozar : Double );{OP: 11/06/08}
    procedure ResetCalendarioEmpleadoActivo( const dInicio, dFinal: TDate );
    function PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                        const iEmpleado: integer;
                                        const eTipo: eStatusPeriodo;
                                        const lPuedeModificar: Boolean;
                                        var sMensaje: string): Boolean;
    function GetChecadaPuntual(const sHorario: string; iTipoChecada: Integer; var sChecada: String): boolean;
    procedure RegistrarTarjetaPuntual(const sChecada: string);
    function ValidarMotivoAutorizacion(const sCodigo :string;const iTipo :Integer):Boolean;
    function GetTipoMotivo( sCampo: String ): Integer;
    function Aprobar_Autorizaciones( oDataSet : TDataSet; const sAprobacion: string ): Boolean;
    procedure RegistrarTarjetaPuntualColectiva( DataSet:TZetaClientDataSet;const sChecada, sCampoChecada,sTitulo: string);
    procedure ObtieneTransferencias( Params: TZetaParams );
    function GetPermisoFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const Dias: integer; var rDiasRango: Double ): TDate;
    function GetPermisoDiasHabiles( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
    function GetHorarioTurno( const iEmpleado: Integer; const dFecha: TDate; iTipo: Integer ): String;

  end;

var
  dmAsistencia: TdmAsistencia;

implementation

uses DCliente,
     DRecursos,
     DTablas,
     DGlobal,
     DCatalogos,
     DSistema,
     ZReconcile,
     ZetaMsgDlg,
     ZetaCommonTools,
     ZetaClientTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     ZBaseEdicion_DevEx,
     ZBaseGridEdicion_DevEx,
     FTressShell,
     FEditAutorizaciones_DevEx,
//**     FEditAutorizacionesTarjeta,
     FEditCalendario_DevEx,
     FEditClasifTemporal_DevEx,
     FGridAutorizaciones_DevEx,
     FGridAutorizacionesXAprobar_DevEx,
     FAutorizacionRepetida_DevEx,
     FHorarioTemp_DevEx,
     FGridHorarioTemp_DevEx,
     FRegTarjeta_DevEx,
     FToolsAsistencia,
     FCalendarioEmpleado_DevEx,
     FCalendarioVacacion_DevEx,
     FEditCosteoTransferencias_DevEx,
{$ifdef INTERFAZ_ELECTROLUX_KRONOS}
     DInterfase,
{$endif}
{$ifdef INTERFAZ_MJD_KRONOS}
     DInterfase,
{$endif}
     ZetaDialogo;

{$R *.DFM}

{$define QUINCENALES}
{$define CAMBIO_TNOM}

const
     K_HORA_NULA = '----';

{ ************ TdmAsistencia ************* }

procedure TdmAsistencia.dmAsistenciaCreate(Sender: TObject);
begin
     FSoloAltas := False;
     FOperacion := Ord( ocReportar );
     FRegistrandoTarjetas := FALSE;
     FGrabandoTarjeta := FALSE;
     FAgregandoTarjeta := False;
     FCambiaComboFestivo := FALSE;
     FAgregandoTransferencia := FALSE;
     {$ifdef DOS_CAPAS}
     FServerSuper := TdmServerSuper.Create( Self );
     {$endif}
end;

procedure TdmAsistencia.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FParametrosTransferencias );
end;

{$ifdef DOS_CAPAS}
function TdmAsistencia.GetServerAsistencia: TdmServerAsistencia;
begin
     Result := DCliente.dmCliente.ServerAsistencia;
end;

function TdmAsistencia.GetServerSuper: TdmServerSuper;
begin
     Result := FServerSuper;
end;
{$else}
function TdmAsistencia.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( dmCliente.CreaServidor( CLASS_dmServerAsistencia, FServidor ) );
end;

function TdmAsistencia.GetServerSuper: IdmServerSuperDisp;
begin
     Result := IdmServerSuperDisp( dmCliente.CreaServidor( CLASS_dmServerSuper, FServidorSuper ) );
end;

{$endif}

{$ifdef VER130}
procedure TdmAsistencia.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmAsistencia.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     FGrabandoTarjeta := not PrenomAutorizada(E);
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

function TdmAsistencia.PrenomAutorizada ( Error: EReconcileError ):Boolean;
begin
     with Error do
     begin
          Result := ( Pos( ' Pre-N�mina Autorizada', Message ) > 0 );
     end;
end;



{$ifdef VER130}
procedure TdmAsistencia.cdsGridTarjetasPeriodoReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmAsistencia.cdsGridTarjetasPeriodoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     with DataSet do
          sError:= Format( ' � Error al Realizar Ajuste de Asistencia !' + CR_LF +
                             ' Fecha: %s Empleado: %d ', [ FechaCorta( FieldByName('AU_FECHA').AsDateTime), FieldByName('CB_CODIGO').AsInteger ] );
     cdsGridTarjetasPeriodo.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;AU_FECHA', sError + CR_LF + GetErrorDescription( E ) );
     Action := raCancel;
     //Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

function TdmAsistencia.GetStatusHorario(const sTurno: String; const dReferencia: TDate): TStatusHorario;
var
   Valor: Variant;
begin
     Valor := ServerAsistencia.GetHorarioStatus( dmCliente.Empresa, VarArrayOf( [ sTurno, dReferencia ] ) );
     with Result do
     begin
          Horario := Valor[ 0 ];
          Status := eStatusAusencia( Valor[ 1 ] );
     end;
end;

procedure TdmAsistencia.TarjetaNueva( Dataset: TDataset );
begin
     if FRegistrandoTarjetas then
     begin
          with Dataset do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := FEmpleadoRegTarjeta;
               FieldByName( 'AU_FECHA' ).AsDateTime := FFechaRegTarjeta;
          end;
     end
     else
         with dmCliente do
         begin
              with Dataset do
              begin
                   FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
                   FieldByName( 'AU_FECHA' ).AsDateTime := FechaAsistencia;
              end;
         end;
     with Dataset do
     begin
          FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmAsistencia.LlenaAusenciaNuevo( Dataset: TDataset );
var
   iEmpleado: TNumEmp;
   dValue: TDate;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FAgregandoTarjeta := True;
        with Dataset do
        begin
             iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
             dValue := FieldByName( 'AU_FECHA' ).AsDateTime;
             with dmRecursos.GetDatosClasificacion( iEmpleado, dValue ) do
             begin
                  FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
                  FieldByName( 'CB_TURNO' ).AsString := Turno;
                  FieldByName( 'CB_PUESTO' ).AsString := Puesto;
                  FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
                  FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
                  FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
                  FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
                  FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
                  FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
                  FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
                  FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
                  FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
{$ifdef ACS}
                  FieldByName( 'CB_NIVEL10' ).AsString := Nivel10;
                  FieldByName( 'CB_NIVEL11' ).AsString := Nivel11;
                  FieldByName( 'CB_NIVEL12' ).AsString := Nivel12;
{$endif}
{$ifdef QUINCENALES}
                  FieldByName( 'CB_SALARIO' ).AsFloat := Salario;
{$endif}
{.$ifdef CAMBIO_TNOM} 
                  FieldByName( 'CB_NOMINA' ).AsInteger:= Nomina;
{.$endif}

             end;

             with GetStatusHorario( FieldByName( 'CB_TURNO' ).AsString, dValue ) do
             begin
                  FieldByName( 'HO_CODIGO' ).AsString := Horario;
                  CambiaHorario := Horario;
                  FieldByName( 'AU_STATUS' ).AsInteger := Ord( Status );
             end;
        end;
     finally
            FAgregandoTarjeta := False;
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmAsistencia.HO_CODIGOChange(Sender: TField);
begin
     // if not FAgregandoTarjeta then
     if (not FAgregandoTarjeta) and (not FCambiaComboFestivo) then
     begin
          with Sender.Dataset do
          begin
               FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_SI;
          end;
     end;

     if FCambiaComboFestivo then
        FCambiaComboFestivo := FALSE;
end;


//#14519, al hacer lookup la tarjeta nueva cambia a manual
//se hizo un nuevo metodo, debido a funcionalidad especifica para esta ventana, ya que otros datamodules tambien estan usando ese procedimiento
procedure TdmAsistencia.cdsAsisTarjetaHO_CODIGOChange(Sender: TField);
begin
     // if not FAgregandoTarjeta then
     if (not FAgregandoTarjeta) and (not FCambiaComboFestivo) and ( CambiaHorario <> cdsAsisTarjeta.FieldByName( 'HO_CODIGO' ).AsString ) then
     begin
          with Sender.Dataset do
          begin
               CambiaHorario := cdsAsisTarjeta.FieldByName( 'HO_CODIGO' ).AsString;
               FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_SI;
          end;
     end;

     if FCambiaComboFestivo then
        FCambiaComboFestivo := FALSE;
end;


procedure TdmAsistencia.AU_FECHAChange(Sender: TField);
begin
     with Sender do
     begin
          if ( Dataset.State = dsInsert ) then
          begin
               LlenaAusenciaNuevo( Dataset );
          end;
     end;
end;

procedure TdmAsistencia.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmAsistencia.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmAsistencia.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enAusencia , Entidades ) or ( Estado = stEmpleado ) then
     {$else}
     if ( enAusencia in Entidades ) or ( Estado = stEmpleado ) then
     {$endif}
     begin
          cdsAsisCalendario.SetDataChange;
          if not FRegistrandoTarjetas then       // La Forma de Registro se encarga de Refrescar
             cdsAsisTarjeta.SetDataChange;
          cdsAutorizaciones.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enAusencia , Entidades ) or ( Estado = stFecha ) then
     {$else}
     if ( enAusencia in Entidades ) or ( Estado = stFecha ) then
     {$endif}
     begin
          if not FRegistrandoTarjetas then       // La Forma de Registro se encarga de Refrescar
             cdsAsisTarjeta.SetDataChange;
          cdsAutorizaciones.SetDataChange;
          cdsClasifiTemp.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enChecadas , Entidades ) then
     {$else}
     if ( enChecadas in Entidades ) then
     {$endif}
     begin
          cdsAsisTarjeta.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enTransferencias , Entidades ) then
     {$else}
     if ( enTransferencias in Entidades ) then
     {$endif}
     begin
          cdsCosteoTransferencias.SetDataChange;
          cdsAsisTarjeta.SetDataChange;
     end;

end;

{ ************ cdsAsisTarjeta ************ }

procedure TdmAsistencia.cdsAsisTarjetaAlAdquirirDatos(Sender: TObject);
begin

     CambiaHorario := VACIO;
     with cdsAsisTarjeta do
     begin
          if FGrabandoTarjeta then
          begin
               Data := FNewTarjeta;
               FChecadasBuffer := FNewChecadas;
          end
          else
              with dmCliente do
              begin
                   if FRegistrandoTarjetas then
                      Data := ServerAsistencia.GetTarjeta( Empresa, FEmpleadoRegTarjeta, FFechaRegTarjeta, FChecadasBuffer )
                   else
                      Data := ServerAsistencia.GetTarjeta( Empresa, Empleado, FechaAsistencia, FChecadasBuffer );
              end;
     end;
     with cdsChecadas do
     begin
          Data := FChecadasBuffer;
          FChecadasHayBuffer := False;
     end;
end;

procedure TdmAsistencia.cdsAsisTarjetaAlCrearCampos(Sender: TObject);
begin
     with cdsAsisTarjeta do
     begin
          with dmCatalogos do
          begin
               CreateSimpleLookup( cdsHorarios, 'HO_DESCRIP', 'HO_CODIGO' );
               CreateSimpleLookup( cdsTurnos, 'TU_DESCRIP', 'CB_TURNO' );
               CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'CB_PUESTO' );
               CreateSimpleLookup( cdsClasifi, 'CLASIFI', 'CB_CLASIFI' );
               CreateSimpleLookup( cdsTPeriodos, 'NO_DESCRIP','CB_NOMINA' );
          end;
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'IN_ELEMENT', 'AU_TIPO' );
          CreateCalculated( 'AU_DIA_SEM', ftString, 10 );
          CreateCalculated( 'AU_PERIODO', ftString, 20 );
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          ListaFija( 'AU_OUT2EAT', lfOut2Eat );
          ListaFija( 'AU_STA_FES', lfStatusFestivos);
          FieldByName( 'AU_FECHA' ).OnChange := AU_FECHAChange;
          FieldByName( 'HO_CODIGO' ).OnChange := cdsAsisTarjetaHO_CODIGOChange;
          FieldByName( 'AU_STATUS' ).OnChange := cdsAsisTarjetaHO_CODIGOChange;
     end;
end;

procedure TdmAsistencia.cdsAsisTarjetaAfterOpen(DataSet: TDataSet);
begin
     with cdsAsisTarjeta do
     begin
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmAsistencia.cdsAsisTarjetaCalcFields(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'AU_DIA_SEM' ).AsString := ZetaCommonTools.DiaSemana( FieldByName( 'AU_FECHA' ).AsDateTime );
          FieldByName( 'AU_PERIODO' ).AsString := ZetaCommonTools.ShowNomina( FieldByName( 'PE_YEAR' ).AsInteger,
                                                                              FieldByName( 'PE_TIPO' ).AsInteger,
                                                                              FieldByName( 'PE_NUMERO' ).AsInteger );
     end;
end;

procedure TdmAsistencia.cdsAsisTarjetaAfterInsert(DataSet: TDataSet);
begin
     with cdsChecadas do
     begin
          if Active then
          begin
               FChecadasBuffer := Data;
               FChecadasHayBuffer := True;
               EmptyDataset;
          end
          else
              FChecadasHayBuffer := False;
     end;
end;

procedure TdmAsistencia.cdsAsisTarjetaAfterCancel(DataSet: TDataSet);
begin
     with cdsChecadas do
     begin
          CancelUpdates;
          if FChecadasHayBuffer then
             Data := FChecadasBuffer;
     end;
end;

procedure TdmAsistencia.cdsAsisTarjetaNewRecord(DataSet: TDataSet);
begin
     TarjetaNueva( cdsAsisTarjeta );
end;

procedure TdmAsistencia.cdsAsisTarjetaAlAgregar(Sender: TObject);
begin
     RegistraTarjetas( eAgregarTarjeta );
end;

procedure TdmAsistencia.cdsAsisTarjetaAlModificar(Sender: TObject);
begin
     RegistraTarjetas( eModificarTarjeta );
{     with cdsChecadas do
     begin
          try
             Filter := 'CH_SISTEMA = ' + ZetaCommonTools.EntreComillas( K_GLOBAL_NO );
             Filtered := True;
             ZBaseEdicion.ShowFormaEdicion( EditTarjeta, TEditTarjeta );
          finally
                 Filtered := False;
                 Filter := '';
          end;
     end; }
end;

procedure TdmAsistencia.cdsAsisTarjetaAlEnviarDatos(Sender: TObject);
var
   iTarjeta, iChecadas, iErrorTarjeta, iErrorChecadas: Integer;
   oTarjeta, oChecadas, ChResults: OleVariant;
   //dValue: TDate;
begin
     with cdsChecadas do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          iChecadas := ChangeCount;
          if ( iChecadas = 0 ) then
             oChecadas := Null
          else
              oChecadas := Delta;
     end;
     with cdsAsisTarjeta do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          //dValue := FieldByName( 'AU_FECHA' ).AsDateTime;
          iTarjeta := ChangeCount;
          if ( iTarjeta = 0 ) then
          begin
               { Siempre debe haber un delta en este dataset }
               { para que la validaci�n de NO Modificar Tarjetas }
               { de un Per�odo Ya Afectado ( Global 132 ) }
               { tenga efecto ( la validaci�n se efect�a en un Trigger de Ausencia ) }
               Edit;
               with FieldByName( 'AU_HORASCK' ) do
               begin
                    AsFloat := AsFloat + 1;
               end;
               Post;
               iTarjeta := ChangeCount;
          end;
          oTarjeta := Delta;
     end;
     if ( iTarjeta > 0 ) or ( iChecadas > 0 ) then
     begin
          iErrorTarjeta := iTarjeta;
          iErrorChecadas := iChecadas;
          oTarjeta := ServerAsistencia.GrabaTarjeta( dmCliente.Empresa, oTarjeta, oChecadas, iErrorTarjeta, iErrorChecadas, ChResults, FNewTarjeta, FNewChecadas );
          FGrabandoTarjeta:= TRUE;        // Esta Bandera forza la lectura de FNewTarjeta al Refrescar
          if ( ( iTarjeta = 0 ) or cdsAsisTarjeta.Reconcile( oTarjeta ) ) and
             ( ( iChecadas = 0 ) or cdsChecadas.Reconcile( ChResults ) ) then
          begin
               with TressShell do
               begin
                    //SetAsistencia( dValue );   ... No mover� el Activo de FechaAsistencia
                    SetDataChange( [ enAusencia, enChecadas ] );
               end;
               try
                  cdsAsisTarjeta.Refrescar;
                  //FChecadasHayBuffer := False;  .. Se invoca al Refrescar
               finally
                  FGrabandoTarjeta:= FALSE;
                  CambiaHorario := VACIO;
               end;
          end
          else
          begin
               with cdsAsisTarjeta do
               begin
                    if ( ChangeCount > 0 ) or ( cdsChecadas.ChangeCount > 0 ) then
                    begin
                         Edit;
                    end;
               end;
          end;
     end;
end;

procedure TdmAsistencia.cdsAsisTarjetaAlBorrar(Sender: TObject);
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Esta Tarjeta ?' ) then
     begin
          with cdsAsisTarjeta do
          begin
               Delete;
               Enviar;
          end;
     end;
end;

{ *************** cdsChecadas **************** }
procedure TdmAsistencia.DescansoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if ( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) in [ chDesconocido, chEntrada, chSalida, chInicio, chFin ] ) then
                Text := FormatFloat( '#0.00;;#', Sender.AsFloat )
          else
                Text := VACIO;

     end;
end;

procedure TdmAsistencia.AprobadasGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if ( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) in [ chConGoce, chSinGoce, chExtras, chDescanso, chConGoceEntrada, chSinGoceEntrada ] ) then
                  Text := FormatFloat( '#0.00;;#', Sender.AsFloat )
          else
                Text := VACIO;
     end;
end;

procedure TdmAsistencia.cdsChecadasAlCrearCampos(Sender: TObject);
begin
     with cdsChecadas do
     begin
          ListaFija( 'CH_TIPO', lfTipoChecadas );
          ListaFija( 'CH_DESCRIP', lfDescripcionChecadas );
          MaskTime( 'CH_H_REAL' );
          //MaskTime( 'CH_H_AJUS' );
          MaskHoras( 'CH_HOR_ORD' );
          MaskHoras( 'CH_HOR_EXT' );
          MaskHoras( 'CH_HOR_DES' );
          MaskHoras( 'CH_APRUEBA' );          
          with FieldByName( 'CH_H_REAL' ) do
          begin
               OnChange := CH_H_REALChange;
               OnSetText := CH_H_REALSetText;
               OnValidate := CH_H_REALValidate;
          end;
          FieldByName( 'CH_H_AJUS' ).OnGetText := CH_H_AJUSGetText;
          FieldByName( 'CH_H_AJUS' ).Alignment := taRightJustify;
          FieldByName( 'CH_HOR_DES' ).OnGetText := DescansoGetText;
          FieldByName( 'CH_APRUEBA' ).OnGetText := AprobadasGetText;
          FieldByName( 'CH_RELOJ' ).OnGetText := CH_RELOJGetText; {ACL150409}
          with FieldByName( 'CH_MOTIVO' ) do
          begin
               OnGetText := CH_MOTIVOGetText; {ACL150409}
               OnValidate := cdsChecadaManualValidate;{ACL160409}
          end;
     end;
end;

{ACL150409}
procedure TdmAsistencia.CH_RELOJGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if ( PermitirCapturaMotivoCH ) AND ( EsTipoChecadaAutorizacion( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) ) ) then
             Text := VACIO
          else
                Text := Sender.AsString;
     end;
end;

{ACL150409}
procedure TdmAsistencia.CH_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if ( PermitirCapturaMotivoCH ) AND ( EsTipoChecadaAutorizacion( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) ) ) then
             Text := FieldByName( 'CH_RELOJ' ).AsString
          else
              Text := Sender.AsString;
     end;
end;                          

{ACL170409}

procedure TdmAsistencia.cdsChecadasAlModificar(Sender: TObject);
begin
//**No se utiliza, la edicion del dataset se hace sobre el grid
{
     if ( eTipoChecadas( cdsChecadas.FieldByName( 'CH_TIPO' ).AsInteger ) in [ chConGoce, chSinGoce, chExtras, chDescanso, chConGoceEntrada, chSinGoceEntrada ] ) then
        ZBaseEdicion.ShowFormaEdicion( EditAutorizacionesTarjeta, TEditAutorizacionesTarjeta );
     }
end;

procedure TdmAsistencia.CH_H_REALChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          with FieldByName('CH_MOTIVO') do
          begin
               if StrVacio(AsString) then
                 AsString:= FUltimoMotivoCH;
          end;
          FieldByName( 'CH_H_AJUS' ).AsString := FieldByName( 'CH_H_REAL' ).AsString;
    end;
end;

procedure TdmAsistencia.CHECADAChange(Sender: TField);
begin
     with cdsGridTarjetasPeriodo do
     begin
          FieldByName( 'US_' + Sender.FieldName ).AsInteger := dmCliente.Usuario;
          with FieldByName('MC_'+ Sender.FieldName) do
          begin
               if StrVacio(AsString) then
                 AsString:= FUltimoMotivoCH;
          end;

     end;
end;

procedure TdmAsistencia.CH_H_REALSetText(Sender: TField; const Text: String);
begin
     Sender.AsString := ConvierteHora( Text );
end;

procedure TdmAsistencia.CH_H_REALValidate(Sender: TField);
var
   sHora: String;
begin
     if ( eTipoChecadas( cdsChecadas.FieldByName( 'CH_TIPO' ).AsInteger ) in [ chConGoce, chSinGoce, chExtras, chDescanso, chConGoceEntrada, chSinGoceEntrada ] ) then
        raise ERangeError.Create( 'No Se Pueden Modificar Las Autorizaciones' );
     sHora := Sender.AsString;
     if not ZetaCommonTools.ValidaHora( sHora, '48' ) then
     begin
          raise ERangeError.Create( 'Hora "' +
                                    Copy( sHora, 1, 2 ) +
                                    ':' +
                                    Copy( sHora, 3, 2 ) +
                                    '" Inv�lida' );
     end;
end;

procedure TdmAsistencia.CH_H_AJUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   iHoras: Integer;
begin
     Text := Sender.AsString;
     if ( Length( Text ) > 0 ) and ( Text <> K_HORA_NULA ) then
     begin
          iHoras := StrToInt( Text );
          if ( iHoras >= 2400 ) then
             iHoras := iHoras - 2400;
          Text := Format( '%4.4d', [ iHoras ] );
          Text := Copy( Text, 1, 2 ) + ':' + Copy( Text, 3, 2 );
     end;
end;

procedure TdmAsistencia.cdsChecadasNewRecord(DataSet: TDataSet);
begin
     with cdsChecadas do
     begin
          FieldByName( 'CB_CODIGO' ).AsInteger := cdsAsisTarjeta.FieldByName( 'CB_CODIGO' ).AsInteger;
          FieldByName( 'AU_FECHA' ).AsDateTime := cdsAsisTarjeta.FieldByName( 'AU_FECHA' ).AsDateTime;
          FieldByName( 'CH_SISTEMA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CH_GLOBAL' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmAsistencia.cdsChecadasAfterInsert(DataSet: TDataSet);
begin
     with cdsAsisTarjeta do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmAsistencia.cdsChecadasAfterDelete(DataSet: TDataSet);
begin
     with cdsAsisTarjeta do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

{ *************** cdsAsisCalendario ********** }
procedure TdmAsistencia.cdsAsisCalendarioAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsAsisCalendario.Data := ServerAsistencia.GetCalendario( Empresa, Empleado, FCalendarioInicio, FCalendarioFin );
     end;
end;

procedure TdmAsistencia.cdsAsisCalendarioAlCrearCampos(Sender: TObject);
begin
     with cdsAsisCalendario do
     begin
          MaskFecha( 'AU_FECHA' );
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          MaskHoras( 'AU_HORASCK');
          MaskHoras( 'AU_HORAS');
          MaskHoras( 'AU_EXTRAS');
          MaskHoras( 'AU_PER_CG');
          MaskHoras( 'AU_PER_SG');
          MaskHoras( 'AU_DES_TRA');
          MaskHoras( 'AU_TARDES');
          FieldByName( 'AU_FECHA' ).OnChange := AU_FECHAChange;
          FieldByName( 'HO_CODIGO' ).OnChange := HO_CODIGOChange;
          FieldByName( 'AU_STATUS' ).OnChange := HO_CODIGOChange;
          CreateCalculated( 'AU_DIA_SEM', ftString, 10 );
     end;
end;

procedure TdmAsistencia.cdsAsisCalendarioAfterOpen(DataSet: TDataSet);
begin
     dmRecursos.cdsDatosEmpleado.Conectar;
     with cdsAsisCalendario do
     begin
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmAsistencia.cdsAsisCalendarioCalcFields(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'AU_DIA_SEM' ).AsString := ZetaCommonTools.DiaSemana( FieldByName( 'AU_FECHA' ).AsDateTime );
     end;
end;

procedure TdmAsistencia.SetCalendarioInicio;
begin
     with dmCliente do
     begin
          FCalendarioInicio := DiaDelMes( FechaAsistencia, True );
          FCalendarioFin := FechaAsistencia;
     end;
end;

procedure TdmAsistencia.cdsAsisCalendarioAfterDelete(DataSet: TDataSet);
begin
     EnviarCalendario;
end;

procedure TdmAsistencia.cdsAsisCalendarioAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCalendario_DevEx, TEditCalendario_DevEx );
end;

procedure TdmAsistencia.cdsAsisCalendarioAlEnviarDatos(Sender: TObject);
var
   dFecha: TDate;
begin
     with cdsAsisCalendario do
     begin
          dFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
          if EnviarCalendario then
          begin
               Refrescar;
               Locate( 'AU_FECHA', dFecha, [] );
          end;
     end;
end;

function TdmAsistencia.EnviarCalendario: Boolean;
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     Result := False;
     with cdsAsisCalendario do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerAsistencia.GrabaCalendario( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enAusencia ] );
                    Result := True;
               end;
          end;
     end;
end;

procedure TdmAsistencia.cdsAsisCalendarioNewRecord(DataSet: TDataSet);
begin
     TarjetaNueva( cdsAsisCalendario );
     with cdsAsisCalendario do
     begin
          FieldByName( 'AU_TIPODIA' ).AsInteger := Ord( auHabil );
          FieldByName( 'AU_HORASCK' ).AsFloat := 0.0;
          FieldByName( 'AU_HORAS' ).AsFloat := 0.0;
          FieldByName( 'AU_PER_CG' ).AsFloat := 0.0;
          FieldByName( 'AU_PER_SG' ).AsFloat := 0.0;
          FieldByName( 'AU_TRIPLES' ).AsFloat := 0.0;
          FieldByName( 'AU_EXTRAS' ).AsFloat := 0.0;
          FieldByName( 'AU_DES_TRA' ).AsFloat := 0.0;
          FieldByName( 'AU_TARDES' ).AsFloat := 0.0;
     end;
end;

procedure TdmAsistencia.AplicaFiltros( sFiltros: String);
begin
     with cdsAsisCalendario do
     begin
          Filter := sFiltros;
          Filtered := True;
     end;
end;

{ *************** cdsAutorizaciones ***************** }

procedure TdmAsistencia.cdsAutorizacionesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsAutorizaciones.Data := ServerAsistencia.GetAutorizaciones( Empresa, FechaAsistencia, FSoloAltas );
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesAlCrearCampos(Sender: TObject);
begin
     with cdsAutorizaciones do
     begin
          MaskHoras( 'HORAS' );
          MaskHoras( 'CH_HOR_DES' );
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsAutorizacionesCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsAutorizacionesCB_CODIGOChange;
          FieldByName( 'CH_RELOJ' ).OnValidate := cdsAutorizacionesCH_RELOJValidate;
          FieldByName( 'CH_RELOJ' ).OnChange := cdsAutorizacionesCH_RELOJChange;
          FieldByName( 'CH_HOR_DES' ).OnChange := cdsAutorizacionesCH_HOR_DESChange;
          FieldByName( 'AU_FECHA' ).OnChange := cdsAutorizacionesCH_TIPOChange;
          FieldByName( 'HORAS' ).OnChange := cdsAutorizacionesCH_TIPOChange;
          with FieldByName( 'CH_TIPO' ) do
          begin
               OnGetText := CH_TIPOGetText;
               Alignment := taLeftJustify;
               OnChange := cdsAutorizacionesCH_TIPOChange;
          end;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DES_OK', 'US_COD_OK' );
     end;
end;

function  TdmAsistencia.GetDescTipo( const nTipo : Integer ) : String;
begin
     if ( nTipo >= K_OFFSET_AUTORIZACION ) then
        Result := ObtieneElemento( lfAutorizaChecadas, nTipo - K_OFFSET_AUTORIZACION )
     else
        Result := VACIO;
end;


procedure TdmAsistencia.CH_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
    Text := GetDescTipo( Sender.AsInteger );
end;

procedure TdmAsistencia.cdsAutorizacionesBeforeOpen(DataSet: TDataSet);
begin
     FUltimaFecha := dmCliente.FechaAsistencia;
     FUltimoTipo := chExtras;
     FUltimaHora := 1.00;
     FUltimoMotivo := '';
end;

procedure TdmAsistencia.cdsAutorizacionesAlBorrar(Sender: TObject);
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Esta Autorizaci�n ?' ) then
     begin
          with cdsAutorizaciones do
          begin
               Delete;
               Enviar;
          end;
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditAutorizaciones_DevEx, TEditAutorizaciones_DevEx );
end;

procedure TdmAsistencia.cdsAutorizacionesNewRecord(DataSet: TDataSet);
begin
     with cdsAutorizaciones do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'CH_SISTEMA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CH_GLOBAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CH_IGNORAR' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CH_TIPO' ).AsInteger := 0;
          FieldByName( 'CH_H_REAL' ).AsString := K_HORA_NULA;
          FieldByName( 'CH_H_AJUS' ).AsString := K_HORA_NULA;
          FieldByName( 'HORAS' ).AsFloat := 0.0;
          //Cada vez que se agregue un registro se inicializa el campo con Cero
          FieldByName( 'US_COD_OK' ).AsInteger := 0;
     end;
end;

//MV: Se comento este Metodo por que cada uno de los campos del ClientDataSet
//Tienen un Evento OnChange
{
procedure TdmAsistencia.cdsAutorizacionesAfterEdit(DataSet: TDataSet);
begin
     with cdsAutorizaciones do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;
}

procedure TdmAsistencia.cdsAutorizacionesBeforePost(DataSet: TDataSet);
begin
     with cdsAutorizaciones do
     begin
          if dmCliente.PuedeCambiarTarjetaDlg( cdsAutorizaciones ) then
          begin
               with FieldByName( 'CB_CODIGO' ) do
               begin
                    if ( AsInteger = 0 ) then
                    begin
                         FocusControl;
                         DataBaseError( 'N�mero de Empleado no Puede Quedar Vac�o' );
                    end;
               end;
               with FieldByName( 'AU_FECHA' ) do
               begin
                    if ( AsDateTime = NullDateTime ) then
                    begin
                         FocusControl;
                         DataBaseError( 'Fecha de Autorizaci�n no Puede quedar Vacia' );
                    end;
               end;
               with FieldByName( 'HORAS' ) do
               begin
                    if ( AsFloat <= 0 ) then
                    begin
                         FocusControl;
                         DataBaseError( 'Horas de Autorizaci�n Deben Ser Mayor a Cero' );
                    end;
               end;
               with FieldByName( 'CH_TIPO' ) do
               begin
                    if ( AsInteger = 0 ) then
                    begin
                         FocusControl;
                         DataBaseError( 'Tipo de Autorizaci�n no Puede Quedar Vac�o' );
                    end;
               end;
               FToolsAsistencia.ValidaAutoBeforePost( eAutorizaChecadas( FieldByName( 'CH_TIPO' ).AsInteger - K_AUTORIZACIONES_OFFSET ), FieldByName( 'HORAS' ).AsFloat, FieldByName( 'CH_RELOJ' ) );
               {
               Se modifica el campo que es bandera para No Perder las Autorizaciones Aprobadas
               Se Cambia el Valor a UNO para que el Filtro del Grid de Autorizaciones lo tome cuando
               se Modifique el US_COD_OK
               }
               FieldByName( 'CAMBIO' ).AsInteger := 1;
          end
          else
          begin
               Abort;
          end;
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   ErrorVar, ErrorData: OleVariant;
begin
     ErrorCount := 0;
     with cdsAutorizaciones do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Repeat
                     ErrorVar := ServerAsistencia.GrabaAutorizaciones( dmCliente.Empresa, Delta, ErrorCount, ErrorData, FOperacion );
                     cdsErrores.Data := ErrorData;
               Until ( Reconciliar( ErrorVar ) );
               TressShell.SetDataChange( [ enAusencia, enChecadas ] );
          end;
     end;
end;

{$ifdef VER130}
procedure TdmAsistencia.cdsAutorizacionesReconcileError(DataSet: TClientDataSet;  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
   iEmpleado, iTipo, iAprobadasPor: Integer;
   dFecha: TDate;
   rHoras, rHorasAprobadas: TDiasHoras;

   procedure SetDatosExistentes;
   begin
        with cdsErrores do
        begin
             if Locate( 'CB_CODIGO;AU_FECHA;CH_TIPO',VarArrayOf([ iEmpleado,
                      //  FechaAsStr( dFecha ), iTipo]), [] ) then
                        dFecha, iTipo]), [] ) then
             begin
                  rHoras := FieldByName( 'HORAS' ).AsFloat;
                  rHorasAprobadas := FieldByName( 'CH_HOR_DES' ).AsFloat;
                  iAprobadasPor := FieldByName( 'US_COD_OK' ).AsInteger;
             end;
        end;
   end;

begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          with DataSet do
          begin
               iEmpleado:= ValorCampo( FieldByName( 'CB_CODIGO' ) );
               dFecha:= ValorCampo( FieldByName( 'AU_FECHA' ) );
               iTipo:= ValorCampo( FieldByName( 'CH_TIPO' ) );
               SetDatosExistentes;
               Opera := ShowDlgAutorizaRepetida( iEmpleado , dFecha, GetDescTipo( iTipo ),
                                                 rHoras, ValorCampo( FieldByName( 'HORAS' ) ),
                                                 rHorasAprobadas, iAprobadasPor );
               if Opera <> ocIgnorar then
               begin
                    Edit;
                    FieldByName( 'OPERACION' ).NewValue := Ord( Opera );
                    if Opera = ocSumar then
                       FieldByName( 'HORAS' ).NewValue := ValorCampo( FieldByName( 'HORAS' ) ) + rHoras;
                    Post;
                    FCambioAutorizacion:= TRUE;
                    Action := raCorrect;
               end;
{               sError := Format( 'La Autorizaci�n Ya Fu� Capturada' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s' + CR_LF + 'Tipo: %s',
                         [ FieldByName( 'CB_CODIGO' ).AsInteger,
                           FechaCorta( FieldByName( 'AU_FECHA' ).AsDateTime ),
                           GetDescTipo( FieldByName( 'CH_TIPO' ).AsInteger ) ] ); }
          end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
         //sError := GetErrorDescription( E );
         //Action := cdsAutorizaciones.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;AU_FECHA;CH_TIPO', sError );
end;
{$else}
procedure TdmAsistencia.cdsAutorizacionesReconcileError(DataSet: TCustomClientDataSet;  E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
   iEmpleado, iTipo, iAprobadasPor: Integer;
   dFecha: TDate;
   rHoras, rHorasAprobadas: TDiasHoras;

   procedure SetDatosExistentes;
   begin
        with cdsErrores do
        begin
             if Locate( 'CB_CODIGO;AU_FECHA;CH_TIPO',VarArrayOf([ iEmpleado,
                     //   FechaAsStr( dFecha ), iTipo]), [] ) then
                        dFecha, iTipo]), [] ) then
             begin
                  rHoras := FieldByName( 'HORAS' ).AsFloat;
                  rHorasAprobadas := FieldByName( 'CH_HOR_DES' ).AsFloat;
                  iAprobadasPor := FieldByName( 'US_COD_OK' ).AsInteger;
             end;
        end;
   end;

begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          with DataSet do
          begin
               iEmpleado:= ValorCampo( FieldByName( 'CB_CODIGO' ) );
               dFecha:= ValorCampo( FieldByName( 'AU_FECHA' ) );
               iTipo:= ValorCampo( FieldByName( 'CH_TIPO' ) );
               SetDatosExistentes;

               // No mostrar di�logo de Autorizaci�n repetida si estamos en la interfaz Callaway - Kronos
               {$IFNDEF INTERFAZ_ELECTROLUX_KRONOS}
                      {$IFNDEF INTERFAZ_MJD_KRONOS}
                      Opera := ShowDlgAutorizaRepetida( iEmpleado , dFecha, GetDescTipo( iTipo ),
                                                        rHoras, ValorCampo( FieldByName( 'HORAS' ) ),
                                                        rHorasAprobadas, iAprobadasPor );
                      {$ELSE}
                      Opera := ocSustituir;
                      dmInterfase.EscribeErrorExterno( 'Ya existe autorizaci�n. Ser� sustituida por la enviada en archivo.' );
                      {$ENDIF}
               {$ELSE}
                      Opera := ocSustituir;
                      dmInterfase.EscribeErrorExterno( 'Ya existe autorizaci�n. Ser� sustituida por la enviada en archivo.' );
               {$ENDIF}
               if Opera <> ocIgnorar then
               begin
                    Edit;
                    FieldByName( 'OPERACION' ).NewValue := Ord( Opera );
                    if Opera = ocSumar then
                       FieldByName( 'HORAS' ).NewValue := ValorCampo( FieldByName( 'HORAS' ) ) + rHoras;
                    Post;
                    FCambioAutorizacion:= TRUE;
                    Action := raCorrect;
               end;
{               sError := Format( 'La Autorizaci�n Ya Fu� Capturada' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s' + CR_LF + 'Tipo: %s',
                         [ FieldByName( 'CB_CODIGO' ).AsInteger,
                           FechaCorta( FieldByName( 'AU_FECHA' ).AsDateTime ),
                           GetDescTipo( FieldByName( 'CH_TIPO' ).AsInteger ) ] ); }
          end;
     end
     else
         if PrenomAutorizada(E)then
         begin
               with DataSet do
               begin
                    iEmpleado:= ValorCampo( FieldByName( 'CB_CODIGO' ) );
                    dFecha:= ValorCampo( FieldByName( 'AU_FECHA' ) );

                    MessageDlg( Format( GetErrorDescription( E )+CR_LF+'Empleado: %s'+CR_LF+'Fecha: %s',[IntToStr(iEmpleado),DateToStr(dFecha)] ), mtError, [ mbOK ], 0 );
               end;
         end
         else
             MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
         //sError := GetErrorDescription( E );
         //Action := cdsAutorizaciones.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;AU_FECHA;CH_TIPO', sError );
end;
{$endif}

procedure TdmAsistencia.cdsAutorizacionesAfterPost(DataSet: TDataSet);
begin
     with cdsAutorizaciones do
     begin
          FUltimaFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
          FUltimoTipo := eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger );
          FUltimaHora := FieldByName( 'HORAS' ).AsFloat;
          FUltimoMotivo := FieldByName( 'CH_RELOJ' ).AsString;
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesCB_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No Existe el Empleado #' + IntToStr( AsInteger ) );
          end
          else
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesCB_CODIGOChange(Sender: TField);
begin
     with cdsAutorizaciones do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          { Asigna Valores Default }
          if ( FieldByName( 'AU_FECHA' ).AsDateTime = 0 ) then
          begin
               FieldByName( 'AU_FECHA' ).AsDateTime := FUltimaFecha;
               FieldByName( 'CH_TIPO' ).AsInteger := Ord( FUltimoTipo );
               FieldByName( 'HORAS' ).AsFloat := FUltimaHora;
               FieldByName( 'CH_RELOJ' ).AsString := FUltimoMotivo;
          end;
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesCH_RELOJValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( NOT ValidarMotivoAutorizacion( cdsAutorizaciones.FieldByName('CH_RELOJ').AsString, cdsAutorizaciones.FieldByName('CH_TIPO').AsInteger ) )then
          begin
               DataBaseError( 'El Motivo de Autorizaci�n es Inv�lido: ' + AsString );
          end;
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesCH_RELOJChange(Sender: TField);
begin
     with cdsAutorizaciones do
     begin
          if ( Sender.AsString <> VACIO ) then
             FieldByName( 'MOTIVOAUTO' ).AsString:= dmTablas.cdsMotAuto.FieldByName( 'TB_ELEMENT' ).AsString
          else
              FieldByName( 'MOTIVOAUTO' ).AsString:= VACIO;
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesCH_TIPOChange(Sender: TField);
begin
     with cdsAutorizaciones do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          if not ValidarMotivoAutorizacion( FieldByName('CH_RELOJ').AsString,FieldByName('CH_TIPO').AsInteger )then
             FieldByName('CH_RELOJ').AsString := VACIO;
     end;
end;

function TdmAsistencia.ValidarMotivoAutorizacion(const sCodigo:string ; const iTipo:Integer ):Boolean;
begin
     Result := StrVacio(sCodigo);
     if not Result then
     begin
          with dmTablas.cdsMotAuto do
          begin
               Result := ( Locate('TB_CODIGO',UpperCase(sCodigo) ,[]) ) and
                         ( ( FieldByName('TB_TIPO').AsInteger = K_MOTIVO_AUTORIZ_OFFSET ) or
                           ( FieldByName('TB_TIPO').AsInteger = iTipo - K_AUTORIZACIONES_OFFSET )
                         );
          end;
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesCH_HOR_DESChange(Sender: TField);
begin
     cdsAutorizaciones.FieldByName( 'US_COD_OK' ).AsInteger:= dmCliente.Usuario;
end;

procedure TdmAsistencia.EditarGridAutorizaciones( const lSoloAltas: Boolean );
const
     K_FILTRO_SIN_AUTORIZACION = '( US_COD_OK = 0 ) or ( CAMBIO = 1 )';
begin
     SoloAltas := lSoloAltas;
     try
        dmCliente.SetLookupEmpleado;
        with cdsAutorizaciones do
        begin
             if ( AprobarPrendido ) and
                ( not TieneDerechoAprobar ) and
                ( not lSoloAltas ) then
             begin
                  Filter := K_FILTRO_SIN_AUTORIZACION;
                  Filtered := True;
             end;
             try
                ZBaseGridEdicion_DevEx.ShowGridEdicion( GridAutorizaciones_DevEx, TGridAutorizaciones_DevEx, lSoloAltas );
             finally
                    Filtered := False;
                    Filter := VACIO;
             end;
        end;
     finally
            dmCliente.ResetLookupEmpleado;
     end;
     SoloAltas := False;
     { Para que muestre los registros que se agregaron }
//     if ( lSoloAltas ) then
     cdsAutorizaciones.Refrescar;
end;

{ ************** cdsHorarioTemp ****************** }

procedure TdmAsistencia.cdsHorarioTempAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsHorarioTemp.Data := ServerAsistencia.GetHorarioTemp( Empresa, Empleado, FHorarioInicio, FHorarioFin );
     end;
end;

procedure TdmAsistencia.cdsHorarioTempAlCrearCampos(Sender: TObject);
begin
     with cdsHorarioTemp do
     begin
          MaskHoras( 'AU_HORASCK' );
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          FieldByName( 'HO_CODIGO' ).OnValidate := cdsHorarioTempHO_CODIGOValidate;
          FieldByName( 'HO_CODIGO' ).OnChange := cdsHorarioTempHO_CODIGOChange;
          FieldByName( 'AU_STATUS' ).OnChange := HO_CODIGOChange;
     end;
end;

procedure TdmAsistencia.cdsHorarioTempBeforePost(DataSet: TDataSet);
begin
     with cdsHorarioTemp do
     begin
          if dmCliente.PuedeCambiarTarjetaDlg( cdsHorarioTemp ) then
          begin
               with FieldByName( 'HO_CODIGO' ) do
               begin
                    if ( AsString = VACIO ) then
                    begin
                         FocusControl;
                         DataBaseError( 'C�digo de Horario no Puede Quedar Vac�o' );
                    end;
               end;
          end
          else
          begin
               //Cancel;
               Abort;
          end;
     end;
end;

procedure TdmAsistencia.cdsHorarioTempAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsHorarioTemp do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconciliar( ServerAsistencia.GrabaHorarioTemp( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enAusencia ] );
               end;
          end;
     end;
end;
{$ifdef VER130}
procedure TdmAsistencia.cdsHorarioTempReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
    if PK_Violation( E ) then
    begin
         with DataSet do
         begin
              sError := Format( 'La Tarjeta Ya Exist�a' +
                                CR_LF +
                                'Fecha: %s',
                                [ ZetaCommonTools.FechaCorta( FieldByName( 'AU_FECHA' ).AsDateTime ) ] )
         end;
    end
    else
        sError := GetErrorDescription( E );
    Action := cdsHorarioTemp.ReconciliaError( DataSet, UpdateKind, E, 'AU_FECHA', sError );
end;
{$else}
procedure TdmAsistencia.cdsHorarioTempReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
    if PK_Violation( E ) then
    begin
         with DataSet do
         begin
              sError := Format( 'La Tarjeta Ya Exist�a' +
                                CR_LF +
                                'Fecha: %s',
                                [ ZetaCommonTools.FechaCorta( FieldByName( 'AU_FECHA' ).AsDateTime ) ] )
         end;
    end
    else
        sError := GetErrorDescription( E );
    Action := cdsHorarioTemp.ReconciliaError( DataSet, UpdateKind, E, 'AU_FECHA', sError );
end;
{$endif}

procedure TdmAsistencia.cdsHorarioTempHO_CODIGOValidate(Sender: TField);
begin
{AV CR 1837  Validacion de Catalogos 2010-11-17}
     ValidaLookup( Sender.AsString,VACIO, dmCatalogos.cdsHorarios );     
end;

procedure TdmAsistencia.cdsHorarioTempHO_CODIGOChange(Sender: TField);
begin
     with cdsHorarioTemp do
     begin
          FieldByName( 'HO_DESCRIP' ).AsString := dmCatalogos.cdsHorarios.FieldByName( 'HO_DESCRIP' ).AsString;
     end;
     HO_CODIGOChange( Sender );
end;

procedure TdmAsistencia.EditarGridHorarioTemp;
begin
     if FHorarioTemp_DevEx.GetFechasHorarioTemporal( FHorarioInicio, FHorarioFin ) then
     begin
          ZBaseGridEdicion_DevEx.ShowGridEdicion( GridHorarioTemp_DevEx, TGridHorarioTemp_DevEx, False );
     end;
end;

{ Registro de Tarjeta }

procedure TdmAsistencia.RegistraTarjetas( const TipoRegistro: TTipoRegTarjeta );
begin
     with cdsChecadas do
     begin
          try
             FRegistrandoTarjetas:= TRUE;
             Filter := 'CH_SISTEMA = ' + ZetaCommonTools.EntreComillas( K_GLOBAL_NO );
             Filtered := True;

             if ( RegTarjeta_DevEx = nil ) then
                RegTarjeta_DevEx := TRegTarjeta_DevEx.Create( Application );
             RegTarjeta_DevEx.TipoRegTarjeta := TipoRegistro;
             RegTarjeta_DevEx.ShowModal;
             // Verifica Si cambiaron Activos de Empleado y Asistencia
             with dmCliente do
             begin
                  if ( FEmpleadoRegTarjeta <> Empleado ) or ( FFechaRegTarjeta <> FechaAsistencia ) then
                  begin
                      if ( FEmpleadoRegTarjeta <> Empleado ) and ( SetEmpleadoNumero( FEmpleadoRegTarjeta ) ) then
                          TressShell.CambiaEmpleadoActivos;              // Como Est� Registrando Tarjetas
                      TressShell.SetAsistencia( FFechaRegTarjeta );     // no se Refrescar� cdsAsisTarjeta
                  end;
             end;
          finally
             Filtered := False;
             Filter := '';
             FRegistrandoTarjetas:= FALSE;
          end;
     end;
end;

function TdmAsistencia.GetVacaDiasGozar( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := ServerAsistencia.GetVacaDiasGozar( dmCliente.Empresa, iEmpleado, dInicio, dFinal );
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmAsistencia.GetVacaFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const rGozo: TPesos; var rDiasRango: Double ): TDate;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := ServerAsistencia.GetVacaFechaRegreso( dmCliente.Empresa, iEmpleado, dInicio, rGozo, rDiasRango );
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmAsistencia.TieneDerechoAprobar: boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_ASIS_DATOS_AUTORIZACIONES, K_DERECHO_SIST_KARDEX );
end;

function TdmAsistencia.AprobarPrendido: boolean;
begin
     Result := Global.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES );
end;

procedure TdmAsistencia.PuedeCambiarTarjetaBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if NOT dmCliente.PuedeCambiarTarjetaDlg( Dataset ) then
          begin
               {CV: Si quiero cancelar el registro porque la Fecha NO se puede cambiar.
               Y no deja salir al usuario del registro hasta que lo cancele}
               Cancel;
               Abort;
          end;
     end;
end;

procedure TdmAsistencia.cdsAutorizacionesBeforeEdit(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if NOT dmCliente.PuedeCambiarTarjetaDlg( Dataset ) then
          begin
               Abort;
          end;
     end;
end;

procedure TdmAsistencia.EditarGridAProbarAutorizaciones;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridAutorizacionesXAprobar_DevEx, TGridAutorizacionesXAprobar_DevEx, FALSE );
end;

procedure TdmAsistencia.cdsAutoXAprobarAlAdquirirDatos(Sender: TObject);
begin
     cdsAutoXAprobar.Data := ServerAsistencia.GetAutoXAprobar( dmCliente.Empresa, FFechaGridAuto, TRUE );
end;

procedure TdmAsistencia.cdsAutoXAprobarAlCrearCampos(Sender: TObject);
begin
     with cdsAutoXAprobar do
     begin
          MaskHoras( 'CH_HORAS' );
          MaskHoras( 'CH_HOR_DES' );
          with FieldByName( 'CH_TIPO' ) do
          begin
               OnGetText := CH_TIPOGetText;
               Alignment := taLeftJustify;
          end;
          CreateSimpleLookup( dmTablas.cdsSupervisores, 'TB_ELEMENT', 'CB_NIVEL' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DES_OK', 'US_COD_OK' );
     end;
end;

procedure TdmAsistencia.cdsAutoXAprobarAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsAutoXAprobar do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile ( ServerAsistencia.GrabaAutoXAprobar( dmCliente.Empresa, Delta, ErrorCount ) );
               TressShell.SetDataChange( [ enAusencia, enChecadas ] );
          end;
     end;
end;

procedure TdmAsistencia.cdsAutoXAprobarBeforePost(DataSet: TDataSet);
begin
     with cdsAutoXAprobar do
     begin
          if dmCliente.PuedeCambiarTarjetaDlg( cdsAutoXAprobar ) then
          begin
               if ( FieldByName( 'CH_HOR_DES' ).AsFloat <> 0 ) then
                  FieldByName( 'US_COD_OK' ).AsInteger := dmCliente.Usuario
               else
                   FieldByName( 'US_COD_OK' ).AsInteger := 0;
          end
          else
          begin
               Cancel;
               Abort;
          end;
     end;
end;

procedure TdmAsistencia.cdsClasifiTempAfterDelete(DataSet: TDataSet);
begin
     TZetaClientDataSet( DataSet ).Enviar;
end;

procedure TdmAsistencia.cdsClasifiTempAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsClasifiTemp.Data := ServerAsistencia.GetClasificacionTemp( Empresa, dmCliente.FechaAsistencia,
                                 VACIO, FALSE );
     end;
end;

procedure TdmAsistencia.cdsClasifiTempAlCrearCampos(Sender: TObject);
begin
     with cdsClasifiTemp do
     begin
          FieldByName( 'CB_CODIGO' ).OnChange := cdsClasifiTempCB_CODIGOChange;
          MaskFecha( 'AU_FECHA' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DES_OK', 'US_COD_OK' );
     end;
end;

procedure TdmAsistencia.cdsClasifiTempAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsClasifiTemp do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerAsistencia.GrabaClasificacionTemp( dmCliente.Empresa, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
                  TressShell.SetDataChange( [ enClasifiTemp ] );
          end;
     end;
end;

procedure TdmAsistencia.cdsClasifiTempAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditClasifTemporal_DevEx, TEditClasifTemporal_DevEx )
end;

procedure TdmAsistencia.cdsClasifiTempBeforePost(DataSet: TDataSet);
const
     K_USUARIO_OK = -1;
begin
     with cdsClasifiTemp do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'US_COD_OK' ).AsInteger := K_USUARIO_OK;
     end;
end;

procedure TdmAsistencia.cdsClasifiTempNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'AU_FECHA').AsDateTime := dmCliente.FechaAsistencia;
          FieldByName( 'CB_CODIGO' ).AsInteger := dmCliente.Empleado;
     end;
end;

procedure TdmAsistencia.cdsClasifiTempCB_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          {
          with dmRecursos.GetDatosClasificacion( FieldByName( 'CB_CODIGO' ).AsInteger, dmCliente.FechaAsistencia ) do
          begin
               FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
               FieldByName( 'CB_TURNO' ).AsString := Turno;
               FieldByName( 'CB_PUESTO' ).AsString := Puesto;
               FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
               FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
               FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
               FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
               FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
               FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
               FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
               FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
               FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
          end;
          }
          FieldByName( 'PrettyName' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescripcion( FieldByName( 'CB_CODIGO' ).AsString );
     end;
end;

procedure TdmAsistencia.MuestraCalendarioEmpleado;
begin
     with dmcliente do
     begin
          FCalendarioEmpleado_DevEx.ShowCalendarioEmpleado( Empleado, FechaAsistencia, cdsCalendarioEmpleado );
     end;
end;

{OP: 11/06/08}
procedure TdmAsistencia.MuestraCalendarioVacacion( const rDiasGozar : Double );
begin
     with dmCliente do
     begin
          FCalendarioVacacion_DevEx.ShowCalendarioVacacion( Empleado, FechaDefault, cdsCalendarioEmpleado, rDiasGozar );
     end;
end;

procedure TdmAsistencia.ResetCalendarioEmpleadoActivo(const dInicio, dFinal: TDate);
begin
     with dmCliente do
     begin
          cdsCalendarioEmpleado.Data := ServerAsistencia.GetCalendarioEmpleado( Empresa, Empleado, dInicio, dFinal );
     end;
end;

function TdmAsistencia.PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                                  const iEmpleado: integer;
                                                  const eTipo: eStatusPeriodo;
                                                  const lPuedeModificar: Boolean;
                                                  var sMensaje: string): Boolean;
begin
     Result := lPuedeModificar;
     if  NOT Result then
     begin
          Result := ServerAsistencia.PuedeCambiarTarjetaStatus( dmCliente.Empresa,
                                                                dInicial,
                                                                iEmpleado,
                                                                Ord( eTipo ) );
          if ( not Result ) then
          begin
               sMensaje := Format( 'No se Puede Modificar Tarjetas de N�minas con Status Mayor a %s', [ ObtieneElemento( lfStatusPeriodo, ord(eTipo) ) ] );
          end;
     end
end;

procedure TdmAsistencia.RegistrarTarjetaPuntual (const sChecada: string);
begin
      FToolsAsistencia.RegistrarTarjetaPuntual(cdsChecadas, sChecada);
end;

function TdmAsistencia.GetChecadaPuntual(const sHorario: string; iTipoChecada: Integer; var sChecada: String): boolean;
begin
     Result := FToolsAsistencia.GetChecadaPuntual(dmCatalogos.cdsHorarios, sHorario, iTipoChecada, sChecada);
end;

procedure TdmAsistencia.cdsGridTarjetasPeriodoAlAdquirirDatos( Sender: TObject );
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with cdsGridTarjetasPeriodo do
        begin
             with Parametros do
             begin
                  with dmCliente do
                  begin
                       AddDate('FechaIni',GetDatosPeriodoActivo.InicioAsis  );
                       AddDate('FechaFin',GetDatosPeriodoActivo.FinAsis );
                       AddDate('Year',GetDatosPeriodoActivo.Year );
                       AddInteger('Tipo',Ord ( GetDatosPeriodoActivo.Tipo ) );
                       AddInteger('Numero',GetDatosPeriodoActivo.Numero );

                       AddInteger ('Empleado',Empleado);

                       AddBoolean ('PuedeAgregarTarjeta',ZAccesosMgr.CheckDerecho( D_ASIS_DATOS_TARJETA_DIARIA, K_DERECHO_ALTA ) );
                       AddBoolean ('PuedeModificarAnteriores',PuedeModificarTarjetasAnteriores );

                       AddBoolean ('EsFechaLimite',( not Global.GetGlobalBooleano( K_GLOBAL_BLOQUEO_X_STATUS ) ) );
                       AddDate('FechaLimite',Global.GetGlobalDate( K_GLOBAL_FECHA_LIMITE ) );

                       AddBoolean('EsSupervisores', False );
                       AddInteger('UsuarioActivo', Usuario );
                  end;
             end;
             Data := ServerSuper.GetTarjetasPeriodo( dmCliente.Empresa, Parametros.VarValues );
        end;
     finally
            FreeAndNil( Parametros );
     end;
end;

function TdmAsistencia.GetTipoMotivo( sCampo: String ): Integer;
begin
     Result:= 0;
     if( sCampo = 'M_HRS_EXTRAS' ) Then
        Result := Ord(acExtras);
     if( sCampo = 'M_DESCANSO' )then
         Result := Ord(acDescanso );
     if( sCampo = 'M_PER_CG' ) Then
            Result := Ord(acConGoce );
     if( sCampo = 'M_PER_CG_ENT' ) Then
         Result :=  Ord(acConGoceEntrada );
     if( sCampo = 'M_PER_SG' ) Then
         Result := Ord(acSinGoce );
     if ( sCampo = 'M_PER_SG_ENT' ) Then
        Result :=  Ord(acSinGoceEntrada );
     if( sCampo = 'M_PRE_FUERA_JOR') Then
         Result:=  Ord(acPrepagFueraJornada );
     if( sCampo = 'M_PRE_DENTRO_JOR') Then
         Result:=  Ord(acPrepagDentroJornada );
end;

function TdmAsistencia.Aprobar_Autorizaciones( oDataSet : TDataSet;const sAprobacion: string ): Boolean;
begin
     if Global.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES ) then
     begin
          with oDataset do
               Result := ( FieldByName( sAprobacion ).AsInteger = 0);
     end
     else
         Result := TRUE;
end;

procedure TdmAsistencia.RegistrarTarjetaPuntualColectiva( DataSet:TZetaClientDataSet; const sChecada, sCampoChecada,sTitulo: string );
begin
     with DataSet do
     begin
          if ( (sCampoChecada = 'CHECADA4') AND (FieldByName( 'CHECADA2' ).AsString = sChecada) ) then
             ZError( sTitulo,  Format( 'Ya existe checada a las %s', [MaskHora(ConvierteHora( sChecada ))] ), 0 )
          else
          begin
               edit;
               FieldByName(sCampoChecada).AsString := ConvierteHora( sChecada );
               //post;
          end;
     end;
end;

procedure TdmAsistencia.cdsGridTarjetasPeriodoAlCrearCampos(
  Sender: TObject);
var
   i:Integer;
begin
      with cdsGridTarjetasPeriodo DO
      begin
           FieldByName( 'AU_FECHA' ).OnGetText := AU_FECHAGetText;
           with dmCatalogos do
               CreateSimpleLookup( cdsHorarios, 'HO_DESCRIP', 'HO_CODIGO' );
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'IN_ELEMENT', 'AU_TIPO' );
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_OUT2EAT', lfOut2Eat );
          MaskHoras( 'AU_HORASCK' );
          MaskHoras( 'AU_NUM_EXT' );
          MaskHoras( 'HRS_EXTRAS' );
          MaskHoras( 'DESCANSO' );
          MaskHoras( 'PER_CG' );
          MaskHoras( 'PER_SG' );
          MaskHoras( 'PER_CG_ENT' );
          MaskHoras( 'PER_SG_ENT' );
          MaskHoras( 'PRE_FUERA_JOR' );
          MaskHoras( 'PRE_DENTRO_JOR' );
          FieldByName( 'HO_CODIGO' ).OnChange := HO_CODIGOChange;
          FieldByName( 'HO_CODIGO' ).OnValidate := cdsHorarioValidate;
          FieldByName( 'AU_STATUS' ).OnChange := HO_CODIGOChange;
          for i := 1 to 4 do
          begin
               MaskTime( 'CHECADA' + IntToStr( i ) );
               with FieldByName( 'CHECADA' + IntToStr( i ) ) do
               begin
                    OnSetText := CH_H_REALSetText;
                    OnValidate := CH_H_REALValidar;
                    OnChange:= CHECADAChange;
               end;

               with FieldByName('MC_CHECADA' + IntToStr( i ) ) do
               begin
                    OnValidate:= cdsMotChecadaValidate;
                    OnChange:= cdsMotChecadaChange;
               end;
          end;

          FieldByName( 'M_HRS_EXTRAS' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_DESCANSO' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PER_CG' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PER_CG_ENT' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PER_SG' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PER_SG_ENT' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PRE_FUERA_JOR' ).OnValidate:= cdsMotAutoValidate;
          FieldByName( 'M_PRE_DENTRO_JOR' ).OnValidate:= cdsMotAutoValidate;

           dmSistema.cdsUsuarios.Conectar;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_HRS_EXTRAS', 'OK_HRS_EXTRAS' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCANSO', 'OK_DESCANSO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_PER_CG', 'OK_PER_CG' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_PER_CG_ENT', 'OK_PER_CG_ENT' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_PER_SG', 'OK_PER_SG' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_PER_SG_ENT', 'OK_PER_SG_ENT' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookup, 'US_PRE_FUERA_JOR', 'OK_PRE_FUERA_JOR' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookup, 'US_PRE_DENTRO_JOR','OK_PRE_DENTRO_JOR' );

      end;
end;

procedure TdmAsistencia.CH_H_REALValidar(Sender: TField);
var
   sHora: String;
begin
     sHora := Sender.AsString;
     if not ZetaCommonTools.ValidaHora( sHora, '48' ) then
     begin
          raise ERangeError.Create( 'Hora "' + Copy( sHora, 1, 2 ) + ':' +
                                    Copy( sHora, 3, 2 ) + '" Inv�lida' );
     end;
end;


procedure TdmAsistencia.AU_FECHAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.DataSet.IsEmpty then
        Text := ''
     else
        Text := Sender.AsString + ' -  ' + ObtieneElemento( lfDiasSemana, DayOfWeek( Sender.AsDateTime ) - 1 );
end;

procedure TdmAsistencia.cdsMotAutoValidate(Sender: TField);
begin
     ValidaLookup( UpperCase(Sender.AsString),Format( 'TB_TIPO = %d OR TB_TIPO = %d',[ K_MOTIVO_AUTORIZ_OFFSET , GetTipoMotivo( Sender.FieldName )  ] ), dmTablas.cdsMotAuto );
end;

{ACL160409}
procedure TdmAsistencia.cdsChecadaManualValidate(Sender: TField);
begin
     if StrLleno( Sender.AsString ) then
     begin
          if ( EsTipoChecadaAutorizacion( eTipoChecadas( Sender.DataSet.FieldByName( 'CH_TIPO' ).AsInteger ) ) ) then
              DataBaseError( 'No Se Pueden Modificar Las Autorizaciones' )
          else
          begin
               ValidaMotChecadaManual( Sender.AsString, Sender.DataSet.FieldByName('US_CODIGO').AsInteger);
               ValidaLookup( UpperCase(Sender.AsString),VACIO, dmTablas.cdsMotCheca );
          end;
     end;
end;

procedure TdmAsistencia.cdsMotChecadaValidate(Sender: TField);
const
     K_ERRROR_MOTIVO = 'No se puede especificar motivo: ';
var
   sCampoUsuario, sCampoChecada: String;
begin
     { Revisa que exista valor en la checada y que sea una checada manual }
     sCampoUsuario:= StrTransform( Sender.FieldName, 'MC_', 'US_');
     sCampoChecada:= StrTransform( Sender.FieldName, 'MC_', VACIO );

     with cdsGridTarjetasPeriodo do
     begin
          if StrLleno(Sender.AsString) then
          begin
               if StrLleno( FieldByName(sCampoChecada).AsString ) then
               begin
                    if FieldByName(sCampoUsuario).AsInteger = 0  then
                       DataBaseError( K_ERRROR_MOTIVO + 'No es una checada manual' )
                    else
                        ValidaLookup( UpperCase(Sender.AsString),VACIO, dmTablas.cdsMotCheca );
               end
               else
                   DataBaseError(K_ERRROR_MOTIVO + 'No se ha registrado checada' );
          end;
     end;
end;

procedure TdmAsistencia.cdsMotChecadaChange(Sender: TField);
begin
     if StrLleno( Sender.AsString ) then  //Guarda ultimo motivo
        FUltimoMotivoCh:= Sender.AsString;
end;

procedure TdmAsistencia.cdsHorarioValidate(Sender: TField);
begin
     ValidaLookup( Sender.AsString,VACIO, dmCatalogos.cdsHorarios );
end;

procedure TdmAsistencia.ValidaLookup( const sValor,sFiltro : String ; oLookup: TZetaLookupDataSet  );
const
     sMensaje = 'No Existe el C�digo: %s en %s';
     sMensajeInactivo = 'No est� Activo el C�digo: %s en %s';
var
   sDescrip: String;
   lActivo, lUsarConfidencialidad : Boolean;
begin
     lActivo := True;
     lUsarConfidencialidad:= True;
     
     if strLleno( sValor ) then
        with oLookup do
             if ( not LookupKey( sValor, sFiltro, sDescrip, lActivo, lUsarConfidencialidad) ) then
                             DataBaseError( Format( sMensaje, [ sValor, LookupName ] ) )
             else
             if ( not lActivo ) then
                          DataBaseError( Format( sMensajeInactivo, [ sValor, LookupName ] ) );
end;

procedure TdmAsistencia.cdsGridTarjetasPeriodoAlEnviarDatos(
  Sender: TObject);
var
   ErrorCount : Integer;   
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     //Result:= TRUE;
     ErrorCount := 0;
     with cdsGridTarjetasPeriodo do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Parametros := TZetaParams.Create;
               try
                  Parametros.AddInteger( 'Cambios', ChangeCount );
                  if Reconcile ( ServerSuper.GrabaTarjetaPeriodo( dmCliente.Empresa, Parametros.VarValues, ErrorCount ,DeltaNull ) ) then
                  begin
                       TressShell.SetDataChange( [ enAusencia,enChecadas,enNomina ] );
                  end;
               finally
                      FreeAndNil( Parametros );
               end;
          end;
     end;
end;

procedure TdmAsistencia.cdsGridTarjetasPeriodoBeforePost(DataSet: TDataSet);

     function ExisteMotivoVacio: Boolean;

          function ChecadaInvalida(sChecada: String): Boolean;
          begin
               with DataSet do
               begin
                    Result:= StrLleno( FieldByName(sChecada).AsString )  and
                             ( FieldByName('US_'+sChecada).AsInteger > 0 ) and
                               StrVacio( FieldByName('MC_'+sChecada).AsString );
                    if Result then
                       FieldByName('MC_'+sChecada).FocusControl;
               end;
          end;
     begin
          Result:= ChecadaInvalida('CHECADA1') or
                   ChecadaInvalida('CHECADA2') or
                   ChecadaInvalida('CHECADA3') or
                   ChecadaInvalida('CHECADA4');
     end;

     procedure ConvierteUpperCaseMotivos;

         procedure Convierte( const sCampo: String );
         begin
              with DataSet.FieldByName(sCampo) do
              begin
                   if StrLleno( AsString ) then
                      AsString:= UpperCase(AsString);
              end;
         end;
     begin
          Convierte('MC_CHECADA1');
          Convierte('MC_CHECADA2');
          Convierte('MC_CHECADA3');
          Convierte('MC_CHECADA4');
     end;

begin
     with DataSet do
     begin
          with dmCliente do
          begin
               if not PuedeCambiarTarjetaDlg( FieldByName( 'AU_FECHA' ).AsDateTime, Empleado )then
               begin
                    Abort;
               end
               else if (CapturaObligatoriaMotCH) and (ExisteMotivoVacio) then
               begin
                    DataBaseError('No se ha especificado un motivo de checada manual');
               end
               else
               begin
                    ValidaAutoBeforePost( acExtras, FieldByName( 'HRS_EXTRAS' ).AsFloat, FieldByName( 'M_HRS_EXTRAS' ) );
                    ValidaAutoBeforePost( acDescanso, FieldByName( 'DESCANSO' ).AsFloat, FieldByName( 'M_DESCANSO' ) );
                    ValidaAutoBeforePost( acConGoce, FieldByName( 'PER_CG' ).AsFloat, FieldByName( 'M_PER_CG' ) );
                    ValidaAutoBeforePost( acConGoceEntrada, FieldByName( 'PER_CG_ENT' ).AsFloat, FieldByName( 'M_PER_CG_ENT' ) );
                    ValidaAutoBeforePost( acSinGoce, FieldByName( 'PER_SG' ).AsFloat, FieldByName( 'M_PER_SG' ) );
                    ValidaAutoBeforePost( acSinGoceEntrada, FieldByName( 'PER_SG_ENT' ).AsFloat, FieldByName( 'M_PER_SG_ENT' ) );
                    ValidaAutoBeforePost( acPrepagFueraJornada, FieldByName( 'PRE_FUERA_JOR' ).AsFloat, FieldByName( 'M_PRE_FUERA_JOR' ) );
                    ValidaAutoBeforePost( acPrepagDentroJornada, FieldByName( 'PRE_DENTRO_JOR' ).AsFloat, FieldByName( 'M_PRE_DENTRO_JOR' ) );
               end;

               if ( PermitirCapturaMotivoCH ) then
                  ConvierteUpperCaseMotivos;
          end;
     end;
end;

procedure TdmAsistencia.cdsGridTarjetasPeriodoBeforeOpen( DataSet: TDataSet);
begin
     FUltimoMotivoCH:= VACIO;
end;

{ACL170409}
procedure TdmAsistencia.cdsChecadasAfterPost(DataSet: TDataSet);
begin
     FUltimoMotivoCh := cdsChecadas.FieldByName( 'CH_MOTIVO' ).AsString;
end;


procedure TdmAsistencia.cdsChecadasBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          PuedeCambiarTarjetaBeforePost(DataSet);
          if ( FieldByName( 'US_CODIGO' ).AsInteger <> 0 ) then
          begin
               if ( StrVacio( FieldByName( 'CH_MOTIVO' ).AsString ) ) then
               begin
                    if ( CapturaObligatoriaMotCH ) then
                       DataBaseError('Motivo no puede quedar vac�o ')
               end
               else
               begin //Lo hace por el agrupamiento en reportes de profesional
                    FieldByName('CH_MOTIVO').AsString:= UpperCase(FieldByName('CH_MOTIVO').AsString);
               end;
          end;
     end;
end;

procedure TdmAsistencia.cdsChecadasAfterOpen(DataSet: TDataSet);
begin
     FUltimoMotivoCH:= VACIO;
end;

procedure TdmAsistencia.cdsChecadasAfterCancel(DataSet: TDataSet);
begin
     FUltimoMotivoCH := VACIO;
end;

//procedure TdmAsistencia.cdsCosteoTransferenciasAlAdquirirDatos(Sender: TObject);

procedure TdmAsistencia.ObtieneTransferencias( Params: TZetaParams );
begin
     cdsCosteoTransferencias.Data := ServerAsistencia.GetCosteoTransferencias( dmCliente.Empresa, Params.VarValues );
     cdsCosteoTransferencias.ResetDataChange;
end;


procedure TdmAsistencia.cdsCosteoTransferenciasAU_FECHAValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsDateTime <> 0 ) then
          begin
               AsignaCCOrigen;
          end
          else
              DataBaseError( 'Fecha es Inv�lida' );
     end;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasCB_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No Existe el Empleado #' + IntToStr( AsInteger ) )
               else
               begin
                    AsignaCCOrigen;
               end;

          end;
{          else
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
}
     end;
end;

procedure TdmAsistencia.AsignaCCOrigen;
 var
    oChecadas: Olevariant;
    sNivel: string;
begin
     //Este metodo me dice cual es el nivel de costeo del empleado.
     //Si la fecha de transferencia es mayor o igual al ultimo cambio de nivel de organigrama, se toma lo que dice COLABORA
     //Si la fecha de transferencia es menor al ultimo cambio de nivel de organigrama, se busca la tarjeta diaria de ese dia
     //Si no hay tarjeta ese dia, se busca en el Kardex
     with cdsCosteoTransferencias do
     begin
          with dmCliente do
          begin
               if  dmCliente.cdsEmpleadoLookUp.Active and
                   (FieldByName('CB_CODIGO').AsInteger <> 0 ) and
                   NOT (FieldByName( 'AU_FECHA' ).IsNull and (State=dsInsert) ) then
               begin
                    if ( cdsEmpleadoLookUp.FieldByName('CB_FEC_NIV').AsDateTime <= FieldByName( 'AU_FECHA' ).AsDateTime ) then
                         FieldByName( 'CC_ORIGEN' ).AsString := cdsEmpleadoLookUp.FieldByName( Global.NivelCosteo ).AsString
                    else
                    begin
                         cdsCosteoTarjetaLookup.Data := ServerAsistencia.GetTarjeta( Empresa, FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'AU_FECHA' ).AsDateTime, oChecadas );
                         if (cdsCosteoTarjetaLookup.RecordCount >0 ) then
                            FieldByName( 'CC_ORIGEN' ).AsString := cdsCosteoTarjetaLookup.FieldByName(Global.NivelCosteo).AsString
                         else
                             with dmRecursos.GetDatosClasificacion(FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'AU_FECHA' ).AsDateTime) do
                             begin
                                  case Global.GetGlobalInteger(K_GLOBAL_NIVEL_COSTEO) of
                                       1: sNivel := Nivel1;
                                       2: sNivel := Nivel2;
                                       3: sNivel := Nivel3;
                                       4: sNivel := Nivel4;
                                       5: sNivel := Nivel5;
                                       6: sNivel := Nivel6;
                                       7: sNivel := Nivel7;
                                       8: sNivel := Nivel8;
                                       9: sNivel := Nivel9;
                                       {$ifdef ACS}
                                       10: sNivel := Nivel10;
                                       11: sNivel := Nivel11;
                                       12: sNivel := Nivel12;
                                       {$endif}
                                  end;
                                  FieldByName( 'CC_ORIGEN' ).AsString := sNivel;
                             end;

                    end;
               end;
          end;
     end
end;

procedure TdmAsistencia.cdsCosteoTransferenciasAlCrearCampos( Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     dmTablas.GetDataSetTransferencia.Conectar;
     with cdsCosteoTransferencias do
     begin
          ListaFija( 'TR_TIPO', lfTipoHoraTransferenciaCosteo );
          ListaFija( 'TR_STATUS', lfStatusTransCosteo );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_APRUEBA', 'TR_APRUEBA' );
          CreateSimpleLookup( dmTablas.GetDataSetTransferencia, 'CC_DESCRIP', 'CC_ORIGEN' );
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsCosteoTransferenciasCB_CODIGOValidate;
          FieldByName( 'AU_FECHA' ).OnValidate := cdsCosteoTransferenciasAU_FECHAValidate;

          MaskFecha('TR_FECHA');
          MaskHoras( 'AU_HORAS');
          MaskHoras( 'AU_EXTRAS');
          MaskHoras( 'TR_HORAS');
          MaskHoras( 'ASIS_HORAS');

          FieldByName( 'TR_STATUS' ).OnChange  := TR_STATUSChange;

          with FieldByName( 'TR_MOTIVO' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := TR_MOTIVOGetText;
          end;

          with FieldByName( 'US_CODIGO' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := US_CODIGOGetText;
          end;

          with FieldByName( 'TR_APRUEBA' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := US_CODIGOGetText;
          end;
     end;
end;

procedure TdmAsistencia.TR_STATUSChange(Sender: TField);
begin
     case eStatusTransCosteo(Sender.AsInteger) of
          stcAprobada..stcCancelada: Sender.DataSet.FieldByName( 'TR_FEC_APR' ).AsDateTime := Date;
          stcPorAprobar: Sender.DataSet.FieldByName( 'TR_FEC_APR' ).AsDateTime := NullDateTime;
     end;
end;

procedure TdmAsistencia.TR_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if StrLleno( Sender.AsString ) then
        Text := Sender.AsString + '=' + dmTablas.cdsMotivoTransfer.GetDescripcion( Sender.AsString );

end;

procedure TdmAsistencia.cdsCosteoTransferenciasAlAgregar(Sender: TObject);
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpTransfer );
        cdsCosteoTransferencias.Append;
        ZBaseEdicion_DevEx.ShowFormaEdicion( EditCosteoTransferencias_DevEx, TEditCosteoTransferencias_DevEx );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasAlModificar( Sender: TObject);
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpTransfer );
        ZBaseEdicion_DevEx.ShowFormaEdicion( EditCosteoTransferencias_DevEx, TEditCosteoTransferencias_DevEx );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasNewRecord( DataSet: TDataSet);
begin
     with cdsCosteoTransferencias do
     begin
          if ( FTransferUltimaFecha <> NullDateTime ) then
              FieldByName( 'AU_FECHA' ).AsDateTime := FTransferUltimaFecha
          else
          begin
               if ( dmCliente.GetDatosPeriodoActivo.Inicio <> NullDateTime ) then
                  FieldByName( 'AU_FECHA' ).AsDateTime := dmCliente.GetDatosPeriodoActivo.Inicio
               else
                   FieldByName( 'AU_FECHA' ).AsDateTime := dmCliente.FechaAsistencia;
          end;
          FieldByName( 'TR_TIPO' ).AsInteger := Ord(FTransferUltimoTipo );
          FieldByName( 'TR_HORAS' ).AsFloat := FTransferUltimaHora;
          FieldByName( 'CC_CODIGO' ).AsString := FTransferUltimoCC;
          FieldByName( 'TR_MOTIVO' ).AsString := FTransferUltimoMotivo;
          FieldByName( 'TR_NUMERO' ).AsFloat := 0;
          FieldByName( 'TR_TEXTO' ).AsString := VACIO;
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'TR_FECHA' ).AsDAteTime := Trunc(Now);
          FieldByName( 'TR_GLOBAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'TR_APRUEBA' ).AsInteger := dmCliente.Usuario;
	  FieldByName( 'TR_FEC_APR' ).AsDateTime := Trunc(Now);
	  FieldByName( 'TR_STATUS' ).AsInteger := ord( stcAprobada );
	  FieldByName( 'TR_TXT_APR' ).AsString := VACIO;
     end;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasAlEnviarDatos( Sender: TObject);
var
   ErrorCount: Integer;
   ResultData: OleVariant;
begin
     ErrorCount := 0;
     with cdsCosteoTransferencias do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               FLastEmpleadoTransferencia := FieldByName('CB_CODIGO').AsInteger ;
               if Reconciliar( ServerAsistencia.GrabaCosteoTransferencias( dmCliente.Empresa, Delta, ParametrosTransferencias.VarValues, ResultData, ErrorCount ) ) then
               begin
                    if FAgregandoTransferencia then
                    begin
                         Data := ResultData;
                         FAgregandoTransferencia := FALSE;
                         Append;
                    end ;
                    TressShell.SetDataChange( [ enTransferencias ] );
               end;
          end;
     end;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasBeforePost( DataSet: TDataSet);

begin
     with cdsCosteoTransferencias do
     begin
          if FieldByName('TR_HORAS').AsFloat = 0 then
             DataBaseError( 'El valor de horas no puede quedar en cero' );
          if StrVacio( FieldByName('CC_CODIGO').AsString ) then
             with Global do
                  DataBaseError( Format( 'El %s no puede quedar vac�o', [Global.NombreCosteo] ) );
          if FieldByName('CB_CODIGO').AsInteger = 0 then
             DataBaseError( 'El n�mero de Empleado no puede quedar en cero' );


          //Si cambia algo en Transferencia, el usuario que Transfiere se cambia al usuario activo
          if CambiaCampo( FieldByName( 'TR_HORAS'  ) ) or
             CambiaCampo( FieldByName( 'CC_CODIGO' ) ) or
             CambiaCampo( FieldByName( 'TR_MOTIVO' ) ) or
             CambiaCampo( FieldByName( 'TR_TIPO'   ) ) or
             CambiaCampo( FieldByName( 'CB_CODIGO' ) ) or
             CambiaCampo( FieldByName( 'AU_FECHA'  ) ) then
          begin
               FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
               FieldByName('TR_FECHA').AsDateTime := Date;
          end;

          //Si cambia el Status de la aprobacion, el usuario que Recibe se cambia al usuario activo
          if CambiaCampo( FieldByName('TR_STATUS') ) then
          begin
               FieldByName('TR_APRUEBA').AsInteger := dmCliente.Usuario;
          end;

     end;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasAfterPost(DataSet: TDataSet);
begin
     with cdsCosteoTransferencias do
     begin
          FTransferUltimaFecha := FieldByName( 'AU_FECHA' ).AsDateTime;
          FTransferUltimoTipo := eTipoHoraTransferenciaCosteo( FieldByName( 'TR_TIPO' ).AsInteger );
          FTransferUltimaHora := FieldByName( 'TR_HORAS' ).AsFloat;
          FTransferUltimoMotivo := FieldByName( 'TR_MOTIVO' ).AsString;
          FTransferUltimoCC := FieldByName( 'CC_CODIGO' ).AsString;
     end;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasAlBorrar(Sender: TObject);
 var ResultData: Olevariant;
     ErrorCount: integer;
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Esta Transferencia ?' ) then
     begin
          with cdsCosteoTransferencias do
          begin
               Delete;
               Reconciliar( ServerAsistencia.GrabaCosteoTransferencias( dmCliente.Empresa, Delta, ParametrosTransferencias.VarValues, ResultData, ErrorCount ) );
          end;
     end;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasBeforeInsert(DataSet: TDataSet);
begin
     FAgregandoTransferencia := TRUE;
end;

procedure TdmAsistencia.cdsCosteoTransferenciasAfterCancel(DataSet: TDataSet);
begin
     FAgregandoTransferencia := FALSE;
end;


function TdmAsistencia.GetPermisoDiasHabiles( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := ServerAsistencia.GetPermisoDiasHabiles( dmCliente.Empresa,iEmpleado,dInicio,dFinal);
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmAsistencia.GetPermisoFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const Dias: integer; var rDiasRango: Double ): TDate;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result :=   ServerAsistencia.GetPermisoFechaRegreso( dmCliente.Empresa, iEmpleado, dInicio, Dias, rDiasRango );
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmAsistencia.GetHorarioTurno( const iEmpleado: Integer; const dFecha: TDate; iTipo: Integer ): String;
var
   Valor: Variant;
begin
     Valor := ServerAsistencia.GetHorarioTurno( dmCliente.Empresa, VarArrayOf( [ iEmpleado, dFecha, iTipo ] ) );

    // with Result do
    // begin
          Result := Valor[0];
   //  end;
end;

end.
