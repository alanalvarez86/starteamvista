object dmEmailService: TdmEmailService
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 878
  Top = 300
  Height = 480
  Width = 696
  object Email: TNMSMTP
    Port = 25
    ReportLevel = 0
    Authtype = NONE
    EncodeType = uuMime
    ClearParams = True
    SubType = mtPlain
    Charset = 'us-ascii'
    OnRecipientNotFound = EmailRecipientNotFound
    Left = 176
    Top = 32
  end
  object IdEmail: TIdSMTP
    MaxLineAction = maException
    ReadTimeout = 0
    Port = 25
    AuthenticationType = atNone
    Left = 232
    Top = 32
  end
  object IdPostMessage: TIdMessage
    AttachmentEncoding = 'MIME'
    BccList = <>
    CCList = <>
    Encoding = meMIME
    NoEncode = True
    Recipients = <>
    ReplyTo = <>
    Left = 296
    Top = 32
  end
end
