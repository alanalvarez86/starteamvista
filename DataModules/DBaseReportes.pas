{$HINTS OFF}
unit DBaseReportes;

interface
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs,ComObj, ActiveX, Db, DBClient, IniFiles,
     {$ifndef VER130}
     Variants,
     {$endif}
{$ifdef DOS_CAPAS}
     DServerReportes,
{$else}
     {$ifdef RDD}
     ReporteadorDD_TLB,
     {$else}
     Reportes_TLB,
     {$endif}
{$endif}
{$IFDEF TRESS_DELPHIXE5_UP}
     FBaseReportes_DevEx,
{$ELSE}
     FBaseReportes,
{$ENDIF}
     ZetaClientDataSet,
     ZetaTipoEntidad,
     ZetaTipoEntidadTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZReportConst,
     ZAgenteSQLClient,
     ZReportTools,
     ZReportToolsConsts;

const
     K_RENGLON_NUEVO = -2;

{$ifdef RDD}
type
    {$ifdef DOS_CAPAS}
    TdmServerReportes = TdmServerReporteadorDD;
    {$else}
    IdmServerReportesDisp = IdmServerReporteadorDDDisp;
    {$endif}
{$endif}


type
    TIRSubsecuente = class(TOBject)
    public
      Resultados: TZetaClientDataSet;
      Reporte: TZetaClientDataSet;
      CampoRep:TZetaClientDataSet;
      Agente:  TSQLAgenteClient;
      constructor Create;
      destructor  Destroy; override;
    end;
type
  TdmBaseReportes = class(TDataModule)
    cdsCampoRepFiltros: TZetaClientDataSet;
    cdsEditReporte: TZetaClientDataSet;
    cdsReportes: TZetaClientDataSet;
    cdsLookupReportes: TZetaLookupDataSet;
    cdsEscogeReporte: TZetaClientDataSet;
    cdsResultados: TZetaClientDataSet;
    cdsCampoRep: TZetaClientDataSet;
    cdsImprimeForma: TZetaClientDataSet;
    cdsSuscripReportes: TZetaClientDataSet;
    cdsPlantilla: TZetaClientDataSet;
    cdsBlobs: TZetaClientDataSet;
    cdsEditReporteTemporal: TZetaClientDataSet;
    procedure cdsReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsReportesAlModificar(Sender: TObject);
    procedure cdsReportesAlCrearCampos(Sender: TObject);
    {$ifdef ver130}
    procedure cdsReportesReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReportesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsEditReporteNewRecord(DataSet: TDataSet);
    procedure cdsEditReporteAlAdquirirDatos(Sender: TObject);
    procedure cdsEditReporteAlEnviarDatos(Sender: TObject);
    procedure cdsEditReporteAfterCancel(DataSet: TDataSet);
    procedure cdsEditReporteAlCrearCampos(Sender: TObject);
    procedure cdsCampoRepFiltrosNewRecord(DataSet: TDataSet);
    procedure cdsCampoRepFiltrosAfterDelete(DataSet: TDataSet);
    procedure cdsEditReporteBeforePost(DataSet: TDataSet);
    procedure cdsReportesAlAgregar(Sender: TObject);
    procedure cdsEditReporteAlModificar(Sender: TObject);
    procedure cdsLookupReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsLookupReportesAlModificar(Sender: TObject);
    procedure cdsEscogeReporteAlCrearCampos(Sender: TObject);
    procedure cdsEscogeReporteAlAdquirirDatos(Sender: TObject);
    procedure cdsEscogeReporteAlModificar(Sender: TObject);
    procedure cdsCampoRepFiltrosAlAdquirirDatos(Sender: TObject);
    procedure cdsCampoRepAfterPost(DataSet: TDataSet);
    procedure cdsImprimeFormaAlAdquirirDatos(Sender: TObject);
    procedure cdsImprimeFormaAlCrearCampos(Sender: TObject);
    procedure cdsImprimeFormaAlModificar(Sender: TObject);
    procedure cdsLookupReportesAlAgregar(Sender: TObject);
    procedure cdsLookupReportesLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsCampoRepAlCrearCampos(Sender: TObject);
    procedure cdsLookupReportesGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsReportesCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FClasifActivo: eClasifiReporte;
    FTipoReporte: eTipoReporte;
    FEntidadReporte: TipoEntidad;
    {$ifdef ADUANAS}
    FListaReportes: ArregloReportes;
    FListaEntidades: ArregloEntidades;
    {$endif}
    FReadOnly : Boolean;
    FCodigoReporte: integer;
    //FReporte : integer;
    lHuboCambios : Boolean;
    FPolizaCuenta: string;
    FPolizaFormula: string;
    FSoloImpresion: Boolean;
    FParamList: TZetaParams;
    FSQLAgente : TSQLAgenteClient;
    FClasificaciones: TStrings;
    FImpresoraDefault: string;
    FPuedeSuscribirse: Boolean;
    FPuedeSuscribirUsuarios: Boolean;
    FImprimeSubsecuentes : Boolean;
    FForzarImpresion: Boolean;
    //DevEx: Lista con las clasificaciones permitidas para el usuario (ramas no bloquedas)
    FClasificacionesPermitidas: TStrings;
    //FModoSuper: Boolean;
    function ReporteValido( const eEntidad: TipoEntidad ): Boolean;
    function EsPoliza: Boolean;
    function GetClasificaciones: string;
{$IFDEF TRESS_DELPHIXE5_UP}
    //DevEx: Este metodo concatena todas las clasificaciones permitidas para mandarlas al dll Solo aplica para
    //Vista Actual
    function GetClasificacionesPermitidas: string;
{$ENDIF}
    procedure GrabaReporte;
    procedure PosicionaConsulta;
    procedure SincronizaDatos;
    procedure CR_OPERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure ListaFijaEntidad( DataSet : TZetaClientDataSet);
  protected
    { Protected declarations }
    FReporteActivo : integer;
{$ifdef DOS_CAPAS}
    function GetServerReportes: TdmServerReportes;
    property ServerReportes: TdmServerReportes read GetServerReportes;
{$else}
    FServidor: IdmServerReportesDisp;
    function GetServerReportes: IdmServerReportesDisp ;
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
{$endif}
    function GeneraSQL( var oSQLAgente: OleVariant; var ParamList : OleVariant; var sError : WideString ): OleVariant; virtual;abstract;
    function EvaluaParams(SQLAgente: TSQLAgenteClient; var sError: WideString): Boolean;
    function ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ): Boolean;virtual;abstract;
    function ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;virtual;abstract;
    procedure AfterGeneraSQL;virtual;
    procedure GetFormaReporte( const eTipo: eTipoReporte );virtual;
  public
    { Public declarations }
    function EstaEnClasifi( iReporte:Integer; sCampo: String ): boolean;

    property PuedeSuscribirse: Boolean read FPuedeSuscribirse;
    property PuedeSuscribirUsuarios: Boolean read FPuedeSuscribirUsuarios;
    property ClasifActivo: eClasifiReporte read FClasifActivo write FClasifActivo;
    property EntidadReporte: TipoEntidad read FEntidadReporte write FEntidadReporte;
    property TipoReporte: eTipoReporte read FTipoReporte write FTipoReporte;
    {$ifdef ADUANAS}
    property ListaReporte: ArregloReportes read FListaReportes write FListaReportes;
    property ListaEntidad: ArregloEntidades read FListaEntidades write FListaEntidades;
    {$endif}
    property CodigoReporte: integer read FCodigoReporte write FCodigoReporte;
    property SoloImpresion: Boolean read FSoloImpresion write FSoloImpresion default FALSE;
    property SoloLectura: Boolean read FReadOnly write FReadOnly default FALSE;
    property ParamList: TZetaParams read FParamList;
    property SQLAgenteRep: TSQLAgenteClient read FSQLAgente;
    property Clasificaciones: TStrings read FClasificaciones write FClasificaciones;
    //DevEx: Propiedad para leer clasificaciones permitidas para el usuario
    property ClasificacionesPermitidas: TStrings read FClasificacionesPermitidas write FClasificacionesPermitidas;
    property ImpresoraDefault: string read FImpresoraDefault write FImpresoraDefault;
    property ImprimeSubsecuentes: Boolean read FImprimeSubsecuentes write FImprimeSubsecuentes;
    property ForzarImpresion: Boolean read FForzarImpresion write FForzarImpresion;

    function ConstruyeSQL(SQLAgente: TSQLAgenteClient; var sError: WideString; const FFiltroFormula, FFiltroDescrip: String; oParamList: TZetaParams; const oTipoPantalla : TipoPantalla; const lGeneraBitacora, lHayImagenes : Boolean ): Boolean;
    function Exporta( const sArchivo: String; var sError: String ): Boolean;
    function Importa( const sArchivo: String; var sError: String ): Boolean;
    function Importa_DevEx( const sArchivo: String; var sError: String ): Boolean; //DevEx(@am):Se declara el metodo DevEx porque utiliza una pantalla en su implementacion
    function GrabaPoliza : Boolean;
    procedure ModificaCampoReporte( const sCampo: String; const iUsuario: Integer );
    procedure BorraReporte;
    //DevEx: Agregados para Nueva Vista
    procedure BorraReporte_DevEx (Codigos: TStringList);
    procedure BorraReporteDeFavoritos;
    procedure BorraReporteDeSuscripciones;
    procedure BorraReportesDeFavoritos (Codigos: TStringList);
    procedure BorraReportesDeSuscripciones (Codigos: TStringList);
    //
    procedure GetPreviewReporte( const eTipo: eTipoReporte; const sFiltro: String = ''; const lImprimeForma: Boolean = FALSE );
    procedure GetImpresionReporte( const eTipo: eTipoReporte; const sFiltro: String = ''; const lImprimeForma: Boolean = FALSE  );

    procedure GetResultadoReporte( const eTipo: eTipoReporte; const sFiltro: String = ''; const lImprimeForma: Boolean = FALSE  );
    procedure GetResultadoIReporte( const iReporte: integer; const sFiltro: String = ''; const lSustituye: Boolean = TRUE;  const lImprimeForma: Boolean = FALSE );

    procedure ImprimeUnaForma(const sFiltro: string);overload;
    procedure ImprimeUnaForma( const sFiltro : string; const iReporte: integer; const eSalida : TipoPantalla = tgPreview );overload;
    procedure AgregaSQLColumnasParam( oSQLAgente : TSQLAgenteClient;oParam : TCampoMaster);
    //function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;virtual;
    function EvaluaParametros( oSQLAgente : TSQLAgenteClient; Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;
    function PruebaFormula(  Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;
    function PuedeAbrirCandado: Boolean;
    function AgregaFavoritos( oDataSet : TDataSet ): boolean;
    function AgregaSuscripciones( oDataSet : TDataSet ): boolean;
    function ChecaFavoritos: boolean;
    function ChecaSuscripciones: boolean;
    function GetPlantilla( const Plantilla: string ): TBlobField;
    function GetLogo( const Logo: string ): TBlobField;
    procedure BuscaConcepto;virtual;
    function DirectorioPlantillas: string;virtual;abstract;
    function ObtieneEntidad( const Index: TipoEntidad ): String;
    function ObtieneClasificacion( const Index: eClasifiReporte ): String;
    function ExisteClasificacion( const Index: eClasifiReporte ): Boolean;
    function ExisteEntidad( const Index: TipoEntidad ): Boolean;
{$IFDEF TRESS_DELPHIXE5_UP}
    procedure ImpresionReportesSubsecuentes;
{$ELSE}
    procedure ImpresionReportesSubsecuentesClasica;
{$ENDIF}
    function RealField(const FieldName: String):Boolean; //@(am): Agregada para saber si un campo viene en el quiery.
  end;

var
  dmBaseReportes: TdmBaseReportes;

//procedure OrdenarPor( DataSet: TClientDataset; const FieldName: String );
{$IFDEF TRESS_DELPHIXE5_UP}
procedure ShowFormaReporte( var Forma; EdicionClass: TBaseReportesClass_DevEx; const lReadOnly, lSoloImpresion: Boolean );
{$ELSE}
procedure ShowFormaReporte( var Forma; EdicionClass: TBaseReportesClass; const lReadOnly, lSoloImpresion: Boolean );
{$ENDIF}

implementation

uses
     ZReconcile,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaSystemWorking,
     ZetaDialogo,
     ZetaAsciiFile,
     DSistema,
     DCliente,
     DCatalogos,
     DDiccionario,
{$IFDEF TRESS_DELPHIXE5_UP}
     ZcxWizardBasico,
     ZetaBusqueda_DevEx,
     FFormas_DevEx,   //Edit By MP: Agregando las formas DevEx necesarias
     FListados_DevEx,
     FImpresionRapida_DevEx,
     FParametros_DevEx,
     FDatosImport_DevEx,
     FaltaReportes_DevEx,
{$ELSE}
     ZWizardBasico,
     ZetaBusqueda,
     FFormas,
     FListados,
     FImpresionRapida,
     FParametros,
     FDatosImport,
     FAltaReportes,
{$ENDIF}
     {$ifdef TRESS}
     FTressShell,
     {$endif}
     ZetaRegistryCliente,
     ZetaClientTools;

{$R *.DFM}

{$IFDEF TRESS_DELPHIXE5_UP}

procedure ShowFormaReporte( var Forma; EdicionClass: TBaseReportesClass_DevEx; const lReadOnly, lSoloImpresion: Boolean );
begin
     if ( TBaseReportes_DevEx( Forma ) = nil ) then
        TBaseReportes_DevEx( Forma ) := EdicionClass.Create( Application ) as TBaseReportes_DevEx;
     with TBaseReportes_DevEx( Forma ) do
     begin
          ReadOnly := lReadOnly;
          SoloImpresion := lSoloImpresion;
          ShowModal;
     end;
end;

procedure ShowPreviewReporte( var Forma; EdicionClass: TBaseReportesClass_DevEx;
                              const sFiltro: String = '';
                              const lImprimeForma: Boolean = FALSE );
begin
     if ( TBaseReportes_DevEx( Forma ) = nil ) then
        TBaseReportes_DevEx( Forma ) := EdicionClass.Create( Application ) as TBaseReportes_DevEx;
     with TBaseReportes_DevEx( Forma ) do
     begin
          {if lImprimeForma then
             Entidad := dmReportes.EntidadReporte;}
          FiltroEspecial := sFiltro;
          try
             Preview;
          finally
                 FiltroEspecial := VACIO;
          end;
     end;
end;

procedure ShowImpresionReporte( var Forma; EdicionClass: TBaseReportesClass_DevEx;
                                const sFiltro: String = '';
                                const lImprimeForma: Boolean = FALSE );
begin
     if ( TBaseReportes_DevEx( Forma ) = nil ) then
        TBaseReportes_DevEx( Forma ) := EdicionClass.Create( Application ) as TBaseReportes_DevEx;
     with TBaseReportes_DevEx( Forma ) do
     begin
          FiltroEspecial := sFiltro;
          try
             Imprime;
          finally
                 FiltroEspecial := VACIO;
          end;
     end;
end;

{$ELSE}

procedure ShowFormaReporte( var Forma; EdicionClass: TBaseReportesClass; const lReadOnly, lSoloImpresion: Boolean );
begin
     if ( TBaseReportes( Forma ) = nil ) then
        TBaseReportes( Forma ) := EdicionClass.Create( Application ) as TBaseReportes;
     with TBaseReportes( Forma ) do
     begin
          ReadOnly := lReadOnly;
          SoloImpresion := lSoloImpresion;
          ShowModal;
     end;
end;

procedure ShowPreviewReporte( var Forma; EdicionClass: TBaseReportesClass;
                              const sFiltro: String = '';
                              const lImprimeForma: Boolean = FALSE );
begin
     if ( TBaseReportes( Forma ) = nil ) then
        TBaseReportes( Forma ) := EdicionClass.Create( Application ) as TBaseReportes;
     with TBaseReportes( Forma ) do
     begin
          {if lImprimeForma then
             Entidad := dmReportes.EntidadReporte;}
          FiltroEspecial := sFiltro;
          try
             Preview;
          finally
                 FiltroEspecial := VACIO;
          end;
     end;
end;

procedure ShowImpresionReporte( var Forma; EdicionClass: TBaseReportesClass;
                                const sFiltro: String = '';
                                const lImprimeForma: Boolean = FALSE );
begin
     if ( TBaseReportes( Forma ) = nil ) then
        TBaseReportes( Forma ) := EdicionClass.Create( Application ) as TBaseReportes;
     with TBaseReportes( Forma ) do
     begin
          FiltroEspecial := sFiltro;
          try
             Imprime;
          finally
                 FiltroEspecial := VACIO;
          end;
     end;
end;

{$ENDIF}

{procedure OrdenarPor( DataSet: TClientDataset; const FieldName: String );
var
   sField, sIndex: String;
   lDescendente: TIndexOptions;
begin
     sField := FieldName+'Index';
     with DataSet do
     begin
          sIndex := IndexName;
          if sIndex <> '' then
          begin
               GetIndexInfo(sIndex);
               IndexDefs.Update;
               with IndexDefs.Find(sIndex) do
               begin
                    if (sField = sIndex) AND (Options = []) then
                       lDescendente := [ixDescending]
                    else lDescendente := [];
               end;
               DeleteIndex(sIndex);
          end
          else
              lDescendente := [];

          AddIndex( sField, FieldName, lDescendente);
          IndexName := sField;
          First;
     end;
end; }

{ ********** TdmReportes ********* }

{$ifdef DOS_CAPAS}
function TdmBaseReportes.GetServerReportes: TdmServerReportes;
begin
     Result := DCliente.dmCliente.ServerReportes;
end;
{$else}
function TdmBaseReportes.GetServerReportes: IdmServerReportesDisp;
begin
     Result := IdmServerReportesDisp(dmCliente.CreaServidor( {$ifdef RDD}CLASS_dmServerReporteadorDD{$ELSE}CLASS_dmServerReportes{$ENDIF}, FServidor ));
end;
{$endif}

procedure TdmBaseReportes.cdsReportesAlAdquirirDatos(Sender: TObject);
var
   lFiltraGetReportes: Boolean;

   procedure FiltraGetReportes;
   begin
        with TZetaClientDataSet(Sender) do
        begin
             IndexFieldNames:='';
             IndexName:='';
             Data := ServerReportes.GetReportes( dmCliente.Empresa,
                                            Ord( FClasifActivo ),
                                            Ord( crFavoritos ),
                                            Ord( crSuscripciones ),
                                            {$IFDEF VISITANTES} GetClasificacionesPermitidas {$ELSE}GetClasificaciones {$ENDIF},
                                            dmDiccionario.VerConfidencial );
             IndexFieldNames:='RE_CODIGO';
        end;
   end;

begin
     lFiltraGetReportes := TRUE;
     {$IFNDEF VISITANTES }
     {$IFNDEF WORKFLOWCFG }
     {$IFNDEF SELECCION }
     {$IFNDEF KIOSCOCFG }
     {$IFDEF TRESS_DELPHIXE5_UP}
     //Invoca Todos los reportes
      with TZetaClientDataSet(Sender) do
      begin
           IndexFieldNames:='';
           IndexName:='';
           Data := ServerReportes.GetReportesTodos( dmCliente.Empresa,
                                            GetClasificacionesPermitidas,
                                            dmDiccionario.VerConfidencial );
      end;
     lFiltraGetReportes := FALSE;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     if lFiltraGetReportes then
     begin
          //Invoca Favoritos o Suscripciones segun sea el caso
          FiltraGetReportes;
     end
end;

procedure TdmBaseReportes.cdsReportesAlAgregar(Sender: TObject);
begin
     cdsEditReporte.Conectar;
     cdsEditReporte.Append;
     FReporteActivo := 0;
{$IFDEF TRESS_DELPHIXE5_UP}
     ZCXWizardBasico.ShowWizard( TAltaReportes_DevEx );
{$ELSE}
     ZWizardBasico.ShowWizard( TAltaReportes );
{$ENDIF}
     if ( lHuboCambios ) then
        PosicionaConsulta;
end;
procedure TdmBaseReportes.GetFormaReporte( const eTipo : eTipoReporte );
begin
     FImprimeSubsecuentes := FALSE;
     FForzarImpresion := FALSE;
     //Cargando GetForma Reporte para las formas Nuevas
{$IFDEF TRESS_DELPHIXE5_UP}
      case eTipo of
           trListado : ShowFormaReporte( Listados_DevEx, TListados_DevEx, SoloLectura, SoloImpresion );
           trForma, trEtiqueta : ShowFormaReporte( Formas_DevEx, TFormas_DevEx, SoloLectura, SoloImpresion );
           //trPoliza : ShowFormaReporte( Poliza, TPoliza, SoloLectura, SoloImpresion ); //(@am): CAMBIO TEMPORAL
           trImpresionRapida : ShowFormaReporte( ImpresionRapida_DevEx, TImpresionRapida_DevEx, SoloLectura, SoloImpresion );
      end;
{$ELSE}
     case eTipo of
          trListado : ShowFormaReporte( Listados, TListados, SoloLectura, SoloImpresion );
          trForma, trEtiqueta : ShowFormaReporte( Formas, TFormas, SoloLectura, SoloImpresion );
          //trPoliza : ShowFormaReporte( Poliza, TPoliza, SoloLectura, SoloImpresion );
          trImpresionRapida : ShowFormaReporte( ImpresionRapida, TImpresionRapida, SoloLectura, SoloImpresion );
     end;
{$ENDIF}
     if FImprimeSubsecuentes then
{$IFDEF TRESS_DELPHIXE5_UP}
        ImpresionReportesSubsecuentes;
{$ELSE}
        ImpresionReportesSubsecuentesClasica;
{$ENDIF}
end;

procedure TdmBaseReportes.GetPreviewReporte( const eTipo: eTipoReporte; const sFiltro: String = ''; const lImprimeForma: Boolean = FALSE  );
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     //edit by MP: cargando preview de reporte con formas de devex
     case eTipo of
          trListado : ShowPreviewReporte( Listados_DevEx, TListados_DevEx {$ifdef ADUANAS}, sFiltro{$endif}{$ifdef CAJAAHORRO}, sFiltro{$endif}{$ifdef CAPTURACURSOS}, sFiltro{$endif});
          trForma : ShowPreviewReporte( Formas_DevEx, TFormas_DevEx, sFiltro, lImprimeForma );
          {$ifdef CAJAAHORRO}
          trImpresionRapida : ShowPreviewReporte( ImpresionRapida_DevEx, TImpresionRapida_DevEx, sFiltro, lImprimeForma );
          {$endif}
     end;
{$ELSE}
     case eTipo of
          trListado : ShowPreviewReporte( Listados, TListados {$ifdef ADUANAS}, sFiltro{$endif}{$ifdef CAJAAHORRO}, sFiltro{$endif});
          trForma : ShowPreviewReporte( Formas, TFormas, sFiltro, lImprimeForma );
          {$ifdef CAJAAHORRO}
          trImpresionRapida : ShowPreviewReporte( ImpresionRapida, TImpresionRapida, sFiltro, lImprimeForma );
          {$endif}
     end;
{$ENDIF}
end;

procedure TdmBaseReportes.GetImpresionReporte( const eTipo: eTipoReporte; const sFiltro: String = ''; const lImprimeForma: Boolean = FALSE  );
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     //edit by MP : Cargando el listado de edicion de reportes de DevEx
     case eTipo of
          trListado : ShowImpresionReporte( Listados_DevEx, TListados_DevEx );
          trForma : ShowImpresionReporte( Formas_DevEx, TFormas_DevEx, sFiltro, lImprimeForma );
     end;
{$ELSE}
     case eTipo of
          trListado : ShowImpresionReporte( Listados, TListados );
          trForma : ShowImpresionReporte( Formas, TFormas, sFiltro, lImprimeForma );
     end;
{$ENDIF}
end;

{$IFDEF TRESS_DELPHIXE5_UP}
procedure ShowResultadoReporte( var Forma; EdicionClass: TBaseReportesClass_DevEx;
                                const sFiltro: String = '';
                                const lSustituye: Boolean = TRUE;
                                const lImprimeForma: Boolean = FALSE );
begin
     if ( TBaseReportes_DevEx( Forma ) = nil ) then
        TBaseReportes_DevEx( Forma ) := EdicionClass.Create( Application ) as TBaseReportes_DevEx;
     with TBaseReportes_DevEx( Forma ) do
     begin
          FiltroEspecial := sFiltro;
          SustituyeFiltro := lSustituye;
          try
             GeneraResultado;
          finally
                 FiltroEspecial := VACIO;
          end;
     end;
end;
{$ELSE}
procedure ShowResultadoReporte( var Forma; EdicionClass: TBaseReportesClass;
                                const sFiltro: String = '';
                                const lSustituye: Boolean = TRUE;
                                const lImprimeForma: Boolean = FALSE );
begin
     if ( TBaseReportes( Forma ) = nil ) then
        TBaseReportes( Forma ) := EdicionClass.Create( Application ) as TBaseReportes;
     with TBaseReportes( Forma ) do
     begin
          FiltroEspecial := sFiltro;
          SustituyeFiltro := lSustituye;
          try
             GeneraResultado;
          finally
                 FiltroEspecial := VACIO;
          end;
     end;
end;
{$ENDIF}

procedure TdmBaseReportes.GetResultadoReporte( const eTipo: eTipoReporte; const sFiltro: String = ''; const lImprimeForma: Boolean = FALSE  );
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     case eTipo of
          trListado : ShowResultadoReporte( Listados_DevEx, TListados_DevEx, sFiltro, lImprimeForma );
          trForma : ShowResultadoReporte( Formas_DevEx, TFormas_DevEx, sFiltro, lImprimeForma );
     end;
{$ELSE}
     case eTipo of
          trListado : ShowResultadoReporte( Listados, TListados, sFiltro, lImprimeForma );
          trForma : ShowResultadoReporte( Formas, TFormas, sFiltro, lImprimeForma );
     end;
{$ENDIF}
end;


procedure TdmBaseReportes.GetResultadoIReporte( const iReporte: integer; const sFiltro: String = ''; const lSustituye: Boolean = TRUE;  const lImprimeForma: Boolean = FALSE );
begin
     FReporteActivo := iReporte;
     cdsEditReporte.Refrescar;
     GetResultadoReporte( eTipoReporte( cdsEditReporte.FieldByName('RE_TIPO').AsInteger ), sFiltro, lImprimeForma );
end;

function TdmBaseReportes.ReporteValido( const eEntidad: TipoEntidad ): Boolean;
begin
     Result := dmCliente.ModuloAutorizadoReportes( eEntidad );
end;

procedure TdmBaseReportes.cdsEditReporteAlModificar(Sender: TObject);
begin
     with cdsEditReporte do
     begin
          if ReporteValido( TipoEntidad( FieldByName( 'RE_ENTIDAD' ).AsInteger ) )  then
             GetFormaReporte( eTipoReporte( FieldByName( 'RE_TIPO' ).AsInteger ) )
     end;
end;

procedure TdmBaseReportes.cdsReportesAlModificar(Sender: TObject);
begin
     with cdsReportes do
     begin
          if ReporteValido( TipoEntidad( FieldByName( 'RE_ENTIDAD' ).AsInteger ) ) then
          begin
               if ( cdsEditReporte.State = dsBrowse ) then
                  cdsEditReporte.Active := FALSE;
               FReporteActivo := FieldByName( 'RE_CODIGO' ).AsInteger;
               GetFormaReporte( eTipoReporte( FieldByName( 'RE_TIPO' ).AsInteger ) );
               if ( lHuboCambios ) then
                  SincronizaDatos;
                  //PosicionaConsulta; //OLD
          end;
     end;
end;

procedure TdmBaseReportes.ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin

     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
              {$ifdef RDD}
              with Sender.DataSet do
                   if ( FindField('RE_TABLA') <> NIL ) then
                   begin
                        if StrLleno(FieldByName('RE_TABLA').AsString)then
                           Text := FieldByName('RE_TABLA').AsString
                        else
                            Text := Format( '< Tabla #%d >', [FieldByName('RE_ENTIDAD').AsInteger] );
                   end
                   else Text := Format( '< Tabla #%d >', [FieldByName('RE_ENTIDAD').AsInteger] );
              {$else}
              Text := ObtieneEntidad( TipoEntidad( Sender.AsInteger ) );
              {$endif}

     end
     else
         Text:= Sender.AsString;
end;

procedure TdmBaseReportes.ListaFijaEntidad( DataSet : TZetaClientDataSet);
var
   oCampo: TField;
begin
     oCampo := DataSet.FindField( 'RE_ENTIDAD' );
     if ( oCampo <> nil )  then
     begin
          with oCampo do
          begin
               OnGetText := ObtieneEntidadGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

procedure TdmBaseReportes.cdsReportesAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with TZetaClientDataSet(Sender) do
     begin
          MaskFecha('RE_FECHA');
          ListaFija('RE_TIPO', lfTipoReporte);
          ListaFijaEntidad( TZetaClientDataSet(Sender) );
          //CreateSimpleLookUp( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' ); //(@am): Ahora la descripcion se trae desde el serivdor.
          {$IFDEF TRESS_DELPHIXE5_UP}
          //DevEx: Campo agregado para mostrar la clasificacion de los reportes
          CreateCalculated( 'RE_CLASIFI_DESC', ftString, 50 );
          {$ENDIF}
     end;
end;

{$ifdef ver130}
procedure TdmBaseReportes.cdsReportesReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
procedure TdmBaseReportes.cdsReportesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$endif}
begin
     if ( ( E.ErrorCode = 1 ) and (Pos('DUPLICATE',UpperCase(E.Message))>0) )
        or ( UK_Violation( E ) )   then
        E.Message := 'El Nombre del Reporte est� Repetido';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

{ Edicion de Reportes }

procedure TdmBaseReportes.cdsEditReporteNewRecord(DataSet: TDataSet);
var
     aClasificacion: array [FALSE..TRUE] of Integer;
begin
     aClasificacion[FALSE]:= Ord( ClasifActivo );
     aClasificacion[TRUE]:= 0;
     with cdsEditReporte do
     begin
          Randomize;
          FieldByName( 'RE_TIPO' ).AsInteger := 0;
          // Cuando es una copia y esta en suscripciones o favoritos toma los valores de la copia.
          FieldByName( 'RE_CLASIFI' ).AsInteger := aClasificacion[ ChecaFavoritos or ChecaSuscripciones ];
           //  FieldByName( 'RE_CLASIFI' ).AsInteger := Ord( ClasifActivo );
          FieldByName( 'RE_NOMBRE' ).AsString := 'Reporte Nuevo ' + IntToStr( Random( 9999 ) );
          FieldByName( 'RE_TITULO' ).AsString := 'T�tulo ' + FieldByName( 'RE_NOMBRE' ).AsString;
          FieldByName( 'RE_VERTICA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'RE_SOLOT' ).AsString := K_GLOBAL_NO;
          FieldByName( 'RE_PFILE' ).AsInteger := Ord( tfImpresora );
          FieldByName( 'RE_REPORTE' ).AsString := K_TEMPLATE;
          FieldByName( 'RE_COPIAS' ).AsInteger := 1;
          FieldByName( 'RE_PRINTER' ).AsString := K_IMP_DEFAULT;
          FieldByName( 'RE_GENERAL' ).AsString := '';
          FieldByName( 'RE_COLNUM' ).AsInteger := 0;
          FieldByName( 'RE_MAR_SUP' ).AsInteger := 0;
          FieldByName( 'RE_ANCHO' ).AsInteger := 66;
          FieldByName( 'RE_RENESPA' ).AsInteger := 66;
          FieldByName( 'RE_COLESPA' ).AsInteger := 0;
          FieldByName( 'RE_ALTO' ).AsInteger := 0;
          FieldByName( 'RE_HOJA' ).AsInteger := 0;
          FieldByName( 'RE_CANDADO' ).AsInteger := Ord( cnAbierto );
          FieldByName('RE_FONTSIZ').AsInteger := 0;
     end;
end;

{***(@am): Se agrega el cursor de crHourGlass, para que
           al dar el Refresh el usuario sepa que el proceso
           de editar/agregar un reporte no ha terminadom pues
           falta mostrar la accion realizada ene l grid de reportes.***}
procedure TdmBaseReportes.PosicionaConsulta;
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with cdsReportes do
        begin
             Refrescar;
             Locate( 'RE_CODIGO', cdsEditReporte.FieldByname( 'RE_CODIGO' ).AsInteger, [] );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmBaseReportes.RealField(const FieldName: String): Boolean;
var
  findField: TField;
begin
  Result := false; {Posit failure}
  findField := nil;
  findField := cdsReportes.FindField(FieldName);
  Result := (Assigned(findField));
end;

procedure TdmBaseReportes.SincronizaDatos;
begin
     try
        with cdsReportes do
        begin
             DisableControls;
             dmSistema.cdsUsuariosLookUp.Conectar;
             if ( not (State in [dsEdit, dsInsert] )) then
                Edit;
             FieldByName('RE_CODIGO').AsInteger := cdsEditReporte.FieldByname( 'RE_CODIGO' ).AsInteger;
             FieldByName('RE_NOMBRE').AsString := cdsEditReporte.FieldByname( 'RE_NOMBRE' ).AsString;
             FieldByName('RE_TIPO').AsInteger := cdsEditReporte.FieldByname( 'RE_TIPO' ).AsInteger;
          {$ifdef RDD}
             FieldByName('RE_TABLA').AsString := cdsEditReporte.FieldByname( 'RE_TABLA' ).AsString;
           {$else}
             FieldByName('RE_ENTIDAD').AsString := cdsEditReporte.FieldByname( 'RE_ENTIDAD' ).AsString;
           {$endif}
             FieldByName('RE_FECHA').AsDateTime := cdsEditReporte.FieldByname( 'RE_FECHA' ).AsDateTime;
             FieldByName('US_CODIGO').AsString := cdsEditReporte.FieldByname( 'US_CODIGO' ).AsString;
             //(@am): En vs 2016 Cambia la forma de actualizar al usuario.
             if RealField('US_DESCRIP_SERVIDOR') then
                FieldByName('US_DESCRIP_SERVIDOR').AsString := dmSistema.cdsUsuariosLookUp.GetDescripcion(cdsEditReporte.FieldByname( 'US_CODIGO' ).AsString);
             FieldByName('RE_CLASIFI').AsString := cdsEditReporte.FieldByname( 'RE_CLASIFI' ).AsString;
             Post;
             MergeChangeLog;
        end;
     finally
            cdsReportes.EnableControls;
     end;
end;
procedure TdmBaseReportes.cdsEditReporteAlAdquirirDatos(Sender: TObject);
var
   CampoRep: OleVariant;
begin
     FPolizaFormula := '';
     FPolizaCuenta := '';
     lHuboCambios := FALSE;
     cdsEditReporte.Data := ServerReportes.GetEditReportes( dmCliente.Empresa, FReporteActivo, CampoRep );
     cdsCampoRep.Data := CampoRep; { Se esta simulando el Master-Detail }
     {$IFNDEF SUPERVISORES}
     dmSistema.GetReporteSuscrip( FReporteActivo );
     {$ENDIF}
     {$ifdef TRESS}
     FPuedeSuscribirse:= ZAccesosMgr.CheckDerecho(D_SUSCRIPCION_REPORTE,K_DERECHO_CONSULTA);
     FPuedeSuscribirUsuarios:= ZAccesosMgr.CheckDerecho(D_SUSCRIPCION_USUARIOS,K_DERECHO_CONSULTA);
     {$else}
     FPuedeSuscribirse:= dmDiccionario.VerConfidencial;
     FPuedeSuscribirUsuarios:= dmDiccionario.VerConfidencial;
     {$endif}
end;

//DevEx: Metodo invocado solo desde la vista actual
procedure TdmBaseReportes.BorraReporteDeFavoritos;
begin
     with cdsReportes do
     begin
          ServerReportes.BorraFavoritos( dmCliente.Empresa, FieldByName( 'RE_CODIGO' ).AsInteger );
          //Delete;
     end;
     cdsLookUpReportes.Close;
end;

procedure TdmBaseReportes.BorraReportesDeFavoritos (Codigos: TStringList);
var i:Integer;
begin
     //Recores lista
     For i:=0 to codigos.Count-1 do
     begin
          with cdsReportes do
          begin
               Locate( 'RE_CODIGO',strToInt(Codigos.Strings[i]), [] );
               ServerReportes.BorraFavoritos( dmCliente.Empresa, FieldByName( 'RE_CODIGO' ).AsInteger );
               //Delete;
          end;
          cdsLookUpReportes.Close;
     end;
end;

//DevEx: Metodo invocado solo desde la vista actual
procedure TdmBaseReportes.BorraReporteDeSuscripciones;
begin
     with cdsReportes do
     begin
          {$IFNDEF SUPERVISORES}
          dmSistema.BorraSuscripciones( FieldByName('RE_CODIGO').AsInteger );
          {$ENDIF}
          //Delete;
     end;
     cdsLookUpReportes.Close;
end;

procedure TdmBaseReportes.BorraReportesDeSuscripciones (Codigos: TStringList);
var i:Integer;
begin
     For i:=0 to codigos.Count-1 do
     begin
          with cdsReportes do
          begin
               Locate( 'RE_CODIGO',strToInt(Codigos.Strings[i]), [] );
               {$IFNDEF SUPERVISORES}
               dmSistema.BorraSuscripciones( FieldByName('RE_CODIGO').AsInteger );
               {$ENDIF}
               //Delete;
          end;
          cdsLookUpReportes.Close;
     end;
end;

//DevEx: Metodo invocado solo desde la vista actual
procedure TdmBaseReportes.BorraReporte_DevEx (Codigos: TStringList);
var
   i:Integer;
begin
     //Recorre lista
     For i:=0 to codigos.Count-1 do
     begin
          with cdsReportes do
          begin
               Locate( 'RE_CODIGO',strToInt(Codigos.Strings[i]), [] );
               ServerReportes.BorraReporte( dmCliente.Empresa, FieldByName( 'RE_CODIGO' ).AsInteger );
               //Delete;
          end;
          cdsLookUpReportes.Close;
     end;
end;

procedure TdmBaseReportes.BorraReporte;
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     with cdsReportes do
     begin
          ServerReportes.BorraReporte( dmCliente.Empresa, FieldByName( 'RE_CODIGO' ).AsInteger );
     end;
{$ELSE}
     with cdsReportes do
     begin
        if ( ChecaFavoritos ) then
             ServerReportes.BorraFavoritos( dmCliente.Empresa, FieldByName( 'RE_CODIGO' ).AsInteger )
        else if ( ChecaSuscripciones ) then
             {$IFNDEF SUPERVISORES}
             dmSistema.BorraSuscripciones( FieldByName('RE_CODIGO').AsInteger )
             {$ENDIF}
        else
            ServerReportes.BorraReporte( dmCliente.Empresa, FieldByName( 'RE_CODIGO' ).AsInteger );
     Delete;
     end;
{$ENDIF}
     cdsLookUpReportes.Close;
end;

function TdmBaseReportes.AgregaFavoritos( oDataSet : TDataSet ): boolean;
begin
     with oDataSet do
     begin
          Result:= True;
          if ( cdsReportes.FieldByName('US_FAVORITO').AsInteger = 0 ) then
          begin
               Result := ( ServerReportes.AgregaFavoritos( dmCliente.Empresa, FieldByname( 'RE_CODIGO' ).AsInteger ) > 0 );
               if Result then
               begin
                    with cdsReportes do
                    begin
                         if ( FieldByName('RE_CODIGO').AsInteger = oDataSet.FieldByName('RE_CODIGO').AsInteger ) then
                         begin
                              ModificaCampoReporte( 'US_FAVORITO', dmCliente.Usuario);
                         end;
                    end;
               end
               else
               begin
                    ZetaDialogo.ZError( 'Reportes', Format( 'Error al Agregar Reporte "%s" a Mis Favoritos', [ FieldByName( 'RE_NOMBRE' ).AsString ] ), 0 );
               end;
          end;
     end;
end;

function TdmBaseReportes.AgregaSuscripciones( oDataSet : TDataSet ): boolean;
begin
     {$IFDEF SUPERVISORES}
     Result:= False;
     {$ELSE}
     with oDataSet do
     begin
          Result := ( dmSistema.AgregaSuscripciones( dmCliente.Usuario, FieldByName('RE_CODIGO').AsInteger  ) );
          if Result then
             dmSistema.GrabaSuscrip;

          if ( cdsReportes.FieldByName('RE_CODIGO').AsInteger = FieldByName('RE_CODIGO').AsInteger ) then
          begin
               if ( cdsReportes.FieldByName('US_SUSCRITO').AsInteger = 0 ) then
               begin
                    if Result then
                    begin
                         ModificaCampoReporte('US_SUSCRITO', dmCliente.Usuario);
                    end
                    else
                    begin
                         ZetaDialogo.ZError( 'Reportes', Format( 'Error al Agregar Reporte "%s" a Mis Suscripciones', [ FieldByName( 'RE_NOMBRE' ).AsString ] ), 0 );
                    end;
               end;
          end;
     end;
     {$ENDIF}

end;

procedure TdmBaseReportes.ModificaCampoReporte( const sCampo: String; const iUsuario: Integer );
begin
     with cdsReportes do
     begin
          Edit;
          FieldByName(sCampo).AsInteger := iUsuario;
          Post;
          MergeChangeLog;
     end;
end;

function TdmBaseReportes.ChecaSuscripciones: boolean;
begin
     Result := ( ClasifActivo = crSuscripciones );
end;

function TdmBaseReportes.ChecaFavoritos: boolean;
begin
     Result := ( ClasifActivo = crFavoritos );
end;

procedure TdmBaseReportes.cdsEditReporteAlEnviarDatos(Sender: TObject);
begin
     if EsPoliza then
        GrabaPoliza
     else
         GrabaReporte;
end;

procedure TdmBaseReportes.GrabaReporte;
var
   ErrorCount, iReporte: Integer;
begin
     ErrorCount := 0;
     with cdsEditReporte do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if (ChangeCount > 0) OR
             (cdsCampoRep.ChangeCount>0)then
          begin
               lHuboCambios := TRUE;
               iReporte := FieldByName('RE_CODIGO').AsInteger ;
               Reconcile( ServerReportes.GrabaReporte( dmCliente.Empresa,
                                                       Delta,
                                                       cdsCampoRep.Delta,
                                                       ErrorCount,
                                                       iReporte) );
               if ErrorCount = 1 then Edit;
               if ( ErrorCount = 0 ) then
               begin
                    if ( FieldByName( 'RE_CODIGO' ).AsInteger <> iReporte ) then
                    begin
                         Edit;
                         FieldByName( 'RE_CODIGO' ).AsInteger := iReporte;
                         Post;
                         MergeChangeLog;

                         dmSistema.cdsSuscrip.First; //posicionar el cursor en el primer registro para recorrer todo el dataset

                         while NOT dmSistema.cdsSuscrip.EOF do
                         begin
                              dmSistema.cdsSuscrip.Edit;
                              dmSistema.cdsSuscrip.FieldByName( 'RE_CODIGO' ).AsInteger := iReporte;
                              dmSistema.cdsSuscrip.Post;
                              dmSistema.cdsSuscrip.Next;
                         end;
                    end;
                    dmSistema.GrabaSuscrip;
               end;
          end;
     end;
end;

function TdmBaseReportes.GrabaPoliza : Boolean;
var
   iReporte, ErrorCount : Integer;
   oCampoRep : OleVariant;
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Result := TRUE;
        ErrorCount := 0;
        with cdsEditReporte do
        begin
             if State in [ dsEdit, dsInsert ] then
                Post;
             if (ChangeCount > 0) OR
                (cdsCampoRep.ChangeCount>0)then
             begin
                lHuboCambios := TRUE;
                if (cdsCampoRep.ChangeCount>0) then
                   oCampoRep := cdsCampoRep.Delta
                else oCampoRep := NULL;
                iReporte := FieldByName('RE_CODIGO').AsInteger;
                Reconcile ( ServerReportes.GrabaPoliza( dmCliente.Empresa,
                                                        Delta, oCampoRep,
                                                        ErrorCount, iReporte ) );
                if (ErrorCount = 0) then
                begin
                     if (FieldByName('RE_CODIGO').AsInteger <> iReporte) then
                     begin
                          Edit;
                          FieldByName('RE_CODIGO').AsInteger := iReporte;
                          Post;
                          MergeChangeLog;
                          cdsLookupReportes.Close;
                     end;
                     cdsCamporep.MergeChangeLog;
                end
                else Result := FALSE;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmBaseReportes.cdsEditReporteAfterCancel(DataSet: TDataSet);
begin
     cdsCampoRep.CancelUpdates;
end;

procedure TdmBaseReportes.cdsEditReporteAlCrearCampos(Sender: TObject);
begin
     with cdsEditReporte do
     begin
          MaskFecha('RE_FECHA');
          ListaFija('RE_TIPO', lfTipoReporte);
          ListaFijaEntidad( cdsEditReporte );
          CreateSimpleLookUp( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

function TdmBaseReportes.EsPoliza : Boolean;
begin
     Result := eTipoReporte(cdsEditReporte.FieldByName('RE_TIPO').AsInteger) in [trPoliza,trPolizaConcepto];
end;

procedure TdmBaseReportes.cdsCampoRepFiltrosNewRecord(DataSet: TDataSet);
begin
     with cdsCampoRep do
     begin
          if EsPoliza then
          begin
               with FieldByName( 'CR_OPER' ) do
               begin
                    if NOT Assigned(OnGetText) then
                    begin
                         OnGetText := CR_OPERGetText;
                         Alignment := taLeftJustify;
                    end;
               end;
               FieldByName('CR_TITULO').AsString := FPolizaCuenta;
               FieldByName('CR_FORMULA').AsString := FPolizaFormula;
          end
          else FieldByName( 'CR_OPER' ).OnGetText := NIL;

          FieldByName('RE_CODIGO').AsInteger := cdsEditReporte.FieldByName('RE_CODIGO').AsInteger;
          FieldByName('CR_TIPO').AsInteger := 0;
          FieldByName('CR_POSICIO').AsInteger := -1;
          FieldByName('CR_SUBPOS').AsInteger := -1;
          FieldByName('CR_REQUIER').AsString := '';
          FieldByName('CR_CALC').AsInteger   := 0;
          FieldByName('CR_MASCARA').AsString := '';
          FieldByName('CR_ANCHO').AsInteger   := 0;
          FieldByName('CR_OPER').AsInteger    := 0;
          FieldByName('CR_TFIELD').AsInteger  := 0;
          FieldByName('CR_SHOW').AsInteger    := 0;
          FieldByName('CR_DESCRIP').AsString := '';
          FieldByName('CR_BOLD').AsString    := '';
          FieldByName('CR_ITALIC').AsString  := '';
          FieldByName('CR_SUBRAYA').AsString := '';
          FieldByName('CR_STRIKE').AsString  := '';
          FieldByName('CR_ALINEA').AsInteger  := 0;
          FieldByName('CR_COLOR').AsInteger   := 0;
     end;
end;

procedure TdmBaseReportes.cdsCampoRepFiltrosAfterDelete(DataSet: TDataSet);
begin
     with cdsEditReporte do
          if State = dsBrowse then Edit;
end;

procedure TdmBaseReportes.cdsEditReporteBeforePost(DataSet: TDataSet);
begin
     with cdsEditReporte do
     begin
          FieldByName('RE_FECHA').AsDateTime := Now;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          with FieldByName('RE_COPIAS') do
               AsInteger := iMax(AsInteger,1);
     end;
end;


procedure TdmBaseReportes.cdsLookupReportesAlAdquirirDatos(Sender: TObject);
begin
     cdsLookUpReportes.Data := ServerReportes.GetLookUpReportes(dmCliente.Empresa,dmDiccionario.VerConfidencial);
end;

procedure TdmBaseReportes.cdsLookupReportesAlModificar(Sender: TObject);
begin
     if ( cdsEditReporte.State = dsBrowse ) then
        cdsEditReporte.Active := FALSE;
     FReporteActivo := cdsLookupReportes.FieldByName('RE_CODIGO').AsInteger;
     GetFormaReporte( eTipoReporte(cdsLookUpReportes.FieldByName('RE_TIPO').AsInteger) )
end;

procedure TdmBaseReportes.cdsEscogeReporteAlCrearCampos(Sender: TObject);
begin
     with cdsEscogeReporte do
     begin
          MaskFecha('RE_FECHA');
          ListaFija('RE_TIPO', lfTipoReporte);
          ListaFijaEntidad( cdsEscogeReporte );
          CreateSimpleLookUp( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmBaseReportes.cdsLookupReportesGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     {GA:  Este LookupDataset no debe tener ningun derecho de Consulta, Alta, Baja o Cambio };
     lHasRights := False;
end;

procedure TdmBaseReportes.cdsEscogeReporteAlAdquirirDatos(Sender: TObject);
 {$ifdef ADUANAS}
 var
    eReporte: eTipoReporte;
    eEntidad: TipoEntidad;
    sTipos, sEntidades: string;
 {$ENDIF}
begin
     {$ifdef ADUANAS}
     if ( FListaReportes = [] ) AND ( FListaEntidades = [] ) then
        cdsEscogeReporte.Data := ServerReportes.GetEscogeReporte(dmCliente.Empresa,Ord(FEntidadReporte),Ord(FTipoReporte),dmDiccionario.VerConfidencial)
     else
     begin
          for eReporte := Low(eTipoReporte) to High(eTipoReporte) do
              if ( eReporte in FListaReportes) then
                 sTipos := ConcatString( sTipos, IntToStr(Ord(eReporte)) , ',' );

          for eEntidad := Low(TipoEntidad) to High(TipoEntidad) do
              if ( eEntidad in FListaEntidades) then
                 sEntidades := ConcatString( sEntidades, IntToStr(Ord(eEntidad)) , ',' );

          cdsEscogeReporte.Data := ServerReportes.GetEscogeReportes(dmCliente.Empresa,sEntidades,sTipos,dmDiccionario.VerConfidencial);
     end;
     {$else}
     cdsEscogeReporte.Data := ServerReportes.GetEscogeReporte(dmCliente.Empresa,Ord(FEntidadReporte),Ord(FTipoReporte),dmDiccionario.VerConfidencial)
     {$endif}
end;


procedure TdmBaseReportes.cdsEscogeReporteAlModificar(Sender: TObject);
begin
     if ( cdsEditReporte.State = dsBrowse ) then
        cdsEditReporte.Active := FALSE;
     SoloLectura := TRUE;
     FReporteActivo := cdsEscogeReporte.FieldByName('RE_CODIGO').AsInteger;
     GetFormaReporte( eTipoReporte(cdsEscogeReporte.FieldByName('RE_TIPO').AsInteger) );
     SoloLectura := FALSE;
end;

function TdmBaseReportes.EvaluaParams(SQLAgente:TSQLAgenteClient; var sError: WideString ) : Boolean;
 var oSQLAgente : OleVariant;
     FParamList : TZetaParams;
begin

     try
        FSQLAgente := SQLAgente;
        oSQLAgente := SQLAgente.AgenteToVariant;
        FParamList := TZetaParams.Create;

        dmCliente.CargaActivosTodos(FParamList);

        with FParamList do
             {$ifdef RDDAPP}
             AddBoolean( 'RDD_APP', TRUE );
             {$else}
             AddBoolean( 'RDD_APP', FALSE );
             {$endif}

        Result := ServidorEvaluaParam( oSQLAgente, FParamList.VarValues, sError );
        if Result then
           SQLAgente.VariantToAgente( oSQLAgente );

     except
           Result := FALSE;
     end;

     FreeAndNil(FParamList);
end;

{***(@am): Descomentar directiva AGENTE_DEBUG para validar los datos del agente antes de enviarlos al servidor**}
{.$define AGENTE_DEBUG}
function TdmBaseReportes.ConstruyeSQL( SQLAgente:TSQLAgenteClient;
                                       var sError: WideString;
                                       const FFiltroFormula, FFiltroDescrip : string;
                                       oParamList : TZetaParams;
                                       const oTipoPantalla : TipoPantalla;
                                       const lGeneraBitacora, lHayImagenes : Boolean) : Boolean;
 var oSQLAgente : OleVariant;
     //sCondicion : string;
     lSoloTotales : Boolean;
     oParams : OleVariant;
     {$IFDEF AGENTE_DEBUG}
        i: Integer;
        FDatos: TStrings;
     {$ENDIF}
begin
    {*********PRUEBA AGENTE**********}
    {$IFDEF AGENTE_DEBUG}
    FDatos := TStringList.Create;
    try
      with FDatos do begin
        Add('********* AGENTE **********');
        Add('');
        SQLAgente.AgenteToTextFile(FDatos);
        Add('');
        Add('****** FIN DE AGENTE ******');
        Add('');
        Add('******* PARAMETROS ********');
        Add('');
          with oParamList do begin
            for i := 0 to (Count - 1) do begin
              with Items[i] do begin
                case DataType of
                  ftString:
                    FDatos.Add(Format('%s = %s', [Name, AsString]));
                  ftWideString:
                    FDatos.Add(Format('%s = %s', [Name, AsWideString]));
                  ftInteger, ftSmallInt:
                    FDatos.Add(Format('%s = %d', [Name, AsInteger]));
                  ftDate, ftDateTime:
                    FDatos.Add(Format('%s = %s', [Name, FormatDateTime('dd/mmm/yyyy hh:nn AM/PM', AsDateTime)]));
                  ftFloat:
                    FDatos.Add(Format('%s = %n', [Name, AsFloat]));
                  ftBoolean:
                    FDatos.Add(Format('%s = %s', [Name, BoolToStr(AsBoolean)]));
                end;
              end;
            end;
          end;
        Add('');
        Add('*** FIN DE PARAMETROS *****');
        Add('');
        SaveToFile('D:\Temp\AgenteSQL.txt');
      end;
    finally
      FreeAndNil(FDatos);
    end;
    {$ENDIF}
    {********************************}

     FSQLAgente := SQLAgente;
     oSQLAgente := SQLAgente.AgenteToVariant;
     FParamList := oParamList;

     dmCliente.CargaActivosTodos(FParamList);

     {$IFDEF TEST_COMPONENTES}
     {***(@am): Valores activos fijos para prueba***}
     {oParamList.ParamValues['RegistroPatronal']:= '1';
       oParamList.ParamValues['IMSSYear']:= 2014;
       oParamList.ParamValues['IMSSMes']:= 6;
       oParamList.ParamValues['Year']:= 2014;
       oParamList.ParamValues['Tipo']:= 1;
       oParamList.ParamValues['Numero']:= 17;
       oParamList.ParamValues['FechaAsistencia']:= '22/07/2014';
       oParamList.ParamValues['YearDefault']:= 2014;
       oParamList.ParamValues['EmpleadoActivo']:= 8662; }
     {$ifdef TRESS}


     if ( Tressshell.FormaActiva <> NIL ) then
        Tressshell.FormaActiva.CargaParametrosReporte(FParamList);
     {$ENDIF}
     {$ENDIF}
     lSoloTotales := cdsEditReporte.FieldByName('RE_SOLOT').AsString = K_GLOBAL_SI;

     with cdsEditReporte do
          ZReportTools.ParametrosReportes( cdsEditReporte, dmCatalogos.cdsCondiciones,
                                           FParamList,
                                           FFiltroFormula,
                                           FFiltroDescrip,
                                           lSoloTotales,
                                           dmDiccionario.VerConfidencial,
                                           lGeneraBitacora,
                                           lHayImagenes );


     with cdsResultados do
     begin
          Active := False;
          while FieldCount > 0 do
                Fields[ FieldCount - 1 ].DataSet := nil;
          FieldDefs.Clear;
          //nos aseguramos que el 2do Resultado no se ordene como el primero
          IndexFieldNames :=''; //a lo mejor cambia por IndexName. 
     end;

     oParams := FParamList.VarValues;

     cdsResultados.Data := GeneraSQL( oSQLAgente, oParams, sError );
     Result := NOT cdsResultados.IsEmpty;

     if Result then
     begin
          {$ifdef CAROLINA}
          cdsResultados.SaveToFile(ExtractFileDir((Application.ExeName )) + '\Reportes.cds',dfBinary);
          {$endif}
          SQLAgente.VariantToAgente( oSQLAgente );
          if NOT lSoloTotales then
             SQLAgente.OrdenaDataset( cdsResultados );

          FParamList.VarValues := oParams;
          AfterGeneraSQL;
     end;
end;

procedure TdmBaseReportes.AfterGeneraSQL;
begin
end;

procedure TdmBaseReportes.cdsCampoRepFiltrosAlAdquirirDatos(Sender: TObject);
begin
     cdsCampoRepFiltros.Data := ServerReportes.CampoRepFiltros( dmCliente.Empresa, CodigoReporte );
end;

procedure TdmBaseReportes.CR_OPERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.AsInteger = 1 then
        Text:= 'Abono'
     else
        Text:= 'Cargo'
end;

procedure TdmBaseReportes.cdsCampoRepAfterPost(DataSet: TDataSet);
begin
     if EsPoliza AND
        ( eTipoCampo( cdsCampoRep.FieldByName('CR_TIPO').AsInteger ) = tcCampos ) then
     begin
          with cdsCampoRep do
          begin
               FPolizaFormula := FieldByName( 'CR_FORMULA' ).AsString;
               FPolizaCuenta := FieldByName( 'CR_TITULO' ).AsString;
          end;
     end;
end;

{***************************************************}
{********** Exportacion de Reportes ****************}
function TdmBaseReportes.Exporta( const sArchivo : string; var sError : string ) : Boolean;

 var oExpReporte : TAsciiFile;
     j : integer;
     sNombre, sDirDefault, sPlantilla, sPathPlantilla : string;
     eTipo : ETipoReporte;
     lAllImportar, lAllSustituir: Boolean;

 procedure AddTxt( sNames : string;
                   const sValues : string='');
 begin
      if sValues <> '' then sNames := sNames + '=' +sValues;
      with oExpReporte do
      begin
           RenglonBegin;
           AddCampo(sNames);
           RenglonEnd;
      end;
 end;

 procedure AgregaSeccion( DataSet : TDataSet; sSeccion : string );
  var i : integer;
 begin
      AddTxt(sSeccion);
      with DataSet do
      begin
           for i := 0 to FieldCount -1 do
           begin
                if ( Fields[i].FieldKind = fkData ) then   // Debe existir en la BD, no incluir calculados, lookups, etc.
                begin
                     case Fields[i].DataType of
                          ftDate, ftDateTime: AddTxt( Fields[i].FieldName, FormatDateTime('mm/dd/yyyy', Fields[i].AsDateTime) );
                          ftString: AddTxt( Fields[i].FieldName, EntreComillas(BorraCReturn(Fields[i].AsString)) )
                          else AddTxt( Fields[i].FieldName, Fields[i].AsString );
                     end;
                end;
           end;
      end;
 end;

 function Confirma : Boolean;
  const K_EXPORTA = 'Exportaci�n de Reportes';
  var sDestino : string;

  procedure ExportaPlantilla;
   procedure CopiaPlantilla;
   begin
        CopyFile({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(ExtractFilePath(sPathPlantilla)+sPlantilla),{$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sDestino),FALSE);
   end;
  begin
       sDestino := VerificaDir(ExtractFileDir(sArchivo))+sPlantilla;
       if (not lAllSustituir) then
       begin
            if FileExists(sDestino) then
               case ZYesToAll( K_EXPORTA, 'La plantilla  ' + Comillas(sDestino) + '  ya existe,'+CR_LF+  '�Desea sustituirla? ',0,mbNo ) of
                    mrYes : CopiaPlantilla;
                    mrAll: lAllSustituir := TRUE;
               end
            else CopiaPlantilla;
       end
       else CopiaPlantilla;

  end;
 begin
      Result := TRUE;
      if (ExtractFilePath(sPathPlantilla) <>
         ExtractFilePath(sArchivo)) then
         begin
              if NOT lAllImportar then
                 {case Dialogs.MessageDlg( 'El reporte  '+ Comillas(sNombre)+'  requiere la plantilla:  ' +
                                          Comillas(sPlantilla) + CR_LF + '�Desea Exportarla?',
                                          mtConfirmation,
                                          [mbYes, mbNo, mbYesToAll],0) of}
                 case ZetaDialogo.ZYesToAll( K_EXPORTA,
                                          'El reporte  '+ Comillas(sNombre)+'  requiere la plantilla:  ' +
                                          Comillas(sPlantilla) + CR_LF + '�Desea Exportarla?',
                                          0, mbYes ) of
                      mrYes: ExportaPlantilla;
                      mrAll :
                      begin
                           ExportaPlantilla;
                           lAllImportar := TRUE;
                      end;
                 end
              else ExportaPlantilla;
         end
         else
         begin
              if FileExists(sPathPlantilla) then
                 sError:= 'El reporte  '+ Comillas(sNombre)+'  requiere la plantilla:  ' + CR_LF +
                          Comillas(sPlantilla) + CR_LF +
                          'Que se encuentra en: ' + Comillas(ExtractFilePath(sArchivo))
              else
                  sError := 'El reporte  '+ Comillas(sNombre)+'  requiere la plantilla:  ' + CR_LF +
                            Comillas(sPlantilla) + CR_LF +
                            'Nota: Esta plantilla no existe en ' + Comillas(ExtractFilePath(sArchivo))
         end
 end;
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Result := TRUE;
        lAllImportar := FALSE;
        lAllSustituir := FALSE;

        with cdsEditReporte do
        begin
             FReporteActivo := cdsReportes.FieldByName('RE_CODIGO').AsInteger;
             Close;
             Conectar;

             sNombre := FieldByName('RE_NOMBRE').AsString;
             sPathPlantilla := UPPERCASE(FieldByName('RE_REPORTE').AsString);
             sDirDefault := zReportTools.DirPlantilla;

             if ExtractFilePath(sPathPlantilla) = '' then
                sPathPlantilla := sDirDefault  + sPathPlantilla;
             if ExtractFileExt( sPathPlantilla)='' then
                sPathPlantilla := sPathPlantilla+K_TEMPLATE_EXT;

             sPlantilla := ExtractFileName(sPathPlantilla);

             eTipo := eTipoReporte(FieldByName('RE_TIPO').AsInteger);
             case eTipo of
                  trListado :
                  if eTipoFormato(FieldByName('RE_PFILE').AsInteger) =tfImpresora  then
                  begin
                       if (Trim(sPlantilla)=K_TEMPLATE ) OR
                          (Trim(sPlantilla)=K_TEMPLATE+'.QR2' ) then
                          sPlantilla := K_TEMPLATE+'.QR2'
                       else Result := Confirma
                  end
                  else sPlantilla := K_NO_REQUIERE;

                  trForma: Result := Confirma;

                  else sPlantilla := K_NO_REQUIERE;
             end;
        end;
        if Result then
        begin
             oExpReporte := TAsciiFile.Create(sArchivo);
             try
                with oExpReporte do
                begin
                     Separador := '';
                     Delimitador := '';
                end;
                AddTxt( K_GENERAL );
                AddTxt( 'EMPRESA', dmCliente.Compania + ':' + dmCliente.GetDatosEmpresaActiva.Nombre );
                AddTxt( 'USUARIO', dmCliente.GetDatosUsuarioActivo.Nombre );
                AddTxt( 'NOMBRE', sNombre );
                AddTxt( 'TIPO', cdsEditReporte.FieldByName('RE_TIPO').AsString );
                AddTxt( 'TABLA', cdsEditReporte.FieldByName('RE_ENTIDAD').AsString );
                AddTxt( 'FECHA', FormatDateTime('dd/mmm/yyyy hh:nn',NOW) );
                AddTxt( 'PLANTILLA', sPlantilla );

                AgregaSeccion(cdsEditReporte,K_REPORTE);

                j:=1;
                with cdsCampoRep do
                     while NOT EOF do
                     begin
                          AgregaSeccion(cdsCampoRep,'[DETALLE'+ PadLCar(IntToStr(j),5,'0')+']' );
                          Inc(j);
                          Next;
                     end;
                AddTxt('[TOTAL_DETALLE]');
                AddTxt( 'TOTAL', IntToStr(j-1) );
             finally
                    oExpReporte.Free;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmBaseReportes.Importa_DevEx( const sArchivo: String; var sError: String ): Boolean;
 const K_ERROR = 'ERROR';
       K_NO_VALIDO = 'El archivo de importaci�n no es v�lido';
       K_IMPORTA = 'Importaci�n de Reportes';

 var oEncabezado,oImpReporte : TStringList;
     {$IFDEF TRESS_DELPHIXE5_UP}
     oDatosImport : TDatosImport_DevEx;
     {$ELSE}
     oDatosImport : TDatosImport;
     {$ENDIF}
     iCount : integer;

 function LeeSeccion( var i : integer;
                      DataSet : TDataSet ): Boolean;
  var sRenglon,sField,sFormat,sValue : string;
      IgualPos : integer;

  function EsCorchete : Boolean;
  begin
       Result := Copy(oImpReporte[i],1,1)='[' ;
  end;
  function QuitaComillas : string;
  begin
       Result := sValue;
       if Copy(Result,1,1) = UnaCOMILLA then
       begin
            Result := Copy(Result,2,Length(Result));
            if Copy(Result,Length(Result),1) = UnaCOMILLA then
               Result := Copy(Result,1,Length(Result)-1);
       end;
  end;
  var oField : TField;
 begin
      Result := TRUE;
      if EsCorchete then
         i := i+1;

      while (i<iCount) AND (NOT EsCorchete) do
      begin
           with DataSet do
           begin
                sRenglon := oImpReporte[i];
                if StrLleno(sRenglon) then
                begin
                     IgualPos := Pos('=',sRenglon);
                     sField := Copy(sRenglon,1,IgualPos-1);
                     sValue := Copy(sRenglon,IgualPos+1,Length(sRenglon));
                     oField := FindField(sField);
                     if oField <> NIL then
                     begin
                          with oField do
                          begin
                               if DataType in [ftDate, ftDateTime] then
                               try
                                  sFormat := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat;
                                  {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat := 'mm/dd/yyyy';
                                  AsString := sValue;
                               finally
                                      {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat:=sFormat;
                               end
                               else if oField is TNumericField then
                                    AsString := sValue
                               else AsString := QuitaComillas;
                          end;
                     end
                     else Result := FALSE; 
                end;
                i := i+1;
           end;
      end;
 end;
 function ImportaPlantilla : Boolean;
  var sFuente, sDestino : string;
 begin
      Result := TRUE;

      if oDatosImport.ImportarPlantilla then
      begin
           sFuente := VerificaDir(ExtractFilePath(sArchivo));
           sDestino := zReportTools.DirPlantilla;
           {if NOT FileExists( sFuente + oDatosImport.Plantilla) then
           begin
                sFuente := sFuente + oDatosImport.Plantilla;
                Result := zConfirm( K_IMPORTA, 'La plantilla  ' + Comillas(sFuente) + ' no existe,'+CR_LF+  '�Desea continuar con la Importaci�n? ',0,mbNo );
           end
           else}
           if sFuente <> sDestino then
           begin
                sFuente := sFuente + oDatosImport.Plantilla;
                sDestino := sDestino + oDatosImport.Plantilla;
                if FileExists(sDestino) then
                   case ZSiNoCancel( K_IMPORTA, 'La Plantilla  ' + Comillas(sDestino) + '  Ya Existe,'+CR_LF+  '�Desea Sustituirla?',0,mbNo ) of
                        mrYes : CopyFile({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sFuente),{$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sDestino),FALSE);
                        mrCancel : Result := FALSE;
                   end
                else CopyFile({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sFuente),{$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sDestino),FALSE);
           end;
      end;
 end;

  var i{ j {, iReporte} : integer;
      oCursor : TCursor;
      sFuente : string;
      lExistePlantilla : Boolean;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     oImpReporte := TStringList.Create;
     oEncabezado := TStringList.Create;
     try
        oImpReporte.LoadFromFile(sArchivo);
        iCount := oImpReporte.Count;
        if Pos('TOTAL=',UpperCase(oImpReporte[iCount-1]))>0 then
           oImpReporte.Delete(iCount-1);
        iCount := oImpReporte.Count;
        if Pos('[TOTAL_DETALLE]',UpperCase(oImpReporte[iCount-1]))>0  then
           oImpReporte.Delete(iCount-1);
        iCount := oImpReporte.Count;
        {$IFDEF TRESS_DELPHIXE5_UP}
        oDatosImport := TDatosImport_DevEx.Create( self );
        {$ELSE}
        oDatosImport := TDatosImport.Create( self );
        {$ENDIF}
        if oImpReporte[0] = K_GENERAL then i :=1
        else i :=0;

        while oImpReporte[i] <> K_REPORTE DO
        begin
             oEncabezado.Add(oImpReporte[i]);
             i := i+1;
        end;
        oEncabezado.Add('RE_CLASIFI='+oImpReporte.Values['RE_CLASIFI']);
        with oEncabezado do
        begin
             sError := Values['NOMBRE'];
             if sError = '' then
             begin
                  Result := FALSE;
                  sError := K_NO_VALIDO;
                  Exit;
             end;
             with oDatosImport do
             begin
                  Empresa:= Values['EMPRESA'];
                  Usuario := Values['USUARIO'];
                  Fecha := Values['FECHA'];
                  Nombre := sError;
                  Tipo := ObtieneElemento(lfTipoReporte,StrToInt(Values['TIPO']));
                  Tabla := ObtieneEntidad(TipoEntidad(StrToInt(Values['TABLA'])));
                  NombreNuevo := 'IMP: '+sError;
                  Plantilla := Values['PLANTILLA'];
                  Clasificacion := eClasifiReporte(StrToInt(Values['RE_CLASIFI']));
                  ShowModal;
                  Result := ModalResult = mrOk;
             end;
        end;

        if ( oDatosImport.ImportarPlantilla ) AND
           ( oDatosImport.Plantilla <> K_NO_REQUIERE )  AND
           ( oDatosImport.Plantilla <> K_TEMPLATE+'.QR2' ) then
        begin
             sFuente := VerificaDir(ExtractFilePath(sArchivo));
             lExistePlantilla := FileExists( sFuente + oDatosImport.Plantilla);
             if NOT lExistePlantilla then
                Result := zConfirm( K_IMPORTA, 'La Plantilla  ' + Comillas(sFuente + oDatosImport.Plantilla) + ' No Existe,'+CR_LF+  '�Desea Continuar con la Importaci�n? ',0,mbNo );
        end
        else
            lExistePlantilla := FALSE;

        if Result then
        begin
             with cdsEditReporte do
             begin
                  Active := FALSE;
                  Conectar;
                  Append;
                  LeeSeccion(i,cdsEditReporte);
                  if Result then
                  begin
                       if NOT ExisteEntidad( TipoEntidad( FieldByName('RE_ENTIDAD').AsInteger ) ) then
                       begin
                            ZError(K_IMPORTA, ObtieneEntidad(TipoEntidad( FieldByName('RE_ENTIDAD').AsInteger )) + CR_LF +
                                              'El reporte no se puede importar',0);
                            Abort;
                       end;
                       FieldByName('RE_CODIGO').AsInteger := 0;
                       FieldByName('RE_NOMBRE').AsString := oDatosImport.NombreNuevo;
                       FieldByName('RE_FECHA').AsDateTime := DATE;
                       FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
                       if ExisteClasificacion(oDatosImport.Clasificacion) then
                          FieldByName('RE_CLASIFI').AsInteger := Ord(oDatosImport.Clasificacion)
                       else
                       begin
                            FieldByName('RE_CLASIFI').AsInteger := Ord( FClasifActivo );
                            ZetaDialogo.ZInformation( K_IMPORTA,
                                                      Format( 'La Clasificaci�n #%d no existe. ' + CR_LF +
                                                              'El reporte se importar� en la Clasificaci�n %s',
                                                              [Ord(oDatosImport.Clasificacion),ObtieneClasificacion(FClasifActivo) ]), 0);
                       end;
                       Post;

                       cdsCampoRep.EmptyDataSet;
                       cdsCampoRep.Conectar;
                       while i < iCount do
                       begin
                            cdsCampoRep.Append;
                            if LeeSeccion(i,cdsCampoRep) then
                               cdsCampoRep.Post
                            else cdsCampoRep.Cancel;
                       end;

                       with dmSistema.cdsSuscrip do
                       begin
                            if Active then
                               EmptyDataSet;               // Las suscripciones no vienen en el archivo a importar
                       end;

                       Enviar;
                       PosicionaConsulta;
                       Result := FieldByName('RE_CODIGO').AsInteger <> 0;

                       if Result then
                       begin
                            if lExistePlantilla then
                               ImportaPlantilla
                       end
                       else sError := '';
                  end;
             end
        end
        else sError := '';
     finally
            FreeAndNil( oEncabezado );
            FreeAndNil( oImpReporte );
            Screen.Cursor := oCursor;
     end;
end;

function TdmBaseReportes.Importa( const sArchivo : string; var sError : string ) : Boolean;
 const K_ERROR = 'ERROR';
       K_NO_VALIDO = 'El archivo de importaci�n no es v�lido';
       K_IMPORTA = 'Importaci�n de Reportes';

 var oEncabezado,oImpReporte : TStringList;
     {$IFDEF TRESS_DELPHIXE5_UP}
     oDatosImport : TDatosImport_DevEx;
     {$ELSE}
     oDatosImport : TDatosImport;
     {$ENDIF}
     iCount : integer;

 function LeeSeccion( var i : integer;
                      DataSet : TDataSet ): Boolean;
  var sRenglon,sField,sFormat,sValue : string;
      IgualPos : integer;

  function EsCorchete : Boolean;
  begin
       Result := Copy(oImpReporte[i],1,1)='[' ;
  end;
  function QuitaComillas : string;
  begin
       Result := sValue;
       if Copy(Result,1,1) = UnaCOMILLA then
       begin
            Result := Copy(Result,2,Length(Result));
            if Copy(Result,Length(Result),1) = UnaCOMILLA then
               Result := Copy(Result,1,Length(Result)-1);
       end;
  end;
  var oField : TField;
 begin
      Result := TRUE;
      if EsCorchete then
         i := i+1;

      while (i<iCount) AND (NOT EsCorchete) do
      begin
           with DataSet do
           begin
                sRenglon := oImpReporte[i];
                if StrLleno(sRenglon) then
                begin
                     IgualPos := Pos('=',sRenglon);
                     sField := Copy(sRenglon,1,IgualPos-1);
                     sValue := Copy(sRenglon,IgualPos+1,Length(sRenglon));
                     oField := FindField(sField);
                     if oField <> NIL then
                     begin
                          with oField do
                          begin
                               if DataType in [ftDate, ftDateTime] then
                               try
                                  sFormat := {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat;
                                  {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat := 'mm/dd/yyyy';
                                  AsString := sValue;
                               finally
                                      {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat:=sFormat;
                               end
                               else if oField is TNumericField then
                                    AsString := sValue
                               else AsString := QuitaComillas;
                          end;
                     end
                     else Result := FALSE; 
                end;
                i := i+1;
           end;
      end;
 end;
 function ImportaPlantilla : Boolean;
  var sFuente, sDestino : string;
 begin
      Result := TRUE;

      if oDatosImport.ImportarPlantilla then
      begin
           sFuente := VerificaDir(ExtractFilePath(sArchivo));
           sDestino := zReportTools.DirPlantilla;
           {if NOT FileExists( sFuente + oDatosImport.Plantilla) then
           begin
                sFuente := sFuente + oDatosImport.Plantilla;
                Result := zConfirm( K_IMPORTA, 'La plantilla  ' + Comillas(sFuente) + ' no existe,'+CR_LF+  '�Desea continuar con la Importaci�n? ',0,mbNo );
           end
           else}
           if sFuente <> sDestino then
           begin
                sFuente := sFuente + oDatosImport.Plantilla;
                sDestino := sDestino + oDatosImport.Plantilla;
                if FileExists(sDestino) then
                   case ZSiNoCancel( K_IMPORTA, 'La Plantilla  ' + Comillas(sDestino) + '  Ya Existe,'+CR_LF+  '�Desea Sustituirla?',0,mbNo ) of
                        mrYes : CopyFile({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sFuente),{$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sDestino),FALSE);
                        mrCancel : Result := FALSE;
                   end
                else CopyFile({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sFuente),{$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sDestino),FALSE);
           end;
      end;
 end;

  var i{ j {, iReporte} : integer;
      oCursor : TCursor;
      sFuente : string;
      lExistePlantilla : Boolean;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     oImpReporte := TStringList.Create;
     oEncabezado := TStringList.Create;
     try
        oImpReporte.LoadFromFile(sArchivo);
        iCount := oImpReporte.Count;
        if Pos('TOTAL=',UpperCase(oImpReporte[iCount-1]))>0 then
           oImpReporte.Delete(iCount-1);
        iCount := oImpReporte.Count;
        if Pos('[TOTAL_DETALLE]',UpperCase(oImpReporte[iCount-1]))>0  then
           oImpReporte.Delete(iCount-1);
        iCount := oImpReporte.Count;
        {$IFDEF TRESS_DELPHIXE5_UP}
        oDatosImport := TDatosImport_DevEx.Create( self );
        {$ELSE}
        oDatosImport := TDatosImport.Create( self );
        {$ENDIF}

        if oImpReporte[0] = K_GENERAL then i :=1
        else i :=0;

        while oImpReporte[i] <> K_REPORTE DO
        begin
             oEncabezado.Add(oImpReporte[i]);
             i := i+1;
        end;
        oEncabezado.Add('RE_CLASIFI='+oImpReporte.Values['RE_CLASIFI']);
        with oEncabezado do
        begin
             sError := Values['NOMBRE'];
             if sError = '' then
             begin
                  Result := FALSE;
                  sError := K_NO_VALIDO;
                  Exit;
             end;
             with oDatosImport do
             begin
                  Empresa:= Values['EMPRESA'];
                  Usuario := Values['USUARIO'];
                  Fecha := Values['FECHA'];
                  Nombre := sError;
                  Tipo := ObtieneElemento(lfTipoReporte,StrToInt(Values['TIPO']));
                  Tabla := ObtieneEntidad(TipoEntidad(StrToInt(Values['TABLA'])));
                  NombreNuevo := 'IMP: '+sError;
                  Plantilla := Values['PLANTILLA'];
                  Clasificacion := eClasifiReporte(StrToInt(Values['RE_CLASIFI']));
                  ShowModal;
                  Result := ModalResult = mrOk;
             end;
        end;

        if ( oDatosImport.ImportarPlantilla ) AND
           ( oDatosImport.Plantilla <> K_NO_REQUIERE )  AND
           ( oDatosImport.Plantilla <> K_TEMPLATE+'.QR2' ) then
        begin
             sFuente := VerificaDir(ExtractFilePath(sArchivo));
             lExistePlantilla := FileExists( sFuente + oDatosImport.Plantilla);
             if NOT lExistePlantilla then
                Result := zConfirm( K_IMPORTA, 'La Plantilla  ' + Comillas(sFuente + oDatosImport.Plantilla) + ' No Existe,'+CR_LF+  '�Desea Continuar con la Importaci�n? ',0,mbNo );
        end
        else
            lExistePlantilla := FALSE;

        if Result then
        begin
             with cdsEditReporte do
             begin
                  Active := FALSE;
                  Conectar;
                  Append;
                  LeeSeccion(i,cdsEditReporte);
                  if Result then
                  begin
                       if NOT ExisteEntidad( TipoEntidad( FieldByName('RE_ENTIDAD').AsInteger ) ) then
                       begin
                            ZError(K_IMPORTA, ObtieneEntidad(TipoEntidad( FieldByName('RE_ENTIDAD').AsInteger )) + CR_LF +
                                              'El reporte no se puede importar',0);
                            Abort;
                       end;
                       FieldByName('RE_CODIGO').AsInteger := 0;
                       FieldByName('RE_NOMBRE').AsString := oDatosImport.NombreNuevo;
                       FieldByName('RE_FECHA').AsDateTime := DATE;
                       FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
                       if ExisteClasificacion(oDatosImport.Clasificacion) then
                          FieldByName('RE_CLASIFI').AsInteger := Ord(oDatosImport.Clasificacion)
                       else
                       begin
                            FieldByName('RE_CLASIFI').AsInteger := Ord( FClasifActivo );
                            ZetaDialogo.ZInformation( K_IMPORTA,
                                                      Format( 'La Clasificaci�n #%d no existe. ' + CR_LF +
                                                              'El reporte se importar� en la Clasificaci�n %s',
                                                              [Ord(oDatosImport.Clasificacion),ObtieneClasificacion(FClasifActivo) ]), 0);
                       end;
                       Post;

                       cdsCampoRep.EmptyDataSet;
                       cdsCampoRep.Conectar;
                       while i < iCount do
                       begin
                            cdsCampoRep.Append;
                            if LeeSeccion(i,cdsCampoRep) then
                               cdsCampoRep.Post
                            else cdsCampoRep.Cancel;
                       end;

                       with dmSistema.cdsSuscrip do
                       begin
                            if Active then
                               EmptyDataSet;               // Las suscripciones no vienen en el archivo a importar
                       end;

                       Enviar;
                       PosicionaConsulta;
                       Result := FieldByName('RE_CODIGO').AsInteger <> 0;

                       if Result then
                       begin
                            if lExistePlantilla then
                               ImportaPlantilla
                       end
                       else sError := '';
                  end;
             end
        end
        else sError := '';
     finally
            FreeAndNil( oEncabezado );
            FreeAndNil( oImpReporte );
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmBaseReportes.cdsImprimeFormaAlAdquirirDatos(Sender: TObject);
begin
     cdsImprimeForma.Data := ServerReportes.GetEscogeReporte(dmCliente.Empresa,Ord(FEntidadReporte),Ord(FTipoReporte),dmDiccionario.VerConfidencial);
end;

procedure TdmBaseReportes.cdsImprimeFormaAlCrearCampos(Sender: TObject);
begin
     with cdsImprimeForma do
     begin
          MaskFecha('RE_FECHA');
          ListaFija('RE_TIPO', lfTipoReporte);
          ListaFijaEntidad( cdsImprimeForma );
          CreateSimpleLookUp( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmBaseReportes.cdsImprimeFormaAlModificar(Sender: TObject);
begin
     if ( cdsImprimeForma.State = dsBrowse ) then
        cdsImprimeForma.Active := FALSE;
     FReporteActivo := cdsImprimeForma.FieldByName('RE_CODIGO').AsInteger;

     GetPreviewReporte( trForma );
end;

procedure TdmBaseReportes.ImprimeUnaForma( const sFiltro : string );
begin
     FReporteActivo := cdsEscogeReporte.FieldByName('RE_CODIGO').AsInteger;
     cdsEditReporte.Refrescar;
     {$ifdef ADUANAS}
     GetPreviewReporte( eTipoReporte(cdsEscogeReporte.FieldByName('RE_TIPO').AsInteger), sFiltro, TRUE );
     {$else}
     GetPreviewReporte( trForma, sFiltro, TRUE );
     {$endif}
end;

procedure TdmBaseReportes.ImprimeUnaForma( const sFiltro : string; const iReporte: integer; const eSalida : TipoPantalla );
begin
     FReporteActivo := iReporte;
     cdsEditReporte.Refrescar;
     if ( eSalida = tgPreview ) then
        {$ifdef CAJAAHORRO}
        GetPreviewReporte( eTipoReporte(cdsEditReporte.FieldByName('RE_TIPO').AsInteger), sFiltro, TRUE )
        {$else}
               {$ifdef CAPTURACURSOS}
               GetPreviewReporte( eTipoReporte(cdsEditReporte.FieldByName('RE_TIPO').AsInteger), sFiltro, TRUE )
               {$else}
               GetPreviewReporte( trForma, sFiltro, TRUE )
               {$endif}
        {$endif}
     else
        {$ifdef CAJAAHORRO}
        GetPreviewReporte( eTipoReporte(cdsEditReporte.FieldByName('RE_TIPO').AsInteger), sFiltro, TRUE );
        {$else}
               {$ifdef CAPTURACURSOS}
               GetPreviewReporte( eTipoReporte(cdsEditReporte.FieldByName('RE_TIPO').AsInteger), sFiltro, TRUE );
               {$else}
               GetImpresionReporte( trForma, sFiltro, TRUE );
               {$endif}
        {$endif}
end;



procedure TdmBaseReportes.cdsLookupReportesAlAgregar(Sender: TObject);
begin
     ShowMessage('no');
end;

procedure TdmBaseReportes.cdsLookupReportesLookupSearch(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
begin
     with Sender do
     begin
          if NOT Filtered then
          begin
               {$ifdef CAPTURACURSOS}
               Filter := 'RE_TIPO IN( 0, 1 )';
               {$else}
               Filter := 'RE_TIPO IN( 3,4 )';
               {$endif}
               Filtered := TRUE;
          end;
          lOk := NOT IsEmpty;
          Filtered := FALSE;
     end;

     if lOk then
        lOk := {$IFDEF TRESS_DELPHIXE5_UP}ZetaBusqueda_DevEx{$ELSE}ZetaBusqueda{$ENDIF}.ShowSearchForm( Sender, sFilter, sKey, sDescription )
     else ZInformation('B�squeda de P�lizas','No se han Definido P�lizas en el Reporteador',0);
end;

procedure TdmBaseReportes.cdsCampoRepAlCrearCampos(Sender: TObject);
begin
     if EsPoliza then
     begin
          //Aplica Solamente para Polizas Contables
          with cdsCampoRep.FieldByName( 'CR_OPER' ) do
          begin
               OnGetText := CR_OPERGetText;
               Alignment := taLeftJustify;
          end;
     end
     else cdsCampoRep.FieldByName( 'CR_OPER' ).OnGetText := NIL;
end;

{function TdmBaseReportes.GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;
begin
     Result := 0;
end;}

procedure TdmBaseReportes.AgregaSQLColumnasParam( oSQLAgente : TSQLAgenteClient;
                                                  oParam : TCampoMaster);
 var sFormula : string;
begin
     sFormula := oParam.Formula;
     if sFormula>'' then
     begin
          oParam.PosAgente := oSQLAgente.AgregaColumna( sFormula,
                                                        FALSE,
                                                        enNinguno,
                                                        oParam.TipoCampo,
                                                        0 );
     end
     else oParam.PosAgente := -1;
end;

 { Si la implementacion de DBaseReportes.EvaluaParametros Cambia,
 tambien debe cambiar la implementacion de TDummyReportes.EvaluaParametros que se encuentra
 en \Designer\Diccionario
 El cambio se debe de probar en todos los modulos}

function TdmBaseReportes.EvaluaParametros( oSQLAgente : TSQLAgenteClient;
                                           Parametros : TStrings;
                                           var sError : wideString;
                                           var oParams : OleVariant;
                                           const lMuestraDialogo : Boolean ) : Boolean;
 var i, iCount: integer;
     oParam : OleVariant;
     oColumna : TSQLColumna;
begin
     Result := TRUE;
     iCount := Parametros.Count;
     oParams := VarArrayCreate([1, K_MAX_PARAM ], varVariant);

     if iCount > 0 then
     begin
          for i:=0 to iCount-1 do
              AgregaSQLColumnasParam(oSQLAgente, TCampoMaster(Parametros.Objects[i]));

          Result := EvaluaParams( oSQLAgente, sError );

          if Result then
          begin
               for i := 0 to Parametros.Count -1do
               begin
                   with TCampoMaster(Parametros.Objects[i]) do
                        if (PosAgente >= 0) then
                        begin
                             oColumna := oSQLAgente.GetColumna( PosAgente );
                             oParam := VarArrayCreate([1, 3], varVariant);
                             oParam[1] := Titulo;
                             oParam[3] := oColumna.TipoFormula;
                             if lMuestraDialogo then
                                oParam[2] := oColumna.Formula
                             else
                             begin
                                  {Esto es en el caso de que se pruebe una formula desde reporteador.
                                  Como en ese caso, no se muestra el dialogo de captura de parametros,
                                  los valores de los parametros deben de ir con algun default}
                                  case oColumna.TipoFormula of
                                       tgBooleano: oParam[2] := TRUE;
                                       tgFloat,tgNumero : oParam[2] := 12.5;
                                       tgFecha : oParam[2] := DATE;
                                       else oParam[2] := 'DUMMY'
                                  end;
                             end;
                        end
                        else
                        begin
                             oParam := VarArrayCreate([1, 3], varVariant);
                             oParam[1] := Titulo;
                             oParam[2] := Formula;
                             oParam[3] := TipoCampo;
                        end;
                   oParams[i+1] := oParam;
               end;
               if lMuestraDialogo then
                  Result := {$IFDEF TRESS_DELPHIXE5_UP}FParametros_DevEx{$ELSE}FParametros{$ENDIF}.MuestraParametros( oParams, Parametros.Count );
          end;
     end;
end;

function TdmBaseReportes.PruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;
begin
     Result := ServidorPruebaFormula( Parametros, sTexto, EntidadActiva, oParams );
end;

function TdmBaseReportes.PuedeAbrirCandado: Boolean;
begin
     with dmCliente.GetDatosUsuarioActivo do
     begin
          Result := ( Grupo = K_GRUPO_SISTEMA );
          if not Result then
          begin
               with cdsReportes do
               begin
                    case eCandado( FieldByName( 'RE_CANDADO' ).AsInteger ) of
                         cnAbierto: Result := True;
                         cnSoloImprimir: Result := ( Codigo = FieldByName( 'US_CODIGO' ).AsInteger );
                    else
                        Result := ( Codigo = FieldByName( 'US_CODIGO' ).AsInteger );
                    end;
               end;
          end;
     end;
          //( UsuarioActivo, Grupo, UsuarioReporte, CandadoReporte )
end;

function TdmBaseReportes.GetClasificaciones: string;
var
   i: integer;
begin
     Result := VACIO;
     if ( not dmCliente.ModoTress ) then
     begin
          with FClasificaciones do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Result := ConcatString( Result,
                                            IntToStr( Ord( eClasifiReporte( Fclasificaciones.objects[ i ] ) ) ),
                                            ',' );
               end;
          end
     end;
end;

{$IFDEF TRESS_DELPHIXE5_UP}
function TdmBaseReportes.GetClasificacionesPermitidas: string;
var
   i: integer;
begin
     Result := VACIO;
          with FClasificacionesPermitidas do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Result := ConcatString( Result,
                                            IntToStr( Ord( eClasifiReporte( FclasificacionesPermitidas.objects[ i ] ) ) ),
                                            ',' );
               end;
          end;
end;
{$ENDIF}

function TdmBaseReportes.GetPlantilla( const Plantilla: string ): TBlobField;
begin
     cdsPlantilla.Data := ServerReportes.GetPlantilla( Plantilla );
     Result := TBlobField( cdsPlantilla.FieldByName( 'CampoBlob' ) );
end;

function TdmBaseReportes.GetLogo( const Logo: string ): TBlobField;
begin
     cdsPlantilla.Data := ServerReportes.GetPlantilla( Logo );
     Result := TBlobField( cdsPlantilla.FieldByName( 'CampoBlob' ) );
end;

procedure TdmBaseReportes.BuscaConcepto;
begin
end;


function TdmBaseReportes.EstaEnClasifi( iReporte: Integer; sCampo: String ): boolean;
begin
     Result:= False;
     if ( cdsEditReporte.FieldByName('RE_CODIGO').AsInteger = iReporte ) then
     begin
          Result:= ( cdsReportes.FieldByName(sCampo).AsInteger = dmCliente.Usuario ) ;

     end;
end;

function TdmBaseReportes.ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     {$ifdef RDD}
     if ExisteEntidad( Index ) then
        Result := aTipoEntidad[Index]
     else
         Result := Format( 'Tabla #%d no Existe', [Ord(Index)] );
     {$else}
     Result := ZetaTipoEntidadTools.ObtieneEntidad( TipoEntidad( Index ) );
     {$endif}

end;

function TdmBaseReportes.ObtieneClasificacion( const Index: eClasifiReporte ): String;
begin
     {$ifdef RDD}
     case Index of
          crFavoritos: Result := K_MIS_FAVORITOS_DES;
          crSuscripciones: Result := K_MIS_SUSCRIPCIONES_DES
          else
          begin
               with dmDiccionario.cdsClasificaciones do
               begin
                    if ExisteClasificacion(Index) then
                       Result := StrDef( FieldByName('RC_NOMBRE').AsString, Format( 'Clasificaci�n #%d' , [Ord( Index )] ) )
                    else
                        Result := Format( 'Clasificaci�n #%d no Existe', [Ord(Index)] );
               end;
          end;
     end;

     {$else}
     Result := ObtieneElemento(lfClasifiReporte,Ord(Index) );
     {$endif}
end;

function TdmBaseReportes.ExisteClasificacion( const Index: eClasifiReporte ): Boolean;
begin
     {$ifdef RDD}
     with dmDiccionario.cdsClasificaciones do
     begin
          Conectar;
          Result := Locate( 'RC_CODIGO', Ord(Index), [] );
     end;
     {$else}
     Result := TRUE;
     {$endif}
end;

function TdmBaseReportes.ExisteEntidad( const Index: TipoEntidad ): Boolean;
begin
     {$ifdef RDD}
     dmDiccionario.LlenaArregloEntidades;
     with dmDiccionario.cdsEntidades do
     begin
          Conectar;
          Result := Locate( 'EN_CODIGO', Ord(Index), [] );
     end;
     {$else}
     Result := TRUE;
     {$endif}
end;

{$IFDEF TRESS_DELPHIXE5_UP}

procedure TdmBaseReportes.ImpresionReportesSubsecuentes;
var
   oListaReportes: TList;
   oLista: TStrings;
   i, iReporte, iReporteOriginal :integer;
   oSubsecuente : TIRSubsecuente;

   procedure ImprimePorReporte( const iReporte: integer);
   begin
        if cdsReportes.Locate('RE_CODIGO', iReporte, [] ) then
        begin
             FReporteActivo := iReporte;
             //cdsEditReporte.Refrescar;
             cdsEditReporte.AlAdquirirDatos( cdsEditReporte );
             ImpresionRapida_DevEx.ReadOnly := TRUE;
             ImpresionRapida_DevEx.SoloImpresion := TRUE;
             ImpresionRapida_DevEx.Imprime;
        end
        else
        begin
             raise Exception.Create(Format('El reporte #%d no existe', [iReporte]))
        end;
   end;

   procedure AsignaOBjecto( Objecto: TObject; const iCual: integer; lCreaSubsecuente: Boolean );
   var
      oSubsecuente : TIRSubsecuente;
   begin
        if lCreaSubsecuente then
        begin
             oSubsecuente := TIRSubsecuente.Create;
             oListaReportes.Add( oSubsecuente );
        end
        else
            oSubsecuente := TIRSubsecuente( oListaReportes[oListaReportes.Count-1] );

        case iCual of
             0: oSubsecuente.Resultados.Data := TZetaClientDataset( OBjecto ).Data;
             1: oSubsecuente.Reporte.Data := TZetaClientDataset( OBjecto ).Data;
             2: oSubsecuente.CampoRep.Data  := TZetaClientDataset( OBjecto ).Data;
             3: oSubsecuente.Agente.VariantToAgente(  TSQLAgenteClient( OBjecto ).AgenteToVariant );
        end;
   end;

begin
     ZetaSystemWorking.InitAnimation( 'Imprimiendo Reportes' );
     iReporteOriginal :=  cdsEditReporte.FieldByName('RE_CODIGO').AsInteger;
     oLista := TStringList.Create;
     oListaReportes:= TList.Create;
     try
        oLista.CommaText := cdsEditReporte.FieldByName('RE_FONTNAM').AsString;
        if eOrganizacionHojas( cdsEditReporte.FieldByName('RE_FONTSIZ').AsInteger ) = ohPorReporte then
        begin
             FForzarImpresion := TRUE;
             ImprimePorReporte(iReporteOriginal);
             for i:=0 to oLista.Count -1 do
             begin
                  try
                     iReporte:= StrToInt( oLista[i] );
                  except
                        raise Exception.Create('La lista de reportes subsecuentes es inv�lida');
                  end;
                  ImprimePorReporte(iReporte);
             end;
        end
        else
        begin
             ImpresionRapida_DevEx.Datasource.Dataset := cdsEditReporte;
                //Esta primer pasada, genera todos los reportes.
                //Esto se hace una sola vez.
                //Cada DATASET con los resultados se guarda en una lista.
             if ImpresionRapida_DevEx.GeneraSQL then
             begin
                  try
                     ImpresionRapida_DevEx.DespuesDeConstruyeSQL;
                     ImpresionRapida_DevEx.PreparaReporte(FALSE);
                     ImpresionRapida_DevEx.SubsecuentesAbreImpresora;
                     AsignaOBjecto( cdsResultados, 0, TRUE );
                     AsignaOBjecto( cdsEditReporte, 1, FALSE );
                     AsignaOBjecto( cdsCampoRep, 2, FALSE );
                     AsignaOBjecto( ImpresionRapida_DevEx.SQLAgenteIR, 3, FALSE );
                     for i:=0 to oLista.Count -1 do
                     begin
                          try
                             iReporte:= StrToInt( oLista[i] );
                          except
                                raise Exception.Create('La lista de reportes subsecuentes es inv�lida');
                          end;
                          ImpresionRapida_DevEx.Datasource.Dataset := NIL;
                          FReporteActivo := iReporte;
                          cdsEditReporte.Refrescar;
                          ImpresionRapida_DevEx.Connect;
                          ImpresionRapida_DevEx.GeneraSQL;
                          ImpresionRapida_DevEx.DespuesDeConstruyeSQL;
                          AsignaOBjecto( cdsResultados, 0, TRUE );
                          AsignaOBjecto( cdsEditReporte, 1, FALSE );
                          AsignaOBjecto( cdsCampoRep, 2, FALSE );
                          AsignaOBjecto( ImpresionRapida_DevEx.SQLAgenteIR, 3, FALSE );
                     end;
                     //Esta segunda pasada es para imprimir el reporte.
                     while ( TIRSubsecuente( oListaReportes[0] ).Resultados.RecNo < TIRSubsecuente( oListaReportes[0] ).Resultados.RecordCount )
                           or (TIRSubsecuente( oListaReportes[0] ).Resultados.RecordCount = 1) do
                     begin
                          for i:=0 to oListaReportes.Count -1 do
                          begin
                               oSubsecuente := TIRSubsecuente( oListaReportes[i] ) ;
                               with oSubsecuente do
                               begin
                                    cdsResultados.Data := Resultados.Data;
                                    if NOT Resultados.BOF then
                                       cdsResultados.RecNo := Resultados.RecNo;

                                    if NOT cdsResultados.EOF then
                                    begin
                                         cdsEditReporte.Data := Reporte.Data;
                                         cdsCamporep.Data :=  Camporep.Data;
                                         ImpresionRapida_DevEx.Connect;
                                         ImpresionRapida_DevEx.AgregaSQLCampoRepClear(Agente);
                                         ImpresionRapida_DevEx.DespuesDeConstruyeSQL;
                                         ImpresionRapida_DevEx.PreparaReporte(TRUE);
                                         ImpresionRapida_DevEx.SubsecuentesGeneraReporte( cdsResultados );
                                         Resultados.RecNo := iMin( cdsResultados.RecNo+1, cdsResultados.RecordCount );
                                    end;
                               end;
                          end;
                          if ( TIRSubsecuente( oListaReportes[0] ).Resultados.RecordCount = 1 ) then
                             Break;
                     end;

                     with TIRSubsecuente( oListaReportes[0] ) , Reporte do
                     begin
                          if ( Resultados.RecordCount <> 1 ) and
                             ( ( FieldByName('RE_ANCHO').AsInteger = FieldByName('RE_COLESPA').AsInteger ) or  //Todo esta en el encabezado
                               ( FieldByName('RE_ANCHO').AsInteger = FieldByName('RE_RENESPA').AsInteger ) or  //Todo esta en el detalle
                               ( (FieldByName('RE_COLESPA').AsInteger + FieldByName('RE_RENESPA').AsInteger )=0 ) ) then //Todo esta en el pie
                          begin
                               for i:=0 to oListaReportes.Count -1 do
                               begin
                                    oSubsecuente := TIRSubsecuente( oListaReportes[i] ) ;
                                    with oSubsecuente do
                                    begin
                                         cdsResultados.Data := Resultados.Data;
                                         if NOT Resultados.BOF then
                                            cdsResultados.RecNo := Resultados.RecNo;

                                         if NOT cdsResultados.EOF then
                                         begin
                                              cdsEditReporte.Data := Reporte.Data;
                                              cdsCamporep.Data :=  Camporep.Data;
                                              ImpresionRapida_DevEx.Connect;
                                              ImpresionRapida_DevEx.AgregaSQLCampoRepClear(Agente);
                                              ImpresionRapida_DevEx.DespuesDeConstruyeSQL;
                                              ImpresionRapida_DevEx.PreparaReporte(TRUE);
                                              ImpresionRapida_DevEx.SubsecuentesGeneraReporte( cdsResultados );
                                              Resultados.RecNo := iMin( cdsResultados.RecNo+1, cdsResultados.RecordCount );
                                         end;
                                    end;
                               end;
                          end;
                     end;
                  finally
                       ImpresionRapida_DevEx.SubsecuentesCierraImpresora;
                  end;
             end;
        end;
     finally
            ZetaSystemWorking.EndAnimation;
            for i := 0 to oListaReportes.Count-1 do
                TObject(oListaReportes[i]).Free;
            FreeAndNil( oListaReportes );
            FreeAndNil( oLista );
            ZetaDialogo.ZInformation('Impresi�n', 'Impresi�n Terminada',0);
     end;
end;

{$ELSE}

procedure TdmBaseReportes.ImpresionReportesSubsecuentesClasica;
var
   oListaReportes: TList;

 procedure ImprimePorReporte( const iReporte: integer);

 begin
      if cdsReportes.Locate('RE_CODIGO', iReporte, [] ) then
      begin
           FReporteActivo := iReporte;
           //cdsEditReporte.Refrescar;
           cdsEditReporte.AlAdquirirDatos( cdsEditReporte );
           ImpresionRapida.ReadOnly := TRUE;
           ImpresionRapida.SoloImpresion := TRUE;
           ImpresionRapida.Imprime;
      end
      else
      begin
           raise Exception.Create(Format('El reporte #%d no existe', [iReporte]))
      end;
 end;

 procedure AsignaOBjecto( Objecto: TObject; const iCual: integer; lCreaSubsecuente: Boolean );
  var
     oSubsecuente : TIRSubsecuente;
 begin

      if oListaReportes = NIL then
         oListaReportes:= TList.Create;

      if lCreaSubsecuente then
      begin
           oSubsecuente := TIRSubsecuente.Create;
           oListaReportes.Add( oSubsecuente );
      end
      else
          oSubsecuente := TIRSubsecuente( oListaReportes[oListaReportes.Count-1] );

      case iCual of
           0: oSubsecuente.Resultados.Data := TZetaClientDataset( OBjecto ).Data;
           1: oSubsecuente.Reporte.Data := TZetaClientDataset( OBjecto ).Data;
           2: oSubsecuente.CampoRep.Data  := TZetaClientDataset( OBjecto ).Data;
           3: oSubsecuente.Agente.VariantToAgente(  TSQLAgenteClient( OBjecto ).AgenteToVariant );
      end;
 end;


 var
    oLista: TStrings;
    i, {j,} {iRecords, }iReporte, iReporteOriginal :integer;
    oSubsecuente : TIRSubsecuente;
    //oColumna: TSQLColumna;
begin
     ZetaSystemWorking.InitAnimation( 'Imprimiendo Reportes' );
     iReporteOriginal :=  cdsEditReporte.FieldByName('RE_CODIGO').AsInteger;
     oLista := TStringList.Create;
     try
        oLista.CommaText := cdsEditReporte.FieldByName('RE_FONTNAM').AsString;
        if eOrganizacionHojas( cdsEditReporte.FieldByName('RE_FONTSIZ').AsInteger ) = ohPorReporte then
        begin
             FForzarImpresion := TRUE;
             ImprimePorReporte(iReporteOriginal);

             for i:=0 to oLista.Count -1 do
             begin
                  try
                     iReporte:= StrToInt( oLista[i] );
                  except
                        raise Exception.Create('La lista de reportes subsecuentes es inv�lida');
                  end;

                  ImprimePorReporte(iReporte);


             end;
        end
        else
        begin
             ImpresionRapida.Datasource.Dataset := cdsEditReporte;
                //Esta primer pasada, genera todos los reportes.
                //Esto se hace una sola vez.
                //Cada DATASET con los resultados se guarda en una lista.
             if ImpresionRapida.GeneraSQL then
             begin
                  try
                     ImpresionRapida.DespuesDeConstruyeSQL;
                     ImpresionRapida.PreparaReporte(FALSE);
                     ImpresionRapida.SubsecuentesAbreImpresora;

                     //iRecords := cdsResultados.RecordCount;

                     AsignaOBjecto( cdsResultados, 0, TRUE );
                     AsignaOBjecto( cdsEditReporte, 1, FALSE );
                     AsignaOBjecto( cdsCampoRep, 2, FALSE );
                     //oColumna := ImpresionRapida.SQLAgenteIR.GetColumna( 1 );
                     AsignaOBjecto( ImpresionRapida.SQLAgenteIR, 3, FALSE );

                     for i:=0 to oLista.Count -1 do
                     begin
                          try
                             iReporte:= StrToInt( oLista[i] );
                          except
                                raise Exception.Create('La lista de reportes subsecuentes es inv�lida');
                          end;

                          ImpresionRapida.Datasource.Dataset := NIL;

                          FReporteActivo := iReporte;
                          cdsEditReporte.Refrescar;
                          ImpresionRapida.Connect;
                          ImpresionRapida.GeneraSQL;
                          ImpresionRapida.DespuesDeConstruyeSQL;

                          AsignaOBjecto( cdsResultados, 0, TRUE );
                          AsignaOBjecto( cdsEditReporte, 1, FALSE );
                          AsignaOBjecto( cdsCampoRep, 2, FALSE );
                          //oColumna := ImpresionRapida.SQLAgenteIR.GetColumna( 1 );
                          AsignaOBjecto( ImpresionRapida.SQLAgenteIR, 3, FALSE );
                     end;

                     //Esta segunda pasada es para imprimir el reporte.
                     while ( TIRSubsecuente( oListaReportes[0] ).Resultados.RecNo < TIRSubsecuente( oListaReportes[0] ).Resultados.RecordCount )
                           or (TIRSubsecuente( oListaReportes[0] ).Resultados.RecordCount = 1) do
                     begin
                          for i:=0 to oListaReportes.Count -1 do
                          begin
                               oSubsecuente := TIRSubsecuente( oListaReportes[i] ) ;
                               with oSubsecuente do
                               begin
                                    cdsResultados.Data := Resultados.Data;
                                    if NOT Resultados.BOF then
                                       cdsResultados.RecNo := Resultados.RecNo;

                                    if NOT cdsResultados.EOF then
                                    begin
                                         //ImpresionRapida.Datasource.Dataset := cdsResultados;
                                         cdsEditReporte.Data := Reporte.Data;
                                         cdsCamporep.Data :=  Camporep.Data;
                                         ImpresionRapida.Connect;
                                         //oColumna := ImpresionRapida.SQLAgenteIR.GetColumna( 1 );
                                         ImpresionRapida.AgregaSQLCampoRepClear(Agente);
                                         ImpresionRapida.DespuesDeConstruyeSQL;
                                         ImpresionRapida.PreparaReporte(TRUE);
                                         ImpresionRapida.SubsecuentesGeneraReporte( cdsResultados );
                                         //cdsResultados.Next;
                                         Resultados.RecNo := iMin( cdsResultados.RecNo+1, cdsResultados.RecordCount );
                                    end;
                               end;
                          end;
                          if ( TIRSubsecuente( oListaReportes[0] ).Resultados.RecordCount = 1 ) then
                             Break;
                     end;

                     with TIRSubsecuente( oListaReportes[0] ) , Reporte do
                     begin
                          if ( Resultados.RecordCount <> 1 ) and
                             ( ( FieldByName('RE_ANCHO').AsInteger = FieldByName('RE_COLESPA').AsInteger ) or  //Todo esta en el encabezado
                               ( FieldByName('RE_ANCHO').AsInteger = FieldByName('RE_RENESPA').AsInteger ) or  //Todo esta en el detalle
                               ( (FieldByName('RE_COLESPA').AsInteger + FieldByName('RE_RENESPA').AsInteger )=0 ) ) then //Todo esta en el pie
                          begin
                               for i:=0 to oListaReportes.Count -1 do
                               begin
                                    oSubsecuente := TIRSubsecuente( oListaReportes[i] ) ;
                                    with oSubsecuente do
                                    begin
                                         cdsResultados.Data := Resultados.Data;
                                         if NOT Resultados.BOF then
                                            cdsResultados.RecNo := Resultados.RecNo;

                                         if NOT cdsResultados.EOF then
                                         begin
                                              //ImpresionRapida.Datasource.Dataset := cdsResultados;
                                              cdsEditReporte.Data := Reporte.Data;
                                              cdsCamporep.Data :=  Camporep.Data;
                                              ImpresionRapida.Connect;
                                              //oColumna := ImpresionRapida.SQLAgenteIR.GetColumna( 1 );
                                              ImpresionRapida.AgregaSQLCampoRepClear(Agente);
                                              ImpresionRapida.DespuesDeConstruyeSQL;
                                              ImpresionRapida.PreparaReporte(TRUE);
                                              ImpresionRapida.SubsecuentesGeneraReporte( cdsResultados );
                                              //cdsResultados.Next;
                                              Resultados.RecNo := iMin( cdsResultados.RecNo+1, cdsResultados.RecordCount );
                                         end;
                                    end;
                               end;
                          end;
                     end;
                  finally
                       ImpresionRapida.SubsecuentesCierraImpresora;
                  end;
             end;
        end;

     finally
            ZetaSystemWorking.EndAnimation;
            FreeAndNil( oLista );
            ZetaDialogo.ZInformation('Impresi�n', 'Impresi�n Terminada',0);
     end;
end;

{$ENDIF}

constructor TIRSubsecuente.Create;
begin
     Resultados:= TZetaClientDataSet.Create(nil);
     Reporte:= TZetaClientDataSet.Create(nil);
     CampoRep:= TZetaClientDataSet.Create(nil);
     Agente:= TSQLAgenteClient.Create;
end;

destructor TIRSubsecuente.Destroy;
begin
     FreeAndNil( Resultados );
     FreeAndNil( Reporte  );
     FreeAndNil( CampoRep );
     FreeAndNil( Agente  );
end;

procedure TdmBaseReportes.cdsReportesCalcFields(DataSet: TDataSet);
{$IFDEF TRESS_DELPHIXE5_UP}
var
   i: Integer;
{$ENDIF}
begin
{$IFDEF TRESS_DELPHIXE5_UP}
     with cdsReportes do
     begin
          begin
               with FClasificacionesPermitidas do
               begin
                    for i := 0 to ( Count - 1 ) do
                    begin
                         {if FieldByName('RE_CLASIFI').AsInteger = Ord( eClasifiReporte( objects[ i ] ) ) then
                         begin
                              FieldByName('RE_CLASIFI_DESC').AsString := ObtieneClasificacion( eClasifiReporte( objects[ i ] ) );
                              Break;
                         end;}
                         if Integer( Objects[i] ) = FieldByName('RE_CLASIFI').AsInteger then
                         begin
                              FieldByName('RE_CLASIFI_DESC').AsString := ObtieneClasificacion( eClasifiReporte( Integer( Objects[ i ] ) ) );
                              Break;
                         end;
                    end;
               end;
          end;
     end;
{$ENDIF}
end;

end.



