unit DSistema;

interface

{$DEFINE MULTIPLES_ENTIDADES}
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,ComCtrls,
     {$ifndef VER130}Variants,{$endif}
     DBaseSistema,
     {$ifdef DOS_CAPAS}
     {$else}
         {$ifdef RDD}
         ReporteadorDD_TLB,
         {$endif}
     {$endif}
     {$ifndef DOS_CAPAS}
     Kiosco_TLB,
     Sistema_TLB,
     {$endif}
     ZetaTipoEntidad,
     ZetaCommonLists,
     FCamposMgr,
     ZAccesosMgr,
     ZetaClientDataSet,
     ZetaServerDataSet,
     ZetaCommonClasses,
     MaskUtils;

type

    {$ifdef RDD}
     {$ifdef DOS_CAPAS}
     {$else}
       IdmServerDiccionarioDisp = IdmServerReporteadorDDDisp;
     {$endif}
    {$endif}


  TdmSistema = class(TdmBaseSistema)
    cdsPoll: TZetaClientDataSet;
    cdsUserSuper: TZetaClientDataSet;
    cdsArbol: TZetaClientDataSet;
    cdsNivel0: TZetaLookupDataSet;
    cdsEmpresasPortal: TZetaLookupDataSet;
    cdsEmpleadosPortal: TZetaLookupDataSet;
    cdsEmpresasSeleccion: TZetaLookupDataSet;
    cdsGruposAdic: TZetaLookupDataSet;
    cdsCamposAdic: TZetaClientDataSet;
    cdsBitacora: TZetaClientDataSet;
    cdsEntidadesAccesos: TZetaClientDataSet;
    cdsClasificacionesAccesos: TZetaClientDataSet;
    cdsRolUsuarios: TZetaLookupDataSet;
    cdsAdicionalesAccesos: TZetaClientDataSet;
    cdsSistLstDispositivos: TZetaClientDataSet;
    cdsLstDispDisponibles: TZetaClientDataSet;
    cdsLstDispAsignados: TZetaClientDataSet;
    cdsDisxCom: TZetaClientDataSet;
    cdsACCArbol: TZetaLookupDataSet;
    cdsCarruseles: TZetaLookupDataSet;
    cdsCarruselesInfo: TZetaClientDataSet;
    cdsBitacoraBio: TZetaClientDataSet;
    cdsTermPorGrupo: TZetaClientDataSet; // SYNERGY
    cdsListaGrupos: TZetaLookupDataSet; // SYNERGY
    cdsMensajes: TZetaLookupDataSet;
    cdsTerminales: TZetaLookupDataSet;
    cdsMensajesPorTerminal: TZetaClientDataSet;
    cdsHuellaGti: TZetaClientDataSet;
    cdsTablasBD: TZetaClientDataSet;
    cdsSistActualizarBDs: TZetaLookupDataSet;
    cdsCamposTabla: TZetaClientDataSet;
    cdsCamposLlaveTabla: TZetaClientDataSet;

    cdsConfiguracionCafeteria: TZetaClientDataSet;
    cdsEstacionesCafe: TZetaClientDataSet;
    cdsCalendarioCafeteria: TZetaClientDataSet;
    cdsCalendarioCafeteriaHORA: TStringField;
    cdsCalendarioCafeteriaACCION: TIntegerField;
    cdsCalendarioCafeteriaCONTADOR: TStringField;
    cdsCalendarioCafeteriaSEMANA: TIntegerField;
    cdsCalendarioCafeteriaSYNC: TStringField;
    cdsCalendarioCafeteriaCF_NOMBRE: TStringField;
    cdsCalendarioCafeteriaDI_TIPO: TIntegerField;
    procedure cdsUserSuperAlEnviarDatos(Sender: TObject);
    procedure cdsPollAfterDelete(DataSet: TDataSet);
    procedure cdsPollAlAdquirirDatos(Sender: TObject);
    procedure cdsPollAlEnviarDatos(Sender: TObject);
    procedure cdsPollAlModificar(Sender: TObject);
    procedure cdsPollBeforePost(DataSet: TDataSet);
    procedure cdsPollNewRecord(DataSet: TDataSet);
    procedure cdsNivel0AfterDelete(DataSet: TDataSet);
    procedure cdsNivel0AlAdquirirDatos(Sender: TObject);
    procedure cdsNivel0AlEnviarDatos(Sender: TObject);
    procedure cdsNivel0AlModificar(Sender: TObject);
    procedure cdsArbolAlEnviarDatos(Sender: TObject);
    procedure cdsEmpresasPortalAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpleadosPortalLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsEmpleadosPortalLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsUsuariosBeforePost(DataSet: TDataSet);
    procedure cdsUsuariosNewRecord(DataSet: TDataSet);
    procedure cdsEmpresasSeleccionAlAdquirirDatos(Sender: TObject);
    procedure cdsGruposAdicAlEnviarDatos(Sender: TObject);
    procedure cdsGruposAdicAlAdquirirDatos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsBitacoraAfterOpen(DataSet: TDataSet);
    procedure cdsBitacoraAlModificar(Sender: TObject);
    procedure cdsClasificacionesAccesosAlEnviarDatos(Sender: TObject);
    procedure cdsClasifiRepEmpAlAdquirirDatos(Sender: TObject);
    procedure cdsEntidadesAccesosAlEnviarDatos(Sender: TObject);
    procedure cdsRolesAlAdquirirDatos(Sender: TObject);
    procedure cdsRolesAlEnviarDatos(Sender: TObject);
    procedure cdsRolesAfterDelete(DataSet: TDataSet);
    procedure cdsRolesAlModificar(Sender: TObject);
    procedure cdsRolesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsRolesAfterCancel(DataSet: TDataSet);
    procedure cdsRolesAfterScroll(DataSet: TDataSet);
    procedure cdsUserRolesAlAdquirirDatos(Sender: TObject);
    procedure cdsUserRolesAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosAlEnviarDatos(Sender: TObject);
    procedure cdsRolesBeforePost(DataSet: TDataSet);
    procedure cdsAdicionalesAccesosAlEnviarDatos(Sender: TObject);
    procedure cdsSistLstDispositivosAlAdquirirDatos(Sender: TObject);
    procedure cdsSistLstDispositivosAlCrearCampos(Sender: TObject);
    procedure cdsSistLstDispositivosAlModificar(Sender: TObject);
    procedure cdsSistLstDispositivosBeforePost(DataSet: TDataSet);
    procedure cdsSistLstDispositivosAlEnviarDatos(Sender: TObject);
    procedure cdsSistLstDispositivosAfterDelete(DataSet: TDataSet);
    procedure cdsLstDispAsignadosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsSistLstDispositivosNewRecord(DataSet: TDataSet);
    procedure cdsACCArbolAlAdquirirDatos(Sender: TObject);
    procedure cdsUsuariosAlBorrar(Sender: TObject);
    procedure cdsCarruselesAlAdquirirDatos(Sender: TObject);
    procedure cdsCarruselesInfoAlAdquirirDatos(Sender: TObject);
    procedure cdsCarruselesAlEnviarDatos(Sender: TObject);
    procedure cdsCarruselesInfoBeforeInsert(DataSet: TDataSet);
    procedure cdsCarruselesInfoBeforePost(DataSet: TDataSet);
    procedure cdsListaGruposAlAdquirirDatos(Sender: TObject); // SYNERGY
    procedure cdsListaGruposAlModificar(Sender: TObject); // SYNERGY
    procedure cdsListaGruposAfterDelete(DataSet: TDataSet); // SYNERGY
    procedure cdsListaGruposAlEnviarDatos(Sender: TObject); // SYNERGY
    procedure cdsListaGruposBeforePost(DataSet: TDataSet); // SYNERGY
    procedure cdsTermPorGrupoAfterDelete(DataSet: TDataSet); // SYNERGY
    procedure cdsTermPorGrupoAlAdquirirDatos(Sender: TObject); // SYNERGY
    procedure cdsTermPorGrupoAlEnviarDatos(Sender: TObject); // SYNERGY
    procedure cdsTermPorGrupoAlCrearCampos(Sender: TObject);  // SYNERGY
    procedure cdsMensajesAlAdquirirDatos(Sender: TObject);  // SYNERGY
    procedure cdsMensajesAfterDelete(DataSet: TDataSet); // SYNERGY
    procedure cdsMensajesAlEnviarDatos(Sender: TObject); // SYNERGY
    procedure cdsMensajesAlModificar(Sender: TObject); // SYNERGY
    procedure cdsMensajesBeforePost(DataSet: TDataSet); // SYNERGY
    procedure cdsTerminalesAlAgregar(Sender: TObject); // SYNERGY
    procedure cdsMensajesNewRecord(DataSet: TDataSet); // SYNERGY
    procedure cdsTerminalesAlAdquirirDatos(Sender: TObject); // SYNERGY
    procedure cdsTerminalesAlCrearCampos(Sender: TObject); // SYNERGY
    procedure cdsMensajesPorTerminalAlAdquirirDatos(Sender: TObject); // SYNERGY
    procedure cdsMensajesPorTerminalNewRecord(DataSet: TDataSet); // SYNERGY
    procedure cdsMensajesPorTerminalAlEnviarDatos(Sender: TObject); // SYNERGY
    procedure cdsMensajesPorTerminalAlModificar(Sender: TObject); // SYNERGY
    procedure cdsMensajesPorTerminalBeforePost(DataSet: TDataSet); // SYNERGY
    procedure cdsMensajesPorTerminalAfterDelete(DataSet: TDataSet); // SYNERGY
    procedure cdsTerminalesAlModificar(Sender: TObject); // SYNERGY
    procedure cdsTerminalesAlEnviarDatos(Sender: TObject); // SYNERGY
    procedure cdsMensajesPorTerminalAlCrearCampos(Sender: TObject); // SYNERGY
    procedure TM_NEXTHORGetText(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
    procedure cdsTerminalesAfterDelete(DataSet: TDataSet); // SYNERGY
    procedure cdsListaGruposBeforeDelete(DataSet: TDataSet);
    procedure cdsHuellaGtiAlAdquirirDatos(Sender: TObject);
                                                                  
    { ** cdsUsuariosSeguridad ** }
    procedure cdsUsuariosSeguridadBeforePost(DataSet: TDataSet);
    procedure cdsUsuariosSeguridadNewRecord(DataSet: TDataSet);
    procedure cdsUsuariosSeguridadAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosSeguridadAlBorrar(Sender: TObject);
    procedure cdsUsuariosAlAdquirirDatos(Sender: TObject);

//    procedure GenericReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEmpresasAlCrearCampos(Sender: TObject);
    procedure cdsEmpresasAlEnviarDatos(Sender: TObject);
    procedure cdsEmpresasBeforeInsert(DataSet: TDataSet);

    procedure cdsSistActualizarBDsAlAdquirirDatos(Sender: TObject);
    procedure cdsSistActualizarBDsAlCrearCampos(Sender: TObject);
    procedure cdsSistActualizarBDsNewRecord(DataSet: TDataSet);
    procedure cdsBitacoraBioAlCrearCampos(Sender: TObject);
    procedure cdsPollAlCrearCampos(Sender: TObject);
    procedure cdsBitacoraAlCrearCampos(Sender: TObject);
    procedure cdsMensajesGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsBitacoraBioAlModificar(Sender: TObject);
    procedure cdsTerminalesGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean); //@DC


    procedure cdsConfiguracionCafeteriaAlAdquirirDatos(Sender: TObject);
    procedure cdsConfiguracionCafeteriaAlEnviarDatos(Sender: TObject);
    procedure cdsConfiguracionCafeteriaBeforePost(DataSet: TDataSet);
    procedure cdsConfiguracionCafeteriaNewRecord(DataSet: TDataSet);
    procedure cdsConfiguracionCafeteriaAlCrearCampos(Sender: TObject);
    procedure cdsCalendarioCafeteriaAlCrearCampos(Sender: TObject);
    procedure cdsCalendarioCafeteriaAfterInsert(DataSet: TDataSet);
    procedure cdsCalendarioCafeteriaAfterDelete(DataSet: TDataSet);
    procedure cdsCalendarioCafeteriaBeforePost(DataSet: TDataSet);
    procedure cdsSistLstDispositivosAlBorrar(Sender: TObject);
    procedure cdsCalendarioCafeteriaNewRecord(DataSet: TDataSet);
    procedure cdsCalendarioCafeteriaHORAGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure cdsCalendarioCafeteriaHORAValidate(Sender: TField);
    procedure cdsCalendarioCafeteriaHORASetText(Sender: TField; const Text: string);
    procedure cdsCalendarioCafeteriaACCIONGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure cdsCalendarioCafeteriaCONTADORGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure cdsCalendarioCafeteriaSEMANAGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure cdsCalendarioCafeteriaSYNCGetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure cdsCalendarioCafeteriaSYNCSetText(Sender: TField; const Text: string);
    procedure cdsSistLstDispositivosReconcileError(
      DataSet: TCustomClientDataSet; E: EReconcileError;
      UpdateKind: TUpdateKind; var Action: TReconcileAction);

  private
    { Private declarations }
    FCamposAdic: TListaCampos;
    FClasificaciones: TListaClasificaciones;
    FEditing: Boolean;
    FGrupoDeTerm: string; // SYNERGY
    FFiltroTerminal: string; // SYNERGY
    FTerminalSeleccionada: string; // SYNERGY
    FCafIdentificador : string;
    FCalendarioBuffer: OleVariant;
    FSemanaCalendario: eDiasCafeteria;

{$ifdef DOS_CAPAS}
{$else}
    FIdBiometrico: Integer;
    //FFechaPivote: TDate;
    FHuellas: String;
    FServerKiosco: IdmServerKioscoDisp;
    function GetServerKiosco: IdmServerKioscoDisp;
{$endif}

    procedure GetUsuarioSupervisores(const iUsuario: Integer);
    procedure GetUsuarioCCosto(const iUsuario: Integer);
    function BuildEmpresaPortal( const sEmpresa: String) :OleVariant;
    function GetListaAsignados(const sCampo: String): String;
    function RolGetActivo: String;
    procedure RolUsuarioModeloGet( const sRol: String );
    function BuildEmpresaDBInfo: OleVariant;

  protected
    { Protected declarations }
{$ifdef DOS_CAPAS}
//    function GetServerDiccionario: TdmServerDiccionario;
  //  property ServerDiccionario: TdmServerDiccionario read GetServerDiccionario;
  {$else}
    FServidor: IdmServerDiccionarioDisp;
    function GetServerDiccionario: IdmServerDIccionarioDisp;
    property ServerDiccionario: IdmServerDiccionarioDisp read GetServerDiccionario;
    function BuscaAccesosClasificaciones(const iPosition: Integer): Boolean;overload;
    function BuscaAccesosEntidades(const iPosition: Integer): Boolean;overload;
    property ServerKiosco: IdmServerKioscoDisp read GetServerKiosco;
   {$endif}
    function GetcdsAccesos : TZetaClientDataSet; override;
    function BuscaAccesosAdicionales(const iPosition: Integer): Boolean;
   {$ifndef DOS_CAPAS}
    procedure TE_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
    procedure GP_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
    procedure TE_APPGetText(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
    procedure TE_ZONAGetTex(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
    procedure ValidaFechaVacia(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
    function GetLocalBioDbPath: string; //BIOMETRICO
   {$endif}   
    procedure DB_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure DB_PROCESOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure DB_APLICAPATCHChange(Sender: TField);
    procedure DB_APLICADICCIONARIOChange(Sender: TField);
    procedure DB_APLICAESPECIALESChange(Sender: TField);
    // procedure CM_CONTROLGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CM_CTRL_RLGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SetSemanaCalendario(const Dia: eDiasCafeteria);
  public
  {***Para Definir a que EXE pertenece este arbol***}
    FTipo_Exe_DevEx : Integer;
    { Public declarations }
    property CamposAdic: TListaCampos read FCamposAdic;
    property Clasificaciones: TListaClasificaciones read FClasificaciones;
    property FiltroTerminal: String read FFiltroTerminal write FFiltroTerminal; // SYNERGY
    property GrupoDeTerm: string read FGrupoDeTerm write FGrupoDeTerm; // SYNERGY
    property TerminalSeleccionada: string read FTerminalSeleccionada write FTerminalSeleccionada; // SYNERGY
    property CafIdentificador : String read FCafIdentificador write FCafIdentificador;
    property SemanaCalendario : eDiasCafeteria read FSemanaCalendario write SetSemanaCalendario;


    function CargaListaSupervisores( Lista: TStrings; MostrarActivos: Boolean  ): String;
    function CargaListaTerminales( Lista: TStrings ): String; // SYNERGY
    function HayNivel0: Boolean;
    function GetNameTabAdic( const oEntidad: TipoEntidad ): String;
    procedure GetArbolUsuario( const iUsuario: Integer );
    procedure DescargaListaSupervisores( Lista: TStrings );
    procedure CargaListaAreas(Lista: TStrings);
    procedure DescargaListaAreas(Lista: TStrings);
    function CargaListaCCosto(Lista: TStrings; MostrarActivos: Boolean): string;
    function CargaListaCCostoSeguridad(Lista: TStrings; MostrarActivos: Boolean): string;
    procedure DescargaListaCCosto(Lista: TStrings);
    procedure DescargaListaCCostoSeguridad(Lista: TStrings);
    function CargaEstacionesCafeteria( Lista: TStrings; const lFiltro : Boolean = FALSE ): String;
    function CargaEstacionesConfiguradas( Lista: TStrings ): String;
    function BuildEmpresa( const sEmpresa: String) :OleVariant;

    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);override;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);override;
    {$endif}
    procedure CargaCamposAdicionales( oCamposAdic: TListaCampos; oClasificaciones: TListaClasificaciones; const lValidaDerechos: Boolean = TRUE );
    procedure DescargarGruposAdic( oClasificaciones: TListaClasificaciones );
    procedure DescargarCamposAdic( oCamposAdic: TListaCampos );
    procedure LeeAdicionales;
    procedure AsignaAdicionalesDefault( oDataSet: TZetaClientDataSet );
    procedure SetLookupNamesAdicionales;
    procedure RefrescarBitacora( Parametros: TZetaParams );
    procedure GetListaClasifiEmpresa( oLista : TStrings ); override;
    { Public declarations for RDD}
    function BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso; override;
    function BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso; override;
    function BuscaDerechoAdicionales(const iPosition: Integer): TDerechosAcceso;override;
    function BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;override;
    function BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;override;
    function BuscaDerechoCopiadoAdicionales(const iPosition: Integer): TDerechosAcceso;override;
//    property ComputerName: String read GetComputerName;

    procedure GrabaDerecho( const iPosition: Integer; const iDerecho: TDerechosAcceso; oNodo: TTreeNode; oTextoBitacora: TGetTextoBitacora);override;
    procedure ConectaAccesos( var sGroupName, sCompanyName: String ); override;
    procedure AgregaEmpleadoUsuario;
    procedure RolUsuarioAgregar( const iUsuario: Integer; const sNombre: String );
    procedure RolUsuarioAgregarTodos;
    procedure RolUsuarioBorrar;
    procedure RolUsuarioBorrarTodos;
    procedure RolEditar;
    function BuscarUsuario( const sFiltro: String; var iUsuario: Integer; var sNombre: String ): Boolean;
    procedure ConectarUsuariosLookup;
    procedure CargaListaRoles( Lista: TStrings );override;
    procedure DescargaListaUserRoles( Lista: TStrings );override;
    procedure CargaListaRolesDefault( Lista: TStrings );override;
    procedure BajaUsuario(const iEmpleado:Integer;const lQuitarUsuario:Boolean);override;
    function SetBajaUsuario(iEmpleado : Integer; sEmpresa : String;const lQuitarUsuario:Boolean) : Boolean; //Davol #Bug: 21992 //#Bug: 15995
    procedure GrabaEmpleadoUsuario(iEmpleado,iUsuario:Integer);
    procedure ConectaGrupoAdAdmin( oEmpresa: OleVariant );override;
    procedure ConectaAccesosAdicionales;override;
    procedure CopyAccesos(iGrupo: Integer; sCompany: String);override;
    procedure CopiaDerechosAdicionales;override;
    function GetDigitoEmpresa: String;
    procedure GetListaDispositivos;
    procedure PreparaListaDispxCom;
    procedure LlenacdsLstDispAsignados(oLista: TStrings);
    procedure GrabaDispxCom;
    function GetGlobalRaizKiosco:string;
    procedure GrabaGlobalRaizKiosco(const sValor:string );
    {$ifndef DOS_CAPAS}
    procedure ReiniciaNIP;
    {$endif}
    {$ifndef DOS_CAPAS}
    function ObtenMaxIdBio: Integer; // SYNERGY
    procedure CargaBitacora( Parametros: TZetaParams ); // SYNERGY
    procedure ActualizaIdBio( iEmpleado, iIdBio: Integer; sGrupo: string; iInvitador: Integer ); // SYNERGY
    function DentroListaTerminales( sGrupo, sTerminales: string ): Boolean; // SYNERGY
    procedure GrabaListaTerminales( sGrupo, sTerminales, sSinTerminales: string ); // SYNERGY
    function TieneHuellaEmpleado( iEmpleado: Integer;iNumero:integer ): Boolean; // SYNERGY
    function CantidadHuellaEmpleado(iEmpleado, iNumero, iTipo: Integer): Integer; // SYNERGY
    function TieneHuellaInvitador( iInvitador: Integer ): Boolean; // BIOMETRICO
    procedure ReiniciaTerminales( iTerminal: Integer ); // SYNERGY
    procedure EliminaHuella( iEmpleado, iInvitador, iTipo : Integer ); // BIOMETRICO
    procedure InsertaHuella( iEmpleado, iDedo : Integer; StreamTemplate : String; Invitador : Integer ); // BIOMETRICO
    property IdBiometrico: Integer read FIdBiometrico write FIdBiometrico; // BIOMETRICO
    //property FechaPivote: TDate read FFechaPivote write FFechaPivote;
    property LocalBioDbPath: String read GetLocalBioDbPath;
    procedure ReiniciaHuellas;
    procedure ReportaHuella( IdHuella : Integer );
    procedure ReportaHuellas;
    procedure ReportaHuellaBorra;

    {$endif}

    function EsBaseDatosEmpleados (BaseDatos: String): Boolean;
    function EsBaseDatosSeleccion (BaseDatos: String): Boolean;
    function EsBaseDatosAgregada (Ubicacion: String): Boolean;
    function EmpGrabarDBSistema (Parametros: TZetaParams): Boolean;
    function CrearBDEmpleados (Parametros: TZetaParams): Boolean;
    function GetLogicalName (Conexion, Name: String; Tipo: Integer): string;
    function BackupDB (Conexion,Origen, Destino, Ruta: String): Boolean;
    function RestoreDB (Conexion,Origen, Destino, DataName, PathDataName, LogName, PathLogName: String): Boolean;
    function DepurarTabla (Conexion,Tabla, Database: String): Boolean;
    function DepurarBitacora(Conexion,Database: String) : Boolean;
    function DepurarAsistencia(Conexion,Database: String) : Boolean;
    function DepurarDigitales(Conexion,Database: String) : Boolean;
    function ConectarseServer(Conexion: String; out Mensaje: WideString) : Boolean;
    function GetDBSize(const Conexion, Database: WideString; bUsarConexion: Boolean = FALSE): String;
    function GetDataLogSize(Conexion, Database: WideString; bUsarConexion: Boolean = FALSE): String;
    function ShrinkDB(const Conexion, Database: WideString) : Boolean;
    function GetConexion(const Conn, Database, Datos, UserName, Password: WideString): Boolean;
    function GrabaDB_INFO(Parametros: OleVariant): Boolean;
    function GetPathSQLServer: String;
    function NoEsBDSeleccionVisitantes (BaseDatos: String): Boolean;
    function NoEsBDTressVisitantes (BaseDatos: String): Boolean;
    function PrepararPresupuestos (Parametros: TZetaParams): Boolean;
    function CatalogosPresupuestos (Parametros: TZetaParams): Boolean;
    function EmpleadosPresupuestos (Parametros: TZetaParams): Boolean;
    function OptimizarPresupuestos (Parametros: TZetaParams): Boolean;
    function GetBDFromDB_INFO (DB_CODIGO: String): String;
    function CrearEstructuraBD(Parametros: TZetaParams): Boolean;
    function CreaInicialesTabla(Parametros: TZetaParams): Boolean;
    function CreaSugeridosTabla(Parametros: TZetaParams): Boolean;
    function CreaDiccionarioTabla(Parametros: TZetaParams): Boolean;
    function CreaEspeciales(Parametros: TZetaParams): Boolean;
    function ExistenEspeciales: Boolean;
    function ExisteBD_DBINFO(Codigo: string): Boolean;
    procedure GetTablasBD (BaseDatosUbicacion, Usuario, Password: WideString);
    procedure GetCamposTabla (BaseDatosUbicacion, Usuario, Password, Tabla: WideString);
    procedure GetCamposLlaveTabla (BaseDatosUbicacion, Usuario, Password, Tabla: WideString);
    function EjecutaMotorPatch(Parametros: TZetaParams): Boolean;
    //function GetVersionesPatch(oLista: TStrings; const Tipo: Integer; const MinVersion: Integer = 0): Boolean;
    procedure GetListaTablas( ListaTablas: TStrings );
    procedure GetTablaInfo( const sTablaName: String; cdsTabla: TZetaClientDataSet );
    procedure InvocarExportarTablasXML;
    function GetBaseDatosImportacion: OleVariant;
    function GetMotorPatchAvance(Parametros: OleVariant): OleVariant;
    // US #11012 Opcion para obtener status de licencia de uso.
    function GetEmpleadosConteo: Integer;

    procedure QuitaFiltro;
    procedure ActualizaStatus;
  end;

var
  dmSistema: TdmSistema;

const
       K_KIOSCO_DIRECTORIO_RAIZ  = 52;
       K_CONST_CARRUSELES        = 18;

       K_SUPERVISORES            = 0;
       K_SUPER_AREAS             = 1;
       K_SUPER_CCOSTO            = 2;
       K_BANNER                  = 'Grupo Tress Internacional, S.A. de C.V.';
       K_GAFETE                  = 'CAFE';
       K_TCOMIDA_1               = 'Tipo de Comida #1';
       K_TCOMIDA_2               = 'Tipo de Comida #2';
       K_TCOMIDA_3               = 'Tipo de Comida #3';
       K_TCOMIDA_4               = 'Tipo de Comida #4';
       K_TCOMIDA_5               = 'Tipo de Comida #5';
       K_TCOMIDA_6               = 'Tipo de Comida #6';
       K_TCOMIDA_7               = 'Tipo de Comida #7';
       K_TCOMIDA_8               = 'Tipo de Comida #8';
       K_TCOMIDA_9               = 'Tipo de Comida #9';
{$ifndef DOS_CAPAS}
       K_EXT_TEMPLATE = '.tem';
       K_FILENAME = 'temp' + K_EXT_TEMPLATE;
{$endif}

implementation

uses ZetaCommonTools,
     ZetaBuscaEmpleado_DevEx,
     ZetaServerTools,
     ZetaDialogo,
     ZetaMsgDlg,
     ZBaseEdicion_DevEx,
     ZAccesosTress,
     ZGlobalTress,
     FAutoClasses,
     FSistEditNivel0_DevEx,
     FSistEditPoll_DevEx,
     FCatRolesEdit_Devex,
     DGlobal,
     DTablas,
     DCliente,
     DReportes,
     ZetaClientTools,
     FBaseEditBitacora_DevEx, //DevEx(by am)
     FEditBitacoraRegBio_DevEx,  //DevEx (by @DC)
     FEditBitacoraSistema_DevEx,
     FEditBitacoraReportes_DevEx,
     ZReconcile,
     FEditSistLstDispositivos_DevEx,
     FEditSistListaGrupos_DevEx,
     FEditMensajes_DevEx,
     FEditMensajesPorTerminal_DevEx,
     FEditTerminales_DevEx,
     ZetaWinAPITools,
     FExportarTablas_DevEx,
     ZBaseDlgModal_DevEx,
     ZetaBusqueda_DevEx,
     {$ifdef TRESS_DELPHIXE5_UP}
     FConfiguraTress,
     FSistlstDispositivos_DevEx,
     {$endif}
     FEditSistSolicitudReportes_DevEx,
     {$ifdef TRESS_DELPHIXE5_UP}
     FWizSistCalendarioReportes_DevEx,
     {$endif}
     FSistSolicitudEnviosLog_DevEx,
     ZcxWizardBasico;

{$R *.DFM}

procedure TdmSistema.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FEditing:= FALSE;
     FCamposAdic := TListaCampos.Create;
     FClasificaciones := TListaClasificaciones.Create;
     EditandoUsuarios := False;
     {$ifndef DOS_CAPAS}
     FFiltroTerminal := VACIO;
     {$endif}
end;

procedure TdmSistema.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FCamposAdic );
     FreeAndNil( FClasificaciones );
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmSistema.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmSistema.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     inherited NotifyDataChange(Entidades, Estado);
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPoll , Entidades ) then
     begin
     {$else}
     if ( enPoll in Entidades ) then
     begin
     {$endif}
         cdsPoll.SetDataChange;
         cdsTerminales.SetDataChange;
//TO-DO          cdsSistTareaCalendario.SetDataChange;
//To-DO         cdsSistTareaCalendarioLookup.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enDispositivos , Entidades ) then
     {$else}
     if ( enDispositivos in Entidades ) then
     {$endif}
     begin
          cdsSistLstDispositivos.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enHuella , Entidades ) then
     {$else}
     if ( enHuella in Entidades ) then
     {$endif}
     begin
          cdsTerminales.SetDataChange;
     end;

end;

procedure TdmSistema.cdsUserSuperAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oDelta: OleVariant;
begin
     inherited;
     ErrorCount := 0;
     with cdsUserSuper do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             oDelta := Delta
          else
              oDelta := NULL;
          case Tag of
               K_SUPER_AREAS : Reconcile( ServerSistema.GrabaUsuarioAreas( dmCliente.Empresa, cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger, oDelta, ErrorCount, FTextoBitacora ) );
               K_SUPER_CCOSTO : Reconcile( ServerSistema.GrabaUsuarioCCosto( dmCliente.Empresa, oDelta, ErrorCount, cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger, FTextoBitacora, Global.NombreCosteo ) )
          else
              Reconcile( ServerSistema.GrabaUsuariosSupervisores( dmCliente.Empresa, oDelta, ErrorCount, cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger, FTextoBitacora ) );
          end;
     end;
     FTextoBitacora:= VACIO;
end;

{ ********** Poll Pendientes ************* }

procedure TdmSistema.cdsPollAlAdquirirDatos(Sender: TObject);
begin
     cdsPoll.Data := ServerSistema.GetPoll;
end;

procedure TdmSistema.cdsPollAlModificar(Sender: TObject);
begin
     ZBaseEdicion_devEx.ShowFormaEdicion( SistEditPoll_DevEx, TSistEditPoll_DevEx )
end;

procedure TdmSistema.cdsPollAfterDelete(DataSet: TDataSet);
begin
     cdsPoll.Enviar;
end;

procedure TdmSistema.cdsPollAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPoll do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconcile( ServerSistema.GrabaPoll( Delta, ErrorCount ) );
     end;
end;

procedure TdmSistema.cdsPollBeforePost(DataSet: TDataSet);
begin
     with cdsPoll do
     begin
          if ( FieldByName( 'PO_NUMERO' ).AsInteger <= 0 ) then
             DatabaseError( 'N�mero Debe Ser Mayor a 0' );
          if StrVacio(FieldByName( 'PO_HORA' ).AsSTring) then
             DataBaseError( 'La Hora De La Checada No Puede Quedar Vac�a' );
     end;
end;

procedure TdmSistema.cdsPollNewRecord(DataSet: TDataSet);
begin
     with cdsPoll do
     begin
          //Global.GetGlobalString( K_GLOBAL_DIGITO_EMPRESA );
          FieldByName( 'PO_EMPRESA' ).AsString := GetDigitoEmpresa;
          FieldByName( 'PO_LETRA' ).AsString := 'A';
          FieldByName( 'PO_FECHA' ).AsDateTime := Date;
     end;
end;

function TdmSistema.GetDigitoEmpresa: String;
begin
     Result := VACIO;
     with cdsEmpresas do
     begin
          Conectar;
          with dmCliente.GetDatosEmpresaActiva do
          begin
               if( FieldByName('CM_CODIGO').AsString = Codigo ) or ( Locate( 'CM_CODIGO', Codigo,[] ) )then
                   Result := FieldByName('CM_DIGITO').AsString;
          end;
     end;
end;

{ ********* cdsNivel0 *********** }

procedure TdmSistema.cdsNivel0AlAdquirirDatos(Sender: TObject);
begin
     cdsNivel0.Data := ServerSistema.GetNivel0;
end;


procedure TdmSistema.cdsNivel0AlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditNivel0_DevEx, TEditNivel0_DevEx );
end;

procedure TdmSistema.cdsNivel0AlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsNivel0 do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconcile( ServerSistema.GrabaNivel0( Delta, ErrorCount ) );
     end;
end;

procedure TdmSistema.cdsNivel0AfterDelete(DataSet: TDataSet);
begin
     cdsNivel0.Enviar;
end;

function TdmSistema.HayNivel0: Boolean;
begin
     cdsNivel0.Conectar;
     Result := ( not cdsNivel0.IsEmpty );
end;

{ *********** Arbol del Usuario ************** }

procedure TdmSistema.GetArbolUsuario( const iUsuario: Integer );
begin
     //cdsArbol.Data := ServerSistema.GetUsuarioArbol( iUsuario, FTipo_Exe_DevEx );
     //cdsArbol.Data := ServerSistema.GetUsuarioArbolApp(iUsuario, FTipo_Exe_DevEx); //old
     cdsArbol.Data := ServerSistema.GetUsuarioArbolApp(iUsuario, dmCliente.GetAppNumber);
end;

procedure TdmSistema.cdsArbolAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsArbol do
     begin
          if ( ChangeCount > 0 ) then
             Reconcile ( ServerSistema.GrabaArbolApp( UsuarioPosicionado, dmCliente.GetAppNumber, Delta, ErrorCount ) );
             //Reconcile ( ServerSistema.GrabaArbolApp( UsuarioPosicionado, FTipo_Exe_DevEx, Delta, ErrorCount ) ); //old
             //Reconcile ( ServerSistema.GrabaArbol( UsuarioPosicionado,FTipo_Exe_DevEx, Delta, ErrorCount ) );
     end;
end;

function TdmSistema.CargaListaSupervisores( Lista: TStrings; MostrarActivos: Boolean ): String;
var
   sCodigo, sOldIndex: String;
   iPos: Integer;
begin
     with dmTablas.cdsSupervisores do
     begin
          Conectar;
          sOldIndex := IndexFieldNames;
          try
             IndexFieldNames := 'TB_ELEMENT';
             First;
             with Lista do
             begin
                  BeginUpdate;
                  Clear;
                  while not Eof do
                  begin
                       AddObject( FieldByName( 'TB_CODIGO' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString, TObject( 0 ) );
                       Next;
                  end;
                  EndUpdate;
             end;
          finally
             IndexFieldNames := sOldIndex;
          end;
     end;
     GetUsuarioSupervisores( cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger );

     with cdsUserSuper do
     begin
          First;
          with Lista do
          begin
               BeginUpdate;
               while not Eof do
               begin
                    sCodigo := FieldByName( 'CB_NIVEL' ).AsString;
                    iPos := IndexOfName( sCodigo );
                    if ( iPos < 0 ) then
                       AddObject( sCodigo + '=' + ' ???', TObject( 1 ) )
                    else
                        Objects[ iPos ] := TObject( 1 );
                    Next;
               end;
               EndUpdate;
          end;
     end;

     if MostrarActivos then
     begin
          with dmTablas.cdsSupervisores do
          begin
               Filter := Format( 'TB_ACTIVO = %s', [EntreComillas( K_GLOBAL_NO )] );
               Filtered:= True;
               try
                  with Lista do
                  begin
                       BeginUpdate;
                       while not Eof do
                       begin
                            sCodigo := FieldByName( 'TB_CODIGO' ).AsString;
                            iPos := IndexOfName( sCodigo );
                                 if( iPos >= 0 ) then
                                 begin
                                      if ( ( Integer ( Objects[ iPos ] ) )= 0 ) then
                                      begin
                                           Lista.Delete( iPos );
                                      end;
                                 end;
                            Next;
                       end;

                  end;
               finally
                      Lista.EndUpdate;
                      Filtered:= False;
               end;
          end;
     end;

     with Global do
     begin
          Result := 'Asignar ' + NombreNivel( GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR ) ) + ' a ' + cdsUsuarios.FieldByName( 'US_NOMBRE' ).AsString;
     end;
end;

// SYNERGY
function TdmSistema.CargaListaTerminales( Lista: TStrings ): String;
begin
     Lista.Clear;
     with cdsTerminales do
     begin
          Conectar;
          First;
          while not EOF do
          begin
               Lista.Add( FieldByName( 'TE_CODIGO' ).AsString + '=' +
                           FieldByName( 'TE_NOMBRE' ).AsString );
               Next;
          end;
          Filter := VACIO;
          Filtered := False;
     end;
end;

function TdmSistema.CargaEstacionesCafeteria( Lista: TStrings; const lFiltro : Boolean ): String;
begin
     Lista.Clear;
     cdsEstacionesCafe.Data := ServerSistema.GetSistListEstaciones( lFiltro );
     with cdsEstacionesCafe do
     begin
          Conectar;
          First;
          while not EOF do
          begin
               Lista.Add( FieldByName( 'DI_NOMBRE' ).AsString );
               Next;
          end;
     end;
end;

function TdmSistema.CargaEstacionesConfiguradas( Lista: TStrings ): String;
const K_TIPO = 1;
begin
     Lista.Clear;
     cdsEstacionesCafe.Data := ServerSistema.GetSistLstDispositivos( dmCliente.Empresa );
     with cdsEstacionesCafe do
     begin
          Filter := Format( 'DI_TIPO = %d and STATUS = %d', [K_TIPO, ord(tConfigurada)] );
          Filtered:= True;
          Conectar;
          First;
          while not EOF do
          begin
               Lista.Add( FieldByName( 'DI_NOMBRE' ).AsString );
               Next;
          end;

          Filtered:= False;
     end;
end;

procedure TdmSistema.DescargaListaSupervisores( Lista: TStrings );
var
   i, iUsuario: Integer;
   sCodigo: String;
begin
     iUsuario := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
     FTextoBitacora:= 'Antes: '+ GetListaAsignados('CB_NIVEL');
     cdsUserSuper.EmptyDataset;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Integer( Lista.Objects[ i ] ) > 0 ) then
               begin
                    sCodigo := Names[ i ];
                    with cdsUserSuper do
                    begin
                         Append;
                         FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                         FieldByName( 'CB_NIVEL' ).AsString := sCodigo;
                         Post;
                    end;
               end;
          end;
     end;
     FTextoBitacora:= FTextoBitacora + CR_LF + 'Despu�s: '+ GetListaAsignados('CB_NIVEL');
     cdsUserSuper.Enviar;
end;

function TdmSistema.GetListaAsignados(const sCampo: String): String;
var
   sTexto: String;
const
     K_COMA = ',';
begin
     with cdsUserSuper do
     begin
          First;
          while ( not EOF ) do
          begin
               sTexto:= ConcatString(sTexto, FieldByName(sCampo).AsString, K_COMA);
               Next;
          end;
     end;
     Result:= sTexto;
end;

procedure TdmSistema.GetUsuarioSupervisores( const iUsuario: Integer );
begin
     cdsUserSuper.Data := ServerSistema.GetUsuarioSupervisores( dmCliente.Empresa, iUsuario );
end;

procedure TdmSistema.CargaListaAreas( Lista: TStrings );
begin
     with cdsUserSuper do
     begin
          Data := ServerSistema.GetUsuarioAreas( dmCliente.Empresa, cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger );
          First;
          with Lista do
          begin
               BeginUpdate;
               Clear;
               while not Eof do
               begin
                    AddObject( FieldByName( 'CB_AREA' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString, TObject( FieldByName( 'US_CODIGO' ).AsInteger ) );
                    Next;
               end;
               EndUpdate;
          end;
     end;
end;

procedure TdmSistema.DescargaListaAreas( Lista: TStrings );
var
   i, iUsuario: Integer;
   sCodigo: String;
   lState: boolean;
begin
     lState := FALSE;
     iUsuario := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
     with cdsUserSuper do
     begin
          FTextoBitacora:= 'Antes: '+ GetListaAsignados('CB_AREA');
          EmptyDataset;
          MergeChangeLog;
          with Lista do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if ( Integer( Lista.Objects[ i ] ) > 0 ) then
                    begin
                         sCodigo := Names[ i ];
                         cdsUserSuper.Append;
                         FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                         FieldByName( 'CB_AREA' ).AsString := sCodigo;
                         Post;
                         lState := TRUE;
                    end
                    else
                        //lState := TRUE;
               end;
               FTextoBitacora:= FTextoBitacora + CR_LF + 'Despues: '+ GetListaAsignados('CB_AREA');
          end;
          if not lState then
             EmptyDataSet;
          Tag := K_SUPER_AREAS;
          try
             Enviar;                                                 
          finally
                 Tag := K_SUPERVISORES;
          end;
     end;
end;

{ cdsEmpresasPortal }
procedure TdmSistema.cdsEmpresasPortalAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          //cdsEmpresasPortal.Data := ServerSistema.GetEmpresas( Ord( TipoCompany ), K_GRUPO_ADMIN );
          cdsEmpresasPortal.Data := dmCliente.GetEmpresas;
          { Para que se refresque el Dataset con el que se escogen las empresas }
          //cdsCompany.SetDataChange;
     end;
end;


function TdmSistema.BuildEmpresaPortal(const sEmpresa: String):OleVariant;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5;
}
begin
     with cdsEmpresasPortal do
     begin
          Locate( 'CM_CODIGO', sEmpresa, [loCaseInsensitive] );
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                          FieldByName( 'CM_USRNAME' ).AsString,
                          FieldByName( 'CM_PASSWRD' ).AsString,
                          Self.Usuario,
                          FieldByName( 'CM_NIVEL0' ).AsString,
                          FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;

{ cdsEmpleadosPortal }
procedure TdmSistema.cdsEmpleadosPortalLookupKey( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
var
   Datos: OleVariant;
   sEmpresa: String;
begin
     sEmpresa := cdsUsuarios.FieldByName('CM_CODIGO').AsString;
     if strLleno( sEmpresa ) then
     begin
          lOk := dmCliente.Servidor.GetLookupEmpleado( BuildEmpresaPortal( sEmpresa ), StrToIntDef( sKey, 0 ), Datos, Ord( eLookEmpGeneral ) );
          if lOk then
          begin
               with cdsEmpleadosPortal do
               begin
                    Init;
                    Data := Datos;
                    sDescription := GetDescription;
               end;
          end;
     end;
end;

procedure TdmSistema.cdsEmpleadosPortalLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
var
   sEmpresa: String;
begin
     sEmpresa := cdsUsuarios.FieldByName('CM_CODIGO').AsString;
     if strLleno( sEmpresa ) then
        lOk := ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogoDataSet( sFilter, sKey, sDescription, cdsEmpleadosPortal, BuildEmpresaPortal( sEmpresa ) );
end;

procedure TdmSistema.cdsUsuariosBeforePost(DataSet: TDataSet);
var
    sFilter: string;
    lFiltered {$ifndef DOS_CAPAS}, lValidaReportaA {$endif} : Boolean;
    {$ifndef DOS_CAPAS}
    sMensaje: string;
    {$endif}
begin
     inherited;

     with cdsUsuarios do
     begin
          if strLleno( FieldByName('CM_CODIGO').AsString ) then
          begin
               if ( FieldByName('CB_CODIGO').AsInteger = 0 )then
                  DataBaseError( 'El Campo De Empleado No Puede Quedar Vac�o' );

               sFilter := cdsUsuariosLookup.Filter;
               lFiltered := cdsUsuariosLookup.Filtered;
               cdsUsuariosLookup.Filter := Format( 'US_CODIGO <> %d',[cdsUsuarios.FieldByName('US_CODIGO').AsInteger] );
               cdsUsuariosLookup.Filtered := TRUE;
               try
                  if cdsUsuariosLookup.Locate('CM_CODIGO;CB_CODIGO', VarArrayOf([FieldByName('CM_CODIGO').AsString,FieldByName('CB_CODIGO').AsInteger]), [] ) then
                  begin
                       DataBaseError( Format( 'El Empleado %d ya esta asignado al Usuario #%d', [FieldByName('CB_CODIGO').AsInteger, cdsUsuariosLookup.FieldByName('US_CODIGO').AsInteger] ) );
                  end;
               finally
                      cdsUsuariosLookup.Filter := sFilter;
                      cdsUsuariosLookup.Filtered := lFiltered;
               end;
          end;


           {V 2013 SOP-4204 CAS-145137- Bivalencia en comportamiento al registrar usuarios con Sistemas con Active Directory}
          // if ( dmCliente.TipoLogin <> tlLoginTress ) then
          if ( dmCliente.TipoLogin = tlAD ) then
          begin
               if StrVacio( FieldByName('US_DOMAIN').AsString ) then
                  DataBaseError( 'El Usuario del Dominio No Puede Quedar Vac�o' );
          end;

          {$ifndef DOS_CAPAS}
          with dmCliente do
          begin
               if ((EditandoUsuarios ) and ( not Autorizacion.EsDemo ) and   ( ( ModuloAutorizadoConsulta ( okEvaluacion, sMensaje ) ) or ( ModuloAutorizadoConsulta ( okWorkflow , sMensaje ) ) ) )then
               begin
                    lValidaReportaA := True;
                    {$ifdef TRESS}
                    if (dmCliente.EmpresaAbierta)  then
                       lValidaReportaA := not Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES );
                    {$endif}

                    if (lValidaReportaA) and (FieldByName('US_JEFE').AsInteger <= 0 )then
                       ZetaDialogo.ZWarning('Advertencia','El Campo De Reporta A No Puede Quedar Vac�o',0,mbOk);
                    EditandoUsuarios := False;
               end;
          end;
          {$endif}
     end;
end;


{ cdsEmpresasSeleccion }

procedure TdmSistema.cdsEmpresasSeleccionAlAdquirirDatos(Sender: TObject);
begin
     cdsEmpresasSeleccion.Data := ServerSistema.GetEmpresas( Ord( tcRecluta ), dmCliente.GetGrupoActivo );
end;

{ cdsGruposAdic }

procedure TdmSistema.ConectaGrupoAdAdmin( oEmpresa: OleVariant );
 var
    oCampos: OleVariant;
begin
     oEmpresa[P_GRUPO] := D_GRUPO_SIN_RESTRICCION;
     cdsGruposAdic.Data := ServerSistema.GetGruposAdic( oEmpresa, oCampos );
end;

procedure TdmSistema.cdsGruposAdicAlAdquirirDatos(Sender: TObject);
var
   oCampos : OleVariant;
begin
     inherited;
     cdsGruposAdic.Data := ServerSistema.GetGruposAdic( dmCliente.Empresa, oCampos );
     cdsCamposAdic.Data := oCampos;
end;

procedure TdmSistema.cdsGruposAdicAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorCampos: Integer;
   oGruposResult, oCamposResult : OleVariant;
begin
     inherited;
     ErrorCount := 0;
     ErrorCampos := 0;
     with cdsGruposAdic do
     begin
          oGruposResult := ServerSistema.GrabaGruposAdic( dmCliente.Empresa, DeltaNull, cdsCamposAdic.DeltaNull, ErrorCount, ErrorCampos, oCamposResult );
          if Reconciliar( oGruposResult ) then
             cdsCamposAdic.Reconciliar( oCamposResult );  // Si hay error se aborta el reconciliar y se quedan los changecount
     end;
end;

procedure TdmSistema.CargaCamposAdicionales( oCamposAdic: TListaCampos; oClasificaciones: TListaClasificaciones; const lValidaDerechos: Boolean );
var
   iLongitud,iDerecho: integer;
begin
     { Se cambio por un conectar. No se desea que se refresque cuando lo carga como administrador
     cdsGruposAdic.Refrescar;}
     cdsGruposAdic.Conectar;
     // TraspasaGrupos
     oClasificaciones.Clear;
     with cdsGruposAdic do
     begin
          First;
          while ( not EOF ) do
          begin
               if ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION ) or ( not lValidaDerechos ) then
               begin
                    iDerecho := 61;//Suma de los derechos
               end
               else
                   iDerecho := FieldByName( 'GX_DERECHO' ).AsInteger;

               oClasificaciones.Add( FieldByName( 'GX_CODIGO' ).AsString,
                                     FieldByName( 'GX_TITULO' ).AsString,
                                     FieldByName( 'GX_POSICIO' ).AsInteger,
                                     iDerecho );
               Next;
          end;
     end;
     // Traspasa Campos
     oCamposAdic.Clear;
     Global.Conectar;
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
        iLongitud := 50
     else
         iLongitud := 30;
     with cdsCamposAdic do
     begin
          First;
          while ( not EOF ) do
          begin
               oCamposAdic.Add( eExCampoTipo( FieldByName( 'CX_TIPO' ).AsInteger ),
                                FieldByName( 'CX_TITULO' ).AsString,
                                FieldByName( 'CX_NOMBRE' ).AsString,
                                FieldByName( 'CX_DEFAULT' ).AsString,
                                FieldByName( 'GX_CODIGO' ).AsString,
                                FieldByName( 'CX_POSICIO' ).AsInteger,
                                eExMostrar( FieldByName( 'CX_MOSTRAR' ).AsInteger ),
                                TipoEntidad( FieldByName( 'CX_ENTIDAD' ).AsInteger ),
                                iLongitud,
                                VACIO,
                                zStrToBool(FieldByName( 'CX_OBLIGA' ).AsString));
               Next;
          end;
     end;
end;

procedure TdmSistema.DescargarGruposAdic( oClasificaciones: TListaClasificaciones );
var
   i: integer;
begin
     cdsGruposAdic.EmptyDataSet;
     with oClasificaciones do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Clasificacion[ i ] do
               begin
                    with cdsGruposAdic do
                    begin
                         Append;
                         try
                            FieldByName( 'GX_CODIGO' ).AsString := Codigo;
                            FieldByName( 'GX_TITULO' ).AsString := Nombre;
                            FieldByName( 'GX_POSICIO' ).AsInteger := Posicion;
                            Post;
                         except
                               Cancel;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TdmSistema.DescargarCamposAdic( oCamposAdic: TListaCampos );
var
   i: integer;
begin
     with cdsCamposAdic do
     begin
          EmptyDataSet;
          with oCamposAdic do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Append;
                    try
                       with Campo[ i ] do
                       begin
                            FieldByName( 'CX_NOMBRE' ).AsString := Campo;
                            FieldByName( 'GX_CODIGO' ).AsString := Clasificacion;
                            FieldByName( 'CX_TITULO' ).AsString := Letrero;
                            FieldByName( 'CX_POSICIO' ).AsInteger := Posicion;
                            FieldByName( 'CX_DEFAULT' ).AsString := ValorDefault;
                            FieldByName( 'CX_MOSTRAR' ).AsInteger := Ord( Mostrar );
                            FieldByName( 'CX_TIPO' ).AsInteger := Ord( Tipo );
                            FieldByName( 'CX_ENTIDAD' ).AsInteger := Ord( Entidad );
                            FieldByName( 'CX_OBLIGA' ).AsString := zBoolToStr(Obligatorio);             
                       end;
                       Post;
                    except
                          Cancel;
                    end;
               end;
          end;
     end;
end;

function TdmSistema.GetNameTabAdic( const oEntidad: TipoEntidad ): String;
begin
     Result := FCamposAdic.GetLetreroEntidad( oEntidad );
end;

procedure TdmSistema.LeeAdicionales;
begin
     { Se traspaso la llamada de CargaCamposAdicionales a LeeAdicionales, para que no refrescara cuando se carga cdsGruposAdic como Administrador}
     cdsGruposAdic.Refrescar;
     CargaCamposAdicionales( FCamposAdic, FClasificaciones, TRUE );
end;


procedure TdmSistema.AsignaAdicionalesDefault( oDataSet: TZetaClientDataSet );
var
   i:Integer;
   oCampo : TCampo;
begin
     for i := 0 to ( FCamposAdic.Count - 1 ) do
     begin
          oCampo := FCamposAdic.Campo[i];
          if ZetaCommonTools.StrLleno( oCampo.Clasificacion ) then
          begin
               with oDataSet.FieldByName( oCampo.Campo ) do
               begin
                    case oCampo.Tipo of
                         xctBooleano, xctBoolStr : AsString := ZetaCommonTools.StrDef( oCampo.ValorDefault, K_GLOBAL_NO );
                         xctFecha : AsDateTime := ZetaCommonTools.StrAsFecha( oCampo.ValorDefault );
                    else
                        AsString := oCampo.ValorDefault;  // xctTexto, xctNumero, xctTabla
                    end;
               end;
          end;
     end;
end;

procedure TdmSistema.SetLookupNamesAdicionales;
var
   i:Integer;
   oCampo : TCampo;
begin
     for i := 0 to ( FCamposAdic.Count - 1 ) do
     begin
          oCampo := FCamposAdic.Campo[i];
          if ( oCampo.Tipo = xctTabla ) then
             dmTablas.GetEntidadAdicional( oCampo.Entidad ).LookupName := oCampo.Letrero;
     end;
end;

{ cdsBitacora }

procedure TdmSistema.RefrescarBitacora(Parametros: TZetaParams);
begin
     cdsBitacora.Data := ServerSistema.GetBitacora( dmCliente.Empresa, Parametros.VarValues );
end;

procedure TdmSistema.cdsBitacoraAfterOpen(DataSet: TDataSet);
begin
     with cdsBitacora do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_CLASE', lfClaseSistBitacora );
          MaskFecha( 'BI_FECHA' );
          MaskFecha( 'BI_FEC_MOV' );
     end;
end;

procedure TdmSistema.cdsBitacoraAlModificar(Sender: TObject);
begin
     FBaseEditBitacora_DevEx.ShowLogDetail( EditBitacoraSistema_DevEx, TEditBitacoraSistema_DevEx, cdsBitacora );
end;

procedure TdmSistema.cdsRolesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsRoles.Data := ServerSistema.GetRoles;
end;

procedure TdmSistema.cdsRolesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   Roles, Usuarios{, Modelos}: OleVariant;
begin
     ErrorCount := 0;
     with cdsRolUsuarios do
     begin
          PostData;
          MergeChangeLog;
          Usuarios := Data;
     end;
     {with cdsRolModelos do
     begin
          PostData;
          MergeChangeLog;
          Modelos := Data;
     end;}
     with cdsRoles do
     begin
          PostData;
          Roles := DeltaNull;
          if not VarIsNull( Roles ) or not VarIsNull( Usuarios ) {or not VarIsNull( Modelos )} then
          begin
               Reconcile( ServerSistema.GrabaRoles( FieldByName( 'RO_CODIGO' ).AsString, Roles, Usuarios, Null, ErrorCount ) );
          end;
     end;
end;

procedure TdmSistema.cdsUsuariosNewRecord(DataSet: TDataSet);
begin
     inherited;
     //Solo Aplica para Tress, otros sistemas tales como Seleccion,Visitantes o Imx, no deben de hacer estas validaciones.
     with cdsUsuarios do
     begin
         {$ifdef DOS_CAPAS}
          FieldByName( 'US_PORTAL' ).AsString := K_GLOBAL_NO;
         {$else}
          FieldByName( 'US_PORTAL' ).AsString := zBoolToStr( dmCliente.ModuloAutorizadoDerechos( okPortal ) );
         {$endif}
     end;
end;

procedure TdmSistema.AgregaEmpleadoUsuario;
 var
    oParametros: TZetaParams;
begin
     {Este metodo agrega al empleado Activo como Usuario del Tress}
     with cdsUsuarios do
     begin
          Conectar;
          if NOT Locate('CB_CODIGO;CM_CODIGO',VarArrayOf( [ dmCliente.GetDatosEmpleadoActivo.Numero,dmCliente.Empresa[ P_CODIGO ] ] ),[] ) then
          begin
               oParametros:= TZetaParams.Create;
               try
                  oParametros.VarValues := ServerSistema.GetDatosEmpleadoUsuario(dmCliente.Empresa, dmCliente.cdsEmpleado.Data );

                  if StrLleno( oParametros.ParamByName('Error').AsString ) then
                  begin
                       ZError( 'Creaci�n del Usuario', oParametros.ParamByName('Error').AsString, 0 );
                  end
                  else
                  begin
                       Append;

                       FieldByName('US_CORTO').AsString := oParametros.ParamByName('US_CORTO').AsString;
                       FieldByName('US_NOMBRE').AsString := dmCliente.GetDatosEmpleadoActivo.Nombre;
                       FieldByName('US_PASSWRD').AsString := oParametros.ParamByName('US_PASSWRD').AsString;
                       FieldByName('US_EMAIL').AsString := oParametros.ParamByName('US_EMAIL').AsString;
                       FieldByName('US_DOMAIN').AsString := oParametros.ParamByName('US_DOMAIN').AsString;

                       FieldByName('CM_CODIGO').AsString := dmCliente.Compania;
                       FieldByName('CB_CODIGO').AsInteger := dmCliente.GetDatosEmpleadoActivo.Numero;
                       FieldByName('GR_CODIGO').AsInteger := Global.GetGlobalInteger(ZGlobalTress.K_GLOBAL_ENROLL_GRUPO_DEFAULT);
                       //FieldByName('US_DOMAIN').AsString := Global.GetGlobalString(ZGlobalTress.K_GLOBAL_ENROLL_DOMINIO_AD);
                       FieldByName('US_CAMBIA').AsString := zBoolToStr( Global.GetGlobalBooleano(ZGlobalTress.K_GLOBAL_ENROLL_CAMBIAR_PWD) );

                  end;
               finally
                      FreeAndNil(oParametros);
               end;
          end
          else
              ZInformation( 'Creaci�n del Usuario', Format( 'Ya existe un Usuario asignado al N�mero de Empleado %d' + CR_LF +
                                                            'Si desea un Usuario nuevo hay que borrar el N�mero de Empleado del Usuario #%d',
                                                            [dmCliente.GetDatosEmpleadoActivo.Numero, cdsUsuarios.FieldByName('US_CODIGO').AsInteger]), 0 );
          ModificaUsuarios(FALSE);
          dmCliente.cdsEmpleado.SetDataChange;
     end;
end;

procedure TdmSistema.CargaListaRoles( Lista: TStrings );
begin
     with cdsUserSuper do
          begin
          Data := ServerSistema.GetRoles;
          First;
          with Lista do
          begin
               BeginUpdate;
               Clear;
               while not Eof do
               begin
                    AddObject( FieldByName( 'RO_CODIGO' ).AsString+' = '+FieldByName( 'RO_NOMBRE' ).AsString , TObject( FieldByName( 'RO_NOMBRE' ).AsString ) );
                    Next;
               end;
               EndUpdate;
          end;
     end;
end;

function TdmSistema.BuildEmpresa(const sEmpresa: String):OleVariant;
begin
     with cdsEmpresasPortal do
     begin
          Conectar;
          Locate( 'CM_CODIGO', sEmpresa, [loCaseInsensitive] );
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                          FieldByName( 'CM_USRNAME' ).AsString,
                          FieldByName( 'CM_PASSWRD' ).AsString,
                          Self.Usuario,
                          FieldByName( 'CM_NIVEL0' ).AsString,
                          FieldByName( 'CM_CODIGO' ).AsString, 1, 0 ] );
     end;
     dmReportes.cdsLookupReportesEmpresa.SetDataChange;
     dmReportes.cdsSuscripReportesLookup.SetDataChange;
end;


procedure TdmSistema.DescargaListaUserRoles( Lista: TStrings );
var
   i, iUsuario: Integer;
   sCodigo: String;
begin
     iUsuario := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
     //FTextoBitacora:= 'Antes: '+ GetListaAsignados('CB_NIVEL');
     cdsUserRoles.EmptyDataset;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Integer( Lista.Objects[ i ] ) > 0 ) then
               begin
                    sCodigo := Names[ i ];
                    with cdsUserRoles do
                    begin
                         Append;
                         FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                         FieldByName( 'RO_CODIGO' ).AsString := sCodigo;
                         Post;
                    end;
               end;
          end;
     end;
     cdsUserRoles.Enviar;
end;

procedure TdmSistema.cdsUserRolesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsUserRoles.Data :=  ServerSistema.GetUserRoles(dmCliente.Empresa,cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger);
end;

procedure TdmSistema.cdsUserRolesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oDelta: OleVariant;
begin
     inherited;
     ErrorCount := 0;
     with cdsUserRoles do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             oDelta := Delta
          else
              oDelta := NULL;
     //Reconcile( ServerSistema.GrabaUsuarioRoles(  dmCliente.Usuario,oDelta,ErrorCount) );
     Reconcile( ServerSistema.GrabaUsuarioRoles(  cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger, dmCliente.Usuario, oDelta,ErrorCount) ); //enviar el codigo del usuario que realiza el cambio
     end;
     FTextoBitacora:= VACIO;
end;

procedure TdmSistema.cdsRolesAfterDelete(DataSet: TDataSet);
begin
     RolUsuarioModeloGet( RolGetActivo );
     cdsRolUsuarios.EmptyDataSet;
     inherited;
     cdsRoles.Enviar;
end;

procedure TdmSistema.cdsRolesAlModificar(Sender: TObject);
begin
     inherited;
     FEditing := True;
     try
        RolUsuarioModeloGet( RolGetActivo );
        ZBaseEdicion_DevEx.ShowFormaEdicion( CatRolesEdit_DevEx, TCatRolesEdit_DevEx );
     finally
            FEditing := False;
     end;
end;

procedure TdmSistema.cdsRolesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     inherited;
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_ROLES, iRight );
end;

function TdmSistema.RolGetActivo: String;
begin
     Result := cdsRoles.FieldByName( 'RO_CODIGO' ).AsString;
end;

procedure TdmSistema.RolUsuarioModeloGet( const sRol: String );
var
   Modelos: OleVariant;
begin
     cdsRolUsuarios.Data := ServerSistema.GetUsuariosRol( sRol, Modelos );
     //cdsRolModelos.Data := Modelos;
end;

procedure TdmSistema.RolUsuarioAgregar( const iUsuario: Integer; const sNombre: String );
var
   sRol: String;
begin
     sRol := RolGetActivo;
     if ZetaCommonTools.StrVacio( sRol ) then
        ZetaDialogo.zError( '� C�digo De Rol Inexistente !', 'Antes De Agregar Usuarios Es Necesario Especificar El C�digo Del Rol', 0 )
     else
     begin
          with cdsRolUsuarios do
          begin
               if not Locate( 'RO_CODIGO;US_CODIGO', VarArrayOf( [ sRol, iUsuario ] ), [] ) then
               begin
                    Append;
                    FieldByName( 'RO_CODIGO' ).AsString := sRol;
                    FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                    FieldByName( 'US_NOMBRE' ).AsString := sNombre;
                    Post;
               end;
          end;
          RolEditar;
     end;
end;

procedure TdmSistema.RolUsuarioAgregarTodos;
begin
     with cdsRolUsuarios do

     begin
          DisableControls;
          try
             with dmSistema do

             begin
                  ConectarUsuariosLookup;
                  with cdsUsuariosLookup do
                  begin
                       Conectar;
                       DisableControls;
                       try
                          First;
                          while not Eof do
                          begin
                               RolUsuarioAgregar( FieldByName( 'US_CODIGO' ).AsInteger, FieldByName( 'US_NOMBRE' ).AsString );
                               Next;
                          end;
                       finally
                              EnableControls;
                       end;
                  end;
             end;
             First;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmSistema.RolUsuarioBorrar;
begin
     with cdsRolUsuarios do
     begin
          Delete;
     end;
     RolEditar;
end;

procedure TdmSistema.RolUsuarioBorrarTodos;
begin
     with cdsRolUsuarios do
     begin
          EmptyDataset;
     end;
     RolEditar;
end;

procedure TdmSistema.RolEditar;
begin
     with cdsRoles do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

function TdmSistema.BuscarUsuario( const sFiltro: String; var iUsuario: Integer; var sNombre: String ): Boolean;
var
   sUsuario: String;

begin
     ConectarUsuariosLookup;
     Result := cdsUsuariosLookup.Search_DevEx( sFiltro, sUsuario, sNombre );
     if Result then
     begin
          iUsuario := StrToIntDef( sUsuario, 0 );
          Result := ( iUsuario > 0 );
     end;
end;

procedure TdmSistema.ConectarUsuariosLookup;
begin
     cdsUsuarios.Conectar;
end;

procedure TdmSistema.cdsRolesAfterCancel(DataSet: TDataSet);
begin
     RolUsuarioModeloGet( RolGetActivo );
end;

procedure TdmSistema.cdsRolesAfterScroll(DataSet: TDataSet);
begin
     if FEditing then
        RolUsuarioModeloGet( RolGetActivo );
end;

procedure TdmSistema.CargaListaRolesDefault( Lista: TStrings  );
var
   sRoles: string;
   i,iUsuario:Integer;
begin
     sRoles := Global.GetGlobalString(ZGlobalTress.K_GLOBAL_ENROLL_ROLES_DEFAULT) +  Global.GetGlobalString(ZGlobalTress.K_GLOBAL_ENROLL_ROLES_DEFAULT2);
     Lista.CommaText := sRoles;
     cdsUserRoles.Conectar;
     iUsuario := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
     with Lista do
     begin
          for i := 0 to ( Count - 2 ) do
          begin
               with cdsUserRoles do
               begin
                     Append;
                     FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                     FieldByName( 'RO_CODIGO' ).AsString := Lista[i];
                     Post;
               end;
          end;
     end;
     cdsUserRoles.Enviar;
end;

//Desactivar usuario
//Davol #Bug: 21992 //#Bug: 15995
procedure TdmSistema.BajaUsuario(const iEmpleado:Integer;const lQuitarUsuario:Boolean);
var
   oDatos:  OleVariant;
begin
     if Not SetBajaUsuario(iEmpleado, dmCliente.Empresa[ P_CODIGO ],lQuitarUsuario) then
     begin
          oDatos := ServerSistema.GetUserConfidencialidad( iEmpleado, dmCliente.Empresa[ P_CODIGO ] );
          if StrLleno(oDatos) then
             if Not SetBajaUsuario(iEmpleado, oDatos, lQuitarUsuario ) then
                GrabaEmpleadoUsuario(iEmpleado,0)
          else
              GrabaEmpleadoUsuario(iEmpleado,0);
     end;

     dmCliente.cdsEmpleado.SetDataChange;
end;

//Davol #Bug: 21992 //#Bug: 15995
function TdmSistema.SetBajaUsuario(iEmpleado : Integer; sEmpresa : String;const lQuitarUsuario:Boolean): Boolean;
begin
     Result := False;
     with cdsUsuarios do
     begin
          Filtered := False;
          Conectar;
          if Locate('CB_CODIGO;CM_CODIGO',VarArrayOf( [ iEmpleado, sEmpresa ] ),[] )then
          begin
               Result := True;
               Edit;
               FieldByName('US_ACTIVO').AsString := K_GLOBAL_NO;
               if( lQuitarUsuario )then
               begin
                    FieldByName('CB_CODIGO').AsInteger := 0;
                    FieldByName('CM_CODIGO').AsString := VACIO;
               end;
               Enviar;
          end;

          Filtered := True;
     end;
end;

procedure TdmSistema.GrabaEmpleadoUsuario(iEmpleado,iUsuario:Integer);
begin
     ServerSistema.GrabaEmpleadoUsuario(dmCliente.Empresa,iUsuario,iEmpleado);
end;

procedure TdmSistema.cdsUsuariosAlEnviarDatos(Sender: TObject);
var
   iOldEmpleado: integer;
begin
     iOldEmpleado := cdsUsuarios.FieldByName('CB_CODIGO').OldValue;
     inherited;
     if ( cdsUsuarios.ChangeCount = 0 ) and ( iOldEmpleado <> cdsUsuarios.FieldByName('CB_CODIGO').AsInteger ) then
     begin
          with dmCliente.cdsEmpleado do
          begin
               if(dmCliente.cdsEmpleado.Active) then
               begin
                    if ( FieldByName('CB_CODIGO').AsInteger in [iOldEmpleado,cdsUsuarios.FieldByName('CB_CODIGO').AsInteger]) then
                    begin
                          Edit;
                          FieldByName('US_CODIGO').AsInteger := cdsUsuarios.FieldByName('US_CODIGO').AsInteger;
                          Post;
                          MergeChangeLog;
                    end;
               end;
          end;
     end;
end;

procedure TdmSistema.GetListaClasifiEmpresa( oLista : TStrings );
const
     crSinRestriccion = MaxInt;
     K_MEN_SIN_RESTRICCION = 'Sin Restricci�n';
begin
       {Aqui se conecta cdsClasifiRepEmp de acuerdo a la Empresa}
       with oLista do
       begin
            Clear;
            AddObject( K_MEN_SIN_RESTRICCION , TObject( crSinRestriccion ));
            try
                cdsClasifiRepEmp.Refrescar;
                with cdsClasifiRepEmp do
                begin
                     First;
                     while NOT EOF do
                     begin
                          AddObject( FieldByName('RC_NOMBRE').AsString, TObject( FieldByName('RC_CODIGO').AsInteger ) );
                          Next;
                     end;
                end;

            except
            end;
       end;
end;

procedure TdmSistema.cdsClasifiRepEmpAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     //Se utiliza cdsEmpresasPortal porque cdsEmpresas trae las claves de la BD ya decodificadas.

     cdsEmpresasPortal.Conectar;
     try
        cdsClasifiRepEmp.Data:= ServerSistema.GetClasifiEmpresa( BuildEmpresaPortal( cdsEmpresas.FieldByName('CM_CODIGO').AsString ) );
     except
     end;
end;

procedure TdmSistema.SetSemanaCalendario(const Dia: eDiasCafeteria);
const
     K_FILTRO_SEMANA = 'SEMANA = %d';
begin
     FSemanaCalendario := Dia;
     with cdsCalendarioCafeteria do begin
          Filter   := Format(K_FILTRO_SEMANA, [Ord(Dia)]);
          Filtered := True;
          First;
     end;
end;

procedure TdmSistema.QuitaFiltro;
begin
     with cdsCalendarioCafeteria do begin
          Filter   := VACIO;
          Filtered := False;
     end;
end;

procedure TdmSistema.cdsConfiguracionCafeteriaAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsConfiguracionCafeteria.Data := ServerSistema.GetConfigCafeteria( CafIdentificador, FCalendarioBuffer);
     with cdsCalendarioCafeteria do
     begin
          Data := FCalendarioBuffer;
     end;
end;

procedure TdmSistema.cdsConfiguracionCafeteriaAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsConfiguracionCafeteria do
     begin
          ListaFija( 'CF_CHECK_S', lfChecadasSimultaneas );
     end;
end;

procedure TdmSistema.cdsConfiguracionCafeteriaAlEnviarDatos(Sender: TObject);
var
   iChangeCountCalend, iChangeCountConfig, iCalend, iConfig: Integer;
   oConfig, oCalend, CalendarioResults: OleVariant;
begin
     with cdsConfiguracionCafeteria do
     begin
          PostData;
          iConfig := ChangeCount;
          oConfig := DeltaNull;
     end;

     with cdsCalendarioCafeteria do
     begin
          PostData;
          iCalend := ChangeCount;
          oCalend := DeltaNull;
     end;

     if ( iConfig > 0 ) or ( iCalend > 0 ) then
     begin
          iChangeCountConfig := iConfig;
          iChangeCountCalend := iCalend;

          oConfig := ServerSistema.GrabaConfigCafeteria( dmCliente.Empresa, oConfig, oCalend, iChangeCountConfig, iChangeCountCalend, CalendarioResults );

          if ( ( iConfig = 0 ) or cdsConfiguracionCafeteria.Reconcile( oConfig ) ) and
             ( ( iCalend = 0 ) or cdsCalendarioCafeteria.Reconcile( CalendarioResults ) ) then  //REVISAR CONDICIONES
          begin
               cdsConfiguracionCafeteria.Refrescar;
          end
          else
          begin
               with cdsConfiguracionCafeteria do
               begin
                    if ( ChangeCount > 0 ) or ( cdsCalendarioCafeteria.ChangeCount > 0 ) then
                         Edit
               end;
          end;
     end;
end;

procedure TdmSistema.cdsConfiguracionCafeteriaBeforePost(DataSet: TDataSet);
const
     K_MIN_LIMPIAR_PANTALLA = 3;
begin
     inherited;
     with cdsConfiguracionCafeteria do
     begin
          if StrVacio(FieldByName('CF_NOMBRE').AsString ) then
          begin
               FieldByName( 'CF_NOMBRE' ).FocusControl;
               DataBaseError( 'El nombre de la Estaci�n no debe quedar vacio' );
          end
          else
          begin
               if FieldByName('CF_T_ESP').AsInteger < K_MIN_LIMPIAR_PANTALLA then
               begin
                    FieldByName('CF_T_ESP').FocusControl;
                    DataBaseError( Format ('El valor para limpiar pantalla no debe ser menor a %d segundos', [K_MIN_LIMPIAR_PANTALLA]) )
               end
               else
                    FieldByName('CF_CAMBIOS').AsString := K_GLOBAL_SI
          end;
          {$ifdef TRESS_DELPHIXE5_UP}
          FieldByName('CF_VELOCI').AsInteger := FormaConfigura.tbVelocidad_DevEx.Position;
          FieldByName('CF_TIP_COM').AsInteger := StrToInt ( FormaConfigura.editTipo.Text );
          FieldByName('CF_TECLADO').AsString := zBoolToStr (FormaConfigura.cbUsaTeclado.CheckBox.Checked);
          {$endif}
     end;
end;

procedure TdmSistema.cdsConfiguracionCafeteriaNewRecord(DataSet: TDataSet);
begin
     inherited;
     cdsConfiguracionCafeteria.FieldByName('CF_NOMBRE').AsString := cdsSistLstDispositivos.FieldByName('DI_NOMBRE').AsString;
     cdsConfiguracionCafeteria.FieldByName('DI_TIPO').AsInteger := ord(dpCafeteria);
     cdsConfiguracionCafeteria.FieldByName('CF_TIP_COM').AsInteger := 1;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_1').AsString := K_TCOMIDA_1;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_2').AsString := K_TCOMIDA_2;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_3').AsString := K_TCOMIDA_3;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_4').AsString := K_TCOMIDA_4;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_5').AsString := K_TCOMIDA_5;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_6').AsString := K_TCOMIDA_6;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_7').AsString := K_TCOMIDA_7;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_8').AsString := K_TCOMIDA_8;
     cdsConfiguracionCafeteria.FieldByName('CF_TCOM_9').AsString := K_TCOMIDA_9;
     cdsConfiguracionCafeteria.FieldByName('CF_FOTO').AsString := K_GLOBAL_SI;
     cdsConfiguracionCafeteria.FieldByName('CF_COM').AsString := K_GLOBAL_SI;
     cdsConfiguracionCafeteria.FieldByName('CF_SIG_COM').AsString := K_GLOBAL_SI;
     cdsConfiguracionCafeteria.FieldByName('CF_TECLADO').AsString := K_GLOBAL_NO;
     cdsConfiguracionCafeteria.FieldByName('CF_PRE_TIP').AsString := K_GLOBAL_NO;
     cdsConfiguracionCafeteria.FieldByName('CF_PRE_QTY').AsString := K_GLOBAL_NO;
     cdsConfiguracionCafeteria.FieldByName('CF_REINICI').AsString := K_GLOBAL_NO;
     cdsConfiguracionCafeteria.FieldByName('CF_A_PRINT').AsString := K_GLOBAL_SI;
     cdsConfiguracionCafeteria.FieldByName('CF_A_CANC').AsString := K_GLOBAL_SI;
     cdsConfiguracionCafeteria.FieldByName('CF_TECLADO').AsString := K_GLOBAL_SI;
     cdsConfiguracionCafeteria.FieldByName('CF_GAFETE').AsString := K_GAFETE; //Seguridad: Gafete
     cdsConfiguracionCafeteria.FieldByName('CF_BANNER').AsString := K_BANNER; //Banner
     cdsConfiguracionCafeteria.FieldByName('CF_T_ESP').AsInteger := 60; //Tiempo de Espera
     cdsConfiguracionCafeteria.FieldByName('CF_VELOCI').AsInteger := 5; //Velocidad
     cdsConfiguracionCafeteria.FieldByName('CF_CHECK_S').AsInteger := 0; //Checada Simultanea
end;

{******************************************************************************}
{$ifdef DOS_CAPAS}
procedure TdmSistema.cdsClasificacionesAccesosAlEnviarDatos(Sender: TObject);
begin
     //No borrar
end;

procedure TdmSistema.cdsEntidadesAccesosAlEnviarDatos(Sender: TObject);
begin
     //No borrar
end;

{procedure TdmSistema.cdsAdicionalesAccesosAlEnviarDatos(Sender: TObject);
begin
     //No borrar
end;}

function TdmSistema.BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

function TdmSistema.BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

{function TdmSistema.BuscaDerechoAdicionales(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;}

function TdmSistema.BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

function TdmSistema.BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;


{$else}

function TdmSistema.GetServerDiccionario: IdmServerDiccionarioDisp;
begin
     Result := IdmServerDiccionarioDisp(dmCliente.CreaServidor( {$ifdef RDD}CLASS_dmServerReporteadorDD{$else}CLASS_dmServerDiccionario{$endif}, FServidor ));
end;

function TdmSistema.BuscaAccesosEntidades(const iPosition: Integer): Boolean;
begin
     Result := cdsEntidadesAccesos.Locate( 'CM_CODIGO;GR_CODIGO;EN_CODIGO',
                                VarArrayOf( [ cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString,
                                cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger,
                                iPosition ] ), [] );
end;

function TdmSistema.BuscaAccesosClasificaciones(const iPosition: Integer): Boolean;
begin
     Result := cdsClasificacionesAccesos.Locate( 'CM_CODIGO;GR_CODIGO;RC_CODIGO',
                                                 VarArrayOf( [ cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString,
                                                               cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger,
                                                               iPosition ] ), [] )
end;


function TdmSistema.BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
      if BuscaAccesosEntidades( iPosition ) then
      begin
           Result := cdsEntidadesAccesos.FieldByName( 'RE_DERECHO' ).AsInteger
      end
      else
         Result := K_SIN_DERECHOS;
end;

function TdmSistema.BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
      if BuscaAccesosClasificaciones( iPosition ) then
      begin
           Result := cdsClasificacionesAccesos.FieldByName( 'RA_DERECHO' ).AsInteger
      end
      else
         Result := K_SIN_DERECHOS;
end;

function TdmSistema.BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
     {$ifdef ANTES}
     if  cdsCopiarAccesos.Locate( 'CM_CODIGO;EN_CODIGO',
                                   VarArrayOf( [ cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString,
                                               iPosition ] ), [] ) then
     {$else}
     {CV: Correcion defecto 1731}
     if  cdsCopiarAccesos.Locate( 'EN_CODIGO',
                                   VarArrayOf( [ iPosition ] ), [] ) then
     {$endif}
     begin
          Result := cdsCopiarAccesos.FieldByName( 'RE_DERECHO' ).AsInteger
     end
     else
        Result := K_SIN_DERECHOS;
end;

function TdmSistema.BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
      {$ifdef ANTES}
      if  cdsCopiarAccesos.Locate( 'CM_CODIGO;RC_CODIGO',
                                   VarArrayOf( [ cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString,
                                               iPosition ] ), [] ) then
      {$else}
      {CV: Correcion defecto 1731}
      if  cdsCopiarAccesos.Locate( 'RC_CODIGO',
                                   VarArrayOf( [ iPosition ] ), [] ) then
      {$endif}
      begin
           Result := cdsCopiarAccesos.FieldByName( 'RA_DERECHO' ).AsInteger
      end
      else
         Result := K_SIN_DERECHOS;
end;



procedure TdmSistema.cdsEntidadesAccesosAlEnviarDatos( Sender: TObject);
var
   ErrorCount: Integer;
   sCodigo: string;
begin
     ErrorCount := 0;
     with cdsEntidadesAccesos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if Reconcile( ServerDiccionario.GrabaAccesosEntidad( GetEmpresaAccesos, cdsGrupos.FieldByName('GR_CODIGO').AsInteger, Delta, ErrorCount ) ) then
             begin
                  sCodigo := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                  cdsEmpresasAccesos.Refrescar;
                  cdsEmpresasAccesos.Locate( 'CM_CODIGO', sCodigo, [ loCaseInsensitive ] );
             end;
     end;
end;


procedure TdmSistema.cdsClasificacionesAccesosAlEnviarDatos( Sender: TObject);
var
   ErrorCount: Integer;
   sCodigo: string;
begin
     ErrorCount := 0;
     with cdsClasificacionesAccesos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if Reconcile( ServerDiccionario.GrabaAccesosClasifi( GetEmpresaAccesos, cdsGrupos.FieldByName('GR_CODIGO').AsInteger, Delta, ErrorCount ) ) then
             begin
                  sCodigo := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                  cdsEmpresasAccesos.Refrescar;
                  cdsEmpresasAccesos.Locate( 'CM_CODIGO', sCodigo, [ loCaseInsensitive ] );
             end;
     end;
end;




{$ENDIF}

procedure TdmSistema.CopyAccesos( iGrupo: Integer; sCompany: String );
 var
    oEmpresa: Olevariant;
begin
     case ArbolSeleccionado of
          adTress:
          begin
               Inherited CopyAccesos( iGrupo, sCompany );
          end;
          adAdicionales:
          begin
               {CV: Correcion defecto 1731}
               oEmpresa := GetEmpresaAccesos(sCompany);
               cdsCopiarAccesos.IndexFieldNames := '';
               cdsCopiarAccesos.Data := ServerSistema.GetDerechosAdicionales( oEmpresa, iGrupo );
               cdsCopiarAccesos.IndexFieldNames := 'GX_CODIGO';
          end;
          {$ifdef RDD}
          adClasifi:
          begin
               {CV: Correcion defecto 1731}
               oEmpresa := GetEmpresaAccesos( sCompany );
               cdsCopiarAccesos.Data := ServerDiccionario.GetDerechosClasifi( oEmpresa, iGrupo );
               cdsCopiarAccesos.IndexFieldNames := 'RC_CODIGO';
          end;
          adEntidades:
          begin
               {CV: Correcion defecto 1731}
               oEmpresa := GetEmpresaAccesos(sCompany);
               cdsCopiarAccesos.Data := ServerDiccionario.GetDerechosEntidades( oEmpresa, iGrupo );
               cdsCopiarAccesos.IndexFieldNames := 'EN_CODIGO';
          end;
          {$endif}
      end;


end;

procedure TdmSistema.CopiaDerechosAdicionales;
begin
     with cdsAdicionalesAccesos do
     begin
          First;
          while NOT EOF do
          begin
               Edit;
               FieldByName( 'GX_DERECHO' ).AsInteger := 0;
               Next;
          end;
     end;

     with cdsCopiarAccesos do
     begin
          First;
          while NOT EOF do
          begin
               if cdsAdicionalesAccesos.Locate('GX_CODIGO', FieldByName('GX_CODIGO').AsString, [] ) then
               begin
                    if ( FieldByName( 'GX_DERECHO' ).AsInteger <> cdsAdicionalesAccesos.FieldByName( 'GX_DERECHO' ).AsInteger ) then
                    begin
                         cdsAdicionalesAccesos.Edit;
                         cdsAdicionalesAccesos.FieldByName( 'GX_DERECHO' ).AsInteger := FieldByName( 'GX_DERECHO' ).AsInteger;
                         cdsAdicionalesAccesos.Post;
                    end;
               end
               else
               begin
                    cdsAdicionalesAccesos.Append;
                    cdsAdicionalesAccesos.FieldByName( 'GR_CODIGO' ).AsInteger := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
                    cdsAdicionalesAccesos.FieldByName( 'CM_CODIGO' ).AsString := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                    cdsAdicionalesAccesos.FieldByName( 'GX_CODIGO' ).AsString := FieldByName( 'GX_CODIGO' ).AsString;
                             //FieldByName( 'LLAVE' ).AsInteger := iPosition;//Es el campo llave
                    cdsAdicionalesAccesos.FieldByName( 'GX_DERECHO' ).AsInteger := FieldByName( 'GX_DERECHO' ).AsInteger;
                    cdsAdicionalesAccesos.Post;
               end;
               Next;
          end;
     end;
end;

function TdmSistema.BuscaDerechoCopiadoAdicionales(const iPosition: Integer ): TDerechosAcceso;
begin
     if  cdsCopiarAccesos.Locate( 'GX_CODIGO',
                                  VarArrayOf( [ iPosition ] ), [] ) then
      begin
           Result := cdsCopiarAccesos.FieldByName( 'GX_DERECHO' ).AsInteger
      end
      else
         Result := K_SIN_DERECHOS;

end;

function TdmSistema.BuscaDerechoAdicionales(const iPosition: Integer): TDerechosAcceso;
begin
      if BuscaAccesosAdicionales( iPosition ) then
      begin
           Result := cdsAdicionalesAccesos.FieldByName( 'GX_DERECHO' ).AsInteger
      end
      else
         Result := K_SIN_DERECHOS;
end;

function TdmSistema.GetcdsAccesos : TZetaClientDataSet;
begin
      case ArbolSeleccionado of
           adTress: Result:= cdsAccesosBase;
           {$ifdef RDD}
           adClasifi: Result := cdsClasificacionesAccesos;
           adEntidades: Result:= cdsEntidadesAccesos;
           {$endif}
           adAdicionales: Result:= cdsAdicionalesAccesos;
           else
               Result := nil;
      end;
end;

procedure TdmSistema.cdsAdicionalesAccesosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   sCodigo: string;
begin
     ErrorCount := 0;
     with cdsAdicionalesAccesos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if Reconcile( ServerSistema.GrabaAccesosAdicionales( GetEmpresaAccesos, cdsGrupos.FieldByName('GR_CODIGO').AsInteger, Delta, ErrorCount ) ) then
             begin
                  sCodigo := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                  cdsEmpresasAccesos.Refrescar;
                  cdsEmpresasAccesos.Locate( 'CM_CODIGO', sCodigo, [ loCaseInsensitive ] );
             end;
     end;
end;

function TdmSistema.BuscaAccesosAdicionales(const iPosition: Integer): Boolean;
begin
     Result := cdsAdicionalesAccesos.Locate( 'CM_CODIGO;GR_CODIGO;LLAVE',
                                              VarArrayOf( [ cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString,
                                                            cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger,
                                                            iPosition - K_IMAGENINDEX_LIMIT ] ), [] );
end;


procedure TdmSistema.ConectaAccesos( var sGroupName, sCompanyName: String );
{$ifdef RDD}
var
   iGrupo: Integer;
   sCompany: String;
{$endif}
begin
{$ifdef RDD}
     if ArbolSeleccionado = adTress then
{$endif}
        inherited ConectaAccesos( sGroupName, sCompanyName )
{$ifdef RDD}
     else
     begin


          with cdsGrupos do
          begin
               iGrupo := FieldByName( 'GR_CODIGO' ).AsInteger;
               sGroupName := FieldByName( 'GR_DESCRIP' ).AsString;
          end;
          with cdsEmpresasAccesos do
          begin
               sCompany := FieldByName( 'CM_CODIGO' ).AsString;
               sCompanyName := FieldByName( 'CM_NOMBRE' ).AsString;
          end;

          cdsClasificacionesAccesos.Data := ServerDiccionario.GetDerechosClasifi( GetEmpresaAccesos, iGrupo );
          cdsEntidadesAccesos.Data := ServerDiccionario.GetDerechosEntidades( GetEmpresaAccesos, iGrupo );
     end;
{$endif}
end;

procedure TdmSistema.ConectaAccesosAdicionales;
begin
     cdsAdicionalesAccesos.Data := ServerSistema.GetDerechosAdicionales( GetEmpresaAccesos, cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger );
end;

procedure TdmSistema.GrabaDerecho( const iPosition: Integer; const iDerecho: TDerechosAcceso; oNodo: TTreeNode; oTextoBitacora: TGetTextoBitacora);
begin
     case ArbolSeleccionado of
          adTress : inherited GrabaDerecho( iPosition, iDerecho, oNodo, oTextoBitacora );
          adAdicionales:
          begin
               with cdsAccesos do
               begin
                    if BuscaAccesosAdicionales( iPosition ) then
                    begin
                         if ( FieldByName( 'GX_DERECHO' ).AsInteger <> iDerecho ) then
                         begin
                              Edit;
                              FieldByName( 'GX_DERECHO' ).AsInteger := iDerecho;
                              Post;
                         end;
                    end
                    else
                    begin

                        if cdsGruposAdic.Locate('GX_TITULO', oNodo.Text, []) then
                        begin
                             Append;
                             FieldByName( 'GR_CODIGO' ).AsInteger := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
                             FieldByName( 'CM_CODIGO' ).AsString := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                             FieldByName( 'GX_CODIGO' ).AsString := cdsGruposAdic.FieldByName( 'GX_CODIGO' ).AsString;
                             //FieldByName( 'LLAVE' ).AsInteger := iPosition;//Es el campo llave
                             FieldByName( 'GX_DERECHO' ).AsInteger := iDerecho;
                             Post;
                        end
                        else
                            DatabaseError('Error al grabar los derechos de acceso de datos adicionales');

                    end;
               end
          end;
{$ifdef RDD}
          adClasifi :
          begin
               with cdsAccesos do
               begin
                    if BuscaAccesosClasificaciones( iPosition ) then
                    begin
                         if ( FieldByName( 'RA_DERECHO' ).AsInteger <> iDerecho ) then
                         begin
                              Edit;
                              FieldByName( 'RA_DERECHO' ).AsInteger := iDerecho;
                              Post;
                         end;
                    end
                    else
                    begin
                        Append;
                        FieldByName( 'GR_CODIGO' ).AsInteger := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
                        FieldByName( 'CM_CODIGO' ).AsString := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                        FieldByName( 'RC_CODIGO' ).AsInteger := iPosition;
                        FieldByName( 'RA_DERECHO' ).AsInteger := iDerecho;
                        Post;
                    end;
               end
          end;
          adEntidades :
          begin
               with cdsAccesos do
               begin
                    if BuscaAccesosEntidades( iPosition ) then
                    begin
                         if ( FieldByName( 'RE_DERECHO' ).AsInteger <> iDerecho ) then
                         begin
                              Edit;
                              FieldByName( 'RE_DERECHO' ).AsInteger := iDerecho;
                              Post;
                         end;
                    end
                    else
                    begin
                         Append;
                         FieldByName( 'GR_CODIGO' ).AsInteger := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
                         FieldByName( 'CM_CODIGO' ).AsString := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                         FieldByName( 'EN_CODIGO' ).AsInteger := iPosition;
                         FieldByName( 'RE_DERECHO' ).AsInteger := iDerecho;
                         Post;
                    end;
               end;
          end;
{$endif}
     end;

end;

procedure TdmSistema.cdsRolesBeforePost(DataSet: TDataSet);
begin
     with cdsRoles do
     begin
          FieldByName('RO_CODIGO').AsString := UpperCase( FieldByName('RO_CODIGO').AsString);
     end;

end;


procedure TdmSistema.cdsSistLstDispositivosAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsSistLstDispositivos.Data := ServerSistema.GetSistLstDispositivos( dmCliente.Empresa );
end;

procedure TdmSistema.cdsSistLstDispositivosAlBorrar(Sender: TObject);
Const
     K_ADVERTENCIA_CAFE = ' Este dispositivo es de tipo cafeter�a, al eliminarlo'+CR_LF+
                          ' tambi�n se borrar� su configuraci�n y calendario. ' + CR_LF +
                          '� Desea continuar ?';
     K_ADVERTENCIA_GENERAL = '� Desea Borrar Este Registro ?';
     K_TIPO_CAFE = 1;
var
     sAdvertencia : String;
begin
  inherited;
  sAdvertencia := K_ADVERTENCIA_GENERAL;
  with cdsSistLstDispositivos do
  begin
       if (FieldByName('DI_TIPO').AsInteger = ord(dpCafeteria)) and (FieldByName('STATUS').AsInteger = ord(tConfigurada)) then
          sAdvertencia := K_ADVERTENCIA_CAFE;
       if ( ZetaMsgDlg.ConfirmaCambio( sAdvertencia ) ) then
          Delete;
  end;
end;

procedure TdmSistema.cdsSistLstDispositivosAlCrearCampos(Sender: TObject);
begin
     inherited;
     cdsSistLstDispositivos.ListaFija('DI_TIPO', lfLstDispositivos);
     cdsSistLstDispositivos.ListaFija('STATUS', lfStatusSistDispositivos);
end;

procedure TdmSistema.cdsSistLstDispositivosNewRecord(DataSet: TDataSet);
begin
     inherited;
     cdsSistLstDispositivos.FieldByName('DI_TIPO').AsInteger := ord(dpRelojChecados);
end;

procedure TdmSistema.cdsSistLstDispositivosReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
  var Action: TReconcileAction);
begin
  inherited;
   Action := ZReconcile.HandleReconcileError(DataSet, UpdateKind, E);
end;

procedure TdmSistema.cdsSistLstDispositivosAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditSistLstDispositivos_DevEx, TEditSistLstDispositivos_DevEx);
end;

procedure TdmSistema.cdsSistLstDispositivosBeforePost(DataSet: TDataSet);
begin
     inherited;
     with cdsSistLstDispositivos do
     begin
          if ( not StrLleno( FieldByName( 'DI_NOMBRE' ).AsString ) ) then
          begin
               FieldByName( 'DI_NOMBRE' ).FocusControl;
               DataBaseError( 'Identificador no puede quedar vac�o' );
          end;

          if FieldByName('DI_TIPO').AsInteger = ord(dpCafeteria) then
          begin
               if FieldByName('STATUS').AsInteger <> ord(tConfigurada) then
                  FieldByName('STATUS').AsInteger := Ord(tNoConfigurada)
          end
          else
              FieldByName('STATUS').AsInteger := Ord(tNoAplica);
     end;
end;

procedure TdmSistema.cdsSistLstDispositivosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     inherited;
     ErrorCount := 0;
     with cdsSistLstDispositivos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerSistema.GrabaSistLstDispositivos( dmCliente.Empresa, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmSistema.cdsSistLstDispositivosAfterDelete(DataSet: TDataSet);
begin
     inherited;
     cdsSistLstDispositivos.Enviar;
end;

procedure TdmSistema.ActualizaStatus;
var TipoActual, StatusActual:Integer;
begin
        with cdsSistLstDispositivos do
        begin
             if State <> dsEdit then
                Edit;
             StatusActual := FieldByName('STATUS').AsInteger;
             TipoActual := FieldByName('DI_TIPO').AsInteger;
             if TipoActual = Ord(dpCafeteria) then
             begin
                  if StatusActual = Ord(tNoAplica) then
                     FieldByName('STATUS').AsInteger := Ord(tNoConfigurada)
                  else
                     FieldByName('STATUS').AsInteger := Ord(tConfigurada); //status = no configurada
             end
             else
             begin //Es un tipo disntinto a cafeteria
                   FieldByName('STATUS').AsInteger := Ord(tNoAplica); //registro que no es de cafeteria
             end;
             Post;
             MergeChangeLog;
        end;
end;

procedure TdmSistema.GetListaDispositivos;
var
    Disponibles, Asignados: Olevariant;
begin
     ServerSistema.GetGlobalLstDispositivos( dmCliente.Empresa, Disponibles, Asignados );
     cdsLstDispDisponibles.Data := Disponibles;
     cdsLstDispAsignados.Data := Asignados;
end;

procedure TdmSistema.cdsLstDispAsignadosReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     inherited;
     Action := ZReconcile.HandleReconcileError(DataSet, UpdateKind, E);
end;

procedure TdmSistema.PreparaListaDispxCom;
begin
     cdsLstDispAsignados.EmptyDataset;
end;

procedure TdmSistema.LlenacdsLstDispAsignados( oLista: TStrings );
var
   i: Integer;
begin
     with cdsLstDispAsignados do
     begin
           for i := 0 to ( oLista.Count ) -1  do
           begin
                cdsLstDispAsignados.append;
                cdsLstDispAsignados.FieldByName('CM_CODIGO').AsString := dmCliente.GetDatosEmpresaActiva.Codigo;
                cdsLstDispAsignados.FieldByName('DI_NOMBRE').AsString := oLista.Names[i];
                cdsLstDispAsignados.FieldByName('DI_TIPO').AsInteger := Integer( oLista.Objects[i] );
                Post;                
           end;
     end;
end;

procedure TdmSistema.GrabaDispxCom;
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsLstDispAsignados do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
             Reconciliar( ServerSistema.GrabaGlobalLstDispositivos( dmCliente.Empresa, DeltaNULL, ErrorCount ) );
     end;
end;

procedure TdmSistema.cdsACCArbolAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsAccArbol.Data := ServerSistema.GetAccArbol(dmCliente.Empresa);
end;

procedure TdmSistema.cdsUsuariosAlBorrar(Sender: TObject);
const
   K_ADV_BORRAR_REGISTRO = '� Desea Borrar Este Registro ?';
{$ifdef COMPARTE_MGR}
   K_ADV_SUPERVISORES ='Este usuario puede pertenecer a un Supervisor Enrolado. Si borra este usuario puede quedar alg�n Supervisor sin enrolar.�Est� seguro que desea borrar este usuario?';
{$else}
   K_ADV_SUPERVISORES = 'Este usuario puede pertenecer a un Supervisor Enrolado en esta o en alguna otra de sus empresas. Si borra este usuario puede quedar alg�n Supervisor sin enrolar.�Est� seguro que desea borrar este usuario?';
{$endif}
var
   sAdvertencia : string;
begin
    sAdvertencia := K_ADV_BORRAR_REGISTRO;

   {$ifdef COMPARTE_MGR}
    sAdvertencia := K_ADV_SUPERVISORES;
   {$endif}

   {$ifndef DOS_CAPAS}
   {$ifdef TRESS}
   if (dmCliente.EmpresaAbierta) and Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
      sAdvertencia := K_ADV_SUPERVISORES;
   {$endif}
   {$endif}
   if ( ZetaMsgDlg.ConfirmaCambio( sAdvertencia ) ) then
       cdsUsuarios.Delete;
end;

procedure TdmSistema.cdsCalendarioCafeteriaACCIONGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
          begin
               if Sender.AsInteger <= 8 then
                  Text := cdsConfiguracionCafeteria.FieldByName('CF_TCOM_' + IntToStr(Sender.AsInteger + 1)).AsString//ZetaRegistryCafe.CafeRegistry.TextoTipoComida(Sender.AsInteger + 1) { OP:21.Mayo.08 }
               else
               begin
                    Text := ObtieneElemento(lfCafeCalendario, Sender.AsInteger)
               end;
          end;
     end
     else
         Text := Sender.AsString;
end;

procedure TdmSistema.cdsCalendarioCafeteriaAfterDelete(DataSet: TDataSet);
begin
     inherited;
     with cdsConfiguracionCafeteria do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmSistema.cdsCalendarioCafeteriaAfterInsert(DataSet: TDataSet);
begin
     inherited;
     with cdsConfiguracionCafeteria do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmSistema.cdsCalendarioCafeteriaAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsCalendarioCafeteria do
     begin
          MaskTime( 'HORA' );
     end;
end;

procedure TdmSistema.cdsCalendarioCafeteriaBeforePost(DataSet: TDataSet);
begin
     inherited;
     with cdsCalendarioCafeteria do
     begin
          FieldByName( 'CF_NOMBRE' ).AsString := cdsConfiguracionCafeteria.FieldByName( 'CF_NOMBRE' ).AsString;
          FieldByName( 'DI_TIPO' ).AsInteger := ord(dpCafeteria);
          FieldByName( 'SEMANA' ).AsInteger := ord(SemanaCalendario);
     end;
end;

procedure TdmSistema.cdsCalendarioCafeteriaCONTADORGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
     if DisplayText then begin
        if Sender.DataSet.IsEmpty then
           Text := ''
        else
            Text := BoolAsSiNo(zStrToBool(Sender.AsString));
     end else
         Text := Sender.AsString;
end;

procedure TdmSistema.cdsCalendarioCafeteriaHORAGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
     if DisplayText then begin
        if Sender.DataSet.IsEmpty then
           Text := ''
        else
            Text := FormatMaskText('99:99;0', Sender.AsString);
     end else
         Text := Sender.AsString;
end;

procedure TdmSistema.cdsCalendarioCafeteriaHORASetText(Sender: TField; const Text: string);
begin
     Sender.AsString := ConvierteHora(Text);
end;

procedure TdmSistema.cdsCalendarioCafeteriaHORAValidate(Sender: TField);
var
   sHora: String;
begin
     sHora := Sender.AsString;
     if not ZetaCommonTools.ValidaHora(sHora, '48') then
        raise ERangeError.Create('Hora "' + Copy(sHora, 1, 2) + ':' + Copy(sHora, 3, 2) + '" Inv�lida');
end;

procedure TdmSistema.cdsCalendarioCafeteriaNewRecord(DataSet: TDataSet);
begin
     inherited;
     cdsCalendarioCafeteria.FieldByName('CONTADOR').AsString := K_GLOBAL_NO;
     cdsCalendarioCafeteria.FieldByName('SYNC').AsString := K_GLOBAL_NO;
end;

procedure TdmSistema.cdsCalendarioCafeteriaSEMANAGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
     if not(Sender.DataSet.IsEmpty) then
        Text := ObtieneElemento(lfDiasCafeteria, Sender.AsInteger)
     else
         Text := '';
end;

procedure TdmSistema.cdsCalendarioCafeteriaSYNCGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
     if DisplayText then begin
        if Sender.DataSet.IsEmpty then
           Text := ''
        else
            Text := BoolAsSiNo(zStrToBool(Sender.AsString));
     end else
         Text := Sender.AsString;
end;

procedure TdmSistema.cdsCalendarioCafeteriaSYNCSetText(Sender: TField; const Text: string);
begin
     Sender.AsString := zBoolToStr(zStrToBool(UpperCase(Text)));
end;

procedure TdmSistema.cdsCarruselesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsCarruseles.Data := ServerKiosco.GetTabla( K_CONST_CARRUSELES);
     {$endif}
end;

procedure TdmSistema.cdsCarruselesInfoAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with cdsCarruselesInfo do
     begin
          Data := ServerKiosco.GetCarruselesInfo( K_MIS_DATOS );
     end;
     {$endif}
end;

procedure TdmSistema.cdsCarruselesAlEnviarDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   ErrorCount, iErrorCount: Integer;
   oCarrusel, oCarruselInfo: OleVariant;
{$endif}
begin
      {$ifndef DOS_CAPAS}
     with cdsCarruselesInfo do
     begin
          PostData;
     end;

     with cdsCarruseles do
     begin
          PostData;

          if ( ( ChangeCount > 0 ) or ( cdsCarruselesInfo.ChangeCount > 0 ) ) then
          begin
               DisableControls;
               try
                  oCarrusel := ServerKiosco.GrabaCarruseles( DeltaNull, cdsCarruselesInfo.Data, FieldByname('KW_CODIGO').AsString,
                                                               oCarruselInfo, ErrorCount, iErrorCount );
                  if NOT Reconcile( oCarrusel ) then
                     Edit;
               finally
                      EnableControls;
               end;
          end;
     end;
     {$endif}
end;


function TdmSistema.GetGlobalRaizKiosco:string;
{$ifndef DOS_CAPAS}
var
   aGlobalServer: Variant;
{$endif}
begin
		 Result := VACIO;
{$ifndef DOS_CAPAS}
     aGlobalServer := ServerKiosco.GetGlobales;
     Result := aGlobalServer[ K_KIOSCO_DIRECTORIO_RAIZ ];
{$endif}
end;


procedure TdmSistema.GrabaGlobalRaizKiosco(const sValor:string );
{$ifndef DOS_CAPAS}
var
   aGlobalServer: Variant;
   ErrorCount: Integer;
{$endif}
begin
{$ifndef DOS_CAPAS}
     aGlobalServer := ServerKiosco.GetGlobales;
     aGlobalServer[ K_KIOSCO_DIRECTORIO_RAIZ ] := sValor;
     ServerKiosco.GrabaGlobales(aGlobalServer,ErrorCount);
{$endif}
end;

{$ifndef DOS_CAPAS}

function TdmSistema.GetServerKiosco: IdmServerKioscoDisp;
begin
     Result :=  IdmServerKioscoDisp( dmCliente.CreaServidor( CLASS_dmServerKiosco, FServerKiosco ) );
end;

procedure TdmSistema.ReiniciaNIP;
begin
     with dmCliente do
     begin
 	   			ServerKiosco.ReseteaPassword( Empresa,Empleado );
	   end;
end;

{$endif}


procedure TdmSistema.cdsCarruselesInfoBeforeInsert(DataSet: TDataSet);
begin
  inherited;
     if cdsCarruselesInfo.RecordCount = 2 then
     begin
          Abort;
     end;
end;

procedure TdmSistema.cdsCarruselesInfoBeforePost(DataSet: TDataSet);
begin
     inherited;
     if ( ( cdsCarruselesInfo.FieldByName('KI_REFRESH').AsString <> K_GLOBAL_SI) and (cdsCarruselesInfo.FieldByName('KI_REFRESH').AsString <> K_GLOBAL_NO ) ) then
     begin
          DatabaseError(Format('El valor de Refrescar s�lo puede ser ''%s'' � ''%s'' may�sculas',[K_GLOBAL_SI,K_GLOBAL_NO]));
     end;

end;


procedure TdmSistema.GetUsuarioCCosto( const iUsuario: Integer );
begin
     cdsUserSuper.Data := ServerSistema.GetUsuarioCCosto( dmCliente.Empresa, iUsuario );
end;

function TdmSistema.CargaListaCCosto(Lista: TStrings; MostrarActivos: Boolean): string;
var
   sCodigo, sOldIndex: String;
   iPos: Integer;
begin
     
     with dmTablas.GetDataSetTransferencia do
     begin
          Conectar;
          sOldIndex := IndexFieldNames;
          try
             IndexFieldNames := 'TB_ELEMENT';
             First;
             with Lista do
             begin
                  BeginUpdate;
                  Clear;
                  while not Eof do
                  begin
                       AddObject( FieldByName( 'TB_CODIGO' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString, TObject( 0 ) );
                       Next;
                  end;
                  EndUpdate;
             end;
          finally
             IndexFieldNames := sOldIndex;
          end;
     end;

     GetUsuarioCCosto( cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger );

     with cdsUserSuper do
     begin
          First;
          with Lista do
          begin
               BeginUpdate;
               while not Eof do
               begin
                    sCodigo := FieldByName( 'CB_NIVEL' ).AsString;
                    iPos := IndexOfName( sCodigo );
                    if ( iPos < 0 ) then
                       AddObject( sCodigo + '=' + ' ???', TObject( 1 ) )
                    else
                        Objects[ iPos ] := TObject( 1 );
                    Next;
               end;
               EndUpdate;
          end;
     end;

     if MostrarActivos then
     begin
          with dmTablas.GetDataSetTransferencia do
          begin
               Filter := Format( 'TB_ACTIVO = %s', [EntreComillas( K_GLOBAL_NO )] );
               Filtered:= True;
               try
                  with Lista do
                  begin
                       BeginUpdate;
                       while not Eof do
                       begin
                            sCodigo := FieldByName( 'TB_CODIGO' ).AsString;
                            iPos := IndexOfName( sCodigo );
                                 if( iPos >= 0 ) then
                                 begin
                                      if ( ( Integer ( Objects[ iPos ] ) )= 0 ) then
                                      begin
                                           Lista.Delete( iPos );
                                      end;
                                 end;
                            Next;
                       end;

                  end;
               finally
                      Lista.EndUpdate;
                      Filtered:= False;
               end;
          end;
     end;


     with Global do
     begin
          Result := 'Asignar ' + NombreCosteo + ' a ' + cdsUsuarios.FieldByName( 'US_NOMBRE' ).AsString;
     end;

end;

procedure TdmSistema.DescargaListaCCosto(Lista: TStrings);
var
   i, iUsuario: Integer;
   sCodigo: String;
begin
     iUsuario := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
     FTextoBitacora:= 'Antes: '+ GetListaAsignados('CB_NIVEL');
     cdsUserSuper.EmptyDataset;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Integer( Lista.Objects[ i ] ) > 0 ) then
               begin
                    sCodigo := Names[ i ];
                    with cdsUserSuper do
                    begin
                         Append;
                         FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                         FieldByName( 'CB_NIVEL' ).AsString := sCodigo;
                         Post;
                    end;
               end;
          end;
     end;
     
     FTextoBitacora:= FTextoBitacora + CR_LF + 'Despu�s: '+ GetListaAsignados('CB_NIVEL');
     
     with cdsUserSuper do
     begin
          Tag := K_SUPER_CCOSTO;
          try
             Enviar;
          finally
                 Tag := K_SUPERVISORES;
          end;
     end;
end;

{$ifndef DOS_CAPAS}
// SYNERGY
procedure TdmSistema.CargaBitacora( Parametros: TZetaParams );
begin
     cdsBitacoraBio.Data := ServerSistema.ConsultaBitacoraBiometrica( Parametros.VarValues );
end;

// SYNERGY
function TdmSistema.ObtenMaxIdBio: Integer;
begin
     Result := ServerSistema.ObtenIdBioMaximo;
end;

// SYNERGY
procedure TdmSistema.ActualizaIdBio( iEmpleado, iIdBio: Integer; sGrupo: string; iInvitador: Integer );
begin
    {$ifndef DOS_CAPAS}
     ServerSistema.ActualizaIdBio( dmCliente.Compania, iEmpleado, iIdBio, sGrupo, iInvitador, dmCliente.Confidencialidad );
    {$endif}
end;

// SYNERGY
procedure TdmSistema.TE_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
begin
     cdsTerminales.Conectar;
     Text := cdsTerminales.GetDescripcion( Sender.AsString );
     if StrVacio( Text )then
        Text := Sender.AsString;
end;

// SYNERGY
procedure TdmSistema.GP_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
begin
     cdsListaGrupos.Conectar;
     Text := cdsListaGrupos.GetDescripcion( Sender.AsString );
     if StrVacio( Text )then
        Text := Sender.AsString;
end;

// SYNERGY
procedure TdmSistema.TE_APPGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if( FieldByName( 'TE_APP' ).AsInteger = 1 )then
              Text := 'Asistencia'
          else if ( FieldByName( 'TE_APP' ).AsInteger = 3 )then
              Text := 'Acceso'
          else if ( FieldByName( 'TE_APP' ).AsInteger = 2 )then
              Text := 'Cafeter�a';
     end;
end;

// SYNERGU
procedure TdmSistema.TE_ZONAGetTex(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if( FieldByName( 'TE_ZONA' ).AsString = 'Eastern Standard Time' )then
              Text := '(GMT-05:00) Eastern Time (US & Canada)'
          else if( FieldByName( 'TE_ZONA' ).AsString = 'Central Standard Time' )then
              Text := '(GMT-06:00) Central Time (US & Canada)'
          else if( FieldByName( 'TE_ZONA' ).AsString = 'Central Standard Time (Mexico)' )then
              Text := '(GMT-06:00) Guadalajara, Mexico City, Monterrey'
          else if( FieldByName( 'TE_ZONA' ).AsString = 'Mountain Standard Time (Mexico)' )then
              Text := '(GMT-07:00) Chihuahua, La Paz, Mazatlan'
          else if( FieldByName( 'TE_ZONA' ).AsString = 'Mountain Standard Time' )then
              Text := '(GMT-07:00) Mountain Time (US & Canada)'
          else if( FieldByName( 'TE_ZONA' ).AsString = 'Pacific Standard Time' )then
              Text := '(GMT-08:00) Pacific Time (US & Canada)'
          else if( FieldByName( 'TE_ZONA' ).AsString = 'Pacific Standard Time (Mexico)' )then
              Text := '(GMT-08:00) Baja California'
          else
              Text := Sender.AsString;
     end;
end;

// SYNERGY
procedure TdmSistema.ValidaFechaVacia(Sender: TField; var Text: String; DisplayText: Boolean); // SYNERGY
begin
     if( Sender.AsDateTime = NullDateTime )then
         Text := VACIO
     else
         Text := Sender.AsString;
end;

// SYNERGY
procedure TdmSistema.GrabaListaTerminales( sGrupo, sTerminales, sSinTerminales: string );
begin
     ServerSistema.GrabaListaTermPorGrupo( sTerminales, dmCliente.Usuario, sGrupo, sSinTerminales );
end;

// SYNERGY
function TdmSistema.DentroListaTerminales( sGrupo, sTerminales: string ): Boolean;
begin
     sTerminales := Copy( sTerminales, 0, Pos( '=', sTerminales ) - 1 );
     Result := cdsTermPorGrupo.Locate( 'TE_CODIGO;GP_CODIGO', VarArrayOf([ sTerminales, sGrupo ]), [] )
end;

// SYNERGY
function TdmSistema.TieneHuellaEmpleado( iEmpleado: Integer; Inumero:integer ): Boolean;
begin
     Result := ServerSistema.ExisteHuellaRegistrada( iEmpleado,intToStr(Inumero));
end;

// SYNERGY
function TdmSistema.CantidadHuellaEmpleado(iEmpleado, iNumero, iTipo:integer): Integer;
begin
      Result := ServerSistema.CantidadHuellaRegistrada(dmCliente.Empresa, iEmpleado, iNumero, iTipo);    
end;

function TdmSistema.TieneHuellaInvitador( iInvitador: Integer ): Boolean; // BIOMETRICO
begin
     Result := ServerSistema.ExisteHuellaInvitador( dmCliente.Compania, iInvitador );
end;

// SYNERGY
procedure TdmSistema.ReiniciaTerminales( iTerminal: Integer );
begin
     ServerSistema.ReiniciaTerminal( iTerminal, dmCliente.Compania );
end;
{$endif}
                                  
// SYNERGY
procedure TdmSistema.cdsListaGruposAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsListaGrupos.Data := ServerSistema.ConsultaListaGrupos(dmCliente.Compania);
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsListaGruposAlModificar(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditSistListaGrupos_DevEx, TEditSistListaGrupos_DevEx );
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsListaGruposAfterDelete(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsListaGrupos.Enviar;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsListaGruposAlEnviarDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   ErrorCount: Integer;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ErrorCount := 0;

     with cdsListaGrupos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if not( Reconciliar( ServerSistema.GrabaSistListaGrupos( Delta, dmCliente.Usuario, ErrorCount  ) ) )then
                FieldByName( 'GP_CODIGO' ).FocusControl;
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsListaGruposBeforePost(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsListaGrupos.FieldByName('CM_CODIGO').Value := dmCliente.Compania;
     with cdsListaGrupos do
     begin
          if ( not StrLleno( FieldByName( 'GP_CODIGO' ).AsString ) ) then
          begin
               FieldByName( 'GP_CODIGO' ).FocusControl;
               DataBaseError( 'El c�digo no puede quedar vac�o' );
          end;
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTermPorGrupoAfterDelete(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsTermPorGrupo.Enviar;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTermPorGrupoAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsTermPorGrupo.Data := ServerSistema.ConsultaTermPorGrupo( FGrupoDeTerm );
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTermPorGrupoAlEnviarDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   ErrorCount: Integer;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ErrorCount := 0;
     with cdsTermPorGrupo do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconciliar( ServerSistema.GrabaTermPorGrupo( Delta, dmCliente.Usuario, ErrorCount ) );
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTermPorGrupoAlCrearCampos(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     inherited;
     with cdsTermPorGrupo do
     begin
          FieldByName( 'TE_CODIGO' ).OnGetText := TE_CODIGOGetText;
          FieldByName( 'GP_CODIGO' ).OnGetText := GP_CODIGOGetText;
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsMensajes.Data := ServerSistema.ConsultaMensaje( FFiltroTerminal );
     FFiltroTerminal := VACIO;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesAfterDelete(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsMensajes.Enviar;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesAlEnviarDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   ErrorCount: Integer;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ErrorCount := 0;
     with cdsMensajes do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if not( Reconciliar( ServerSistema.GrabaMensaje( Delta, dmCliente.Usuario, ErrorCount ) ) )then
                FieldByName( 'DM_CODIGO' ).FocusControl;
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesAlModificar(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ZBaseEdicion_DevEx.ShowFormaEdicion(EditMensajes_DevEx, TEditMensajes_DevEx);
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesBeforePost(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with cdsMensajes do
     begin
          if (FieldByName( 'DM_CODIGO' ).AsInteger <= 0)then
          begin
               FieldByName( 'DM_CODIGO' ).FocusControl;
               DataBaseError( 'El c�digo no puede quedar vac�o' );
          end;
     end;
     {$endif}
end;

procedure TdmSistema.cdsMensajesGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
    inherited;
    lHasRights := False;
end;

// SYNERGY
procedure TdmSistema.cdsMensajesNewRecord(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsMensajes.FieldByName( 'DM_ACTIVO' ).AsString := K_GLOBAL_SI;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTerminalesAlAgregar(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     ZetaDialogo.ZWarning('Advertencia','No se puede agregar terminales por este medio',0,mbOk);
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTerminalesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsTerminales.Data := ServerSistema.ConsultaTerminal( FFiltroTerminal );
     FFiltroTerminal := VACIO;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTerminalesAlCrearCampos(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with cdsTerminales do
     begin
          FieldByName( 'TE_APP' ).OnGetText := TE_APPGetText;
          FieldByName( 'TE_BEAT' ).OnGetText := ValidaFechaVacia;
          FieldByName( 'TE_ZONA' ).OnGetText := TE_ZONAGetTex;
          FieldByName( 'TE_TER_HOR' ).OnGetText := ValidaFechaVacia;
          ListaFija( 'TE_ID', lfIdTerminales );
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTerminalesAlModificar(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ZBaseEdicion_DevEx.ShowFormaEdicion(EditTerminales_DevEx, TEditTerminales_DevEx);
     {$endif}
end;

procedure TdmSistema.cdsTerminalesGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     inherited;
     lHasRights := False;
end;

// SYNERGY
procedure TdmSistema.cdsTerminalesAfterDelete(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsTerminales.Enviar;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsTerminalesAlEnviarDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   ErrorCount: Integer;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ErrorCount := 0;
     with cdsTerminales do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if not( Reconciliar( ServerSistema.GrabaTerminal( Delta, dmCliente.Usuario, ErrorCount ) ) )then
                FieldByName( 'TE_NOMBRE' ).FocusControl;
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesPorTerminalAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsMensajesPorTerminal.Data := ServerSistema.ConsultaMensajeTerminal( FFiltroTerminal );
     FFiltroTerminal := VACIO;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesPorTerminalNewRecord(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with cdsMensajesPorTerminal do
     begin
          FieldByName( 'TE_CODIGO' ).AsString := FTerminalSeleccionada;
          FieldByName( 'TM_NEXT' ).AsDateTime := Date;
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesPorTerminalAlEnviarDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   ErrorCount: Integer;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ErrorCount := 0;
     with cdsMensajesPorTerminal do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             if not( Reconciliar( ServerSistema.GrabaMensajeTerminal( Delta, dmCliente.Usuario, ErrorCount ) ) )then
                FieldByName( 'DM_CODIGO' ).FocusControl;
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesPorTerminalAlModificar (Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     ZBaseEdicion_DevEx.ShowFormaEdicion(EditMensajesPorTerminal_DevEx, TEditMensajesPorTerminal_DevEx);
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesPorTerminalBeforePost(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with cdsMensajesPorTerminal do
     begin
          if StrVacio( FieldByName( 'TE_CODIGO' ).AsString )then
          begin
               FieldByName( 'TE_CODIGO' ).FocusControl;
               DataBaseError( 'Es necesario especificar una terminal' );
          end
          else if StrVacio( FieldByName( 'DM_CODIGO' ).AsString )then
          begin
               FieldByName( 'DM_CODIGO' ).FocusControl;
               DataBaseError( 'Es necesario especificar un mensaje' );
          end;
     end;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesPorTerminalAfterDelete(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     cdsMensajesPorTerminal.Enviar;
     {$endif}
end;

// SYNERGY
procedure TdmSistema.cdsMensajesPorTerminalAlCrearCampos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   filtrotmp: String;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     filtrotmp := FFiltroTerminal;
     FFiltroTerminal := VACIO;
     cdsMensajes.Conectar;
     FFiltroTerminal := filtrotmp;
     with cdsMensajesPorTerminal do
     begin
          CreateSimpleLookUp( cdsMensajes, 'DM_MENSAJE', 'DM_CODIGO' );
          FieldByName( 'TM_NEXT' ).OnGetText := ValidaFechaVacia;
          FieldByName( 'TM_ULTIMA' ).OnGetText := ValidaFechaVacia;
          FieldByName( 'TM_NEXTHOR' ).OnGetText := TM_NEXTHORGetText;
     end;
     {$endif}
end;

procedure TdmSistema.TM_NEXTHORGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   iHoras: Integer;
begin
     Text := Sender.AsString;
     if ( Length( Text ) > 0 ) // and ( Text <> K_HORA_NULA )
      then
     begin
          iHoras := StrToInt( Text );
          {if ( iHoras >= 2400 ) then
             iHoras := iHoras - 2400;}
          Text := Format( '%4.4d', [ iHoras ] );
          Text := Copy( Text, 1, 2 ) + ':' + Copy( Text, 3, 2 );
     end;
end;

procedure TdmSistema.cdsListaGruposBeforeDelete(DataSet: TDataSet);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     if( ServerSistema.ExisteEmpleadoEnGrupo( cdsListaGrupos.FieldByName( 'GP_CODIGO' ).AsString ) )then
         DataBaseError( 'No se puede eliminar el grupo ya que est� relacionado a un(os) empleado(s)' );
     {$endif}
end;

// -------------------------------------------------------------------------------------------------------------------------------------------------- BIOMETRICO
{$ifndef DOS_CAPAS}
procedure TdmSistema.EliminaHuella( iEmpleado, iInvitador, iTipo : Integer ); // BIOMETRICO
begin
     ServerSistema.EliminaHuellas( dmCliente.Compania, iEmpleado, iInvitador, dmcliente.usuario, iTipo );
end;


procedure TdmSistema.InsertaHuella( iEmpleado, iDedo : Integer; StreamTemplate : String; Invitador : Integer ); // BIOMETRICO
var
   oParametros: TZetaParams;
begin
     try
        oParametros:= TZetaParams.Create;

        with oParametros do
        begin
             AddString( 'Empresa', dmCliente.Compania );
             AddInteger( 'Empleado', iEmpleado );
             AddInteger( 'Dedo', iDedo );
             AddString( 'Huella', StreamTemplate );
             AddInteger( 'Invitador', Invitador );
             AddString( 'Computadora', dmCliente.ComputerName );
             AddInteger( 'Usuario', dmCliente.Usuario );

             ServerSistema.InsertaHuella( VarValues );
        end;

     finally
            FreeAndNil(oParametros);
     end;
end;

function TdmSistema.GetLocalBioDbPath: string;
begin
     Result := VACIO;
     if strLleno( GetMyDocumentsDir ) then
         Result := VerificaDir( GetMyDocumentsDir ) + K_FILENAME;
end;

procedure TdmSistema.ReiniciaHuellas;
var
   oParametros: TZetaParams;
begin
     try
        oParametros:= TZetaParams.Create;
        oParametros.AddString('ComputerName', dmCliente.ComputerName );
        
        ServerSistema.ReportaHuella( oParametros.VarValues );
     finally
            FreeAndNil(oParametros);
     end;
end;

//Crea la lista de IDs que se enviaran
procedure TdmSistema.ReportaHuella( IdHuella : Integer );
begin
     if ( StrLleno( FHuellas ) ) then
          FHuellas := FHuellas + ',' + IntToStr(IdHuella)
     else
          FHuellas := IntToStr(IdHuella);

end;

//Envia la cadena de IDS que corresponden a la computadora
procedure TdmSistema.ReportaHuellas;
var
   oParametros: TZetaParams;
begin
     if ( StrLleno( FHuellas ) ) then
     begin
          oParametros := TZetaParams.Create;
          try
             with oParametros do
             begin
                  AddString('ComputerName', dmCliente.ComputerName); //Computadora con las huellas
                  AddString('HuellaId', FHuellas); //Huella o listado de huellas a reportar que ya la tiene la computadora

                  ServerSistema.ReportaHuella( VarValues );
             end;
          finally
                 FreeAndNil(oParametros);
          end;
     end;
end;

//Borra el listado de ids de huellas
procedure TdmSistema.ReportaHuellaBorra;
begin
     FHuellas := VACIO;
end;
{$endif}

procedure TdmSistema.cdsHuellaGtiAlAdquirirDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   oParametros: TZetaParams;
{$endif}
begin
     inherited;
{$ifndef DOS_CAPAS}
     try
        oParametros:= TZetaParams.Create;

        with oParametros do
        begin
             //AddDate('FechaPivote', FechaPivote );
             AddString('ComputerName', dmCliente.ComputerName );

             cdsHuellaGti.Data := ServerSistema.ObtieneTemplates( VarValues );
        end;
     finally
            FreeAndNil(oParametros);              
     end;
{$endif}
end;

procedure TdmSistema.cdsUsuariosSeguridadBeforePost(DataSet: TDataSet);
var
    sFilter: string;
    lFiltered {$ifndef DOS_CAPAS}, lValidaReportaA {$endif} : Boolean;
    {$ifndef DOS_CAPAS}
    sMensaje: string;
    {$endif}
begin
     inherited;

     with cdsUsuariosSeguridad do
     begin
          if strLleno( FieldByName('CM_CODIGO').AsString ) then
          begin
               if ( FieldByName('CB_CODIGO').AsInteger = 0 )then
                  DataBaseError( 'El Campo De Empleado No Puede Quedar Vac�o' );

               sFilter := cdsUsuariosSeguridadLookup.Filter;
               lFiltered := cdsUsuariosSeguridadLookup.Filtered;
               cdsUsuariosSeguridadLookup.Filter := Format( 'US_CODIGO <> %d',[cdsUsuariosSeguridad.FieldByName('US_CODIGO').AsInteger] );
               cdsUsuariosSeguridadLookup.Filtered := TRUE;
               try
                  if cdsUsuariosSeguridadLookup.Locate('CM_CODIGO;CB_CODIGO', VarArrayOf([FieldByName('CM_CODIGO').AsString,FieldByName('CB_CODIGO').AsInteger]), [] ) then
                  begin
                       DataBaseError( Format( 'El Empleado %d ya esta asignado al Usuario #%d', [FieldByName('CB_CODIGO').AsInteger, cdsUsuariosSeguridadLookup.FieldByName('US_CODIGO').AsInteger] ) );
                  end;
               finally
                      cdsUsuariosSeguridadLookup.Filter := sFilter;
                      cdsUsuariosSeguridadLookup.Filtered := lFiltered;
               end;
          end;


           {V 2013 SOP-4204 CAS-145137- Bivalencia en comportamiento al registrar usuarios con Sistemas con Active Directory}
          // if ( dmCliente.TipoLogin <> tlLoginTress ) then
          if ( dmCliente.TipoLogin = tlAD ) then
          begin
               if StrVacio( FieldByName('US_DOMAIN').AsString ) then
                  DataBaseError( 'El Usuario del Dominio No Puede Quedar Vac�o' );
          end;

          {$ifndef DOS_CAPAS}
          with dmCliente do
          begin
               if ((EditandoUsuarios ) and ( not Autorizacion.EsDemo ) and   ( ( ModuloAutorizadoConsulta ( okEvaluacion, sMensaje ) ) or ( ModuloAutorizadoConsulta ( okWorkflow , sMensaje ) ) ) )then
               begin
                    lValidaReportaA := True;
                    {$ifdef TRESS}
                    if (dmCliente.EmpresaAbierta)  then
                       lValidaReportaA := not Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES );
                    {$endif}

                    if (lValidaReportaA) and (FieldByName('US_JEFE').AsInteger <= 0 )then
                       ZetaDialogo.ZWarning('Advertencia','El Campo De Reporta A No Puede Quedar Vac�o',0,mbOk);
                    EditandoUsuarios := False;
               end;
          end;
          {$endif}
     end;
end;

procedure TdmSistema.cdsUsuariosSeguridadNewRecord(DataSet: TDataSet);
begin
     inherited;
     //Solo Aplica para Tress, otros sistemas tales como Seleccion,Visitantes o Imx, no deben de hacer estas validaciones.
     with cdsUsuariosSeguridad do
     begin
         {$ifdef DOS_CAPAS}
          FieldByName( 'US_PORTAL' ).AsString := K_GLOBAL_NO;
         {$else}
          FieldByName( 'US_PORTAL' ).AsString := zBoolToStr( dmCliente.ModuloAutorizadoDerechos( okPortal ) );
         {$endif}
     end;
end;

procedure TdmSistema.cdsUsuariosSeguridadAlEnviarDatos(Sender: TObject);
var
   iOldEmpleado: integer;
begin
     iOldEmpleado := cdsUsuariosSeguridad.FieldByName('CB_CODIGO').OldValue;
     inherited;
     if ( cdsUsuariosSeguridad.ChangeCount = 0 ) and ( iOldEmpleado <> cdsUsuariosSeguridad.FieldByName('CB_CODIGO').AsInteger ) then
     begin
          with dmCliente.cdsEmpleado do
          begin
               if ( FieldByName('CB_CODIGO').AsInteger in [iOldEmpleado,cdsUsuariosSeguridad.FieldByName('CB_CODIGO').AsInteger]) then

               begin
                    Edit;
                    FieldByName('US_CODIGO').AsInteger := cdsUsuariosSeguridad.FieldByName('US_CODIGO').AsInteger;
                    Post;
                    MergeChangeLog;
               end;
          end;
     end;
end;

procedure TdmSistema.cdsUsuariosSeguridadAlBorrar(Sender: TObject);
const
   K_ADV_BORRAR_REGISTRO = '� Desea Borrar Este Registro ?';
{$ifdef COMPARTE_MGR}
   K_ADV_SUPERVISORES ='Este usuario puede pertenecer a un Supervisor Enrolado. Si borra este usuario puede quedar alg�n Supervisor sin enrolar.�Est� seguro que desea borrar este usuario?';
{$else}
   K_ADV_SUPERVISORES = 'Este usuario puede pertenecer a un Supervisor Enrolado en esta o en alguna otra de sus empresas. Si borra este usuario puede quedar alg�n Supervisor sin enrolar.�Est� seguro que desea borrar este usuario?';
{$endif}
var
   sAdvertencia : string;
begin
    sAdvertencia := K_ADV_BORRAR_REGISTRO;

   {$ifdef COMPARTE_MGR}
    sAdvertencia := K_ADV_SUPERVISORES;
   {$endif}

   {$ifndef DOS_CAPAS}
   {$ifdef TRESS}
   if (dmCliente.EmpresaAbierta) and Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
      sAdvertencia := K_ADV_SUPERVISORES;
   {$endif}
   {$endif}

   if ( ZetaMsgDlg.ConfirmaCambio( sAdvertencia ) ) then
       cdsUsuariosSeguridad.Delete;
end;

function TdmSistema.CargaListaCCostoSeguridad(Lista: TStrings; MostrarActivos: Boolean): string;
var
   sCodigo, sOldIndex: String;
   iPos: Integer;
begin
     
     with dmTablas.GetDataSetTransferencia do
     begin
          Conectar;
          sOldIndex := IndexFieldNames;
          try
             IndexFieldNames := 'TB_ELEMENT';
             First;
             with Lista do
             begin
                  BeginUpdate;
                  Clear;
                  while not Eof do
                  begin
                       AddObject( FieldByName( 'TB_CODIGO' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString, TObject( 0 ) );
                       Next;
                  end;
                  EndUpdate;
             end;
          finally
             IndexFieldNames := sOldIndex;
          end;
     end;

     GetUsuarioCCosto( cdsUsuariosSeguridad.FieldByName( 'US_CODIGO' ).AsInteger );

     with cdsUserSuper do
     begin
          First;
          with Lista do
          begin
               BeginUpdate;
               while not Eof do
               begin
                    sCodigo := FieldByName( 'CB_NIVEL' ).AsString;
                    iPos := IndexOfName( sCodigo );
                    if ( iPos < 0 ) then
                       AddObject( sCodigo + '=' + ' ???', TObject( 1 ) )
                    else
                        Objects[ iPos ] := TObject( 1 );
                    Next;
               end;
               EndUpdate;
          end;
     end;

     if MostrarActivos then
     begin
          with dmTablas.GetDataSetTransferencia do
          begin
               Filter := Format( 'TB_ACTIVO = %s', [EntreComillas( K_GLOBAL_NO )] );
               Filtered:= True;
               try
                  with Lista do
                  begin
                       BeginUpdate;
                       while not Eof do
                       begin
                            sCodigo := FieldByName( 'TB_CODIGO' ).AsString;
                            iPos := IndexOfName( sCodigo );
                                 if( iPos >= 0 ) then
                                 begin
                                      if ( ( Integer ( Objects[ iPos ] ) )= 0 ) then
                                      begin
                                           Lista.Delete( iPos );
                                      end;
                                 end;
                            Next;
                       end;

                  end;
               finally
                      Lista.EndUpdate;
                      Filtered:= False;
               end;
          end;
     end;


     with Global do
     begin
          Result := 'Asignar ' + NombreCosteo + ' a ' + cdsUsuariosSeguridad.FieldByName( 'US_NOMBRE' ).AsString;
     end;

end;

procedure TdmSistema.DescargaListaCCostoSeguridad(Lista: TStrings);
var
   i, iUsuario: Integer;
   sCodigo: String;
begin
     iUsuario := cdsUsuariosSeguridad.FieldByName( 'US_CODIGO' ).AsInteger;
     FTextoBitacora:= 'Antes: '+ GetListaAsignados('CB_NIVEL');
     cdsUserSuper.EmptyDataset;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Integer( Lista.Objects[ i ] ) > 0 ) then
               begin
                    sCodigo := Names[ i ];
                    with cdsUserSuper do
                    begin
                         Append;
                         FieldByName( 'US_CODIGO' ).AsInteger := iUsuario;
                         FieldByName( 'CB_NIVEL' ).AsString := sCodigo;
                         Post;
                    end;
               end;
          end;
     end;
     
     FTextoBitacora:= FTextoBitacora + CR_LF + 'Despu�s: '+ GetListaAsignados('CB_NIVEL');
     
     with cdsUserSuper do
     begin
          Tag := K_SUPER_CCOSTO;
          try
             Enviar;
          finally
                 Tag := K_SUPERVISORES;
          end;
     end;
end;


procedure TdmSistema.cdsUsuariosAlAdquirirDatos(Sender: TObject);
begin
  inherited;

end;

{
procedure TdmSistema.GenericReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
  var Action: TReconcileAction);
begin
  inherited;

end;
}

//DevEx (by am): Agregado para mostrar la fecha en la bitacora con el mismo formato que en la vista clasica
procedure TdmSistema.cdsBitacoraBioAlCrearCampos(Sender: TObject);
begin
  inherited;
  {$ifndef DOS_CAPAS}
     with cdsBitacoraBio do
     begin
          FieldByName( 'WS_FECHA' ).OnGetText := ValidaFechaVacia;
     end;
  {$endif}
end;

procedure TdmSistema.cdsBitacoraBioAlModificar(Sender: TObject);  //@DC
begin
  inherited;
  ZBaseDlgModal_DevEx.ShowDlgModal(EditBitacoraRegBio_DevEx, TEditBitacoraRegBio_DevEx);
end;



{ cdsBitacoraReportes Fin }

procedure TdmSistema.cdsPollAlCrearCampos(Sender: TObject);
begin
  inherited;
   with cdsPoll do
   begin
   maskfecha('PO_FECHA');
   end;
end;

procedure TdmSistema.cdsBitacoraAlCrearCampos(Sender: TObject);
begin
  inherited;
  with cdsBitacora do
  begin
       MaskFecha( 'BI_FECHA' );
       MaskFecha( 'BI_FEC_MOV' );
  end;
end;

procedure TdmSistema.DB_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          Text := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Sender.AsInteger);
     end;
end;

procedure TdmSistema.cdsEmpresasAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsEmpresas do
     begin
          FieldByName( 'CM_CTRL_RL' ).OnGetText := CM_CTRL_RLGetText;
     end;
end;

procedure TdmSistema.CM_CTRL_RLGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tc3Datos ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tc3Datos ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tcRecluta ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tcRecluta ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tcVisitas ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tcVisitas ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tcOtro ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tcOtro ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tcPresupuesto ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tcPresupuesto ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tc3Prueba ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tc3Prueba ) );
end;

procedure TdmSistema.cdsEmpresasAlEnviarDatos(Sender: TObject);
begin
     inherited;
     cdsSistBaseDatosLookUp.Refrescar;
end;

procedure TdmSistema.cdsEmpresasBeforeInsert(DataSet: TDataSet);
begin
     cdsEmpresasLookUp.Data := cdsEmpresas.Data;  // si se hace despu�s, regresa el Dataset a dsBrowse
     inherited;
end;

function TdmSistema.EsBaseDatosEmpleados (BaseDatos: String): Boolean;
begin
     Result := ServerSistema.EsBDEmpleados(BaseDatos);
end;

function TdmSistema.EsBaseDatosSeleccion (BaseDatos: String): Boolean;
begin
     Result := ServerSistema.EsBDSeleccion(BaseDatos);
end;

function TdmSistema.EsBaseDatosAgregada (Ubicacion: String): Boolean;
begin
     Result := ServerSistema.EsBDAgregada(Ubicacion);
end;

function TdmSistema.CrearEstructuraBD(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CrearEstructuraBD(Parametros.VarValues);
end;

function TdmSistema.CreaInicialesTabla(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CreaInicialesTabla(Parametros.VarValues);
end;

function TdmSistema.CreaSugeridosTabla(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CreaSugeridosTabla(Parametros.VarValues);
end;

function TdmSistema.CreaDiccionarioTabla(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CreaDiccionarioTabla(Parametros.VarValues);
end;

function TdmSistema.EmpGrabarDBSistema (Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.EmpGrabarDBSistema(Parametros.VarValues);
end;

function TdmSistema.CrearBDEmpleados (Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CrearBDEmpleados(Parametros.VarValues);
end;

function TdmSistema.GetLogicalName(Conexion, Name: String; Tipo: Integer): string;
begin
     Result := ServerSistema.GetLogicalName(Conexion,Name, Tipo);
end;

function TdmSistema.BackupDB(Conexion,Origen, Destino, Ruta: String): Boolean;
begin
     Result := ServerSistema.BackupDB(Conexion,Origen, Destino, Ruta);
end;

function TdmSistema.RestoreDB(Conexion,Origen, Destino, DataName, PathDataName, LogName, PathLogName: String): Boolean;
begin
     Result := ServerSistema.RestoreDB(Conexion,Origen, Destino, DataName, PathDataName, LogName, PathLogName);
end;

function TdmSistema.DepurarTabla(Conexion,Tabla, Database: String): Boolean;
begin
     Result := ServerSistema.DepurarTabla(Conexion,Tabla, Database);
end;

function TdmSistema.DepurarBitacora(Conexion,Database: String): Boolean;
var
   lok, ltemp : Boolean;
begin
     ltemp := ServerSistema.DepurarTabla(Conexion,'BITACORA', Database);
     lok := ServerSistema.DepurarTabla(Conexion,'PROCESO', Database);
     lok :=  ltemp and lok;
     Result := lok;
end;

function TdmSistema.DepurarAsistencia(Conexion,Database: String): Boolean;
var
   lok, ltemp : Boolean;
begin
     ltemp := ServerSistema.DepurarTabla(Conexion,'CHECADAS', Database);
     lok := ServerSistema.DepurarTabla(Conexion,'AUSENCIA', Database);
     lok :=  ltemp and lok;
     Result := lok;
end;

function TdmSistema.DepurarDigitales(Conexion,Database: String): Boolean;
begin
     Result := ServerSistema.DepurarTabla(Conexion,'DOCUMENTO', Database);
end;

function TdmSistema.ConectarseServer(Conexion: String; out Mensaje: WideString): Boolean;
var
   Conn : WideString;
begin
     Conn :=   Conexion;
     Result := ServerSistema.ConectarseServidor(Conexion, Mensaje);
     
end;

function TdmSistema.GetDBSize(const Conexion,Database: WideString; bUsarConexion: Boolean = FALSE): String;
begin
     Result := ServerSistema.GetDBSize(Conexion, Database, bUsarConexion);
end;

function TdmSistema.GetDataLogSize(Conexion,Database: WideString; bUsarConexion: Boolean = FALSE): String;
begin
     Result := ServerSistema.GetDataLogSize(Conexion, Database, bUsarConexion);
end;

function TdmSistema.ShrinkDB(const Conexion,Database: WideString): Boolean;
begin
     Result := ServerSistema.ShrinkDB(Conexion, Database);
end;

function TdmSistema.GetConexion(const Conn, Database, Datos, UserName, Password: WideString): Boolean;
begin
     Result := ServerSistema.GetConexion(Conn, Database, Datos, UserName, ZetaServerTools.Encrypt (Password));
end;

function TdmSistema.GrabaDB_INFO(Parametros: OleVariant): Boolean;
begin
     Result := ServerSistema.GrabaDB_INFO(Parametros);
end;

function TdmSistema.GetPathSQLServer: String;
begin
     Result := ServerSistema.GetPathSQLServer;
end;

function TdmSistema.NoEsBDSeleccionVisitantes (BaseDatos: String): Boolean;
begin
     Result := ServerSistema.NoEsBDSeleccionVisitantes(BaseDatos);
end;

function TdmSistema.NoEsBDTressVisitantes (BaseDatos: String): Boolean;
begin
     Result := ServerSistema.NoEsBDTressVisitantes(BaseDatos);
end;

function TdmSistema.PrepararPresupuestos (Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.PrepararPresupuestos(Parametros.VarValues);
end;

function TdmSistema.CatalogosPresupuestos (Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CatalogosPresupuestos(Parametros.VarValues);
end;

function TdmSistema.EmpleadosPresupuestos (Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.EmpleadosPresupuestos(Parametros.VarValues);
end;

function TdmSistema.OptimizarPresupuestos (Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.OptimizarPresupuestos(Parametros.VarValues);
end;

function TdmSistema.GetBDFromDB_INFO (DB_CODIGO: String): String;
begin
     Result := ServerSistema.GetBDFromDB_INFO(DB_CODIGO);
end;

function TdmSistema.CreaEspeciales(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CreaEspeciales(Parametros.VarValues);
end;

function TdmSistema.ExistenEspeciales: Boolean;
begin
     Result := ServerSistema.ExistenEspeciales;
end;

function TdmSistema.ExisteBD_DBINFO(Codigo: string): Boolean;
begin
     Result := ServerSistema.ExisteBD_DBINFO(Codigo);
end;

procedure TdmSistema.cdsSistActualizarBDsAlAdquirirDatos(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  try
    TZetaLookupDataSet(Sender).Data := ServerSistema.ConsultaActualizarBDs(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TdmSistema.cdsSistActualizarBDsAlCrearCampos(Sender: TObject);
begin
  inherited;
  with cdsSistActualizarBDs do begin
    FieldByName('DB_TIPO' ).OnGetText            := DB_TIPOGetText;
    FieldByName('DB_APLICAPATCH').OnChange       := DB_APLICAPATCHChange;
    FieldByName('DB_APLICADICCIONARIO').OnChange := DB_APLICADICCIONARIOChange;
    FieldByName('DB_APLICAREPORTES').OnChange    := DB_APLICADICCIONARIOChange;
    FieldByName('DB_APLICAESPECIALES').OnChange  := DB_APLICAESPECIALESChange;
    FieldByName('DB_PROCESO' ).OnGetText         := DB_PROCESOGetText;
  end;
end;

procedure TdmSistema.DB_PROCESOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
          if Sender.AsInteger <> 0 then
            Text := 'Ver detalle '
          else
            Text := '';
end;

{* Validar los cambios hechos al campo cdsSistActualizarBDs.DB_APLICAPATCH. @author Ricardo Carrillo Morales}
procedure TdmSistema.DB_APLICAPATCHChange(Sender: TField);
var
  Event: TFieldNotifyEvent;
begin
  with cdsSistActualizarBDs do 
  begin
       with Sender do
           if AsBoolean and (FieldByName('DB_VERSION').AsInteger >= FieldByName('DB_APLICAVERSION').AsInteger) then 
           begin
                Event := OnChange;
                OnChange := nil;
                AsBoolean := False;
               OnChange := Event;
           end;
       FieldByName('DB_APLICADICCIONARIO').AsBoolean := Sender.AsBoolean;
       FieldByName('DB_APLICAREPORTES').AsBoolean    := Sender.AsBoolean;
       FieldByName('DB_APLICAESPECIALES').AsBoolean  := Sender.AsBoolean and ZStrToBool(FieldByName('DB_ESPC').AsString);
  end;
end;

{* Validar los cambios hechos al campo cdsSistActualizarBDs.DB_APLICADICCIONARIO. @author Ricardo Carrillo Morales}
procedure TdmSistema.DB_APLICADICCIONARIOChange(Sender: TField);
var
  Event: TFieldNotifyEvent;
begin
  if not cdsSistActualizarBDs.FieldByName('DB_APLICAPATCH').AsBoolean then
    with Sender do begin
      Event := OnChange;
      OnChange := nil;
      AsBoolean := False;
      OnChange := Event;
    end;
end;

{* Validar los cambios hechos al campo cdsSistActualizarBDs.DB_APLICAESPECIALES. @author Ricardo Carrillo Morales}
procedure TdmSistema.DB_APLICAESPECIALESChange(Sender: TField);
var
  OldEvent: TFieldNotifyEvent;
begin
  with Sender do begin
    OldEvent := OnChange;
    OnChange := nil;
    AsBoolean := AsBoolean and ZStrToBool(cdsSistActualizarBDs.FieldByName('DB_ESPC').AsString);
    OnChange := OldEvent;
  end;
end;

{* Inicializaci�n de un registro al agregarlo al DataSet. @author Ricardo Carrillo Morales}
procedure TdmSistema.cdsSistActualizarBDsNewRecord(DataSet: TDataSet);
begin
  inherited;
  with cdsSistActualizarBDs do begin
    FieldByName( 'DB_USRNAME' ).AsString := GetSQLUserName;
    FieldByName( 'DB_CODIGO' ).AsString := VACIO;
  end;
end;


{* Ejecutar el Motor de Patch en el servidor. @author Ricardo Carrillo Morales}
function TdmSistema.EjecutaMotorPatch(Parametros: TZetaParams): Boolean;
var
  VarValues: OleVariant;
begin
  VarValues := Parametros.VarValues;
  Result := ServerSistema.EjecutaMotorPatch(VarValues);
  Parametros.VarValues := VarValues;
end;

procedure TdmSistema.GetTablasBD (BaseDatosUbicacion, Usuario, Password: WideString);
begin
     cdsTablasBD.Data := ServerSistema.GetTablasBD(BaseDatosUbicacion, Usuario, Password);
end;

procedure TdmSistema.GetCamposTabla (BaseDatosUbicacion, Usuario, Password, Tabla: WideString);
begin
     cdsCamposTabla.Data := ServerSistema.GetCamposTabla(BaseDatosUbicacion, Usuario, Password, Tabla);
end;

procedure TdmSistema.GetCamposLlaveTabla (BaseDatosUbicacion, Usuario, Password, Tabla: WideString);
begin
     cdsCamposLlaveTabla.Data := ServerSistema.GetCamposLlaveTabla(BaseDatosUbicacion, Usuario, Password, Tabla);
end;

procedure TdmSistema.GetListaTablas( ListaTablas: TStrings );
begin
     ListaTablas.Clear;
     with cdsSistBaseDatos do
     begin
          GetTablasBD( FieldByName ('DB_DATOS').AsString, FieldByName ('DB_USRNAME').AsString, FieldByName ('DB_PASSWRD').AsString );
     end;
     with cdsTablasBD do
     begin
          First;
          while ( not EOF ) do
          begin
               ListaTablas.Add( FieldByName ('TABLE_NAME').AsString );
               Next;
          end;
     end;
end;

procedure TdmSistema.InvocarExportarTablasXML;
begin
     with cdsSistBaseDatos do
     begin
          if ( not Active ) or IsEmpty then
             ZetaDialogo.ZError( 'Exportar Tablas', 'No se pueden exportar tablas, se requiere indicar una base de datos', 0 )
          else
          begin
               ZBaseDlgModal_DevEx.ShowDlgModal( ExportarTablas_DevEx, TExportarTablas_DevEx );  //DevEx
          end;
     end;
end;

function TdmSistema.BuildEmpresaDBInfo: OleVariant;
begin
     with cdsSistBaseDatos do
     begin
          Result := VarArrayOf( [ FieldByName( 'DB_DATOS' ).AsString,
                                  FieldByName( 'DB_USRNAME' ).AsString,
                                  FieldByName( 'DB_PASSWRD' ).AsString,
                                  Self.Usuario,
                                  ZetaCommonClasses.VACIO,
                                  ZetaCommonClasses.VACIO,
                                  dmCliente.GetGrupoActivo,
                                  dmCliente.ApplicationID ] );
     end;
end;

procedure TdmSistema.GetTablaInfo( const sTablaName: String; cdsTabla: TZetaClientDataSet );
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourGlass;
        with cdsSistBaseDatos do
        begin
             cdsTabla.Data := ServerSistema.GetTablaInfo( BuildEmpresaDBInfo, sTablaName );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmSistema.GetBaseDatosImportacion: OleVariant;
begin
     with cdsSistBaseDatos do
     begin
          Result := VarArrayOf( [ FieldByName( 'DB_DATOS' ).AsString,
                             FieldByName( 'DB_USRNAME' ).AsString,
                             FieldByName( 'DB_PASSWRD' ).AsString,
                             Self.Usuario,
                             // FieldByName( 'CM_NIVEL0' ).AsString,
                             FieldByName( 'DB_CODIGO' ).AsString,
                             dmCliente.GetGrupoActivo
                             // dmCliente.GetApplicationID
                             ] );
     end;
end;

{* Regresar una tabla con datos de un proceso de actualizaci�n. @autor Ricardo Carrillo}
function TdmSistema.GetMotorPatchAvance(Parametros: OleVariant): OleVariant;
begin
  Result := ServerSistema.GetMotorPatchAvance(Parametros);
end;

// US #11012 Opcion para obtener status de licencia de uso.
function TdmSistema.GetEmpleadosConteo: Integer;
begin
  Result := 0;
  with cdsSistBaseDatos do
  begin
       first;
       while not Eof do
       begin
            if FieldByName ('DB_CTRL_RL').AsString = '3DATOS'  then
               Result := Result + FieldByName ('DB_EMPLEADOS').AsInteger;
            Next;
       end;
  end;
end;

end.
