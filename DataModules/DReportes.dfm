inherited dmReportes: TdmReportes
  OldCreateOrder = True
  inherited cdsSuscripReportes: TZetaClientDataSet
    AlAdquirirDatos = cdsSuscripReportesAlAdquirirDatos
    AlCrearCampos = cdsSuscripReportesAlCrearCampos
  end
  object cdsSuscripReportesLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsSuscripReportesLookupAlAdquirirDatos
    AlCrearCampos = cdsSuscripReportesLookupAlCrearCampos
    LookupName = 'Reportes'
    LookupDescriptionField = 'RE_NOMBRE'
    LookupKeyField = 'RE_CODIGO'
    OnLookupSearch = cdsSuscripReportesLookupLookupSearch
    Left = 120
    Top = 296
  end
  object cdsLookupReportesEmpresa: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    AlAdquirirDatos = cdsLookupReportesEmpresaAlAdquirirDatos
    LookupName = 'Reportes'
    LookupDescriptionField = 'RE_NOMBRE'
    LookupKeyField = 'RE_CODIGO'
    OnLookupSearch = cdsSuscripReportesLookupLookupSearch
    Left = 120
    Top = 352
  end
end
