unit DTablas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerTablas,
{$else}
     Tablas_TLB,
{$endif}
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaClientTools,
     ZetaClientDataSet;

type
  TdmTablas = class(TDataModule)
    cdsHabitacion: TZetaLookupDataSet;
    cdsViveCon: TZetaLookupDataSet;
    cdsMonedas: TZetaLookupDataSet;
    cdsTipoCambio: TZetaClientDataSet;
    cdsTransporte: TZetaLookupDataSet;
    cdsExtra1: TZetaLookupDataSet;
    cdsExtra2: TZetaLookupDataSet;
    cdsExtra3: TZetaLookupDataSet;
    cdsExtra4: TZetaLookupDataSet;
    cdsEstado: TZetaLookupDataSet;
    cdsTipoCursos: TZetaLookupDataSet;
    cdsEstudios: TZetaLookupDataSet;
    cdsMovKardex: TZetaLookupDataSet;
    cdsNivel1: TZetaLookupDataSet;
    cdsNivel2: TZetaLookupDataSet;
    cdsNivel3: TZetaLookupDataSet;
    cdsNivel4: TZetaLookupDataSet;
    cdsNivel5: TZetaLookupDataSet;
    cdsNivel6: TZetaLookupDataSet;
    cdsNivel7: TZetaLookupDataSet;
    cdsNivel8: TZetaLookupDataSet;
    cdsNivel9: TZetaLookupDataSet;
    cdsIncidencias: TZetaLookupDataSet;
    cdsMotivoBaja: TZetaLookupDataSet;
    cdsTAhorro: TZetaLookupDataSet;
    cdsTPresta: TZetaLookupDataSet;
    cdsMotAuto: TZetaLookupDataSet;
    cdsSalMin: TZetaClientDataSet;
    cdsUma: TZetaClientDataSet;
    cdsLeyIMSS: TZetaClientDataSet;
    cdsNumericas: TZetaLookupDataSet;
    cdsGradosRiesgo: TZetaClientDataSet;
    cdsEstadoCivil: TZetaLookupDataSet;
    cdsArt80: TZetaClientDataSet;
    cdsTallas: TZetaLookupDataSet;
    cdsMotTool: TZetaLookupDataSet;
    cdsClasifiCurso: TZetaLookupDataSet;
    cdsPrioridAhorroPresta: TZetaClientDataSet;
    cdsTArt80: TZetaClientDataSet;
    cdsEditNumericas: TZetaClientDataSet;
    cdsColonia: TZetaLookupDataSet;
    cdsExtra5: TZetaLookupDataSet;
    cdsExtra6: TZetaLookupDataSet;
    cdsExtra7: TZetaLookupDataSet;
    cdsExtra8: TZetaLookupDataSet;
    cdsExtra9: TZetaLookupDataSet;
    cdsExtra11: TZetaLookupDataSet;
    cdsExtra10: TZetaLookupDataSet;
    cdsExtra12: TZetaLookupDataSet;
    cdsExtra13: TZetaLookupDataSet;
    cdsExtra14: TZetaLookupDataSet;
    cdsNivel10: TZetaLookupDataSet;
    cdsNivel11: TZetaLookupDataSet;
    cdsNivel12: TZetaLookupDataSet;
    cdsMotCheca: TZetaLookupDataSet;
    cdsOcupaNac: TZetaLookupDataSet;
    cdsAreaTemCur: TZetaLookupDataSet;
    cdsMunicipios: TZetaLookupDataSet;
    cdsMotivoTransfer: TZetaLookupDataSet;
    cdsTPerfiles: TZetaLookupDataSet;
    cdsTCompetencias: TZetaLookupDataSet;
    cdsTNacComp: TZetaLookupDataSet;
    cdsBancos: TZetaLookupDataSet;
    cdsTipoContratoSat: TZetaLookupDataSet;
    cdsTipoJornadaSat: TZetaLookupDataSet;
    cdsRiesgosPuestoSat: TZetaLookupDataSet;
    cdsRegimenContrataTrabajador: TZetaLookupDataSet;
    procedure cdsTablaNewRecord(DataSet: TDataSet);
    procedure cdsTablaBeforePost(DataSet: TDataSet);
    procedure cdsTablaAfterPost(DataSet: TDataSet);
    procedure cdsTablaAlAdquirirDatos(Sender: TObject);
    procedure cdsTablaAlModificar(Sender: TObject);
    procedure cdsTablaGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    {$ifdef VER130}
    procedure cdsTablaReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsTablaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsGradosRiesgoAfterOpen(DataSet: TDataSet);
    //procedure cdsNumericasAfterOpen(DataSet: TDataSet);
    //procedure cdsNumericasAfterCancel(DataSet: TDataSet);
    //procedure cdsNumericasBeforeInsert(DataSet: TDataSet);
    //procedure cdsNumericasAlEnviarDatos(Sender: TObject);
    //procedure cdsNumericasNewRecord(DataSet: TDataSet);
    //procedure cdsNumericasAfterPost(DataSet: TDataSet);
    //procedure cdsNumericasAlAdquirirDatos(Sender: TObject);
    procedure cdsMovKardexNewRecord(DataSet: TDataSet);
    procedure cdsArt80AfterDelete(DataSet: TDataSet);
    procedure cdsArt80NewRecord(DataSet: TDataSet);
    procedure cdsSalMinNewRecord(DataSet: TDataSet);
    procedure cdsUmaNewRecord(DataSet: TDataSet);
    procedure cdsTipoCambioNewRecord(DataSet: TDataSet);
    procedure cdsIncidenciasBeforePost(DataSet: TDataSet);
    procedure cdsIncidenciasBeforeDelete(DataSet: TDataSet);
    procedure cdsMovKardexBeforeDelete(DataSet: TDataSet);
    procedure cdsMovKardexBeforePost(DataSet: TDataSet);
    procedure cdsMotivoBajaNewRecord(DataSet: TDataSet);
    procedure cdsTAhorroBeforePost(DataSet: TDataSet);
    procedure cdsTAhorroNewRecord(DataSet: TDataSet);
    procedure cdsIncidenciasNewRecord(DataSet: TDataSet);
    procedure cdsLeyIMSSNewRecord(DataSet: TDataSet);
    procedure cdsLeyIMSSAlCrearCampos(Sender: TObject);
    procedure cdsGradosRiesgoAlCrearCampos(Sender: TObject);
    procedure cdsNumericasAlCrearCampos(Sender: TObject);
    procedure cdsArt80AlCrearCampos(Sender: TObject);
    procedure cdsSalMinAlCrearCampos(Sender: TObject);
    procedure cdsUmaAlCrearCampos(Sender: TObject);
    procedure cdsMovKardexAlCrearCampos(Sender: TObject);
    procedure cdsTipoCambioAlCrearCampos(Sender: TObject);
    procedure cdsMotToolLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsPrioridAhorroPrestaAlAdquirirDatos(Sender: TObject);
    procedure cdsTArt80AfterCancel(DataSet: TDataSet);
    procedure cdsEditNumericasAfterCancel(DataSet: TDataSet);
    procedure cdsTArt80AfterOpen(DataSet: TDataSet);
    procedure cdsEditNumericasAlAdquirirDatos(Sender: TObject);
    procedure cdsEditNumericasAlCrearCampos(Sender: TObject);
    procedure cdsEditNumericasAlEnviarDatos(Sender: TObject);
    procedure cdsNumericasAlModificar(Sender: TObject);
    procedure cdsNumericasAlAgregar(Sender: TObject);
    procedure cdsEditNumericasNewRecord(DataSet: TDataSet);
    procedure cdsTArt80NewRecord(DataSet: TDataSet);
    procedure NU_CODIGOValidate(Sender: TField);
    procedure NU_CODIGOChange(Sender: TField);
    procedure cdsTArt80AlModificar(Sender: TObject);
    procedure cdsTArt80AlBorrar(Sender: TObject);
    procedure cdsTArt80AfterDelete(DataSet: TDataSet);
    procedure cdsEditNumericasAlAgregar(Sender: TObject);
    procedure cdsTablasPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure cdsNivelesNewRecord(DataSet: TDataSet);
    procedure cdsMotAutoNewRecord(DataSet: TDataSet);
    procedure cdsMunicipiosAlCrearCampos(Sender: TObject);
    procedure cdsNivelesAlCrearCampos(Sender: TObject);
    procedure cdsNivelesBeforePost(DataSet: TDataSet);
    procedure cdsMunicipiosBeforePost(DataSet: TDataSet); 
    procedure cdsNivelesTB_CODIGOValidate(Sender: TField);
    procedure cdsNivel1FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure cdsTNacCompBeforePost(DataSet: TDataSet);
    procedure cdsTNacCompAlCrearCampos(Sender: TObject);
    procedure cdsNumericasBeforeDelete(DataSet: TDataSet);
    procedure cdsEditNumericasBeforeDelete(DataSet: TDataSet);
    procedure cdsTArt80BeforeDelete(DataSet: TDataSet);
    procedure cdsTablaSATNewRecord(DataSet: TDataSet);
    procedure cdsUmaCalcFields(DataSet: TDataSet);
    procedure cdsUmaBeforePost(DataSet: TDataSet);
    procedure cdsUmaAlAdquirirDatos(Sender: TObject);
    procedure cdsUmaAfterPost(DataSet: TDataSet);
    procedure cdsRegimenContrataTrabajadorLookupSearch(
      Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: string;
      var sKey, sDescription: string);
  private
    { Private declarations }
    //FUltimaTabla: Integer;
    FLookupMotivoTool: Boolean;
{$ifdef DOS_CAPAS}
    function GetServerTablas: TdmServerTablas;
    property ServerTablas: TdmServerTablas read GetServerTablas;
{$else}
    FServidor: IdmServerTablasDisp;
    function GetServerTablas: IdmServerTablasDisp;
    property ServerTablas: IdmServerTablasDisp read GetServerTablas;
{$endif}
    function GetIndexRights( const iTabla: Integer ): Integer;
    procedure ShowEdicion( const iTabla: Integer );
    procedure LlenarListaAhorroPrestamo( Lista: TStrings; const lAhorro: Boolean; ZetaDataset: TZetaClientDataset);
  public
    { Public declarations }
    property LookupMotivoTool : Boolean read FLookupMotivoTool write FLookupMotivoTool;
    function GrabaPrioridad( Lista: TStrings ): boolean;
    function GetCodigoEstudio( const Estudios: eEstudios ): String;
    function cdsSupervisores: TZetaLookupDataSet;
    function GetEntidadAdicional( const eTipoEntidad: TipoEntidad ): TZetaLookupDataSet;
    function GetEntidadAdicionalConnect( const eTipoEntidad: TipoEntidad ): TZetaLookupDataSet;
    function GetEstadoCURP( const sEstado: String ): String;
    procedure LlenarListaAhorros( Lista: TStrings );
    procedure LlenarListaPrestamos( Lista: TStrings );
    procedure SetLookupNames;
    procedure CargaListaTArt80( oItems: TStrings );
    function GetNumericaCuota( const iNumerica: Integer; const dFecha: TDate; const rValor: Double ): Double;
    function GetConcepto( const sTipo: String; const lPresta, lDeduccion: Boolean ): Integer;
    procedure cdsMovKardexCambiaEmpresa( sEmpresa: String ); {OP: 26/05/08}
    function GetDataSetTransferencia( const lLevantaException: Boolean = TRUE): TZetaLookupDataset;
    function HayDataSetTransferencia( var sMensaje: String ): Boolean;
    procedure ValidaMemoField( DataSet : TDataSet; const sField : string );
  end;

const
     kTipoCambio = 1 ;
     kSalMin = 2 ;
     kLeyIMSS = 3 ;
     kNumericas = 4 ;
     kGradosRiesgo = 5 ;
     kHabitacion = 6 ;
     kEstadoCivil = 7 ;
     kViveCon = 8 ;
     kTransporte = 9 ;
     kExtra1 = 10;
     kExtra2 = 11;
     kExtra3 = 12;
     kExtra4 = 13;
     kEstado = 14;
     kTipoCursos = 15;
     kEstudios = 16;
     kMovKardex = 17;
     kNivel1 = 18;
     kNivel2 = 19;
     kNivel3 = 20;
     kNivel4 = 21;
     kNivel5 = 22;
     kNivel6 = 23;
     kNivel7 = 24;
     kNivel8 = 25;
     kNivel9 = 26;
     kIncidencias = 27;
     kMotivoBaja = 28;
     kTAhorro = 29;
     kTPresta = 30;
     kMotAuto = 31 ;
     kMonedas = 32;
     kTallas = 33;
     kMotTool = 34;
     kClasifiCurso = 35;
     kTArt80 = 36;
     kColonia = 37;
     kExtra5 = 38;
     kExtra6 = 39;
     kExtra7 = 40;
     kExtra8 = 41;
     kExtra9 = 42;
     kExtra10 = 43;
     kExtra11 = 44;
     kExtra12 = 45;
     kExtra13 = 46;
     kExtra14 = 47;
     kMotCheca = 48;
     {$ifdef ACS}
     kNivel10 = 49;
     kNivel11 = 50;
     kNivel12 = 51;
     {$endif}
     kOcupaNac    = 52;
     kAreaTemCur  = 53;
     kMunicipios  = 54;
     kMotTransfer = 55;
     kTCompetencias = 56;
     kTPerfiles = 57;
     kTNacComp = 58;
     kBancos = 59;
     kTipoContratoSAT = 60;
     kTipoJornadaSAT = 61;
     kRiesgoPuestoSAT = 62;
     kUma = 63 ;

     K_SIN_CONFIG_COSTEO = 'El Nivel de Organigrama para Costeo no ha sido definido.';
var
   dmTablas: TdmTablas;

implementation

uses ZReconcile,
     ZBaseEdicion_DevEx,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaBusqueda_DevEx,
     ZetaDialogo,
     ZetaDataSetTools,
     ZAccesosTress,
     ZAccesosMgr,
     DGlobal,
     DCliente,
     DSistema,
     FEditTablas_DevEx,  //DevEx
     FEditMovKardex_DevEx,
     FEditMotivoBaja_DevEx,
     FEditSalMin_DevEx,
     FEditLeyIMSS_DevEx,
     FEditGradoRiesgo_DevEx,
     FEditTAhorro_DevEx,
     FEditTPrestamo_DevEx,
     FEditTipoCambio_DevEx,
     FEditMonedas_DevEx,
     FEditNumerica_DevEx,
     FEditIncidencias_DevEx,
     FEditMotTools_DevEx,            //DevEx (icm)
     FEditGradoEstudio_DevEx,
     FEditTArt80_DevEx,
     FEditEntidad_DevEx,
     FEditColonia_DevEx,
     FEditNiveles_DevEx,
     FEditMotivoAuto_DevEx,
     FEditMotivoChecaManual_DevEx,
     FEditMunicipios_DevEx,
     FEditTNacCompetencias_DevEx,
     FEditBancos_DevEx,
     FEditTabOfiSatTipoContrato_DevEx,
     FEditTabOfiSatTipoJornada_DevEx,
     FEditTabOfiSatRiesgoPuesto_DevEx,
     FEditUma_DevEx;

var
   EditEstadoCivil_DevEx : TTOEditEstadoCivil_DevEx;
   EditHabitacion_DevEx: TTOEditHabitacion_DevEx ;
   EditViveCon_DevEx: TTOEditViveCon_DevEx ;
   EditTransporte_DevEx: TTOEditTransporte_DevEx ;
   EditExtra1_DevEx: TTOEditExtra1_DevEx;
   EditExtra2_DevEx: TTOEditExtra2_DevEx;
   EditExtra3_DevEx: TTOEditExtra3_DevEx;
   EditExtra4_DevEx: TTOEditExtra4_DevEx;
   EditEstado_DevEx: TTOEditEntidad_DevEx ;
   EditTipoCursos_DevEx: TTOEditTipoCursos_DevEx ;
   EditEstudios_DevEx: TTOEditEstudios_DevEx;
   EditNivel1_DevEx: TTOEditNivel1_DevEx ;
   EditNivel2_DevEx: TTOEditNivel2_DevEx ;
   EditNivel3_DevEx: TTOEditNivel3_DevEx ;
   EditNivel4_DevEx: TTOEditNivel4_DevEx ;
   EditNivel5_DevEx: TTOEditNivel5_DevEx ;
   EditNivel6_DevEx: TTOEditNivel6_DevEx ;
   EditNivel7_DevEx: TTOEditNivel7_DevEx ;
   EditNivel8_DevEx: TTOEditNivel8_DevEx ;
   EditNivel9_DevEx: TTOEditNivel9_DevEx ;
   {$ifdef ACS}
   EditNivel10_DevEx: TTOEditNivel9_DevEx ;
   EditNivel11_DevEx: TTOEditNivel9_DevEx ;
   EditNivel12_DevEx: TTOEditNivel9_DevEx ;
   {$endif}
   EditMotivoAutorizacion_DevEx: TTOEditMotivoAutorizacion_DevEx ;
   EditMovKardex_DevEx : TEditMovKardex_DevEx;
   EditSalMin_DevEx: TEditSalMin_DevEx;
   EditUma_DevEx: TEditUma_DevEx;
   EditLeyIMSS_DevEx: TEditLeyIMSS_DevEx;
   EditGradoRiesgo_DevEx: TEditGradoRiesgo_DevEx;
   EditIncidencias_DevEx: TEditIncidencias_DevEx;
   EditMotivoBaja_DevEx: TTOEditMotivoBaja_DevEx;
   EditTallas_DevEx: TTOTallas_DevEx;    //DevEx
   EditClasifiCurso_DevEx: TTOTEditClasifiCurso_DevEx;
   EditColonia_DevEx: TTOEditColonia_DevEx;
   EditExtra5_DevEx: TTOEditExtra5_DevEx;
   EditExtra6_DevEx: TTOEditExtra6_DevEx;
   EditExtra7_DevEx: TTOEditExtra7_DevEx;
   EditExtra8_DevEx: TTOEditExtra8_DevEx;
   EditExtra9_DevEx: TTOEditExtra9_DevEx;
   EditExtra10_DevEx: TTOEditExtra10_DevEx;
   EditExtra11_DevEx: TTOEditExtra11_DevEx;
   EditExtra12_DevEx: TTOEditExtra12_DevEx;
   EditExtra13_DevEx: TTOEditExtra13_DevEx;
   EditExtra14_DevEx: TTOEditExtra14_DevEx;
   EditMotivoChecaManual_DevEx: TTOEditMotivoChecaManual_DevEx;
   EditOcupaNac_DevEx : TTOTEditOcupaNac_DevEx;
   EditAreaTemCur_DevEx : TTOTEditAreaTemCur_DevEx;
   EditMotivoTransfer_DevEx : TTOMotivoTransfer_DevEx;
   EditTCompetencias_DevEx : TEditTCompetencas_DevEx;
   EditTPerfiles_DevEx : TEditTPerfiles_DevEx;
   EditTNacCompetencias_DevEx: TEditTNacCompetencias_DevEx;
{$R *.DFM}

const
     K_ESPACIO = ' ';

{ *********** TdmTablas ********** }

{$ifdef DOS_CAPAS}
function TdmTablas.GetServerTablas: TdmServerTablas;
begin
     Result := DCliente.dmCliente.ServerTablas;
end;
{$else}
function TdmTablas.GetServerTablas: IdmServerTablasDisp;
begin
     Result := IdmServerTablasDisp( dmCliente.CreaServidor( CLASS_dmServerTablas, FServidor ) );
end;
{$endif}

procedure TdmTablas.ShowEdicion( const iTabla: Integer );
begin
       case iTabla of
          kTipoCambio:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoCambio_DevEx, TEditTipoCambio_DevEx );
          kSalMin:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditSalMin_DevEx, TEditSalMin_DevEx );
          kLeyIMSS:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditLeyIMSS_DevEx, TEditLeyIMSS_DevEx );
          kNumericas:   ZBaseEdicion_DevEx.ShowFormaEdicion( EditNumerica_DevEx, TEditNumerica_DevEx );
          kGradosRiesgo:ZBaseEdicion_DevEx.ShowFormaEdicion( EditGradoRiesgo_DevEx, TEditGradoRiesgo_DevEx );
          kHabitacion:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditHabitacion_DevEx, TTOEditHabitacion_DevEx );
          kEstadoCivil: ZBaseEdicion_DevEx.ShowFormaEdicion( EditEstadoCivil_DevEx, TTOEditEstadoCivil_DevEx);
          kViveCon:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditViveCon_DevEx, TTOEditViveCon_DevEx );
          kTransporte:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTransporte_DevEx, TTOEditTransporte_DevEx );
          kExtra1:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra1_DevEx, TTOEditExtra1_DevEx );
          kExtra2:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra2_DevEx, TTOEditExtra2_DevEx);
          kExtra3:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra3_DevEx, TTOEditExtra3_DevEx );
          kExtra4:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra4_DevEx, TTOEditExtra4_DevEx );
          kEstado:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditEstado_DevEx, TTOEditEntidad_DevEx );
          kTipoCursos:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoCursos_DevEx, TTOEditTipoCursos_DevEx ); //DevEx (by am)
          kEstudios:    ZBaseEdicion_DevEx.ShowFormaEdicion( EditEstudios_DevEx, TTOEditEstudios_DevEx );
          kMovKardex:   ZBaseEdicion_DevEx.ShowFormaEdicion( EditMovKardex_DevEx, TEditMovKardex_DevEx );

          //De la forma ShowFormaEdicionAndFree  cada vez que se cierra la pantalla de edici�n esta es liberada
          //para que se vuelva a crear y ordenar los componentes segun los globales de Enrolamiento en caso de Reabrir la empresa
          kNivel1:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel1_DevEx, TTOEditNivel1_DevEx );
          kNivel2:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel2_DevEx, TTOEditNivel2_DevEx );
          kNivel3:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel3_DevEx, TTOEditNivel3_DevEx );
          kNivel4:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel4_DevEx, TTOEditNivel4_DevEx );
          kNivel5:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel5_DevEx, TTOEditNivel5_DevEx );
          kNivel6:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel6_DevEx, TTOEditNivel6_DevEx );
          kNivel7:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel7_DevEx, TTOEditNivel7_DevEx );
          kNivel8:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel8_DevEx, TTOEditNivel8_DevEx );
          kNivel9:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel9_DevEx, TTOEditNivel9_DevEx );
          {$ifdef ACS}
          kNivel10:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel10_DevEx, TTOEditNivel10_DevEx );
          kNivel11:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel11_DevEx, TTOEditNivel11_DevEx );
          kNivel12:      ZBaseEdicion_DevEx.ShowFormaEdicionAndFree( EditNivel12_DevEx, TTOEditNivel12_DevEx );
          {$endif}
          kIncidencias:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditIncidencias_DevEx, TEditIncidencias_DevEx );
          kMotivoBaja:   ZBaseEdicion_DevEx.ShowFormaEdicion( EditMotivoBaja_DevEx, TTOEditMotivoBaja_DevEx );
          kTAhorro:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditTAhorro_DevEx, TEditTAhorro_DevEx );
          kTPresta:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditTPrestamo_DevEx, TEditTPrestamo_DevEx );
          kMotAuto:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditMotivoAutorizacion_DevEx, TTOEditMotivoAutorizacion_DevEx );
          kMonedas:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditMonedas_DevEx, TEditMonedas_DevEx );
          kTallas:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditTallas_DevEx, TTOTallas_DevEx );
          kMotTool:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditMotTools_DevEx, TEditMotTools_DevEx );
          kClasifiCurso:ZBaseEdicion_DevEx.ShowFormaEdicion( EditClasifiCurso_DevEx, TTOTEditClasifiCurso_DevEx ); //DevEx (by am)
          kTArt80:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditTArt80_DevEx, TEditTArt80_DevEx );
          kColonia:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditColonia_DevEx, TTOEditColonia_DevEx);
          kExtra5:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra5_DevEx, TTOEditExtra5_DevEx );
          kExtra6:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra6_DevEx, TTOEditExtra6_DevEx );
          kExtra7:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra7_DevEx, TTOEditExtra7_DevEx );
          kExtra8:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra8_DevEx, TTOEditExtra8_DevEx );
          kExtra9:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra9_DevEx, TTOEditExtra9_DevEx );
          kExtra10:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra10_DevEx, TTOEditExtra10_DevEx );
          kExtra11:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra11_DevEx, TTOEditExtra11_DevEx );
          kExtra12:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra12_DevEx, TTOEditExtra12_DevEx );
          kExtra13:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra13_DevEx, TTOEditExtra13_DevEx );
          kExtra14:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditExtra14_DevEx, TTOEditExtra14_DevEx );
          kMotCheca:    ZBaseEdicion_DevEx.ShowFormaEdicion( EditMotivoChecaManual_DevEx, TTOEditMotivoChecaManual_DevEx );
          kOcupaNac:    ZBaseEdicion_DevEx .ShowFormaEdicion( EditOcupaNac_DevEx,TTOTEditOcupaNac_DevEx  );
          kAreaTemCur:  ZBaseEdicion_DevEx .ShowFormaEdicion( EditAreaTemCur_DevEx,TTOTEditAreaTemCur_DevEx  );
          kMunicipios:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditMunicipios_DevEx,TEditMunicipios_DevEx);
          kMotTransfer: ZBaseEdicion_DevEx.ShowFormaEdicion( EditMotivoTransfer_DevEx, TTOMotivoTransfer_DevEx );
          kTCompetencias:ZBaseEdicion_DevEx .ShowFormaEdicion( EditTCompetencias_DevEx, TEditTCompetencas_DevEx );
          kTPerfiles :ZBaseEdicion_DevEx .ShowFormaEdicion( EditTPerfiles_DevEx, TEditTPerfiles_DevEx );
          kTNacComp :ZBaseEdicion_DevEx.ShowFormaEdicion( EditTNacCompetencias_DevEx , TEditTNacCompetencias_DevEx );
          kBancos:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditBancos_DevEx,TEditBancos_DevEx);
          kTipoContratoSAT:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTabOfiSatTipoContrato_DevEx,TEditTabOfiSatTipoContrato_DevEx);
          kTipoJornadaSAT:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTabOfiSatTipoJornada_DevEx,TEditTabOfiSatTipoJornada_DevEx);
          kRiesgoPuestoSAT:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditTabOfiSatRiesgoPuesto_DevEx,TEditTabOfiSatRiesgoPuesto_DevEx);
          kUma:      ZBaseEdicion_DevEx.ShowFormaEdicion( EditUma_DevEx, TEditUma_DevEx );
     end;  //end case
end;

function TdmTablas.GetIndexRights( const iTabla: Integer ): Integer;
begin
     case iTabla of
          kTipoCambio: Result := D_TAB_OFI_TIPOS_CAMBIO;
          kSalMin: Result := D_TAB_OFI_SAL_MINIMOS;
          kLeyIMSS: Result := D_TAB_OFI_CUOTAS_IMSS;
          kNumericas: Result := D_TAB_OFI_ISPT_NUMERICAS;
          kGradosRiesgo: Result := D_TAB_OFI_GRADOS_RIESGO;
          kHabitacion: Result := D_TAB_PER_VIVE_EN;
          kEstadoCivil: Result := D_TAB_PER_EDOCIVIL;
          kViveCon: Result := D_TAB_PER_VIVE_CON;
          kTransporte: Result := D_TAB_PER_TRANSPORTE;
          kExtra1: Result := D_TAB_ADICION_TABLA1;
          kExtra2: Result := D_TAB_ADICION_TABLA2;
          kExtra3: Result := D_TAB_ADICION_TABLA3;
          kExtra4: Result := D_TAB_ADICION_TABLA4;
          kEstado: Result := D_TAB_PER_ESTADOS;
          kTipoCursos: Result := D_CAT_CAPA_TIPO_CURSO;
          kEstudios: Result := D_TAB_PER_ESTUDIOS;
          kMovKardex: Result := D_TAB_HIST_TIPO_KARDEX;
          kNivel1: Result := D_TAB_AREAS_NIVEL1;
          kNivel2: Result := D_TAB_AREAS_NIVEL2;
          kNivel3: Result := D_TAB_AREAS_NIVEL3;
          kNivel4: Result := D_TAB_AREAS_NIVEL4;
          kNivel5: Result := D_TAB_AREAS_NIVEL5;
          kNivel6: Result := D_TAB_AREAS_NIVEL6;
          kNivel7: Result := D_TAB_AREAS_NIVEL7;
          kNivel8: Result := D_TAB_AREAS_NIVEL8;
          kNivel9: Result := D_TAB_AREAS_NIVEL9;
          {$ifdef ACS}
          kNivel10: Result := D_TAB_AREAS_NIVEL10;
          kNivel11: Result := D_TAB_AREAS_NIVEL11;
          kNivel12: Result := D_TAB_AREAS_NIVEL12;
          {$endif}
          kIncidencias: Result := D_TAB_HIST_INCIDEN;
          kMotivoBaja: Result := D_TAB_HIST_MOT_BAJA;
          kTAhorro: Result := D_TAB_NOM_TIPO_AHORRO;
          kTPresta: Result := D_TAB_NOM_TIPO_PRESTA;
          kMotAuto: Result := D_TAB_HIST_AUT_EXTRAS;
          kMonedas: Result := D_TAB_NOM_MONEDAS;
          kTallas: Result := D_TAB_HERR_TALLAS;
          KMotTool: Result := D_TAB_HERR_MOTTOOL;
          kClasifiCurso: Result := D_CAT_CLASIFI_CURSO;
          kTArt80: Result := D_TAB_OFI_ISPT_NUMERICAS;
          kColonia: Result := D_TAB_COLONIA;
          kExtra5: Result := D_TAB_ADICION_TABLA5;
          kExtra6: Result := D_TAB_ADICION_TABLA6;
          kExtra7: Result := D_TAB_ADICION_TABLA7;
          kExtra8: Result := D_TAB_ADICION_TABLA8;
          kExtra9: Result := D_TAB_ADICION_TABLA9;
          kExtra10: Result := D_TAB_ADICION_TABLA10;
          kExtra11: Result := D_TAB_ADICION_TABLA11;
          kExtra12: Result := D_TAB_ADICION_TABLA12;
          kExtra13: Result := D_TAB_ADICION_TABLA13;
          kExtra14: Result := D_TAB_ADICION_TABLA14;
          kMotCheca: Result := D_TAB_HIST_CHECA_MANUAL;
          kAreaTemCur: Result := D_TAB_OFI_STPS_ARE_TEM_CUR;
          kMunicipios: Result := D_TAB_PER_MUNICIPIOS;
          kMotTransfer: Result := D_COSTEO_MOTIVOS_TRANSFER;
          kTCompetencias :Result := D_TAB_TCOMPETENCIAS;
          kTPerfiles  :Result := D_TAB_TPERFILES;
          kTNacComp  :Result := D_CAT_TAB_NAC_COMP;
          kTipoContratoSAT :Result := D_TAB_OFI_SAT_TIPO_CONTRATO;
          kTipoJornadaSAT :Result := D_TAB_OFI_SAT_TIPO_JORNADA;
          kRiesgoPuestoSAT :Result := D_TAB_OFI_SAT_RIESGO_PUESTO;
          kUma: Result := D_TAB_UMA;
     else
         Result := 0;
     end;
end;

{ cdsNumericas }

procedure TdmTablas.cdsNumericasAlCrearCampos(Sender: TObject);
begin
     cdsNumericas.MaskPesos( 'TB_NUMERO' );
end;

procedure TdmTablas.cdsNumericasAlAgregar(Sender: TObject);
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             with cdsEditNumericas do
             begin
                  if ( not Active ) then
                     Refrescar;            // Para traer la estructura si nunca se ha inicializado
                  EmptyDataSet;            // Siempre limpiar� el DataSet antes de agregar
                  cdsTArt80.EmptyDataSet;
                  Append;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

procedure TdmTablas.cdsNumericasAlModificar(Sender: TObject);
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             cdsEditNumericas.Refrescar;
             cdsTArt80.Last;   // Se posiciona en el mas reciente
          finally
                 Cursor := oCursor;
          end;
     end;
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

{
procedure TdmTablas.cdsNumericasAlAdquirirDatos(Sender: TObject);
begin
     cdsNumericas.Data := ServerTablas.GetNumericas( dmCliente.Empresa );
end;

procedure TdmTablas.cdsNumericasAfterOpen(DataSet: TDataSet);
begin
     cdsArt80.DataSetField := TDataSetField( cdsNumericas.FieldByName( 'qryDetail' ) );
end;

procedure TdmTablas.cdsNumericasAfterCancel(DataSet: TDataSet);
begin
     cdsArt80.CancelUpdates;
end;

procedure TdmTablas.cdsNumericasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with cdsNumericas do
     begin
          Post;
          if ( Changecount > 0 ) then
             Reconcile( ServerTablas.GrabaNumerica( dmCliente.Empresa, Delta, ErrorCount ) );
     end;
end;

procedure TdmTablas.cdsNumericasBeforeInsert(DataSet: TDataSet);
begin
     with cdsNumericas do
     begin
          DisableControls;
          Last;
          FUltimaTabla := FieldByName( 'NU_CODIGO' ).AsInteger;
          EnableControls;
     end;
end;

procedure TdmTablas.cdsNumericasNewRecord(DataSet: TDataSet);
begin
     with cdsNumericas do
     begin
          FieldByName( 'NU_CODIGO' ).AsInteger := FUltimaTabla + 1;
     end;
end;

procedure TdmTablas.cdsNumericasAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with cdsNumericas do
     begin
          if ( Changecount > 0 ) then
          begin
               Reconcile( ServerTablas.GrabaNumerica( dmCliente.Empresa, Delta, ErrorCount ) );
          end;
     end;
end;
}

{ cdsEditNumericas }

procedure TdmTablas.cdsEditNumericasAlAdquirirDatos(Sender: TObject);
var
   oTArt80: OleVariant;
begin
     cdsEditNumericas.Data := ServerTablas.GetNumerica( dmCliente.Empresa,
                                                        cdsNumericas.FieldByName( 'NU_CODIGO' ).AsInteger,
                                                        oTArt80 );
     cdsTArt80.Data := oTArt80;
end;

procedure TdmTablas.cdsEditNumericasAlCrearCampos(Sender: TObject);
begin
     with cdsEditNumericas do
     begin
          MaskPesos( 'TB_NUMERO' );
          FieldByName( 'NU_CODIGO' ).OnValidate := NU_CODIGOValidate;
          FieldByName( 'NU_CODIGO' ).OnChange := NU_CODIGOChange;
     end;
end;

procedure TdmTablas.cdsEditNumericasNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'NU_CODIGO' ).AsInteger := ZetaDataSetTools.GetNextEntero( cdsNumericas, 'NU_CODIGO', 1 );
     end;
     with cdsTArt80 do
     begin
          Append;        // Se agrega un TArt80 para poder editar la tabla
          Post;
     end;
end;

procedure TdmTablas.cdsEditNumericasAlAgregar(Sender: TObject);
begin
     // No se Hace nada - Solo se usa al Editar cdsNumericas
end;

procedure TdmTablas.cdsEditNumericasAfterCancel(DataSet: TDataSet);
begin
     cdsArt80.CancelUpdates;
     cdsTArt80.CancelUpdates;
end;

procedure TdmTablas.cdsEditNumericasAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorArt80: Integer;
   oNumericas, oArt80Result: OleVariant;
begin
     cdsArt80.PostData;
     cdsTArt80.PostData;
     cdsEditNumericas.PostData;
     with cdsEditNumericas do
     begin
          if ( Changecount > 0 ) or ( cdsTArt80.ChangeCount > 0 ) or
             ( cdsArt80.ChangeCount > 0 ) then
          begin
               oNumericas := ServerTablas.GrabaNumerica( dmCliente.Empresa, DeltaNull,
                                                         cdsTArt80.DeltaNull, ErrorCount,
                                                         ErrorArt80, oArt80Result );
               if Reconcile( oNumericas ) and cdsTArt80.Reconcile( oArt80Result ) then
               begin
                    if ( ErrorCount = 0 ) then   // Si no hay problemas con Numericas se puede refrescar
                    begin
                         cdsNumericas.Refrescar;
                         cdsNumericas.Locate( 'NU_CODIGO', FieldByName( 'NU_CODIGO' ).AsInteger, [] );
                    end;
               end;
          end;
     end;
end;

procedure TdmTablas.NU_CODIGOValidate(Sender: TField);
var
   sMensaje: String;
begin
     if ( Sender.DataSet.State = dsEdit ) then         // Solo afecta cambiar el c�digo cuando se edita un registro ya existente
     begin
          sMensaje := 'Al Cambiar El C�digo Se Inhibe' + CR_LF + 'La Edici�n De La Tabla';
          if ( ( cdsTArt80.ChangeCount > 0 ) or ( cdsArt80.ChangeCount > 0 ) ) then
             sMensaje := sMensaje + CR_LF + '<Se Cancelar�n Los Cambios Realizados>';
          if ( not ZetaDialogo.ZWarningConfirm( cdsNumericas.LookupName, sMensaje + CR_LF +
                                                CR_LF + '� Desea Continuar ?', 0, mbOk ) ) then
             Abort;
     end;
end;

procedure TdmTablas.NU_CODIGOChange(Sender: TField);

   procedure CambiaCodigoNumerica;
   var
      Pos : TBookMark;
   begin
        with cdsTArt80 do
        begin
             if ( not IsEmpty ) then
             begin
                  DisableControls;
                  Pos := GetBookMark;
                  try
                     First;
                     while ( not EOF ) do       // Est� ordenado ascendente y se ocupa descendente en el combo
                     begin
                          Edit;
                          FieldByName( 'NU_CODIGO' ).AsInteger := Sender.AsInteger;
                          Next;
                     end;
                  finally
                         if ( Pos <> nil ) then
                         begin
                              GotoBookMark( Pos );
                              FreeBookMark( Pos );
                         end;
                         EnableControls;
                  end;
             end;
        end;
   end;

begin
     with Sender do
     begin
          if ( Dataset.State = dsEdit ) then     // Si se cambia el c�digo de un NUMERICA ya existente se deshacen los cambios y en la interfase se prohibe la edici�n
          begin
               cdsArt80.CancelUpdates;
               cdsTArt80.CancelUpdates;
          end
          else if ( Dataset.State = dsInsert ) then   // Propagar el cambio de c�digo a los cdsTArt80 que se hayan agregado
          begin
               CambiaCodigoNumerica;
          end;
     end;
end;

{ cdsTArt80 }

procedure TdmTablas.cdsTArt80AfterOpen(DataSet: TDataSet);
begin
     cdsArt80.DataSetField := TDataSetField( DataSet.FieldByName( 'qryDetail' ) );
end;

procedure TdmTablas.cdsTArt80NewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'NU_CODIGO' ).AsInteger  := cdsEditNumericas.FieldByName( 'NU_CODIGO' ).AsInteger;
          FieldByName( 'TI_INICIO' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'TI_DESCRIP' ).AsString  := cdsEditNumericas.FieldByName( 'NU_DESCRIP' ).AsString;   // Se sugiere la Descripci�n de N�merica
     end;
end;

procedure TdmTablas.cdsTArt80AlModificar(Sender: TObject);
begin
     cdsArt80.PostData;                    // Se aplica cualquier cambio pendiente en cdsArt80 para que no afecte si se cancela
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

procedure TdmTablas.cdsTArt80AlBorrar(Sender: TObject);
begin
     with cdsTArt80 do
     begin
          if ( not IsEmpty ) and
             ZetaDialogo.ZWarningConfirm( cdsNumericas.LookupName, 'Se Borrar� La Tabla Vigente Desde ' +
                                          FechaCorta( FieldByName( 'TI_INICIO' ).AsDateTime ) +
                                          CR_LF + CR_LF + '� Seguro De Borrar El Registro ?', 0, mbCancel ) then
             Delete;
     end;
end;

procedure TdmTablas.cdsTArt80AfterCancel(DataSet: TDataSet);
begin
     //cdsArt80.CancelUpdates;     // Se cancela en Cascada desde cdsEditNumericas
end;

procedure TdmTablas.cdsTArt80AfterDelete(DataSet: TDataSet);
begin
     with cdsEditNumericas do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmTablas.CargaListaTArt80( oItems: TStrings );

   procedure AgregaItemsTArt80;
   var
      dFecha : TDate;
      Pos : TBookMark;
   begin
        with cdsTArt80 do
        begin
             if ( not IsEmpty ) then
             begin
                  DisableControls;
                  Pos := GetBookMark;
                  try
                     Last;
                     while ( not BOF ) do       // Est� ordenado ascendente y se ocupa descendente en el combo
                     begin
                          dFecha := FieldByName( 'TI_INICIO' ).AsDateTime;
                          oItems.Add( FechaAsStr( dFecha ) + '=' + FechaCorta( dFecha ) );
                          Prior;
                     end;
                  finally
                         if ( Pos <> nil ) then
                         begin
                              GotoBookMark( Pos );
                              FreeBookMark( Pos );
                         end;
                         EnableControls;
                  end;
             end;
        end;
   end;

begin
     with oItems do
     begin
          BeginUpdate;
          try
             Clear;
             AgregaItemsTArt80;
          finally
                 EndUpdate;
          end;
     end;
end;

{ cdsArt80 }

procedure TdmTablas.cdsArt80AlCrearCampos(Sender: TObject);
begin
     with cdsArt80 do
     begin
          MaskPesos( 'A80_LI' );
          MaskPesos( 'A80_CUOTA' );
          MaskTasa( 'A80_TASA' );
     end;
end;

procedure TdmTablas.cdsArt80NewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'NU_CODIGO' ).AsInteger := cdsTArt80.FieldByName( 'NU_CODIGO' ).AsInteger;
          FieldByName( 'TI_INICIO' ).AsDateTime := cdsTArt80.FieldByName( 'TI_INICIO' ).AsDateTime;
          FieldByName( 'A80_LI' ).AsFloat := 0;
          FieldByName( 'A80_CUOTA' ).AsFloat := 0;
          FieldByName( 'A80_TASA' ).AsFloat := 0;
     end;
end;

procedure TdmTablas.cdsTablaSATNewRecord(DataSet: TDataSet);
begin
    DataSet.FieldByName( 'TB_ACTIVO').AsString:= K_GLOBAL_SI;
end;

procedure TdmTablas.cdsArt80AfterDelete(DataSet: TDataSet);
begin
     self.cdsTArt80AfterDelete( DataSet );
     cdsTArt80.PostData;           // Se aplican los cambios a cdsTArt80 para que no afecte a cdsArt80 si se edita en forma individual
end;

{ ************ cdsLeyIMSS ************* }

procedure TdmTablas.cdsLeyIMSSAlCrearCampos(Sender: TObject);
begin
     with cdSLeyIMSS do
     begin
          MaskTasa( 'SS_O_A1062' );
          MaskTasa( 'SS_O_A1062' );
          MaskTasa( 'SS_O_A1062' );
          MaskTasa( 'SS_O_A107' );
          MaskTasa( 'SS_O_A25' );
          MaskTasa( 'SS_O_IV' );
          MaskTasa( 'SS_O_CV' );
          MaskTasa( 'SS_P_A1061' );
          MaskTasa( 'SS_P_A1062' );
          MaskTasa( 'SS_P_A107' );
          MaskTasa( 'SS_P_A25' );
          MaskTasa( 'SS_P_CV' );
          MaskTasa( 'SS_P_GUARD' );
          MaskTasa( 'SS_P_INFO' );
          MaskTasa( 'SS_P_IV' );
          MaskTasa( 'SS_P_SAR' );
          MaskFecha('SS_INICIAL'); //DevEx
     end;
end;

procedure TdmTablas.cdsLeyIMSSNewRecord(DataSet: TDataSet);
begin
     cdsLeyIMSS.FieldByName( 'SS_INICIAL' ).AsDateTime := dmCliente.FechaDefault;
end;

{ *********** cdsGradosRiesgo ********** }

procedure TdmTablas.cdsGradosRiesgoAlCrearCampos(Sender: TObject);
begin
     cdsGradosRiesgo.MaskNumerico( 'RI_PRIMA', '#,#0.00000' );
end;

procedure TdmTablas.cdsGradosRiesgoAfterOpen(DataSet: TDataSet);
begin
end;

{ cdsSalMin }

procedure TdmTablas.cdsSalMinAlCrearCampos(Sender: TObject);
begin
     with cdsSalMin do
     begin
          MaskPesos( 'SM_ZONA_A' );
          MaskPesos( 'SM_ZONA_B' );
          MaskPesos( 'SM_ZONA_C' );
          MaskFecha('SM_FEC_INI');    //DevEx
     end;
end;

procedure TdmTablas.cdsSalMinNewRecord(DataSet: TDataSet);
begin
     with cdsSalMin do
     begin
          FieldByName( 'SM_FEC_INI' ).AsDateTime := Date;
     end;
end;

{ cdsValUma }


procedure TdmTablas.cdsUmaAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( DataSet ) do
     begin
          if ( Changecount > 0 ) then
             Reconcile( ServerTablas.GrabaTabla( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
             Refrescar;
     end;
end;

procedure TdmTablas.cdsUmaAlAdquirirDatos(Sender: TObject);
begin
     cdsUma.Data := ServerTablas.GetUMA( dmCliente.Empresa );
end;

procedure TdmTablas.cdsUmaAlCrearCampos(Sender: TObject);
begin
     with cdsUma do
     begin
          MaskPesos( 'UM_VALOR' );
          MaskFecha('UM_FEC_INI');    //DevEx
          MaskPesos( 'UM_FDESC' );

          CreateCalculated('UM_VALOR_MENSUAL', ftFloat, 0);
          CreateCalculated('UM_VALOR_ANUAL', ftFloat, 0);
          MaskPesos( 'UM_VALOR_MENSUAL' );
          MaskPesos( 'UM_VALOR_ANUAL' );
     end;
end;


procedure TdmTablas.cdsUmaBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
           if (FieldByName('UM_VALOR').AsFloat < 0)  then
           begin
                 DataBaseError( 'El valor diario debe ser mayor o igual a cero' );
           end;
     end;

end;

procedure TdmTablas.cdsUmaCalcFields(DataSet: TDataSet);
var
   dValorMensual, dValorAnual :  Double;
begin
    with cdsUma do
    begin
         if (FieldByName('UM_VALOR').AsFloat >= 0) then
         begin
               dValorMensual := StrToFloat ( Formatfloat('#.00', FieldByName('UM_VALOR').AsFloat * 30.4 ) );
               dValorAnual :=  StrToFloat ( Formatfloat('#.00', (dValorMensual * 12) ) );
               FieldByName('UM_VALOR_MENSUAL').AsFloat :=  dValorMensual;
               FieldByName('UM_VALOR_ANUAL').AsFloat :=  dValorAnual;
         end;
    end;
end;

procedure TdmTablas.cdsUmaNewRecord(DataSet: TDataSet);
begin
     with cdsUma do
     begin
          FieldByName( 'UM_FEC_INI' ).AsDateTime := Date;
     end;
end;

{**************************************************}
{************ METODOS PARA AGREGAR DATOS **********}
{**************************************************}

procedure TdmTablas.cdsTablaAlAdquirirDatos(Sender: TObject);
begin
     FLookupMotivoTool := FALSE;
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerTablas.GetTabla( dmCliente.Empresa, Tag );
     end;

     if ( TZetaClientDataSet( Sender ).Tag = 18 ) then
     begin
          with TZetaClientDataSet( Sender ) do
          begin
               First;
               Edit;
               while not EOF do
               begin
                   { if StrLleno( FieldByName('TB_NIVEL0').AsString )  then
                        FieldByName('TB_APLICA_CONF').AsInteger := 0;}
                    Next;
               end;
          end;

     end;

end;

procedure TdmTablas.cdsTablaAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( DataSet ) do
     begin
          if ( Changecount > 0 ) then
             Reconcile( ServerTablas.GrabaTabla( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;

{$ifdef VER130}
procedure TdmTablas.cdsTablaReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$else}
procedure TdmTablas.cdsTablaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError:string;
begin
     if FK_Violation(E)then
     begin
          with DataSet do
          begin
               sError := Format('No se Puede Borrar %s, Tiene Registros Relacionados',[DataSet.FieldByName('TB_CODIGO').AsString ]);
          end;
          ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
          case UpdateKind of
               ukDelete: Action := raCancel;
          else
              Action := raAbort;
          end;
     end
     else
         Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$endif}

procedure TdmTablas.cdsTablaAlModificar(Sender: TObject);
begin
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

procedure TdmTablas.cdsTablaNewRecord(DataSet: TDataSet);
var
   i: Integer;
begin
     with Dataset do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               if Fields[ i ] is TNumericField then
                  Fields[ i ].AsFloat := 0
               else
                   Fields[ i ].AsString := '';
          end;
     end;
end;

procedure TdmTablas.cdsTablaBeforePost(DataSet: TDataSet);
const
     CODIGO = 0;
begin
     with DataSet do
     begin
          if Fields[ CODIGO ].IsNull then
             DB.DatabaseError( 'C�digo No Puede Quedar Vac�o' );
     end;
end;

procedure TdmTablas.cdsTablaGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( GetIndexRights( Sender.Tag ), iRight );
end;

procedure TdmTablas.cdsNivelesNewRecord(DataSet: TDataSet);
begin
     Dataset.FieldByName('TB_ACTIVO').AsString:= K_GLOBAL_SI;
end;

{ ************ cdsMovKardex *********** }

procedure TdmTablas.cdsMovKardexAlCrearCampos(Sender: TObject);
begin
     cdsMovKardex.MaskPesos( 'TB_NUMERO' );
end;

procedure TdmTablas.cdsMovKardexNewRecord(DataSet: TDataSet);
begin
     with cdsMovKardex do
     begin
          FieldByName( 'TB_NIVEL' ).AsInteger := 1;
          FieldByName( 'TB_SISTEMA' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmTablas.cdsMovKardexBeforeDelete(DataSet: TDataSet);
begin
     if ZetaCommonTools.zStrToBool( cdsMovKardex.FieldByName( 'TB_SISTEMA' ).AsString ) then
        DB.DatabaseError( 'No Se Permite Borrar Movimientos de Kardex Tipo Sistema' )
end;

procedure TdmTablas.cdsMovKardexBeforePost(DataSet: TDataSet);
var
   sCodigo: String;

   function EsTipoSistema( const sTipo: String ) : Boolean;
   begin
        Result := ( sTipo = K_T_ALTA ) or ( sTipo = K_T_BAJA ) or ( sTipo = K_T_CAMBIO ) or
                  ( sTipo = K_T_RENOVA ) or ( sTipo = K_T_PUESTO ) or ( sTipo = K_T_AREA ) or
                  ( sTipo = K_T_TURNO ) or ( sTipo = K_T_PRESTA ) or ( sTipo = K_T_VACA ) or
                  ( sTipo = K_T_PERM ) or ( sTipo = K_T_INCA ) or ( sTipo = K_T_EVENTO );
   end;

begin
     with cdsMovKardex do
     begin
          sCodigo := FieldByName( 'TB_CODIGO' ).AsString;
          if strVacio( sCodigo ) then
             DatabaseError( 'El C�digo De Tipo De Kardex No Puede Quedar Vac�o' );
          if ( FieldByName( 'TB_NIVEL' ).AsInteger < 0 ) or ( FieldByName( 'TB_NIVEL' ).AsInteger > {$ifdef ACS}12{$else}9{$endif}  ) then
             DatabaseError( 'El Valor De Nivel Debe Estar Entre 0 y '+{$ifdef ACS}'12'{$else}'9'{$endif} );
          if ( State = dsInsert ) and  EsTipoSistema( sCodigo ) then
             DatabaseError( 'El C�digo De Tipo De Kardex (' + sCodigo + ') Est� reservado por el Sistema' );
     end;
end;

{ ****************** cdsTipoCambio ***************** }

procedure TdmTablas.cdsTipoCambioAlCrearCampos(Sender: TObject);
begin
     cdsTipoCambio.MaskNumerico( 'TC_MONTO', '$#,0.0000' );
     cdsTipoCambio.MaskFecha('TC_FEC_INI');
end;

procedure TdmTablas.cdsTipoCambioNewRecord(DataSet: TDataSet);
begin
     with cdsTipoCambio do
     begin
          FieldByName( 'TC_FEC_INI' ).AsDateTime := Date;
     end;
end;

{ *********** cdsIncidencias ************ }

procedure TdmTablas.cdsIncidenciasBeforePost(DataSet: TDataSet);
begin
     with cdsIncidencias do
     begin
          // if Fields[ 0 ].IsNull then
          if StrVacio( FieldByName('TB_CODIGO').AsString ) then
             DB.DatabaseError( 'C�digo No Puede Quedar Vac�o' );
          if ( eIncidencias( FieldByName( 'TB_INCIDEN' ).AsInteger ) = eiIncapacidad ) and
             ( eIncidenciaIncapacidad( FieldByName( 'TB_INCAPA' ).AsInteger ) = inNoIncapacidad ) then
             DB.DatabaseError( 'Falta Especificar Tipo de Incapacidad' );
          if ( eIncidencias( FieldByName( 'TB_INCIDEN' ).AsInteger ) <> eiPermiso  ) then
             FieldByName( 'TB_PERMISO' ).AsInteger := -1;
     end;
end;

procedure TdmTablas.cdsIncidenciasBeforeDelete(DataSet: TDataSet);
begin
     if ZetaCommonTools.zStrToBool( cdsIncidencias.FieldByName( 'TB_SISTEMA' ).AsString ) then
        DB.DataBaseError('No Se Permite Borrar Incidencias Tipo Sistema');
end;

procedure TdmTablas.cdsIncidenciasNewRecord(DataSet: TDataSet);
begin
     with cdsIncidencias do
     begin
          FieldByName( 'TB_SISTEMA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'TB_PERMISO' ).AsInteger := -1;
     end;
end;

{ *********** cdsMotivoBaja ************* }

procedure TdmTablas.cdsMotivoBajaNewRecord(DataSet: TDataSet);
begin
     cdsMotivoBaja.FieldByName( 'TB_IMSS' ).AsInteger := Ord( mbVoluntaria );
end;

{ ************* cdsTAhorro ************** }

procedure TdmTablas.cdsTAhorroBeforePost(DataSet: TDataSet);
const
     CODIGO = 0;
begin
     with DataSet do
     begin
          if StrVacio( FieldByName('TB_CODIGO').AsString ) then
          //if Fields[ CODIGO ].IsNull then
             DB.DatabaseError( 'C�digo No Puede Quedar Vac�o' );
          if ( FieldByName( 'TB_CONCEPT' ).AsInteger = 0 )then
          begin
               DB.DatabaseError( 'El Concepto de Deducci�n No Puede Quedar Vac�o' );
          end;

          if strLleno( FieldByName( 'TB_CONCEPT' ).AsString ) and strLleno( FieldByName( 'TB_RELATIV' ).AsString ) then
          begin
               if FieldByName( 'TB_CONCEPT' ).AsInteger = FieldByName( 'TB_RELATIV' ).AsInteger then
                  DB.DatabaseError( 'Los Conceptos Deducci�n y Relativo No Pueden Ser Los Mismos' );
          end;
     end;
end;

procedure TdmTablas.cdsTAhorroNewRecord(DataSet: TDataSet);
begin
     DataSet.FieldByName( 'TB_LIQUIDA' ).AsInteger := Ord( taAutomaticamente );
     DataSet.FieldByName( 'TB_ACTIVO'  ).AsString  := K_GLOBAL_SI;
     if ( DataSet = cdsTAhorro ) then
     begin
          DataSet.FieldByName( 'TB_VAL_RAN' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmTablas.LlenarListaAhorroPrestamo( Lista: TStrings; const lAhorro: Boolean; ZetaDataset: TZetaClientDataset );
var
   i : Byte;
   oObjeto: TAhorroPresta;
begin
     with Lista do
     begin
          if Count > 0 then
             for i := 0 to Count - 1 do
                 if Assigned( Objects[i] ) then
                    Objects[i].Free;
          Clear;
     end;
     with ZetaDataset do
     begin
          Conectar;
          if not IsEmpty then
          begin
               First;
               while not Eof do
               begin
                    if ( FieldByName( 'TB_ALTA' ).AsInteger > 0 ) then
                    begin
                         oObjeto := TAhorroPresta.Create;
                         with oObjeto do
                         begin
                              Tipo := FieldByName( 'TB_CODIGO' ).AsString;
                              Confirmar := ( FieldByName( 'TB_ALTA' ).AsInteger > 1 );
                              EsAhorro := lAhorro;
                         end;
                         Lista.AddObject( FieldByName( 'TB_CODIGO' ).AsString +
                                          '=' +
                                          FieldByName( 'TB_ELEMENT' ).AsString ,
                                          oObjeto );
                    end;
                    Next;
               end;
          end;
     end;
end;

procedure TdmTablas.LlenarListaAhorros( Lista: TStrings );
begin
     LlenarListaAhorroPrestamo( Lista, True, cdsTAhorro );
end;

procedure TdmTablas.LlenarListaPrestamos( Lista: TStrings );
begin
     LlenarListaAhorroPrestamo( Lista, False, cdsTPresta );
end;

function TdmTablas.cdsSupervisores: TZetaLookupDataSet;
begin

     case Global.GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR ) of
          1: Result := cdsNivel1;
          2: Result := cdsNivel2;
          3: Result := cdsNivel3;
          4: Result := cdsNivel4;
          5: Result := cdsNivel5;
          6: Result := cdsNivel6;
          7: Result := cdsNivel7;
          8: Result := cdsNivel8;
          9: Result := cdsNivel9;
          {$ifdef ACS}
          10: Result := cdsNivel10;
          11: Result := cdsNivel11;
          12: Result := cdsNivel12;
          {$endif}
     else
         Result := nil;
     end;
end;

procedure TdmTablas.cdsMotToolLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
     FLookupMotivoTool := TRUE;
     lOk := ZetaBusqueda_DevEx.ShowSearchForm( Sender, sFilter, sKey, sDescription ) ;
end;

procedure TdmTablas.SetLookupNames;
begin
     cdsNivel1.LookupName := Global.NombreNivel(1);
     cdsNivel2.LookupName := Global.NombreNivel(2);
     cdsNivel3.LookupName := Global.NombreNivel(3);
     cdsNivel4.LookupName := Global.NombreNivel(4);
     cdsNivel5.LookupName := Global.NombreNivel(5);
     cdsNivel6.LookupName := Global.NombreNivel(6);
     cdsNivel7.LookupName := Global.NombreNivel(7);
     cdsNivel8.LookupName := Global.NombreNivel(8);
     cdsNivel9.LookupName := Global.NombreNivel(9);
     {$ifdef ACS}
     cdsNivel10.LookupName := Global.NombreNivel(10);
     cdsNivel11.LookupName := Global.NombreNivel(11);
     cdsNivel12.LookupName := Global.NombreNivel(12);
     {$endif}
{
     cdsExtra1.LookupName := Global.GetGlobalString( K_GLOBAL_TAB1 );
     cdsExtra2.LookupName := Global.GetGlobalString( K_GLOBAL_TAB2 );
     cdsExtra3.LookupName := Global.GetGlobalString( K_GLOBAL_TAB3 );
     cdsExtra4.LookupName := Global.GetGlobalString( K_GLOBAL_TAB4 );
}
     {$ifndef RDDAPP}
     dmSistema.SetLookupNamesAdicionales;
     {$ENDIF}
end;

{ cdsPrioridAhorroPresta }
procedure TdmTablas.cdsPrioridAhorroPrestaAlAdquirirDatos(Sender: TObject);
begin
     cdsPrioridAhorroPresta.Data := ServerTablas.GetPrioridadDescuento( dmCliente.Empresa );
end;

procedure TdmTablas.cdsRegimenContrataTrabajadorLookupSearch(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: string; var sKey,
  sDescription: string);
begin
     lOk := ZetaBusqueda_DevEx.ShowSearchForm( Sender, sFilter, sKey, sDescription, FALSE ) ;
end;

function TdmTablas.GrabaPrioridad( Lista: TStrings ): boolean;
const
     K_MINIMA_PRIORIDAD = 1;
     K_POSICION_TIPO = 2;
var
   ErrorCount: Integer;
   i, iPrioridad: integer;
   sCodigo, sTabla: string;
begin
     cdsPrioridAhorroPresta.EmptyDataset;
     iPrioridad := K_MINIMA_PRIORIDAD;
     with Lista do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               sCodigo := Chr( Integer( Objects[ i ] ) );
               sTabla := Copy( Strings[ i ], K_POSICION_TIPO, 1 );
               with cdsPrioridAhorroPresta do
               begin
                    Append;
                    FieldByName( 'TB_CODIGO' ).AsString := sCodigo;
                    FieldByName( 'TB_CLASE' ).AsString := sTabla;
                    FieldByName( 'TB_PRIORID' ).AsInteger := iPrioridad;
                    Post;
               end;
               Inc( iPrioridad );
          end;
     end;
     ServerTablas.GrabaPrioridadDescuento( dmCliente.Empresa, cdsPrioridAhorroPresta.DeltaNull, ErrorCount );
     Result := ( ErrorCount = 0 );
end;

function TdmTablas.GetCodigoEstudio( const Estudios: eEstudios ): String;
begin
     Result := ZetaCommonClasses.VACIO;
     with cdsEstudios do
     begin
          if Locate( 'TB_TIPO', Ord( Estudios ), [] ) then
             Result := FieldByName( 'TB_CODIGO' ).AsString;
     end;
end;

procedure TdmTablas.cdsTablasPostError(DataSet: TDataSet; E: EDatabaseError;
          var Action: TDataAction);
begin
     ZetaDialogo.ZError( 'Error', GetDBErrorDescription( E ), 0 );
     Action := daAbort;
end;

function TdmTablas.GetEntidadAdicional( const eTipoEntidad: TipoEntidad ): TZetaLookupDataSet;
begin
     case eTipoEntidad of
          enExtra1 : Result := cdsExtra1;
          enExtra2 : Result := cdsExtra2;
          enExtra3 : Result := cdsExtra3;
          enExtra4 : Result := cdsExtra4;
          enExtra5 : Result := cdsExtra5;
          enExtra6 : Result := cdsExtra6;
          enExtra7 : Result := cdsExtra7;
          enExtra8 : Result := cdsExtra8;
          enExtra9 : Result := cdsExtra9;
          enExtra10 : Result := cdsExtra10;
          enExtra11 : Result := cdsExtra11;
          enExtra12 : Result := cdsExtra12;
          enExtra13 : Result := cdsExtra13;
          enExtra14 : Result := cdsExtra14;
     else
         Result := nil;
     end;
end;

function TdmTablas.GetEntidadAdicionalConnect( const eTipoEntidad: TipoEntidad ): TZetaLookupDataSet;
begin
     Result := GetEntidadAdicional( eTipoEntidad );
     if Assigned( Result ) then
        Result.Conectar;
end;

function TdmTablas.GetEstadoCURP( const sEstado: String ): String;
begin
     Result := VACIO;
     if strLleno( sEstado ) then   // Si no tiene capturado estado no se investiga nada
     begin
          with cdsEstado do
          begin
               Conectar;
               if Locate( 'TB_CODIGO', sEstado, [] ) then
                  Result := FieldByName( 'TB_CURP' ).AsString;
          end;
     end;
end;

function TdmTablas.GetNumericaCuota( const iNumerica: Integer; const dFecha: TDate; const rValor: Double ): Double;
begin
     Result:= 0;
     cdsNumericas.Conectar;
     if ( dmTablas.cdsNumericas.Locate( 'NU_CODIGO', IntToStr( iNumerica ), [] ) ) then
     begin
          dmTablas.cdsEditNumericas.Conectar;
          with cdsArt80 do
          begin
               cdsTArt80.First;
               while not cdsTArt80.EOF do
               begin
                    if ( FieldByName('TI_INICIO').AsDateTime <= dFecha) then
                    begin
                         First;
                         while not EOF do
                         begin
                              if (  rValor >= FieldByName('A80_LI').AsFloat ) then
                                   Result:= FieldByName('A80_CUOTA').AsFloat;
                              Next;
                         end;
                    end;
                    cdsTArt80.Next;
               end;
          end;
     end;
end;

function TdmTablas.GetConcepto( const sTipo: String; const lPresta, lDeduccion: Boolean ): Integer;
const
     aCampo: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('TB_RELATIV','TB_CONCEPT');
     K_NINGUNO = 0;
var
    oDataSet: TZetaLookupDataSet;
begin
     Result:= K_NINGUNO;
     if ( lPresta ) then
        oDataSet:= cdsTPresta
     else
         oDataSet:= cdsTAhorro;

     with oDataSet do
     begin
          Conectar;
          if ( Locate('TB_CODIGO', sTipo, [] ) ) then
          begin
               Result:= FieldByName(aCampo[lDeduccion]).AsInteger;
          end;
     end;
end;

{OP: 26/05/08}
{
 Este m�todo sirve en el wizard de transferencia de empleado para poder
 seleccionar el tipo de movimiento de kardex de la empresa seleccionada
 como destino.
}
procedure TdmTablas.cdsMovKardexCambiaEmpresa( sEmpresa: String );
var
   sEmpresaAnterior : String;
begin
     with dmCliente do
     begin
          sEmpresaAnterior := Compania;
          try
             FindCompany( sEmpresa );
             SetCompany;
             with cdsMovKardex do
             begin
                  Data := ServerTablas.GetTabla( Empresa, Tag );
             end;
          finally
                 FindCompany( sEmpresaAnterior );
                 SetCompany;
          end;
     end;
end;

procedure TdmTablas.cdsMotAutoNewRecord(DataSet: TDataSet);
begin
     with cdsMotAuto do
     begin
          FieldByName('TB_TIPO').AsInteger := -1;
     end;
end;

procedure TdmTablas.cdsMunicipiosAlCrearCampos(Sender: TObject);
begin
     cdsEstado.Conectar;
     with cdsMunicipios do
     begin
          CreateSimpleLookUp( cdsEstado, 'ENTIDAD_TXT', 'TB_ENTIDAD' );
     end;
end;

procedure TdmTablas.cdsNivelesAlCrearCampos(Sender: TObject);
begin
{$ifndef DOS_CAPAS}
   with TZetaClientDataSet( Sender ) do
   begin
     dmSistema.cdsUsuarios.Conectar;
     CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     FieldByName( 'TB_CODIGO' ).OnValidate := cdsNivelesTB_CODIGOValidate;
   end;
{$endif}
end;

procedure TdmTablas.cdsNivelesBeforePost(DataSet: TDataSet);
const
     K_TAG_NIVEL1 = 18;
     K_TAG_NIVEL9 = 26;
{$ifdef ACS}
     K_TAG_NIVEL10 = 49;
     K_TAG_NIVEL12 = 51;
{$endif}
{$ifndef DOS_CAPAS}
var
   iNivel : integer;
{$endif}
begin
     cdsTablaBeforePost(DataSet);
{$ifndef DOS_CAPAS}
     iNivel := 0;
if ( Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) ) then
begin
   if DataSet.Tag in [K_TAG_NIVEL1..K_TAG_NIVEL9] then
      iNivel := DataSet.Tag - K_TAG_NIVEL1 + 1 ;
   {$ifdef ACS}
   if DataSet.Tag in [K_TAG_NIVEL10..K_TAG_NIVEL12] then
      iNivel := DataSet.Tag - K_TAG_NIVEL10 + 1 ;
   {$endif}

   if ( iNivel > 0 ) and ( iNivel = Global.GetGlobalInteger( K_GLOBAL_NIVEL_SUPERVISOR ))  then
   begin
        if DataSet.FieldByName('US_CODIGO').AsInteger = 0  then
        begin
                DB.DatabaseError( 'Usuario No Puede Quedar Vac�o' );
        end;
   end;
end;
{$endif}
end;

function TdmTablas.GetDataSetTransferencia( const lLevantaException: Boolean ): TZetaLookupDataset;
begin
     case Global.GetGlobalInteger( K_GLOBAL_NIVEL_COSTEO ) of
          1: Result := cdsNivel1;
          2: Result := cdsNivel2;
          3: Result := cdsNivel3;
          4: Result := cdsNivel4;
          5: Result := cdsNivel5;
          6: Result := cdsNivel6;
          7: Result := cdsNivel7;
          8: Result := cdsNivel8;
          9: Result := cdsNivel9;
          {$ifdef ACS}
          10: Result := cdsNivel10;
          11: Result := cdsNivel11;
          12: Result := cdsNivel12;
          {$endif}
     else
         Result := nil;
     end;

     if (Result = NIL) and lLevantaException then
        raise Exception.Create( K_SIN_CONFIG_COSTEO );
end;

procedure TdmTablas.cdsMunicipiosBeforePost(DataSet: TDataSet);
begin
     cdsTablaBeforePost(DataSet);
     if(StrVacio( DataSet.FieldByName('TB_ENTIDAD').AsString ))then
     begin
          DataSet.FieldByName('TB_ENTIDAD').FocusControl; 
          DataBaseError('Estado No puede Quedar Vac�o');
     end;
end;

function TdmTablas.HayDataSetTransferencia( var sMensaje: String ): Boolean;
begin
     Result := Assigned(dmTablas.GetDataSetTransferencia( False ) );
     if ( not Result ) then
          sMensaje := K_SIN_CONFIG_COSTEO;
end;

procedure TdmTablas.cdsNivel1FilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
      Accept := StrVacio( DataSet.FieldByName( 'TB_NIVEL0' ).AsString ) ;
end;

procedure TdmTablas.cdsNivelesTB_CODIGOValidate(Sender: TField);
begin
     if Pos (K_ESPACIO, Sender.AsString) > 0 then
        DataBaseError( 'C�digo No Puede Contener Espacios');
end;

procedure TdmTablas.cdsTNacCompBeforePost(DataSet: TDataSet);
begin
     ValidaMemoField( DataSet, 'TN_PROPOSI' );
     ValidaMemoField( DataSet, 'TN_ACT_NIV' );
     ValidaMemoField( DataSet, 'TN_PERFIL' );
     ValidaMemoField( DataSet, 'TN_CR_EVA' );
     ValidaMemoField( DataSet, 'TN_AC_COMP' );
     cdsTablaBeforePost(DataSet);
end;

procedure TdmTablas.ValidaMemoField( DataSet : TDataSet; const sField : string );
begin
     with DataSet do
     begin
          if( State = dsEdit ) then
          begin
               with FieldByName(sField) do
               begin
                    if StrVacio(AsString) then
                       AsString := K_ESPACIO;
               end;
          end;
     end;
end;


procedure TdmTablas.cdsTNacCompAlCrearCampos(Sender: TObject);
begin
     cdsTNacComp.MaskFecha('TN_FEC_APR'); //DevEx
end;

procedure TdmTablas.cdsNumericasBeforeDelete(DataSet: TDataSet);
begin
     with cdsArt80 do
     begin
          First;
          while not Eof do
          begin
               Delete;
          end;
     end;
end;

procedure TdmTablas.cdsEditNumericasBeforeDelete(DataSet: TDataSet);
begin
     with cdsArt80 do
     begin
          First;
          while not Eof do
          begin
               Delete;
          end;
     end;
end;

procedure TdmTablas.cdsTArt80BeforeDelete(DataSet: TDataSet);
begin
with cdsArt80 do
     begin
          First;
          while not Eof do
          begin
               Delete;
          end;
     end;
end;

end.
