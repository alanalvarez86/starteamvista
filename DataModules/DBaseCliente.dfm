inherited BaseCliente: TBaseCliente
  OldCreateOrder = True
  Left = 0
  Top = 0
  object cdsEmpleadoLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Empleados'
    LookupDescriptionField = 'PRETTYNAME'
    LookupKeyField = 'CB_CODIGO'
    OnLookupKey = cdsEmpleadoLookUpLookupKey
    OnLookupSearch = cdsEmpleadoLookUpLookupSearch
    Left = 144
    Top = 8
  end
  object cdsTPeriodos: TZetaLookupDataSet
    Tag = 33
    Aggregates = <>
    IndexFieldNames = 'TP_TIPO'
    Params = <>
    AlAdquirirDatos = cdsTPeriodosAlAdquirirDatos
    LookupName = 'Tipos de periodo'
    LookupDescriptionField = 'TP_DESCRIP'
    LookupKeyField = 'TP_TIPO'
    LookupConfidenField = 'TP_NIVEL0'
    Left = 137
    Top = 74
  end
end
