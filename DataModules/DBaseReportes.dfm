object dmBaseReportes: TdmBaseReportes
  OldCreateOrder = False
  Height = 479
  Width = 741
  object cdsCampoRepFiltros: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CR_TIPO;CR_POSICIO;CR_SUBPOS'
    Params = <>
    AlAdquirirDatos = cdsCampoRepFiltrosAlAdquirirDatos
    Left = 48
    Top = 168
  end
  object cdsEditReporte: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    BeforePost = cdsEditReporteBeforePost
    AfterCancel = cdsEditReporteAfterCancel
    OnNewRecord = cdsEditReporteNewRecord
    OnReconcileError = cdsReportesReconcileError
    AlAdquirirDatos = cdsEditReporteAlAdquirirDatos
    AlEnviarDatos = cdsEditReporteAlEnviarDatos
    AlCrearCampos = cdsEditReporteAlCrearCampos
    AlModificar = cdsEditReporteAlModificar
    Left = 136
    Top = 8
  end
  object cdsReportes: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'RE_CODIGOOIndex'
        Fields = 'RE_CODIGO'
      end
      item
        Name = 'RE_NOMBREIndex'
        Fields = 'RE_NOMBRE'
      end
      item
        Name = 'RE_TIPOIndex'
        Fields = 'RE_TIPO'
      end>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    StoreDefs = True
    OnCalcFields = cdsReportesCalcFields
    OnReconcileError = cdsReportesReconcileError
    AlAdquirirDatos = cdsReportesAlAdquirirDatos
    AlCrearCampos = cdsReportesAlCrearCampos
    AlAgregar = cdsReportesAlAgregar
    AlModificar = cdsReportesAlModificar
    Left = 48
    Top = 8
  end
  object cdsLookupReportes: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    AlAdquirirDatos = cdsLookupReportesAlAdquirirDatos
    AlAgregar = cdsLookupReportesAlAgregar
    AlModificar = cdsLookupReportesAlModificar
    LookupName = 'Reportes'
    LookupDescriptionField = 'RE_NOMBRE'
    LookupKeyField = 'RE_CODIGO'
    OnLookupSearch = cdsLookupReportesLookupSearch
    OnGetRights = cdsLookupReportesGetRights
    Left = 48
    Top = 56
  end
  object cdsEscogeReporte: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsEscogeReporteAlAdquirirDatos
    AlCrearCampos = cdsEscogeReporteAlCrearCampos
    AlModificar = cdsEscogeReporteAlModificar
    Left = 144
    Top = 168
  end
  object cdsResultados: TZetaClientDataSet
    Aggregates = <>
    AutoCalcFields = False
    Params = <>
    AfterDelete = cdsCampoRepFiltrosAfterDelete
    OnNewRecord = cdsCampoRepFiltrosNewRecord
    Left = 136
    Top = 112
  end
  object cdsCampoRep: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CR_TIPO;CR_POSICIO;CR_SUBPOS'
    Params = <>
    AfterPost = cdsCampoRepAfterPost
    AfterDelete = cdsCampoRepFiltrosAfterDelete
    OnNewRecord = cdsCampoRepFiltrosNewRecord
    AlCrearCampos = cdsCampoRepAlCrearCampos
    Left = 136
    Top = 56
  end
  object cdsImprimeForma: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsImprimeFormaAlAdquirirDatos
    AlCrearCampos = cdsImprimeFormaAlCrearCampos
    AlModificar = cdsImprimeFormaAlModificar
    Left = 48
    Top = 104
  end
  object cdsSuscripReportes: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'RE_CODIGOOIndex'
        Fields = 'RE_CODIGO'
      end
      item
        Name = 'RE_NOMBREIndex'
        Fields = 'RE_NOMBRE'
      end
      item
        Name = 'RE_TIPOIndex'
        Fields = 'RE_TIPO'
      end>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    StoreDefs = True
    OnReconcileError = cdsReportesReconcileError
    AlAdquirirDatos = cdsReportesAlAdquirirDatos
    AlCrearCampos = cdsReportesAlCrearCampos
    Left = 48
    Top = 224
  end
  object cdsPlantilla: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'RE_CODIGOOIndex'
        Fields = 'RE_CODIGO'
      end
      item
        Name = 'RE_NOMBREIndex'
        Fields = 'RE_NOMBRE'
      end
      item
        Name = 'RE_TIPOIndex'
        Fields = 'RE_TIPO'
      end>
    Params = <>
    StoreDefs = True
    Left = 440
    Top = 16
  end
  object cdsBlobs: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'RE_CODIGOOIndex'
        Fields = 'RE_CODIGO'
      end
      item
        Name = 'RE_NOMBREIndex'
        Fields = 'RE_NOMBRE'
      end
      item
        Name = 'RE_TIPOIndex'
        Fields = 'RE_TIPO'
      end>
    Params = <>
    StoreDefs = True
    Left = 448
    Top = 104
  end
  object cdsEditReporteTemporal: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    Left = 144
    Top = 224
  end
end
