object dmReportGenerator: TdmReportGenerator
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 462
  Width = 508
  object cdsSuscripcion: TZetaClientDataSet
    Aggregates = <>
    AutoCalcFields = False
    Params = <>
    Left = 32
    Top = 240
  end
  object cdsReporte: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 72
  end
  object cdsCampoRep: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 136
  end
  object cdsUsuarios: TZetaClientDataSet
    Aggregates = <>
    AutoCalcFields = False
    Params = <>
    Left = 32
    Top = 24
  end
  object cdsResultados: TZetaClientDataSet
    Aggregates = <>
    AutoCalcFields = False
    Params = <>
    Left = 32
    Top = 184
  end
  object cdsEmpleados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 296
  end
  object cdsPlantilla: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'RE_CODIGOOIndex'
        Fields = 'RE_CODIGO'
      end
      item
        Name = 'RE_NOMBREIndex'
        Fields = 'RE_NOMBRE'
      end
      item
        Name = 'RE_TIPOIndex'
        Fields = 'RE_TIPO'
      end>
    Params = <>
    StoreDefs = True
    Left = 440
    Top = 16
  end
end
