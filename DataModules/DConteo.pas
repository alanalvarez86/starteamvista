unit DConteo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet, ZetaTipoEntidad, ZetaCommonLists,
  {$ifndef VER130}Variants,{$endif}  
{$ifdef DOS_CAPAS}
  DServerConsultas;
{$else}
  Consultas_TLB;
{$endif}

type
  TRecConteo = record
    Descripcion : string;
    Tipo: TipoEntidad;
    LookUp : TZetaLookupDataSet;
  end;
  TdmConteo = class(TDataModule)
    cdsNivel0: TZetaClientDataSet;
    cdsNivel1: TZetaClientDataSet;
    cdsNivel2: TZetaClientDataSet;
    cdsNivel3: TZetaClientDataSet;
    cdsNivel4: TZetaClientDataSet;
    cdsNivel5: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsNivel0AlCrearCampos(Sender: TObject);
    procedure cdsNivel0CalcFields(DataSet: TDataSet);
    procedure cdsConteoAlAdquirirDatos(Sender: TObject);
    procedure cdsConteoAlModificar(Sender: TObject);
    procedure cdsConteoAlCrearCampos(Sender: TObject);
    procedure cdsConteoAlEnviarDatos(Sender: TObject);
    procedure cdsConteoNewRecord(DataSet: TDataSet);
    procedure cdsConteoBeforePost(DataSet: TDataSet);
    procedure cdsConteoBeforeEdit(DataSet: TDataSet);
  private
    { Private declarations }
    FDataSets : array[ 0..5 ] of TZetaClientDataSet;
    FNiveles, FOldCuantos : Integer;
//    FRefreshTotales : Boolean;
    FPropagarTotal : Boolean;
{$ifdef DOS_CAPAS}
    function GetServerConsultas: TdmServerConsultas;
{$else}
    FServidor: IdmServerConsultasDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
{$endif}
    function GetcdsConteo : TZetaClientDataSet;
    procedure DisableDatasets;
    procedure EnableDatasets;
    procedure Totaliza( cdsFuente, cdsDestino : TClientDataSet; const nNivel : Integer );
    procedure CierraDatasets;
    procedure CalculaTotales;
    procedure CalculaVacantes(DataSet:TDataSet; const lGrabar: Boolean = TRUE );
    procedure LimpiaEventos;
    procedure PreparaTotales;
    procedure CrearCampos(DataSet: TZetaClientDataSet; const iTag: integer; const FieldDes: string);
  public
    { Public declarations }
{$ifdef DOS_CAPAS}
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
{$else}
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
{$endif}
    property cdsConteo : TZetaClientDataSet read GetcdsConteo;
    property NumNiveles : integer read FNiveles;
//    property RefreshTotales : Boolean read FRefreshTotales write FRefreshTotales;
    function GetGlobalConteo: Boolean;
    function DataSetTotales( const nNivel : Integer ) : TZetaClientDataSet;
    function NumConteoNiveles: Integer;
    function GetRecordConteo(const eTipo: eCamposConteo): TRecConteo;
    function GetConteoDescrip(const eTipo: eCamposConteo): String;
    function GetConteoNiveles(const iNivel: integer): eCamposConteo;
    function GetConteoEntidad(const iNivel: integer): TipoEntidad;
    function GetDescripConteoNiveles(const iNivel: integer): string;
    function GetListaCodigos( const iNivel: Integer; var iCuantos: Integer ): String;
    procedure PreparaConteo;
    procedure QuitaFiltros;
    procedure RecalculaTotales;
  end;

var
   dmConteo: TdmConteo;

implementation

uses DCliente,
     DGlobal,
     ZGlobalTress,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
{$ifdef TRESS_DELPHIXE5_UP}
     ZBaseEdicion_DevEx,
     FEditConteo_DevEx,
     FGlobalConteo_DevEx,
{$else}
     ZBaseEdicion,
     FEditConteo,
     FGlobalConteo,
{$endif}
     FTressShell;

{$R *.DFM}

{ ************************** TdmConteo *************************** }

{$ifdef DOS_CAPAS}
function TdmConteo.GetServerConsultas: TdmServerConsultas;
begin
     Result := DCliente.dmCliente.ServerConsultas;
end;
{$else}
function TdmConteo.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServidor ) );
end;
{$endif}

procedure TdmConteo.DataModuleCreate(Sender: TObject);
begin
     FDataSets[ 0 ] := cdsNivel0;
     FDataSets[ 1 ] := cdsNivel1;
     FDataSets[ 2 ] := cdsNivel2;
     FDataSets[ 3 ] := cdsNivel3;
     FDataSets[ 4 ] := cdsNivel4;
     FDataSets[ 5 ] := cdsNivel5;
end;

{ Metodos sobre ClientDatasets }

procedure TdmConteo.EnableDatasets;
var
   i: integer;
begin
     for i:= 0 to K_MAX_CONTEO_NIVELES do
         FDataSets[i].EnableControls;
end;

procedure TdmConteo.DisableDatasets;
var
   i: integer;
begin
     for i:= 0 to K_MAX_CONTEO_NIVELES do
         FDataSets[i].DisableControls;
end;

procedure TdmConteo.QuitaFiltros;
var
   i: integer;
begin
     for i:= 0 to K_MAX_CONTEO_NIVELES do
         FDataSets[i].Filtered := FALSE;
end;

procedure TdmConteo.LimpiaEventos;
var
   i: integer;
begin
     for i:= 0 to K_MAX_CONTEO_NIVELES do
     begin
          with FDataSets[i] do
          begin
               AlAdQuirirDatos := NIL;
               AlModificar := NIL;
               AlCrearCampos := NIL;
               AlEnviarDatos := NIL;
               BeforePost := NIL;
               BeforeEdit := NIL;
          end;
     end;
end;

procedure TdmConteo.CierraDatasets;
var
   i: integer;
begin
     for i:= 0 to K_MAX_CONTEO_NIVELES do
         with FDataSets[i] do
              if Active then
              begin
                   EmptyDataset;
                   Data := NULL;
              end;
end;

procedure TdmConteo.CrearCampos( DataSet: TZetaClientDataSet; const iTag : integer;
          const FieldDes : string);
var
   oRecConteo : TRecConteo;
begin
     with DataSet do
     begin
         if ( iTag > 0 ) then              // cdsNivel0 no tiene Lookup
         begin
              oRecConteo := GetRecordConteo(eCamposConteo(Global.GetGlobalInteger(K_GLOBAL_CONTEO_NIVEL1+iTag-1)));
              if oRecConteo.LookUp <> NIL then
              begin
                   oRecConteo.LookUp.Conectar;
                   CreateSimpleLookup( oRecConteo.LookUp, FieldDes, 'CT_NIVEL_'+ IntToStr(iTag) );
                   FieldByName(FieldDes).DisplayWidth := 50;
              end;
         end;
     end;
end;

procedure TdmConteo.PreparaConteo;
var
   i: integer;
begin
     FNiveles := NumConteoNiveles;
     LimpiaEventos;

     with cdsConteo do
     begin
          AlAdQuirirDatos := cdsConteoAlAdquirirDatos;
          AlModificar := cdsConteoAlModificar;
          AlEnviarDatos := cdsConteoAlEnviarDatos;
          AlCrearCampos := cdsConteoAlCrearCampos;
          OnNewRecord := cdsConteoNewRecord;
          BeforePost := cdsConteoBeforePost;
          BeforeEdit := cdsConteoBeforeEdit;
          IndexFieldNames := 'CT_NIVEL_1;CT_NIVEL_2;CT_NIVEL_3;CT_NIVEL_4;CT_NIVEL_5';
     end;
     for i:= 0 to FNiveles-1 do
     begin
          with FDataSets[i] do
          begin
               AlCrearCampos := cdsNivel0AlCrearCampos;
         end;
     end;
end;

{ Funciones sobre ClientDataSets }

function TdmConteo.DataSetTotales(const nNivel: Integer): TZetaClientDataSet;
begin
     Result := FDataSets[ nNivel ];
end;

function TdmConteo.NumConteoNiveles: Integer;
begin
     Result := 0;
     // Cuenta los niveles NO vacios. En cuanto encuentra uno vac�o, deja de contar
     while ( Result < K_MAX_CONTEO_NIVELES ) and
           ( GetConteoNiveles(Result) <> coNinguno ) do
     begin
          Inc( Result );
     end;
end;

function TdmConteo.GetRecordConteo( const eTipo : eCamposConteo  ) : TRecConteo;
begin
     with Result do
     begin
          Descripcion := GetConteoDescrip(eTipo);
          Tipo := ZetaTipoEntidad.GetConteoEntidad(eTipo);
          Lookup := TressShell.GetLookUpDataSet(Tipo);
     end;
end;

function TdmConteo.GetConteoEntidad(const iNivel: integer): TipoEntidad;
begin
     Result := ZetaTipoEntidad.GetConteoEntidad( GetConteoNiveles(iNivel) );
end;

function TdmConteo.GetConteoNiveles(const iNivel: integer): eCamposConteo;
begin
     Result := eCamposConteo(Global.GetGlobalInteger(K_GLOBAL_CONTEO_NIVEL1+iNivel));
end;

function TdmConteo.GetDescripConteoNiveles(const iNivel: integer): string;
begin
     Result := GetConteoDescrip(GetConteoNiveles(iNivel));
end;

function TdmConteo.GetConteoDescrip(const eTipo: eCamposConteo):String;
begin
     case eTipo of
          coPuesto : Result := 'Puesto';
          coTurno : Result := 'Turno';
          coClasifi : Result := 'Clasificaci�n';
          coConfidencial : Result := 'Confidencialidad';
          coNivel1..coNivel9 : Result := Global.NombreNivel( Ord(eTipo) - Ord(coNivel1) +1 )
          else Result := '';
     end;
end;

function TdmConteo.GetListaCodigos( const iNivel: Integer; var iCuantos: Integer ): String;
var
   Pos : TBookMark;
begin
     Result := VACIO;
     iCuantos := 0;
     with DataSetTotales(iNivel) do
     begin
          DisableControls;
          try
             Pos:= GetBookMark;
             First;
             while not EOF do
             begin
                  Result := ConcatString( Result, EntreComillas( Fields[iNivel-1].AsString ), ',' );
                  iCuantos := iCuantos + 1;
                  Next;
             end;
             if ( Pos <> nil ) then
             begin
                  GotoBookMark( Pos );
                  FreeBookMark( Pos );
             end;
          finally
             EnableControls;
          end;
     end;
end;

{ Recalculo de Totales }

procedure TdmConteo.RecalculaTotales;
begin
     DisableDatasets;
     QuitaFiltros;
     PreparaTotales;
     EnableDatasets;
end;

procedure TdmConteo.PreparaTotales;
var
   i : Integer;
begin
     // Copia estructura Aqu�, realmente van los CreateFields y CreateLookups
     FPropagarTotal := FALSE;                      // No se debe disparar el Post
     try
        cdsNivel0.Data := cdsConteo.Data;
        cdsNivel0.EmptyDataSet;

        for i := 1 to FNiveles -1 do
            FDataSets[ i ].Data := cdsNivel0.Data;

        CalculaTotales;
     finally
        FPropagarTotal := TRUE;
     end;
end;

procedure TdmConteo.CalculaTotales;
var
   oFuente, oDestino : TClientDataset;
   i : Integer;
   lChangeCount : Boolean;
   sOldIndexFuente, sOldIndexDestino: String;

   procedure SetIndicesRequeridos;
   begin
        with oFuente do
        begin
             sOldIndexFuente := IndexFieldNames;
             IndexFieldNames := 'CT_NIVEL_1;CT_NIVEL_2;CT_NIVEL_3;CT_NIVEL_4;CT_NIVEL_5';
        end;
        with oDestino do
        begin
             sOldIndexDestino := IndexFieldNames;
             IndexFieldNames := 'CT_NIVEL_1;CT_NIVEL_2;CT_NIVEL_3;CT_NIVEL_4;CT_NIVEL_5';
        end;
   end;

   procedure RestoreIndicesAnteriores;
   begin
        oFuente.IndexFieldNames := sOldIndexFuente;
        oDestino.IndexFieldNames := sOldIndexDestino;
   end;

begin
     oFuente := cdsConteo;

     lChangeCount := (oFuente.ChangeCount > 0);

     for i := FNiveles-1 downto 0 do
     begin
          oDestino := FDataSets[ i ];
          oDestino.Filtered := FALSE;     // Limpia sesi�n anterior
          SetIndicesRequeridos;
          try
             Totaliza( oFuente, oDestino, i );
          finally
             RestoreIndicesAnteriores;
          end;
          oFuente := oDestino;
     end;

     if NOT lChangeCount then cdsConteo.MergeChangeLog; //Para que no me reporte el Datasource que cambio el dataset, y espere que se grabe
end;

procedure TdmConteo.Totaliza(cdsFuente, cdsDestino: TClientDataSet; const nNivel: Integer);

   function GetLlave : String;
   var
      i : Integer;
   begin
        // Supone que Fields[0] = 'CT_NIVEL_1', Fields[1] = 'CT_NIVEL_2', etc.
        Result := '';
        for i := 0 to nNivel-1 do
            Result := Result + ',' + cdsFuente.Fields[ i ].AsString;
   end;

   procedure InsertaTotal;
   var
      i : Integer;
   begin
        with cdsDestino do
        begin
            // Agrega un registro y copia CODIGOS
            Append;
            for i := 0 to nNivel-1 do
                Fields[ i ].AsString := cdsFuente.Fields[ i ].AsString;
        end;
   end;

   procedure SumaTotal;
   begin
        with cdsDestino do
        begin
             FieldByName( 'CT_REAL' ).AsInteger := FieldByName( 'CT_REAL' ).AsInteger + cdsFuente.FieldByName( 'CT_REAL' ).AsInteger;
             FieldByName( 'CT_CUANTOS' ).AsInteger := FieldByName( 'CT_CUANTOS' ).AsInteger + cdsFuente.FieldByName( 'CT_CUANTOS' ).AsInteger;
             FieldByName( 'CT_VACANTES' ).AsInteger := FieldByName( 'CT_VACANTES' ).AsInteger + cdsFuente.FieldByName( 'CT_VACANTES' ).AsInteger;
             FieldByName( 'CT_EXCEDENTE' ).AsInteger := FieldByName( 'CT_EXCEDENTE' ).AsInteger + cdsFuente.FieldByName( 'CT_EXCEDENTE' ).AsInteger;
        end;
   end;

var
   sActual, sNueva : String;
begin   // Totaliza
     cdsDestino.EmptyDataSet;
     sActual := '*';     // Para que se cumpla primer registro
     with cdsFuente do
     begin
          First;
          while not Eof do
          begin
               if nNivel = FNiveles-1 then CalculaVacantes(cdsFuente);
               sNueva := GetLlave;
               if ( sActual <> sNueva ) then
               begin
                    InsertaTotal;
                    sActual := sNueva;
               end;
               SumaTotal;
               Next;
          end;
     end;
     if cdsDestino.State <> dsBrowse then
        cdsDestino.Post;
end;

procedure TdmConteo.CalculaVacantes(DataSet:TDataSet; const lGrabar: Boolean = TRUE );
var
   iExcedente,iVacantes: integer;
begin
     with DataSet do
     begin
          iVacantes := FieldByName( 'CT_CUANTOS' ).AsInteger - FieldByName( 'CT_REAL' ).AsInteger;
          iExcedente := iMin(iVacantes,0)*-1;
          iVacantes := iMax(iVacantes,0);

          if (FieldByName( 'CT_VACANTES' ).AsInteger <> iVacantes) OR
             (FieldByName( 'CT_EXCEDENTE' ).AsInteger <> iExcedente) then
          begin
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
               FieldByName( 'CT_VACANTES' ).AsInteger := iVacantes;
               FieldByName( 'CT_EXCEDENTE' ).AsInteger := iExcedente;
               if lGrabar then
                  Post;
          end;
     end;
end;

{ cdsConteo }

function TdmConteo.GetcdsConteo : TZetaClientDataSet;
begin
     Result := DataSetTotales(FNiveles);
end;

procedure TdmConteo.cdsConteoAlAdquirirDatos(Sender: TObject);
begin
     DisableDatasets;
     CierraDatasets;
     PreparaConteo;
     QuitaFiltros;
     cdsConteo.Data := ServerConsultas.GetConteo( dmCliente.Empresa );
     PreparaTotales;
     EnableDatasets;
end;

procedure TdmConteo.cdsConteoAlCrearCampos(Sender: TObject);
var
   i: integer;
begin
     for i:=1 to FNiveles  do
         CrearCampos(Sender as TZetaClientDataSet, i, 'TB_ELEMENT' + IntToStr(i) );
     with Sender as TZetaClientDataSet do
     begin
          CreateCalculated( 'VACANTES', ftInteger, 0 );
          CreateCalculated( 'EXCEDENTE', ftInteger, 0 );
          CreateCalculated( 'CT_DIFERENCIA', ftInteger, 0 );
          CreateCalculated( 'CT_TASA', ftInteger, 0 );
          CreateCalculated( 'TB_ELEMENT', ftSTRING, 20 );
          MaskNumerico( 'CT_VACANTES', '#,0;-#,0;#' );
          MaskNumerico( 'CT_EXCEDENTE', '#,0;-#,0;#' );
          MaskNumerico( 'VACANTES', '#,0;-#,0;#' );
          MaskNumerico( 'EXCEDENTE', '#,0;-#,0;#' );
          MaskNumerico( 'CT_REAL', '#,0' );
          MaskNumerico( 'CT_CUANTOS', '#,0' );
          MaskNumerico( 'CT_TASA', '#,0 %' );
     end;
end;

procedure TdmConteo.cdsConteoNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'CT_VACANTES').AsInteger := 0;
          FieldByName( 'CT_EXCEDENTE').AsInteger := 0;
     end;
end;

procedure TdmConteo.cdsConteoAlModificar(Sender: TObject);
begin
{$ifdef TRESS_DELPHIXE5_UP}
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditConteo_DevEx, TEditConteo_DevEx ); //(@am): Se necesita instalar TeeChart
{$else}
     ZBaseEdicion.ShowFormaEdicion( EditConteo, TEditConteo );
{$endif}
end;

procedure TdmConteo.cdsConteoBeforeEdit(DataSet: TDataSet);
begin
     FOldCuantos := DataSet.FieldByName( 'CT_CUANTOS' ).AsInteger;    // Guarda valor anterior
end;

procedure TdmConteo.cdsConteoBeforePost(DataSet: TDataSet);
var
   i, iDiferencia: Integer;
begin
     with DataSet.FieldByName( 'CT_CUANTOS' ) do
          if ( AsInteger < 0 ) then
          begin
               FocusControl;
               DataBaseError( 'Presupuesto debe Ser Igual o Mayor a Cero' );
          end
          else if ( DataSet.State = dsEdit ) and FPropagarTotal and ( AsInteger <> FOldCuantos ) then  // Grabar, Calcular Vacantes y Propagar Cambio
          begin
               CalculaVacantes( DataSet, FALSE );
               iDiferencia := AsInteger - FOldCuantos;
               for i:= FNiveles-1 downto 0 do
               begin
                    with FDataSets[i] do
                    begin
                         if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                            Edit;
                         FieldByName( 'CT_CUANTOS' ).AsInteger := FieldByName( 'CT_CUANTOS' ).AsInteger + iDiferencia;
                    end;
                    CalculaVacantes( FDataSets[i] );   // Aqu� se hace el Post
               end;
          end;
end;

procedure TdmConteo.cdsConteoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   NewConteo : OleVariant;
begin
     ErrorCount := 0;
     with cdsConteo do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               DisableDatasets;
               PreparaConteo;
               QuitaFiltros;
               Reconcile(ServerConsultas.GrabaConteo( dmCliente.Empresa, Delta, ErrorCount, NewConteo ));
               Data := NewConteo;
               PreparaTotales;
               EnableDatasets;
          end;
     end;
end;

{ cdsNivel0 }

procedure TdmConteo.cdsNivel0AlCrearCampos(Sender: TObject);
begin
     CrearCampos(Sender as TZetaClientDataSet, TZetaClientDataSet(Sender).Tag, 'TB_ELEMENT');
     with TZetaClientDataSet( Sender ) do
     begin
          CreateCalculated( 'CT_DIFERENCIA', ftInteger, 0 );
          CreateCalculated( 'CT_TASA', ftInteger, 0 );
          MaskNumerico( 'CT_TASA', '#,0 %' );
          MaskNumerico( 'CT_VACANTES', '#,0;-#,0;#' );
          MaskNumerico( 'CT_EXCEDENTE', '#,0;-#,0;#' );
     end;
end;

procedure TdmConteo.cdsNivel0CalcFields(DataSet: TDataSet);
var
   iVacantes, nPresupuesto, nReal, nDiferencia : Integer;
begin
     with DataSet do
     begin
         nPresupuesto := FieldByName( 'CT_CUANTOS' ).AsInteger;
         nReal        := FieldByName( 'CT_REAL' ).AsInteger;
         nDiferencia  := nReal - nPresupuesto;

         FieldByName( 'CT_DIFERENCIA' ).AsInteger := nDiferencia;

         if ( nPresupuesto > 0 ) then
            FieldByName( 'CT_TASA' ).AsFloat := Trunc(nReal / nPresupuesto * 100.0)
         else
             FieldByName( 'CT_TASA' ).AsFloat := 0;

         if Tag = FNiveles then
         begin
              FieldByName('TB_ELEMENT').AsString := FieldByName('TB_ELEMENT'+IntToStr(FNiveles)).AsString;

              iVacantes := FieldByName( 'CT_CUANTOS' ).AsInteger - FieldByName( 'CT_REAL' ).AsInteger;
              FieldByName( 'VACANTES' ).AsInteger := iMax(iVacantes,0);
              FieldByName( 'EXCEDENTE' ).AsInteger := iMin(iVacantes,0)*-1;
         end;

         if Tag <> 0 then
            FieldByName('TB_ELEMENT').AsString := PropperCase(FieldByName('TB_ELEMENT').AsString);
     end;
end;

{ Configurar Globales }
function TdmConteo.GetGlobalConteo: Boolean;
begin
     Result := FALSE;
     if ZAccesosMgr.CheckDerecho( D_CONTEO_CONFIGURACION, K_DERECHO_CONSULTA) then
     begin
          {$ifdef TRESS_DELPHIXE5_UP}
          GlobalConteo_DevEx := TGlobalConteo_DevEx.Create( Application );
          with GlobalConteo_DevEx do
          {$else}
          GlobalConteo := TGlobalConteo.Create( Application );
          with GlobalConteo do
          {$endif}
          begin
               try
                  ShowModal;
                  Result := LastAction = K_EDICION_MODIFICACION ;
               finally
                      Free;
               end;
          end;
     end
     else
         ZetaDialogo.zInformation( 'Presupuesto de Personal', 'No Tiene Permiso Para Configurar Presupuesto de Personal', 0 );
end;

end.
