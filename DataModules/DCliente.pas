unit DCliente;

interface

{$DEFINE MULTIPLES_ENTIDADES}
{$DEFINE REPORTING}                                                                                                                         
{$define VALIDAEMPLEADOSGLOBAL}
{$define CONFIDENCIALIDAD_MULTIPLE}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, dxSkinsForm,
     DBaseCliente,
{$ifdef DOS_CAPAS}
   {$ifdef MULTIPLES_ENTIDADES}
      DServerPresupuestos,
   {$endif}
   {$IFDEF REPORTING}
      DServerReporting,
   {$endif}
     DServerCafeteria,
     DServerCatalogos,
     DServerConsultas,
     DServerGlobal,
     DServerIMSS,
     DServerSistema,
     DServerTablas,
     DServerAsistencia,
     DServerNomina,
     DServerReportes,
     DServerCalcNomina,
     DServerAnualNomina,
     DServerRecursos,
     DServerLabor,
     DServerSuper,
     DServerLogin,
{$endif}
{$ifndef VER130}
     Variants,
{$endif}
{$ifdef INTERFAZ_TRESS}
     {$ifndef POLIZA_SENDA}
     {$ifndef CAMBIO}
     DInterfase,
     {$endif}
     {$endif}
{$endif}
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet,
     ZetaWinAPITools;

{$Define QUINCENALES}
{.$undefine QUINCENALES}

type
  TdmCliente = class(TBaseCliente)
    cdsPatron: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsPeriodo: TZetaClientDataSet;
    procedure cdsEmpleadoAfterDelete(DataSet: TDataSet);
    procedure cdsEmpleadoAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsEmpleadoAlAgregar(Sender: TObject);
    procedure cdsEmpleadoAlEnviarDatos(Sender: TObject);
    procedure cdsEmpleadoAlBorrar(Sender: TObject);
    procedure cdsEmpleadoBeforePost(DataSet: TDataSet);
    procedure cdsEmpleadoNewRecord(DataSet: TDataSet);
    {$ifdef VER130}
    procedure cdsEmpleadoReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure CB_SEGSOCValidate(Sender: TField);
    procedure CB_E_MAILValidate(Sender: TField);    
    procedure CB_SEGSOCChange(Sender: TField);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ContratoChange( Sender: TField ); {OP: 12.May.98 P�blico por que tambi�n lo accesa dRecursos}
    procedure CB_ESTADOChange( Sender: TField );
    procedure CB_MUNICIPChange( Sender: TField );
    procedure cdsEmpleadoCB_NOMINAChange(Sender: TField);
  private
    { Private declarations }
    FEmpleado: TNumEmp;
    FEmpleadoBOF: Boolean;
    FEmpleadoEOF: Boolean;
    FFechaAsistencia: TDate;
    FPeriodoTipo: eTipoPeriodo;
    FPeriodoNumero: Integer;
    FPeriodoClasificacion: eClasifiPeriodo;
    FPeriodoBOF: Boolean;
    FPeriodoEOF: Boolean;
    FIMSSPatron: String;
    FIMSSYear: Integer;
    FIMSSTipo: eTipoLiqIMSS;
    FIMSSMes: Integer;
    FParamsCliente : TZetaParams;
    FCambiarDigito: Boolean;
    FEmpresaSeleccion: Variant;
    FRazonSocial : string;
    {*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
    FRazonSocialAcumulado : string;
    {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
    FSistemaYearAux: Integer;
    FPeriodoActivoNumeroAux: Integer;
    {*** FIN ***}
    FSesionIDDashlet: Integer;
    function FetchEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchNextEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchNextPeriod(const iNumero: Integer): Boolean;
    function FetchPeriod(const iNumero: Integer): Boolean;
    function FetchPreviousEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchPreviousPeriod(const iNumero: Integer): Boolean;
    function GetFechaAsistencia: TDate;
    function GetMatsushita: Boolean;
    function GetEmpresaSeleccion: Variant;
    procedure InicializaPeriodo;
    procedure Nombre_NacChange(Sender: TField);
    procedure DatosCURPChange(Sender: TField);
    //procedure ValidaCreditoInfo;
    procedure ValidaPaterno;
    procedure AntiguedadChange(Sender: TField);
    procedure SetFechaAsistencia(const Value: TDate);
    procedure SetIMSSMes(const Value: Integer);
    procedure SetIMSSPatron(const Value: String);
    procedure SetIMSSTipo(const Value: eTipoLiqIMSS);
    procedure SetIMSSYear(const Value: Integer);
    procedure SetPeriodoTipo(const Value: eTipoPeriodo);
    procedure ClearInfonavit(DataSet: TDataSet);
    procedure SetEmpresaSeleccion( const sCodigo: String );
    procedure ColoniaChange(Sender: TField);
    procedure ValidarAdicionales;
    procedure SetRazonSocial(const Value: String);
    function ValidaSimFiniquito( const oDatosPeriodo: OleVariant ): Boolean;{OP: 10/06/08}
    procedure CB_INF_ANT_Change( Sender: TField );
    function GetComputerName: String;
    function GetSesionIDDashlet: Integer;
    procedure SetSesionIDDashlet( const Value: Integer );
  protected
    { Protected declarations }
{$ifdef DOS_CAPAS}
    {$ifdef MULTIPLES_ENTIDADES}
    FServerPresupuestos: TdmServerPresupuestos;
    {$endif}
    {$ifdef REPORTING}
    FServerReporteador: TdmServerReporting;
    {$endif}
    FServerCafeteria: TdmServerCafeteria;
    FServerIMSS: TdmServerIMSS;
    FServerAsistencia: TdmServerAsistencia;
    FServerLabor: TdmServerLabor;
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerGlobal: TdmServerGlobal;
    FServerSistema: TdmServerSistema;
    FServerTablas: TdmServerTablas;
    FServerNomina: TdmServerNomina;
    FServerReportes : TdmServerReportes;
    FServerCalcNomina: TdmServerCalcNomina;
    FServerAnualNomina: TdmServerAnualNomina;
    FServerRecursos: TdmServerRecursos;
    FServerSuper: TdmServerSuper;
    FServerLogin: TdmServerLogin;

{$endif}
    procedure SetYearDefault(const Value: Integer); override;
    procedure InitCacheModules; override;
    function PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                        const iEmpleado: integer;
                                        const eTipo: eStatusPeriodo;
                                        const lPuedeModificar: Boolean;
                                        var sMensaje: string): Boolean;override;
    {***DevEx(@am): Metodos virtuales para poder compilar unidades utilizadas en varios proyectos ***}
    {$ifdef DEVEX}
    function GetSkinController: TdxSkinController; override;
    {$endif}
    {***}
  public
    { Public declarations }
    property Empleado: TNumEmp read FEmpleado;
    property FechaAsistencia: TDate read GetFechaAsistencia write SetFechaAsistencia;
    property PeriodoTipo: eTipoPeriodo read FPeriodoTipo write SetPeriodoTipo;
    property PeriodoNumero: Integer read FPeriodoNumero;
    property PeriodoClasificacion: eClasifiPeriodo read FPeriodoClasificacion write FPeriodoClasificacion;
    property IMSSPatron: String read FIMSSPatron write SetIMSSPatron;
    property IMSSYear: Integer read FIMSSYear write SetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read FIMSSTipo write SetIMSSTipo;
    property IMSSMes: Integer read FIMSSMes write SetIMSSMes;
    property Matsushita: Boolean read GetMatsushita;
    property EmpresaSeleccion: Variant read GetEmpresaSeleccion;
    property RazonSocial : String read FRazonSocial write SetRazonSocial;
    property SesionIDDashlet: Integer read GetSesionIDDashlet write SetSesionIDDashlet;
{$ifdef DOS_CAPAS}
      {$ifdef MULTIPLES_ENTIDADES}
      property ServerPresupuestos: TdmServerPresupuestos read FServerPresupuestos;
      {$endif}
      {$ifdef REPORTING}
      property ServerReporteador: TdmServerReporting read FServerReporteador;
      {$endif}
    property ServerCafeteria: TdmServerCafeteria read FServerCafeteria;
    property ServerIMSS: TdmServerIMSS read FServerIMSS;
    property ServerAsistencia: TdmServerAsistencia read FServerAsistencia;
    property ServerLabor: TdmServerLabor read FServerLabor;
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerSistema: TdmServerSistema read FServerSistema;
    property ServerTablas: TdmServerTablas read FServerTablas;
    property ServerNomina: TdmServerNomina read FServerNomina;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerCalcNomina: TdmServerCalcNomina read FServerCalcNomina;
    property ServerAnualNomina: TdmServerAnualNomina read FServerAnualNomina;
    property ServerSuper: TdmServerSuper read FServerSuper;
    property ServerLogin: TdmServerLogin read FServerLogin;
    property ServerRecursos: TdmServerRecursos read FServerRecursos;

{$endif}
    {*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
    property RazonSocialAcumulado: string read FRazonSocialAcumulado write FRazonSocialAcumulado;
    property ComputerName: String read GetComputerName;
    {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
    property SistemaYearAux: Integer read FSistemaYearAux write FSistemaYearAux;
    property PeriodoActivoNumeroAux: Integer read FPeriodoActivoNumeroAux write FPeriodoActivoNumeroAux;
    {*** FIN ***}
    function EmpleadoDownEnabled: Boolean;
    function EmpleadoUpEnabled: Boolean;
    function GetAsistenciaDescripcion: String;
    function GetEmpleadoAnterior: Boolean;
    function GetEmpleadoInicial: Boolean;
    function GetEmpleadoPrimero: Boolean;
    function GetEmpleadoSiguiente: Boolean;
    function GetEmpleadoUltimo: Boolean;
    function GetEmpleadoDescripcion: String;
    function GetDatosEmpleadoActivo: TInfoEmpleado;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetIMSSDescripcion: String;
    function GetRazonSocialDescripcion : String;
    function GetPeriodoAnterior: Boolean;
    function GetPeriodoInicial: Boolean;
    function GetPeriodoPrimero: Boolean;
    function GetPeriodoSiguiente: Boolean;
    function GetPeriodoUltimo: Boolean;
    function GetPeriodoCampoDescripcion: String;
    function GetPeriodoCampoEmpleados: Integer;
    function GetPeriodoDescripcion: String;
    function GetSistemaDescripcion: String;
    function GetValorActivoStr(const eTipo: TipoEstado): String; override;
    function PeriodoDownEnabled: Boolean;
    function PeriodoUpEnabled: Boolean;
    function SetEmpleadoNumero(const Value: TNumEmp): Boolean;
    function SetPeriodoNumero(const Value: Integer): Boolean;
    function GetFiltroLookupPuestos: String;
    //function EsPuestoValido( const sPuesto: String; var sMensaje: String ): Boolean;
    function EsPuestoValido( const sPuesto: String ): Boolean;
    function NominaYaAfectada: Boolean;
    function ValidaSEGSOC( const sNumero: String; var sMensaje: String; const lConfirmar: Boolean = TRUE ): Boolean;
    {$ifdef BIOMONITOR}
    function GetEmpresas: OleVariant;
    {$endif}
    function  GetPeriodoInicialTipoNomina(const TipoNomina: eTipoPeriodo;const bReiniciar:Boolean = False ): Integer;
    procedure CargaActivosIMSS(Parametros: TZetaParams);
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema(Parametros: TZetaParams);
    procedure CargaActivosTodos(Parametros: TZetaParams); override;
    procedure CierraEmpresa; override;
    procedure IngresoChange(Sender: TField);
    procedure InitActivosAsistencia;
    procedure InitActivosEmpleado;
    procedure InitActivosIMSS;
    procedure InitActivosPeriodo;
    procedure LeePatrones(Lista: TStrings);
    { Se paso a DBaseCliente
    procedure LeeTiposNomina(Lista: TStrings);}
    procedure PosicionaSiguienteEmpleado;
    procedure RefrescaPeriodo;
    procedure RefrescaEmpleado;
    function  ParametrosNomina( const iEmpleado : TNumEmp) : OleVariant;
    procedure ActualizaClaveUsuario( const sAnterior, sClave: String ); override;
    procedure SetSeguridad( const Valores: OleVariant ); override;
    procedure ValidaCreditoInfo(DataSet: TDataSet);
    procedure InicializaInfonavit(DataSet: TDataSet);
    procedure SetDatosPuestoMultiples;
    procedure SetDatosPuesto( const sPuesto: String );
    procedure SetDatosPlaza(const IPlaza: integer);
    procedure GrabaBloqueoSistema( const lBlockSistema: Boolean);
    procedure InitNavegadorEmpleado;
    {*** US 9304: Valida el RFC y El CURP capturado por el usuario ***}
    procedure ValidaRFCYCURP;
    {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
    function CambioValoresActivosGraficas: Boolean;
    procedure RespaldaValoresActivosGraficas;
    procedure AnaliticaContabilizarUso(const sModulo: String; const HelpContext, iConteo: Integer);
  end;

var
  dmCliente: TdmCliente;

implementation

uses DRecursos,
     DGlobal,
     DTablas,
     DCatalogos,
     DDiccionario,
     DAsistencia,
     DSistema,
     ZAccesosTress,
     ZGlobalTress,
     ZReconcile,
     ZetaClientTools,
     ZetaMsgDlg,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaDataSetTools,
     ZAccesosMgr,
     ZReportTools,
     FEmpAltaOtros_DevEx,
     FTressShell,
     ZetaRegistryCliente, 
     DBasicoCliente;
{$R *.DFM}

const
     K_PERIODO_NUMERO_TOPE = 999;

function SetFileNamePath( const sPath, sFile: String ): String;
begin
     Result := sPath;
     if ( Result[ Length( Result ) ] <> '\' ) then
        Result := Result + '\';
     Result := Result + sFile;
end;

{ ********* TdmCliente *********** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
{$ifdef PRESUPUESTOS}
     TipoCompany := tcPresupuesto;
{$endif}
     FParamsCliente := TZetaParams.Create;
{$ifdef DOS_CAPAS}
     {$ifdef MULTIPLES_ENTIDADES}
     FServerPresupuestos := TdmServerPresupuestos.Create( self );
     {$endif}
     {$ifdef REPORTING}
     FServerReporteador := TdmServerReporting.Create( self );
     {$endif}

     FServerCafeteria := TdmServerCafeteria.Create( Self );
     FServerIMSS := TdmServerIMSS.Create( Self );
     FServerAsistencia := TdmServerAsistencia.Create( Self );
     FServerLabor := TdmServerLabor.Create( Self );
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerIMSS := TdmServerIMSS.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
     FServerTablas := TdmServerTablas.Create( Self );
     FServerAsistencia := TdmServerAsistencia.Create( Self );
     FServerNomina := TdmServerNomina.Create( self );
     FServerReportes := TdmServerReportes.Create( self );
     FServerCalcNomina := TdmServerCalcNomina.Create( self );
     FServerAnualNomina := TdmServerAnualNomina.Create( self );
     FServerRecursos := TdmServerRecursos.Create( self );
     FServerLabor := TdmServerLabor.Create( Self );
     FServerSuper := TdmServerSuper.Create( Self );
     FServerLogin := TdmServerLogin.Create( Self );
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     {$ifdef MULTIPLES_ENTIDADES}
     FreeAndNil( FServerPresupuestos );
     {$endif}
     {$ifdef REPORTING}
     FreeAndNil( FServerReporteador );
     {$endif}
     FreeAndNil( FServerLabor );
     FreeAndNil( FServerAsistencia );
     FreeAndNil( FServerIMSS );
     FreeAndNil( FServerCafeteria );
     FreeAndNil( FServerRecursos );
     FreeAndNil( FServerCalcNomina );
     FreeAndNil( FServerAnualNomina );
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerNomina );
     FreeAndNil( FServerAsistencia );
     FreeAndNil( FServerTablas );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerIMSS );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerCafeteria );
     FreeAndNil( FServerSuper);
     FreeAndNil( FServerLogin );
     FreeAndNil( FDashleetsConsulta );
{$endif}
     FParamsCliente.Free;
     inherited;
end;

procedure TdmCliente.InitCacheModules;
begin
     inherited;
     DataCache.AddObject( 'Tabla', dmTablas );
     DataCache.AddObject( 'Cat�logo', dmCatalogos );
     DataCache.AddObject( 'Diccionario', dmDiccionario );
end;

procedure TdmCliente.InitActivosAsistencia;
begin
     FFechaAsistencia := Now;
end;

procedure TdmCliente.InitActivosEmpleado;
begin
     FEmpleadoBOF := False;
     FEmpleadoEOF := False;
     FEmpleado := 0;
     GetEmpleadoInicial;
end;

procedure TdmCliente.InitActivosPeriodo;
procedure ValidaPeriodo;
var iPeriodo : integer;
begin
     try
        iPeriodo := StrToInt(FListaTiposPeriodoConfidencialidad.Values[IntToStr(ClientRegistry.TipoNomina)]);
     except
           on Error : Exception do
                iPeriodo := K_PERIODO_VACIO;
     end;


     if FArregloPeriodoConfidencial.Count = 0 then
        FPeriodoTipo := K_PERIODO_VACIO
     else
         if (ClientRegistry.TipoNomina = K_PERIODO_VACIO) or ( iPeriodo = K_PERIODO_VACIO ) then
            FPeriodoTipo := eTipoPeriodo(K_PERIODO_INICIAL)
         else
             FPeriodoTipo := eTipoPeriodo(ClientRegistry.TipoNomina);
end;
begin
     ValidaPeriodo;
     InicializaPeriodo;
end;

procedure TdmCliente.InicializaPeriodo;
begin
     if FPeriodoTipo > K_PERIODO_VACIO then
        FPeriodoClasificacion := GetClasificacionPeriodo( FListaTiposPeriodoConfidencialidad ,Ord(FPeriodoTipo)); //@(am): Obtiene la clasificacion para el TP_TIPO seleccionado.

     if GetPeriodoInicial then
     begin
          FPeriodoNumero := cdsPeriodo.FieldByName( 'PE_NUMERO' ).AsInteger;
          FPeriodoBOF := False;
          FPeriodoEOF := False;
     end
     else
     begin
          FPeriodoNumero := 0;
          FPeriodoBOF := True;
          FPeriodoEOF := True;
     end;
end;

procedure TdmCliente.InitActivosIMSS;
begin
     FimssPatron := ClientRegistry.RegPatronal;
     with cdsPatron do
     begin
          Data := Servidor.GetPatrones( Empresa );
          if IsEmpty then
             FIMSSPatron := ''
          else if ( not Locate( 'TB_CODIGO', FimssPatron, [] ) ) then
             FIMSSPatron := FieldByName( 'TB_CODIGO' ).AsString;
     end;

     with dmCatalogos.cdsRSocial do
     begin
          Conectar;
          First;
          if IsEmpty then
             FRazonSocial  := ''
          else if ( not Locate( 'RS_CODIGO', FRazonSocial, [] ) ) then
             FRazonSocial := FieldByName( 'RS_CODIGO' ).AsString;
     end;

     FIMSSYear := TheYear( Now );
     FIMSSTipo := tlOrdinaria;
     FIMSSMes := TheMonth( Now ) - 1;
     if FIMSSMes = 0 then
     begin
          FIMSSMes := 12;
          FIMSSYear := FIMSSYear - 1;
     end;
end;



procedure TdmCliente.CierraEmpresa;
begin
     with ClientRegistry do
     begin
          TipoNomina  := Integer (FPeriodoTipo);
          RegPatronal := FImssPatron;
     end;
{$ifdef DOS_CAPAS}
     {$ifndef MULTIPLES_ENTIDADES}
     ServerPresupuestos.CierraEmpresa;
     {$endif}
     {$ifdef REPORTING}
     ServerReporteador.CierraEmpresa;
     {$endif}
     ServerLabor.CierraEmpresa;
     ServerAsistencia.CierraEmpresa;
     ServerIMSS.CierraEmpresa;
     ServerCafeteria.CierraEmpresa;
     ServerRecursos.CierraEmpresa;
     ServerAnualNomina.CierraEmpresa;
     ServerCalcNomina.CierraEmpresa;
     ServerReportes.CierraEmpresa;
     ServerNomina.CierraEmpresa;
     ServerAsistencia.CierraEmpresa;
     ServerTablas.CierraEmpresa;
     ServerSistema.CierraEmpresa;
     ServerIMSS.CierraEmpresa;
     ServerGlobal.CierraEmpresa;
     ServerConsultas.CierraEmpresa;
     ServerCatalogos.CierraEmpresa;
     ServerCafeteria.CierraEmpresa;
     ServerSuper.CierraEmpresa;
     ServerLogin.CierraEmpresa;
{$endif}
     inherited CierraEmpresa;
     cdsPatron.Close;
     cdsEmpleado.Close;
     cdsPeriodo.Close;
end;

procedure TdmCliente.CargaActivosIMSS( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', IMSSPatron );
          AddInteger( 'IMSSYear', IMSSYear );
          AddInteger( 'IMSSMes', IMSSMes );
          AddInteger( 'IMSSTipo', Ord( IMSSTipo ) );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'Year', YearDefault );
          AddInteger( 'Tipo', Ord( PeriodoTipo ) );
          AddInteger( 'Numero', PeriodoNumero );
          AddInteger( 'Clasifi', Ord( PeriodoClasificacion ) );
     end;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaAsistencia );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', YearDefault );
          AddInteger( 'EmpleadoActivo', Empleado );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

{ ********** Prende/Apaga Botones de Navegaci�n ************* }
procedure TdmCliente.InitNavegadorEmpleado;
begin
     FEmpleadoBOF := False;
     FEmpleadoEOF := False;
end;

{ ********** Asignaci�n de Valores Activos ************* }

procedure TdmCliente.SetYearDefault(const Value: Integer);
begin
     inherited SetYearDefault( Value );
     InicializaPeriodo;
end;

function TdmCliente.SetEmpleadoNumero( const Value: TNumEmp ): Boolean;
begin
     if ( FEmpleado <> Value ) then
     begin
          Result := FetchEmployee( Value );
          if Result then
          begin
               with cdsEmpleado do
               begin
                    FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               end;
               FEmpleadoBOF := False;
               FEmpleadoEOF := False;
          end;
     end
     else
         Result := True;
end;

procedure TdmCliente.RefrescaEmpleado;
begin
     cdsEmpleado.Refrescar;
end;

procedure TdmCliente.SetPeriodoTipo(const Value: eTipoPeriodo);
begin
     if ( FPeriodoTipo <> Value ) then
     begin
          FPeriodoTipo := Value;
          InicializaPeriodo;
     end;
end;

function TdmCliente.SetPeriodoNumero(const Value: Integer): Boolean;
begin
     if ( FPeriodoNumero <> Value ) then
     begin
          Result := FetchPeriod( Value );
          if Result then
          begin
               with cdsPeriodo do
               begin
                    FPeriodoNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
               end;
               FPeriodoBOF := False;
               FPeriodoEOF := False;
          end;
     end
     else
         Result := True;
end;

procedure TdmCliente.RefrescaPeriodo;
begin
     FetchPeriod( FPeriodoNumero );
end;

procedure TdmCliente.SetIMSSMes(const Value: Integer);
begin
     if ( FIMSSMes <> Value ) then
     begin
          FIMSSMes := Value;
     end;
end;

procedure TdmCliente.SetIMSSPatron(const Value: String);
begin
     if ( FIMSSPatron <> Value ) then
     begin
          FIMSSPatron := Value;
     end;
end;

procedure TdmCliente.SetIMSSTipo(const Value: eTipoLiqIMSS);
begin
     if ( FIMSSTipo <> Value ) then
     begin
          FIMSSTipo := Value;
     end;
end;

procedure TdmCliente.SetIMSSYear(const Value: Integer);
begin
     if ( FIMSSYear <> Value ) then
     begin
          FIMSSYear := Value;
     end;
end;

function TdmCliente.GetFechaAsistencia: TDate;
begin
     Result := Trunc( FFechaAsistencia );
end;

procedure TdmCliente.SetFechaAsistencia(const Value: TDate);
begin
     if ( FFechaAsistencia <> Value ) then
     begin
          FFechaAsistencia := Value;
     end;
end;

function TdmCliente.GetAsistenciaDescripcion: String;
begin
     Result := FechaCorta( FechaAsistencia );
end;

function TdmCliente.GetEmpleadoDescripcion: String;
begin
     with cdsEmpleado do
     begin
          Result := IntToStr( FEmpleado ) + ': '+ FieldByName( 'PRETTYNAME' ).AsString;
     end;
end;

function TdmCliente.GetIMSSDescripcion: String;
begin
     Result := 'Patr�n ' +
               FIMSSPatron +
               ', ' +
               {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortMonthNames[ FIMSSMes ] +
               ' / ' +
               IntToStr( FIMSSYear ) +
               ', ' +
               ObtieneElemento( lfTipoLiqIMSS, Ord( FIMSSTipo ) );
end;

function TdmCliente.GetRazonSocialDescripcion: String;
begin
     Result := 'Raz�n social ' +
               FRazonSocial +
               ', ' +
               {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortMonthNames[ FIMSSMes ] +
               ' / ' +
               IntToStr( FIMSSYear ) +
               ', ' +
               ObtieneElemento( lfTipoLiqIMSS, Ord( FIMSSTipo ) );
end;

function TdmCliente.GetPeriodoCampoDescripcion: String;
begin
     Result := cdsPeriodo.FieldByName( 'PE_DESCRIP' ).AsString;
end;

function TdmCliente.GetPeriodoCampoEmpleados: Integer;
begin
     Result := cdsPeriodo.FieldByName( 'PE_NUM_EMP' ).AsInteger;
end;

function TdmCliente.GetPeriodoDescripcion: String;
begin
     Result := ObtieneElemento( lfTipoNomina, Ord( FPeriodoTipo ) ) + ': ' + IntToStr( FPeriodoNumero );
end;

function TdmCliente.GetSistemaDescripcion: String;
begin
     Result := 'A�o: ' + IntToStr( YearDefault );
end;

function TdmCliente.GetDatosEmpleadoActivo: TInfoEmpleado;
begin
     with Result do
     begin
          Numero := Self.Empleado;
          with cdsEmpleado do
          begin
               Nombre := FieldByName( 'PRETTYNAME' ).AsString;
               Activo := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
               Baja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
               Ingreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
               Recontratable:= zStrToBool( FieldByName( 'CB_RECONTR').AsString );
          end;

     end;
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          Year := YearDefault;
          Tipo := FPeriodoTipo;
          Numero := FPeriodoNumero;
          Clasifi := FPeriodoClasificacion;
          with cdsPeriodo do
          begin
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
{$ifdef QUINCENALES}
               InicioAsis := FieldByName( 'PE_ASI_INI' ).AsDateTime;
               FinAsis := FieldByName( 'PE_ASI_FIN' ).AsDateTime;
{$endif}
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               StatusTimbrado := eStatusTimbrado( FieldByName( 'PE_TIMBRO' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;

procedure TdmCliente.LeePatrones( Lista: TStrings );
begin
     with Lista do
     begin
          BeginUpdate;
          Clear;
          with cdsPatron do
          begin
               First;
               while not Eof do
               begin
                    // Mejora Confidencialidad
                    if ListaIntersectaConfidencialidad(FieldByName (dmCatalogos.cdsRPatron.LookupConfidenField).AsString, dmCliente.Confidencialidad) then
                       Add( FieldByName( 'TB_CODIGO' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString );
                    Next;
               end;
          end;
          EndUpdate;
     end;
end;

{ Se paso a DBaseCliente
procedure TdmCliente.LeeTiposNomina( Lista: TStrings );
begin
     with Lista do
     begin
          BeginUpdate;
          Clear;
          with dmCatalogos.cdsTPeriodos do
          begin
               Conectar;
               First;
               while ( not EOF ) do
               begin
                    if strLleno( FieldByName( 'TP_DESCRIP' ).AsString ) then
                       Add( FieldByName( 'TP_TIPO' ).AsString + '=' + FieldByName( 'TP_DESCRIP' ).AsString );
                    Next;
               end;
          end;
          EndUpdate;
     end;
end;}

{ ********* Conexiones y Navegaci�n para Empleados ********** }

function TdmCliente.GetEmpleadoInicial: Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoAnterior( Empresa, MAXINT, dmCatalogos.Navegacion, Datos );
     with cdsEmpleado do
     begin
          Data := Datos;
          FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
     end;
     FEmpleadoEOF := True;
end;

function TdmCliente.GetEmpleadoPrimero: Boolean;
var
   iNewEmp: TNumEmp;
begin
     Result := FetchNextEmployee( 0 );
     if Result then
     begin
          with cdsEmpleado do
          begin
               iNewEmp := FieldByName( 'CB_CODIGO' ).AsInteger;
               FEmpleado := iNewEmp;
               FEmpleadoBOF := ( iNewEmp = FEmpleado );
               FEmpleadoEOF := False;
          end;
     end;
end;

function TdmCliente.GetEmpleadoAnterior: Boolean;
begin
     Result := FetchPreviousEmployee( FEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoEOF := False;
     end
     else
         FEmpleadoBOF := True;
end;

function TdmCliente.GetEmpleadoSiguiente: Boolean;
begin
     Result := FetchNextEmployee( FEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoBOF := False;
     end
     else
         FEmpleadoEOF := True;
end;

function TdmCliente.GetEmpleadoUltimo: Boolean;
var
   iNewEmp: TNumEmp;
begin
     Result := FetchPreviousEmployee( MAXINT );
     if Result then
     begin
          with cdsEmpleado do
          begin
               iNewEmp := FieldByName( 'CB_CODIGO' ).AsInteger;
               FEmpleadoBOF := False;
               FEmpleado := iNewEmp;
               FEmpleadoEOF := ( iNewEmp = FEmpleado );
          end;
     end;
end;

function TdmCliente.FetchEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchPreviousEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     if TressShell.FormaActivaNomina then
        Result := Servidor.GetNominaAnterior( Empresa, ParametrosNomina( iEmpleado ), Datos )
     else
        Result := Servidor.GetEmpleadoAnterior( Empresa, iEmpleado, dmCatalogos.Navegacion,  Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchNextEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     if TressShell.FormaActivaNomina then
        Result := Servidor.GetNominaSiguiente( Empresa, ParametrosNomina( iEmpleado ), Datos )
     else
        Result := Servidor.GetEmpleadoSiguiente( Empresa, iEmpleado, dmCatalogos.Navegacion, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.EmpleadoDownEnabled: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := Active and not IsEmpty and not FEmpleadoBOF;
     end;
end;

function TdmCliente.EmpleadoUpEnabled: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := Active and not IsEmpty and not FEmpleadoEOF;
     end;
end;

{ ******* Conexiones y Navegaci�n para Per�odos ********** }

function TdmCliente.GetPeriodoInicial: Boolean;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, YearDefault,  Ord(PeriodoTipo) );
          Result := not IsEmpty;
     end;
end;

{ ***** GetPeriodoInicialTipoNomina ******}

function TdmCliente.GetPeriodoInicialTipoNomina( const TipoNomina: eTipoPeriodo ;const bReiniciar:Boolean = False ): Integer;
var
   cdsPeriodoTemporal: TZetaClientDataSet;
begin
     if (  ( TipoNomina = PeriodoTipo ) and ( not bReiniciar ) ) then
     begin
          Result:= PeriodoNumero;
     end
     else
     begin
          {OP: 10/06/08 Recomend� Eduardo que se cambiar� a 'self' este create}
          cdsPeriodoTemporal := TZetaClientDataSet.Create( self );
          try
             with cdsPeriodoTemporal do
             begin
                  Data := Servidor.GetPeriodoInicial( Empresa, YearDefault, Ord( TipoNomina ) );
                  if not IsEmpty then
                  begin
                       Result:= cdsPeriodoTemporal.FieldByName('PE_NUMERO').AsInteger;
                  end
                  else
                  begin
                       Result := 0;
                  end;
             end;
          finally
             FreeAndNil( cdsPeriodoTemporal );
          end;
     end;

end;

function TdmCliente.GetPeriodoPrimero: Boolean;
var
   iNewNumber: Integer;
begin
     Result := FetchNextPeriod( 0 );
     if Result then
     begin
          with cdsPeriodo do
          begin
               iNewNumber := FieldByName( 'PE_NUMERO' ).AsInteger;
               FPeriodoNumero := iNewNumber;
               FPeriodoBOF := ( iNewNumber = FPeriodoNumero );
               FPeriodoEOF := False;
          end;
     end;
end;

function TdmCliente.GetPeriodoAnterior: Boolean;
begin
     Result := FetchPreviousPeriod( FPeriodoNumero );
     if Result then
     begin
          with cdsPeriodo do
          begin
               FPeriodoNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
          end;
          FPeriodoEOF := False;
     end
     else
         FPeriodoBOF := True;
end;

function TdmCliente.GetPeriodoSiguiente: Boolean;
begin
     Result := FetchNextPeriod( FPeriodoNumero );
     if Result then
     begin
          with cdsPeriodo do
          begin
               FPeriodoNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
          end;
          FPeriodoBOF := False;
     end
     else
         FPeriodoEOF := True;
end;

function TdmCliente.GetPeriodoUltimo: Boolean;
var
   iNewNumber: Integer;
begin
     Result := FetchPreviousPeriod( K_PERIODO_NUMERO_TOPE + 1 );
     if Result then
     begin
          with cdsPeriodo do
          begin
               iNewNumber := FieldByName( 'PE_NUMERO' ).AsInteger;
               FPeriodoBOF := False;
               FPeriodoNumero := iNewNumber;
               FPeriodoEOF := ( iNewNumber = FPeriodoNumero );
          end;
     end;
end;

function TdmCliente.FetchPeriod( const iNumero: Integer ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetPeriodo( Empresa, YearDefault, Ord( PeriodoTipo ), iNumero, Datos );
     if Result then
     begin
          Result := ValidaSimFiniquito( Datos );{OP: 09/06/08}
          if Result then
               with cdsPeriodo do
                    Data := Datos;
     end;
end;

function TdmCliente.FetchPreviousPeriod( const iNumero: Integer ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetPeriodoAnterior( Empresa, YearDefault, Ord( PeriodoTipo ), iNumero, Datos );
     if Result then
     begin
          {OP: 09/06/08}
          if ( Not ValidaSimFiniquito( Datos ) ) then
          begin
               Result := Servidor.GetPeriodoAnterior( Empresa, YearDefault, Ord( PeriodoTipo ),
                                                      Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ), Datos );
          end;
          if Result then
             with cdsPeriodo do
                  Data := Datos;
     end;
end;

function TdmCliente.FetchNextPeriod( const iNumero: Integer ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetPeriodoSiguiente( Empresa, YearDefault, Ord( PeriodoTipo ), iNumero, Datos );
     if Result then
     begin
          {OP: 10/06/08}
          if ( Not ValidaSimFiniquito( Datos ) ) then
          begin
               Result := Servidor.GetPeriodoSiguiente( Empresa, YearDefault, Ord( PeriodoTipo ),
                                                      Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ), Datos );
          end;
          if Result then
             with cdsPeriodo do
                  Data := Datos;
     end;
end;

function TdmCliente.PeriodoDownEnabled: Boolean;
begin
     with cdsPeriodo do
     begin
          Result := Active and not IsEmpty and not FPeriodoBOF;
     end;
end;

function TdmCliente.PeriodoUpEnabled: Boolean;
begin
     with cdsPeriodo do
     begin
          Result := Active and not IsEmpty and not FPeriodoEOF;
     end;
end;

{ ******* cdsEmpleado ********** }

procedure TdmCliente.IngresoChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          FieldByName( 'CB_FEC_ANT' ).AsDateTime := Sender.AsDateTime;
          FieldByName( 'CB_FEC_CON' ).AsDateTime := Sender.AsDateTime;
     end;
end;

procedure TdmCliente.AntiguedadChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          if ( Sender.AsDateTime > FieldByName( 'CB_FEC_ING' ).AsDateTime ) then
{$ifdef INTERFAZ_TRESS}
                 raise Exception.Create( 'Antig�edad No Puede Ser Mayor Que Fecha De Ingreso' );
{$else}
                 DataBaseError( 'Antig�edad No Puede Ser Mayor Que Fecha De Ingreso' );
{$endif}
     end;
end;

procedure TdmCliente.Nombre_NacChange(Sender: TField);
begin
     // Cambios en CB_APE_MAT, CB_APE_PAT, CB_NOMBRES, o CB_FEC_NAC
     with cdsEmpleado do
     begin
          {
          Ya no se hace la validaci�n de la fecha de Nacimiento Nula, de que el Apellido Paterno este en Vacio
          o de que el Campo de Nombres este vacio. Todas las validaciones se hacen den la funci�n CalcRFC
          }
          FieldByName( 'CB_RFC' ).AsString := ZetaCommonTools.CalcRFC( FieldByName( 'CB_APE_PAT' ).AsString,
                                                                       FieldByName( 'CB_APE_MAT' ).AsString,
                                                                       FieldByName( 'CB_NOMBRES' ).AsString,
                                                                       FieldByName( 'CB_FEC_NAC' ).AsDateTime );
     end;
     DatosCURPChange( Sender );   // Tambien cambia el CURP
end;

procedure TdmCliente.DatosCURPChange(Sender: TField);
var
   sTemp: String;
begin
     // Cambios en CB_APE_MAT, CB_APE_PAT, CB_NOMBRES, CB_FEC_NAC, CB_SEXO, CB_ENT_NAC
     with cdsEmpleado do
     begin
          sTemp := ZetaCommonTools.CalcCURP( FieldByName( 'CB_APE_PAT' ).AsString,
                                             FieldByName( 'CB_APE_MAT' ).AsString,
                                             FieldByName( 'CB_NOMBRES' ).AsString,
                                             FieldByName( 'CB_FEC_NAC' ).AsDateTime,
                                             FieldByName( 'CB_SEXO' ).AsString,
                                             dmTablas.GetEstadoCURP( FieldByName( 'CB_ENT_NAC' ).AsString ) );
          if strLleno( sTemp ) then   // Si lo pudo calcular lo encima, si no deja lo que tenga
             FieldByName( 'CB_CURP' ).AsString := sTemp;
     end;
end;

procedure TdmCliente.PosicionaSiguienteEmpleado;
begin
     if not GetEmpleadoSiguiente then // Para posicionar en el siguiente empleado o el Ultimo
        GetEmpleadoUltimo;
     TressShell.CambiaEmpleadoActivos;
end;

{Procedure que valida que el cr�dito de infonavit cumpla con los criterios, se debe de llamar siempre
en un beforepost}
procedure TdmCliente.ValidaCreditoInfo(DataSet: TDataSet);
const
     K_LONGITUD_INFO = 10;
var
   i: Integer;              
   sSignos: String;
begin
     with DataSet do
     begin
          if ( FieldByName( 'CB_INFTIPO' ).AsInteger <> ord( tiNoTiene ) ) then
          begin
               if not( State in [dsEdit, dsInsert] ) then
                  Edit;
               FieldByName( 'CB_INFCRED' ).AsString:= Trim(FieldByName( 'CB_INFCRED' ).AsString);
               i := Length( FieldByName( 'CB_INFCRED' ).AsString );
               if ( i > 0 ) then
               begin
                    if ( i < K_LONGITUD_INFO ) then
                    begin
{$ifdef INTERFAZ_TRESS}
                         raise Exception.Create( Format('# de Cr�dito INFONAVIT INCOMPLETO. Debe tener %d d�gitos', [ K_LONGITUD_INFO ] ) );
{$else}
                         FieldByName( 'CB_INFCRED' ).FocusControl;
                         DatabaseError( Format('# de Cr�dito INFONAVIT INCOMPLETO. Debe tener %d d�gitos', [ K_LONGITUD_INFO ] ) );
{$endif}
                    end
                    else
                    if ( i > K_LONGITUD_INFO ) then
                    begin
{$ifdef INTERFAZ_TRESS}
                         raise Exception.Create( Format('# de Cr�dito INFONAVIT EXCEDE LIMITE. Debe tener %d d�gitos', [ K_LONGITUD_INFO ] ) );
{$else}
                         FieldByName( 'CB_INFCRED' ).FocusControl;
                         DatabaseError( Format('# de Cr�dito INFONAVIT EXCEDE LIMITE. Debe tener %d d�gitos', [ K_LONGITUD_INFO ] ) );
{$endif}
                    end
                    else
                    if ( i = K_LONGITUD_INFO ) and not ZetaCommonTools.ValidaCredito( Trim( FieldByName( 'CB_INFCRED' ).Text) ,sSignos ) then
                    begin
{$ifdef INTERFAZ_TRESS}
                         raise Exception.Create( '# de Cr�dito INFONAVIT:' + sSignos );
{$else}
                         FieldByName( 'CB_INFCRED' ).FocusControl;
                         DatabaseError( '# de Cr�dito INFONAVIT:' + sSignos  );
{$endif}
                    end
                    else if ( FieldByName( 'CB_INFTIPO' ).AsInteger = ord(tiPorcentaje) ) and
                           ( zStrToBool( FieldByName('CB_INFDISM').AsString )  ) and
                        not( FieldByName('CB_INFTASA').AsInteger in [ 20,25,30 ] ) then
                    begin
{$ifdef INTERFAZ_TRESS}
                         raise Exception.Create( 'Al aplicar la tabla de disminuci�n %, el valor de descuento debe ser igual a 20,25 o 30' );
{$else}
                         FieldByName('CB_INFDISM').FocusControl;
                         DatabaseError ( 'Al aplicar la tabla de disminuci�n %, el valor de descuento debe ser igual a 20,25 o 30' );
{$endif}
                         {if not( ZetaClientTools.VerificaValorTasa( FieldByName('CB_INF_OLD').AsFloat ) )then
                         begin
                              if not( ZConfirm( 'Informaci�n', 'La Tasa Anterior Es Un Valor Diferente A Los Valores Propuestos.' + CR_LF +
                                                              '�Desea Continuar?',0,mbYes ) )then
                                 Abort;
                          end;}
                    end;
               end;
          end;
     end;
end;


procedure TdmCliente.ValidaPaterno;
begin
     with cdsEmpleado do
     begin
          if ( Length( FieldByName( 'CB_APE_PAT' ).AsString ) = 0 ) then
          begin
               FieldByName( 'CB_APE_PAT' ).FocusControl;
               if ( Length( FieldByName( 'CB_APE_MAT' ).AsString ) > 0 ) then
{$ifdef INTERFAZ_TRESS}
                  raise Exception.Create( 'Si el Empleado tiene un solo apellido, se debe poner en el Paterno' )
{$else}
                  DataBaseError( 'Si el Empleado tiene un solo apellido, se debe poner en el Paterno' )
{$endif}
               else
{$ifdef INTERFAZ_TRESS}
                  raise Exception.Create( 'Apellido Paterno no puede quedar vac�o' );
{$else}
                  DatabaseError( 'Apellido Paterno no puede quedar vac�o' );
{$endif}
          end;
     end;
end;

procedure TdmCliente.cdsEmpleadoAfterDelete(DataSet: TDataSet);
begin
     cdsEmpleado.Enviar;
     PosicionaSiguienteEmpleado;
end;

procedure TdmCliente.cdsEmpleadoAlEnviarDatos(Sender: TObject);
begin
     dmRecursos.GrabaEmpleado;
end;

procedure TdmCliente.InicializaInfonavit(DataSet: TDataSet);
var
   eInfTipo: eTipoInfonavit;
begin
     with DataSet do
     begin
          eInfTipo := eTipoInfonavit( FieldByName('CB_INFTIPO').AsInteger );
          if (eInfTipo  = tiNoTiene) then
          begin
               ClearInfonavit(DataSet);
          end
          else
          begin
               if (eInfTipo <> tiPorcentaje) then
                  FieldByName('CB_INFDISM').AsString := K_GLOBAL_NO;
               FieldByName('CB_INFACT').AsString:= K_GLOBAL_SI;
          end;
     end;
end;

procedure TdmCliente.ClearInfonavit(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( 'CB_INFCRED' ).AsString := VACIO;
          FieldByName( 'CB_INFTASA' ).AsFloat := 0.0;
          FieldByName( 'CB_INF_OLD' ).AsFloat := 0.0;
          FieldByName( 'CB_INFMANT' ).AsString := K_GLOBAL_NO;
          // EZM: Y1900 Problem
          if (FieldByName( 'CB_INF_INI' ).AsDateTime > 0)then
            FieldByName( 'CB_INF_INI' ).AsDateTime := 0;

          FieldByName('CB_INFACT').AsString:= K_GLOBAL_NO;
          FieldByName('CB_INFDISM').AsString:= K_GLOBAL_NO;
     end;
end;

procedure TdmCliente.cdsEmpleadoBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          {$ifndef DOS_CAPAS}
          if( ( FieldByName( 'CB_ID_BIO' ).AsInteger <> 0 ) and StrVacio( FieldByName( 'CB_GP_COD' ).AsString ) )then
          begin
               FieldByName( 'CB_GP_COD' ).FocusControl;
               DataBaseError( 'Grupo de terminales es requerido' );
          end
          else
          begin
          {$endif}
          // Identificacion
          ValidaPaterno;
          // Otros
          if ( State = dsInsert ) then //Solamente cuando este dando de alta el empleado
          begin
               ValidaCreditoInfo(cdsEmpleado);
               // Limpia Valores dependiendo del tipo de prestamo
               InicializaInfonavit(cdsEmpleado);
          end;
          //Validar campos adicionales obligatorios
          ValidarAdicionales;
          {$ifndef DOS_CAPAS}
          end;
          {$endif}
          //ValidaRFCYCURP; {*** US 9304: Valida el RFC y CURP capturado por el usuario ***}
     end;
end;

procedure TdmCliente.cdsEmpleadoNewRecord(DataSet: TDataSet);
begin
     with cdsEmpleado do
     begin
          FieldByName( 'CB_ACTIVO' ).AsString   := K_GLOBAL_SI;
          FieldByName( 'CB_FEC_ING' ).AsDateTime:= FechaDefault;
          FieldByName( 'CB_FEC_ANT' ).AsDateTime:= FieldByName( 'CB_FEC_ING' ).AsDateTime;
          FieldByName( 'CB_FEC_CON' ).AsDateTime:= FieldByName( 'CB_FEC_ING' ).AsDateTime;
          FieldByName( 'CB_FEC_RES' ).AsDateTime:= FieldByName( 'CB_FEC_ING' ).AsDateTime;
          FieldByName( 'CB_PASAPOR' ).AsString  := K_GLOBAL_NO;
          FieldByName( 'CB_EST_HOY' ).AsString  := K_GLOBAL_NO;
          FieldByName( 'CB_HABLA' ).AsString    := K_GLOBAL_NO;
          FieldByName( 'CB_INFMANT' ).AsString  := K_GLOBAL_NO;
          FieldByName( 'CB_INFDISM' ).AsString  := K_GLOBAL_NO;
          FieldByName( 'CB_G_LOG_1' ).AsString  := K_GLOBAL_NO;
          FieldByName( 'CB_G_LOG_2' ).AsString  := K_GLOBAL_NO;
          FieldByName( 'CB_G_LOG_3' ).AsString  := K_GLOBAL_NO;
          FieldByName( 'CB_NOMYEAR' ).AsInteger := 0;
          FieldByName( 'CB_NOMTIPO' ).AsInteger := 0;
          FieldByName( 'CB_NOMNUME' ).AsInteger := 0;
          FieldByName( 'CB_MOT_BAJ' ).AsString  := '';
          FieldByName( 'CB_DER_PAG' ).AsFloat   := 0;
          FieldByName( 'CB_DER_GOZ' ).AsFloat   := 0;
          FieldByName( 'CB_RANGO_S' ).AsFloat   := 100;
          FieldByName( 'CB_NIVEL0' ).AsString   := ConfidencialidadDefault;
          FieldByName( 'CB_DISCAPA').AsString   := K_GLOBAL_NO;
          FieldByName( 'CB_INDIGE').AsString    := K_GLOBAL_NO;
          FieldByName( 'CB_EMPLEO').AsString    := K_GLOBAL_NO;
          FieldByName( 'CB_REGIMEN' ).AsInteger := 0;
          FieldByName( 'CB_E_MAIL' ).AsString  := VACIO;

          with Global.GetEmpDefault do
          begin
               FieldByName( 'CB_CIUDAD' ).AsString   := Ciudad;
               FieldByName( 'CB_ESTADO' ).AsString   := Estado;
               FieldByName( 'CB_MUNICIP' ).AsString  := Municipio;
               FieldByName( 'CB_CHECA' ).AsString    := ZetaCommonTools.zBoolToStr( Checa );
               FieldByName( 'CB_NACION' ).AsString   := Nacion;
               FieldByName( 'CB_SEXO' ).AsString     := Sexo;
               FieldByName( 'CB_CREDENC' ).AsString  := Credencial;
               FieldByName( 'CB_EDO_CIV' ).AsString  := EstadoCivil;
               FieldByName( 'CB_VIVEEN' ).AsString   := ViveEn;
               FieldByName( 'CB_VIVECON' ).AsString  := ViveCon;
               FieldByName( 'CB_MED_TRA' ).AsString  := MedioTransporte;
               FieldByName( 'CB_ESTUDIO' ).AsString  := Estudio;
               FieldByName( 'CB_AUTOSAL' ).AsString  := ZetaCommonTools.zBoolToStr( AutoSal );
               FieldByName( 'CB_ZONA_GE' ).AsString  := ZonaGeografica;
               FieldByName( 'CB_PATRON' ).AsString   := Patron;
               FieldByName( 'CB_SALARIO' ).AsFloat   := Salario;
               FieldByName( 'CB_CONTRAT' ).AsString  := Contrato;
               FieldByName( 'CB_PUESTO' ).AsString   := Puesto;
               FieldByName( 'CB_CLASIFI' ).AsString  := Clasifi;
               FieldByName( 'CB_TURNO' ).AsString    := Turno;
               FieldByName( 'CB_TABLASS' ).AsString  := TablaSS;
               FieldByName( 'CB_NIVEL1' ).AsString   := Nivel1;
               FieldByName( 'CB_NIVEL2' ).AsString   := Nivel2;
               FieldByName( 'CB_NIVEL3' ).AsString   := Nivel3;
               FieldByName( 'CB_NIVEL4' ).AsString   := Nivel4;
               FieldByName( 'CB_NIVEL5' ).AsString   := Nivel5;
               FieldByName( 'CB_NIVEL6' ).AsString   := Nivel6;
               FieldByName( 'CB_NIVEL7' ).AsString   := Nivel7;
               FieldByName( 'CB_NIVEL8' ).AsString   := Nivel8;
               FieldByName( 'CB_NIVEL9' ).AsString   := Nivel9;
{$ifdef ACS}
               FieldByName( 'CB_NIVEL10' ).AsString   := Nivel10;
               FieldByName( 'CB_NIVEL11' ).AsString   := Nivel11;
               FieldByName( 'CB_NIVEL12' ).AsString   := Nivel12;
{$endif}
               FieldByName( 'CB_NOMINA' ).AsInteger  := Ord( PeriodoTipo );
               FieldByName( 'CB_TDISCAP' ).AsInteger  := Ord( disSinDefinir );

               FieldByName( 'CB_BANCO' ).AsString  := Banco;
               FieldByName( 'CB_REGIMEN' ).AsInteger := Regimen;

          end;
     end;
     dmSistema.AsignaAdicionalesDefault( cdsEmpleado );
end;

{$ifdef VER130}
procedure TdmCliente.cdsEmpleadoReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     with dmRecursos do
     begin
          if PK_Violation( e ) and ( UpdateKind = ukInsert ) then // Solo en la Alta del Empleado //
          begin
               TipoErrorEmpleado := teLlavePrimaria;
               ZetaDialogo.ZError( 'Error Al Agregar',
                                   'El N�mero de Empleado ' +
                                   IntToStr( DataSet.FieldByName( 'CB_CODIGO' ).AsInteger ) +
                                   ' Ya Existe',
                                   0 );
               Action := raAbort;
          end
          else
          begin
               TipoErrorEmpleado := teNinguno;           // Deja que el Reconcile maneje el Error
               Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
          end;
     end;
end;
{$else}
procedure TdmCliente.cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
{$ifdef INTERFAZ_TRESS}
     {$ifndef POLIZA_SENDA}
     {$ifndef CAMBIO}
     if PK_Violation( E ) and ( UpdateKind = ukInsert ) then // Solo en la Alta del Empleado //
        dmInterfase.EscribeErrorExterno( 'No se pudo registrar la alta, el n�mero de empleado ya existe' )
     else
        dmInterfase.EscribeErrorExterno( E.Message );
     {$endif}
     {$endif}
     Action := raCancel;
{$else}
     with dmRecursos do
     begin
          if PK_Violation( e ) and ( UpdateKind = ukInsert ) then // Solo en la Alta del Empleado //
          begin
               TipoErrorEmpleado := teLlavePrimaria;
               ZetaDialogo.ZError( 'Error Al Agregar',
                                   'El N�mero de Empleado ' +
                                   IntToStr( DataSet.FieldByName( 'CB_CODIGO' ).AsInteger ) +
                                   ' Ya Existe',
                                   0 );
               Action := raAbort;
          end
          else
          begin
               TipoErrorEmpleado := teNinguno;           // Deja que el Reconcile maneje el Error
               Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
          end;
     end;
{$endif}
end;
{$endif}

procedure TdmCliente.cdsEmpleadoAlAgregar(Sender: TObject);
begin
     // Aqu� se programar� la Alta del Empleado
     // No hacer Append aqu�, zBoolToStr presentar forma EmpAltaOtros
     // Sugiriendo el siguiente n�mero de empleado ( ProximoEmpleado )
     // en EmpAltaOtros, se pide el n�mero y se hace un
     // GetDatosEmpleado( del n�mero solicitado )
     // Si viene vacio se manda llamar la forma de Alta haciendo un Append
     // Si no viene Vacio se queda en EmpAltaOtros y se activan
     // los botones que mandan llamar a formas de edici�n que
     // ya se tienen programadas.

     {$ifdef VALIDAEMPLEADOSGLOBAL}
     if (ValidaLicenciaEmpleados(evOpAlta) ) then
     begin
     {$endif}
     if ( EmpAltaOtros_DevEx = nil ) then
           EmpAltaOtros_DevEx := TEmpAltaOtros_DevEx.Create( Application );
        with EmpAltaOtros_DevEx do
        begin
             ShowModal;
        end;
     {$ifdef VALIDAEMPLEADOSGLOBAL}
     end;
     {$endif}
end;

procedure TdmCliente.cdsEmpleadoAlBorrar(Sender: TObject);
const
     K_CAPTION = 'Baja Del Empleado';
begin
     with GetDatosEmpleadoActivo do
     begin
          if Activo then
          begin
               if ZetaMsgDlg.ConfirmaCambio( '� Desea Efectuar la Baja de Este Empleado ?' ) then
               begin
                    dmRecursos.AgregaKardex( K_T_BAJA );
			      {$ifndef DOS_CAPAS}
                  // SYNERGY
                  dmSistema.ActualizaIdBio( Empleado, cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger, cdsEmpleado.FieldByName( 'CB_GP_COD' ).AsString, 0 );
			      {$endif}
               end;
          end
          else
          begin
               if ZConfirm( K_CAPTION,
                           'El Empleado ' + IntToStr( Empleado ) +
                           CR_LF +
                           ' Ya Fu� Dado De Baja El ' + ZetaCommonTools.FechaCorta( Baja ) +
                           CR_LF +
                           '� Desea BORRARLO Permanentemente Del Sistema ?',
                           H11521_baja_permanente,
                           mbNo ) then
               begin
                    if ZAccesosMgr.Revisa( D_SIST_PROC_BORRAR_BAJAS ) then
                    begin
                         if dmRecursos.ChecaAcumulados then
                         begin
                              dmSistema.BajaUsuario(Empleado,True);

			      {$ifndef DOS_CAPAS}
                              // SYNERGY
                              dmSistema.ActualizaIdBio( Empleado, 0, VACIO, 0 );
			      {$endif}

                              cdsEmpleado.Delete; // Borra Permanentemente del Sistema
                         end
                         else
                             ZetaDialogo.ZError( K_CAPTION,'El Empleado No Se Puede Borrar Porque Tiene Acumulados En El A�o Activo',0 );
                    end
                    else
                        ZetaDialogo.ZInformation( K_CAPTION,'No Tiene Permiso Para Borrar al Empleado De Forma Permanente',0 );
               end;
          end;
     end;
end;

procedure TdmCliente.cdsEmpleadoAlAdquirirDatos(Sender: TObject);
begin
     FetchEmployee( FEmpleado );
end;

procedure TdmCliente.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     with dmTablas do
     begin
          cdsEstado.Conectar;
          cdsColonia.Conectar;
          cdsMunicipios.Conectar;
          cdsBancos.Conectar;
     end;
     with cdsEmpleado do
     begin
          Global.Conectar;
          if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
          begin
               FieldByName( 'CB_RFC' ).EditMask:= VACIO;
               FieldByName( 'CB_CURP' ).EditMask:= VACIO;
               FieldByName( 'CB_SEGSOC' ).EditMask := VACIO;
          end
          else
          begin
               {*** US 9304: Invalid input value Use escape key to abandon changes RFC Mask: 'LLLL-999999-aaa;0', CURP Mask := 'LLLL-999999-L-LL-LLL-A-9;0'
                    Ocurria a que la cadena capturada no tenia el formato correcto como se especificaba en la mascara ***}
               FieldByName( 'CB_RFC' ).EditMask := 'llll-999999-aaa;0';
               FieldByName( 'CB_CURP' ).EditMask:= 'llll-999999-l-ll-lll-a-9;0';
               {***FIN***}
               with FieldByName( 'CB_SEGSOC' ) do
               begin
                    EditMask := '99-99-99-9999-9;0';
                    {$IFNDEF LITTELFUSE_INTERFAZ}
                    onValidate := CB_SEGSOCValidate;
                    {$ENDIF}
                    onChange := CB_SEGSOCChange;
               end;
          end;
          FieldByName( 'CB_APE_PAT' ).onChange:= Nombre_NacChange;
          FieldByName( 'CB_APE_MAT' ).onChange:= Nombre_NacChange;
          FieldByName( 'CB_NOMBRES' ).onChange:= Nombre_NacChange;
          FieldByName( 'CB_FEC_NAC' ).onChange:= Nombre_NacChange;
          FieldByName( 'CB_SEXO' ).onChange:= DatosCURPChange;
          FieldByName( 'CB_ENT_NAC' ).onChange:= DatosCURPChange;
          FieldByName( 'CB_FEC_ING' ).onChange:= IngresoChange;
          FieldByName( 'CB_FEC_ANT' ).onChange:= AntiguedadChange;
          FieldByName( 'CB_COD_COL' ).onChange:= ColoniaChange;
          FieldByName( 'CB_CONTRAT' ).onChange:= ContratoChange;
          FieldByName( 'CB_FEC_CON' ).onChange:= ContratoChange;
          FieldByName( 'CB_INF_INI' ).onchange:=  CB_INF_ANT_Change;
          CreateSimpleLookup( dmTablas.cdsEstado, 'TB_ELEMENT', 'CB_ENT_NAC' );
          CreateSimpleLookup( dmTablas.cdsColonia, 'TB_ELEM_COL', 'CB_COD_COL' );
          CreateSimpleLookup( dmTablas.cdsBancos, 'TB_ELEM_BAN', 'CB_BANCO' );
          FieldByName( 'CB_ESTADO' ).onchange:=  CB_ESTADOChange;
          FieldByName( 'CB_MUNICIP' ).onchange:=  CB_MUNICIPChange;
          FieldByName( 'CB_NOMINA' ).onvalidate := cdsEmpleadoCB_NOMINAChange;

          ListaFija('CB_REGIMEN',lfTipoRegimenesSAT);
          FieldByName( 'CB_E_MAIL').onvalidate := CB_E_MAILValidate;

     end;
end;

{OP: 12.May.08}
{Si se cambia el contrato o la fecha de inicio de contrato, se
actualiza el campo de fecha de fin de contrato.}
procedure TdmCliente.ContratoChange( Sender: TField );
begin
{$ifndef LITTELFUSE_INTERFAZ}
{$ifndef INTERFAZ_NILFISK}
{$ifndef INTERFAZ_GREATBATCH}
     with dmCatalogos.cdsContratos do
     begin
          if( Not Active ) then
              Conectar;

          if( Locate( 'TB_CODIGO', Sender.DataSet.FieldByName( 'CB_CONTRAT' ).AsString, [] ) ) then
              Sender.DataSet.FieldByName( 'CB_FEC_COV' ).AsDateTime := dmRecursos.CalculaVencimiento( Sender.DataSet.FieldByName( 'CB_FEC_CON' ).AsDateTime,
                                                                                                   FieldByName( 'TB_DIAS' ).AsInteger );
     end;
{$endif}
{$endif}
{$endif}
end;

procedure TdmCliente.CB_ESTADOChange( Sender: TField );
begin
{$ifndef LITTELFUSE_INTERFAZ}
{$ifndef INTERFAZ_NILFISK}
{$ifndef INTERFAZ_GREATBATCH}
     if Sender.AsString = VACIO then
        Sender.DataSet.FieldByName('CB_MUNICIP').AsString := VACIO;
{$endif}
{$endif}
{$endif}
end;

procedure TdmCliente.CB_MUNICIPChange( Sender: TField );
begin
{$ifndef LITTELFUSE_INTERFAZ}
{$ifndef INTERFAZ_NILFISK}
{$ifndef INTERFAZ_GREATBATCH}
     if StrLleno(Sender.AsString) then
        Sender.DataSet.FieldByName('CB_ESTADO').AsString := dmTablas.cdsMunicipios.FieldByName('TB_ENTIDAD').AsString;
{$endif}
{$endif}
{$endif}
end;


procedure TdmCliente.CB_INF_ANT_Change( Sender: TField );
begin
{$ifndef LITTELFUSE_INTERFAZ}
{$ifndef INTERFAZ_NILFISK}
{$ifndef INTERFAZ_GREATBATCH}
     with dmCliente.cdsEmpleado do
     begin
          FieldByName('CB_INF_ANT').AsDateTime := FieldByName('CB_INF_INI').AsDateTime;
     end;
{$endif}
{$endif}
{$endif}
end;

procedure TdmCliente.ColoniaChange(Sender: TField);
{$ifndef INTERFAZ_TRESS}
var
    sColonia, sClinica, sCodPost, sZona: String;
{$endif}
begin
{$ifndef INTERFAZ_TRESS}
     if( strLleno( Sender.AsString ) )then
     begin
          with dmTablas.cdsColonia do
          begin
               Conectar;
               sColonia := FieldByName('TB_ELEMENT').AsString;
               sClinica := FieldByName('TB_CLINICA').AsString;
               sCodPost := FieldByName('TB_CODPOST').AsString;
               sZona := FieldByName('TB_ZONA').AsString;
               if( strLleno( sColonia ) ) then
                    cdsEmpleado.FieldByName('CB_COLONIA').AsString := sColonia;
               if( strLleno( sClinica ) ) then
                     cdsEmpleado.FieldByName('CB_CLINICA').AsString := sClinica;
               if( strLleno( sCodPost) )then
                      cdsEmpleado.FieldByName('CB_CODPOST').AsString := sCodPost;
               if( strLleno( sZona ) )then
                      cdsEmpleado.FieldByName('CB_ZONA').AsString := sZona;
         end;
    end;
{$endif}
end;


function TdmCliente.ValidaSEGSOC( const sNumero: String; var sMensaje: String; const lConfirmar: Boolean = TRUE ): Boolean;
const
     K_ERROR = '� Error En D�gito Verificador !';
     K_PREGUNTA = CR_LF + '� Asignar el Correcto ?';
     K_LONG_MIN = 10;
     K_LONG_OK = K_LONG_MIN + 1;
var
   iLongitud: Integer;
begin
     FCambiarDigito := FALSE;
     Result := TRUE;
     sMensaje := VACIO;
     iLongitud := Length( Trim( sNumero ) );
     if ( iLongitud > 0 ) then
     begin
          if ( StrToInt64Def( sNumero, 0 ) <= 0 ) then
          begin
               sMensaje := 'El N�mero de Seguro Social Solo Debe Incluir D�gitos';
               Result := FALSE;
          end
          else if ( iLongitud < K_LONG_MIN ) then
          begin
               sMensaje := Format( 'Longitud Debe Ser %d + D�gito Verificador', [ K_LONG_MIN ] );
               Result := FALSE;
          end
          else if ( iLongitud = K_LONG_MIN ) then
          begin
{$ifdef INTERFAZ_TRESS}
               sMensaje := 'Falta D�gito Verificador.';
               FCambiarDigito := FALSE;
               Result := FALSE;
{$else}
               if ( not lConfirmar ) or ZetaDialogo.zConfirm( K_ERROR, 'Falta D�gito Verificador' + K_PREGUNTA, 0, mbYes ) then
               begin
                    if ( not lConfirmar ) then
                       sMensaje := 'Falta D�gito Verificador - Se Calcular� el Correcto';
                    FCambiarDigito := TRUE;
               end
               else
                  Result := FALSE;
{$endif}
          end
{$ifdef INTERFAZ_TRESS}
          else if ( ( iLongitud = K_LONG_OK ) and ( not ValidaDigito( sNumero ) ) ) then
          begin
               FCambiarDigito := FALSE;
               {$ifdef BADGER_INTERFAZ}
               sMensaje := 'ADVERTENCIA: D�gito verificador no es v�lido.';
               {$else}
               sMensaje := 'D�gito Verificador No Es V�lido.';
               {$endif}
               Result := FALSE;
          end;
{$else}
          else if ( iLongitud = K_LONG_OK ) and ( not ValidaDigito( sNumero ) ) and
                  ( ( not lConfirmar ) or ZetaDialogo.zConfirm( K_ERROR, 'D�gito Verificador No Es V�lido' + K_PREGUNTA, 0, mbYes ) ) then
          begin
               FCambiarDigito := TRUE;
               if ( not lConfirmar ) then
                  sMensaje := 'D�gito Verificador No Es V�lido - Se Calcular� el Correcto';
          end;
{$endif}
     end;
end;

procedure TdmCliente.CB_SEGSOCValidate(Sender: TField);
var
   sMensaje: String;
begin
     if not ValidaSEGSOC( Sender.AsString, sMensaje ) then
     begin
          if strLleno( sMensaje ) then
{$ifdef INTERFAZ_TRESS}
          begin
               {$ifdef LITTELFUSE_INTERFAZ}
               if( sMensaje = 'D�gito Verificador No Es V�lido.' ) then
               begin
                    sMensaje := 'D�gito verificador de seguro social del empleado es invalido.';
                    dmInterfase.EscribeErrorExterno( sMensaje );
               end;
               if( sMensaje = 'Falta D�gito Verificador.') then
               begin
                    sMensaje := 'Falta d�gito verificador en el n�mero de seguro social del empleado.';
                    dmInterfase.EscribeErrorExterno( sMensaje );
               end;
               {$else}
               {raise Exception.Create( sMensaje );
               Sender.AsString := VACIO;}
               {$ifndef CAMBIO}
               dmInterfase.EscribeErrorExterno(sMensaje);
               {$endif}
               {$endif}
          end
{$else}
             DataBaseError( sMensaje )
{$endif}
          else
             Abort;
     end;
end;

procedure TdmCliente.CB_SEGSOCChange(Sender: TField);
var
   sImss: String;
begin
     if FCambiarDigito then
     begin
          with Sender do
          begin
               sImss := Copy( AsString, 1, 10 );
               AsString := sImss + CalculaDigito( sImss );
          end;
     end;
end;

procedure TdmCliente.SetDatosPuestoMultiples;
var
   i : Integer;
begin
     with dmCatalogos.cdsPuestos do      // Ya debe estar posicionado en el registro correspondiente
     begin
          // Salario x Tabulador - Booleano Global
          if ( FieldByName( 'PU_AUTOSAL' ).AsString <> ZetaCommonClasses.K_USAR_GLOBAL ) then    // Si NO se quiere Usar Global de Empresa
             ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_AUTOSAL' ), cdsEmpleado.FieldByName( 'CB_AUTOSAL' ) )
          else if ( cdsEmpleado.State <> dsInsert ) then                                         // Si est� insertando ya se asign� el default de Empresa
             cdsEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString := ZetaCommonTools.zBoolToStr( Global.GetEmpDefault.AutoSal );   // Si no est� insertando se investiga el Global de la Empresa - Viene de registro de cambios m�ltiples

          ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_CLASIFI' ), cdsEmpleado.FieldByName( 'CB_CLASIFI' ) );
          ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_TURNO' ), cdsEmpleado.FieldByName( 'CB_TURNO' ) );
          ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_CONTRAT' ), cdsEmpleado.FieldByName( 'CB_CONTRAT' ) );
          for i := 1 to K_GLOBAL_NIVEL_MAX do
              ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_NIVEL' + IntToStr(i) ), cdsEmpleado.FieldByName( 'CB_NIVEL' + IntToStr(i) ) );
     end;
end;

procedure TdmCliente.SetDatosPuesto( const sPuesto: String );

     function TieneSolaConfidencialidad( sConfiden : string ) : Boolean;
     var
        lista : TStringList;
     begin
         Result := TRUE;

         if strLleno( sConfiden )  then
         begin
              lista :=   TStringList.Create;
              lista.CommaText := sConfiden;
              Result :=  not( lista.Count > 1);
              FreeAndNil ( lista );
         end;
     end;

begin
     if strLleno( sPuesto ) then
     begin
          with dmCatalogos.cdsPuestos do

          begin
               Conectar;
               if ( not IsEmpty ) then
               begin
                    if ( sPuesto = FieldByName( 'PU_CODIGO' ).AsString ) or
                       Locate( 'PU_CODIGO', sPuesto, [] ) then
                    begin
                         cdsEmpleado.FieldByName( 'CB_PUESTO' ).AsString := sPuesto;   // Directamente se asigna este puesto
                         ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_TABLASS' ), cdsEmpleado.FieldByName( 'CB_TABLASS' ) );
                         ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_PATRON' ), cdsEmpleado.FieldByName( 'CB_PATRON' ) );
                         ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_ZONA_GE' ), cdsEmpleado.FieldByName( 'CB_ZONA_GE' ) );
                         ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_SALARIO' ), cdsEmpleado.FieldByName( 'CB_SALARIO' ) );
                         ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_PER_VAR' ), cdsEmpleado.FieldByName( 'CB_PER_VAR' ) );
                         if TieneSolaConfidencialidad( FieldByName( 'PU_NIVEL0' ).AsString ) then
                            ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_NIVEL0' ), cdsEmpleado.FieldByName( 'CB_NIVEL0' ) );
                            
                         // Checa Tarjeta - Booleano Global
                         if ( FieldByName( 'PU_CHECA' ).AsString <> ZetaCommonClasses.K_USAR_GLOBAL ) then    // Si se quiere dejar UsarGlobal de Empresa ya se asign� en OnNewRecord
                            ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_CHECA' ), cdsEmpleado.FieldByName( 'CB_CHECA' ) );
                         // Labor
                         ZetaDataSetTools.RevisaCambioCampo( FieldByName( 'PU_AREA' ), cdsEmpleado.FieldByName( 'CB_AREA' ) );
                         // Multiples
                         SetDatosPuestoMultiples;
                    end;
               end;
          end;
     end;
end;

procedure TdmCliente.SetDatosPlaza( const iPlaza: integer );
var
   sDescrip: String;
begin
     if ( iPlaza <> 0 ) then
     begin
          with dmRecursos.cdsPlazasLookup do
          begin
               if ( Active and ( iPlaza = FieldByName( 'PL_FOLIO' ).AsInteger ) ) or
                  Lookupkey( IntToStr( iPlaza ), VACIO, sDescrip ) then
               begin
                    //Todos los datos de Plaza estan llenos, y son los defaults del empleado.
                    cdsEmpleado.FieldByName( 'CB_PUESTO' ).AsString := FieldByName('PU_CODIGO').AsString;   // Directamente se asigna este puesto
                    cdsEmpleado.FieldByName( 'CB_PLAZA' ).AsInteger := FieldByName('PL_FOLIO').AsInteger;   // Directamente se asigna esta plaza
                    cdsEmpleado.FieldByName( 'CB_CLASIFI' ).AsString := FieldByName('PL_CLASIFI' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_TURNO' ).AsString := FieldByName('PL_TURNO' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_PATRON' ).AsString  := FieldByName('PL_PATRON' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL0' ).AsString  := FieldByName('PL_NIVEL0' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL1' ).AsString  := FieldByName('PL_NIVEL1' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL2' ).AsString  := FieldByName('PL_NIVEL2' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL3' ).AsString  := FieldByName('PL_NIVEL3' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL4' ).AsString  := FieldByName('PL_NIVEL4' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL5' ).AsString  := FieldByName('PL_NIVEL5' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL6' ).AsString  := FieldByName('PL_NIVEL6' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL7' ).AsString  := FieldByName('PL_NIVEL7' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL8' ).AsString  := FieldByName('PL_NIVEL8' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL9' ).AsString  := FieldByName('PL_NIVEL9' ).AsString;
{$ifdef ACS}
                    cdsEmpleado.FieldByName( 'CB_NIVEL10' ).AsString  := FieldByName('PL_NIVEL10' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL11' ).AsString  := FieldByName('PL_NIVEL11' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NIVEL12' ).AsString  := FieldByName('PL_NIVEL12' ).AsString;
{$endif}

                    cdsEmpleado.FieldByName( 'CB_CONTRAT' ).AsString := FieldByName('PL_CONTRAT' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_SALARIO' ).AsString := FieldByName('PL_SALARIO' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_PER_VAR' ).AsString := FieldByName('PL_PER_VAR' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_TABLASS' ).AsString := FieldByName('PL_TABLASS' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString  := FieldByName('PL_AUTOSAL' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_ZONA_GE' ).AsString  := FieldByName('PL_ZONA_GE' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_CHECA' ).AsString  := FieldByName('PL_CHECA' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_AREA' ).AsString  := FieldByName('PL_AREA' ).AsString;
                    cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger := FieldByName( 'PL_NOMINA' ).AsInteger;
               end
               else raise Exception.Create('La plaza especificada no existe');
          end;
     end;
end;


function TdmCliente.GetFiltroLookupPuestos: String;
var
   sFiltroConfid: String;
begin

// Ya se filtra por el requerimiento #2

{$ifndef CONFIDENCIALIDAD_MULTIPLE}
    if strLleno( Confidencialidad ) then
        sFiltroConfid := Format( '( ( PU_NIVEL0 = %s ) or ( PU_NIVEL0 = %s ) )', [ EntreComillas( VACIO ),                // Saldr�n Puestos que no tengan nivel de confidencialidad asignado
                                                                                       EntreComillas( Confidencialidad ) ] )  // o los puestos que le correspondan para su nivel de confidencialidad
     else
{$endif}
        sFiltroConfid := VACIO;

     Result := sFiltroConfid;
end;

function TdmCliente.EsPuestoValido( const sPuesto: String ): Boolean;
begin
     Result := TRUE;
     if strLleno( sPuesto ) then
     begin
          with dmCatalogos.cdsPuestos do
          begin
               Conectar;
               if ( not IsEmpty ) then
               begin
                    if ( sPuesto = FieldByName( 'PU_CODIGO' ).AsString ) or
                       Locate( 'PU_CODIGO', sPuesto, [] ) then
                    begin
                         // valida que est� activo
                         Result := zStrToBool( FieldByName( 'PU_ACTIVO' ).AsString );
                         if ( not Result ) then
                            //sMensaje := '� El Puesto Seleccionado No Est� Activo !'
                            ZetaDialogo.ZError('Error', '� El Puesto Seleccionado No Est� Activo !', 0 )
       {$ifdef CONFIDENCIALIDAD_MULTIPLE}
               ;
       {$else}
                         else
                         begin
                              Result := strVacio( Confidencialidad ) or
                                        ( strLleno( Confidencialidad ) and EsDeConfidencialidad( FieldByName( 'PU_NIVEL0' ).AsString ) );
                              if ( not Result ) then
                                 //sMensaje := '� El Puesto no Corresponde al Nivel de Confidencialidad de la Empresa !';
                                 Result := ZetaDialogo.ZWarningConfirm('� Atenci�n !', Format( 'El Nuevo Puesto Tiene Un Nivel De Confidencialidad %0:s Diferente Del Asignado Al Empleado%0:s� Desea Continuar ?', [ CR_LF ] ), 0, mbCancel );
                         end;
       {$endif}
                    end;
               end;
          end;
     end;
end;

{
procedure TdmCliente.CB_SEGSOCValidate(Sender: TField);
const
     K_ERROR = '� Error En D�gito Verificador !';
     K_PREGUNTA = CR_LF + '� Asignar el Correcto ?';
var
   i: Integer;
begin
     lCambiarDigito := FALSE;
     with Sender do
     begin
          i := Length( Trim( AsString ) );
          if ( i > 0 ) then
          begin
               if ( i < 10 ) then
                  DataBaseError( 'Longitud Debe Ser 10 + D�gito Verificador' )
               else
                   if ( i = 10 ) then
                   begin
                        if ZetaDialogo.zConfirm( K_ERROR, 'Falta D�gito Verificador' + K_PREGUNTA, 0, mbYes ) then
                           lCambiarDigito := TRUE
                        else
                            Abort;
                   end
                   else
                       if ( i = 11 ) and not ValidaDigito( AsString ) and ZetaDialogo.zConfirm( K_ERROR, 'D�gito Verificador No Es V�lido' + K_PREGUNTA, 0, mbYes ) then
                          lCambiarDigito := TRUE;
          end;
     end;
end;
}

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
    CargaActivosIMSS(Parametros);
    CargaActivosPeriodo(Parametros);
    CargaActivosSistema(Parametros);
end;

function TdmCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
     case eTipo of
          stEmpleado: Result := GetEmpleadoDescripcion;
          stPeriodo:
          begin
               Result := VACIO;
               if PeriodoTipo <> K_PERIODO_VACIO then
                  Result := GetPeriodoDescripcion;
          end;
          stIMSS: Result := GetIMSSDescripcion;
          stFecha: Result := GetAsistenciaDescripcion;
          stSistema: Result := GetSistemaDescripcion;
          stRazonSocial : Result := GetRazonSocialDescripcion;
     end;
end;

function TdmCliente.ParametrosNomina( const iEmpleado : TNumEmp ) : OleVariant;
begin
     with FParamsCliente do
     begin
          Clear;
          CargaActivosPeriodo( FParamsCliente );
          AddInteger( 'Empleado', iEmpleado );
          Result := VarValues;
     end;
end;

function TdmCliente.GetMatsushita: Boolean;
begin
     Result := Global.GetGlobalBooleano(K_GLOBAL_MATSUSHITA);
end;

procedure TdmCliente.SetEmpresaSeleccion( const sCodigo: String );
var
   oDataSet: TZetaClientDataSet;
begin
     oDataSet := TZetaClientDataSet.Create( self );
     try
        with oDataSet do
        begin
             Data := Servidor.GetCompanys( 0, Ord( tcRecluta ) );   // No validar derechos de acceso
             if Locate( 'CM_CODIGO', sCodigo, [] ) then
                FEmpresaSeleccion := BuildEmpresa( oDataSet )
             else
                DataBaseError( 'No Se Pudo Localizar Base De Datos De Selecci�n' + CR_LF +
                               'Verifique La Configuraci�n' );
        end;
     finally
            FreeAndNil( oDataSet );
     end;
end;

function TdmCliente.GetEmpresaSeleccion: Variant;
var
   sCodigo: String;
begin
     sCodigo := Global.GetGlobalString( K_GLOBAL_EMPRESA_SELECCION );
     if strLleno( sCodigo ) then
     begin
          if VarIsEmpty( FEmpresaSeleccion ) or ( FEmpresaSeleccion[ P_CODIGO ] <> sCodigo ) then
             SetEmpresaSeleccion( sCodigo );
          Result := FEmpresaSeleccion;
     end
     else
         DataBaseError( 'No Se Tiene Configurado El C�digo de Empresa de Selecci�n' );
end;

function TdmCliente.NominaYaAfectada: Boolean;
begin
     Result := ( GetDatosPeriodoActivo.Status in [ spAfectadaParcial,spAfectadaTotal ] );
end;

{ Vencimiento de Password }

procedure TdmCliente.ActualizaClaveUsuario(const sAnterior, sClave: String);
begin
     inherited ActualizaClaveUsuario( sAnterior, sClave );
     TressShell.SetDataChange( [ enUsuarios ] );
end;

procedure TdmCliente.SetSeguridad( const Valores: OleVariant );
begin
     inherited;
      TressShell.ChangeTimerInfo;
end;
{$ifdef BIOMONITOR }
function TdmCliente.GetEmpresas: OleVariant;
begin
     Result := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
end;
{$endif}


function TdmCliente.PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                               const iEmpleado: integer;
                                               const eTipo: eStatusPeriodo;
                                               const lPuedeModificar: Boolean;
                                               var sMensaje: string ): Boolean;
begin
     Result := dmAsistencia.PuedeCambiarTarjetaStatus( dInicial, iEmpleado, eTipo, lPuedeModificar, sMensaje );
end;

procedure TdmCliente.GrabaBloqueoSistema(const lBlockSistema: Boolean);
begin
      Servidor.GrabaBloqueoSistema( lBlockSistema ) ;
end;

procedure TdmCliente.ValidarAdicionales;
var
   Mensaje:string;
   i:Integer;

function ValidarPorTipo(Field:TField):Boolean;
begin
     result := False;
     Mensaje := VACIO;
     with cdsEmpleado do
     begin
          case Field.DataType of
               ftString :
                         begin
                               result := not StrLleno(Field.AsString);
                               Mensaje := dmSistema.CamposAdic.CampoField[Field.FieldName].Letrero +' no puede quedar vac�o';
                         end;
               ftInteger ,ftSmallint :
                         begin
                              result := Field.AsInteger = 0 ;
                               Mensaje := dmSistema.CamposAdic.CampoField[Field.FieldName].Letrero +' no puede ser cero';
                         end;
               ftFloat :
                         begin
                              result := Field.AsFloat = 0;
                              Mensaje := dmSistema.CamposAdic.CampoField[Field.FieldName].Letrero +' no puede ser cero';
                         end;
               ftDate,ftDateTime :
                         begin
                              result := Field.AsDateTime = NullDateTime;
                              Mensaje := dmSistema.CamposAdic.CampoField[Field.FieldName].Letrero +' es una fecha inv�lida';
                         end;
          end;
     end;
end;
begin
     with cdsEmpleado do
     begin
          for i := 0  to Fields.Count - 1 do
          begin
               if Fields[i].Tag = 1 then
               begin
                    if ValidarPorTipo ( Fields[i] ) then
                    begin
{$ifdef INTERFAZ_TRESS}
                         raise Exception.Create( Mensaje );
{$else}
                         DatabaseError( Mensaje )
{$endif}
                    end;
               end;
          end;
     end;
end;

{OP: 10/06/08}
function TdmCliente.ValidaSimFiniquito( const oDatosPeriodo: OleVariant ): Boolean;
var
   iNominaSim, iNumero :Integer;
   cdsPeriodoTmp : TZetaClientDataSet;
begin
     cdsPeriodoTmp := TZetaClientDataSet.Create( self );
     try
        with cdsPeriodoTmp do
        begin
             Data := oDatosPeriodo;
             iNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
        end;
     finally
            FreeAndNil( cdsPeriodoTmp );
     end;

     iNominaSim := Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS );
     Result := ( iNumero <> iNominaSim ) or
                    ( ( iNumero = iNominaSim ) and ZAccesosMgr.CheckDerecho( D_EMP_NOM_SIM_FINIQUITOS, K_DERECHO_CONSULTA ) );
end;

function TdmCliente.GetComputerName: String;
begin
     Result := ZetaWinAPITools.GetComputerName;
end;

procedure TdmCliente.cdsEmpleadoCB_NOMINAChange(Sender: TField);
begin
     dmRecursos.validacionTipoNominaChange(Sender);
end;


procedure TdmCliente.SetRazonSocial(const Value: String);
begin
     if ( FRazonSocial <> Value ) then
     begin
          FRazonSocial := Value;
     end;
end;

procedure TdmCliente.CB_E_MAILValidate(Sender: TField);
var
   sMensaje: String;
begin
     if strLleno( Sender.AsString ) then
     begin
        if not ZetaCommonTools.ValidarEmail( Sender.AsString ) then
        begin
             sMensaje := 'El correo electr�nico no es v�lido, debe estar capturado en el formato usuario@dominio.com';

   {$ifdef INTERFAZ_TRESS}
             {raise Exception.Create( sMensaje );
             Sender.AsString := VACIO;}
             {$ifndef CAMBIO}
             dmInterfase.EscribeErrorExterno(sMensaje);
             {$endif}
   {$else}
             DataBaseError( sMensaje )
   {$endif}
        end;
     end;
end;

{$ifdef DEVEX}
function TdmCliente.GetSkinController: TdxSkinController;
begin
     Result:= TressShell.DevEx_SkinController;
end;
{$endif}

{*** US 9304: Invalid input value Use escape key to abandon changes
     Se valida que el RFC y el CURP tenga el formato correcto***}
procedure TdmCliente.ValidaRFCYCURP;
const
     K_LONGITUD_RFC  = 13;
     K_LONGITUD_CURP = 18;
var
   sMensajeError: String;
   sRFC_OLD: String;
   sCURP_OLD: String;
begin
     with cdsEmpleado do
     begin
          sRFC_OLD := FieldByName( 'CB_RFC' ).OldValue;
          sCURP_OLD := FieldByName( 'CB_CURP' ).OldValue;
          if ( sRFC_OLD <> FieldByName( 'CB_RFC' ).AsString ) and ( Length( FieldByName( 'CB_RFC' ).AsString ) = K_LONGITUD_RFC ) and ( not ValidaRFC( FieldByName( 'CB_RFC' ).AsString, sMensajeError ) ) then
             DataBaseError(sMensajeError);
          if ( sCURP_OLD <> FieldByName( 'CB_CURP' ).AsString ) and ( Length( FieldByName( 'CB_CURP' ).AsString ) = K_LONGITUD_CURP ) and ( not ValidaCURP( FieldByName( 'CB_CURP' ).AsString, sMensajeError ) ) then
             DataBaseError(sMensajeError);
     end;
end;

{*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
function TdmCliente.CambioValoresActivosGraficas: Boolean;
begin
     Result := False;
     if ( FPeriodoActivoNumeroAux <> FPeriodoNumero ) or ( FSistemaYearAux <> YearDefault ) then
     begin
          Result := True;
     end;
end;

procedure TdmCliente.RespaldaValoresActivosGraficas;
begin
     FPeriodoActivoNumeroAux := FPeriodoNumero;
     FSistemaYearAux := YearDefault;
end;

procedure TdmCliente.AnaliticaContabilizarUso(const sModulo: String;const HelpContext, iConteo: Integer);
begin
    Servidor.AnaliticaContabilizarUso(Empresa,sModulo,HelpContext,iConteo);
end;

{*** FIN ***}
procedure TdmCliente.SetSesionIDDashlet( const Value: Integer );
begin
     Self.FSesionIDDashlet := Value;
end;

function TdmCliente.GetSesionIDDashlet: Integer;
begin
     Result := FSesionIDDashlet;
end;

end.





