unit DBaseCliente;

interface
{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBasicoCliente, ZetaClientDataSet, Db, DBClient,
{$ifdef DOS_CAPAS}
{$else}
     Login_TLB,
{$endif}
  ZetaTipoEntidad,
  ZetaCommonClasses,
  ZetaCommonLists;

type
  TInfoEmpleado = record
    Numero: TNumEmp;
    Nombre: String;
    Activo: Boolean;
    Baja: TDate;
    Ingreso: TDate;
    Recontratable: Boolean;
  end;

  TBaseCliente = class(TBasicoCliente)
    cdsEmpleadoLookUp: TZetaLookupDataSet;
    cdsTPeriodos: TZetaLookupDataSet;
    procedure cdsEmpleadoLookUpLookupKey(Sender: TZetaLookupDataSet;var lOk: Boolean; const sFilter, sKey: String;var sDescription: String);
    procedure cdsEmpleadoLookUpLookupSearch(Sender: TZetaLookupDataSet;var lOk: Boolean; const sFilter: String; var sKey,sDescription: String);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsTPeriodosAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FYearDefault: Integer;
    TipoLookup: TTipoLookupEmpleado;
    procedure LeeTiposNominaDinamicos(const lTodosPeriodos: Boolean = FALSE );
    procedure LeeTiposNominaConfidencialDinamicos;
  protected
    FEmpleado: TNumEmp;
    function GetUsaPlazas: Boolean;
    procedure SetYearDefault(const Value: Integer);virtual;//tress
    function PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                        const iEmpleado: integer;
                                        const eTipo: eStatusPeriodo;
                                        const lPuedeModificar: Boolean;
                                        var sMensaje: string): Boolean;virtual;
  public
    { Public declarations }
    property Empleado: TNumEmp read FEmpleado;
    property YearDefault: Integer read FYearDefault write SetYearDefault;
    property UsaPlazas: Boolean read GetUsaPlazas;
    function ModuloAutorizadoReportes(const Entidad: TipoEntidad): Boolean;override;
    function ModuloAutorizadoCosteo: Boolean;
    procedure CierraEmpresa; override;
    procedure GetEmpleadosBuscados(const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    procedure GetEmpleadosBuscados_DevEx (const sPista: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    procedure InitActivosSistema;
    procedure ResetLookupEmpleado;
    procedure SetLookupEmpleado( Tipo: TTipoLookupEmpleado = eLookEmpGeneral );
    procedure SetTipoLookupEmpleado( const Tipo: TTipoLookupEmpleado );
    function GetListaEmpleados( const sFiltro : string ): string;virtual;
    function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String; override;
    function PuedeModificarTarjetasAnteriores: Boolean;
    function PuedeCambiarTarjeta( Dataset: TDataset ): Boolean;overload;
    function PuedeCambiarTarjeta( Dataset: TDataset; var sMensaje: string ): Boolean;overload;
    function PuedeCambiarTarjeta( const dFecha: TDate; const iEmpleado: integer ): Boolean;overload;
    function PuedeCambiarTarjeta( const dFecha: TDate;  const iEmpleado: integer; var sMensaje: string ): Boolean;overload;
    function PuedeCambiarTarjeta( const dInicial, dFinal: TDate; const iEmpleado: integer; var sMensaje: string ):Boolean;overload;virtual;

    function PuedeCambiarTarjetaDlg( Dataset: TDataset ): Boolean;overload;
    function PuedeCambiarTarjetaDlg( const dFecha: TDate; const iEmpleado: integer): Boolean;overload;
    function PuedeCambiarTarjetaDlg( const dInicial, dFinal: TDate; const iEmpleado: integer = 0 ):Boolean;overload;

    function ValidaTarjetaStatus( const eTipo: eStatusPeriodo ): Boolean;overload;
    function ValidaTarjetaStatus( const eTipo: eStatusPeriodo; var sMensaje: string ): Boolean;overload;
    function ValidaTarjetaStatusDlg( const eTipo: eStatusPeriodo ): Boolean;
    procedure GetStatusEmpleado(EmpleadoActivo: TInfoEmpleado; var Caption, Hint : string; var Color : TColor );
    procedure InitArrayTPeriodo;//acl
  end;

var
  BaseCliente: TBaseCliente;

implementation

uses FAutoClasses,
     DGlobal,
     DDiccionario,
     ZGlobalTress,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaDialogo,
     ZetaBuscaEmpleado_DevEx,
     ZetaCommonTools,
     DCliente, FTressShell;

{$R *.DFM}

procedure TBaseCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     TipoLookup := eLookEmpGeneral;

     //(@am): Creacion de listas de objetos
     FListaTiposPeriodoConfidencialidad := TStringList.Create;

     //(@am): Creacion de listas
     FArregloPeriodo := TStringList.Create;
     FArregloPeriodoConfidencial := TStringList.Create;
     FArregloTipoNomina := TStringList.Create;

end;

procedure TBaseCliente.CierraEmpresa;
begin
     cdsEmpleadoLookup.Active := False;
     inherited CierraEmpresa;
end;

procedure TBaseCliente.cdsEmpleadoLookUpLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
var
   Datos: OleVariant;
begin
     if ( FServidor = nil ) then
        lOk := Servidor.GetLookupEmpleado( Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( TipoLookup ) )
     else
        lOk := FServidor.GetLookupEmpleado( Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( TipoLookup ) );
     if lOk then
     begin
          with cdsEmpleadoLookUp do
          begin
               Init;
               Data := Datos;
               sDescription := GetDescription;
          end;
     end;
end;

procedure TBaseCliente.cdsEmpleadoLookUpLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
     lOk := ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription );
end;

procedure TBaseCliente.InitActivosSistema;
begin
     FFechaDefault := Now;
     FYearDefault := TheYear( FFechaDefault );
end;

//acl
procedure TBaseCliente.InitArrayTPeriodo;
begin
     LeeTiposNominaDinamicos(TRUE);
     // Mejora Confidencialidad
     LeeTiposNominaConfidencialDinamicos;
end;

//(@am): Se crea este metodo para guardar en un alista de objetos (estructura TPeriodo) 'N' cantidad de tipos de periodo.
 procedure TBaseCliente.LeeTiposNominaDinamicos(const lTodosPeriodos: Boolean = FALSE );
 var
   sValorLista: String;
   iPos: Integer;
 begin
          try

              FArregloPeriodo.BeginUpdate;
              FArregloTipoNomina.BeginUpdate;

              FArregloPeriodo.Clear;
              FArregloTipoNomina.Clear;

              with cdsTPeriodos do
              begin
                   Refrescar;
                   First;
                   while ( not EOF ) do
                   begin
                        if strLleno( FieldByName( 'TP_DESCRIP' ).AsString ) or ( lTodosPeriodos ) then
                        begin
                             //Llena lista de strings
                             sValorLista:= FieldByName( 'TP_DESCRIP' ).AsString;
                             iPos:= Pos ( '/', sValorLista );

                             if ( ( iPos > 0 ) and ( sValorLista[iPos+1] <> '' ) and ( sValorLista[iPos-1] <> '' ) ) then
                                  FArregloPeriodo.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + Copy( sValorLista, 1, iPos - 1 ))
                             else
                                  FArregloPeriodo.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + sValorLista);

                             FArregloTipoNomina.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + Copy( sValorLista, Pos ( '/', sValorLista ) + 1, Length ( sValorLista )));
                        end;
                        Next;
                   end;
              end;
          finally
                 FArregloPeriodo.EndUpdate;
                 FArregloTipoNomina.EndUpdate;
          end;
 end;

//(@am): Se crea este metodo para guardar una lista de objetos (estructura TPeriodo) 'N' cantidad de tipos de periodo.
procedure TBaseCliente.LeeTiposNominaConfidencialDinamicos;
var
   sConfidencialidad : String;
   bIntersectaConfidencialidad : boolean;
   sValorLista: String;
   iPos: Integer;
begin
     try
          FArregloPeriodoConfidencial.BeginUpdate;
          FListaTiposPeriodoConfidencialidad.Clear;
          FArregloPeriodoConfidencial.Clear;

          with cdsTPeriodos do
          begin
               Refrescar;
               First;
               while ( not EOF ) do
               begin
                    sConfidencialidad :=  FieldByName (LookupConfidenField).AsString;
                    bIntersectaConfidencialidad := ListaIntersectaConfidencialidad(dmCliente.Confidencialidad, sConfidencialidad);

                    //Lista de objetos
                    FListaTiposPeriodoConfidencialidad.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + FieldByName( 'TP_CLAS' ).AsString );

                    //Lista de strings
                    sValorLista:= FieldByName( 'TP_DESCRIP' ).AsString;
                    iPos:= Pos ( '/', sValorLista );

                    if bIntersectaConfidencialidad then
                    begin
                          if ( ( iPos > 0 ) and ( sValorLista[iPos+1] <> '' ) and ( sValorLista[iPos-1] <> '' ) ) then
                               FArregloPeriodoConfidencial.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + Copy( sValorLista, 1, iPos - 1 ))
                          else
                               FArregloPeriodoConfidencial.Add(FieldByName( 'TP_TIPO' ).AsString + '=' + sValorLista);
                    end;
                    Next;
               end;
          end;
     finally
            FArregloPeriodoConfidencial.EndUpdate;
     end;
end;

procedure TBaseCliente.SetYearDefault(const Value: Integer);
begin
     if ( FYearDefault <> Value ) then
     begin
          FYearDefault := Value;
     end;
end;

procedure TBaseCliente.GetEmpleadosBuscados( const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet );
begin
     with oDataSet do
     begin
          Init;
          if ( FServidor = nil ) then
             Data := Servidor.GetEmpleadosBuscados( oEmpresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca )
          else
             Data := FServidor.GetEmpleadosBuscados( oEmpresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca );
     end;
end;

//DevEx: Metodo creado para invocar la nueva Busqueda de empleados al Servidor
procedure TBaseCliente.GetEmpleadosBuscados_DevEx (const sPista: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
begin
     with oDataSet do
     begin
          Init;
          if ( FServidor = nil ) then
             Data := Servidor.GetEmpleadosBuscados_DevEx( oEmpresa, sPista )
          else
             Data := FServidor.GetEmpleadosBuscados_DevEx( oEmpresa, sPista );
     end;
end;

procedure TBaseCliente.SetLookupEmpleado( Tipo: TTipoLookupEmpleado = eLookEmpGeneral );
begin
     TipoLookup := Tipo;
{$ifndef DOS_CAPAS}
     CreateFixedServer( CLASS_dmServerLogin, FServidor );
{$endif}
end;

procedure TBaseCliente.ResetLookupEmpleado;
begin
     if Assigned( FServidor ) then
     begin
{$ifndef DOS_CAPAS}
          DestroyFixedServer( FServidor );
{$endif}
          TipoLookup := eLookEmpGeneral;
     end;
end;

procedure TBaseCliente.SetTipoLookupEmpleado(const Tipo: TTipoLookupEmpleado);
begin
     { Permite especificar lista de datos de Empleado que regrese cdsEmpleadoLookup
       sin tener que definir un Servidor Fijo }
     TipoLookup := Tipo;
end;


function TBaseCliente.ModuloAutorizadoReportes( const Entidad: TipoEntidad ): Boolean;
begin
     //CV: esto estaba anteriormente en la clase base, pero
     //es algo muy especifico de Tress
     if ( Entidad = enComida ) then
        Result := ModuloAutorizado( okCafeteria )
     else
         if ( Entidad in [ enCurProg, enKarCurso ] ) then
            Result := ModuloAutorizado( okCursos )
         else
             Result := True;
end;

function TBaseCliente.ModuloAutorizadoCosteo: Boolean;
begin
     result := {$ifdef DOS_CAPAS} FALSE {$ELSE} ModuloAutorizadoDerechos( OkSupervisores ) and ( not Sentinel_EsProfesional_MSSQL ){$endif};
end;

function TBaseCliente.GetListaEmpleados( const sFiltro : string ): string;
begin
     Result := '';
end;

function TBaseCliente.PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                                 const iEmpleado: integer;
                                                 const eTipo: eStatusPeriodo;
                                                 const lPuedeModificar: Boolean;
                                                 var sMensaje: string ): Boolean;
begin
     Result := FALSE;
end;


function TBaseCliente.PuedeModificarTarjetasAnteriores: Boolean;
begin
     Result := CheckDerecho( D_ASIS_BLOQUEO_CAMBIOS, K_DERECHO_MODIFICAR_TARJETAS_ANTERIORES );
end;

function TBaseCliente.PuedeCambiarTarjeta( const dInicial, dFinal: TDate; const iEmpleado: integer; var sMensaje : string ):Boolean;
begin
     Result := PuedeModificarTarjetasAnteriores;
     if NOT Result then
     begin
          if Global.GetGlobalBooleano( K_GLOBAL_BLOQUEO_X_STATUS ) then
          begin
               //si el empleado es CERO, quiere decir que viene de un proceso
               Result := iEmpleado = 0;
               if NOT Result then
               begin
                    if ( dInicial = dFinal )   then
                       Result := PuedeCambiarTarjetaStatus( dInicial,
                                                            iEmpleado,
                                                            eStatusPeriodo( Global.GetGlobalInteger( K_LIMITE_MODIFICAR_ASISTENCIA ) ),
                                                            FALSE,
                                                            sMensaje )
                    else
                        //En teoria nunca debe de llegar a mostrarse este error.
                        sMensaje := 'Bloqueo por No se Puede Validar por Rango de Fechas';
               end
          end
          else
          begin
               Result := ZetaCommonTools.PuedeCambiarTarjeta( dInicial,
                                                              dFinal,
                                                              Global.GetGlobalDate( K_GLOBAL_FECHA_LIMITE ),
                                                              FALSE,
                                                              sMensaje  );
          end;
     end;
end;

function TBaseCliente.PuedeCambiarTarjetaDlg( Dataset: TDataset ): Boolean;
begin
     if ( DataSet <> NIL ) then
        with Dataset do
             Result := PuedeCambiarTarjetaDlg( FieldByName('AU_FECHA').AsDateTime,
                                               FieldByName('CB_CODIGO').AsInteger )
     else
         Result := FALSE;
end;

function TBaseCliente.PuedeCambiarTarjeta( Dataset: TDataset ): Boolean;
 var
    sMensaje: string;
begin
     if ( DataSet <> NIL ) then
        with Dataset do
             Result := PuedeCambiarTarjeta( Dataset, sMensaje )
     else
         Result := FALSE;
end;

function TBaseCliente.PuedeCambiarTarjeta( Dataset: TDataset; var sMensaje: string ): Boolean;
begin
     if ( DataSet <> NIL ) then
        with Dataset do
             Result := PuedeCambiarTarjeta( FieldByName('AU_FECHA').AsDateTime,
                                            FieldByName('CB_CODIGO').AsInteger,
                                            sMensaje )
     else
         Result := FALSE;
end;


function TBaseCliente.PuedeCambiarTarjeta(const dFecha: TDate; const iEmpleado: integer): Boolean;
 var
    sMensaje : string;
begin
     Result := PuedeCambiarTarjeta( dFecha, iEmpleado, sMensaje );
end;

function TBaseCliente.PuedeCambiarTarjeta( const dFecha: TDate; const iEmpleado: integer; var sMensaje : string ):Boolean;
begin
     Result := PuedeCambiarTarjeta( dFecha, dFecha, iEmpleado, sMensaje );
end;


function TBaseCliente.PuedeCambiarTarjetaDlg( const dFecha: TDate; const iEmpleado: integer ):Boolean;
begin
     Result := PuedeCambiarTarjetaDlg( dFecha, dFecha, iEmpleado );
end;

function TBaseCliente.PuedeCambiarTarjetaDlg( const dInicial, dFinal: TDate; const iEmpleado: integer = 0 ):Boolean;
 var
    sMensaje : string;
begin
     Result := PuedeCambiarTarjeta( dInicial, dFinal, iEmpleado, sMensaje );
     if not Result then
     begin
          ZetaDialogo.ZWarning( 'Mensaje', sMensaje, 0, mbYes );
     end;
end;

function TBaseCliente.ValidaTarjetaStatusDlg( const eTipo: eStatusPeriodo ): Boolean;
 var
    sMensaje: string;
begin
     Result := ValidaTarjetaStatus( eTipo, sMensaje );
     if not Result then
     begin
          ZetaDialogo.ZWarning( 'Mensaje', sMensaje, 0, mbYes );
     end;
end;

function TBaseCliente.ValidaTarjetaStatus( const eTipo: eStatusPeriodo ): Boolean;
 var
    sMensaje: string;
begin
     Result := ValidaTarjetaStatus( eTipo, sMensaje );
end;

function TBaseCliente.ValidaTarjetaStatus( const eTipo: eStatusPeriodo; var sMensaje: string ): Boolean;
begin
     if Global.GetGlobalBooleano( K_GLOBAL_BLOQUEO_X_STATUS ) then
     begin
          Result := PuedeModificarTarjetasAnteriores;
          if NOT Result then
             Result := ZetaCommonTools.ValidaStatusNomina( eTipo,
                                                           eStatusPeriodo( Global.GetGlobalInteger( K_LIMITE_MODIFICAR_ASISTENCIA ) ),
                                                           sMensaje );
     end
     else
     begin
          //Quiere decir que el bloqueo es por Fecha de Asistencia.
          Result := TRUE;
     end;
end;

function TBaseCliente.GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin
     Result := dmDiccionario.GetValorActivo(eValor);
end;

procedure TBaseCliente.GetStatusEmpleado(EmpleadoActivo: TInfoEmpleado; var Caption, Hint : string; var Color : TColor );
const
   aRecontratable: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( ZetaCommonClasses.CR_LF + 'NO RECONTRATAR', VACIO );
begin
      with EmpleadoActivo do
      begin
            if (not Global.GetGlobalBooleano(K_GLOBAL_CB_ACTIVO_AL_DIA) ) then
            begin
                 if Activo then
                 begin
                      if ( Baja = NullDateTime ) then
                      begin
                           Caption := 'Activo';
                           Hint := 'Ingres� ' + FechaCorta( Ingreso );
                           Color := clBlack;
                      end
                      else
                      begin
                           Caption := 'Reingreso';
                           Hint := 'Baja: ' + FechaCorta( Baja ) + ' Reingreso: ' + FechaCorta( Ingreso );
                           Color := clGreen;
                      end;
                 end
                 else
                 begin
                      Caption := 'Baja';
                      Hint := 'Baja desde ' + FechaCorta( Baja ) + aRecontratable[ Recontratable ];
                      Color := clRed;
                 end;
            end
            else
            begin
                 if Activo then
                 begin
                      if ( Baja = NullDateTime ) then
                      begin
                           if ( Ingreso > FechaDefault ) then
                           begin
                                Caption := 'Antes ingreso';
                                Hint := 'Ingresa ' + FechaCorta( Ingreso );
                                Color := clRed;
                           end
                           else
                           begin
                                Caption := 'Activo';
                                Hint := 'Ingres� ' + FechaCorta( Ingreso );
                                Color := clBlack;
                           end;
                      end
                      else if (Ingreso <= FechaDefault) then
                      begin
                           Caption := 'Reingreso';
                           Hint := 'Baja: ' + FechaCorta( Baja ) + ' Reingreso: ' + FechaCorta( Ingreso );
                           Color := clGreen;
                      end
                      else
                      begin
                           Caption := 'Baja';
                           Hint := 'Baja desde ' + FechaCorta( Baja )+ aRecontratable [ Recontratable ];
                           Color := clRed;
                      end;
                 end
                 else
                 begin
                      if Baja < FechaDefault then
                      begin
                           Caption := 'Baja';
                           Hint := 'Baja desde ' + FechaCorta( Baja )+ aRecontratable[ Recontratable ];
                           Color := clRed;
                      end
                      else
                      begin
                           Caption := 'Activo';
                           Hint := 'Ingres� ' + FechaCorta( Ingreso );
                           Color := clBlack;
                      end;
                 end;
            end;
      end;
end;

function TBaseCliente.GetUsaPlazas: Boolean;
begin
     Result :=Global.GetGlobalBooleano( K_GLOBAL_RH_VALIDA_ORGANIGRAMA );
end;

procedure TBaseCliente.DataModuleDestroy(Sender: TObject);
begin
     //(@am): Destruccion de listas de objetos tipo TPeriodo
     FreeAndNil(FListaTiposPeriodoConfidencialidad);
     //(@am): Destruccion de listas locales
     FreeAndNil(FArregloPeriodo);
     FreeAndNil(FArregloPeriodoConfidencial);
     FreeAndNil(FArregloTipoNomina);
     inherited;
end;

procedure TBaseCliente.cdsTPeriodosAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsTPeriodos.Data :=   Servidor.GetTipoPeriodo( Empresa );
end;

end.


