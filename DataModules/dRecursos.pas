{$HINTS OFF}
unit dRecursos;

interface

{$DEFINE MULTIPLES_ENTIDADES}
{$DEFINE CONFIDENCIALIDAD_MULTIPLE}
                                                                                                                   
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,
{$ifdef DOS_CAPAS}
  DServerRecursos,
  DServerAsistencia,
{$else}
  Recursos_TLB,
  Asistencia_TLB,
{$endif}
{$ifndef VER130}
  Variants,
{$endif}
  ZetaTipoEntidad,
  ZetaCommonLists,
  ZetaCommonClasses,
{$ifdef INTERFAZ_TRESS}
   {$ifndef POLIZA_SENDA}
   {$ifndef CAMBIO}
   DInterfase,
   {$endif}
   {$endif}
{$endif}
  ZetaFecha,
  ZGlobalTress;

type

  TOrdenKardex = (eokAscendente, eokDescendente);
  TTipoErrorEmpleado = ( teNinguno, teLlavePrimaria );

  TOtrasPer = Class(TObject)
  public
     Codigo:string;
     Monto :TPesos;
     Tipo  :eTipoOtrasPer;
     Tasa  :TPesos;
     Imss  :eIMSSOtrasPer;
     ISPT  :eISPTOtrasPer;
     Tope  :TPesos;
     Gravado :TPesos;
  end;

  TEmpleadoPercepcion = class( TObject )
   public
      Codigo   : string;
      Monto    : TPesos;
      Gravado  : TPesos;
      Imss     : eIMSSOtrasPer;
  end;

  TDatosPrestamoAnterior = record
    Referencia   : String;
    Inicio       : TDate;
    Status       : Integer;
    Monto        : TPesos;
    MontoInicial : TPesos;
    Formula      : String;
  end;

  TDatosCertificacionAnterior = record
    Fecha        : TDate;
    Aprobo       : Boolean;
    Renovar      : Smallint;
  end;



  TdmRecursos = class(TDataModule)
    cdsHisCursos: TZetaClientDataSet;
    cdsHisPagosIMSS: TZetaClientDataSet;
    cdsEmpAntesCur: TZetaClientDataSet;
    cdsEmpAntesPto: TZetaClientDataSet;
    cdsEmpFoto: TZetaClientDataSet;
    cdsEmpParientes: TZetaClientDataSet;
    cdsHisAcumulados: TZetaClientDataSet;
    cdsHisAhorros: TZetaClientDataSet;
    cdsHisCursosProg: TZetaClientDataSet;
    cdsHisIncapaci: TZetaClientDataSet;
    cdsHisNominas: TZetaClientDataSet;
    cdsHisPermiso: TZetaClientDataSet;
    cdsHisPrestamos: TZetaClientDataSet;
    cdsHisVacacion: TZetaClientDataSet;
    cdsIdentFoto: TZetaClientDataSet;
    cdsEmpPercepcion: TZetaClientDataSet;
    cdsPCarAbo: TZetaClientDataSet;
    cdsACarAbo: TZetaClientDataSet;
    cdsHisKardex: TZetaClientDataSet;
    cdsEditHisKardex: TZetaClientDataSet;
    cdsKar_Fija: TZetaClientDataSet;
    cdsEmpVacacion: TZetaClientDataSet;
    cdsGridCursos: TZetaClientDataSet;
    cdsHisTools: TZetaClientDataSet;
    cdsGridEmpTools: TZetaClientDataSet;
    cdsGridGlobalTools: TZetaClientDataSet;
    cdsGridBackTools: TZetaClientDataSet;
    cdsGridIncapaci: TZetaClientDataSet;
    cdsInfoEmp: TZetaClientDataSet;
    cdsBancaElec: TZetaClientDataSet;
    cdsInfonavit: TZetaClientDataSet;
    cdsGridPrestamos: TZetaClientDataSet;
    cdsOrganigrama: TZetaClientDataSet;
    cdsFotoOrganigrama: TZetaClientDataSet;
    cdsInfoCandidato: TZetaClientDataSet;
    cdsCursosProg: TZetaClientDataSet;
    cdsSesion: TZetaClientDataSet;
    cdsInscritos: TZetaClientDataSet;
    cdsAprobados: TZetaClientDataSet;
    cdsListaAsistencia: TZetaClientDataSet;
    cdsReservasSesion: TZetaClientDataSet;
    cdsReserva: TZetaClientDataSet;
    cdsCursosEmpleado: TZetaClientDataSet;
    cdsKarCertificaciones: TZetaClientDataSet;
    cdsPlanVacacion: TZetaClientDataSet;
    cdsInscribirAlumnos: TZetaClientDataSet;
    cdsPlazas: TZetaClientDataSet;
    cdsListaEmpPlazas: TZetaClientDataSet;
    cdsPlazasLookup: TZetaLookupDataSet;
    cdsPlazasSearch: TZetaLookupDataSet;
    cdsArbolPlazas: TZetaClientDataSet;
    cdsGridEmpCertific: TZetaClientDataSet;
    cdsGridGlobalCertific: TZetaClientDataSet;
    cdsHisCerProg: TZetaClientDataSet;
    cdsHisCreInfonavit: TZetaClientDataSet;
    cdsLookupInfonavit: TZetaClientDataSet;
    cdsTarjetasGasolina: TZetaClientDataSet;
    cdsTarjetasDespensa: TZetaClientDataSet;
    cdsDocumento: TZetaClientDataSet;
    cdsHisPensionesAlimenticias: TZetaClientDataSet;
    cdsPorcPensiones: TZetaClientDataSet;
    cdsHisSGM: TZetaClientDataSet;
    cdsSaldoVacaciones: TZetaClientDataSet;
    cdsSaldoVacacionesTotales: TZetaClientDataSet;
    cdsListaConflicto: TZetaClientDataSet;
    cdsEmpPlaza: TZetaClientDataSet;
    cdsHisCompeten: TZetaClientDataSet;
    cdsHisCompEvalua: TZetaClientDataSet;
    cdsPlanCapacitacion: TZetaClientDataSet;
    cdsMatrizHabilidades: TZetaClientDataSet;
    cdsImagenes: TZetaClientDataSet;
    cdsGetEmpleadosImagenes: TZetaClientDataSet;
    cdsEvaluacionSel: TZetaClientDataSet;
    cdsMatrizCursos: TZetaClientDataSet;
    cdsDataSet: TZetaClientDataSet;
    cdsHisAcumuladosRS: TZetaClientDataSet;
    cdsHistorialFonacot: TZetaClientDataSet;
    cdsHisVacacionSoloLectura: TZetaClientDataSet;
    Procedure cdsEvaluacionSelNewRecord(Dataset :Tdataset);
    procedure cdsHisCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisPagosIMSSAlAdquirirDatos(Sender: TObject);
    procedure cdsHisAcumuladosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCursosProgAlAdquirirDatos(Sender: TObject);
    procedure cdsHisKardexAlAdquirirDatos(Sender: TObject);
    procedure cdsHisNominasAlAdquirirDatos(Sender: TObject);
    procedure cdsIdentFotoAlAdquirirDatos(Sender: TObject);
    procedure dmRecursosCreate(Sender: TObject);
    procedure PA_FEC_NACGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure LS_MONTHGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsPlanVacacionVP_FEC_INIChange(Sender: TField);
    procedure cdsPlanVacacionVP_FEC_FINChange(Sender: TField);
    procedure cdsPlanVacacionVP_STATUSChange(Sender: TField);
    procedure cdsPlanVacacionCB_CODIGOChange(Sender: TField);
    procedure cdsPlanVacacionVP_PAGO_USChange(Sender: TField);
    procedure cdsEmpPercepcionAlAdquirirDatos(Sender: TObject);
    procedure cdsHisAcumuladosAlModificar(Sender: TObject);
    procedure cdsHisAcumuladosAlCrearCampos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsHisNominasAlModificar(Sender: TObject);
    procedure cdsHistorialAfterPost(DataSet: TDataSet);

    {$ifdef VER130}
    procedure cdsPlanVacacionReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsPlanVacacionReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}

    //HISTORIAL DE Ahorros
    procedure cdsHisAhorrosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisAhorrosAfterOpen(DataSet: TDataSet);
    procedure cdsHisAhorrosAlCrearCampos(Sender: TObject);
    procedure cdsHisAhorrosAfterCancel(DataSet: TDataSet);
    procedure cdsHisAhorrosNewRecord(DataSet: TDataSet);
    procedure cdsHisAhorrosAlModificar(Sender: TObject);
    procedure cdsHisAhorrosBeforePost(DataSet: TDataSet);
    procedure cdsHisAhorrosAlEnviarDatos(Sender: TObject);
    procedure cdsHisAhorrosPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    //Cargos y Abonos
    procedure cdsACarAboAfterDelete(DataSet: TDataSet);
    procedure cdsACarAboNewRecord(DataSet: TDataSet);
    procedure cdsCarAboBeforePost(DataSet: TDataSet);

    //HISTORIAL DE PRESTAMOS
    procedure cdsHisPrestamosAlEnviarDatos(Sender: TObject);
    procedure cdsHisPrestamosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisPrestamosAfterOpen(DataSet: TDataSet);
    procedure cdsHisPrestamosAlCrearCampos(Sender: TObject);
    procedure cdsHisPrestamosAfterCancel(DataSet: TDataSet);
    procedure cdsHisPrestamosNewRecord(DataSet: TDataSet);
    procedure cdsHisPrestamosAhorroBeforePost(DataSet: TDataSet);
    procedure cdsHisPrestamosAlModificar(Sender: TObject);
    //Cargos y Abonos
    procedure cdsPCarAboAfterDelete(DataSet: TDataSet);
    procedure cdsPCarAboNewRecord(DataSet: TDataSet);

    //HISTORIAL DE INCAPACIDADES
    procedure cdsHisIncapaciAlAdquirirDatos(Sender: TObject);
    procedure cdsHisIncapaciAlModificar(Sender: TObject);
    procedure cdsHisIncapaciAlCrearCampos(Sender: TObject);

    //Historial de Permisos
    procedure cdsHisPermisoAlCrearCampos(Sender: TObject);
    procedure cdsHisPermisoAlModificar(Sender: TObject);
    procedure cdsHisPermisoAlAdquirirDatos(Sender: TObject);
    procedure cdsHisIncapaciNewRecord(DataSet: TDataSet);
    procedure cdsHisIncapaciBeforePost(DataSet: TDataSet);
    procedure cdsHisPermisoBeforePost(DataSet: TDataSet);
    procedure cdsHisPermisoNewRecord(DataSet: TDataSet);
    procedure cdsHisAcumuladosNewRecord(DataSet: TDataSet);
    procedure cdsHisCursosAlModificar(Sender: TObject);
    procedure cdsHisCursosNewRecord(DataSet: TDataSet);
    procedure cdsHisCursosBeforePost(DataSet: TDataSet);
    procedure cdsHisCursosAlCrearCampos(Sender: TObject);

    //Historial de Vacaciones
    procedure cdsHisVacacionAlAdquirirDatos(Sender: TObject);
    procedure cdsHisVacacionAlModificar(Sender: TObject);
    procedure cdsHisVacacionAlCrearCampos(Sender: TObject);
    procedure cdsHisVacacionNewRecord(DataSet: TDataSet);
    procedure cdsHisKardexAlModificar(Sender: TObject);
    procedure cdsHisKardexAlAgregar(Sender: TObject);

    procedure cdsEmpParientesAlModificar(Sender: TObject);
    procedure cdsEmpParientesAfterPost(DataSet: TDataSet);
    procedure cdsEmpParientesNewRecord(DataSet: TDataSet);
    procedure cdsEmpAntesCurAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpAntesCurAfterPost(DataSet: TDataSet);
    procedure cdsEmpAntesCurAlModificar(Sender: TObject);
    procedure cdsEmpAntesCurBeforeInsert(DataSet: TDataSet);
    procedure cdsEmpAntesCurNewRecord(DataSet: TDataSet);
    procedure cdsEmpAntesPtoAfterPost(DataSet: TDataSet);
    procedure cdsEmpAntesPtoAlModificar(Sender: TObject);
    procedure cdsEmpAntesPtoBeforeInsert(DataSet: TDataSet);
    procedure cdsEmpAntesPtoNewRecord(DataSet: TDataSet);
    procedure cdsEmpFotoAfterPost(DataSet: TDataSet);
    procedure cdsEmpFotoAlModificar(Sender: TObject);
    procedure cdsEmpFotoBeforeInsert(DataSet: TDataSet);
    procedure cdsEmpFotoNewRecord(DataSet: TDataSet);
    procedure cdsEmpAntesPtoAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpFotoAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpParientesAlAdquirirDatos(Sender: TObject);
    procedure cdsEditHisKardexAlAdquirirDatos(Sender: TObject);
    procedure cdsEditHisKardexAlCrearCampos(Sender: TObject);
    procedure cdsEditHisKardexNewRecord(DataSet: TDataSet);
    procedure cdsHisKardexAlCrearCampos(Sender: TObject);
    procedure cdsEditHisKardexBeforePost(DataSet: TDataSet);
    procedure cdsKar_FijaAlCrearCampos(Sender: TObject);
    procedure cdsEmpPercepcionAlCrearCampos(Sender: TObject);
    procedure cdsEmpParientesAlCrearCampos(Sender: TObject);
    procedure cdsEmpAntesPtoAlCrearCampos(Sender: TObject);
    procedure cdsEmpAntesCurAlCrearCampos(Sender: TObject);
    procedure cdsHisPagosIMSSAlCrearCampos(Sender: TObject);
    procedure cdsHisCursosProgAlCrearCampos(Sender: TObject);
    procedure cdsHisNominasAlCrearCampos(Sender: TObject);
    procedure cdsEmpVacacionAlCrearCampos(Sender: TObject);
    procedure cdsACarAboAlCrearCampos(Sender: TObject);
    procedure cdsPCarAboAlCrearCampos(Sender: TObject);
    procedure cdsHisKardexBeforeDelete(DataSet: TDataSet);
    procedure cdsEditHisKardexAlEnviarDatos(Sender: TObject);
    procedure cdsKar_FijaAlAdquirirDatos(Sender: TObject);
    procedure cdsHisKardexAfterDelete(DataSet: TDataSet);
    procedure cdsHisKardexAlBorrar(Sender: TObject);
    procedure cdsHisPermisoAlEnviarDatos(Sender: TObject);
    procedure cdsHisIncapaciAlEnviarDatos(Sender: TObject);
    procedure cdsHisVacacionAlEnviarDatos(Sender: TObject);
    procedure cdsHisVacacionAlBorrar(Sender: TObject);
    procedure cdsHisPrestamosBeforeDelete(DataSet: TDataSet);
    procedure cdsHisAhorrosBeforeDelete(DataSet: TDataSet);
    procedure cdsHisVacacionBeforeDelete(DataSet: TDataSet);
    procedure cdsGridCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsGridCursosBeforeOpen(DataSet: TDataSet);
    procedure cdsGridCursosAlCrearCampos(Sender: TObject);
    procedure cdsGridCursosBeforePost(DataSet: TDataSet);
    procedure cdsGridCursosAlEnviarDatos(Sender: TObject);
    procedure cdsGridCursosAfterPost(DataSet: TDataSet);
    procedure cdsGridCursosCB_CODIGOChange(Sender: TField);
    procedure cdsGridCB_CODIGOValidate(Sender: TField);
    procedure cdsGridCursosKC_EVALUAValidate(Sender: TField);
    procedure cdsAlBorrar(Sender: TObject);
    {$ifdef VER130}
    procedure cdsGridCursosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsGridCursosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsHisIncapaciAlBorrar(Sender: TObject);
    procedure cdsHisPermisoAlBorrar(Sender: TObject);
    procedure cdsHisNominasAlEnviarDatos(Sender: TObject);
    procedure cdsHisNominasAfterDelete(DataSet: TDataSet);
    procedure cdsHisVacacionBeforePost(DataSet: TDataSet);
    procedure cdsHisToolsAlAdquirirDatos(Sender: TObject);
    //procedure cdsHisToolsAlEnviarDatos(Sender: TObject);
    procedure cdsHisToolsAfterDelete(DataSet: TDataSet);
    procedure cdsHisToolsAlCrearCampos(Sender: TObject);
    procedure cdsHisToolsNewRecord(DataSet: TDataSet);
    procedure cdsHisToolsBeforePost(DataSet: TDataSet);
    procedure cdsHisToolsAlModificar(Sender: TObject);
    procedure cdsGridEmpToolsAlAdquirirDatos(Sender: TObject);
    procedure cdsGridEmpToolsAlCrearCampos(Sender: TObject);
    procedure EmpTO_CODIGOValidate(Sender: TField);
    procedure EmpTO_CODIGOChange(Sender: TField);
    procedure TallaValidate(Sender: TField);
    procedure cdsGridEmpToolsBeforePost(DataSet: TDataSet);
    {$ifdef VER130}
    procedure cdsGridToolsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisPermisoReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisIncapaciReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsGridIncapaciReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    //procedure cdsGridBackToolsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsGridToolsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisPermisoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisIncapaciReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsGridIncapaciReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsGridToolsAlEnviarDatos(Sender: TObject);
    procedure cdsGridGlobalToolsAlAdquirirDatos(Sender: TObject);
    procedure cdsGridGlobalToolsAlCrearCampos(Sender: TObject);
    procedure ToolsCB_CODIGOChange(Sender: TField);
    procedure cdsGridBackToolsAlAdquirirDatos(Sender: TObject);
    procedure cdsGridBackToolsAlCrearCampos(Sender: TObject);
    procedure KT_ACTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsGridIncapaciAlAdquirirDatos(Sender: TObject);
    procedure cdsGridIncapaciAlCrearCampos(Sender: TObject);
    procedure IncapaciCB_CODIGOChange(Sender: TField);
    procedure IncapaciIN_TIPOValidate(Sender: TField);
    {cdsBancaElectronica}
    procedure cdsBancaElecAlAdquirirDatos(Sender: TObject);
    procedure cdsBancaElecAlCrearCampos(Sender: TObject);
    procedure cdsBancaElecCB_CODIGOChange(Sender: TField);
    procedure cdsTarjetasGasolinaCB_CODIGOChange(Sender: TField);
    procedure cdsTarjetasDespensaCB_CODIGOChange(Sender: TField);
    procedure cdsBancaElecAlEnviarDatos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsBancaElecReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisToolsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsBancaElecReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisToolsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    {cdsInfonavit}
    procedure cdsInfonavitAlAdquirirDatos(Sender: TObject);
    procedure cdsInfonavitAlCrearCampos(Sender: TObject);
    procedure cdsInfonavitCB_CODIGOChange(Sender: TField);
    procedure cdsInfonavitNewRecord(DataSet: TDataSet);
    procedure CB_INFTASAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CB_INFOLDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CB_INF_INIGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CB_INFMANTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsInfonavitAlEnviarDatos(Sender: TObject);
    procedure cdsInfonavitBeforePost(DataSet: TDataSet);
    {$ifdef VER130}
    procedure cdsInfonavitReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsInfonavitReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    { cdsGridPrestamos }
    procedure cdsGridPrestamosAlAdquirirDatos(Sender: TObject);
    procedure cdsGridPrestamosAlCrearCampos(Sender: TObject);
    procedure cdsGridPrestamosCB_CODIGOChange(Sender: TField);
    procedure cdsGridPrestamosBeforePost(DataSet: TDataSet);
    procedure cdsGridPrestamosAlEnviarDatos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsGridPrestamosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsGridPrestamosReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsGridPrestamosAfterPost(DataSet: TDataSet);
    procedure cdsCursosProgAlAdquirirDatos(Sender: TObject);
    procedure cdsCursosProgAlAgregar(Sender: TObject);
    procedure cdsCursosProgAlModificar(Sender: TObject);
    procedure cdsCursosProgNewRecord(DataSet: TDataSet);
    procedure cdsCursosProgBeforePost(DataSet: TDataSet);
    procedure cdsCursosProgAlBorrar(Sender: TObject);
    procedure cdsCursosProgAlEnviarDatos(Sender: TObject);
    procedure cdsInscritosAlCrearCampos(Sender: TObject);
    procedure cdsInscritosAlAdquirirDatos(Sender: TObject);
    procedure cdsInscritosAlModificar(Sender: TObject);
    procedure cdsInscritosNewRecord(DataSet: TDataSet);
    procedure cdsInscritosBeforePost(DataSet: TDataSet);
    procedure cdsInscritosAlEnviarDatos(Sender: TObject);
    procedure cdsAprobadosAlAdquirirDatos(Sender: TObject);
    procedure cdsAprobadosAlCrearCampos(Sender: TObject);
    procedure cdsAprobadosAlEnviarDatos(Sender: TObject);
    procedure cdsAprobadosAlModificar(Sender: TObject);
    procedure cdsAprobadosNewRecord(DataSet: TDataSet);
    procedure cdsSesionNewRecord(DataSet: TDataSet);
    procedure cdsSesionAlCrearCampos(Sender: TObject);
    procedure cdsSesionCalcFields(DataSet: TDataSet);
    procedure cdsSesionBeforePost(DataSet: TDataSet);
    procedure cdsSesionAlEnviarDatos(Sender: TObject);
    procedure cdsSesionAfterDelete(DataSet: TDataSet);
    procedure cdsSesionAlModificar(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsListaAsistenciaAlCrearCampos(Sender: TObject);
    procedure cdsReservasSesionAlAdquirirDatos(Sender: TObject);
    procedure cdsReservaAlCrearCampos(Sender: TObject);
    procedure cdsReservaCalcFields(DataSet: TDataSet);
    procedure cdsReservaNewRecord(DataSet: TDataSet);
    procedure cdsReservaBeforePost(DataSet: TDataSet);
    procedure cdsReservaAlEnviarDatos(Sender: TObject);
    procedure cdsReservaAlModificar(Sender: TObject);
    procedure cdsListaAsistenciaAlEnviarDatos(Sender: TObject);
    procedure cdsInscritosReconcileError(DataSet: TClientDataSet;E: EReconcileError; UpdateKind: TUpdateKind;var Action: TReconcileAction);
    procedure cdsReservasSesionAlCrearCampos(Sender: TObject);
    procedure cdsReservaAlBorrar(Sender: TObject);
    procedure cdsReservaBeforeDelete(DataSet: TDataSet);
    procedure cdsKarCertificacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsKarCertificacionesAlCrearCampos(Sender: TObject);
    procedure cdsKarCertificacionesAlEnviarDatos(Sender: TObject);
    procedure cdsKarCertificacionesAlModificar(Sender: TObject);
    procedure cdsKarCertificacionesNewRecord(DataSet: TDataSet);
    procedure cdsKarCertificacionesCalcFields(DataSet: TDataSet);
    procedure cdsKarCertificacionesBeforePost(DataSet: TDataSet);
    procedure cdsPlanVacacionAfterDelete(DataSet: TDataSet);
    procedure cdsPlanVacacionAlCrearCampos(Sender: TObject);
    procedure cdsPlanVacacionAlAgregar(Sender: TObject);
    procedure cdsPlanVacacionNewRecord(DataSet: TDataSet);
    procedure cdsPlanVacacionBeforePost(DataSet: TDataSet);
    procedure cdsPlanVacacionAlEnviarDatos(Sender: TObject);
    procedure cdsPlanVacacionAlModificar(Sender: TObject);
    procedure PR_MontosChange(Sender: TField);
    procedure PR_MESESChange(Sender: TField);
    procedure PR_INTERESChange(Sender: TField);
    procedure PR_PagosChange(Sender: TField);
    procedure PR_TIPOChange(Sender: TField);
    procedure PR_PagosOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsPlazasAlAdquirirDatos(Sender: TObject);
    procedure cdsPlazasAlEnviarDatos(Sender: TObject);
    procedure CrearListaPlazas;
    procedure cdsListaCB_CODIGOValidate(Sender: TField);
    procedure cdsListaCB_CODIGOChange(Sender: TField);
    procedure cdsPlazasAlModificar(Sender: TObject);
    procedure cdsPlazasAlCrearCampos(Sender: TObject);
    procedure cdsPlazasBeforePost(DataSet: TDataSet);
    procedure cdsPlazasLookupLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsListaBeforeEdit(DataSet: TDataSet);
    procedure cdsPlazasAfterDelete(DataSet: TDataSet);
    procedure cdsKarCertificacionesAfterDelete(DataSet: TDataSet);
    procedure cdsPlazasLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsPlazasBeforeEdit(DataSet: TDataSet);
    procedure cdsEditHisKardexBeforeEdit(DataSet: TDataSet);
    procedure cdsPlazasLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsArbolPlazasAlAdquirirDatos(Sender: TObject);
    {cdsGridEmpCertific}
    procedure cdsGridEmpCertificAlAdquirirDatos(Sender: TObject);
    procedure cdsGridEmpCertificAlCrearCampos(Sender: TObject);
    procedure cdsGridCertificAlEnviarDatos(Sender: TObject);
    procedure cdsGridEmpCertificBeforePost(DataSet: TDataSet);
    procedure cdsGridCertificReconcileError(
      DataSet: TCustomClientDataSet; E: EReconcileError;
      UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure EmpCI_CODIGOChange(Sender: TField);
    procedure EmpCI_CODIGOValidate(Sender: TField);
    procedure KI_APROBOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    {cdsGridGlobalCertific}
    procedure cdsGridGlobalCertificAlAdquirirDatos(Sender: TObject);
    procedure cdsGridGlobalCertificAlCrearCampos(Sender: TObject);
    procedure cdsGridGlobalCertificBeforePost(DataSet: TDataSet);
    procedure cdsGridGlobalCB_CODIGOChange(Sender: TField);
    procedure cdsGridCertificAfterPost(DataSet: TDataSet);
    procedure cdsHisCerProgAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCerProgAlCrearCampos(Sender: TObject);
    procedure cdsHisIncapaciBeforeEdit(DataSet: TDataSet);
    {cdsHisCreInfonavit}
    procedure cdsHisCreInfonavitAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCreInfonavitAlCrearCampos(Sender: TObject);
    procedure cdsHisCreInfonavitAlModificar(Sender: TObject);
    procedure cdsHisCreInfonavitAlEnviarDatos(Sender: TObject);
    procedure cdsHisCreInfonavitNewRecord(DataSet: TDataSet);
    procedure cdsHisCreInfonavitBeforePost(DataSet: TDataSet);
    procedure cdsHisCreInfonavitAlBorrar(Sender: TObject);
    procedure cdsHisCreInfonavitBeforeDelete(DataSet: TDataSet);
    procedure cdsHisPrestamosReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsCursosProgAlCrearCampos(Sender: TObject);
    procedure cdsTarjetasDespensaAlAdquirirDatos(Sender: TObject);
    procedure cdsTarjetasDespensaAlCrearCampos(Sender: TObject);
    procedure cdsTarjetasDespensaAlEnviarDatos(Sender: TObject);
    procedure cdsTarjetasGasolinaAlCrearCampos(Sender: TObject);
    procedure cdsTarjetasGasolinaAlAdquirirDatos(Sender: TObject);
    procedure cdsTarjetasGasolinaAlEnviarDatos(Sender: TObject);

    // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
    procedure cdsDocumentoAlAdquirirDatos(Sender: TObject);
    procedure cdsDocumentoAlCrearCampos(Sender: TObject);
    procedure cdsDocumentoAlModificar(Sender: TObject);
    procedure cdsDocumentoAlEnviarDatos(Sender: TObject);
    procedure cdsDocumentoAlBorrar(Sender: TObject);
    procedure cdsDocumentoAlAgregar(Sender: TObject);
    procedure cdsDocumentoBeforePost(DataSet: TDataSet);
    procedure cdsDocumentoDO_EXT_OnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsDocumentoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsSaldoVacacionesAlCrearCampos(Sender: TObject);
    procedure cdsSaldoVacacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsSaldoVacacionesTotalesAlCrearCampos(Sender: TObject);
    procedure cdsEmpVacacionesCB_TABLASSOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsHisPensionesAlimenticiasAlAdquirirDatos(Sender: TObject);
    procedure cdsHisPensionesAlimenticiasAlCrearCampos(Sender: TObject);
    procedure cdsHisPensionesAlimenticiasAlEnviarDatos(Sender: TObject);
    procedure cdsHisPensionesAlimenticiasAlModificar(Sender: TObject);
    procedure cdsHisPensionesAlimenticiasAfterOpen(DataSet: TDataSet);
    procedure cdsHisPensionesAlimenticiasNewRecord(DataSet: TDataSet);
    procedure cdsHisPensionesAlimenticiasBeforePost(DataSet: TDataSet);
    procedure cdsHisPensionesAlimenticiasAfterCancel(DataSet: TDataSet);
    procedure cdsPorcPensionesAfterDelete(DataSet: TDataSet);
    procedure cdsPorcPensionesAlCrearCampos(Sender: TObject);
    procedure cdsPorcPensionesBeforePost(DataSet: TDataSet);
    procedure cdsPorcPensionesNewRecord(DataSet: TDataSet);
    procedure PorcPensionesTP_CODIGOValidate(Sender: TField);
    procedure cdsHisPensionesAlimenticiasBeforeDelete(DataSet: TDataSet);
    procedure cdsHisPensionesAlimenticiasAfterDelete(DataSet: TDataSet);
    procedure cdsHisPensionesAlimenticiasBeforeInsert(DataSet: TDataSet);
    procedure cdsHisSGMAlAdquirirDatos(Sender: TObject);
    procedure cdsHisSGMAlCrearCampos(Sender: TObject);
    procedure cdsHisSGMNewRecord(DataSet: TDataSet);
    procedure cdsHisSGMAlEnviarDatos(Sender: TObject);
    procedure cdsHisSGMAlModificar(Sender: TObject);
    procedure cdsEmpParientesBeforePost(DataSet: TDataSet);
    procedure cdsHisSGMAfterDelete(DataSet: TDataSet);
    procedure cdsHisSGMBeforePost(DataSet: TDataSet);
    procedure cdsHisSGMAfterEdit(DataSet: TDataSet);
    procedure cdsEmpPlazaAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCompetenAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCompetenAlCrearCampos(Sender: TObject);
    procedure cdsHisCompetenAlModificar(Sender: TObject);
    procedure cdsHisCompetenNewRecord(DataSet: TDataSet);
    procedure cdsHisCompetenBeforePost(DataSet: TDataSet);
    procedure cdsHisCompetenAlEnviarDatos(Sender: TObject);
    procedure cdsHisCompetenAfterDelete(DataSet: TDataSet);
    procedure cdsHisCompEvaluaAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCompEvaluaAlCrearCampos(Sender: TObject);
    procedure cdsHisCompEvaluaAlModificar(Sender: TObject);
    procedure cdsHisCompEvaluaNewRecord(DataSet: TDataSet);
    procedure cdsHisCompEvaluaBeforePost(DataSet: TDataSet);
    procedure cdsHisCompEvaluaAlEnviarDatos(Sender: TObject);
    procedure cdsHisCompEvaluaAfterDelete(DataSet: TDataSet);
    procedure cdsPlanCapacitacionAlAdquirirDatos(Sender: TObject);
    procedure cdsPlanCapacitacionAlCrearCampos(Sender: TObject);
    procedure cdsMatrizHabilidadesAlAdquirirDatos(Sender: TObject);
    procedure cdsMatrizHabilidadesAlCrearCampos(Sender: TObject);
    procedure cdsHisCompEvaluaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure validacionTipoNominaChange(Sender: TField);
    procedure cdsGetEmpleadosImagenesAlAdquirirDatos(Sender: TObject);
    procedure cdsEvaluacionSelAlCrearCampos(Sender: TObject);
    procedure cdsInfoCandidatoAlCrearCampos(Sender: TObject);
    procedure cdsMatrizCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsMatrizCursosAlCrearCampos(Sender: TObject);
    {*** US 13904: Es necesario cambiar el proceso interno de Calculo De Acumulados en procesos de Afectar, Desafectar Nomina,y Rec�lculo de Acumulados ***}
    procedure cdsHisAcumuladosRSNewRecord(DataSet: TDataSet);
    procedure cdsHisAcumuladosRSAfterPost(DataSet: TDataSet);
    procedure cdsHisAcumuladosRSAlAdquirirDatos(Sender: TObject);
    procedure cdsHisAcumuladosRSAlCrearCampos(Sender: TObject);
    procedure cdsHisAcumuladosRSAlModificar(Sender: TObject);
    procedure cdsHisAcumuladosRSReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);    
    function  HandleReconcileErrorAcumulados( DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError ): TReconcileAction;
    procedure OnChangeAcumuladosRS(Sender:TField);
    procedure cdsHisAcumuladosRSBeforePost(DataSet: TDataSet);
    procedure cdsHistorialFonacotAlAdquirirDatos(Sender: TObject);
    procedure cdsHistorialFonacotAlCrearCampos(Sender: TObject);
    procedure PF_MESGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsHisVacacionSoloLecturaAlCrearCampos(Sender: TObject);
    procedure cdsHisVacacionSoloLecturaAlEnviarDatos(Sender: TObject);
    procedure cdsHisVacacionSoloLecturaAlBorrar(Sender: TObject);
  private
    FEvaluarMatriz:Boolean;
    FAgregandoMatriz:Boolean;
    FOrdenKardex: TOrdenKardex;
    FOrdenPrestamo: TOrdenKardex;
    AgregandoKardex, FRefrescaHisKardex, FCambiosMultiples, FCambioVacaciones, FDesconectaVaca : Boolean;
    FVacaCalculaDiasGozados: Boolean;
    FUltimoAntesCur : Integer;
    FUltimoAntesPto : Integer;
    FTieneFoto, FCancelaAjuste : Boolean;
    FSaldosVaca : OleVariant;
    FListaPercepFijas : String;
    FOtrosDatos : Variant;
    FFechaReingreso : TDate;
    FTabKardex : Byte;
    FUltimaEval: Double;
    FSumaPagosFonacot : Double;
    FSumaPagosRetencion : Double;
    FTipoErrorEmpleado : TTipoErrorEmpleado;
    FEmpleadoTools : TNumEmp;
    FOperacion : eOperacionConflicto;
    FDatosPrestamoAnterior : TDatosPrestamoAnterior;
    FFolioSesion: Integer;
//  FPuestoPlaza: string;
    FFiltroEmpleadosPlaza: String;
    FFiltroReportaAPlaza: String;
    FPlazaPatron: string;
    FPlazaArbol: Integer;
    FFechaAntiguedad: TDate;
    FFechaIngreso: TDate;
    FDatosCertificacionAnterior : TDatosCertificacionAnterior;
    cdsIncapacidades : TZetaClientDataSet;
    FValidarReglas : Boolean;
    FMensajePrestamo :WideString;
    FForzarGrabarPrestamos:Boolean;
    {$ifdef FAPSA}
    FEsPrestamoDias: Boolean;
    {$endif}
    FMaxOrdenPensiones: Integer;
    FOrdenABorrar :Integer;
    FFiltroMatHabilidades:TZetaParams;
    FFiltroMatCursos: TZetaParams;
    {*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
    FRazonSocialAcumulados: String;
{$ifdef DOS_CAPAS}
    function GetServerRecursos : TdmServerRecursos;
    property ServerRecursos :TdmServerRecursos read GetServerRecursos;
{$else}
    FServidor : IdmServerRecursosDisp;
    function GetServerRecursos : IdmServerRecursosDisp ;
    property ServerRecursos : IdmServerRecursosDisp read GetServerRecursos;
{$endif}
    function GetcdsInfo(const sTipo: String): variant;
    function ValidaBajaEmpleado( const sTipo: String ): Boolean;
    function AbreGridInfoAlta: Boolean;
    {$ifdef VER130}
    function EvaluaToolsError( DataSet: TClientDataSet; E: EReconcileError; const ActionDef: TReconcileAction ): TReconcileAction;
    {$else}
    function EvaluaToolsError( DataSet: TCustomClientDataSet; E: EReconcileError; const ActionDef: TReconcileAction ): TReconcileAction;
    {$endif}
    function GetCantidadXMes: integer;
    function ExisteEnListaEmpleados( const iEmpleado: Integer ): Boolean;
    procedure ValidaCargoAbono(Sender : TField; const sCampo: string);
    procedure OnChangeAbono(Sender: TField);
    procedure OnChangeCargo(Sender: TField);
    procedure OnChangeCB_PLAZA(Sender: TField);
    procedure GetSaldos( DataSet : TDataSet; const sCargo, sAbono : string; var dCargo, dAbono : Real );
    procedure OnIN_FEC_INIChange(Sender: TField);
    //procedure OnIN_DIASSUBChange(Sender: TField);
    procedure OnIN_DIASChange(Sender: TField);
    procedure OnIN_SUA_INIChange(Sender: TField);
    procedure OnPM_FEC_INIChange(Sender: TField);
    procedure OnPM_FEC_FINChange(Sender: TField);
    procedure OnVA_FEC_INIChange(Sender: TField);
    procedure OnVA_FEC_FINChange(Sender: TField);
    procedure OnVA_PAGOChange(Sender: TField);
    procedure OnVA_GOZOChange(Sender: TField);
    procedure OnVA_TIPOChange(Sender: TField);
    procedure OnVA_OTROSChange(Sender: TField);
    procedure OnCI_CODIGOChange(Sender: TField);
    procedure OnKI_TIPOChange(Sender: TField);
    procedure RecalculaMontoVaca;
    procedure GetSaldosVacacion;
    procedure OnChangeCB_TIPO(Sender: TField);
    procedure OnChangeCB_FECHA(Sender: TField);
    procedure LimpiaFactorIntegracion(Sender: TField);
    procedure AgregaPercepcion(DataSet: TClientDataSet; Lista: TStrings);
    procedure PresentaAltaKardex(sTipo: String);
    procedure SetOrdenKardex( const eOrden : TOrdenKardex );
    procedure SetOrdenPrestamo( const eOrden : TOrdenKardex );
    procedure NO_USR_PAGGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure OnChangeAcumulados(Sender: TField);
    procedure OnKC_FEC_TOMChange(Sender: TField);
    procedure DepuraCuentas(dsDataSet:TZetaClientDataSet;sCampo,sCampoOld:string);
    procedure DepuraInfonavit;
    procedure SetStatusPrestamo( DataSet: TDataSet );
    Procedure TraspasaInfoCandidato;
    procedure TransfiereCampoAdicional( Fuente, Destino: TDataset );
    procedure ObtieneCursosIndividuales;

    procedure cdsInscritosCB_CODIGOChange(Sender: TField);
    procedure cdsAprobadosCB_CODIGOChange(Sender: TField);
    procedure cdsListaAsistenciaCB_CODIGOChange(Sender: TField);
    procedure CS_ASISTIOOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure EmpleadoPlazasOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
//    procedure CB_CODIGOOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);

    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SESIONCU_CODIGOChange(Sender: TField);
    procedure CU_CODIGOChange(Sender: TField);
    procedure CONFLICTOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ASISTENCIAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure RV_FEC_INIOnChange(Sender : TField );
    procedure CB_CODIGOOnValidate(Sender: TField);
    procedure ObtieneCursoTomadoPorEmpleado( const iEmpleado: Integer );
    Procedure cdsPlanVacacionRecalculaDias;
    procedure RecalculaDiasSaldos;
//    procedure CopiaPlazaADataset(Dataset:TDataSet);
    procedure RevisaUltimaPlaza;
    procedure InitGridCertific;
    procedure DefaultGridCertific( DataSet:TZetaClientDataSet );
    procedure OnPM_CLASIFIChange(Sender: TField);
    procedure CB_INF_ANT_Change( Sender: TField );
    procedure SaldaVacacionesPendientes;
    procedure PA_FOLIO_OnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure OnChangeDatosPariente(Sender: TField);

    {$ifdef INTERFAZ_SAP}
    procedure DataBaseError( sMensaje: String );
    {$endif}
    {$ifdef FAPSA}
    procedure ObtenDiasPorSalario;
    {$endif}
    function ImagenAlCrearCampos(const sFileName: String; sTipo: String): Boolean;
    function CargaGridImagen(const sFileName: String; sTipo, sRuta, sObserva: String): Boolean;
  public
    FParametrosSesion: TZetaParams;
    FParametrosReservacion: TZetaParams;
    FFiltroEmpleado:string;
    FUltimoCurso: string;
    FSesionCurso:string;
    FIntercambioPlaza:Boolean;
    FCambioMatrizCursos: Boolean;
    property OrdenKardex: TOrdenKardex read FOrdenKardex write SetOrdenKardex;
    property OrdenPrestamo: TOrdenKardex read FOrdenPrestamo write SetOrdenPrestamo;
    property EvaluarMatriz: Boolean read FEvaluarMatriz write FEvaluarMatriz;
    property AgregandoMatriz: Boolean read FAgregandoMatriz write FAgregandoMatriz;
    property ListaPercepFijas: String read FListaPercepFijas write FListaPercepFijas;
    property OtrosDatos : Variant read FOtrosDatos write FOtrosDatos;
    property RefrescaHisKardex : Boolean read FRefrescaHisKardex write FRefrescaHisKardex;
    property CambioVacaciones : Boolean read FCambioVacaciones write FCambioVacaciones;
    property CambiosMultiples: Boolean read FCambiosMultiples write FCambiosMultiples;
    property TabKardex: Byte read FTabKardex write FTabKardex;
    property DesconectaVaca: Boolean read FDesconectaVaca write FDesconectaVaca;
    property TipoErrorEmpleado: TTipoErrorEmpleado read FTipoErrorEmpleado write FTipoErrorEmpleado;
    property EmpleadoTools: TNumEmp read FEmpleadoTools write FEmpleadoTools;
    property ParametrosSesion: TZetaParams read FParametrosSesion write FParametrosSesion;
    property ParametrosReservacion: TZetaParams read FParametrosReservacion write FParametrosReservacion;
//    property PuestoPlaza: string read FPuestoPlaza write FPuestoPlaza;
    property FiltroEmpleadosPlaza: String read FFiltroEmpleadosPlaza write FFiltroEmpleadosPlaza;
    property FiltroReportaAPlaza: string read FFiltroReportaAPlaza write FFiltroReportaAPlaza;
    property PlazaArbol: Integer read FPlazaArbol write FPlazaArbol;
    property ValidarReglas: Boolean read FValidarReglas write FValidarReglas;
    property ForzarGrabarPrestamos: Boolean read FForzarGrabarPrestamos write FForzarGrabarPrestamos;
    property FiltroMatHabilidades: TZetaParams read FFiltroMatHabilidades write FFiltroMatHabilidades;
    property FiltroMatCursos: TZetaParams read FFiltroMatCursos write FFiltroMatCursos;
    property CambioMatrizCursos: Boolean read FCambioMatrizCursos write FCambioMatrizCursos;
    property SumaPagosFonacot: Double read FSumaPagosFonacot write FSumaPagosFonacot;
    property SumaPagosRetencion: Double read FSumaPagosRetencion write FSumaPagosRetencion;
    {*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
    property RazonSocialAcumulados: String read FRazonSocialAcumulados write FRazonSocialAcumulados;
    {$ifdef FAPSA}
    property EsPrestamoDias: Boolean read FEsPrestamoDias write FEsPrestamoDias;
    {$endif}

    { cdsEmpleado }
    function ProximoEmpleado: Integer;
    function CalculaVencimiento( dFecha : TDateTime; iDuracion : Integer ) : TDateTime;
    function KardexBuscaUltimo( sTipo: String ): Boolean;
    function GetStatus(iEmpleado: integer; dFecha: TDate): eStatusEmpleado;
    function RevisaInformacionParams( Parametros: TZetaParams ): Boolean;
    function cdsDatosEmpleado: TZetaClientDataSet;
    function GetDatosClasificacion(const iEmpleado: TNumEmp; const dValor: TDate): TDatosClasificacion;
    function GetDatosSalario(const iEmpleado: TNumEmp; const dInicial, dFinal: TDate): TDatosSalario;
    function CalculaRangoSalarial(Rango: TPesos; oLista: TStrings; const dSalario: TPesos): TPesos;
    function GetSalClasifi(const sClasifi: String): TPesos;
    function DerechoAlta(var sMensaje: String): Boolean;
    function DerechoBaja(var sMensaje: String): Boolean;
    function GetFiltroIncapacidad: String;
    //function CheckBajaEmpleado( const sAccion: String; var sMensaje: String ): Boolean;
    //function CheckEmpleadoActivoKardex( const sTipo: String; const lActivo: Boolean ): Boolean;
    function EsKardexRequiereActivo( const sTipo: String ): Boolean;
    function CheckEmpleadoActivoBaja: Boolean;
    function ShowAdvertenciaBaja( const iEmpleado: Integer; const sNombre: String; const dBaja: TDate ): Boolean;
    function ChecaAcumulados: Boolean;
    function ValidaVacantes( const sPuesto: String ): Boolean;
    function GetPlazasOcupadas( const sPuesto: string ): integer;
    function ValidaSalario( const rOldSalario, rNewSalario: TPesos ): Boolean;
    function AplicaFiltrosKardex(const sFiltros: String): Integer;
    //function AplicaFiltrosPrestamo(const sFiltros: String): Integer;
    function ChecaFolioSesion: Boolean;
    function VerificaPrerequisitos: Boolean;
    function PuedeVerNeto: Boolean;
    function GetMaxOrdenPlazas(const sPuesto:string):Integer;
    function FiltroPlazaVacantes(const sPuesto: string = VACIO; const iPlaza:integer = 0): string;
    function CambiarTitularPlaza( const iTitularIntercambio, iPlazaIntercambio: Integer; const dFecha: TDate; const sObserva: String ): Boolean;
    function CambiaCodigoReporta( const iReporta: Integer ): Boolean;
    function BorrarPlazas( const sPlazas: String; var iCuantas: Integer ): Boolean;
    function GetPlazasArbol( const iPlaza: Integer ): Boolean;
{$ifdef COVIDIEN_INTERFAZ_HR}
    function BuscarIncapacidadAnterior( const FechaInicial:TDate; const sTipo :string ):Boolean;
{$endif}
    // (JB) Se genera metodo para subir archivo de documentacion de Expediente INFONAVIT - TASK 951 - CR 1842
    function CargaDocumento( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
    function ValidacionForzadaCuenta( const sTitulo, sBanca , sCampo : String): Boolean;

	// (JB) Anexar al expediente del trabajador documentos CR1880 T1107
    function CargaDocumentoExpediente( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
    procedure AbreDocumentoExpediente;
    procedure AbreDocumento;
    procedure BorraDocumento;
    {$ifdef ICUMEDICAL_CURSOS}
    function CargaDocumentoICU( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
    procedure AbreDocumentoICU;
    procedure BorraDocumentoICU;
    {$endif}
    {$ifdef DENSO}
    function  GetSugiereNumeroEmpleado( DataSet: TDataSet ) : Integer; { DENSO }
    {$endif}
    procedure InitDatosKardexInfonavit;
    procedure ModificaDigitoVerificador( sSegSoc: String );
    procedure CalculaRFC;
    procedure CalculaCURP;
    procedure ObtenerFotografia;
    procedure AplicaKardexReingreso(const dFecReingreso: TDate );
    {RECURSOS HUMANOS}
    procedure LlenaOtrasPer(Lista: TStrings);
    procedure LlenaListaTabulador( Lista: TStrings ; sClasifi : String; var dSalario : TPesos );
    procedure SetNuevoKardex( const sTipo: String );
    procedure AgregaKardex( sTipo: String );
    procedure AgregaKardexMultiple;
    procedure RefrescaKardex(sTipo: String);
    procedure ProcesaReingreso;
    procedure SetDatosReingreso(dFecha: TDate);
    procedure AgregaCierreVacacion;
    procedure GrabaEmpleado;
    procedure EditarGridCursos;
    procedure ValidaBancaElectronica( const sTitulo, sBanca,sCampo : String);
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure EditarGridEmpTools;
    procedure EditarGridGlobalTools;
    procedure EditarGridRegresarTools;
    procedure ConectarKardexLookups;
    procedure EditarGridIncapaci;
    procedure RevisaTools( const lShowGrid: Boolean = TRUE );
    procedure EditarGridBancaElectronica;
    procedure EditarGridTarjetasGasolina;
    procedure EditarGridTarjetasDespensa;
    procedure EditarGridInfonavit;
    procedure EditarGridPrestamos;
    procedure RecalculaVacaDiasGozados;
    procedure RecalculaVacaFechaRegreso;
    procedure SetSaldosVacaciones( const lInfoAniversario: Boolean; var rGozo, rPago, rPrima: TPesos );
    procedure LlenaGridOrganigrama(const iPlaza: Integer );
    procedure GetOrganigramaFoto( const iEmpleado: integer );
    procedure BuscaCandidato( Parametros: TZetaParams );
    procedure AgregarFotoSeleccion( const iFolio: Integer );
    procedure GetPrestamos( cdsPrestamos: TZetaClientDataset; const iEmpleado: integer );
    procedure ObtieneSesiones;
    procedure LlenaReservaCombo( oLista : TStrings );
    procedure GetListaAsistencia( const iReserva : integer; const lTraerInscritos: Boolean = FALSE);
    procedure ObtieneReservaciones;
    procedure ObtieneReservasSesion( const iSesion: Integer );
    procedure EditarInscripciones;
    procedure EditarAprobados;
    procedure EditarListaAsistencia;
    procedure EditarGridReservacionesSesion;
    procedure EditarEvaluaciones;
    procedure AgregaCertificacion;
    procedure RefrescarPlanVacacion(Parametros : TZetaParams);
    procedure SetAutorizarPlanVacacion( const tipoAutorizacion: eStatusPlanVacacion; const lTodos: Boolean );
    procedure MostrarEmpleadosAInscribir (Parametros : TZetaParams);
    procedure AgregarAlumnos ( const lTruncar : boolean);
    procedure ModificarPlaza( const FechaModificacion:TDateTime; const sObserva: String = VACIO );
    procedure AgregaCambioSalarioPlaza;
    procedure LlenaFiltroEmpleados( DataSet: TDataSet );
    procedure RefrescaPlaza( const iPlaza: Integer; const lFoto: Boolean );
    procedure EditarGridEmpCertific;
    procedure EditarGridGlobalCertific;
    procedure RefrescaTabuladorPlaza;
    procedure RevisaUsuario;
    procedure ReactivarUsuario(const lMostrarEdicion:Boolean);
    procedure AgregarCertificacionesProgramadas(const Certificacion:string);
    function  ValidarMotivoPermisos(const sCodigo :string; const iTipo:Integer ):Boolean;
    procedure AsignarDataTemporal;
    procedure AsignaDefaultsInfonavit( const eTMovimiento: eTipoMovInfonavit );
    procedure GetSaldoVacacionBaja;
    procedure BorrarFiniquito;
    function PuedePagarVacaciones(const Periodo:Integer) : Boolean;
    function GetMaxOrdenPolizas(const Poliza,Referencia:string):Integer;
    function TienePlazaAsignada(const iEmpleado:Integer):Boolean;
    function CargaGridImagenesImportar( Parametros: TZetaParams; const sFileSpec: String; lConservarImagen : Boolean = FALSE ): Boolean;
    {$IFNDEF SUPERVISORES}
    procedure CambioMasivoTurnoGetLista(Parametros: TZetaParams);
    function CambioMasivoTurno(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    {$ENDIF}
    function EsPrestamoFonacot( const sTipo: String ): Boolean;
    {$ifdef INGREDION_INTERFAZ}
    procedure RegistroAhorrosAutomaticos(iTipoRegistro: Integer);
    {$endif}
    //US 17705: INTERMEX - Al momento de capturar vacaciones deja el dato de d�as de prima vacacional en cero si solo se modifica la fecha de inicio y ya no se mueve la fecha de regreso.
    procedure SetDiasPrimasVacaciones( oFechaInicio: TZetaDBFecha; rPrima: TPesos );
  end;

const
     K_ORDEN_ASC = 0;
     K_ORDEN_DESC = 1;

     K_TAB_CONTRATA = 0;
     K_TAB_AREA = 1;
     K_TAB_SALARIOS = 2;

     K_T_MULTIPLE = 'MULTIP';

     K_MASCARA_SIN_CEROS = '#,0.00;-#,0.00;#';

     K_DERECHO_VACANTES = K_DERECHO_ALTA;
     K_DERECHO_CAMBIOS_PUESTO = K_DERECHO_BAJA;

     K_TITLE_ADVERTENCIA = 'Advertencia';

     K_SIN_PLAZA_ASIGNADA = 'CB_PLAZA = 0';

     K_ESPACIO = ' ';
var
    dmRecursos: TdmRecursos;

implementation

uses DCliente,
	 DCatalogos, 
	 DTablas,
     DSistema, 
	 DGlobal, {$ifndef CAJAAHORRO}DAsistencia, {$endif} FToolsRH, ZetaDataSetTools,
     ZetaDialogo, 
	 ZetaCommonTools,
	 ZetaMsgDlg,
	 ZReconcile,
	 ZetaClientTools,
     FTressShell,
	 ZBaseGridEdicion_DevEx,
	 ZBaseEdicion_DevEx,
	 ZAccesosMgr,
	 ZAccesosTress,
     FEditHisAcumulados_DevEx,
         FEditHisNominas_DevEx,
     FEditHisIncapaci_DevEx,
	 FeditHisCurso_DevEx,
	 FEditHisVacacion_DevEx,
     FEditHisVacacionSoloLectura_DevEx,
   FEditHisCierre_DevEx,
	 FEditHisPermiso_DevEx,
     FEditEmpPariente_DevEx,
	 FEditEmpAntesCur_DevEx,
	 FEditEmpAntesPto_DevEx,
     {$IFDEF TRESS_DELPHIXE5_UP}
     FEditEmpFoto_DevEx,
     FGridValidaInfo_DevEx,
     FCapturarFoto_DevEx,
     {$ELSE}
     ZBaseEdicion,
     FEditEmpFoto,
     FGridValidaInfo,
     FCapturarFoto,
     {$ENDIF}
	 FKardexAlta_DevEx,
     FKardexReingreso_DevEx,
	 FKardexCambio_DevEx,
	 FKardexArea_DevEx,
	 FKardexPuesto_DevEx,
	 FKardexRenova_DevEx,
     FKardexTurno_DevEx,
	 FKardexBaja_DevEx,
	 FKardexGral_DevEx,
	 FKardexPresta_DevEx,
   FKardexVarios_DevEx,
   FGridCursos_DevEx,
   FEditHisTools_DevEx,
   FGridEmpTools_DevEx,
   FGridGlobalTools_DevEx,
   FGridRegresarTools_DevEx,
	 FAutoClasses,
     FGridIncapaci_DevEx,
   FGridBancaElec_DevEx,
      //**	 FGridCreditoInfonavit,
     FGridPrestamos_DevEx,
	 FCandidatosGridShow_DevEx,
	 ZBaseGridShow_DevEx,
	 FEditCursoProg_DEvEx,
     FEditReservaAula_DevEx,
	 FInscritos_DevEx,
	 FEditInscrito_DevEx,
	 FEditAprobados_DevEx,
	 FEditSesion_DevEx,
	 FAprobados_DevEx,
     FListaAsistencia_DevEx,
	 FGridReservacionesSesion_DevEx,
	 FEvaluaciones_DevEx,
	 FEditKardexCertificaciones_DevEx,
     FRegPlanVacacion_DevEx,
	 FEditPLanVacacion_DevEx,
	 DBaseGlobal,
	 ZetaBuscaPlaza_DevEx,
         FEditPlaza_DevEx,
	 FKardexPlaza_DevEx,
	 FKardexTipoNomina_DevEx,
     FGridEmpCertific_DevEx,
   FGridGlobalCertific_DevEx,
	 FCurDlgAsis_DevEx,
	 //FToolsFoto,
	 FEditHisCreInfonavit_DevEx,
	 FShowAdvVacaPendientes_DevEx,
	 FSaldaVacaciones_DevEx,
   FGridCuentasDespensa_DevEx,
   FGridCuentasGasolina_DevEx,
	 DNomina, 
	 ZetaFilesTools,
	 FEditEmpDocumento_DevEx,
	 ZToolsPE,
	 FEditHisPensionesAlimenticias_DevEx,
     DBasicoCliente,
	 FEditHisSegurosGastosMedicos_DevEx,
	 ZetaServerTools,
	 DBaseCliente,
 	 FEditHisCompeten_DevEx,
	 FEditHisEvaluaCompeten_DevEx,
   {*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
   FEditHisAcumulados_RS_DevEx,
     FEditHisAhorros_DevEx, FEditHisPrestamos_DevEx
     {$IFNDEF SUPERVISORES},DProcesos{$ENDIF};

{$R *.DFM}


{ *********** TdmRecursos ********** }

procedure TdmRecursos.dmRecursosCreate(Sender: TObject);
begin
     FUltimoCurso := VACIO;
     FPlazaPatron := VACIO;
     FSesionCurso := VACIO;
     FPlazaArbol  := 0;
     //SetOrdenKardex( eokAscendente ); //Realmente no se necesitaba, se quita porque estorba para el gridmode
     //SetOrdenPrestamo( eokAscendente ); //Realmente no se necesitaba, se quita porque estorba para el gridmode
     FRefrescaHisKardex:= FALSE;
     AgregandoKardex:= FALSE;
     FListaPercepFijas:= VACIO;
     FCambiosMultiples:= FALSE;
     FTabKardex:= K_TAB_CONTRATA;
     FSaldosVaca := NULL;
     FDesconectaVaca:= FALSE;
     FVacaCalculaDiasGozados := TRUE;
     FParametrosSesion := TZetaParams.Create;
     FParametrosReservacion := TZetaParams.Create;
     FValidarReglas := False;
     FIntercambioPlaza := False;
     FiltroMatHabilidades := TZetaParams.Create;
     {*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
     FRazonSocialAcumulados := VACIO;
     FiltroMatCursos := TZetaParams.Create;
end;

procedure TdmRecursos.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FParametrosSesion );
     FreeAndNil( FParametrosReservacion );
     FreeAndNil( FFiltroMatHabilidades );
     FreeAndNil( FFiltroMatCursos );
end;

{$ifdef DOS_CAPAS}
function TdmRecursos.GetServerRecursos: TdmServerRecursos;
begin
     Result:= DCliente.dmCliente.ServerRecursos;
end;
{$else}
function TdmRecursos.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidor ) );
end;
{$endif}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmRecursos.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmRecursos.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enEmpleado , Entidades ) or ( Estado = stEmpleado ) then
     {$else}
     if ( enEmpleado in Entidades ) or ( Estado = stEmpleado ) then
     {$endif}
     begin
          cdsEmpPercepcion.SetDataChange;
          cdsHisVacacion.SetDataChange;
          if ( Estado = stEmpleado ) then
          begin
               cdsEmpAntesCur.SetDataChange;
               cdsEmpAntesPto.SetDataChange;
               cdsEmpFoto.SetDataChange;
               cdsIdentFoto.SetDataChange;
               cdsEmpParientes.SetDataChange;
               cdsHisIncapaci.SetDataChange;
               cdsHisAcumulados.SetDataChange;
               cdsHisCursos.SetDataChange;
               cdsHisPagosIMSS.SetDataChange;
               cdsHisCursosProg.SetDataChange;
               cdsHisNominas.SetDataChange;
               cdsHisPermiso.SetDataChange;
               cdsHisPrestamos.SetDataChange;
               cdsHisAhorros.SetDataChange;
               cdsHisKardex.SetDataChange;
               cdsHisTools.SetDataChange;
               cdsKarCertificaciones.SetDataChange;
               cdsHisCreInfonavit.SetDataChange;
               cdsHisCerProg.SetDataChange;
               cdsDocumento.SetDataChange;
               cdsHisPensionesAlimenticias.SetDataChange;
               cdsHisSGM.SetDataChange;
               cdsHisCompeten.SetDataChange;
               cdsHisCompEvalua.SetDataChange;
               cdsPlanCapacitacion.SetDataChange;
          end;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enImagen , Entidades ) then
     {$else}
     if ( enImagen in Entidades ) then
     {$endif}
     begin
          cdsEmpFoto.SetDataChange;
          cdsIdentFoto.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enVacacion , Entidades ) then
     {$else}
     if ( enVacacion in Entidades ) then
     {$endif}
     begin
          cdsHisVacacion.SetDataChange;
          cdsHisKardex.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPlanVacacion , Entidades ) then
     {$else}
     if ( enPlanVacacion in Entidades ) then
     {$endif}
     begin
          cdsPlanVacacion.SetDataChange;
          cdsHisVacacion.SetDataChange;
          cdsHisKardex.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enIncapacidad , Entidades ) then
     {$else}
     if ( enIncapacidad in Entidades ) then
     {$endif}
     begin
          cdsHisIncapaci.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enAcumula , Entidades ) or Dentro( enConcepto , Entidades ) or ( Estado = stSistema ) then
     {$else}
     if ( enAcumula in Entidades ) or ( enConcepto in Entidades ) or ( Estado = stSistema ) then
     {$endif}
     begin
          cdsHisAcumulados.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enKarCurso , Entidades ) then
     {$else}
     if ( enKarCurso in Entidades ) then
     {$endif}
     begin
          cdsHisCursos.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enLiq_Emp , Entidades ) then
     {$else}
     if ( enLiq_Emp in Entidades ) then
     {$endif}
     begin
          cdsHisPagosIMSS.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCurProg , Entidades ) then
     {$else}
     if ( enCurProg in Entidades ) then
     {$endif}
     begin
          cdsHisCursosProg.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enNomina , Entidades ) or ( Estado = stSistema ) then
     {$else}
     if ( enNomina in Entidades ) or ( Estado = stSistema ) then
     {$endif}
     begin
          cdsHisNominas.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPermiso , Entidades ) then
     {$else}
     if ( enPermiso in Entidades ) then
     {$endif}
     begin
          cdsHisPermiso.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPrestamo , Entidades ) then
     {$else}
     if ( enPrestamo in Entidades ) then
     {$endif}
     begin
          cdsHisPrestamos.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enAhorro , Entidades ) then
     {$else}
     if ( enAhorro in Entidades ) then
     {$endif}
     begin
          cdsHisAhorros.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enSal_Min , Entidades ) or Dentro( enKardex , Entidades ) then
     {$else}
     if ( enSal_Min in Entidades ) or ( enKardex in Entidades ) then
     {$endif}
     begin
          cdsHisKardex.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enTools , Entidades ) then
     {$else}
     if ( enTools in Entidades ) then
     {$endif}
     begin
          cdsHisTools.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enSesion , Entidades ) then
     {$else}
     if ( enSesion in Entidades ) then
     {$endif}
     begin
          cdsSesion.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enReserva , Entidades )then
     {$else}
     if( enReserva in Entidades )then
     {$endif}
     begin
          cdsReserva.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enKarCert , Entidades ) then
     {$else}
     if ( enKarCert in Entidades ) then
     {$endif}
     begin
          cdsKarCertificaciones.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPensiones , Entidades ) then
     {$else}
     if ( enPensiones in Entidades ) then
     {$endif}
     begin
          cdsHisPensionesAlimenticias.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enHisSegGasMed , Entidades ) then
     {$else}
     if ( enHisSegGasMed in Entidades ) then
     {$endif}
     begin
          cdsHisSGM.SetDataChange;
     end;

{$ifndef DOS_CAPAS} //Competencias solo para corporativa

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enHisCompeten , Entidades ) then
     {$else}
     if ( enHisCompeten in Entidades ) then
     {$endif}
     begin
          cdsHisCompeten.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPlanCapacitacion , Entidades ) then
     {$else}
     if ( enPlanCapacitacion in Entidades ) then
     {$endif}
     begin
          cdsPlanCapacitacion.SetDataChange;
     end;
{$endif}


end;
{ ************** METODOS GENERALES DE CLIENTDATASETS ************** }

{$ifdef VER130}
procedure TdmRecursos.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmRecursos.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     {$IFNDEF INTERFAZ_KRONOS}
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
     {$ELSE}
     if ansiPos( '@', E.Message ) > 0 then
        E.Message := Copy( E.Message, 1, ansiPos( '@', E.Message ) - 1 );
     dmInterfase.EscribeErrorExternoKronos (E.Message, FALSE);
     Action := raCancel;
     {$ENDIF}
end;
{$endif}

procedure TdmRecursos.cdsAlBorrar(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
          begin
               Delete;
               Enviar;
          end;
     end;
end;

function TdmRecursos.ChecaFolioSesion: Boolean;
begin
     Result := ( cdsHisCursos.FieldByName('SE_FOLIO').AsInteger = 0 );
end;

procedure TdmRecursos.cdsHistorialAfterPost(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
             Reconcile ( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;

procedure TdmRecursos.cdsGridCB_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No Existe el Empleado #' + IntToStr( AsInteger ) );
          end
          else
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
     end;
end;

function TdmRecursos.GetFiltroIncapacidad: String;
begin
     Result := 'TB_INCIDEN =' + IntToStr( Ord( eiIncapacidad ) );
end;


{ ****************** EVENTOS DE CLIENTDATASETS **************** }

{ cdsHisCursosProg }

procedure TdmRecursos.cdsHisCursosProgAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisCursosProg.Data := ServerRecursos.GetHisCursosProg( Empresa, Empleado, FALSE );
end;

procedure TdmRecursos.cdsHisCursosProgAlCrearCampos(Sender: TObject);
begin
     with cdsHisCursosProg do
     begin
          MaskFecha( 'KC_FEC_PRO' );
          MaskFecha( 'KC_FEC_TOM' );
          MaskFecha( 'KC_PROXIMO' );
     end;
end;


{ cdsEmpPercepcion }

procedure TdmRecursos.cdsEmpPercepcionAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsEmpPercepcion.Data := ServerRecursos.GetEmpPercepcion( Empresa, Empleado,
                              cdsEmpleado.FieldByName( 'CB_TIP_REV' ).AsString,
                              cdsEmpleado.FieldByName( 'CB_FEC_SAL' ).AsDateTime );
end;

procedure TdmRecursos.cdsEmpPercepcionAlCrearCampos(Sender: TObject);
begin
     with cdsEmpPercepcion do
     begin
          MaskPesos( 'KF_MONTO' );
          MaskPesos( 'KF_GRAVADO' );
     end;
end;

{ cdsHisPagosIMSS }

procedure TdmRecursos.cdsHisPagosIMSSAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisPagosIMSS.Data := ServerRecursos.GetHisPagosIMSS( Empresa, Empleado);
end;

procedure TdmRecursos.cdsHisPagosIMSSAlCrearCampos(Sender: TObject);
begin
     with cdsHisPagosIMSS do
     begin
          ListaFija( 'LS_TIPO', lfTipoLiqIMSS );
          MaskPesos( 'LE_TOT_IMS' );
          MaskPesos( 'LE_TOT_RET' );
          MaskPesos( 'LE_TOT_INF' );
          with FieldByName( 'LS_MONTH' ) do
          begin
               OnGetText:= LS_MONTHGetText;
               Alignment:= taLeftJustify;
          end;
     end;
end;

procedure TdmRecursos.LS_MONTHGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.AsInteger > 0 then
        Text:= MesConLetraMes( Sender.AsInteger );
end;

{ cdsHisNominas }

procedure TdmRecursos.cdsHisNominasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisNominas.Data := ServerRecursos.GetHisNominas( Empresa, Empleado, YearDefault, Ord( PeriodoTipo ) );
end;

procedure TdmRecursos.cdsHisNominasAlCrearCampos(Sender: TObject);
begin
     with cdsHisNominas do
     begin
          ListaFija( 'PE_TIPO', lfTipoNomina );
          MaskPesos( 'NO_HORAS' );
          MaskPesos( 'NO_EXTRAS' );
          MaskPesos( 'NO_PERCEPC' );
          MaskPesos( 'NO_DEDUCCI' );
          MaskPesos( 'NO_NETO' );
          MaskFecha( 'NO_FEC_PAG' );
          FieldByName('NO_USR_PAG').OnGetText := NO_USR_PAGGetText;
     end;
end;

procedure TdmRecursos.cdsHisNominasAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisNominas_DevEx, TEditHisNominas_DevEx);
end;

procedure TdmRecursos.cdsHisNominasAfterDelete(DataSet: TDataSet);
begin
     cdsHisNominas.Enviar;
end;

procedure TdmRecursos.cdsHisNominasAlEnviarDatos(Sender: TObject);
var
     ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisNominas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
             Reconcile ( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;

procedure TdmRecursos.NO_USR_PAGGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ' '
     else
     begin
          Sender.Alignment := taLeftJustify;
          Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
     end;
end;

{ cdsHisAhorros }

procedure TdmRecursos.cdsHisAhorrosAlAdquirirDatos(Sender: TObject);
begin
     dmTablas.cdsTAhorro.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmCliente do
          cdsHisAhorros.Data := ServerRecursos.GetHisAhorros( Empresa, Empleado );
end;



procedure TdmRecursos.cdsHisAhorrosAfterOpen(DataSet: TDataSet);
begin
     cdsACarAbo.DataSetField := cdsHisAhorros.FieldByName('qryDetail') As TDataSetField;
end;

procedure TdmRecursos.cdsHisAhorrosAlCrearCampos(Sender: TObject);
begin
     with cdsHisAhorros do
     begin
          ListaFija( 'AH_STATUS', lfStatusAhorro );
          MaskPesos( 'AH_SALDO' );
          MaskFecha( 'AH_FECHA' );
          CreateSimpleLookup( dmTablas.cdsTAhorro, 'TB_ELEMENT', 'AH_TIPO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmRecursos.cdsHisAhorrosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisAhorros_DevEx, TEditHisAhorros_DevEx );
end;

procedure TdmRecursos.cdsHisAhorrosNewRecord(DataSet: TDataSet);
begin
     with cdsHisAhorros do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('AH_CARGOS').AsFloat := 0;
          FieldByName('AH_ABONOS').AsFloat := 0;
          FieldByName('AH_NUMERO').AsInteger := 0;
          FieldByName('AH_SALDO_I').AsFloat := 0;
          FieldByName('AH_STATUS').AsInteger := Ord( saActivo );
          FieldByName('AH_TOTAL').AsFloat  := 0;
          FieldByName('AH_SALDO').AsFloat  := 0;
          FieldByName('AH_FECHA').AsDateTime := dmCliente.FechaDefault;
     end;
end;

procedure TdmRecursos.cdsHisAhorrosBeforeDelete(DataSet: TDataSet);
begin
     //cdsACarAbo.EmptyDataSet;
     with cdsACarAbo do
     begin
          First;
          while not Eof do
          begin
               Delete;
          end;
     end;
end;

procedure TdmRecursos.cdsHisAhorrosBeforePost(DataSet: TDataSet);
var
     dCargo, dAbono : Real;
begin
     if ZetaCommonTools.StrVacio( cdsHisAhorros.FieldByName( 'AH_TIPO' ).AsString ) then
     begin
          cdsHisAhorros.FieldByName( 'AH_TIPO' ).FocusControl;
          DB.DatabaseError( 'El C�digo del Ahorro no Puede quedar Vacio' );
     end;
     GetSaldos( cdsACarAbo, 'CR_CARGO', 'CR_ABONO', dCargo, dAbono );
     with cdsHisAhorros do
     begin
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('AH_ABONOS').AsFloat := dAbono;
          FieldByName('AH_CARGOS').AsFloat := dCargo;
          FieldByName('AH_SALDO').AsFloat := FieldByName('AH_SALDO_I').AsFloat +
                                             FieldByName('AH_TOTAL').AsFloat +
                                             dAbono-dCargo ;
     end;
end;

procedure TdmRecursos.cdsHisAhorrosAfterCancel(DataSet: TDataSet);
begin
     cdsACarAbo.CancelUpdates;
end;

procedure TdmRecursos.cdsHisAhorrosAlEnviarDatos(Sender: TObject);
var
     ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisAhorros do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
         if ChangeCount > 0 then
             Reconcile ( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;

procedure TdmRecursos.cdsHisAhorrosPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
     ZetaDialogo.ZError('Error', GetDBErrorDescription( E ), 0 );
     Action := daAbort;
end;

procedure TdmRecursos.ValidaCargoAbono( Sender : TField; const sCampo : string );
var
     AlCambiar : TFieldNotifyEvent;
begin
     with Sender do
     begin
          AlCambiar := DataSet.FieldByName(sCampo).OnChange;
          DataSet.FieldByName(sCampo).OnChange := NIL;

          if (AsFloat <=0 ) AND
             (DataSet.FieldByName(sCampo).AsFloat<=0) then
             DataBaseError( 'Cargo\Abono debe ser Mayor a Cero')
          else if ( AsFloat > 0 ) then
               DataSet.FieldByName(sCampo).AsFloat:= 0;

          Sender.DataSet.FieldByName(sCampo).OnChange := AlCambiar;
     end;
end;

procedure TdmRecursos.OnChangeAbono(Sender: TField);
begin
     ValidaCargoAbono( Sender, 'CR_CARGO' );
end;

procedure TdmRecursos.OnChangeCargo(Sender: TField);
begin
     ValidaCargoAbono( Sender, 'CR_ABONO' );
end;

{ cdsACarAbo }

procedure TdmRecursos.cdsACarAboAlCrearCampos(Sender: TObject);
begin
     with cdsACarAbo do
     begin
          MaskNumerico( 'CR_ABONO', '$#,0.00;-$#,0.00;#' );
          MaskNumerico( 'CR_CARGO', '$#,0.00;-$#,0.00;#' );
          FieldByName( 'CR_ABONO').OnChange := OnChangeAbono;
          FieldByName( 'CR_CARGO').OnChange := OnChangeCargo;
     end;
end;

procedure TdmRecursos.cdsACarAboNewRecord(DataSet: TDataSet);
begin
     with cdsACarAbo do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('CR_FECHA').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('AH_TIPO').AsString  := cdsHisAhorros.FieldByName('AH_TIPO').AsString;
          FieldByName('CR_CAPTURA').AsDateTime:= dmCliente.FechaDefault;
     end;
end;

{ cdsHisPrestamos }

procedure TdmRecursos.cdsHisPrestamosAlAdquirirDatos(Sender: TObject);
begin
     dmTablas.cdsTPresta.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmCliente do
          cdsHisPrestamos.Data := ServerRecursos.GetHisPrestamos( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisPrestamosAfterOpen(DataSet: TDataSet);
begin
     cdsPCarAbo.DataSetField := cdsHisPrestamos.FieldByName('qryDetail') as TDataSetField;
end;

procedure TdmRecursos.cdsHisPrestamosAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisPrestamos do
     begin
          ListaFija( 'PR_STATUS', lfStatusPrestamo );
          MaskPesos('PR_SALDO');
          MaskPesos('PR_MONTO');
          MaskPesos('PR_PAGOS');
          MaskFecha('PR_FECHA');
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmTablas.cdsTPresta, 'TB_ELEMENT', 'PR_TIPO' );
          ListaFija( 'CM_PRESTA', lfTiposdePrestamo );
          FieldByName( 'PR_MONTO_S' ).OnChange := PR_MontosChange;
          FieldByName( 'PR_TASA' ).OnChange := PR_MontosChange;
          FieldByName( 'PR_MESES' ).OnChange := PR_MESESChange;
          FieldByName( 'PR_INTERES' ).OnChange := PR_INTERESChange;
          FieldByName( 'PR_MONTO' ).OnChange := PR_PagosChange;
          FieldByName( 'PR_PAGOS' ).OnChange := PR_PagosChange;
          FieldByName( 'PR_TIPO' ).OnChange := PR_TIPOChange;
          FieldByName( 'PR_SALDO' ).OnGetText := PR_PagosOnGetText;
          FieldByName( 'PR_MONTO' ).OnGetText := PR_PagosOnGetText;
     end;
end;

procedure TdmRecursos.PR_TIPOChange(Sender: TField);
begin
     {$ifdef FAPSA}
     if not FEsPrestamoDias then
     begin
     {$endif}
     with Sender.DataSet do
     begin
          dmTablas.cdsTPresta.Conectar;
          if ( dmTablas.cdsTpresta.Locate( 'TB_CODIGO', FieldByName( 'PR_TIPO' ).AsString,[] ) ) then
          {$ifdef FAPSA}
          begin
               if ( dmTablas.cdsTPresta.FieldByName( 'TB_TASA1' ).AsFloat > 0 ) then
          {$endif}
                  FieldByName( 'PR_TASA' ).AsFloat := dmTablas.cdsTPresta.FieldByName( 'TB_TASA1' ).AsFloat;
          {$ifdef FAPSA}
          end;
          {$endif}
     end;
     {$ifdef FAPSA}
     end;
     {$endif}
end;

procedure TdmRecursos.PR_MontosChange(Sender: TField);
begin
     {$ifdef FAPSA}
     if not FEsPrestamoDias then
     begin
     {$endif}
     with Sender.DataSet do
     begin
          {$ifdef FAPSA}
          if ( FieldByName( 'PR_MONTO_S' ).AsFloat > 0 ) then
          {$endif}
          if not ( EsPrestamoFonacot( FieldByName( 'PR_TIPO' ).AsString ) ) then
             FieldByName( 'PR_INTERES' ).AsFloat := ( FieldByName( 'PR_MONTO_S' ).AsFloat *
                                                    ( FieldByName( 'PR_TASA' ).AsFloat / 100 ) *
                                                      FieldByName( 'PR_MESES' ).AsFloat );
     end;
     {$ifdef FAPSA}
     end;
     {$endif}
end;

function TdmRecursos.GetCantidadXMes: integer;
const
     K_TREINTA = 30;
     K_CUATRO  = 4;
     K_DOS     = 2;
     K_UNO     = 1;
     K_TRES    = 3;
begin
     with dmCliente do
     begin
         case GetClasificacionPeriodo(FListaTiposPeriodoConfidencialidad, cdsEmpleado.FieldByName('CB_NOMINA').AsInteger) of
              tpDiario:                                 Result := K_TREINTA;
              tpSemanal:                                Result := K_CUATRO;
              tpCatorcenal,tpQuincenal:				    Result := K_DOS;
              tpMensual:                                Result := K_UNO;
              tpDecenal:                                Result := K_TRES;
         else
              Result := K_CUATRO;
         end;
     end;
end;

function TdmRecursos.EsPrestamoFonacot( const sTipo: String ): Boolean;
begin
     Result := ( Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO) = sTipo );
end;

procedure TdmRecursos.PR_MESESChange(Sender: TField);
begin
     {$ifdef FAPSA}
     if not FEsPrestamoDias then
     begin
     {$endif}
     self.PR_MontosChange( Sender );
     with Sender.DataSet do
     begin
          {$ifdef FAPSA}
          if ( FieldByName( 'PR_MESES' ).AsFloat > 0 ) then
          {$endif}
          if not ( EsPrestamoFonacot( FieldByName( 'PR_TIPO' ).AsString ) ) then
             FieldByName( 'PR_PAGOS' ).AsInteger := Trunc( FieldByName( 'PR_MESES' ).AsFloat * GetCantidadXMes );
     end;
     {$ifdef FAPSA}
     end
     else
         ObtenDiasPorSalario;
     {$endif}
end;

procedure TdmRecursos.PR_INTERESChange(Sender: TField);
begin
     {$ifdef FAPSA}
     if not FEsPrestamoDias then
     begin
     {$endif}
     with Sender.DataSet do
     begin
          {$ifdef FAPSA}
          if ( ( FieldByName( 'PR_MONTO_S' ).AsFloat + FieldByName( 'PR_INTERES' ).AsFloat ) > 0 ) then
          {$endif}
          if not ( EsPrestamoFonacot( FieldByName( 'PR_TIPO' ).AsString ) ) then
              FieldByName( 'PR_MONTO' ).AsFloat := ( FieldByName( 'PR_MONTO_S' ).AsFloat + FieldByName( 'PR_INTERES' ).AsFloat );
     end;
     {$ifdef FAPSA}
     end;
     {$endif}
end;

procedure TdmRecursos.PR_PagosChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          if not ( EsPrestamoFonacot( FieldByName( 'PR_TIPO' ).AsString ) ) then
          begin
               if ( FieldByName( 'PR_PAGOS' ).AsInteger > 0 ) then
               begin
                    begin
                        FieldByName( 'PR_PAG_PER' ).AsFloat := ( FieldByName( 'PR_MONTO' ).AsFloat / FieldByName( 'PR_PAGOS' ).AsInteger );
                        FieldByName( 'PR_FORMULA' ).AsString := FloatToStr( FieldByName( 'PR_PAG_PER' ).AsFloat );
                    end
               end
               else
               begin
                    FieldByName( 'PR_PAG_PER' ).AsFloat := 0;
               end;
          end;
     end;
end;

procedure TdmRecursos.PR_PagosOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if EsPrestamoFonacot( FieldByName( 'PR_TIPO' ).AsString ) then
          begin
               if ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) ) then
                     Text := 'No aplica'
               else
                   Text := FormatFloat( '#,0.00', Sender.AsFloat );
          end
          else
              Text := FormatFloat( '#,0.00', Sender.AsFloat );
     end;
end;

{$ifdef FAPSA}
procedure TdmRecursos.ObtenDiasPorSalario;
begin
     with cdsHisPrestamos do
     begin
          with dmRecursos.GetDatosSalario( FieldByName( 'CB_CODIGO' ).AsInteger, Now, Now )do
          begin
               FieldByName( 'PR_MONTO' ).AsFloat := FieldByName( 'PR_MESES' ).AsInteger * Diario; //Trunc( FieldByName( 'PR_MESES' ).AsInteger * Diario );
               FieldByName( 'PR_MONTO_S' ).AsFloat := FieldByName( 'PR_MONTO' ).AsFloat;
          end;
     end;
end;
{$endif}

procedure TdmRecursos.cdsHisPrestamosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisPrestamos_DevEx, TEditHisPrestamos_DevEx );
end;

procedure TdmRecursos.cdsHisPrestamosNewRecord(DataSet: TDataSet);
begin
     with cdsHisPrestamos do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('PR_CARGOS').AsFloat := 0;
          FieldByName('PR_ABONOS').AsFloat := 0;
          FieldByName('PR_NUMERO').AsInteger := 0;
          FieldByName('PR_SALDO_I').AsFloat := 0;
          FieldByName('PR_STATUS').AsInteger := Ord( spActivo );
          FieldByName('PR_TOTAL').AsFloat  := 0;
          FieldByName('PR_SALDO').AsFloat  := 0;
          FieldByName('PR_FECHA').AsDateTime := dmCliente.FechaDefault;
          FieldByName('PR_REFEREN').AsString := VACIO;
     end;
end;

procedure TdmRecursos.cdsHisPrestamosBeforeDelete(DataSet: TDataSet);
begin
     //cdsPCarAbo.EmptyDataSet;
     with cdsPCarAbo do
     begin
          First;
          while not Eof do
          begin
               Delete;
          end;
     end;
end;

procedure TdmRecursos.cdsHisPrestamosAhorroBeforePost(DataSet: TDataSet);
var
   dCargo, dAbono : Real;
   lRecalcularSaldo : Boolean;
begin
     if ZetaCommonTools.StrVacio( cdsHisPrestamos.FieldByName( 'PR_TIPO' ).AsString ) then
     begin
          cdsHisPrestamos.FieldByName( 'PR_TIPO' ).FocusControl;
          DB.DatabaseError( 'El C�digo del Pr�stamo no Puede quedar Vacio' );
     end
     {OP: 28/05/08}
     else
     begin
         if ZetaCommonTools.StrVacio( cdsHisPrestamos.FieldByName( 'PR_REFEREN' ).AsString ) then
         begin
              cdsHisPrestamos.FieldByName( 'PR_REFEREN' ).FocusControl;
              DB.DatabaseError( 'La Referencia no puede quedar vac�a' );
         {$ifdef FAPSA}
         end
         else
         begin
              if FEsPrestamoDias and (cdsHisPrestamos.FieldByName( 'PR_MESES' ).AsInteger <= 0)then
              begin
                   cdsHisPrestamos.FieldByName( 'PR_MESES' ).FocusControl;
                   DB.DataBaseError( 'Los d�as de pr�stamo deben ser mayor a 0');
              end;
         {$endif}
         end;
     end;

     GetSaldos( cdsPCarAbo, 'CR_CARGO', 'CR_ABONO', dCargo, dAbono );

     lRecalcularSaldo := TRUE;
     with cdsHisPrestamos do
     begin
          if ( ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) ) and
                ( FieldByName('PR_TIPO').AsString = Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO) ) ) then
          begin
               lRecalcularSaldo := FALSE;
          end;

          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;

          if ( lRecalcularSaldo )  then
          begin
              FieldByName('PR_ABONOS').AsFloat := dAbono;
              FieldByName('PR_CARGOS').AsFloat := dCargo;
              FieldByName('PR_SALDO').AsFloat := FieldByName('PR_MONTO').AsFloat -
                                                 FieldByName('PR_SALDO_I').AsFloat -
                                                 FieldByName('PR_TOTAL').AsFloat -
                                                 dAbono + dCargo;
          end;
     end;

     with cdsHisPrestamos do
     begin
          if ( ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) ) and
          ( FieldByName('PR_TIPO').AsString = Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO) ) and
          ( FieldByName('PR_STATUS').AsInteger = ord ( spSaldado ) ) ) then
          begin
               FieldByName( 'PR_STATUS' ).FocusControl;
               DB.DatabaseError( 'No es posible saldar un pr�stamo Fonacot cuando est� activo ' + CR_LF + ' el mecanismo para administrar pr�stamos por c�dula mensual.' );
               //No est� activo el mecanismo para Administrar pr�stamos sin conciliaci�n en Globales de N�mina
          end;
     end;

     SetStatusPrestamo( cdsHisPrestamos );
end;

procedure TdmRecursos.SetStatusPrestamo( DataSet: TDataSet );
var
     OldStatus, NewStatus : eStatusPrestamo;

     function AplicarStatus : boolean;
     begin
          //Si Global Apagado aplica siempre
          //Si Global Activo  aplica cuando no es Fonacot
          Result := Not ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) )
                    or ( Global.GetGlobalBooleano(K_GLOBAL_FONACOT_PRESTAMO_CONCILIA ) and ( DataSet.FieldByName('PR_TIPO').AsString <> Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO))  )
     end;

begin
     with DataSet do
     begin
          OldStatus:= eStatusPrestamo( FieldByName( 'PR_STATUS' ).AsInteger );
          NewStatus:= OldStatus;

          if ( OldStatus <> spCancelado ) then
          begin

               if ( AplicarStatus) then
               begin
                     if ( FieldByName( 'PR_MONTO' ).AsFloat >= 0 ) then
                     begin
                         if ( FieldByName( 'PR_SALDO' ).AsFloat <= 0 ) then
                            NewStatus:= spSaldado
                         else if ( OldStatus <> spCancelado ) then
                              NewStatus:= spActivo;

                     end
                     else
                     begin
                         if ( FieldByName( 'PR_SALDO' ).AsFloat >= 0 ) then
                            NewStatus:= spSaldado
                         else if ( OldStatus <> spCancelado ) then
                              NewStatus:= spActivo;

                     end;
               end;
               if ( OldStatus <> NewStatus ) then
               begin
                    FieldByName( 'PR_STATUS' ).AsInteger := Ord( NewStatus );
                    ZInformation('Pr�stamos', 'El Status del Pr�stamo se Ajust� a : ' +
                                              ObtieneElemento( lfStatusPrestamo, Ord( NewStatus ) ), 0 );
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsHisPrestamosAfterCancel(DataSet: TDataSet);
begin
     cdsPCarAbo.CancelUpdates;
end;

procedure TdmRecursos.cdsPCarAboAfterDelete(DataSet: TDataSet);
begin
     if cdsHisPrestamos.State = dsBrowse then
        cdsHisPrestamos.Edit;
end;

procedure TdmRecursos.cdsHisPrestamosAlEnviarDatos(Sender: TObject);
var
     ErrorCount : Integer;
     oParametros: TZetaParams;
begin
     ErrorCount := 0;
     with cdsHisPrestamos do
     begin
          PostData;
          cdsPCarAbo.PostData;
          if (Changecount > 0 ) or (cdsPCarAbo.ChangeCount > 0)  then
          begin
               oParametros:= TZetaParams.Create;
               try
                    dmCliente.CargaActivosTodos( oParametros);
                    if Reconcile ( ServerRecursos.GrabaPrestamos( dmCliente.Empresa, oParametros.VarValues, Delta, FALSE, FMensajePrestamo, ErrorCount)) then
                    begin
                         {$ifdef CAJAAHORRO}
                         cdsHisAhorros.Refrescar;
                         {$endif}
                    end
                    else
                    begin
                         if FValidarReglas then
                         begin
                              Reconcile ( ServerRecursos.GrabaPrestamos(dmCliente.Empresa, oParametros.VarValues, Delta, TRUE, FMensajePrestamo, ErrorCount));
                         end;

                    end;
                    FValidarReglas := False;
               finally
                    FreeAndNil(oParametros)
               end
          end;
     end;
end;

procedure TdmRecursos.GetSaldos( DataSet : TDataSet; const sCargo, sAbono : string; var dCargo, dAbono : Real );
begin
     dCargo := 0;
     dAbono := 0;

     with DataSet do
     begin
          DisableControls;
          First;
          while NOT EOF do
          begin
               dCargo := dCargo + FieldByName(sCargo).AsFloat;
               dAbono := dAbono + FieldByName(sAbono).AsFloat;
               Next;
          end;
          EnableControls;
     end;
end;

procedure TdmRecursos.GetPrestamos( cdsPrestamos: TZetaClientDataset; const iEmpleado: integer );
begin
     { Optimizar por si se trata del empleado activo }
     with dmCliente do
     begin
          if ( Empleado = iEmpleado ) then
          begin
               cdsHisPrestamos.Conectar;
               cdsPrestamos.Data := cdsHisPrestamos.Data;
          end
          else
              cdsPrestamos.Data := ServerRecursos.GetHisPrestamos( Empresa, iEmpleado );
     end;
end;

{ cdsPCarAbo }

procedure TdmRecursos.cdsPCarAboAlCrearCampos(Sender: TObject);
begin
     with cdsPCarAbo do
     begin
          MaskNumerico( 'CR_ABONO', '$#,0.00;-$#,0.00;#' );
          MaskNumerico( 'CR_CARGO', '$#,0.00;-$#,0.00;#' );
          FieldByName( 'CR_ABONO' ).OnChange := OnChangeAbono;
          FieldByName( 'CR_CARGO' ).OnChange := OnChangeCargo;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmRecursos.cdsPCarAboNewRecord(DataSet: TDataSet);
begin
     with cdsPCarAbo do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('PR_TIPO').AsString  := cdsHisPrestamos.FieldByName('PR_TIPO').AsString;
          FieldByName('PR_REFEREN').AsString  := cdsHisPrestamos.FieldByName('PR_REFEREN').AsString;
          FieldByName('CR_FECHA').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('CR_CAPTURA').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmRecursos.cdsCarAboBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          //US 12370 Rastreo de abonos y cargos hechos por Fonacot
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          if FieldByName('CR_FECHA').AsDateTime = 0 then
             DatabaseError( 'Fecha no puede quedar vac�a');

          if (FieldByName('CR_CARGO').AsFloat <=0 ) AND
             (FieldByName('CR_ABONO').AsFloat <=0 ) then
             DatabaseError( 'Cargo\Abono debe ser Mayor a Cero')
     end;
end;

procedure TdmRecursos.cdsACarAboAfterDelete(DataSet: TDataSet);
begin
     if cdsHisAhorros.State = dsBrowse then
        cdsHisAhorros.Edit;
end;

{ cdsHisIncapaci }

procedure TdmRecursos.cdsHisIncapaciAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisIncapaci.Data := ServerRecursos.GetHisIncapaci( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisIncapaciAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with Sender as TZetaClientDataSet do    //cdsHisIncapaci o cdsGridIncapaci
     begin
          MaskFecha( 'IN_FEC_INI' );
          MaskFecha( 'IN_FEC_FIN' );
          MaskFecha( 'IN_SUA_INI' );
          MaskFecha( 'IN_SUA_FIN' );
          MaskFecha( 'IN_FEC_RH' );

          FieldByName('IN_FEC_INI').OnChange := OnIN_FEC_INIChange;
          FieldByName('IN_DIAS').OnChange := OnIN_DIASChange;

          FieldByName('IN_SUA_INI').OnChange := OnIN_SUA_INIChange;
          FieldByName('IN_NOMTIPO').OnChange := validacionTipoNominaChange;

          // FieldByName( 'IN_DIASSUB' ).OnChange := OnIN_DIASSUBChange;
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'TB_ELEMENT', 'IN_TIPO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          FieldByName( 'IN_NUMERO' ).EditMask := 'll999999;1;';
     end;
end;

procedure TdmRecursos.cdsHisIncapaciAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisIncapaci_DevEx, TEditHisIncapaci_DevEx );
     RefrescaKardex( K_T_INCA );
end;

procedure TdmRecursos.cdsHisIncapaciAlBorrar(Sender: TObject);
begin
     with cdsHisIncapaci do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
          begin
               Delete;
               Enviar;
               Refrescar;
               RefrescaKardex( K_T_INCA );
          end;
     end;
end;

procedure TdmRecursos.cdsHisIncapaciNewRecord(DataSet: TDataSet);
var
TipoNomina: eTipoPeriodo;
begin
     with cdsHisIncapaci do
     begin
          FieldByName('CB_CODIGO').AsInteger  := dmCliente.Empleado;
          FieldByName('IN_FEC_INI').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('IN_DIAS').AsInteger    := 1;
          FieldByName('IN_MOTIVO').AsInteger  := Ord( miInicial );
          FieldByName('IN_FIN').AsInteger     := Ord( fiNoTermina );
          FieldByName('IN_DIASSUB').AsFloat   := 0;
          with dmCliente do
          begin
               FieldByName( 'IN_NOMYEAR' ).AsInteger  := YearDefault;
               TipoNomina:= eTipoPeriodo ( cdsEmpleado.FieldByName('CB_NOMINA').AsInteger );
               FieldByName( 'IN_NOMTIPO' ).AsInteger  :=  Ord( TipoNomina );
               FieldByName( 'IN_NOMNUME' ).AsInteger  := GetPeriodoInicialTipoNomina(  TipoNomina );
          end;
     end;
end;

procedure TdmRecursos.cdsHisIncapaciBeforePost(DataSet: TDataSet);

function BuscarAnterior( const FechaInicial:TDate; const sTipo :string ):Boolean;
begin
     Result := False;
     with cdsIncapacidades do
     begin
          if Not IsEmpty then
          begin
               First;
               While ( ( FieldByName('IN_FEC_INI').AsDateTime < FechaInicial ) and ( not Eof ) ) do
               begin
                    Result := (( FieldByName('IN_MOTIVO').AsInteger in [ Ord( miInicial ), Ord( miSubsecuente ) ] ) and
                               ( FieldByName('IN_TIPO').AsString = sTipo  ) and
                               ( FieldByName('IN_FEC_FIN').AsDateTime = FechaInicial )
                               );
                    if Result then
                       Break;
                    Next;
               end;
          end;
     end;
end;

begin
     with DataSet do
     begin
          if FieldByName( 'IN_FEC_INI').AsDateTime = 0 then
               DatabaseError( 'Fecha de Inicio de Incapacidad no puede quedar Vac�a');

          if ( FieldByName( 'IN_DIAS').AsInteger <= 0 ) then
       	     DataBaseError( 'Dias de Incapacidad debe ser mayor a 0');

          if NOT StrLleno(FieldByName( 'IN_TIPO').AsString ) then
               DataBaseError( 'Tipo de Incapacidad no puede quedar vac�o') ;

          if eFinIncapacidad( FieldByName( 'IN_FIN').AsInteger ) <>  fiPermanente then
               FieldByName( 'IN_TASA_IP').AsFloat := 0;

{$ifndef COVIDIEN_INTERFAZ_HR}
          if ( FieldByName( 'IN_DIAS' ).AsFloat < FieldByName( 'IN_DIASSUB' ).AsFloat ) and
             ( not ZetaDialogo.ZWarningConfirm('� Atenci�n !', Format( 'Los d�as a subsidiar son mayores a los d�as de Incapacidad%s� Desea Continuar ?', [ CR_LF ] ), 0, mbCancel ) ) then
             Abort;
{$endif}

          with dmCliente do
          begin
{$ifdef COVIDIEN_INTERFAZ_HR}
               FieldByName( 'US_CODIGO').AsInteger   := dmInterfase.UsuarioMovimiento;
{$else}
               FieldByName( 'US_CODIGO').AsInteger   := Usuario;
{$endif}
               FieldByName( 'IN_CAPTURA').AsDateTime := FechaDefault;
          end;

{$ifndef COVIDIEN_INTERFAZ_HR}
          {Validacion de incapacidad subsecuente}
          if FieldByName('IN_MOTIVO').AsInteger = Ord(miSubsecuente ) then
          begin
               { if not BuscarAnterior( FieldByName('IN_FEC_INI').AsDateTime, FieldByName('IN_TIPO').AsString ) and
                 ( not ZetaDialogo.ZWarningConfirm('� Atenci�n !',
                 Format( 'No se encontr� una Incapacidad Inicial o Subsecuente, que ' +
                 'termine el %0:s%1:s� Desea Continuar ?', [ DateToStr ( FieldByName ('IN_FEC_INI').AsDateTime - 1 ), CR_LF + CR_LF ] ), 0, mbCancel ) ) then }
               if not BuscarAnterior( FieldByName('IN_FEC_INI').AsDateTime, FieldByName('IN_TIPO').AsString ) and
                 ( not ZetaDialogo.ZWarningConfirm('� Atenci�n !',
                 Format( 'No se encontr� una Incapacidad Inicial o Subsecuente de tipo %0:s, que ' +
                 'termine el %1:s%2:s� Desea Continuar ?', [ FieldByName('TB_ELEMENT').AsString, DateToStr ( FieldByName ('IN_FEC_INI').AsDateTime - 1 ), CR_LF + CR_LF ] ), 0, mbCancel ) ) then
                    Abort;
               {AP( 31/Julio/2008): Defecto #1041: Se cambio de error a mensaje de advertencia}
               // DataBaseError( Format ( 'No se puede registrar una incapacidad subsecuente si no existe una inicial o subsecuente anterior del mismo tipo que termine el %s',[ DateToStr ( FieldByName ('IN_FEC_INI').AsDateTime - 1 ) ] ) ) ;
          end;
{$endif}
     end;
end;

procedure TdmRecursos.cdsHisIncapaciBeforeEdit(DataSet: TDataSet);
begin
     AsignarDataTemporal;
end;

procedure TdmRecursos.AsignarDataTemporal;
begin
     if cdsIncapacidades = Nil then
     begin
          cdsIncapacidades := TZetaClientDataSet.Create(Self);
     end;
     cdsIncapacidades.Data := cdsHisIncapaci.Data;
end;


procedure TdmRecursos.cdsHisIncapaciAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   dFechaInicioRegNuevo : TDateTime;

   procedure CrearDatosConflictos;
   begin
        with cdsListaConflicto do
        begin
             if Active then
                EmptyDataSet
             else
             begin
                  Init;
                  AddIntegerField( 'CB_CODIGO');
                  AddDateTimeField('IN_FEC_INI' );
                  AddIntegerField( 'IN_DIAS');
                  AddDateTimeField('IN_FEC_FIN');
                  AddDateTimeField('IN_SUA_INI' );
                  AddDateTimeField('IN_SUA_FIN' );
                  AddDateTimeField('IN_FEC_INI' + '_BUS' );
                  CreateTempDataset;
             end;
        end;
   end;

begin
     ErrorCount := 0;
     with Sender as TZetaClientDataSet do              // cdsHisIncapaci o cdsGridIncapaci
     begin
          if State in [ dsEdit, dsInsert ] then
               Post;
          if ChangeCount > 0 then
          begin
               FCancelaAjuste := FALSE;
               CrearDatosConflictos;
               Repeat
               Until ( FCancelaAjuste or Reconciliar( ServerRecursos.GrabaIncapacidad( dmCliente.Empresa, Delta, ErrorCount, cdsListaConflicto.Data ) ) );
               if ErrorCount = 0 then
               begin      
                    FRefrescaHisKardex:= TRUE;
                    if not cdsListaConflicto.IsEmpty then
                    begin
                         dFechaInicioRegNuevo := FieldByName ('IN_FEC_INI').AsDateTime;
                         cdsHisIncapaci.Refrescar;
                         //cdsHisIncapaci.Locate('IN_FEC_INI', FechaAsStr(dFechaInicioRegNuevo), []);
                         cdsHisIncapaci.Locate('IN_FEC_INI', dFechaInicioRegNuevo, []);
                    end;
               end;
          end;
     end;
end;

{$ifdef VER130}
procedure TdmRecursos.cdsHisIncapaciReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoAccion( DataSet, FCancelaAjuste, K_SUB_INCAPACIDAD, 'Error al Registrar Incapacidad', E.Message );
end;
{$else}
procedure TdmRecursos.cdsHisIncapaciReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoIncapacidadAccion( DataSet, FCancelaAjuste, K_SUB_INCAPACIDAD, 'Error al Registrar Incapacidad', E.Message,cdsListaConflicto );
end;
{$endif}

procedure TdmRecursos.OnIN_FEC_INIChange(Sender : TField );
begin
     with Sender.DataSet do
     begin
          FieldByName( 'IN_FEC_FIN').AsDateTime := FieldByName( 'IN_FEC_INI').AsDateTime +
                                                  FieldByName( 'IN_DIAS').AsInteger;
          FieldByName( 'IN_SUA_INI').AsDateTime := FieldByName( 'IN_FEC_INI').AsDateTime;
          FieldByName( 'IN_FEC_RH').AsDateTime := FieldByName( 'IN_FEC_INI').AsDateTime;
     end;
end;

procedure TdmRecursos.OnIN_SUA_INIChange(Sender : TField );
begin
      with Sender.DataSet do
          FieldByName( 'IN_SUA_FIN').AsDateTime := FieldByName( 'IN_SUA_INI').AsDateTime +
                                                   FieldByName( 'IN_DIAS').AsInteger;
end;

procedure TdmRecursos.OnIN_DIASChange(Sender : TField );
begin
     with Sender.DataSet do
     begin
          FieldByName( 'IN_SUA_FIN').AsDateTime := FieldByName( 'IN_SUA_INI').AsDateTime +
                                                   FieldByName( 'IN_DIAS').AsInteger;
          FieldByName( 'IN_FEC_FIN').AsDateTime := FieldByName( 'IN_FEC_INI').AsDateTime +
                                                   FieldByName( 'IN_DIAS').AsInteger;
     end;
end;

{
procedure TdmRecursos.OnIN_DIASSUBChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          if ( Sender.AsFloat > 0 ) and ( FieldByName ( 'IN_NOMYEAR' ).AsInteger = 0 ) then   // Inicializar con periodo activo
          begin
               with dmCliente do
               begin
                    FieldByName ( 'IN_NOMYEAR' ).AsInteger := YearDefault;
                    FieldByName ( 'IN_NOMTIPO' ).AsInteger := Ord( PeriodoTipo );
                    FieldByName ( 'IN_NOMNUME' ).AsInteger := PeriodoNumero;
               end
          end;
     end;
end;
}

{ cdsGridIncapaci }

procedure TdmRecursos.EditarGridIncapaci;
begin
     with cdsGridIncapaci do
     begin
          if Active then
             EmptyDataSet
          else
             Conectar;
     end;
     try
        dmCliente.SetLookupEmpleado;
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridIncapaci_DevEx, TGridIncapaci_DevEx, TRUE );
        with cdsGridIncapaci do
        begin
             if cdsGridIncapaci.ChangeCount = 0 then
                TressShell.SetDataChange( [ enIncapacidad ] )
             else
                CancelUpdates;
        end;
     finally
        dmCliente.ResetLookupEmpleado;
     end;
     TressShell.ReconectaFormaActiva;  // Solo Refresca si la forma Activa incluye a cdsHisIncapaci
end;

procedure TdmRecursos.cdsGridIncapaciAlAdquirirDatos(Sender: TObject);
begin
     cdsGridIncapaci.Data:= ServerRecursos.GetGridIncapaci( dmCliente.Empresa, TRUE );   // Siempre se manda llamar con solo ALTAS ( Grid Vacio para Registro )
end;

procedure TdmRecursos.cdsGridIncapaciAlCrearCampos(Sender: TObject);
begin
     cdsHisIncapaciAlCrearCampos( Sender );      // Inicializa y Crea los mismos Campos que el Historial x Empleado
     with Sender as TZetaClientDataSet do
     begin
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := IncapaciCB_CODIGOChange;
          FieldByName( 'IN_TIPO' ).OnValidate:= IncapaciIN_TIPOValidate;
          ListaFija( 'IN_MOTIVO', lfMotivoIncapacidad );
          ListaFija( 'IN_FIN', lfFinIncapacidad );
          MaskTasa( 'IN_TASA_IP' );
     end;
end;

{$ifdef VER130}
procedure TdmRecursos.cdsGridIncapaciReconcileError( DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoAccion( DataSet, FCancelaAjuste, K_SUB_INCAPACIDAD, 'Error al Registrar Incapacidad', E.Message );
     if ( Action = raAbort ) then       // En el Grid no se puede Abortar, deben reconciliarse todos los cambios cancelando los que no se pueda o quiera ajustar fechas
     begin
          Action := raCancel;
          FCancelaAjuste := FALSE;
     end;
end;
{$else}
procedure TdmRecursos.cdsGridIncapaciReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoIncapacidadAccion( DataSet, FCancelaAjuste, K_SUB_INCAPACIDAD, 'Error al Registrar Incapacidad', E.Message, cdsListaConflicto );
     if ( Action = raAbort ) then       // En el Grid no se puede Abortar, deben reconciliarse todos los cambios cancelando los que no se pueda o quiera ajustar fechas
     begin
          Action := raCancel;
          FCancelaAjuste := FALSE;
     end;
end;
{$endif}

procedure TdmRecursos.IncapaciCB_CODIGOChange(Sender: TField);
var
   TipoNomina: eTipoPeriodo;
begin
     { Despues de Indicar No. de Empleado, sugiere el resto de los valores }
     with Sender.DataSet do
     begin
          with dmCliente do
          begin
               FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
               FieldByName('IN_FEC_INI').AsDateTime:= FechaDefault;
               FieldByName( 'IN_NOMYEAR' ).AsInteger  := YearDefault;
               // Mejora Administraci�n de Incapacidades
               // FieldByName( 'IN_NOMTIPO' ).AsInteger  := Ord( PeriodoTipo );
               TipoNomina:= eTipoPeriodo ( cdsEmpleado.FieldByName('CB_NOMINA').AsInteger );
               FieldByName( 'IN_NOMTIPO' ).AsInteger  :=  Ord( TipoNomina );
               FieldByName( 'IN_NOMNUME' ).AsInteger  := GetPeriodoInicialTipoNomina(  TipoNomina );
               // ----- -----
          end;
          FieldByName('IN_DIAS').AsInteger    := 1;
          FieldByName('IN_MOTIVO').AsInteger  := Ord( miInicial );
          FieldByName('IN_FIN').AsInteger     := Ord( fiNoTermina );
          FieldByName('IN_DIASSUB').AsFloat   := 0;
     end;
end;

procedure TdmRecursos.IncapaciIN_TIPOValidate(Sender: TField);
var
   sDescrip: String;
begin
     with Sender do
          if ( strLleno( AsString ) ) and
             ( not dmTablas.cdsIncidencias.LookupKey( AsString, GetFiltroIncapacidad, sDescrip ) ) then
             DataBaseError( 'No Existe el C�digo de Tipo de Incidencia: ' + AsString );
end;

{ cdsHisPermiso }

procedure TdmRecursos.cdsHisPermisoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisPermiso.Data := ServerRecursos.GetHisPermiso( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisPermisoAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisPermiso do
     begin
          ListaFija( 'PM_CLASIFI', lfTipoPermiso );
          MaskFecha( 'PM_FEC_INI' );
          MaskFecha( 'PM_FEC_FIN' );                                           
          FieldByName('PM_FEC_INI').OnChange := OnPM_FEC_INIChange;
          FieldByName('PM_DIAS').OnChange := OnPM_FEC_INIChange;
          FieldByName('PM_FEC_FIN').OnChange := OnPM_FEC_FINChange;
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'TB_ELEMENT', 'PM_TIPO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          FieldByName('PM_CLASIFI').OnChange := OnPM_CLASIFIChange;

     end;
end;

procedure TdmRecursos.OnPM_CLASIFIChange(Sender: TField);
begin
     with cdsHisPermiso do
     begin
          if( not ValidarMotivoPermisos( FieldByName('PM_TIPO').AsString,FieldByName('PM_CLASIFI').AsInteger ) )then
              FieldByName('PM_TIPO').AsString := VACIO;
     end;
end;

function TdmRecursos.ValidarMotivoPermisos(const sCodigo :string; const iTipo:Integer ):Boolean;
begin
     Result := StrVacio(sCodigo);
     if not Result  then
     begin
          with dmTablas.cdsIncidencias do
          begin
               Result := (  Locate('TB_CODIGO',sCodigo,[]) ) and
                         ( ( FieldByName('TB_PERMISO').AsInteger = K_MOTIVO_PERMISOS_OFFSET ) or
                           ( FieldByName('TB_PERMISO').AsInteger = iTipo  )
                         );
          end;
     end;
end;

procedure TdmRecursos.cdsHisPermisoAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisPermiso_DevEx, TEditHisPermiso_DevEx );
     RefrescaKardex( K_T_PERM );
end;

procedure TdmRecursos.cdsHisPermisoAlBorrar(Sender: TObject);
begin
     with cdsHisPermiso do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
          begin
               Delete;
               Enviar;
               Refrescar;
               RefrescaKardex( K_T_PERM );
          end;
     end;
end;

procedure TdmRecursos.cdsHisPermisoNewRecord(DataSet: TDataSet);
begin
     with cdsHisPermiso do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('PM_FEC_INI').AsDateTime := dmCliente.FechaDefault;
          FieldByName('PM_DIAS').AsInteger := 1;
          FieldByName('PM_CLASIFI').AsInteger := Ord( tpConGoce);
          FieldByName( 'PM_GLOBAL').AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmRecursos.cdsHisPermisoBeforePost(DataSet: TDataSet);
begin
     with cdsHisPermiso do
     begin
          if ( FieldByName( 'PM_DIAS').AsInteger <= 0 ) then
             DataBaseError( 'D�as de Permiso Debe Ser Mayor Que Cero' );
          if strVacio( FieldByName( 'PM_TIPO').AsString ) then
             DataBaseError( 'No Se Ha Especificado Un Tipo de Permiso' ) ;
          if (FieldByName( 'PM_FEC_FIN').AsDateTime < (FieldByName( 'PM_FEC_INI').AsDateTime + 1)) then
             DataBaseError('Fecha de regreso debe ser mayor/igual a Fecha Inicio m�s 1 d�a');
          FieldByName( 'US_CODIGO').AsInteger   := dmCliente.Usuario;
          FieldByName( 'PM_CAPTURA').AsDateTime := dmCliente.FechaDefault;
     end;
end;

procedure TdmRecursos.cdsHisPermisoAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisPermiso do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               FCancelaAjuste := FALSE;
               Repeat
               Until ( FCancelaAjuste or Reconciliar( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) );
               if ErrorCount = 0 then
                  FRefrescaHisKardex:= TRUE;
          end;
     end;
end;

{$ifdef VER130}
procedure TdmRecursos.cdsHisPermisoReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoAccion( DataSet, FCancelaAjuste, K_SUB_PERMISO, 'Error al Registrar Permiso', E.Message );
end;
{$else}
procedure TdmRecursos.cdsHisPermisoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$IFDEF INTERFAZ_ELECTROLUX_KRONOS}
const
     ID_FECHA_INI = '@1';
var
   iPosFechaIni : Integer;
{$ENDIF}
begin
       {$IFNDEF INTERFAZ_ELECTROLUX_KRONOS}
//     Action := ConflictoAccion( DataSet, FCancelaAjuste, K_SUB_PERMISO, 'Error al Registrar Permiso', E.Message );
       Action := ConflictoAccionGetDias( DataSet, FCancelaAjuste, K_SUB_PERMISO, 'Error al Registrar Permiso', E.Message, dmAsistencia.GetPermisoDiasHabiles );
       {$ELSE}
       iPosFechaIni := pos( ID_FECHA_INI, E.Message );
       if iPosFechaIni > 0 then
          dmInterfase.EscribeErrorExternoKronos (Copy( E.Message, 1, iPosFechaIni - 1 ), FALSE)
       else
           dmInterfase.EscribeErrorExternoKronos (E.Message, FALSE);
       Action := raCancel;
       {$ENDIF}
end;
{$endif}

procedure TdmRecursos.OnPM_FEC_INIChange(Sender : TField);
var
   DiasRango : Double; // ??
   oCursor: TCursor;
begin
     cdsHisPermiso.FieldByName( 'PM_FEC_FIN').OnChange := nil;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     with cdsHisPermiso do
     begin
          if Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES ) then
          begin
               FieldByName( 'PM_FEC_FIN').AsDateTime := dmAsistencia.GetPermisoFechaRegreso(dmCliente.Empleado,FieldByName( 'PM_FEC_INI').AsDateTime,FieldByName( 'PM_DIAS').AsInteger, DiasRango);
          end
          else
          begin
               FieldByName( 'PM_FEC_FIN').AsDateTime := FieldByName( 'PM_FEC_INI').AsDateTime + FieldByName( 'PM_DIAS').AsInteger;
          end;
     end;
     Screen.Cursor := oCursor;
     cdsHisPermiso.FieldByName( 'PM_FEC_FIN').OnChange := OnPM_FEC_FINChange;
end;

procedure TdmRecursos.OnPM_FEC_FINChange(Sender : TField);
var
   oCursor: TCursor;
begin
     cdsHisPermiso.FieldByName( 'PM_DIAS').OnChange := nil;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     cdsHisPermiso.FieldByName( 'PM_DIAS').AsFloat := dmAsistencia.GetPermisoDiasHabiles(dmCliente.Empleado,cdsHisPermiso.FieldByName( 'PM_FEC_INI').AsDateTime,cdsHisPermiso.FieldByName( 'PM_FEC_FIN').AsDateTime);
     Screen.Cursor := oCursor;
     cdsHisPermiso.FieldByName( 'PM_DIAS').OnChange := OnPM_FEC_INIChange;
end;

{ cdsHisAcumulados }
procedure TdmRecursos.cdsHisAcumuladosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisAcumulados.Data := ServerRecursos.GetHisAcumulados( Empresa, Empleado, YearDefault );
end;

procedure TdmRecursos.cdsHisAcumuladosAlCrearCampos(Sender: TObject);
var
     i : Byte;
     sCampo : String;
begin
     with cdsHisAcumulados do
     begin
          for i := 1 to 13 do
          begin
               sCampo := 'AC_MES_' + strZero( IntToStr( i ), 2 );
               MaskPesos( sCampo );
               FieldByName( sCampo ).OnChange := OnChangeAcumulados;
          end;
          MaskPesos( 'AC_ANUAL' );
          CreateSimpleLookup( dmCatalogos.cdsConceptos, 'CO_DESCRIP', 'CO_NUMERO' );
     end;
end;

procedure TdmRecursos.cdsHisAcumuladosNewRecord(DataSet: TDataSet);
begin
     with cdsHisAcumulados do
     begin
          FieldByName('AC_YEAR').AsInteger := dmCliente.YearDefault;
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
     end;
end;

procedure TdmRecursos.cdsHisAcumuladosRSAfterPost(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
             Reconcile ( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;

procedure TdmRecursos.cdsHisAcumuladosRSAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisAcumuladosRS.Data := ServerRecursos.GetHisAcumuladosRS( Empresa, Empleado, YearDefault, FRazonSocialAcumulados );
end;

procedure TdmRecursos.cdsHisAcumuladosRSAlCrearCampos(Sender: TObject);
var
     i : Byte;
     sCampo : String;
begin
     with cdsHisAcumuladosRS do
     begin
          for i := 1 to 13 do
          begin
               sCampo := 'AC_MES_' + strZero( IntToStr( i ), 2 );
               MaskPesos( sCampo );
               FieldByName( sCampo ).OnChange := OnChangeAcumuladosRS;
          end;
          MaskPesos( 'AC_ANUAL' );
          CreateSimpleLookup( dmCatalogos.cdsConceptos, 'CO_DESCRIP', 'CO_NUMERO' );
          CreateSimpleLookup( dmCatalogos.cdsRSocial,   'RS_NOMBRE' , 'RS_CODIGO' );
     end;
end;

procedure TdmRecursos.cdsHisAcumuladosRSAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisAcumulados_RS_DevEx, TEditHisAcumulados_RS_DevEx );
end;

procedure TdmRecursos.cdsHisAcumuladosRSBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if StrVacio( FieldByName('RS_CODIGO').AsString ) then
          begin
               FieldByName('RS_CODIGO').FocusControl;
               DatabaseError('Capturar Raz�n Social');
          end;
          if FieldByName('CO_NUMERO').AsInteger = 0 then
          begin
               FieldByName('CO_NUMERO').FocusControl;
               DatabaseError('Capturar Concepto');
          end;
     end;
end;

procedure TdmRecursos.cdsHisAcumuladosRSNewRecord(DataSet: TDataSet);
begin
     with cdsHisAcumuladosRS do
     begin
          FieldByName('AC_YEAR').AsInteger := dmCliente.YearDefault;
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
     end;
end;

procedure TdmRecursos.cdsHisAcumuladosRSReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction );
begin
     Action := HandleReconcileErrorAcumulados(Dataset, UpdateKind, E);
end;

function TdmRecursos.HandleReconcileErrorAcumulados( DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError ): TReconcileAction;
var
   sError: String;
begin
     with ReconcileError do
     begin
          { PK_VIOLATION }
          if PK_VIOLATION( ReconcileError ) then
             sError := '� C�digo Ya Existe !'
          else if UKC_Violation( ReconcileError ) then
                  sError := '� C�digo Ya Existe !'
          else
              sError := Message;
          if ( Context <> '' ) then
              sError := sError + CR_LF + Context;
     end;
     ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 );
     case UpdateKind of
          ukDelete: Result := raCancel;
     else
         Result := raAbort;
     end;
end;

procedure TdmRecursos.OnChangeAcumuladosRS(Sender:TField);
begin
     with cdsHisAcumuladosRS do
          FieldByName( 'AC_ANUAL' ).AsFloat := FieldByName( 'AC_MES_01' ).AsFloat +
                                               FieldByName( 'AC_MES_02' ).AsFloat +
                                               FieldByName( 'AC_MES_03' ).AsFloat +
                                               FieldByName( 'AC_MES_04' ).AsFloat +
                                               FieldByName( 'AC_MES_05' ).AsFloat +
                                               FieldByName( 'AC_MES_06' ).AsFloat +
                                               FieldByName( 'AC_MES_07' ).AsFloat +
                                               FieldByName( 'AC_MES_08' ).AsFloat +
                                               FieldByName( 'AC_MES_09' ).AsFloat +
                                               FieldByName( 'AC_MES_10' ).AsFloat +
                                               FieldByName( 'AC_MES_11' ).AsFloat +
                                               FieldByName( 'AC_MES_12' ).AsFloat +
                                               FieldByName( 'AC_MES_13' ).AsFloat;
end;

{*** FIN ***}

procedure TdmRecursos.cdsHisAcumuladosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisAcumulados_DevEx, TEditHisAcumulados_DevEx );
end;

procedure TdmRecursos.OnChangeAcumulados(Sender:TField);
begin
     with cdsHisAcumulados do
          FieldByName( 'AC_ANUAL' ).AsFloat := FieldByName( 'AC_MES_01' ).AsFloat +
                                               FieldByName( 'AC_MES_02' ).AsFloat +
                                               FieldByName( 'AC_MES_03' ).AsFloat +
                                               FieldByName( 'AC_MES_04' ).AsFloat +
                                               FieldByName( 'AC_MES_05' ).AsFloat +
                                               FieldByName( 'AC_MES_06' ).AsFloat +
                                               FieldByName( 'AC_MES_07' ).AsFloat +
                                               FieldByName( 'AC_MES_08' ).AsFloat +
                                               FieldByName( 'AC_MES_09' ).AsFloat +
                                               FieldByName( 'AC_MES_10' ).AsFloat +
                                               FieldByName( 'AC_MES_11' ).AsFloat +
                                               FieldByName( 'AC_MES_12' ).AsFloat +
                                               FieldByName( 'AC_MES_13' ).AsFloat;
end;

{ cdsHisCursos }
procedure TdmRecursos.cdsHisCursosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsHisCursos.Data := ServerRecursos.GetHisCursos( Empresa, Empleado );
     end;
end;

procedure TdmRecursos.cdsHisCursosAlCrearCampos(Sender: TObject);
begin
     with cdsHisCursos do
     begin
          MaskNumerico( 'SE_FOLIO', '0;-0;#' );
          MaskFecha('KC_FEC_TOM');
          CreateSimpleLookup( dmCatalogos.cdsCursos, 'CU_NOMBRE', 'CU_CODIGO');
          FieldByName('KC_FEC_TOM').OnChange := OnKC_FEC_TOMChange;
          FieldByName('CU_CODIGO').OnChange := CU_CODIGOChange;
     end;
end;

procedure TdmRecursos.CU_CODIGOChange(Sender: TField);
begin
     with Sender do
     begin
          if ( FUltimoCurso <> NewValue ) or ( cdsHisCursos.State in [dsInsert] ) then
          begin
               with cdsHisCursos do
               begin
                    FieldByName('MA_CODIGO').AsString:= dmCatalogos.cdsCursos.FieldByName('MA_CODIGO').AsString;
                    FieldByName('KC_HORAS').AsString:= dmCatalogos.cdsCursos.FieldByName('CU_HORAS').AsString;
                    FieldByName ('KC_REVISIO').AsString := dmCatalogos.cdsCursos.FieldByName( 'CU_REVISIO' ).AsString;
               end;
          end;
          FUltimoCurso := NewValue;
     end;
end;


procedure TdmRecursos.cdsHisCursosAlModificar(Sender: TObject);
begin
     dmCatalogos.RefrescaEntidades:= FALSE;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisCurso_DevEx, TEditHisCurso_DevEx );
     dmCatalogos.RefrescaEntidades:= TRUE;
end;

procedure TdmRecursos.cdsHisCursosNewRecord(DataSet: TDataSet);
begin
     with cdsHisCursos do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('KC_FEC_TOM').AsDateTime := dmCliente.FechaDefault;
          if dmCatalogos.cdsCursos.Locate( 'CU_CODIGO', FUltimoCurso, [] ) then
             FieldByName('CU_CODIGO').AsString  := FUltimoCurso
          else
          begin
               FieldByName('KC_HORAS').AsInteger  := 0;
               FieldByName('KC_EVALUA').AsInteger  := 0;
          end;
          FieldByName('KC_EST').AsString := VACIO;
          FieldByName('SE_FOLIO').AsInteger  := 0;
          with dmRecursos.GetDatosClasificacion( dmCliente.Empleado, dmCliente.FechaDefault ) do
          begin
               FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
               FieldByName( 'CB_TURNO' ).AsString := Turno;
               FieldByName( 'CB_PUESTO' ).AsString := Puesto;
               FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
               FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
               FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
               FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
               FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
               FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
               FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
               FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
               FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
               {$ifdef ACS}
               FieldByName( 'CB_NIVEL10' ).AsString := Nivel10;
               FieldByName( 'CB_NIVEL11' ).AsString := Nivel11;
               FieldByName( 'CB_NIVEL12' ).AsString := Nivel12;
               {$endif}
          end;
     end;
end;

procedure TdmRecursos.cdsHisCursosBeforePost(DataSet: TDataSet);
begin
     with cdsHisCursos do
     begin
          if ( FieldByName('KC_FEC_FIN').AsDateTime < FieldByName('KC_FEC_TOM').AsDateTime ) then
             DataBaseError( 'Fecha de Terminaci�n debe ser Mayor o Igual a Inicio' );
          if strVacio(FieldByName('CU_CODIGO').AsString) then
             DataBaseError( 'El C�digo del Curso no puede quedar vac�o' );
          FUltimoCurso := FieldByName('CU_CODIGO').AsString;
     end;
end;

procedure TdmRecursos.OnKC_FEC_TOMChange(Sender: TField);
begin
     cdsHisCursos.FieldByName('KC_FEC_FIN').AsDateTime := Sender.AsDateTime;
end;

{ cdsHisVacacion }
procedure TdmRecursos.cdsHisVacacionAlAdquirirDatos(Sender: TObject);
var
     DatosEmpleado : OleVariant;
     dFecha : TDate;
     oCursor: TCursor;
begin
     with dmCliente do
     begin
          with GetDatosEmpleadoActivo do
          begin
               if Activo then
                  dFecha := FechaDefault
               else
                  dFecha := Baja;
          end;
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  cdsHisVacacion.Data := ServerRecursos.GetHisVacacion( Empresa, Empleado,
                                                                        dFecha, DatosEmpleado );
               finally
                  Cursor := oCursor;
               end;
          end;
     end;
     cdsEmpVacacion.Data := DatosEmpleado;
     if Global.GetGlobalBooleano(K_GLOBAL_VER_SALDO_VACACION) then
     begin
          cdsSaldoVacaciones.Refrescar;
     end;
end;

procedure TdmRecursos.cdsHisVacacionAlCrearCampos(Sender: TObject);
begin
     with cdsHisVacacion do
     begin
          ListaFija( 'VA_TIPO', lfTipoVacacion );
          {***(@am):No se habia agregado la mascara de la fecha desde 2014, se corrigue para vs 2015***}
          MaskFecha( 'VA_FEC_INI' );
          MaskHoras( 'VA_D_PAGO' );
          MaskHoras( 'VA_PAGO' );
          MaskHoras( 'VA_S_PAGO' );
          MaskHoras( 'VA_D_GOZO' );
          MaskHoras( 'VA_GOZO' );
          MaskHoras( 'VA_S_GOZO' );
          MaskHoras( 'VA_D_PRIMA' );
          MaskHoras( 'VA_P_PRIMA' );
          MaskHoras( 'VA_S_PRIMA' );

          FieldByName('VA_FEC_INI').OnChange := OnVA_FEC_INIChange;
          FieldByName('VA_FEC_FIN').OnChange := OnVA_FEC_FINChange;
          FieldByName('VA_TIPO').OnChange := OnVA_TIPOChange;
          FieldByName('VA_GOZO').OnChange := OnVA_GOZOChange;
          FieldByName('VA_OTROS').OnChange := OnVA_OTROSChange;
          FieldByName('VA_P_PRIMA').OnChange  := OnVA_OTROSChange;
          FieldByName('VA_PAGO').OnChange  := OnVA_PAGOChange;
          FieldByName ('VA_NOMTIPO').OnChange := validacionTipoNominaChange;

          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmRecursos.GetSaldosVacacion;
begin
     with dmCliente do
          FSaldosVaca := ServerRecursos.GetSaldosVacacion( Empresa, Empleado, cdsHisVacacion.FieldByName( 'VA_FEC_INI' ).AsDateTime );
end;

procedure TdmRecursos.GetSaldoVacacionBaja;
begin
     with dmCliente.cdsEmpleado do
     begin
          if not ( FieldByName('CB_DER_FEC').AsDateTime > FieldByName('CB_FEC_BAJ').AsDateTime ) then
              cdsHisVacacion.FieldByName('VA_FEC_INI').AsDateTime:= dmCliente.cdsEmpleado.FieldByName('CB_FEC_BAJ').AsDateTime;
     end;
end;


procedure TdmRecursos.OnVA_FEC_INIChange(Sender: TField);
{
var
   dGozo : Currency;
}
begin
    with cdsHisVacacion do
    begin
         if ( State = dsInsert ) then    // Esta Insertando y se solicitaran los saldos
         begin
              GetSaldosVacacion;
              if eTipoVacacion(FieldByName('VA_TIPO').AsInteger) = tvVacaciones then
              begin
{
                   dGozo := Trunc( FSaldosVaca[ 0 ] );
                   if ( FieldByName( 'VA_GOZO' ).AsFloat <> dGozo ) then
                      FieldByName( 'VA_GOZO' ).AsFloat := dGozo
                   else
                      OnVA_GOZOChange( Sender );
}
                   FieldByName( 'CB_TABLASS' ).AsString := FSaldosVaca[ K_VACA_TABLA_PREST ];
                   FieldByName( 'CB_SALARIO' ).AsFloat := FSaldosVaca[ K_VACA_SAL_DIARIO ];
                   FieldByName( 'VA_TASA_PR' ).AsFloat := FSaldosVaca[ K_VACA_PRIMA_VACA ];
                   { VA_PAGO va al final para que el OnChange Recalcule Todo }
                   //FieldByName( 'VA_PAGO' ).AsFloat    := FSaldosVaca[ 1 ];
              end
              else
              begin
                   FieldByName( 'VA_D_GOZO' ).AsFloat := FSaldosVaca[ K_VACA_PAGO_PROP ] + FSaldosVaca[ K_VACA_PAGO_PEN ];
                   FieldByName( 'VA_D_PAGO' ).AsFloat := FSaldosVaca[ K_VACA_PAGO_PROP ] + FSaldosVaca[ K_VACA_PAGO_PEN ];
                   FieldByName( 'VA_D_PRIMA' ).AsFloat := FSaldosVaca[ K_VACA_DPRIMA_PROP ] + FSaldosVaca[ K_VACA_PRIMA_PEN ];

              end;
              RecalculaVacaDiasGozados;
         end;
    end;
end;

procedure TdmRecursos.OnVA_FEC_FINChange(Sender: TField);
begin
     if ( Sender.DataSet.State = dsInsert ) and FVacaCalculaDiasGozados then
        RecalculaVacaDiasGozados;
end;

procedure TdmRecursos.OnVA_OTROSChange(Sender: TField);
begin
    RecalculaMontoVaca;
end;

procedure TdmRecursos.OnVA_TIPOChange(Sender: TField);
begin
     with dmCliente, cdsHisVacacion do
     begin
          if eTipoVacacion(FieldByName('VA_TIPO').AsInteger) = tvCierre  then
          begin
               FieldByName('VA_FEC_INI').AsDateTime :=  NextDate( cdsEmpleado.FieldByName( 'CB_FEC_ANT' ).AsDateTime, FechaDefault );
               FieldByName('VA_GOZO').AsFloat := 0;
               //FieldByName('VA_PAGO').AsFloat := 0;    // Lo har� el OnChange de VA_GOZO
          end
          else
          begin
               FieldByName( 'VA_FEC_INI' ).AsDateTime:= FechaDefault;
               FieldByName( 'VA_FEC_FIN' ).AsDateTime:= FechaDefault;     // Comienzan siendo iguales
          end;
     end;
end;

procedure TdmRecursos.OnVA_GOZOChange(Sender: TField);
begin

     with cdsHisVacacion do
     begin
          if PuedePagarVacaciones( FieldByName('VA_NOMNUME').AsInteger ) then
             FieldByName('VA_PAGO').AsFloat := FieldByName('VA_GOZO').AsFloat;
{
          if ( eTipoVacacion(FieldByName('VA_TIPO').AsInteger) = tvVacaciones ) then
             FieldByName('VA_FEC_FIN').AsDateTime := FieldByName('VA_FEC_INI').AsDateTime + FieldByName('VA_GOZO').AsFloat;
}
     end;
end;

function TdmRecursos.PuedePagarVacaciones(const Periodo:Integer) : Boolean;
begin
     dmCatalogos.cdsPeriodo.Conectar;
     if dmCatalogos.cdsPeriodo.Locate('PE_NUMERO',Periodo,[] ) then
        Result :=  ( ZAccesosMgr.CheckDerecho( D_EMP_EXP_VACA, K_DERECHO_SIST_KARDEX )) or ( dmCatalogos.cdsPeriodo.FieldByName('PE_STATUS').AsInteger < Ord(spAfectadaParcial) )
     else
         Result := ( Periodo = 0 );
end;

procedure TdmRecursos.OnVA_PAGOChange(Sender: TField);
begin
     if VarIsNull( FSaldosVaca ) then
           GetSaldosVacacion;
     if Global.GetGlobalBooleano ( K_GLOBAL_PAGO_PRIMA_VACA_ANIV ) then
        RecalculaMontoVaca
     else
        FToolsRH.SetDiasPrima( FSaldosVaca, cdsHisVacacion );
end;

procedure TdmRecursos.RecalculaVacaDiasGozados;
{
var
   dInicio, dFinal: TDate;
   rGozo: TPesos;
}
begin
     if ( eTipoVacacion( cdsHisVacacion.FieldByName('VA_TIPO').AsInteger ) = tvVacaciones ) then
        FToolsRH.RecalculaVacaDiasGozados( cdsHisVacacion,
                                           eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ),
                                           dmAsistencia.GetVacaDiasGozar );
{
     rGozo := 0;                // Default es cero
     with cdsHisVacacion do
     begin
          if ( eTipoVacacion( FieldByName('VA_TIPO').AsInteger ) = tvVacaciones ) then
          begin
               dInicio := FieldByName( 'VA_FEC_INI' ).AsDateTime;
               dFinal  := FieldByName( 'VA_FEC_FIN' ).AsDateTime;
               if ( dFinal > dInicio ) then
               begin
                    if ( eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ) = dvNoUsar ) then
                       rGozo := ( dFinal - dInicio )
                    else
                       rGozo := dmAsistencia.GetVacaDiasGozar( FieldByName( 'CB_CODIGO' ).AsInteger, dInicio, dFinal );
end;
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'VA_GOZO' ).AsFloat := rGozo;
               //FieldByName( 'VA_PAGO' ).AsFloat := rGozo;   // Lo har� el OnChange de VA_GOZO
          end;
     end;
}
end;

procedure TdmRecursos.RecalculaVacaFechaRegreso;
{
var
   dInicio, dFinal: TDate;
   rGozo: TPesos;
   rDiasRango: Double;
}
begin
     if ( eTipoVacacion( cdsHisVacacion.FieldByName('VA_TIPO').AsInteger ) = tvVacaciones ) then
     begin
          FVacaCalculaDiasGozados := FALSE;
          try
             FToolsRH.RecalculaVacaFechaRegreso( cdsHisVacacion,
                                                 eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ),
                                                 dmAsistencia.GetVacaFechaRegreso );
          finally
                 FVacaCalculaDiasGozados := TRUE;
          end;
     end;
{
     with cdsHisVacacion do
     begin
          if ( eTipoVacacion( FieldByName('VA_TIPO').AsInteger ) = tvVacaciones ) then
          begin
               dInicio := FieldByName( 'VA_FEC_INI' ).AsDateTime;
               rGozo   := FieldByName( 'VA_GOZO' ).AsFloat;
               dFinal  := dInicio;                // Default es la Fecha Inicial
               if ( rGozo > 0 ) then
               begin
                    if ( eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ) = dvNoUsar ) then
                    begin
                         rDiasRango := Trunc( rGozo );            // D�as Calendario: Dias Gozo es igual al rango en D�as
                         dFinal := ( dInicio + rDiasRango );
                    end
                    else
                       dFinal := dmAsistencia.GetVacaFechaRegreso( FieldByName( 'CB_CODIGO' ).AsInteger, dInicio, rGozo, rDiasRango );
end;
               if ( dFinal <> FieldByName( 'VA_FEC_FIN' ).AsDateTime ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FVacaCalculaDiasGozados := FALSE;
                    try
                       FieldByName( 'VA_FEC_FIN' ).AsDateTime := dFinal;
                    finally
                           FVacaCalculaDiasGozados := TRUE;
                    end;
               end;
               if ( rGozo <> rDiasRango ) then
                  zWarning( 'Rec�lculo de Fecha de Regreso', '� El C�lculo De La Fecha De Regreso No Es Exacto Contra Los D�as Gozados ! ' + CR_LF + CR_LF +
                                                             Format( 'Se Ajust� La Fecha De Regreso Hacia Abajo, Que Corresponde a %3.2f D�as Gozados.', [ rDiasRango ] ) + CR_LF +
                                                             'Verifique Los Valores Del Per�odo De Vacaciones' + CR_LF, ZetaCommonClasses.Configurando_el_valor_de_Cada_Dia_de_Vacaciones, mbOK );
          end;
     end;
}
end;

procedure TdmRecursos.SetSaldosVacaciones( const lInfoAniversario: Boolean; var rGozo, rPago, rPrima: TPesos );
begin
     FToolsRH.SetDiasSaldosVacaciones( FSaldosVaca, lInfoAniversario, rGozo, rPago, rPrima );
end;

procedure TdmRecursos.RecalculaMontoVaca;
{
var
    nMonto, nSeptimoDia, nPrima : Currency;
}
begin
{
    with cdsHisVacacion do
    begin
}
        { S�lo se usa en ALTA y en EDICION cuando cambia VA_PAGO }
        if VarIsNull( FSaldosVaca ) then
           GetSaldosVacacion;
        FToolsRH.SetMontosVaca( FSaldosVaca, cdsHisVacacion );
{
        nMonto := Redondea( FieldByName( 'VA_PAGO' ).AsFloat * FieldByName( 'CB_SALARIO' ).AsFloat );
        if ( FSaldosVaca[ K_VACA_SEPTIMO_DIA ] ) then    // Se paga el 7o. Dia?
            nSeptimoDia := nMonto / 6
        else
            nSeptimoDia := 0;
        if ( FSaldosVaca[ K_VACA_SEPTIMO_PRIMA ] ) then    // Se paga la prima sobre 7o. Dia?
            nPrima := ( nMonto + nSeptimoDia ) * FieldByName( 'VA_TASA_PR' ).AsFloat / 100
        else
            nPrima := nMonto * FieldByName( 'VA_TASA_PR' ).AsFloat / 100;

        FieldByName( 'VA_MONTO' ).AsFloat := nMonto;
        FieldByName( 'VA_SEVEN' ).AsFloat := nSeptimoDia;
        FieldByName( 'VA_PRIMA' ).AsFloat := nPrima;
        FieldByName( 'VA_TOTAL' ).AsFloat := nMonto + nSeptimoDia + nPrima + FieldByName( 'VA_OTROS' ).AsFloat;
end;
}
end;

procedure TdmRecursos.cdsHisVacacionAlModificar(Sender: TObject);
begin
     FCambioVacaciones := FALSE;
     with cdsHisVacacion do
     begin
          { Obliga a solicitar FSaldosVaca cuando se modifique VA_PAGO }
          if ( State <> dsInsert ) then
             FSaldosVaca := NULL;

          if ( FieldByName( 'VA_TIPO' ).AsInteger  = Ord( tvVacaciones ) ) then
             ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisVacacion_DevEx, TEditHisVacacion_DevEx )
          else ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisCierre_DevEx, TEditHisCierre_DevEx );

     end;
     RefrescaKardex( K_T_VACA );
     if FCambioVacaciones then
        cdsHisVacacion.Refrescar;
end;

procedure TdmRecursos.cdsHisVacacionNewRecord(DataSet: TDataSet);
var
TipoNomina: eTipoPeriodo;
NominaSugerir: Integer;
begin
     with cdsHisVacacion, dmCliente do
     begin
          FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
          FieldByName( 'VA_TIPO' ).AsInteger    := Ord( tvVacaciones );
          //FieldByName( 'VA_FEC_INI' ).AsDateTime:= FechaDefault;      ... Se Agrega en onChange de VA_TIPO
          FieldByName( 'VA_NOMYEAR' ).AsInteger  := YearDefault;
          TipoNomina:= eTipoPeriodo( cdsEmpleado.FieldByName('CB_NOMINA').AsInteger );
          FieldByName( 'VA_NOMTIPO' ).AsInteger  := Ord( TipoNomina );
          if TipoNomina <> dmCliente.PeriodoTipo then
             NominaSugerir := GetPeriodoInicialTipoNomina( TipoNomina ,True )
          else
              NominaSugerir := dmCliente.PeriodoNumero;
          FieldByName( 'VA_NOMNUME' ).AsInteger  := NominaSugerir;
          if (  not ZAccesosMgr.CheckDerecho( D_EMP_EXP_VACA, K_DERECHO_SIST_KARDEX ) )then
          begin
               if cdsPeriodo.Locate('PE_NUMERO',NominaSugerir,[] ) then
               begin
                    if ( cdsPeriodo.FieldByName('PE_STATUS').AsInteger >= Ord(spAfectadaParcial) )then
                    begin
                         FieldByName( 'VA_NOMNUME' ).AsInteger := 0
                    end;
               end
               else
                   FieldByName( 'VA_NOMNUME' ).AsInteger := 0;
          end;
          FieldByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
          FieldByName('VA_AJUSTE').AsString:= K_GLOBAL_NO;
     end;
end;

// DES US #17300: Bombardier Aerospace M�xico-En Kardex de empleado
// no se visualizan datos de los registros de Vacaciones anteriores cuando se reingresa al empleado
procedure TdmRecursos.cdsHisVacacionSoloLecturaAlBorrar(Sender: TObject);
begin
     with cdsHisVacacionSoloLectura do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
          begin
               Delete;
               Enviar;
               RefrescaKardex( K_T_VACA );
               Refrescar;
          end;
     end;
end;

// DES US #17300: Bombardier Aerospace M�xico-En Kardex de empleado
// no se visualizan datos de los registros de Vacaciones anteriores cuando se reingresa al empleado
procedure TdmRecursos.cdsHisVacacionSoloLecturaAlCrearCampos(Sender: TObject);
begin
     with cdsHisVacacionSoloLectura do
     begin
          ListaFija( 'VA_TIPO', lfTipoVacacion );
          {***(@am):No se habia agregado la mascara de la fecha desde 2014, se corrigue para vs 2015***}
          MaskFecha( 'VA_FEC_INI' );
          MaskHoras( 'VA_D_PAGO' );
          MaskHoras( 'VA_PAGO' );
          MaskHoras( 'VA_S_PAGO' );
          MaskHoras( 'VA_D_GOZO' );
          MaskHoras( 'VA_GOZO' );
          MaskHoras( 'VA_S_GOZO' );
          MaskHoras( 'VA_D_PRIMA' );
          MaskHoras( 'VA_P_PRIMA' );
          MaskHoras( 'VA_S_PRIMA' );      

          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

// DES US #17300: Bombardier Aerospace M�xico-En Kardex de empleado
// no se visualizan datos de los registros de Vacaciones anteriores cuando se reingresa al empleado
procedure TdmRecursos.cdsHisVacacionSoloLecturaAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisVacacionSoloLectura do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               Reconcile ( ServerRecursos.GrabaHisVacacion( dmCliente.Empresa, Delta, FALSE, ErrorCount ) );
               if ErrorCount = 0 then
               begin
                    FRefrescaHisKardex := TRUE;
                    FCambioVacaciones  := TRUE;
               end;
          end;
     end
end;

procedure TdmRecursos.cdsHisVacacionAlBorrar(Sender: TObject);
var
   iYear:Integer;
   TipoPer :eTipoPeriodo;
   bRestringir:Boolean;
   lFiltered: Boolean;
begin
     bRestringir := False;
     with cdsHisVacacion do
     begin
          if ( ( not ZAccesosMgr.CheckDerecho( D_EMP_EXP_VACA, K_DERECHO_SIST_KARDEX ) ) and ( FieldByName( 'VA_TIPO' ).AsInteger = Ord( tvVacaciones ) ) and ( FieldByName( 'VA_NOMNUME' ).AsInteger > 0 )  )then
          begin
               iYear := FieldByName( 'VA_NOMYEAR' ).AsInteger;
               TipoPer := eTipoPeriodo( FieldByName( 'VA_NOMTIPO' ).AsInteger );
               with dmCatalogos do
               begin
                    {*** ALCOM-El sistema permite borrar registros de vacaciones en n�minas afectadas ***}
                    lFiltered := cdsPeriodo.Filtered;
                    cdsPeriodo.Filtered := False;
                    {*** FIN ***}
                    GetDatosPeriodo(iYear,TipoPer);
                    if cdsPeriodo.Locate('PE_NUMERO',FieldByName( 'VA_NOMNUME' ).AsInteger,[] )then
                       bRestringir := cdsPeriodo.FieldByName('PE_STATUS').AsInteger >= Ord(spAfectadaParcial);
                    {*** ALCOM-El sistema permite borrar registros de vacaciones en n�minas afectadas ***}
                    cdsPeriodo.Filtered := lFiltered;
                    {*** FIN ***}
               end;

          end;

          if not bRestringir then
          begin
                if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
                begin
                     Delete;
                     Enviar;
                     RefrescaKardex( K_T_VACA );
                     Refrescar;
                end;
          end
          else
              ZWarning ( 'Al borrar Vacaciones','No tienes derecho a Borrar Vacaciones Pagadas',0,mbOK); 
     end;
end;

procedure TdmRecursos.cdsHisVacacionBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if( FieldByName( 'VA_TIPO' ).AsInteger = Ord( tvVacaciones ) )then
          begin
               if( FieldByName('VA_FEC_INI').AsDateTime > FieldByName('VA_FEC_FIN').AsDateTime )then
                   DataBaseError( 'La fecha de inicio debe de ser menor � igual a la de fecha de regreso' );
          end;
          FieldByName( 'VA_CAPTURA' ).AsDateTime:= Date;
          FieldByName( 'US_CODIGO' ).AsInteger  := dmCliente.Usuario;
          if ( eTipoVacacion( FieldByName( 'VA_TIPO' ).AsInteger ) = tvCierre ) then            // Asegura que Fecha Final sea Nula en caso de cierres
             FieldByName( 'VA_FEC_FIN' ).Clear;
     end;
end;

procedure TdmRecursos.cdsHisVacacionBeforeDelete(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if ( eTipoVacacion( FieldByName( 'VA_TIPO' ).AsInteger ) = tvCierre ) and
             ( FieldByName( 'VA_FEC_INI' ).AsDateTime < dmCliente.cdsEmpleado.FieldByName( 'CB_DER_FEC' ).AsDateTime ) then
             DataBaseError( 'No se permite borrar cierres antes de:' + FechaCorta( dmCliente.cdsEmpleado.FieldByName( 'CB_DER_FEC' ).AsDateTime ) );
     end;
end;

procedure TdmRecursos.cdsHisVacacionAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisVacacion do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               Reconcile ( ServerRecursos.GrabaHisVacacion( dmCliente.Empresa, Delta, FALSE, ErrorCount ) );
               if ErrorCount = 0 then
               begin
                    FRefrescaHisKardex := TRUE;
                    FCambioVacaciones  := TRUE;
               end;
          end;
     end;
end;

{ cdsEmpVacacion }

procedure TdmRecursos.cdsEmpVacacionAlCrearCampos(Sender: TObject);
begin
     with cdsEmpVacacion do
     begin
          // Dias Pagados
          MaskPesos( 'CB_DER_PAG' );
          MaskPesos( 'CB_V_PAGO' );
          MaskPesos( 'CB_SUB_PAG' );
          MaskPesos( 'CB_PRO_PAG' );
          MaskPesos( 'CB_TOT_PAG' );
          // Dias Gozados
          MaskPesos( 'CB_DER_GOZ' );
          MaskPesos( 'CB_V_GOZO' );
          MaskPesos( 'CB_SUB_GOZ' );
          MaskPesos( 'CB_PRO_GOZ' );
          MaskPesos( 'CB_TOT_GOZ' );
          // Dias Prima
          MaskPesos( 'CB_DER_PV' );
          MaskPesos( 'CB_V_PRIMA' );
          MaskPesos( 'CB_SUB_PV' );
          MaskPesos( 'CB_PRO_PV' );
          MaskPesos( 'CB_TOT_PV' );
          // Fechas
          MaskFecha( 'CB_FEC_VAC' );
          MaskFecha( 'CB_DER_FEC' );
          MaskFecha( 'CB_FEC_ANT' );
          FieldByName('CB_TABLASS').OnGetText := cdsEmpVacacionesCB_TABLASSOnGetText;
          
     end;
end;

procedure TdmRecursos.cdsEmpVacacionesCB_TABLASSOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with dmCatalogos.cdsSSocial do
     begin
          Conectar;
          Text := Sender.AsString + ' : '+ GetDescripcion(Sender.AsString);
     end;
end;

{ cdsIdentFoto }

procedure TdmRecursos.cdsIdentFotoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsIdentFoto.Data := ServerRecursos.GetIdentFoto( Empresa, Empleado );
end;

{ cdsEmpParientes }

procedure TdmRecursos.cdsEmpParientesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsEmpParientes.Data := ServerRecursos.GetEmpParientes( Empresa, Empleado );
end;

procedure TdmRecursos.cdsEmpParientesAlCrearCampos(Sender: TObject);
begin
     with cdsEmpParientes do
     begin
          ListaFija( 'PA_RELACIO', lfTipoPariente );
          FieldByName( 'PA_FEC_NAC' ).OnGetText:= PA_FEC_NACGetText;
          FieldByName( 'PA_NOMBRES' ).OnChange := OnChangeDatosPariente;
          FieldByName( 'PA_APE_PAT' ).OnChange := OnChangeDatosPariente;
          FieldByName( 'PA_APE_MAT' ).OnChange := OnChangeDatosPariente;
     end;
end;

procedure TdmRecursos.PA_FEC_NACGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.AsDateTime = 0 then
          Text := ''
     else
          Text := IntToStr( Trunc( DaysToYears( Trunc( Now - Sender.AsDateTime ) ) ) );
end;

procedure TdmRecursos.OnChangeDatosPariente(Sender: TField);
begin
     with cdsEmpParientes do
     begin
          FieldByName('PA_NOMBRE').AsString := FieldByName('PA_NOMBRES').AsString +' '+ FieldByName('PA_APE_PAT').AsString +' '+ FieldByName('PA_APE_MAT').AsString;
     end;
end;


procedure TdmRecursos.cdsEmpParientesNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'CB_CODIGO' ).AsInteger := dmCliente.Empleado;
          FieldByName( 'PA_RELACIO' ).AsInteger:= 0;
          FieldByName( 'PA_FOLIO' ).AsInteger  := 1;
          FieldByName( 'PA_SEXO' ).AsString    := 'M';
          FieldByName( 'PA_FEC_NAC' ).AsDateTime := dmCliente.FechaDefault;
     end;
end;

procedure TdmRecursos.cdsEmpParientesAlModificar(Sender: TObject);
begin
     with dmCliente do
     begin
          SetTipoLookupEmpleado( eLookEmpMedicos ); //permite Traer la fecha de nacimiento del Empleado
          try
              ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpPariente_DevEx, TEditEmpPariente_DevEx );
          finally
                 SetTipoLookupEmpleado( eLookEmpGeneral );
          end;
     end;
end;

procedure TdmRecursos.cdsEmpParientesAfterPost(DataSet: TDataSet);
var
     ErrorCount : Integer;
begin
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
          begin
               Reconcile ( ServerRecursos.GrabaEmpParientes( dmCliente.Empresa, Delta, ErrorCount ) );
               cdsHisSGM.SetDataChange;
          end;
     end;
end;

procedure TdmRecursos.cdsEmpParientesBeforePost(DataSet: TDataSet);

     function CambioCampoSGM(const oField:TField):Boolean;
     begin
          with oField,DataSet do
          begin
               Result := ( OldValue <> NewValue );
          end;
     end;
begin
     with DataSet do
     begin
          if ( StrLleno(FieldByName('PA_APE_MAT').AsString ) ) and ( StrVacio(FieldByName('PA_APE_PAT').AsString) OR StrVacio(FieldByName('PA_NOMBRES').AsString ) ) then
          begin
               FieldByName('PA_NOMBRES').FocusControl;
               DatabaseError('Capturar Datos Completos del Pariente');
          end;

          if ( StrLleno( FieldByName('PA_APE_PAT').AsString ) )   and ( StrVacio( FieldByName('PA_NOMBRES').AsString ) ) then
          begin
               FieldByName('PA_NOMBRES').FocusControl;
               DatabaseError('Capturar Nombres del Pariente');
          end;

          if ( StrLleno( FieldByName('PA_NOMBRES').AsString ) )  and ( StrVacio(FieldByName('PA_APE_PAT').AsString ) ) then
          begin
               FieldByName('PA_APE_PAT').FocusControl;
               DatabaseError('Capturar Apellido Paterno del Pariente');
          end;

          {if ( CambioCampoSGM( FieldByName('PA_RELACIO') ) ) and ( ParienteTieneSGM( FieldByName('PA_FOLIO').AsInteger ,FieldByName('PA_RELACIO').OldValue,FieldByName('CB_CODIGO').AsInteger ) ) then
          begin
               DataBaseError('No se Puede Cambiar Tipo de Parentezco del Pariente, tiene asignado un Seguro de Gastos M�dicos');
          end;
          if ( CambioCampoSGM(FieldByName('PA_FOLIO') ) and Not CambioCampoSGM( FieldByName('PA_RELACIO') ) and ( ParienteTieneSGM(FieldByName('PA_FOLIO').OldValue ,FieldByName('PA_RELACIO').AsInteger,FieldByName('CB_CODIGO').AsInteger ) ) )then
          begin
               DataBaseError('No se Puede Cambiar el Orden del Pariente, tiene asignado un Seguro de Gastos M�dicos');
          end;}
     end;
end;



{ cdsEmpAntesCur }

procedure TdmRecursos.cdsEmpAntesCurAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsEmpAntesCur.Data := ServerRecursos.GetEmpAntesCur( Empresa, Empleado );
end;

procedure TdmRecursos.cdsEmpAntesCurAlCrearCampos(Sender: TObject);
begin
     with cdsEmpAntesCur do
     begin
          MaskFecha( 'AR_FECHA' );
     end;
end;

procedure TdmRecursos.cdsEmpAntesCurAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpAntesCur_DevEx, TEditEmpAntesCur_DevEx ) ;
end;

procedure TdmRecursos.cdsEmpAntesCurBeforeInsert(DataSet: TDataSet);
begin
     with cdsEmpAntesCur do
     begin
          DisableControls;
          IndexFieldNames:= 'AR_FOLIO';
          Last;
          FUltimoAntesCur:= FieldByName( 'AR_FOLIO' ).AsInteger;
          EnableControls;
     end;
end;

procedure TdmRecursos.cdsEmpAntesCurNewRecord(DataSet: TDataSet);
begin
     with cdsEmpAntesCur, dmCliente do
     begin
          FieldByName( 'AR_FOLIO' ).AsInteger:= FUltimoAntesCur + 1;
          FieldByName( 'CB_CODIGO' ).AsInteger:= Empleado;
          FieldByName( 'AR_FECHA' ).AsDateTime := FechaDefault;
     end;
end;

procedure TdmRecursos.cdsEmpAntesCurAfterPost(DataSet: TDataSet);
var
     ErrorCount : Integer;
begin
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
             Reconcile ( ServerRecursos.GrabaEmpAntesCur( dmCliente.Empresa, Delta, ErrorCount ) );
     end;
end;

{ cdsEmpAntesPto }

procedure TdmRecursos.cdsEmpAntesPtoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsEmpAntesPto.Data := ServerRecursos.GetEmpAntesPto( Empresa, Empleado);
end;

procedure TdmRecursos.cdsEmpAntesPtoAlCrearCampos(Sender: TObject);
begin
     with cdsEmpAntesPto do
     begin
          MaskFecha( 'AP_FEC_INI' );
          MaskFecha( 'AP_FEC_FIN' );
     end;
end;

procedure TdmRecursos.cdsEmpAntesPtoAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpAntesPto_DevEx, TEditEmpAntesPto_DevEx )
end;

procedure TdmRecursos.cdsEmpAntesPtoBeforeInsert(DataSet: TDataSet);
begin
     with cdsEmpAntesPto do
     begin
          DisableControls;
          IndexFieldNames:= 'AP_FOLIO';
          Last;
          FUltimoAntesPto:= FieldByName( 'AP_FOLIO' ).AsInteger;
          EnableControls;
     end;
end;

procedure TdmRecursos.cdsEmpAntesPtoNewRecord(DataSet: TDataSet);
begin
     with cdsEmpAntesPto, dmCliente do
     begin
          FieldByName( 'AP_FOLIO' ).AsInteger := FUltimoAntesPto + 1;
          FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
          FieldByName( 'AP_FEC_INI' ).AsDateTime := FechaDefault;
          FieldByName( 'AP_FEC_FIN' ).AsDateTime := FechaDefault;
     end;
end;

procedure TdmRecursos.cdsEmpAntesPtoAfterPost(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
             Reconcile ( ServerRecursos.GrabaEmpAntesPto( dmCliente.Empresa, Delta, ErrorCount ) );
     end;
end;

{ cdsEmpFoto }

procedure TdmRecursos.cdsEmpFotoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsEmpFoto.Data := ServerRecursos.GetEmpFoto( Empresa, Empleado );
end;

procedure TdmRecursos.cdsEmpFotoAlModificar(Sender: TObject);
begin
     {$IFDEF TRESS_DELPHIXE5_UP}
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpFoto_DevEx, TEditEmpFoto_DevEx );
     {$ELSE}
     ZBaseEdicion.ShowFormaEdicion( EditEmpFoto, TEditEmpFoto )
     {$ENDIF}
end;

procedure TdmRecursos.cdsEmpFotoBeforeInsert(DataSet: TDataSet);
begin
     FTieneFoto:= cdsEmpFoto.Locate( 'IM_TIPO', K_FOTO, [] );
end;

procedure TdmRecursos.cdsEmpFotoNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'CB_CODIGO' ).AsInteger:= dmcliente.Empleado;
          if FTieneFoto then
             FieldByName( 'IM_TIPO' ).AsString := ''
          else
             FieldByName( 'IM_TIPO' ).AsString := K_FOTO;
     end;
end;

procedure TdmRecursos.cdsEmpFotoAfterPost(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
             if Reconcile ( ServerRecursos.GrabaEmpFoto( dmCliente.Empresa, Delta, ErrorCount ) ) then
                cdsIdentFoto.SetDataChange;
     end;
end;

procedure TdmRecursos.ObtenerFotografia;
const
     K_MENSAJE = 'No Tiene Permiso Para %s Registros';
var
   iDerecho: Integer;
   sVerbo: String;
begin
     with cdsEmpFoto do
     begin
          if IsEmpty then //Es Alta
          begin
               iDerecho := K_DERECHO_ALTA;
               sVerbo := 'Agregar';
          end
          else
          begin
               iDerecho := K_DERECHO_CAMBIO;
               sVerbo := 'Modificar';
          end;
          if( CheckDerecho( D_EMP_CURR_FOTO, iDerecho ) )then
          begin
               if( iDerecho = K_DERECHO_ALTA )then
                  Insert
               else
                   Edit;
               try
                  if CapturarImagen( dmCliente.GetEmpleadoDescripcion ) then
                     Post
                  else
                      Cancel;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                          Application.HandleException( Error );
                     end;
               end;
          end
          else
              ZInformation( 'Capturar Imagen', Format( K_MENSAJE, [ sVerbo ] ), 0 );
     end;
end;

{ cdsGridCursos }

procedure TdmRecursos.cdsGridCursosAlAdquirirDatos(Sender: TObject);
begin
     cdsGridCursos.Data := ServerRecursos.GetGridCursos( dmCliente.Empresa );
end;

procedure TdmRecursos.cdsGridCursosBeforeOpen(DataSet: TDataSet);
begin
     FUltimaEval := 0;
end;

procedure TdmRecursos.cdsGridCursosAlCrearCampos(Sender: TObject);
begin
     with cdsGridCursos do
     begin
          MaskHoras( 'KC_HORAS' );
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsGridCursosCB_CODIGOChange;
          FieldByName( 'KC_EVALUA' ).OnValidate := cdsGridCursosKC_EVALUAValidate;
     end;
end;

procedure TdmRecursos.cdsGridCursosBeforePost(DataSet: TDataSet);
begin
     with cdsGridCursos do
     begin
          with FieldByName( 'CB_CODIGO' ) do
          begin
               if ( AsInteger = 0 ) then
               begin
                    FocusControl;
                    DataBaseError( 'N�mero de Empleado no Puede Quedar Vac�o' );
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsGridCursosAfterPost(DataSet: TDataSet);
begin
     with cdsGridCursos do
          FUltimaEval := FieldByName( 'KC_EVALUA' ).AsFloat;
end;

procedure TdmRecursos.cdsGridCursosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TZetaClientDataSet( Sender ) do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
             if Reconciliar( ServerRecursos.GrabaGridCursos( dmCliente.Empresa, Delta, ErrorCount ) ) then
             begin
                  FCambioMatrizCursos := True;
                  if ( TZetaClientDataSet( Sender ).Name = 'cdsGridCursos' ) then
                     TressShell.SetDataChange( [ enKarCurso, enCurProg ] )
                  else
                     TressShell.SetDataChange( [ enCurProg ] );
             end;
     end;
end;

{$ifdef VER130}
procedure TdmRecursos.cdsGridCursosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
    sError : String;
begin
    if PK_Violation( E ) then
      with DataSet do
        sError := Format( 'El curso ya fu� registrado.' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s',
                    [ FieldByName( 'CB_CODIGO' ).AsInteger, FechaCorta( FieldByName( 'KC_FEC_TOM' ).AsDateTime ) ] )
    else
        sError := GetErrorDescription( E );
    Action := cdsGridCursos.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO', sError );
end;
{$else}
procedure TdmRecursos.cdsGridCursosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
    sError : String;
begin
    if PK_Violation( E ) then
      with DataSet do
        sError := Format( 'El curso ya fu� registrado.' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s',
                    [ FieldByName( 'CB_CODIGO' ).AsInteger, FechaCorta( FieldByName( 'KC_FEC_TOM' ).AsDateTime ) ] )
    else
        sError := GetErrorDescription( E );
    cdsGridCursos.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO', sError );
    Action := raCancel;
end;
{$endif}

procedure TdmRecursos.cdsGridCursosCB_CODIGOChange(Sender: TField);
begin
     with cdsGridCursos, dmCliente do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
          FieldByName( 'CB_TURNO' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_TURNO' ).AsString;
          FieldByName( 'CB_PUESTO' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_PUESTO' ).AsString;
          FieldByName( 'CB_CLASIFI' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_CLASIFI' ).AsString;
          FieldByName( 'CB_NIVEL1' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL1' ).AsString;
          FieldByName( 'CB_NIVEL2' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL2' ).AsString;
          FieldByName( 'CB_NIVEL3' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL3' ).AsString;
          FieldByName( 'CB_NIVEL4' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL4' ).AsString;
          FieldByName( 'CB_NIVEL5' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL5' ).AsString;
          FieldByName( 'CB_NIVEL6' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL6' ).AsString;
          FieldByName( 'CB_NIVEL7' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL7' ).AsString;
          FieldByName( 'CB_NIVEL8' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL8' ).AsString;
          FieldByName( 'CB_NIVEL9' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL9' ).AsString;
          {$ifdef ACS}
          FieldByName( 'CB_NIVEL10').AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL10').AsString;
          FieldByName( 'CB_NIVEL11').AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL11').AsString;
          FieldByName( 'CB_NIVEL12').AsString := cdsEmpleadoLookup.FieldByName( 'CB_NIVEL12').AsString;
          {$endif}
          { Asigna Valores Default }
          if ( FieldByName( 'KC_EVALUA' ).AsFloat = 0 ) then
             FieldByName( 'KC_EVALUA' ).AsFloat:= FUltimaEval;
     end;
end;

procedure TdmRecursos.cdsGridCursosKC_EVALUAValidate(Sender: TField);
begin
     if ( Sender.AsFloat < 0 ) then
        DataBaseError( 'La evaluaci�n debe ser mayor � igual a cero' );
end;

{cdsBancaElec}

procedure TdmRecursos.EditarGridBancaElectronica;
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpBancaElec );
        ZBaseGridEdicion_DevEx.ShowGridEdicion( BancaElectronica_DevEx, TBancaElectronica_DevEx, TRUE );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmRecursos.EditarGridTarjetasGasolina;
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpBancaElec );
        ZBaseGridEdicion_DevEx.ShowGridEdicion( TarjetasGasolina_DevEx, TTarjetasGasolina_DevEx, TRUE );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;


procedure TdmRecursos.EditarGridTarjetasDespensa;
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpBancaElec );
        ZBaseGridEdicion_DevEx.ShowGridEdicion( TarjetasDespensa_DevEx, TTarjetasDespensa_DevEx, TRUE );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmRecursos.cdsBancaElecAlAdquirirDatos(Sender: TObject);
begin
     cdsBancaElec.Data := ServerRecursos.GetBancaElectronica( dmCliente.Empresa, TRUE,'CB_BAN_ELE' );
end;

procedure TdmRecursos.cdsBancaElecAlCrearCampos(Sender: TObject);
begin
     with cdsBancaElec do
     begin
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsBancaElecCB_CODIGOChange;
     end;
end;



procedure TdmRecursos.cdsBancaElecCB_CODIGOChange(Sender: TField);
begin
     with cdsBancaElec do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          FieldByName('CB_BAN_ELE').AsString := dmCliente.cdsEmpleadoLookup.FieldByName('CB_BAN_ELE').AsString;
          FieldByName('BANCAOLD').AsString := dmCliente.cdsEmpleadoLookup.FieldByName('CB_BAN_ELE').AsString;
     end;
end;

procedure TdmRecursos.DepuraCuentas(dsDataSet:TZetaClientDataSet;sCampo,sCampoOld:string);
begin
     with dsDataSet do
     begin
          First;
          while not Eof do
          begin
               if ( FieldByName(sCampo).AsString = FieldByName(sCampoOld).AsString ) then
                   Delete
               else
                   Next;
          end;
     end;
end;

procedure TdmRecursos.cdsBancaElecAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsBancaElec do
     begin
          if State in [ dsEdit, dsInsert ] then
               Post;
          DepuraCuentas(cdsBancaElec,'CB_BAN_ELE','BANCAOLD');
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerRecursos.GrabaBancaElectronica( dmCliente.Empresa, Delta, ErrorCount,'CB_BAN_ELE' ) );
               if ( ErrorCount = 0 ) then
                    TressShell.SetDataChange( [ enEmpleado ] );
          end;
     end;
end;

//Tarjeta de Despensa
procedure TdmRecursos.cdsTarjetasDespensaAlAdquirirDatos(Sender: TObject);
begin
     cdsTarjetasDespensa.Data := ServerRecursos.GetBancaElectronica( dmCliente.Empresa, TRUE,'CB_CTA_VAL');
end;

procedure TdmRecursos.cdsTarjetasDespensaAlCrearCampos(Sender: TObject);
begin
     with cdsTarjetasDespensa do
     begin
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsTarjetasDespensaCB_CODIGOChange;
     end;
end;

procedure TdmRecursos.cdsTarjetasDespensaCB_CODIGOChange(Sender: TField);
begin
     with cdsTarjetasDespensa do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          FieldByName('CB_CTA_VAL').AsString := dmCliente.cdsEmpleadoLookup.FieldByName('CB_CTA_VAL').AsString;
          FieldByName('CTAVALOLD').AsString := dmCliente.cdsEmpleadoLookup.FieldByName('CB_CTA_VAL').AsString;
     end;
end;

procedure TdmRecursos.cdsTarjetasDespensaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsTarjetasDespensa do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          DepuraCuentas(cdsTarjetasDespensa,'CB_CTA_VAL','CTAVALOLD');
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerRecursos.GrabaBancaElectronica( dmCliente.Empresa, Delta, ErrorCount,'CB_CTA_VAL' ) );
               if ( ErrorCount = 0 ) then
                  TressShell.SetDataChange( [ enEmpleado ] );
          end;
     end;
end;

//Tarjeta de Gasolina
procedure TdmRecursos.cdsTarjetasGasolinaAlAdquirirDatos(Sender: TObject);
begin
      cdsTarjetasGasolina.Data := ServerRecursos.GetBancaElectronica( dmCliente.Empresa, TRUE ,'CB_CTA_GAS');
end;

procedure TdmRecursos.cdsTarjetasGasolinaAlCrearCampos(Sender: TObject);
begin
      with cdsTarjetasGasolina do
     begin
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsTarjetasGasolinaCB_CODIGOChange;
     end;
end;

procedure TdmRecursos.cdsTarjetasGasolinaCB_CODIGOChange(Sender: TField);
begin
     with cdsTarjetasGasolina do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          FieldByName('CB_CTA_GAS').AsString := dmCliente.cdsEmpleadoLookup.FieldByName('CB_CTA_GAS').AsString;
          FieldByName('CTAGASOLD').AsString := dmCliente.cdsEmpleadoLookup.FieldByName('CB_CTA_GAS').AsString;
     end;
end;

procedure TdmRecursos.cdsTarjetasGasolinaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsTarjetasGasolina do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          DepuraCuentas(cdsTarjetasGasolina,'CB_CTA_GAS','CTAGASOLD');
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerRecursos.GrabaBancaElectronica( dmCliente.Empresa, Delta, ErrorCount,'CB_CTA_GAS' ) );
               if ( ErrorCount = 0 ) then
                  TressShell.SetDataChange( [ enEmpleado ] );
          end;
     end;
end;

{$ifdef VER130}
procedure TdmRecursos.cdsBancaElecReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     ZetaDialogo.ZError( 'Error',  GetErrorDescription( E ), 0 );
     Action := raCancel;    // En el Grid no se puede Abortar, deben cancelarse y reportar todos los errores
end;
{$else}
procedure TdmRecursos.cdsBancaElecReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     ZetaDialogo.ZError( 'Error',  GetErrorDescription( E ), 0 );
     Action := raCancel;    // En el Grid no se puede Abortar, deben cancelarse y reportar todos los errores
end;
{$endif}

{cdsInfonavit}

procedure TdmRecursos.EditarGridInfonavit;
begin
     try
          dmCliente.SetLookupEmpleado( eLookEmpInfonavit );
          //**La accion muestra esta forma esta deshabilitada en el shell de tress
        //**  ZBaseGridEdicion.ShowGridEdicion( CreditoInfonavit, TCreditoInfonavit, TRUE );
     finally
          dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmRecursos.cdsInfonavitAlAdquirirDatos(Sender: TObject);
begin
    cdsInfonavit.Data := ServerRecursos.GetCreditoInfonavit( dmCliente.Empresa, TRUE );
end;

procedure TdmRecursos.cdsInfonavitAlCrearCampos(Sender: TObject);
begin
     with cdsInfonavit do
     begin
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsInfonavitCB_CODIGOChange;
          ListaFija( 'CB_INFTIPO', lfTipoInfonavit );
          with FieldByName( 'CB_INFTASA' ) do
          begin
               OnGetText:= CB_INFTASAGetText;
               Alignment:= taLeftJustify;
          end;
          with FieldByName( 'CB_INF_OLD' ) do
          begin
               OnGetText:= CB_INFOLDGetText;
               Alignment:= taLeftJustify;
          end;
          FieldByName( 'CB_INFMANT' ).OnGetText := CB_INFMANTGetText;
          FieldByName( 'CB_INF_INI' ).OnGetText := CB_INF_INIGetText;
          FieldByName( 'CB_INF_ANT' ).OnGetText := CB_INF_INIGetText;
     end;
end;

procedure TdmRecursos.CB_INF_INIGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsDateTime = NULLDATETIME ) then
        Text := VACIO
     else
         Text := Sender.AsString;
end;

procedure TdmRecursos.CB_INFMANTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if zStrToBool( Sender.AsString ) then
        Text := 'S�'
     else
         Text := 'No';
end;

procedure TdmRecursos.CB_INFOLDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := FormatFloat( '##0.0####" %"', Sender.AsFloat );//Mascara := mnTasa;
end;

procedure TdmRecursos.CB_INFTASAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   TipoInfo: eTipoInfonavit;
begin
     TipoInfo := eTipoInfonavit(Sender.DataSet.FieldByName('CB_INFTIPO').AsInteger);
     case TipoInfo of
          tiNoTiene   : Text := FormatFloat( '#,0.00', Sender.AsFloat );//Mascara := mnPesos;
          tiPorcentaje: Text := FormatFloat( '##0.0####" %"', Sender.AsFloat );//Mascara := mnTasa;
          tiCuotaFija : Text := FormatFloat( '#,0.00', Sender.AsFloat );//Mascara := mnPesos;
          tiVeces     : Text := FormatFloat( '#0.00##" SMGDF"', Sender.AsFloat );//Mascara := mnVecesSMGDF;
          tiVecesUMA  : Text := FormatFloat( '#0.00##" UMA"', Sender.AsFloat );//Mascara := mnVecesUMA;
     else
         Text := Sender.AsString;
     end
end;

procedure TdmRecursos.cdsInfonavitCB_CODIGOChange(Sender: TField);
begin
     with cdsInfonavit do
     begin
          with dmCliente do
          begin
               FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
               FieldByName( 'CB_INFTIPO' ).AsInteger := cdsEmpleadoLookup.FieldByName('CB_INFTIPO').AsInteger;
               FieldByName('TIPO_OLD').AsInteger := cdsEmpleadoLookup.FieldByName('CB_INFTIPO').AsInteger;
               FieldByName( 'CB_INFCRED' ).AsString := cdsEmpleadoLookup.FieldByName('CB_INFCRED').AsString;
               FieldByName( 'CRED_OLD' ).AsString := cdsEmpleadoLookup.FieldByName('CB_INFCRED').AsString;
               FieldByName( 'CB_INFTASA' ).AsFloat := cdsEmpleadoLookup.FieldByName('CB_INFTASA').AsFloat;
               FieldByName( 'TASA_OLD' ).AsFloat := cdsEmpleadoLookup.FieldByName('CB_INFTASA').AsFloat;
               FieldByName( 'CB_INF_OLD' ).AsFloat := cdsEmpleadoLookup.FieldByName('CB_INF_OLD').AsFloat;
               FieldByName( 'TASA_ANT' ).AsFloat := cdsEmpleadoLookup.FieldByName('CB_INF_OLD').AsFloat;
               FieldByName( 'CB_INFMANT' ).AsString := cdsEmpleadoLookup.FieldByName('CB_INFMANT').AsString;
               FieldByName( 'MANT_OLD' ).AsString := cdsEmpleadoLookup.FieldByName('CB_INFMANT').AsString;
               FieldByName( 'CB_INF_INI' ).AsDateTime := cdsEmpleadoLookup.FieldByName('CB_INF_INI').AsDateTime;
               FieldByName( 'FECHA_OLD' ).AsDateTime := cdsEmpleadoLookup.FieldByName('CB_INF_INI').AsDateTime;

          end;
     end;
end;

procedure TdmRecursos.DepuraInfonavit;
begin
     with cdsInfonavit do
     begin
          DisableControls;
          try
             First;
             while not Eof do
             begin
                  if ( FieldByName('CB_INFTIPO').AsInteger = FieldByName('TIPO_OLD').AsInteger ) and
                     ( FieldByName('CB_INFCRED').AsString = FieldByName('CRED_OLD').AsString ) and
                     ( FieldByName( 'CB_INFTASA' ).AsFloat = FieldByName('TASA_OLD').AsFloat ) and
                     ( FieldByName( 'CB_INF_OLD' ).AsFloat = FieldByName('TASA_ANT').AsFloat ) and
                     ( FieldByName( 'CB_INFMANT' ).AsString = FieldByName('MANT_OLD').AsString ) and
                     ( FieldByName( 'CB_INF_INI' ).AsDateTime = FieldByName('FECHA_OLD').AsDateTime ) then
                      Delete
                  else
                      Next;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmRecursos.cdsInfonavitNewRecord(DataSet: TDataSet);
begin
     with cdsInfonavit do
     begin
          FieldByName('CB_INFTIPO').AsInteger := Ord( tiNoTiene );
          FieldByName('CB_INFTASA').AsFloat := 0.0;
          FieldByName('CB_INF_OLD').AsFloat := 0.0;
          FieldByName('CB_INFMANT').AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmRecursos.cdsInfonavitAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsInfonavit do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          DepuraInfonavit;
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerRecursos.GrabaCreditoInfonavit( dmCliente.Empresa, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
                  TressShell.SetDataChange( [ enEmpleado ] );
          end;
     end;
end;

procedure TdmRecursos.cdsInfonavitBeforePost(DataSet: TDataSet);
begin
     if ( cdsInfonavit.FieldByName( 'CB_CODIGO' ).AsInteger <= 0 ) then
           DataBaseError('N�mero de Empleado Debe Ser Mayor a Cero' );
     with dmCliente do
     begin
          ValidaCreditoInfo(DataSet);
          InicializaInfonavit(DataSet);
     end;
end;

{$ifdef VER130}
procedure TdmRecursos.cdsInfonavitReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := cdsInfonavit.ReconciliaError( DataSet, UpdateKind, E, 'CB_CODIGO', GetErrorDescription( E ) );
end;
{$else}
procedure TdmRecursos.cdsInfonavitReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := cdsInfonavit.ReconciliaError( DataSet, UpdateKind, E, 'CB_CODIGO', GetErrorDescription( E ) );
end;
{$endif}

{ cdsGridPrestamos }

procedure TdmRecursos.EditarGridPrestamos;
begin
     with FDatosPrestamoAnterior do         // Inicializar Record
     begin
          Referencia := VACIO;
          Inicio := dmCliente.FechaDefault;
          Status := Ord( spActivo );
          Monto := 0;
          MontoInicial := 0;
          Formula := VACIO;
     end;
     dmCliente.SetLookupEmpleado;
     try
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridPrestamos_DevEx, TGridPrestamos_DevEx, TRUE );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
     TressShell.ReconectaFormaActiva;  // Solo Refresca si la forma Activa incluye a cdsHisIncapaci
end;

procedure TdmRecursos.cdsGridPrestamosAlAdquirirDatos(Sender: TObject);
begin
     with cdsGridPrestamos do
     begin
          if Active then           // Si ya est� activo se limpia el Dataset
             EmptyDataSet
          else
             Data:= ServerRecursos.GetGridPrestamos( dmCliente.Empresa, TRUE );   // Se manda llamar como solo Altas
     end;
end;

procedure TdmRecursos.cdsGridPrestamosAlCrearCampos(Sender: TObject);
begin
     with cdsGridPrestamos do
     begin
          ListaFija( 'PR_STATUS', lfStatusPrestamo );
          MaskFecha( 'PR_FECHA' );
          MaskPesos( 'PR_MONTO' );
          MaskPesos( 'PR_SALDO_I' );
          MaskPesos( 'PR_SALDO' );
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsGridPrestamosCB_CODIGOChange;
     end;
end;

procedure TdmRecursos.cdsGridPrestamosBeforePost(DataSet: TDataSet);
begin
     with cdsGridPrestamos do
     begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger <= 0 ) then
             DataBaseError('N�mero de Empleado Debe Ser Mayor a Cero' );
          if ( FieldByName( 'PR_FECHA' ).AsDateTime = NullDateTime ) then
             DataBaseError( 'Falta Especificar Fecha de Inicio del Pr�stamo' );
          if ZetaCommonTools.StrVacio( FieldByName( 'PR_REFEREN' ).AsString ) then {OP:28/05/08}
             DataBaseError( 'La Referencia no puede quedar vac�a' );
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('PR_SALDO').AsFloat := FieldByName('PR_MONTO').AsFloat -
                                             FieldByName('PR_SALDO_I').AsFloat;
     end;
     SetStatusPrestamo( cdsGridPrestamos );
end;

procedure TdmRecursos.cdsGridPrestamosAfterPost(DataSet: TDataSet);
begin
     with FDatosPrestamoAnterior do
     begin
          Referencia := DataSet.FieldByName( 'PR_REFEREN' ).AsString;
          Inicio := DataSet.FieldByName( 'PR_FECHA' ).AsDateTime;
          Status := DataSet.FieldByName( 'PR_STATUS' ).AsInteger;
          Monto := DataSet.FieldByName( 'PR_MONTO' ).AsFloat;
          MontoInicial := DataSet.FieldByName( 'PR_SALDO_I' ).AsFloat;
          Formula := DataSet.FieldByName( 'PR_FORMULA' ).AsString;
     end;
end;

procedure TdmRecursos.cdsGridPrestamosAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   oParametros: TZetaParams;
begin
     ErrorCount := 0;
     with cdsGridPrestamos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;

          if ChangeCount > 0 then
          begin
               oParametros:= TZetaParams.Create;
               try
                  dmCliente.CargaActivosTodos( oParametros);
                  Repeat
                  Until ( Reconciliar( ServerRecursos.GrabaGridPrestamos( dmCliente.Empresa, Delta, oParametros.VarValues, ErrorCount,FForzarGrabarPrestamos ) ) );
               finally
                      FreeAndNIl( oParametros );
               end;
               if ErrorCount = 0 then
                  TressShell.SetDataChange( [ enPrestamo ] );
          end;
     end;
end;

{$ifdef DOS_CAPAS}
procedure TdmRecursos.cdsGridPrestamosReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sDatosPrestamo: string;
begin
     with DataSet do
          sDatosPrestamo :=  Format( 'Empleado: %d' + CR_LF +
                                     'Tipo: %s' + CR_LF +
                                     'Referencia: %s',
                                     [ FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'PR_TIPO' ).AsString, FieldByName( 'PR_REFEREN' ).AsString ] );

    if PK_Violation( E ) then
    begin
         FMensajePrestamo := 'Este Pr�stamo Ya Fu� Registrado.' + CR_LF + sDatosPrestamo;
         FValidarReglas := FALSE;
    end
    else
    begin
         FMensajePrestamo := 'El Pr�stamo no cumple con los siguientes requisitos' + CR_LF +sDatosPrestamo + CR_LF + GetErrorDescription( E );
    end;
    ZetaDialogo.ZError( 'Error', FMensajePrestamo, 0 );

    if NOT FValidarReglas then
       Action := raCancel

    // En el Grid no se puede Abortar, deben cancelarse y reportar todos los errores
end;
{$else}
procedure TdmRecursos.cdsGridPrestamosReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sDatosPrestamo: string;
begin
     with DataSet do
          sDatosPrestamo :=  Format( 'Empleado: %d' + CR_LF +
                                     'Tipo: %s' + CR_LF +
                                     'Referencia: %s',
                                     [ FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'PR_TIPO' ).AsString, FieldByName( 'PR_REFEREN' ).AsString ] );

    if PK_Violation( E ) then
    begin
         FMensajePrestamo := 'Este Pr�stamo Ya Fu� Registrado.' + CR_LF + sDatosPrestamo;
         FValidarReglas := FALSE;
    end
    else
    begin
         FMensajePrestamo := 'El Pr�stamo no cumple con los siguientes requisitos' + CR_LF +sDatosPrestamo + CR_LF + GetErrorDescription( E );
    end;
    ZetaDialogo.ZError( 'Error', FMensajePrestamo, 0 );

    if NOT FValidarReglas then
       Action := raCancel

    // En el Grid no se puede Abortar, deben cancelarse y reportar todos los errores
end;
{$endif}

procedure TdmRecursos.cdsGridPrestamosCB_CODIGOChange(Sender: TField);
begin
     with cdsGridPrestamos do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
          { Asigna Valores Default }
          if ( State = dsInsert ) then    // Solo cuando est� agregando - No se pone en onNewRecord para que aparezca vacio el rengl�n hasta que se indique el Empleado
          begin
               with FDatosPrestamoAnterior do
               begin
                    FieldByName( 'PR_REFEREN' ).AsString := Referencia;
                    FieldByName( 'PR_FECHA' ).AsDateTime := Inicio;
                    FieldByName( 'PR_STATUS' ).AsInteger := Status;
                    FieldByName( 'PR_MONTO' ).AsFloat := Monto;
                    FieldByName( 'PR_SALDO_I' ).AsFloat := MontoInicial;
                    FieldByName( 'PR_FORMULA' ).AsString := Formula;
               end;
          end;
     end;
end;

{ ************* HISTORIAL DE KARDEX ***************** }

{ cdsHisKardex }

procedure TdmRecursos.cdsHisKardexAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisKardex.Data := ServerRecursos.GetHisKardex( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisKardexAlCrearCampos(Sender: TObject);
begin
     with cdsHisKardex do
     begin
          MaskFecha( 'CB_FECHA' );
          MaskNumerico( 'CB_SALARIO', K_MASCARA_SIN_CEROS );
          MaskNumerico( 'CB_MONTO', K_MASCARA_SIN_CEROS );
     end;
end;

procedure TdmRecursos.cdsHisKardexAlBorrar(Sender: TObject);
var
   sTipo : String;
   dFecha : TDate;
   lAutoSal, lCambioAutoSal : Boolean;
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
     begin
          with cdsHisKardex do
          begin
               sTipo := FieldByName( 'CB_TIPO' ).AsString;
               dFecha := FieldByName( 'CB_FECHA' ).AsDateTime;
               lAutoSal := zStrToBool( FieldByName( 'CB_AUTOSAL' ).AsString );
               if ( sTipo = K_T_BAJA ) then
               begin
                    if ( dFecha < dmCliente.GetDatosEmpleadoActivo.Ingreso ) then
                       DataBaseError( 'Fecha de Baja es Anterior a Reingreso del Empleado' );
                   if Global.OK_ProyectoEspecial(peRecontrataciones)then
                   begin
                        AgregandoKardex:= FALSE;
                        cdsEditHisKardex.Active := False;
                        cdsEditHisKardex.Conectar;
                        if not ( zStrToBool( cdsEditHisKardex.FieldByName('CB_GLOBAL').AsString ) ) then
                        begin
                             dmCatalogos.GetDatosPeriodo( cdsEditHisKardex.FieldByName('CB_NOMYEAR').AsInteger,eTipoPeriodo(cdsEditHisKardex.FieldByName('CB_NOMTIPO').AsInteger) );
                             dmCatalogos.cdsPeriodo.Locate('PE_NUMERO',cdsEditHisKardex.FieldByName('CB_NOMNUME').AsInteger,[]);
                             if ( eStatusPeriodo( dmCatalogos.cdsPeriodo.FieldByName('PE_STATUS').AsInteger ) in [ spAfectadaParcial, spAfectadaTotal ] ) then
                             begin
                                  DatabaseError('No se puede borrar liquidaci�n del empleado en una n�mina ya afectada');
                             end;
                        end;
                    end;
                    RevisaUltimaPlaza;

               end;
               if ( sTipo = K_T_ALTA ) then
                  with dmCliente.GetDatosEmpleadoActivo do
                  begin
                       if ( Baja = NullDateTime ) then
                          DataBaseError( 'No se Puede Borrar Registro de Alta del Empleado' );
                       if ( Baja > NullDateTime ) and ( dFecha < Baja ) then
                          DataBaseError( 'No se Puede Borrar Registro de Alta por que existe una Baja Posterior' );
                  end;
               Delete;
          end;
          if FRefrescaHisKardex then  // Proceso parecido al de RefrescaKardex()
          begin
               // Verifica Cambio de Salario derivado de un Cambio de Puesto "Tabulador"
               with cdsHisKardex do
               begin
                    {$ifdef ANTES}
                    lCambioAutosal := ( sTipo = K_T_PUESTO ) and lAutoSal and
                                      ( Locate( 'CB_FECHA;CB_TIPO', VarArrayOf( [
                                  //      ZetaCommonTools.FechaAsStr( dFecha ), K_T_CAMBIO ] ), [] ) ) and
                                        dFecha , K_T_CAMBIO ] ), [] ) ) and
                                      ( ZetaMsgDlg.ConfirmaCambio( 'El Empleado Tiene Salario X Tabulador' + CR_LF +
                                        'y Un Registro de Cambio de Salario para' + CR_LF +
                                        'la misma Fecha !' + CR_LF +
                                        '� Desea Borrar El Registro de Cambio de Salario ?' ) );
                    {$ELSE}
                    lCambioAutosal := ( ( sTipo = K_T_PUESTO ) or ( sTipo = K_T_PLAZA ) )and
                                      lAutoSal and
                                      ( Locate( 'CB_FECHA;CB_TIPO', VarArrayOf( [
                                     //   ZetaCommonTools.FechaAsStr( dFecha ), K_T_CAMBIO ] ), [] ) ) and
                                        dFecha, K_T_CAMBIO ] ), [] ) ) and
                                      ( ZetaMsgDlg.ConfirmaCambio( 'El Empleado Tiene Salario X Tabulador' + CR_LF +
                                        'y Un Registro de Cambio de Salario para' + CR_LF +
                                        'la misma Fecha !' + CR_LF +
                                        '� Desea Borrar El Registro de Cambio de Salario ?' ) );
                    {$ENDIF}

                    if lCambioAutoSal then
                    begin
                         FRefrescaHisKardex:= FALSE;    // Lo Requiere el AfterDelete
                         Delete;
                    end;
               end;
               // Refrescar DataSets
               if ( sTipo = K_T_CAMBIO ) or ( sTipo = K_T_PUESTO ) or ( sTipo = K_T_TURNO ) or
                  ( sTipo = K_T_AREA ) or ( sTipo = K_T_RENOVA ) or ( sTipo = K_T_PRESTA ) or
                  ( sTipo = K_T_VACA ) or ( sTipo = K_T_BAJA ) or ( sTipo = K_T_ALTA ) or
                  ( sTipo = K_T_PLAZA ) then
               begin
                    dmCliente.cdsEmpleado.Refrescar;  // Solo cuando Cambien Datos de Colabora

                    if ( sTipo = K_T_CAMBIO ) or ( sTipo = K_T_BAJA ) or ( sTipo = K_T_ALTA ) or
                       ( sTipo = K_T_PRESTA ) or ( sTipo = K_T_PUESTO ) or ( lCambioAutoSal ) or
                       ( sTipo = K_T_PLAZA ) then
                    begin
                         if ( sTipo = K_T_CAMBIO ) or ( sTipo = K_T_ALTA ) or ( lCambioAutoSal ) then
                             cdsEmpPercepcion.Active := FALSE;    //Menos tardado que Refrescar
                         if ( sTipo = K_T_BAJA ) or ( sTipo = K_T_ALTA ) then
                         begin
                              TressShell.CargaEmpleadoActivos;
                         end;
                         if ( sTipo = K_T_BAJA )then
                         begin
                              ReactivarUsuario(False);
                              if Global.OK_ProyectoEspecial(peRecontrataciones)then 
                              begin
                              		 BorrarFiniquito
                              end;
                         end;
                         if ( sTipo = K_T_ALTA )then
                         begin
                              RevisaUsuario;
                              TressShell.SetDataChange( [ enVacacion ] );
                         end;
                         if ( sTipo = K_T_BAJA ) or ( sTipo = K_T_ALTA ) or ( sTipo = K_T_PRESTA ) then
                             cdsHisVacacion.Active := FALSE;
                         if ( sTipo = K_T_PLAZA ) or ( sTipo = K_T_PUESTO ) or ( sTipo = K_T_ALTA ) or ( sTipo = K_T_BAJA ) then
                             TressShell.SetDataChange( [ enPlazas ] );
                         TressShell.ReconectaFormaActiva;
                    end;
                    if ( sTipo = K_T_VACA ) then      // No esta activa la forma, pero pudiera estar abierta
                       cdsHisVacacion.Active := FALSE;
               end;
               FRefrescaHisKardex:= FALSE;
          end;
     end;
end;

procedure TdmRecursos.BorrarFiniquito;
begin
     with dmNomina do
     begin
          GetDatosNominaClasifi(dmCliente.Empleado,cdsEditHisKardex.FieldByName('CB_NOMYEAR').AsInteger,cdsEditHisKardex.FieldByName('CB_NOMNUME').AsInteger,cdsEditHisKardex.FieldByName('CB_NOMTIPO').AsInteger);
          if( not cdsDatosClasifi.IsEmpty ) then
             cdsDatosClasifi.Delete;
     end;
end;

procedure TdmRecursos.ReactivarUsuario(const lMostrarEdicion:Boolean);
var
   sUsuario: string;
   eAlta :eAltaUsuario;
   lAgrega : Boolean;
begin
     lAgrega := FALSE;
     with dmSistema.cdsUsuarios do
     begin
          Filtered := False;
          Conectar;
          if Locate('CB_CODIGO;CM_CODIGO', VarArrayOf( [ dmCliente.cdsEmpleado.FieldByName('CB_CODIGO').AsInteger,dmCliente.Empresa[P_CODIGO ] ]),[] )then
          begin
               sUsuario := FieldByName('US_CODIGO').AsString +' : '+FieldByName('US_CORTO').AsString;
               if ( ZetaMsgDlg.ConfirmaCambio(  Format ('� Desea activar el Usuario: %s' + CR_LF + ' asignado al empleado ?',[sUsuario])))then
               begin
                    Edit;
                    FieldByName('US_ACTIVO').AsString := K_GLOBAL_SI;
                    if (lMostrarEdicion)then
                    begin
                         dmSistema.ModificaUsuarios(FALSE)
                    end
                    else
                        Enviar;
               end;
          end
          else
              begin
                   eAlta := eAltaUsuario( Global.GetGlobalInteger(K_GLOBAL_ENROLL_AUTO_USER ) );
                   if ( eAlta in [ apAgregarAutomaticamente, apAgregarPreguntando ] ) then
                   begin
                        lAgrega:= True;
                        if ( eAlta = apAgregarPreguntando ) then
                        begin
                             lAgrega:= ZConfirm( 'Acceso a Tress', Format( '� Desea darle Acceso a Tress al Empleado %s ?',
                                                                       [ dmCliente.GetDatosEmpleadoActivo.Nombre ] ), 0, mbYes );
                        end;
                   end;
                   if lAgrega then
                   begin
                        dmSistema.AgregaEmpleadoUsuario;
                   end;
              end;
          Filtered := True;
     end;
end;

procedure TdmRecursos.SaldaVacacionesPendientes;
var
   rGozo,rPago,rPrima: TPesos;
begin
     {Ultimo Kardex y fecha de antiguedad menor a la fecha de ingreso }
     with dmCliente.cdsEmpleado do
          if ( cdsEditHisKardex.FieldByName('CB_FECHA').AsDateTime = FieldByName('CB_FEC_ING').AsDateTime ) and
             ( FieldByName('CB_FEC_ANT').AsDateTime < FieldByName('CB_FEC_ING').AsDateTime ) then
          begin
               with cdsHisVacacion do
               begin
                    Conectar;
                    Append;
                    FieldByName('VA_FEC_INI').AsDateTime:= dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime;
               end;
               SetSaldosVacaciones( FALSE, rGozo, rPago, rPrima );

               if ( rGozo <> 0 ) or ( rPago <> 0 ) or ( rPrima <> 0 ) then
               begin
                    {$ifdef INTERFAZ_TRESS}
                    // Simula, cuando se usa la interfaz, que se indic� que saldara las vacaciones, para
                    // evitar que se mostrara la caja de dialogo durante el proceso de la interfaz.
                    with cdsHisVacacion do
                    begin
                         FieldByName( 'VA_FEC_FIN' ).AsDateTime := dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime;

                         FieldByName( 'VA_GOZO' ).AsFloat := rGozo;
                         FieldByName( 'VA_PAGO' ).AsFloat := rPago;
                         FieldByName( 'VA_P_PRIMA' ).AsFloat := rPrima;

                         FieldByName( 'VA_COMENTA' ).AsString := K_AJUSTE_SALDO_REINGRESO;
                         FieldByName( 'VA_AJUSTE' ).AsString := K_GLOBAL_SI;

                         FieldByName( 'CB_SALARIO' ).AsInteger  := 0;
                         FieldByName( 'VA_NOMYEAR' ).AsInteger  := 0;
                         FieldByName( 'VA_NOMTIPO' ).AsInteger  := Ord( tpDiario );
                         FieldByName( 'VA_NOMNUME' ).AsInteger  := 0;
                         FieldByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
                         FieldByName( 'VA_MONTO' ).AsFloat := 0;
                         FieldByName( 'VA_SEVEN' ).AsFloat := 0;
                         FieldByName( 'VA_PRIMA' ).AsFloat := 0;
                         FieldByName( 'VA_TOTAL' ).AsFloat := 0;

                         Post;
                         Enviar;

                       {$ifdef INGREDION_INTERFAZ}
                       dmInterfase.EscribeBitacora(Format( 'Las vacaciones fueron saldadas al reingreso. Gozo: %f - Pago: %f - Prima %f', [ rGozo, rPago, rPrima ] ) );
                       {$endif}
                    end;
                    {$else}
              
                    {$else} 
                        {$ifndef INTERFAZ_NILFISK}
						{$ifndef INTERFAZ_GREATBATCH}
                         if FShowAdvVacaPendientes_DevEx.ShowDlgVacacionesPendientes( dmCliente.Empleado, dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime, dmCliente.cdsEmpleado.FieldByName('CB_FEC_ANT').AsDateTime,rGozo, rPago,rPrima ) then
                         begin
                              if ( SaldaVacaciones_DevEx = nil ) then
                              begin
                                   SaldaVacaciones_DevEx := TSaldaVacaciones_DevEx.Create( Application ); // Se destruye al Salir del Programa //
                              end;
                              SaldaVacaciones_DevEx.ShowModal;
                         end
                         else
                             cdsHisVacacion.CancelUpdates;
                         {$endif}
						 {$endif}
				    {$endif}
               end
               else
                   cdsHisVacacion.CancelUpdates;
          end;
end;



procedure TdmRecursos.cdsHisKardexAlModificar(Sender: TObject);
var
   sTipo : string;
   oCursor: TCursor;
begin
     sTipo := cdsHisKardex.FieldByName( 'CB_TIPO' ).AsString;
     AgregandoKardex:= FALSE;

     if ( sTipo <> K_T_VACA) AND (sTipo <> K_T_INCA) AND ( sTipo <> K_T_PERM ) AND
        ( cdsEditHisKardex.State = dsBrowse ) then
        cdsEditHisKardex.Active := FALSE;
     ConectarKardexLookups;  // Se ocupa para el create simple lookup de cdsEditHisKardex
     begin
     if ( sTipo = K_T_ALTA ) then        ZBaseEdicion_DevEx.ShowFormaEdicion( KardexAlta_DevEx, TKardexAlta_DevEx )
     else if ( sTipo = K_T_CAMBIO ) then ZBaseEdicion_DevEx.ShowFormaEdicion( KardexCambio_DevEx,TKardexCambio_DevEx)
     else if ( sTipo = K_T_AREA ) then   ZBaseEdicion_DevEx.ShowFormaEdicion( KardexArea_DevEx,TKardexArea_DevEx)
     else if ( sTipo = K_T_PUESTO ) then ZBaseEdicion_DevEx.ShowFormaEdicion( KardexPuesto_DevEx,TKardexPuesto_DevEx)
     else if ( sTipo = K_T_RENOVA ) then ZBaseEdicion_DevEx.ShowFormaEdicion( KardexRenova_DevEx, TKardexRenova_DevEx)
     else if ( sTipo = K_T_TURNO ) then  ZBaseEdicion_DevEx.ShowFormaEdicion( KardexTurno_DevEx,TKardexTurno_DevEx)
     else if ( sTipo = K_T_BAJA ) then   ZBaseEdicion_DevEx.ShowFormaEdicion( KardexBaja_DevEx,TKardexBaja_DevEx)
     else if ( sTipo = K_T_PRESTA ) then ZBaseEdicion_DevEx.ShowFormaEdicion( KardexPresta_DevEx,TKardexPresta_DevEx)
     else if ( sTipo = K_T_PLAZA ) then ZBaseEdicion_DevEx.ShowFormaEdicion( KardexPlaza_DevEx,TKardexPlaza_DevEx)
     else if ( sTipo = K_T_TIPNOM) then ZBaseEdicion_DevEx.ShowFormaEdicion( KardexTipoNomina_DevEx,TKardexTipoNomina_DevEx)
     else if ( sTipo = K_T_VACA ) then
     begin
          with cdsHisVacacion do
          begin
               Conectar;
               if IsEmpty or ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
                  //Locate( 'VA_FEC_INI;VA_TIPO',VarArrayOf([ZetaCommonTools.FechaAsStr( cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime ), 1]),[] );

                  // DES US #17300: Bombardier Aerospace M�xico-En Kardex de empleado
                  // no se visualizan datos de los registros de Vacaciones anteriores cuando se reingresa al empleado
                  if Locate( 'VA_FEC_INI;VA_TIPO', VarArrayOf([cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime, 1]),[] ) then
                  begin
                       ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisVacacion_DevEx,TEditHisVacacion_DevEx);
                  end
                  else
                  begin
                       // Es registro anterior a fecha de ingreso.
                       // Revisar fecha de ingreso contra fecha de registro de vacaciones en kardex.
                       if dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime > cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime then
                       begin
                            with Screen do
                            begin
                                 oCursor := Cursor;
                                 Cursor := crHourglass;
                                 try
                                    with dmCliente do
                                    begin
                                         cdsHisVacacionSoloLectura.Data :=
                                            ServerRecursos.GetHisVacacionAFecha( Empresa, Empleado,
                                            cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime);

                                         ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisVacacionSoloLectura_DevEx,TEditHisVacacionSoloLectura_DevEx);
                                    end;
                                 finally
                                        Cursor := oCursor;
                                 end;
                            end;
                       end;
                  end;
          end;
     end
     else if ( sTipo = K_T_PERM ) then
     begin
          with cdsHisPermiso do
          begin
               Conectar;
               if IsEmpty or ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
               //Locate( 'PM_FEC_INI',ZetaCommonTools.FechaAsStr( cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime ),[] );
               Locate( 'PM_FEC_INI', cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime ,[] );
               ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisPermiso_DevEx,TEditHisPermiso_DevEx);
          end;
     end
     else if ( sTipo = K_T_INCA ) then
     begin
          with cdsHisIncapaci do
          begin
               Conectar;
               if IsEmpty or ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
               //Locate( 'IN_FEC_INI',ZetaCommonTools.FechaAsStr( cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime ),[] );
               Locate( 'IN_FEC_INI', cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime,[] );
               ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisIncapaci_DevEx,TEditHisIncapaci_DevEx);
          end;
     end
     else ZBaseEdicion_DevEx.ShowFormaEdicion( KardexGeneral_DevEx,TKardexGeneral_DevEx);
     end;
     RefrescaKardex( sTipo );
end;

procedure TdmRecursos.cdsHisKardexAlAgregar(Sender: TObject);
var
   sTipo: String;
begin
     with cdsHisKardex do
     begin
          sTipo := FieldByName('CB_TIPO').AsString;
          if ( sTipo = K_T_ALTA ) or
             ( ( sTipo = K_T_BAJA ) and ( not ZAccesosMgr.CheckDerecho( D_EMP_REG_BAJA, K_DERECHO_CONSULTA ) ) ) or
             ( ( zStrToBool( FieldByName( 'TB_SISTEMA' ).AsString ) ) and ( not ZAccesosMgr.CheckDerecho( D_EMP_EXP_KARDEX, K_DERECHO_SIST_KARDEX ) ) ) or
             ( ( sTipo = K_T_CAMBIO ) and ( not ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA ) ) ) then
             sTipo := VACIO;
     end;
     AgregaKardex( sTipo );
end;

procedure TdmRecursos.cdsHisKardexBeforeDelete(DataSet: TDataSet);
var
   sTipo : String;
begin
     sTipo := cdsHisKardex.FieldByName( 'CB_TIPO' ).AsString;

     if ( sTipo = K_T_VACA ) then
     begin
          with cdsHisVacacion do
          begin
               Conectar;
               if IsEmpty or ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
               //if Locate( 'VA_FEC_INI;VA_TIPO',VarArrayOf([ZetaCommonTools.FechaAsStr( cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime ), 1 ]),[] ) then
               if Locate( 'VA_FEC_INI;VA_TIPO',VarArrayOf([ cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime, 1 ]),[] ) then
               begin
                    Delete;    // No se puede indicar Borrar, por que este vuelve a preguntar si se quiere borrar
                    Enviar;
               end
               // DES US #17300: Bombardier Aerospace M�xico-En Kardex de empleado
               // no se visualizan datos de los registros de Vacaciones anteriores cuando se reingresa al empleado
               else
               begin
                    // Es registro anterior a fecha de ingreso.
                    // Revisar fecha de ingreso contra fecha de registro de vacaciones en kardex.
                    if dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime > cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime then
                    begin
                         with Screen do
                         begin
                              with dmCliente do
                              begin
                                   cdsHisVacacionSoloLectura.Data :=
                                      ServerRecursos.GetHisVacacionAFecha( Empresa, Empleado,
                                      cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime);
                                   cdsHisVacacionSoloLectura.Delete;
                                   cdsHisVacacionSoloLectura.Enviar;
                              end;
                         end;
                    end;
               end;
          end;
     end
     else if ( sTipo = K_T_PERM ) then
     begin
          with cdsHisPermiso do
          begin
               Conectar;
               if IsEmpty or ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
               //if Locate( 'PM_FEC_INI',ZetaCommonTools.FechaAsStr( cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime ),[] ) then
               if Locate( 'PM_FEC_INI', cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime,[] ) then
               begin
                    Delete;
                    Enviar;
               end;
          end;
     end
     else if ( sTipo = K_T_INCA ) then
     begin
          with cdsHisIncapaci do
          begin
               Conectar;
               if IsEmpty or ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
               //if Locate( 'IN_FEC_INI',ZetaCommonTools.FechaAsStr( cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime ),[] ) then
               if Locate( 'IN_FEC_INI', cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime,[] ) then
               begin
                    Delete;
                    Enviar;
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsHisKardexAfterDelete(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisKardex do
     begin
          if ChangeCount > 0 then
             if not FRefrescaHisKardex then
             begin
                  Reconcile( ServerRecursos.GrabaBorrarKardex( dmCliente.Empresa, Delta, ErrorCount ) );
                  if ErrorCount = 0 then
                     FRefrescaHisKardex:= TRUE;
             end
             else
                MergeChangeLog;
     end;
end;

function TdmRecursos.AplicaFiltrosKardex(const sFiltros: String): Integer;
begin
     with cdsHisKardex do
     begin
          Filtered := strLleno(sFiltros );
          if( Filtered )then
              Filter := sFiltros;
          Result := RecordCount;
     end;
end;

{
function TdmRecursos.AplicaFiltrosPrestamo(const sFiltros: String): Integer;
begin
     with cdsHisPrestamos do
     begin
          Filtered := strLleno( sFiltros );
          if( Filtered )then
              Filter := sFiltros;
          Result := RecordCount;
     end;
end;
}

{ cdsEditHisKardex }

procedure TdmRecursos.cdsEditHisKardexAlAdquirirDatos(Sender: TObject);
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             if AgregandoKardex then
                with dmCliente do
                     cdsEditHisKardex.Data := ServerRecursos.GetEditHisKardex( Empresa, Empleado, 0, '' )
             else
                with cdsHisKardex do
                     cdsEditHisKardex.Data := ServerRecursos.GetEditHisKardex( dmCliente.Empresa,
                                                                               FieldByName('CB_CODIGO').AsInteger,
                                                                               FieldByName('CB_FECHA').AsDateTime,
                                                                               FieldByName('CB_TIPO').AsString );
          finally
             Cursor := oCursor;
          end;
     end;
end;

procedure TdmRecursos.cdsEditHisKardexAlCrearCampos(Sender: TObject);
begin
     with cdsEditHisKardex do
     begin
          MaskFecha( 'CB_FECHA' );
          ListaFija( 'CB_STATUS', lfStatusKardex );
          FieldByName( 'CB_TIPO' ).OnChange := OnChangeCB_TIPO;
          FieldByName( 'CB_FECHA' ).OnChange := OnChangeCB_FECHA;
          FieldByName( 'CB_FEC_ING' ).OnChange := dmCliente.IngresoChange;
          FieldByName( 'CB_FECHA_2' ).OnChange := LimpiaFactorIntegracion;
          FieldByName( 'CB_SALARIO' ).OnChange := LimpiaFactorIntegracion;
          FieldByName( 'CB_PER_VAR' ).OnChange := LimpiaFactorIntegracion;
          FieldByName( 'CB_TABLASS' ).OnChange := LimpiaFactorIntegracion;
          FieldByName( 'CB_CONTRAT' ).OnChange := dmCliente.ContratoChange;
          FieldByName( 'CB_FEC_CON' ).OnChange := dmCliente.ContratoChange;
          if dmCliente.Matsushita then
             FieldByName( 'CB_RANGO_S' ).OnChange := LimpiaFactorIntegracion;
          if dmCliente.UsaPlazas then
             FieldByName( 'CB_PLAZA' ).OnChange := OnChangeCB_PLAZA
          else
              FieldByName( 'CB_PLAZA' ).OnChange := NIL;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateLookup(dmTablas.cdsMovKardex, 'TB_SISTEMA', 'CB_TIPO', 'TB_CODIGO', 'TB_SISTEMA');

          FieldByName( 'CB_NOMINA' ).OnChange := validacionTipoNominaChange;
          FieldByName( 'CB_NOMTIPO' ).OnChange := validacionTipoNominaChange;
     end;
end; 

procedure TdmRecursos.cdsEditHisKardexNewRecord(DataSet: TDataSet);
begin
     with cdsEditHisKardex do
     begin
          FieldByName( 'CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName( 'CB_FECHA').AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'CB_GLOBAL').AsString  := 'N';
          FieldByName( 'CB_REINGRE').AsString := 'N';
          FieldByName( 'CB_AUTOSAL').AsString := 'N';
          FieldByName( 'CB_NIVEL').AsInteger  := 5;
          FieldByName( 'CB_STATUS').AsInteger := Ord( skCapturado );
          FieldByName( 'CB_NOMINA').AsInteger := Ord( dmCliente.PeriodoTipo );
          if dmCliente.Matsushita then
             FieldByName( 'CB_RANGO_S' ).AsFloat := 100;
     end;
end;

procedure TdmRecursos.cdsEditHisKardexBeforeEdit(DataSet: TDataSet);
begin
     FPlazaPatron := cdsEditHisKardex.FieldByName( 'CB_PATRON' ).AsString;
end;

procedure TdmRecursos.cdsEditHisKardexBeforePost(DataSet: TDataSet);
var
   sTipo : string;
   dFecha, dIngreso : TDate;
begin
     with dmCliente, cdsEditHisKardex do
     begin
          sTipo := FieldByName( 'CB_TIPO').AsString;
          dFecha := FieldByName( 'CB_FECHA').AsDateTime;
          dIngreso := cdsEmpleado.FieldByName( 'CB_FEC_ING' ).AsDateTime;
          // Valida Fecha del Movimiento
          if ( sTipo <> K_T_ALTA ) and ( dFecha < dIngreso ) then
{$ifdef INTERFAZ_TRESS}
             raise Exception.Create( Format( 'Fecha Movimiento [%s] Tiene que ser Mayor a Fecha de Alta del Empleado: %s',
                                    [ FechaCorta( dFecha ), FechaCorta( dIngreso ) ] ) );
{$else}
             DataBaseError( Format( 'Fecha Movimiento [%s] Tiene que ser Mayor a Fecha de Alta del Empleado: %s',
                                    [ FechaCorta( dFecha ), FechaCorta( dIngreso ) ] ) );
{$endif}
          if Matsushita and zStrToBool( FieldByName( 'CB_AUTOSAL' ).AsString ) and
             ( FieldByName( 'CB_RANGO_S' ).AsFloat <> 100 ) and
             ( FieldByName( 'CB_FAC_INT' ).AsFloat = 0 ) then
             FieldByName( 'CB_SALARIO' ).AsFloat := GetSalClasifi( FieldByName( 'CB_CLASIFI' ).AsString );

          if ( sTipo = K_T_ALTA ) or ( sTipo = K_T_CAMBIO ) or ( sTipo = K_T_BAJA ) then
          begin
               if ( FieldByName( 'CB_LOT_IDS' ).AsString <> VACIO  ) and ( FieldByName( 'CB_FEC_IDS').AsDateTime = 0 ) then
               begin
                     DatabaseError('Debe capturarse la Fecha de Transacci�n IDSE');
               end;

               if ( FieldByName( 'CB_LOT_IDS' ).AsString = VACIO  ) and ( FieldByName( 'CB_FEC_IDS').AsDateTime > 0 ) then
               begin
                    DatabaseError('Debe capturarse el N�mero de Lote IDSE');
               end;

          end;

          if UsaPlazas then
          begin
               if (sTipo = K_T_PLAZA) or ( (sTipo = K_T_ALTA) and (FieldByName('CB_REINGRE').AsString = K_GLOBAL_SI)) then
               begin
                    if ( FieldByName('CB_PLAZA').AsInteger = 0 ) then
{$ifdef INTERFAZ_TRESS}
                       raise Exception.Create( 'Favor de especificar la Plaza del Empleado' );
{$else}
                       DatabaseError('Favor de especificar la Plaza del Empleado');
{$endif}
               end;

               if (sTipo = K_T_PLAZA) then
               begin
                    if ( State = dsEdit ) and ( FPlazaPatron <> FieldByName('CB_PATRON').AsString ) then
{$ifdef INTERFAZ_TRESS}
                       raise Exception.Create( 'El Cambio de Plaza no se puede realizar porque el Registro Patronal es diferente al anterior' );
{$else}
                       DatabaseError('El Cambio de Plaza no se puede realizar porque el Registro Patronal es diferente al anterior');
{$endif}

                    if ( State = dsInsert ) and ( cdsEmpleado.FieldByName('CB_PATRON').AsString <> FieldByName('CB_PATRON').AsString ) then
{$ifdef INTERFAZ_TRESS}
                       raise Exception.Create('El Cambio de Plaza no se puede realizar porque el Registro Patronal es diferente al anterior');
{$else}
                       DatabaseError('El Cambio de Plaza no se puede realizar porque el Registro Patronal es diferente al anterior');
{$endif}
               end;

          end;
          if Global.OK_ProyectoEspecial(peRecontrataciones)then
          begin
               if (sTipo = K_T_BAJA)then
               begin
                     dmCatalogos.cdsPeriodo.Locate('PE_NUMERO;PE_TIPO;PE_YEAR',VarArrayOf([ FieldByName('CB_NOMNUME').AsInteger,FieldByName('CB_NOMTIPO').AsInteger,FieldByName('CB_NOMYEAR').AsInteger]),[]);
                     if ( eStatusPeriodo( dmCatalogos.cdsPeriodo.FieldByName( 'PE_STATUS' ).AsInteger ) in [ spAfectadaParcial, spAfectadaTotal ] ) then
                     begin
                          DatabaseError('No se puede registrar la Liquidaci�n en una N�mina Afectada');
                     end
                     else
                         dmNomina.FechaBajaLiquidacion := FieldByName('CB_FECHA').AsDateTime;
               end;
          end;
          { Se guardan las fechas para saber si se debe recalcular el ajuste por vacaciones }
          FFechaIngreso:= dIngreso;
          FFechaAntiguedad:= cdsEmpleado.FieldByName( 'CB_FEC_ANT' ).AsDateTime;

{$ifdef COVIDIEN_INTERFAZ_HR}
          cdsEditHisKardex.FieldByName( 'US_CODIGO' ).AsInteger := dminterfase.UsuarioMovimiento;
{$endif}
     end;
end;

procedure TdmRecursos.cdsEditHisKardexAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsEditHisKardex do
     begin
          {if FieldByName('CB_TIPO').AsString = K_T_PLAZA then
             CopiaPlazaAKardex;}

          if State in [ dsEdit, dsInsert ] then
          begin
               if State = dsEdit then
                  FieldByName( 'CB_NOTA' ).AsString := FieldByName( 'CB_NOTA' ).AsString + ' ';
               Post;
          end;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile ( ServerRecursos.GrabaHisKardex( dmCliente.Empresa, Delta, FListaPercepFijas, ErrorCount) );
               if ErrorCount = 0 then
               begin
                    FRefrescaHisKardex:= TRUE;
                    TressShell.SetDataChange( [ enIncapacidad, enPlazas ] );

                    {$ifndef DOS_CAPAS}
                    if ( FieldByName('CB_TIPO').AsString = K_T_ALTA ) or ( FieldByName('CB_TIPO').AsString = K_T_AREA) then
                    begin
                         if Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES ) then
                         begin
                               TressShell.SetDataChange( [ enUsuarios ] );
                         end;
                    end;
                    {$endif}

                    if ( FieldByName('CB_TIPO').AsString = K_T_ALTA ) and ( zStrToBool( FieldByName('CB_REINGRE').AsString ) ) then
                    begin
                         dmCliente.cdsEmpleado.Refrescar;

                         if ( FFechaIngreso <> dmCliente.cdsEmpleado.FieldByName( 'CB_FEC_ING' ).AsDateTime ) or
                            ( FFechaAntiguedad <> dmCliente.cdsEmpleado.FieldByName( 'CB_FEC_ANT' ).AsDateTime ) then
                         begin
                              SaldaVacacionesPendientes;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TdmRecursos.OnChangeCB_FECHA(Sender:TField);
begin
     with cdsEditHisKardex do
     begin
          FieldByName( 'CB_FECHA_2' ).AsDateTime := Sender.AsDateTime;
          FieldByName( 'CB_FEC_CON' ).AsDateTime := Sender.AsDateTime;
          if ( FieldByName( 'CB_TIPO').AsString = K_T_ALTA ) then
          begin
               FieldByName( 'CB_FEC_ING' ).AsDateTime := Sender.AsDateTime;
               if( not dmCliente.GetDatosEmpleadoActivo.Activo)then
               begin
                    if DentroBimestre(Sender.AsDateTime, dmCliente.GetDatosEmpleadoActivo.Baja)then
                      FieldByName( 'CB_PER_VAR' ).AsFloat   := dmCliente.cdsEmpleado.FieldByName( 'CB_PER_VAR' ).AsFloat
                    else
                       FieldByName( 'CB_PER_VAR' ).AsFloat := 0;
               end;
          end;
     end;
end;

procedure TdmRecursos.OnChangeCB_TIPO(Sender: TField);
var
TipoNomina: eTipoPeriodo;

   procedure AsignaCampo( const sCampo : string );
   begin
        cdsEditHisKardex[sCampo] := dmCliente.cdsEmpleado[sCampo];
   end;

begin
     with cdsEditHisKardex do
     begin
          if FieldByName( 'CB_TIPO').AsString = K_T_BAJA then
          begin
               with dmCliente do
               begin
                    FieldByName( 'CB_NOMYEAR').AsInteger := YearDefault;
                    TipoNomina:= eTipoPeriodo( cdsEmpleado.FieldByName('CB_NOMINA').AsInteger ) ;
                    FieldByName( 'CB_NOMTIPO').AsInteger :=  Ord( TipoNomina ) ;
                    {if ( not ZAccesosMgr.CheckDerecho( D_EMP_EXP_VACA, K_DERECHO_SIST_KARDEX )) then
                    begin
                          with cdsPeriodo do
                          begin
                               Filtered := False;
                               Filter := Format('PE_STATUS < %d',[Ord(spAfectadaParcial)]);
                               Filtered := True;
                          end;
                    end;}
                    FieldByName( 'CB_NOMNUME').AsInteger := GetPeriodoInicialTipoNomina(  TipoNomina );
                    FieldByName('CB_RECONTR').AsString:= K_GLOBAL_SI;
               end;
          end
          else if FieldByName( 'CB_TIPO').AsString = K_T_CAMBIO then
          begin
               AsignaCampo('CB_SALARIO');
               AsignaCampo('CB_PER_VAR');
               AsignaCampo('CB_ZONA_GE');
               AsignaCampo('CB_TABLASS');
               AsignaCampo('CB_CLASIFI');
               AsignaCampo('CB_FEC_ANT');
          end
          else if FieldByName( 'CB_TIPO').AsString = K_T_TURNO then
               AsignaCampo('CB_TURNO')
          else if FieldByName( 'CB_TIPO').AsSTring = K_T_PUESTO then
          begin
               AsignaCampo('CB_PUESTO');
               AsignaCampo('CB_CLASIFI');
               AsignaCampo('CB_AUTOSAL');
               //Matsushita
               AsignaCampo('CB_RANGO_S');
          end
          else if FieldByName( 'CB_TIPO').AsSTring = K_T_AREA then
          begin
               AsignaCampo('CB_NIVEL1');
               AsignaCampo('CB_NIVEL2');
               AsignaCampo('CB_NIVEL3');
               AsignaCampo('CB_NIVEL4');
               AsignaCampo('CB_NIVEL5');
               AsignaCampo('CB_NIVEL6');
               AsignaCampo('CB_NIVEL7');
               AsignaCampo('CB_NIVEL8');
               AsignaCampo('CB_NIVEL9');
               {$ifdef ACS}
               AsignaCampo('CB_NIVEL10');
               AsignaCampo('CB_NIVEL11');
               AsignaCampo('CB_NIVEL12');
               {$endif}

          end
          else if FieldByName( 'CB_TIPO').AsString = K_T_RENOVA then
          begin
               AsignaCampo('CB_CONTRAT');
               if( dmCliente.cdsEmpleado.FieldByName( 'CB_FEC_COV' ).AsDateTime <> NullDateTime ) then
                   FieldByName( 'CB_FECHA' ).AsDateTime := dmCliente.cdsEmpleado.FieldByName( 'CB_FEC_COV' ).AsDateTime + 1;
          end
          else if FieldByName( 'CB_TIPO').AsString = K_T_PRESTA then
          begin
               AsignaCampo('CB_TABLASS');
               AsignaCampo('CB_FEC_ANT');
          end;
     end;
end;

procedure TdmRecursos.LimpiaFactorIntegracion(Sender:TField);
begin
     with cdsEditHisKardex do
     begin
          FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
     end;
end;

function TdmRecursos.CalculaRangoSalarial( Rango: TPesos; oLista: TStrings; const dSalario: TPesos ): TPesos;

   function GetPrestaciones: TPesos;
   var
      i:Integer;
      nMonto: TPesos;
   begin
        Result:= 0;
        with oLista do
             for i := 0 to Count-1 do
                 with TOtrasPer( Objects[ i ] ) do
                 begin
                      if ( Tipo = toCantidad ) then
                         nMonto := Monto
                      else
                      begin
                           nMonto:= dSalario * ( Tasa / 100 );
                           if ( Tope > 0 ) then
                              nMonto := rMin( nMonto, Tope );
                      end;
                      Result := Result + nMonto;
                 end;
   end;

begin
     Result := dSalario;
     if ( Rango <= 0 ) then
        Rango:= 100;
     if ( Rango <> 100 ) and ( dSalario > 0 ) then
     begin
          Rango:= Rango / 100;  // Convertir a Porcentaje
          Result:= Redondea( ( Rango + ( GetPrestaciones / dSalario ) * ( Rango - 1 ) )* dSalario );
     end;
end;

{ cdsKar_Fija }

procedure TdmRecursos.cdsKar_FijaAlAdquirirDatos(Sender: TObject);
var
   dFecha : TDate;
begin
     with dmCliente do
     begin
          if AgregandoKardex then
             dFecha := cdsEmpleado.FieldByName( 'CB_FEC_SAL' ).AsDateTime
          else if ( cdsHisKardex.Active ) then
             dFecha := cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime
          else
             dFecha := FechaDefault;
          cdsKar_Fija.Data := ServerRecursos.GetEditKarFijas( Empresa, Empleado, dFecha );
     end;
end;

procedure TdmRecursos.cdsKar_FijaAlCrearCampos(Sender: TObject);
begin
     with cdsKar_fija do
     begin
          ListaFija( 'KF_IMSS', lfIMSSOtrasPer );
          MaskPesos( 'KF_MONTO');
          MaskPesos( 'KF_GRAVADO');
     end;
end;

{ cdsHisTools }

procedure TdmRecursos.cdsHisToolsAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisTools.Data := ServerRecursos.GetHisTools( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisToolsAlCrearCampos(Sender: TObject);
begin
     with cdsHisTools do
     begin
          CreateSimpleLookup( dmCatalogos.cdsTools, 'TO_DESCRIP', 'TO_CODIGO' );
          MaskFecha('KT_FEC_INI');
     end;
end;

procedure TdmRecursos.cdsHisToolsNewRecord(DataSet: TDataSet);
begin
     with cdsHisTools do
     begin
          with dmCliente do
          begin
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'KT_FEC_INI' ).AsDateTime := FechaDefault;
          end;
          FieldByName( 'TO_CODIGO' ).AsString := dmCatalogos.cdsTools.FieldByName( 'TO_CODIGO' ).AsString;
          FieldByName( 'KT_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'KT_REFEREN' ).AsString := VACIO;
     end;
end;

procedure TdmRecursos.cdsHisToolsBeforePost(DataSet: TDataSet);
begin
     with cdsHisTools do
     begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger <= 0 ) then
             DataBaseError( 'No puede quedar vacio el N�mero de Empleado' );
          if zstrToBool( FieldByName( 'KT_ACTIVO' ).AsString ) then
          begin
               FieldByName( 'KT_FEC_FIN' ).AsDateTime := NullDateTime;
               FieldByName( 'KT_MOT_FIN' ).AsString := VACIO;
          end;
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmRecursos.cdsHisToolsAfterDelete(DataSet: TDataSet);
begin
     cdsHisTools.Enviar;
end;

{
procedure TdmRecursos.cdsHisToolsAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     if dmCliente.ModuloAutorizado( okHerramientas ) then
     begin
          ErrorCount := 0;
          with cdsHisTools do
          begin
               if State in [ dsEdit, dsInsert ] then
                  Post;
               if ChangeCount > 0 then
                  if Reconcile ( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
                     TressShell.SetDataChange( [ enPrestamo ] );
          end;
     end;
end;
}

procedure TdmRecursos.cdsHisToolsAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisTools_DevEx, TEditHisTools_DevEx);
end;

{$ifdef VER130}
procedure TdmRecursos.cdsHisToolsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( UpdateKind = ukDelete ) then
        Action := raCancel
     else
        Action := raAbort;
     Action := EvaluaToolsError( DataSet, E, Action );
     if ( Action = raAbort ) then                          // En edici�n individual al Abortar no se consigue reconciliar
        FOperacion := ocReportar;                          // por esta raz�n se cambia FOperacion = ocReportar para que no se quede en Repeat ... Until indefinidamente
end;
{$else}
procedure TdmRecursos.cdsHisToolsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( UpdateKind = ukDelete ) then
        Action := raCancel
     else
        Action := raAbort;
     Action := EvaluaToolsError( DataSet, E, Action );
     if ( Action = raAbort ) then                          // En edici�n individual al Abortar no se consigue reconciliar
        FOperacion := ocReportar;                          // por esta raz�n se cambia FOperacion = ocReportar para que no se quede en Repeat ... Until indefinidamente
end;
{$endif}

{ cdsGridEmpTools }

procedure TdmRecursos.cdsGridEmpToolsAlAdquirirDatos(Sender: TObject);
begin
     cdsGridEmpTools.Data := ServerRecursos.GetGridTools( dmCliente.Empresa, FALSE );
end;

procedure TdmRecursos.cdsGridEmpToolsAlCrearCampos(Sender: TObject);
begin
     with cdsGridEmpTools do
     begin
          with FieldByName( 'TO_CODIGO' ) do
          begin
               OnValidate := EmpTO_CODIGOValidate;
               OnChange := EmpTO_CODIGOChange;
          end;
          FieldByName( 'KT_TALLA' ).OnValidate := TallaValidate;
     end;
end;

procedure TdmRecursos.cdsGridEmpToolsBeforePost(DataSet: TDataSet);
begin
     with cdsGridEmpTools do
     begin
          with FieldByName( 'TO_CODIGO' ) do
          begin
               if strVacio( AsString ) then
               begin
                    FocusControl;
                    DataBaseError( 'C�digo de Herramienta NO Puede Quedar Vac�o' );
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsGridToolsAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   ErrorVar: OleVariant;
   Parametros: TZetaParams;
begin
     if dmCliente.ModuloAutorizado( okHerramientas ) then
     begin
          ErrorCount := 0;
          with TZetaClientDataSet( Sender ) do
          begin
               if State in [dsEdit, dsInsert] then
                  Post;
               if ( ChangeCount > 0 ) then
               begin
                    Parametros := TZetaParams.Create;
                    try
                       FOperacion := ocReportar;
                       Repeat
                             Parametros.AddInteger( 'Operacion', Ord( FOperacion ) );
                             ErrorVar:= ServerRecursos.GrabaGridTools( dmCliente.Empresa, Parametros.VarValues, Delta, ErrorCount );
                       Until ( Reconciliar( ErrorVar ) or ( FOperacion = ocReportar ) );   // Cuando es individual se abortar� y pondr� ocReportar en True
                       TressShell.SetDataChange( [ enTools, enPrestamo ] );
                    finally
                       FreeAndNil( Parametros );
                    end;
               end;
          end;
     end;
end;

{$ifdef VER130}
function TdmRecursos.EvaluaToolsError( DataSet: TClientDataSet; E: EReconcileError; const ActionDef: TReconcileAction ): TReconcileAction;
const
     K_MESS_PK_VIOLATION = '� Esta Herramienta ya fu� Entregada !' + CR_LF + 'Empleado: %d' + CR_LF +
                           'Herramienta: %s' + CR_LF + 'Fecha: %s' + CR_LF + 'Referencia: %s';
     K_MESS_PK_PRESTAMO_VIOLATION = '� Error al Registrar %s!' + CR_LF + 'Empleado: %d' + CR_LF +
                                    'Herramienta: %s' + CR_LF + 'Fecha Devoluci�n: %s' + CR_LF + 'Mot. Devoluci�n: %s';
var
    sError : String;

    function ToolPrestada: Boolean;
    begin
         Result := ( Pos( K_MESS_TOOL_PRESTADA, E.Message ) > 0 );
    end;

    function PK_Tool: Boolean;
    begin
         Result := ( Pos( 'KAR_TOOL', E.Message ) > 0 );
    end;

    function GetTablaPrestamo: String;
    begin
         if ( Pos( 'PCAR_ABO', E.Message ) > 0 ) then
            Result := 'Cargo'
         else
            Result := 'Pr�stamo';
    end;

begin
     Result := ActionDef;
     if ToolPrestada then
     begin
          if ZetaDialogo.ZWarningConfirm( K_TITLE_ADVERTENCIA, E.Message + CR_LF + CR_LF +
                                          '� Desea Continuar ?', 0, mbCancel ) then
          begin
               {  Con un solo registro que se elija continuar se enviar� ocIgnorar para volver a reconciliar,
                  los registros que se decide no continuar se cancelan }
               FOperacion := ocIgnorar;
               Result := raCorrect;
          end;
     end
     else
     begin
          if PK_Violation( E ) then
          begin
               with DataSet do
               begin
                    if PK_Tool then
                    begin
                         sError := Format( K_MESS_PK_VIOLATION, [ FieldByName( 'CB_CODIGO' ).AsInteger,
                                           FieldByName( 'TO_CODIGO' ).AsString,
                                           FechaCorta( FieldByName( 'KT_FEC_INI' ).AsDateTime ),
                                           FieldByName( 'KT_REFEREN' ).AsString ] );
                    end
                    else
                    begin
                         sError := Format( K_MESS_PK_PRESTAMO_VIOLATION, [ GetTablaPrestamo,
                                           FieldByName( 'CB_CODIGO' ).AsInteger,
                                           FieldByName( 'TO_CODIGO' ).AsString,
                                           FechaCorta( FieldByName( 'KT_FEC_FIN' ).AsDateTime ),
                                           FieldByName( 'KT_MOT_FIN' ).AsString ] );
                    end;
               end;
          end
          else
              sError := GetErrorDescription( E );
          ZetaDialogo.ZError( 'Error', sError, 0 );
     end;
end;
{$else}
function TdmRecursos.EvaluaToolsError( DataSet: TCustomClientDataSet; E: EReconcileError; const ActionDef: TReconcileAction ): TReconcileAction;
const
     K_MESS_PK_VIOLATION = '� Esta Herramienta ya fu� Entregada !' + CR_LF + 'Empleado: %d' + CR_LF +
                           'Herramienta: %s' + CR_LF + 'Fecha: %s' + CR_LF + 'Referencia: %s';
     K_MESS_PK_PRESTAMO_VIOLATION = '� Error al Registrar %s!' + CR_LF + 'Empleado: %d' + CR_LF +
                                    'Herramienta: %s' + CR_LF + 'Fecha Devoluci�n: %s' + CR_LF + 'Mot. Devoluci�n: %s';
var
    sError : String;

    function ToolPrestada: Boolean;
    begin
         Result := ( Pos( K_MESS_TOOL_PRESTADA, E.Message ) > 0 );
    end;

    function PK_Tool: Boolean;
    begin
         Result := ( Pos( 'KAR_TOOL', E.Message ) > 0 );
    end;

    function GetTablaPrestamo: String;
    begin
         if ( Pos( 'PCAR_ABO', E.Message ) > 0 ) then
            Result := 'Cargo'
         else
            Result := 'Pr�stamo';
    end;

begin
     Result := ActionDef;
     if ToolPrestada then
     begin
          if ZetaDialogo.ZWarningConfirm( K_TITLE_ADVERTENCIA, E.Message + CR_LF + CR_LF +
                                          '� Desea Continuar ?', 0, mbCancel ) then
          begin
               {  Con un solo registro que se elija continuar se enviar� ocIgnorar para volver a reconciliar,
                  los registros que se decide no continuar se cancelan }
               FOperacion := ocIgnorar;
               Result := raCorrect;
          end;
     end
     else
     begin
          if PK_Violation( E ) then
          begin
               with DataSet do
               begin
                    if PK_Tool then
                    begin
                         sError := Format( K_MESS_PK_VIOLATION, [ FieldByName( 'CB_CODIGO' ).AsInteger,
                                           FieldByName( 'TO_CODIGO' ).AsString,
                                           FechaCorta( FieldByName( 'KT_FEC_INI' ).AsDateTime ),
                                           FieldByName( 'KT_REFEREN' ).AsString ] );
                    end
                    else
                    begin
                         sError := Format( K_MESS_PK_PRESTAMO_VIOLATION, [ GetTablaPrestamo,
                                           FieldByName( 'CB_CODIGO' ).AsInteger,
                                           FieldByName( 'TO_CODIGO' ).AsString,
                                           FechaCorta( FieldByName( 'KT_FEC_FIN' ).AsDateTime ),
                                           FieldByName( 'KT_MOT_FIN' ).AsString ] );
                    end;
               end;
          end
          else
              sError := GetErrorDescription( E );
          ZetaDialogo.ZError( 'Error', sError, 0 );
     end;
end;
{$endif}

{$ifdef VER130}
procedure TdmRecursos.cdsGridToolsReconcileError( DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := EvaluaToolsError( DataSet, E, raCancel );
end;
{$else}
procedure TdmRecursos.cdsGridToolsReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := EvaluaToolsError( DataSet, E, raCancel );
end;
{$endif}

procedure TdmRecursos.EmpTO_CODIGOValidate(Sender: TField);
var
   sDescrip: String;
begin
     with Sender do
     begin
          if strLleno( AsString ) then
          begin
               if not ( dmCatalogos.cdsTools.LookupKey( UpperCase( AsString ), '', sDescrip ) ) then
                  DataBaseError( 'No Existe el C�digo de Herramienta: ' + AsString );
          end
          else
              DataBaseError( 'C�digo de Herramienta NO Puede Quedar Vac�o' );
     end;
end;

procedure TdmRecursos.EmpTO_CODIGOChange(Sender: TField);
begin
     Sender.DataSet.FieldByName( 'TO_DESCRIP' ).AsString := dmCatalogos.cdsTools.GetDescription;
end;

procedure TdmRecursos.TallaValidate(Sender: TField);
var
   sDescrip: String;
begin
     with Sender do
          if ( strLleno( AsString ) ) and
             ( not dmTablas.cdsTallas.LookupKey( AsString, '', sDescrip ) ) then
             DataBaseError( 'No Existe el C�digo de Talla: ' + AsString );
end;

{ cdsGridGlobalTools }

procedure TdmRecursos.cdsGridGlobalToolsAlAdquirirDatos(Sender: TObject);
begin
     cdsGridGlobalTools.Data := ServerRecursos.GetGridTools( dmCliente.Empresa, TRUE );
end;

procedure TdmRecursos.cdsGridGlobalToolsAlCrearCampos(Sender: TObject);
begin
     with cdsGridGlobalTools do
     begin
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := ToolsCB_CODIGOChange;
          FieldByName( 'KT_TALLA' ).OnValidate := TallaValidate;
     end;
end;

procedure TdmRecursos.ToolsCB_CODIGOChange(Sender: TField);
begin
     cdsGridGlobalTools.FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
end;

{ cdsGridBackTools }

procedure TdmRecursos.cdsGridBackToolsAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsGridBackTools.Data := ServerRecursos.GetGridToolsBack( Empresa, FEmpleadoTools );
end;

procedure TdmRecursos.cdsGridBackToolsAlCrearCampos(Sender: TObject);
begin
     with cdsGridBackTools do
     begin
          CreateSimpleLookup( dmCatalogos.cdsTools, 'TO_DESCRIP', 'TO_CODIGO' );
          FieldByName( 'KT_ACTIVO' ).OnGetText := KT_ACTIVOGetText;
     end;
end;

procedure TdmRecursos.KT_ACTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := VACIO;
end;

{
procedure TdmRecursos.cdsGridBackToolsReconcileError(DataSet: TClientDataSet;
          E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
const
     K_MESS_PK_VIOLATION = 'Error al Registrar %s!' + CR_LF + 'Empleado: %d' + CR_LF +
                           'Herramienta: %s' + CR_LF + 'Fecha Devoluci�n: %s' + CR_LF + 'Mot. Devoluci�n: %s';
var
    sError, sTabla : String;
begin
     Action := raCancel;
     if PK_Violation( E ) then
     begin
          if ( Pos( 'PCAR_ABO', E.Message ) > 0 ) then
             sTabla := 'Cargo'
          else
             sTabla := 'Pr�stamo';
          with DataSet do
               sError := Format( K_MESS_PK_VIOLATION, [ sTabla,
                                 FieldByName( 'CB_CODIGO' ).AsInteger,
                                 FieldByName( 'TO_CODIGO' ).AsString,
                                 FechaCorta( FieldByName( 'KT_FEC_FIN' ).AsDateTime ),
                                 FieldByName( 'KT_MOT_FIN' ).AsString ] )
     end
     else
         sError := GetErrorDescription( E );

     ZetaDialogo.ZError( 'Error', sError, 0 );
end;
}

{ ********** METODOS PUBLICOS ... INCLUYE METODOS PRIVADOS DE ESTOS ************* }

{ Metodos Adicionales de Historial de Kardex }

procedure TdmRecursos.SetNuevoKardex( const sTipo: String );
begin
     ConectarKardexLookups; // Se ocupa para el create simple lookup de cdsEditHisKardex
     AgregandoKardex:= TRUE;
     if ( ( sTipo <> K_T_VACA ) and ( sTipo <> K_T_PERM ) and ( sTipo <> K_T_INCA ) ) then
     begin
          with cdsEditHisKardex do
          begin
               Conectar;
               Append;
               FieldByName( 'CB_TIPO' ).AsString:= sTipo;
          end;
     end;
end;

procedure TdmRecursos.AgregaKardex(sTipo: String);     // Cuando no se esta posicionado en Historial de Kardex en el tipo deseado
begin
     if ValidaBajaEmpleado( sTipo ) then
     begin
          SetNuevoKardex( sTipo );
          PresentaAltaKardex( sTipo );
          if ( sTipo = K_T_BAJA ) then
          begin
               RevisaTools;
               RevisaUsuario;
               if Global.OK_ProyectoEspecial(peRecontrataciones)then
               begin
                    if FRefrescaHisKardex then
                    begin
                         if ( cdsEditHisKardex.FieldByName('CB_NOMYEAR').AsInteger <> 0 ) and
                            ( cdsEditHisKardex.FieldByName( 'CB_NOMNUME' ).AsInteger <> 0 ) then
                         begin
                              dmCliente.YearDefault := cdsEditHisKardex.FieldByName('CB_NOMYEAR').AsInteger;
                              dmCliente.PeriodoTipo := eTipoPeriodo( cdsEditHisKardex.FieldByName('CB_NOMTIPO').AsInteger );
                              dmCliente.SetPeriodoNumero( cdsEditHisKardex.FieldByName( 'CB_NOMNUME' ).AsInteger );
                              TressShell.CambiaSistemaActivos;
                              TressShell.CambiaPeriodoActivos;
                              dmNomina.AplicarLiquidacion;
                         end;
                    end;
               end;
          end;
          RefrescaKardex( sTipo );
     end;
end;

procedure TdmRecursos.RevisaUsuario;
begin
     {V 2013. SOP-4538 CAS-152930-Desactivaci�n de usuario al procesar una baja que no se lleva a cabo al final}
     if FRefrescaHisKardex then
        dmSistema.BajaUsuario(dmCliente.cdsEmpleado.FieldByName('CB_CODIGO').AsInteger,False);
end;

procedure TdmRecursos.PresentaAltaKardex( sTipo: String );
begin
     if ( sTipo = K_T_CAMBIO ) then
          ZBaseEdicion_DevEx.ShowFormaEdicion( KardexCambio_DevEx,TKardexCambio_DevEx )
     else if ( sTipo = K_T_AREA ) then
          ZBaseEdicion_DevEx.ShowFormaEdicion( KardexArea_DevEx,TKardexArea_DevEx )
     else if ( sTipo = K_T_PUESTO ) then
          ZBaseEdicion_DeVEx.ShowFormaEdicion( KardexPuesto_DevEx,TKardexPuesto_DevEx )
     else if ( sTipo = K_T_RENOVA ) then
          ZBaseEdicion_DevEx.ShowFormaEdicion( KardexRenova_DevEx,TKardexRenova_DevEx )
     else if ( sTipo = K_T_TURNO ) then
          ZBaseEdicion_DevEx.ShowFormaEdicion( KardexTurno_DevEx,TKardexTurno_DevEx )
     else if ( sTipo = K_T_BAJA ) then
          ZBaseEdicion_DevEx.ShowFormaEdicion( KardexBaja_DevEx,TKardexBaja_DevEx )
     else if ( sTipo = K_T_PRESTA ) then
          ZBaseEdicion_DevEx.ShowFormaEdicion( KardexPresta_DevEx,TKardexPresta_DevEx )
     else if ( sTipo = K_T_PLAZA ) then
          ZBaseEdicion_DevEx.ShowFormaEdicion( KardexPlaza_DevEx,TKardexPlaza_DevEx )
     else if ( sTipo = K_T_TIPNOM ) then
          ZBaseEdicion_DevEx.ShowFormaEdicion( KardexTipoNomina_DevEx,TKardexTipoNomina_DevEx )
     else if ( sTipo = K_T_ALTA ) then
     begin
          SetDatosReingreso( FFechaReingreso );
           // Reingresos
           ZBaseEdicion_DevEx.ShowFormaEdicion( KardexAlta_DevEx,TKardexAlta_DevEx );
          {V 2013. SOP-4538 CAS-152930-Desactivaci�n de usuario al procesar una baja que no se lleva a cabo al final}
          if FRefrescaHisKardex then
             ReactivarUsuario(True);
     end
     else if ( sTipo = K_T_VACA ) then  //DevEx (by am): La validacion de vista esta en su evento AlModificar
     begin
          with cdsHisVacacion do
          begin
               Conectar;
               if not IsEmpty and ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
               Agregar;
          end;
     end
     else if ( sTipo = K_T_PERM ) then //DevEx (by am): La validacion de vista esta en su evento AlModificar
     begin
          with cdsHisPermiso do
          begin
               Conectar;
               if not IsEmpty and ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
               Agregar;
          end;
     end
     else if ( sTipo = K_T_INCA ) then  //DevEx (by am): La validacion de vista esta en su evento AlModificar
     begin
          with cdsHisIncapaci do
          begin
               Conectar;
               if not IsEmpty and ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then
                  Refrescar;
               Agregar;
          end;
     end
     else
         ZBaseEdicion_DevEx.ShowFormaEdicion( KardexGeneral_DevEx, TKardexGeneral_DevEx );
end;

procedure TdmRecursos.AgregaKardexMultiple;
var
   iEmpleado: TNumEmp;
begin
     if ValidaBajaEmpleado( K_T_MULTIPLE ) then
     begin
          iEmpleado := dmCliente.Empleado;
          FCambiosMultiples := TRUE;
          try
             ZBaseEdicion_DevEx.ShowFormaEdicion( KardexVarios_DevEx, TKardexVarios_DevEx );
             if ( dmCliente.Empleado <> iEmpleado ) then      // Cambi� de Empleado
                TressShell.CambiaEmpleadoActivos;
             RefrescaKardex( K_T_MULTIPLE );        // Si se indica Tipo MULTIP, se posiciona solo por Fecha
          finally
             FCambiosMultiples :=  FALSE;
          end;
     end;
end;

{
function TdmRecursos.CheckEmpleadoActivoKardex( const sTipo: String; const lActivo: Boolean ): Boolean;
begin
     Result := lActivo or ( ( sTipo <> K_T_CAMBIO ) and ( sTipo <> K_T_PUESTO ) and
                            ( sTipo <> K_T_TURNO ) and ( sTipo <> K_T_AREA ) and
                            ( sTipo <> K_T_RENOVA ) and ( sTipo <> K_T_PRESTA ) and
                            ( sTipo <> K_T_MULTIPLE ) and ( sTipo <> VACIO ) );
end;
}

function TdmRecursos.ValidaBajaEmpleado( const sTipo: String ): Boolean;
begin
     Result := ( not EsKardexRequiereActivo( sTipo ) ) or CheckEmpleadoActivoBaja;
end;

function TdmRecursos.EsKardexRequiereActivo( const sTipo: String ): Boolean;
begin
     Result := ( ( sTipo = K_T_CAMBIO ) or ( sTipo = K_T_PUESTO ) or
                 ( sTipo = K_T_TURNO ) or ( sTipo = K_T_AREA ) or
                 ( sTipo = K_T_RENOVA ) or ( sTipo = K_T_PRESTA ) or
                 ( sTipo = K_T_MULTIPLE ) or ( sTipo = VACIO ) or
                 ( sTipo = K_T_PLAZA ) );
end;

function TdmRecursos.ShowAdvertenciaBaja( const iEmpleado: Integer; const sNombre: String;
         const dBaja: TDate ): Boolean;
const
     K_MESS_ADVERTENCIA_BAJA = 'El Empleado %d: %s' + CR_LF + 'Ya Fu� Dado De Baja El %s';
begin
     Result :=ZetaDialogo.ZWarningConfirm( K_TITLE_ADVERTENCIA, Format( K_MESS_ADVERTENCIA_BAJA, [
                                           iEmpleado, sNombre, FechaCorta( dBaja ) ] ) + CR_LF +
                                           CR_LF + '� Desea Continuar ?', 0, mbCancel );
end;

function TdmRecursos.CheckEmpleadoActivoBaja: Boolean;
begin
     with dmCliente.GetDatosEmpleadoActivo do
     begin
          Result := Activo;
          if not Result then
             Result := ShowAdvertenciaBaja( Numero, Nombre, Baja );
     end;
end;

function TdmRecursos.KardexBuscaUltimo( sTipo: String ): Boolean;
var
   Pos : TBookMark;
begin
     with cdsHisKardex do
     begin
          Filter:= Format( 'CB_TIPO = ''%s'' or CB_TIPO = ''%s'' ', [ K_T_ALTA, sTipo ] );
          Filtered:= True;
          if FOrdenKardex = eokAscendente then
             Last;
          Result:= not IsEmpty;
          if Result then
          begin
               Pos:= GetBookMark;
               Filtered:= False;
               if ( Pos <> nil ) then
               begin
                    GotoBookMark( Pos );
                    FreeBookMark( Pos );
               end;
          end
          else
               Filtered:= False;
     end;
end;

procedure TdmRecursos.RefrescaKardex( sTipo: String );
var
   oCursor: TCursor;
begin
     if FRefrescaHisKardex then
     begin
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  with cdsHisKardex do
                       if Active then
                       begin
                            Refrescar;
                            if sTipo = K_T_MULTIPLE then
                               //Locate('CB_FECHA', ZetaCommonTools.FechaAsStr( FOtrosDatos[0] ), [] )   // Cuando se usa FKardexVarios, aqu� se guarda la fecha del movimiento
                               Locate('CB_FECHA', FOtrosDatos[0] , [] )   // Cuando se usa FKardexVarios, aqu� se guarda la fecha del movimiento
                            else
                               Locate('CB_FECHA;CB_TIPO', GetcdsInfo( sTipo ), [] );
                       end;

                       if ( strLleno( sTipo ) ) and ( ( sTipo <> K_T_PERM ) and ( sTipo <> K_T_INCA ) ) then
                       begin
                            dmCliente.cdsEmpleado.Refrescar;  // Solo cuando Cambien Datos de Colabora

                            if ( sTipo = K_T_CAMBIO ) or ( sTipo = K_T_BAJA ) or ( sTipo = K_T_ALTA ) or ( sTipo = K_T_PRESTA ) or ( sTipo = K_T_PUESTO ) or ( sTipo = K_T_MULTIPLE ) or ( sTipo = K_T_PLAZA ) then
                            begin
                                 if ( sTipo = K_T_CAMBIO ) or ( sTipo = K_T_ALTA ) or ( sTipo = K_T_MULTIPLE ) or ( sTipo = K_T_PLAZA ) then
                                     cdsEmpPercepcion.Active := FALSE;    //Menos tardado que Refrescar
                                 if ( sTipo = K_T_BAJA ) or ( sTipo = K_T_ALTA ) then
                                     TressShell.CargaEmpleadoActivos;
                                 if ( sTipo = K_T_BAJA ) or ( sTipo = K_T_ALTA ) or ( sTipo = K_T_PRESTA ) then
                                     cdsHisVacacion.Active := FALSE;
                                 if ( sTipo = K_T_PUESTO ) or ( sTipo = K_T_BAJA ) or ( sTipo = K_T_ALTA ) or ( sTipo = K_T_MULTIPLE ) or ( sTipo = K_T_PLAZA ) then
                                     cdsOrganigrama.SetDataChange;
                                 TressShell.ReconectaFormaActiva;
                            end;
                       end;
                       FRefrescaHisKardex:= FALSE;
               finally
                  Cursor := oCursor;
               end;
          end;
     end;
end;

function TdmRecursos.GetcdsInfo( const sTipo: String ): variant;
begin
     {
     if ( sTipo = K_T_VACA ) then
        Result := VarArrayOf( [ ZetaCommonTools.FechaAsStr( cdsHisVacacion.FieldByName( 'VA_FEC_INI' ).AsDateTime ), sTipo ] )
     else if ( sTipo = K_T_PERM ) then
        Result := VarArrayOf( [ ZetaCommonTools.FechaAsStr( cdsHisPermiso.FieldByName( 'PM_FEC_INI' ).AsDateTime ), sTipo ] )
     else if ( sTipo = K_T_INCA ) then
        Result := VarArrayOf( [ ZetaCommonTools.FechaAsStr( cdsHisIncapaci.FieldByName( 'IN_FEC_INI' ).AsDateTime ), sTipo ] )
     else
        Result := VarArrayOf( [ ZetaCommonTools.FechaAsStr( cdsEditHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime ), cdsEditHisKardex.FieldByName( 'CB_TIPO' ).AsString ] );
end; }
    if ( sTipo = K_T_VACA ) then
        Result := VarArrayOf( [ cdsHisVacacion.FieldByName( 'VA_FEC_INI' ).AsDateTime, sTipo ] )
     else if ( sTipo = K_T_PERM ) then
        Result := VarArrayOf( [ cdsHisPermiso.FieldByName( 'PM_FEC_INI' ).AsDateTime, sTipo ] )
     else if ( sTipo = K_T_INCA ) then
        Result := VarArrayOf( [ cdsHisIncapaci.FieldByName( 'IN_FEC_INI' ).AsDateTime, sTipo ] )
     else
        Result := VarArrayOf( [ cdsEditHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime, cdsEditHisKardex.FieldByName( 'CB_TIPO' ).AsString ] );
end;

procedure TdmRecursos.SetOrdenKardex(const eOrden: TOrdenKardex);
procedure  OrdenaPorColumna(ColumnIndexName, ColumnFieldName: String; DataSet: TZetaClientDataset; lDescendente: TIndexOptions );
var
   sField, sIndex, sFieldName: String;

begin
  sField := ColumnIndexName +'Index';
  sFieldName := ColumnFieldName;
  with DataSet do
     begin
          sIndex := IndexName;
          if sIndex <> '' then
          begin
               GetIndexInfo(sIndex);
               IndexDefs.Update;
               DeleteIndex(sIndex);
               IndexDefs.Update;
          end;
          AddIndex( sField, sFieldName, lDescendente);
          IndexDefs.Update;
          IndexName := sField;
          First;
     end;
end;
begin
    FOrdenKardex := eOrden;
    if cdsHisKardex.Active then
    begin
        if eOrden = eokAscendente then
            OrdenaPorColumna('CB_FECHA','CB_FECHA;CB_NIVEL', cdsHisKardex, [])
        else
            OrdenaPorColumna('CB_FECHA','CB_FECHA;CB_NIVEL', cdsHisKardex,  [ixDescending]);
    end;
end;

{
procedure TdmRecursos.SetOrdenPrestamo(const eOrden: TOrdenKardex);
begin
    FOrdenPrestamo := eOrden;
    with cdsHisPrestamos do
    begin
         if eOrden = eokAscendente then
           IndexName := 'Ascendente'
         else
           IndexName := 'Descendente';
         if Active then
            First;
    end;
end;  }

procedure TdmRecursos.SetOrdenPrestamo(const eOrden: TOrdenKardex);
procedure  OrdenaPorColumna(ColumnIndexName, ColumnFieldName: String; DataSet: TZetaClientDataset; lDescendente: TIndexOptions );
var
   sField, sIndex, sFieldName: String;

begin
  sField := ColumnIndexName +'Index';
  sFieldName := ColumnFieldName;
  with DataSet do
     begin
          sIndex := IndexName;
          if sIndex <> '' then
          begin
               GetIndexInfo(sIndex);
               IndexDefs.Update;
               DeleteIndex(sIndex);
               IndexDefs.Update;
          end;
          AddIndex( sField, sFieldName, lDescendente);
          IndexDefs.Update;
          IndexName := sField;
          First;
     end;
end;
begin
    FOrdenPrestamo := eOrden;
    if cdsHisPrestamos.Active then
    begin
        if eOrden = eokAscendente then
            OrdenaPorColumna('PR_FECHA','PR_FECHA', cdsHisPrestamos, [])
        else
            OrdenaPorColumna('PR_FECHA','PR_FECHA', cdsHisPrestamos,  [ixDescending]);
    end;
end;

procedure TdmRecursos.RevisaTools( const lShowGrid: Boolean = TRUE );
var
   sMensaje: String;
begin
     if FRefrescaHisKardex and ( dmCliente.ModuloAutorizadoConsulta( okHerramientas, sMensaje ) ) then
     begin
          EmpleadoTools := dmCliente.Empleado;
          dmCatalogos.cdsTools.Conectar;
          with cdsGridBackTools do
          begin
               Refrescar;
               if not IsEmpty then
               begin
                    if lShowGrid then
                    begin
                         ZBaseGridEdicion_DevEx.ShowGridEdicion( GridRegresarTools_DevEx, TGridRegresarTools_DevEx, cdsEditHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime );
                    end
                    else
                    begin
                         while not EOF do
                         begin
                              Edit;
                              FieldByName( 'KT_ACTIVO' ).AsString := K_GLOBAL_NO;
                              FieldByName( 'KT_FEC_FIN' ).AsDateTime := cdsEditHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime;
                              FieldByName( 'KT_MOT_FIN' ).AsString := K_T_BAJA;
                              FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                              Next;
                         end;
                         Enviar;
                    end;
               end;
          end;
     end;
end;

{ Llenar Lista de Percepciones Fijas }

procedure TdmRecursos.AgregaPercepcion( DataSet : TClientDataSet; Lista : TStrings );
var
   oDatos : TOtrasPer;
begin
     oDatos := TOtrasPer.Create;
     with oDatos, DataSet do
     begin
          Codigo:= FieldByName( 'TB_CODIGO' ).AsString;
          Monto := FieldByName( 'TB_MONTO' ).AsFloat;
          Tipo  := eTipoOtrasPer( FieldByName( 'TB_TIPO' ).AsInteger );
          Tasa  := FieldByName( 'TB_TASA' ).AsFloat;
          Imss  := eIMSSOtrasPer( FieldByName( 'TB_IMSS' ).AsInteger );
          ISPT  := eISPTOtrasPer( FieldByName( 'TB_ISPT' ).AsInteger );
          Tope  := FieldByName( 'TB_TOPE' ).AsFloat;
     end;
     Lista.AddObject( oDatos.Codigo + '=' + DataSet.FieldByName( 'TB_ELEMENT' ).AsString, oDatos );
end;

procedure TdmRecursos.LlenaOtrasPer( Lista : TStrings );
begin
     LimpiaLista( Lista );
     with dmCatalogos do
     begin
          with cdsOtrasPer do
          begin
               DisableControls;
               try
                  Conectar;
                  First;
                  while NOT EOF do
                  begin
                       AgregaPercepcion( cdsOtrasPer, Lista );
                       Next;
                  end;
               finally
                  EnableControls;
               end;
          end;
     end;
end;

procedure TdmRecursos.LlenaListaTabulador( Lista: TStrings ; sClasifi : String;
                                           var dSalario : TPesos );
   procedure Agrega( const sCodigo : string );
   begin
        if sCodigo > '' then
           with dmCatalogos do
           begin
                cdsOtrasPer.Locate( 'TB_CODIGO',sCodigo,[loCaseInsensitive] );
                AgregaPercepcion( cdsOtrasPer, Lista );
           end;
   end;

begin
     LimpiaLista( Lista );
     with dmCatalogos do
     begin
          cdsClasifi.Conectar;
          cdsOtrasPer.Conectar;

          with cdsClasifi do
          begin
               //if sClasifi = '' then sClasifi := cdsEditHisKardex.FieldByName( 'CB_CLASIFI' ).AsString;
               if cdsClasifi.Locate( 'TB_CODIGO', sClasifi, [loCaseInsensitive] ) then
               begin
                    dSalario := FieldByName( 'TB_SALARIO' ).AsFloat;
                    Agrega( FieldByName( 'TB_OP1' ).AsString );
                    Agrega( FieldByName( 'TB_OP2' ).AsString );
                    Agrega( FieldByName( 'TB_OP3' ).AsString );
                    Agrega( FieldByName( 'TB_OP4' ).AsString );
                    Agrega( FieldByName( 'TB_OP5' ).AsString );
               end
               else
                    dSalario := 0;
          end;
     end;
end;

function TdmRecursos.GetSalClasifi( const sClasifi: String ): TPesos;
begin
     Result:= 0;
     with dmCatalogos.cdsClasifi do
     begin
          Conectar;
          if Locate( 'TB_CODIGO', sClasifi, [loCaseInsensitive] ) then
             Result := FieldByName( 'TB_SALARIO' ).AsFloat;
     end;
end;

{ Procesar Reingresos }

procedure TdmRecursos.SetDatosReingreso( dFecha: TDate );
begin
     with dmCliente, cdsEditHisKardex do
     begin
          FieldByName( 'CB_CODIGO' ).AsInteger := dmCliente.Empleado;
          FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CB_FECHA' ).AsDateTime := dFecha;

          FieldByName( 'CB_AUTOSAL' ).AsString  := cdsEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString;
          FieldByName( 'CB_CLASIFI' ).AsString  := cdsEmpleado.FieldByName( 'CB_CLASIFI' ).AsString;
          FieldByName( 'CB_CONTRAT' ).AsString  := cdsEmpleado.FieldByName( 'CB_CONTRAT' ).AsString;
          FieldByName( 'CB_TURNO' ).AsString    := cdsEmpleado.FieldByName( 'CB_TURNO' ).AsString;
          FieldByName( 'CB_MOT_BAJ' ).AsString  := cdsEmpleado.FieldByName( 'CB_MOT_BAJ' ).AsString;
          FieldByName( 'CB_PATRON' ).AsString   := cdsEmpleado.FieldByName( 'CB_PATRON' ).AsString;
          FieldByName( 'CB_PUESTO' ).AsString   := cdsEmpleado.FieldByName( 'CB_PUESTO' ).AsString;
          FieldByName( 'CB_SALARIO' ).AsFloat   := cdsEmpleado.FieldByName( 'CB_SALARIO' ).AsFloat;
          FieldByName( 'CB_TABLASS' ).AsString  := cdsEmpleado.FieldByName( 'CB_TABLASS' ).AsString;
          FieldByName( 'CB_ZONA_GE' ).AsString  := cdsEmpleado.FieldByName( 'CB_ZONA_GE' ).AsString;;
          FieldByName( 'CB_NIVEL1' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL1' ).AsString;
          FieldByName( 'CB_NIVEL2' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL2' ).AsString;
          FieldByName( 'CB_NIVEL3' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL3' ).AsString;
          FieldByName( 'CB_NIVEL4' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL4' ).AsString;
          FieldByName( 'CB_NIVEL5' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL5' ).AsString;
          FieldByName( 'CB_NIVEL6' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL6' ).AsString;
          FieldByName( 'CB_NIVEL7' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL7' ).AsString;
          FieldByName( 'CB_NIVEL8' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL8' ).AsString;
          FieldByName( 'CB_NIVEL9' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL9' ).AsString;
          //TI-Group cr 3161
          if DentroBimestre(dFecha, GetDatosEmpleadoActivo.Baja)then
             FieldByName( 'CB_PER_VAR' ).AsFloat   := cdsEmpleado.FieldByName( 'CB_PER_VAR' ).AsFloat
          else
              FieldByName( 'CB_PER_VAR' ).AsFloat := 0;
          {$ifdef ACS}
          FieldByName( 'CB_NIVEL10' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL10' ).AsString;
          FieldByName( 'CB_NIVEL11' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL11' ).AsString;
          FieldByName( 'CB_NIVEL12' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL12' ).AsString;
          {$endif}
     end;
end;

procedure TdmRecursos.ProcesaReingreso;

          function GetNota: String;
          begin
               ConectarKardexLookups;
               with dmCliente.GetDatosEmpleadoActivo  do
               begin
                    cdsEditHisKardex.Data := ServerRecursos.GetEditHisKardex( dmCliente.Empresa, Numero, Baja, K_T_BAJA );

                    Result:= cdsEditHisKardex.FieldByName('CB_NOTA').AsString;
                    if strLleno( Result ) then
                       Result:= CR_LF + CR_LF + 'Motivo:  ' + Result;
               end;
          end;
		  
		  {*** US 11793: Pay Human Link No calcula la liquidaci�n del mes de �septiembre� en adelante de un empleado Reingreso donde las fechas abarcan 2 registros patronales. ***}
          function ObtenerFechaBajaIMSS: TDate;
          begin
               ConectarKardexLookups;
               with dmCliente.GetDatosEmpleadoActivo  do
               begin
                    cdsEditHisKardex.Data := ServerRecursos.GetEditHisKardex( dmCliente.Empresa, Numero, Baja, K_T_BAJA );
                    Result:= cdsEditHisKardex.FieldByName('CB_FECHA_2').AsDateTime;
               end;
          end;

begin
     with dmCliente.GetDatosEmpleadoActivo do
     begin
          if Activo then
             ZError('','El Empleado ' + Nombre + CR_LF+ ' NO ha Sido Dado de Baja '+ CR_LF +
                       'NO se puede efectuar ReIngreso !!', 0)
          else
          if ( ( Recontratable ) or
             ( ZWarningConfirm('� Atenci�n !', Format( 'Este empleado esta marcado como No Recontratable %s %s %s� Desea Continuar con el Reingreso ?',[ GetNota, CR_LF, CR_LF ]) , 0, mbCancel ) ) )then
          begin
                if ( KardexReingreso_DevEx = nil ) then
                    KardexReingreso_DevEx := TKardexReingreso_DevEx.Create( Application );
                with KardexReingreso_DevEx, dmCliente do
                begin
                        ValorActivo1.Caption:= GetEmpleadoDescripcion;
                        ZReingreso.Valor:= FechaDefault;
                        FechaBaja:= GetDatosEmpleadoActivo.Baja;
                        FechaBajaIMSS := ObtenerFechaBajaIMSS;
                        ShowModal;
                        {if ModalResult = mrOK then   // Proceder con el Reingreso
                        begin
                              FFechaReingreso := ZReingreso.Valor;
                              FTabKardex := K_TAB_CONTRATA;
                              AgregaKardex( K_T_ALTA );  // Se considera Reingreso, las Altas se procesan sobre cdsEmpleado
                              TressShell.RefrescaEmpleado;
                        end;}
                end;
          end;
     end;
end;

procedure TdmRecursos.AplicaKardexReingreso(const dFecReingreso: TDate );
begin
     FFechaReingreso := dFecReingreso;
     FTabKardex := K_TAB_CONTRATA;
     AgregaKardex( K_T_ALTA );  // Se considera Reingreso, las Altas se procesan sobre cdsEmpleado
     {V 2013. SOP-4538 CAS-152930-Desactivaci�n de usuario al procesar una baja que no se lleva a cabo al final}
     // ReactivarUsuario(True);     
     TressShell.RefrescaEmpleado;
end;


{ Metodos de dmCliente.cdsEmpleado }

function TdmRecursos.cdsDatosEmpleado: TZetaClientDataSet;
begin
     Result:= dmCliente.cdsEmpleado;
end;

procedure TdmRecursos.GrabaEmpleado;
var
   ErrorCount : Integer;
{$ifndef DOS_CAPAS}
   lAlta, lCambioIDBio, lCambioGrupoBio: Boolean; // SYNERGY
   idBio: Integer;
{$endif}
begin
     ErrorCount := 0;
     with dmCliente.cdsEmpleado do
     begin
{$ifndef DOS_CAPAS}
          // SYNERGY
          lAlta := ( State = dsInsert );
          lCambioIDBio := lAlta;
          lCambioGrupoBio := False;
          idBio := 0;
{$endif}

          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
{$ifndef DOS_CAPAS}
               // SYNERGY
               if not lCambioIDBio then
                  lCambioIDBio := ( ( ChangeCount > 0 ) and
                                    ( ( ZetaServerTools.CambiaCampo( FieldByName( 'CB_ID_BIO' ) ) ) or
                                    ( ZetaServerTools.CambiaCampo( FieldByName( 'CB_GP_COD' ) ) ) ) );

               if( lCambioIDBio )then
               begin
                    if ( FieldByName( 'CB_ID_BIO' ).AsInteger = 0 ) and
                       ( FieldByName( 'CB_GP_COD' ).AsString = VACIO ) then
                       lCambioGrupoBio := False
                    else
                    begin
                         lCambioIDBio := ZetaServerTools.CambiaCampo( FieldByName( 'CB_ID_BIO' ) );
                         if lCambioIDBio then
                            idBio := dmSistema.ObtenMaxIdBio
                         else
                         begin
                              lCambioGrupoBio := ZetaServerTools.CambiaCampo( FieldByName( 'CB_GP_COD' ) );
                              if not lCambioGrupoBio then
                              begin
                                   if ( dmSistema.ObtenMaxIdBio <> FieldByName( 'CB_ID_BIO' ).AsInteger ) then
                                   begin
                                        idBio := dmSistema.ObtenMaxIdBio;
                                        lCambioIDBio := True;
                                   end
                                   else
                                   begin
                                        idBio := FieldByName( 'CB_ID_BIO' ).AsInteger;
                                   end;
                              end;
                         end;
                         if lCambioIDBio then
                         begin
                              if not( State in [ dsEdit, dsInsert ] )then
                                 Edit;
                              FieldByName( 'CB_ID_BIO' ).AsInteger := idBio;
                              PostData;
                         end;
                    end;
               end;

{$endif}
               if FCambiosMultiples then
               begin
                    Reconcile ( ServerRecursos.GrabaCambiosMultiples( dmCliente.Empresa, Delta, FOtrosDatos, ErrorCount ) );
                    if ErrorCount = 0 then
                       FRefrescaHisKardex:= TRUE;
               end
               else
               begin
                    Reconcile ( ServerRecursos.GrabaDatosEmpleado( dmCliente.Empresa, Delta, FOtrosDatos, FListaPercepFijas, ErrorCount ) );
                    if ( ErrorCount = 0 )  then
                    begin
                         if lAlta or lCambioIDBio then
                            dmSistema.ActualizaIdBio( FieldByName( 'CB_CODIGO' ).AsInteger, dmSistema.ObtenMaxIdBio, FieldByName( 'CB_GP_COD' ).AsString, 0 )
                         else if lCambioGrupoBio then
                              dmSistema.ActualizaIdBio( FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CB_ID_BIO' ).AsInteger, FieldByName( 'CB_GP_COD' ).AsString, 0 );

                         TressShell.SetDataChange( [ enPlazas ] );
                         if ( FDesconectaVaca ) then
                         begin
                              cdsHisVacacion.Active:= FALSE;
                              FDesconectaVaca:= FALSE;
                         end
                    end;
               end;
          end;
     end;
end;

function TdmRecursos.ProximoEmpleado: Integer;
begin
     Result:= ServerRecursos.GetLastNumeroEmpleado( dmCliente.Empresa ) + 1;
end;

procedure TdmRecursos.ModificaDigitoVerificador( sSegSoc: String );
var
   iLon : integer;
begin
     with dmCliente.cdsEmpleado do
     begin
          iLon := Length( sSegSoc );
          if iLon = 10 then
          begin
               if not ( state in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_SEGSOC' ).AsString:= sSegSoc + ZetaCommonTools.CalculaDigito( sSegSoc )
          end
          else
              if iLon < 10 then
                 DataBaseError( 'Para Calcular el D�gito Verificador, el N�mero de Seguro Social debe tener 10 D�gitos' )
              else
                  if iLon = 11 then
                     DataBaseError( 'El D�gito Verificador ya Est� Calculado' );
     end;
end;

procedure TdmRecursos.CalculaRFC;
begin
     { Este M�todo se utiliza desde la forma de Edici�n de Identificaci�n del Empleado }
     with dmCliente.cdsEmpleado do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( 'CB_RFC' ).AsString:= ZetaCommonTools.CalcRFC( FieldByName( 'CB_APE_PAT' ).AsString,
                                                                      FieldByName( 'CB_APE_MAT' ).AsString,
                                                                      FieldByName( 'CB_NOMBRES' ).AsString,
                                                                      FieldByName( 'CB_FEC_NAC' ).AsDateTime );
     end;
end;

procedure TdmRecursos.CalculaCURP;
begin
     { Este M�todo se utiliza desde la forma de Edici�n de Identificaci�n del Empleado }
     with dmCliente.cdsEmpleado do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( 'CB_CURP' ).AsString:= ZetaCommonTools.CalcCURP( FieldByName( 'CB_APE_PAT' ).AsString,
                                                                        FieldByName( 'CB_APE_MAT' ).AsString,
                                                                        FieldByName( 'CB_NOMBRES' ).AsString,
                                                                        FieldByName( 'CB_FEC_NAC' ).AsDateTime,
                                                                        FieldByName( 'CB_SEXO' ).AsString,
                                                                        dmTablas.GetEstadoCURP( FieldByName( 'CB_ENT_NAC' ).AsString ) );
     end;
end;

procedure TdmRecursos.ValidaBancaElectronica( const sTitulo, sBanca , sCampo : String );
var
   sMensaje,sMessage : WideString;
begin
       if sCampo = 'CB_BAN_ELE' then
     begin
          sMessage := 'Banca Electr�nica'
     end
     else if sCampo = 'CB_CTA_GAS' then
     begin
          sMessage := 'Cuenta de Tarjeta Gasolina'
     end
     else if sCampo = 'CB_CTA_VAL' then
     begin
          sMessage := 'Cuenta de Tarjeta Despensa'
     end;
     with dmCliente do
     begin
          if strVacio( sBanca ) then
             ZInformation( sTitulo, Format('N�mero de %s est� vac�o, capt�relo y presione de nuevo el bot�n para verificar que no pertenezca a otro empleado',[sMessage]), 0 )
          else if ( ServerRecursos.ExisteBancaElectronica( Empresa, sBanca, cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger, sMensaje,sCampo ) ) then
             ZError( sTitulo, sMensaje, 0 )
          else
             ZInformation( sTitulo, Format('N�mero Correcto, no se tienen empleados que tengan ese n�mero de %s',[sMessage]), 0 );
     end;
end;

function TdmRecursos.ValidacionForzadaCuenta( const sTitulo, sBanca , sCampo : String ): Boolean;
var
   sMensaje : WideString;
begin
     with dmCliente do
     begin
          Result := ServerRecursos.ExisteBancaElectronica( Empresa, sBanca, cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger, sMensaje, sCampo );
          if Result then
             ZError( sTitulo, sMensaje, 0 );
     end;

end;

{ Agregar Cierre de Vacaciones }

procedure TdmRecursos.AgregaCierreVacacion;
{
var
   sMensaje: String;
}
begin
     //if CheckBajaEmpleado( 'No se Puede Registrar Cierre de Vacaciones', sMensaje ) then
     if CheckEmpleadoActivoBaja then
     begin
          dmSistema.cdsUsuarios.Conectar;         // Se ocupa para el create simple lookup de cdsHisVacacion
          with cdsHisVacacion do
          begin
               Conectar;
               Append;
               FieldByName( 'VA_TIPO' ).AsInteger:= Ord( tvCierre );
               Modificar;
          end;
     end;
{
     else
          ZError( '', sMensaje, 0 );
}
end;

{ Registro - Grid de Cursos }

procedure TdmRecursos.EditarGridCursos;
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpCursos );
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridCursos_DevEx, TGridCursos_DevEx, TRUE );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
     cdsHisCursos.Active := FALSE;
     dmCatalogos.cdsSesiones.SetDataChange;
     TressShell.ReconectaFormaActiva;  // Solo Refresca si la forma Activa incluye a cdsHisCursos
end;

{ Registro - Grid de Herramientas }

procedure TdmRecursos.EditarGridEmpTools;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridEmpTools_DevEx, TGridEmpTools_DevEx, TRUE );
end;

procedure TdmRecursos.EditarGridGlobalTools;
begin
     try
        dmCliente.SetLookupEmpleado;
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridGlobalTools_DevEx, TGridGlobalTools_DevEx, TRUE );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmRecursos.EditarGridRegresarTools;
begin
     with cdsGridBackTools do
          if Active then
          begin
               EmptyDataSet;
               FEmpleadoTools := 0;
          end;
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridRegresarTools_DevEx, TGridRegresarTools_DevEx, FALSE );
end;

{ Otros M�todos }

function TdmRecursos.GetStatus(iEmpleado: integer; dFecha: TDate): eStatusEmpleado;
begin
    Result := eStatusEmpleado(ServerRecursos.GetStatus(dmCliente.Empresa, iEmpleado, dFecha));
end;

function TdmRecursos.GetDatosClasificacion(const iEmpleado: TNumEmp; const dValor: TDate): TDatosClasificacion;
begin
     Result := ZetaCommonTools.DatosClasificacionFromVariant( ServerRecursos.GetDatosClasificacion( dmCliente.Empresa, iEmpleado, dValor ) );
end;

function TdmRecursos.GetDatosSalario(const iEmpleado: TNumEmp; const dInicial, dFinal: TDate): TDatosSalario;
begin
     Result := ZetaCommonTools.DatosSalarioFromVariant( ServerRecursos.GetDatosSalario( dmCliente.Empresa, iEmpleado, dInicial, dFinal ) );
end;

function TdmRecursos.CalculaVencimiento( dFecha : TDateTime; iDuracion : Integer ) : TDateTime;
begin
     if iDuracion = 0 then
         Result := 0
     else
         Result := dFecha + iDuracion - 1;
end;

function TdmRecursos.DerechoAlta(var sMensaje: String): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_EMP_REG_ALTA, K_DERECHO_CONSULTA );
     if not Result then
        sMensaje := 'Usuario no Posee Derechos para dar de Alta Empleados';
end;

function TdmRecursos.DerechoBaja(var sMensaje: String): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_EMP_REG_BAJA, K_DERECHO_CONSULTA );
     if not Result then
        sMensaje := 'Usuario no Posee Derechos para dar de Baja Empleados';
end;

{
function TdmRecursos.CheckBajaEmpleado(const sAccion: String; var sMensaje: String): Boolean;
begin
     with dmCliente.GetDatosEmpleadoActivo do
     begin
          Result := Activo;
          if not Result then
             sMensaje := 'El Empleado ' + IntToStr( Numero ) + ': '+ Nombre + CR_LF + ' ya fu� dado de Baja el '+
                         FechaCorta( Baja ) + CR_LF + sAccion;
     end;
end;
}

procedure TdmRecursos.ConectarKardexLookups;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmTablas.cdsMovKardex.Conectar;
end;

function TdmRecursos.AbreGridInfoAlta : Boolean;
begin
     {$IFDEF TRESS_DELPHIXE5_UP}
     FGridInfoAlta_devEx := TFGridInfoAlta_DevEx.Create( Self );
     try
        with FGridInfoAlta_devEx do
        begin
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     finally
            FreeAndNil( FGridInfoAlta_DevEx );
     end;
     {$ELSE}
     FGridInfoAlta := TFGridInfoAlta.Create( Self );
     try
        with FGridInfoAlta do
        begin
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     finally
            FreeAndNil( FGridInfoAlta );
     end;
     {$ENDIF}
end;

function TdmRecursos.RevisaInformacionParams( Parametros: TZetaParams ): Boolean;
begin
     with cdsInfoEmp do
     begin
          Data := ServerRecursos.ValidaInformacion( dmCliente.Empresa, Parametros.VarValues );
          Result := IsEmpty;
          if not Result then
          begin
               Result := AbreGridInfoAlta;
          end;
     end;
end;

function TdmRecursos.ChecaAcumulados: Boolean;
begin
     with cdsHisAcumulados do
     begin
          dmCatalogos.cdsConceptos.Conectar;
          Conectar;
          Result := IsEmpty;
     end;
end;

function TdmRecursos.ValidaVacantes( const sPuesto: String ): Boolean;
var
   iPlazas, iTitulares : Integer;

   function GetErrorMsg: string;
   begin
        Result := 'No se Tienen Vacantes para Este Puesto' + CR_LF +
                  'Plazas = ' + IntToStr( iPlazas ) + ' ' + K_PIPE + ' ' +
                  'Ocupadas = ' + IntToStr( iTitulares );
   end;
   
begin
     Result := ( not ( Global.GetGlobalBooleano( K_GLOBAL_RH_VALIDA_ORGANIGRAMA ) ) ) or      // No se Aplican Restricciones de Organigrama �
               strVacio( sPuesto );                                                           // El c�digo de Puesto est� vacio
     if ( not Result ) then
     begin
          with dmCatalogos.cdsPuestos do
          begin
               Conectar;
               if ( not IsEmpty ) then
               begin
                    if ( sPuesto = FieldByName( 'PU_CODIGO' ).AsString ) or
                       Locate( 'PU_CODIGO', sPuesto, [] ) then
                    begin
                         iPlazas    := FieldByName( 'PU_PLAZAS' ).AsInteger;
                         iTitulares := GetPlazasOcupadas( sPuesto );
                         Result := ( iTitulares < iPlazas );
                         if ( not Result ) then
                         begin
                              if ( ZAccesosMgr.CheckDerecho( D_EMP_REG_ALTA, K_DERECHO_VACANTES ) ) then   // Si tiene el Derecho de Acceso Confirma
                              begin
                                   if ( ZetaDialogo.ZConfirm( K_TITLE_ADVERTENCIA, GetErrorMsg + CR_LF + CR_LF + '� Desea Continuar ?', 0, mbNo ) ) then
                                      Result := True;
                              end
                              else
                                  ZetaDialogo.ZError( 'Error', GetErrorMsg, 0 );
                         end
                    end;
               end
               else
                    Result := TRUE;
          end;
     end;
end;

function TdmRecursos.GetPlazasOcupadas( const sPuesto: string ): integer;
begin
     Result := ServerRecursos.GetQtyTitularesPuesto( dmCliente.Empresa, sPuesto );
end;

procedure TdmRecursos.LlenaGridOrganigrama( const iPlaza: Integer );
begin
     with cdsOrganigrama do
     begin
          Data := ServerRecursos.GetEmpOrganigrama( dmCliente.Empresa,iPlaza );
     end;
end;

procedure TdmRecursos.GetOrganigramaFoto( const iEmpleado: integer );
begin
     with cdsFotoOrganigrama do
     begin
          if ( not Active ) or ( FieldByName( 'CB_CODIGO' ).AsInteger <> iEmpleado ) then
             Data := ServerRecursos.GetIdentFoto( dmCliente.Empresa, iEmpleado );
     end;
end;

function TdmRecursos.ValidaSalario( const rOldSalario, rNewSalario: TPesos ): Boolean;
const
     K_MESS_SALARIO_MENOR = 'El Salario Nuevo (%s) es Menor Al Salario Actual (%s)';
begin
     Result := ( rOldSalario <= rNewSalario ) or
               ZetaDialogo.ZWarningConfirm( K_TITLE_ADVERTENCIA,
                                            Format( K_MESS_SALARIO_MENOR, [
                                                    FormatFloat( K_MASCARA_FLOAT, rNewSalario ),
                                                    FormatFloat( K_MASCARA_FLOAT, rOldSalario ) ] ) + CR_LF + CR_LF + '� Desea Continuar ?', 0, mbCancel );
end;

procedure TdmRecursos.BuscaCandidato( Parametros: TZetaParams );
const
     K_MESS_NO_CANDIDATOS = 'No Se Encontraron Candidatos Contratados';
var
   lEncontrado: WordBool;
begin
     try
        with dmCliente do
        begin
             cdsInfoCandidato.Data := ServerRecursos.BuscaCandidato( Empresa, EmpresaSeleccion,
                                      Parametros.VarValues, lEncontrado );
        end;
        if cdsInfoCandidato.IsEmpty then
           ZetaDialogo.ZWarning( K_TITLE_ADVERTENCIA, K_MESS_NO_CANDIDATOS, 0, mbOK )
        else
        begin
             if ZBaseGridShow_DevEx.GridShow( cdsInfoCandidato, TCandidatosGridShow_DevEx ) then
                TraspasaInfoCandidato;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

Procedure TdmRecursos.TraspasaInfoCandidato;
const
     K_DESCRIP_IDIOMA = '%s %d%%';
var
   iIngles, iOtroIdioma: Integer;
   lHabla: Boolean;
   sDescripIdioma: String;
begin
     with dmCliente.cdsEmpleado do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( 'CB_APE_PAT' ).AsString := cdsInfoCandidato.FieldByName( 'SO_APE_PAT' ).AsString;
          FieldByName( 'CB_APE_MAT' ).AsString := cdsInfoCandidato.FieldByName( 'SO_APE_MAT' ).AsString;
          FieldByName( 'CB_NOMBRES' ).AsString := cdsInfoCandidato.FieldByName( 'SO_NOMBRES' ).AsString;
          FieldByName( 'CB_FEC_NAC' ).AsDateTime := cdsInfoCandidato.FieldByName( 'SO_FEC_NAC' ).AsDateTime;
          FieldByName( 'CB_SEXO' ).AsString := cdsInfoCandidato.FieldByName( 'SO_SEXO' ).AsString;
          FieldByName( 'CB_EDO_CIV' ).AsString := cdsInfoCandidato.FieldByName( 'SO_EDO_CIV' ).AsString;
          FieldByName( 'CB_TEL' ).AsString := Copy( cdsInfoCandidato.FieldByName( 'SO_TEL' ).AsString, 1, K_ANCHO_DESCRIPCION );
          FieldByName( 'CB_CANDIDA' ).AsInteger := cdsInfoCandidato.FieldByName( 'SO_FOLIO' ).AsInteger;
          FieldByName( 'CB_CURP' ).AsString := cdsInfoCandidato.FieldByName( 'SO_CURP' ).AsString;
          ZetaDataSetTools.RevisaCambioCampo( cdsInfoCandidato.FieldByName( 'SO_RFC' ), FieldByName( 'CB_RFC' ) );   // Verifica que traiga algo el RFC si no se deja lo que calcula tress
          FieldByName( 'CB_CALLE' ).AsString := cdsInfoCandidato.FieldByName( 'SO_CALLE' ).AsString;
          FieldByName( 'CB_COLONIA' ).AsString := cdsInfoCandidato.FieldByName( 'SO_COLONIA' ).AsString;
          FieldByName( 'CB_CODPOST' ).AsString := cdsInfoCandidato.FieldByName( 'SO_CODPOST' ).AsString;
          FieldByName( 'CB_CIUDAD' ).AsString := cdsInfoCandidato.FieldByName( 'SO_CIUDAD' ).AsString;
          FieldByName( 'CB_ESTADO' ).AsString := cdsInfoCandidato.FieldByName( 'SO_ESTADO' ).AsString;
          FieldByName( 'CB_FEC_RES' ).AsDateTime := cdsInfoCandidato.FieldByName( 'SO_FEC_RES' ).AsDateTime;
          FieldByName( 'CB_LUG_NAC' ).AsString := cdsInfoCandidato.FieldByName( 'SO_PAIS' ).AsString;
          FieldByName( 'CB_NUM_EXT' ).AsString := cdsInfoCandidato.FieldByName( 'SO_NUM_EXT' ).AsString;
          FieldByName( 'CB_NUM_INT' ).AsString := cdsInfoCandidato.FieldByName( 'SO_NUM_INT' ).AsString;

          FieldByName( 'CB_ESTUDIO' ).AsString := dmTablas.GetCodigoEstudio( eEstudios( cdsInfoCandidato.FieldByName( 'SO_ESTUDIO' ).AsInteger ) );
          // Habla otros idiomas
          iIngles := cdsInfoCandidato.FieldByName( 'SO_INGLES' ).AsInteger;
          iOtroIdioma := cdsInfoCandidato.FieldByName( 'SO_DOMINA2' ).AsInteger;
          lHabla := ( iIngles > 0 ) or ( iOtroIdioma > 0 );
          FieldByName( 'CB_HABLA' ).AsString := zBoolToStr( lHabla );
          if lHabla then
          begin
               sDescripIdioma := ZetaCommonClasses.VACIO;
               if ( iIngles > 0 ) then
                  sDescripIdioma := ConcatString( sDescripIdioma, Format( K_DESCRIP_IDIOMA,
                                                  [ 'Ingl�s', iIngles ] ), ',' );
               if ( iOtroIdioma > 0 ) then
                  sDescripIdioma := ConcatString( sDescripIdioma, Format( K_DESCRIP_IDIOMA,
                                                  [ cdsInfoCandidato.FieldByName( 'SO_IDIOMA2' ).AsString,
                                                  iOtroIdioma ] ), ',' );
               FieldByName( 'CB_IDIOMA' ).AsString := sDescripIdioma;
          end;
          TransfiereCampoAdicional(cdsInfoCandidato, dmCliente.cdsEmpleado);
     end;
end;

procedure TdmRecursos.TransfiereCampoAdicional( Fuente, Destino: TDataset);
const
     K_PREFIJO = 'CB_G_';
var
   i: Integer;
   sCampo: String;
begin
     with Fuente do
     begin
           for i := 0 to ( FieldCount - 1 ) do
           begin
               sCampo := Fields[ i ].FieldName;
               if ( Pos( K_PREFIJO, sCampo) > 0 ) then
               begin
                    Destino.FieldByName( sCampo ).Assign( Fields[ i ] );
               end;
          end;
     end;
end;

procedure TdmRecursos.AgregarFotoSeleccion( const iFolio: Integer );
var
   ErrorCount: Integer;
begin
     if ( iFolio > 0 ) then
     begin
          with dmCliente do
               ServerRecursos.GrabaFotoCandidato( Empresa, EmpresaSeleccion, Empleado,
                                                  iFolio, ErrorCount );
          if ( ErrorCount = 0 ) then
               TressShell.SetDataChange( [ enImagen ] )
          else
              ZetaDialogo.ZError( 'Error', 'No Se Pudo Transferir La Foto Del Candidato', 0 );
     end;
end;

{cdsCursosProg}
procedure TdmRecursos.cdsCursosProgAlAdquirirDatos(Sender: TObject);
begin
     ObtieneCursosIndividuales;
end;

procedure TdmRecursos.cdsCursosProgAlAgregar(Sender: TObject);
begin
     with cdsCursosProg do
     begin
          if( IsEmpty )then
          begin
               Conectar;
          end
          else
          begin
               EmptyDataSet;
               MergeChangeLog;
          end;
          Append;
     end;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCurProg_DevEx, TEditCurProg_DevEx )
end;

procedure TdmRecursos.cdsCursosProgAlModificar(Sender: TObject);
begin
     ObtieneCursosIndividuales;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCurProg_DevEx, TEditCurProg_DevEx )
end;

procedure TdmRecursos.ObtieneCursosIndividuales;
begin
     with cdsHisCursosProg do
     begin
          cdsCursosProg.Data := ServerRecursos.GetEmpProg( dmCliente.Empresa, dmCliente.Empleado, FieldByName('CU_CODIGO').AsString );
     end;
end;

procedure TdmRecursos.cdsCursosProgNewRecord(DataSet: TDataSet);
begin
     with cdsCursosProg do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('EP_DIAS').AsInteger := 0;          
          FieldByName('EP_OPCIONA').AsString := K_GLOBAL_NO;
          FieldByName('EP_FECHA').AsDateTime := dmCliente.FechaDefault;
          FieldByName('EP_GLOBAL').AsString := K_GLOBAL_NO;
          FieldByName('EP_PORDIAS').AsString := K_GLOBAL_SI;          
     end;
end;

procedure TdmRecursos.cdsCursosProgBeforePost(DataSet: TDataSet);
begin
     with cdsCursosProg do
     begin
          with dmCliente do
          begin
               if ( GetStatus( GetDatosEmpleadoActivo.Numero,FieldByName('EP_FECHA').AsDateTime  ) = steBaja )then
               begin
                    DatabaseError('Empleado dado de baja: ' + DateToStr( GetDatosEmpleadoActivo.Baja ))
               end;
          end;
          if( strVacio( FieldByName('CU_CODIGO').AsString ) )then
              DatabaseError('El C�digo Del Curso No Puede Quedar Vac�o');
     end;
end;

procedure TdmRecursos.cdsCursosProgAlBorrar(Sender: TObject);
begin
     with cdsHisCursosProg do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
          begin
               ServerRecursos.BorraCurIndivid( dmCliente.Empresa, dmCliente.Empleado, FieldByName('CU_CODIGO').AsString );
               cdsHisCursosProg.Refrescar;
               cdsCursosProg.Refrescar;
          end;
     end;
end;

procedure TdmRecursos.cdsCursosProgAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iEmpleado : Integer;
   sCurso: TCodigo;
begin
     ErrorCount := 0;
     with cdsCursosProg do
     begin
          if( State in [ dsEdit, dsInsert ] ) then
              Post;
          if ChangeCount > 0 then
          begin
               sCurso := cdsHisCursosProg.FieldByName('CU_CODIGO').AsString;
               iEmpleado := cdsHisCursosProg.FieldByName('CB_CODIGO').AsInteger;
               Reconcile ( ServerRecursos.GrabaEmpProg( dmCliente.Empresa, Delta, ErrorCount ) );
               cdsHisCursosProg.Refrescar;
               cdsHisCursosProg.Locate( 'CB_CODIGO;CU_CODIGO;CP_MANUAL', Vararrayof([ iEmpleado, sCurso, K_GLOBAL_SI ]), [] );
          end;
     end;
end;



{ ******************************* cdsInscritos **************************** }
procedure TdmRecursos.EditarInscripciones;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( Inscritos_DevEx, TInscritos_DevEx, NULL )
end;

procedure TdmRecursos.EditarAprobados;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( Aprobados_DevEx, TAprobados_DevEx, NULL );
end;

procedure TdmRecursos.EditarListaAsistencia;
begin
     ObtieneReservasSesion( cdsSesion.FieldByName( 'SE_FOLIO' ).AsInteger );
     if( cdsReserva.RecordCount > 0 )then
         ZBaseGridEdicion_DevEx.ShowGridEdicion( ListaAsistencia_DevEx, TListaAsistencia_DevEx, NULL )
     else
         ZetaDialogo.ZInformation( 'Informaci�n', 'Es necesario tener por lo menos una reservaci�n '+ CR_LF +
                                                  'para el grupo seleccionado', 0 );
end;

procedure TdmRecursos.EditarEvaluaciones;
begin
     cdsInscritos.Refrescar;
     if( cdsInscritos.IsEmpty )then
         ZetaDialogo.ZInformation( 'Informaci�n', 'Es necesario tener empleados inscritos', 0 )
     else
         ZBaseGridEdicion_DevEx.ShowGridEdicion( Evaluaciones_DevEx, TEvaluaciones_DevEx, NULL );
end;

procedure TdmRecursos.cdsInscritosAlAdquirirDatos(Sender: TObject);
begin
     cdsInscritos.Data := ServerRecursos.GetInscritos( dmCliente.Empresa, cdsSesion.FieldByName('SE_FOLIO').AsInteger );
end;

procedure TdmRecursos.cdsInscritosAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsInscritos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               if( Reconcile ( ServerRecursos.GrabaInscritos( dmCliente.Empresa, Delta, ErrorCount ) ) )then
               begin
                    cdsSesion.Edit;
                    cdsSesion.FieldByName('SE_INSCRITO').AsInteger := RecordCount;
                    cdsSesion.Post;
                    cdsSesion.MergeChangeLog;
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsInscritosAlModificar(Sender: TObject);
begin
     if( cdsInscritos.IsEmpty )then
        ZetaDialogo.ZInformation( 'Informaci�n', 'No hay datos para modificar', 0 )
     else
         ZBaseEdicion_DevEx.ShowFormaEdicion( EditInscrito_DevEx, TEditInscrito_DevEx );
end;

procedure TdmRecursos.cdsInscritosAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsInscritos do
     begin
          ListaFija( 'IC_STATUS', lfStatusInscrito );
          MaskFecha( 'IC_FEC_BAJ' ) ;
          MaskFecha( 'IC_FEC_INS' );
          MaskPesos( 'IC_EVA_1'  );
          MaskPesos( 'IC_EVA_2'  );
          MaskPesos( 'IC_EVA_3' );
          MaskPesos( 'IC_EVA_FIN'  );
          MaskHoras( 'IC_HOR_INS' );
          FieldByName( 'CB_CODIGO').OnValidate := CB_CODIGOOnValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsInscritosCB_CODIGOChange;
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmRecursos.CB_CODIGOOnValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No Existe el Empleado #' + IntToStr( AsInteger ) );
          end
          else
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
          ObtieneCursoTomadoPorEmpleado( AsInteger );
          VerificaPrerequisitos;
     end;
end;

procedure TdmRecursos.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;

procedure TdmRecursos.cdsInscritosNewRecord(DataSet: TDataSet);
begin
     with cdsInscritos do
     begin
          FieldByName( 'SE_FOLIO' ).AsInteger := cdsSesion.FieldByName('SE_FOLIO').AsInteger;
          FieldByName( 'IC_STATUS' ).AsInteger := Ord( siActivo );
          FieldByName( 'IC_FEC_INS' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'IC_HOR_INS' ).AsString := HoraAsStr( Time );
     end;
end;

procedure TdmRecursos.cdsInscritosBeforePost(DataSet: TDataSet);
begin
     with cdsInscritos do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          //ObtieneCursoTomadoPorEmpleado( FieldByName('CB_CODIGO').AsInteger );
          //VerificaPrerequisitos;
     end;
end;

procedure TdmRecursos.cdsInscritosCB_CODIGOChange(Sender: TField);
begin
     with cdsInscritos do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
     end;
end;

function TdmRecursos.VerificaPrerequisitos: Boolean;
const
     K_OPCIONALES = 'CP_OPCIONA = %s ';
var
   sCurso: String;
begin
     Result := True;
     dmCatalogos.PrerequisitosCurso := cdsSesion.FieldByName('CU_CODIGO').AsString;
     with dmCatalogos.cdsPrerequisitosCurso do
     begin
          Refrescar;
          Filter := Format( K_OPCIONALES, [ EntreComillas( K_GLOBAL_NO ) ] );
          Filtered := True;
          if( RecordCount > 0 )then
          begin
               sCurso := VACIO;
               while not EOF do
               begin
                    if not( cdsCursosEmpleado.Locate( 'CU_CODIGO', FieldByName( 'CP_CURSO' ).AsString, [] ) )then
                    begin
                         sCurso := sCurso + FieldByName( 'CP_CURSO' ).AsString + '= ' +
                                            FieldByName( 'CU_NOMBRE' ).AsString + CR_LF;
                    end;
                    Next;
               end;
               if( strLleno( sCurso ) )then
               begin
                    ZetaDialogo.ZInformation( 'Informaci�n', 'Es necesario tomar primero los cursos requeridos: ' + CR_LF + CR_LF + sCurso, 0 );
                    Result := False;
                    Filtered := False;
                    Abort;
               end;
          end;
          if Result then
          begin
               Filtered := False;
               Filter := Format( K_OPCIONALES, [ EntreComillas( K_GLOBAL_SI ) ] );
               Filtered := True;
               if( RecordCount > 0 )then
               begin
                    sCurso := VACIO;
                    while not EOF do
                    begin
                         if not( cdsCursosEmpleado.Locate( 'CU_CODIGO', FieldByName('CP_CURSO').AsString, [] ) )then
                         begin
                              sCurso := sCurso + FieldByName( 'CP_CURSO' ).AsString + ': ' +
                                                 FieldByName( 'CU_NOMBRE' ).AsString + CR_LF;
                         end;
                         Next;
                    end;
                    if( strLleno( sCurso ) )then
                    begin
                         if not ZetaDialogo.ZConfirm( 'Confirmaci�n', 'Se recomienda haber tomado los siguientes cursos: ' + CR_LF + sCurso +
                                                      CR_LF + CR_LF + '� Desea inscribir al empleado ?', 0, mbYes ) then
                         begin
                              Filtered := False;
                              Abort;
                         end;
                    end;
               end;
          end;
          Filtered := False;
     end;
end;

procedure TdmRecursos.ObtieneCursoTomadoPorEmpleado( const iEmpleado: Integer );
begin
     cdsCursosEmpleado.Data :=  ServerRecursos.GetHisCursos( dmCliente.Empresa, iEmpleado );
end;

procedure TdmRecursos.cdsInscritosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
          var Action: TReconcileAction);
begin
     ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
     Action := raCancel;
end;

{********************* cdsAprobados ***************************}
procedure TdmRecursos.cdsAprobadosAlAdquirirDatos( Sender: TObject );
 var
     lTraerInscritos,lAsistencia,lFiltrado : Boolean;
     Parameters:TZetaParams;
     oAsistencia : TZetaClientDataSet;
     sFiltroEvaluaciones,sFiltroAnterior:string;

 procedure AgregarAprobado ;
 begin
      with cdsAprobados do
      begin
          Append;
          FieldByName('SE_FOLIO').AsInteger := cdsInscritos.FieldByName('SE_FOLIO').AsInteger;
          FieldByName('CB_CODIGO').AsInteger := cdsInscritos.FieldByName('CB_CODIGO').AsInteger;
          FieldByName('KC_EVALUA').AsFloat := cdsInscritos.FieldByName('IC_EVA_FIN').AsFloat;
          Post;
      end;
 end;

begin
     lTraerInscritos := FALSE;
     sFiltroEvaluaciones := VACIO;
     lAsistencia := False;
     Parameters := TZetaParams.Create;
     sFiltroAnterior := VACIO;

     cdsAprobados.Data := ServerRecursos.GetAprobados( dmCliente.Empresa, cdsSesion.FieldByName('SE_FOLIO').AsInteger  );
     if cdsAprobados.IsEmpty then
     begin
         if( cdsSesion.FieldByName('SE_INSCRITO').AsInteger > 0  )then
          try
             with CurDlgAsis_DevEx do
             begin
                  CurDlgAsis_DevEx := TCurDlgAsis_DevEx.Create(Self);
                  Parametros := Parameters;
                  ShowModal;
                  lTraerInscritos := ( ModalResult = mrOk ) ;
             end;
             finally
                    CurDlgAsis_DevEx.Free;
          end;
     end;
     if lTraerInscritos then
     begin
          cdsInscritos.Refrescar;
          lFiltrado := cdsInscritos.Filtered;
          sFiltroAnterior := cdsInscritos.Filter;
          oAsistencia := TZetaClientDataSet.Create(Self);
          with Parameters do
          begin
               if ParamByName('ValidarAsis').AsBoolean then
               begin
                    lAsistencia := True;
                    oAsistencia.Data := ServerRecursos.GetAsistenciaCurso( dmCliente.Empresa,
                                                                           ParamByName('AsisSesiones').AsInteger,
                                                                           cdsSesion.FieldByName('SE_FOLIO').AsInteger);
               end;
               if ParamByName('ValidarEval').AsBoolean then
               begin
                    if ParamByName('Eval1').AsFloat <> 0 then
                       sFiltroEvaluaciones := ConcatFiltros (sFiltroEvaluaciones,'IC_EVA_1 >= '+ FloatToStr(ParamByName('Eval1').AsFloat) );
                    if ParamByName('Eval2').AsFloat <> 0 then
                       sFiltroEvaluaciones := ConcatFiltros (sFiltroEvaluaciones,'IC_EVA_2 >= '+ FloatToStr(ParamByName('Eval2').AsFloat) );
                    if ParamByName('Eval3').AsFloat <> 0 then
                       sFiltroEvaluaciones := ConcatFiltros (sFiltroEvaluaciones,'IC_EVA_3 >= '+ FloatToStr(ParamByName('Eval3').AsFloat) );
                    if ParamByName('EvalFin').AsFloat <> 0 then
                       sFiltroEvaluaciones := ConcatFiltros (sFiltroEvaluaciones,'IC_EVA_FIN >= '+ FloatToStr(ParamByName('EvalFin').AsFloat) );
                    cdsInscritos.Filter := concatFiltros(cdsInscritos.Filter, sFiltroEvaluaciones );
                    cdsInscritos.Filtered := True;
               end;
          end;
          cdsInscritos.First;
          while not cdsInscritos.EOF  do
          begin
               if ( lAsistencia )then
               begin
                    if ( oAsistencia.Locate('CB_CODIGO',cdsInscritos.FieldByName('CB_CODIGO').AsInteger, [])) then
                    begin
                         AgregarAprobado;
                         oAsistencia.Delete;
                         if (oAsistencia.RecordCount = 0 )then
                            cdsInscritos.Last;
                    end
               end
               else
               begin
                    AgregarAprobado
               end;
               cdsInscritos.Next;
          end;
          cdsInscritos.Filtered := lFiltrado;
          cdsInscritos.Filter := sFiltroAnterior;
          cdsInscritos.Close;
     end;
end;



procedure TdmRecursos.cdsAprobadosAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsAprobados do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               Reconcile ( ServerRecursos.GrabaGridCursos(dmCliente.Empresa, Delta, ErrorCount ) );

               if ( ErrorCount = 0 ) then
               begin
                    cdsSesion.Edit;
                    cdsSesion.FieldByName('SE_APROBADO').AsInteger := RecordCount;
                    cdsSesion.Post;
                    cdsSesion.MergeChangeLog;
               end;
               TressShell.SetDataChange( [ enKarCurso, enCurProg ] );
          end;
     end;
end;

procedure TdmRecursos.cdsAprobadosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditAprobado_DevEx, TEditAprobado_DevEx )
end;

procedure TdmRecursos.cdsAprobadosAlCrearCampos(Sender: TObject);
begin
     with cdsAprobados do
     begin
          MaskNumerico( 'SE_FOLIO', '0;-0;#' );
          MaskHoras( 'KC_HORAS' );
          MaskPesos( 'KC_EVALUA'  );
          FieldByName( 'KC_EVALUA' ).OnValidate := cdsGridCursosKC_EVALUAValidate; 
          FieldByName( 'CB_CODIGO').OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsAprobadosCB_CODIGOChange;
     end;
end;

procedure TdmRecursos.cdsAprobadosNewRecord(DataSet: TDataSet);
begin
     with cdsAprobados do
     begin
          FieldByName('CU_CODIGO').AsString  := cdsSesion.FieldByName('CU_CODIGO').AsString;
          FieldByName('SE_FOLIO').AsInteger  := cdsSesion.FieldByName('SE_FOLIO').AsInteger;
          FieldByName('MA_CODIGO').AsString  := cdsSesion.FieldByName('MA_CODIGO').AsString;
          FieldByName('KC_FEC_TOM').AsDateTime  := cdsSesion.FieldByName('SE_FEC_INI').AsDateTime;
          FieldByName('KC_FEC_FIN').AsDateTime  := cdsSesion.FieldByName('SE_FEC_FIN').AsDateTime;
          FieldByName('KC_REVISIO').AsString  := cdsSesion.FieldByName('SE_REVISIO').AsString;
          FieldByName('KC_HORAS').AsFloat  := cdsSesion.FieldByName('SE_HORAS').AsFloat;
          FieldByName('KC_EST').AsString := cdsSesion.FieldByName('SE_EST').AsString;
     end;
end;

procedure TdmRecursos.cdsAprobadosCB_CODIGOChange(Sender: TField);
begin
     with cdsAprobados do
     begin
         FieldByName( 'PRETTYNAME' ).AsString :=dmCliente.cdsEmpleadoLookup.GetDescription;

         with GetDatosClasificacion( FieldByName('CB_CODIGO').AsInteger, cdsSesion.FieldByName('SE_FEC_INI').AsDateTime ) do
         begin
               FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
               FieldByName( 'CB_TURNO' ).AsString := Turno;
               FieldByName( 'CB_PUESTO' ).AsString := Puesto;
               FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
               FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
               FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
               FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
               FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
               FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
               FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
               FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
               FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
               {$ifdef ACS}
               FieldByName( 'CB_NIVEL10' ).AsString := Nivel10;
               FieldByName( 'CB_NIVEL11' ).AsString := Nivel11;
               FieldByName( 'CB_NIVEL12' ).AsString := Nivel12;
               {$endif}

         end;
     end;
end;

{cdsSesion}
procedure TdmRecursos.ObtieneSesiones;
const
     K_SIN_USUARIO = 0;
begin
     with cdsSesion do
     begin
          //Filtros para integrar cursos en supervisores
          with FParametrosSesion do
          begin
               AddBoolean( 'FiltraCurso', FALSE );
               AddInteger('Usuario', K_SIN_USUARIO );
          end;

          Data := ServerRecursos.GetSesiones( dmCliente.Empresa, FParametrosSesion.VarValues );
     end;
end;



procedure TdmRecursos.cdsSesionNewRecord(DataSet: TDataSet);
begin
     with cdsSesion do
     begin
          FieldByName('SE_STATUS').AsInteger := Ord( ssAbierta );
          FieldByName( 'SE_FEC_INI' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'SE_FEC_FIN' ).AsDateTime := dmCliente.Fechadefault;
          FieldByName( 'SE_HOR_INI' ).AsString := FormatDateTime( 'hhnn', Now );
          FieldByName( 'SE_HOR_FIN' ).AsString := FormatDateTime( 'hhnn', Now );
          FieldByName('SE_EST').AsString := VACIO;
     end;
end;

procedure TdmRecursos.cdsSesionAlCrearCampos(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsMaestros.Conectar;
          {$ifdef ICUMEDICAL_CURSOS}
          cdsSesiones.Conectar;
          {$endif}
     end;
     with cdsSesion do
     begin
          CreateSimpleLookup( dmCatalogos.cdsCursos, 'CU_NOMBRE', 'CU_CODIGO' );
          CreateSimpleLookup( dmCatalogos.cdsMaestros, 'MA_NOMBRE', 'MA_CODIGO' );
          CreateCalculated( 'SE_COSTOT', ftFloat, 0 );
          MaskFecha('SE_FEC_INI');
          MaskFecha('SE_FEC_FIN');
          MaskTime( 'SE_HOR_INI' );
          MaskTime( 'SE_HOR_FIN' );
          MaskPesos( 'SE_COSTOT' );
          ListaFija( 'SE_STATUS', lfStatusSesiones );
          FieldByName('CU_CODIGO').OnChange := SESIONCU_CODIGOChange;
     end;
end;

procedure TdmRecursos.cdsSesionCalcFields(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'SE_COSTOT' ).AsFloat := FieldByName('SE_COSTO1' ).AsFloat +
                                                FieldByName( 'SE_COSTO2' ).AsFloat +
                                                FieldByName( 'SE_COSTO3' ).AsFloat;
     end;
end;

procedure TdmRecursos.SESIONCU_CODIGOChange(Sender: TField);
begin
     with cdsSesion do
     begin
          if FSesionCurso <> Sender.NewValue then
          begin
               if ( FieldByName( 'CU_CODIGO' ).AsString <> dmCatalogos.cdsCursos.FieldByName('CU_CODIGO').AsString ) then
                    dmCatalogos.cdsCursos.Locate( 'CU_CODIGO', FieldByName( 'CU_CODIGO' ).AsString, [] );
               if( strLleno( FieldByName('CU_CODIGO').AsString ) )then
               begin
                    FieldByName( 'SE_HORAS' ).AsFloat := dmCatalogos.cdsCursos.FieldByName( 'CU_HORAS' ).AsFloat;
                    FieldByName( 'MA_CODIGO' ).AsString := dmCatalogos.cdsCursos.FieldByName('MA_CODIGO').AsString;
                    FieldByName( 'SE_REVISIO' ).AsString := dmCatalogos.cdsCursos.FieldByName( 'CU_REVISIO' ).AsString;
                    FieldByName( 'SE_COSTO1' ).AsFloat := dmCatalogos.cdsCursos.FieldByName( 'CU_COSTO1' ).AsFloat;
                    FieldByName( 'SE_COSTO2' ).AsFloat := dmCatalogos.cdsCursos.FieldByName( 'CU_COSTO2' ).AsFloat;
                    FieldByName( 'SE_COSTO3' ).AsFloat := dmCatalogos.cdsCursos.FieldByName( 'CU_COSTO3' ).AsFloat;
               end;
          end;
          FSesionCurso := Sender.NewValue;
     end;
end;


procedure TdmRecursos.cdsSesionBeforePost(DataSet: TDataSet);
begin
     with cdsSesion do
     begin
          FieldByName('US_CODIGO').AsInteger:= dmCliente.Usuario;
          if( FieldByName('SE_FEC_FIN').AsDateTime < FieldByName('SE_FEC_INI').AsDateTime )then
          begin
               FieldByName('SE_FEC_FIN').FocusControl;
               DataBaseError( 'La fecha de inicio debe ser menor o igual a la fecha de terminaci�n' );
          end;
          if( strVacio( FieldByName('CU_CODIGO').AsString ) )then
              DatabaseError( ' El c�digo del curso no puede quedar vac�o ');
			
		  {$ifdef ICUMEDICAL_CURSOS}
          if ( State in [dsEdit] ) then
          begin
               FieldByName( 'SE_D_NOM' ).AsString := ExtractFileName( FieldByName( 'SE_D_NOM' ).AsString );
          end;
		  {$endif}
     end;

end;

procedure TdmRecursos.cdsSesionAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iFolio: Integer;
   oSesion: OleVariant;
begin
     ErrorCount := 0;
     with cdsSesion do
     begin
          PostData;
          if (  ChangeCount > 0 ) then
          begin
               ErrorCount := ChangeCount;
               DisableControls;
               try
                  oSesion := ServerRecursos.GrabaSesiones( dmCliente.Empresa, DeltaNull, iFolio, ErrorCount );
                  if (  ( ChangeCount = 0 ) or Reconcile( oSesion )  )then
                  begin
                       if( iFolio > 0)then
                       begin
                            Edit;
                            FieldByName('SE_FOLIO').AsInteger := iFolio;
                            Post;
                            MergeChangeLog;
                            FFolioSesion := iFolio;
                       end
                       else
                            FFolioSesion := FieldByName('SE_FOLIO').AsInteger;
                       TressShell.SetDataChange( [ enSesion ] );
                  end
               finally
                 EnableControls;
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsSesionAfterDelete(DataSet: TDataSet);
begin
     cdsSesion.Enviar
end;

procedure TdmRecursos.cdsSesionAlModificar(Sender: TObject);
begin
     FFolioSesion := 0;
     try
        ZBaseEdicion_DevEx.ShowFormaEdicion( EditSesion_DevEx, TEditSesion_DevEx );
     finally
            if( FFolioSesion > 0 )then
            begin
                 ObtieneSesiones;
                 cdsSesion.Locate( 'SE_FOLIO', FFolioSesion, [] );
            end;
     end;
end;


{ ************************ cdsListaAsistencia ************************* }

procedure TdmRecursos.GetListaAsistencia( const iReserva : integer; const lTraerInscritos: Boolean );
var
   sMsg: WideString;
begin
     cdsListaAsistencia.Data := ServerRecursos.GetListaAsistencia( dmCliente.Empresa, cdsSesion.FieldByName('SE_FOLIO').AsInteger, iReserva, lTraerInscritos, sMsg );
     if( strLleno( sMsg ) )then
         ZetaDialogo.ZInformation( 'Informaci�n', sMsg, 0 );
end;

procedure TdmRecursos.cdsListaAsistenciaAlCrearCampos(Sender: TObject);
begin
     with cdsListaAsistencia do
     begin
          FieldByName( 'CB_CODIGO' ).OnChange := cdsListaAsistenciaCB_CODIGOChange;
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CS_ASISTIO' ).OnGetText := CS_ASISTIOOnGetText;
          MaskNumerico( 'SE_FOLIO', '0;-0;#' );
          MaskPesos( 'CS_EVA_1'  );
          MaskPesos( 'CS_EVA_2'  );
          MaskPesos( 'CS_EVA_3' );
          MaskPesos( 'CS_EVA_FIN'  );
     end;
end;

procedure TdmRecursos.CS_ASISTIOOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := VACIO;
end;


procedure TdmRecursos.cdsListaAsistenciaAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsListaAsistencia do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               Reconcile ( ServerRecursos.GrabaListaAsistencia( dmCliente.Empresa, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmRecursos.cdsListaAsistenciaCB_CODIGOChange(Sender: TField);
begin
     with cdsListaAsistencia do
     begin
          FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription;
     end;
end;

procedure TdmRecursos.LlenaReservaCombo( oLista : TStrings );
begin
     with oLista do
     begin
          BeginUpdate;
          try
             Clear;
             with cdsReservasSesion do
             begin
                  Refrescar;
                  while NOT EOF do
                  begin
                       if( FieldByName('RV_FEC_INI').AsDateTime = FieldByName('RV_FEC_FIN').AsDateTime )then
                           oLista.AddObject( Format( '%s = %s  %s, de %s a %s', [ FieldByName('RV_ORDEN').AsString,
                                                                                 FieldByName('AL_NOMBRE').AsString,
                                                                                 FechaCorta( FieldByName('RV_FEC_INI').AsDateTime ),
                                                                                 MaskHora( FieldByName('RV_HOR_INI').AsString ),
                                                                                 MaskHora( FieldByName('RV_HOR_FIN').AsString ) ] ),
                                                                                 TObject( FieldByName('RV_FOLIO').AsInteger ) )
                       else
                           oLista.AddObject( Format( '%s = %s  del %s al %s, de %s a %s', [ FieldByName('RV_ORDEN').AsString,
                                                                                           FieldByName('AL_NOMBRE').AsString,
                                                                                           FechaCorta( FieldByName('RV_FEC_INI').AsDateTime ),
                                                                                           FechaCorta( FieldByName('RV_FEC_FIN').AsDateTime ),
                                                                                           MaskHora( FieldByName('RV_HOR_INI').AsString ),
                                                                                           MaskHora( FieldByName('RV_HOR_FIN').AsString ) ] ),
                                                                                           TObject( FieldByName('RV_FOLIO').AsInteger ) );
                       Next;
                  end;
             end;
             cdsReservasSesion.Close;
          finally
                 EndUpdate;
          end;

     end;
end;

{ ********************** cdsReservasSesion ******************************* }
procedure TdmRecursos.cdsReservasSesionAlAdquirirDatos( Sender: TObject );
begin
     cdsReservasSesion.Data :=  ServerRecursos.GetReservasSesion( dmCliente.Empresa, cdsSesion.FieldByName('SE_FOLIO').AsInteger );
end;

procedure TdmRecursos.cdsReservasSesionAlCrearCampos(Sender: TObject);
begin
     dmCatalogos.cdsAulas.Conectar;
     with cdsReservasSesion do
     begin
          CreateSimpleLookup( dmCatalogos.cdsAulas, 'AL_NOMBRE', 'AL_CODIGO' );
     end;
end;

{ ************************ cdsReserva ************************************ }
procedure TdmRecursos.ObtieneReservaciones;
begin
     with cdsReserva do
     begin
          Data := ServerRecursos.GetReservas( dmCliente.Empresa, ParametrosReservacion.VarValues );
          ResetDataChange;
     end;
end;

procedure TdmRecursos.cdsReservaAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     dmCatalogos.cdsMaestros.Conectar;
     with cdsReserva do
     begin
          MaskFecha('RV_FEC_INI');
          MaskTime( 'RV_HOR_INI' );
          MaskTime( 'RV_HOR_FIN' );
          MaskTime( 'RV_HOR_RES' );
          ListaFija( 'RV_TIPO', lfTipoReserva );
          FieldByName('CONFLICTO').OnGetText := CONFLICTOGetText;
          FieldByName('ASISTENCIA').OnGetText := ASISTENCIAGetText;
          FieldByName('ASISTENCIA').Alignment := taLeftJustify;
          FieldByName('RV_FEC_INI').OnChange := RV_FEC_INIOnChange;
          CreateCalculated('RV_DIA', ftString, 10 );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmCatalogos.cdsMaestros, 'MA_NOMBRE', 'MA_CODIGO' );
     end;
end;

procedure TdmRecursos.RV_FEC_INIOnChange(Sender : TField );
begin
      with Sender.DataSet do
          FieldByName( 'RV_FEC_FIN').AsDateTime := Sender.AsDateTime;
end;

procedure TdmRecursos.CONFLICTOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := '';
end;

procedure TdmRecursos.ASISTENCIAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.Dataset.IsEmpty then
        Text := VACIO
     else
     begin
          Text := K_PROPPERCASE_NO;
          if( Sender.AsInteger > 0 )then
              Text := K_PROPPERCASE_SI;
     end;
end;

procedure TdmRecursos.cdsReservaCalcFields(DataSet: TDataSet);
begin
     with cdsReserva do
     begin
          FieldByName('RV_DIA').AsString := ZetaCommonTools.DiaSemana( FieldByName( 'RV_FEC_INI' ).AsDateTime );
     end;
end;

procedure TdmRecursos.cdsReservaNewRecord(DataSet: TDataSet);
begin
     with cdsReserva do
     begin
          FieldbyName('RV_FEC_INI').AsDateTime := dmCliente.FechaDefault;
          FieldbyName('RV_FEC_FIN').AsDateTime := dmCliente.FechaDefault;
          FieldByName('RV_HOR_INI').AsString := FormatDateTime( 'hhnn', Now );
          FieldByName('RV_HOR_FIN').AsString := FormatDateTime( 'hhnn', Now );
          FieldByName('RV_TIPO').AsInteger := Ord( rvBloqueo );
          FieldByName('RV_LISTA').AsString := K_GLOBAL_NO;
          FieldbyName('RV_RESUMEN').AsString := 'Bloqueo de aula';
          if ( ParametrosReservacion.FindParam( 'Aula' ) <> nil ) then
             FieldbyName('AL_CODIGO').AsString := ParametrosReservacion.ParamByName('Aula').AsString;
     end;
end;

procedure TdmRecursos.cdsReservaBeforePost(DataSet: TDataSet);
begin
     with cdsReserva do
     begin
          if( FieldByName('RV_FEC_FIN').AsDateTime < FieldByName('RV_FEC_INI').AsDateTime )then
          begin
               FieldByName('RV_FEC_FIN').FocusControl;
               DataBaseError( 'La fecha de inicio debe ser menor o igual a la fecha de terminaci�n' );
          end;
          if( strVacio( FieldByName('RV_ORDEN').AsString ) )then
              DataBaseError( 'La referencia de la reservaci�n no puede quedar vac�a' );
          if( strVacio( FieldByName('AL_CODIGO').AsString ) )then
              DataBaseError( 'El aula de la reservaci�n no puede quedar vac�a' );
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName('RV_FEC_RES').AsDateTime := dmCliente.FechaDefault;
          FieldByName('RV_HOR_RES').AsString := FormatDateTime( 'hhnn', Now );
          if( strVacio( FieldByName('RV_DETALLE').AsString ) )then
              FieldByName('RV_DETALLE').AsString := ' ';
     end;
end;

procedure TdmRecursos.cdsReservaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iFolioReserva: Integer;
   oReserva: OleVariant;
begin
     ErrorCount := 0;
     with cdsReserva do
     begin
          PostData;
          if (  ChangeCount > 0 ) then
          begin
               ErrorCount := ChangeCount;
               DisableControls;
               try
                  oReserva:= ServerRecursos.GrabaReservas( dmCliente.Empresa, DeltaNull, iFolioReserva, ErrorCount );
                  if (  ( ChangeCount = 0 ) or Reconcile(oReserva)  )then
                  begin
                       if( iFolioReserva = 0 )then
                           iFolioReserva := FieldByName('RV_FOLIO').AsInteger;
                       ObtieneReservaciones;
                       cdsReserva.Locate( 'RV_FOLIO', iFolioReserva, [] );
                  end
               finally
                 EnableControls;
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsReservaAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditReservaAula_DevEx, TEditReservaAula_DevEx );
end;

procedure TdmRecursos.cdsReservaAlBorrar(Sender: TObject);
begin
     with cdsReserva do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar este registro ?' ) then
          begin
               Delete;
               Enviar
          end;
     end;
end;

procedure TdmRecursos.cdsReservaBeforeDelete(DataSet: TDataSet);
begin
     if( cdsReserva.FieldByName('ASISTENCIA').AsInteger > 0 )then
     begin
          if not( ZetaDialogo.ZConfirm( 'Confirmaci�n', ' La reservaci�n seleccionada cuenta con lista de asistencia ' +CR_LF +
                                                '� Desea borrar la reservaci�n ?', 0, mbNo ) )then
                  Abort;
     end;
end;

{ ******************* Reservas de Sesi�n ***********************}
procedure TdmRecursos.ObtieneReservasSesion( const iSesion: Integer );
begin
     with ParametrosReservacion do
     begin
          AddInteger( 'FolioSesion', iSesion );
     end;
     ObtieneReservaciones;
end;

procedure TdmRecursos.EditarGridReservacionesSesion;
begin
     ObtieneReservasSesion( cdsSesion.FieldByName( 'SE_FOLIO' ).AsInteger );
     FFolioSesion := 0;
     try
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridReservacionesSesion_DevEx, TGridReservacionesSesion_DevEx, FALSE );
     finally
            if( FFolioSesion > 0 )then
            begin
                 ObtieneSesiones;
                 cdsSesion.Locate( 'SE_FOLIO', FFolioSesion, [] );
            end;
     end;
     cdsReserva.SetDataChange;
end;

{ ******************* Kardex de Certificaciones ***********************}
procedure TdmRecursos.cdsKarCertificacionesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsKarCertificaciones.Data := ServerRecursos.GetKarCertificaciones( Empresa , Empleado );
     end;
end;

procedure TdmRecursos.cdsKarCertificacionesAlCrearCampos(Sender: TObject);
begin
     with cdsKarCertificaciones do
     begin
          CreateSimpleLookup( dmCatalogos.cdsCertificaciones, 'CI_NOMBRE', 'CI_CODIGO' );
          CreateCalculated( 'VENCIMIENTO', ftstring,15 );
          FieldByName( 'CI_CODIGO' ).OnChange := OnCI_CODIGOChange;
          MaskFecha('KI_FEC_CER'); //DevEx ( by am ): Mascara agregada para desplegar la fecha correctamente en el grid nuevo.
     end;
end;

procedure TdmRecursos.OnCI_CODIGOChange(Sender: TField);
begin
     with dmCatalogos.cdsCertificaciones do
     begin
          if Active and Locate( 'CI_CODIGO', Sender.AsString, [] ) then
             cdsKarCertificaciones.FieldByName('KI_RENOVAR').AsInteger := FieldByName('CI_RENOVAR').AsInteger;
     end;
end;

procedure TdmRecursos.cdsKarCertificacionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsKarCertificaciones do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile ( ServerRecursos.GrabaHistorial( dmCliente.Empresa,Tag, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enKarCert ] );
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsKarCertificacionesAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditKardexCertificaciones_DevEx, TEditKardexCertificaciones_DevEx );
end;

procedure TdmRecursos.cdsKarCertificacionesNewRecord(DataSet: TDataSet);
begin
     with cdsKarCertificaciones do
     begin
          FieldByName('CB_CODIGO').AsInteger    := dmCliente.Empleado;
          FieldByName('KI_FEC_CER').AsDateTime  := dmCliente.FechaDefault;
          FieldByName('KI_APROBO').AsString     := K_GLOBAL_SI;
          FieldByName( 'US_CODIGO' ).AsInteger  := dmCliente.Usuario;
     end;

end;

procedure TdmRecursos.cdsKarCertificacionesCalcFields(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if FieldByName( 'KI_RENOVAR' ).AsInteger = 0 then
          begin
               FieldByName( 'VENCIMIENTO' ).AsString := VACIO
          end
          else
          begin
               FieldByName( 'VENCIMIENTO' ).AsString := FechaCorta( FieldByName( 'KI_FEC_CER' ).AsDateTime +
                                                                FieldByName( 'KI_RENOVAR' ).AsInteger);
          end;
     end;
end;

procedure TdmRecursos.cdsKarCertificacionesBeforePost(DataSet: TDataSet);
begin
      with DataSet do
      begin
           if StrVacio( FieldByName('CI_CODIGO').AsString ) then
           begin
                DataBaseError( 'El c�digo de certificaci�n no puede quedar vac�o' )
           end;
           if FieldByName('KI_RENOVAR').AsInteger < 0 then
           begin
                DataBaseError('La renovaci�n de la certificaci�n debe ser Mayor o igual a Cero(0)');
           end;
           if State in [ dsEdit ] then
           begin
                if StrVacio( FieldByName( 'KI_OBSERVA' ).AsString )then
                   FieldByName( 'KI_OBSERVA' ).AsString := K_ESPACIO;
           end;
      end;
end;

procedure TdmRecursos.AgregaCertificacion;
begin
     dmCatalogos.cdsCertificaciones.Conectar;
     with cdsKarCertificaciones do
     begin
          Conectar;
          Append;
          Modificar;
     end;
end;

procedure TdmRecursos.cdsKarCertificacionesAfterDelete(DataSet: TDataSet);
begin
     cdsKarCertificaciones.Enviar;
end;

//********************** PLAN DE VACACIONES *********************************///

procedure TdmRecursos.RefrescarPlanVacacion(Parametros : TZetaParams);
begin
     cdsPlanVacacion.Data := ServerRecursos.GetPlanVacacion( dmCliente.Empresa, Parametros.VarValues );
     cdsPlanVacacion.ResetDataChange;
end;

procedure TdmRecursos.cdsPlanVacacionAfterDelete(DataSet: TDataSet);
begin
     cdsPlanVacacion.Enviar;
end;

procedure TdmRecursos.cdsPlanVacacionAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsPlanVacacion do
     begin
          ListaFija('VP_STATUS', lfStatusPlanVacacion );
          MaskFecha('VP_FEC_INI');
          MaskFecha('VP_FEC_FIN');
          MaskHoras('VP_DIAS');
          FieldByName( 'VP_FEC_INI' ).OnChange := cdsPlanVacacionVP_FEC_INIChange;
          FieldByName( 'VP_FEC_FIN' ).OnChange := cdsPlanVacacionVP_FEC_FINChange;
          FieldByName( 'VP_STATUS' ).OnChange := cdsPlanVacacionVP_STATUSChange;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsPlanVacacionCB_CODIGOChange;
          FieldByName( 'VP_PAGO_US' ).OnChange := cdsPlanVacacionVP_PAGO_USChange;
          FieldByName( 'VP_NOMTIPO' ).OnChange := validacionTipoNominaChange;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'USR_AUT_DES', 'VP_AUT_USR' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'USR_SOL_DES', 'VP_SOL_USR' );
     end;
end;

procedure TdmRecursos.cdsPlanVacacionAlAgregar(Sender: TObject);
begin
     cdsPlanVacacion.Append;
     ZbaseEdicion_DevEx.ShowFormaEdicion( RegPlanVacacion_DevEx, TRegPlanVacacion_DevEx );
end;

procedure TdmRecursos.cdsPlanVacacionNewRecord(DataSet: TDataSet);
begin
     with cdsPlanVacacion do
     begin
          FieldByName( 'VP_STATUS' ).AsInteger :=  Ord(ZetaCommonlists.spvPendiente);
          FieldByName( 'VP_FEC_INI' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'VP_FEC_FIN' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'VP_SOL_USR').AsInteger := dmCliente.Usuario;
          FieldByName( 'VP_SOL_FEC').AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'VP_PAGO_US').AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmRecursos.cdsPlanVacacionCB_CODIGOChange(Sender: TField);
begin
     cdsPlanVacacionRecalculaDias;
     RecalculaDiasSaldos;
     with Sender.DataSet do
     begin
          FieldByName( 'PrettyName' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescripcion( FieldByName( 'CB_CODIGO' ).AsString );
     end;

end;

procedure TdmRecursos.cdsPlanVacacionVP_FEC_INIChange(Sender: TField);
begin
     cdsPlanVacacionRecalculaDias;
     RecalculaDiasSaldos;
end;

procedure TdmRecursos.cdsPlanVacacionVP_FEC_FINChange(Sender: TField);
begin
     cdsPlanVacacionRecalculaDias;
end;
procedure TdmRecursos.cdsPlanVacacionVP_STATUSChange(Sender: TField);
begin
     with cdsPlanVacacion do
     begin
          If ( eStatusPlanVacacion( FieldByName('VP_STATUS').AsInteger ) ) in [spvAutorizada , spvRechazada ]  then
          begin
               FieldByName( 'VP_AUT_USR').AsInteger := dmCliente.Usuario;
               FieldByName( 'VP_AUT_FEC').AsDateTime := dmCliente.FechaDefault;
          End
          Else
          begin
               FieldByName( 'VP_AUT_USR' ).AsInteger := 0;
               FieldByName ( 'VP_AUT_FEC' ).AsDateTime := ZetaCommonClasses.NullDateTime;
          End;
     end;
end;

{M�todo repetido en Super/DSuper.pas por la llamada al servidor ...}
procedure TdmRecursos.cdsPlanVacacionVP_PAGO_USChange(Sender: TField);
var
   iAnio,iNumPeriodo :Integer;
   PeriodoValido :Boolean;
begin
     PeriodoValido := True;
     with cdsPlanVacacion do
     begin
          if(  zStrToBool( FieldByName('VP_PAGO_US').AsString  )and
              ( FieldByName('VP_NOMYEAR').AsInteger = 0 ) and
              ( FieldByName('VP_NOMNUME').AsInteger = 0 ) )then
          begin
               //obtener el periodo a sugerir del servidor
               //si no existe ponemos el activo
               if( not ServerRecursos.GetPeriodoPlanVacacion( dmCliente.Empresa, dmCliente.cdsEmpleadoLookUp.FieldByName('CB_NOMINA').AsInteger,
                                                      FieldByName('VP_FEC_INI').AsDateTime,
                                                      iAnio,
                                                      iNumPeriodo ) )then
               begin
                    with dmCliente do
                    begin
                         iAnio  := YearDefault;
                         iNumPeriodo  := PeriodoNumero;
                    end;
               end;
               FieldByName('VP_NOMYEAR').AsInteger := iAnio;
               with dmCatalogos do
               begin
                    if ( ( not ZAccesosMgr.CheckDerecho( D_CONS_PLANVACACION, K_DERECHO_NIVEL0 ) ) )then
                    begin
                         if cdsPeriodo.Locate('PE_NUMERO',iNumPeriodo,[] )then
                            PeriodoValido := cdsPeriodo.FieldByName('PE_STATUS').AsInteger < Ord(spAfectadaParcial)
                         else
                             PeriodoValido := False;
                    end;
               end;
               if  PeriodoValido then
                  iNumPeriodo  := dmCliente.PeriodoNumero
               else
                   iNumPeriodo := 0;
               FieldByName('VP_NOMNUME').AsInteger := iNumPeriodo;
               FieldByName('VP_NOMTIPO').AsInteger := dmCliente.cdsEmpleadoLookUp.FieldByName('CB_NOMINA').AsInteger;
          end;
     end;
end;



Procedure TdmRecursos.cdsPlanVacacionRecalculaDias;
begin
     FToolsRH.RecalculaVacaDiasGozados( cdsPlanVacacion,eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ),dmAsistencia.GetVacaDiasGozar, 'VP_', 'VP_DIAS' );
end;

procedure TdmRecursos.RecalculaDiasSaldos;
begin
     with cdsPlanVacacion do
          FToolsRH.RecalculaDiasSaldoVacaciones(cdsPlanVacacion, ServerRecursos.GetSaldosVacacion( dmCliente.Empresa, FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'VP_FEC_INI' ).AsDateTime ));
end;

procedure TdmRecursos.cdsPlanVacacionBeforePost(DataSet: TDataSet);
begin
     FToolsRH.cdsPlanVacacionBeforePost( DataSet );
end;

procedure TdmRecursos.cdsPlanVacacionAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPlanVacacion do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerRecursos.GrabaPlanVacacion( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enPlanVacacion ] );
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsPlanVacacionAlModificar(Sender: TObject);
begin
     with dmCliente do
     begin
          SetTipoLookupEmpleado( eLookEmpNominas );
          cdsEmpleadoLookUp.Init;
          try
             ZbaseEdicion_DevEx.ShowFormaEdicion( EditPlanVacacion_DevEx, TEditPlanVacacion_DevEx );
          finally
             SetTipoLookupEmpleado( eLookEmpGeneral );
          end;
     end;
end;

procedure TdmRecursos.SetAutorizarPlanVacacion( const tipoAutorizacion: eStatusPlanVacacion; const lTodos: Boolean );
begin
     FToolsRH.SetStatusPlanVacacion( cdsPlanVacacion, tipoAutorizacion, lTodos );
     cdsPlanVacacion.Enviar;
end;

{$ifdef VER130}
procedure TdmRecursos.cdsPlanVacacionReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
procedure TdmRecursos.cdsPlanVacacionReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$endif}
begin
     ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
     Action := raCancel;
end;

function TdmRecursos.PuedeVerNeto: Boolean;
begin
     Result := ( Global.GetGlobalInteger( ZGlobalTress.K_GLOBAL_PIRAMIDA_CONCEPTO ) <> 0 ) and
               ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );
end;

procedure TdmRecursos.MostrarEmpleadosAInscribir (Parametros : TZetaParams);
begin
     cdsInscribirAlumnos.Data := ServerRecursos.MostrarEmpleadosAInscribir(dmCliente.Empresa, Parametros.VarValues);
end;

procedure TdmRecursos.AgregarAlumnos (const lTruncar : boolean);
begin
     with cdsInscribirAlumnos do
     begin
          First;
          while ( not EOF ) and ( ( not lTruncar ) or ( cdsInscritos.RecordCount < cdsSesion.FieldByName('SE_CUPO').AsInteger ) ) do
          begin
               if ( not cdsInscritos.Locate('CB_CODIGO', FieldByName('CB_CODIGO').AsInteger, [] ) ) then
               begin
                    cdsInscritos.Append;
                    cdsInscritos.FieldByName('CB_CODIGO').AsInteger := FieldByName('CB_CODIGO').AsInteger;
                    cdsInscritos.Post;
               end;
               Next;
          end;
     end;
end;

{********************cdsPlazas**************************************************}

procedure TdmRecursos.cdsPlazasAlAdquirirDatos(Sender: TObject);
begin
//     cdsPlazas.Data := ServerRecursos.GetPlazas( dmCliente.Empresa );
//     cdsPlazasLookUp.Data := cdsPlazas.Data;
end;

procedure TdmRecursos.ModificarPlaza( const FechaModificacion:TDateTime; const sObserva: String = VACIO );
var
   ErrorCount: Integer;
///   lInsert:Boolean;
begin
     ErrorCount := 0;
//   lInsert := False;
     with cdsPlazas do
     begin
//          if State in [ dsInsert ] then
//             lInsert := True;
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerRecursos.GrabaPlazas( dmCliente.Empresa, Delta, FechaModificacion, sObserva, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enPlazas, enEmpleado, enKardex, enNomina ] );
                    cdsEmpPlaza.Refrescar;
//                    if lInsert then
//                       Refrescar;
//                    else cdsPlazasLookUp.Data := cdsPlazas.Data;
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsPlazasAlEnviarDatos(Sender: TObject);
begin
     ModificarPlaza(NullDateTime);
end;

function TdmRecursos.GetMaxOrdenPlazas(const sPuesto:string):Integer;
begin
     Result := ServerRecursos.GetMaxOrdenPlazas( dmCliente.Empresa,sPuesto);
end;

procedure TdmRecursos.CrearListaPlazas;
begin
     with cdsListaEmpPlazas do
     begin
          if Active then
             EmptyDataSet
          else
          begin
               Init;
               AddIntegerField( 'PL_ORDEN' );
               AddIntegerField( 'CB_CODIGO' );
               AddStringField( 'PRETTYNAME', K_ANCHO_DESCRIPCION );
               AddStringField( 'PL_NOMBRE', K_ANCHO_DESCRIPCION );
               CreateTempDataset;
               BeforeEdit := cdsListaBeforeEdit;
               FieldByName( 'CB_CODIGO' ).OnValidate := cdsListaCB_CODIGOValidate;
               FieldByName( 'CB_CODIGO' ).OnChange := cdsListaCB_CODIGOChange;
          end;
     end;
     FFiltroEmpleadosPlaza := VACIO;
end;

procedure TdmRecursos.cdsListaCB_CODIGOChange(Sender: TField);
begin
     if ( Sender.AsInteger > 0 ) then
        cdsListaEmpPlazas.FieldByName( 'PRETTYNAME' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescription
     else
         cdsListaEmpPlazas.FieldByName( 'PRETTYNAME' ).AsString := VACIO;
end;

procedure TdmRecursos.cdsListaCB_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               with dmCliente.cdsEmpleadoLookup do
               begin
                    if ( GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                       DataBaseError( 'No Existe el Empleado #' + AsString )
                    else if ( not zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) ) then
                         DataBaseError( 'El Empleado # ' + IntToStr( AsInteger ) + ' est� dado de baja' )
                    else if ( dmCliente.UsaPlazas and ( FieldByName( 'CB_PLAZA' ).AsInteger > 0 ) )or ( ExisteEnListaEmpleados( AsInteger ) ) then
                       DataBaseError( 'Empleado ya tiene asignada plaza' )
                    else if ( not dmCliente.UsaPlazas and ( TienePlazaAsignada (AsInteger) )) then
                       DataBaseError( 'Empleado ya tiene asignada plaza' );
               end;
          end;
     end;
end;

function TdmRecursos.ExisteEnListaEmpleados( const iEmpleado: Integer ): Boolean;
var
   oLista: TStrings;
   i : Integer;
begin
     Result := FALSE;
     if StrLleno( FFiltroEmpleadosPlaza ) then
     begin
          try
             oLista := TStringList.Create;
             oLista.CommaText := FFiltroEmpleadosPlaza;

             for i:= 0 to oLista.Count - 1 do
             begin
                  Result := ( StrToIntDef( oLista[i], 0 ) = iEmpleado );
                  if Result then
                     Break;
             end;

          finally
                 FreeAndNil( oLista );
          end;
     end;
end;

procedure TdmRecursos.cdsListaBeforeEdit(DataSet: TDataSet);
begin
     LlenaFiltroEmpleados( DataSet );
end;

procedure TdmRecursos.LlenaFiltroEmpleados( DataSet: TDataSet );
var
   Pos : TBookMark;
   iEmpleadoOK : Integer;
   sListaEmpleados: String;
begin
     FFiltroEmpleadosPlaza := VACIO;
     sListaEmpleados := VACIO;
     with DataSet do
     begin
          iEmpleadoOK := FieldByName( 'CB_CODIGO' ).AsInteger;
          Pos:= GetBookMark;
          First;
          while ( not EOF ) do
          begin
               if ( FieldByName( 'CB_CODIGO' ).AsInteger <> iEmpleadoOK ) then
                  sListaEmpleados := ConcatString( sListaEmpleados, FieldByName( 'CB_CODIGO' ).AsString, ',' );
               Next;
          end;
          if ( Pos <> nil ) then
          begin
               GotoBookMark( Pos );
               FreeBookMark( Pos );
          end;
          if strLleno( sListaEmpleados ) then
             FFiltroEmpleadosPlaza := 'NOT ' + GetFiltroLista( 'CB_CODIGO', sListaEmpleados );
     end;
end;

procedure TdmRecursos.cdsPlazasAlModificar(Sender:TObject);
begin
     with dmCliente do
     begin
          SetLookupEmpleado( eLookEmpPlaza );
          cdsEmpleadoLookUp.Init;
          try
             ZBaseEdicion_DevEx.ShowFormaEdicion( EditPlaza_DevEx, TEditPlaza_DevEx);
          finally
                 dmCliente.ResetLookupEmpleado;
          end;
     end;
end;

procedure TdmRecursos.cdsPlazasAlCrearCampos(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
{
          cdsTurnos.Conectar;
          cdsContratos.Conectar;
          cdsClasifi.Conectar;}
          with cdsPlazas do
          begin
               CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'PU_CODIGO' );
{
               CreateSimpleLookup( cdsTurnos,  'TU_DESCRIP', 'PL_TURNO' );
               CreateSimpleLookup( cdsClasifi, 'TB_ELEMENT', 'PL_CLASIFI' );
               CreateSimpleLookup( cdsContratos,'CONTRATO', 'PL_CONTRAT' );
}
          end;
     end;
     with cdsPlazas do
     begin
          ListaFija( 'PL_ZONA_GE', lfZonaGeografica );
          ListaFija( 'PL_TIPO', lfJITipoPlaza );
          ListaFija( 'PL_TIREP', lfJIPlazaReporta );
          FieldByName('PL_REPORTA').OnGetText := EmpleadoPlazasOnGetText;
          FieldByName('CB_CODIGO').OnGetText := EmpleadoPlazasOnGetText;
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsListaCB_CODIGOValidate;
          FieldByName( 'PL_NOMINA' ).OnChange := validacionTipoNominaChange;
     end;
end;

procedure TdmRecursos.cdsPlazasAfterDelete(DataSet: TDataSet);
begin
//   cdsPlazas.Enviar;
end;

procedure TdmRecursos.EmpleadoPlazasOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText and ( Sender.AsInteger = 0 ) then
        Text := VACIO
     else
         Text:= Sender.AsString;
end;

{
procedure TdmRecursos.CB_CODIGOOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.AsInteger = 0 then
        Text := VACIO
     else
     Text:= IntToStr( Sender.AsInteger );
end;
}

procedure TdmRecursos.cdsPlazasBeforePost(DataSet: TDataSet);
var
   iIndice:Integer;

   function CambioTitular:Boolean;
   begin
        with DataSet.FieldByName('CB_CODIGO') do
        begin
             Result := ( OldValue <> NewValue );
        end;
   end;

   function LimpiandoPlaza:Boolean;
   begin
        with DataSet.FieldByName('CB_CODIGO') do
        begin
             Result :=  AsInteger = 0; 
        end;
   end;

begin
     with cdsPlazas do
     begin
          if StrVacio( FieldByName('PU_CODIGO').AsString ) then
          begin
               DataBaseError( 'El puesto no puede quedar vac�o' );
          end
          else
          if StrVacio( FieldByName('PL_NOMBRE').AsString ) then
          begin
               DataBaseError( 'La Descripci�n no puede quedar vac�a');
          end
          else
          if FieldByName('PL_FEC_INI').AsDateTime = NullDateTime  then
          begin
               DataBaseError( 'La fecha Inicial no puede quedar vac�a' );
          end
          else
          if ( FieldByName('PL_FEC_FIN').AsDateTime <> NullDateTime) and (  FieldByName('PL_FEC_FIN').AsDateTime <=  FieldByName('PL_FEC_INI').AsDateTime  )then
          begin
               DataBaseError( 'La fecha final de vigencia debe ser mayor a la fecha de inicio');
          end
          else
          if dmCliente.UsaPlazas then
          begin
               if StrVacio( FieldByName('PL_CLASIFI').AsString ) then
               begin
                    DataBaseError( 'La clasificaci�n no puede quedar vac�a');
               end
               else
               if StrVacio( FieldByName('PL_TURNO').AsString ) then
               begin
                    DataBaseError( 'El turno no puede quedar vac�o');
               end
               else
               if StrVacio( FieldByName('PL_PATRON').AsString ) then
               begin
                    DataBaseError( 'El Registro Patronal no puede quedar vac�o');
               end
               else
               if ( State = dsEdit ) and ( FieldByName('CB_CODIGO').AsInteger <> 0 ) and
                  ( FPlazaPatron <> FieldByName('PL_PATRON').AsString ) then
                  DataBaseError( 'El Registro Patronal no puede cambiar si la Plaza tiene Titular')
               else
               for iIndice := 1  to Global.NumNiveles do
                    begin
                         if StrVacio( FieldByName('PL_NIVEL'+IntToStr(iIndice)).AsString )then
                         begin
                               DataBaseError( StrLeft(Global.NombreNivel( iIndice ),Length( Global.NombreNivel( iIndice ) ) ) + ' no puede quedar vac�o') ;
                         end;
                    end
          end
          else
          begin //Validacion de Empleado en Otra Plaza
                if not LimpiandoPlaza and CambioTitular and  TienePlazaAsignada(FieldByName('CB_CODIGO').AsInteger ) and not FIntercambioPlaza then
                begin
                     DataBaseError( 'Empleado ya tiene Plaza Asignada');
                end
          end;
     end;
end;

function TdmRecursos.TienePlazaAsignada(const iEmpleado:Integer):Boolean;
begin
     Result := cdsEmpPlaza.Locate('CB_CODIGO',iEmpleado,[]);
end;

 {$ifdef INGREDION_INTERFAZ}
procedure TdmRecursos.RegistroAhorrosAutomaticos(iTipoRegistro: Integer);
var
   lAgrega, lModifica: Boolean;
   oStatus : eStatusAhorro;
   sDescripAhorro : String;
begin
    dmRecursos.cdsHisAhorros.Conectar;
     with dmTablas.cdsTAhorro do
     begin
          Conectar;
          First;
     end;

     While ( not dmTablas.cdsTAhorro.EOF ) do
     begin
          oStatus := saActivo;
          lAgrega := false;
          lModifica := false;
          
          with dmTablas.cdsTAhorro do
          begin
               sDescripAhorro := FieldByName('TB_CODIGO').AsString + '=' + FieldByName('TB_ELEMENT').AsString;
               if dmRecursos.cdsHisAhorros.Locate('AH_TIPO', FieldByName('TB_CODIGO').AsString, []) then
               begin
                    lModifica := true;
                    Case eAltaAhorroPrestamo (FieldByName('TB_ALTA').AsInteger) of
                         apNoAgregar : lModifica := False;
                         apAgregarAutomaticamente, apAgregarPreguntando : oStatus := saActivo;
                    end;
               end
               else
               begin
                    Case eAltaAhorroPrestamo (FieldByName('TB_ALTA').AsInteger) of
                         apNoAgregar : lAgrega := False;
                         apAgregarAutomaticamente,apAgregarPreguntando : lAgrega := true;
                    end;
               end;
          end;

          with dmRecursos.cdsHisAhorros do
          begin
               if ((iTipoRegistro =  0 )and (lAgrega)) then
               begin
                    Append;
                    FieldByName( 'AH_TIPO' ).AsString:= dmTablas.cdsTAhorro.FieldByName('TB_CODIGO').AsString;
                    FieldByName( 'AH_FECHA' ).AsDateTime:= dmCliente.GetDatosEmpleadoActivo.Ingreso;
                    Post;
                    dmInterfase.EscribeBitacora(Format( 'Se agreg� %s al reingreso. ', [ sDescripAhorro ] ) );
               end
               else if ((iTipoRegistro =  1 )and (lModifica)) then
               begin
                    Edit;
                    FieldByName('AH_STATUS').AsInteger := Ord(oStatus);
                    FieldByName( 'AH_FECHA' ).AsDateTime:= dmCliente.GetDatosEmpleadoActivo.Ingreso;
                    Post;
                    dmInterfase.EscribeBitacora(Format( 'Se activo %s al reingreso. ', [ sDescripAhorro ] ) );
               end;

          end;
          dmTablas.cdsTAhorro.next;
     end;
     dmRecursos.cdsHisAhorros.Enviar;
end;
 {$endif}
 
{
procedure TdmRecursos.CopiaPlazaADataSet(DataSet:TDataset);
begin
     with DataSet do
     begin
          if ( State = dsBrowse ) then
             Edit;
          if ( FieldByName('CB_PLAZA').AsInteger <> 0 ) and
             cdsPlazas.Locate('PL_FOLIO', FieldByName('CB_PLAZA').AsInteger, [] ) then
          begin
               FieldByName('CB_PUESTO').AsString := cdsPlazas.FieldByName('PU_CODIGO').AsString;
               FieldByName('CB_CLASIFI').AsString := cdsPlazas.FieldByName('PL_CLASIFI').AsString;
               FieldByName('CB_TURNO').AsString := cdsPlazas.FieldByName('PL_TURNO').AsString;
               FieldByName('CB_PATRON').AsString := cdsPlazas.FieldByName('PL_PATRON').AsString;
               FieldByName('CB_NIVEL1').AsString := cdsPlazas.FieldByName('PL_NIVEL1').AsString;
               FieldByName('CB_NIVEL2').AsString := cdsPlazas.FieldByName('PL_NIVEL2').AsString;
               FieldByName('CB_NIVEL3').AsString := cdsPlazas.FieldByName('PL_NIVEL3').AsString;
               FieldByName('CB_NIVEL4').AsString := cdsPlazas.FieldByName('PL_NIVEL4').AsString;
               FieldByName('CB_NIVEL5').AsString := cdsPlazas.FieldByName('PL_NIVEL5').AsString;
               FieldByName('CB_NIVEL6').AsString := cdsPlazas.FieldByName('PL_NIVEL6').AsString;
               FieldByName('CB_NIVEL7').AsString := cdsPlazas.FieldByName('PL_NIVEL7').AsString;
               FieldByName('CB_NIVEL8').AsString := cdsPlazas.FieldByName('PL_NIVEL8').AsString;
               FieldByName('CB_NIVEL9').AsString := cdsPlazas.FieldByName('PL_NIVEL9').AsString;
          end
          else
          begin
               FieldByName('CB_PUESTO').AsString := VACIO;
               FieldByName('CB_CLASIFI').AsString := VACIO;
               FieldByName('CB_TURNO').AsString := VACIO;
               FieldByName('CB_PATRON').AsString := VACIO;
               FieldByName('CB_NIVEL1').AsString := VACIO;
               FieldByName('CB_NIVEL2').AsString := VACIO;
               FieldByName('CB_NIVEL3').AsString := VACIO;
               FieldByName('CB_NIVEL4').AsString := VACIO;
               FieldByName('CB_NIVEL5').AsString := VACIO;
               FieldByName('CB_NIVEL6').AsString := VACIO;
               FieldByName('CB_NIVEL7').AsString := VACIO;
               FieldByName('CB_NIVEL8').AsString := VACIO;
               FieldByName('CB_NIVEL9').AsString := VACIO;
          end;
     end;
end;
}

function TdmRecursos.FiltroPlazaVacantes(const sPuesto: string = VACIO; const iPlaza:integer = 0): string;
const
     K_PLAZAS_VACANTES = '( CB_CODIGO = 0 ) AND '+
                         '( ( ( PL_FEC_INI<= %0:s ) AND ( PL_FEC_FIN>=%0:s ) ) or '+
                         '( ( PL_FEC_INI<= %0:s ) AND ( PL_FEC_FIN=''12/30/1899'' ) ) )';
{$ifdef CONFIDENCIALIDAD_MULTIPLE}
     K_CONFIDENCIALIDAD = '( ( PL_NIVEL0 = '''' ) or ( PL_NIVEL0 in %s ) ) ';
{$else}
     K_CONFIDENCIALIDAD = '( ( PL_NIVEL0 = '''' ) or ( PL_NIVEL0 = ''%s'' ) ) ';
{$endif}
     K_FILTRO_PUESTO = '( PU_CODIGO = ''%s'' ) ';
var
     sFiltroPlaza: string;
begin
     Result := Format( K_PLAZAS_VACANTES, [ ZetacommonTools.DateToStrSQLC( dmCliente.FechaDefault ) ] );

     if dmSistema.HayNivel0 then
     begin
          if StrLLeno(dmCliente.Confidencialidad) then
          {$ifdef CONFIDENCIALIDAD_MULTIPLE}
               Result := ConcatFiltros( Format(K_CONFIDENCIALIDAD,[dmCliente.ConfidencialidadListaIN]),Result )
          {$else}
               Result := ConcatFiltros( Format(K_CONFIDENCIALIDAD,[dmCliente.Confidencialidad]),Result )
          {$endif}
     end;

     if StrLleno(sPuesto) then
          Result := ConcatFiltros( Format(K_FILTRO_PUESTO,[sPuesto]),Result );

     if (iPlaza <> 0 ) then
          sFiltroPlaza := Parentesis( Format( 'PL_FOLIO = %d', [iPlaza] ) )
     else
          sFiltroPlaza := VACIO;

     Result := ConcatString( sFiltroPlaza, Parentesis( Result ), 'or' );
end;

procedure TdmRecursos.OnChangeCB_PLAZA(Sender: TField);
var
   iPlaza: Integer;

   function PosicionaPlazasLookup: Boolean;
   var
        sDescrip: String;
   begin
        with cdsPlazasLookup do
        begin
             Result := ( Active and ( iPlaza = FieldByName( 'PL_FOLIO' ).AsInteger ) ) or
                       Lookupkey( IntToStr( iPlaza ), VACIO, sDescrip );
        end;
   end;

begin
     iPlaza := Sender.AsInteger;
     with Sender.DataSet do
     begin
          if ( iPlaza <> 0 ) and PosicionaPlazasLookup then
          begin
               FieldByName('CB_PUESTO').AsString := cdsPlazasLookup.FieldByName('PU_CODIGO').AsString;
               FieldByName('CB_CLASIFI').AsString := cdsPlazasLookup.FieldByName('PL_CLASIFI').AsString;
               FieldByName('CB_TURNO').AsString := cdsPlazasLookup.FieldByName('PL_TURNO').AsString;
               FieldByName('CB_PATRON').AsString := cdsPlazasLookup.FieldByName('PL_PATRON').AsString;
               FieldByName('CB_NIVEL1').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL1').AsString;
               FieldByName('CB_NIVEL2').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL2').AsString;
               FieldByName('CB_NIVEL3').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL3').AsString;
               FieldByName('CB_NIVEL4').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL4').AsString;
               FieldByName('CB_NIVEL5').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL5').AsString;
               FieldByName('CB_NIVEL6').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL6').AsString;
               FieldByName('CB_NIVEL7').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL7').AsString;
               FieldByName('CB_NIVEL8').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL8').AsString;
               FieldByName('CB_NIVEL9').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL9').AsString;
               {$ifdef ACS}
               FieldByName('CB_NIVEL10').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL10').AsString;
               FieldByName('CB_NIVEL11').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL11').AsString;
               FieldByName('CB_NIVEL12').AsString := cdsPlazasLookup.FieldByName('PL_NIVEL12').AsString;
               {$endif}
          end
          else
          begin
               FieldByName('CB_PUESTO').AsString := VACIO;
               FieldByName('CB_CLASIFI').AsString := VACIO;
               FieldByName('CB_TURNO').AsString := VACIO;
               FieldByName('CB_PATRON').AsString := VACIO;
               FieldByName('CB_NIVEL1').AsString := VACIO;
               FieldByName('CB_NIVEL2').AsString := VACIO;
               FieldByName('CB_NIVEL3').AsString := VACIO;
               FieldByName('CB_NIVEL4').AsString := VACIO;
               FieldByName('CB_NIVEL5').AsString := VACIO;
               FieldByName('CB_NIVEL6').AsString := VACIO;
               FieldByName('CB_NIVEL7').AsString := VACIO;
               FieldByName('CB_NIVEL8').AsString := VACIO;
               FieldByName('CB_NIVEL9').AsString := VACIO;
               {$ifdef ACS}
               FieldByName('CB_NIVEL10').AsString := VACIO;
               FieldByName('CB_NIVEL11').AsString := VACIO;
               FieldByName('CB_NIVEL12').AsString := VACIO;
               {$endif}
          end;
     end;
{
     CopiaPlazaADataSet( Sender.Dataset );
}
end;

procedure TdmRecursos.AgregaCambioSalarioPlaza;
var
     oLista: TStringList;
     dFecha : TDateTime;
     sClasifi: string;
     dSalario : TPesos;
     i: Byte;
begin
     with cdsEditHisKardex do
     begin
          dFecha :=FieldByName('CB_FECHA').AsDateTime;
          sClasifi := FieldByName( 'CB_CLASIFI' ).AsString;
          //Con este metodo se graba el movimiento de Plaza
          Enviar;

          if ( ChangeCount = 0 ) then
          begin
               dmCliente.RefrescaEmpleado;

               oLista := TStringList.Create;

               try
                    LlenaListaTabulador( oLista, sClasifi, dSalario );
                    ListaPercepFijas:= '';
                    if oLista.Count > 0 then
                         for i:= 0 to oLista.Count -1 do
                              ListaPercepFijas := ConcatString( ListaPercepFijas, TOtrasPer(oLista.Objects[i]).Codigo, ',' );
               finally
                    FreeAndNil( oLista );
               end;
               Append;
               FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
               FieldByName('CB_TIPO').AsString := K_T_CAMBIO;
               FieldByName('CB_FECHA').AsDateTime := dFecha;
               FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_SI;
               Post;
               //Se graba el movimiento de Cambio de salario
               Enviar;
          end;
     end;
end;

procedure TdmRecursos.RefrescaTabuladorPlaza;
var
     sMensaje: WideString;
     oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        sMensaje:= ServerRecursos.UpdPlazaSalTab( dmCliente.Empresa );
     finally
            Screen.Cursor := oCursor;
     end;
     ZetaDialogo.ZInformation('Aplicaci�n de Tabulador en plazas', sMensaje, 0 );
end;

{ cdsPlazasLookup }
procedure TdmRecursos.cdsPlazasLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
var
     Datos: OleVariant;
begin
     lOk := ServerRecursos.GetLookupPlazas( dmCliente.Empresa, StrToIntDef( sKey, 0 ), sFilter, Datos );
     if lOk then
     begin
          with cdsPlazasLookup do
          begin
               Init;
               Data := Datos;
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmRecursos.cdsPlazasLookupLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
var
     oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             cdsPlazasSearch.Data := ServerRecursos.GetSearchPlazas( dmCliente.Empresa, sFilter );
          finally
                 Cursor := oCursor;
          end;
     end;
     lOk := ZetaBuscaPlaza_DevEx.ShowSearchForm( cdsPlazasSearch, VACIO, sKey, sDescription );  // El filtro se usa para obtener el data
end;

procedure TdmRecursos.cdsPlazasLookupGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean );
begin
     lHasRights := FALSE;
end;

{ cdsArbolPlazas }

procedure TdmRecursos.cdsArbolPlazasAlAdquirirDatos(Sender: TObject);
begin
     with cdsArbolPlazas do
     begin
          Init;
          Data := ServerRecursos.GetArbolPlazas( dmCliente.Empresa, -1 );   // Todas las plazas
     end;
end;

procedure TdmRecursos.RevisaUltimaPlaza;
const
     K_MENSAJE = CR_LF +
                 'Ya no es posible dar reversa a la Baja del Empleado.' + CR_LF +
                 ' Si quiere activarlo, favor de reingresarlo y asignarle una plaza vacante';
var
     eOrden: TOrdenKardex;
     dFecha: TDateTime;
     iPlaza: integer;
     sDescrip: String;
begin
     if dmCliente.UsaPlazas then
     begin
          //cdsPlazas.Conectar;
          eOrden := FOrdenKardex;
          dFecha := cdsHisKardex.FieldByName('CB_FECHA').AsDateTime;
          try
               SetOrdenKardex(eokDescendente);
               cdsHisKardex.Next;
               iPlaza := cdsHisKardex.FieldByName('CB_PLAZA').AsInteger;

               if ( iPlaza <> 0 )then
               begin
                    if cdsPlazasLookup.Lookupkey( IntToStr( iPlaza ), VACIO, sDescrip ) then
                    begin
                         if ( cdsPlazasLookup.FieldByName('CB_CODIGO').AsInteger <> 0 ) then
                              DataBaseError( Format( 'La plaza #%d que ten�a el Empleado %d, se encuentra ocupada por el Empleado %d.' +
                                                     K_MENSAJE,
                                                     [iPlaza, cdsHisKardex.FieldByName('CB_CODIGO').AsInteger, cdsPlazasLookup.FieldByName('CB_CODIGO').AsInteger] ));
                    end
                    else
                         DataBaseError( Format( 'La plaza #%d que ten�a el Empleado %d, ya no existe.' + K_MENSAJE,
                                                [iPlaza, cdsHisKardex.FieldByName('CB_CODIGO').AsInteger] ) );
               end
               else
                    DataBaseError( Format( 'No se pud� determinar la �ltima plaza que ten�a el Empleado %d.'+ K_MENSAJE,
                                           [cdsHisKardex.FieldByName('CB_CODIGO').AsInteger] ) );
          finally
                 SetOrdenKardex(eOrden);
                 if not cdsHisKardex.Locate('CB_FECHA;CB_TIPO', VarArrayOf([ZetaCommonTools.FechaAsStr( dFecha ),K_T_BAJA]), [] ) then
                      DataBaseError( 'La Baja del Empleado ya no existe' );
          end;
     end;
end;

procedure TdmRecursos.cdsPlazasBeforeEdit(DataSet: TDataSet);
begin
     FPlazaPatron := cdsPlazas.FieldByName('PL_PATRON').AsString;
end;


{ AP(30/10/2007): Se cambi� para solucionar el problema de asignaci�n de plaza a otro titular  }
function TdmRecursos.CambiarTitularPlaza( const iTitularIntercambio, iPlazaIntercambio: Integer; const dFecha: TDate; const sObserva: String ): Boolean;
var
     oCursor: TCursor;
     iTitular, iPlaza: Integer;

     procedure SetTitularPlaza( const iTitularTemp: Integer );
     begin
          with cdsPlazas do
          begin
               Edit;
               FieldByName( 'CB_CODIGO' ).AsInteger := iTitularTemp;
               Post;
          end;
     end;

     procedure GetPlazasIntercambio;
     var
          sFiltro: String;
     begin
          sFiltro:= Format( ' PL_FOLIO in ( %d, %d ) ', [ iPlaza, iPlazaIntercambio ] );
          with cdsPlazas do
          begin
               Data:= ServerRecursos.GetPlazasFiltro( dmCliente.Empresa, sFiltro );
          end;
     end;

begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
               with cdsPlazas do
               begin
                    iPlaza := FieldByName( 'PL_FOLIO' ).AsInteger;
                    iTitular := FieldByName( 'CB_CODIGO' ).AsInteger;
                    GetPlazasIntercambio;
                    FieldByName( 'CB_CODIGO' ).OnValidate := nil;   // No valida el titular
                    DisableControls;
                    try
                         Result:= Locate( 'PL_FOLIO', iPlaza, [] );
                         if ( Result ) then
                         begin
                              FIntercambioPlaza := True;
                              SetTitularPlaza( iTitularIntercambio );
                              Result:= Locate( 'PL_FOLIO', iPlazaIntercambio, [] );
                              if ( Result ) then
                              begin
                                   SetTitularPlaza( iTitular );
                                   ModificarPlaza( dFecha, sObserva );
                                   Result := ( ChangeCount = 0 );      // Si se grab� quedan completos los cambios
                              end;
                         end;

                         if ( not Result ) then
                         begin
                              CancelUpdates;                        // Si no tiene exito se cancelan los cambios del dataset y se queda la plaza como estaba
                              ZetaDialogo.ZError( 'Cambiar Titular', '< Error al intercambiar plazas >', 0 );
                         end;

                    finally
                           //La plaza se queda como estaba, el refrescaplaza ya hace un alcrear campos y asigna otravez el OnValidate
                           RefrescaPlaza( iPlaza, FALSE );
                           EnableControls;
                           FIntercambioPlaza := False;
                    end;
               end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TdmRecursos.RefrescaPlaza( const iPlaza: Integer; const lFoto: Boolean );
begin
     cdsPlazas.Data := ServerRecursos.GetPlaza( dmCliente.Empresa, iPlaza );
     cdsOrganigrama.Data := ServerRecursos.GetEmpOrganigrama( dmCliente.Empresa, iPlaza );
     if lFoto then
          GetOrganigramaFoto( cdsPlazas.FieldByName( 'CB_CODIGO' ).AsInteger );
end;

function TdmRecursos.CambiaCodigoReporta( const iReporta: Integer ): Boolean;
begin
     Result := FALSE;
     with dmRecursos.cdsPlazas do
     begin
          try
               Edit;                                                // Ya est� posicionado en el nodo
               FieldByName( 'PL_REPORTA' ).AsInteger := iReporta;
               Post;
               Enviar;
               Result := TRUE;
          except
               Cancel;
          end;
     end;
end;

function TdmRecursos.BorrarPlazas( const sPlazas: String; var iCuantas: Integer ): Boolean;
begin
     try
          iCuantas := ServerRecursos.BorrarPlazas( dmCliente.Empresa, sPlazas );
     finally
          Result := ( iCuantas > 0 );
     end;
end;

function TdmRecursos.GetPlazasArbol( const iPlaza: Integer ): Boolean;
begin
     with cdsArbolPlazas do
     begin
          Init;
          Data := ServerRecursos.GetArbolPlazas( dmCliente.Empresa, iPlaza ); // Solo los nodos de esta plaza
          Result := ( RecordCount > 0 );
     end;
end;

{cdsGridEmpCertific}
procedure TdmRecursos.InitGridCertific;
begin
     with FDatosCertificacionAnterior do // Inicializar Record
     begin
          Fecha  := dmCliente.FechaDefault;
          Aprobo := zStrToBool( K_GLOBAL_SI );
          Renovar := 0;
     end;
end;

procedure TdmRecursos.DefaultGridCertific( DataSet:TZetaClientDataSet );
begin
     with DataSet do
     begin
          if ( State = dsInsert ) then // Solo cuando est� agregando - No se pone en onNewRecord para que aparezca vacio el rengl�n hasta que se indique la certificacion
          begin
               with FDatosCertificacionAnterior do
               begin
                    FieldByName( 'KI_FEC_CER' ).AsDateTime := Fecha;
                    FieldByName( 'KI_APROBO' ).AsString    := zBoolToStr( Aprobo );
                    FieldByName( 'KI_RENOVAR' ).AsInteger := Renovar;
               end;
          end;
     end;
end;


procedure TdmRecursos.EditarGridEmpCertific;
begin
     InitGridCertific;
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridEmpCertific_DevEx, TGridEmpCertific_DevEx, TRUE )
end;


procedure TdmRecursos.cdsGridEmpCertificAlAdquirirDatos(Sender: TObject);
begin
     cdsGridEmpCertific.Data := ServerRecursos.GetGridCertificaciones(dmCliente.Empresa,FALSE);
end;

procedure TdmRecursos.cdsGridEmpCertificAlCrearCampos(Sender: TObject);
begin
     with cdsGridEmpCertific do
     begin
          with FieldByName( 'CI_CODIGO' ) do
          begin
               OnValidate := EmpCI_CODIGOValidate;
               OnChange := EmpCI_CODIGOChange;
          end;
          FieldByName('KI_APROBO').OnGetText := KI_APROBOGetText;
     end;
end;

procedure TdmRecursos.cdsGridCertificAlEnviarDatos(Sender: TObject);
var
     ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TZetaClientDataSet( Sender ) do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
               if Reconciliar( ServerRecursos.GrabaGridCertificaciones( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enKarCert ] );
               end;
     end;
end;

procedure TdmRecursos.cdsGridEmpCertificBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          with FieldByName( 'CI_CODIGO' ) do
          begin
               if strVacio( AsString ) then
               begin
                    FocusControl;
                    DataBaseError( 'C�digo de certificaci�n no puede quedar vac�o' );
               end;
          end;
          if FieldByName('KI_RENOVAR').AsInteger < 0 then
          begin
               FieldByName('KI_RENOVAR').FocusControl;
               DataBaseError('La renovaci�n de la certificaci�n debe ser mayor o igual a cero(0)');
          end;
          if (FieldByName('KI_FEC_CER').AsDateTime = NullDateTime  ) then
          begin
               FieldByName('KI_FEC_CER').FocusControl;
               DataBaseError('La fecha de la certificaci�n no puede quedar vac�a');
          end;
          FieldByName( 'US_CODIGO' ).AsInteger  := dmCliente.Usuario;
     end;
end;

procedure TdmRecursos.cdsGridCertificReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
     sError : String;
begin
     if PK_Violation( E ) then
     begin
          with DataSet do
          begin
               sError := Format( 'La certificaci�n ya fu� registrada.' + CR_LF + 'Empleado: %d' + CR_LF + 'C�digo de certificaci�n: %s'+ CR_LF + 'Fecha: %s',
                                 [ FieldByName( 'CB_CODIGO' ).AsInteger,
                                   FieldByName( 'CI_CODIGO' ).AsString,
                                   FechaCorta( FieldByName( 'KI_FEC_CER' ).AsDateTime ) ] );
          end;
     end
     else
     begin
          sError := GetErrorDescription( E );
     end;
     cdsGridEmpCertific.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO', sError );
     Action := raCancel;
end;


procedure TdmRecursos.EmpCI_CODIGOValidate(Sender: TField);
var
     sDescrip: String;
     lActivo, lUsarConfidencialidad : Boolean;
begin
     lActivo := True;
     lUsarConfidencialidad := False;
     with Sender do
     begin
          if strLleno( AsString ) then
          begin
               if not ( dmCatalogos.cdsCertificaciones.LookupKey( UpperCase( AsString ), '', sDescrip, lActivo, lUsarConfidencialidad ) ) then
                    DataBaseError( 'No existe el c�digo de la certificaci�n: ' + AsString )
               else
               if ( not lActivo ) then
                    DataBaseError( 'No est� activo el c�digo de la certificaci�n : ' + AsString );
          end
          else
               DataBaseError( 'C�digo de certificaci�n no puede quedar vac�o' );
     end;
end;

procedure TdmRecursos.EmpCI_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          FieldByName( 'CI_NOMBRE' ).AsString   := dmCatalogos.cdsCertificaciones.GetDescription;
          DefaultGridCertific(TZetaClientDataSet( Sender.DataSet ) );
     end;
end;

procedure TdmRecursos.cdsGridCertificAfterPost(DataSet: TDataSet);
begin
     with FDatosCertificacionAnterior do
     begin
          with DataSet do
          begin
               Fecha   := FieldByName('KI_FEC_CER').AsDateTime;
               Aprobo  := zStrToBool ( FieldByName('KI_APROBO').AsString) ;
               Renovar := FieldByName('KI_RENOVAR').AsInteger;
          end;
     end;
end;

procedure TdmRecursos.KI_APROBOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if zStrToBool( Sender.AsString ) then
          Text := K_PROPPERCASE_SI
     else
          Text := K_PROPPERCASE_NO;
end;


{cdsGridGlobalCertific  //////////////////////////////////////////////////////}

procedure TdmRecursos.EditarGridGlobalCertific;
begin
     InitGridCertific;
     try
          dmCliente.SetLookupEmpleado;
          ZBaseGridEdicion_DevEx.ShowGridEdicion( GridGlobalCertific_DevEx, TGridGlobalCertific_DevEx, TRUE );
     finally
          dmCliente.ResetLookupEmpleado;
    end;
end;


procedure TdmRecursos.cdsGridGlobalCertificAlAdquirirDatos( Sender: TObject );
begin
     cdsGridGlobalCertific.Data := ServerRecursos.GetGridCertificaciones(dmCliente.Empresa,TRUE);
end;

procedure TdmRecursos.cdsGridGlobalCertificAlCrearCampos(Sender: TObject);
begin
     with cdsGridGlobalCertific do
     begin
          with FieldByName( 'CB_CODIGO' ) do
          begin
               OnValidate := cdsGridCB_CODIGOValidate;
               OnChange := cdsGridGlobalCB_CODIGOChange;
          end;
          FieldByName('KI_APROBO').OnGetText := KI_APROBOGetText;
     end;
end;

procedure TdmRecursos.cdsGridGlobalCB_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          FieldByName( 'PRETTYNAME' ).AsString  := dmCliente.cdsEmpleadoLookup.GetDescripcion( FieldByName( 'CB_CODIGO' ).AsString );
          DefaultGridCertific( TZetaClientDataSet( Sender.DataSet ) );
     end;
end;


procedure TdmRecursos.cdsGridGlobalCertificBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          with FieldByName( 'CB_CODIGO' ) do
          begin
               if strVacio( AsString ) then
               begin
                    FocusControl;
                    DataBaseError( 'C�digo de empleado no puede quedar vac�o' );
               end;
          end;
          if FieldByName('KI_RENOVAR').AsInteger < 0 then
          begin
                FieldByName('KI_RENOVAR').FocusControl;
                DataBaseError('La renovaci�n de la certificaci�n debe ser mayor o igual a cero(0)');
          end;
          if( FieldByName('KI_FEC_CER').AsDateTime = NullDateTime ) then
          begin
                FieldByName('KI_FEC_CER').FocusControl;
                DataBaseError('La fecha de la certificaci�n no puede quedar vac�a');
          end;
          FieldByName( 'US_CODIGO' ).AsInteger  := dmCliente.Usuario;
     end;
end;


procedure TdmRecursos.cdsHisCerProgAlAdquirirDatos(Sender: TObject);
begin
     With dmCliente do
          cdsHisCerProg.Data := ServerRecursos.GetHisCertificaProg( Empresa, Empleado );

end;

procedure TdmRecursos.cdsHisCerProgAlCrearCampos(Sender: TObject);
begin
     with cdsHisCerProg do
     begin
          MaskFecha( 'KI_FEC_PRO' );
          MaskFecha( 'KI_FEC_TOM' );
     end;
end;

procedure TdmRecursos.AgregarCertificacionesProgramadas(const Certificacion:string);
Var
     oEmpleadosProgramados :TZetaClientDataSet;
begin
     oEmpleadosProgramados := TZetaClientDataSet.Create(Self);
     oEmpleadosProgramados.Data := ServerRecursos.GetCertificacionesGlobal( dmCliente.Empresa,dmCliente.FechaDefault,Certificacion );
     with oEmpleadosProgramados do
     begin
          First;
          While (not Eof)do
          begin
               cdsGridGlobalCertific.Append;
               cdsGridGlobalCertific.FieldByName('CB_CODIGO').AsInteger := FieldByName('CB_CODIGO').AsInteger;
               cdsGridGlobalCertific.FieldByName('KI_FEC_CER').AsDateTime := FieldByName('KI_FEC_PRO').AsDateTime;
               cdsGridGlobalCertific.FieldByName('KI_APROBO').AsString := K_GLOBAL_SI ;
               cdsGridGlobalCertific.FieldByName('KI_RENOVAR').AsInteger := 0;
               cdsGridGlobalCertific.Post;
               Next;
          end;
     end;

end;

{ cdsHisCreInfonavit }
procedure TdmRecursos.InitDatosKardexInfonavit;
begin
     with cdsLookupInfonavit do
     begin
          CloneCursor( cdsHisCreInfonavit, FALSE, TRUE );
     end;
end;

procedure TdmRecursos.AsignaDefaultsInfonavit( const eTMovimiento: eTipoMovInfonavit );
var
     dFecha: TDateTime;
begin
     dFecha:= dmCliente.FechaDefault;
     with cdsHisCreInfonavit do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
               Edit;
          case eTMovimiento of
               infoInicio,infoReinicio,infoSuspen: dFecha:= dmCliente.FechaDefault;
               {AP(23/Feb/2009): Las suspensiones pueden ser a medio bimestre
               infoSuspen: dFecha:= LastDayOfBimestre( dmCliente.FechaDefault );}
               infocambiotd,infocambiovd, infocambionocre: dFecha:= FirstDayOfBimestre ( dmCliente.FechaDefault );
          end;
          FieldByName('KI_FECHA').AsDateTime:= dFecha;
     end;
end;


procedure TdmRecursos.cdsHisCreInfonavitAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsHisCreInfonavit.Data := ServerRecursos.GetHisCreInfonavit( Empresa, Empleado );
     InitDatosKardexInfonavit;
end;

procedure TdmRecursos.cdsHisCreInfonavitAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisCreInfonavit do
     begin
          MaskFecha('KI_FECHA');
          MaskFecha('CB_INF_ANT');          
          ListaFija( 'KI_TIPO', lfTipoMovInfonavit );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          FieldByName('KI_TIPO').OnChange := OnKI_TIPOChange;
          FieldByName('KI_FECHA').OnChange := CB_INF_ANT_Change;
     end;
end;

procedure TdmRecursos.CB_INF_ANT_Change( Sender: TField );
var
     eTipo: eTipoMovInfonavit;
begin
     with cdsHisCreInfonavit do
     begin
          eTipo:= eTipoMovInfonavit(FieldByName('KI_TIPO').AsInteger);

          if ( eTipo = infoInicio) OR ( eTipo = infoReinicio ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                    Edit;
               FieldByName('CB_INF_ANT').AsDateTime := FieldByName('KI_FECHA').AsDateTime;
          end;
     end;
end;

procedure TdmRecursos.OnKI_TIPOChange(Sender: TField);
    procedure AsignaDatosEmpleadoInfo;
    begin
         { Asigna todos los datos relacionados con el Empleado }
          with cdsHisCreInfonavit do
          begin
               FieldByName('CB_CODIGO').AsInteger:= dmCliente.Empleado;
               FieldByName('CB_INFTIPO').AsInteger:= dmCliente.cdsEmpleado.FieldByName('CB_INFTIPO').AsInteger;
               FieldByName('CB_INFCRED').AsString:= dmCliente.cdsEmpleado.FieldByName('CB_INFCRED').AsString;
               FieldByName('CB_INFTASA').AsFloat:= dmCliente.cdsEmpleado.FieldByName('CB_INFTASA').AsFloat;
               FieldByName('CB_INFDISM').AsString:= dmCliente.cdsEmpleado.FieldByName('CB_INFDISM').AsString;
               FieldByName('CB_INF_ANT').AsDateTime:= dmCliente.cdsEmpleado.FieldByName('CB_INF_ANT').AsDateTime;
          end;
    end;
begin
     AsignaDatosEmpleadoInfo;
     AsignaDefaultsInfonavit( eTipoMovInfonavit( Sender.AsInteger ) );
end;


procedure TdmRecursos.cdsHisCreInfonavitAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisCreInfonavit_DevEx, TEditHisCreInfonavit_DevEx );
end;

procedure TdmRecursos.cdsHisCreInfonavitAlEnviarDatos(Sender: TObject);
var
     ErrorCount, iTipo : Integer;
     dFecha : TDate;   
begin
     ErrorCount := 0;
     with cdsHisCreInfonavit do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if ( Reconcile(ServerRecursos.GrabaHistorial( dmCliente.Empresa, cdsHisCreInfonavit.Tag, cdsHisCreInfonavit.Delta, ErrorCount ) ) ) then
               begin
                    dFecha := FieldByName('KI_FECHA').AsDateTime;
                    iTipo := FieldByName('KI_TIPO').AsInteger;
                    TressShell.SetDataChange( [ enEmpleado ] );
                    refrescar;
               
                    //Locate('KI_FECHA;KI_TIPO', VarArrayOf([FechaAsStr( dFecha ),iTipo]), [] );                    
					Locate('KI_FECHA;KI_TIPO', VarArrayOf([ dFecha, iTipo]), [] );
                    InitDatosKardexInfonavit;
               end;
          end;
     end;
end;


procedure TdmRecursos.cdsHisCreInfonavitNewRecord(DataSet: TDataSet);
const
     aValueTipoMov: array[FALSE..TRUE] of Integer = ( Ord ( infocambiotd ), Ord( infoInicio ) );
begin
     with cdsHisCreInfonavit, dmCliente do
     begin
          cdsLookupInfonavit.Last;
          FieldByName('KI_TIPO').AsInteger:= aValueTipoMov[ ( RecordCount = 0 ) ];
     end;
end;

procedure TdmRecursos.cdsHisCreInfonavitBeforePost(DataSet: TDataSet);
var
   dFecha{,dIngreso}, dFechaInicioCredito: TDate;
   eTipo: eTipoMovInfonavit;
   lEsPrimerRegistro, lCreditoActivo, lInsertando,lEsInicioSuspen,lEsInicioReinicio: Boolean;
   sMensaje: String;

const
     K_IMPOSIBLE_REGISTRAR = 'No se puede ingresar movimientos.';

    function ExisteMovCredito: Boolean;
    begin
         Result:= cdsLookupInfonavit.Locate('KI_FECHA',VarArrayOf([ FechaAsStr( cdsHisCreInfonavit.FieldByName('KI_FECHA').AsDateTime) ]), [] ) and
                  ( cdsHisCreInfonavit.FieldByName('KI_TIPO').AsInteger <> cdsLookupInfonavit.FieldByName('KI_TIPO').AsInteger ) ;
    end;

    function EsMovimientoMenor: Boolean;
    begin
         cdsLookupInfonavit.Last;
         Result:= not ( dFecha >= cdsLookupInfonavit.FieldByName('KI_FECHA').AsDateTime ) ;
    end;

    function ObligatoriosVacios: Boolean;
    begin
         with cdsHisCreInfonavit do
         begin
              if ( StrVacio ( FieldByName('CB_INFCRED').AsString ) ) then
              begin
                   FieldByName('CB_INFCRED').FocusControl;
                   sMensaje:= 'El # de cr�dito no puede quedar vac�o';
              end
              else if ( FieldByName('CB_INFTIPO').AsInteger = Ord ( tiNoTiene ) ) then
              begin
                   FieldByName('CB_INFTIPO').FocusControl;
                   sMensaje:= 'Se debe seleccionar un tipo de descuento';
              end

              else if ( FieldByName('CB_INFTASA').AsFloat <= 0 ) then
              begin
                   FieldByName('CB_INFTASA').FocusControl;
                   sMensaje:= 'El valor del cr�dito debe ser mayor a 0';
              end;
              Result:= StrLleno( sMensaje );
         end;
    end;

    function ValidaFechaBimestre( const lFinalBimestre: Boolean ): Boolean;
    begin
         if ( lFinalBimestre ) then
         begin
              Result:= ( dFecha = LastDayOfBimestre(dFecha ));
         end
         else
         begin
              Result:= ( dFecha = FirstDayOfBimestre(dFecha));
         end;

         if not Result then
         begin
              cdsHisCreInfonavit.FieldByName('KI_FECHA').FocusControl;
         end;
    end;

    function SinCambios: Boolean;
    begin
         Result:= FALSE;
         with dmCliente.cdsEmpleado do
         begin
              case eTipo of
                   infocambiotd:
                   begin
                        Result:= ( FieldByName('CB_INFTIPO').AsInteger = cdsHisCreInfonavit.FieldByName('CB_INFTIPO').AsInteger );
                        if ( Result ) then
                           cdsHisCreInfonavit.FieldByName('CB_INFTIPO').FocusControl;
                   end;
                   infocambiovd:
                   begin
                        Result:= ( FieldByName('CB_INFTASA').AsFloat = cdsHisCreInfonavit.FieldByName('CB_INFTASA').AsFloat );
                         if ( Result ) then
                           cdsHisCreInfonavit.FieldByName('CB_INFTASA').FocusControl;
                   end;
                   infocambionocre:
                   begin
                         Result:= ( FieldByName('CB_INFCRED').AsString = cdsHisCreInfonavit.FieldByName('CB_INFCRED').AsString );
                         if ( Result ) then
                           cdsHisCreInfonavit.FieldByName('CB_INFCRED').FocusControl;
                   end;
              end;
         end;
    end;


    function InvalidoPorTipoMov: Boolean;
    begin
         case eTipo of
             infoSuspen:
             begin
                  {if not ( ValidaFechaBimestre( TRUE ) ) then
                  begin
                       sMensaje:= 'La fecha debe ser igual al �ltimo d�a del bimestre' ;
                  end
                  else }if ( lInsertando ) and ( not lCreditoActivo ) then
                  begin
                       sMensaje:= 'El cr�dito tiene que estar en status activo' ;
                  end
                  else if ( dFecha < dFechaInicioCredito )  then
                  begin
                       sMensaje:= Format( 'Fecha Suspensi�n[%s] tiene que ser mayor a la fecha de Alta del Cr�dito: %s',
                                      [ FechaCorta( dFecha ), FechaCorta( dFechaInicioCredito ) ] );
                  end
                  { La fecha de suspensi�n tiene que ser igual o menor al �ltimo mov }
                  else if ( EsMovimientoMenor ) then
                  begin
                       sMensaje:= 'La Suspensi�n debe ser el Ultimo Movimiento';
                  end;
             end;
             infoReinicio:
             begin
                  if ( lInsertando ) then
                  begin
                       if ( lCreditoActivo ) then
                       begin
                            sMensaje:= 'El cr�dito tiene que estar en status suspendido' ;
                       end
                       else
                       if ( EsMovimientoMenor ) then
                       begin
                            sMensaje:= 'El Reinicio debe ser el Ultimo Movimiento';
                       end
                  end;
             end;
             infocambiotd, infocambiovd, infocambionocre:
             begin
                  if not (  ValidaFechaBimestre( FALSE ) ) then
                  begin
                       sMensaje:= 'La fecha debe ser igual al primer d�a del bimestre' ;
                  end
                  else if ( lInsertando ) and ( SinCambios ) then
                  begin
                       sMensaje:= Format( 'No se registr� %s', [ ObtieneElemento(lfTipoMovInfonavit,Ord( eTipo ) )] )
                  end;
             end;
         end;
         Result:= StrLleno( sMensaje );
    end;

begin
     with cdsHisCreInfonavit do
     begin
          cdsLookupInfonavit.Last;
          //dIngreso := cdsDatosEmpleado.FieldByName( 'CB_FEC_ING' ).AsDateTime;
          dFechaInicioCredito:= dmCliente.cdsEmpleado.FieldByName('CB_INF_INI').AsDateTime;
          dFecha:= FieldByName('KI_FECHA').AsDateTime;
          eTipo:= eTipoMovInfonavit( FieldByName('KI_TIPO').AsInteger );
          lEsPrimerRegistro:= ( RecordCount = 0 );
          lEsInicioReinicio:= ( cdsLookupInfonavit.FieldByName('KI_TIPO').AsInteger = Ord( infoInicio ) ) and
                             ( FieldByName('KI_TIPO').AsInteger = Ord( inforeinicio ) );  
          lCreditoActivo:= zStrToBool(cdsDatosEmpleado.FieldByName( 'CB_INFACT' ).AsString );
          lInsertando:= ( State = dsInsert );
          lEsInicioSuspen:=  ( cdsLookupInfonavit.FieldByName('KI_TIPO').AsInteger = Ord( infoInicio ) ) and
                             ( FieldByName('KI_TIPO').AsInteger = Ord( infoSuspen ) );

          {El primer registro tiene que ser de tipo alta}
          if ( lEsPrimerRegistro ) and ( eTipo <> infoinicio ) then
          begin
               FieldByName('KI_TIPO').FocusControl;
               DataBaseError ( Format( 'El primer registro debe ser de tipo ' + CR_LF + '%s',[ ObtieneElemento(lfTipoMovInfonavit, Ord( infoInicio ) )]) );
          end
          { No se puede hacer ningun tipo d movimiento si el cr�dito esta suspendido }
          else if ( lInsertando ) and not( eTipo in [ infoInicio, infoReinicio ] ) and ( not lCreditoActivo ) then
          begin
               DataBaseError( K_IMPOSIBLE_REGISTRAR + 'El cr�dito esta suspendido' )
          end
          { Fecha de Movimiento menor o igual a la fecha de inicio de cr�dito, IGUAL solamente cuando sea diferente a suspensi�n. }
          else if ( not ( eTipo in [infoInicio, infoReinicio, infoSuspen ] ) and ( dFecha < dFechaInicioCredito )  ) then
          begin
               FieldByName('KI_FECHA').FocusControl;
               DataBaseError( Format( 'Fecha movimiento [%s] tiene que ser mayor a fecha de Alta del Cr�dito: %s',
                                    [ FechaCorta( dFecha ), FechaCorta( dFechaInicioCredito ) ] ) )
          end
          {No se puede insertar un registro si existe un movimiento en esa fecha, exceptuando el inicio y suspension }
          else if ( not lEsInicioSuspen ) and ( ExisteMovCredito  ) then
          begin
               FieldByName('KI_FECHA').FocusControl;
               DataBaseError( 'Ya existe un movimiento de cambio en esa misma fecha' );
          end
          else if ( lEsInicioReinicio ) OR ( FieldByName( 'CB_INF_ANT').AsDateTime > FieldByName('KI_FECHA').AsDateTime ) then
          begin
               if not ZWarningConfirm('� Atenci�n !', 'La fecha de otorgamiento es mayor que la fecha del movimiento' + CR_LF + '�Seguro de grabar? ', 0, mbNO) then
                    Abort;
          end
          { Si los registros obligatorios estan vaci�s o el movimiento no cumple x especificaci�n de c/T. Mov }
          else if ObligatoriosVacios or InvalidoPorTipoMov then
          begin
               DataBaseError ( sMensaje );
          end
          else
          begin
               dmCliente.ValidaCreditoInfo(cdsHisCreInfonavit);
               FieldByName('KI_CAPTURA').AsDateTime:= dmCliente.FechaDefault;
               FieldByName('US_CODIGO').AsInteger:= dmCliente.Usuario;
               if StrVacio ( FieldByName('KI_NOTA').AsString )  then
                      FieldByName('KI_NOTA').AsString := K_ESPACIO;
          end;
     end;
end;

procedure TdmRecursos.cdsHisCreInfonavitAlBorrar(Sender: TObject);
begin
     with cdsHisCreInfonavit do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar �ste registro del historial de Infonavit ?' ) then
          begin
               Delete;
               Enviar;
          end;
     end;
end;


procedure TdmRecursos.cdsHisCreInfonavitBeforeDelete(DataSet: TDataSet);
     function InvalidoPorTipoMov: Boolean;
     begin
          with cdsHisCreInfonavit do
          begin
               cdsLookupInfonavit.Last;
               case eTipoMovInfonavit( FieldByName('KI_TIPO').AsInteger ) of
                    infoSuspen,infoReinicio,infoInicio:
                    begin
                          Result:= not ( FieldByName('KI_FECHA').AsDateTime >= cdsLookupInfonavit.FieldByName('KI_FECHA').AsDateTime ) ;
                    end
                    else
                          Result:= FALSE;
               end;
          end;
     end;
begin
     if ( InvalidoPorTipoMov ) then
     begin
          DataBaseError( 'No se puede borrar un inicio, reinicio o suspensi�n si existen movimientos posteriores' );
     end;
end;

procedure TdmRecursos.cdsHisPrestamosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     //FMensajePrestamo := VACIO;
     if Global.GetGlobalBooleano(K_GLOBAL_VALIDAR_PRESTAMOS) and not FValidarReglas then
     begin
         //E.Message := StrTransAll( E.Message, 'PRE_FILTRO', 'Problema al evaluar el filtro');
         //E.Message := StrTransAll( E.Message, 'PRE_CONDICION', 'Problema al evaluar la condici�n');
         //FMensajePrestamo := E.Message;
         if Strlleno( FMensajePrestamo ) then
         begin
              if CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_SIST_KARDEX ) then
              begin
                   Action := Zreconcile.HandleReconcilePrestamosWrng(DataSet,UpdateKind,E, FMensajePrestamo );
                   FValidarReglas := ( Action = raCorrect );
              end
              else
                   Action := Zreconcile.HandleReconcilePrestamosError(DataSet,UpdateKind,E, FMensajePrestamo );
         end
         else
              Action := Zreconcile.HandleReconcileError(DataSet,UpdateKind,E);
     end
     else
     begin
          Action := Zreconcile.HandleReconcileError(DataSet,UpdateKind,E);
     end;
end;

procedure TdmRecursos.cdsCursosProgAlCrearCampos(Sender: TObject);
begin
     cdsCursosProg.MaskFecha( 'EP_FECHA' );
end;

function TdmRecursos.CargaDocumento( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
begin
     Result := StrLleno( sObservaciones ) ;
     if Result then
     begin
          with cdsHisCreInfonavit do
          begin
               if State = dsBrowse then
                    Edit;
               if lEncimarArchivo then
                    Result := ZetaFilesTools.CargaDocumento( cdsHisCreInfonavit,
                                                             sPath,
                                                             'KI_D_BLOB',
                                                             'KI_D_EXT' );
               if Result then
                    FieldByName('KI_D_NOM').AsString := sObservaciones;
          end
     end
     else
          ZetaDialogo.ZError( 'Error en el Documento...', 'El nombre no puede quedar vac�o', 0 );
end;

procedure TdmRecursos.AbreDocumento;
begin
     ZetaFilesTools.AbreDocumento( cdsHisCreInfonavit, 'KI_D_BLOB', 'KI_D_EXT' );
end;

procedure TdmRecursos.BorraDocumento;
begin
     with cdsHisCreInfonavit do
     begin
          if State = dsBrowse then
             Edit;

          FieldByName('KI_D_NOM').AsString := VACIO;
          FieldByName('KI_D_EXT').AsString := VACIO;
          FieldByName('KI_D_BLOB').AsString := VACIO;
     end;
end;

{$ifdef COVIDIEN_INTERFAZ_HR}
function TdmRecursos.BuscarIncapacidadAnterior( const FechaInicial:TDate; const sTipo :string ):Boolean;
begin
       Result := False;
       with cdsIncapacidades do
       begin
            if Not IsEmpty then
            begin
                 First;
                 While ( ( FieldByName('IN_FEC_INI').AsDateTime < FechaInicial ) and ( not Eof ) ) do
                 begin
                      Result := (( FieldByName('IN_MOTIVO').AsInteger in [ Ord( miInicial ), Ord( miSubsecuente ) ] ) and
                                 ( FieldByName('IN_TIPO').AsString = sTipo  ) and
                                 ( FieldByName('IN_FEC_FIN').AsDateTime = FechaInicial )
                                 );
                      if Result then
                         Break;
                      Next;
                 end;
            end;
       end;
 end;
{$endif}

// (JB) Anexar al expediente del trabajador documentos CR1880 T1107---------------------------------
// -------------------------------------------------------------------------------------------------
function TdmRecursos.CargaDocumentoExpediente( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
begin
     Result := StrLleno( sObservaciones ) ;
     if Result then
     begin
          with cdsDocumento do
          begin
               if State = dsBrowse then
                    Edit;
               if lEncimarArchivo then
                    Result := ZetaFilesTools.CargaDocumento( cdsDocumento,
                                                             sPath,
                                                             'DO_BLOB',
                                                             'DO_EXT' );
               if Result then
                    FieldByName('DO_NOMBRE').AsString := sObservaciones;
          end
     end
     else
          ZetaDialogo.ZError( 'Error en el Documento...', 'El nombre no puede quedar vac�o', 0 );
end;

procedure TdmRecursos.AbreDocumentoExpediente;
begin
     ZetaFilesTools.AbreDocumento( cdsDocumento, 'DO_BLOB', 'DO_EXT' );
end;

{$ifdef ICUMEDICAL_CURSOS}
procedure TdmRecursos.AbreDocumentoICU;
begin
     with cdsSesion do
     begin
          if( FileExists( FieldByName('SE_D_RUTA').AsString ) ) then
              ZetaFilesTools.AbreDocumentoICU( cdsSesion, cdsSesion.FieldByName('SE_D_RUTA').AsString )
          else
              ZetaDialogo.ZError('Problema al abrir el documento', 'No se pudo abrir el archivo de la ruta almacenada. Favor de asegurarse que exista un archivo con ese nombre.', 0);
     end;
end;

procedure TdmRecursos.BorraDocumentoICU;
begin
     with cdsSesion do
     begin
          if State = dsBrowse then
             Edit;
          FieldByName('SE_D_NOM').AsString := VACIO;
          FieldByName('SE_D_EXT').AsString := VACIO;
          FieldByName('SE_D_RUTA').AsString := VACIO;
     end;
end;

function TdmRecursos.CargaDocumentoICU( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
begin
     Result := StrLleno( sPath ) ;
     if Result then
     begin
           with cdsSesion do
           begin
                if State = dsBrowse then
                   Edit;

                if lEncimarArchivo then
                   Result := ZetaFilesTools.CargaDocumentoICU( cdsSesion, sPath, 'SE_D_EXT' );

                if Result then
                   FieldByName('SE_D_NOM').AsString := Copy( ExtractFileName( sPath ),1,Pos( '.', sPath )-1 );
           end
     end
     else
         ZetaDialogo.ZError( 'Error en el Documento...', 'El nombre no puede quedar vac�o', 0 );
end;

{$endif}

procedure TdmRecursos.cdsDocumentoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsDocumento.Data := ServerRecursos.GetDocumentos( Empresa, Empleado );
end;

procedure TdmRecursos.cdsDocumentoAlBorrar(Sender: TObject);
begin
     with cdsDocumento do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar �ste documento ?' ) then
          begin
               Delete;
               Enviar;
          end;
     end;
end;

procedure TdmRecursos.cdsDocumentoAlCrearCampos(Sender: TObject);
begin
     with cdsDocumento do
     begin
          FieldByName('DO_EXT').OnGetText := cdsDocumentoDO_EXT_OnGetText;
     end;
end;

procedure TdmRecursos.cdsDocumentoDO_EXT_OnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( StrVacio( Sender.AsString ) ) then
        Text := ''
     else
         Text := ZetaFilesTools.GetTipoDocumento( Sender.AsString );
end;

procedure TdmRecursos.cdsDocumentoAlEnviarDatos(Sender: TObject);
var
     ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsDocumento do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if ( Reconcile(ServerRecursos.GrabaDocumento( dmCliente.Empresa, cdsDocumento.Delta, ErrorCount ) ) ) then
               begin
                    TressShell.SetDataChange( [ enEmpleado ] );
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsDocumentoAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpDocumento_DevEx, TEditEmpDocumento_DevEx )
end;

procedure TdmRecursos.cdsDocumentoAlAgregar(Sender: TObject);
begin
     cdsDocumento.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpDocumento_DevEx, TEditEmpDocumento_DevEx )
end;

procedure TdmRecursos.cdsDocumentoBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if ( StrVacio( FieldByName('DO_TIPO').AsString ) ) then
               DataBaseError ( 'No est� especificado el Tipo de Documento!' )
          else if ( StrVacio( FieldByName('DO_BLOB').AsString ) ) then
               DataBaseError ( 'No est� especificado el Documento!' )
          else if ( StrVacio( FieldByName('DO_NOMBRE').AsString ) ) then
               DataBaseError ( 'No est� especificado el Nombre del Documento!' )
          else if ( StrVacio( FieldByName('DO_EXT').AsString ) ) then
               DataBaseError ( 'No est� especificada la Extensi�n del Documento!' );
     end;
end;

procedure TdmRecursos.cdsDocumentoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     with E do
     begin
          { PK_VIOLATION }
          if PK_VIOLATION( E ) then
             sError := '� Tipo de Documento ya Existe para ese Empleado !'
          else
              sError := Message;
          if ( Context <> '' ) then
              sError := sError + CR_LF + Context;
     end;
     ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ) + ' Documento', sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
     case UpdateKind of
          ukDelete: Action := raCancel;
     else
         Action := raAbort;
     end;
end;

procedure TdmRecursos.cdsSaldoVacacionesAlCrearCampos(Sender: TObject);
begin
     with cdsSaldoVacaciones do
     begin
          MaskPesos( 'VS_D_PAGO' );
          MaskHoras( 'VS_PAGO' );
          MaskHoras( 'VS_S_PAGO' );

          MaskPesos( 'VS_D_GOZO' );
          MaskHoras( 'VS_GOZO' );
          MaskHoras( 'VS_S_GOZO' );

          MaskPesos( 'VS_D_PRIMA' );
          MaskHoras( 'VS_PRIMA' );
          MaskHoras( 'VS_S_PRIMA' );
          MaskFecha( 'VS_FEC_VEN' );
          MaskPesos( 'VS_CB_SAL' );

     end;
end;

procedure TdmRecursos.cdsSaldoVacacionesAlAdquirirDatos(Sender: TObject);
var
   DatosTotales : OleVariant;
begin
     with dmCliente do
     begin
          cdsSaldoVacaciones.Data  := ServerRecursos.GetSaldosVacaciones(Empresa,Empleado,DatosTotales );
          cdsSaldoVacacionesTotales.Data := DatosTotales;
     end;
end;

procedure TdmRecursos.cdsSaldoVacacionesTotalesAlCrearCampos(Sender: TObject);
begin
      with cdsSaldoVacacionesTotales do
     begin
          MaskHoras( 'SaldoGozo' );
          MaskHoras( 'SaldoPago' );
          MaskHoras( 'SaldoPrima' );
     end;
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasAlAdquirirDatos(Sender: TObject);
begin
     dmCatalogos.cdsTiposPension.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmCliente do
          cdsHisPensionesAlimenticias.Data := ServerRecursos.GetPensionesAlimenticias( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasAfterOpen(DataSet: TDataSet);
begin
     cdsPorcPensiones.DataSetField := cdsHisPensionesAlimenticias.FieldByName('qryDetail') as TDataSetField;
end;


procedure TdmRecursos.cdsHisPensionesAlimenticiasAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisPensionesAlimenticias do
     begin
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          MaskFecha('PS_FECHA');
     end;
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   oPension: OleVariant;
begin
     ErrorCount := 0;
     with cdsHisPensionesAlimenticias do
     begin
          PostData;
          cdsPorcPensiones.PostData;
          if (Changecount > 0 ) or (cdsPorcPensiones.ChangeCount > 0)  then
          begin
               ErrorCount := ChangeCount;
               DisableControls;
               try
                  oPension := ServerRecursos.GrabaPensionesAlimenticias( dmCliente.Empresa, Delta, ErrorCount );
                  if (  ( ChangeCount = 0 ) or Reconcile( oPension )  )then
                  begin
                       TressShell.SetDataChange( [ enPensiones ] );
                  end
               finally
                 EnableControls;
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion(EditHisPensionAlimenticia_DevEx,TEditHisPensionAlimenticia_DevEx);
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasNewRecord(DataSet: TDataSet);
begin
     with cdsHisPensionesAlimenticias do
     begin
          FieldByName('PS_ORDEN').AsInteger := FMaxOrdenPensiones + 1;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('PS_FECHA').AsDateTime := dmCliente.FechaDefault;
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('PS_ACTIVA').AsString := 'S';
     end;
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasBeforePost(DataSet: TDataSet);
begin
     with cdsHisPensionesAlimenticias do
     begin
          if StrVacio( FieldByName('PS_BENEFIC').AsString )then
          begin
               FieldByName('PS_BENEFIC').FocusControl;
               DataBaseError('Nombre del Beneficiario No Puede Quedar Vac�o');
          end;
     end;
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasAfterCancel(
  DataSet: TDataSet);
begin
     cdsPorcPensiones.CancelUpdates;
end;

procedure TdmRecursos.cdsPorcPensionesAfterDelete(DataSet: TDataSet);
begin
     if cdsHisPensionesAlimenticias.State = dsBrowse then
        cdsHisPensionesAlimenticias.Edit;
end;

procedure TdmRecursos.cdsPorcPensionesAlCrearCampos(Sender: TObject);
begin
     with cdsPorcPensiones do
     begin
          MaskTasa('PP_PORCENT');
          CreateSimpleLookup(dmCatalogos.cdsTiposPension ,'TP_CODIGO_TXT','TP_CODIGO');
          FieldByName( 'TP_CODIGO' ).OnValidate := PorcPensionesTP_CODIGOValidate;
     end;
end;

procedure TdmRecursos.PorcPensionesTP_CODIGOValidate(Sender: TField);
const
    sMensaje = 'No Existe el C�digo: %s en Cat�logo de Tipos de Pensi�n';
var
   sValor, sDescripcion : string;
begin
     sValor := Sender.AsString;
     if ( sValor <> VACIO ) then
     begin
          with dmCatalogos.cdsTiposPension do
               if NOT LookUpKey( UpperCase(sValor), Vacio, sDescripcion )  then
                  DataBaseError( Format( sMensaje, [ sValor ] ) );
     end;
end;

procedure TdmRecursos.cdsPorcPensionesBeforePost(DataSet: TDataSet);
begin
     //Buscar Tipos de Pension Repetidos
     with cdsPorcPensiones do
     begin
          If StrVacio(FieldByName('TP_CODIGO').AsString) then
          begin
               FieldByName('TP_CODIGO').FocusControl;
                DataBaseError( 'C�digo No Puede Quedar Vac�o');
          end;
     end;
end;

procedure TdmRecursos.cdsPorcPensionesNewRecord(DataSet: TDataSet);
begin
     with cdsPorcPensiones do
     begin
          FieldByName('PP_PORCENT').AsFloat := 0.0;
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('PS_ORDEN').AsInteger := cdsHisPensionesAlimenticias.FieldByName('PS_ORDEN').AsInteger;
     end;
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasBeforeDelete(DataSet: TDataSet);
begin
     FOrdenABorrar := cdsHisPensionesAlimenticias.FieldByName('PS_ORDEN').AsInteger;
     with cdsPorcPensiones do
     begin
          first;
          while not eof do
          		delete;
     end;
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasAfterDelete(DataSet: TDataSet);
begin
     with cdsHisPensionesAlimenticias do
     begin
          First;
          while not Eof do
          begin
               if FOrdenABorrar < FieldByName('PS_ORDEN').AsInteger then
               begin
                    Edit;
                    FieldByName('PS_ORDEN').AsInteger := FieldByName('PS_ORDEN').AsInteger - 1;
                    Post;
               end;
               Next;
          end;
          Enviar;
     end;
end;

procedure TdmRecursos.cdsHisPensionesAlimenticiasBeforeInsert(DataSet: TDataSet);
begin
     with cdsHisPensionesAlimenticias do
     begin
          DisableControls;
          try
             IndexFieldNames := 'PS_ORDEN';
             Last;
             FMaxOrdenPensiones := FieldByName( 'PS_ORDEN' ).AsInteger;
          finally
                 EnableControls;
          end;
     end;

end;

{Expediente SGM }
procedure TdmRecursos.cdsHisSGMAlAdquirirDatos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCliente do
          cdsHisSGM.Data := ServerRecursos.GetSegurosGastosMedicos( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisSGMAlCrearCampos(Sender: TObject);
begin
     cdsEmpParientes.Conectar;
     dmCatalogos.cdsSegGastosMed.Conectar;
     with cdsHisSGM do
     begin
          MaskFecha('EP_FEC_INI');
          MaskFecha('EP_FEC_FIN');
          MaskPesos('EP_CFIJO');
          MaskPesos('EP_CPOLIZA');
          MaskPesos('EP_CTITULA');
          MaskPesos('EP_CEMPRES');
          MaskFecha('EP_CAN_FEC');
          MaskPesos('EP_CAN_MON');
          MaskFecha('EP_FEC_REG');
          CreateSimpleLookup(dmCatalogos.cdsSegGastosMed ,'PM_DESCRIP','PM_CODIGO');
          //CreateLookup(dmCatalogos.cdsSegGastosMed ,'POLIZA','PM_CODIGO','PM_CODIGO','PM_NUMERO');
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          ListaFija('EP_TIPO',lfTipoEmpSGM);
          ListaFija('EP_STATUS',lfStatusSGM);
          FieldByName('PA_RELACIO').OnGetText := PA_FOLIO_OnGetText;
          FieldByName('PA_RELACIO').Alignment := taLeftJustify; 
     end;
end;

procedure TdmRecursos.PA_FOLIO_OnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if cdsHisSGM.FieldByName('EP_ASEGURA').AsInteger = Ord(taPariente) then
            Text := ObtieneElemento(lfTipoPariente,Sender.AsInteger )
          else
              Text := VACIO;
     end;
end;

procedure TdmRecursos.cdsHisSGMNewRecord(DataSet: TDataSet);
begin
     with cdsHisSGM do
     begin
          with dmCliente do
          begin
               FieldByName('CB_CODIGO').AsInteger := Empleado;
               FieldByName('EP_TIPO').AsInteger := Ord(tesTitular);
               FieldByName('EP_ASEGURA').AsInteger := Ord(taEmpleado);
               FieldByName('EP_CANCELA').AsString := K_GLOBAL_NO;
               FieldByName('EP_FEC_INI').AsDateTime := FechaDefault;
               FieldByName('EP_FEC_FIN').AsDateTime := FechaDefault + 365;
               FieldByName('EP_CAN_FEC').AsDateTime := NullDateTime;
               FieldByName('EP_FEC_REG').AsDateTime := FechaDefault;
               FieldByName('US_CODIGO').AsInteger := Usuario;
               FieldByName('EP_STATUS').AsInteger := Ord(ssActiva);
               FieldByName('EP_ORDEN').AsInteger := 1;
          end;
     end;
end;

procedure TdmRecursos.cdsHisSGMAlEnviarDatos(Sender: TObject);
var
   ErrorCount,Orden: Integer;
   bEditando:Boolean;
   Codigo,Referencia:string;
begin
     ErrorCount := 0;
     with cdsHisSGM do
     begin
          bEditando := State = dsEdit;
          Codigo := FieldByName('PM_CODIGO').AsString;
          Referencia := FieldByName('PV_REFEREN').AsString;
          Orden := FieldByName('EP_ORDEN').AsInteger;
          PostData;
          if ( Changecount > 0 )then
          begin
               DisableControls;
               try
                  Reconcile( ServerRecursos.GrabaSegurosGastosMedicos(dmCliente.Empresa, Delta, ErrorCount ) );
               finally
                      if not bEditando then
                      begin
                           Refrescar;
                           Locate('PM_CODIGO;PV_REFEREN;EP_ORDEN',VarArrayOf([Codigo,Referencia,Orden]),[]);
                      end;
                      EnableControls;
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsHisSGMAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisSeguroGastosMedicos_DevEx, TEditHisSeguroGastosMedicos_DevEx );         
end;


procedure TdmRecursos.cdsHisSGMAfterDelete(DataSet: TDataSet);
begin
     cdsHisSGM.Enviar;
end;

procedure TdmRecursos.cdsHisSGMBeforePost(DataSet: TDataSet);
begin
     with cdsHisSGM do
     begin
          if StrVacio( FieldByName('PM_CODIGO').AsString ) then
          begin
               FieldByName('PM_CODIGO').FocusControl;
               DataBaseError('C�digo de P�liza no puede quedar Vac�o');
          end;
          if StrVacio( FieldByName('PV_REFEREN').AsString ) then
          begin
               FieldByName('PV_REFEREN').FocusControl;
               DataBaseError('Referencia de P�liza no puede quedar Vac�o');
          end;
          if (FieldByName('EP_TIPO').AsInteger = Ord(tesDependiente)) and( ( FieldByName('PA_RELACIO').AsInteger = 0 ) and ( FieldByName('PA_FOLIO').AsInteger = 0 ) )then
          begin
               DataBaseError('Seleccionar Dependiente');
          end;
          if StrVacio( FieldByName('EP_CERTIFI').AsString ) then
          begin
               FieldByName('EP_CERTIFI').FocusControl;
               DataBaseError('Certificado de P�liza no puede quedar Vac�o');
          end;
          if (FieldByName('EP_FEC_INI').AsDateTime >= FieldByName('EP_FEC_FIN').AsDateTime )then
          begin
               FieldByName('EP_FEC_FIN').FocusControl;
               DataBaseError('Fecha Final debe ser Mayor a la Fecha Inicial');
          end
          else
          begin
               //Revisar
               dmCatalogos.cdsVigenciasSGM.Locate('PM_CODIGO;PV_REFEREN',VarArrayOf([FieldByName('PM_CODIGO').AsString,FieldByName('PV_REFEREN').AsString]),[]);
               if ( FieldByName('EP_FEC_INI').AsDateTime > dmCatalogos.cdsVigenciasSGM.FieldByName('PV_FEC_FIN').AsDateTime ) OR
                  ( FieldByName('EP_FEC_FIN').AsDateTime < dmCatalogos.cdsVigenciasSGM.FieldByName('PV_FEC_INI').AsDateTime ) OR
                  ( FieldByName('EP_FEC_INI').AsDateTime < dmCatalogos.cdsVigenciasSGM.FieldByName('PV_FEC_INI').AsDateTime ) OR
                  ( FieldByName('EP_FEC_FIN').AsDateTime > dmCatalogos.cdsVigenciasSGM.FieldByName('PV_FEC_FIN').AsDateTime ) then
               begin
                    DataBaseError('Las Fechas de Vigencia estan fuera de Rango de la P�liza Seleccionada');
               end;
          end;

          if ( zStrToBool(FieldByName('EP_CANCELA').AsString)) and ( FieldByName('EP_CAN_FEC').AsDateTime = NullDateTime )then
          begin
               FieldByName('EP_CAN_FEC').FocusControl;
               DataBaseError('Fecha de Cancelaci�n es requerida');
          end;

          if ( zStrToBool(FieldByName('EP_CANCELA').AsString)) and ( StrVacio( FieldByName('EP_CAN_MOT').AsString ) )then
          begin
               FieldByName('EP_CAN_MOT').FocusControl;
               DataBaseError('Motivo de Cancelaci�n No Puede Quedar Vac�o');
          end;

          FieldByName('EP_FEC_REG').AsDateTime := dmCliente.FechaDefault;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;

     end;
end;

procedure TdmRecursos.cdsEmpPlazaAlAdquirirDatos(Sender: TObject);
begin
     cdsEmpPlaza.Data := ServerRecursos.GetPlazas(dmCliente.Empresa);
end;

function TdmRecursos.GetMaxOrdenPolizas(const Poliza,Referencia:string):Integer;
begin
     with cdsHisSGM do
     begin
          Result := ServerRecursos.GetMaxOrden(dmCliente.Empresa,Poliza,Referencia,dmCliente.Empleado);
     end;
end;

procedure TdmRecursos.cdsHisSGMAfterEdit(DataSet: TDataSet);
begin
     cdsHisSGM.Edit;
end;

procedure TdmRecursos.validacionTipoNominaChange(Sender: TField);
begin
    if Sender.AsString <> Sender.OldValue then
     begin
          with Sender do
          begin
               dmCliente.cdsTPeriodos.Locate('TP_TIPO', Sender.AsString, []);
               if not ListaIntersectaConfidencialidad(
                  dmCliente.cdsTPeriodos.FieldByName (dmCliente.cdsTPeriodos.LookupConfidenField).AsString,
                  dmCliente.Confidencialidad) then
                  begin
                       FocusControl;
                       if dmCliente.GetDatosPeriodoActivo.Tipo = K_PERIODO_VACIO then
                          DataBaseError( 'El tipo de n�mina es invalido.' )
                       else
                           DataBaseError( Format ('No es posible asignar el tipo de n�mina: ''%s'' ',
                             [ ObtieneElemento(lfTipoPeriodo, Sender.AsInteger) ]));
                  end;
          end;
     end;
end;

procedure TdmRecursos.cdsHisCompetenAlAdquirirDatos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;

     with dmCliente do
          cdsHisCompeten.Data := ServerRecursos.GetCompetenciasEmp( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisCompetenAlCrearCampos(Sender: TObject);
begin
     dmCatalogos.cdsCompetencias.Conectar;
     with cdsHisCompeten do
     begin
          MaskFecha('ECC_FECHA');
          CreateSimpleLookup(dmCatalogos.cdsCompetencias ,'TB_ELEMENT','CC_CODIGO');
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmRecursos.cdsHisCompetenAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisCompetencia_DevEx, TEditHisCompetencia_DevEx )
end;

procedure TdmRecursos.cdsHisCompetenNewRecord(DataSet: TDataSet);
begin
     with cdsHisCompeten do
     begin
          with dmCliente do
          begin
               FieldByName('CB_CODIGO').AsInteger := Empleado;
               FieldByName('ECC_FECHA').AsDateTime := FechaDefault;
               FieldByName('US_CODIGO').AsInteger := Usuario;
               FieldByName('ECC_COMENT').AsString := VACIO;
          end;
     end;
end;

procedure TdmRecursos.cdsHisCompetenBeforePost(DataSet: TDataSet);
begin
     with cdsHisCompeten do
     begin
          if StrVacio( FieldByName('CC_CODIGO').AsString ) then
          begin
               DataBaseError('Competencia No Puede Quedar Vac�o');
          end;

           if FieldByName('NC_NIVEL').AsInteger <= 0  then
          begin
               DataBaseError('Nivel No Puede Quedar Vac�o');
          end;

          FieldByName('RC_FEC_INI').AsDateTime := dmCatalogos.GetUltimaRevisionCompetencia; 
     end;
end;

procedure TdmRecursos.cdsHisCompetenAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsHisCompeten do
     begin
          PostData;
          if ( Changecount > 0 )then
          begin
               DisableControls;
               try
                  if Reconcile( ServerRecursos.GrabaCompetenciasEmp(dmCliente.Empresa, Delta, ErrorCount ) )then
                  begin
                       cdsPlanCapacitacion.Refrescar;
                  end;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TdmRecursos.cdsHisCompetenAfterDelete(DataSet: TDataSet);
begin
     cdsHisCompeten.Enviar;
end;

procedure TdmRecursos.cdsHisCompEvaluaAlAdquirirDatos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCliente do
          cdsHisCompEvalua.Data := ServerRecursos.GetEvaluaCompEmp( Empresa, Empleado );
end;

procedure TdmRecursos.cdsHisCompEvaluaAlCrearCampos(Sender: TObject);
begin
     dmCatalogos.cdsCompetencias.Conectar;
     with cdsHisCompEvalua do
     begin
          MaskFecha('EC_FECHA');
          CreateSimpleLookup(dmCatalogos.cdsCompetencias ,'TB_ELEMENT','CC_CODIGO');
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmRecursos.cdsHisCompEvaluaAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisEvaluaComp_DevEx, TEditHisEvaluaComp_DevEx )
end;

procedure TdmRecursos.cdsHisCompEvaluaNewRecord(DataSet: TDataSet);
begin
      with cdsHisCompEvalua do
     begin
          with dmCliente do
          begin
               FieldByName('CB_CODIGO').AsInteger := Empleado;
               FieldByName('EC_FECHA').AsDateTime := Now;
               FieldByName('US_CODIGO').AsInteger := Usuario;
               FieldByName('EC_COMENT').AsString := VACIO;
          end;
     end;
end;

procedure TdmRecursos.cdsHisCompEvaluaBeforePost(DataSet: TDataSet);
begin
     with cdsHisCompEvalua do
     begin
          if StrVacio( FieldByName('CC_CODIGO').AsString ) then
          begin
               DataBaseError('Competencia No Puede Quedar Vac�o');
          end;

          if FieldByName('NC_NIVEL').AsInteger <= 0  then
          begin
               DataBaseError('Nivel No Puede Quedar Vac�o');
          end;

          FieldByName('RC_FEC_INI').AsDateTime := dmCatalogos.GetUltimaRevisionCompetencia;
     end;
end;

procedure TdmRecursos.cdsHisCompEvaluaAlEnviarDatos(Sender: TObject);
var
   ErrorCount,iNivel: Integer;
   sCodigo:string;
begin
     ErrorCount := 0;
     with cdsHisCompEvalua do
     begin
          PostData;
          sCodigo := FieldByName('CC_CODIGO').AsString;
          iNivel  := FieldByName('NC_NIVEL').AsInteger;
          if ( Changecount > 0 )then
          begin
                  if Reconcile( ServerRecursos.GrabaEvaluaCompEmp(dmCliente.Empresa, Delta, ErrorCount ) )then
                  begin
                       try
                          DisableControls;
                          Refrescar;
                          Locate('CC_CODIGO;NC_NIVEL',VarArrayOf([sCodigo,iNivel]),[]);
                          cdsPlanCapacitacion.SetDataChange;
                          cdsMatrizHabilidades.SetDataChange; 
                       finally
                              cdsHisCompEvalua.EnableControls;
                       end;
                  end;
          end;
     end;
end;

procedure TdmRecursos.cdsHisCompEvaluaAfterDelete(DataSet: TDataSet);
begin
     cdsHisCompEvalua.Enviar; 
end;

procedure TdmRecursos.cdsPlanCapacitacionAlAdquirirDatos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCliente do
          cdsPlanCapacitacion.Data := ServerRecursos.GetPlanCapacitacion( Empresa, Empleado );
end;

procedure TdmRecursos.cdsPlanCapacitacionAlCrearCampos(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          cdsCatPerfiles.Conectar;
          cdsMatPerfilComps.Conectar;
     end;
     with cdsPlanCapacitacion do
     begin
          MaskFecha('EC_FECHA');
          CreateSimpleLookup(dmCatalogos.cdsCompetencias ,'TB_ELEMENT','CC_CODIGO');
          CreateSimpleLookup(dmCatalogos.cdsCatPerfiles  ,'CP_ELEMENT','CP_CODIGO');

          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmRecursos.cdsMatrizCursosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsMatrizCursos.Data := ServerRecursos.GetMatrizCursos( Empresa, FiltroMatCursos.VarValues );
     end;
end;

procedure TdmRecursos.cdsMatrizCursosAlCrearCampos(Sender: TObject);
begin
     with dmTablas do
     begin
          cdsTipoCursos.Conectar;
          cdsClasifiCurso.Conectar;
     end;
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsPuestos.Conectar;
     end;
end;

procedure TdmRecursos.cdsMatrizHabilidadesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsMatrizHabilidades.Data := ServerRecursos.GetMatrizHabilidades( Empresa, FiltroMatHabilidades.VarValues );
end;

procedure TdmRecursos.cdsMatrizHabilidadesAlCrearCampos(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          cdsCatPerfiles.Conectar;
          with cdsMatrizHabilidades do
          begin
               CreateSimpleLookup(cdsCompetencias ,'CC_ELEMENT','CC_CODIGO');
               CreateSimpleLookup(cdsCatPerfiles ,'CP_ELEMENT','CP_CODIGO');
          end;
     end;
end;

procedure TdmRecursos.cdsHisCompEvaluaReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError:string;

   function MensajeCompe:Boolean;
   begin
        Result :=  Pos( 'C_REV_COMP',E.Message) > 0;
   end;

begin
     if ( dmCatalogos.GetUltimaRevisionCompetencia = NullDateTime  ) and ( UpdateKind = ukInsert ) and ( MensajeCompe ) then
     begin
          with DataSet do
          begin
               sError := Format('No se Puede Agregar Competencia %s ; No Tiene Capturada al menos una Revisi�n ',[ FieldByName('CC_CODIGO').AsString ]);
          end;
          ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
          case UpdateKind of
               ukInsert: Action := raCancel;
          else
              Action := raAbort;
          end;
     end
     else
         Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);

end;
procedure TdmRecursos.cdsEvaluacionSelNewRecord(DataSet: TDataSet);
begin
     if not FAgregandoMatriz then
     begin
          DataSet.Cancel;
          DataSet.Edit;
     end;
end;
procedure TdmRecursos.cdsHistorialFonacotAlAdquirirDatos(Sender: TObject);
begin
     FSumaPagosFonacot := 0;
     FSumaPagosRetencion := 0;
     with cdsHisPrestamos do
     begin
          if State <> dsInsert then
             cdsHistorialFonacot.Data := ServerRecursos.GetHisHistorialFonacot( dmCliente.Empresa, dmCliente.Empleado, FieldByName('PR_REFEREN').AsString, FSumaPagosFonacot, FSumaPagosRetencion );
     end;
end;

procedure TdmRecursos.cdsHistorialFonacotAlCrearCampos(Sender: TObject);
begin
     with cdsHistorialFonacot do
     begin
          FieldByName( 'PF_MES' ).OnGetText := PF_MESGetText;
          MaskPesos('PF_PAGO');
          MaskPesos('PF_NOMINA');
     end;
end;

procedure TdmRecursos.PF_MESGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
    Text := ObtieneElemento( lfMeses, Sender.AsInteger - 1 )
end;

procedure TdmRecursos.cdsEvaluacionSelAlCrearCampos(Sender: TObject);
begin

end;

/// Importar Imagenes
function TdmRecursos.CargaGridImagenesImportar( Parametros: TZetaParams; const sFileSpec: String ; lConservarImagen : Boolean): Boolean;
var
   Archivo: TSearchRec;
begin
     Result := False;
      try
         if cdsImagenes.FieldCount = 0 then
         begin
              ImagenAlCrearCampos(Archivo.Name, Parametros.ParamByName('Tipo').AsString);
         end;
         cdsImagenes.EmptyDataSet;
         if ( FindFirst( sFileSpec, faAnyFile, Archivo ) = 0 ) then
         begin
              CargaGridImagen(Archivo.Name,  Parametros.ParamByName('Tipo').AsString, Parametros.ParamByName('Directorio').AsString, Parametros.ParamByName('Observaciones').AsString);
              while ( FindNext( Archivo ) = 0 ) do
              begin
                   CargaGridImagen(Archivo.Name, Parametros.ParamByName('Tipo').AsString, Parametros.ParamByName('Directorio').AsString , Parametros.ParamByName('Observaciones').AsString);
              end;
              FindClose( Archivo );
         end;
         Result := True;
      except
            on Error: Exception do
            begin
            end;
      end;
end;

function TdmRecursos.ImagenAlCrearCampos(const sFileName: String; sTipo: String): Boolean;
begin
     with cdsImagenes do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';

          AddStringField('CB_CODIGO', 25);
          AddStringField('colNombre', 50);
          AddStringField('IM_TIPO', 25);
          AddStringField('colArchivo', 30);
          AddBlobField('IM_BLOB', 0);
          AddStringField('IM_OBSERVA', 30);
          AddStringField('colExt', 30);
          CreateTempDataset;
          IndexFieldNames := 'CB_CODIGO';
          Open;
     end;
     Result :=  TRUE;
end;

function TdmRecursos.CargaGridImagen(const sFileName: String; sTipo, sRuta, sObserva: String): Boolean;
var
   iEmpleado: Integer;
   sFile : String;
begin
    iEmpleado := StrToIntDef( ChangeFileExt( ExtractFileName( sFileName ), '' ), 0 );
    if iEmpleado > 0 then
    begin
         cdsImagenes.Insert;
         cdsImagenes.FieldByName('CB_CODIGO').AsString := IntToStr(iEmpleado);
         cdsImagenes.FieldByName('IM_TIPO').AsString := sTipo;
         cdsImagenes.FieldByName('IM_OBSERVA').AsString := sObserva;
         cdsImagenes.FieldByName('colArchivo').AsString := sFileName;
         cdsImagenes.FieldByName('colExt').AsString := ExtractFileExt(sFileName);
         cdsImagenes.Post;

         try
            cdsGetEmpleadosImagenes.Conectar;

            with cdsGetEmpleadosImagenes do
            begin
                  First;
                  while not Eof do
                  begin
                      if (FieldByName('CB_CODIGO').AsString = cdsImagenes.FieldByName('CB_CODIGO').AsString) then
                      begin
                           cdsImagenes.Edit;
                           cdsImagenes.FieldByName('colNombre').AsString := FieldByName('PRETTYNAME').AsString;
                           sFile  := IncludeTrailingPathDelimiter(sRuta) + FieldByName('CB_CODIGO').AsString + ExtractFileExt(sFileName);
                           if FileExists(sFile) then
                           begin
                                TBlobField(cdsImagenes.FieldByName('IM_BLOB')).LoadFromFile(sFile);
                           end;
                           cdsImagenes.Post;
                      end;
                      Next;
                 end;
            end;

            with cdsImagenes do
            begin
                First;
                while not Eof do
                begin
                     if FieldByName('colNombre').AsString = VACIO then
                        Delete
                     else
                         Next;
                end;
            end;

         finally
         end;
    end;
    Result :=  TRUE;
end;

procedure TdmRecursos.cdsGetEmpleadosImagenesAlAdquirirDatos(Sender: TObject);
begin
     cdsGetEmpleadosImagenes.Data := ServerRecursos.GetEmpleadosImagenes( dmCliente.Empresa);
end;


procedure TdmRecursos.cdsInfoCandidatoAlCrearCampos(Sender: TObject);
begin
      with cdsInfoCandidato do
      begin
      maskfecha('SO_FEC_NAC');
      end;
end;

{$IFNDEF SUPERVISORES}
procedure TdmRecursos.CambioMasivoTurnoGetLista(Parametros: TZetaParams);
begin
     cdsDataset.Data := ServerRecursos.CambioMasivoTurnoGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmRecursos.CambioMasivoTurno(Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
      if lVerificacion then
            cdsDataSet.MergeChangeLog
      else
            cdsDataSet := TZetaClientDataSet.Create(Self);

      Result := dmProcesos.CambioMasivoTurnos( Parametros, cdsDataSet, lVerificacion );
end;

{$ENDIF}

{$ifdef DENSO}
function TdmRecursos.GetSugiereNumeroEmpleado( DataSet: TDataSet ) : Integer;
var
   Parametros : TZetaParams;
begin
          with TZetaClientDataSet( DataSet ) do
          begin
                    Parametros := TZetaParams.Create;
                    try
                       Parametros.AddInteger( 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger );
                       Parametros.AddString(  'CB_PATRON', FieldByName( 'CB_PATRON' ).AsString );
                       Parametros.AddInteger( 'CB_NOMINA', FieldByName('CB_NOMINA').AsInteger );
                       Parametros.AddString(  'CB_PUESTO', FieldByName('CB_PUESTO').AsString );
                       Parametros.AddString( 'CB_CLASIFI', FieldByName('CB_CLASIFI').AsString);
                       Parametros.AddString( 'CB_TURNO', FieldByName('CB_TURNO').AsString);
                       Parametros.AddInteger( 'CB_PLAZA', FieldByName('CB_PLAZA').AsInteger );
                       Parametros.AddString(  'CB_NIVEL1', FieldByName( 'CB_NIVEL1' ).AsString );
                       Parametros.AddString(  'CB_NIVEL2', FieldByName( 'CB_NIVEL2' ).AsString );
                       Parametros.AddString(  'CB_NIVEL3', FieldByName( 'CB_NIVEL3' ).AsString );
                       Parametros.AddString(  'CB_NIVEL4', FieldByName( 'CB_NIVEL4' ).AsString );
                       Parametros.AddString(  'CB_NIVEL5', FieldByName( 'CB_NIVEL5' ).AsString );
                       Parametros.AddString(  'CB_NIVEL6', FieldByName( 'CB_NIVEL6' ).AsString );
                       Parametros.AddString(  'CB_NIVEL7', FieldByName( 'CB_NIVEL7' ).AsString );
                       Parametros.AddString(  'CB_NIVEL8', FieldByName( 'CB_NIVEL8' ).AsString );
                       Parametros.AddString(  'CB_NIVEL9', FieldByName( 'CB_NIVEL9' ).AsString );

                       Result := ServerRecursos.GetSugiereEmpleado( dmCliente.Empresa, Parametros.VarValues );
                    finally
                       FreeAndNil( Parametros );
                    end;
          end;
end;
{$endif}

procedure TdmRecursos.SetDiasPrimasVacaciones( oFechaInicio: TZetaDBFecha; rPrima: TPesos );
var
   aFecha, bFecha : String;
begin
     aFecha := Copy(oFechaInicio.ValorAsText, 0, 5);
     bFecha := Copy(FormatDateTime('dd/mm/yy', dmRecursos.cdsEmpVacacion.FieldByName('CB_FEC_ANT').AsDateTime), 0, 5);
     with dmRecursos.cdsHisVacacion do
     begin
          if Global.GetGlobalBooleano( K_GLOBAL_PAGO_PRIMA_VACA_ANIV ) then
          begin
               if (aFecha = bFecha) then
                  FieldByName('VA_P_PRIMA').AsString := FormatFloat( '#,0.00', rPrima )
               else
                   FieldByName('VA_P_PRIMA').AsString := FormatFloat( '#,0.00', 0 );
          end;
     end;
end;

end.


