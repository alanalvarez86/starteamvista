unit dBorradorSUA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, FileCtrl, Data.Win.ADODB,
  ZetaCommonLists;

type
  TdmBorradorSUA = class(TDataModule)
    ADOConn: TADOConnection;
    qryMaster: TADOQuery;
    QryConsulta: TADOQuery;
    QryAdicional: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FPatron: String;
    FEmpleados: Boolean;
    FBorrarMovimientos: eBorrarMovimSUA;
    FPath: String;
    FYear: Integer;
    FMes: Integer;
    FUnPatron: Boolean;
    function ConectarBaseDeDatos: Boolean;
    function Ejecuta: Integer;
    procedure EmpiezaTransaccion;
    procedure RollBackTransaccion;
    procedure TerminaTransaccion(lCommit: Boolean);
    procedure BorraEmpleados;
    procedure BorraMovimientos;
    procedure PreparaQuery(oCursor: TADOQuery; const sScript: String);
    procedure Prepara(const sScript: String);
    procedure ParamAsDate(oQuery: TADOQuery; const sField: string; const dValue: TDate);
    procedure ParamAsString(oQuery: TADOQuery; const sField: string; const sValue: String);
  public
    { Public declarations }
    property Patron: String read FPatron write FPatron;
    property Empleados: Boolean read FEmpleados write FEmpleados;
    property BorrarMovimientos: eBorrarMovimSUA read FBorrarMovimientos write FBorrarMovimientos;
    property Path: String read FPath write FPath;
    property Year: Integer read FYear write FYear;
    property Mes: Integer read FMes write FMes;
    procedure Borrar;
  end;

var
  dmBorradorSUA: TdmBorradorSUA;

implementation

{$R *.DFM}

uses ZetaCommonClasses,
     ZetaCommonTools;

const
     K_SUA_ACCESS_FILE = 'SUA.MDB';
     {$ifndef ANTES}
     K_SUA_ACCESS_CODE = 'S5@N52V49'; //Apartir del complemento 3.2.5 cambio la clave de 'NUEVOSUA' a 'S5@N52V49'
     {$else}
     K_SUA_ACCESS_CODE = 'NUEVOSUA';
     {$endif}
     K_FILTRO_PATRON = '( REG_PATR = %s )';
     K_FILTRO_PATRON2 = '( REG_PAT = %s )';

     Q_BORRA_EMPLEADOS = 0;
     Q_BORRA_MOVIMIENTOS = 1;
     Q_REVISA_INCAPACI = 2;
     Q_DELETE_INCAPACI = 3;
     Q_BORRA_AFILIATORIOS = 4;
     Q_DELETE_INFOINCAPACI = 5;

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_BORRA_EMPLEADOS: Result := 'delete from ASEGURA%s';
          Q_BORRA_AFILIATORIOS: Result := 'delete from AFILIACION%s';
          Q_BORRA_MOVIMIENTOS: Result := 'delete from MOVTOS%s';
          Q_REVISA_INCAPACI: Result := 'select NUM_AFIL, FOL_INC from MOVTOS%s';
          Q_DELETE_INCAPACI: Result := 'delete from MOVTOS%s';
          Q_DELETE_INFOINCAPACI: Result := 'delete from INCAPACIDADES%s';
     end;
end;

{ TdmBorradorSUA }

procedure TdmBorradorSUA.DataModuleCreate(Sender: TObject);
begin
     FPath := ZetaCommonClasses.VACIO;
     FPatron := ZetaCommonClasses.VACIO;
end;

function TdmBorradorSUA.ConectarBaseDeDatos: Boolean;
var
   sArchivoBD, sConnString: String;
begin
     Result := False;
     if FileCtrl.DirectoryExists( FPath ) then
     begin
          sArchivoBD := ZetaCommonTools.VerificaDir( FPath ) + K_SUA_ACCESS_FILE;
          if FileExists( sArchivoBD ) then
          begin
               with ADOConn do
               begin
                    Connected := FALSE;
                    {$IFDEF WIN32}
                    sConnString := 'Driver={Microsoft Access Driver (*.mdb)};' +
                    'DBQ=%s;Persist Security Info=False;password=%s';
                    {$ELSE}
                    sConnString := 'Driver={Microsoft Access Driver (*.mdb, *.accdb)};' +
                    'DBQ=%s;Persist Security Info=False;password=%s';
                    {$ENDIF}
                    ADOConn.ConnectionString := Format( sConnString, [ sArchivoBD, K_SUA_ACCESS_CODE ] );
                    Connected := True;
                    Result := Connected;
               end;
          end
          else
          begin
               DataBaseError( 'El Archivo De Datos De SUA No Existe' );
          end;
     end
     else
     begin
          DataBaseError( Format( 'El Directorio %s No Existe', [ FPath ] ) );
     end;
end;

procedure TdmBorradorSUA.EmpiezaTransaccion;
begin
     ADOConn.BeginTrans;
end;

procedure TdmBorradorSUA.TerminaTransaccion(lCommit: Boolean);
begin
     ADOConn.CommitTrans;
end;

procedure TdmBorradorSUA.RollBackTransaccion;
begin
     ADOConn.RollbackTrans;
end;

procedure TdmBorradorSUA.PreparaQuery( oCursor: TADOQuery; const sScript: String );
begin
     with oCursor do
     begin
          Active := FALSE;
          SQL.Text := sScript;
          Prepared := TRUE;
     end;
end;

procedure TdmBorradorSUA.Prepara( const sScript: String );
begin
     PreparaQuery( qryMaster, sScript );
end;

function TdmBorradorSUA.Ejecuta: Integer;
begin
     EmpiezaTransaccion;
     try
        with qryMaster do
        begin
             ExecSQL;
             Result := RowsAffected;
        end;
        TerminaTransaccion(True);
     except
           RollBackTransaccion;
           raise;
     end;
end;

procedure TdmBorradorSUA.BorraEmpleados;
var
   sFiltro: String;
begin
     sFiltro:= ZetaCommonClasses.VACIO;
     if FUnPatron then
        sFiltro := ' where ' + Format( K_FILTRO_PATRON, [ EntreComillas( FPatron ) ] );

     // Borra Empleados
     Prepara( Format( GetSQLScript( Q_BORRA_EMPLEADOS ), [ sFiltro ] ) );
     Ejecuta;
     // Borra Datos Afiliatorios
     Prepara( Format( GetSQLScript( Q_BORRA_AFILIATORIOS ), [ sFiltro ] ) );
     Ejecuta;

     // Borra movimientos creados por SUA y agregados por Tress - BORRAR TODO
     Prepara( Format( GetSQLScript( Q_BORRA_MOVIMIENTOS ), [ sFiltro ] ) );
     Ejecuta;

     if FUnPatron then
        sFiltro := ' where ' + Format( K_FILTRO_PATRON2, [ EntreComillas( FPatron ) ] );
     // Borra informacion de Incapacidades agregados por Tress - BORRAR TODO
     Prepara( Format( GetSQLScript( Q_DELETE_INFOINCAPACI ), [ sFiltro ] ) );
     Ejecuta;

end;

procedure TdmBorradorSUA.BorraMovimientos;
const
     K_FILTRO_TIPO_MOVTOS  = '( ( ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) ) or' +
                             '  ( ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) or ( TIP_MOVS = %s ) ) )';
     K_FILTRO_FECHA_MOVTOS = '( FEC_INIC between :FECHAINI and :FECHAFIN )';
     K_FILTRO_NO_INCAPACI = '( TIP_MOVS <> %s )';
     K_FILTRO_INCAPACI = '( TIP_MOVS = %s )';
     K_FILTRO_AFIL_INCAPACI = '( ( NUM_AFIL = :NUM_AFIL ) and ( FOL_INC = :FOL_INC ) )';
     K_FILTRO_AFIL_INCAPACI2 = '( ( NUM_AFI = :NUM_AFI ) and ( FOL_INC = :FOL_INC ) )';
var
   sFiltro, sFiltroPatron, sFiltroPatron2, sFiltroIncap: String;
   dFechaIni, dFechaFin: TDate;
   lBorrarSoloElMes: Boolean;

   procedure BorraIncapacidades;
   begin
        // Consulta de Incapacidades
        sFiltroIncap := ConcatFiltros( sFiltroIncap, Format( K_FILTRO_INCAPACI, [ EntreComillas( K_TIPO_SUA_INCAPACIDAD ) ] ) );
        if strLleno( sFiltroIncap ) then
           sFiltroIncap := ' where ' + sFiltroIncap;
        PreparaQuery( QryConsulta, Format( GetSQLScript( Q_REVISA_INCAPACI ), [ sFiltroIncap ] ) );

        // Borrar Incapacidades
        sFiltro := sFiltroPatron;
        sFiltro := ConcatFiltros( sFiltro, K_FILTRO_AFIL_INCAPACI );
        if strLleno( sFiltro ) then
           sFiltro := ' where ' + sFiltro;
        Prepara( Format( GetSQLScript( Q_BORRA_MOVIMIENTOS ), [ sFiltro ] ) );
        // Borrar Informaci�n de Incapacidades
        sFiltro := sFiltroPatron2;
        sFiltro := ConcatFiltros( sFiltro, K_FILTRO_AFIL_INCAPACI2 );
        if strLleno( sFiltro ) then
           sFiltro := ' where ' + sFiltro;
        PreparaQuery( QryAdicional, Format( GetSQLScript( Q_DELETE_INFOINCAPACI ), [ sFiltro ] ) );

        EmpiezaTransaccion;
        try
           ParamAsDate( QryConsulta, 'FECHAINI', dFechaIni );
           ParamAsDate( QryConsulta, 'FECHAFIN', dFechaFin );
           with QryConsulta do
           begin
                Active := TRUE;
                while ( not EOF ) do
                begin
                     // Borra incapacidad
                     ParamAsString( QryMaster, 'NUM_AFIL', FieldByName( 'NUM_AFIL' ).AsString );
                     ParamAsString( QryMaster, 'FOL_INC', FieldByName( 'FOL_INC' ).AsString );
                     QryMaster.ExecSQL;
                     // Borra Informaci�n de Incapacidad
                     ParamAsString( QryAdicional, 'NUM_AFI', FieldByName( 'NUM_AFIL' ).AsString );
                     ParamAsString( QryAdicional, 'FOL_INC', FieldByName( 'FOL_INC' ).AsString );
                     QryAdicional.ExecSQL;
                     Next;
                end;
                Active := FALSE;
           end;
           TerminaTransaccion(True);
        except
              RollBackTransaccion;
              raise;
        end;
   end;

begin
     sFiltro:= ZetaCommonClasses.VACIO;
     sFiltroPatron := ZetaCommonClasses.VACIO;
     sFiltroIncap  := ZetaCommonClasses.VACIO;
     dFechaIni := NullDateTime;
     dFechaFin := NullDateTime;

     if FunPatron then
     begin
          sFiltroPatron := Format( K_FILTRO_PATRON, [ EntreComillas( FPatron ) ] );
          sFiltroPatron2 := Format( K_FILTRO_PATRON2, [ EntreComillas( FPatron ) ] );
          sFiltro := sFiltroPatron;
     end;

     sFiltro := ConcatFiltros( sFiltro, Format( K_FILTRO_TIPO_MOVTOS, [ EntreComillas( K_TIPO_SUA_BAJA ),
                                                                        EntreComillas( K_TIPO_SUA_CAMBIO ),
                                                                        EntreComillas( K_TIPO_SUA_REINGRESO ),
                                                                        EntreComillas( K_TIPO_SUA_VOLUNTARIA ),
                                                                        EntreComillas( K_TIPO_SUA_AUSENTISMO ),
                                                                        EntreComillas( K_TIPO_SUA_INCAPACIDAD ),
                                                                        EntreComillas( K_TIPO_SUA_INI_CREDITO ),
                                                                        EntreComillas( K_TIPO_SUA_SUS_CREDITO ),
                                                                        EntreComillas( K_TIPO_SUA_REINICIO_CRE ),
                                                                        EntreComillas( K_TIPO_SUA_CAMBIO_TD ),
                                                                        EntreComillas( K_TIPO_SUA_CAMBIO_VD ),
                                                                        EntreComillas( K_TIPO_SUA_CAMBIO_NUM ) ] ) );
     
     lBorrarSoloelMes := ( BorrarMovimientos <> bsTodo );
     if lBorrarSoloElMes then    // Si no se borrar� todo el SUA se borra solo el mes a exportar
     begin
          dFechaIni := CodificaFecha( Year, Mes, 1 );
          dFechaFin := LastDayOfMonth( dFechaIni );
          sFiltro := ConcatFiltros( sFiltro, K_FILTRO_FECHA_MOVTOS );
          // Si borra solo lo del mes, las incapacidades se borran con otro m�todo ( Se usar� el mismo filtro )
          sFiltroIncap := sFiltro;
          sFiltro := ConcatFiltros( sFiltro, Format( K_FILTRO_NO_INCAPACI, [ EntreComillas( K_TIPO_SUA_INCAPACIDAD ) ] ) );
     end;

     if strLleno( sFiltro ) then
        sFiltro := ' where ' + sFiltro;

     Prepara( Format( GetSQLScript( Q_BORRA_MOVIMIENTOS ), [ sFiltro ] ) );

     if lBorrarSoloElMes then
     begin
          ParamAsDate( qryMaster, 'FECHAINI', dFechaIni );
          ParamAsDate( qryMaster, 'FECHAFIN', dFechaFin );
     end;

     Ejecuta;

     if lBorrarSoloElMes then
        BorraIncapacidades;
end;

procedure TdmBorradorSUA.Borrar;
begin
     if ConectarBaseDeDatos then
     begin
          FUnPatron := StrLleno( FPatron );

          if FEmpleados then
             BorraEmpleados                                 // Si borra empleados, borra todo
          else if ( BorrarMovimientos <> bsNinguno ) then   // Borrar movimientos, ya sea todos los creados por Tress o solo del mes
             BorraMovimientos;
     end;
end;

procedure TdmBorradorSUA.ParamAsDate( oQuery: TADOQuery; const sField: string; const dValue: TDate);
begin
     with oQuery.Parameters.ParamByName( sField ) do
     begin
          if ( DataType = ftUnknown ) then
             DataType := ftDate;
          Value := dValue;
     end;
end;

procedure TdmBorradorSUA.ParamAsString(oQuery: TADOQuery; const sField, sValue: String);
begin
     with oQuery.Parameters.ParamByName( sField ) do
     begin
          if ( DataType = ftUnknown ) then
             DataType := ftString;
          Value := sValue;
     end;
end;

end.
