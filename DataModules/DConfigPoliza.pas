unit DConfigPoliza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
  ZReportConst,
  ZReportTools,
  ZetaClientDataSet,
  ZetaServerDataSet,
  ZetaTipoEntidad;


type
  eGroupType = ( eGrupo1, eGrupo2, eGrupo3, eGrupo4, eGrupo5 );
  TGroupList = class;
  TGroupData = class( TObject )
  private
    { Private declarations }
    FLista: TGroupList;
    FNombre: String;
    FFormula: String;
    function GetEntidad: TipoEntidad;
  public
    { Public declarations }
    constructor Create( Lista: TGroupList );
    property Nombre: String read FNombre write FNombre;
    property Formula: String read FFormula write FFormula;
    property Entidad: TipoEntidad read GetEntidad;
  end;
  TGroupList = class( TObject )
  private
    { Private declarations }
    FItems: TList;
    function GetItem(Index: Integer): TGroupData;
    procedure Delete(const Index: Integer);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Items[ Index: Integer ]: TGroupData read GetItem;
    function AddGroup(const sNombre, sExpresion: String ): TGroupData;
    function Count: Integer;
    procedure Clear;
  end;

  TdmConfigPoliza = class(TDataModule)
    cdsTemporal: TZetaClientDataSet;
    cdsCtaConcepto: TServerDataSet;
    cdsGrupo1: TServerDataSet;
    cdsGrupo2: TServerDataSet;
    cdsGrupo3: TServerDataSet;
    cdsGrupo4: TServerDataSet;
    cdsGrupo5: TServerDataSet;
    cdsPoliza: TServerDataSet;
  private
    { Private declarations }
    FGrupos: TGroupList;
    FListaCodigo: array[ 0..K_MAXGRUPOS ] of TStrings;
    FListaCuenta: array[ 0..K_MAXGRUPOS ] of TStrings;
    FListaAsiento: TStrings;

    function GetCamporep : TZetaClientDataSet;
    function GetReporte : TZetaClientDataSet;
    function GetCuantosAsientos: Integer;
    function GetPolizaActiva: Integer;
    function GetPolizaNombre: String;
    function UltimoAsiento: Integer;
    procedure LimpiaListas;
    procedure BorraFormulas(Dataset: TClientDataset);
    procedure FillGrupos(Dataset: TClientDataset; Datos: OleVariant; const sCodigo, sDescripcion, sSubcuenta: String);
    procedure CreaDatasetGrupo(oDataSet: TServerDataSet);
    procedure CreateCtaConcepto;
    procedure CreatePoliza;
    property cdsCampoRep : TZetaClientDataSet read GetCamporep;
    property cdsReporte : TZetaClientDataSet read GetReporte;
  public
    { Public declarations }
    {Metodos para Configurar la Poliza Contable}
    property CuantosAsientos: Integer read GetCuantosAsientos;
    property PolizaActiva: Integer read GetPolizaActiva;
    property PolizaNombre: String read GetPolizaNombre;

    function ConectaConfiguraPoliza: Boolean;
    function GetDatasetGrupo(const eGrupo: eGroupType): TServerDataset;
    procedure DesconectaConfiguraPoliza;
    procedure LlenaConceptos( eTiposConcepto : TiposConcepto );
    procedure LlenaPolizaGrupos( Lista, oListGrupos: TStrings );
    procedure ExportaArchivo(Dataset: TClientDataSet; const sFile: String);
    procedure ExportaCtaConcepto(const sFile: String);
    procedure ImportaArchivo(Dataset: TClientDataSet; const sFile: String);
    procedure ImportaCtaConcepto(const sFile: String);
    procedure ExportaGrupo(const eGrupo: eGroupType; const sFile: String);
    procedure ImportaGrupo(const eGrupo: eGroupType; const sFile: String);
    procedure ImportaGrupoTress(const eGrupo: eGroupType);
    procedure BorraGruposTodos(const eGrupo: eGroupType);
    procedure BorraFormulasPoliza;
    procedure GenerarPoliza(const sTemplate: String);
    procedure AgregaAsientos(Callback: TNotifyEvent);
  end;

var
  dmConfigPoliza: TdmConfigPoliza;

implementation

uses FTressShell,
     DReportes,
     DCatalogos,
     DCliente,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;
const
     K_CARGO = 'C';
     K_CARGO_INT = 0;
     K_ABONO = 'A';
     K_ABONO_INT = 1;
     K_MASCARA_ASIENTO = '>L';
     K_FLD_CUENTA = 'CUENTA';
     K_FLD_CONCEPTO = 'CONCEPTO';
     K_FLD_GRUPO = 'GRUPO%D';
     K_FLD_GRUPO1 = 'GRUPO1';
     K_FLD_GRUPO2 = 'GRUPO2';
     K_FLD_GRUPO3 = 'GRUPO3';
     K_FLD_GRUPO4 = 'GRUPO4';
     K_FLD_GRUPO5 = 'GRUPO5';
     K_FLD_TIPO = 'TIPO';
     K_FLD_ASIENTO = 'ASIENTO';
     K_FLD_CODIGO = 'CODIGO';
     K_FLD_COMENTARIO = 'COMENTARIO';
     K_FLD_DESCRIPCION = 'DESCRIPCION';
     K_FLD_SUBCUENTA = 'SUBCUENTA';

{$R *.DFM}

{*****************************************************************************}
{************ Metodos para la Configuracion de la P�liza Contable ************}
{************************* Antes Utileria CreaPoliza *************************}

{ *********** TGroupData ************ }

constructor TGroupData.Create(Lista: TGroupList);
begin
     FLista := Lista;
end;

function TGroupData.GetEntidad: TipoEntidad;
const
     K_LEN_NIVEL = {$ifdef ACS}11{$else}8{$endif};
var
   sFormula: String;
begin
     { FFormulas almacena strings en formato VALOR=TABLA.CAMPO}
     sFormula := FFormula;
     { Elimina la parte VALOR= }
     sFormula := Copy( sFormula, Pos( '=', sFormula ) + 1, MAXINT );
     { Elimina la parte TABLA. }
     sFormula := Copy( sFormula, Pos( '.', sFormula ) + 1, MAXINT );
     if ( Copy( sFormula, 1, K_LEN_NIVEL ) = 'CB_NIVEL' ) then
     begin
          case StrToIntDef( Copy( sFormula, ( K_LEN_NIVEL + 1 ), ( Length( sFormula ) - K_LEN_NIVEL ) ), -1 ) of
               0: Result := enNivel0;
               1: Result := enNivel1;
               2: Result := enNivel2;
               3: Result := enNivel3;
               4: Result := enNivel4;
               5: Result := enNivel5;
               6: Result := enNivel6;
               7: Result := enNivel7;
               8: Result := enNivel8;
               9: Result := enNivel9;
{$ifdef ACS}
               10: Result := enNivel10;
               11: Result := enNivel11;
               12: Result := enNivel12;
{$endif}
          else
              Result := enNinguno;
          end;
     end
     else
         if ( sFormula = 'CB_PUESTO' ) then
            Result := enPuesto
         else
             if ( sFormula = 'CB_CLASIFI' ) then
                Result := enClasifi
             else
                 if ( sFormula = 'CB_TURNO' ) then
                    Result := enTurno
                 else
                     Result := enNinguno;
end;

{ ********* TGroupList ******** }

constructor TGroupList.Create;
begin
     FItems := TList.Create;
end;

destructor TGroupList.Destroy;
begin
     Clear;
     FreeAndNil( FItems );
     inherited Destroy;
end;

function TGroupList.GetItem(Index: Integer): TGroupData;
begin
     with FItems do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TGroupData( Items[ Index ] )
          else
              Result := nil;
     end;
end;

function TGroupList.AddGroup(const sNombre, sExpresion: String ): TGroupData;
begin
     Result := TGroupData.Create( Self );
     try
        FItems.Add( Result );
        with Result do
        begin
             Nombre := sNombre;
             Formula := sExpresion;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

procedure TGroupList.Clear;
var
   i: Integer;
begin
     with FItems do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TGroupList.Count: Integer;
begin
     Result := FItems.Count;
end;

procedure TGroupList.Delete(const Index: Integer);
begin
     Items[ Index ].Free;
     FItems.Delete( Index );
end;


function TdmConfigPoliza.GetCamporep : TZetaClientDataSet;
begin
     Result := dmReportes.cdsCamporep;
end;

function TdmConfigPoliza.GetReporte : TZetaClientDataSet;
begin
     Result := dmReportes.cdsEditReporte;
end;

procedure TdmConfigPoliza.CreaDatasetGrupo( oDataSet : TServerDataSet );
begin
     with oDataset do
     begin
          InitTempDataset;
          AddStringField( K_FLD_CODIGO, 6 ); { C�dio del Grupo }
          AddStringField( K_FLD_DESCRIPCION, 30 );  { Descripcion del Grupo }
          AddStringField( K_FLD_SUBCUENTA, 30 );  { SubCuenta }
          AddStringField( K_FLD_COMENTARIO, 30 );  { Comentario }
          CreateTempDataset;
     end;
end;

procedure TdmConfigPoliza.DesconectaConfiguraPoliza;
 var i: integer;
begin
     FreeAndNil( FGrupos );
     FreeAndNil( FListaAsiento );
     for i := K_MAXGRUPOS downto 0 do
     begin
          FListaCodigo[ i ].Free;
          FListaCuenta[ i ].Free;
     end;
end;

function TdmConfigPoliza.ConectaConfiguraPoliza : Boolean;
var
   i: Integer;
begin
     for i := 0 to K_MAXGRUPOS do
     begin
          FListaCodigo[ i ] := TStringList.Create;
          FListaCuenta[ i ] := TStringList.Create;
     end;
     FListaAsiento := TStringList.Create;
     FGrupos := TGroupList.Create;


     dmCatalogos.cdsConceptosLookup.Conectar;

     {Todos los grupos quedan con el Dataset Vacio}
     //Datos := GetSQLData( GetSQLScript( Q_GRUPO ) );

     CreatePoliza;
     CreateCtaConcepto;
     CreaDatasetGrupo( cdsGrupo1 );
     CreaDatasetGrupo( cdsGrupo2 );
     CreaDatasetGrupo( cdsGrupo3 );
     CreaDatasetGrupo( cdsGrupo4 );
     CreaDatasetGrupo( cdsGrupo5 );

     Result := True;
end;

procedure TdmConfigPoliza.CreateCtaConcepto;
begin
     with cdsCtaConcepto do
     begin
          InitTempDataset;
          AddIntegerField( K_FLD_CONCEPTO ); { N�mero del Concepto }
          AddStringField( K_FLD_DESCRIPCION, 30 );  { Descripcion del Concepot }
          AddStringField( K_FLD_SUBCUENTA, 30 );  { Subcuenta }
          AddStringField( K_FLD_ASIENTO, 1 );  { Tipo de Asiento, Cargo/Abono }
          AddStringField( K_FLD_COMENTARIO, 255 );  { Comentario }
          AddStringField( K_FLD_TIPO, 20 ); {Tipo de Concepto (eTipoConcepto)}
          CreateTempDataset;
     end;
end;

procedure TdmConfigPoliza.CreatePoliza;
begin
     with cdsPoliza do
     begin
          InitTempDataset;
          AddIntegerField( K_FLD_CONCEPTO ); {N�mero de Concepto}
          AddStringField( K_FLD_CUENTA, 30 );  { Cuenta Contable }
          AddStringField( K_FLD_GRUPO1, 6 );  { C�digo del Grupo 1 }
          AddStringField( K_FLD_GRUPO2, 6 );  { C�digo del Grupo 2 }
          AddStringField( K_FLD_GRUPO3, 6 );  { C�digo del Grupo 3 }
          AddStringField( K_FLD_GRUPO4, 6 );  { C�digo del Grupo 4 }
          AddStringField( K_FLD_GRUPO5, 6 );  { C�digo del Grupo 5 }
          AddStringField( K_FLD_TIPO, 1 );  { Tipo de Asiento, Cargo/Abono }
          CreateTempDataset;
     end;
end;

{procedure TdmConfigPoliza.FilterCampoRepOn;
begin
     with cdsCampoRep do
     begin
          Filter := Format( 'CR_TIPO = %d', [ Ord( tcCampos ) ] );
          Filtered := True;
     end;
end;

procedure TdmConfigPoliza.FilterCampoRepOff;
begin
     with cdsCampoRep do
     begin
          Filtered := False;
          Filter := VACIO;
     end;
end;

procedure TdmConfigPoliza.FilterCampoRepOrdenOn;
begin
     with cdsCampoRep do
     begin
          Filter := Format( 'CR_TIPO = %d', [ Ord( tcOrden ) ] );
          Filtered := True;
     end;
end;
}
procedure TdmConfigPoliza.LlenaPolizaGrupos( Lista, oListGrupos: TStrings );
var
   i: Integer;
begin
     FGrupos.Clear;
     for i:= 0  to oListGrupos.Count - 1 do
     begin
          FGrupos.AddGroup( oListGrupos[ i ],
                            TGrupoOpciones( oListGrupos.Objects[ i ] ).Formula );
     end;
     { Columnas del DataSet de P�liza }
     with cdsPoliza do
     begin
          for i := 1 to K_MAXGRUPOS do
          begin
               FieldByName( Format( K_FLD_GRUPO, [ i ] ) ).Visible := ( FGrupos.Count >= i );
          end;
     end;
     { Llenar la Lista del ComboBox }
     with Lista do
     begin
          Clear;
          BeginUpdate;
          try
             for i := 0 to ( FGrupos.Count - 1 ) do
             begin
                  with FGrupos.Items[ i ] do
                  begin
                       Add( Format( '%s=%s', [ Nombre, Formula ] ) );
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmConfigPoliza.ImportaArchivo( Dataset: TClientDataSet; const sFile: String );
begin
     with Dataset do
     begin
          LoadFromFile( sFile );
          IndexFieldNames := Fields[ 0 ].FieldName;
     end;
end;

procedure TdmConfigPoliza.ExportaArchivo( Dataset: TClientDataSet; const sFile: String);
begin
     with Dataset do
     begin
          SaveToFile( sFile );
     end;
end;

procedure TdmConfigPoliza.ImportaCtaConcepto(const sFile: String);
begin
     try
        ImportaArchivo( cdsCtaConcepto, sFile );
     finally
            if NOT cdsCtaConcepto.Active then
               CreateCtaConcepto;
     end;
end;

procedure TdmConfigPoliza.ImportaGrupo(const eGrupo: eGroupType; const sFile: String);
 var cdsGrupo : TServerDataset;
begin
     cdsGrupo := GetDatasetGrupo( eGrupo );
     try
        ImportaArchivo( cdsGrupo, sFile );
     finally
            if NOT cdsGrupo.Active then
               CreaDatasetGrupo( cdsGrupo );
     end;
end;

procedure TdmConfigPoliza.ExportaCtaConcepto(const sFile: String);
begin
     ExportaArchivo( cdsCtaConcepto, sFile );
end;

procedure TdmConfigPoliza.ExportaGrupo(const eGrupo: eGroupType; const sFile: String);
begin
     ExportaArchivo( GetDatasetGrupo( eGrupo ), sFile );
end;

procedure TdmConfigPoliza.ImportaGrupoTress( const eGrupo: eGroupType );
var
   FDataset: TClientDataset;
   oDatasetGrupo: TZetaLookupDataSet;
   sSubCuenta : string;
begin
     FDataset := GetDatasetGrupo( eGrupo );
     if Assigned( FDataset ) then
     begin
          with FGrupos.Items[ Ord( eGrupo ) ] do
          begin
               {with dmCliente do
               begin
                    case Entidad of
                         enClasifi: FillGrupos( FDataset, ServerCatalogos.GetClasifi( Empresa ), 'TB_CODIGO', 'TB_ELEMENT', 'TB_TEXTO' );
                         enNivel1: FillNivel( FDataset, 1  );
                         enNivel2: FillNivel( FDataset, 2  );
                         enNivel3: FillNivel( FDataset, 3  );
                         enNivel4: FillNivel( FDataset, 4  );
                         enNivel5: FillNivel( FDataset, 5  );
                         enNivel6: FillNivel( FDataset, 6  );
                         enNivel7: FillNivel( FDataset, 7  );
                         enNivel8: FillNivel( FDataset, 8  );
                         enNivel9: FillNivel( FDataset, 9  );
                         enPuesto: FillGrupos( FDataset, ServerCatalogos.GetPuestos( Empresa ), 'PU_CODIGO', 'PU_DESCRIP', 'PU_TEXTO' );
                         enTurno: FillGrupos( FDataset, ServerCatalogos.GetTurnos( Empresa ), 'TU_CODIGO', 'TU_DESCRIP', 'TU_TEXTO' );
                    else
                        DataBaseError( Format( 'El Campo %s No Est� Preparado', [ Formula ] ) );
                    end;
               end;}
               oDatasetGrupo := TressShell.GetLookUpDataSet( Entidad );
               if oDataSetGrupo = NIL then
               begin
                    ZError('Configuraci�n de la P�liza Contable', 'El Campo por el Cual se Quiere Agrupar la P�liza no es Una Tabla', 0 );
               end
               else
               begin
                    case Entidad of
                         enPuesto : sSubCuenta := 'PU_SUB_CTA';
                         enTurno : sSubCuenta := 'TU_SUB_CTA';
                         enClasifi, enNivel1..enNivel9{$ifdef ACS},enNivel10,enNivel11, enNivel12{$endif},enNivel0: sSubCuenta := 'TB_SUB_CTA'
                         else sSubCuenta := 'TB_TEXTO';
                    end;
                    
                    with oDatasetGrupo do
                    begin
                         Conectar;
                         FillGrupos( FDataset, Data, LookupKeyField, LookupDescriptionField, sSubCuenta );
                    end;
               end;
          end;
     end
     else
         ZError( 'Configuraci�n de la P�liza Contable', 'No Hay Grupos Asignados', 0 );
end;

procedure TdmConfigPoliza.FillGrupos( Dataset: TClientDataset; Datos: OleVariant; const sCodigo, sDescripcion, sSubcuenta: String );
begin
     with Dataset do
     begin
          DisableControls;
          EmptyDataSet;
          with cdsTemporal do
          begin
               Data := Datos;
               if Active then
               begin
                    First;
                    while not Eof do
                    begin
                         Dataset.Append;
                         Dataset.FieldByName( K_FLD_CODIGO ).AsString := FieldByName( sCodigo ).AsString;
                         Dataset.FieldByName( K_FLD_DESCRIPCION ).AsString := FieldByName( sDescripcion ).AsString;
                         Dataset.FieldByName( K_FLD_SUBCUENTA ).AsString := FieldByName( sSubCuenta ).AsString;
                         Dataset.Post;
                         Next;
                    end;
               end;
               Active := False;
          end;
          First;
          EnableControls;
     end;
end;

function TdmConfigPoliza.GetDatasetGrupo(const eGrupo: eGroupType): TServerDataset;
begin
     case eGrupo of
          eGrupo1: Result := cdsGrupo1;
          eGrupo2: Result := cdsGrupo2;
          eGrupo3: Result := cdsGrupo3;
          eGrupo4: Result := cdsGrupo4;
          eGrupo5: Result := cdsGrupo5;
     else
         Result := nil;
     end;
end;

procedure TdmConfigPoliza.LlenaConceptos( eTiposConcepto : TiposConcepto );
const
     aAsiento: array[ False..True ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( K_CARGO, K_ABONO );
var
   eTipo: eTipoConcepto;
begin
     with cdsCtaConcepto do
     begin
          DisableControls;
          EmptyDataSet;
     end;
     try
        with dmCatalogos.cdsConceptosLookup do
        begin
             if Active then
             begin
                  First;
                  while not Eof do
                  begin
                       eTipo := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
                       if ( eTipo in eTiposConcepto ) then
                       begin
                            cdsCtaConcepto.Append;
                            cdsCtaConcepto.FieldByName( K_FLD_CONCEPTO ).AsInteger := FieldByName( 'CO_NUMERO' ).AsInteger;
                            cdsCtaConcepto.FieldByName( K_FLD_DESCRIPCION ).AsString := FieldByName( 'CO_DESCRIP' ).AsString;
                            cdsCtaConcepto.FieldByName( K_FLD_ASIENTO ).AsString := aAsiento[ ( eTipo in [coDeduccion,coObligacion] ) ];
                            cdsCtaConcepto.FieldByName( K_FLD_TIPO ).AsString := ObtieneElemento( lfTipoConcepto, FieldByName( 'CO_TIPO' ).AsInteger );
                            cdsCtaConcepto.FieldByName( K_FLD_SUBCUENTA ).AsString :=  FieldByName( 'CO_SUB_CTA' ).AsString;
                            cdsCtaConcepto.Post;
                       end;
                       Next;
                  end;
             end;
        end;
     finally
            with cdsCtaConcepto do
            begin
                 if Active then
                    First;
                 EnableControls;
            end;
     end;
end;

procedure TdmConfigPoliza.BorraGruposTodos( const eGrupo: eGroupType );
var
   FDataset: TClientDataset;
begin
     FDataset := GetDatasetGrupo( eGrupo );
     if Assigned( FDataset ) then
     begin
          FDataset.EmptyDataSet;
     end;
end;

procedure TdmConfigPoliza.BorraFormulasPoliza;
begin
     BorraFormulas( cdsCampoRep );
end;

procedure TdmConfigPoliza.BorraFormulas( Dataset: TClientDataset );
begin
     with Dataset do
     begin
          DisableControls;
          try
             First;
             while not Eof do
             begin
                  if ( eTipoCampo( FieldByName( 'CR_TIPO' ).AsInteger ) = tcCampos ) then
                  begin
                       Delete;
                  end
                  else
                      Next;
             end;
          finally
                 EnableControls;
          end;
     end;
end;


procedure TdmConfigPoliza.LimpiaListas;
var
   i: Integer;
begin
     for i := 0 to K_MAXGRUPOS do
     begin
          FListaCodigo[ i ].Clear;
          FListaCuenta[ i ].Clear;
     end;
     FListaAsiento.Clear;
end;

procedure TdmConfigPoliza.GenerarPoliza( const sTemplate: String );
var
   iConcepto, iG1, iG2, iG3, iG4, iG5 : Integer;
   sCuenta: String;

    procedure LlenaLista( const iPos: Integer; DataSet: TClientDataset );
    const
         K_CODIGO = 0;
    var
       sSubCuenta: String;
    begin
         with DataSet do
         begin
              DisableControls;
              First;
              while not Eof do
              begin
                   sSubCuenta := FieldByName( 'SubCuenta' ).AsString;
                   if ZetaCommonTools.StrLleno( Trim( sSubCuenta ) ) then
                   begin
                        FListaCodigo[ iPos ].Add( Fields[ K_CODIGO ].AsString );
                        FListaCuenta[ iPos ].Add( sSubCuenta );
                        if ( iPos = 0 ) then
                           FListaAsiento.Add( FieldByName( K_FLD_ASIENTO ).AsString );
                   end;
                   Next;
              end;
              First;
              EnableControls;
              // Se necesita al menos uno para que funcionen los 'for' anidados
              if ( FListaCodigo[ iPos ].Count = 0 ) then
              begin
                   FListaCodigo[ iPos ].Add( VACIO );
                   FListaCuenta[ iPos ].Add( VACIO );
                   if ( iPos = 0 ) then
                       FListaAsiento.Add( VACIO );
              end
         end;
    end;

    function Sustituye( const sValor, sPatron, sNuevo: String ): String;
    begin
         Result := StringReplace( sValor, sPatron, sNuevo, [ rfReplaceAll, rfIgnoreCase ] );
    end;

begin
     LimpiaListas;
     LlenaLista( 0, cdsCtaConcepto );
     LlenaLista( 1, cdsGrupo1 );
     LlenaLista( 2, cdsGrupo2 );
     LlenaLista( 3, cdsGrupo3 );
     LlenaLista( 4, cdsGrupo4 );
     LlenaLista( 5, cdsGrupo5 );
     with cdsPoliza do
     begin
          DisableControls;
          try
            {10-Jun-2003}
            {CV: El EmptyDataset, hace que me marque el error de 'Operation Not Applicable',
            cuando se ejecuta varias veces este codigo. Se cambio por el Delete y ya no marca el error.}
            //EmptyDataSet;
            while NOT EOF do
                  Delete;
                
            for iConcepto := 0 to ( FListaCodigo[ 0 ].Count - 1 ) do
            begin
                 for iG1 := 0 to ( FListaCodigo[ 1 ].Count - 1 ) do
                 begin
                      for iG2 := 0 to ( FListaCodigo[ 2 ].Count - 1 ) do
                      begin
                           for iG3 := 0 to ( FListaCodigo[ 3 ].Count - 1 ) do
                           begin
                                for iG4 := 0 to ( FListaCodigo[ 4 ].Count - 1 ) do
                                begin
                                     for iG5 := 0 to ( FListaCodigo[ 5 ].Count - 1 ) do
                                     begin
                                          sCuenta := sTemplate;
                                          sCuenta := Sustituye( sCuenta, '#CO', FListaCuenta[ 0 ][ iConcepto ] );
                                          sCuenta := Sustituye( sCuenta, '#G1', FListaCuenta[ 1 ][ iG1 ] );
                                          sCuenta := Sustituye( sCuenta, '#G2', FListaCuenta[ 2 ][ iG2 ] );
                                          sCuenta := Sustituye( sCuenta, '#G3', FListaCuenta[ 3 ][ iG3 ] );
                                          sCuenta := Sustituye( sCuenta, '#G4', FListaCuenta[ 4 ][ iG4 ] );
                                          sCuenta := Sustituye( sCuenta, '#G5', FListaCuenta[ 5 ][ iG5 ] );
                                          Append;
                                          FieldByName( K_FLD_CUENTA ).AsString := sCuenta;
                                          FieldByName( K_FLD_CONCEPTO ).AsInteger := StrToIntDef( FListaCodigo[ 0 ][ iConcepto ], 0 );
                                          FieldByName( K_FLD_GRUPO1 ).AsString := FListaCodigo[ 1 ][ iG1 ];
                                          FieldByName( K_FLD_GRUPO2 ).AsString := FListaCodigo[ 2 ][ iG2 ];
                                          FieldByName( K_FLD_GRUPO3 ).AsString := FListaCodigo[ 3 ][ iG3 ];
                                          FieldByName( K_FLD_GRUPO4 ).AsString := FListaCodigo[ 4 ][ iG4 ];
                                          FieldByName( K_FLD_GRUPO5 ).AsString := FListaCodigo[ 5 ][ iG5 ];
                                          FieldByName( K_FLD_TIPO ).AsString := FListaAsiento[ iConcepto ];
                                          Post;
                                     end;
                                end;
                           end;
                      end;
                 end;
            end;
            MergeChangeLog;
            First;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmConfigPoliza.AgregaAsientos( Callback: TNotifyEvent );
var
   iUltimo: Integer;
   sCuenta, sUltimaCuenta, sTipo: String;

    function GetDatasetGrupo( const iPos: Integer ): String;
    begin
         with FListaCodigo[ iPos ] do
         begin
            if ( Count > 0 ) then
               Result := '"' + FListaCodigo[ iPos ].CommaText + '"'
            else
                Result := VACIO;
         end;
    end;

    function CreaFormula: String;
    var
       i: Integer;
       sGrupo: String;
    begin
        Result := 'SUMA( ' + GetDatasetGrupo( 0 );
        for i := 1 to FGrupos.Count do
        begin
            sGrupo := GetDatasetGrupo( i );
            { Si no hay grupo y es el �ltimo grupo, entonces no agrega la ',' }
            if ZetaCommonTools.StrLleno( sGrupo ) or ( i < FGrupos.Count ) then
                Result := Result + ', ' + sGrupo;
        end;
        Result := Result + ' )';
    end;

    procedure AgregaUnaCuenta;
    var
       iOperacion: Integer;
    begin
         Inc( iUltimo );
         if ( sTipo = K_CARGO ) then
            iOperacion := K_CARGO_INT
         else
             iOperacion := K_ABONO_INT;
         with cdsCampoRep do
         begin
              Append;
              //FieldByName( 'CR_POSICIO' ).AsInteger := iUltimo;
              FieldByName( 'CR_POSICIO' ).AsInteger := -1;
              FieldByName( 'CR_FORMULA' ).AsString := CreaFormula;
              FieldByName( 'CR_TITULO' ).AsString := sUltimaCuenta;
              FieldByName( 'CR_OPER' ).AsInteger := iOperacion;
              Post;
         end;
         LimpiaListas;
    end;

    procedure AgregaUnCodigo( const iPos: Integer; const sCampo: String );
    var
       sCodigo: String;
    begin
        sCodigo := cdsPoliza.FieldByName( sCampo ).AsString;
        if ZetaCommonTools.StrLleno( Trim( sCodigo ) ) then
        begin
             with FListaCodigo[ iPos ] do
             begin
                 if ( IndexOf( sCodigo ) < 0 ) then
                    Add( sCodigo );
             end;
        end;
    end;

    procedure AgregaCodigos;
    var
       i: Integer;
    begin
         AgregaUnCodigo( 0, K_FLD_CONCEPTO );
         for i := 1 to FGrupos.Count do
         begin
              AgregaUnCodigo( i, Format( K_FLD_GRUPO, [ i ] ) );
         end;
    end;

begin
     sUltimaCuenta := VACIO;
     LimpiaListas;
     iUltimo := UltimoAsiento;
     with cdsCampoRep do
     begin
          DisableControls;
     end;
     with cdsPoliza do
     begin
          DisableControls;
          try
             IndexFieldNames := K_FLD_CUENTA;
             First;
             while not Eof do
             begin
                  if Assigned( Callback ) then
                     CallBack( Self );
                  Application.ProcessMessages;
                  sCuenta := FieldByName( K_FLD_CUENTA ).AsString;
                  { Agrupa SUMA de todas las que van a la misma cuenta }
                  if ( sCuenta <> sUltimaCuenta ) then
                  begin
                       if ZetaCommonTools.StrLleno( sUltimaCuenta ) then { Que no sea la primera }
                          AgregaUnaCuenta;
                       sUltimaCuenta := sCuenta;
                       sTipo := FieldByName( K_FLD_TIPO ).AsString;
                  end;
                  AgregaCodigos;
                  Next;
             end;
             AgregaUnaCuenta;
          finally
                 EnableControls;
          end;
     end;
     with cdsCampoRep do
     begin
          Last;
          EnableControls;
     end;
end;

function TdmConfigPoliza.UltimoAsiento: Integer;
begin
     with cdsCampoRep do
     begin
          if Active then
          begin
               DisableControls;
               try
                  Last;
                  Result := FieldByName( 'CR_POSICIO' ).AsInteger;
                  First;
               finally
                      EnableControls;
               end;
          end
          else
              Result := 0;
     end;
end;


function TdmConfigPoliza.GetCuantosAsientos: Integer;
begin
     with cdsPoliza do
     begin
          if Active then
             Result := RecordCount
          else
              Result := 0;
     end;
end;

function TdmConfigPoliza.GetPolizaActiva: Integer;
begin
     Result := cdsReporte.FieldByName( 'RE_CODIGO' ).AsInteger;
end;

function TdmConfigPoliza.GetPolizaNombre: String;
begin
     Result := cdsReporte.FieldByName( 'RE_NOMBRE' ).AsString;
end;


{
procedure TdmConfigPoliza.CreateLookupServer( Fuente: TZetaLookupDataset; const sFieldName, sKeyFields, sLookupKeyFields, sResultField: String );
var
   FieldClassType: TFieldClass;
   Campo: TField;
   CampoDef: TFieldDef;
begin
     with Fuente do
     begin
          if not Active then
            DatabaseError( 'Falta Conectar ' + Name );
          CampoDef := FieldDefs.Find( sResultField );
     end;
     Campo := TStringField.Create( cdsCtaConcepto );
     try
        with Campo do
        begin
             Size := CampoDef.Size;
             SetFieldType( CampoDef.DataType );
             FieldName := sFieldName;
             Name := sFieldName;
             Required := False;
             ReadOnly := True;
             FieldKind := fkLookup;
             Lookup := True;
             KeyFields := sKeyFields;
             LookupDataset := Fuente;
             LookupKeyFields := sLookupKeyFields;
             LookupResultField := sResultField;
             Dataset := cdsCtaConcepto;
        end;
     except
           Campo.Free;
           raise;
     end;
end;
}
{procedure TdmConfigPoliza.cdsCtaConceptoAfterOpen(DataSet: TDataSet);
begin
     inherited;
     //CreateLookupServer( dmCatalogos.cdsConceptos, K_FLD_DESCRIPCION, K_FLD_CONCEPTO, 'CO_NUMERO', 'CO_DESCRIP' );
end;}

end.
