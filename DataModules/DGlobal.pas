unit DGlobal;

interface

uses Forms, Controls, DB, Classes, StdCtrls,
     ZetaCommonClasses,
     ZetaCommonLists,
     dBaseGlobal,
{$ifdef DOS_CAPAS}
     DServerGlobal,
{$else}
     Global_TLB,
{$endif}
     ZGlobalTress;

type
  TdmGlobal = class( TdmBaseGlobal )
  private
    FNombreCosteo: string;

{$ifdef DOS_CAPAS}
    function GetServer: TdmServerGlobal;
    property Server: TdmServerGlobal read GetServer;
{$else}
    FServidor: IdmServerGlobalDisp;
    function GetServer: IdmServerGlobalDisp;
    property Server: IdmServerGlobalDisp read GetServer;
{$endif}
  protected
    function GetGlobales: Variant; override;
    procedure GrabaGlobales( const aGlobalServer: Variant; const lActualizaDiccion: Boolean;
              var ErrorCount: Integer ); override;
    function GetNombreCosteo: string;
    function GetDescripcionesGV: Variant; override;
    procedure GrabaDescripcionesGV( const aDescripcionServer: Variant; const lActualizaDiccion: Boolean;
              var ErrorCount: Integer ); override;
  public
    { Globales especializados TRESS }
    property NombreCosteo: string read GetNombreCosteo;

    // Niveles de Organigrama
    function NombreNivel( const Index: Integer ): String;
    function NumNiveles: Integer;
    function NivelCosteo: string;

    // Cursos & Certificaciones
    function GetCampoNivelProg (const iGlobalNivel:Integer): String;
    function GetDescNivelProg (const iGlobalNivel:Integer): String;
    // Valores Defaults de Alta
    function GetEmpDefault: TEmpDefault;
    // Fecha Limite de Bloqueo
    function GetFechaLimite: OleVariant;
    function SetFechaLimite( dGlobal: TDate ): Boolean;
    procedure ProcesaDescripcionesGV(const aGlobalDescripciones: Variant );

  end;

var
   Global: TdmGlobal;

   procedure SetCampoNivel( const iNivel, iNiveles : Integer; oLabel: TLabel; oControl: TControl;
                            oControl2 : TControl = nil );
   procedure SetDescuentoComida( lblLabel: TLabel; lblControl : TControl );
   
implementation

uses DCliente, SysUtils, ZetaCommonTools;

procedure SetCampoNivel( const iNivel, iNiveles : Integer; oLabel: TLabel; oControl: TControl;
                         oControl2 : TControl = nil );
var
   lEnabled : Boolean;
begin
     lEnabled := ( iNivel <= iNiveles );
     oLabel.Visible   := lEnabled;
     oControl.Visible := lEnabled;
     if oControl2 <> nil then
        oControl2.Visible := lEnabled;
     if lEnabled then
        oLabel.Caption := Global.NombreNivel( iNivel ) + ':';
end;

procedure SetDescuentoComida( lblLabel: TLabel; lblControl : TControl );
 var
    lDescontarComida: Boolean;
begin
     lDescontarComida := Global.GetGlobalBooleano( K_GLOBAL_DESCONTAR_COMIDAS );
     lblLabel.Visible := lDescontarComida;
     lblControl.Visible := lDescontarComida;
end;

{ ************** TdmGlobal *************** }
{
 GetGlobales recibe un arreglo ajustado al numero total de CONSTANTES Globales.
 Cuando NO hay un registro correspondiente a la Constante, en su lugar viene un 'NIL'.
 De lo contrario, en su lugar viene la formula (GLOBAL.GL_FORMULA ).
}

{$ifdef DOS_CAPAS}
function TdmGlobal.GetServer: TdmServerGlobal;
begin
     Result := DCliente.dmCliente.ServerGlobal;
end;
{$else}
function TdmGlobal.GetServer: IdmServerGlobalDisp;
begin
     Result := IdmServerGlobalDisp( dmCliente.CreaServidor( CLASS_dmServerGlobal, FServidor ) );
end;
{$endif}

function TdmGlobal.GetGlobales: Variant;
begin
     Result := Server.GetGlobales( dmCliente.Empresa );
end;

procedure TdmGlobal.GrabaGlobales(const aGlobalServer: Variant; const lActualizaDiccion: Boolean;
          var ErrorCount: Integer);
begin
     Server.GrabaGlobales( dmCliente.Empresa, aGlobalServer, lActualizaDiccion, ErrorCount );
end;

function TdmGlobal.GetDescripcionesGV: Variant;
begin
     Result := Server.GetDescripcionesGV( dmCliente.Empresa );
end;

procedure TdmGlobal.GrabaDescripcionesGV(const aDescripcionServer: Variant; const lActualizaDiccion: Boolean;
          var ErrorCount: Integer);
begin
     Server.GrabaDescripcionesGV( dmCliente.Empresa, aDescripcionServer, lActualizaDiccion, ErrorCount );
end;

{ *********************** Globales especializados TRESS **********************}

function TdmGlobal.GetCampoNivelProg (const iGlobalNivel:Integer ): String;
var
   iProg: Integer;
begin
     iProg := GetGlobalInteger( iGlobalNivel );
     case iProg of
          0: Result := '';
          1: Result := 'CB_CLASIFI';
          2: Result := 'CB_TURNO'
          else Result := 'CB_NIVEL' + IntToStr(iProg - 2)
     end;
end;

function TdmGlobal.GetDescNivelProg (const iGlobalNivel:Integer) : String;
var
   iProg: Integer;
begin
     iProg := GetGlobalInteger( iGlobalNivel );
     case iProg of
          0: Result := 'Nivel';
          1: Result := 'Clasificaci�n';
          2: Result := 'Turno'
     else
         Result := NombreNivel( iProg - 2 );
     end;
end;

function TdmGlobal.NombreNivel( const Index: Integer ): String;
begin
     if( Index > 0 ) and ( Index < 10 ) then
         Result := Trim( GetGlobalString( K_GLOBAL_NIVEL_BASE + Index ) )
     else if ( Index > 9 ) and ( Index < 13 ) then {ACS}
         Result := Trim( GetGlobalString( K_GLOBAL_NIVEL_BASE2 + Index ) );
end;

function TdmGlobal.NumNiveles: Integer;
begin
     Result := 0;
     // Cuenta los niveles NO vacios. En cuanto encuentra uno vac�o, deja de contar
     while ( Result < K_GLOBAL_NIVEL_MAX ) and ( Length( NombreNivel( Result+1 )) > 0 ) do
     begin
          Inc( Result );
     end;
end;

procedure TdmGlobal.ProcesaDescripcionesGV(const aGlobalDescripciones: Variant);
var
   iErrorCount : integer;
   lActualizaDiccion : boolean;
begin
      lActualizaDiccion := true;
      GrabaDescripcionesGV(aGlobalDescripciones, lActualizaDiccion, iErrorCount);
end;

function TdmGlobal.GetEmpDefault: TEmpDefault;
begin
     with Result do
     begin
          Ciudad            := GetGlobalString( K_GLOBAL_CIUDAD_EMPRESA );
          Estado            := GetGlobalString( K_GLOBAL_ENTIDAD_EMPRESA );
          Municipio         := GetGlobalString( K_GLOBAL_DEF_MUNICIPIO );
          Checa             := GetGlobalBooleano( K_GLOBAL_DEF_CHECA_TARJETA);
          Nacion            := GetGlobalString( K_GLOBAL_DEF_NACIONALIDAD );
          Sexo              := GetGlobalString( K_GLOBAL_DEF_SEXO );
          Credencial        := GetGlobalString( K_GLOBAL_DEF_LETRA_GAFETE );
          EstadoCivil       := GetGlobalString( K_GLOBAL_DEF_ESTADO_CIVIL );
          ViveEn            := GetGlobalString( K_GLOBAL_DEF_VIVE_EN  );
          ViveCon           := GetGlobalString( K_GLOBAL_DEF_VIVE_CON );
          MedioTransporte   := GetGlobalString( K_GLOBAL_DEF_MEDIO_TRANSPORTE );
          Estudio           := GetGlobalString( K_GLOBAL_DEF_ESTUDIOS );
          AutoSal           := GetGlobalBooleano( K_GLOBAL_DEF_SALARIO_TAB );
          ZonaGeografica    := Copy( GetGlobalString(K_GLOBAL_ZONAS_GEOGRAFICAS),1,1);
          Patron            := GetGlobalString( K_GLOBAL_DEF_REGISTRO_PATRONAL );
          Salario           := GetGlobalReal( K_GLOBAL_DEF_SALARIO_DIARIO );
          Contrato          := GetGlobalString( K_GLOBAL_DEF_CONTRATO );
          Puesto            := GetGlobalString( K_GLOBAL_DEF_PUESTO );
          Clasifi           := GetGlobalString( K_GLOBAL_DEF_CLASIFICACION );
          Turno             := GetGlobalString( K_GLOBAL_DEF_TURNO );
          TablaSS           := GetGlobalString( K_GLOBAL_DEF_PRESTACIONES );
          Nivel1            := GetGlobalString( K_GLOBAL_DEF_NIVEL_1 );
          Nivel2            := GetGlobalString( K_GLOBAL_DEF_NIVEL_2 );
          Nivel3            := GetGlobalString( K_GLOBAL_DEF_NIVEL_3 );
          Nivel4            := GetGlobalString( K_GLOBAL_DEF_NIVEL_4 );
          Nivel5            := GetGlobalString( K_GLOBAL_DEF_NIVEL_5 );
          Nivel6            := GetGlobalString( K_GLOBAL_DEF_NIVEL_6 );
          Nivel7            := GetGlobalString( K_GLOBAL_DEF_NIVEL_7 );
          Nivel8            := GetGlobalString( K_GLOBAL_DEF_NIVEL_8 );
          Nivel9            := GetGlobalString( K_GLOBAL_DEF_NIVEL_9 );
          {$ifdef ACS}
          Nivel10            := GetGlobalString( K_GLOBAL_DEF_NIVEL_10 );
          Nivel11            := GetGlobalString( K_GLOBAL_DEF_NIVEL_11 );
          Nivel12            := GetGlobalString( K_GLOBAL_DEF_NIVEL_12 );
          {$endif}
          Siguiente         := GetGlobalBooleano( K_GLOBAL_NUM_EMP_AUTOMATICO );
          Regimen           := GetGlobalInteger( K_GLOBAL_DEF_REGIMEN );
          Banco             := GetGlobalString( K_GLOBAL_DEF_BANCO );
     end;
end;

function TdmGlobal.GetFechaLimite: OleVariant;
begin
     Result := Server.GetFechaLimite( dmCliente.Empresa );
end;

function TdmGlobal.SetFechaLimite( dGlobal: TDate ): Boolean;
begin
     Result := Server.SetFechaLimite( dmCliente.Empresa, dGlobal );
end;

function TdmGlobal.NivelCosteo: string;
begin
     Result := 'CB_NIVEL' + IntTostr( GetGlobalInteger(K_GLOBAL_NIVEL_COSTEO) );
end;

function TdmGlobal.GetNombreCosteo: string;
begin
     if StrVacio( FNombreCosteo ) then
        FNombreCosteo := NombreNivel( GetGlobalInteger( K_GLOBAL_NIVEL_COSTEO ) );
     Result := FNombreCosteo;
end;



end.
