unit DBaseDiccionario;

interface

{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
{$ifdef DOS_CAPAS}
     {$ifdef RDD}
        DServerReporteadorDD,
     {$else}
        DServerDiccionario,
     {$endif}
{$else}
     {$ifdef RDD}
     ReporteadorDD_TLB,
     {$else}
     Diccionario_TLB,
     {$endif}
{$endif}
{$ifdef RDD}
{$else}
     DEntidadesTress,
{$endif}
     ZetaTipoEntidad,
     ZetaTipoEntidadTools,
     ZetaCommonLists,
     ZetaClientDataSet,
     cxCheckListBox;

const
     K_DICCION_SISTEMA = 10000;

type



  {$ifdef RDD}
    ListaClasificaciones= integer;
    {$ifdef DOS_CAPAS}
       TdmServerDiccionario = TdmServerReporteadorDD;
    {$else}
       IdmServerDiccionarioDisp = IdmServerReporteadorDDDisp;
    {$endif}
  {$else}
  ListaClasificaciones= set of eClasifiReporte;
  {$endif}

  TObjetoEntidad = class( TObject )
  private
    { Private declarations }
  public
    { Public declarations }
    Entidad: TipoEntidad;
  end;
  TObjetoString = class( TObject )
  private
    { Private declarations }
  public
    { Public declarations }
{$IFDEF TRESS_DELPHIXE5_UP}
    Campo: String;
{$ELSE}
    Campo: String[ 30 ];
{$ENDIF}
    Entidad: TipoEntidad;
  end;

  //TClasifiReporte = Array[Ord(Low(eClasifiReporte)) .. Ord( High(eClasifiReporte) )] of eClasifiReporte;

  TdmBaseDiccionario = class(TDataModule)
    cdsGlobal: TZetaClientDataSet;
    cdsBuscaPorCampo: TZetaClientDataSet;
    cdsBuscaPorTabla: TZetaClientDataSet;
    cdsDiccion: TZetaClientDataSet;
    cdsClasificaciones: TZetaLookupDataSet;
    cdsTablasPorClasificacion: TZetaClientDataSet;
    cdsRelaciones: TZetaClientDataSet;
    cdsCamposPorTabla: TZetaClientDataSet;
    cdsLookupGeneral: TZetaLookupDataSet;
    cdsClasifiDerechos: TZetaClientDataSet;
    cdsDatosDefault: TZetaClientDataSet;
    cdsListasFijas: TZetaLookupDataSet;
    cdsModulos: TZetaLookupDataSet;
    cdsListasFijasValores: TZetaClientDataSet;
    cdsFunciones: TZetaClientDataSet;
    cdsEntidades: TZetaLookupDataSet;
    cdsEntidadesLookup: TZetaLookupDataSet;
    cdsModulosLookup: TZetaLookupDataSet;
    cdsEntidadesPorModulo: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsFuncionesAlAdquirirDatos(Sender: TObject);
    procedure cdsGlobalAlAdquirirDatos(Sender: TObject);
    procedure cdsDiccionAlAdquirirDatos(Sender: TObject);
    procedure cdsClasificacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsTablasPorClasificacionAlAdquirirDatos(Sender: TObject);
    procedure cdsRelacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsLookupGeneralAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiDerechosAlAdquirirDatos(Sender: TObject);
    procedure cdsModulosAlAdquirirDatos(Sender: TObject);
    procedure cdsListasFijasAlAdquirirDatos(Sender: TObject);
    procedure cdsListasFijasValoresAlAdquirirDatos(Sender: TObject);
    procedure cdsCamposPorTablaAlCrearCampos(Sender: TObject);
    procedure cdsEntidadesAlAdquirirDatos(Sender: TObject);
    procedure cdsEntidadesNewRecord(DataSet: TDataSet);
    procedure cdsEntidadesBeforePost(DataSet: TDataSet);
    procedure cdsEntidadesPorModuloAlAdquirirDatos(Sender: TObject);
    
  private
    { Private declarations }
    sFiltro: String;
    lVerConfidencial: Boolean;
    lRevisaConfidencial: Boolean;
    FEntidad: TipoEntidad;
    FClasifiDefault: eClasifiReporte;
    FListaTemporal :TStrings;
    lArregloVacio: Boolean;

{$ifdef DOS_CAPAS}
    FServidor: TdmServerDiccionario;
{$else}
    FServidor: IdmServerDiccionarioDisp;
    function GetServerDiccionario: IdmServerDiccionarioDisp;
{$endif}

{$ifdef RDD}
{$else}
    function BuscarTabla( const iEntidad: TipoEntidad ): String;
    function GetRelaciones( const iEntidad: TipoEntidad ): String;
    function GetDescrip( const eTipo : eClasifiReporte ) : String;
{$endif}
  protected
    { Protected declarations }
    {$ifdef RDD}
    {$else}
    dmEntidadesShell: TdmEntidadesTress;
    {$endif}

    sFiltroConfi: String;

{$ifdef DOS_CAPAS}
    property ServerDiccionario: TdmServerDiccionario read FServidor;
{$else}
    property ServerDiccionario: IdmServerDiccionarioDisp read GetServerDiccionario;
{$endif}

    procedure SetFiltro( const sFiltrar, sIndice: String );
    procedure SetAgregaRelacion;virtual;
    {$ifdef RDD}

    {$else}
    procedure AgregaClasifi( oLista: TStrings; const eClasifi: eClasifiReporte; const lValidaDerecho: Boolean= FALSE );
    {$endif}


  protected
    FClasificacion : Variant;
    function GetConfidencial: Boolean;virtual;
    {$ifdef RDD}
    {$else}
    function GetNOClasificacion( const lFavoritos, lMigracion, lSuscripciones: Boolean ): ListaClasificaciones;virtual;
    {$endif}

    function ChecaDerechoConfidencial: Boolean;virtual;
    function ValidaModulo( const eClasifi: eClasifiReporte ): Boolean;virtual;
  public
    { Public declarations }
    property Entidad : TipoEntidad read FEntidad write FEntidad;
    property VerConfidencial : Boolean read GetConfidencial;
    Property ClasifiDefault: eClasifiReporte read FClasifiDefault  write FClasifiDefault;

    procedure TablasPorClasificacion( const iClasifi: integer; oLista : TStrings );
    procedure GetListaDatosTablas( const oEntidad : TipoEntidad;
                                   const lTodos: Boolean;
                                   const lSoloLookups : Boolean = FALSE );
    procedure BuscarCampo( const oEntidad : TipoEntidad;
                           const sCampo : string;
                           const lPorNombre, lTodos, lConRelaciones : Boolean );

    procedure GetListaTablas( const oEntidad : TipoEntidad; oLista : TStrings; const lTodos, lConRelaciones : Boolean );

    procedure ConectaGlobal;
    procedure ConectaDiccion;overload;
    procedure ConectaDiccion( cdsDataSet: TZetaClientDataSet );overload;

    function GetCampoDescripcion(const iEntidad: TipoEntidad): string;
    function PruebaFormula( var sFormula : string;
                            const EntidadActiva : TipoEntidad;
                            const eTipoEv : ETipoEvaluador;
                            const oParametros : TStrings = NIL ) : Boolean;
    function SQLValido( var sFormula : string;
                        const EntidadActiva : TipoEntidad ) : Boolean;

    {$ifdef RDD}
    function EntidadValida( const NuevaEntidad, EntidadActual: TipoEntidad ) : Boolean;
    procedure ConectarRelaciones(const oEntidad : TipoEntidad);
    procedure ConectarDatosDefault(const oEntidad : TipoEntidad);
    function ConectaCamposPorTabla(const oEntidad: TipoEntidad): Boolean;
    procedure LlenaArregloEntidades;
    procedure SetFiltroCamposDefault(const lSetFiltro: Boolean = TRUE );
    procedure SetFiltroCamposOrden(const lSetFiltro: Boolean = TRUE );
    procedure SetFiltroCamposFiltro(const lSetFiltro: Boolean = TRUE );
    {$else}
    function GetRequeridos(const oEntidad: TipoEntidad): string;
    {$endif}
    procedure GetListaFunciones;
    procedure CamposPorEntidad( Entidad : TipoEntidad;lTodos : Boolean;oLista : TStrings );overload;
    procedure CamposPorEntidad( Entidad : TipoEntidad;lTodos : Boolean;oLista : tcxCheckListbox );overload;
    procedure GetListaClasifi( oLista : TStrings; const lMigracion : Boolean = TRUE );
    procedure GetListaClasifiModulo( oLista : TStrings; const lMigracion : Boolean = TRUE );virtual;
    procedure GetListaClasifiFavoritos( oLista : TStrings; const lMigracion : Boolean = TRUE );
    function GetPosClasifi( oLista: TStrings; const eTipo: eClasifiReporte; lIncluyeFavoritos: Boolean ): Integer;overload;
    function GetPosClasifi( oLista: TStrings; const eTipo: eClasifiReporte ):Integer;overload;
    function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;virtual;

    procedure CambioGlobales; virtual;
    procedure SetLookupNames; virtual;
    procedure CierraEmpresa;
    procedure RefrescaFormaConsulta(const oEntidad:TipoEntidad);
    procedure GetTablas(oLista: TStrings);
    function GetValorActivo(const eValor: {$ifdef RDD}eRDDValorActivo{$else}string{$endif}): string;virtual;
    procedure LlenaLista( Tipo: integer; Lista: TStrings; const LeerValoresSistema : Boolean = TRUE );
    function CodigoSistema( const iCodigo: integer ): Boolean;
    function RecordSistema( Dataset: TZetaLookupDataSet ): Boolean;
    procedure ConectaClasificacionesGrupoAdmin( oEmpresa: OleVariant );
    procedure ConectaEntidadesGrupo( oEmpresa: OleVariant );
    procedure ConectaEntidadesPorModulo( oEmpresa: OleVariant );
    {$ifdef RDD}
    function ExisteEntidadEnTablasPOrClasificacion( const iEntidad: TipoEntidad ): Boolean;
    function GetEsTablaNivel0: String;
    {$endif}
    function GetNombreTabla( const iEntidad: TipoEntidad ): string;
  end;

var
  dmBaseDiccionario: TdmBaseDiccionario;

implementation

uses FAutoClasses,
     ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo,
     ZReportConst,
     ZReportModulo,
     ZReportTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAgenteSQLClient,
     DGlobal,
     DReportes,
     DCliente;

{$R *.DFM}

{ TdmBaseDiccionario }

procedure TdmBaseDiccionario.DataModuleCreate(Sender: TObject);
begin
     lVerConfidencial := FALSE;
     lRevisaConfidencial := FALSE;
     sFiltroConfi := '';
{$ifdef DOS_CAPAS}
     FServidor := TdmServerDiccionario.Create( Self );
{$endif}
     lArregloVacio := TRUE;
end;

procedure TdmBaseDiccionario.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServidor );
{$endif}
end;

{$ifndef DOS_CAPAS}
function TdmBaseDiccionario.GetServerDiccionario: IdmServerDiccionarioDisp;
begin
     Result := IdmServerDiccionarioDisp(dmCliente.CreaServidor( {$ifdef RDD}CLASS_dmServerReporteadorDD{$else}CLASS_dmServerDiccionario{$endif}, FServidor ));
end;
{$endif}

procedure TdmBaseDiccionario.CierraEmpresa;
begin
     lRevisaConfidencial := FALSE;
{$ifdef DOS_CAPAS}
     ServerDiccionario.CierraEmpresa;
{$endif}
end;

{$ifdef RDD}
procedure TdmBaseDiccionario.LlenaArregloEntidades;
begin
     if lArregloVacio then
     begin
          cdsEntidades.Conectar;
          ZetaTipoEntidadTools.LlenaArregloEntidades( cdsEntidadesLookup );
          lArregloVacio := FALSE;
     end;
end;


function TdmBaseDiccionario.ConectaCamposPorTabla(const oEntidad: TipoEntidad): Boolean;
begin
     with cdsCamposPorTabla do
     begin
          Result := NOT Active or NOT Locate('TABLAPRINCIPAL',Ord(oEntidad),[]);
          if Result then
             with dmCliente do
                  Data := ServerDiccionario.CamposPorTabla( Empresa, Ord(oEntidad) );
     end;

end;
{$endif}

{$ifdef RDD}
procedure TdmBaseDiccionario.GetListaDatosTablas( const oEntidad: TipoEntidad;
                                                  const lTodos: Boolean;
                                                  const lSoloLookups : Boolean = FALSE );

 const K_FILTRO_RANGO = '((AT_TRANGO = 1) OR (AT_TRANGO = 2))';
       K_FILTRO_ENTIDAD = 'EN_CODIGO = %d';
begin
     with cdsBuscaPorTabla do
     begin
          DisableControls;
          try
             ConectaCamposPorTabla(oEntidad);
             Data := cdsCamposPorTabla.Data;

             IndexFieldNames := 'AT_CAMPO';
             
             Filtered := FALSE;
             Filter := Format( K_FILTRO_ENTIDAD, [ord(oEntidad)] );

             if lSoloLookups then
             begin
                  Filter := ConcatFiltros( Filter, K_FILTRO_RANGO );
             end;

             Filtered := TRUE;
          finally
                 EnableControls;
          end;
     end;
end;
{$else}
procedure TdmBaseDiccionario.GetListaDatosTablas( const oEntidad: TipoEntidad;
                                                  const lTodos: Boolean;
                                                  const lSoloLookups : Boolean = FALSE );

 const K_FILTRO_RANGO = ' AND ((DI_TRANGO = 1) OR (DI_TRANGO = 2))';
 var sOrden, sLookups: String;
begin
     if lTodos then
        sOrden := ''
     else
         sOrden := 'AND DI_ORDEN = ' + ZetaCommonTools.EntreComillas( K_GLOBAL_SI );
     if lSoloLookups then
        sLookups := K_FILTRO_RANGO
     else sLookups := '';
     SetFiltro(Format( '(DI_CLASIFI = %d) AND (DI_CALC <=0 ) %s %s',[Ord(oEntidad),sOrden,sLookups] ),'DI_NOMBRE');
     cdsBuscaPorTabla.Data := cdsDiccion.Data;
end;
{$endif}


{$ifdef RDD}
procedure TdmBaseDiccionario.TablasPorClasificacion(const iClasifi: integer; oLista: TStrings);
begin
     oLista.Clear;
     with cdsTablasPorClasificacion do
     begin
          Conectar;
          try
             DisableControls;
             Filtered := FALSE;
             Filter := Format( 'RC_CODIGO=%d', [iClasifi] );
             Filtered := TRUE;
             First;
             while NOT EOF do
             begin
                  oLista.AddObject(FieldByName('EN_TITULO').AsString, TObject(FieldByName('EN_CODIGO').AsInteger));
                  Next;
             end;
          finally
                 Filtered := FALSE;
                 EnableControls;
          end;
     end;
end;
{$else}
procedure TdmBaseDiccionario.TablasPorClasificacion(const iClasifi: integer; oLista: TStrings);
begin
     oLista.Clear;
     SetFiltro(Format( 'DI_CLASIFI = -1 AND DI_ANCHO = %d', [iClasifi] ), 'DI_TFIELD');
     with cdsDiccion do
     begin
          First;
          while NOT EOF do
          begin

               oLista.AddObject(FieldByName('DI_TITULO').AsString, TObject(FieldByName('DI_CALC').AsInteger));
               Next;
          end;
     end;
end;

function TdmBaseDiccionario.GetRelaciones(const iEntidad : TipoEntidad) : string;
var
   oRelaciones : TStringList;
begin
     oRelaciones := TStringList.Create;
     try
        oRelaciones.Sorted := TRUE;
        oRelaciones.Duplicates := dupIgnore;

        {$ifdef RDD}
        {$else}
        if dmEntidadesShell = NIL then
        begin
             dmEntidadesShell := TdmEntidadesTress.Create( self );
             SetAgregaRelacion;
        end;
        dmEntidadesShell.RelacionesEntidad( iEntidad, oRelaciones, TRUE );
        {$endif}


        Result := oRelaciones.CommaText;

        if Result > '' then
           Result := IntToStr(Ord(iEntidad))+','+Result
        else Result := IntToStr(Ord(iEntidad));
     except
           FreeAndNil(oRelaciones);
     end;
end;

{$endif}


procedure TdmBaseDiccionario.SetAgregaRelacion;
begin
end;

{$ifdef RDD}
procedure TdmBaseDiccionario.BuscarCampo( const oEntidad : TipoEntidad;
                                          const sCampo: string;
                                          const lPorNombre, lTodos, lConRelaciones: Boolean);

var
   sNombre, sBusca, sDescrip : string;
begin
     sBusca := UpperCase(sCampo);
     //Q_BUSCA_CAMPOS

     if lPorNombre then
     begin
          if ( Length( sBusca ) = 10 ) OR ( Pos( '%', sBusca ) > 0 ) then
             sNombre := ZetaCommonTools.EntreComillas( sBusca )
          else if ( Length( sBusca ) = 9 ) then
             sNombre := ZetaCommonTools.EntreComillas( sBusca + '%' )
          else
              sNombre := ZetaCommonTools.EntreComillas( '%' + sBusca + '%' );
          sDescrip := ZetaCommonTools.EntreComillas( '%' );
     end
     else
     begin
          sNombre := ZetaCommonTools.EntreComillas( '%' );
          if ( Pos( '%', sBusca ) > 0 ) then
              sDescrip := ZetaCommonTools.EntreComillas( sBusca )
          else
              sDescrip := ZetaCommonTools.EntreComillas( '%' + sBusca + '%' );
     end;
     with cdsBuscaPorCampo do
     begin
          DisableControls;
          try
             ConectaCamposPorTabla(oEntidad) ;
             Data := cdsCamposPorTabla.Data;

             IndexFieldNames := 'AT_CAMPO';
             Filtered := FALSE;
             Filter := Format( '( AT_CAMPO LIKE %s) AND '+
                           '( ( UPPER( AT_TITULO ) LIKE %s) OR '+
                           '( UPPER( AT_CLAVES ) LIKE %s) ) ',
                           [sNombre,sDescrip,sDescrip] );
             Filtered := TRUE;
          finally
                 EnableControls;

          end;
     end;
end;
{$else}
procedure TdmBaseDiccionario.BuscarCampo( const oEntidad : TipoEntidad;
                                          const sCampo: string;
                                          const lPorNombre, lTodos, lConRelaciones: Boolean);

var
   sNombre, sBusca, sDescrip, sOrden, sRelaciones : string;
begin
     sBusca := UpperCase(sCampo);
     //Q_BUSCA_CAMPOS
     sRelaciones := GetRelaciones(oEntidad);
     if lTodos then
     begin
          sRelaciones := sRelaciones  + ',' + IntToStr( Ord(enFunciones) );
          sOrden := '';
     end
     else
     begin
          sOrden := 'AND DI_ORDEN = ' + EntreComillas( K_GLOBAL_SI );
     end;

     if lPorNombre then
     begin
          if ( Length( sBusca ) = 10 ) OR ( Pos( '%', sBusca ) > 0 ) then
             sNombre := ZetaCommonTools.EntreComillas( sBusca )
          else if ( Length( sBusca ) = 9 ) then
             sNombre := ZetaCommonTools.EntreComillas( sBusca + '%' )
          else
              sNombre := ZetaCommonTools.EntreComillas( '%' + sBusca + '%' );
          sDescrip := ZetaCommonTools.EntreComillas( '%' );
     end
     else
     begin
          sNombre := ZetaCommonTools.EntreComillas( '%' );
          if ( Pos( '%', sBusca ) > 0 ) then
              sDescrip := ZetaCommonTools.EntreComillas( sBusca )
          else
              sDescrip := ZetaCommonTools.EntreComillas( '%' + sBusca + '%' );
     end;
     SetFiltro(Format( '( DI_NOMBRE LIKE %s) AND '+
                        '( ( UPPER( DI_TITULO ) LIKE %s) OR '+
                        '( UPPER( DI_CLAVES ) LIKE %s) ) AND '+
                        '( DI_CLASIFI IN ( %s ) AND '+
                        'DI_CALC <= 0 ) %s ' ,
                        [sNombre,sDescrip,sDescrip,sRelaciones,sOrden] ), 'DI_NOMBRE');
     cdsBuscaPorCampo.Data := cdsDiccion.Data;
end;
{$endif}


function TdmBaseDiccionario.GetConfidencial: Boolean;
begin
     if NOT lRevisaConfidencial then
     begin
          lVerConfidencial := ChecaDerechoConfidencial;
          lRevisaConfidencial := TRUE;
     end;
     Result := lVerConfidencial;
end;


{$ifdef RDD}
procedure TdmBaseDiccionario.GetListaTablas( const oEntidad : TipoEntidad;
                                             oLista: TStrings;
                                             const lTodos, lConRelaciones: Boolean);
 var oContainer : TCampoMaster;
begin
     ConectarRelaciones( oEntidad );

     with cdsRelaciones do
     begin
          First;
          while NOT EOF do
          begin
               if lConRelaciones then
               begin
                    if StrLleno(FieldByName( 'EN_TABLA' ).AsString) then
                    begin
                         oContainer := TCampoMaster.Create;
                         with oContainer do
                         begin
                              Formula := FieldByName( 'EN_TABLA' ).AsString;
                              Titulo := FieldByName( 'EN_TITULO' ).AsString;
                              Entidad := TipoEntidad( FieldByName( 'EN_CODIGO' ).AsInteger );
                              oLista.AddObject( ZetaCommonTools.Padr( Formula, 9 ) + ': ' + Titulo, oContainer );
                         end;
                    end;
               end
               else
               begin
                    if ( TipoEntidad( FieldByName( 'EN_CODIGO' ).AsInteger) = oEntidad ) then
                    begin
                         if StrLleno(FieldByName( 'EN_TABLA' ).AsString) then
                         begin
                              oContainer := TCampoMaster.Create;
                              with oContainer do
                              begin
                                   Formula := FieldByName( 'EN_TABLA' ).AsString;
                                   Titulo := FieldByName( 'EN_TITULO' ).AsString;
                                   Entidad := TipoEntidad( FieldByName( 'EN_CODIGO' ).AsInteger );
                                   oLista.AddObject( ZetaCommonTools.Padr( Formula, 9 ) + ': ' + Titulo, oContainer );
                              end;
                         end;
                    end;
               end;
               Next;
          end;
     end;
end;

procedure TdmBaseDiccionario.ConectarRelaciones(const oEntidad : TipoEntidad);
begin
     with cdsRelaciones do
     begin
          IndexFieldNames := 'EN_TABLA';
          if NOT Active or NOT Locate('TABLAPRINCIPAL',Ord(oEntidad),[]) then
             with dmCliente do
                  Data := ServerDiccionario.GetRelaciones( Empresa,  Ord(oEntidad) );

     end;
end;

function TdmBaseDiccionario.EntidadValida( const NuevaEntidad, EntidadActual: TipoEntidad ) : Boolean;
begin
     ConectarRelaciones(NuevaEntidad);
     with cdsRelaciones do
          Result := Locate('EN_CODIGO',EntidadActual, []);
end;

procedure TdmBaseDiccionario.ConectarDatosDefault(const oEntidad : TipoEntidad);
begin
     with cdsDatosDefault do
     begin
          if NOT Active or NOT Locate('TABLAPRINCIPAL',Ord(oEntidad),[]) then
             with dmCliente do
                  Data := ServerDiccionario.GetDatosDefault( Empresa, Ord(oEntidad) );
     end;
end;

{$else}
procedure TdmBaseDiccionario.GetListaTablas( const oEntidad : TipoEntidad;
                                             oLista: TStrings;
                                             const lTodos, lConRelaciones: Boolean);
 var oContainer : TCampoMaster;
     sRelaciones : string;
begin
     if lConRelaciones then sRelaciones := GetRelaciones(TipoEntidad(oEntidad))
     else sRelaciones := IntToStr( Ord( oEntidad ) );

     if lTodos then
        sRelaciones := sRelaciones + ',' + IntToStr( Ord(enFunciones) );

     SetFiltro(Format( '(DI_CLASIFI = -1) AND (DI_CALC IN ( %s ))', [sRelaciones] ), 'DI_NOMBRE');
     with cdsDiccion do
          while NOT EOF do
          begin
               oContainer := TCampoMaster.Create;
               with oContainer do
               begin
                    Formula := FieldByName( 'DI_NOMBRE' ).AsString;
                    Titulo := FieldByName( 'DI_TITULO' ).AsString;
                    Entidad := TipoEntidad( FieldByName( 'DI_CALC' ).AsInteger );
                    oLista.AddObject( ZetaCommonTools.Padr( Formula, 9 ) + ': ' + Titulo, oContainer );
               end;
               Next;
          end;
end;
function TdmBaseDiccionario.BuscarTabla(const iEntidad: TipoEntidad): string;
begin
     with cdsDiccion do
     begin
          SetFiltro(Format('DI_CLASIFI = -1 and DI_CALC = %d',[Ord(iEntidad)] ), '');
          Result := FieldByName('DI_REQUIER').AsString;
     end;
end;
{$endif}



function TdmBaseDiccionario.GetCampoDescripcion( const iEntidad : TipoEntidad ): string;
{$ifdef RDD}
{$else}
  var iPos : integer;
{$endif}
begin
     {$ifdef RDD}
     with cdsCamposPorTabla do
          Result := FieldByName('EN_TABLA').AsString + '.' + FieldByName('EN_ATDESC').AsString;
     {$else}
     Result := BuscarTabla(iEntidad);
     iPos := Pos( ';', Result );

     if iPos > 0 then
        Result := Copy( Result, iPos+1, MaxInt );

     iPos := Pos( ';', Result );

     if iPos > 0 then
        Result := Copy( Result, 1, iPos );

     iPos := Pos( ';', Result );

     if iPos > 0 then
        Result := Copy( Result, 1, iPos-1 );
     {$endif}
end;

procedure TdmBaseDiccionario.cdsFuncionesAlAdquirirDatos(Sender: TObject);
begin
     cdsFunciones.Data:= ServerDiccionario.GetListaFunciones(dmCliente.Empresa);
end;

procedure TdmBaseDiccionario.cdsGlobalAlAdquirirDatos(Sender: TObject);
begin
     cdsGlobal.Data:= ServerDiccionario.GetListaGlobal(dmCliente.Empresa);
end;

procedure TdmBaseDiccionario.ConectaGlobal;
begin
     cdsGlobal.Conectar;
end;

procedure TdmBaseDiccionario.ConectaDiccion;
begin
     cdsDiccion.Conectar;
end;

procedure TdmBaseDiccionario.ConectaDiccion( cdsDataSet: TZetaClientDataSet );
begin
     ConectaDiccion;
end;

procedure TdmBaseDiccionario.SetFiltro( const sFiltrar, sIndice: string);
begin
     cdsDiccion.Close;
     sFiltro := ConcatFiltros(sFiltrar, sFiltroConfi );
     ConectaDiccion;
     cdsDiccion.IndexFieldNames := sIndice;
end;

procedure TdmBaseDiccionario.cdsDiccionAlAdquirirDatos(Sender: TObject);
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     cdsDiccion.Data := ServerDiccionario.GetDiccionario( dmCliente.Empresa, GetConfidencial, sFiltro );
     Screen.Cursor := oCursor;
end;

function TdmBaseDiccionario.SQLValido( var sFormula : string;
                                       const EntidadActiva : TipoEntidad ) : Boolean;
 var
     sError : WideString;
begin
     Result := TRUE;
     if strLleno( sFormula ) then
     begin
          {$ifdef ADUANAS}
          raise Exception.Create('No existe SQLValido para Comercio Exterior');
          //Result := dmReportes.PruebaFormula( Parametros, sTexto, EntidadActiva, oParams );
          {$else}
          Result := ServerDiccionario.SQLValido( dmCliente.Empresa, sFormula, Ord(EntidadActiva),sError );
          if NOT Result then
             sFormula := sFormula + CR_LF + sError;
          {$endif}
     end;
end;

function TdmBaseDiccionario.PruebaFormula( var sFormula : string;
                                           const EntidadActiva : TipoEntidad;
                                           const eTipoEv : ETipoEvaluador;
                                           const oParametros : TStrings ) : Boolean;
 var Parametros : TZetaParams;
     sTexto, sError : WideString;
     oSQLAgente : TSQLAgenteClient;
     oParams : OleVariant;
begin
     Result := TRUE;
     if strLleno( sFormula ) then
     begin
          Parametros := TZetaParams.Create;
          dmCliente.CargaActivosTodos(Parametros);
          Parametros.AddInteger('TipoEvaluador',Ord(eTipoEv));
          sTexto := sFormula;
          if (oParametros <> NIL) and (oParametros.Count > 0) then
          begin
               oSQLAgente := TSQLAgenteClient.Create;
               try
                  Result := dmReportes.EvaluaParametros( oSQLAgente, oParametros, sError, oParams, FALSE );
               finally
                      oSQLAgente.Free;
               end;
          end;
          if Result then
             {$ifdef ADUANAS}
             Result := dmReportes.PruebaFormula( Parametros, sTexto, EntidadActiva, oParams );
             {$else}
             Result := ServerDiccionario.PruebaFormula( dmCliente.Empresa, Parametros.VarValues,
                                                        sTexto, Ord(EntidadActiva), oParams );
             {$endif}
          sFormula := sTexto;
     end;
end;

{$ifdef RDD}

procedure TdmBaseDiccionario.SetFiltroCamposDefault(const lSetFiltro: Boolean = TRUE );
begin
     with cdsDatosDefault do
     begin
          Filter := 'TIPO=2';
          Filtered := lSetFiltro;
     end;
end;

procedure TdmBaseDiccionario.SetFiltroCamposOrden(const lSetFiltro: Boolean = TRUE );
begin
     with cdsDatosDefault do
     begin
          Filter := 'TIPO=1';
          Filtered := lSetFiltro;
     end;
end;

procedure TdmBaseDiccionario.SetFiltroCamposFiltro(const lSetFiltro: Boolean = TRUE );
begin
     with cdsDatosDefault do
     begin
          Filter := 'TIPO=3';
          Filtered := lSetFiltro;
     end;
end;



{$else}
//Esta llamada se cambia a CAmposDefault;
function TdmBaseDiccionario.GetRequeridos(const oEntidad : TipoEntidad ) : string;
begin
     with cdsDiccion do
     begin
          {Filtered := FALSE;
          if Locate('DI_CLASIFI;DI_CALC', VarArrayOf([-1,Ord(oEntidad)]), [] ) then
             Result := FieldByName('DI_REQUIER').AsString;}

          SetFiltro(Format('DI_CLASIFI = %d AND DI_CALC = %d', [-1,Ord(oEntidad)]), 'DI_NOMBRE' );

          Result := FieldByName('DI_REQUIER').AsString;
     end;
end;

{$endif}

procedure TdmBaseDiccionario.GetListaFunciones;
begin
     cdsFunciones.Conectar;
     cdsFunciones.INdexFieldNames := 'DI_NOMBRE';
     cdsFunciones.First;
end;

procedure TdmBaseDiccionario.CamposPorEntidad(Entidad: TipoEntidad; lTodos: Boolean; oLista: TStrings);
var
   oCampo: TObjetoString;
   sIndex : string;
begin
     oLista.Clear;
     GetListaDatosTablas( Entidad, lTodos );
     with cdsBuscaPorTabla do
     begin
          sIndex := IndexFieldNames;
          {$ifdef RDD}
          IndexFieldNames := 'AT_TITULO';
          {$else}
          IndexFieldNames := 'DI_TITULO';
          {$endif}
          try
             First;
             while NOT EOF do
             begin
                  oCampo := TObjetoString.Create;
                  {$ifdef RDD}
                  oCampo.Campo := FieldByName( 'AT_CAMPO' ).AsString;
                  oCampo.Entidad := TipoEntidad( FieldByName('EN_CODIGO').AsInteger );
                  oLista.AddObject( FieldByName( 'AT_TITULO' ).AsString, oCampo );
                  {$else}
                  oCampo.Campo := FieldByName( 'DI_NOMBRE' ).AsString;
                  oLista.AddObject( FieldByName( 'DI_TITULO' ).AsString, oCampo );
                  {$endif}

                  Next;
             end;
          finally
                 IndexFieldNames := sIndex;
          end;
     end;

end;

procedure TdmBaseDiccionario.CamposPorEntidad(Entidad: TipoEntidad; lTodos: Boolean; oLista: TcxCheckListBox);
var
   oCampo: TObjetoString;
   sIndex : string;
begin
     oLista.items.Clear;
     GetListaDatosTablas( Entidad, lTodos );
     with cdsBuscaPorTabla do
     begin
          sIndex := IndexFieldNames;
          {$ifdef RDD}
          IndexFieldNames := 'AT_TITULO';
          {$else}
          IndexFieldNames := 'DI_TITULO';
          {$endif}
          try
             First;
             while NOT EOF do
             begin
                  oCampo := TObjetoString.Create;
                  {$ifdef RDD}
                  oCampo.Campo := FieldByName( 'AT_CAMPO' ).AsString;
                  oCampo.Entidad := TipoEntidad( FieldByName('EN_CODIGO').AsInteger );
                  with oLista.Items.Add do
                  begin
                  text := FieldByName( 'AT_TITULO' ).AsString ;
                  Tag:=integer( oCampo );
                  end;
                  {$else}
                  oCampo.Campo := FieldByName( 'DI_NOMBRE' ).AsString;
                  with oLista.items.Add do
                  begin
                  text := FieldByName( 'DI_TITULO' ).AsString;
                  Tag := integer(oCampo);
                  end;
                  {$endif}
                  Next;
             end;
          finally
                 IndexFieldNames := sIndex;
          end;
     end;

end;



procedure TdmBaseDiccionario.CambioGlobales;
begin
     cdsDiccion.Refrescar;
     SetLookupNames;
end;

procedure TdmBaseDiccionario.SetLookupNames;
begin

end;

procedure TdmBaseDiccionario.GetListaClasifiFavoritos( oLista : TStrings; const lMigracion : Boolean );
begin
     with oLista do
     begin
          Clear;
          //if ((dmCliente.GetDatosUsuarioActivo.Vista = tvClasica) or (Dmcliente.ModoSuper)) then
          {$IFDEF TRESS_DELPHIXE5_UP}
          if ( Dmcliente.ModoSuper) then
          {$ENDIF}
          begin
               AddObject( K_MIS_FAVORITOS_DES, TObject( crFavoritos ));
               AddObject( K_MIS_SUSCRIPCIONES_DES, TObject( crSuscripciones ));
          end;
          GetListaClasifiModulo( oLista, lMigracion );
     end;
end;

{$ifdef RDD}

{$else}
function TdmBaseDiccionario.GetNOClasificacion( const lFavoritos, lMigracion, lSuscripciones : Boolean ): ListaClasificaciones;
begin
     Result := [];
     if NOT lFavoritos or NOT lSuscripciones then
        Result := Result + [crFavoritos ]+ [crSuscripciones];
end;
{$endif}

function TdmBaseDiccionario.ValidaModulo(const eClasifi: eClasifiReporte):Boolean;
begin
     Result := TRUE;
    
end;

{$ifdef RDD}

{$else}
procedure TdmBaseDiccionario.AgregaClasifi( oLista: TStrings; const eClasifi: eClasifiReporte; const lValidaDerecho: Boolean= FALSE );
begin
     if NOT lValidaDerecho OR (  ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION ) OR
                                 ( ZAccesosMgr.CheckDerecho( GetDerechosClasifi( eClasifi ), K_DERECHO_CONSULTA ) AND
                                   ValidaModulo( eClasifi  ) ) ) then
     begin
          oLista.AddObject( GetDescrip( eClasifi ), TObject( eClasifi ) );
     end;
end;

function TdmBaseDiccionario.GetDescrip( const eTipo : eClasifiReporte ) : String;
begin
     Result := ObtieneElemento( lfClasifiReporte, Ord(eTipo) );
end;
{$endif}

procedure TdmBaseDiccionario.GetListaClasifiModulo( oLista : TStrings; const lMigracion : Boolean );
begin
     {$ifdef RDD}
     with cdsClasificaciones do
     begin
          Conectar;
          First;
          while NOT EOF do
          begin
               oLista.AddObject( FieldByName('RC_NOMBRE').AsString, TObject( FieldByName('RC_CODIGO').AsInteger ) );
               Next;
          end;
     end;
     {$else}
     {$endif}
end;

procedure TdmBaseDiccionario.GetListaClasifi( oLista : TStrings; const lMigracion : Boolean );
begin
     with oLista do
     begin
          Clear;
          GetListaClasifiModulo( oLista, lMigracion );
     end;
end;

function TdmBaseDiccionario.GetPosClasifi( oLista: TStrings; const eTipo: eClasifiReporte ):Integer;
begin
     Result := GetPosClasifi( oLista, eTipo, FALSE );
end;

function TdmBaseDiccionario.GetPosClasifi( oLista: TStrings; const eTipo: eClasifiReporte; lIncluyeFavoritos: boolean ):Integer;
 var
    i: integer;
begin
     Result := -1;
     for i:= 0 to oLista.Count -1 do
     begin
          if eClasifiReporte( oLista.Objects[i] ) = eTipo then
          begin
               Result := i;
               Break;
          end;
     end;
end;

procedure TdmBaseDiccionario.RefrescaFormaConsulta(const oEntidad:TipoEntidad);
 var iEntidad : integer;
begin
     if oEntidad = enNinguno then iEntidad := -1
     else iEntidad := Ord(oEntidad);
     SetFiltro(Format('DI_CLASIFI =%d',[iEntidad]), 'DI_NOMBRE');
end;

procedure TdmBaseDiccionario.GetTablas(oLista: TStrings);
 var oNumero : TObjetoEntidad;
begin
     oNumero := TObjetoEntidad.Create;
     oNumero.Entidad := enNinguno;
     oLista.AddObject( '< Lista de Tablas >', oNumero );
     SetFiltro(Format('DI_CLASIFI =%d',[-1]), 'DI_NOMBRE');
     with cdsDiccion do
          while NOT EOF do
          begin
               oNumero := TObjetoEntidad.Create;
               oNumero.Entidad := TipoEntidad( FieldByName('DI_CALC').AsInteger );
               oLista.AddObject( FieldByName('DI_NOMBRE').AsString + ':' +
                                 FieldByName('DI_TITULO').AsString,
                                 oNumero );
               Next;
          end;
end;

function TdmBaseDiccionario.GetValorActivo(const eValor: {$ifdef RDD}eRDDValorActivo{$else}string{$endif}): string;
begin
     Result := VACIO;
end;

procedure TdmBaseDiccionario.cdsClasificacionesAlAdquirirDatos( Sender: TObject);
begin
     {$ifdef RDD}
     with dmCliente do
          cdsClasificaciones.Data := ServerDiccionario.GetListaClasifi( Empresa );
                                                                        //dmSistema.GetEmpresaAccesos
     {$else}
     {$endif}
end;

procedure TdmBaseDiccionario.ConectaClasificacionesGrupoAdmin( oEmpresa: OleVariant );
begin
     {$ifdef RDD}
//     with dmCliente do
//          oEmpresa := Empresa;
     oEmpresa[P_GRUPO] := D_GRUPO_SIN_RESTRICCION;
     cdsClasificaciones.Data := ServerDiccionario.GetListaClasifi( oEmpresa );
     {$else}
     {$endif}
end;

procedure TdmBaseDiccionario.ConectaEntidadesGrupo( oEmpresa: OleVariant );
begin
     {$ifdef RDD}
          cdsModulos.Data := ServerDiccionario.GetModulos(oEmpresa);
     {$endif}
end;

procedure TdmBaseDiccionario.cdsTablasPorClasificacionAlAdquirirDatos( Sender: TObject);
begin
     {$ifdef RDD}
     with dmCliente do
          cdsTablasPorClasificacion.Data := ServerDiccionario.GetTablasPorClasifi( Empresa, 0 );
     {$else}
     {$endif}
end;

procedure TdmBaseDiccionario.cdsClasifiDerechosAlAdquirirDatos(Sender: TObject);
begin
     {$ifdef RDD}
     with dmCliente do
          cdsClasifiDerechos.Data := ServerDiccionario.GetDerechosClasifi(Empresa, GetGrupoActivo);
     {$else}
     {$endif}
end;


procedure TdmBaseDiccionario.cdsLookupGeneralAlAdquirirDatos(Sender: TObject);
 const
      K_CAMPO_TP_NIVEL0 = 3;
 var
    sDatosLookup: WideString;


 function GetSiguiente: String;
  var
    iPos: Word;
  begin
       iPos := Pos( ',', sDatosLookup );
       Result := Copy( sDatosLookup, 0, ( iPos - 1 ) );
       sDatosLookup := Copy( sDatosLookup, ( iPos + 1 ), MaxInt );
  end;
begin
     {$ifdef RDD}
     with dmCliente do
          cdsLookupGeneral.Data := ServerDiccionario.GetTablaGenerica( Empresa, cdsLookupGeneral.Tag, sDatosLookup );
     {$else}
     {$endif}

     if StrLleno( sDatosLookup ) then
     begin
          if ( FListaTemporal = NIL ) then
             FListaTemporal := TStringList.Create;
          FListaTemporal.CommaText := sDatosLookup;

          with cdsLookupGeneral do
          begin
               LookupName := FListaTemporal[0];
               LookupDescriptionField := FListaTemporal[1];
               LookupKeyField := FListaTemporal[2];
               {$IF (not Defined(VISITANTES) ) and ( not Defined(VISITANTES_MGR) ) and ( not Defined(SELECCION) ) and ( not Defined(WORKFLOWCFG) ) and ( not Defined(KIOSCOCFG) ) }
               if ( FListaTemporal.Count > K_CAMPO_TP_NIVEL0 ) and (  TipoEntidad(cdsLookupGeneral.Tag) = enTPeriodo ) then
                  LookupConfidenField := FListaTemporal[K_CAMPO_TP_NIVEL0];
               {$ifend}
          end;
     end;

end;

procedure TdmBaseDiccionario.cdsRelacionesAlAdquirirDatos(Sender: TObject);
begin
end;


//Funciones que eran Virtual;Abstract
function TdmBaseDiccionario.GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;
begin
     {$ifdef RDD}
     if ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION ) then
        Result := K_SIN_RESTRICCION
     else
     begin
          Result := 0;
          with cdsClasifiDerechos do
          begin
               Conectar;
               if Locate('RC_CODIGO', eClasifi, [] ) then
                  Result := FieldByName('RA_DERECHO').AsInteger;
          end;
     end;
     {$else}
     Result := 0;
     {$endif}

end;

function TdmBaseDiccionario.ChecaDerechoConfidencial: Boolean;
begin
     Result := TRUE;
end;

procedure TdmBaseDiccionario.LlenaLista( Tipo: integer; Lista: TStrings; const LeerValoresSistema : Boolean = TRUE );
begin
     Lista.Clear;
     if LeerValoresSistema and ( Tipo <= Ord( High(ListasFijas) ) ) then
     begin
          ZetaCommonLists.LlenaLista( ListasFijas( Tipo ), Lista );
     end
     else
     begin
          with cdsListasFijasValores do
          begin
               Conectar;
               Filter := Format( 'LV_CODIGO=%d', [Tipo] );
               IndexFieldNames := 'LV_CODIGO;VL_CODIGO';
               Filtered := TRUE;
               First;
               while NOT EOF do
               begin
                    Lista.Add(FieldByName('VL_DESCRIP').AsString);
                    Next;
               end;
               Filtered := FALSE;
          end;
     end;
end;


procedure TdmBaseDiccionario.cdsModulosAlAdquirirDatos(Sender: TObject);
begin
     {$ifdef RDD}
     with dmCliente do
          cdsModulos.Data := ServerDiccionario.GetModulos(Empresa);
          cdsModulosLookup.Data := cdsModulos.Data;
     {$else}
     {$endif}
end;

procedure TdmBaseDiccionario.cdsListasFijasAlAdquirirDatos( Sender: TObject);
begin
     {$ifdef RDD}
     cdsListasFijas.Data := ServerDiccionario.GetListasFijas(dmCliente.Empresa);
     {$else}
     {$endif}
end;

procedure TdmBaseDiccionario.cdsListasFijasValoresAlAdquirirDatos(Sender: TObject);
begin
     {$ifdef RDD}
     cdsListasFijasValores.Data := ServerDiccionario.GetListasFijasValores(dmCliente.Empresa);
     {$else}
     {$endif}
end;

function TdmBaseDiccionario.RecordSistema( Dataset: TZetaLookupDataSet ): Boolean;
begin
     with Dataset do
          Result := ( State in [ dsBrowse, dsEdit ]) and CodigoSistema( FieldByName(LookupKeyField).AsInteger )
end;

function TdmBaseDiccionario.CodigoSistema( const iCodigo: integer ): Boolean;
begin
     Result := ( iCodigo <= K_DICCION_SISTEMA );
end;

procedure TdmBaseDiccionario.cdsCamposPorTablaAlCrearCampos(Sender: TObject);
begin
     with cdsCamposPorTabla do
     begin
          ListaFija( 'AT_TIPO', lfTipoGlobal );
     end;
end;

procedure TdmBaseDiccionario.cdsEntidadesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifdef RDD}
     //cdsEntidades.Data := ServerDiccionario.GetEntidades( dmSistema.GetEmpresaAccesos );
     cdsEntidades.Data := ServerDiccionario.GetEntidades( dmCliente.Empresa );
     {$else}
     {$endif}

     cdsEntidadesLookup.Data := cdsEntidades.Data;
end;

procedure TdmBaseDiccionario.cdsEntidadesNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsEntidades do
     begin
          FieldByName('EN_ALIAS').AsInteger := 0;
          FieldByName('EN_ACTIVO').AsString := K_GLOBAL_SI;
          FieldByName('EN_NIVEL0').AsString := K_GLOBAL_NO;
          {$ifdef TRESS}
          FieldByName('EN_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          {$endif}
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;

end;

procedure TdmBaseDiccionario.cdsEntidadesBeforePost(DataSet: TDataSet);
 procedure Valida( const sField, sDato: string);
 begin
      if StrVacio( Dataset.FieldByName(sField).AsString ) then
      begin
           ZError('Diccionario de Datos',Format( '%s de la Tabla no puede quedar vac�o',[sDato]),0);
           Dataset.FieldByName(sField).FocusControl;
           Abort;
      end;
 end;
begin
     inherited;
     with cdsEntidades do
     begin
          Valida( 'EN_TITULO', 'El t�tulo' );
          Valida( 'EN_TABLA', 'La tabla' );
          Valida( 'EN_DESCRIP', 'La descripci�n' );

          if StrVacio( FieldByName('EN_ATDESC').AsString ) then
             FieldByName('EN_ATDESC').AsString := ' ';
          if StrVacio( FieldByName('EN_PRIMARY').AsString ) then
             FieldByName('EN_PRIMARY').AsString := ' ';
     end;
      DataSet.FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
end;

procedure TdmBaseDiccionario.cdsEntidadesPorModuloAlAdquirirDatos(Sender: TObject);
begin
     {$ifdef TRESS}
      {$ifndef DOS_CAPAS}
//      cdsEntidadesPorModulo.Data := ServerDiccionario.EntidadesPorModulo( dmCliente.Empresa  );
        ConectaEntidadesPorModulo( dmCliente.Empresa );
      {$endif}
     {$endif}
end;

procedure TdmBaseDiccionario.ConectaEntidadesPorModulo( oEmpresa: OleVariant );
begin
     {$ifdef RDD}
      cdsEntidadesPorModulo.Data := ServerDiccionario.EntidadesPorModulo( oEmpresa );
     {$else}
     {$endif}
end;

{$ifdef RDD}
function TdmBaseDiccionario.ExisteEntidadEnTablasPOrClasificacion( const iEntidad: TipoEntidad ): Boolean;
begin
     Result := cdsTablasPorClasificacion.Locate( 'EN_CODIGO', ord(iEntidad) ,[]);
end;

function TdmBaseDiccionario.GetNombreTabla( const iEntidad: TipoEntidad ): string;
begin
     Result := cdsTablasPorClasificacion.FieldByName('EN_TABLA').AsString
end;

function TdmBaseDiccionario.GetEsTablaNivel0: string;
begin
     Result := cdsTablasPorClasificacion.FieldByName('EN_NIVEL0').AsString

end;
{$else}
function TdmBaseDiccionario.GetNombreTabla( const iEntidad: TipoEntidad ): string;
begin
     with cdsDiccion do
     begin
          SetFiltro(Format('DI_CLASIFI = -1 and DI_CALC = %d',[Ord(iEntidad)] ), '');
          Result := FieldByName('DI_NOMBRE').AsString;
     end;
end;
{$endif}
end.



