unit DDashlets;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient, Variants,
     ZetaClientDataSet,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     DCliente,
     Consultas_TLB;

type
  TdmDashleets = class(TDataModule)
    cdsQueryGeneral: TZetaClientDataSet;
  private
    { Private declarations }
    FSQLText: String;
    FServidor: IdmServerConsultasDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
  public
    { Public declarations }
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property SQLText: String read FSQLText write FSQLText;
    procedure GetDatosProcedure( const sQuery: String; var sError : String; const dFechaComponente: TDate; var oDatosGrafica: TZetaClientDataSet); //Regresa la consulta SQL
    function ObtenerQuery( const sQuery: String; var sError: String; const dFechaComponente: TDate ): String; //Sustituye el texto @Valor con el valor activo que le corresponde
    function SetSesionGraficas: Integer;
    function CreateTablerosCompany( var sError: String; iTableroID: Integer; iCompanyTableroOrden: Integer ): Integer;
    procedure SetRolTableros( var sError: String; iCompanyTableroID: Integer; sRO_CODIGO: String; lActivo: Boolean );
    function GetListaTableros( var sError: String; iSesionID: Integer ): OleVariant;
    function GetConsultaDashletsTablero( sError: String; iSesionID: Integer; iTableroID: Integer ): OleVariant;
    function EjecutarDashlet( sError: String; iSesionID: Integer; iDashletID: Integer ): OleVariant;
  end;

var
  dmDashleets: TdmDashleets;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress;
{$R *.DFM}

{ ************** TdmConsultas **************** }
{
 @EmpleadoActivo
 @SistemaFecha
 @SistemaYear
 @PeriodoActivoYear
 @PeriodoActivoTipo
 @PeriodoActivoNumero
 @PeriodoActivoAsiIni
 @PeriodoActivoAsiFin
}

function TdmDashleets.ObtenerQuery( const sQuery: String; var sError: String; const dFechaComponente: TDate ): String;
var
   sScript: String;
   dFechaAsistencia: TDate;
begin
     try
        if dFechaComponente = NullDateTime  then
           dFechaAsistencia := dmCliente.FechaAsistencia
        else
            dFechaAsistencia := dFechaComponente;
        sError := VACIO;
        sScript := StrTransAll( StrTransAll( StrTransAll( StrTransAll(  StrTransAll( StrTransAll(
        StrTransAll( StrTransAll( StrTransAll( StrTransAll( StrTransAll( sQuery ,
        '@EmpleadoActivo', IntToStr( dmCliente.Empleado ) ),
        '@SistemaFecha', DateToStrSQLClientC ( dmCliente.FechaDefault ) ),
        '@SistemaYear', IntToStr( dmCliente.YearDefault ) ),
        '@PeriodoActivoYear', IntToStr( dmCliente.GetDatosPeriodoActivo.Year ) ),
        '@PeriodoActivoTipo', IntToStr( Ord( dmCliente.GetDatosPeriodoActivo.Tipo ) ) ),
        '@PeriodoActivoNumero', IntToStr( dmCliente.GetDatosPeriodoActivo.Numero ) ),
        '@PeriodoActivoAsiIni', DateToStrSQLClientC( dmCliente.GetDatosPeriodoActivo.InicioAsis ) ),
        '@PeriodoFechaIni', DateToStrSQLClientC( dmCliente.GetDatosPeriodoActivo.Inicio ) ),
        '@PeriodoFechaFin', DateToStrSQLClientC( dmCliente.GetDatosPeriodoActivo.Fin ) ),
        '@FechaAsistencia', DateToStrSQLClientC( dFechaAsistencia ) ),
        '@Confidencialidad', UnaCOMILLA + dmCliente.Confidencialidad + UnaCOMILLA );
        Result := sScript;
     except
           on Error: Exception do
           begin
                sError := Error.Message;
                Result := VACIO;
           end;
     end;
end;

procedure TdmDashleets.GetDatosProcedure(const sQuery: String; var sError : String; const dFechaComponente: TDate; var oDatosGrafica: TZetaClientDataSet);
begin
     try
        sError := VACIO;
        FSQLText := ObtenerQuery(sQuery, sError, dFechaComponente);
        oDatosGrafica.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, FSQLText );
     except
           on Error: Exception do
           begin
                sError := Error.Message;
                oDatosGrafica := nil;
           end;
     end;
end;

function TdmDashleets.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServidor ) );
end;


function TdmDashleets.SetSesionGraficas: Integer;
var
   oParametros: TZetaParams;
   ErrroCount: Integer;
   sError: String;
begin
     try
        try
           sError := VACIO;
           ErrroCount := 0;
           oParametros:= TZetaParams.Create;
           with oParametros do
           begin
                AddInteger('SesionIDActual', dmCliente.SesionIDDashlet);
                AddInteger('US_CODIGO', dmCliente.Usuario);
                AddString('CM_CODIGO', dmCliente.Compania);
                AddInteger('PE_TIPO', Ord( dmCliente.GetDatosPeriodoActivo.Tipo ) );
                AddInteger('PE_YEAR', dmCliente.GetDatosPeriodoActivo.Year);
                AddInteger('PE_NUMERO', dmCliente.GetDatosPeriodoActivo.Numero);
                AddDate('FechaActiva', dmCliente.FechaAsistencia);
           end;
           Result := ServerConsultas.SetSesionGraficas( dmCliente.Empresa, oParametros.VarValues, ErrroCount  );
        except
              on Error: Exception do
              begin
                   sError := Error.Message;
                   Result := 0;
              end;
        end;
     finally
            begin
                 freeAndNil( oParametros );
            end;
     end;
end;

function TdmDashleets.CreateTablerosCompany( var sError: String; iTableroID: Integer; iCompanyTableroOrden: Integer ): Integer;
var
   oParametros: TZetaParams;
begin
     try
        try
           sError := VACIO;
           oParametros:= TZetaParams.Create;
           with oParametros do
           begin
                AddString('CM_CODIGO', dmCliente.Compania);
                AddInteger('TableroID', iTableroID );
                AddInteger('CompanyTableroOrden', iCompanyTableroOrden);
           end;
               Result := ServerConsultas.CreateTablerosCompany( dmCliente.Empresa, oParametros.VarValues );
           except
                 on Error: Exception do
                 begin
                      Result := 0;
                      sError := Error.Message;
                 end;
           end;
     finally
            begin
                 freeAndNil( oParametros );
            end;
     end;
end;

procedure TdmDashleets.SetRolTableros( var sError: String; iCompanyTableroID: Integer; sRO_CODIGO: String; lActivo: Boolean );
var
   oParametros: TZetaParams;
   ErrroCount: Integer;
begin
     try
        try
           sError := VACIO;
           ErrroCount := 0;
           oParametros:= TZetaParams.Create;
           with oParametros do
           begin
                AddString('CM_CODIGO', dmCliente.Compania );
                AddInteger('CompanyTableroID', iCompanyTableroID );
                AddString('RO_CODIGO', sRO_CODIGO);
                AddBoolean('Activo',lActivo);
           end;
           ServerConsultas.SetRolTableros( dmCliente.Empresa, oParametros.VarValues, ErrroCount );
        except
              on Error: Exception do
              begin
                   sError := Error.Message;
              end;
        end;
     finally
            begin
                 freeAndNil( oParametros );
            end;
     end;
end;

function TdmDashleets.GetListaTableros( var sError: String; iSesionID: Integer ): OleVariant;
var
   oParametros: TZetaParams;
   ErrroCount: Integer;
begin
     try
        try
           sError := VACIO;
           Result := 0;
           ErrroCount := 0;
           oParametros:= TZetaParams.Create;
           with oParametros do
           begin
                AddInteger('SesionID', iSesionID );
           end;
               Result := ServerConsultas.GetListaTableros( dmCliente.Empresa, oParametros.VarValues, ErrroCount );
        except
              on Error: Exception do
              begin
                   sError := Error.Message;
              end;
        end;
     finally
            begin
                 freeAndNil( oParametros );
            end;
     end;
end;

function TdmDashleets.GetConsultaDashletsTablero( sError: String; iSesionID: Integer; iTableroID: Integer ): OleVariant;
var
   oParametros: TZetaParams;
   ErrroCount: Integer;
begin
     try
        try
           sError := VACIO;
           Result := 0;
           ErrroCount := 0;
           oParametros:= TZetaParams.Create;
           with oParametros do
           begin
                AddInteger('SesionID', iSesionID );
                AddInteger('TableroID', iTableroID );
           end;
           Result := ServerConsultas.GetConsultaDashletsTablero( dmCliente.Empresa, oParametros.VarValues, ErrroCount );
        except
              on Error: Exception do
              begin
                   sError := Error.Message;
              end;
        end;
     finally
            begin
                 freeAndNil( oParametros );
            end;
     end;
end;

function TdmDashleets.EjecutarDashlet( sError: String; iSesionID: Integer; iDashletID: Integer ): OleVariant;
var
   oParametros: TZetaParams;
   ErrroCount: Integer;
begin
     try
        try
           sError := VACIO;
           Result := 0;
           ErrroCount := 0;
           oParametros:= TZetaParams.Create;
           with oParametros do
           begin
                AddInteger('SesionID', iSesionID );
                AddInteger('DashletID', iDashletID );
           end;
           Result := ServerConsultas.EjecutarDashlet( dmCliente.Empresa, oParametros.VarValues, ErrroCount );
        except
              on Error: Exception do
              begin
                   sError := Error.Message;
              end;
        end;
     finally
            begin
                 freeAndNil( oParametros );
            end;
     end;
end;

end.
