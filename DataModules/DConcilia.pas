unit DConcilia;

{.$define NO_FECHA_INC}    {ER:06/Oct/2010: Fonacot vuelve a pedir fechas de Incapacidad}

interface

uses
  SysUtils, Classes, DB, DBClient, ZetaClientDataSet,ZetaCommonLists,
  ZetaServerDataSet,ZetaCommonClasses,ZetaASCIIFile,Forms,Controls
  , Super_TLB, Recursos_TLB, DConsultas
   ;

{ Cr�dito Fonacot
  Clase que maneja el registro de cada credito importado de la Cedula de Fonacot
}
type
    TCreditoFonacot = class( TObject )
    private
     FNumFonacot :string;
     FNumEmpleado :string;
     FNomCompleto :string;
     FRFC :string;
     FNumCredito :string;
     FMontoMensual: Currency;
     FPlazo: Smallint;
     FMeses :Smallint;
     FPosicion :Integer;
     FMontoPrestado: Currency;
     FReubicado: string;
     FDiferencia: Currency;
    public
     constructor Create;
     procedure SetCreditoFromCedula( oDataSet:TZetaClientDataSet );
     property NumFonacot :string read FNumFonacot write FNumFonacot;
     property NumEmpleado :string read FNumEmpleado write FNumEmpleado;
     property NomCompleto :string read FNomCompleto write FNomCompleto;
     property RFC :string read FRFC write FRFC;
     property NumCredito :string read FNumCredito write FNumCredito;
     property MontoMensual :Currency read FMontoMensual write FMontoMensual;
     property Plazo :Smallint read FPlazo write FPlazo;
     property Meses :Smallint read FMeses write FMeses;
     property Posicion:Integer read FPosicion write FPosicion;
     property MontoPrestado: Currency read FMontoPrestado write FMontoPrestado;
     property Reubicado: string read FReubicado write FReubicado;
     property Diferencia: Currency read FDiferencia write FDiferencia;

  end;

{Modulo}
type
  TdmConcilia = class(TDataModule)
    cdsPrestamos: TZetaClientDataSet;
    cdsCedulas: TZetaClientDataSet;
    cdsEmpleados: TZetaClientDataSet;
    cdsEmpCreditos: TServerDataSet;
    cdsCreditosActivos: TServerDataSet;
    cdsEmpCreditosNFonacot: TStringField;
    cdsEmpCreditosNEmpleado: TIntegerField;
    cdsEmpCreditosNomCompleto: TStringField;
    cdsEmpCreditosRFC: TStringField;
    cdsEmpCreditosCredTress: TSmallintField;
    cdsEmpCreditosCredCedulas: TSmallintField;
    cdsCreditosActivosNomCompleto: TStringField;
    cdsCreditosActivosRFC: TStringField;
    cdsCreditosActivosNCredito: TStringField;
    cdsCreditosActivosMPrestado: TFloatField;
    cdsCreditosActivosRMensual: TFloatField;
    cdsCreditosActivosSaldo: TFloatField;
    cdsCreditosActivosNumEmpleado: TIntegerField;
    cdsImportPrestamos: TServerDataSet;
    cdsImportKardex: TServerDataSet;
    cdsEmpleaFonacot: TZetaClientDataSet;
    cdsCreditosNuevos: TZetaClientDataSet;
    cdsEmpleaFonacotNomCompleto: TStringField;
    cdsEmpleaFonacotNomTress: TStringField;
    cdsEmpleaFonacotRFC: TStringField;
    cdsEmpleaFonacotNFonacot: TStringField;
    cdsCreditosNuevosNFonacot: TStringField;
    cdsCreditosNuevosNEmpleado: TIntegerField;
    cdsCreditosNuevosNomCompleto: TStringField;
    cdsCreditosNuevosRFC: TStringField;
    cdsCreditosNuevosNCredito: TStringField;
    cdsCreditosNuevosMMensual: TFloatField;
    cdsCreditosNuevosPagos: TFloatField;
    cdsCreditosNuevosMeses: TIntegerField;
    cdsCreditosNuevosMontoTotal: TFloatField;
    cdsAscii: TZetaClientDataSet;
    cdsAsciiRenglon: TStringField;
    cdsCreditosNuevosPosicion: TIntegerField;
    cdsCreditosNoActivos: TServerDataSet;
    StringField3: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    cdsCreditosNoActivosFechaInicial: TDateField;
    cdsCreditosNoActivosStatus: TStringField;
    cdsCreditosNoActivosTotalPrestamo: TFloatField;
    cdsCreditosNoActivosPagos: TIntegerField;
    cdsEmpleaFonacotPosicion: TIntegerField;
    cdsCreditosNoActivosPosicion: TIntegerField;
    cdsEmpleaFonacotStatus: TStringField;
    cdsEmpleaFonacotNEmpleado: TIntegerField;
    cdsActualizaPrestamos: TServerDataSet;
    cdsCreditosDiferencias: TZetaClientDataSet;
    cdsCreditosDiferenciasNFonacot: TStringField;
    cdsCreditosDiferenciasNEmpleado: TIntegerField;
    cdsCreditosDiferenciasNomCompleto: TStringField;
    cdsCreditosDiferenciasNCredito: TStringField;
    cdsCreditosDiferenciasCRetencion: TFloatField;
    cdsCreditosDiferenciasPosicion: TIntegerField;
    cdsCreditosDiferenciasMTress: TFloatField;
    cdsCreditosDiferenciasCPlazo: TIntegerField;
    cdsCreditosDiferenciasCMeses: TIntegerField;
    cdsCreditosDiferenciasTressPlazo: TIntegerField;
    cdsCreditosDiferenciasTressMeses: TIntegerField;
    cdsCreditosDiferenciasPR_SALDO_I: TFloatField;
    cdsCreditosDiferenciasPR_FECHA: TDateField;
    cdsCreditosDiferenciasPR_MONTO: TFloatField;
    cdsCreditosDiferenciasPR_FORMULA: TStringField;
    cdsCreditosDiferenciasPR_SUB_CTA: TStringField;
    cdsCreditosDiferenciasPR_MONTO_S: TFloatField;
    cdsCreditosDiferenciasPR_TASA: TFloatField;
    cdsCreditosDiferenciasPR_INTERES: TFloatField;
    cdsRetCreditos: TZetaClientDataSet;
    cdsTotales: TZetaClientDataSet;
    cdsConfrontacion: TZetaClientDataSet;
    cdsConfrontacionNFonacot: TStringField;
    cdsConfrontacionNEmpleado: TIntegerField;
    cdsConfrontacionNomCompleto: TStringField;
    cdsConfrontacionRFC: TStringField;
    cdsConfrontacionNCredito: TStringField;
    cdsConfrontacionRetCedula: TFloatField;
    cdsConfrontacionRetTress: TFloatField;
    cdsConfrontacionIncidencia: TStringField;
    cdsConfrontacionFecInicio: TDateField;
    cdsConfrontacionFecFin: TDateField;
    cdsConfrontacionPlazo: TIntegerField;
    cdsConfrontacionMeses: TIntegerField;
    cdsAuxiliar: TZetaClientDataSet;
    cdsEmpleadosFonacot: TZetaClientDataSet;
    cdsPrestamosAltas: TZetaClientDataSet;
    cdsProcesos: TZetaClientDataSet;
    cdsLogDetail: TZetaClientDataSet;
    cdsCreditosDiferenciasCMontoPres: TFloatField;
    cdsActualizaPCarAbo: TServerDataSet;
    cdsCreditosDiferenciasActualizaMontoP: TBooleanField;
    cdsCreditosDiferenciasTresMontoP: TFloatField;
    cdsEmpCreditosStatus: TStringField;
    cdsCreditosActivosStatus: TStringField;
    cdsCreditosNuevosStatusEmp: TStringField;
    cdsCreditosDiferenciasStatusEmp: TStringField;
    cdsConfrontacionStatusEmp: TStringField;
    cdsCreditosNoActivosNEmpleado: TIntegerField;
    cdsCreditosNoActivosNomCompleto: TStringField;
    cdsCreditosNoActivosNFonacot: TStringField;
    cdsCreditosNoActivosStatusEmp: TStringField;
    cdsCreditosNoActivosSaldoAjuste: TCurrencyField;
    cdsPrestaAjus: TZetaClientDataSet;
    cdsConfrontacionSaldoAjus: TFloatField;
    cdsPCarAbo: TZetaClientDataSet;
    cdsConfrontacionStatusPrestamo: TIntegerField;
    cdsConfrontacionObserva: TStringField;
    cdsCreditosActivosObserva: TStringField;
    cdsCreditosNoActivosObserva: TStringField;
    cdsCreditosDiferenciasPR_OBSERVA: TStringField;
    cdsEmpleaFonacotFechaBaja: TDateField;
    cdsCreditosNuevosReubicado: TStringField;
    cdsConfrontacionReubicado: TStringField;
    cdsCreditosDiferenciasDIFERENCIAS: TCurrencyField;
    cdsAsciiDiferencia: TCurrencyField;
    cdsCreditosDiferenciasDiferenciaCredito: TFloatField;
    cdsConfrontacionDIFERENCIA: TFloatField;
    cdsConfrontacionDiferenciaCredito: TFloatField;
    cdsCreditosNoActivosReubicado: TStringField;
    cdsCreditosActivosReubicado: TStringField;
    cdsCreditosNoActivosTressPlazo: TIntegerField;
    cdsCreditosNoActivosTressCedula: TIntegerField;
    cdsCreditosDiferenciasDIFERENCIAPLAZO: TIntegerField;
    cdsEmpleaFonacotReubicado: TStringField;
    cdsCreditosDiferenciasReubicado: TStringField;
    cdsCreditosDiferenciasTemp: TZetaClientDataSet;
    cdsCreditosDiferenciasTempPosicion: TIntegerField;
    cdsCreditosDiferenciasTempNFonacot: TStringField;
    cdsCreditosDiferenciasTempNEmpleado: TIntegerField;
    cdsCreditosDiferenciasTempNomCompleto: TStringField;
    cdsCreditosDiferenciasTempNomStatusEmp: TStringField;
    cdsCreditosDiferenciasTempNCredito: TStringField;
    cdsCreditosDiferenciasTempCRetencion: TFloatField;
    cdsCreditosDiferenciasTempCPlazo: TIntegerField;
    cdsCreditosDiferenciasTempCMeses: TIntegerField;
    cdsCreditosDiferenciasTempCMontoPres: TFloatField;
    cdsCreditosDiferenciasTempTressRetencion: TFloatField;
    cdsCreditosDiferenciasTempTressPlazo: TIntegerField;
    cdsCreditosDiferenciasTempTressMeses: TIntegerField;
    cdsCreditosDiferenciasTempTressMontoP: TFloatField;
    cdsCreditosDiferenciasTempPR_SALDO_I: TFloatField;
    cdsCreditosDiferenciasTempPR_MONTO: TFloatField;
    cdsCreditosDiferenciasTempPR_FECHA: TDateField;
    cdsCreditosDiferenciasTempPR_FORMULA: TStringField;
    cdsCreditosDiferenciasTempPR_SUB_CTA: TStringField;
    cdsCreditosDiferenciasTempPR_MONTO_S: TFloatField;
    cdsCreditosDiferenciasTempPR_TASA: TFloatField;
    cdsCreditosDiferenciasTempPR_INTERES: TFloatField;
    cdsCreditosDiferenciasTempActualizaMontoP: TBooleanField;
    cdsCreditosDiferenciasTempPR_OBSERVA: TStringField;
    cdsCreditosDiferenciasTempDIFERENCIA: TCurrencyField;
    cdsCreditosDiferenciasTempDiferenciaCredito: TFloatField;
    cdsCreditosDiferenciasTempDIFERENCIAPLAZO: TIntegerField;
    cdsCreditosDiferenciasTempReubicado: TStringField;
    cdsDataLista: TZetaClientDataSet;
    procedure cdsEmpleaFonacotNEmpleadoValidate(Sender: TField);
    procedure cdsEmpleaFonacotNEmpleadoChange(Sender: TField);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsConfrontacionFecInicioGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure cdsConfrontacionAlCrearCampos(Sender: TObject);
    procedure cdsProcesosAlModificar(Sender: TObject);
    procedure cdsLogDetailAfterOpen(DataSet: TDataSet);
    procedure cdsLogDetailAlModificar(Sender: TObject);
    procedure GetProcessLog(const iProceso: Integer);
    procedure cdsProcesosCalcFields(DataSet: TDataSet);
    procedure cdsProcesosAfterOpen(DataSet: TDataSet);
    procedure cdsProcesosAlCrearCampos(Sender: TObject);
    procedure cdsReubicadoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  private
    { Private declarations }
    FMes  : Integer;
    FAnio : Integer;
    FHayDiferencia :Boolean;
    FFechaPrestamo : TDateTime;
    FTotalPrestado:Currency;
    FTotalSaldo:Currency;
    FTotalCredTress:Integer;
    FTotalCredCed:Integer;
    FConciliando :Boolean;
    FBitacora: TAsciiLog;
    FPathDatos :string;
    FErroresCedula: Boolean;
    FArchivoInvalidoVacio: Boolean;
    FTotalAltas : Integer;
    FTotalCedula: Currency;
    FRazonSocial : string;

    FServidor : IdmServerSuperDisp;
    FServidorRH : IdmServerRecursosDisp;
    function GetServerSuper : IdmServerSuperDisp;
    property ServerSuper : IdmServerSuperDisp read GetServerSuper;
    function GetServerRecursos : IdmServerRecursosDisp ;
    property ServerRecursos : IdmServerRecursosDisp read GetServerRecursos;

    procedure GetEmpleados;
    procedure CargaCedulaFonacot;
    procedure AddCreditos( oCredito:TCreditoFonacot; Bajas: Boolean; FechaBajas: TDate );
    procedure AddCreditoNuevo( oCredito :TCreditoFonacot;iNumEmpleado:Integer; sNombre:string ; const eStatus: eStatusEmpleado );
    function PreparaConciliar:Boolean;
    procedure InicializarDataSet ( oDataSet:TClientDataSet );
    procedure AgregarCreditosActivosTress;
    procedure DefineColumnasPrestamos(sdsData:TServerDataSet; const lEsPCarAbo: Boolean = FALSE );
    procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );

    procedure LlenaRenglonAscci( const dFechaKardex:TDateTime );
    procedure EscribeBitacora(Resultado: OleVariant; lMuestraResultado: Boolean = TRUE);
    function  GetTipoPrestamo:string;
    function GetTipoPrestamoAjus: String;
    function Check( const Resultado: OleVariant): Boolean;
    function CheckSilencioso( const Resultado: OleVariant): Boolean;

    //procedure FinalizaConfrontar;
    procedure GetTotalesCalculo;
    procedure GetCargosAbonos;
    function PreparaConfrontar:Boolean;
    procedure AgregarCreditosAltas;
    function CedulaNoTieneErrores: Boolean;
    procedure ExportarCedulaFonacot(Sender: TAsciiExport; DataSet: TDataset);
    function AgregaMensaje(const sMensaje, sValor: String): String;
    function GetDataConsulta(const iOpcion:Integer;cdsData:TZetaClientDataSet; sParam: String = VACIO ):Boolean;
    procedure LeerUnProceso(const iFolio: Integer);
    function GetStatusProceso: eProcessStatus;
    procedure ConfigProcIdField(Campo: TField);
    procedure GetProcessText(Sender: TField; var Text: String; DisplayText: Boolean);
    function GetStatusEmpleado( const iEmpleado: Integer ): String;
    function GetSaldoAjuste( const iEmpleado: Integer; const sCredito: String ): TPesos;
    function GetAjusteMesAnterior( const iEmpleado: Integer; sCredito: String ): TPesos;
    function GetFieldPrestamo( const sReferencia, sCampo: String; iEmpleado: Integer ): String;
    procedure MaskBisiestos( const sCampo: String; oDataSet: TDataSet );
    procedure FechaBisGetText(Sender: TField; var Text: String; DisplayText: Boolean);



  public
    { Public declarations }
    procedure ConciliarFonacot(const Anio, Mes: Integer; Bajas: Boolean; FechaBajas: TDate);//Conciliador FONACOT - Mejoras Se Determina el Anio y Mes de Consiciliacion
    procedure TransferirEmpleadosFonacot( const dFechaKardex:TDateTime );
    //procedure ImportarNuevosCreditos(const dFechaInicial:TDateTime);
    procedure ImportarNuevosCreditos(const dFechaInicial:TDateTime; ConsideraCuotas: Boolean );
    procedure LogEnd;
    procedure Log(sTexto: string);
    procedure LogInit( sPath,sProceso:string );
    function  ExistenErroresCedula:Boolean;
    function  ArchivoInvalido: Boolean;
    procedure ActualizarCreditos(const RemoverFormula, PlazoAumenta, PlazoDisminuye, CambioRetencion, RecalculaRetencion:Boolean );
    procedure Confrontar(const Anio, Mes: Integer);
    function GetStatusCalculoFonacot: string;
    function CrearAsciiCedula(cdsDataSet: TZetaClientDataSet; const sArchivoCedula: string): String;
    procedure EnMascaraFechas(const lConsideraBis: Boolean);
    procedure GetcdsAuxiliar( const lBisiestos: Boolean );
    // procedure SetPathBitacora(const iPageIndex: Integer; sValue: String);
    // procedure SetUltimaCedula( const sValue: String );
    // function GetPathBitacora( const iPageIndex: Integer ): String;
    function GetUltimaCedula: String;
    procedure LimpiarDataSetsConciliacion;
    function GetDescripcionFechaConciliacion: String;//Conciliador FONACOT - Mejoras Fecha Conciliacion formato: Septiembre/2015
    function GetDescripcionFechaCortaConciliacion: String;//Conciliador FONACOT - Mejoras Fecha Conciliacion formato: Sep/2015
    procedure GetEmpFonacotGetLista(Parametros: TZetaParams);

    property Mes : Integer read FMes write FMes;
    property Anio : Integer read FAnio write FAnio;
    property HayDiferencia :Boolean read FHayDiferencia write FHayDiferencia;
    property FechaPrestamo : TDateTime read FFechaPrestamo write FFechaPrestamo;
    property TotalPrestado:Currency read FTotalPrestado;
    property TotalSaldo:Currency read FTotalSaldo;
    property TotalCredTress:Integer read FTotalCredTress;
    property TotalCredCed:Integer read FTotalCredCed;
    property Conciliando:Boolean read FConciliando write FConciliando;
    property Bitacora :TAsciiLog read FBitacora write FBitacora;
    property PathDatos:string read FPathDatos write FPathDatos;
    property TotalAltas: Integer read FTotalAltas;
    property TotalCedula:Currency read FTotalCedula;
    property RazonSocial: string read FRazonSocial write FRazonSocial;

  end;

const
     K_FORMATO = '#,###,###,###,##0';
     K_ALTA = 'A';
     K_BAJA = 'B';
     K_INCAPA = 'I';
     K_SIN_INCIDE = '0';
     K_EMPLEADO_INVALIDO =  'No v�lido';
     K_EMPLEADO_INVALIDO_POR_RAZON_SOCIAL =  'No v�lido (RS)';
     K_TIPO_AJUSTE = 5;
     K_FECHA_BIS = 39507;
     K_FECHA_SIN_BIS = 39506;
     K_REUBICADO = 'R';

     K_MSJ_CREDITO_SALDADO = 'Cr�dito saldado en Tress';

     QRY_FILTRO_RAZONSOCIAL = ' CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO=%s )';

     K_EMPLEADOS_NUEVOS_FONACOT = 2;
     K_CREDITOS_NUEVOS = 3;
     K_ACTUALIZA_PRESTAMOS = 5;

     K_RELLENO     = 10;

var
  dmConcilia: TdmConcilia;
  TBajas: Boolean;
  TFechaBajas : TDate;

implementation

uses DCliente,FTressShell,ZetaCommonTools,dGlobal,ZGlobalTress,ZetaDialogo,
  DBaseGlobal, ZBaseThreads,ZAsciiTools,ZetaWizardFeedBack_DevEx,Variants,FProcessLogShow_DevEx,FBaseEditBitacora_DevEx,FEditBitacora_DevEx,
  ZetaClientTools, ZetaServerTools,ZetaRegistryCliente,ZAccesosMgr,ZAccesosTress;

{$R *.dfm}

{OnCreate del M�dulo }
procedure TdmConcilia.DataModuleCreate(Sender: TObject);
begin
     FBitacora := TAsciiLog.Create;
end;

{OnDestroy del M�dulo }
procedure TdmConcilia.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FBitacora );
end;


///Obtener los datos de las llamadas al servidor de Consultas...
function TdmConcilia.GetDataConsulta(const iOpcion:Integer;cdsData:TZetaClientDataSet; sParam: String ):Boolean;
const



     QRY_EMP_FONACOT = 'select CB_CODIGO,CB_FONACOT,CB_RFC,(CB_APE_PAT||'' ''||CB_APE_MAT||'', ''||CB_NOMBRES)as PRETTYNAME from COLABORA where CB_FONACOT <> %s ';


     QRY_RETENCIONES = 'select PR_REFEREN,FC_RETENC,FC_NOMINA,FC_AJUSTE,FC.CB_CODIGO,FE_INCIDE,FE_FECHA1,FE_FECHA2'+
                       ' from FON_CRE FC left outer join FON_EMP FE on ( FE.CB_CODIGO = FC.CB_CODIGO and FC.FT_YEAR = FE.FT_YEAR and FC.FT_MONTH = FE.FT_MONTH ) '+
                       '                 left outer join COLABORA CB on FE.CB_CODIGO = CB.CB_CODIGO ' +
                       '  where ( FC.PR_TIPO = %s and  FC.FT_YEAR = %d and FC.FT_MONTH = %d )';


     QRY_PRESTAMOS   = 'select PR_TIPO,P.CB_CODIGO,C.CB_FONACOT ,(CB_APE_PAT||'' ''||CB_APE_MAT||'', ''||CB_NOMBRES)as PRETTYNAME, '+
                       'PR_FECHA,PR_PAGOS,PR_REFEREN,PR_SALDO,PR_STATUS,PR_MONTO,PR_PAG_PER,PR_ABONOS,PR_SALDO_I,PR_FORMULA,PR_SUB_CTA,PR_MONTO_S,PR_TASA,PR_MESES,PR_INTERES, PR_CARGOS, PR_OBSERVA ' +
                       ' from PRESTAMO P left outer join COLABORA C on C.CB_CODIGO = P.CB_CODIGO where PR_TIPO = %s ';

     QRY_TOTALES     = 'select FT_RETENC,FT_NOMINA,FT_CUANTOS,FT_STATUS,FT_EMPLEAD,FT_AJUSTE,FT_BAJAS,FT_INCAPA,FT_TAJUST from FON_TOT FT  where ( PR_TIPO = %s and  FT_YEAR = %d and FT_MONTH = %d )';

     QRY_TOTALES_RS     =
                                  'select FE.FT_YEAR, FE.FT_MONTH, FE.PR_TIPO, SUM(FE_RETENC) FT_RETENC, SUM(FE_NOMINA) FT_NOMINA, SUM(FE_CUANTOS) FT_CUANTOS, FT.FT_STATUS, ' +
                                  'COUNT(FE.CB_CODIGO) FT_EMPLEAD, SUM(FE.FE_AJUSTE) FT_AJUSTE, ' +
                                  '(select count(FE_INCIDE) from FON_EMP FE_IN where  FE_IN.FT_YEAR = FE.FT_YEAR and FE_IN.FT_MONTH = FE.FT_MONTH and FE_IN.PR_TIPO=FE.PR_TIPO and FE_INCIDE=''I'')  FT_INCAPA, ' +
                                  '(select count(FE_INCIDE) from FON_EMP FE_IN where  FE_IN.FT_YEAR = FE.FT_YEAR and FE_IN.FT_MONTH = FE.FT_MONTH and FE_IN.PR_TIPO=FE.PR_TIPO and FE_INCIDE=''B'')  FT_BAJAS, ' +
                                  'FT.FT_TAJUST ' +
                                  'from FON_EMP FE ' +
                                  'join COLABORA CB on CB.CB_CODIGO = FE.CB_CODIGO '+
                                  'join FON_TOT  FT on  FT.FT_YEAR = FE.FT_YEAR and FT.FT_MONTH = FE.FT_MONTH and FT.PR_TIPO=FE.PR_TIPO ' +
                                  'where ( %0:s = '''' OR CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO=%0:s)) ' +
                                  'and ( FE.PR_TIPO = %1:s and  FE.FT_YEAR = %2:d and FE.FT_MONTH = %3:d ) ' +
                                  'group by FE.FT_YEAR, FE.FT_MONTH, FE.PR_TIPO, FT.FT_STATUS, FT.FT_TAJUST' ;

     QRY_PRESTA_ALTA = 'select PR_MESES,PR_PAGOS,PR_REFEREN,PR_PAG_PER, CB.CB_CODIGO from PRESTAMO ' +
                       ' join COLABORA CB on PRESTAMO.CB_CODIGO = CB.CB_CODIGO ' +
                       ' where ( PR_TIPO = %s ) ';

     QRY_AJUSTE_MES = 'select PR_REFEREN,  SUM( CR_CARGO -  CR_ABONO ) SALDO_AJUSTE,  PCAR_ABO.CB_CODIGO from PCAR_ABO ' +
                      'join COLABORA CB on PCAR_ABO.CB_CODIGO = CB.CB_CODIGO '+
                      '%s GROUP BY PR_REFEREN, PCAR_ABO.CB_CODIGO';

var
   sScript, sTipoPrestaAjus:string;
begin
     with dmCliente do
     begin
          Result := StrLleno( GetTipoPrestamo );
          if Result then
          begin
               case iOpcion of
                    0:
                    begin
                         sScript := Format( QRY_EMP_FONACOT ,[EntreComillas( VACIO ) ] );
                         if StrLleno( RazonSocial ) then
                            sScript := sScript + ' and ' + Format(QRY_FILTRO_RAZONSOCIAL,[EntreComillas(RazonSocial)]);
                    end;
                    1:
                    begin
                         sScript := Format( QRY_RETENCIONES,[ EntreComillas( GetTipoPrestamo ),FAnio, FMes  ] );
                         if StrLleno( RazonSocial ) then
                            sScript := sScript + ' and ' + Format(QRY_FILTRO_RAZONSOCIAL,[EntreComillas(RazonSocial)]);
                    end;
                    2:
                    begin
                         sScript := Format( QRY_PRESTAMOS,[ EntreComillas( GetTipoPrestamo )  ] );
                         if StrLleno( RazonSocial ) then
                            sScript := sScript +' and '+ Format(QRY_FILTRO_RAZONSOCIAL,[EntreComillas(RazonSocial)]);
                    end;
                    3:
                    begin
                         if StrLleno( RazonSocial ) then
                             sScript := Format( QRY_TOTALES_RS,[ EntreComillas(RazonSocial), EntreComillas( GetTipoPrestamo ),FAnio, FMes  ] )
                         else
                             sScript := Format( QRY_TOTALES,[EntreComillas( GetTipoPrestamo ),FAnio, FMes  ] );
                    end;
                    4:
                    begin
                         sScript := Format( QRY_PRESTA_ALTA,[ EntreComillas( GetTipoPrestamo ) ] );
                         if StrLleno( RazonSocial ) then
                            sScript := sScript + ' and ' + Format(QRY_FILTRO_RAZONSOCIAL,[EntreComillas(RazonSocial)]);
                    end;
                    5:
                    begin
                         if StrLleno( sParam ) then
                            sTipoPrestaAjus:= sParam
                         else
                             sTipoPrestaAjus:= GetTipoPrestamoAjus;

                         sScript := Format( QRY_PRESTAMOS,[ EntreComillas( sTipoPrestaAjus ) ] );

                         if StrLleno( RazonSocial ) then
                            sScript := sScript + ' and '+ Format(QRY_FILTRO_RAZONSOCIAL,[EntreComillas(RazonSocial)]);

                    end;
                    6:
                    begin
                         if StrLleno( RazonSocial ) then
                            sParam := sParam + ' and ' + Format(QRY_FILTRO_RAZONSOCIAL,[EntreComillas(RazonSocial)]);
                         sScript:= Format( QRY_AJUSTE_MES, [ sParam ] );
                    end;
               end;
               cdsData.Data := dmConsultas.ServerConsultas.GetQueryGralTodos( Empresa,sScript );
          end
          else
              ZetaDialogo.ZError('Error de configuraci�n','El global de Tipo de Pr�stamo de Fonacot no tiene asignado un tipo de pr�stamo',0);
     end;

end;

procedure TdmConcilia.GetCargosAbonos;
var
   dFechaReferen: TDate;
   sParam: String;
begin
     dFechaReferen:= EncodeDate( FAnio, FMes, 1);
     sParam:= Format( ' where ( PR_TIPO = %s ) and ( CR_FECHA between %s and %s ) ', [ EntreComillas( cdsTotales.FieldByName('FT_TAJUST').AsString ),
                                                                                           EntreComillas(FechaAsStr(FirstDayofMonth(dFechaReferen ))),
                                                                                           EntreComillas(FechaAsStr(LastDayofMonth(dFechaReferen )))] );
     GetDataConsulta( 6, cdsPCarAbo, sParam );
end;


///Obtener el Global aignado para tipo de prestamo de Fonacot
function TdmConcilia.GetTipoPrestamo:string;
begin
     Result := Global.GetGlobalString( K_GLOBAL_FONACOT_PRESTAMO );
end;

function TdmConcilia.GetTipoPrestamoAjus: String;
begin
     Result := Global.GetGlobalString( K_GLOBAL_DEF_NOM_TIPOPRESTAAJUSTE );
end;

procedure TdmConcilia.GetEmpleados;
const
     QRY_EMPLEADOS = 'select CB_CODIGO,PRETTYNAME,CB_FONACOT,CB_RFC, CB_FEC_BAJ, ( dbo.SP_STATUS_EMP( %s ,CB_CODIGO ) )as STATUS from COLABORA WHERE ';
var
   sQuery : string;
begin

     with dmCliente do
     begin
          sQuery :=  Format ( QRY_EMPLEADOS,[ EntreComillas( FechaAsStr( FechaDefault ) ) ] );
          sQuery := sQuery + '( ( SELECT COUNT(*) from PRESTAMO where PRESTAMO.PR_TIPO = ' + EntreComillas ( Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO) )  + ' and PRESTAMO.PR_STATUS = 0 and PRESTAMO.CB_CODIGO = COLABORA.CB_CODIGO ) > 0 ) OR ';

          if  TBajas then
            sQuery := sQuery + '( CB_ACTIVO = ' + EntreComillas ( 'S' ) + ' OR ' + 'CB_FEC_BAJ >= ' + EntreComillas( FechaAsStr( TFechaBajas ) ) + ' ) '
          else
            sQuery := sQuery + '( CB_ACTIVO = ' + EntreComillas ( 'S' ) + ' ) ';

          if StrLleno( RazonSocial ) then
          begin
               sQuery := sQuery + ' AND ' + Format( QRY_FILTRO_RAZONSOCIAL, [ EntreComillas( RazonSocial ) ] );
          end;
          cdsEmpleados.Data := dmConsultas.ServerConsultas.GetQueryGralTodos( Empresa, sQuery);
     end;
end;

{ M�todo principal para la conciliaci�n }
procedure TdmConcilia.ConciliarFonacot(const Anio, Mes: Integer; Bajas: Boolean; FechaBajas: TDate);
var
   oCredito :TCreditoFonacot;
   iNumEmpleado : Integer;
   function NoTieneFonacot :Boolean;
   begin
        Result := ( Not ( ( cdsEmpleados.Locate( 'CB_FONACOT',oCredito.NumFonacot,[] ) ) ) );
   end;

   procedure AgregarEmpleadoSinFonacot ;
   begin
        with cdsEmpleaFonacot do
        begin
             Append;
             with oCredito do
             begin
                  FieldByName('NEmpleado').AsInteger := iNumEmpleado;
                  FieldByName('NomCompleto').AsString := NomCompleto;
                  FieldByName('NomTress').AsString := cdsEmpleados.FieldByName('PRETTYNAME').AsString;
                  FieldByName('RFC').AsString := RFC;
                  FieldByName('NFonacot').AsString := NumFonacot;
                  FieldByName('Status').AsString := ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger );
                  FieldByName('Posicion').AsInteger := Posicion;
                  FieldByName('FechaBaja').AsDateTime:= cdsEmpleados.FieldByName('CB_FEC_BAJ').AsDateTime;
                  FieldByName('Reubicado').AsString :=  Reubicado;
             end;

             MergeChangeLog;
        end;
   end;

   procedure AgregaEmpleadoSinNumero;
   begin
        with cdsEmpleaFonacot do
        begin
             Append;
             with oCredito do
             begin
                  FieldByName('NEmpleado').AsInteger := 0;
                  FieldByName('NomCompleto').AsString := NomCompleto;
                  FieldByName('NomTress').AsString := VACIO;
                  FieldByName('RFC').AsString := RFC;
                  FieldByName('NFonacot').AsString := NumFonacot;
                  FieldByName('Status').AsString := VACIO;
                  FieldByName('Posicion').AsInteger := Posicion;
                  FieldByName('Reubicado').AsString := Reubicado;
             end;
             MergeChangeLog;
        end;
   end;

   function ExisteRFCEnTress:Boolean;
   begin
        Result := ( cdsEmpleados.Locate( 'CB_RFC', oCredito.RFC,[ loPartialkey ] ) );
        iNumEmpleado := cdsEmpleados.FieldByName('CB_CODIGO').AsInteger;
   end;

   function CreditoRegistrado :Boolean;
   begin
        Result := ( cdsEmpleaFonacot.Locate('NFonacot',oCredito.NumFonacot,[]) );
   end;


   ///Inicio de conciliaci�n de Fonacot
   begin
     Conciliando := True;
     FErroresCedula := False;
     FAnio := Anio;
     FMes := Mes;
     TBajas := Bajas;
     TFechaBajas := FechaBajas;

     oCredito := TCreditoFonacot.Create;
     try
        if PreparaConciliar then
        begin
             with TressShell do
             begin
                  with cdsCedulas do
                  begin
                       // ConciliarStart( RecordCount );
                       First;
                       // While ( not Eof and ConciliarStep )do
                       While ( not Eof  )do
                       begin
                            if ( CedulaNoTieneErrores )then
                            begin
                                 oCredito.SetCreditoFromCedula( cdsCedulas );
                                 //Buscar el n�mero de fonacot
                                 if NoTieneFonacot  then
                                 begin
                                      if( Not ( CreditoRegistrado ) )then//Para evitar cr�ditos ya comparados
                                      begin
                                           if ExisteRFCEnTress then
                                           begin
                                                AgregarEmpleadoSinFonacot
                                           end
                                           else
                                           begin
                                                AgregaEmpleadoSinNumero
                                           end;
                                      end;
                                 end
                                 else //Si existe el n�mero de fonacot con alg�n empleado
                                 begin
                                      AddCreditos( oCredito, Bajas, FechaBajas );
                                 end;
                                 Delete;
                                 MergeChangeLog;
                            end
                            else
                            begin
                                 FErroresCedula := True;
                                 Next;
                            end;
                       end;
                  end;
                  //Segunda parte de la conciliaci�n
                  AgregarCreditosActivosTress;
                  // ConciliarEnd;
                  Conciliando := False;
             end;
        end;
     finally
            FreeAndNil(oCredito);
     end;
end;


function TdmConcilia.CedulaNoTieneErrores :Boolean;
begin
     Result := ( cdsCedulas.FieldByName('NUM_ERROR').AsInteger <= 0 );
end;


function TdmConcilia.ArchivoInvalido:Boolean;
begin
     Result := FArchivoInvalidoVacio;
end;

function TdmConcilia.PreparaConciliar:Boolean;
begin
     Result := GetDataConsulta(2,cdsPrestamos);
     if Result then
     begin
          CargaCedulaFonacot;
          GetEmpleados;
          GetDataConsulta(K_TIPO_AJUSTE,cdsPrestaAjus);
          InicializarDataSet( cdsEmpCreditos );
          InicializarDataSet( cdsEmpleaFonacot );
          InicializarDataSet( cdsCreditosActivos );
          InicializarDataSet( cdsCreditosNuevos );
          InicializarDataSet( cdsCreditosNoActivos );
          InicializarDataSet( cdsCreditosDiferencias );

          FTotalPrestado := 0;
          FTotalSaldo := 0;
          FTotalCredTress := 0;
          FTotalCredCed := 0;
     end;
     FArchivoInvalidoVacio := cdsCedulas.IsEmpty;
end;

procedure TdmConcilia.InicializarDataSet ( oDataSet:TClientDataSet );
begin

if oDataSet <> nil then
begin
     with oDataSet do
     begin
          if not Active then
          begin
               //Close;
               CreateDataSet;
               Open;
          end;
          EmptyDataSet;
     end;
end;
end;

{Regresa el Saldo del Ajuste}
function TdmConcilia.GetSaldoAjuste(const iEmpleado: Integer; const sCredito: String): TPesos;
begin
     Result:= 0;
     if ( iEmpleado <> 0 ) then
     begin
          with cdsPrestaAjus do
          begin
               if( Locate( 'PR_REFEREN;CB_CODIGO',VarArrayOf([sCredito,iEmpleado]),[])) then
                   Result:= FieldByName('PR_SALDO').AsFloat;
          end;
     end;
end;

{Agregando cr�ditos a las vistas ..}
procedure TdmConcilia.AddCreditos( oCredito:TCreditoFonacot; Bajas: Boolean; FechaBajas: TDate );
const
     K_CERO = 0.00;
var
   iCreditoTress,iNumEmpleado:Integer;
   sNombre:string ;

   procedure AddToTotalTress;
   begin
        FTotalCredTress := FTotalCredTress + 1;
   end;

   procedure AddToTotalCedula;
   begin
        FTotalCredCed := FTotalCredCed + 1;
   end;

   {function GetSaldoAjuste: TPesos;
   begin
        Result:= K_CERO;
        with cdsPrestaAjus do
        begin
             if( Locate( 'PR_REFEREN;CB_CODIGO',VarArrayOf([oCredito.NumCredito,iNumEmpleado]),[])) then
                 Result:= FieldByName('PR_SALDO').AsFloat;
        end;
   end;}

   function ExisteCreditoEnTress:Boolean;
   begin
       //Localizo el NumEmpleado a buscar
       cdsEmpleados.Locate( 'CB_FONACOT', oCredito.NumFonacot,[] );
       iNumEmpleado := cdsEmpleados.FieldByName('CB_CODIGO').AsInteger;

       {if Bajas Then
          begin
              if ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger ) =  'Baja' then
                  if  ( cdsEmpleados.FieldByName('CB_FEC_BAJ').AsDateTime < FechaBajas ) then
                      vActivar := False;
          end
       else
          begin
              if ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger ) =  'Baja' then
                  vActivar := False ;
          end;

       if vActivar then
       begin   }
       sNombre := cdsEmpleados.FieldByName('PRETTYNAME').AsString;

       //Result := ( cdsPrestamos.Locate('PR_REFEREN',oCredito.NumCredito,[] ) );
       //Buscar por Numero de Cr�dito y Empleado
       Result := ( cdsPrestamos.Locate( 'PR_REFEREN;CB_CODIGO',VarArrayOf([oCredito.NumCredito,iNumEmpleado]),[]));
       if Result then
       begin
            with cdsPrestamos do
            begin //Agregar a los creditos que NO son activos
                 if cdsPrestamos.FieldByName('PR_STATUS').AsInteger <> Ord(spActivo) then
                 begin
                      with cdsCreditosNoActivos do
                      begin
                           Append;
                           FieldByName('NCredito').AsString :=  cdsPrestamos.FieldByName('PR_REFEREN').AsString;
                           FieldByName('FechaInicial').AsDateTime :=  cdsPrestamos.FieldByName('PR_FECHA').AsDateTime;
                           FieldByName('Status').AsString :=  ObtieneElemento( lfStatusPrestamo,cdsPrestamos.FieldByName('PR_STATUS').AsInteger ); ///STATUS TEXTO
                           FieldByName('MPrestado').AsFloat :=  cdsPrestamos.FieldByName('PR_MONTO').AsFloat;
                           FieldByName('Saldo').AsFloat :=  cdsPrestamos.FieldByName('PR_SALDO').AsFloat;
                           FieldByName('RMensual').AsFloat := oCredito.MontoMensual;
                           FieldByName('Pagos').AsFloat := oCredito.Plazo ;
                           FieldByName('TotalPrestamo').AsFloat := ( oCredito.MontoMensual * oCredito.Plazo );
                           FieldByName('Posicion').AsInteger := oCredito.Posicion;
                           FieldByName('NEmpleado').AsInteger := cdsPrestamos.FieldByName('CB_CODIGO').AsInteger;
                           FieldByName('NomCompleto').AsString:= oCredito.NomCompleto;
                           FieldByName('NFonacot').AsString:= oCredito.NumFonacot;
                           FieldByName('StatusEmp').AsString:= ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger );
                           FieldByName('SaldoAjuste').AsFloat := GetSaldoAjuste( iNumEmpleado, oCredito.NumCredito );
                           FieldByName('Reubicado').AsString := oCredito.Reubicado;  //US: 12873 Reubicado
                           FieldByName('TressCedula').AsInteger :=  oCredito.Meses;  //US: 12873 Plazo en Cedula
                           FieldByName('TressPlazo').AsInteger :=  cdsPrestamos.FieldByName('PR_MESES').AsInteger;  //US: 12873 Plazo en Tress
                           FieldByName('Observa').AsString:= cdsPrestamos.FieldByName('PR_OBSERVA').AsString;
                           Post;
                      end;

                 end
                 else
                 begin  //Agregar a los creditos que tiene diferencias en Tress en (Retencion Mnesual � Cuantos Pagos � Plazo)
                      if (  ( cdsPrestamos.FieldByName('PR_PAG_PER').AsCurrency <> oCredito.MontoMensual ) or
                            { AP(28/Feb/2008): Este campo vendria diferente cada mes
                            ( cdsPrestamos.FieldByName('PR_PAGOS').AsInteger <> oCredito.Meses )or }
                            ( cdsPrestamos.FieldByName('PR_MESES').AsInteger <> oCredito.Plazo ) ) then
                      begin
                           with cdsCreditosDiferencias do
                           begin
                                Append;
                                FieldByName('Posicion').AsInteger := oCredito.Posicion;
                                FieldByName('NFonacot').AsString :=  oCredito.NumFonacot;
                                FieldByName('NEmpleado').AsInteger :=  cdsPrestamos.FieldByName('CB_CODIGO').AsInteger;
                                FieldByName('NomCompleto').AsString :=  cdsPrestamos.FieldByName('PRETTYNAME').AsString;
                                FieldByName('NCredito').AsString :=  cdsPrestamos.FieldByName('PR_REFEREN').AsString;
                                FieldByName('CRetencion').AsFloat := oCredito.MontoMensual;
                                FieldByName('CPlazo').AsInteger :=  oCredito.Plazo;
                                FieldByName('CMeses').AsInteger :=  oCredito.Meses;
                                FieldByName('CMontoPres').AsFloat := oCredito.MontoPrestado;
                                FieldByName('TressRetencion').AsFloat :=   cdsPrestamos.FieldByName('PR_PAG_PER').AsFloat;
                                FieldByName('TressMeses').AsInteger :=  cdsPrestamos.FieldByName('PR_PAGOS').AsInteger;
                                FieldByName('TressPlazo').AsInteger :=  cdsPrestamos.FieldByName('PR_MESES').AsInteger;
                                FieldByName('TressMontoP').AsFloat := cdsPrestamos.FieldByName('PR_SALDO').AsFloat;  //cdsPrestamos.FieldByName('PR_MONTO').AsFloat + cdsPrestamos.FieldByName('PR_CARGOS').AsFloat;
                                FieldByName('StatusEmp').AsString:= ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger );

                                // Campos para la actualizaci�n
                                FieldByName('PR_FECHA').AsDateTime :=  cdsPrestamos.FieldByName('PR_FECHA').AsDateTime;
                                FieldByName('PR_MONTO').AsFloat :=  cdsPrestamos.FieldByName('PR_MONTO').AsFloat;

                                // El saldo inicial parte del saldo
                                FieldByName('PR_SALDO_I').AsFloat :=  cdsPrestamos.FieldByName('PR_SALDO_I').AsFloat;
                                FieldByName('PR_FORMULA').AsString :=  cdsPrestamos.FieldByName('PR_FORMULA').AsString;
                                FieldByName('PR_SUB_CTA').AsString :=  cdsPrestamos.FieldByName('PR_SUB_CTA').AsString;
                                FieldByName('PR_MONTO_S').AsFloat :=  cdsPrestamos.FieldByName('PR_MONTO_S').AsFloat;
                                FieldByName('PR_TASA').AsFloat :=  cdsPrestamos.FieldByName('PR_TASA').AsFloat;
                                FieldByName('PR_INTERES').AsFloat :=  cdsPrestamos.FieldByName('PR_INTERES').AsFloat;
                                FieldByName('PR_OBSERVA').AsString := cdsPrestamos.FieldByName('PR_OBSERVA').AsString;
                                //Booleano que indica si se debe de actualizar el monto prestado.
                                //Si el plazo es diferente significa que si debe de.

                                FieldByName('ActualizaMontoP').AsBoolean := ( FieldByName('TressPlazo').AsFloat <> FieldByName('CPlazo').AsFloat );
                                FieldByName('DIFERENCIA').AsFloat := Abs( FieldByName('CRetencion').AsFloat - FieldByName('TressRetencion').AsFloat );
                                FieldByName('DiferenciaCredito').AsFloat := ( FieldByName('CRetencion').AsFloat - FieldByName('TressRetencion').AsFloat );
                                {***US 12870: Conciliador Fonacot - Modificar columnas ventana Cr�ditos con Diferencias***}
                                FieldByName('DIFERENCIAPLAZO').AsInteger := oCredito.Plazo - FieldByName('TressPlazo').AsInteger;
                                FieldByName('Reubicado').AsString := oCredito.Reubicado;
                                Post;
                                MergeChangeLog;
                           end;
                      end;
                 end;
                 Delete;
                 MergeChangeLog;
            end;
       end;
   {end
   else
      cdsEmpleados.Delete; }

end;
begin
     iCreditoTress := 0;
     iNumEmpleado := 0;
     AddToTotalCedula;
     with cdsEmpCreditos do
     begin
           {vActivo := True;
           if Bajas Then
              begin
                  if ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger ) =  'Baja' then
                      if  ( cdsEmpleados.FieldByName('CB_FEC_BAJ').AsDateTime < FechaBajas ) then
                          vActivo := False;
              end
           else
              begin
                  if ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger ) =  'Baja' then
                      vActivo := False ;
              end;


          if vActivo then
          begin}
                with oCredito do
                begin
                     if ( ExisteCreditoEnTress )then
                     begin
                          iCreditoTress := 1;
                          AddToTotalTress;
                     end
                     else
                     begin
                          AddCreditoNuevo( oCredito , iNumEmpleado ,sNombre, eStatusEmpleado( cdsEmpleados.FieldByName('STATUS').AsInteger) );
                     end;

                     if ( Locate('NFonacot',NumFonacot ,[]) )then
                     begin
                          Edit;
                          FieldByName('CredTress').AsInteger := FieldByName('CredTress').AsInteger + iCreditoTress;
                          FieldByName('CredCedulas').AsInteger := FieldByName('CredCedulas').AsInteger + 1;
                     end
                     else
                     begin
                          Append;
                          FieldByName('NFonacot').AsString := NumFonacot;
                          FieldByName('NEmpleado').AsString := IntToStr( iNumEmpleado );
                          FieldByName('NomCompleto').AsString := sNombre;
                          FieldByName('RFC').AsString := RFC;
                          FieldByName('CredTress').AsInteger := iCreditoTress;
                          FieldByName('CredCedulas').AsInteger :=  1;
                          FieldByName('Status').AsString:= ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger );
                     end;
                     Post;
                end;
          {end
          else
              Delete;  }
     end;

     if cdsCreditosDiferencias.RecordCount > 0 then
     begin
         with cdsCreditosDiferencias do
         begin
              First;
              dmCliente.SetEmpleadoNumero( FieldByName('NEmpleado').AsInteger);
         end;
     end;
end;

procedure TdmConcilia.AddCreditoNuevo( oCredito :TCreditoFonacot; iNumEmpleado:Integer;sNombre:string; const eStatus: eStatusEmpleado );
begin
     with cdsCreditosNuevos do
     begin
          Append;
          with oCredito do
          begin
               FieldByName('NFonacot').AsString := NumFonacot;
               FieldByName('NEmpleado').AsString := IntToStr( iNumEmpleado );
               FieldByName('NomCompleto').AsString := sNombre;
               FieldByName('RFC').AsString := RFC;
               FieldByName('NCredito').AsString := NumCredito;
               FieldByName('MMensual').AsFloat := MontoMensual ;
               FieldByName('Pagos').AsInteger :=  Plazo;
               FieldByName('Meses').AsInteger :=  Meses;
               //FieldByName('MontoTotal').AsFloat := ( Plazo * MontoMensual );
               FieldByName('MontoTotal').AsFloat := ( Plazo - Meses ) * MontoMensual;
               FieldByName('Posicion').AsInteger := Posicion;
               FieldByName('StatusEmp').AsString:= ObtieneElemento( lfStatusEmpleadoCedula, Ord( eStatus ) );
               FieldByName('Reubicado').AsString:= Reubicado;
          end;
          MergeChangeLog;
     end;
end;


procedure TdmConcilia.AgregarCreditosActivosTress;
function GetRetMensual:double;
begin
     with cdsPrestamos do
     begin
          Result := 0;
          if ( FieldByName('PR_PAGOS').AsInteger > 0 )then
             Result := ( FieldByName('PR_SALDO').AsFloat / FieldByName('PR_PAGOS').AsInteger );
     end;
end;


procedure AddToTotalTress;
   begin
        FTotalCredTress := FTotalCredTress + 1;
   end;

begin
     with cdsPrestamos do
     begin
          First;
          while( not Eof )do
          begin
               with cdsCreditosActivos do
               begin
                    if( cdsPrestamos.FieldByName('PR_STATUS').AsInteger = Ord(spActivo) )then
                    begin
                         Append;
                         FieldByName('NEmpleado').AsInteger := cdsPrestamos.FieldByName('CB_CODIGO').AsInteger;;
                         FieldByName('NomCompleto').AsString := cdsPrestamos.FieldByName('PRETTYNAME').AsString;
                         FieldByName('NFonacot').AsString := cdsPrestamos.FieldByName('CB_FONACOT').AsString;
                         FieldByName('NCredito').AsString := cdsPrestamos.FieldByName('PR_REFEREN').AsString;
                         FieldByName('MPrestado').AsFloat := cdsPrestamos.FieldByName('PR_MONTO').AsFloat;
                         FieldByName('RMensual').AsFloat :=  GetRetMensual;
                         FieldByName('Saldo').AsFloat :=  cdsPrestamos.FieldByName('PR_SALDO').AsFloat;
                         FieldByName('Observa').AsString:= cdsPrestamos.FieldByName('PR_OBSERVA').AsString;
                         if cdsEmpleados.Locate ( 'CB_CODIGO', FieldByName('NEmpleado').AsInteger, [] ) then
                         begin
                              FieldByName('StatusEmp').AsString:= ObtieneElemento( lfStatusEmpleadoCedula, cdsEmpleados.FieldByName('STATUS').AsInteger );
                         end;
                         Post;
                         FTotalPrestado := FTotalPrestado + cdsPrestamos.FieldByName('PR_MONTO').AsCurrency;
                         FTotalSaldo := FTotalSaldo + cdsPrestamos.FieldByName('PR_SALDO').AsCurrency;

                         //agregar al contador de resumen de empleados
                         with dmConcilia.cdsEmpCreditos do
                         begin
                               if ( Locate('NFonacot', cdsPrestamos.FieldByName('CB_FONACOT').AsString ,[]) ) then
                               begin
                                    Edit;
                                    FieldByName('CredTress').AsInteger := FieldByName('CredTress').AsInteger + 1;
                                    AddToTotalTress;
                               end;
                         end;
                    end;
               end;
               Next;
          end;
     end;
end;

procedure TdmConcilia.CargaCedulaFonacot;
begin
      // cdsCedulas.Data := dmCliente.CargaCedulaFonacot( PathDatos );
end;

{---------------------------------------------- Importaci�n-----------------------------------------------}


procedure TdmConcilia.TransferirEmpleadosFonacot( const dFechaKardex:TDateTime );
var
   Parametros :TZetaParams;
   oInfokardex :OleVariant;
   Errores: Integer;

   function GetEstructuraImportacion( Parametros:TZetaParams ):Olevariant;
   begin
        LlenaRenglonAscci( dFechaKardex );
        // Result := dmCliente.ServerRecursos.ImportarKardexGetASCII( dmCliente.Empresa,Parametros.VarValues,cdsAscii.Data,Errores );
   end;

begin
     Parametros := TZetaParams.Create( Self );
     try
        with Parametros do
        begin
             AddInteger( 'Formato', Ord(faASCIIDel) );
             AddInteger( 'FormatoImpFecha',Ord( ifDDMMYYYYs));
             AddString( 'Archivo', 'Conciliaci�n de Fonacot' );
             AddMemo( 'Observaciones', 'Importaci�n datos Fonacot' );
        end;
        with dmCliente do
        begin
             cdsImportKardex.Data := GetEstructuraImportacion( Parametros );
             //Llamada para obtener la estructura de la importacion del kardex
             if ( Errores > 0 ) then         // Si hubo errores quitarlos del DataSet
             begin
                  ClearErroresASCII( cdsImportKardex );
             end;
             oInfokardex :=  ServerRecursos.ImportarKardexGetLista( Empresa,Parametros.VarValues,cdsImportKardex.Data );
             //llamada para insertar el kardex..
             EscribeBitacora( ServerRecursos.ImportarKardexLista( Empresa,
                                                                  oInfoKardex,
                                                                  Parametros.VarValues )
                                                                  );
        end;
     finally
            FreeAndNil( Parametros );
     end;
end;


procedure TdmConcilia.LlenaRenglonAscci( const dFechaKardex:TDateTime );
CONST
     K_FONACOT = 'CB_FONACOT';
var
   sData :string;

   function GetFechaKardex: TDate;
   begin
        with cdsEmpleaFonacot do
        begin
             Result:= dFechaKardex;
             { Nos traemos la fecha de baja para tomar esa fecha en la importaci�n a Kardex e ignoramos los reingresos}
             if ( FieldByName('Status').AsString = ObtieneElemento( lfStatusEmpleadoCedula, Ord(steBaja))  ) then
             begin
                  Result:= dMin( dFechaKardex, FieldByName('FechaBaja').AsDateTime );
             end;
        end;
   end;

begin
     InicializarDataSet( cdsAscii );
     with cdsEmpleaFonacot do
     begin
          First;
          while (not Eof)do
          begin
               sData := IntToStr( FieldByName('NEmpleado').AsInteger ) +','+IntToStr( Ord( eoAsignaCampo ) )+','+
                        FormatDateTime( 'dd/mm/yyyy', GetFechaKardex ) +','+
                        K_FONACOT +','+
                        FieldByName('NFonacot').AsString;

               cdsAscii.Append;
               cdsAscii.FieldByName('Renglon').AsString := sData;
               cdsAscii.FieldByName('DIFERENCIA').AsCurrency := 0.0;
               Next;
          end;
          cdsAscii.MergeChangeLog;
     end;
end;

procedure TdmConcilia.ImportarNuevosCreditos(const dFechaInicial:TDateTime; ConsideraCuotas: Boolean );
var
   Parametros :TZetaParams;
   fMonto:Currency;
   iPagos : Integer;
begin
     DefineColumnasPrestamos(cdsImportPrestamos);
     InicializarDataSet( cdsImportPrestamos );
     Parametros := TZetaParams.Create( Self );
     try
        with cdsCreditosNuevos do
        begin
             First;
             while ( not Eof )do
             begin
                  fMonto := ( FieldByName('Pagos').AsInteger * FieldByName('MMensual').AsFloat );
                  iPagos := cdsCreditosNuevos.FieldByName('Pagos').AsInteger;

                  cdsImportPrestamos.Append;
                  cdsImportPrestamos.FieldByName('CB_CODIGO').AsInteger := FieldByName('NEmpleado').AsInteger;
                  cdsImportPrestamos.FieldByName('PR_TIPO').AsString    := GetTipoPrestamo;//Global
                  cdsImportPrestamos.FieldByName('PR_REFEREN').AsString := FieldByName('NCredito').AsString;
                  cdsImportPrestamos.FieldByName('PR_FECHA').AsDateTime := dFechaInicial;    //Fecha Inicial ;
                  cdsImportPrestamos.FieldByName('PR_MONTO').AsFloat    := fMonto ;
                  if ( ConsideraCuotas ) then
                    cdsImportPrestamos.FieldByName('PR_SALDO_I').AsFloat  := cdsCreditosNuevos.FieldByName('Meses').AsInteger * cdsCreditosNuevos.FieldByName('MMensual').AsFloat //0;
                  else
                    cdsImportPrestamos.FieldByName('PR_SALDO_I').AsFloat  := 0;
                  cdsImportPrestamos.FieldByName('PR_FORMULA').AsString := VACIO;
                  cdsImportPrestamos.FieldByName('PR_SUB_CTA').AsString := VACIO;
                  cdsImportPrestamos.FieldByName('PR_MONTO_S').AsFloat  := fMonto;
                  cdsImportPrestamos.FieldByName('PR_TASA').AsFloat     := 0;
                  cdsImportPrestamos.FieldByName('PR_MESES').AsFloat    := iPagos;
                  cdsImportPrestamos.FieldByName('PR_INTERES').AsFloat  := 0;
                  cdsImportPrestamos.FieldByName('PR_PAGOS').AsFloat    := cdsCreditosNuevos.FieldByName('Meses').AsInteger;
                  cdsImportPrestamos.FieldByName('PR_PAG_PER').AsFloat  := cdsCreditosNuevos.FieldByName('MMensual').AsFloat;
                  cdsImportPrestamos.Post;

                  Next;
             end;
        end;
        with Parametros do
        begin
             AddInteger('TipoImportar',0);
             AddBoolean('PuedeActualizar',False);
             AddInteger('Formato',Ord(faASCIIDel));
             AddString( 'Archivo', 'Conciliaci�n de Fonacot' );
             AddInteger( 'FormatoFecha', Ord( ifDDMMYYYYs) );
             AddBoolean( 'TieneDerechoReglas', True);//Al agregar un prestamo de Fonacot , se brinca las reglas ,colocando una advertencia de informacion para el usuario.
        end;
        // EscribeBitacora(dmCliente.ServerRecursos.ImportarAhorrosPrestamos( dmCliente.Empresa,Parametros.VarValues,cdsImportPrestamos.Data ) );
     Finally
            FreeAndNil( Parametros );
     end;
end;

/// Actualiza las diferencias de Monto Mensual entre los pr�stamos de Fonacot y Tress
procedure TdmConcilia.ActualizarCreditos(const RemoverFormula, PlazoAumenta, PlazoDisminuye, CambioRetencion, RecalculaRetencion: Boolean );
const
     K_IMPORTA_PRESTA = 0;
     K_IMPORTA_CARGOS = 2;
     aTipoMov: array[FALSE..TRUE] of PChar = ( 'A', 'C' );
var
   Parametros :TZetaParams;
   iEmpleado: Integer;
   sTPrestamo, sReferen: String;
   rMontoPlazo, rMontoRetencion: TPesos;

   function GetFormula :string;
   begin
        if(RemoverFormula)then
           Result := VACIO
        else
            Result := cdsCreditosDiferencias.FieldByName('PR_FORMULA').AsString;
   end;

   function GetDescripCambio: String;
   var
      sTextoPlazo, sTextoRetencion: String;
   begin
        if ( rMontoPlazo > 0 ) then
           sTextoPlazo:= 'Sube plazo'
        else if ( rMontoPlazo < 0 ) then
            sTextoPlazo:= 'Baja plazo';
        if ( rMontoRetencion > 0 ) then
           sTextoRetencion := 'Sube pago'
        else if ( rMontoRetencion < 0 ) then
            sTextoRetencion := 'Baja pago';
        Result := 'Conciliador: ' + ConcatString( sTextoPlazo, sTextoRetencion, '/' );
        {
         Conciliador: Sube plazo/Sube pago
         Conciliador: Baja plazo/Sube pago
         Conciliador: Sube plazo/Baja pago
         Conciliador: Baja plazo/Baja pago
        }
   end;

   procedure AgregaCargoAbono;
   var
      rMonto, rMesesAjuste, rMesesPlazo: TPesos;
      lCambioRetencion: Boolean;
   begin
        with cdsCreditosDiferencias do
        begin
             lCambioRetencion := CambioRetencion and ( FieldByName('DiferenciaCredito').AsFloat <> 0 );
             if lCambioRetencion and RecalculaRetencion then
             begin
                  if ( PlazoAumenta or PlazoDisminuye ) then
                     rMesesAjuste := ( FieldByName('CPlazo').AsFloat - FieldByName('CMeses').AsInteger )
                  else
                      rMesesAjuste := ( FieldByName('TressPlazo').AsFloat - FieldByName('TressMeses').AsInteger );
                  rMontoRetencion := ( rMesesAjuste * FieldByName('DiferenciaCredito').AsFloat );
             end
             else
                 rMontoRetencion := 0;
             if ( PlazoAumenta or PlazoDisminuye ) then
             begin
                  rMesesPlazo := ( FieldByName('CPlazo').AsInteger - FieldByName('TressPlazo').AsInteger );
                  rMontoPlazo := ( rMesesPlazo * FieldByName('TressRetencion').AsFloat );
             end
             else
                 rMontoPlazo := 0;

             // Suma ambos montos
             rMonto := rMontoPlazo + rMontoRetencion;

             if ( rMonto <> 0  ) then
             begin
                  with cdsActualizaPCarAbo do
                  begin
                       Append;
                       FieldByName('CB_CODIGO').AsInteger:= iEmpleado;
                       FieldByName('PR_TIPO').AsString := sTPrestamo;
                       FieldByName('PR_REFEREN').AsString := sReferen;
                       FieldByName('CR_FECHA').AsDateTime := dmCliente.FechaDefault;
                       FieldByName('TIPOMOV').AsString:= aTipoMov[ rMonto >= 0 ];
                       FieldByName('MONTO').AsFloat:= Abs( rMonto );
                       FieldByName('CR_OBSERVA').AsString := GetDescripCambio + ' ' + GetDescripcionFechaCortaConciliacion;
                  end;
             end;
        end;
   end;

   procedure EjecutaActualizacion( const cdsDataSet: TServerDataSet; const iTipoActualiza: Integer; lMuestraResultado: Boolean = TRUE );
   begin
        with Parametros do
        begin
             AddInteger('TipoImportar',iTipoActualiza);
             AddBoolean('PuedeActualizar',True);
             AddInteger('Formato',Ord(faASCIIDel));
             AddString( 'Archivo', 'Conciliaci�n de Fonacot' );
             AddInteger( 'FormatoFecha', Ord( ifDDMMYYYYs) );
             AddBoolean( 'TieneDerechoReglas', CheckDerecho( D_EMP_NOM_PRESTAMOS,K_DERECHO_SIST_KARDEX ));
        end;
        // EscribeBitacora(dmCliente.ServerRecursos.ImportarAhorrosPrestamos( dmCliente.Empresa,Parametros.VarValues,cdsDataSet.Data ), lMuestraResultado ) ;
   end;

begin
     DefineColumnasPrestamos(cdsActualizaPrestamos);
     //Define columnas para el wizard de importar cargos y abonos.
     DefineColumnasPrestamos(cdsActualizaPCarAbo, TRUE );
     InicializarDataSet( cdsActualizaPrestamos );
     //Abre el DataSet de Cargo Abono
     InicializarDataSet( cdsActualizaPCarAbo );
     Parametros := TZetaParams.Create( Self );
     try
        with dmConcilia.cdsCreditosDiferenciasTemp do  //dmConcilia.cdsCreditosDiferencias
        begin
             First;
             while ( not Eof )do
             begin
                  cdsActualizaPrestamos.Append;

                  iEmpleado:= FieldByName('NEmpleado').AsInteger;
                  sTPrestamo:= GetTipoPrestamo;
                  sReferen:=  FieldByName('NCredito').AsString;


                  cdsActualizaPrestamos.FieldByName('CB_CODIGO').AsInteger := iEmpleado;
                  cdsActualizaPrestamos.FieldByName('PR_TIPO').AsString    := sTPrestamo;
                  cdsActualizaPrestamos.FieldByName('PR_REFEREN').AsString := sReferen;

                  cdsActualizaPrestamos.FieldByName('PR_FECHA').AsDateTime := FieldByName('PR_FECHA').AsDateTime;
                  cdsActualizaPrestamos.FieldByName('PR_MONTO').AsFloat    := FieldByName('PR_MONTO').AsFloat;
                  //cdsActualizaPrestamos.FieldByName('PR_SALDO_I').AsFloat  := FieldByName('PR_SALDO_I').AsFloat;
                  cdsActualizaPrestamos.FieldByName('PR_SALDO_I').AsFloat  := FieldByName('PR_SALDO_I').AsFloat;
                  cdsActualizaPrestamos.FieldByName('PR_FORMULA').AsString := GetFormula;
                  cdsActualizaPrestamos.FieldByName('PR_SUB_CTA').AsString := FieldByName('PR_SUB_CTA').AsString;
                  cdsActualizaPrestamos.FieldByName('PR_MONTO_S').AsFloat  := FieldByName('PR_MONTO_S').AsFloat;
                  cdsActualizaPrestamos.FieldByName('PR_TASA').AsFloat     := FieldByName('PR_TASA').AsFloat;
                  {***US 12872: Conciliador Fonacot - El proceso que actualiza datos de cr�ditos no respeta la opci�n que se marca y actualiza todas las diferencias***}
                  if ( PlazoAumenta ) or ( PlazoDisminuye ) then
                  begin
                       cdsActualizaPrestamos.FieldByName('PR_MESES').AsFloat    := FieldByName('CPlazo').AsInteger;
                       {*** Mantener la retencion de Tress Ya registrado, Solo si los Filtros seleccionado son: Aumeta Plazo o Disminuye plazo
                            Si no selecciona cambio retencion, Mantener la Retencion de Tress Ya registrado***}
                       if not ( CambioRetencion ) then
                          cdsActualizaPrestamos.FieldByName('PR_PAG_PER').AsFloat := FieldByName('TressRetencion').AsFloat;
                  end;
                  cdsActualizaPrestamos.FieldByName('PR_INTERES').AsFloat  := FieldByName('PR_INTERES').AsFloat;
                  {***US 12872: Conciliador Fonacot - El proceso que actualiza datos de cr�ditos no respeta la opci�n que se marca y actualiza todas las diferencias
                   ***Cuando CambioRetencion or PlazoAumenta  PlazoDisminuye se actualiza este dato ***}
                  if ( PlazoAumenta ) or ( PlazoDisminuye ) then
                     cdsActualizaPrestamos.FieldByName('PR_PAGOS').AsFloat    := FieldByName('CMeses').AsInteger
                  else
                      //Mantener Cuantos Pagos de Tress
                      cdsActualizaPrestamos.FieldByName('PR_PAGOS').AsFloat := FieldByName('TressMeses').AsInteger;

                  {***US 12872: Conciliador Fonacot - El proceso que actualiza datos de cr�ditos no respeta la opci�n que se marca y actualiza todas las diferencias***}
                  if CambioRetencion then
                  begin
                       cdsActualizaPrestamos.FieldByName('PR_PAG_PER').AsFloat  := FieldByName('CRetencion').AsFloat;
                       //Mantener el Plazo de Tress Ya registrado, Solo si el Filtro seleccionado es cambio en Retencion
                       if not ( PlazoAumenta ) and not ( PlazoDisminuye ) then
                          cdsActualizaPrestamos.FieldByName('PR_MESES').AsFloat := FieldByName('TressPlazo').AsInteger;
                  end;
                  cdsActualizaPrestamos.Post;
                  {***US 12872: Conciliador Fonacot - El proceso que actualiza datos de cr�ditos no respeta la opci�n que se marca y actualiza todas las diferencias***}
                  if ( PlazoAumenta ) or ( PlazoDisminuye ) then
                  begin
                       if ( FieldByName('ActualizaMontoP').AsBoolean ) then
                       begin
                            AgregaCargoAbono;
                       end;
                  end;

                  Next;
             end;
        end;
        if ( PlazoAumenta ) or ( PlazoDisminuye ) then
           EjecutaActualizacion( cdsActualizaPCarAbo, K_IMPORTA_CARGOS, FALSE );
        EjecutaActualizacion( cdsActualizaPrestamos, K_IMPORTA_PRESTA );

     Finally
            FreeAndNil( Parametros );
     end;
end;


procedure TdmConcilia.DefineColumnasPrestamos(sdsData:TServerDataSet; const lEsPCarAbo: Boolean);
begin
     if ( sdsData.FieldCount <= 0 )then
     begin
          if ( lEsPCarAbo ) then
          begin
               AgregaColumna( 'CB_CODIGO', tgNumero, K_ANCHO_NUMEROEMPLEADO, sdsData );
               AgregaColumna( 'PR_TIPO', tgTexto, K_ANCHO_CODIGO1, sdsData);
               AgregaColumna( 'PR_REFEREN', tgTexto, K_ANCHO_REFERENCIA, sdsData);
               AgregaColumna( 'CR_FECHA', tgFecha, K_ANCHO_FECHA, sdsData);
               AgregaColumna( 'TIPOMOV', tgTexto, K_ANCHO_CODIGO1, sdsData);
               AgregaColumna( 'MONTO', tgFloat, K_ANCHO_PESOS, sdsData );
               AgregaColumna( 'CR_OBSERVA', tgTexto, K_ANCHO_DESCLARGA, sdsData );//Mejoras FonaCot - Dominio BD DescLarga
          end
          else
          begin
               AgregaColumna( 'CB_CODIGO', tgNumero, K_ANCHO_NUMEROEMPLEADO,sdsData);
               AgregaColumna( 'PR_TIPO', tgTexto, K_ANCHO_CODIGO1, sdsData);
               AgregaColumna( 'PR_REFEREN', tgTexto, K_ANCHO_REFERENCIA,sdsData );
               AgregaColumna( 'PR_FECHA', tgFecha, K_ANCHO_FECHA,sdsData);
               AgregaColumna( 'PR_MONTO', tgFloat, K_ANCHO_PESOS,sdsData);
               AgregaColumna( 'PR_SALDO_I', tgFloat, K_ANCHO_PESOS,sdsData);
               AgregaColumna( 'PR_FORMULA', tgTexto, K_ANCHO_FORMULA,sdsData );
               AgregaColumna( 'PR_SUB_CTA', tgTexto, K_ANCHO_DESCRIPCION,sdsData );
               AgregaColumna( 'PR_MONTO_S', tgFloat, K_ANCHO_PESOS,sdsData);
               AgregaColumna( 'PR_TASA', tgFloat, K_ANCHO_TASA,sdsData );
               AgregaColumna( 'PR_MESES', tgNumero, K_ANCHO_STATUS,sdsData );
               AgregaColumna( 'PR_INTERES', tgFloat, K_ANCHO_PESOS,sdsData );
               AgregaColumna( 'PR_PAGOS', tgNumero, K_ANCHO_STATUS,sdsData );
               AgregaColumna( 'PR_PAG_PER', tgFloat, K_ANCHO_PESOS,sdsData);
          end;
     end;
end;



procedure TdmConcilia.AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
begin
     // Agrega Campo al DataSet
     with oSDataSet do
     begin
          case eTipo of
               tgBooleano : AddBooleanField( sCampo );
               tgFloat : AddFloatField( sCampo );
               tgNumero : AddIntegerField( sCampo );
               tgFecha : AddDateField( sCampo );
          else
               AddStringField( sCampo, iLongitud );
          end;
     end;
end;

procedure TdmConcilia.cdsEmpleaFonacotNEmpleadoValidate(Sender: TField);
begin
     if ( not Conciliando )then
     begin
          with Sender do
          begin
               if ( AsInteger  >= 0 ) then
               begin
                    if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                         DataBaseError( 'No Existe el Empleado #' + IntToStr( AsInteger ) );
               end
               else
                   DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
          end;
     end;
end;

procedure TdmConcilia.cdsEmpleaFonacotNEmpleadoChange(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               cdsEmpleaFonacot.FieldByName('NomTress').AsString := dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) ;    // Posiciona el empleado en cdsEmpleadoLookup
          end;
     end;
end;

{ Eventos de la bit�cora }
procedure TdmConcilia.LogInit( sPath,sProceso:string );
begin
     with FBitacora do
     begin
          try
             if not Used then
             begin
                  Init( sPath );
             end;
             WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ) );
             WriteTexto( Format( 'Empresa: %s', [ dmCliente.GetDatosEmpresaActiva.Nombre ] ) );
             WriteTexto( StringOfChar( '-', 40 ) );
             WriteTexto( sProceso );

             except on Error: Exception do
             begin
                  Raise Exception.Create( 'Error al grabar la bit�cora' );
             end;
             
          end;
     end;
end;

procedure TdmConcilia.EscribeBitacora(Resultado: OleVariant; lMuestraResultado: Boolean);
const
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
begin
     with TProcessInfo.Create( nil ) do
     begin
          try
             SetResultado(Resultado);
             case Status of
                  epsEjecutando: Log( '� Proceso Incompleto !' );
                  epsOK:
                  begin
                       if ( TotalErrores > 0 ) then
                          Log( Format( 'Termin� Con %d Errores', [ TotalErrores ] ) )
                       else
                           if ( TotalAdvertencias > 0 ) then
                              Log( Format( 'Termin� Con %d Advertencias', [ TotalAdvertencias ] ) )
                           else
                               if ( TotalEventos > 0 ) then
                                  Log( Format( 'Termin� Con %d Mensajes', [ TotalEventos ] ) )
                                else
                                    Log( 'Termin� Con Exito' );
                  end;
                  epsCancelado: Log( '� Proceso Cancelado !' );
                  epsError: Log( '� Proceso Con Errores !' );
             end;
             Log( Format( 'Folio:    %d', [ Folio ] ) );
             Log( Format( 'Inicio:   %s', [ FormatDateTime( K_DIA_HORA, Inicio ) ] ) );
             Log( Format( 'Fin:      %s', [ FormatDateTime( K_DIA_HORA, Fin ) ] ) );
             if ( lMuestraResultado ) then
                Check( Resultado )
             else
                 CheckSilencioso( Resultado );
          finally
                 Free;
          end;
     end;
end;

function TdmConcilia.CheckSilencioso(const Resultado: OleVariant ): Boolean;
begin
     Result:= TRUE;
     if eProcessStatus( Resultado[ K_PROCESO_STATUS ] ) <> epsOK then
     begin
          Result := Check( Resultado );
     end;
end;

procedure TdmConcilia.Log( sTexto:string );
begin
     with FBitacora do
     begin
          WriteTexto( sTexto );
     end;
end;

procedure TdmConcilia.LogEnd;
begin
     with FBitacora do
     begin
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Final %s ' + StringOfChar( '*', K_RELLENO ) );
          WriteTexto( '' );
          Close;
     end;
end;

function TdmConcilia.ExistenErroresCedula:Boolean;
begin
     Result := FErroresCedula;
end;

/// Forma de informaci�n del proceso ..

function TdmConcilia.Check( const Resultado: OleVariant): Boolean;
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             with ProcessData do
             begin
                  SetResultado( Resultado );
                  Result := ( Status in [ epsEjecutando, epsOK ] );
             end;
            LeerUnProceso( ShowProcessInfo(True));
          finally
             Free;
          end;
     end;
end;

procedure TdmConcilia.LeerUnProceso( const iFolio: Integer );
var
   oCursor: TCursor;
begin
     if ( iFolio > 0 ) then
     begin
          with cdsProcesos do
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := oCursor;
               try
                  // Data := dmCliente.ServerConsultas.GetProcess( dmCliente.Empresa, iFolio );
                  Data := dmConsultas.ServerConsultas.GetProcess( dmCliente.Empresa, iFolio );
               finally
                      Screen.Cursor := oCursor;
               end;
               if IsEmpty then
                  ZetaDialogo.ZError( '� Bit�cora Vac�a !', 'La Bit�cora Del Proceso # ' + IntToStr( iFolio ) + ' No Existe', 0 )
               else
                   Modificar;
          end;
     end;
end;


procedure TdmConcilia.cdsProcesosAlModificar(Sender: TObject);
begin
     with cdsProcesos do
     begin
          if not IsEmpty then
          begin
               FProcessLogShow_DevEx.GetBitacora( Procesos(FieldByName( 'PC_PROC_ID' ).AsInteger) ,
                                            FieldByName( 'PC_NUMERO' ).AsInteger );
          end;
     end;
end;


function TdmConcilia.GetFieldPrestamo(const sReferencia, sCampo: String; iEmpleado: Integer ): String;
begin
     Result := VACIO;
     with cdsPrestamos do
     begin
          if ( Locate( 'PR_REFEREN;CB_CODIGO',VarArrayOf([sReferencia, iEmpleado]),[])) then
             Result:= FieldByName(sCampo).AsString;
     end;
end;


{ ////////////////////////// Confrontaci�n ////////////////////////////////////}
procedure TdmConcilia.Confrontar( const Anio: Integer;const Mes:Integer);
var
   oCredito :TCreditoFonacot;
   EmpleadoCalculado,EmpleadoEncontrado, lEmpleadoEsBaja: Boolean;
   iNumEmpleado: Integer;
   dUltimoDiaMes: TDate;
   /// Si se enecontro el empleado calculado obtiene el codigo del empleado en Tress
   function GetNumEmpleado : Integer;
   begin
        if ( EmpleadoEncontrado )then
        begin
             Result := cdsEmpleadosFonacot.FieldByName('CB_CODIGO').AsInteger;
        end
        else
            Result := 0;
   end;
   ///Si encontro el empleado como calculado regresa la retencion de Tress
   function GetRetTress :Currency;
   begin
        if ( EmpleadoCalculado )then
        begin
             Result := cdsRetCreditos.FieldByName('FC_NOMINA').AsFloat + cdsRetCreditos.FieldByName('FC_AJUSTE').AsFloat;
        end
        else
            Result := 0;
   end;
   //Obtener la incidencia si el empleado esta en Tress
   function GetIncidencia :String;
   var
      sIncidencia: String[ 1 ];
   begin
        Result := K_SIN_INCIDE;
        sIncidencia:= cdsRetCreditos.FieldByName('FE_INCIDE').AsString;

        if ( EmpleadoCalculado ) then
        begin
             case sIncidencia[ 1 ] of
                  K_INCAPA:
                  begin
                       //if ( cdsConfrontacion.FieldByName('RetCedula').AsFloat > cdsConfrontacion.FieldByName('RetTress').AsFloat ) then
                          Result:= sIncidencia;
                  end;
                  K_BAJA: Result:= sIncidencia;
             end;
        end
        else if ( lEmpleadoEsBaja )  then
        begin
             Result:= K_BAJA;
        end
   end;

   //Obtener la Fecha Inicio  si el empleado esta en Tress
   function GetFechaIni :TDateTime;
   begin
        Result := NullDateTime; 

        if ( cdsConfrontacion.FieldByName('Incidencia').AsString  <> K_SIN_INCIDE )
        {$ifdef NO_FECHA_INC}
           { 29/05/2009: Fonacot no pide las fechas de inicio y fin en la incapacidad}
           and ( cdsConfrontacion.FieldByName('Incidencia').AsString <> K_INCAPA )
        {$endif}
        then
        begin
             if ( EmpleadoCalculado ) then
             begin
                  Result := cdsRetCreditos.FieldByName('FE_FECHA1').AsDateTime;
             end
             else if ( lEmpleadoEsBaja ) then
             begin
                  Result:= cdsEmpleados.FieldByName('CB_FEC_BAJ').AsDateTime;
             end;
        end;
   end;

   //Obtener la Fecha Final si el empleado esta en Tress
   function GetFechaFin :TDateTime;
   begin
        Result := NullDateTime;
        if ( cdsConfrontacion.FieldByName('Incidencia').AsString  <> K_SIN_INCIDE )
        {$ifdef NO_FECHA_INC}
           { 29/05/2009: Fonacot no pide las fechas de inicio y fin en la incapacidad}
           and ( cdsConfrontacion.FieldByName('Incidencia').AsString <> K_INCAPA )
        {$endif}
        then
        begin
             if ( EmpleadoCalculado )  then
             begin
                  Result := cdsRetCreditos.FieldByName('FE_FECHA2').AsDateTime;
             end;
        end;
   end;

begin
     oCredito := TCreditoFonacot.Create;
     // Pruebas.
     {FAnio := Anio;
     FMes := Mes;}

     // dUltimoDiaMes:= LastDayofMonth( EncodeDate( FAnio, FMes, 1 ) );
     dUltimoDiaMes:= LastDayofMonth( EncodeDate( 2017, 9, 1 ) );
     // ----- -----
     try
        if PreparaConfrontar then
        begin
             with cdsCedulas do
             begin
                  // TressShell.ConciliarStart( RecordCount );
                  First;
                  while( not Eof )do
                  // while ( not Eof and TressShell.ConciliarStep ) do
                  begin
                       oCredito.SetCreditoFromCedula( cdsCedulas );
                       //Obtener la sumatoria de la cedula
                       FTotalCedula := FTotalCedula + oCredito.MontoMensual;
                       if ( CedulaNoTieneErrores )then
                       begin
                            EmpleadoEncontrado := cdsEmpleadosFonacot.Locate('CB_FONACOT',oCredito.NumFonacot,[] );
                            iNumEmpleado:= GetNumEmpleado;
                            EmpleadoCalculado := EmpleadoEncontrado and cdsRetCreditos.Locate( 'PR_REFEREN;CB_CODIGO',VarArrayOf([oCredito.NumCredito,cdsEmpleadosFonacot.FieldByName('CB_CODIGO').AsInteger]),[] );
                            lEmpleadoEsBaja:= ( cdsEmpleados.Locate('CB_CODIGO', iNumEmpleado, [] ) ) and
                                              ( cdsEmpleados.FieldByName('STATUS').AsInteger = Ord( steBaja ) ) and
                                              ( cdsEmpleados.FieldByName('CB_FEC_BAJ').AsDateTime <= dUltimoDiaMes );

                            cdsConfrontacion.Append;
                            cdsConfrontacion.FieldByName('NFonacot').AsString := oCredito.NumFonacot;
                            cdsConfrontacion.FieldByName('NEmpleado').AsInteger := iNumEmpleado;
                            cdsConfrontacion.FieldByName('NomCompleto').AsString := oCredito.NomCompleto;
                            cdsConfrontacion.FieldByName('RFC').AsString := oCredito.RFC;
                            cdsConfrontacion.FieldByName('Plazo').AsInteger := oCredito.Plazo;
                            cdsConfrontacion.FieldByName('Meses').AsInteger := oCredito.Meses;
                            cdsConfrontacion.FieldByName('NCredito').AsString := oCredito.NumCredito;
                            cdsConfrontacion.FieldByName('RetCedula').AsFloat := oCredito.MontoMensual;
                            cdsConfrontacion.FieldByName('RetTress').AsFloat := GetRetTress;
                            cdsConfrontacion.FieldByName('Incidencia').AsString := GetIncidencia;
                            cdsConfrontacion.FieldByName('FecInicio').AsDateTime := GetFechaIni;
                            cdsConfrontacion.FieldByName('FecFin').AsDateTime := GetFechaFin;
                            cdsConfrontacion.FieldByName('StatusEmp').AsString:= GetStatusEmpleado( iNumEmpleado );
                            cdsConfrontacion.FieldByName('SaldoAjus').AsFloat:= GetAjusteMesAnterior( iNumEmpleado, oCredito.NumCredito );
                            cdsConfrontacion.FieldByName('StatusPrestamo').AsInteger:= StrAsInteger( GetFieldPrestamo( oCredito.NumCredito, 'PR_STATUS', iNumEmpleado ) ) ;
                            if ( StrAsInteger( GetFieldPrestamo( oCredito.NumCredito, 'PR_STATUS', iNumEmpleado ) )  = Ord(spSaldado) ) then
                                cdsConfrontacion.FieldByName('Observa').AsString:= K_MSJ_CREDITO_SALDADO
                            else
                                cdsConfrontacion.FieldByName('Observa').AsString:= GetFieldPrestamo( oCredito.NumCredito, 'PR_OBSERVA', iNumEmpleado );
                            cdsConfrontacion.FieldByName('Reubicado').AsString:= oCredito.Reubicado; //Nueva columna de reubicado en c�dula
                            cdsConfrontacion.FieldByName('DIFERENCIA').AsFloat := Abs( oCredito.MontoMensual - GetRetTress );
                            cdsConfrontacion.FieldByName('DiferenciaCredito').AsFloat := oCredito.MontoMensual - GetRetTress;
                            cdsConfrontacion.Post;

                            Delete;
                            MergeChangeLog;
                            //borrar el credito encontrado para dejar las altas
                            if EmpleadoCalculado then
                            begin
                                 cdsRetCreditos.Delete;
                                 cdsRetCreditos.MergeChangeLog;
                            end;
                       end
                       else
                       begin
                            FErroresCedula:= True;
                            Next;
                       end;
                  end;
                  AgregarCreditosAltas;
                  cdsConfrontacion.MergeChangeLog;
                  // TressShell.ConciliarEnd;
             end;
        end;
     finally
            FreeAndNil(oCredito);
     end;
     {Se cambi� porque se consulta FT_TAJUST
     FinalizaConfrontar; }
end;

///Agregar las incidencias de altas a la cedula de Fonacot
procedure TdmConcilia.AgregarCreditosAltas;
var
   r_ret_tress: TPesos;
begin
     GetDataConsulta(4,cdsPrestamosAltas);
     with cdsRetCreditos do
     begin
          First;
          FTotalAltas := RecordCount;
          while( not Eof )do
          begin
               {Se cambi� debido a que cdsEmpleadosFonacot, solamente contiene los empleados que traen n�mero de Fonacot.
                Se asegura que el empleado que se gener� en el pago, exista en Tress, para poderlo posicionar correctamente.}
               if ( cdsEmpleados.Locate( 'CB_CODIGO',FieldByName('CB_CODIGO').AsInteger,[] ) )  then
               begin
                    cdsPrestamosAltas.Locate( 'PR_REFEREN;CB_CODIGO',VarArrayOf([FieldByName('PR_REFEREN').AsString,FieldByName('CB_CODIGO').AsInteger]),[]);
                    r_ret_tress:= FieldByName('FC_NOMINA').AsFloat + FieldByName('FC_AJUSTE').AsFloat;

                    { Si no tiene retenci�n no se agrega, marca error al subirlo a p�gina de Internet }
                    if  ( r_ret_tress > 0 ) then
                    begin
                         cdsConfrontacion.Append;
                         cdsConfrontacion.FieldByName('NFonacot').AsString :=  cdsEmpleados.FieldByName('CB_FONACOT').AsString;
                         cdsConfrontacion.FieldByName('NEmpleado').AsInteger := FieldByName('CB_CODIGO').AsInteger;
                         cdsConfrontacion.FieldByName('NomCompleto').AsString := StrTransform( cdsEmpleados.FieldByName('PRETTYNAME').AsString,',',VACIO);
                         cdsConfrontacion.FieldByName('RFC').AsString := cdsEmpleados.FieldByName('CB_RFC').AsString;
                         cdsConfrontacion.FieldByName('NCredito').AsString := FieldByName('PR_REFEREN').AsString;
                         cdsConfrontacion.FieldByName('Plazo').AsInteger := cdsPrestamosAltas.FieldByName('PR_MESES').AsInteger;
                         cdsConfrontacion.FieldByName('Meses').AsInteger := 0;// no se puede saber cual es el mes en que va se tiene que capturar....
                         // Al dar de alta un cr�dito se iguala los montos
                         cdsConfrontacion.FieldByName('RetCedula').AsFloat := 0;
                         cdsConfrontacion.FieldByName('RetTress').AsFloat := r_ret_tress;
                         cdsConfrontacion.FieldByName('Incidencia').AsString := K_ALTA;
                         //Por prioridad de Alta se coloca las fechas vac�as
                         cdsConfrontacion.FieldByName('FecInicio').AsDateTime := NullDateTime;
                         cdsConfrontacion.FieldByName('FecFin').AsDateTime := NullDateTime;
                         cdsConfrontacion.FieldByName('StatusEmp').AsString:= GetStatusEmpleado( cdsEmpleados.FieldByName('CB_CODIGO').AsInteger );
                         cdsConfrontacion.FieldByName('SaldoAjus').AsFloat:= GetAjusteMesAnterior( FieldByName('CB_CODIGO').AsInteger,
                                                                                                   FieldByName('PR_REFEREN').AsString );

                         cdsConfrontacion.FieldByName('StatusPrestamo').AsInteger:= StrAsInteger( GetFieldPrestamo(FieldByName('PR_REFEREN').AsString, 'PR_STATUS',
                                                                                                      FieldByName('CB_CODIGO').AsInteger ) );

                         cdsConfrontacion.FieldByName('Observa').AsString:=GetFieldPrestamo(FieldByName('PR_REFEREN').AsString, 'PR_OBSERVA',
                                                                                                      FieldByName('CB_CODIGO').AsInteger );

                         cdsConfrontacion.Post;
                    end
                    else
                        Dec( FTotalAltas );
               end;
               Next;
          end;
          cdsConfrontacion.MergeChangeLog;
     end;
end;

function TdmConcilia.GetStatusEmpleado( const iEmpleado: Integer ): String;
begin
     if ( RazonSocial <> VACIO ) then
        Result := K_EMPLEADO_INVALIDO_POR_RAZON_SOCIAL
     else
        Result:= K_EMPLEADO_INVALIDO;

     with cdsEmpleados do
     begin
          if ( Locate( 'CB_CODIGO', iEmpleado, [] ) ) then
             Result:= ObtieneElemento( lfStatusEmpleadoCedula, FieldByName('STATUS').AsInteger );
     end;
end;


//Regresa la descipci�n del estatus del calculo de Fonacot
function TdmConcilia.GetStatusCalculoFonacot:string;
begin
     Result := 'Desconocido';
     case cdsTotales.FieldByName('FT_STATUS').AsInteger of
          0: Result := 'Sin calcular';
          1: Result := 'Calculada parcial';
          2: Result := 'Calculada total';
          3: Result := 'Afectada total';
     end;

end;

// Preparando la confrontaci�n
function TdmConcilia.PreparaConfrontar:Boolean;
begin
     //Obtener los totales
     EnMascaraFechas( TRUE );
     FTotalCedula := 0;
     GetTotalesCalculo;
     Result := ( GetDataConsulta(1,cdsRetCreditos) and GetDataConsulta(0,cdsEmpleadosFonacot) and
                 GetDataConsulta(K_TIPO_AJUSTE, cdsPrestaAjus, cdsTotales.FieldByName('FT_TAJUST').AsString ));
     if Result then
     begin
          GetCargosAbonos;
          CargaCedulaFonacot;
          InicializarDataSet( cdsConfrontacion );
          FTotalCredCed := cdsCedulas.RecordCount;
          GetEmpleados;
     end;
end;

// Preparando la confrontaci�n
procedure TdmConcilia.GetTotalesCalculo;
begin
     GetDataConsulta(2,cdsPrestamos);
     GetDataConsulta(3,cdsTotales );
end;

//Al mostrar la fechas
procedure TdmConcilia.cdsConfrontacionFecInicioGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsDateTime = NULLDATETIME ) then
        Text := VACIO
     else
         Text := Sender.AsString;
end;

///////////////////////////////////////////////////////////////////////////////
/// Eventos de los procesos de bit�cora
///////////////////////////////////////////////////////////////////////////////

procedure TdmConcilia.GetProcessLog( const iProceso: Integer );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        // cdsLogDetail.Data := dmCliente.ServerConsultas.GetProcessLog( dmCliente.Empresa, iProceso );
        cdsLogDetail.Data := dmConsultas.ServerConsultas.GetProcessLog( dmCliente.Empresa, iProceso );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmConcilia.cdsLogDetailAfterOpen(DataSet: TDataSet);
begin
     with cdsLogDetail do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_CLASE', lfClaseBitacora );
          MaskFecha( 'BI_FECHA' );
          MaskFecha( 'BI_FEC_MOV' );
     end;
end;

procedure TdmConcilia.cdsLogDetailAlModificar(Sender: TObject);
begin
     if not cdsLogDetail.IsEmpty then
        FBaseEditBitacora_DevEx.ShowLogDetail( FormaBitacora_DevEx, TFormaBitacora_DevEx, cdsLogDetail );
end;

procedure TdmConcilia.cdsProcesosCalcFields(DataSet: TDataSet);
var
   rAvance: Extended;
   iMaximo: Integer;
   dInicial, dFinal, dValue: TDateTime;
   eStatus: eProcessStatus;
   i: integer;
   sTemp, sArchivo: string;

function GetFechaHora( const dValue: TDateTime ): String;
const
     K_SHOW_DATE_TIME = 'hh:nn:ss AM/PM dd/mmm/yyyy';
begin
     if ( dValue = NullDateTime ) then
        Result := ''
     else
         Result := FormatDateTime( K_SHOW_DATE_TIME, dValue );
end;

begin
     eStatus := GetStatusProceso;
     with Dataset do
     begin
          iMaximo := FieldByName( 'PC_MAXIMO' ).AsInteger;
          if ( eStatus = epsOK ) then
             rAvance := 1
          else
              rAvance := ZetaCommonTools.rMax( 0.01, FieldByName( 'PC_PASO' ).AsInteger / ZetaCommonTools.iMax( 1, iMaximo ) );
          dInicial := ZetaClientTools.AddDateTime( FieldByName( 'PC_FEC_INI' ).AsDateTime,
                                                   FieldByName( 'PC_HOR_INI' ).AsString );
          dFinal := ZetaClientTools.AddDateTime( FieldByName( 'PC_FEC_FIN' ).AsDateTime,
                                                 FieldByName( 'PC_HOR_FIN' ).AsString );
          dValue := ZetaClientTools.GetElapsed( dFinal, dInicial );
          FieldByName( 'PC_INICIO' ).AsString := GetFechaHora( dInicial );
          FieldByName( 'PC_FIN' ).AsString := GetFechaHora( dFinal );
          FieldByName( 'PC_AVANCE' ).AsString := FormatFloat( '#0.# %', 100 * rAvance );
          FieldByName( 'PC_TIEMPO' ).AsString := ZetaClientTools.GetDuration( dValue );
          with FieldByName( 'PC_FALTA' ) do
          begin
               if ( rAvance < 0.001 ) or ( eStatus <> epsEjecutando ) then
                  AsString := ''
               else if ( rAvance < 0.15 ) then  // Avance menor al 15%
                  AsString := '???'
               else
                  AsString := '???';//GetEstimatedTime( dValue * ( ( 1 / rAvance ) - 1 ) );
          end;
          with FieldByName( 'PC_STATUS' ) do
          begin
               case eStatus of
                    epsEjecutando: AsString := 'Abierto';
                    epsCancelado: AsString := 'Cancelado';
                    epsOK: AsString := 'OK';
               else
                   AsString := 'Con Error';
               end;
          end;
          FieldByName( 'PC_SPEED' ).AsString := ZetaWizardFeedBack_DevEx.GetProcessSpeed( iMaximo, dInicial, dFinal );
          
          //Campo de Par�metros del Proceso
          sArchivo := FieldByName( 'PC_PARAMS' ).AsString;
          if ( sArchivo <> VACIO ) then
          begin
               for i := 1 to ( Length( sArchivo ) ) do
               begin
                    if ( sArchivo[ i ] = ZetaCommonClasses.K_PIPE ) then
                    begin
                         FieldByName( 'PC_PARAM' ).ASString := FieldByName( 'PC_PARAM' ).ASString + sTemp + CR_LF;
                         sTemp := '';
                    end
                    else
                        sTemp := sTemp + sArchivo[ i ];
               end;
               FieldByName( 'PC_PARAM' ).ASString := FieldByName( 'PC_PARAM' ).ASString + sTemp;
          end;
     end;

end;

function TdmConcilia.GetStatusProceso: eProcessStatus;
begin
     Result := ZetaClientTools.GetProcessStatus( cdsProcesos.FieldByName( 'PC_ERROR' ).AsString );
end;

procedure TdmConcilia.cdsProcesosAfterOpen(DataSet: TDataSet);
begin
 with cdsProcesos do
     begin
          MaskFecha( 'PC_FEC_INI' );
          MaskFecha( 'PC_FEC_FIN' );
          ConfigProcIdField( FieldByName( 'PC_PROC_ID' ) );
          with FieldByName( 'US_CODIGO' ) do
          begin
               Alignment := taLeftJustify;
               //OnGetText := UsuarioGetText;
          end;
          //FieldByName( 'US_CANCELA' ).OnGetText := UsuarioGetText;
          //FieldByName( 'PC_MAXIMO' ).OnGetText := PC_MAXIMOGetText;
          // Para que no salga Empleado '0'
          TNumericField( FieldByName( 'CB_CODIGO' ) ).DisplayFormat := '0;;#';
     end;
end;


procedure TdmConcilia.GetProcessText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Sender.Dataset.IsEmpty then
             Text := ''
          else
              Text := ZetaClientTools.GetProcessName( Procesos( Sender.AsInteger ) );
     end
     else
         Text := Sender.AsString;
end;

procedure TdmConcilia.ConfigProcIdField( Campo: TField );
begin
     with Campo do
     begin
          OnGetText := GetProcessText;
          Alignment := taLeftJustify;
     end;
end;

procedure TdmConcilia.cdsProcesosAlCrearCampos(Sender: TObject);
begin
     with cdsProcesos do
     begin
          CreateCalculated( 'PC_AVANCE', ftString, 10 );
          CreateCalculated( 'PC_TIEMPO', ftString, 40 );
          CreateCalculated( 'PC_FALTA', ftString, 20 );
          CreateCalculated( 'PC_STATUS', ftString, 10 );
          CreateCalculated( 'PC_INICIO', ftString, 30 );
          CreateCalculated( 'PC_FIN', ftString, 30 );
          CreateCalculated( 'PC_SPEED', ftString, 30 );
          CreateCalculated( 'PC_PARAM', ftString, 255 );
     end;
end;



{--------------------------Generacion de la c�dula CSV---------------------}
procedure TdmConcilia.ExportarCedulaFonacot( Sender: TAsciiExport; DataSet: TDataset );
var
   MontoCedula:Currency;
   function GetFecha(dFecha:TDateTime):string;
   begin
        if( dFecha = NullDateTime )then
           Result := VACIO
        else
           Result := FechaAsStr ( dFecha );
   end;
begin
     with Sender do
     begin
          with Dataset do
          begin
               MontoCedula := FieldByName( 'RetCedula' ).AsCurrency;
               if (FieldByName( 'Incidencia' ).AsString = K_ALTA )then
               begin
                    MontoCedula := FieldByName( 'RetTress' ).AsCurrency
               end;
               ColumnByName( 'NFonacot' ).Datos := FieldByName( 'NFonacot' ).AsString;
               ColumnByName( 'RFC' ).Datos := FieldByName( 'RFC' ).AsString;
               ColumnByName( 'NomCompleto' ).Datos := FieldByName( 'NomCompleto' ).AsString;
               ColumnByName( 'NCredito' ).Datos := FieldByName( 'NCredito' ).AsString;
               ColumnByName( 'RetCedula' ).Datos := CurrToStr( MontoCedula );
               ColumnByName( 'NEmpleado' ).Datos := IntToStr( FieldByName( 'NEmpleado' ).AsInteger );
               ColumnByName( 'Plazo' ).Datos := IntToStr( FieldByName( 'Plazo' ).AsInteger );
               ColumnByName( 'Meses' ).Datos := IntToStr( FieldByName( 'Meses' ).AsInteger );
               ColumnByName( 'RetTress' ).Datos := CurrToStr( FieldByName( 'RetTress' ).AsCurrency );
               ColumnByName( 'Incidencia' ).Datos := FieldByName( 'Incidencia' ).AsString ;
               ColumnByName( 'FecInicio' ).Datos := GetFecha ( FieldByName('FecInicio' ).AsDateTime );
               ColumnByName( 'FecFin' ).Datos := GetFecha( FieldByName( 'FecFin' ).AsDateTime );
               ColumnByName( 'Reubicado').Datos := FieldByName('Reubicado').AsString;
          end;
     end;
end;



function TdmConcilia.CrearAsciiCedula (cdsDataSet: TZetaClientDataSet;const sArchivoCedula:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoCedula, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  Delimitador := VACIO;
                  AgregaRenglon('No_FONACOT,RFC,NOMBRE,No_CREDITO,RETENCION_MENSUAL,CLAVE_EMPLEADO,PLAZO,MESES,RETENCION_REAL,INCIDENCIA,FECHA_INI_BAJA,FECHA_FIN,REUBICADO');
                  AgregaColumna( 'NFonacot', tgTexto, 30 );
                  AgregaColumna( 'RFC', tgTexto, 13 );
                  AgregaColumna( 'NomCompleto',tgTexto,60 );
                  AgregaColumna( 'NCredito',tgTexto,30 );
                  AgregaColumna( 'RetCedula',tgTexto,15 );
                  AgregaColumna( 'NEmpleado',tgTexto,10 );
                  AgregaColumna( 'Plazo',tgTexto,3 );
                  AgregaColumna( 'Meses',tgTexto,3 );
                  AgregaColumna( 'RetTress',tgTexto,15 );
                  AgregaColumna( 'Incidencia',tgTexto,1 );
                  AgregaColumna( 'FecInicio',tgTexto,10 );
                  AgregaColumna( 'FecFin',tgTexto,10 );
                  AgregaColumna( 'Reubicado',tgTexto,1 );

                  AlExportarColumnas := ExportarCedulaFonacot;
                  Result := 'Se generaron: '+ AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIDel, cdsDataset ) ) + ' registros en la c�dula de Fonacot' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;


function TdmConcilia.AgregaMensaje( const sMensaje, sValor: String ): String;
begin
     Result := sMensaje + CR_LF + sValor;
end;



{-------------------------- TCreditoFonacot ------------------------------}
constructor TCreditoFonacot.Create;
begin
     FNumFonacot := VACIO;
     FNumEmpleado := VACIO;
     FNomCompleto := VACIO;
     FRFC := VACIO;
     FNumCredito := VACIO;
     FMontoMensual := 0.0;
     FPlazo := 0;
     FMeses := 0;
     FPosicion := 0;
     FMontoPrestado := 0.0;
     FDiferencia := 0.0;
end;

procedure TCreditoFonacot.SetCreditoFromCedula( oDataSet: TZetaClientDataSet );
begin
     with oDataSet do
     begin
          FNumFonacot := FieldByName('N_FONACOT').AsString;
          FRFC := FieldByName('RFC').AsString;
          FNomCompleto := FieldByName('NOM_COMPLETO').AsString;
          FNumCredito := FieldByName('N_CREDITO').AsString;
          FMontoMensual := FieldByName('RET_MENS').AsCurrency;
          FNumEmpleado := FieldByName('N_EMPLEADO').AsString;
          FPlazo := FieldByName('PLAZO').AsInteger;
          FMeses := FieldByName('MESES').AsInteger;
          FPosicion := FieldByName('RENGLON').AsInteger;
          FMontoPrestado := ( FieldByName('PLAZO').AsInteger - FieldByName('MESES').AsInteger ) * FieldByName('RET_MENS').AsCurrency;
          FReubicado:= FieldByName('REUBICADO').AsString;
          FDiferencia:= FieldByName('DIFERENCIA').AsCurrency;
     end;
end;

procedure TdmConcilia.cdsConfrontacionAlCrearCampos(Sender: TObject);
begin
     with cdsConfrontacion do
     begin
           MaskFecha('FecInicio');
           MaskFecha('FecFin');
     end;

end;



function TdmConcilia.GetAjusteMesAnterior(const iEmpleado: Integer; sCredito: String): TPesos;

     function GetMontoAjuste: TPesos;
     begin
          Result:= 0;

          with cdsPCarAbo do
          begin
               if( Locate( 'PR_REFEREN;CB_CODIGO',VarArrayOf([sCredito,iEmpleado]),[])) then
                   Result:= cdsPCarAbo.FieldByName('SALDO_AJUSTE').AsFloat;
          end;
     end;
begin
     Result:= GetSaldoAjuste( iEmpleado, sCredito ) - GetMontoAjuste;
end;

procedure TdmConcilia.MaskBisiestos( const sCampo: String; oDataSet: TDataSet );
var
   oCampo: TField;
begin
     oCampo := oDataSet.FindField( sCampo );
     if ( oCampo <> nil ) and ( oCampo is TDateTimeField ) then
     begin
          with oCampo do
          begin
               OnGetText := FechaBisGetText
          end;
     end;
end;

procedure TdmConcilia.FechaBisGetText(Sender: TField; var Text: String;DisplayText: Boolean);
var
   dValue: TDate;
begin
     if DisplayText then
     begin
          if StrVacio( Sender.AsString ) then
             Text := ''
          else
          begin
               dValue := Sender.AsDateTime;
               if ( dValue <= 0 ) then
                  Text := ''
               else
               begin
                    if ( dValue = K_FECHA_BIS ) then
                    begin                
                         Text:= FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat, K_FECHA_SIN_BIS );
                    end
                    else
                        Text := FormatDateTime( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat, dValue )
               end;
          end;
     end;
end;

procedure TdmConcilia.EnMascaraFechas(const lConsideraBis: Boolean);
begin
     if ( not lConsideraBis ) then
     begin
          MaskBisiestos('FecInicio', cdsConfrontacion);
          MaskBisiestos('FecFin', cdsConfrontacion);
     end
     else
     begin
          with cdsConfrontacion do
          begin
               MaskFecha('FecInicio');
               MaskFecha('FecFin');
          end;
     end;
end;

procedure TdmConcilia.GetcdsAuxiliar(const lBisiestos: Boolean);
const
     K_INICIO = 1;

begin

     // Pruebas.
     // Prepraci�n y ejecuci�n de Confrontaci�n.
     // if PreparaConfrontar then
        Confrontar (2017, 9);
     // ----- -----

     cdsAuxiliar.Data:= cdsConfrontacion.Data;
     if not lBisiestos then
     begin
          with cdsAuxiliar do
          begin
               // TressShell.ConciliarStart( RecordCount + K_INICIO );
               First;
               while not EOF do
               // while ( not Eof and TressShell.ConciliarStep )do
               begin
                    if ( FieldByName('FecInicio').AsDateTime = K_FECHA_BIS ) then
                    begin
                         Edit;
                         FieldByName('FecInicio').AsDateTime:= K_FECHA_SIN_BIS;
                    end;
                    Next;
               end;
          end;
     end
     {else
         TressShell.ConciliarStart( K_INICIO );}
end;

{procedure TdmConcilia.SetPathBitacora(const iPageIndex: Integer; sValue: String);
begin
     with ClientRegistry do
     begin
          case iPageIndex of
               2: RutaImportarKardex:= sValue;
               3: RutaImportarPrestamos:= sValue;
               5: RutaActualizaPrestamos:= sValue;
          end;
     end;
end;}

{procedure TdmConcilia.SetUltimaCedula( const sValue: String );
begin
     ClientRegistry.RutaUltimaCedula:= sValue;
end;}

{function TdmConcilia.GetPathBitacora( const iPageIndex: Integer ): String;
begin
     with Application, ClientRegistry do
     begin
          Result:= VACIO;
          case iPageIndex of
               //tsEmpFonacot.PageIndex:
               //2:
               K_EMPLEADOS_NUEVOS_FONACOT:
               begin
                    if StrVacio( RutaImportarKardex ) then
                    begin
                         Result:= ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + 'ImportarKardex.log';
                    end
                    else
                    begin
                         Result:=  RutaImportarKardex;
                    end;
               end;
               //tsCredNuevos.PageIndex:
               //3:
               K_CREDITOS_NUEVOS:
               begin
                    if StrVacio( RutaImportarPrestamos ) then
                    begin
                         Result:= ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + 'ImportarPrestamos.log';
                    end
                    else
                    begin
                         Result:=  RutaImportarPrestamos;
                    end;
               end;
               //tbsActualizaPresta.PageIndex:
               //5:
               K_ACTUALIZA_PRESTAMOS:
               begin
                    if StrVacio( RutaActualizaPrestamos ) then
                    begin
                         Result:= ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + 'ActualizaPrestamos.log';
                    end
                    else
                    begin
                         Result:=  RutaActualizaPrestamos;
                    end;
               end;
          end;
     end;
end;}

function TdmConcilia.GetUltimaCedula: String;
begin
     // Result:= ClientRegistry.RutaUltimaCedula;
end;


procedure TdmConcilia.cdsReubicadoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsString = K_REUBICADO ) then
       Text:= K_PROPPERCASE_SI
     else if not ( cdsCreditosNuevos.IsEmpty ) then
         Text:= K_PROPPERCASE_NO;
end;

procedure TdmConcilia.LimpiarDataSetsConciliacion;
begin
     with dmConcilia do
     begin
          cdsEmpCreditos.Active := False;
          cdsCreditosActivos.Active := False;
          cdsEmpleaFonacot.Active := False;
          cdsCreditosNuevos.Active := False;
          cdsCreditosNoActivos.Active := False;
          cdsCedulas.Active := False;
          cdsCreditosDiferencias.Active := False;
     end;
end;

function TdmConcilia.GetDescripcionFechaConciliacion: String;//Conciliador FONACOT - Mejoras Obtener Fecha Larga Noviembre/2015
begin
      Result := Format( '%s / %d', [ ObtieneElemento( lfMeses, FMes - 1) ,FAnio ] )
end;

function TdmConcilia.GetDescripcionFechaCortaConciliacion: String;//Conciliador FONACOT - Mejoras Obtener Fecha Corta Nov/15
const
     FORMAT_DATE_TIME = 'mmm/yy';
var
   dFechaConciliacion: TDate;
begin
     dFechaConciliacion:= EncodeDate( FAnio, FMes, 1);
     Result := FormatDateTime(FORMAT_DATE_TIME, dFechaConciliacion);
end;

function TdmConcilia.GetServerSuper: IdmServerSuperDisp;
begin
     Result:= IdmServerSuperDisp( dmCliente.CreaServidor( CLASS_dmServerSuper, FServidor ) );
end;

function TdmConcilia.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidorRH ) );
end;



procedure TdmConcilia.GetEmpFonacotGetLista(Parametros: TZetaParams);
begin
     cdsDataLista.Data := ServerRecursos.GetEmpFonacotLista( dmCliente.Empresa, Parametros.VarValues );
end;


end.
