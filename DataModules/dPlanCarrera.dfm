object dmPlanCarrera: TdmPlanCarrera
  OldCreateOrder = False
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object cdsTCompetencia: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TC_CODIGO'
    Params = <>
    BeforePost = cdsTCompetenciaBeforePost
    AfterDelete = cdsTCompetenciaAfterDelete
    OnNewRecord = cdsTCompetenciaNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTCompetenciaAlAdquirirDatos
    AlEnviarDatos = cdsTCompetenciaAlEnviarDatos
    AlCrearCampos = cdsTCompetenciaAlCrearCampos
    AlModificar = cdsTCompetenciaAlModificar
    LookupName = 'Cat'#225'logo Tipo de Competencia'
    LookupDescriptionField = 'TC_DESCRIP'
    LookupKeyField = 'TC_CODIGO'
    Left = 376
    Top = 8
  end
  object cdsCalificaciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CA_CODIGO'
    Params = <>
    BeforePost = cdsCalificacionesBeforePost
    AfterDelete = cdsCalificacionesAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsCalificacionesAlAdquirirDatos
    AlEnviarDatos = cdsCalificacionesAlEnviarDatos
    AlModificar = cdsCalificacionesAlModificar
    LookupName = 'Cat'#225'logo Tabla de Calificaciones'
    LookupDescriptionField = 'CA_DESCRIP'
    LookupKeyField = 'CA_CODIGO'
    Left = 280
    Top = 8
  end
  object cdsDimensiones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'DM_CODIGO'
    Params = <>
    BeforePost = cdsDimensionesBeforePost
    AfterDelete = cdsDimensionesAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsDimensionesAlAdquirirDatos
    AlEnviarDatos = cdsDimensionesAlEnviarDatos
    AlModificar = cdsDimensionesAlModificar
    LookupName = 'Cat'#225'logo de Dimensiones'
    LookupDescriptionField = 'DM_DESCRIP'
    LookupKeyField = 'DM_CODIGO'
    Left = 48
    Top = 64
  end
  object cdsAcciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'AN_CODIGO'
    Params = <>
    AfterDelete = cdsAccionesAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsAccionesAlAdquirirDatos
    AlEnviarDatos = cdsAccionesAlEnviarDatos
    AlCrearCampos = cdsAccionesAlCrearCampos
    AlAgregar = cdsAccionesAlAgregar
    AlModificar = cdsAccionesAlModificar
    LookupName = 'Cat'#225'logo de Acciones'
    LookupDescriptionField = 'AN_NOMBRE'
    LookupKeyField = 'AN_CODIGO'
    Left = 168
    Top = 64
  end
  object cdsNiveles: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'NP_CODIGO'
    Params = <>
    AfterDelete = cdsNivelesAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsNivelesAlAdquirirDatos
    AlAgregar = cdsNivelesAlAgregar
    AlModificar = cdsNivelesAlModificar
    LookupName = 'Cat'#225'logo de Niveles'
    LookupDescriptionField = 'NP_DESCRIP'
    LookupKeyField = 'NP_CODIGO'
    OnGetRights = ReadOnlyGetRights
    Left = 48
    Top = 8
  end
  object cdsFamilias: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'FP_CODIGO'
    Params = <>
    AfterDelete = cdsFamiliasAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsFamiliasAlAdquirirDatos
    AlAgregar = cdsFamiliasAlAgregar
    AlModificar = cdsFamiliasAlModificar
    LookupName = 'Cat'#225'logo de Familias'
    LookupDescriptionField = 'FP_DESCRIP'
    LookupKeyField = 'FP_CODIGO'
    Left = 280
    Top = 64
  end
  object cdsCompetencia: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CM_CODIGO'
    Params = <>
    BeforeInsert = cdsCompetenciaBeforeInsert
    AfterDelete = cdsCompetenciaAfterDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsCompetenciaAlAdquirirDatos
    AlCrearCampos = cdsCompetenciaAlCrearCampos
    AlAgregar = cdsCompetenciaAlAgregar
    AlModificar = cdsCompetenciaAlModificar
    LookupName = 'Cat'#225'logo de Competencias / Habilidades'
    LookupDescriptionField = 'CM_DESCRIP'
    LookupKeyField = 'CM_CODIGO'
    Left = 376
    Top = 64
  end
  object cdsPuestos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsPuestosAlAdquirirDatos
    AlCrearCampos = cdsPuestosAlCrearCampos
    AlModificar = cdsPuestosAlModificar
    LookupName = 'Cat'#225'logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    OnGetRights = ReadOnlyGetRights
    Left = 168
    Top = 8
  end
  object cdsEditNiveles: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'NP_CODIGO'
    Params = <>
    BeforeInsert = cdsEditNivelesBeforeInsert
    BeforePost = cdsEditNivelesBeforePost
    BeforeDelete = cdsEditNivelesBeforeDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEditNivelesAlAdquirirDatos
    AlEnviarDatos = cdsEditNivelesAlEnviarDatos
    AlAgregar = cdsEditNivelesAlAgregar
    Left = 48
    Top = 232
  end
  object cdsNivelDimension: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'DM_CODIGO'
    Params = <>
    AfterInsert = cdsNivelDimensionAfterInsert
    AfterEdit = cdsNivelDimensionAfterInsert
    AfterDelete = cdsNivelDimensionAfterInsert
    OnNewRecord = cdsNivelDimensionNewRecord
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsNivelDimensionAlCrearCampos
    Left = 168
    Top = 232
  end
  object cdsEditFamilia: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforeInsert = cdsEditFamiliaBeforeInsert
    BeforePost = cdsEditFamiliaBeforePost
    BeforeDelete = cdsEditFamiliaBeforeDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEditFamiliaAlAdquirirDatos
    AlEnviarDatos = cdsEditFamiliaAlEnviarDatos
    AlAgregar = cdsEditFamiliaAlAgregar
    Left = 48
    Top = 288
  end
  object cdsCompetenciaFamilia: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cdsCompetenciaFamiliaAfterInsert
    AfterEdit = cdsCompetenciaFamiliaAfterInsert
    AfterDelete = cdsCompetenciaFamiliaAfterInsert
    OnNewRecord = cdsCompetenciaFamiliaNewRecord
    OnReconcileError = cdsReconcileError
    Left = 168
    Top = 288
  end
  object cdsEditPuesto: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    BeforePost = cdsEditPuestoBeforePost
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEditPuestoAlAdquirirDatos
    AlEnviarDatos = cdsEditPuestoAlEnviarDatos
    AlCrearCampos = cdsEditPuestoAlCrearCampos
    AlAgregar = cdsEditPuestoAlAgregar
    Left = 48
    Top = 176
  end
  object cdsPuestoDimension: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cdsCompetenciaPuestoAfterInsert
    AfterEdit = cdsCompetenciaPuestoAfterInsert
    AfterDelete = cdsCompetenciaPuestoAfterInsert
    OnNewRecord = cdsPuestoDimensionNewRecord
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsPuestoDimensionAlCrearCampos
    Left = 288
    Top = 176
  end
  object cdsCompetenciaPuesto: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cdsCompetenciaPuestoAfterInsert
    AfterEdit = cdsCompetenciaPuestoAfterInsert
    AfterDelete = cdsCompetenciaPuestoAfterInsert
    OnNewRecord = cdsCompetenciaPuestoNewRecord
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsCompetenciaPuestoAlCrearCampos
    Left = 168
    Top = 176
  end
  object cdsEditCompetencia: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CM_CODIGO'
    Params = <>
    BeforeInsert = cdsEditCompetenciaBeforeInsert
    BeforePost = cdsEditCompetenciaBeforePost
    BeforeDelete = cdsEditCompetenciaBeforeDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEditCompetenciaAlAdquirirDatos
    AlEnviarDatos = cdsEditCompetenciaAlEnviarDatos
    AlAgregar = cdsEditCompetenciaAlAgregar
    Left = 48
    Top = 120
  end
  object cdsCompetenciaCalifica: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cdsCompetenciaCalificaAfterInsert
    AfterEdit = cdsCompetenciaCalificaAfterInsert
    AfterDelete = cdsCompetenciaCalificaAfterInsert
    OnNewRecord = cdsCompetenciaCalificaNewRecord
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsCompetenciaCalificaAlCrearCampos
    Left = 288
    Top = 120
  end
  object cdsCompetenciaMapa: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CM_ORDEN'
    Params = <>
    AfterOpen = cdsCompetenciaMapaAfterOpen
    AfterInsert = cdsCompetenciaCalificaAfterInsert
    AfterEdit = cdsCompetenciaCalificaAfterInsert
    AfterDelete = cdsCompetenciaCalificaAfterInsert
    OnNewRecord = cdsCompetenciaMapaNewRecord
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsCompetenciaMapaAlCrearCampos
    Left = 168
    Top = 120
  end
  object cdsEditAcciones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforeInsert = cdsEditAccionesBeforeInsert
    BeforePost = cdsEditAccionesBeforePost
    BeforeDelete = cdsEditAccionesBeforeDelete
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEditAccionesAlAdquirirDatos
    AlEnviarDatos = cdsEditAccionesAlEnviarDatos
    AlAgregar = cdsEditAccionesAlAgregar
    Left = 48
    Top = 344
  end
  object cdsCompetenciaAccion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cdsCompetenciaAccionAfterInsert
    AfterEdit = cdsCompetenciaAccionAfterInsert
    AfterDelete = cdsCompetenciaAccionAfterInsert
    OnNewRecord = cdsCompetenciaAccionNewRecord
    OnReconcileError = cdsReconcileError
    Left = 168
    Top = 344
  end
end
