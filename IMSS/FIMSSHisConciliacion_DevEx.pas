unit FIMSSHisConciliacion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, Grids, DBGrids, StdCtrls, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox, ZBaseGridLecturaImss_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  ComCtrls, ZetaStateComboBox, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid;

type
  TIMSSHisConciliacion_DevEx = class(TBaseGridLecturaImss_DevEx)
    dsLiq_Emp: TDataSource;
    Panel1: TPanel;
    Label8: TLabel;
    LE_DIAS_BM: TZetaDBTextBox;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    LE_INF_PAT: TZetaDBTextBox;
    Label5: TLabel;
    LE_INF_AMO: TZetaDBTextBox;
    Label7: TLabel;
    LE_TOT_INF: TZetaDBTextBox;
    gbConciliacionBimestral: TGroupBox;
    GroupBox4: TGroupBox;
    LE_BIM_CALC: TZetaDBTextBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    LE_ACUMULA: TZetaDBTextBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    LE_BIM_ANT: TZetaDBTextBox;
    LE_BIM_ACTUAL: TZetaDBTextBox;
    LE_BIM_SIG: TZetaDBTextBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    GroupBox5: TGroupBox;
    Label18: TLabel;
    LE_DIF_PROV: TZetaDBTextBox;
    Label19: TLabel;
    LE_VA_PAGO: TZetaDBTextBox;
    Label20: TLabel;
    LE_VA_GOZO: TZetaDBTextBox;
    LE_PROV: TZetaDBTextBox;
    Label21: TLabel;
    gbConciliacionSoloAcumula: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    difLE_ACUMULA: TZetaDBTextBox;
    GroupBox7: TGroupBox;
    Label28: TLabel;
    difLE_VA_PAGO: TZetaDBTextBox;
    Label29: TLabel;
    difLE_VA_GOZO: TZetaDBTextBox;
    difLE_PROV: TZetaDBTextBox;
    Label30: TLabel;
    Label4: TLabel;
    difLE_INF_AMO: TZetaDBTextBox;
    Label22: TLabel;
    GroupBox6: TGroupBox;
    LE_DIF_ACUM: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetControlesConciliacion;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  end;

var
  IMSSHisConciliacion_DevEx: TIMSSHisConciliacion_DevEx;

implementation

uses DIMSS,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZGlobalTress,
     DGlobal;
const
     K_MINIMO = 1.00;

{$R *.DFM}

{ TIMSSDatosBimestral }

procedure TIMSSHisConciliacion_DevEx.FormCreate(Sender: TObject);
const
     K_HEIGHT_DESC_BIMESTRAL = 169;
     K_HEIGHT_DESC_SOLOACUMU = 130;
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stIMSS;
     HelpContext := H_IMSS_CONCILIA_INFONAVIT;


     //eTipoDescConciliaInfonavit = (tciBimestral,  tciSoloAcmulado);
     if Global.GetGlobalInteger( K_GLOBAL_INFO_DESCUENTO_INFONAVIT ) = ord(tciBimestral) then
     begin
          gbConciliacionBimestral.Visible := TRUE;
          gbConciliacionSoloAcumula.Visible := FALSE;
          Panel1.Height := K_HEIGHT_DESC_BIMESTRAL;
     end
     else
     begin
          gbConciliacionSoloAcumula.Visible := TRUE;
          gbConciliacionBimestral.Visible := FALSE;
          Panel1.Height := K_HEIGHT_DESC_SOLOACUMU;
     end;

end;

procedure TIMSSHisConciliacion_DevEx.Connect;
begin
     with dmIMSS do
     begin
          cdsHisConciliaInfo.Conectar;
          cdsHisTotalBim.Conectar;
          DataSource.DataSet:= cdsHisConciliaInfo;
          dsLiq_Emp.DataSet:= cdsHisTotalBim;
     end;

end;

procedure TIMSSHisConciliacion_DevEx.Refresh;
begin
     with dmIMSS do
     begin
          cdsHisConciliaInfo.Refrescar;
          cdsHisTotalBim.Refrescar;
     end;
end;

function TIMSSHisConciliacion_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar A Conciliación INFONAVIT';
end;

function TIMSSHisConciliacion_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar A Conciliación INFONAVIT';
end;

function TIMSSHisConciliacion_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar en Conciliación INFONAVIT';
end;

procedure TIMSSHisConciliacion_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     SetControlesConciliacion;
end;

procedure TIMSSHisConciliacion_DevEx.SetControlesConciliacion;
const
     aColor: array[FALSE..TRUE] of TColor = ( clBlack, clRed );
begin
     LE_DIF_PROV.Font.Color:= aColor[dmImss.cdsHisConciliaInfo.FieldByName( 'LE_DIF_PROV' ).AsFloat > K_MINIMO  ];


     with  dmImss.cdsHisConciliaInfo do
      LE_DIF_ACUM.Font.Color:= aColor[ FieldByName( 'LE_INF_AMO' ).AsFloat > FieldByName( 'LE_ACUMULA' ).AsFloat ];
end;

procedure TIMSSHisConciliacion_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;

end;

end.
