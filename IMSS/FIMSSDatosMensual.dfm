inherited IMSSDatosMensual: TIMSSDatosMensual
  Left = 185
  Top = 184
  Caption = 'IMSS : Datos Mensuales por Empleado'
  ClientHeight = 353
  ClientWidth = 553
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 553
    inherited Slider: TSplitter
      Left = 334
    end
    inherited ValorActivo1: TPanel
      Width = 318
    end
    inherited ValorActivo2: TPanel
      Left = 337
      Width = 216
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 553
    Height = 146
    Align = alTop
    TabOrder = 1
    object LE_INV_VID: TZetaDBTextBox
      Left = 196
      Top = 104
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_INV_VID'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_INV_VID'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_EYM_FIJ: TZetaDBTextBox
      Left = 196
      Top = 5
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_EYM_FIJ'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_EYM_FIJ'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_RIESGOS: TZetaDBTextBox
      Left = 196
      Top = 84
      Width = 99
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_RIESGOS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_RIESGOS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_EYM_EXC: TZetaDBTextBox
      Left = 196
      Top = 25
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_EYM_EXC'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_EYM_EXC'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_EYM_DIN: TZetaDBTextBox
      Left = 196
      Top = 45
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_EYM_DIN'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_EYM_DIN'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_EYM_ESP: TZetaDBTextBox
      Left = 196
      Top = 65
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_EYM_ESP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_EYM_ESP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_GUARDER: TZetaDBTextBox
      Left = 196
      Top = 124
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_GUARDER'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_GUARDER'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_TOT_IMS: TZetaDBTextBox
      Left = 422
      Top = 84
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_TOT_IMS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_TOT_IMS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_DIAS_CO: TZetaDBTextBox
      Left = 422
      Top = 124
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_DIAS_CO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_DIAS_CO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label1: TLabel
      Left = 115
      Top = 105
      Width = 77
      Height = 13
      Alignment = taRightJustify
      Caption = 'Invalidez y Vida:'
    end
    object Label2: TLabel
      Left = 113
      Top = 7
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'EYM: Cuota Fija:'
    end
    object Label3: TLabel
      Left = 97
      Top = 85
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = 'Riesgos de Trabajo:'
    end
    object Label4: TLabel
      Left = 109
      Top = 26
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'EYM: Excedente:'
    end
    object Label5: TLabel
      Left = 129
      Top = 46
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'EYM: Dinero:'
    end
    object Label6: TLabel
      Left = 122
      Top = 66
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = 'EYM: Especie:'
    end
    object Label7: TLabel
      Left = 138
      Top = 126
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = 'Guarderias:'
    end
    object Label8: TLabel
      Left = 360
      Top = 86
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total IMSS:'
    end
    object Label9: TLabel
      Left = 341
      Top = 126
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'D�as Cotizados:'
    end
    object Label10: TLabel
      Left = 352
      Top = 7
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'IMSS Obrero:'
    end
    object LE_IMSS_OB: TZetaDBTextBox
      Left = 422
      Top = 5
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_IMSS_OB'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_IMSS_OB'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label11: TLabel
      Left = 345
      Top = 26
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = 'IMSS Patronal:'
    end
    object LE_IMSS_PA: TZetaDBTextBox
      Left = 422
      Top = 24
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_IMSS_PA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_IMSS_PA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LE_APO_VOL: TZetaDBTextBox
      Left = 422
      Top = 45
      Width = 101
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_APO_VOL'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_APO_VOL'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label12: TLabel
      Left = 312
      Top = 46
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = 'Aportaci'#243'n Voluntaria:'
    end
  end
  object ZetaDBGrid2: TZetaDBGrid [2]
    Left = 0
    Top = 165
    Width = 553
    Height = 188
    Align = alClient
    DataSource = dsLiq_Mov
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'LM_FECHA'
        Title.Caption = 'Fecha'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_CLAVE'
        Title.Caption = 'Clave'
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_DIAS'
        Title.Alignment = taRightJustify
        Title.Caption = 'D�as'
        Width = 28
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_INCAPAC'
        Title.Alignment = taRightJustify
        Title.Caption = 'INC'
        Width = 24
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_AUSENCI'
        Title.Alignment = taRightJustify
        Title.Caption = 'AUS'
        Width = 27
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_BASE'
        Title.Alignment = taRightJustify
        Title.Caption = 'Base'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_EYM_FIJ'
        Title.Alignment = taRightJustify
        Title.Caption = 'EYM Fijo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_EYM_EXC'
        Title.Alignment = taRightJustify
        Title.Caption = 'EYM Excede'
        Width = 87
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_EYM_DIN'
        Title.Alignment = taRightJustify
        Title.Caption = 'EYM Dinero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_EYM_ESP'
        Title.Alignment = taRightJustify
        Title.Caption = 'EYM Especie'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_RIESGOS'
        Title.Alignment = taRightJustify
        Title.Caption = 'Riesgos'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_INV_VID'
        Title.Alignment = taRightJustify
        Title.Caption = 'Inv. y Vida'
        Width = 87
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_GUARDER'
        Title.Alignment = taRightJustify
        Title.Caption = 'Guarder�as'
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 488
    Top = 152
  end
  object dsLiq_Mov: TDataSource
    Left = 424
    Top = 192
  end
end
