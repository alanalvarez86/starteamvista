unit FIMSSHisBimestral_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, Grids, DBGrids, StdCtrls, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  ZBaseGridLecturaImss_DevEx, ComCtrls, ZetaStateComboBox;

type
  TIMSSHisBimestral_DevEx = class(TBaseGridLecturaImss_DevEx)
    dsLiq_Emp: TDataSource;
    Panel1: TPanel;
    LS_TOT_RET: TZetaDBTextBox;
    LS_TOT_INF: TZetaDBTextBox;
    Label3: TLabel;
    Label1: TLabel;
    LS_DIAS_BM: TZetaDBTextBox;
    Label4: TLabel;
    Label2: TLabel;
    LS_NUM_BIM: TZetaDBTextBox;
    LS_INF_NUM: TZetaDBTextBox;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;

  end;

var
  IMSSHisBimestral_DevEx: TIMSSHisBimestral_DevEx;

implementation

uses DIMSS,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

{ TIMSSHisBimestral }

procedure TIMSSHisBimestral_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo2 := stIMSS;
     HelpContext := H40422_Hist_IMSS_bimestral_empleado;
end;

procedure TIMSSHisBimestral_DevEx.Connect;
begin
     with dmIMSS do
     begin
          cdsIMSSHisBim.Conectar;
          cdsMovHisBim.Conectar;
          DataSource.DataSet:= cdsIMSSHisBim;
          dsLiq_Emp.DataSet:= cdsMovHisBim;
     end;
end;

procedure TIMSSHisBimestral_DevEx.Refresh;
begin
     with dmIMSS do
     begin
          cdsIMSSHisBim.Refrescar;
          cdsMovHisBim.Refrescar;
     end;
end;

function TIMSSHisBimestral_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar Al Historial De Pagos IMSS';
end;

function TIMSSHisBimestral_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En El Historial De Pagos IMSS';
end;

function TIMSSHisBimestral_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar En El Historial De Pagos IMSS';
end;

procedure TIMSSHisBimestral_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;

end;

end.
