unit FEditIMSSDatosTot_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, StdCtrls, Mask, ZetaNumero, DB, ExtCtrls, DBCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditIMSSDatosTot_DevEx = class(TBaseEdicion_DevEx)
    GroupBox6: TGroupBox;
    lblParcialidad: TLabel;
    LS_DES_20: TDBCheckBox;
    LS_FINAN: TDBCheckBox;
    LS_PARCIAL: TZetaDBNumero;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FDecreto20, FParcialidades: Boolean;
  protected
     procedure Connect; override;
     function PuedeAgregar(var sMensaje: String): Boolean;override;
     function PuedeBorrar(var sMensaje: String): Boolean;override;
     function PuedeModificar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  EditIMSSDatosTot_DevEx: TEditIMSSDatosTot_DevEx;

implementation

uses DImss,
     ZAccesosTress;

{$R *.dfm}

{ TBaseEdicion2 }

procedure TEditIMSSDatosTot_DevEx.Connect;
begin
     inherited;
     DataSource.DataSet:= dmImss.cdsIMSSDatosTot;
end;

procedure TEditIMSSDatosTot_DevEx.FormShow(Sender: TObject);


   procedure AlineaControles;
   const
        aTopParcialidad: array[FALSE..TRUE] of Integer = (68, 44);
   begin
        lblParcialidad.Top:= aTopParcialidad[( not FDecreto20 ) and FParcialidades];
        LS_PARCIAL.Top:= (lblParcialidad.Top - 4);
   end;

begin
     inherited;
     dmImss.AplicaDecreto20( FDecreto20, FParcialidades );
     LS_DES_20.Visible:= FDecreto20;
     LS_FINAN.Visible:= FDecreto20;
     lblParcialidad.Visible:= FParcialidades;
     LS_PARCIAL.Visible:= FParcialidades;

     AlineaControles;

     dxBarButton_AgregarBtn.Visible := ivNever;
     dxBarButton_BorrarBtn.Visible := ivNever;
     DevEx_cxDBNavigatorEdicion.Visible := False;
     Datasource.AutoEdit := TRUE;
end;

function TEditIMSSDatosTot_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar A Los Totales De IMSS';
end;

function TEditIMSSDatosTot_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Se Pueden Borrar Registros En Esta Pantalla';
     Result := False;
end;

function TEditIMSSDatosTot_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     //No valida derechos de acceso
     Result := True;
end;

procedure TEditIMSSDatosTot_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_IMSS_DATOS_TOTALES;
end;

end.
