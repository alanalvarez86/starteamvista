inherited IMSSHisMensual_DevEx: TIMSSHisMensual_DevEx
  Left = 156
  Top = 167
  Caption = 'IMSS: Historia Mensual por Empleado'
  ClientHeight = 264
  ClientWidth = 580
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Top = 30
    Width = 580
    inherited ValorActivo2: TPanel
      Width = 321
      inherited textoValorActivo2: TLabel
        Width = 315
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 49
    Width = 580
    Height = 65
    Align = alTop
    TabOrder = 3
    object Label6: TLabel
      Left = 266
      Top = 16
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total IMSS:'
    end
    object LS_TOT_IMS: TZetaDBTextBox
      Left = 326
      Top = 14
      Width = 90
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_TOT_IMS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_TOT_IMS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LS_NUM_TRA: TZetaDBTextBox
      Left = 150
      Top = 14
      Width = 90
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_NUM_TRA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_NUM_TRA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label7: TLabel
      Left = 87
      Top = 16
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Asegurados:'
    end
    object Label8: TLabel
      Left = 71
      Top = 34
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as Cotizados:'
    end
    object LS_DIAS_CO: TZetaDBTextBox
      Left = 150
      Top = 32
      Width = 90
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_DIAS_CO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_DIAS_CO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label1: TLabel
      Left = 252
      Top = 34
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total Mensual:'
    end
    object LS_TOT_MES: TZetaDBTextBox
      Left = 326
      Top = 32
      Width = 90
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_TOT_MES'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_TOT_MES'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 114
    Width = 580
    Height = 150
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = dsLiq_Emp
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 65
        Width = 65
      end
      object PrettyName: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PrettyName'
        MinWidth = 100
        Width = 333
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = 'NSS'
        DataBinding.FieldName = 'CB_SEGSOC'
        MinWidth = 80
        Width = 92
      end
      object LE_TOT_IMS: TcxGridDBColumn
        Caption = 'Total IMSS'
        DataBinding.FieldName = 'LE_TOT_IMS'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 80
      end
      object LE_DIAS_CO: TcxGridDBColumn
        Caption = 'D'#237'as Cotizados'
        DataBinding.FieldName = 'LE_DIAS_CO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 100
        Width = 100
      end
    end
  end
  inherited IMSSPanel: TPanel
    Top = 0
    Width = 580
  end
  inherited DataSource: TDataSource
    Left = 488
    Top = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsLiq_Emp: TDataSource
    Left = 536
    Top = 40
  end
end