inherited IMSSDatosTot_DevEx: TIMSSDatosTot_DevEx
  Left = 560
  Top = 171
  Caption = 'Totales de IMSS'
  ClientHeight = 422
  ClientWidth = 625
  ExplicitWidth = 320
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 625
    Height = 18
    ExplicitWidth = 625
    ExplicitHeight = 18
    inherited ImgNoRecord: TImage
      Height = 18
      ExplicitHeight = 18
    end
    inherited Slider: TSplitter
      Left = 323
      Height = 18
      ExplicitLeft = 323
      ExplicitHeight = 18
    end
    inherited ValorActivo1: TPanel
      Width = 307
      Height = 18
      ExplicitWidth = 307
      ExplicitHeight = 18
      inherited textoValorActivo1: TLabel
        Width = 83
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 299
      Height = 18
      ExplicitLeft = 326
      ExplicitWidth = 299
      ExplicitHeight = 18
      inherited textoValorActivo2: TLabel
        Left = 213
        Width = 83
        ExplicitLeft = 213
      end
    end
  end
  object PageControl1: TcxPageControl [1]
    Left = 0
    Top = 48
    Width = 625
    Height = 374
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = TabSheet1
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    OnMouseDown = GenericoMouseDown
    ClientRectBottom = 347
    ClientRectLeft = 2
    ClientRectRight = 623
    ClientRectTop = 2
    object TabSheet1: TcxTabSheet
      Caption = 'Mensuales'
      OnMouseDown = GenericoMouseDown
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GBResumenMensual: TcxGroupBox
        Left = 229
        Top = 0
        Caption = ' Resumen Mensual '
        TabOrder = 0
        OnDblClick = GBDblClick
        Height = 237
        Width = 232
        object Label20: TLabel
          Left = 62
          Top = 19
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total Mensual:'
        end
        object LS_TOT_MES: TZetaDBTextBox
          Left = 138
          Top = 18
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_TOT_MES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_TOT_MES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label19: TLabel
          Left = 73
          Top = 37
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Asegurados:'
        end
        object LS_NUM_TRA: TZetaDBTextBox
          Left = 138
          Top = 36
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_NUM_TRA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_NUM_TRA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_DIAS_CO: TZetaDBTextBox
          Left = 138
          Top = 54
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_DIAS_CO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_DIAS_CO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label5: TLabel
          Left = 57
          Top = 55
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'as Cotizados:'
        end
        object LS_FAC_ACT: TZetaDBTextBox
          Left = 138
          Top = 72
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_FAC_ACT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_FAC_ACT'
          DataSource = DataSource
          FormatFloat = '%14.5n'
          FormatCurrency = '%m'
        end
        object Label24: TLabel
          Left = 7
          Top = 74
          Width = 125
          Height = 13
          Alignment = taRightJustify
          Caption = 'Factor de Actualizaciones:'
        end
        object Label18: TLabel
          Left = 41
          Top = 92
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa de Recargos:'
        end
        object LS_FAC_REC: TZetaDBTextBox
          Left = 138
          Top = 90
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_FAC_REC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_FAC_REC'
          DataSource = DataSource
          FormatFloat = '%14.3n'
          FormatCurrency = '%m'
        end
        object Label34: TLabel
          Left = 68
          Top = 131
          Width = 64
          Height = 13
          Caption = 'IMSS Obrero:'
        end
        object Label35: TLabel
          Left = 61
          Top = 149
          Width = 71
          Height = 13
          Caption = 'IMSS Patronal:'
        end
        object LS_IMSS_OB: TZetaDBTextBox
          Left = 138
          Top = 129
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_IMSS_OB'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_IMSS_OB'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_IMSS_PA: TZetaDBTextBox
          Left = 138
          Top = 147
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_IMSS_PA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_IMSS_PA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GBCuentaImss: TcxGroupBox
        Left = 8
        Top = 0
        Caption = ' Cuenta de IMSS '
        TabOrder = 1
        OnDblClick = GBDblClick
        Height = 237
        Width = 217
        object Label1: TLabel
          Left = 41
          Top = 21
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'EYM: Cuota Fija:'
        end
        object LS_EYM_FIJ: TZetaDBTextBox
          Left = 126
          Top = 19
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_EYM_FIJ'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_EYM_FIJ'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label2: TLabel
          Left = 37
          Top = 39
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'EYM: Excedente:'
        end
        object LS_EYM_EXC: TZetaDBTextBox
          Left = 126
          Top = 37
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_EYM_EXC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_EYM_EXC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label3: TLabel
          Left = 57
          Top = 57
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'EYM: Dinero:'
        end
        object LS_EYM_DIN: TZetaDBTextBox
          Left = 126
          Top = 55
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_EYM_DIN'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_EYM_DIN'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_EYM_ESP: TZetaDBTextBox
          Left = 126
          Top = 74
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_EYM_ESP'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_EYM_ESP'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 50
          Top = 76
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'EYM: Especie:'
        end
        object Label7: TLabel
          Left = 64
          Top = 94
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'Guarder'#237'as:'
        end
        object LS_GUARDER: TZetaDBTextBox
          Left = 126
          Top = 92
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_GUARDER'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_GUARDER'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label10: TLabel
          Left = 25
          Top = 113
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Riesgos de Trabajo:'
        end
        object LS_RIESGOS: TZetaDBTextBox
          Left = 126
          Top = 111
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_RIESGOS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_RIESGOS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label11: TLabel
          Left = 43
          Top = 131
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Invalidez y Vida:'
        end
        object LS_INV_VID: TZetaDBTextBox
          Left = 126
          Top = 129
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INV_VID'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INV_VID'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label12: TLabel
          Left = 45
          Top = 149
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'SubTotal IMSS:'
        end
        object LS_SUB_IMS: TZetaDBTextBox
          Left = 126
          Top = 147
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_SUB_IMS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_SUB_IMS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label13: TLabel
          Left = 43
          Top = 167
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Actualizaciones:'
        end
        object LS_ACT_IMS: TZetaDBTextBox
          Left = 126
          Top = 165
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_ACT_IMS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_ACT_IMS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label15: TLabel
          Left = 71
          Top = 186
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Recargos:'
        end
        object LS_REC_IMS: TZetaDBTextBox
          Left = 126
          Top = 184
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_REC_IMS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_REC_IMS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label17: TLabel
          Left = 64
          Top = 204
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total IMSS:'
        end
        object LS_TOT_IMS: TZetaDBTextBox
          Left = 126
          Top = 202
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_TOT_IMS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_TOT_IMS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GBDecreto20: TcxGroupBox
        Left = 8
        Top = 237
        Caption = 'Decreto 20%'
        TabOrder = 2
        OnDblClick = GBDblClick
        Height = 98
        Width = 217
        object lblParcialidad: TLabel
          Left = 28
          Top = 66
          Width = 93
          Height = 13
          Caption = 'Importe Parcialidad:'
        end
        object LS_PARCIAL: TZetaDBTextBox
          Left = 126
          Top = 63
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_PARCIAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_PARCIAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_DES_20: TDBCheckBox
          Left = 23
          Top = 24
          Width = 115
          Height = 17
          TabStop = False
          Alignment = taLeftJustify
          Caption = 'Aplic'#243' Decreto 20%:'
          DataField = 'LS_DES_20'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object LS_FINAN: TDBCheckBox
          Left = 23
          Top = 43
          Width = 115
          Height = 17
          TabStop = False
          Alignment = taLeftJustify
          Caption = 'Con Financiamiento:'
          DataField = 'LS_FINAN'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
    object TabSheet2: TcxTabSheet
      Caption = 'Bimestrales'
      OnMouseDown = GenericoMouseDown
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GBCuentaInd: TcxGroupBox
        Left = 8
        Top = 0
        Caption = ' Cuenta Individual '
        TabOrder = 0
        OnDblClick = GBDblClick
        Height = 160
        Width = 207
        object Label22: TLabel
          Left = 86
          Top = 19
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Retiro:'
        end
        object LS_RETIRO: TZetaDBTextBox
          Left = 121
          Top = 17
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_RETIRO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_RETIRO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label14: TLabel
          Left = 34
          Top = 36
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cesant'#237'a y Vejez:'
        end
        object LS_CES_VEJ: TZetaDBTextBox
          Left = 121
          Top = 35
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_CES_VEJ'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_CES_VEJ'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_SUB_RET: TZetaDBTextBox
          Left = 121
          Top = 53
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_SUB_RET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_SUB_RET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label21: TLabel
          Left = 46
          Top = 54
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = 'SubTotal RCV:'
        end
        object Label26: TLabel
          Left = 40
          Top = 72
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Actualizaciones:'
        end
        object LS_ACT_RET: TZetaDBTextBox
          Left = 121
          Top = 71
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_ACT_RET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_ACT_RET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label23: TLabel
          Left = 68
          Top = 90
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Recargos:'
        end
        object LS_REC_RET: TZetaDBTextBox
          Left = 121
          Top = 89
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_REC_RET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_REC_RET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label25: TLabel
          Left = 13
          Top = 108
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Aportaci'#243'n Voluntaria:'
        end
        object LS_APO_VOL: TZetaDBTextBox
          Left = 121
          Top = 107
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_APO_VOL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_APO_VOL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label33: TLabel
          Left = 65
          Top = 127
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total RCV:'
        end
        object LS_TOT_RET: TZetaDBTextBox
          Left = 121
          Top = 125
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_TOT_RET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_TOT_RET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GBCuentaInfonavit: TcxGroupBox
        Left = 221
        Top = 0
        Caption = ' Cuenta INFONAVIT '
        TabOrder = 1
        OnDblClick = GBDblClick
        Height = 160
        Width = 212
        object Label6: TLabel
          Left = 39
          Top = 19
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'No Acreditados:'
        end
        object LS_INF_NAC: TZetaDBTextBox
          Left = 119
          Top = 17
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INF_NAC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INF_NAC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label8: TLabel
          Left = 56
          Top = 37
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Acreditados:'
        end
        object LS_INF_ACR: TZetaDBTextBox
          Left = 119
          Top = 35
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INF_ACR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INF_ACR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 41
          Top = 55
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Amortizaciones:'
        end
        object LS_INF_AMO: TZetaDBTextBox
          Left = 119
          Top = 53
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INF_AMO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INF_AMO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label29: TLabel
          Left = 9
          Top = 73
          Width = 106
          Height = 13
          Alignment = taRightJustify
          Caption = 'SubTotal INFONAVIT:'
        end
        object LS_SUB_INF: TZetaDBTextBox
          Left = 119
          Top = 71
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_SUB_INF'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_SUB_INF'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label30: TLabel
          Left = 38
          Top = 91
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Actualizaciones:'
        end
        object LS_ACT_INF: TZetaDBTextBox
          Left = 119
          Top = 89
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_ACT_INF'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_ACT_INF'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label31: TLabel
          Left = 66
          Top = 109
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Recargos:'
        end
        object LS_REC_INF: TZetaDBTextBox
          Left = 119
          Top = 107
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_REC_INF'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_REC_INF'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label32: TLabel
          Left = 28
          Top = 127
          Width = 87
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total INFONAVIT:'
        end
        object LS_TOT_INF: TZetaDBTextBox
          Left = 119
          Top = 125
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_TOT_INF'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_TOT_INF'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GBResumenBimestral: TcxGroupBox
        Left = 8
        Top = 160
        Caption = ' Resumen Bimestral '
        TabOrder = 2
        OnDblClick = GBDblClick
        Height = 88
        Width = 207
        object LS_INF_NUM: TZetaDBTextBox
          Left = 121
          Top = 15
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INF_NUM'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INF_NUM'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label16: TLabel
          Left = 58
          Top = 17
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Acreditados:'
        end
        object LS_NUM_BIM: TZetaDBTextBox
          Left = 121
          Top = 34
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_NUM_BIM'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_NUM_BIM'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label27: TLabel
          Left = 58
          Top = 36
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Asegurados:'
        end
        object LS_DIAS_BM: TZetaDBTextBox
          Left = 121
          Top = 53
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_DIAS_BM'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_DIAS_BM'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label28: TLabel
          Left = 42
          Top = 55
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'as Cotizados:'
        end
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Left = 465
    Top = 48
    Width = 160
    Height = 343
    Align = alNone
    Enabled = False
    Visible = False
    ExplicitLeft = 465
    ExplicitTop = 48
    ExplicitWidth = 160
    ExplicitHeight = 343
  end
  inherited IMSSPanel: TPanel
    Top = 18
    Width = 625
    ExplicitTop = 18
    ExplicitWidth = 625
  end
  inherited DataSource: TDataSource
    Left = 240
    Top = 40
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001A140F7F5A4634BF3E3024A93E3024A95744
          32BD221B148B0000000000000000000000000000000000000000000000000000
          0000000000000000000000000012BF966FF5D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC09871F60000001300000000000000000000000000000000000000000000
          0000000000000000000000000013C09771F6D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC0966FF50000001200000000000000000000000000000000000000000000
          0000000000040000000000000015C59968F3D8AA7DFFD8AA7DFFD8AA7DFFD8AA
          7DFFC49869F300000012000000000000000D0000000000000000000000000505
          0659847992FA7474A4FF110E0E77785B3CCCD7AA7DFFD8AA7DFFD8AA7DFFD9AA
          7DFF7F6243D20303034B706F9AFA8D829BFF0605075C0000000000000000201E
          249E8B819CFF7776A3FF1411107D725637C7DCAD7BFFDDAD7BFFDDAD7BFFDEAE
          7BFF7B5F3FCF110F107B0C0C10781614188B2E2B34B200000000000000001E1B
          219A8B819CFF7D79A1FF0E0C0C720100002F18120D7B110D096E110D096E1712
          0D7A020101380705055A413F55CF5A5466DE232128A300000000000000001D1B
          21998B819CFF8C819CFF655F78E9100F127D0F0E107A1210138012101380100F
          117C0D0C0E75585261DB8A819CFF8B819CFF1D1B219900000000000000001D1B
          21998B819CFF8C819CFF8A819CFF847B9FFF847B9FFF847B9FFF847B9FFF847B
          9FFF837B9FFF89809DFF8C829CFF8B819CFF1D1B21990000000000000000201E
          249E8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B81
          9CFF8B819CFF8B819CFF8B819CFF8B819CFF201E249E00000000000000001817
          1C918B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B819CFF8B81
          9CFF8B819CFF8B819CFF8B819CFF8B819CFF19181D9300000000000000000000
          002819181D9325232AA61110138001010136010101380101013B0101013B0000
          00300303034C1B191E95201E249E1B191E950000002B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00041A4831B34ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF256445C70000001000000000000000000205
          034C4ED293FF040C0864040A0760040A0760040A0760040A0760040A0760040A
          0760040A0760040A0760040C086443B47EF3050F0A6C0000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348D62DF0A1A12800000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000000611
          0C7043B47EF34ED293FF4ED293FF43B47EF3000201380000000000000000040A
          07604ED293FF00000000000000000000000000000000000000000000000047BD
          85F74ED293FF4ED293FF4BC88DFB02070454000000000000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000004ED2
          93FF4ED293FF4BC88DFB0207045400000000000000000000000000000000040A
          07604ED293FF0000000000000000000000000000000000000000000000004ED2
          93FF4BC88DFB0207045400000000000000000000000000000000000000000103
          02444ED293FF050F0A6C040A0760040A0760040A0760040A0760040C08644ED2
          93FF0309065C0000000000000000000000000000000000000000000000000000
          00000E271B9347BD85F74ED293FF4ED293FF4ED293FF4ED293FF4ED293FF0611
          0C70000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00004656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF2F3A87DF090B1980161C40AF4656CCFF4656CCFF1D24
          55BF090B1980252E6DCF4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF1D2455BF0000000003040A604656CCFF4656CCFF090B
          1980000000001015319F4656CCFF4656CCFF0000000000000000000000000000
          00004656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF0000000000000000000000000000
          000003040A6003040A6003040A6003040A6003040A6003040A6003040A600304
          0A6003040A6003040A6003040A6003040A600000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF00000000000000004656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF00000000000000000000
          00000000000000000000000000004656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000000004656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF0000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000080A5C00384BAF0002023C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000090C6000B2E9FF00B2E9FF005975CB00080A5C00000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000011177800B2E9FF00B2E9FF0083ACE7000E137000000008000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000161D800092BEEF00161D8000000010000000000000000C000102380000
          0000000000000000000000000000000000000000000000000000000000000000
          00000001023800000018000000000000000000242F97007DA4E300536FC70000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000080A5C004962BF00A8DCFB00B2E9FF00B2E9FF0008
          0A5C000000000000000000000000000000000000000000000000000000000000
          00000000000000161D80008AB6EB00B2E9FF00B2E9FF00B2E9FF00B2E9FF0083
          ACE70000000C0000000000000000000000000000000000000000000000000000
          0000000000000000002000A0D4F700B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00181F830000000000000000000000000000000000000000000000000000
          0000000000000000000000181F8300B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00A0D4F70000002400000000000000000000000000000000000000000000
          000000000000000000000000000C0083ACE700B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF003446AB00000000000000000000000000000000000000000000
          000000000000000000000000000000090C6000B2E9FF00B2E9FF00B2E9FF00B2
          E9FF004E67C30001013400000000000000000000000000000000000000000000
          000000000000000000000000000000000000005975CB00B2E9FF00779BDF0006
          09580000000000000000000000100000000C0000000000000000000000000000
          0000000000000000000000000000000000000002023C00131A7C000000080000
          000000000004000E13700092BEEF001015740000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000005
          0650007093DB00B2E9FF00B2E9FF000B0F680000000000000000000000000000
          0000000000000000000000000000000000000000000000000004004155B700B2
          E9FF00B2E9FF005975CB0002023C000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000090C600059
          75CB00080A5C0000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
end
