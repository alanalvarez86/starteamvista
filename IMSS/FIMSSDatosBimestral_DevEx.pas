unit FIMSSDatosBimestral_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, Grids, DBGrids, StdCtrls, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox, ZBaseGridLecturaImss_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  ComCtrls, ZetaStateComboBox, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid;

type
  TTIMSSDatosBimestral_DevEx = class(TBaseGridLecturaImss_DevEx)
    dsLiq_Mov: TDataSource;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    LE_RETIRO: TZetaDBTextBox;
    Label1: TLabel;
    LE_CES_VEJ: TZetaDBTextBox;
    Label3: TLabel;
    LE_APO_VOL: TZetaDBTextBox;
    Label4: TLabel;
    LE_TOT_RET: TZetaDBTextBox;
    Label8: TLabel;
    LE_DIAS_BM: TZetaDBTextBox;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    LE_INF_PAT: TZetaDBTextBox;
    Label5: TLabel;
    LE_INF_AMO: TZetaDBTextBox;
    Label7: TLabel;
    LE_TOT_INF: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  end;

var
  TIMSSDatosBimestral_DevEx: TTIMSSDatosBimestral_DevEx;

implementation

uses DIMSS,
     ZetaCommonLists,
     ZetaCommonClasses, ZBaseGridLectura_DevEx;

{$R *.DFM}

{ TIMSSDatosBimestral }

procedure TTIMSSDatosBimestral_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stIMSS;
     HelpContext := H40413_IMSS_bimestral_empleado;
end;

procedure TTIMSSDatosBimestral_DevEx.Connect;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosBim.Conectar;
          cdsMovBim.Conectar;
          DataSource.DataSet:= cdsIMSSDatosBim;
          dsLiq_Mov.DataSet:= cdsMovBim;
     end;
end;

procedure TTIMSSDatosBimestral_DevEx.Refresh;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosBim.Refrescar;
          cdsMovBim.Refrescar;
     end;
end;

function TTIMSSDatosBimestral_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar A Datos Bimestrales IMSS';
end;

function TTIMSSDatosBimestral_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En Datos Bimestrales IMSS';
end;

function TTIMSSDatosBimestral_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar En Datos Bimestrales IMSS';
end;

procedure TTIMSSDatosBimestral_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  ApplyMinWidth;
  inherited;
  ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
