unit DSUA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, FileCtrl, DBTables,
  {$ifndef VER130}
  Variants,
  {$endif}
  DCliente,
  DConciliador,
  ZetaCommonClasses,
  ZetaBorradorSUA,
  ZetaClientDataSet,
  ZetaServerDataSet, ODSI, OCL;

type
  TArregloPatron = array[1..29] of string;
  TdmSUA = class(TdmConciliador)
    cdsTemp: TServerDataSet;
    cdsAusentismo: TZetaClientDataSet;
    Hdbc: THdbc;
    FTablaDBF: TOEQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    //FTablaDBF: TTbDBF;
    FDirectory: string;
    FMesYear: string;
    function OpenSQL( const sSQL: string ): OleVariant;
    function ConectarTabla(Tabla: TClientDataset; const sQuery,sTabla: String): Boolean;
    function GetSQLScript(const eTipo: eEmisionTipo): String;
    procedure ConciliarPatrones;
    procedure ConciliarEmpleados;
    procedure ConciliarListaEmpleados(Original, Datos: TDataset; const lTress: Boolean; Log: TStrings);
    procedure ConciliarMovimientos(const iEmpleado: TNumEmp; const sAfiliacion: String; Log: TStrings);
    procedure ConciliarListaMovimientos(Original, Datos: TDataset;const lTress: Boolean; Log: TStrings);
    function ConectarMovimientoMOV_TRA(const sAfiliacion: String): Boolean;
    procedure ConciliarListaMovimientosACREDITA(Original, Datos: TDataset;
      const lTress: Boolean; Log: TStrings);
    function ConectarMovimientoACREDITA(
      const sAfiliacion: String): Boolean;
  protected
    function GetConciliaDescripcion: string;override;
  public
    { Public declarations }
    function ConectarBaseDeDatos( const sFileName: String ): Boolean;override;
    procedure DesconectaBasedeDatos;override;
    function ConectarTablas: Boolean;override;
    procedure Conciliar;override;
  end;

var
  dmSUA: TdmSUA;

implementation
uses
    ZetaDialogo,
    ZetaCommonLists,
    ZetaCommonTools,
    FTressShell;

{$R *.DFM}

{ TdmSUA }

function GetScript( const eValor: eScript ): String;
begin
     case eValor of
          esEmpleadoMensual,
          esEmpleadoBimestral: Result := 'select -1 as CB_CODIGO, '+
                                                '0 as ' + K_FOUND + ', ' +
                                                'NUM_AFIL as ' + K_NUM_SS + ', ' +
                                                'TMP_NOM  as NOMBRE_SS,  ' +
                                                // SOP 4527
                                                // Aportaciones voluntarias se calculan mensualmente
                                                'APO_VOL  as LE_APO_VOL, ' +
                                                // ----- -----
                                                'CES_VEJ  as LE_CES_VEJ, ' +
                                                'DIA_COTB as LE_DIAS_BM, ' +
                                                'DIA_COT  as LE_DIAS_CO, ' +
                                                'PRE_DIN  as LE_EYM_DIN, ' +
                                                'PRE_ESP  as LE_EYM_ESP, ' +
                                                'CUO_EXC  as LE_EYM_EXC, ' +
                                                'CUO_FIJ  as LE_EYM_FIJ, ' +
                                                'GUA_PRE  as LE_GUARDER, ' +
                                                'PAT_INF  as LE_INF_PAT, ' +
                                                'INV_VID  as LE_INV_VID, ' +
                                                'RET_EMP  as LE_RETIRO , ' +
                                                'RIE_TRA  as LE_RIESGOS,  ' +
                                                'AMO_CRE  as LE_INF_AMO  ' +
                                           'from REL_TRA.DBF ' +
                                           'where ( REG_PATR = %s ) ';{ +
                                           'order by NUM_AFIL';        }
          esMovimientoMensual  : Result := 'select '+
                                           'NUM_AFIL as ' + K_NUM_SS + ', ' +
                                           '0 as ' + K_ORIGEN + ', '+
                                           {$ifdef ANTES}
                                           'CAST((CVE_MOV) as INTEGER ) as LM_TIPO_MOV, '+
                                           'CAST(('''') as Date ) as LM_FECHA, '+
                                           {$else}
                                           'CVE_MOV as CLAVE_MOV, '+
                                           '0 as LM_TIPO_MOV, '+
                                           'Now as LM_FECHA, '+
                                           {$endif}
                                           'FEC_MOV, '+
                                           'SAL_IMSS as LM_BASE, '+
                                           '0  as LM_AUSENCI, '+
                                           '0  as LM_INCAPAC '+
                                           //''''' as LM_CLAVE '+
                                           'from MOV_TRA.DBF '+
                                           'where '+
                                           '( REG_PATR = %s ) and '+
                                           '( NUM_AFIL = %s ) and '+
                                           '( TIP_PRO = %s ) '+
                                           'order by NUM_AFIL, FEC_MOV';
          esMovtosAusentismos : Result := 'select '+
                                          'NUM_AFIL as CB_SEGSOC, '+
                                          'CVE_MOV  as LM_TIPO_MOV,  '+
                                          'sum(DIA_AUS)  as LM_AUSENCI, '+
                                          'sum(DIA_INC)  as LM_INCAPAC  '+
                                          'from MOV_TRA.DBF '+
                                          'where ( REG_PATR = %s ) '+
                                          'and ( NUM_AFIL = %s ) '+
                                          'and ( TIP_PRO = %s ) '+
                                          'group by NUM_AFIL, CVE_MOV '+
                                          'order by NUM_AFIL' ;
          esMovimientos : Result := 'select ' +
                                    'REG_PATR, ' +
                                    'NUM_AFIL as ' + K_NUM_SS + ', ' +
                                    'MES_ANO, ' +
                                    'SAL_DIA as LM_BASE, '+
                                    'VIV_INF as LM_INF_PAT, ' +
                                    'AMO_INF as LM_INF_AMO, ' +
                                    'CTA_FIJ as LM_EYM_FIJ ' +
                                    'from ACREDITA.DBF ' +
                                    'where ' +
                                    '( REG_PATR = %s ) and '+
                                    '( MES_ANO = %s ) and  '+
                                    '( NUM_AFIL = %s ) '+
                                    'order by NUM_AFIL, MES_ANO, SAL_DIA';
          esAusentismo : Result := 'select NUM_AFI, SUM(NUM_AUS) as AUSENCIA '+
                                   'from AUSENT.DBF '+
                                   'where ( REG_PAT = %s ) '+
                                   'and ( MES_ANO = %s ) ' +
                                   'group by NUM_AFI';


//'AMO_INF  as LM_INF_AMO, '+
//'VIV_INF  as LM_INF_PAT '+
//from ACREDITA
          esPatron: Result := 'select ACT_AMO  as LS_ACT_AMO, '+
                                     'ACT_APO  as LS_ACT_APO, '+
                                     'ACT_IMS  as LS_ACT_IMS, '+
                                     'ACT_VIV  as LS_ACT_INF, '+
                                     'ACT_SAR  as LS_ACT_RET, '+
                                     'APO_VOL  as LS_APO_VOL, '+
                                     'CEN_VEJ  as LS_CES_VEJ, '+
                                     {$ifdef ANTES}
                                     'Cast( ( TOT_DIAB ) as integer ) as LS_DIAS_BM, '+
                                     'Cast( ( TOT_DIA ) as integer ) as LS_DIAS_CO, '+
                                     'Cast( ( TOT_COT ) as integer ) as LS_NUM_TRA, '+
                                     'Cast( ( TOT_COT ) as integer ) as LS_NUM_BIM, '+
                                     {$ELSE}
                                     'TOT_DIAB, '+
                                     '0 AS LS_DIAS_BM, '+
                                     'TOT_DIA, '+
                                     '0 as LS_DIAS_CO, '+
                                     'TOT_ACR,  ' +
                                     '0 as LS_INF_NUM, '+
                                     '0 as LS_NUM_TRA, '+
                                     '0 as LS_NUM_BIM, '+
                                     'TOT_COT, '+
                                     {$ENDIF}
                                     'PRE_DIN  as LS_EYM_DIN, '+
                                     'PRE_ESP  as LS_EYM_ESP, '+
                                     'CTA_EXC  as LS_EYM_EXC, '+
                                     'CTA_FIJ  as LS_EYM_FIJ, '+
                                     'FAC_ACT  as LS_FAC_ACT, '+
                                     'FAC_REC  as LS_FAC_REC, '+
                                     'GUA_DER  as LS_GUARDER, '+
                                     'VIV_CON  as LS_INF_ACR, '+
                                     'AMO_INF  as LS_INF_AMO, '+
                                     'VIV_SIN  as LS_INF_NAC, '+
                                     'INV_VID  as LS_INV_VID, '+
                                     'REC_AMO  as LS_REC_AMO, '+
                                     'REC_APO  as LS_REC_APO, '+
                                     'REC_IMS  as LS_REC_IMS, '+
                                     'REC_VIV  as LS_REC_INF, '+
                                     'REC_SAR  as LS_REC_RET, '+
                                     'RET_SAR  as LS_RETIRO , '+
                                     'RIE_TRA  as LS_RIESGOS '+
                                  'from RESUMEN.dbf '+
                                  'where ( REG_PATR = %s ) and ( MES_ANO = %s )';
          esAusentismoPatron: Result := 'select '+
                                        'sum(DIA_COT)- sum(DIA_INC)- sum(DIA_AUS) as DIAS_MENSUAL, '+
                                        'sum(DIA_COTB) - sum(DIA_INCB) - sum(DIA_AUSB) as DIAS_BIMESTRE '+
                                        'from REL_TRA.dbf '+
                                        'where ( REG_PATR = %s ) ';
     else
         Result := VACIO;
     end;
end;
procedure TdmSUA.DataModuleCreate(Sender: TObject);
begin
     inherited;
     //FTablaDBF := TtbDBF.Create( self );
end;

procedure TdmSUA.DataModuleDestroy(Sender: TObject);
begin
     FTablaDBF.Active:= FALSE;
     inherited;

end;

function TdmSUA.GetConciliaDescripcion: string;
begin
     Result := 'SUA';
end;

function TdmSUA.GetSQLScript( const eTipo: eEmisionTipo ): String;
begin
     case Clase of
          eeCMensual:
          begin
               case eTipo of
                    eeTPatron: Result := GetScript( esPatron );
                    eeTEmpleados: Result := GetScript( esEmpleadoMensual );
                    eeTMovimientos: Result := GetScript( esMovimientoMensual );
               end;
           end;
           eeCBimestral:
           begin
                case eTipo of
                     eeTPatron: Result := GetScript( esPatron );
                     eeTEmpleados: Result := GetScript( esEmpleadoBimestral );
                     eeTMovimientos: Result := GetScript( esMovimientoBimestral );
                end;
           end;

     end;
end;

procedure TdmSUA.DesconectaBasedeDatos;
begin

end;

function TdmSUA.ConectarBaseDeDatos( const sFileName: String ): Boolean;
begin
     Result := False;
     FDirectory := VACIO;

     if ZetaCommonTools.StrVacio( sFileName ) then
     begin
          ZetaDialogo.zError( '� Atenci�n !', 'No Se Especific� El Directorio De Los Archivos del SUA', 0 );
     end
     else
     begin
          Result := DirectoryExists( sFileName );
          if Result then
          begin
               FDirectory := VerificaDir( sFileName );
               {$ifdef ANTES}
               with FTablaDBF do
               begin
                    Active := FALSE;
                    DatabaseName := FDirectory;
               end;
               {$else}
               with Hdbc do
               begin
                    Attributes.Add( Format( 'DefaultDir=%s',[FDirectory] ) );
                    Connected := TRUE;
               end;
               {$endif}
          end;
     end;
end;

function TdmSUA.ConectarTabla( Tabla: TClientDataset; const sQuery, sTabla: String ): Boolean;
begin
     try
        with Tabla do
        begin
             DisableControls;
             try
                Active := False;
                Data := OpenSQL( Format( sQuery, [ Comillas( FPatronID ) , Comillas( FMesYear ) ] ) );
                if not Active then
                   Active := True;
             finally
                    EnableControls;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Abrir Tabla %s Del SUA', [ sTabla ] ), Error, 0 );
                Result := False;
           end;
     end;
end;

function TdmSUA.ConectarTablas: Boolean;
 var
    sFieldDias : string;
begin
     try
        FPatronID := dmCliente.ImssRegistro;
        if Clase = eecMensual then
           FMesYear :=  PadLCar( IntToStr( Mes ), 2, '0' ) + IntToStr( Year )
        else
           FMesYear :=  PadLCar( IntToStr( Mes * 2 ), 2, '0' ) + IntToStr( Year );

        Result := ConectarTabla( cdsPatron, GetSQLScript( eetPatron ), 'RESUMEN.DBF' ) and
                  ConectarTabla( cdsEmpleados, GetSQLScript( eetEmpleados ), 'REL_TRA.DBF' );

        with cdsPatron do
        begin
             First;
             while NOT EOF do
             begin
                  Edit;
                  FieldByName( 'LS_DIAS_BM' ).AsInteger := FieldByName( 'TOT_DIAB' ).AsInteger;
                  FieldByName( 'LS_DIAS_CO' ).AsInteger := FieldByName( 'TOT_DIA' ).AsInteger;
                  FieldByName( 'LS_NUM_TRA' ).AsInteger := FieldByName( 'TOT_COT' ).AsInteger;
                  FieldByName( 'LS_NUM_BIM' ).AsInteger := FieldByName( 'TOT_COT' ).AsInteger;
                  FieldByName( 'LS_INF_NUM' ).AsInteger := FieldByname( 'TOT_ACR' ).AsInteger;

                  Next;
             end;
             First;
        end;


        cdsAusentismo.IndexFieldNames := '';
        ConectarTabla( cdsAusentismo,
                       Format( GetScript(esAusentismoPatron), [Comillas(FPatronID)] ) ,
                       'REL_TRA.DBF' );

        while NOT cdsAusentismo.EOF do
        begin
             with cdsPatron do
             begin
                  Edit;
                  FieldByName( 'LS_DIAS_CO' ).AsInteger := cdsAusentismo.FieldByName( 'DIAS_MENSUAL' ).AsInteger;
                  FieldByName( 'LS_DIAS_BM' ).AsInteger := cdsAusentismo.FieldByName( 'DIAS_BIMESTRE' ).AsInteger;
                  Post;
             end;
             cdsAusentismo.Next;
        end;

        ConectarTabla( cdsAusentismo,
                       Format( GetScript(esAusentismo), [ Comillas(FPatronID), Comillas(FMesYear) ] ) ,
                       'AUSENT.DBF' );
        cdsAusentismo.IndexFieldNames := 'NUM_AFI';
        cdsAusentismo.First;
        if ( Clase = eecMensual ) then
           sFieldDias := 'LE_DIAS_CO'
        else
            sFieldDias := 'LE_DIAS_BM';

        while NOT cdsAusentismo.EOF do
        begin
             with cdsEmpleados do
             begin
                  if Locate('CB_SEGSOC', cdsAusentismo.FieldByName('NUM_AFI').AsString, [] ) then
                  begin
                       Edit;
                       FieldByName( sFieldDias ).AsInteger := FieldByName( sFieldDias ).AsInteger - cdsAusentismo.FieldByName('AUSENCIA').AsInteger;
                       Post;
                  end;
             end;
             cdsAusentismo.Next;
        end;
        //cdsAusentismo.SaveToFile('d:\temp\ausentismo.cds');
        cdsAusentismo.IndexFieldNames := VACIO;
        cdsAusentismo.Close;

        //cdsPatron.SaveToFile( 'd:\temp\resumen.cds' );
        //cdsEmpleados.SaveToFile( 'd:\temp\relTra.cds' );

     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', 'Error Al Abrir Tablas De CD IMSS', Error, 0 );
                Result := False;
           end;
     end;
end;

function TdmSUA.ConectarMovimientoMOV_TRA( const sAfiliacion: String ): Boolean;
{Se trae la informacion de la tabla MOV_TRA}
const
     K_AUSENTISMO = 11;
     K_INCAPACIDAD = 12;
var
   Datos: Variant;
   sTipo: string;
begin
     Result := False;
     case Clase of
          eeCMensual: sTipo := 'M';
          else sTipo := 'B';
     end;
     try
        Datos := OpenSQL( Format( GetScript( esMovimientoMensual ), [ Comillas( FPatronID ),
                                                                      Comillas( sAfiliacion ),
                                                                      Comillas( sTipo ) ] ) );
        with cdsMovimientos do
        begin
             Active := False;
             Data := Datos;
             First;

             while NOT EOF do
             begin
                  Edit;
                  FieldByName('LM_TIPO_MOV').AsInteger  := FieldByName('CLAVE_MOV').AsInteger;
                  Next;
             end;
             
             First;
             while NOT EOF do
             begin
                  if FieldByName('LM_TIPO_MOV').AsInteger in [ K_AUSENTISMO,  K_INCAPACIDAD ] then
                     Delete
                  else
                  begin
                       Edit;
                       FieldByName( 'LM_FECHA').AsDateTime := StrAsFecha( FieldByName( 'FEC_MOV' ).AsString );

                       ConectarTabla( cdsAusentismo,
                                      Format( GetScript(esMovtosAusentismos), [ Comillas(FPatronID),
                                                                                Comillas(sAfiliacion),
                                                                                Comillas(sTipo) ] ) ,
                                      'MOV_TRA.DBF' );

                       while NOT cdsAusentismo.EOF do
                       begin
                            if ( FieldByName( 'LM_TIPO_MOV' ).AsInteger = K_INCAPACIDAD ) then
                               FieldByName('LM_INCAPAC').AsInteger := cdsAusentismo.FieldByName('LM_INCAPAC').AsInteger
                            else
                                FieldByName('LM_AUSENCI').AsInteger := cdsAusentismo.FieldByName('LM_AUSENCI').AsInteger;
                            cdsAusentismo.Next;
                       end;

                       Post;
                       Next;
                  end;
             end;
             First;
        end;

        cdsAusentismo.Close;
        with cdsMovConc do
        begin
             Active := False;
             Data := Datos;
             EmptyDataset;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Leer Movimientos de IMSS # %s', [ sAfiliacion ] ), Error, 0 );
           end;
     end;
end;

function TdmSUA.ConectarMovimientoACREDITA( const sAfiliacion: String ): Boolean;
{Se trae la informacion de la tabla ACREDITA}
begin
     Result := False;
     try
        with cdsMovimientos do
        begin
             Active := False;
             Data := OpenSQL( Format( GetScript( esMovimientos ), [ Comillas( FPatronID ),
                                                                    Comillas( FMesYear ),
                                                                    Comillas( sAfiliacion ) ] ) );
             Active := TRUE;
             Result := TRUE;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Leer Movimientos de IMSS # %s', [ sAfiliacion ] ), Error, 0 );
           end;
     end;
end;

function TdmSUA.OpenSQL( const sSQL: string ): Olevariant;

 var
    i: integer;
    sFieldName: string;
begin
     try
        with FTablaDBF do
        begin
             Active := FALSE;
             SQL.Text := sSQL;
             Active := TRUE;
        end;

        with cdsTemp do
        begin
             InitTempDataset;
             for i:= 0 to FTablaDBF.FieldCount - 1 do
             begin
                  sFieldName := FTablaDBF.Fields[i].FieldName;

                  case FTablaDBF.Fields[i].DataType of
                       ftString: AddStringField( sFieldName, FTablaDBF.Fields[i].DisplayWidth );
                       ftSmallint: AddIntegerField( sFieldName );
                       ftInteger: AddIntegerField( sFieldName );
                       ftWord: AddIntegerField( sFieldName );
                       ftBoolean: AddBooleanField( sFieldName );
                       ftFloat: AddFloatField( sFieldName );
                       ftCurrency: AddFloatField( sFieldName );
                       ftDate: AddDateField( sFieldName );
                       ftDateTime: AddDateTimeField( sFieldName );
                  end;
             end;
             CreateTempDataset;
        end;

        with FTablaDBF do
        begin
             while NOT EOF do
             begin
                  cdsTemp.Append;
                  for i:= 0 to FieldCount - 1 do
                  begin
                       sFieldName := Fields[i].FieldName;
                       case Fields[i].DataType of
                            ftString: cdsTemp.FieldByName(sFieldName).AsString := FieldByName(sFieldName).AsString;
                            ftSmallint: cdsTemp.FieldByName(sFieldName).AsInteger := FieldByName(sFieldName).AsInteger;
                            ftInteger: cdsTemp.FieldByName(sFieldName).AsInteger := FieldByName(sFieldName).AsInteger;
                            ftWord: cdsTemp.FieldByName(sFieldName).AsInteger := FieldByName(sFieldName).AsInteger;
                            ftBoolean: cdsTemp.FieldByName(sFieldName).AsBoolean := FieldByName(sFieldName).AsBoolean;
                            ftFloat: cdsTemp.FieldByName(sFieldName).AsFloat := FieldByName(sFieldName).AsFloat;
                            ftCurrency: cdsTemp.FieldByName(sFieldName).AsFloat := FieldByName(sFieldName).AsFloat;
                            ftDate: cdsTemp.FieldByName(sFieldName).AsDateTime := FieldByName(sFieldName).AsDateTime;
                            ftDateTime: cdsTemp.FieldByName(sFieldName).AsDateTime := FieldByName(sFieldName).AsDateTime;
                       end;

                  end;
                  cdsTemp.Post;
                  Next;
             end;
        end;

        Result := cdsTemp.Data;
        
     finally
            FTablaDBF.Active:= FALSE;
     end;
end;
procedure TdmSUA.Conciliar;
var
   iSteps: Integer;
begin
     FDiferencias := 0;
     iSteps := cdsEmpleados.RecordCount + dmCliente.cdsLiqEmp.RecordCount + 1;
     TressShell.ConciliarStart( iSteps );
     try
        ResetBitacoras;
        ConciliarPatrones;
        ConciliarEmpleados;
        FinishBitacoras;
     finally
            if TressShell.CancelaConciliacion then
            begin
                 ListaEspacio;
                 ListaFiller;
                 ListaFiller( 'Proceso Cancelado por el Usuario' );
                 ListaFiller;
            end;
            TressShell.ConciliarEnd;
     end;
end;

procedure TdmSUA.ConciliarPatrones;
 const
      aEmisionClase: array[ eEmisionClase ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Mensual', 'Bimestral' );
 {var
    rTotalTress, rTotalCdImss: TPesos;}
    
begin
     with dmCliente do
     begin
          ListaEspacio;
          ListaTexto( Format( 'Conciliaci�n Del Patr�n: %s ( # %s )', [ IMSSRegistro, IMSSPatron ] ) );
          ListaTexto( Format( '                    A�o: %d', [ IMSSYear ] ) );
          ListaTexto( Format( '                    Mes: %s', [ ZetaCommonLists.ObtieneElemento( lfMeses, IMSSMes - 1 ) ] ) );
          ListaTexto( Format( '                   Tipo: %s', [ ZetaCommonLists.ObtieneElemento( lfTipoLiqIMSS, Ord( IMSSTipo ) ) ] ) );
          ListaTexto( Format( '                Periodo: %s', [ aEmisionClase[ Clase ] ] ) );
     end;
     ListaEspacio;
     ListaEspacio;
     ListaFiller;
     ListaFiller( 'TOTALES' );
     ListaFiller;
     case Clase of
          eeCMensual:
          begin
               ListaTitulo( 'Cuotas del IMSS - Mensual' );
               ConciliarCampoPatron( 'EYM: Cuota Fija', 'LS_EYM_FIJ' );
               ConciliarCampoPatron( 'EYM: Excedente', 'LS_EYM_EXC' );
               ConciliarCampoPatron( 'EYM: Dinero', 'LS_EYM_DIN' );
               ConciliarCampoPatron( 'EYM: Especie', 'LS_EYM_ESP' );
               ConciliarCampoPatron( 'Guarder�as:', 'LS_GUARDER' );
               ConciliarCampoPatron( 'Riesgos de Trabajo:', 'LS_RIESGOS' );
               ConciliarCampoPatron( 'Invalidez y Vida:', 'LS_INV_VID' );
               ConciliarCampoPatron( 'Actualizaci�nes:', 'LS_ACT_IMS' );
               ConciliarCampoPatron( 'Recargos:', 'LS_REC_IMS' );

               ListaTitulo( 'Resumen Mensual' );
               ConciliarCampoPatron( 'Asegurados:', 'LS_NUM_TRA' );
               ConciliarCampoPatron( 'D�as Cotizados:', 'LS_DIAS_CO' );
               ConciliarCampoPatron( 'Factor de Actualizaciones:', 'LS_FAC_ACT' );
               ConciliarCampoPatron( 'Tasa de Recargos:', 'LS_FAC_REC' );

               {Estos datos estan en ambas partes, pero el Tress no los muestra
               ConciliarCampoPatron( 'Actual. Amortizaciones INFO', 'LS_ACT_AMO' );
               ConciliarCampoPatron( 'Actual. Aportacion Voluntaria', 'LS_ACT_APO' );
               ConciliarCampoPatron( 'Recargos Amortizaciones INFO', 'LS_REC_AMO' );
               ConciliarCampoPatron( 'Recargos Aporta Voluntarias', 'LS_REC_APO' );}

          end;
          eeCBimestral:
          begin
               ListaTitulo( 'Cuenta Individual' );
               ConciliarCampoPatron( 'Retiro:', 'LS_RETIRO' );
               ConciliarCampoPatron( 'Cesant�a y Vejez:', 'LS_CES_VEJ' );
               ConciliarCampoPatron( 'Actualizaciones:', 'LS_ACT_RET' );
               ConciliarCampoPatron( 'Recargos:', 'LS_REC_RET' );
               ConciliarCampoPatron( 'Aportaci�n Voluntaria:', 'LS_APO_VOL' );

               ListaTitulo( 'Resumen Bimestral' );
               ConciliarCampoPatron( 'Acreditados:', 'LS_INF_NUM' );
               ConciliarCampoPatron( 'Asegurados:', 'LS_NUM_BIM' );
               ConciliarCampoPatron( 'D�as Cotizados:', 'LS_DIAS_BM' );

               ListaTitulo( 'Cuenta INFONAVIT' );
               ConciliarCampoPatron( 'Cuota INFO Emp. NO Acreditado:', 'LS_INF_NAC' );
               ConciliarCampoPatron( 'Cuota INFO Emp. Acreditados:', 'LS_INF_ACR' );
               ConciliarCampoPatron( 'Amortizaciones INFONAVIT:', 'LS_INF_AMO' );
               ConciliarCampoPatron( 'Actual. Cuotas INFO:', 'LS_ACT_INF' );
               ConciliarCampoPatron( 'Recargos de Cuotas INFONAVIT:', 'LS_REC_INF' );
          end;
     end;
     TressShell.ConciliarStep;
end;


procedure TdmSUA.ConciliarEmpleados;
var
   FDatos: TStrings;
begin
     ListaEspacio;
     ListaEspacio;
     ListaFiller;
     ListaFiller( 'EMPLEADOS' );
     ListaFiller;
     ListaEspacio;
     FDatos := TStringList.Create;
     try
        ConciliarListaEmpleados( dmCliente.cdsLiqEmp, cdsEmpleados, True, FDatos );
        ConciliarListaEmpleados( cdsEmpleados, dmCliente.cdsLiqEmp, False, FDatos );
        if ( FDatos.Count > 0 ) then
        begin
             ListaAgregar( FDatos, Lista );
        end;
     finally
            FreeAndNil( FDatos );
     end;
end;

procedure TdmSUA.ConciliarListaEmpleados( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings  );
const
     K_FILLER = '=';
     K_FILLER_ANCHO = 60;
var
   sSeguroSocial, sTexto: String;
   iEmpleado: TNumEmp;
begin
     FEmpNotFound.Clear;
     with Original do
     begin
          First;
          while not Eof and TressShell.ConciliarStep do
          begin
               if ( FieldByName( K_FOUND ).AsInteger = 0 ) then
               begin
                    sSeguroSocial := FieldByName( K_NUM_SS ).AsString;
                    if lTress then
                    begin
                         sTexto := Format( '# IMSS %s : %s %s, %s ( # Tress %d )', [ sSeguroSocial, FieldByName( 'CB_APE_PAT' ).AsString, FieldByName( 'CB_APE_MAT' ).AsString, FieldByName( 'CB_NOMBRES' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger ] );
                    end
                    else
                    begin
                         sTexto := Format( '# IMSS %s : %s', [ sSeguroSocial, FieldByName( 'NOMBRE_SS' ).AsString ] );
                    end;
                    if Datos.Locate( K_NUM_SS, sSeguroSocial, [] ) then
                    begin
                         iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                         SetEmployeeFound( Original );
                         SetEmployeeFound( Datos );
                         FEmpleados.Clear;
                         case Clase of
                              eeCMensual:
                              begin
                                   ConciliarCampoEmpleado( K_CUOTA_FIJA, 'LE_EYM_FIJ', FEmpleados, cdsDatosEmpleados );
{$IFDEF FROMEX}
                                   ConciliarCampoEmpleadoPobla( K_CUOTA_FIJA, 'LE_EYM_FIJ', FEmpleados, cdsDatosEmpleadosTodos );
{$ENDIF}
                                   ConciliarCampoEmpleado( K_Excedente, 'LE_EYM_EXC', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( K_Dinero, 'LE_EYM_DIN', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( K_Especie, 'LE_EYM_ESP', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( K_Riesgos, 'LE_RIESGOS', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( K_Invalidez, 'LE_INV_VID', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( K_Guarderias, 'LE_GUARDER', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( K_Dias, 'LE_DIAS_CO', FEmpleados, cdsDatosEmpleados ); 
                                   // SOP 4527
                                   // Aportaciones voluntarias se calcula mensualmente
                                   ConciliarCampoEmpleado( K_Voluntaria, 'LE_APO_VOL', FEmpleados, cdsDatosEmpleados);
                                   // ----- -----
                              end;
                              eeCBimestral:
                              begin
                                   ConciliarCampoEmpleado( K_RETIRO, 'LE_RETIRO', FEmpleados, cdsDatosEmpleados );
{$IFDEF FROMEX}
                                   ConciliarCampoEmpleadoPobla( K_RETIRO, 'LE_RETIRO', FEmpleados, cdsDatosEmpleadosTodos );
{$ENDIF}
                                   ConciliarCampoEmpleado( K_Cesantia, 'LE_CES_VEJ', FEmpleados, cdsDatosEmpleados );
                                   // SOP 4527
                                   // Aportaciones voluntarias se calcula mensualmente
                                   // ConciliarCampoEmpleado( K_Voluntaria, 'LE_APO_VOL', FEmpleados, cdsDatosEmpleados);
                                   // ----- -----
                                   ConciliarCampoEmpleado( K_Infonavit, 'LE_INF_PAT', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( k_Amortiza, 'LE_INF_AMO', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( K_Dias, 'LE_DIAS_BM', FEmpleados, cdsDatosEmpleados );


                                   {
                                   ASEGURA.DBF
                                   ConciliarCampoEmpleado( 'M�dulo', 'TB_MODULO', FEmpleados );
                                   ConciliarCampoEmpleado( 'Jornada', 'TU_TIP_JOR', FEmpleados );
                                   if RevisaCurp then
                                      ConciliarCampoEmpleado( 'CURP', 'CB_CURP', FEmpleados );
                                   ConciliarCampoEmpleado( '# Cr�dito INFONAVIT', 'CB_INFCRED', FEmpleados );
                                   ConciliarCampoEmpleado( 'Tipo de Pr�stamo', 'CB_INFTIPO', FEmpleados );
                                   ConciliarCampoEmpleado( 'Amortizaci�n', 'CB_INFTASA', FEmpleados );
                                   if RevisaInfonavit then
                                      ConciliarCampoEmpleado( 'Inicio Del Cr�dito', 'CB_INF_INI', FEmpleados );}
                              end;
                         end;
                         if ( iEmpleado > 0 ) then
                         begin
                              ConciliarMovimientos( iEmpleado, sSeguroSocial, FMovimientos );
                         end;
                         if ( FEmpleados.Count > 0 ) or ( FMovimientos.Count > 0 ) then
                         begin
                              ListaEspacio;
                              ListaDelimitador( '_', '', K_ANCHO );
                              ListaDelimitador( '_', '', K_ANCHO );
                              ListaEspacio;
                              ListaSubFiller( sTexto );
                              ListaEspacio;
                         end;
                         if ( FEmpleados.Count > 0 ) then
                         begin
                              ListaEncabezados;
                              ListaAgregar( FEmpleados, FLista );
                         end;
                         if ( FMovimientos.Count > 0 ) then
                         begin
                              ListaAgregar( FMovimientos, FLista );
                         end;
                    end
                    else
                    begin
                         SetDiferencias;
                         FEmpNotFound.Add( sTexto );
                         if lTress then
                            AgregaRegistro( cdsEmpleadosTress, [ sSeguroSocial,
                                                                 PrettyName( FieldByName( 'CB_APE_PAT' ).AsString,
                                                                             FieldByName( 'CB_APE_MAT' ).AsString,
                                                                             FieldByName( 'CB_NOMBRES' ).AsString),
                                                                 FieldByName( 'CB_CODIGO' ).AsString ] )
                         else
                             AgregaRegistro( cdsEmpleadosIMSS, [ sSeguroSocial, FieldByName( 'NOMBRE_SS' ).AsString ] )
                    end;
               end;
               Next;
          end;
     end;
     if ( FEmpNotFound.Count > 0 ) then
     begin
          with Log do
          begin
               Add( VACIO );
               Add( StringOfChar( K_FILLER, K_FILLER_ANCHO ) );
               if lTress then
                  Add( 'Empleados de Tress Que No Aparecen En SUA' )
               else
                   Add( 'Empleados En SUA Que No Aparecen En Tress' );
               Add( StringOfChar( K_FILLER, K_FILLER_ANCHO ) );
          end;
          ListaAgregar( FEmpNotFound, Log );
     end;
end;

procedure TdmSUA.ConciliarMovimientos( const iEmpleado: TNumEmp; const sAfiliacion: String; Log: TStrings  );
const
     K_ANCHO_COL_MOV = 15;
     K_FILLER = '-';
var
   sOrigen, sTexto: String;
   i, iTope: Integer;
begin
     iTope := 6;
     Log.Clear;
     if dmCliente.ConectarMovimientos( Clase, iEmpleado ) then
     begin
          if ConectarMovimientoMOV_TRA( sAfiliacion ) then
          begin
               ConciliarListaMovimientos( dmCliente.cdsLiqMov, cdsMovimientos, True, Log );
               ConciliarListaMovimientos( cdsMovimientos, dmCliente.cdsLiqMov, False, Log );
          end;

          if ConectarMovimientoACREDITA( sAfiliacion ) then
          begin
               ConciliarListaMovimientosACREDITA( dmCliente.cdsLiqMov, cdsMovimientos, True, Log );
               ConciliarListaMovimientosACREDITA( cdsMovimientos, dmCliente.cdsLiqMov, False, Log );
          end;

          with cdsMovConc do
          begin
               if ( RecordCount > 0 ) then
               begin
                    Log.Add( VACIO );
                    Log.Add( ZetaCommonTools.PadR( 'Origen', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'Fecha', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'Tipo', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'Salario Base', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'Ausentismos', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'Incapacidades', K_ANCHO_COL_MOV ) + K_ESPACIO );
                    { Agregar L�neas de Encabezado }
                    sTexto := VACIO;
                    for i := 1 to iTope do
                    begin
                         sTexto := sTexto + StringOfChar( K_FILLER, K_ANCHO_COL_MOV ) + K_ESPACIO;
                    end;
                    Log.Add( sTexto );
                    { Agregar Datos }
                    First;
                    while not Eof do
                    begin
                         case eOrigen( FieldByName( K_ORIGEN ).AsInteger ) of
                            eoTress: sOrigen := 'Tress'                    
                         else                                              
                             sOrigen := 'SUA';                             
                         end;                                              
                         sTexto := ZetaCommonTools.PadR( sOrigen, K_ANCHO_COL_MOV ) + K_ESPACIO +
                                   ZeTaCommonTools.PadL( GetCampoMovimiento( 'LM_FECHA' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                   ZeTaCommonTools.PadL( GetCampoMovimiento( 'LM_TIPO_MOV' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                   ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_BASE' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                   ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_AUSENCI' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                   ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_INCAPAC' ), K_ANCHO_COL_MOV ) + K_ESPACIO ;
                         Log.Add( sTexto );
                         Next;
                    end;
               end;
          end;
     end;
end;

procedure ResetTag( Dataset : TDataSet );
 var
    i: integer;
begin
     for i:= 0 to Dataset.RecordCount - 1 do
         Dataset.Fields[i].Tag := 0;
end;

procedure TdmSUA.ConciliarListaMovimientos( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings );
var
   dFecha: TDate;
   sFecha: String;
   iTipo: integer;
   lEncontrado{,lValidaCambiosSalario}: Boolean;
begin
     {Si se hace opcional esta validacion, esta variable va en funcion al control que se ponga en interfase de usuario}
     //lValidaCambiosSalario := TRUE;

     ResetTag( Original );
     with Original do
     begin
          case Clase of
               eeCMensual,eeCBimestral:
               begin
                    {FieldByName( 'LM_EYM_FIJ' ).Tag := 1;
                    FieldByName( 'LM_EYM_EXC' ).Tag := 1;
                    FieldByName( 'LM_EYM_DIN' ).Tag := 1;
                    FieldByName( 'LM_EYM_ESP' ).Tag := 1;
                    FieldByName( 'LM_INV_VID' ).Tag := 1;
                    FieldByName( 'LM_RIESGOS' ).Tag := 1;
                    FieldByName( 'LM_GUARDER' ).Tag := 1; }
                    //FieldByName( 'LM_CLAVE' ).Tag := 1;
                    //FieldByName( 'LM_FECHA' ).Tag := 1;
                    FieldByName( 'LM_BASE' ).Tag := 1;
                    FieldByName( 'LM_AUSENCI' ).Tag := 1;
                    FieldByName( 'LM_INCAPAC' ).Tag := 1;
                    FieldByName( 'LM_TIPO_MOV' ).Tag := 1;
               end;
               {eeCBimestral:
               begin
                    FieldByName( 'LM_RETIRO' ).Tag := 1;
                    FieldByName( 'LM_CES_VEJ').Tag := 1;
                    FieldByName( 'LM_INF_PAT').Tag := 1;
                    FieldByName( 'LM_INF_AMO').Tag := 1;
               end;}
          end;
          {FieldByName( 'LM_DIAS' ).Tag := 1;
          FieldByName( 'LM_BASE' ).Tag := 1;   }

          First;
          while not Eof do
          begin
               dFecha := FieldByName( 'LM_FECHA' ).AsDateTime;
               sFecha := ZetaCommonTools.FechaAsStr( dFecha );
               iTipo := FieldByName( K_TIPO_MOV ).AsInteger;

               lEncontrado := Datos.Locate( Format( 'LM_FECHA;%s', [ K_TIPO_MOV ] ), VarArrayOf( [ sFecha, iTipo ] ), [] );

               {if lTress AND lValidaCambiosSalario {AND (NOT lEncontrado) AND (iTipo = K_MAESTRA_EAMP_CAMBIO_COTIZACION) then
               begin
                    lEncontrado := Datos.Locate( Format( 'LM_FECHA;%s', [ K_TIPO_MOV ] ), VarArrayOf( [ sFecha, K_MAESTRA_INICIAL ] ), [] );
               end;
}
               if lEncontrado then
               begin
                    if CamposSonDiferentes( Original, Datos ) then
                    begin
                         SetDiferencias;
                         AddMovimiento( Original, lTress );
                         AddMovimiento( Datos, not lTress );

                         //Movimientos para la bitacora
                         //RegistraBitacora( cdsMovtos, Original, lTress );
                         //RegistraBitacora( cdsMovtos, Datos, not lTress );

                    end;
                    { Evita que se vuelva a procesar }
                    Datos.Delete;
               end
               else
               begin
                    SetDiferencias;
                    AddMovimiento( Original, lTress );

                    if lTress then
                       RegistraBitacora( cdsMovtosTress, Original, lTress )
                    else
                        RegistraBitacora( cdsMovtosImss, Original, lTress );
               end;
               Next;
          end;
     end;
end;



procedure TdmSUA.ConciliarListaMovimientosACREDITA( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings );
var
   rSalario: Currency;
   lEncontrado: Boolean;
begin
     ResetTag( Original );

     with Original do
     begin
          FieldByName( 'LM_BASE' ).Tag := 1;
          FieldByName( 'LM_INF_PAT' ).Tag := 1;
          FieldByName( 'LM_INF_AMO' ).Tag := 1;
          FieldByName( 'LM_EYM_FIJ' ).Tag := 1;

          First;
          while not Eof do
          begin
               rSalario := FieldByName( 'LM_BASE' ).AsFloat;

               lEncontrado := Datos.Locate( 'LM_BASE', VarArrayOf( [ rSalario ] ), [] );

               if lEncontrado then
               begin
                    if CamposSonDiferentes( Original, Datos ) then
                    begin
                         SetDiferencias;
                         AddMovimiento( Original, lTress );
                         AddMovimiento( Datos, not lTress );
                    end;
                    { Evita que se vuelva a procesar }
                    Datos.Delete;
               end;
               {else
               begin
                    SetDiferencias;
                    AddMovimiento( Original, lTress );
               end;}
               Next;
          end;
     end;
end;

end.








