unit FEmpleadosNoRegistradosIMSS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TEmpleadosNoRegistradosIMSS = class(TBaseGridLectura_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;
  public
    { Public declarations }

  end;

const
     K_NOMBRE_FORMA = 'empleados no registrados en el IMSS';

var
  EmpleadosNoRegistradosIMSS: TEmpleadosNoRegistradosIMSS;

implementation

{$R *.dfm}


uses
     FTressShell, ZetaCommonClasses;


procedure TEmpleadosNoRegistradosIMSS.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n empleado no registrado en el IMSS'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

     ZetaDBGridDBTableView.OptionsData.Editing := False;
     HelpContext := H_CONCIL_EMP_NO_REG_IMSS;
end;

procedure TEmpleadosNoRegistradosIMSS.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

function TEmpleadosNoRegistradosIMSS.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosNoRegistradosIMSS.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosNoRegistradosIMSS.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosNoRegistradosIMSS.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TEmpleadosNoRegistradosIMSS.Refresh;
begin

end;

procedure TEmpleadosNoRegistradosIMSS.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsEmpleadosTress.DataSet;
end;

end.
