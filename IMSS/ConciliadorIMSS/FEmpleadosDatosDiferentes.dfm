inherited EmpleadosDatosDiferentes: TEmpleadosDatosDiferentes
  Left = 0
  Top = 0
  Caption = 'Empleados con datos o montos diferentes'
  ClientHeight = 412
  ClientWidth = 842
  OldCreateOrder = False
  ExplicitWidth = 842
  ExplicitHeight = 412
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 842
    ExplicitWidth = 842
    inherited ValorActivo2: TPanel
      Width = 583
      ExplicitWidth = 583
      inherited textoValorActivo2: TLabel
        Width = 577
        ExplicitLeft = 497
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 237
    Width = 842
    Height = 175
    ExplicitTop = 237
    ExplicitWidth = 842
    ExplicitHeight = 175
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.CancelOnExit = True
      OptionsData.Editing = True
      OptionsSelection.HideSelection = True
      OptionsView.NoDataToDisplayInfoText = 'No se encontraron diferencias'
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        MinWidth = 64
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        MinWidth = 64
        Width = 64
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = '# Seguro Social'
        DataBinding.FieldName = 'CB_SEGSOC'
        MinWidth = 64
        Width = 64
      end
      object DESCRIPCION: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'DESCRIPCION'
        MinWidth = 64
        Width = 64
      end
      object TRESS: TcxGridDBColumn
        Caption = 'Tress'
        DataBinding.FieldName = 'TRESS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 64
        Width = 64
      end
      object CD_IMSS: TcxGridDBColumn
        Caption = 'IMSS'
        DataBinding.FieldName = 'CD_IMSS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 64
        Width = 64
      end
      object DIFERENCIA: TcxGridDBColumn
        Caption = 'Diferencia'
        DataBinding.FieldName = 'DIFERENCIA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 64
        Width = 64
      end
    end
  end
  object gbMensual: TcxGroupBox [2]
    Left = 0
    Top = 19
    Hint = ''
    Align = alTop
    Caption = ' Mostrar diferencias: '
    TabOrder = 2
    Height = 110
    Width = 842
    object Label11: TLabel
      Left = 581
      Top = 50
      Width = 52
      Height = 13
      Caption = 'Mayores a:'
    end
    object BtnFiltro1: TcxButton
      Left = 688
      Top = 47
      Width = 21
      Height = 21
      Hint = 'Aplicar filtro'
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFE4C3A5FFEEDBC9FFF7EEE5FFFEFCFBFFF9F2EBFFF3E4
        D7FFE7CAAFFFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFDDB58FFFDAAE85FFF0DFCFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F3EDFFE4C3A5FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF9F2EBFFFBF7F3FFFFFF
        FFFFFEFEFDFFEFDCCBFFE4C3A5FFDDB58FFFE0BB99FFEAD1B9FFFBF7F3FFFFFF
        FFFFFEFCFBFFE1BE9DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFFCF8F5FFFFFFFFFFFFFFFFFFECD6C1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD9AD83FFF3E6D9FFFFFFFFFFFBF7F3FFDBB189FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFEFCFBFFF0DFCFFFDAAE
        85FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFF9F3EDFFFFFF
        FFFFE9CFB7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFFDFAF7FFEDD8
        C5FFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFE9CEB5FFFFFFFFFFF7EEE5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD9AD83FFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFE2BF9FFFE2BF9FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF0DFCFFFF5EA
        DFFFE2BF9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFDAAE85FFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFEDD8C5FFFFFFFFFFF3E6D9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFEDD7C3FFFBF7F3FFE2BF
        9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFBA97FFFFFFFFFFFFFFFFFFE4C5
        A7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF2E2D3FFFFFF
        FFFFFFFFFFFFFFFFFFFFDEB691FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFF2E3D5FFFFFFFFFFFDFBF9FFE4C5A7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFE9CEB5FFFFFFFFFFFFFFFFFFFFFFFFFFDAAE85FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF6EBE1FFFFFFFFFFFFFFFFFFF6EB
        E1FFEAD2BBFFE7CAAFFFEAD2BBFFF5EADFFFFFFFFFFFFFFFFFFFFBF6F1FFFDFA
        F7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE
        85FFEFDCCBFFFEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEEDBC9FFDAAE85FFDDB58FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE0BB99FFEDD8C5FFF3E6D9FFF9F2
        EBFFF3E4D7FFEBD3BDFFE1BD9BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 13
      OnClick = BtnFiltro1Click
    end
    object cbLM_DIAS: TcxCheckBox
      Left = 16
      Top = 23
      Hint = ''
      Caption = 'D'#237'as cotizados'
      State = cbsChecked
      TabOrder = 0
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object cbLM_EYM_FIJ: TcxCheckBox
      Left = 144
      Top = 48
      Hint = ''
      Caption = 'EYM: Cuota Fija'
      TabOrder = 2
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 104
    end
    object cbLM_EYM_EXC: TcxCheckBox
      Left = 272
      Top = 23
      Hint = ''
      Caption = 'EYM: Excedente'
      TabOrder = 4
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 104
    end
    object cbLM_EYM_DIN: TcxCheckBox
      Left = 144
      Top = 73
      Hint = ''
      Caption = 'EYM: Dinero'
      TabOrder = 3
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object cbLM_EYM_ESP: TcxCheckBox
      Left = 272
      Top = 48
      Hint = ''
      Caption = 'EYM: Especie'
      TabOrder = 5
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object cbLM_INV_VID: TcxCheckBox
      Left = 408
      Top = 47
      Hint = ''
      Caption = 'Riesgos de Trabajo'
      TabOrder = 7
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 115
    end
    object cbLM_RIESGOS: TcxCheckBox
      Left = 408
      Top = 24
      Hint = ''
      Caption = 'Invalidez y Vida'
      TabOrder = 8
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object cbLM_GUARDER: TcxCheckBox
      Left = 272
      Top = 72
      Hint = ''
      Caption = 'Guarder'#237'as'
      TabOrder = 6
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object cbLM_BASE: TcxCheckBox
      Left = 144
      Top = 25
      Hint = ''
      Caption = 'Salario base'
      State = cbsChecked
      TabOrder = 1
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object nMontos: TZetaNumero
      Left = 641
      Top = 47
      Width = 41
      Height = 21
      Mascara = mnHoras
      TabOrder = 9
      Text = '0.00'
      UseEnterKey = True
      OnExit = nMontosExit
    end
    object cbLM_INCAPACI: TcxCheckBox
      Left = 16
      Top = 48
      Hint = ''
      Caption = 'D'#237'as incapacidad'
      State = cbsChecked
      TabOrder = 10
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 107
    end
    object cbLM_AUSENCI: TcxCheckBox
      Left = 16
      Top = 72
      Hint = ''
      Caption = 'D'#237'as ausentismo'
      State = cbsChecked
      TabOrder = 11
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 103
    end
    object cbLE_APO_VOL: TcxCheckBox
      Left = 408
      Top = 72
      Hint = ''
      Caption = 'Aportaci'#243'n Voluntaria'
      TabOrder = 12
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 124
    end
  end
  object gbBimestral: TcxGroupBox [3]
    Left = 0
    Top = 129
    Hint = ''
    Align = alTop
    Caption = ' Mostrar diferencias: '
    TabOrder = 3
    Height = 108
    Width = 842
    object Label12: TLabel
      Left = 451
      Top = 52
      Width = 52
      Height = 13
      Caption = 'Mayores a:'
    end
    object BtnFiltro2: TcxButton
      Left = 558
      Top = 48
      Width = 21
      Height = 21
      Hint = 'Aplicar filtro'
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFE4C3A5FFEEDBC9FFF7EEE5FFFEFCFBFFF9F2EBFFF3E4
        D7FFE7CAAFFFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFDDB58FFFDAAE85FFF0DFCFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F3EDFFE4C3A5FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF9F2EBFFFBF7F3FFFFFF
        FFFFFEFEFDFFEFDCCBFFE4C3A5FFDDB58FFFE0BB99FFEAD1B9FFFBF7F3FFFFFF
        FFFFFEFCFBFFE1BE9DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFFCF8F5FFFFFFFFFFFFFFFFFFECD6C1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD9AD83FFF3E6D9FFFFFFFFFFFBF7F3FFDBB189FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFEFCFBFFF0DFCFFFDAAE
        85FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFF9F3EDFFFFFF
        FFFFE9CFB7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFFDFAF7FFEDD8
        C5FFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFE9CEB5FFFFFFFFFFF7EEE5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD9AD83FFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFE2BF9FFFE2BF9FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF0DFCFFFF5EA
        DFFFE2BF9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFDAAE85FFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFEDD8C5FFFFFFFFFFF3E6D9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFEDD7C3FFFBF7F3FFE2BF
        9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFBA97FFFFFFFFFFFFFFFFFFE4C5
        A7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF2E2D3FFFFFF
        FFFFFFFFFFFFFFFFFFFFDEB691FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFF2E3D5FFFFFFFFFFFDFBF9FFE4C5A7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFE9CEB5FFFFFFFFFFFFFFFFFFFFFFFFFFDAAE85FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF6EBE1FFFFFFFFFFFFFFFFFFF6EB
        E1FFEAD2BBFFE7CAAFFFEAD2BBFFF5EADFFFFFFFFFFFFFFFFFFFFBF6F1FFFDFA
        F7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE
        85FFEFDCCBFFFEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFEEDBC9FFDAAE85FFDDB58FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE0BB99FFEDD8C5FFF3E6D9FFF9F2
        EBFFF3E4D7FFEBD3BDFFE1BD9BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnClick = BtnFiltro1Click
    end
    object cbLM_DIAS2: TcxCheckBox
      Left = 16
      Top = 23
      Hint = ''
      Caption = 'D'#237'as cotizados'
      State = cbsChecked
      TabOrder = 0
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object cbLM_RETIRO: TcxCheckBox
      Left = 16
      Top = 73
      Hint = ''
      Caption = 'Retiro'
      TabOrder = 2
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object cbLM_CES_VEJ: TcxCheckBox
      Left = 144
      Top = 24
      Hint = ''
      Caption = 'Cesant'#237'a y Vejez'
      TabOrder = 3
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 104
    end
    object cbLM_INF_AMO: TcxCheckBox
      Left = 280
      Top = 48
      Hint = ''
      Caption = 'INFONAVIT Amortizaci'#243'n'
      TabOrder = 6
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 153
    end
    object cbLM_INF_PAT: TcxCheckBox
      Left = 280
      Top = 23
      Hint = ''
      Caption = 'INFONAVIT Patronal'
      TabOrder = 4
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 123
    end
    object cbLM_BASE2: TcxCheckBox
      Left = 16
      Top = 48
      Hint = ''
      Caption = 'Salario base'
      State = cbsChecked
      TabOrder = 1
      Transparent = True
      OnClick = cbLM_DIASClick
      Width = 97
    end
    object nMontos2: TZetaNumero
      Left = 511
      Top = 48
      Width = 41
      Height = 21
      Mascara = mnHoras
      TabOrder = 5
      Text = '0.00'
      UseEnterKey = True
      OnExit = nMontosExit
    end
  end
  inherited DataSource: TDataSource
    Left = 376
    Top = 304
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 19923448
  end
  inherited ActionList: TActionList
    Left = 466
    Top = 304
  end
  inherited PopupMenu1: TPopupMenu
    Left = 432
    Top = 304
  end
end
