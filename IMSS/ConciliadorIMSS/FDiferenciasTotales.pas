unit FDiferenciasTotales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, DB, cxDBData, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, StdCtrls, cxButtons, ExtCtrls,
  ActnList, ComCtrls, ZetaStateComboBox, System.Actions,
  ZetaRegistryServer, cxTextEdit, cxPCdxBarPopupMenu, cxPC;

type
  TDiferenciasTotales = class(TBaseGridLectura_DevEx)
    DESCRIPCION: TcxGridDBColumn;
    TRESS: TcxGridDBColumn;
    CD_IMSS: TcxGridDBColumn;
    DIFERENCIA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;

  public
    { Public declarations }
  end;


const
     K_NOMBRE_FORMA = 'datos de diferencias en totales';

var
  DiferenciasTotales: TDiferenciasTotales;

implementation


{$R *.dfm}


uses
     FTressShell, ZetaCommonClasses;


procedure TDiferenciasTotales.FormCreate(Sender: TObject);
begin
  inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontraron diferencias'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

      ZetaDBGridDBTableView.OptionsData.Editing := False;
      HelpContext := H_CONCIL_DIF_TOTAL;
 end;


procedure TDiferenciasTotales.FormShow(Sender: TObject);
begin
     inherited;
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

procedure TDiferenciasTotales.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsMontosTotales.DataSet;
end;


function TDiferenciasTotales.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TDiferenciasTotales.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TDiferenciasTotales.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TDiferenciasTotales.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TDiferenciasTotales.Refresh;
begin

end;


end.
