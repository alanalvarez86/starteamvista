unit FWizIMSSConciliarSUA;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FWizIMSSConciliarBase, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.Menus, cxClasses,
  cxShellBrowserDialog, Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Vcl.StdCtrls,
  ZetaKeyCombo, Vcl.Mask, ZetaNumero, cxTextEdit, cxButtons, cxMaskEdit,
  cxDropDownEdit, ZetaCXStateComboBox, DCliente, cxRadioGroup, ZcxBaseWizard,
  Vcl.PlatformDefaultStyleActnCtrls, System.Actions, Vcl.ActnList, Vcl.ActnMan,
  cxSpinEdit, cxProgressBar;

type
  TWizIMSSConciliarSUA = class(TWizIMSSConciliarBase)
    gbDatosSua: TcxGroupBox;
    cxLabel1: TcxLabel;
    btnBaseDatosSUA: TcxButton;
    BaseDatosSUA: TcxTextEdit;
    OpenDialog: TOpenDialog;

    procedure CargaParametros;override;
    procedure btnBaseDatosSUAClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);



  private
    { Private declarations }
  public
    { Public declarations }
    function EjecutarWizard: Boolean;override;

  end;
var
  WizIMSSConciliarSUA: TWizIMSSConciliarSUA;


implementation

{$R *.dfm}

uses
    ZetaRegistryCliente, ZetaCommonTools, ZetaDialogo, ZetaCommonClasses,
    FTressShell;

procedure TWizIMSSConciliarSUA.FormCreate(Sender: TObject);
begin
     inherited;

     try
        BaseDatosSUA.Text :=  ClientRegistry.ConciliadorIMSSBaseDatosSUA;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zInformation( '', 'Error al leer configuración de valores activos.', 0 );
           end;
     end;
     HelpContext := H_CONCIL_SUA;

end;



procedure TWizIMSSConciliarSUA.FormShow(Sender: TObject);
begin
  inherited;
     Advertencia.Caption := 'Al aplicar el mensaje se comparará la información que se tiene en Sistema TRESS ' +
                         'con la que se tiene en la base de datos de SUA, lo cual arrojará diferencias si hubiese.';

end;

procedure TWizIMSSConciliarSUA.btnBaseDatosSUAClick(Sender: TObject);
begin
     with OpenDialog do
     begin
          if ( BaseDatosSUA.Text = '' )  then
             InitialDir :=  VerificaDir( ExtractFilePath(Application.ExeName) )
          else
          begin
               FileName := BaseDatosSUA.Text;
               InitialDir := ExtractFilePath( BaseDatosSUA.Text );
          end;

          if Execute then
          begin
               try
                  if FileExists ( FileName ) then
                  begin
                       BaseDatosSUA.Text := FileName;
                  end
                  else
                      ZetaDialogo.zInformation( '', 'El archivo seleccionado no existe.', 0 );

               except
                     on Error: Exception do
                     begin
                          ZetaDialogo.zInformation( '', 'No se tienen los permisos requeridos para guardar esta configuración, firmarse con otro usuario.', 0 );
                     end;
               end;
          end;
     end;
end;

procedure TWizIMSSConciliarSUA.CargaParametros;
begin
     Descripciones.Clear;
     with Descripciones do
     begin
          AddString( 'Base de datos SUA', BaseDatosSUA.Text);
     end;
     inherited CargaParametros;
end;

function TWizIMSSConciliarSUA.EjecutarWizard: Boolean;
begin
     TressShell.SetProgressbar( Self.ProgressBar);
     TressShell.ConciliarProceso(Year.Value, Mes.Indice, Tipo.Indice, Patron.Llave, BaseDatosSUA.Text, tcSUA2006, GetEmision );
end;



procedure TWizIMSSConciliarSUA.WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
  var CanMove: Boolean);
begin
  inherited;
      with Wizard do
      begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
             begin
                  if ( BaseDatosSUA.Text = '') then
                  begin
                     CanMove := Error( 'El archivo de base de datos no puede quedar vacío', btnBaseDatosSUA)
                  end

             end;

     end;

end;



end.
