unit FIMSSVerificaImportarCreditosGridSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, Data.DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls,
  cxCalendar;

type
  TIMSSVerificaImportarCreditosGridSelect = class(TBasicoGridSelect_DevEx)
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  IMSSVerificaImportarCreditosGridSelect: TIMSSVerificaImportarCreditosGridSelect;

implementation

{$R *.dfm}

procedure TIMSSVerificaImportarCreditosGridSelect.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= False;
end;

procedure TIMSSVerificaImportarCreditosGridSelect.FormShow(Sender: TObject);
begin
     inherited;
     (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).Caption := 'N�mero';
     (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).MinWidth := 70;
end;

end.
