inherited EmpleadosNoRegistradosTRESS: TEmpleadosNoRegistradosTRESS
  Left = 0
  Top = 0
  Caption = 'Empleados no registrados en TRESS'
  ClientHeight = 300
  ClientWidth = 635
  OldCreateOrder = False
  ExplicitWidth = 635
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 635
    ExplicitWidth = 635
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 376
      ExplicitWidth = 376
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 370
        ExplicitLeft = 647
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 635
    Height = 281
    ParentFont = False
    ExplicitWidth = 635
    ExplicitHeight = 281
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.Editing = True
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 64
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = '# Seguro Social'
        DataBinding.FieldName = 'CB_SEGSOC'
        Width = 92
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
