unit FEmpleadosNoRegistradosTRESS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TEmpleadosNoRegistradosTRESS = class(TBaseGridLectura_DevEx)
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
   protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;

  public
    { Public declarations }
  end;

const
     K_NOMBRE_FORMA = 'empleados no registrados en TRESS';

var
  EmpleadosNoRegistradosTRESS: TEmpleadosNoRegistradosTRESS;

implementation

{$R *.dfm}

uses
     FTressShell, ZetaCommonClasses;


procedure TEmpleadosNoRegistradosTRESS.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsEmpleadosIMSS.DataSet;
end;


procedure TEmpleadosNoRegistradosTRESS.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n empleado no registrado en TRESS'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';
     ZetaDBGridDBTableView.OptionsData.Editing := False;
     HelpContext := H_CONCIL_EMP_NO_REG_TRESS
end;

procedure TEmpleadosNoRegistradosTRESS.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('PRETTYNAME'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

function TEmpleadosNoRegistradosTRESS.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosNoRegistradosTRESS.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosNoRegistradosTRESS.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosNoRegistradosTRESS.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TEmpleadosNoRegistradosTRESS.Refresh;
begin

end;
end.
