inherited DiferenciasDatosINFONAVIT: TDiferenciasDatosINFONAVIT
  Left = 0
  Top = 0
  Caption = 'Diferencias en datos INFONAVIT'
  ClientHeight = 300
  ClientWidth = 733
  OldCreateOrder = False
  ExplicitWidth = 733
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 733
    ExplicitWidth = 733
    inherited ValorActivo2: TPanel
      Width = 474
      ExplicitWidth = 474
      inherited textoValorActivo2: TLabel
        Width = 468
        ExplicitLeft = 388
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 115
    Width = 733
    Height = 185
    ParentFont = False
    ExplicitTop = 115
    ExplicitWidth = 733
    ExplicitHeight = 185
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.Editing = True
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        Options.Grouping = False
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Options.Grouping = False
        Width = 64
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = '# Seguro Social'
        DataBinding.FieldName = 'CB_SEGSOC'
        Options.Grouping = False
        Width = 64
      end
      object STATUS_EMP: TcxGridDBColumn
        Caption = 'Estatus'
        DataBinding.FieldName = 'STATUS_EMP'
        Width = 64
      end
      object DESCRIPCION: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'DESCRIPCION'
        Options.Grouping = False
        Width = 64
      end
      object TRESS: TcxGridDBColumn
        Caption = 'Tress'
        DataBinding.FieldName = 'TRESS'
        Options.Grouping = False
        Width = 64
      end
      object CD_IMSS: TcxGridDBColumn
        Caption = 'IMSS'
        DataBinding.FieldName = 'CD_IMSS'
        Options.Grouping = False
        Width = 64
      end
      object CB_FEC_ING: TcxGridDBColumn
        Caption = 'Fecha de Ingreso'
        DataBinding.FieldName = 'CB_FEC_ING'
        Options.Grouping = False
        Width = 64
      end
      object CB_FEC_BAJ: TcxGridDBColumn
        Caption = 'Fecha de Baja'
        DataBinding.FieldName = 'CB_FEC_BAJ'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
        Options.Grouping = False
        Width = 64
      end
    end
  end
  object gbFiltrosInfo: TcxGroupBox [2]
    Left = 0
    Top = 19
    Hint = ''
    Align = alTop
    Caption = 'Filtros'
    ParentFont = False
    TabOrder = 2
    Height = 96
    Width = 733
    object lblDiferencias: TcxLabel
      Left = 32
      Top = 26
      Hint = ''
      Caption = 'Diferencias en:'
      Transparent = True
    end
    object GroupBox4: TcxGroupBox
      Left = 302
      Top = 24
      Hint = ''
      TabOrder = 0
      Height = 57
      Width = 363
      object lblFechaFiltro: TcxLabel
        Left = 15
        Top = 20
        Hint = ''
        Caption = 'Otorgamiento del cr'#233'dito apartir del:'
        Transparent = True
      end
      object zFechaFiltro: TZetaFecha
        Left = 200
        Top = 17
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '09/mar/09'
        Valor = 39881.000000000000000000
        OnChange = cbDiferenciasInfonavitChange
      end
    end
    object lFiltrarPorInicio: TcxCheckBox
      Left = 315
      Top = 16
      Hint = ''
      Caption = ' Filtrar por fecha de Otorgamiento '
      ParentBackground = False
      ParentColor = False
      Style.Color = clWhite
      TabOrder = 1
      OnClick = cbDiferenciasInfonavitChange
      Width = 198
    end
    object cbDiferenciasInfonavit: TcxStateComboBox
      Left = 114
      Top = 24
      Hint = ''
      AutoSize = False
      Properties.DropDownRows = 5
      Properties.ImmediateDropDownWhenKeyPressed = False
      Properties.IncrementalSearch = False
      Properties.Items.Strings = (
        '<Todos>'
        'Inicio del Cr'#233'dito'
        '# de Cr'#233'dito Infonavit'
        'Tipo de Descuento'
        'Valor de Descuento')
      Properties.OnChange = cbDiferenciasInfonavitChange
      TabOrder = 3
      Text = '<Todos>'
      OnClick = cbDiferenciasInfonavitClick
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      EsconderVacios = False
      LlaveNumerica = True
      MaxItems = 10
      Offset = 0
      Height = 21
      Width = 142
    end
  end
  inherited DataSource: TDataSource
    Left = 64
    Top = 208
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 13631680
  end
  inherited ActionList: TActionList
    Left = 154
    Top = 208
  end
  inherited PopupMenu1: TPopupMenu
    Left = 120
    Top = 208
  end
end
