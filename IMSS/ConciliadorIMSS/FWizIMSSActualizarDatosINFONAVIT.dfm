inherited WizIMSSActualizarDatosINFONAVIT: TWizIMSSActualizarDatosINFONAVIT
  Left = 0
  Top = 0
  Caption = 'Actualizar datos INFONAVIT'
  ClientHeight = 401
  ClientWidth = 537
  Font.Name = 'Tahoma'
  OldCreateOrder = False
  ExplicitWidth = 543
  ExplicitHeight = 429
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 537
    Height = 401
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
      F8000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      72490000044D49444154484BB5957F4C94751CC7DFCFC181171994B3916158B6
      6A38CAC56866E88236EF39903B24F08E6389B58AD5DA12C82584F5073343ACB6
      DC989AB916A2F3D0FB09662A654B11B8030395CEC01F87C7DDF31CC715FF34DD
      6C4F9FEF038777C09A36FAE3B5E7FBE33EEFF7E7FBF97CEF792049D2FFCAAC8B
      73099C06357A4A347730F2D1F3120DE73468DE7295F0AB5CC55AF4BFFB12DC5B
      D230FCF583104F00821DF034AB70AD2105211B8DF73F801B071220B4026316DC
      95C14E97819768FD9AB3B060C5AF6FBE8C81CA67E1695CA00A9C440E1968BDA6
      38EDF09E0586A0954BF7EC4BBA27831D4C5C46AF915CEBF34EB9B72EC350FD63
      393E93EA5CA00D92D88AE3421BD6048E41126C9CE43D38EF1332E0EEC660FB94
      38D15D983F3A509DFEA4608E798D846F8F1E97C56F7A0FCD335EDD950CD1AC78
      87E6E70949B0284EF9ED5CD2BF19C86599A2442D75E416950FEE484D0E9EC46D
      96B960C305D182E784C331F0B7281344AA7FC086C5A21DE3010724BF1D074216
      28A20D8CB241439438D15DB0F64ADFDB59F70B96F8B372E676D920C76F05442B
      CA48B09FD6BE211333DB0B13B062CDF4134495254C275FB86B687B6A66E8170A
      B45120CBD0A4CCBAFE652ABCDF25BEC7EA1F291C8692688E34882ECB244E6AAE
      53A72DBFFE794A55F0A788602BD7E233C5AB7C9658080E2C22BEA01BF557A401
      21824498F89DDB42F59E1A33C8A043AB2D73372CFE38F46354B0243AD041E48B
      0E0EFEA3B1F0D9B82C5A1F0DEF93E14D3A01FF6994E00C035E3AAB2BA8BCBC73
      C9FA190661212B77DA6F8A5B48A503CDDBA7F66CE88453CF0F44094E870CBAD7
      E9DA07EB9E9E4F0D1EA78CC3C16EC225CFBFC72DE1A862698835DD861F28F309
      631BB7092EBD3ABBC7C0FFC9C49C7A7517196EA0B19E3010A5F49AE87016E5FE
      EDDA98BDD27744B971AC7D32D88A34D1CA61D48E1537F6252DF1B5C4B1ECD3A8
      B13E661A68C5A0AF150F83C41348E80A33203613725F18B4C79EEF13525751FE
      EFDD1B3263FD1645B35C2A2B774674280A84C34AB8ABD2E13BA4FA88FE1F4112
      A6D37192608ED58C5862C0441E2181612642E39A49D12903A296EDB1669F5BAB
      6DEC2D7F2171E4A0F2B3D019486374ABE81D54EFAE59F68CDF1CC3B296468F61
      C8DB34BFF0C2E64C5CAA593EC3A076523492ADB201DB2FE5A50EB5D6D155BA2A
      E56AFDE3AFF89AEFFB2A741A757FFC8C378227D044A5A9F39BF190F7DB44F46D
      5A096632DDA02A42384C65D840FE8D91F52A7FBC935F57DDFB7A76E6C096E519
      BF553CBFFAF2B6A7A87C4A886D80DF148FFE8A1771F1C38C0903C2C382A9C14D
      F45C44F364795DAF2673F5EE488330D47CA9FBD5FC5B5D3A5D90109C457915BD
      463506EB53E1D9B31017AB3370A976A2448F1223134172B09760861EBA61C44C
      F1D9E8D16BB6B98A73E1D4E9E02ACEA30F5316FACA5783353391E88FFCF17FA2
      44FD0101192A2D9D88C805CE97E9D0539A5740D9EEA793EC261AEF91BD3D0675
      7D6729FF0481E9CCFAA19E4B665D9C3B24FC03136AE714F220A9950000000049
      454E44AE426082}
    ExplicitWidth = 537
    ExplicitHeight = 401
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso actualiza el expediente de INFONAVIT con los datos ' +
        'encontrados del archivo de IMSS.'
      Header.Title = 'Actualizar datos de INFONAVIT'
      ExplicitWidth = 515
      ExplicitHeight = 261
      object lblBimestreInfo: TcxLabel
        Left = 91
        Top = 161
        Hint = ''
        Caption = 'Bimestre:'
        ParentFont = False
        Transparent = True
      end
      object lblYearInfo: TcxLabel
        Left = 305
        Top = 161
        Hint = ''
        Caption = 'A'#241'o:'
        ParentFont = False
        Transparent = True
      end
      object gbFiltrarFecha: TcxGroupBox
        Left = 93
        Top = 83
        Hint = ''
        TabOrder = 2
        Height = 57
        Width = 330
        object lblFechaFiltro: TcxLabel
          Left = 15
          Top = 20
          Hint = ''
          Caption = 'Otorgamiento del cr'#233'dito apartir del:'
          Transparent = True
        end
        object zFechaFiltro: TZetaFecha
          Left = 200
          Top = 17
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '09/mar/09'
          Valor = 39881.000000000000000000
          OnChange = cbDiferenciasInfonavitChange
        end
      end
      object lFiltrarPorInicio: TcxCheckBox
        Left = 118
        Top = 75
        Hint = ''
        Caption = ' Filtrar por fecha de otorgamiento '
        ParentBackground = False
        ParentColor = False
        Style.Color = clWhite
        TabOrder = 1
        OnClick = cbDiferenciasInfonavitChange
        Width = 198
      end
      object cbDiferenciasInfonavit: TcxStateComboBox
        Left = 173
        Top = 35
        Hint = ''
        AutoSize = False
        Properties.DropDownListStyle = lsFixedList
        Properties.DropDownRows = 5
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.IncrementalSearch = False
        Properties.Items.Strings = (
          '<Todos>'
          'Inicio del Cr'#233'dito'
          '# de Cr'#233'dito Infonavit'
          'Tipo de Descuento'
          'Valor de Descuento')
        TabOrder = 0
        Text = '<Todos>'
        OnClick = cbDiferenciasInfonavitChange
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        EsconderVacios = False
        LlaveNumerica = True
        MaxItems = 10
        Offset = 0
        Height = 21
        Width = 121
      end
      object lblDiferencias: TcxLabel
        Left = 91
        Top = 37
        Hint = ''
        Caption = 'Diferencias en:'
        Transparent = True
      end
      object YearInfo: TcxSpinEdit
        Left = 333
        Top = 159
        Hint = 'Seleccionar el a'#241'o'
        ParentShowHint = False
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taBottomJustify
        Properties.AssignedValues.DisplayFormat = True
        Properties.MaxValue = 2050.000000000000000000
        Properties.MinValue = 1899.000000000000000000
        Properties.SpinButtons.Position = sbpHorzLeftRight
        ShowHint = True
        TabOrder = 4
        Value = 1899
        Width = 90
      end
      object cbBimestre: TcxStateComboBox
        Left = 142
        Top = 159
        Hint = ''
        AutoSize = False
        Properties.DropDownListStyle = lsFixedList
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.IncrementalSearch = False
        TabOrder = 3
        ListaFija = lfBimestres
        ListaVariable = lvPuesto
        EsconderVacios = False
        LlaveNumerica = True
        MaxItems = 10
        Offset = 1
        Height = 21
        Width = 130
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 515
      ExplicitHeight = 261
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 515
        ExplicitHeight = 166
        Height = 166
        Width = 515
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 515
        Width = 515
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 447
          ExplicitHeight = 74
          Width = 447
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 64
    Top = 304
  end
end
