inherited EditMontosConceptos_DevEx: TEditMontosConceptos_DevEx
  Left = 376
  Top = 232
  Caption = 'Configuraci'#243'n de Montos SUA vs Conceptos de N'#243'mina'
  ClientHeight = 242
  ClientWidth = 455
  OnCreate = FormCreate
  ExplicitWidth = 461
  ExplicitHeight = 271
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 206
    Width = 455
    ExplicitTop = 206
    ExplicitWidth = 455
    inherited OK_DevEx: TcxButton
      Left = 280
      OnClick = OKClick
      ExplicitLeft = 280
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 360
      ExplicitLeft = 360
    end
  end
  object GbMensual: TGroupBox [1]
    Left = 8
    Top = 8
    Width = 433
    Height = 57
    Caption = 'Mensual'
    TabOrder = 1
    object Label1: TLabel
      Left = 41
      Top = 24
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = 'IMSS Subtotal:'
    end
    object IMSSSubtotalLookup: TZetaKeyLookup_DevEx
      Left = 120
      Top = 20
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
  end
  object GbBimestral: TGroupBox [2]
    Left = 8
    Top = 72
    Width = 433
    Height = 121
    Caption = 'Bimestral'
    TabOrder = 2
    object Label2: TLabel
      Left = 81
      Top = 23
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Retiro:'
    end
    object Label3: TLabel
      Left = 29
      Top = 45
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cesant'#237'a y Vejez:'
    end
    object Label4: TLabel
      Left = 26
      Top = 67
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = 'Infonavit Patronal:'
    end
    object Label5: TLabel
      Left = 49
      Top = 88
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Amortizaci'#243'n:'
    end
    object RetiroLookup: TZetaKeyLookup_DevEx
      Left = 120
      Top = 18
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
    object CesantiaYVejezLookup: TZetaKeyLookup_DevEx
      Left = 120
      Top = 40
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 60
    end
    object InfonavitPatronalLookup: TZetaKeyLookup_DevEx
      Left = 120
      Top = 62
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 2
      TabStop = True
      WidthLlave = 60
    end
    object AmortizacionLookup: TZetaKeyLookup_DevEx
      Left = 120
      Top = 84
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 3
      TabStop = True
      WidthLlave = 60
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
