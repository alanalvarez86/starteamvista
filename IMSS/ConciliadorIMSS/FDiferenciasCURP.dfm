inherited DiferenciasCURP: TDiferenciasCURP
  Left = 0
  Top = 0
  Caption = 'Diferencias en CURP'
  ClientHeight = 314
  ClientWidth = 834
  OldCreateOrder = False
  ExplicitWidth = 834
  ExplicitHeight = 314
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 834
    ExplicitWidth = 834
    inherited ValorActivo2: TPanel
      Width = 575
      ExplicitWidth = 575
      inherited textoValorActivo2: TLabel
        Width = 569
        ExplicitLeft = 489
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 834
    Height = 295
    ParentFont = False
    ExplicitTop = 22
    ExplicitWidth = 834
    ExplicitHeight = 295
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.CancelOnExit = True
      OptionsData.Editing = True
      OptionsSelection.HideSelection = True
      OptionsView.NoDataToDisplayInfoText = 'No se encontraron diferencias'
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 64
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = '# Seguro Social'
        DataBinding.FieldName = 'CB_SEGSOC'
        Width = 64
      end
      object TRESS: TcxGridDBColumn
        Caption = 'Tress'
        DataBinding.FieldName = 'TRESS'
        Width = 64
      end
      object CD_IMSS: TcxGridDBColumn
        Caption = 'IMSS'
        DataBinding.FieldName = 'CD_IMSS'
        Width = 64
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
