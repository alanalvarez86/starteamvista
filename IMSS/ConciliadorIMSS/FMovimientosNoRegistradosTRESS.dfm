inherited MovimientosNoRegistradosTRESS: TMovimientosNoRegistradosTRESS
  Left = 0
  Top = 0
  Caption = 'Movimientos no registrados en TRESS'
  ClientHeight = 300
  ClientWidth = 635
  OldCreateOrder = False
  ExplicitWidth = 635
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 635
    ExplicitWidth = 635
    inherited ValorActivo2: TPanel
      Width = 376
      ExplicitWidth = 376
      inherited textoValorActivo2: TLabel
        Width = 370
        ExplicitLeft = 290
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 635
    Height = 281
    ExplicitWidth = 635
    ExplicitHeight = 281
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'PRETTYNAME'
        Options.Grouping = False
        Width = 64
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = '# Seguro Social'
        DataBinding.FieldName = 'CB_SEGSOC'
        Options.Grouping = False
        Width = 64
      end
      object LM_TIPO_MOV: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'LM_TIPO_MOV'
      end
      object LM_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'LM_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyy'
        Options.Grouping = False
        Width = 64
      end
      object LM_BASE: TcxGridDBColumn
        Caption = 'Base'
        DataBinding.FieldName = 'LM_BASE'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
