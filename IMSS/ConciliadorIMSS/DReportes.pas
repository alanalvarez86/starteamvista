unit DReportes;

interface

uses
  SysUtils, Classes, DB, DBClient, ZetaClientDataSet, forms;

type
  TSQLAgenteClient = class(TObject);
  TdmReportes = class(TDataModule)
    cdsResultados: TZetaClientDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
    function EvaluaParametros( oSQLAgente : TSQLAgenteClient; Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;virtual;abstract;
    function GetPlantilla( const Plantilla: string ): TBlobField;
    function GetLogo( const Logo: string ): TBlobField;
    function DirectorioPlantillas: string;
  end;

var
  dmReportes: TdmReportes;

implementation

{$R *.dfm}



{ TdmReportes }

{Esta funciones estan aqui como DUMMY,
se tienen que agregar porque no hereda de DBaseReportes}
function TdmReportes.GetLogo(const Logo: string): TBlobField;
begin
     Result := NIL;
end;

function TdmReportes.GetPlantilla(const Plantilla: string): TBlobField;
begin
     Result := NIL;
end;

function TdmReportes.DirectorioPlantillas: string;
begin
     Result := '';
end;
end.
