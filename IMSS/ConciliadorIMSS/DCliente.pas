unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerConsultas,
     DServerGlobal,
     DServerRecursos,
     {$else}
     Catalogos_TLB,
     Consultas_TLB,
     Global_TLB,
     Recursos_TLB,
     {$endif}
     DBasicoCliente,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaClientDataSet, 
     {$IFDEF FROMEX}
     IniFiles, 
     ZetaMessages,
     {$ENDIF}
     ZetaServerDataSet;

var
   K_MAESTRA_INICIAL: integer;
const
     K_NUM_SS = 'CB_SEGSOC';
     K_TIPO_MOV = 'LM_TIPO_MOV';
     K_FOUND = 'EE_FOUND';
     K_ORIGEN = 'EE_ORIGEN';

     K_MAESTRA_EAMP_ALTA = 1;
     K_MAESTRA_EAMP_ALTA2 = 8;
     K_MAESTRA_EAMP_BAJA = 2;
     K_MAESTRA_EAMP_CAMBIO_COTIZACION = 7;
     K_MAESTRA_EAMP_RESTABLECIMIENTO = 3;
     K_MAESTRA_INICIAL_IMSS = 9;
     K_MAESTRA_INICIAL_SUA = 0;

{$IFDEF FROMEX}
     K_ARCHIVO_MONTOSCONCEPTOS = 'MontosConceptos.ini'; 
     K_IMSS_SUBTOTAL = 'ImssSubtotal';
     K_RETIRO = 'Retiro';
     K_CESANTIA_VLEJEZ = 'CesantiaYVejez';
     K_INFONAVIT_PATRONAL = 'InvonavitPatronal';
     K_AMORTIZACION = 'Amortizacion';
{$ENDIF}

type
  eEmisionClase = ( eeCMensual, eecBimestral );
  eScript = ( esPatronMensual,
              esPatronBimestral,
              esEmpleadoMensual,
              esEmpleadoBimestral,
              esMovimientoMensual,
              esMovimientoBimestral,
              esPatronIDMensual,
              esPatronIDBimestral,
              esPatron,
              esMovimientos,
              esAusentismo,
              esAusentismoPatron,
              esMovtosAusentismos,
              esPatronID,
              esMovimientoMensualIDSE,
              esMovimientoBimestralIDSE
			  {$IFDEF FROMEX}
			  ,esConceptosNomina
			  {$ENDIF} );

  eTipoConciliacion = ( tcSUA, tcSUA2006, tcCDImss, tcIDSE, tcNinguno );

  TdmCliente = class(TBasicoCliente)
    cdsRPatron: TZetaLookupDataSet;
    cdsLiqIMSS: TZetaClientDataSet;
    cdsLiqEmp: TZetaClientDataSet;
    cdsLiqMov: TZetaClientDataSet;
    cdsEmpleadoLookUp: TZetaLookupDataSet;
    cdsImportInfonavit: TServerDataSet;
    cdsProcesos: TZetaClientDataSet;
    cdsLogDetail: TZetaClientDataSet;
{$IFDEF FROMEX}
    cdsConceptos: TZetaLookupDataSet;
{$ENDIF}
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsRPatronLookupDescription(Sender: TZetaLookupDataSet; var sDescription: String);
    procedure cdsLiqMovAfterOpen(DataSet: TDataSet);
    procedure cdsLogDetailAfterOpen(DataSet: TDataSet);
    procedure cdsLogDetailAlModificar(Sender: TObject);
    procedure cdsProcesosAfterOpen(DataSet: TDataSet);
    procedure cdsProcesosAlCrearCampos(Sender: TObject);
    procedure cdsProcesosAlModificar(Sender: TObject);
    procedure cdsProcesosCalcFields(DataSet: TDataSet);
    procedure cdsRPatronGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);

    procedure cdsConceptosAlAdquirirDatos(Sender: TObject);

  private
    { Private declarations }

{$IFDEF FROMEX}
    FImagenIni : TIniFile;
    FImssSubtotal: String;
    FRetiro: String;
    FCesantiaYVejez: String;
    FInfonavitPatronal: String;
    FAmortizacion: String;
{$ENDIF}
    
    FDatosIMSS: TDatosIMSS;
    FClase: eEmisionClase;
    FConciliaSua: eTipoConciliacion;
    FDatosPeriodo: TDatosPeriodo;

    {$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerGlobal: TdmServerGlobal;
    FServerRecursos:TdmServerRecursos;
    {$else}
    FServerCatalogos: IdmServerCatalogosDisp;
    FServerConsultas: IdmServerConsultasDisp;
    FServerGlobal: IdmServerGlobalDisp;
    FserverRecursos:IdmServerRecursosDisp;

    function GetServerCatalogos: IdmServerCatalogosDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerGlobal: IdmServerGlobalDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    {$endif}

    function GetIMSSMes: Integer;
    function GetIMSSPatron: String;
    function GetIMSSRegistro: String;
    function GetIMSSTipo: eTipoLiqIMSS;
    function GetIMSSYear: Integer;
    procedure SetIMSSMes(const Value: Integer);
    procedure SetIMSSPatron(const Value: String);
    procedure SetIMSSTipo(const Value: eTipoLiqIMSS);
    procedure SetIMSSYear(const Value: Integer);
    procedure SetConciliaSua(const Value: eTipoConciliacion);
    procedure EscribeBitacora(Resultado: OleVariant; lMuestraResultado: Boolean = TRUE);
    function Check( const Resultado: OleVariant): Boolean;
    function CheckSilencioso( const Resultado: OleVariant): Boolean;
    procedure ConfigProcIdField(Campo: TField);
    procedure GetProcessText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    function GetStatusProceso: eProcessStatus;
    procedure LeerUnProceso(const iFolio: Integer);
{$IFDEF FROMEX}
    function GetMontosConceptosPath : String;
{$ENDIF}
  protected
    { Protected declarations }
  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerRecursos: TdmServerRecursos read FServerRecursos;
    {$else}
    property ServerCatalogos: IdmServerCatalogosDisp read GetServerCatalogos;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerGlobal: IdmServerGlobalDisp read GetServerGlobal;
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    {$endif}
    property IMSSPatron: String read GetIMSSPatron write SetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear write SetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo write SetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes write SetIMSSMes;
    property IMSSRegistro: String read GetIMSSRegistro;
    property Clase: eEmisionClase read FClase write FClase;
    property ConciliaSua : eTipoConciliacion read FConciliaSua write SetConciliaSua;
    property GetDatosPeriodoActivo: TDatosPeriodo read FDatosPeriodo;
    function FechaInicial: TDate;
    function ConectarLiquidaciones( const eClase: eEmisionClase; const lSUA: Boolean ): Boolean;
    function ConectarMovimientos( const eClase: eEmisionClase; const iEmpleado: TNumEmp ): Boolean;
    function GetSQLData( const sSQL: String ): OleVariant;
    procedure GetIMSSInicial;
    procedure OrdenarPor(Field: TField);
    function ChecaAccesoGrabaInfonavit: Boolean;
    procedure ActualizarInfonavit(const dFechaCambio: TDateTime; oDataSetInfonavit: TClientDataSet );
    procedure ImportarCreditosInfonavit(const dFechaCambio: TDateTime; const lIniciaFechaOtorga : boolean; const dFechaInicio : TDateTime; const sComentarios : string; oDataSetInfonavit: TClientDataSet );
    procedure GetProcessLog(const iProceso: Integer);
    procedure SuspenderInfonavit(const dFechaCambio: TDateTime; oDataSetInfonavit: TClientDataSet );
    procedure ReiniciarInfonavit(const dFechaCambio: TDateTime; oDataSetInfonavit: TClientDataSet );
    procedure GetEmpleadosBuscados_DevEx (const sPista: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    procedure AnaliticaContabilizarUso(const sModulo: String;const HelpContext, iConteo: Integer);
{$IFDEF FROMEX}
    property ImssSubtotal: String read FImssSubtotal write FImssSubtotal;
    property Retiro: String read FRetiro write FRetiro;
    property CesantiaYVejez: String read FCesantiaYVejez write FCesantiaYVejez;
    property InfonavitPatronal: String read FInfonavitPatronal write FInfonavitPatronal;
    property Amortizacion: String read FAmortizacion write FAmortizacion;

    procedure CargaMontosConceptos;
    procedure GuardaMontosConceptos;
{$ENDIF}
  end;

var
  dmCliente: TdmCliente;
  aEmisionClase: array[ eEmisionClase ] of ZetaPChar =('Mensual','Bimestral');
  aTipoConciliacion: array[ eTipoConciliacion ] of ZetaPChar =('SUA','SUA', 'IMSS', 'IMSS', '');

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaWizardFeedBack_DevEx,
     FProcessLogShow_DevEx,
     ZBaseThreads,
     FBaseEditBitacora_DevEx,
     FEditBitacora_DevEx,
     ZetaClientTools;

{$R *.DFM}

const
     K_M_OK = VACIO;
     K_M_ALTA = 'A';
     K_M_BAJA = 'B';
     K_M_MODIFICACION = 'MS';
     K_M_REINGRESO = 'R ';
     K_M_REINGRESO2 = 'R';
     K_MENSUAL = 'M';
     K_BIMESTRAL = 'B';

function GetScript( const eValor: eScript ): String;
begin
     case eValor of
          esPatronMensual: Result := 'select RP.TB_NUMREG, '+
                                     '( select RS.RT_PRIMA from PRIESGO RS where ( RS.TB_CODIGO = L.LS_PATRON ) and ( RS.RT_FECHA = ( select MAX( RS2.RT_FECHA ) from PRIESGO RS2 where ( RS2.TB_CODIGO = RS.TB_CODIGO ) and ( RS2.RT_FECHA <= ''%s'' ) ) ) ) as PRIMA_RT, '+
                                     'L.LS_NUM_TRA, '+
                                     'L.LS_EYM_FIJ, '+
                                     'L.LS_EYM_EXC, '+
                                     'L.LS_EYM_DIN, '+
                                     'L.LS_EYM_ESP, '+
                                     'L.LS_INV_VID, '+
                                     'L.LS_RIESGOS, '+
                                     'L.LS_GUARDER, '+
                                     'L.LS_DIAS_CO, '+
                                     'L.LS_ACT_AMO, '+
                                     'L.LS_ACT_APO, '+
                                     'L.LS_ACT_IMS, '+
                                     'L.LS_ACT_INF, '+
                                     'L.LS_ACT_RET, '+
                                     'L.LS_APO_VOL, '+
                                     'L.LS_CES_VEJ, '+
                                     'L.LS_DIAS_BM, '+
                                     'L.LS_FAC_ACT, '+
                                     'L.LS_FAC_REC, '+
                                     'L.LS_INF_ACR, '+
                                     'L.LS_INF_AMO, '+
                                     'L.LS_INF_NAC, '+
                                     'L.LS_INF_NUM, '+
                                     //'L.LS_INV_VID, '+
                                     'L.LS_REC_AMO, '+
                                     'L.LS_REC_APO, '+
                                     'L.LS_REC_IMS, '+
                                     'L.LS_REC_INF, '+
                                     'L.LS_REC_RET, '+
                                     'L.LS_RETIRO  '+
                                     //'L.LS_RIESGOS '+
                                     'from LIQ_IMSS L '+
                                     'left outer join RPATRON RP on ( RP.TB_CODIGO = L.LS_PATRON ) '+
                                     'where '+
                                     '( L.LS_PATRON = ''%s'' ) and '+
                                     '( L.LS_YEAR = %d ) and '+
                                     '( L.LS_MONTH = %d ) and '+
                                     '( L.LS_TIPO = %d )';
          esPatronBimestral: Result := 'select RP.TB_NUMREG, '+
                                       'L.LS_NUM_BIM, '+
                                       'L.LS_RETIRO, '+
                                       'L.LS_CES_VEJ, '+
                                       'L.LS_DIAS_BM, '+
                                       'L.LS_SUB_RET, '+
                                       'L.LS_INF_NUM, '+
                                       '( L.LS_INF_NAC + L.LS_INF_ACR ) as LS_INF_TOT, '+
                                       'L.LS_INF_AMO, '+
                                       'L.LS_SUB_INF, '+
                                       '( L.LS_SUB_RET + L.LS_SUB_INF ) as LS_TOTAL, '+
                                       'L.LS_ACT_AMO, '+
                                       'L.LS_ACT_APO, '+
                                       'L.LS_ACT_IMS, '+
                                       'L.LS_ACT_INF, '+
                                       'L.LS_ACT_RET, '+
                                       'L.LS_APO_VOL, '+
                                       //'L.LS_CES_VEJ, '+
                                       //'L.LS_DIAS_BM, '+
                                       'L.LS_FAC_ACT, '+
                                       'L.LS_FAC_REC, '+
                                       'L.LS_INF_ACR, '+
                                       //'L.LS_INF_AMO, '+
                                       'L.LS_INF_NAC, '+
                                       //'L.LS_INF_NUM, '+
                                       'L.LS_INV_VID, '+
                                       'L.LS_REC_AMO, '+
                                       'L.LS_REC_APO, '+
                                       'L.LS_REC_IMS, '+
                                       'L.LS_REC_INF, '+
                                       'L.LS_REC_RET, '+
                                       'L.LS_RIESGOS '+

                                       'from LIQ_IMSS L '+
                                       'left outer join RPATRON RP on ( RP.TB_CODIGO = L.LS_PATRON ) '+
                                       'where '+
                                       '( L.LS_PATRON = ''%s'' ) and '+
                                       '( L.LS_YEAR = %d ) and '+
                                       '( L.LS_MONTH = %d ) and '+
                                       '( L.LS_TIPO = %d )';
          esEmpleadoMensual: Result := 'select '+
                                       'L.CB_CODIGO, '+
                                       '0 as ' + K_FOUND + ', '+
                                       'C.CB_SEGSOC as ' + K_NUM_SS + ', '+
                                       'P.TB_MODULO, '+
                                       'C.CB_APE_PAT, '+
                                       'C.CB_APE_MAT, '+
                                       'C.CB_NOMBRES, '+
                                       'T.TU_TIP_JOR -1 as TU_TIP_JOR, '+
                                       'C.CB_INF_INI, '+
                                       'C.CB_CURP, '+
                                       'C.CB_RFC,' +
                                       'L.LE_EYM_FIJ,'+
                                       'L.LE_EYM_EXC,'+
                                       'L.LE_EYM_DIN,'+
                                       'L.LE_EYM_ESP,'+
                                       'L.LE_RIESGOS,'+
                                       'L.LE_INV_VID,'+
                                       'L.LE_GUARDER,'+
                                       'L.LE_DIAS_CO, '+
                                       // SOP 4527
                                       // Aportaciones voluntarias se calculan mensualmente
                                       'L.LE_APO_VOL '+
                                       // ----- -----
                                       'from LIQ_EMP L '+
                                       'left outer join COLABORA C on ( C.CB_CODIGO = L.CB_CODIGO ) '+
                                       'left outer join RPATRON P on ( P.TB_CODIGO = L.LS_PATRON ) '+
                                       'left outer join TURNO T on ( T.TU_CODIGO = C.CB_TURNO ) '+
                                       'where '+
                                       '( L.LS_PATRON = ''%s'' ) and '+
                                       '( L.LS_YEAR = %d ) and '+
                                       '( L.LS_MONTH = %d ) and '+
                                       '( L.LS_TIPO = %d ) '+
                                       //'AND ( L.CB_CODIGO < 1000 ) '+
                                       'order by 2';
          esEmpleadoBimestral: Result := 'select '+
                                         'L.CB_CODIGO, '+
                                         '0 as ' + K_FOUND + ', '+
                                         'C.CB_SEGSOC as ' + K_NUM_SS + ', '+
                                         'P.TB_MODULO, '+
                                         'C.CB_APE_PAT, '+    { GetNombreSUA( COLABORA.CB_APE_PAT ) + '$' + GetNombreSUA( COLABORA.CB_APE_MAT ) + '$' + GetNombreSUA( COLABORA.CB_NOMBRES ) }
                                         'C.CB_APE_MAT, '+
                                         'C.CB_NOMBRES, '+
                                         'T.TU_TIP_JOR - 1 as TU_TIP_JOR, '+    { TURNO.TU_TIP_JOR - 1 where ( TURNO.TU_CODIGO = COLABORA.CB_TURNO ) }
                                         'C.CB_CURP, '+
                                         'C.CB_RFC,' +
                                         'K.CB_INFCRED, '+
                                         'K.CB_INFTIPO, '+
                                         'K.CB_INFTASA, '+
                                         'K.CB_INFDISM, '+
                                         '%0:s, '+       { ZetaCommonTools.FechaToStr() = COLABORA.CB_INF_INI }
                                         'L.LE_RETIRO,' +
                                         'L.LE_CES_VEJ,'+
                                         // SOP 4527
                                         // Aportaciones voluntarias se calculan mensualmente
                                         // 'L.LE_APO_VOL,'+
                                         // ----- -----
                                         'L.LE_INF_PAT,'+
                                         'L.LE_INF_AMO,'+
                                         'L.LE_DIAS_BM, '+
                                         'C.CB_INFACT,'+
                                         'C.CB_ACTIVO, '+
                                         'K.KI_FECHA, '+
                                         'K.KI_TIPO, ' +
                                         'C.CB_FEC_ING, ' +
                                         'C.CB_FEC_BAJ, ' +
                                         'C.CB_INFCRED as CB_INFCRED_ACTUAL, ' +
                                         'C.CB_INF_INI as CB_INF_INI_ACTUAL ' +
                                         'from LIQ_EMP L '+
                                         'left outer join COLABORA C on ( C.CB_CODIGO = L.CB_CODIGO ) '+
                                         'left outer JOIN KARINF K on ( K.CB_CODIGO = L.CB_CODIGO ) and ( K.KI_FECHA = ( select ' +
                                         {$ifdef MSSSQL}
                                         ' top 1 KI_FECHA '+
                                         {$else}
                                         ' MAX(KI_FECHA) ' +
                                         {$endif}
                                         'from KARINF KAR where KAR.KI_FECHA <=%1:s  AND KAR.CB_CODIGO = K.CB_CODIGO ) ) ' +
                                         'and ( K.KI_TIPO <> %7:d OR K.KI_FECHA between %2:s and %1:s ) ' +
                                         'left outer join RPATRON P on ( P.TB_CODIGO = L.LS_PATRON ) '+
                                         'left outer join TURNO T on ( T.TU_CODIGO = C.CB_TURNO ) '+
                                         'where '+
                                         '( L.LS_PATRON = ''%3:s'' ) and '+
                                         '( L.LS_YEAR = %4:d ) and '+
                                         '( L.LS_MONTH = %5:d ) and '+
                                         '( L.LS_TIPO = %6:d ) '+
                                         'order by 2';
          esMovimientoMensual: Result := 'select '+
                                         '0 as ' + K_ORIGEN + ', '+
                                         'L.LM_FECHA, '+
                                         'L.LM_CLAVE, '+
                                         '0 as ' + K_TIPO_MOV + ', ' +
                                         'L.LM_DIAS, '+
                                         'L.LM_BASE, '+
                                         'L.LM_EYM_FIJ, '+
                                         'L.LM_EYM_EXC, '+
                                         'L.LM_EYM_DIN, '+
                                         'L.LM_EYM_ESP, '+
                                         'L.LM_INV_VID, '+
                                         'L.LM_RIESGOS, '+
                                         'L.LM_GUARDER, '+
                                         'L.LM_AUSENCI, '+
                                         'L.LM_INCAPAC, '+
                                         'L.LM_INF_PAT, '+
                                         'L.LM_INF_AMO, '+
                                         'L.LM_EYMEXCP, '+
                                         'L.LM_EYMDINP, '+
                                         'L.LM_EYMESPP, '+
                                         'L.LM_INVVIDP, '+
                                         '( select K.CB_TIPO from KARDEX K '+
                                         'where K.CB_CODIGO = L.CB_CODIGO '+
                                         'and K.CB_FECHA = L.LM_FECHA '+
                                         'and K.CB_TIPO = ''CAMBIO'' '+
                                         'and K.CB_FEC_INT < K.CB_FECHA ) MOV_KARDEX '+

                                         'from LIQ_MOV L '+
                                         'where '+
                                         '( L.LS_PATRON = ''%s'' ) and '+
                                         '( L.LS_YEAR = %d ) and '+
                                         '( L.LS_MONTH = %d ) and '+
                                         '( L.LS_TIPO = %d ) and '+
                                         Format( '( L.LM_TIPO = ''%s'' ) and ', [ K_MENSUAL ] ) +
                                         '( L.CB_CODIGO = %d ) '+
                                         'order by L.LM_FECHA, L.LM_CLAVE';
          esMovimientoMensualIdse: Result := 'select '+
                                         '0 as ' + K_ORIGEN + ', '+
                                         'L.LM_FECHA, '+
                                         'L.LM_CLAVE, '+
                                         '0 as ' + K_TIPO_MOV + ', ' +
                                         'L.LM_DIAS - L.LM_AUSENCI - L.LM_INCAPAC LM_DIAS, '+
                                         'L.LM_BASE, '+
                                         'L.LM_EYM_FIJ, '+
                                         'L.LM_EYM_EXC, '+
                                         'L.LM_EYM_DIN, '+
                                         'L.LM_EYM_ESP, '+
                                         'L.LM_INV_VID, '+
                                         'L.LM_RIESGOS, '+
                                         'L.LM_GUARDER, '+
                                         'L.LM_AUSENCI, '+
                                         'L.LM_INCAPAC, '+
                                         'L.LM_INF_PAT, '+
                                         'L.LM_INF_AMO, '+
                                         'L.LM_EYMEXCP, '+
                                         'L.LM_EYMDINP, '+
                                         'L.LM_EYMESPP, '+
                                         'L.LM_INVVIDP, '+
                                         '( select K.CB_TIPO from KARDEX K '+
                                         'where K.CB_CODIGO = L.CB_CODIGO '+
                                         'and K.CB_FECHA = L.LM_FECHA '+
                                         'and K.CB_TIPO = ''CAMBIO'' '+
                                         'and K.CB_FEC_INT < K.CB_FECHA ) MOV_KARDEX '+

                                         'from LIQ_MOV L '+
                                         'where '+
                                         '( L.LS_PATRON = ''%s'' ) and '+
                                         '( L.LS_YEAR = %d ) and '+
                                         '( L.LS_MONTH = %d ) and '+
                                         '( L.LS_TIPO = %d ) and '+
                                         Format( '( L.LM_TIPO = ''%s'' ) and ', [ K_MENSUAL ] ) +
                                         '( L.CB_CODIGO = %d ) '+
                                         'order by L.LM_FECHA, L.LM_CLAVE';
          esMovimientoBimestral: Result := 'select '+
                                           '0 as ' + K_ORIGEN + ', '+
                                           'L.LM_FECHA, '+
                                           'L.LM_CLAVE, '+
                                           '0 as ' + K_TIPO_MOV + ', ' +
                                           'L.LM_DIAS, '+
                                           'L.LM_BASE, '+
                                           'L.LM_RETIRO, '+
                                           'L.LM_CES_VEJ, '+
                                           'L.LM_CESVEJP, '+
                                           'L.LM_INF_PAT, '+
                                           'L.LM_INF_AMO, '+
                                           'L.LM_AUSENCI, '+
                                           'L.LM_INCAPAC, '+
                                          // 'L.LM_INF_PAT, '+
                                          // 'L.LM_INF_AMO, '+
                                           'L.LM_EYM_FIJ,  '+
                                           '( select K.CB_TIPO from KARDEX K '+
                                           'where K.CB_CODIGO = L.CB_CODIGO '+
                                           'and K.CB_FECHA = L.LM_FECHA '+
                                           'and K.CB_TIPO = ''CAMBIO'' '+
                                           'and K.CB_FEC_INT < K.CB_FECHA ) MOV_KARDEX '+
                                           'from LIQ_MOV L '+
                                           'where '+
                                           '( L.LS_PATRON = ''%s'' ) and '+
                                           '( L.LS_YEAR = %d ) and '+
                                           '( L.LS_MONTH = %d ) and '+
                                           '( L.LS_TIPO = %d ) and '+
                                           Format( '( L.LM_TIPO = ''%s'' ) and ', [ K_BIMESTRAL ] ) +
                                           '( L.CB_CODIGO = %d ) '+
                                           'order by L.LM_FECHA, L.LM_CLAVE';
          esMovimientoBimestralIDSE: Result := 'select '+
                                           '0 as ' + K_ORIGEN + ', '+
                                           'L.LM_FECHA, '+
                                           'L.LM_CLAVE, '+
                                           '0 as ' + K_TIPO_MOV + ', ' +
                                           'L.LM_DIAS - L.LM_AUSENCI - L.LM_INCAPAC LM_DIAS, '+
                                           'L.LM_BASE, '+
                                           'L.LM_RETIRO, '+
                                           'L.LM_CES_VEJ, '+
                                           'L.LM_CESVEJP, '+
                                           'L.LM_INF_PAT, '+
                                           'L.LM_INF_AMO, '+
                                           'L.LM_AUSENCI, '+
                                           'L.LM_INCAPAC, '+
                                          // 'L.LM_INF_PAT, '+
                                          // 'L.LM_INF_AMO, '+
                                           'L.LM_EYM_FIJ,  '+
                                           '( select K.CB_TIPO from KARDEX K '+
                                           'where K.CB_CODIGO = L.CB_CODIGO '+
                                           'and K.CB_FECHA = L.LM_FECHA '+
                                           'and K.CB_TIPO = ''CAMBIO'' '+
                                           'and K.CB_FEC_INT < K.CB_FECHA ) MOV_KARDEX '+
                                           'from LIQ_MOV L '+
                                           'where '+
                                           '( L.LS_PATRON = ''%s'' ) and '+
                                           '( L.LS_YEAR = %d ) and '+
                                           '( L.LS_MONTH = %d ) and '+
                                           '( L.LS_TIPO = %d ) and '+
                                           Format( '( L.LM_TIPO = ''%s'' ) and ', [ K_BIMESTRAL ] ) +
                                           '( L.CB_CODIGO = %d ) '+
                                           'order by L.LM_FECHA, L.LM_CLAVE';
{$IFDEF FROMEX}
          esConceptosNomina : Result := 'SELECT CO_ACTIVO, CO_DESCRIP, CO_NUMERO ' +
                                        'FROM CONCEPTO ' +
                                        'WHERE (CO_ACTIVO = ''S'')';
{$ENDIF}
     else
         Result := VACIO;
     end;
end;

{ ********* TdmCliente ********** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( self );
     FServerRecursos := TdmServerRecursos.Create( self );
     {$endif}
     with FDatosIMSS do
     begin
          Patron := VACIO;
          Mes := ZetaCommonTools.TheMonth( Date );
          Tipo := tlOrdinaria;
          Year := ZetaCommonTools.TheYear( Date );
     end;
     inherited;

     {cdsLiqImss.LogChanges := FALSE;
     cdsLiqEmp.LogChanges := FALSE;
     cdsLiqMov.LogChanges := FALSE;}
{$IFDEF FROMEX}
     FImagenIni := TIniFile.Create( GetMontosConceptosPath );
{$ENDIF}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
{$IFDEF FROMEX}
     FreeAndNil( FImagenIni );
{$ENDIF}
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerRecursos );
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerGlobal );
     {$endif}
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( CreaServidor( CLASS_dmServerCatalogos, FServerCatalogos ) );
end;

function TdmCliente.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;

function TdmCliente.GetServerGlobal: IdmServerGlobalDisp;
begin
     Result := IdmServerGlobalDisp( CreaServidor( CLASS_dmServerGlobal, FServerGlobal ) );
end;

function TdmCliente.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result := IdmServerRecursosDisp( CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;
{$endif}

{ ******** Valores Activos del IMSS ********** }

procedure TdmCliente.GetIMSSInicial;
begin
     with FDatosIMSS do
     begin
          with cdsRPatron do
          begin
               Data := ServerCatalogos.GetRPatron( Empresa );
               if IsEmpty then
                  Patron := ''
               else
                   Patron := FieldByName( 'TB_CODIGO' ).AsString;
          end;
          Year := ZetaCommonTools.TheYear( Now );
          Tipo := tlOrdinaria;
          Mes := ZetaCommonTools.TheMonth( Date ) - 1;
          if ( Mes = 0 ) then
          begin
               Mes := 12;
               Year := Year - 1;
          end;
     end;
end;

procedure TdmCliente.cdsRPatronLookupDescription( Sender: TZetaLookupDataSet; var sDescription: String);
begin
     inherited;
     with Sender do
     begin
          sDescription := Format( '%s ( %s )', [ FieldByName( 'TB_ELEMENT' ).AsString, FieldByName( 'TB_NUMREG' ).AsString ] );
     end;
end;

procedure TdmCliente.cdsLiqMovAfterOpen(DataSet: TDataSet);
const K_CAMBIO = 'CAMBIO';
var
   sTipo: String;
   iTipo: integer;
begin
     inherited;
     with Dataset do
     begin
          while not Eof do
          begin
               Edit;
               try
                  sTipo := FieldByName( 'LM_CLAVE' ).AsString;
                  if ( sTipo = K_M_ALTA ) then
                     iTipo := K_MAESTRA_EAMP_ALTA
                  else
                  if ( sTipo = K_M_BAJA ) then
                     iTipo := K_MAESTRA_EAMP_BAJA
                  else
                  if ( sTipo = K_M_MODIFICACION ) then
                     iTipo := K_MAESTRA_EAMP_CAMBIO_COTIZACION
                  else
                  if ( sTipo = K_M_REINGRESO ) or ( sTipo = K_M_REINGRESO2 ) then
                     //iTipo := K_MAESTRA_EAMP_RESTABLECIMIENTO
                     iTipo := K_MAESTRA_EAMP_ALTA2
                  else
                      iTipo := K_MAESTRA_INICIAL;
                  FieldByName( K_TIPO_MOV ).AsInteger := iTipo;

                  if ( iTipo = K_MAESTRA_INICIAL ) {and ( Clase = eeCMensual )} then
                     FieldByName('LM_FECHA').AsDateTime := FechaInicial;

                  //if NOT ConciliaSua then
                  begin
                       if ( iTipo = K_MAESTRA_EAMP_CAMBIO_COTIZACION ) and (FieldByName('MOV_KARDEX').AsString = K_CAMBIO) then
                          FieldByName( K_TIPO_MOV ).AsInteger := K_MAESTRA_INICIAL;
                  end;

                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          First;
     end;
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := FDatosIMSS.Patron;
end;

procedure TdmCliente.SetIMSSPatron(const Value: String);
begin
     FDatosIMSS.Patron := Value;
end;

function TdmCliente.GetIMSSRegistro: String;
begin
     with cdsRPatron do
     begin
          if ( FieldByName( 'TB_CODIGO' ).AsString = FDatosIMSS.Patron ) or Locate( 'TB_CODIGO', FDatosIMSS.Patron, [] ) then
          begin
               Result := FieldByName( 'TB_NUMREG' ).AsString;
          end
          else
              Result := VACIO;
     end;
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := FDatosIMSS.Mes;
end;

procedure TdmCliente.SetIMSSMes(const Value: Integer);
begin
     FDatosIMSS.Mes := Value;
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := FDatosIMSS.Tipo;
end;

procedure TdmCliente.SetIMSSTipo(const Value: eTipoLiqIMSS);
begin
     FDatosIMSS.Tipo := Value;
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := FDatosIMSS.Year;
end;

procedure TdmCliente.SetIMSSYear(const Value: Integer);
begin
     FDatosIMSS.Year := Value;
end;

function TdmCliente.FechaInicial: TDate;
begin
     if ( Clase = eeCMensual ) then
        Result := EncodeDate( ImssYear, ImssMes, 1 )
     else
         Result := EncodeDate( ImssYear, ImssMes - 1, 1 )
end;


{ ******** Llamadas a Servidores ********** }

function TdmCliente.GetSQLData(const sSQL: String): OleVariant;
begin
     Result := ServerConsultas.GetQueryGralTodos( Empresa, sSQL );
end;

function TdmCliente.ConectarLiquidaciones( const eClase: eEmisionClase; const lSUA: Boolean ): Boolean;
const
     aCampoInicio: array[FALSE..TRUE] of PChar = ('K.CB_INF_ANT CB_INF_INI', 'C.CB_INF_INI');
var
   sLastDay: String;
begin
     try
        case eClase of
             eeCMensual:
             begin
                  sLastDay := ZetaCommonTools.FechaAsStr( ZetaCommonTools.LastDayOfMonth( EncodeDate( IMSSYear, IMSSMes, 1 ) ) );
                  cdsLiqIMSS.Data := GetSQLData( Format( GetScript( esPatronMensual ), [ sLastDay, IMSSPatron, IMSSYear, IMSSMes, Ord( IMSSTipo ) ] ) );
                  cdsLiqEmp.Data := GetSQLData( Format( GetScript( esEmpleadoMensual ), [ IMSSPatron, IMSSYear, IMSSMes, Ord( IMSSTipo ) ] ) );
             end;
             eecBimestral:
             begin
                  if lSUA then
                     sLastDay:=  FechaAsStr( LastDayOfBimestre( EncodeDate( IMSSYear, IMSSMes, 1 )  ) )
                  else
                      sLastDay:=  FechaAsStr( LastDayOfBimestre( EncodeDate( IMSSYear, IMSSMes, 1 ) ) + 1 );

                  cdsLiqIMSS.Data := GetSQLData( Format( GetScript( esPatronBimestral ), [ IMSSPatron, IMSSYear, IMSSMes, Ord( IMSSTipo ) ] ) );
                  cdsLiqEmp.Data := GetSQLData( Format( GetScript( esEmpleadoBimestral ), [ aCampoInicio[lSUA],
                                                                                            EntreComillas(sLastDay),
                                                                                            EntreComillas( FechaAsStr( FirstDayOfBimestre( EncodeDate( IMSSYear, IMSSMes, 1 )  ) ) ),
                                                                                            IMSSPatron, IMSSYear, IMSSMes, Ord( IMSSTipo ), Ord(infoSuspen) ] ) );
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', 'Error Al Leer Liquidaciones de Tress', Error, 0 );
                Result := False;
           end;
     end;
end;

function TdmCliente.ConectarMovimientos( const eClase: eEmisionClase; const iEmpleado: TNumEmp ): Boolean;
var
   eSQL: eScript;
begin
     try
        case eClase of
             eeCMensual:
             begin
                  if FConciliaSua =tcIDSE then  eSQL := esMovimientoMensualIdse
                  else
                      eSQL := esMovimientoMensual;
             end
             else
             begin
                  if FConciliaSua =tcIDSE then  eSQL := esMovimientoBimestralIdse
                  else
                      eSQL := esMovimientoBimestral;
             end;
        end;
        cdsLiqMov.Data := GetSQLData( Format( GetScript( eSQL ), [ IMSSPatron, IMSSYear, IMSSMes, Ord( IMSSTipo ), iEmpleado ] ) );
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Leer Movimientos Del Empleado %d', [ iEmpleado ] ), Error, 0 );
                Result := False;
           end;
     end;
end;

procedure TdmCliente.SetConciliaSua(const Value: eTipoConciliacion);
begin
     FConciliaSua := Value;
     if FConciliaSua in [ tcSUA, tcSUA2006] then
        K_MAESTRA_INICIAL := K_MAESTRA_INICIAL_SUA
     else
         K_MAESTRA_INICIAL := K_MAESTRA_INICIAL_IMSS;
end;

procedure TdmCliente.OrdenarPor( Field: TField );
const
     aPosOrden: array[ FALSE..TRUE ] of PChar = ( 'DESC', 'ASC' );
     aOpcOrden: array[ FALSE..TRUE ] of TIndexOptions = ( [ixCaseInsensitive, ixDescending ], [ixCaseInsensitive] );
var
   sNombreIndice: String;
   iPosIndice: Integer;
   lAscendente: Boolean;
begin
     lAscendente := Field.Tag = 0;
     if Field.Tag = 0 then Field.Tag := 1
     else Field.Tag := 0;

     with TClientDataSet( Field.Dataset ) do
     begin
          DisableControls;
          try
             sNombreIndice := Field.FieldName + aPosOrden[ lAscendente ];
             IndexDefs.Update;
             iPosIndice := IndexDefs.IndexOf( sNombreIndice );
             if ( iPosIndice >= 0 ) then
                IndexName := IndexDefs.Items[iPosIndice].Name
             else
             begin
                  AddIndex( sNombreIndice, Field.FieldName, aOpcOrden[lAscendente], VACIO, VACIO, 0 );

                  IndexName := sNombreIndice;
             end;
          finally
                 First;
                 EnableControls;
          end;
     end;
end;

function TdmCliente.ChecaAccesoGrabaInfonavit: Boolean;
begin
     Result:= ZAccesosMgr.CheckDerecho( D_EMP_EXP_INFONAVIT, K_DERECHO_ALTA ) and
              ZAccesosMgr.CheckDerecho( D_EMP_EXP_INFONAVIT, K_DERECHO_CAMBIO );
end;

procedure TdmCliente.ActualizarInfonavit(const dFechaCambio: TDateTime; oDataSetInfonavit: TClientDataSet );
var
   Parametros :TZetaParams;
   iEmpleado: Integer;
   TipoDiferencia: eTipoDifInfonavit;
   lEmpleadoDiferente: Boolean;

     procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
     begin
           // Agrega Campo al DataSet
           with oSDataSet do
           begin
                case eTipo of
                     tgBooleano : AddBooleanField( sCampo );
                     tgFloat : AddFloatField( sCampo );
                     tgNumero : AddIntegerField( sCampo );
                     tgFecha : AddDateField( sCampo );
                else
                     AddStringField( sCampo, iLongitud );
                end;
           end;
     end;

     procedure LimpiaEstructuraCreditos;
     begin
          cdsImportInfonavit.Close;
          cdsImportInfonavit.Fields.Clear;
          cdsImportInfonavit.FieldDefs.Clear;
     end;


     procedure DefineEstructuraCreditos;
     begin
          LimpiaEstructuraCreditos;

          with cdsImportInfonavit do
          begin
               if ( FieldCount <= 0  ) then
               begin
                    AgregaColumna( 'CB_CODIGO', tgNumero, 9, cdsImportInfonavit );
                    AgregaColumna( 'TIPO_MOVIMIENTO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
                    AgregaColumna( 'FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
                    AgregaColumna( 'CB_INFCRED', tgTexto, 10, cdsImportInfonavit );
                    AgregaColumna( 'CB_INFTIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
                    AgregaColumna( 'CB_INFTASA', tgFloat, K_ANCHO_PESOS, cdsImportInfonavit );
                    AgregaColumna( 'CB_INFDISM', tgTexto, 2, cdsImportInfonavit );
                    AgregaColumna( 'CB_INF_ANT', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
                    AgregaColumna( 'DESCRIPCION', tgTexto, K_ANCHO_OBSERVACIONES, cdsImportInfonavit );
               end;
               if not Active then
               begin
                    CreateDataSet;
                    Open;
               end;
               EmptyDataSet;

          end;
     end;

     procedure InicializaDatos;
     begin
          //Los primeros datos que se capturan
          with cdsImportInfonavit do
          begin
               Append;
               FieldByName('TIPO_MOVIMIENTO').AsInteger:= Ord(TipoDiferencia);
               FieldByName('CB_CODIGO').AsInteger:= oDataSetInfonavit.FieldByName('CB_CODIGO').AsInteger;
               FieldByName('DESCRIPCION').AsString:= 'Importaci�n de Datos de Infonavit';
               FieldByName('FECHA').AsDateTime:= dFechaCambio;
          end;
     end;


begin
     Parametros := TZetaParams.Create( Self );
     try
        DefineEstructuraCreditos;

        with Parametros do
        begin
             AddDate('FechaCambio', dFechaCambio );
             AddInteger( 'Formato', Ord(faASCIIDel) );
             AddInteger( 'FormatoImpFecha',Ord( ifDDMMYYYYs));
             AddString( 'Archivo', 'Concilia Datos Infonavit' );
             AddMemo( 'Observaciones', 'Importaci�n Datos Infonavit' );
        end;
        iEmpleado:= 0;
        with cdsImportInfonavit do
        begin
             oDataSetInfonavit.IndexFieldNames:= 'CB_CODIGO;DIFERENCIA';
             oDataSetInfonavit.First;

             while not oDataSetInfonavit.Eof do   //Se asume que los tipos de movimientos vienen ordenados
             begin
                  TipoDiferencia:= eTipoDifInfonavit( oDataSetInfonavit.FieldByName('DIFERENCIA').AsInteger );
                  lEmpleadoDiferente:= ( iEmpleado <> oDataSetInfonavit.FieldByName('CB_CODIGO').AsInteger );

                  if ( ( lEmpleadoDiferente ) or ( TipoDiferencia = DifInfOtorgamiento )  ) and
                     ( State = dsInsert ) then
                       Post;

                  if ( TipoDiferencia = DifInfNoCredito ) then
                  begin
                       InicializaDatos;
                       cdsImportInfonavit.FieldByName('CB_INFCRED').AsString:= oDataSetInfonavit.FieldByName('CD_IMSS').AsString;
                  end
                  else if ( TipoDiferencia = DifInfTipoDescuento ) then
                  begin
                       if ( lEmpleadoDiferente ) then
                            InicializaDatos;
                       cdsImportInfonavit.FieldByName('CB_INFTIPO').AsInteger:= StrAsInteger(oDataSetInfonavit.FieldByName('CD_IMSS').AsString);
                  end
                  else if ( TipoDiferencia = DifInfValorDesc ) then
                  begin
                       if ( lEmpleadoDiferente ) then
                            InicializaDatos;

                       cdsImportInfonavit.FieldByName('CB_INFTASA').AsFloat:= StrToReal(oDataSetInfonavit.FieldByName('CD_IMSS').AsString);
                  end
                  else if ( TipoDiferencia = DifInfOtorgamiento ) then
                  begin
                       InicializaDatos;
                       cdsImportInfonavit.FieldByName('FECHA').AsDateTime:= oDataSetInfonavit.FieldByName('CB_INF_INI').AsDateTime;
                  end;

                  iEmpleado:= oDataSetInfonavit.FieldByName('CB_CODIGO').AsInteger;
                  oDataSetInfonavit.Next;
             end;

             if ( State = dsInsert ) then //Ultimo registro
                Post;
        end;
        EscribeBitacora( ServerRecursos.ImportarInfonavitLista( dmCliente.Empresa, cdsImportInfonavit.Data, Parametros.VarValues ) );
     finally
            FreeAndNil(Parametros);
     end;
end;

// (JB) Importar
procedure TdmCliente.ImportarCreditosInfonavit(const dFechaCambio: TDateTime; const lIniciaFechaOtorga : boolean; const dFechaInicio : TDateTime; const sComentarios : string; oDataSetInfonavit: TClientDataSet );
var
   Parametros :TZetaParams;
   //iEmpleado: Integer;
   //TipoDiferencia: eTipoDifInfonavit;
   //lEmpleadoDiferente: Boolean;

     procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
     begin
           // Agrega Campo al DataSet
           with oSDataSet do
           begin
                case eTipo of
                     tgBooleano : AddBooleanField( sCampo );
                     tgFloat : AddFloatField( sCampo );
                     tgNumero : AddIntegerField( sCampo );
                     tgFecha : AddDateField( sCampo );
                else
                     AddStringField( sCampo, iLongitud );
                end;
           end;
     end;

     procedure LimpiaEstructuraCreditos;
     begin
          cdsImportInfonavit.Close;
          cdsImportInfonavit.Fields.Clear;
          cdsImportInfonavit.FieldDefs.Clear;
     end;

     procedure DefineEstructuraCreditos;
     begin
          LimpiaEstructuraCreditos;
          
          with cdsImportInfonavit do
          begin
               if ( FieldCount <= 0  ) then
               begin
                    AgregaColumna( 'CB_CODIGO', tgNumero, 9, cdsImportInfonavit );
                    AgregaColumna( 'CB_INFCRED', tgTexto, 10, cdsImportInfonavit );
                    AgregaColumna( 'CB_INFTIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
                    AgregaColumna( 'CB_INFTASA', tgFloat, K_ANCHO_PESOS, cdsImportInfonavit );
                    AgregaColumna( 'CB_INFDISM', tgTexto, 2, cdsImportInfonavit );
                    AgregaColumna( 'CB_INF_ANT', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
                    AgregaColumna( 'DESCRIPCION', tgTexto, K_ANCHO_OBSERVACIONES, cdsImportInfonavit );
                    AgregaColumna( 'CB_INF_INI', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
                    AgregaColumna( 'CB_SEGSOC', tgTexto, K_ANCHO_DESCINFO, cdsImportInfonavit );
                    AgregaColumna( 'TPRESTAMO', tgNumero, 2, cdsImportInfonavit );
               end;
               if not Active then
               begin
                    CreateDataSet;
                    Open;
               end;
               EmptyDataSet;

          end;
     end;

     procedure InicializaDatos;
     begin
          //Los primeros datos que se capturan
          with cdsImportInfonavit do
          begin
               Append;
               FieldByName('CB_CODIGO').AsInteger  := oDataSetInfonavit.FieldByName('CB_CODIGO').AsInteger;
               FieldByName('DESCRIPCION').AsString := sComentarios;
               FieldByName('CB_INFDISM').AsString  := K_GLOBAL_NO;
          end;
     end;


begin
     Parametros := TZetaParams.Create( Self );
     try
        DefineEstructuraCreditos;

        with Parametros do
        begin
             AddDate( 'FechaCambio', dFechaCambio );
             AddInteger( 'Formato', Ord(infoInicio) );
             AddInteger( 'FormatoImpFecha',Ord( ifDDMMYYYYs));
             AddString( 'Archivo', 'Concilia Datos Infonavit' );
             AddMemo( 'Observaciones', 'Importaci�n Prestamo Infonavit' );
        end;
       // iEmpleado:= 0;
        with cdsImportInfonavit do
        begin
             oDataSetInfonavit.IndexFieldNames:= 'CB_CODIGO'; 
             oDataSetInfonavit.First;

             while not oDataSetInfonavit.Eof do   //Se asume que los tipos de movimientos vienen ordenados
             begin
                  InicializaDatos;
                  cdsImportInfonavit.FieldByName('CB_INFCRED').AsString   := oDataSetInfonavit.FieldByName('NO_CREDITO').AsString;
                  cdsImportInfonavit.FieldByName('CB_INFTASA').AsFloat    := StrToReal(oDataSetInfonavit.FieldByName('VDESCUENTO').AsString);

                  if ( lIniciaFechaOtorga ) then
                     cdsImportInfonavit.FieldByName('CB_INF_INI').AsDateTime := oDataSetInfonavit.FieldByName('CB_INF_INI').AsDateTime
                  else
                     cdsImportInfonavit.FieldByName('CB_INF_INI').AsDateTime := dFechaInicio;

                  cdsImportInfonavit.FieldByName('CB_INF_ANT').AsDateTime  := oDataSetInfonavit.FieldByName('CB_INF_INI').AsDateTime; // (JB) ConciliadorIMSS: Al importar los nuevos credito la fecha otorgamiento queda vac�a Bug 2918
                  cdsImportInfonavit.FieldByName('CB_SEGSOC').AsString    := oDataSetInfonavit.FieldByName('CB_SEGSOC').AsString;
                  cdsImportInfonavit.FieldByName('CB_INFTIPO').AsInteger  := oDataSetInfonavit.FieldByName('TPRESTAMO').AsInteger; // (JB) Para poder obtener el texto del Tipo de Prestamo es: ObtieneElemento(lfTipoInfonavit, StrAsInteger(oDataSetInfonavit.FieldByName('TPRESTAMO').AsString) )
                  oDataSetInfonavit.Next;
             end;
             if ( State = dsInsert ) then //Ultimo registro
                Post;
        end;
        EscribeBitacora( ServerRecursos.ImportarInfonavitLista( dmCliente.Empresa, cdsImportInfonavit.Data, Parametros.VarValues ) );
     finally
            FreeAndNil(Parametros);
     end;
end;


procedure TdmCliente.SuspenderInfonavit(const dFechaCambio: TDateTime; oDataSetInfonavit: TClientDataSet );
var
   Parametros :TZetaParams;

     procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
     begin
           // Agrega Campo al DataSet
           with oSDataSet do
           begin
                case eTipo of
                     tgBooleano : AddBooleanField( sCampo );
                     tgFloat : AddFloatField( sCampo );
                     tgNumero : AddIntegerField( sCampo );
                     tgFecha : AddDateField( sCampo );
                else
                     AddStringField( sCampo, iLongitud );
                end;
           end;
     end;

     procedure LimpiaEstructuraCreditos;
     begin
          cdsImportInfonavit.Close;
          cdsImportInfonavit.Fields.Clear;
          cdsImportInfonavit.FieldDefs.Clear;
     end;

     procedure DefineEstructuraCreditos;
     begin
          LimpiaEstructuraCreditos;
          with cdsImportInfonavit do
          begin
               AgregaColumna( 'CB_CODIGO', tgNumero, 9, cdsImportInfonavit );
               AgregaColumna( 'TIPO_MOVIMIENTO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
               AgregaColumna( 'FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
               AgregaColumna( 'CB_INFCRED', tgTexto, 10, cdsImportInfonavit );
               AgregaColumna( 'CB_INFTIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
               AgregaColumna( 'CB_INFTASA', tgFloat, K_ANCHO_PESOS, cdsImportInfonavit );
               AgregaColumna( 'CB_INFDISM', tgTexto, 2, cdsImportInfonavit );
               AgregaColumna( 'CB_INF_ANT', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
               AgregaColumna( 'DESCRIPCION', tgTexto, K_ANCHO_OBSERVACIONES, cdsImportInfonavit );

               if not Active then
               begin
                    CreateDataSet;
                    Open;
               end;
               EmptyDataSet;

          end;
     end;

     procedure InicializaDatos;
     begin
          //Los primeros datos que se capturan
          with cdsImportInfonavit do
          begin
               Append;
               FieldByName('TIPO_MOVIMIENTO').AsInteger:= 5; //Ord(TipoDiferencia);
               FieldByName('CB_CODIGO').AsInteger:= oDataSetInfonavit.FieldByName('CB_CODIGO').AsInteger;
               FieldByName('CB_INFCRED').AsString:= oDataSetInfonavit.FieldByName('NO_CREDITO').AsString;
               FieldByName('DESCRIPCION').AsString:= 'Importaci�n de Suspensiones de Infonavit';
               FieldByName('FECHA').AsDateTime:= dFechaCambio;
          end;
     end;


begin
     Parametros := TZetaParams.Create( Self );
     try
        DefineEstructuraCreditos;

        with Parametros do
        begin
             AddDate('FechaCambio', dFechaCambio );
             AddInteger( 'Formato', Ord(faASCIIDel) );
             AddInteger( 'FormatoImpFecha',Ord( ifDDMMYYYYs));
             AddString( 'Archivo', 'Concilia Datos Infonavit' );
             AddMemo( 'Observaciones', 'Importaci�n Suspensiones Infonavit' );
        end;

        with cdsImportInfonavit do
        begin
             oDataSetInfonavit.IndexFieldNames:= 'CB_CODIGO';
             oDataSetInfonavit.First;

             while not oDataSetInfonavit.Eof do   //Se asume que los tipos de movimientos vienen ordenados
             begin
                  begin
                       InicializaDatos;
                  end;
                  oDataSetInfonavit.Next;
             end;

             if ( State = dsInsert ) then //Ultimo registro
                Post;
        end;
        EscribeBitacora( ServerRecursos.ImportarInfonavitLista( dmCliente.Empresa, cdsImportInfonavit.Data, Parametros.VarValues ) );
     finally
            FreeAndNil(Parametros);
     end;
end;



///////////////////////////////////////////////////////////////////////////////
/// Eventos de los procesos de bit�cora
///////////////////////////////////////////////////////////////////////////////

procedure TdmCliente.EscribeBitacora(Resultado: OleVariant; lMuestraResultado: Boolean);
const
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
begin
     with TProcessInfo.Create( nil ) do
     begin
          try
             SetResultado(Resultado);
             if ( lMuestraResultado ) then
                Check( Resultado )
             else
                 CheckSilencioso( Resultado );
          finally
                 Free;
          end;
     end;
end;


function TdmCliente.Check(const Resultado: OleVariant): Boolean;
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             with ProcessData do
             begin
                  SetResultado( Resultado );
                  Result := ( Status in [ epsEjecutando, epsOK ] );
             end;
            LeerUnProceso( ShowProcessInfo(True));
          finally
             Free;
          end;
     end;
end;

function TdmCliente.CheckSilencioso(const Resultado: OleVariant): Boolean;
begin
     Result:= TRUE;
     if eProcessStatus( Resultado[ K_PROCESO_STATUS ] ) <> epsOK then
     begin
          Result := Check( Resultado );
     end;
end;



procedure TdmCliente.GetProcessLog( const iProceso: Integer );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        cdsLogDetail.Data := dmCliente.ServerConsultas.GetProcessLog( dmCliente.Empresa, iProceso );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmCliente.cdsLogDetailAfterOpen(DataSet: TDataSet);
begin
     with cdsLogDetail do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_CLASE', lfClaseBitacora );
          MaskFecha( 'BI_FECHA' );
          MaskFecha( 'BI_FEC_MOV' );
     end;
end;

procedure TdmCliente.cdsLogDetailAlModificar(Sender: TObject);
begin
     if not cdsLogDetail.IsEmpty then
        FBaseEditBitacora_DevEx.ShowLogDetail( FormaBitacora_DevEx, TFormaBitacora_DevEx, cdsLogDetail );
end;

procedure TdmCliente.cdsProcesosCalcFields(DataSet: TDataSet);
var
   rAvance: Extended;
   iMaximo: Integer;
   dInicial, dFinal, dValue: TDateTime;
   eStatus: eProcessStatus;
   i: integer;
   sTemp, sArchivo: string;

function GetFechaHora( const dValue: TDateTime ): String;
const
     K_SHOW_DATE_TIME = 'hh:nn:ss AM/PM dd/mmm/yyyy';
begin
     if ( dValue = NullDateTime ) then
        Result := ''
     else
         Result := FormatDateTime( K_SHOW_DATE_TIME, dValue );
end;

begin
     eStatus := GetStatusProceso;
     with Dataset do
     begin
          iMaximo := FieldByName( 'PC_MAXIMO' ).AsInteger;
          if ( eStatus = epsOK ) then
             rAvance := 1
          else
              rAvance := ZetaCommonTools.rMax( 0.01, FieldByName( 'PC_PASO' ).AsInteger / ZetaCommonTools.iMax( 1, iMaximo ) );
          dInicial := ZetaClientTools.AddDateTime( FieldByName( 'PC_FEC_INI' ).AsDateTime,
                                                   FieldByName( 'PC_HOR_INI' ).AsString );
          dFinal := ZetaClientTools.AddDateTime( FieldByName( 'PC_FEC_FIN' ).AsDateTime,
                                                 FieldByName( 'PC_HOR_FIN' ).AsString );
          dValue := ZetaClientTools.GetElapsed( dFinal, dInicial );
          FieldByName( 'PC_INICIO' ).AsString := GetFechaHora( dInicial );
          FieldByName( 'PC_FIN' ).AsString := GetFechaHora( dFinal );
          FieldByName( 'PC_AVANCE' ).AsString := FormatFloat( '#0.# %', 100 * rAvance );
          FieldByName( 'PC_TIEMPO' ).AsString := ZetaClientTools.GetDuration( dValue );
          with FieldByName( 'PC_FALTA' ) do
          begin
               if ( rAvance < 0.001 ) or ( eStatus <> epsEjecutando ) then
                  AsString := ''
               else if ( rAvance < 0.15 ) then  // Avance menor al 15%
                  AsString := '???'
               else
                  AsString := '???';//GetEstimatedTime( dValue * ( ( 1 / rAvance ) - 1 ) );
          end;
          with FieldByName( 'PC_STATUS' ) do
          begin
               case eStatus of
                    epsEjecutando: AsString := 'Abierto';
                    epsCancelado: AsString := 'Cancelado';
                    epsOK: AsString := 'OK';
               else
                   AsString := 'Con Error';
               end;
          end;
          FieldByName( 'PC_SPEED' ).AsString := ZetaWizardFeedBack_DevEx.GetProcessSpeed( iMaximo, dInicial, dFinal );
          
          //Campo de Par�metros del Proceso
          sArchivo := FieldByName( 'PC_PARAMS' ).AsString;
          if ( sArchivo <> VACIO ) then
          begin
               for i := 1 to ( Length( sArchivo ) ) do
               begin
                    if ( sArchivo[ i ] = ZetaCommonClasses.K_PIPE ) then
                    begin
                         FieldByName( 'PC_PARAM' ).ASString := FieldByName( 'PC_PARAM' ).ASString + sTemp + CR_LF;
                         sTemp := '';
                    end
                    else
                        sTemp := sTemp + sArchivo[ i ];
               end;
               FieldByName( 'PC_PARAM' ).ASString := FieldByName( 'PC_PARAM' ).ASString + sTemp;
          end;
     end;

end;

function TdmCliente.GetStatusProceso: eProcessStatus;
begin
     Result := ZetaClientTools.GetProcessStatus( cdsProcesos.FieldByName( 'PC_ERROR' ).AsString );
end;

procedure TdmCliente.cdsProcesosAfterOpen(DataSet: TDataSet);
begin
 with cdsProcesos do
     begin
          MaskFecha( 'PC_FEC_INI' );
          MaskFecha( 'PC_FEC_FIN' );
          ConfigProcIdField( FieldByName( 'PC_PROC_ID' ) );
          with FieldByName( 'US_CODIGO' ) do
          begin
               Alignment := taLeftJustify;
               //OnGetText := UsuarioGetText;
          end;
          //FieldByName( 'US_CANCELA' ).OnGetText := UsuarioGetText;
          //FieldByName( 'PC_MAXIMO' ).OnGetText := PC_MAXIMOGetText;
          // Para que no salga Empleado '0'
          TNumericField( FieldByName( 'CB_CODIGO' ) ).DisplayFormat := '0;;#';
     end;
end;


procedure TdmCliente.GetProcessText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Sender.Dataset.IsEmpty then
             Text := ''
          else
              Text := ZetaClientTools.GetProcessName( Procesos( Sender.AsInteger ) );
     end
     else
         Text := Sender.AsString;
end;

procedure TdmCliente.ConfigProcIdField( Campo: TField );
begin
     with Campo do
     begin
          OnGetText := GetProcessText;
          Alignment := taLeftJustify;
     end;
end;

procedure TdmCliente.cdsProcesosAlCrearCampos(Sender: TObject);
begin
     with cdsProcesos do
     begin
          CreateCalculated( 'PC_AVANCE', ftString, 10 );
          CreateCalculated( 'PC_TIEMPO', ftString, 40 );
          CreateCalculated( 'PC_FALTA', ftString, 20 );
          CreateCalculated( 'PC_STATUS', ftString, 10 );
          CreateCalculated( 'PC_INICIO', ftString, 30 );
          CreateCalculated( 'PC_FIN', ftString, 30 );
          CreateCalculated( 'PC_SPEED', ftString, 30 );
          CreateCalculated( 'PC_PARAM', ftString, 255 );
     end;
end;




procedure TdmCliente.cdsProcesosAlModificar(Sender: TObject);
begin
     with cdsProcesos do
     begin
          if not IsEmpty then
          begin
               FProcessLogShow_DevEx.GetBitacora( Procesos(FieldByName( 'PC_PROC_ID' ).AsInteger) ,
                                            FieldByName( 'PC_NUMERO' ).AsInteger );
          end;
     end;
end;

procedure TdmCliente.LeerUnProceso(const iFolio: Integer);
var
   oCursor: TCursor;
begin
     if ( iFolio > 0 ) then
     begin
          with cdsProcesos do
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := oCursor;
               try
                  Data := dmCliente.ServerConsultas.GetProcess( dmCliente.Empresa, iFolio );
               finally
                      Screen.Cursor := oCursor;
               end;
               if IsEmpty then
                  ZetaDialogo.ZError( '� Bit�cora Vac�a !', 'La Bit�cora Del Proceso # ' + IntToStr( iFolio ) + ' No Existe', 0 )
               else
                   Modificar;
          end;
     end;
end;

procedure TdmCliente.cdsRPatronGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     inherited;
     lHasRights:= FALSE;
end;

procedure TdmCliente.ReiniciarInfonavit(const dFechaCambio: TDateTime;
  oDataSetInfonavit: TClientDataSet);
var
   Parametros :TZetaParams;

     procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
     begin
           // Agrega Campo al DataSet
           with oSDataSet do
           begin
                case eTipo of
                     tgBooleano : AddBooleanField( sCampo );
                     tgFloat : AddFloatField( sCampo );
                     tgNumero : AddIntegerField( sCampo );
                     tgFecha : AddDateField( sCampo );
                else
                     AddStringField( sCampo, iLongitud );
                end;
           end;
     end;

     procedure LimpiaEstructuraCreditos;
     begin
          cdsImportInfonavit.Close;
          cdsImportInfonavit.Fields.Clear;
          cdsImportInfonavit.FieldDefs.Clear;
     end;

     procedure DefineEstructuraCreditos;
     begin
          LimpiaEstructuraCreditos;

          with cdsImportInfonavit do
          begin
               AgregaColumna( 'CB_CODIGO', tgNumero, 9, cdsImportInfonavit );
               AgregaColumna( 'TIPO_MOVIMIENTO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
               AgregaColumna( 'FECHA', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
               AgregaColumna( 'CB_INFCRED', tgTexto, 10, cdsImportInfonavit );
               AgregaColumna( 'CB_INFTIPO', tgNumero, K_ANCHO_CODIGO1, cdsImportInfonavit );
               AgregaColumna( 'CB_INFTASA', tgFloat, K_ANCHO_PESOS, cdsImportInfonavit );
               AgregaColumna( 'CB_INFDISM', tgTexto, 2, cdsImportInfonavit );
               AgregaColumna( 'CB_INF_ANT', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );
               AgregaColumna( 'DESCRIPCION', tgTexto, K_ANCHO_OBSERVACIONES, cdsImportInfonavit );
               AgregaColumna( 'CB_INF_INI', tgFecha, K_ANCHO_FECHA, cdsImportInfonavit );

               if not Active then
               begin
                    CreateDataSet;
                    Open;
               end;
               EmptyDataSet;

          end;
     end;

     procedure InicializaDatos;
     begin
          //Los primeros datos que se capturan
          with cdsImportInfonavit do
          begin
               Append;
               FieldByName('TIPO_MOVIMIENTO').AsInteger:= 6; //Ord(TipoDiferencia);
               FieldByName('CB_CODIGO').AsInteger:= oDataSetInfonavit.FieldByName('CB_CODIGO').AsInteger;
               FieldByName('CB_INFCRED').AsString   := oDataSetInfonavit.FieldByName('NO_CREDITO').AsString;
               FieldByName('CB_INFTASA').AsFloat    := StrToReal(oDataSetInfonavit.FieldByName('VDESCUENTO').AsString);
               FieldByName('CB_INFTIPO').AsInteger  := oDataSetInfonavit.FieldByName('TPRESTAMO').AsInteger; // (JB) Para poder obtener el texto del Tipo de Prestamo es: ObtieneElemento(lfTipoInfonavit, StrAsInteger(oDataSetInfonavit.FieldByName('TPRESTAMO').AsString) )
               FieldByName('DESCRIPCION').AsString:= 'Importaci�n de Suspensiones de Infonavit';
               FieldByName('FECHA').AsDateTime:= dFechaCambio;
          end;
     end;


begin
     Parametros := TZetaParams.Create( Self );
     try
        DefineEstructuraCreditos;

        with Parametros do
        begin
             AddDate('FechaCambio', dFechaCambio );
             AddInteger( 'Formato', Ord(faASCIIDel) );
             AddInteger( 'FormatoImpFecha',Ord( ifDDMMYYYYs));
             AddString( 'Archivo', 'Concilia Datos Infonavit' );
             AddMemo( 'Observaciones', 'Importaci�n Suspensiones Infonavit' );
        end;

        with cdsImportInfonavit do
        begin
             oDataSetInfonavit.IndexFieldNames:= 'CB_CODIGO';
             oDataSetInfonavit.First;

             while not oDataSetInfonavit.Eof do   //Se asume que los tipos de movimientos vienen ordenados
             begin
                  begin
                       InicializaDatos;
                  end;
                  oDataSetInfonavit.Next;
             end;

             if ( State = dsInsert ) then //Ultimo registro
                Post;
        end;
        EscribeBitacora( ServerRecursos.ImportarInfonavitLista( dmCliente.Empresa, cdsImportInfonavit.Data, Parametros.VarValues ) );
     finally
            FreeAndNil(Parametros);
     end;
end;


procedure TdmCliente.cdsConceptosAlAdquirirDatos(Sender: TObject);
begin
     inherited;

{$IFDEF FROMEX}
     cdsConceptos.Data := GetSQLData( GetScript( esConceptosNomina ) ); 
{$ENDIF}

end;
{$IFDEF FROMEX}
procedure TdmCliente.CargaMontosConceptos;
var
   sCompania : String;
begin
    inherited;
    sCompania := dmCliente.Compania;
    FImagenIni := TIniFile.Create( GetMontosConceptosPath );
    with FImagenIni do
    begin
         IMSSSubtotal := ReadString( sCompania, K_IMSS_SUBTOTAL, '' );
         Retiro := ReadString( sCompania, K_RETIRO, '' );
         CesantiaYVejez := ReadString( sCompania, K_CESANTIA_VLEJEZ, '' );
         InfonavitPatronal := ReadString( sCompania, K_INFONAVIT_PATRONAL, '' );
         Amortizacion := ReadString( sCompania, K_AMORTIZACION, '' );
    end;
end;

procedure TdmCliente.GuardaMontosConceptos;
var
   sCompania : String;
begin
    inherited;
    sCompania := dmCliente.Compania;
    if FImagenIni <> nil then
       FImagenIni := TIniFile.Create( GetMontosConceptosPath );

    with FImagenIni do
    begin
         WriteString( sCompania, K_IMSS_SUBTOTAL, ImssSubtotal );
         WriteString( sCompania, K_RETIRO, Retiro );
         WriteString( sCompania, K_CESANTIA_VLEJEZ, CesantiaYVejez );
         WriteString( sCompania, K_INFONAVIT_PATRONAL, InfonavitPatronal );
         WriteString( sCompania, K_AMORTIZACION, Amortizacion );
    end;
end;

function TdmCliente.GetMontosConceptosPath : String;
begin
     Result := ZetaMessages.SetFileNameDefaultPath( K_ARCHIVO_MONTOSCONCEPTOS );
end;
{$ENDIF}

procedure TdmCliente.AnaliticaContabilizarUso(const sModulo: String;const HelpContext, iConteo: Integer);
begin
    Servidor.AnaliticaContabilizarUso(Empresa,sModulo,HelpContext,iConteo);
end;

//DevEx: Metodo creado para invocar la nueva Busqueda de empleados al Servidor
procedure TdmCliente.GetEmpleadosBuscados_DevEx (const sPista: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
begin
     with oDataSet do
     begin
          Init;
          if ( FServidor = nil ) then
             Data := Servidor.GetEmpleadosBuscados_DevEx( oEmpresa, sPista )
          else
             Data := FServidor.GetEmpleadosBuscados_DevEx( oEmpresa, sPista );
     end;
end;

end.
