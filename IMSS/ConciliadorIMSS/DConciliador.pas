unit DConciliador;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DCliente, Db, DBClient,  ZetaServerDataSet,
  {$ifndef VER130}Variants, MaskUtils,{$else}Mask,{$endif}
  ZetaClientDataSet;

const K_ESPACIO = ' ';
      K_ANCHO_COLUMNA = 25;
      K_ANCHO = 104;
      K_IGUALES = 'Iguales';

      K_CUOTA_FIJA = 'EYM: Cuota Fija';
      K_Excedente = 'EYM: Excedente';
      K_Dinero = 'EYM: Dinero' ;
      K_Especie = 'EYM: Especie' ;
      K_ExcedenteP = K_Excedente + ' Patronal';
      K_DineroP =  K_Dinero + ' Patronal' ;
      K_EspecieP = K_Especie + ' Patronal' ;
      K_Riesgos = 'Riesgos de Trabajo' ;
      K_Invalidez = 'Invalidez y Vida' ;
      K_InvalidezP = K_Invalidez + ' Patronal' ;
      K_Guarderias = 'Guarder�as' ;

      K_RETIRO = 'Retiro';
      K_Cesantia = 'Cesant�a y Vejez';
      K_CesantiaP = K_Cesantia + ' Patronal';
      K_Voluntaria = 'Aportaci�n Voluntaria';
      K_Infonavit = 'Infonavit Patronal';
      k_Amortiza = 'Infonavit Amortizaci�n';

      K_Dias = 'D�as Cotizados';
      K_Incapacidad = 'D�as Incapacidad';
      K_Ausentismo = 'D�as Ausentismo';
      K_Integrado = 'Salario Base';





type
  eEmisionTipo = ( eetPatron, eetEmpleados, eetMovimientos );
  eOrigen = ( eoTress, eoCDIMSS );

  {
            false = ImssNOTressSI
            true = ImssSITressNO
            ImssSITressSI

  }
  eTipoCredActivos = ( ImssNOTressSI, ImssSITressNO, ImssSITressSuspendido , ImssNOTressSuspendido);

  TdmConciliador = class(TDataModule)
    cdsPatron: TZetaClientDataSet;
    cdsEmpleados: TZetaClientDataSet;
    cdsMovimientos: TZetaClientDataSet;
    cdsMovConc: TZetaClientDataSet;
    cdsMontosTotales: TZetaClientDataSet;
    cdsEmpleadosTress: TZetaClientDataSet;
    cdsEmpleadosIMSS: TZetaClientDataSet;
    cdsDatosEmpleados: TZetaClientDataSet;
    cdsCurp: TZetaClientDataSet;
    cdsInfonavit: TZetaClientDataSet;
    cdsMovtosTress: TZetaClientDataSet;
    cdsMovtosIMSS: TZetaClientDataSet;
    cdsInactivosTress: TServerDataSet;
    cdsInactivosImss: TServerDataSet;
    cdsSuspendidosTress: TServerDataSet;
    cdsInactivosImssSuspendidosTress: TServerDataSet;
    cdsDatosEmpleadosTodos: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FContrasena: string;
    procedure ImssGetText(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure TipoCreditoGetText(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure MaskImss(DataSet: TClientDataset; const Campo: String  );
    procedure CurpGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure FechaTressGetText(Sender: TField; var Text: String;DisplayText: Boolean);

    procedure MaskCurp(DataSet: TZetaClientDataset; const Campo: String);
    procedure MaskTipoCredito( DataSet: TZetaClientDataset; const Campo: String);
    procedure MaskFechaTress(DataSet: TDataSet; const Campo: String);
  protected
    FRecsOut: Integer;
    FPatronID: String;
    FLista: TStrings;
    FDiferencias: Integer;
    FNumero: Integer;
    FYear: Integer;
    FClase: eEmisionClase;
    FEmpNotFound: TStrings;
    FEmpleados: TStrings;
    FMovimientos: TStrings;
    FRevisaCurp: Boolean;
    FRevisaInfonavit: Boolean;
    FPrecision: Currency;

    function GetHayDiferencia: Boolean;
    procedure SetDiferencias;
    procedure SetNumero(const iMes: Integer);virtual;
    procedure SetYear(const iYear: Integer);virtual;
    procedure ConciliarCampos(cdsTress, cdsIMSS: TDataset; const sNombre,sCampo: String; Log: TStrings; const lSoloDiferencias: Boolean; cdsLog: TClientDataset = NIL );overload;
    procedure ConciliarCampos(cdsTress, cdsIMSS: TDataset; const sNombre,sCampo: String; Log: TStrings; const lSoloDiferencias: Boolean; cdsLog: TClientDataset = NIL; const lSetDiferencias: Boolean = TRUE; const iTipoDiferencia: Integer = 0 );overload;
    function GetCampoAsString(Campo: TField): String;
    function GetDiferencias(const lIguales: Boolean; const sDiferencia: String): String;
    function GetFloatAsStr(const rValue: Extended): String;
    function GetIntegerAsStr(const iValue: Integer): String;
    function GetConciliaDescripcion: string;virtual;

    procedure ListaAgregar(Fuente, Destino: TStrings);
    procedure ListaDelimitador(const cFiller: Char; const sTexto: String;const iAncho: Integer);
    procedure ListaEncabezados;
    procedure ListaEspacio;
    procedure ListaFiller(const sTexto: String = '');
    procedure ListaSubFiller(const sTexto: String = '');
    procedure ListaTexto(const sTexto: String);
    procedure ListaTitulo( const sTexto: string );
    function TienenMismoTipo(Original, Nuevo: TField): Boolean;
    function CamposSonDiferentes(Original, Datos: TDataset): Boolean;
    function  EsTipoDescuentoValidoEmpleado : Boolean;

    procedure ConciliarCampoEmpleadoPobla(const sNombre, sCampo: String; Log: TStrings; cdsLog : TClientDataset ); virtual;
    procedure ConciliarCampoEmpleado(const sNombre, sCampo: String; Log: TStrings; cdsLog : TClientDataset );overload;
    procedure ConciliarCampoEmpleado(const sNombre, sCampo: String; Log: TStrings; cdsLog : TClientDataset; const lSetDiferencias: Boolean  );overload;
    procedure ConciliarCampoEmpleado(const sNombre, sCampo: String; Log: TStrings; cdsLog : TClientDataset; const iTipoDiferencia: Integer  );overload;
    procedure ConciliarCampoPatron(const sNombre, sCampo: String);
    procedure SetEmployeeFound(Datos: TDataset);
    procedure AddMovimiento(Datos: TDataset; const lTress: Boolean);overload;
    procedure AddMovimiento( Bitacora, Datos: TDataset; const lTress: Boolean );overload;
    procedure RegistraBitacora(Bitacora, Datos: TDataset; const lTress: Boolean);

    function GetCampoMovimiento(const sCampo: String): String;

    {Dataset para Bitacoras}
    procedure AgregaRegistro( cdsDataSet: TDataSet; Campos: array of string );//overload;
    procedure EditaRegistro(cdsDataSet: TDataSet; Campos: array of string);

    //procedure AgregaRegistro( cdsDataset: TClientDataset; oLista: TStrings );overload;

    procedure CreaDatasets;
    procedure CierraDatasets;
    procedure ResetBitacoras;virtual;
    procedure FinishBitacoras;
    function  PrettyName(sPaterno, sMaterno, sNombres: String): String;
    procedure SetCreditosNoActivos(cdsDataSet: TServerDataSet;const eTipo : eTipoCredActivos);

  public
    property Lista: TStrings read FLista write FLista;
    property Clase: eEmisionClase read FClase write FClase;
    property RevisaCurp: Boolean read FRevisaCurp write FRevisaCurp;
    property RevisaInfonavit: Boolean read FRevisaInfonavit write FRevisaInfonavit;
    property Mes: Integer read FNumero write SetNumero;
    property Year: Integer read FYear write SetYear;
    property Precision : Currency read FPrecision write FPrecision;
    property HayDiferencia: Boolean read GetHayDiferencia;
    property Diferencias: Integer read FDiferencias;
    property ConciliaDescripcion: string read GetConciliaDescripcion;
    property Contrasena : string read FContrasena write FContrasena;
    function ConectarBaseDeDatos( const sFileName: String ): Boolean;virtual;abstract;
    function ConectarTablas: Boolean;virtual;abstract;
    procedure DesconectaBasedeDatos;virtual;abstract;
    procedure Conciliar;virtual;abstract;
  end;

const
     K_NUMERO_CREDITO = '# Cr�dito INFONAVIT';
     K_TIPO_DESCUENTO = 'Tipo de Descuento';
     K_VALOR_DESCUENTO = 'Valor de Descuento';
     K_INICIO_CREDITO = 'Inicio Del Cr�dito';
     K_REINICIO_CREDITO = 'Reinicio de Descuento';

var
  dmConciliador: TdmConciliador;

implementation
uses
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaCommonLists;



{$R *.DFM}

{ TdmConciliador }

procedure TdmConciliador.DataModuleCreate(Sender: TObject);
begin
     FNumero := 0;
     FYear := 0;
     FClase := eeCMensual;
     FEmpNotFound := TStringList.Create;
     FEmpleados := TStringList.Create;
     FMovimientos := TStringList.Create;
     CreaDatasets;
end;

procedure TdmConciliador.DataModuleDestroy(Sender: TObject);
begin
     CierraDataSets;
     
     FreeAndNil( FMovimientos );
     FreeAndNil( FEmpleados );
     FreeAndNil( FEmpNotFound );
end;

procedure TdmConciliador.SetNumero( const iMes: Integer );
begin
     case Clase of
          eeCMensual: FNumero := iMes;
          eeCBimestral: FNumero := ZetaCommonTools.NumeroBimestre( iMes );
     end;
end;

procedure TdmConciliador.SetYear( const iYear: Integer );
begin
     FYear := iYear;
end;

function TdmConciliador.GetHayDiferencia: Boolean;
begin
     Result := ( FDiferencias > 0 );
end;

procedure TdmConciliador.SetDiferencias;
begin
     Inc( FDiferencias );
end;

procedure TdmConciliador.ListaTexto( const sTexto: String );
begin
     with Lista do
     begin
          Add( sTexto );
     end;
end;

procedure TdmConciliador.ListaDelimitador(const cFiller: Char; const sTexto: String; const iAncho: Integer );
var
   sValue: String;
begin
     sValue := StringOfChar( cFiller, iAncho );
     if ZetaCommonTools.StrLleno( sTexto ) then
     begin
          Insert( K_ESPACIO + sTexto + K_ESPACIO, sValue, Trunc( ( iAncho - Length( sTexto ) - 2 ) / 2 ) );
          sValue := Copy( sValue, 1, iAncho );
     end;
     ListaTexto( sValue );
end;

procedure TdmConciliador.ListaSubFiller(const sTexto: String = '' );
const
     K_FILLER = '=';
begin
     ListaDelimitador( K_FILLER, sTexto, K_ANCHO );
end;

procedure TdmConciliador.ListaFiller(const sTexto: String = '' );
const
     K_FILLER = '_';
begin
     ListaDelimitador( K_FILLER, sTexto, K_ANCHO );
end;

procedure TdmConciliador.ListaEspacio;
begin
     ListaTexto( VACIO );
end;

procedure TdmConciliador.ListaEncabezados;
const
     K_FILLER = '-';
begin
     ListaTexto( ZetaCommonTools.PadR( 'Campo', K_ANCHO_COLUMNA ) + K_ESPACIO +
                 ZetaCommonTools.PadL( 'Tress', K_ANCHO_COLUMNA ) + K_ESPACIO +
                 ZetaCommonTools.PadL( ConciliaDescripcion, K_ANCHO_COLUMNA ) + K_ESPACIO +
                 ZetaCommonTools.PadL( 'Diferencia', K_ANCHO_COLUMNA ) + K_ESPACIO );
     ListaTexto( StringOfChar( K_FILLER, K_ANCHO_COLUMNA ) + K_ESPACIO +
                 StringOfChar( K_FILLER, K_ANCHO_COLUMNA ) + K_ESPACIO +
                 StringOfChar( K_FILLER, K_ANCHO_COLUMNA ) + K_ESPACIO +
                 StringOfChar( K_FILLER, K_ANCHO_COLUMNA ) );
end;

procedure TdmConciliador.ListaTitulo(const sTexto: string);

 const
      K_FILLER = '-';
begin
     ListaEspacio;
     ListaEspacio;
     ListaTexto( sTexto );
     ListaDelimitador( K_FILLER, VACIO, Length( sTexto ) );
     ListaEspacio;
     ListaEncabezados;
end;

procedure TdmConciliador.ListaAgregar( Fuente, Destino: TStrings );
var
   i: Integer;
begin
     with Fuente do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               Destino.Add( Strings[ i ] );
          end;
     end;
end;


function TdmConciliador.GetFloatAsStr( const rValue: Extended ): String;
begin
     Result := Format( '%16.2n', [ Abs(rValue) ] );
end;

function TdmConciliador.GetIntegerAsStr( const iValue: Integer ): String;
begin
     Result := Format( '%16.0n', [ Abs(iValue) / 1 ] );
end;

function TdmConciliador.GetDiferencias( const lIguales: Boolean; const sDiferencia: String ): String;

begin
     if lIguales then
     begin
          Result := K_IGUALES;
     end
     else
     begin
          SetDiferencias;
          Result := sDiferencia;
     end;
end;

function TdmConciliador.TienenMismoTipo( Original, Nuevo: TField ): Boolean;
begin
     case Original.DataType of
          //ftString: Result := ( Nuevo.DataType = ftString );
          ftString: Result := ( Nuevo.DataType in [ ftString, ftWideString ] );                      //**correccion, nuevo tipo de dato por uso de conexion con ADO
          ftSmallint: Result := ( Nuevo.DataType in [ ftSmallInt, ftInteger, ftWord ] );
          //**ftInteger: Result := ( Nuevo.DataType in [ ftSmallInt, ftInteger, ftWord, ftFloat, ftCurrency ] );
          ftInteger: Result := ( Nuevo.DataType in [ ftSmallInt, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD ] );    //**correccion, nuevo tipo de dato por uso de conexion con ADO
          ftWord: Result := ( Nuevo.DataType in [ ftSmallInt, ftInteger, ftWord ] );
          ftBoolean: Result := ( Nuevo.DataType = ftBoolean );
          //**ftFloat: Result := ( Nuevo.DataType in [ ftSmallInt, ftInteger, ftWord, ftFloat, ftCurrency ] );
          ftFloat: Result := ( Nuevo.DataType in [ ftSmallInt, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD ] );  //**correccion, nuevo tipo de dato por uso de conexion con ADO
          ftCurrency: Result := ( Nuevo.DataType in [ ftSmallInt, ftInteger, ftWord, ftFloat, ftCurrency ] );
          ftDate: Result := ( Nuevo.DataType in [ ftDate, ftDateTime ] );
          ftDateTime: Result := ( Nuevo.DataType in [ ftDate, ftDateTime ] );
     else
         Result := False;
     end;
end;

function TdmConciliador.GetCampoAsString( Campo: TField ): String;
begin
     with Campo do
     begin
          case DataType of
               ftString: Result := AsString;
               ftWideString: Result := AsString; //nuevo tipo de dato por uso de conexion con ADO
               ftSmallint: Result := GetIntegerAsStr( AsInteger );
               ftInteger: Result := GetIntegerAsStr( AsInteger );
               ftWord: Result := GetIntegerAsStr( AsInteger );
               ftBoolean: Result := ZetaCommonTools.BoolToSiNo( AsBoolean );
               ftFloat: Result := GetFloatAsStr( AsFloat );
               ftBCD: Result := GetFloatAsStr( AsFloat );  //nuevo tipo de dato por uso de conexion con ADO
               ftCurrency: Result := GetFloatAsStr( AsCurrency );
               ftDate: Result := ZetaCommonTools.FechaCorta( AsDateTime );
               ftDateTime: Result := ZetaCommonTools.FechaCorta( AsDateTime );
          else
              Result := 'XXX';
          end;
     end;
end;

procedure TdmConciliador.ConciliarCampos( cdsTress, cdsIMSS: TDataset; const sNombre, sCampo: String; Log: TStrings; const lSoloDiferencias: Boolean; cdsLog: TClientDataset = NIL );
begin
     ConciliarCampos( cdsTress, cdsIMSS, sNombre, sCampo, Log, lSoloDiferencias, cdsLog, TRUE );
end;

procedure TdmConciliador.ConciliarCampos(cdsTress, cdsIMSS: TDataset; const sNombre,sCampo: String; Log: TStrings; const lSoloDiferencias: Boolean; cdsLog: TClientDataset = NIL; const lSetDiferencias: Boolean = TRUE; const iTipoDiferencia: Integer = 0);
const
     K_DIFERENTES = 'Diferentes';
     aStatusEmp: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('Empleado Baja', 'Empleado Activo' );
var
   oTress, oIMSS: TField;
   sTress, sIMSS, sDiferencia: String;
   iDiferencias: Integer;
begin
     oTress := cdsTress.FieldByName( sCampo );
     oIMSS := cdsIMSS.FieldByName( sCampo );
     iDiferencias := FDiferencias;

     if lSoloDiferencias then
        sDiferencia := VACIO
     else
         sDiferencia := K_DIFERENTES;

     if Assigned( oTress ) and Assigned( oIMSS ) then
     begin
          sTress := GetCampoAsString( oTress );
          sIMSS := GetCampoAsString( oIMSS );
          if TienenMismoTipo( oTress, oIMSS ) then
          begin
               case oTress.DataType of
                    ftString: sDiferencia := GetDiferencias( ( Trim( oTress.AsString ) = Trim( oIMSS.AsString ) ), sDiferencia );
                    ftWideString : sDiferencia := GetDiferencias( ( Trim( oTress.AsString ) = Trim( oIMSS.AsString ) ), sDiferencia );   //nuevo tipo de dato por uso de conexion con ADO
                    ftSmallint: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftInteger: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftWord: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftBoolean: sDiferencia := GetDiferencias( ( oTress.AsBoolean = oIMSS.AsBoolean ), sDiferencia );
                    ftFloat: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsFloat, oIMSS.AsFloat, FPrecision ), GetFloatAsStr( oTress.AsFloat - oIMSS.AsFloat ) );
                    ftBCD: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsFloat, oIMSS.AsFloat, FPrecision ), GetFloatAsStr( oTress.AsFloat - oIMSS.AsFloat ) );                 //nuevo tipo de dato por uso de conexion con ADO
                    ftCurrency: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsCurrency, oIMSS.AsCurrency, FPrecision ), GetFloatAsStr( oTress.AsCurrency - oIMSS.AsCurrency ) );
                    ftDate: sDiferencia := GetDiferencias( ( oTress.AsDateTime = oIMSS.AsDateTime ), sDiferencia );
                    ftDateTime: sDiferencia := GetDiferencias( ( oTress.AsDateTime = oIMSS.AsDateTime ), sDiferencia );
               else
                   begin
                        if lSetDiferencias then
                           SetDiferencias;
                        sDiferencia := 'YYY';
                   end;
               end;
          end
          else
          begin
               if lSetDiferencias then
                  SetDiferencias;
               sDiferencia := 'XXX';
          end;
     end
     else
     begin
          if lSetDiferencias then
             SetDiferencias;
          if not Assigned( oTress ) then
          begin
               sTress := ZetaCommonTools.PadR( '???', K_ANCHO_COLUMNA );
          end;
          if not Assigned( oIMSS ) then
          begin
               sIMSS := ZetaCommonTools.PadR( '???', K_ANCHO_COLUMNA );
          end;
          sDiferencia := VACIO;
     end;
     if not lSoloDiferencias or ( FDiferencias > iDiferencias ) then
     begin
          if ( cdsLog.FindField('CB_INF_INI') <> NIL ) then
          begin
                AgregaRegistro( cdsLog, [ cdsTress.FieldByName('CB_SEGSOC').AsString,
                                       PrettyName( cdsTress.FieldByName('CB_APE_PAT').AsString,
                                                   cdsTress.FieldByName('CB_APE_MAT').AsString,
                                                   cdsTress.FieldByName('CB_NOMBRES').AsString ),
                                       cdsTress.FieldByName('CB_CODIGO').AsString,
                                       sNombre,oTress.AsString,oImss.AsString,FloatToStr(cdsIMSS.FieldByName('CB_INF_INI').AsFloat), IntToStr(iTipoDiferencia),
                                       aStatusEmp[zStrToBool( cdsTress.FieldByName('CB_ACTIVO').AsString ) ] ,
                                       FloatToStr(cdsTress.FieldByName('CB_FEC_ING').AsFloat), FloatToStr(cdsTress.FieldByName('CB_FEC_BAJ').AsFloat) ] )
          end
          else if ( cdsTress.FindField('CB_CODIGO') <> NIL ) then
             AgregaRegistro( cdsLog, [ cdsTress.FieldByName('CB_SEGSOC').AsString,
                                       PrettyName( cdsTress.FieldByName('CB_APE_PAT').AsString,
                                                   cdsTress.FieldByName('CB_APE_MAT').AsString,
                                                   cdsTress.FieldByName('CB_NOMBRES').AsString ),
                                       cdsTress.FieldByName('CB_CODIGO').AsString,
                                       sNombre,oTress.AsString,oImss.AsString,sDiferencia ] )
          else
              AgregaRegistro( cdsLog, [ sNombre,oTress.AsString,oImss.AsString,sDiferencia ] );

          if lSetDiferencias then
             Log.Add( ZetaCommonTools.PadR( sNombre, K_ANCHO_COLUMNA ) + K_ESPACIO +
                   ZetaCommonTools.PadL( sTress, K_ANCHO_COLUMNA ) + K_ESPACIO +
                   ZetaCommonTools.PadL( sIMSS, K_ANCHO_COLUMNA ) + K_ESPACIO +
                   ZetaCommonTools.PadL( sDiferencia, K_ANCHO_COLUMNA ) );

     end;
end;

function TdmConciliador.CamposSonDiferentes( Original, Datos: TDataset ): Boolean;
var
   i: Integer;
   Fuente, Destino: TField;
begin
     Result := False;
     with Original do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               if ( Fields[ i ].Tag > 0 ) then
               begin
                    Destino := Fields[ i ];
                    Fuente := Datos.FieldByName( Destino.FieldName );
                    if TienenMismoTipo( Fuente, Destino ) then
                    begin
                         case Fuente.DataType of
                              ftString: Result := ( Fuente.AsString <> Destino.AsString );
                              ftWideString: Result := ( Fuente.AsString <> Destino.AsString );    //nuevo tipo de dato por uso de conexion con ADO
                              ftSmallint: Result := ( Fuente.AsInteger <> Destino.AsInteger );
                              ftInteger: Result := ( Fuente.AsInteger <> Destino.AsInteger );
                              ftWord: Result := ( Fuente.AsInteger <> Destino.AsInteger );
                              ftBoolean: Result := ( Fuente.AsBoolean <> Destino.AsBoolean );
                              ftFloat: Result := NOT ZetaCommonTools.PesosIgualesP( Fuente.AsFloat, Destino.AsFloat, FPrecision );
                              ftBCD: Result := NOT ZetaCommonTools.PesosIgualesP( Fuente.AsFloat, Destino.AsFloat, FPrecision );     //nuevo tipo de dato por uso de conexion con ADO
                              ftCurrency: Result := NOT ZetaCommonTools.PesosIgualesP( Fuente.AsCurrency, Destino.AsCurrency, FPrecision );
                              ftDate: Result := ( Fuente.AsDateTime <> Destino.AsDateTime );
                              ftDateTime: Result := ( Fuente.AsDateTime <> Destino.AsDateTime );
                         else
                             Result := True;
                         end;
                    end
                    else
                    begin
                         Result := True;
                    end;

                    if Result then
                       Break;
               end;
          end;
     end;
end;

procedure TdmConciliador.ConciliarCampoEmpleado( const sNombre, sCampo: String; Log: TStrings; cdsLog : TClientDataset );
begin
     ConciliarCampoEmpleado( sNombre, sCampo, Log, cdsLog, TRUE );
end;

procedure TdmConciliador.ConciliarCampoEmpleado(const sNombre, sCampo: String; Log: TStrings; cdsLog : TClientDataset; const lSetDiferencias: Boolean );
begin
     ConciliarCampos( dmCliente.cdsLiqEmp, cdsEmpleados, sNombre, sCampo, Log, True, cdsLog, lSetDiferencias );
end;

procedure TdmConciliador.ConciliarCampoEmpleado(const sNombre,sCampo: String; Log: TStrings; cdsLog: TClientDataset; const iTipoDiferencia: Integer);
begin
     ConciliarCampos( dmCliente.cdsLiqEmp, cdsEmpleados, sNombre, sCampo, Log, True, cdsLog, TRUE, iTipoDiferencia );
end;

procedure TdmConciliador.ConciliarCampoPatron( const sNombre, sCampo: String );
begin
     ConciliarCampos( dmCliente.cdsLiqIMSS, cdsPatron, sNombre, sCampo, FLista, False, cdsMontosTotales, TRUE );
end;

function TdmConciliador.GetConciliaDescripcion: string;
begin
     Result := VACIO;
end;


procedure TdmConciliador.SetEmployeeFound( Datos: TDataset );
begin
     with Datos do
     begin
          Edit;
          try
             FieldByName( K_FOUND ).AsInteger := 1;
             Post;
          except
                on Error: Exception do
                begin
                     Cancel;
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

procedure TdmConciliador.AddMovimiento( Datos: TDataset; const lTress: Boolean );
begin
     AddMovimiento( cdsMovConc, Datos, lTress );
end;

procedure TdmConciliador.AddMovimiento( Bitacora, Datos: TDataset; const lTress: Boolean );
var
   eSource: eOrigen;
   i: Integer;
   Fuente, Destino: TField;
begin
     if lTress then
        eSource := eoTress
     else
         eSource := eoCDIMSS;
     with Bitacora do
     begin
          Append;
          try
             for i := 0 to ( FieldCount - 1 ) do
             begin
                  Destino := Fields[ i ];
                  {FieldByName, marca Error, FindField regresa NIL si no se encuentra el campo}
                  Fuente := Datos.FindField( Destino.FieldName );
                  //Fuente := Datos.FieldByName( Destino.FieldName );
                  if Assigned( Fuente ) then
                  begin
                       with Destino do
                       begin
                            case DataType of
                                 ftString: AsString := Fuente.AsString;
                                 ftWideString: AsString := Fuente.AsString;     //nuevo tipo de dato por uso de conexion con ADO
                                 ftSmallint: AsInteger := Fuente.AsInteger;
                                 ftInteger: AsInteger := Fuente.AsInteger;
                                 ftWord: AsInteger := Fuente.AsInteger;
                                 ftBoolean: AsBoolean := Fuente.AsBoolean;
                                 ftFloat: AsFloat := Fuente.AsFloat;
                                 ftBCD: AsFloat := Fuente.AsFloat;              //nuevo tipo de dato por uso de conexion con ADO
                                 ftCurrency: AsCurrency := Fuente.AsCurrency;
                                 ftDate: AsDateTime := Fuente.AsDateTime;
                                 ftDateTime: AsDateTime := Fuente.AsDateTime;
                            end;
                       end;
                  end;
             end;
             FieldByName( K_ORIGEN ).AsInteger := Ord( eSource );
             Post;
          except
                on Error: Exception do
                begin
                     Cancel;
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

function TdmConciliador.GetCampoMovimiento( const sCampo: String ): String;
var
   Campo: TField;
begin
     Campo := cdsMovConc.FieldByName( sCampo );
     if Assigned( Campo ) then
     begin
          Result := GetCampoAsString( Campo );
     end
     else
         Result := '???';
end;

{******************* MANEJO DE LOS DATASET PARA BITACORAS **********************************}

procedure TdmConciliador.CreaDatasets;
 procedure InitTempDataset( DataSet : TClientDataset );
 begin
      with Dataset do
      begin
           Active := False;
           Filtered := FALSE;
           Filter := '';
           IndexFieldNames := '';
           while FieldCount > 0 do
           begin
                Fields[ FieldCount - 1 ].DataSet := nil;
           end;
           FieldDefs.Clear;
      end;
 end;

 procedure MaskPesos( DataSet : TZetaClientDataset );
  var
     i: integer;
 begin
      with Dataset do
      begin
           for i:= 0 to FieldCount - 1 do
           begin
                if Fields[i] is TFloatField then
                   MaskPesos( Fields[i].FieldName );
           end;
      end;
 end;

begin
     with cdsMontosTotales do
     begin
          InitTempDataSet(cdsMontosTotales);
          AddIntegerField('FOLIO');
          AddStringField( 'DESCRIPCION', 100 );
          AddFloatField( 'TRESS' );
          AddFloatField( 'CD_IMSS' );
          AddStringField( 'DIFERENCIA',30 );
          CreateTempDataset;
     end;


     with cdsEmpleadosTress do
     begin
          InitTempDataset(cdsEmpleadosTress);
          AddIntegerField('FOLIO');
          AddStringField('CB_SEGSOC',30);
          AddStringField('PRETTYNAME',255);
          AddIntegerField('CB_CODIGO');
          CreateTempDataset;

     end;

     with cdsEmpleadosIMSS do
     begin
          InitTempDataset(cdsEmpleadosIMSS);
          AddIntegerField('FOLIO');
          AddStringField('CB_SEGSOC',30);
          AddStringField('PRETTYNAME',30);
          CreateTempDataset;
     end;

     with cdsDatosEmpleados do
     begin
          InitTempDataset(cdsDatosEmpleados);
          AddIntegerField('FOLIO');
          AddStringField('CB_SEGSOC',30);
          AddStringField('PRETTYNAME',255);
          AddIntegerField('CB_CODIGO');
          AddStringField( 'DESCRIPCION', 100 );
          AddFloatField( 'TRESS' );
          AddFloatField( 'CD_IMSS' );
          //AddStringField( 'DIFERENCIA',30 );
          AddFloatField('DIFERENCIA');
          CreateTempDataset;

     end;
     with cdsDatosEmpleadosTodos do
     begin
          InitTempDataset(cdsDatosEmpleadosTodos);
          AddIntegerField('FOLIO');
          AddStringField('CB_SEGSOC',30);
          AddStringField('PRETTYNAME',255);
          AddIntegerField('CB_CODIGO');
          AddStringField( 'DESCRIPCION', 100 );
          AddFloatField( 'TRESS' );
          AddFloatField( 'CD_IMSS' );
          //AddStringField( 'DIFERENCIA',30 );
          AddFloatField('DIFERENCIA');
          CreateTempDataset;

     end;


     with cdsCurp do
     begin
          InitTempDataset(cdsCurp);
          AddIntegerField('FOLIO');
          AddStringField('CB_SEGSOC',30);
          AddStringField('PRETTYNAME',255);
          AddIntegerField('CB_CODIGO');
          AddStringField( 'DESCRIPCION', 100 );
          AddStringField( 'TRESS', 30 );
          AddStringField( 'CD_IMSS', 30 );
          CreateTempDataset;


     end;

     with cdsInfonavit do
     begin
          InitTempDataset(cdsInfonavit);
          AddIntegerField('FOLIO');
          AddStringField('CB_SEGSOC',30);
          AddStringField('PRETTYNAME',255);
          AddIntegerField('CB_CODIGO');
          AddStringField( 'DESCRIPCION', 100 );
          AddStringField( 'TRESS', 30 );
          AddStringField( 'CD_IMSS', 30 );
          AddDateTimeField('CB_INF_INI');
          AddIntegerField('DIFERENCIA');
          AddStringField( 'STATUS_EMP', 30 );          
          AddDateTimeField('CB_FEC_ING');
          AddDateTimeField('CB_FEC_BAJ');
          CreateTempDataset;
     end;

     with cdsMovtosTress do
     begin
          InitTempDataset(cdsMovtosTress);
          AddIntegerField( 'CB_CODIGO' );
          AddStringField( 'CB_SEGSOC', 30 );
          AddStringField( 'PRETTYNAME', 255 );

          AddIntegerField('FOLIO');
          AddIntegerField( K_ORIGEN );
          AddFloatField( 'LM_EYM_FIJ' );
          AddFloatField( 'LM_EYM_EXC' );
          AddFloatField( 'LM_EYM_DIN' );
          AddFloatField( 'LM_EYM_ESP' );
          AddFloatField( 'LM_INV_VID' );
          AddFloatField( 'LM_RIESGOS' );
          AddFloatField( 'LM_GUARDER' );
          AddFloatField( 'LM_RETIRO' );
          AddFloatField( 'LM_CES_VEJ' );
          AddFloatField( 'LM_INF_PAT' );
          AddFloatField( 'LM_INF_AMO' );
          AddFloatField( 'LM_DIAS' );
          AddFloatField( 'LM_BASE' );
          AddDateField( 'LM_FECHA' );
          AddIntegerField( 'LM_TIPO' );
          AddIntegerField( 'LM_INCAPAC' );
          AddIntegerField( 'LM_AUSENCI' );
          AddStringField( 'LM_CLAVE', 2 );

          CreateTempDataset;

     end;

     with cdsMovtosImss do
     begin
          InitTempDataset(cdsMovtosImss);
          AddStringField( 'CB_SEGSOC', 30 );
          AddStringField( 'PRETTYNAME', 255 );

          AddIntegerField('FOLIO');
          AddIntegerField( K_ORIGEN );
          AddFloatField( 'LM_EYM_FIJ' );
          AddFloatField( 'LM_EYM_EXC' );
          AddFloatField( 'LM_EYM_DIN' );
          AddFloatField( 'LM_EYM_ESP' );
          AddFloatField( 'LM_INV_VID' );
          AddFloatField( 'LM_RIESGOS' );
          AddFloatField( 'LM_GUARDER' );
          AddFloatField( 'LM_RETIRO' );
          AddFloatField( 'LM_CES_VEJ' );
          AddFloatField( 'LM_INF_PAT' );
          AddFloatField( 'LM_INF_AMO' );
          AddFloatField( 'LM_DIAS' );
          AddFloatField( 'LM_BASE' );
          AddDateField( 'LM_FECHA' );
          AddStringField( 'LM_TIPO_MOV', 3 );

          CreateTempDataset;

     end;

     MaskImss( cdsEmpleadosTress, 'CB_SEGSOC' );
     MaskImss( cdsEmpleadosIMSS, 'CB_SEGSOC' );
     MaskImss( cdsDatosEmpleados, 'CB_SEGSOC' );
{$IFDEF FROMEX}
     MaskImss( cdsDatosEmpleadosTodos, 'CB_SEGSOC' );     
{$ENDIF}
     MaskImss( cdsMovtosTress, 'CB_SEGSOC' );
     MaskImss( cdsMovtosImss, 'CB_SEGSOC' );
     MaskImss( cdsInfonavit, 'CB_SEGSOC' );
     MaskImss( cdsCurp, 'CB_SEGSOC' );
     MaskCurp( cdsCurp, 'TRESS' );
     MaskCurp( cdsCurp, 'CD_IMSS' );

     MaskPesos( cdsMontosTotales );
     MaskPesos( cdsEmpleadosTress );
     MaskPesos( cdsEmpleadosIMSS );
     MaskPesos( cdsDatosEmpleados );
{$IFDEF FROMEX}
     MaskPesos( cdsDatosEmpleadosTodos );     
{$ENDIF}
     MaskPesos( cdsMovtosTress );
     MaskPesos( cdsMovtosImss );
     MaskPesos( cdsInfonavit );
     MaskPesos( cdsCurp  );

     MaskTipoCredito( cdsInfonavit, 'TRESS');
     MaskTipoCredito(cdsInfonavit, 'CD_IMSS');
     MaskFechaTress( cdsInfonavit, 'CB_FEC_ING');
     MaskFechaTress( cdsInfonavit, 'CB_FEC_BAJ');


end;

procedure TdmConciliador.CierraDatasets;
begin
     cdsMontosTotales.Close;
     cdsEmpleadosTress.Close;
     cdsEmpleadosIMSS.Close;
     cdsDatosEmpleados.Close;
     cdsDatosEmpleadosTodos.Close;
     cdsCurp.Close;
     cdsInfonavit.Close;
     cdsMovtosTress.Close;
     cdsMovtosImss.Close;
end;

procedure TdmConciliador.ResetBitacoras;
begin
     cdsMontosTotales.EmptyDataset;
     cdsEmpleadosTress.EmptyDataSet;
     cdsEmpleadosIMSS.EmptyDataSet;
     cdsDatosEmpleados.EmptyDataset;
     cdsDatosEmpleadosTodos.EmptyDataset;
     cdsCurp.EmptyDataset;
     cdsInfonavit.EmptyDataset;
     cdsMovtosTress.EmptyDataset;
     cdsMovtosImss.EmptyDataset;
end;

procedure TdmConciliador.FinishBitacoras;
 procedure SetFirst( DataSet : TDataSet );
 begin
      if DataSet.Active then DataSet.First;
 end;
begin

     cdsMontosTotales.IndexFieldNames := 'FOLIO';
     cdsEmpleadosTress.IndexFieldNames := 'CB_CODIGO';
     cdsEmpleadosIMSS.IndexFieldNames :=  'PRETTYNAME';
     cdsDatosEmpleados.IndexFieldNames := 'CB_CODIGO';
     cdsDatosEmpleadosTodos.IndexFieldNames := 'CB_CODIGO';
     cdsCurp.IndexFieldNames := 'CB_CODIGO';
     cdsInfonavit.IndexFieldNames := 'CB_CODIGO';
     cdsMovtosTress.IndexFieldNames := 'CB_CODIGO';
     cdsMovtosImss.IndexFieldNames := 'PRETTYNAME';


     SetFirst( cdsMontosTotales );
     SetFirst( cdsEmpleadosTress );
     SetFirst( cdsEmpleadosIMSS );
     SetFirst( cdsDatosEmpleados );
     SetFirst( cdsDatosEmpleadosTodos );
     SetFirst( cdsCurp );
     SetFirst( cdsInfonavit );
     SetFirst( cdsMovtosTress );
     SetFirst( cdsMovtosImss );

     with cdsDatosEmpleados do
     begin
          while not EOF do
          begin
               Edit;
               FieldByName('DIFERENCIA').AsFloat := Abs(FieldByName('DIFERENCIA').AsFloat);
               Next;
          end;
          First;
     end;

     with cdsDatosEmpleadosTodos do
     begin
          while not EOF do
          begin
               Edit;
               FieldByName('DIFERENCIA').AsFloat := Abs(FieldByName('DIFERENCIA').AsFloat);
               Next;
          end;
          First;
     end;
end;

procedure TdmConciliador.AgregaRegistro( cdsDataSet: TDataSet; Campos: array of string);
 var
    j,i: integer;
begin
     with cdsDataset do
     begin
          Append;
          FieldByName('FOLIO').AsInteger := RecordCount;
          for i:= 1 to FieldCount - 1 do
          begin
               j := i-1;
               if Fields[i] is TNumericField then
               begin
                    try
                       if ( Pos('X', Campos[j] ) = 0 ) then  //A veces rellena valores con X
                          Fields[i].AsFloat := StrToReal( Campos[j] ) //StrToFloat( Campos[j] );
                       else
                           Fields[i].AsFloat := 0;
                    except
                          Fields[i].AsFloat := 0;
                    end;
               end
               else if Fields[i] is TDateTimeField then
                   Fields[i].AsDateTime:= StrToReal(Campos[j])
               else
                   Fields[i].AsString := Campos[j]
          end;
          Post;
     end;
end;

procedure TdmConciliador.EditaRegistro( cdsDataSet: TDataSet; Campos: array of string);
 var
    i: integer;
begin
     with cdsDataset do
     begin
          Edit;
          for i:= 0 to High( Campos ) do
          begin
               if Fields[i] is TNumericField then
               begin
                    try
                       Fields[i].AsFloat := StrToReal( Campos[i] );  //StrToFloat( Campos[i] );
                    except
                          Fields[i].AsFloat := 0;
                    end;
               end
               else
                   Fields[i].AsString := Campos[i]
          end;
          FieldByName( 'FOLIO' ).AsInteger := RecordCount;
          Post;
     end;
end;


procedure TdmConciliador.RegistraBitacora(  Bitacora, Datos: TDataset; const lTress: Boolean );
begin
     AddMovimiento( Bitacora, Datos, lTress );
     if lTress then
     begin
          EditaRegistro( Bitacora, [ dmCliente.cdsLiqEmp.FieldByName('CB_CODIGO').AsString,
                                     dmCliente.cdsLiqEmp.FieldByName('CB_SEGSOC').AsString,
                                     PrettyName( dmCliente.cdsLiqEmp.FieldByName('CB_APE_PAT').AsString,
                                                 dmCliente.cdsLiqEmp.FieldByName('CB_APE_MAT').AsString,
                                                 dmCliente.cdsLiqEmp.FieldByName('CB_NOMBRES').AsString ) ] )
     end
     else
     begin
          EditaRegistro( Bitacora, [ cdsEmpleados.FieldByName(K_NUM_SS).AsString,
                                     cdsEmpleados.FieldByName('NOMBRE_SS').AsString ] );
     end;


end;

function TdmConciliador.PrettyName( sPaterno, sMaterno, sNombres : String ) : String;
begin
     Result := Copy( Trim( sPaterno) + ' ' + Trim( sMaterno ) + ', ' +  Trim( sNombres), 1, 255 );
end;

procedure TdmConciliador.MaskTipoCredito(DataSet: TZetaClientDataset; const Campo: String);
var
   oCampo: TField;
begin
     oCampo := DataSet.FindField( Campo );

     if ( oCampo <> nil ) then
     begin
          with oCampo do
          begin
               OnGetText := TipoCreditoGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

procedure TdmConciliador.MaskFechaTress(DataSet: TDataSet; const Campo: String);
var
   oCampo: TField;
begin
     oCampo := DataSet.FindField( Campo );

     if ( oCampo <> nil ) then
     begin
          with oCampo do
          begin
               OnGetText := FechaTressGetText;
              // Alignment := taLeftJustify;
          end;
     end;
end;



procedure TdmConciliador.MaskImss( DataSet: TClientDataset; const Campo: String  );
var
   oCampo: TField;
begin
     oCampo := DataSet.FindField( Campo );
     if ( oCampo <> nil )  then
     begin
          with oCampo do
          begin
               OnGetText := ImssGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

procedure TdmConciliador.MaskCurp( DataSet: TZetaClientDataset; const Campo: String  );
var
   oCampo: TField;
begin
     oCampo := DataSet.FindField( Campo );
     if ( oCampo <> nil )  then
     begin
          with oCampo do
          begin
               OnGetText := CurpGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;


procedure TdmConciliador.ImssGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if Sender.DataSet.IsEmpty or StrVacio( Sender.AsString ) then
        Text := ''
     else
     begin
          Text := FormatMaskText( '99-99-99-9999-9;0', Sender.AsString );
     end;
end;

procedure TdmConciliador.TipoCreditoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if cdsInfonavit.FieldByName('DIFERENCIA').AsInteger = Ord(DifInfTipoDescuento ) then
     begin
          Text := ObtieneElemento(lfTipoInfonavit, StrAsInteger(Sender.AsString) );
     end
     else
         Text:= Sender.AsString;
end;

procedure TdmConciliador.CurpGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if Sender.DataSet.IsEmpty or StrVacio( Sender.AsString ) then
        Text := ''
     else
     begin
          Text := FormatMaskText( 'LLLL-999999-L-LL-LLL-A-9;0', Sender.AsString );
     end;
end;

procedure TdmConciliador.SetCreditosNoActivos(cdsDataSet: TServerDataSet; const eTipo : eTipoCredActivos);
const
     aStatusEmp: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('Empleado Baja', 'Empleado Activo' );
     aStatusCredito: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('Cr�dito No Activo','');
     aFieldImss: array[ImssNOTressSI..ImssNOTressSuspendido] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('TRESS','CD_IMSS', 'CD_IMSS', 'TRESS' );
var
   sTress, sImss, sFilter: String;
   iEmpleado: Integer;
   lFiltrado: Boolean;
   lBorrar, lIgnorar, lAgrego: Boolean;

   procedure BorraTodasDiferencias( lAgrego : Boolean );
   begin
        with cdsInfonavit do
        begin
             Repeat
                   iEmpleado:= FieldByName('CB_CODIGO').AsInteger;
                   lBorrar := TRUE;
                   if {lNoActivosTress} ( (  eTipo = ImssSITressNO ) or ( eTipo = ImssSITressSuspendido ) ) and lAgrego   then
                   begin
                        if not ( cdsDataSet.State in [dsEdit, dsInsert] ) then
                           cdsDataSet.Edit;
                        with FieldByName('DIFERENCIA') do
                        begin
                             if ( eTipoDifInfonavit(AsInteger) = DifInfValorDesc ) then
                                cdsDataSet.FieldByName('VDESCUENTO').AsString:= FieldByName('CD_IMSS').AsString
                             else
                                if ( eTipoDifInfonavit(AsInteger) = DifInfTipoDescuento ) then
                                begin
                                     cdsDataSet.FieldByName('TPRESTAMO').AsInteger:= StrAsInteger( FieldByName('CD_IMSS').AsString );
                                     cdsDataSet.FieldByName('TDESCUENTO').AsString:= ObtieneElemento( lfTipoInfonavit, StrAsInteger(FieldByName('CD_IMSS').AsString) );
                                end
                                else
                                   if ( eTipo = ImssSiTressSuspendido ) and ( eTipoDifInfonavit(AsInteger) = DifInfNoCredito ) then
                                   begin
                                        cdsDataSet.FieldByName('NO_CREDITO').AsString:= FieldByName('CD_IMSS').AsString;
                                        lBorrar := TRUE;
                                   end;
                        end;
                   end;
                   Delete
             Until ( iEmpleado <> FieldByName('CB_CODIGO').AsInteger );
        end;
   end;

   function GetInfoAmoFromIDSE( const iEmpleado : integer ) : TPesos;
   begin
        Result := 0.00;
        with cdsDatosEmpleadosTodos do
        begin
             First;
             while not Eof do
             begin
                  if ( FieldByName('CB_CODIGO').AsInteger = iEmpleado ) then
                     Result := Result + FieldByName('CD_IMSS').AsFloat;
                  Next;
             end;
        end;
   end;

   procedure InsertaRegistro;
   begin
        with cdsDataSet do
        begin
             Append;
             FieldByName('CB_CODIGO').AsInteger:= cdsInfonavit.FieldByName('CB_CODIGO').AsInteger;
             FieldByName('PRETTYNAME').AsString:= cdsInfonavit.FieldByName('PRETTYNAME').AsString;
             FieldByName('CB_SEGSOC').AsString:= cdsInfonavit.FieldByName('CB_SEGSOC').AsString;
             FieldByName('NO_CREDITO').AsString:= cdsInfonavit.FieldByName( aFieldImss[eTipo] ).AsString;
             FieldByName('CB_INF_INI').AsDateTime:= cdsInfonavit.FieldByName( 'CB_INF_INI' ).AsDateTime;

             if dmCliente.cdsLiqEmp.Locate('CB_CODIGO', iEmpleado, [] ) then
             begin
                  FieldByName('STATUS_EMP').AsString:= aStatusEmp[zStrToBool( dmCliente.cdsLiqEmp.FieldByName('CB_ACTIVO').AsString ) ];
                  FieldByName('CB_FEC_ING').AsDateTime:= dmCliente.cdsLiqEmp.FieldByName( 'CB_FEC_ING' ).AsDateTime;
                  FieldByName('CB_FEC_BAJ').AsDateTime:= dmCliente.cdsLiqEmp.FieldByName( 'CB_FEC_BAJ' ).AsDateTime;

                  if {lNoActivosTress} ( eTipo=ImssSITressNO )  or ( eTipo = ImssSITressSuspendido )  then
                  begin
                       FieldByName('OBSERVA').AsString:= aStatusCredito[ zStrToBool( dmCliente.cdsLiqEmp.FieldByName('CB_INFACT').AsString ) ];
                       if (  eTipo = ImssSITressSuspendido  ) then
                          FieldByName('NO_CREDITO').AsString:= dmCliente.cdsLiqEmp.FieldByName('CB_INFCRED').AsString ;

                       FieldByName( 'INF_AMO' ).AsFloat := GetInfoAmoFromIDSE( iEmpleado );
                  end
                  else
                  begin
                       FieldByName('CB_INF_INI').AsDateTime:= dmCliente.cdsLiqEmp.FieldByName('CB_INF_INI').AsDateTime
                  end;
             end;

        end;
   end;

   procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer; var oSDataSet:TServerDataSet );
     begin
           // Agrega Campo al DataSet
           with oSDataSet do
           begin
                case eTipo of
                     tgBooleano : AddBooleanField( sCampo );
                     tgFloat : AddFloatField( sCampo );
                     tgNumero : AddIntegerField( sCampo );
                     tgFecha : AddDateField( sCampo );
                else
                     AddStringField( sCampo, iLongitud );
                end;
           end;
     end;

     procedure DefineEstructuraNoTress;
     begin
          with cdsDataSet do
          begin
               if ( FieldCount <= 0  ) then
               begin
                    AgregaColumna( 'CB_CODIGO', tgNumero, K_ANCHO_NUMEROEMPLEADO, cdsDataSet );
                    AgregaColumna( 'PRETTYNAME', tgTexto, K_ANCHO_PRETTYNAME, cdsDataSet );
                    AgregaColumna( 'CB_SEGSOC', tgTexto, K_ANCHO_DESCRIPCION, cdsDataSet );
                    AgregaColumna( 'NO_CREDITO', tgTexto, K_ANCHO_DESCINFO, cdsDataSet );
                    AgregaColumna( 'CB_INF_INI', tgFecha, K_ANCHO_FECHA, cdsDataSet );
                    AgregaColumna( 'STATUS_EMP', tgTexto, K_ANCHO_OBSERVACIONES, cdsDataSet );
                    if ( {lNoActivosTress} eTipo=ImssSITressNO ) or ( eTipo = ImssSITressSuspendido)  then
                    begin
                         AgregaColumna('OBSERVA', tgTexto, K_ANCHO_OBSERVACIONES, cdsDataSet );
                         AgregaColumna('TDESCUENTO', tgTexto, K_ANCHO_DESCINFO, cdsDataSet );
                         AgregaColumna('VDESCUENTO', tgTexto, K_ANCHO_DESCRIPCION, cdsDataSet );
                         AgregaColumna('TPRESTAMO', tgNumero, 2, cdsDataSet );
                         AgregaColumna('INF_AMO', tgFloat, K_ANCHO_PESOS, cdsDataSet);
                    end;
                    AgregaColumna( 'CB_FEC_ING', tgFecha, K_ANCHO_FECHA, cdsDataSet );
                    AgregaColumna( 'CB_FEC_BAJ', tgFecha, K_ANCHO_FECHA, cdsDataSet );
               end;
               if not Active then
               begin
                    CreateDataSet;
                    Open;
               end;
               EmptyDataSet;
          end;
          MaskImss( cdsDataset, 'CB_SEGSOC' );
          MaskFechaTress( cdsDataset, 'CB_FEC_ING');
          MaskFechaTress( cdsDataset, 'CB_FEC_BAJ');

     end;

     function EsCreditoActivoTRESS : boolean;
     begin
         Result := False;
         if dmCliente.cdsLiqEmp.Locate('CB_CODIGO', iEmpleado, [] ) then
         begin
              Result :=  zStrToBool( dmCliente.cdsLiqEmp.FieldByName('CB_INFACT').AsString )
         end;
     end;

     function ExisteCreditoActivoTRESS( sNumeroCred : string )  : boolean;
     begin
         Result := False;
         if dmCliente.cdsLiqEmp.Locate('CB_CODIGO', iEmpleado, [] ) then
         begin
              Result :=  zStrToBool( dmCliente.cdsLiqEmp.FieldByName('CB_INFACT').AsString );
              Result := Result and  ( sNumeroCred =  dmCliente.cdsLiqEmp.FieldByName('CB_INFCRED_ACTUAL').AsString );
         end;
     end;


begin
     with cdsInfonavit do
     begin
          DefineEstructuraNoTress;
          sFilter:= Filter;
          lFiltrado:= Filtered;
          Filtered:= FALSE;
          try
             First;
             while not EOF do
             begin
                  IndexFieldNames:= 'CB_CODIGO;DIFERENCIA';
                  if ( eTipo=ImssSITressSuspendido  ) then
                  begin
                                    sTress:= FieldByName('TRESS').AsString;
                  sImss:=  FieldByName('CD_IMSS').AsString;
                  iEmpleado:= FieldByName('CB_CODIGO').AsInteger;

                  end;

                  sTress:= FieldByName('TRESS').AsString;
                  sImss:=  FieldByName('CD_IMSS').AsString;
                  iEmpleado:= FieldByName('CB_CODIGO').AsInteger;
                  lIgnorar := FALSE;
                  lAgrego := FALSE;

                  if ( (eTipo=ImssSITressNO) and StrVacio( sTress ) and StrLleno(sImss)  ) or
                     ( (eTipo=ImssNOTressSI) and StrVacio(sImss) and StrLleno(sTress) and EsCreditoActivoTRESS ) or
                     ( (eTipo=ImssSITressSuspendido) and (FieldByName('DIFERENCIA').AsInteger = Ord(DifReinicioDescuento) ) and StrLleno(sImss) and StrLleno(sTress) and (not EsCreditoActivoTRESS) ) or
                     ( (eTipo=ImssNOTressSuspendido)  and StrVacio(sImss) and (not EsCreditoActivoTRESS)  )
                     then //Con la primera diferencia (No de credito) borra en secuencia
                  begin

                       if ( eTipo = ImssSITressNo ) and ExisteCreditoActivoTRESS(sImss) then
                          lIgnorar := TRUE;

                       if not lIgnorar then
                       begin
                          InsertaRegistro;
                          lAgrego := TRUE;
                       end;

                       BorraTodasDiferencias( lAgrego );

                       if ( cdsDataSet.State in [ dsEdit, dsInsert ] ) then
                          cdsDataSet.Post;
                  end
                  else
                  begin
                       Next;
                  end;

             end;
             MergeChangeLog;
          finally
                 Filter:=  sFilter;
                 Filtered:= lFiltrado;
          end;

     end;
end;

function TdmConciliador.EsTipoDescuentoValidoEmpleado: Boolean;
begin
     Result := FALSE;
     if not cdsEmpleados.IsEmpty then
     begin
          if ( cdsEmpleados.FieldByName('CB_INFTIPO') <> nil ) then
             Result := ( cdsEmpleados.FieldByName('CB_INFTIPO').AsInteger >= 0 ) and ( cdsEmpleados.FieldByName('CB_INFTIPO').AsInteger <= Ord(tiVeces) ) ;
     end;
end;

procedure TdmConciliador.ConciliarCampoEmpleadoPobla(const sNombre,
  sCampo: String; Log: TStrings; cdsLog: TClientDataset);
begin
     ConciliarCampos( dmCliente.cdsLiqEmp, cdsEmpleados, sNombre, sCampo, Log, False, cdsLog, TRUE );
end;

procedure TdmConciliador.FechaTressGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
     if ( Sender.DataSet.IsEmpty ) or ( Sender.AsDateTime = NullDateTime ) then
        Text := ''
     else
     begin
          Text := FormatDateTime( 'dd/mmm/yyyy', Sender.AsDateTime );;
     end;
end;

end.
