unit ZArbolTress;

interface

uses SysUtils, ComCtrls, Dialogs, DB,Controls,
     ZArbolTools, ZNavBarTools, cxtreeview, ZetaCommonTools;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses
     DCliente,
     ZetaDespConsulta,
     ZetaTipoEntidad,
     ZAccesosMgr,
     ZAccesosTress;

const
     {******** IMSS **********}
     K_IMSS                                       = 1;
     K_DIFERENCIAS_TOTALES                        = 10;
     K_EMPLEADOS_DATOS_DIFERENTES                 = 20;
     K_DIFERENCIAS_CURP                           = 30;
     K_EMP_NO_REGISTRADOS_IMSS                    = 40;
     K_EMP_NO_REGISTRADOS_TRESS                   = 50;
     K_MOVTO_NO_REGISTRADOS_IMSS                  = 60;
     K_MOVTO_NO_REGISTRADOS_TRESS                 = 70;
{$IFDEF FROMEX}
     K_MONTOS_EMPLEADOS_SUA                       =900;
{$ENDIF}
     {******** INFONAVIT **********}

     K_INFONAVIT                                  = 80;
     K_DIFERENCIAS_DATOS_INFONAVIT                = 90;
     K_CREDITOS_NO_REGISTRADOS_TRESS              = 100;
     K_CREDITOS_NO_REGISTRADOS_INFONAVIT          = 110;
     K_CREDITOS_SUSPENDIDOS_TRESS                 = 120;

     {******** SISTEMA **********}
     K_SISTEMA                                    = 130;
     K_ARCHIVOS_CONCILIACION                      = 140;
     K_FULL_ARBOL_TRESS                           =9999;



function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma := TRUE;
     Result.Caption := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma := FALSE;
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;
//DevEx: Funcion para definir Grupos del NavBar
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     case iNodo of
          {******** IMSS **********}
                K_DIFERENCIAS_TOTALES            : Result := Forma( 'Diferencias en totales', efcDiferenciasTotales );
                K_EMPLEADOS_DATOS_DIFERENTES     : Result := Forma( 'Empleados con datos diferentes', efcEmpleadosDatosDiferentes );
                K_DIFERENCIAS_CURP               : Result := Forma( 'Diferencias en CURP', efcDiferenciasCURP );
                K_EMP_NO_REGISTRADOS_IMSS        : Result := Forma( 'Empleados no registrados en IMSS', efcEmpNoRegistradosIMSS );
                K_EMP_NO_REGISTRADOS_TRESS       : Result := Forma( 'Empleados no registrados en TRESS', efcEmpNoRegistradosTRESS );
                K_MOVTO_NO_REGISTRADOS_IMSS      : Result := Forma( 'Movimientos no registrados en IMSS', efcMovtoNoRegistradosIMSS );
                K_MOVTO_NO_REGISTRADOS_TRESS     : Result := Forma( 'Movimientos no registrados en TRESS', efcMovtoNoRegistradosTRESS );
      {$IFDEF FROMEX}
                K_MONTOS_EMPLEADOS_SUA           : Result := Forma( 'Montos empleados SUA',efcEmpleadosSua  );
      {$ENDIF}

          {******** INFONAVIT **********}
                K_DIFERENCIAS_DATOS_INFONAVIT    : Result := Forma( 'Diferencias en datos INFONAVIT', efcDiferenciasDatosINFONAVIT );
                K_CREDITOS_NO_REGISTRADOS_TRESS  : Result := Forma( 'Cr�ditos no registrados en TRESS', efcCreditosNoRegistradosTRESS );
                K_CREDITOS_NO_REGISTRADOS_INFONAVIT  : Result := Forma( 'Cr�ditos no registrados en INFONAVIT', efcCreditosNoRegistradosINFONAVIT );
                K_CREDITOS_SUSPENDIDOS_TRESS     : Result := Forma( 'Cr�ditos suspendidos en TRESS', efcCreditosSuspendidosTRESS );

          {******** SISTEMA **********}
                K_ARCHIVOS_CONCILIACION          : Result := Forma( 'Archivos de Conciliaci�n', efcArchivosConciliacion );

            else
           Result := Folder( '', 0 );
     end;
end;
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
     case iNodo of
          K_IMSS            : Result := Grupo( 'IMSS', ZAccesosTress.D_IMSS );
          K_INFONAVIT       : Result := Grupo( 'INFONAVIT', ZAccesosTress.D_IMSS );
          K_SISTEMA         : Result := Grupo( 'Sistema', ZAccesosTress.D_IMSS );
	 else
          Result := Grupo( '', 0 );
     end;
end;


procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
var
   i, iNiveles: Integer;

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;
begin
    // CreaArbolDefault
    //usar 0 para indicar que es una ra�z de nodo, hija del grupo
    case iAccesoGlobal of
    // ************ IMSS *****************
     K_IMSS :
     begin
          NodoNivel( 0, K_DIFERENCIAS_TOTALES);
          NodoNivel( 0, K_EMPLEADOS_DATOS_DIFERENTES);
          NodoNivel( 0, K_DIFERENCIAS_CURP);
          NodoNivel( 0, K_EMP_NO_REGISTRADOS_IMSS);
          NodoNivel( 0, K_EMP_NO_REGISTRADOS_TRESS);
          NodoNivel( 0, K_MOVTO_NO_REGISTRADOS_IMSS);
          NodoNivel( 0, K_MOVTO_NO_REGISTRADOS_TRESS);
     {$IFDEF FROMEX}
          NodoNivel( 0, K_MONTOS_EMPLEADOS_SUA);
     {$ENDIF}
     end;
     // ************ INFONAVIT *****************
     K_INFONAVIT :
     begin
          NodoNivel( 0, K_DIFERENCIAS_DATOS_INFONAVIT);
          NodoNivel( 0, K_CREDITOS_NO_REGISTRADOS_TRESS);
          NodoNivel( 0, K_CREDITOS_NO_REGISTRADOS_INFONAVIT);
          NodoNivel( 0, K_CREDITOS_SUSPENDIDOS_TRESS);
     end;
     // ************ SISTEMA *****************
     K_SISTEMA :
     begin
           NodoNivel( 0, K_ARCHIVOS_CONCILIACION);
     end;
     end;

end;


procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     with oArbolMgr do
                     begin
                          CreaArbolitoDefault( oArbolMgr, K_GLOBAL  );
                          Arbol_Devex.FullCollapse;
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;
var
   NodoRaiz: TGrupoInfo;

begin
     // ************ CONCILIADOR *****************//
     begin
        if( NodoNivel( K_IMSS, 0 )) then
        begin
          CreaArbolito ( K_IMSS, Arbolitos[0]);
        end;
        if( NodoNivel( K_INFONAVIT, 1 )) then
        begin
          CreaArbolito ( K_INFONAVIT, Arbolitos[1]);
        end;
        if( NodoNivel( K_SISTEMA, 2 )) then
        begin
          CreaArbolito ( K_SISTEMA, Arbolitos[2]);
        end;

     end;

     // Si por derechos no tiene nada, agregar grupo
     if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
     begin
          with NodoRaiz do
          begin
               Caption := 'No tiene derechos';
               IndexDerechos := K_SIN_RESTRICCION;
               oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)
          end;
     end;
end;

end.
