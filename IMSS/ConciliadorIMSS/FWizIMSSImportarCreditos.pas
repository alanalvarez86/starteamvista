unit FWizIMSSImportarCreditos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, ZetaEdit, Vcl.StdCtrls, Vcl.Mask, ZetaFecha, cxRadioGroup,
  DBClient, cxMemo, cxSpinEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  ZetaCXStateComboBox;

type
  TWizIMSSImportarCreditos = class(TcxBaseWizard)
    lblFechaInicio: TcxLabel;
    GroupBox5: TcxGroupBox;
    rbFechaOtorga: TcxRadioButton;
    rbFechaVariable: TcxRadioButton;
    dFechaVariableImporta: TZetaFecha;
    lblObservaciones: TcxLabel;
    lblBimestreInfo: TcxLabel;
    cbBimestre: TcxStateComboBox;
    lblYearInfo: TcxLabel;
    YearInfo: TcxSpinEdit;
    txtImportaObserva: TZetaEdit;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rbFechaOtorgaClick(Sender: TObject);
    procedure rbFechaVariableClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);

  private
    { Private declarations }
    DataSet: TClientDataSet;
    lEsReadOnly: Boolean;
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
   WizIMSSImportarCreditos: TWizIMSSImportarCreditos;
   function ShowWizard( aDataSet: TClientDataSet): Boolean;overload;

implementation

{$R *.dfm}

uses
    ZetaClientDataSet, DCliente, ZetaRegistryCliente,
    ZBasicoSelectGrid_DevEx, ZetaDialogo, ZetaCommonClasses,
    ZetaCommonTools, ZetaCommonLists,
    FIMSSVerificaImportarCreditosGridSelect;


procedure TWizIMSSImportarCreditos.FormCreate(Sender: TObject);
var i:integer;
begin
     inherited;
     rbFechaOtorga.Checked := TRUE;
     rbFechaVariable.Checked := FALSE;
     dFechaVariableImporta.Valor :=  dmCliente.FechaDefault;
     dFechaVariableImporta.Enabled := FALSE;
     txtImportaObserva.Text := VACIO;

     YearInfo.Value := ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSYear);

     cbBimestre.Indice := NumeroBimestre( ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes) ) + cbBimestre.Offset;

     if ( eBimestres( NumeroBimestre( ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes) ) - 1 ) = bimNovDic ) then
     begin
          cbBimestre.Indice:= 1;
          YearInfo.Value :=  YearInfo.Value + 1;
     end;
     HelpContext := H_CONCIL_WIZ_IMPORTAR_CREDITOS;

end;

procedure TWizIMSSImportarCreditos.FormDestroy(Sender: TObject);
begin
     inherited;
     TClientDataSet( DataSet ).ReadOnly:= lEsReadOnly;
end;

procedure TWizIMSSImportarCreditos.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Se importar�n los cr�ditos de INFONAVIT de los empleados indicados en los par�metros.';

     lEsReadOnly := TClientDataSet( DataSet).ReadOnly;
     TClientDataSet( DataSet).ReadOnly:= FALSE;
     TClientDataSet( DataSet ).LogChanges := True;
end;

procedure TWizIMSSImportarCreditos.rbFechaOtorgaClick(Sender: TObject);
begin
     inherited;
      dFechaVariableImporta.Enabled := FALSE;
end;

procedure TWizIMSSImportarCreditos.rbFechaVariableClick(Sender: TObject);
begin
     inherited;
     dFechaVariableImporta.Enabled := TRUE;
end;


procedure TWizIMSSImportarCreditos.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if ( lok ) then
        TClientDataSet(DataSet).CancelUpdates;
end;

procedure TWizIMSSImportarCreditos.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                  if ( cbBimestre.Indice = ( cbBimestre.Offset -1 )  )  then
                     CanMove := Error( 'El bimestre no puede quedar vac�o', cbBimestre)
                  else
                  //si no hay registros a procesar despues de aplicar los filtros en la seleccion de par�metros, no avanzar
                  if ( DataSet.IsEmpty ) then
                  begin
                       ZetaDialogo.ZError( 'Error al importar cr�ditos INFONAVIT', 'Lista de cr�ditos a importar est� vac�a', 0 );
                       CanMove := False;
                  end
                  else
                  begin
                       CargaParametros;
                       //si hay registros a procesar, abrir el grid de empleados para filtrar
                       CanMove := ZBasicoSelectGrid_DevEx.GridSelectBasico(
                               TZetaClientDataSet( DataSet ), TIMSSVerificaImportarCreditosGridSelect, FALSE );
                       //si no hay registros a procesar, despues de filtrarlos por el grid de empleados, no avanzar
                       if ( DataSet.IsEmpty ) then
                       begin
                            ZetaDialogo.ZError( 'Error al importar cr�ditos INFONAVIT', 'Lista de cr�ditos a importar est� vac�a', 0 );
                            CanMove := False;
                            //cancelar los cambios realizados por el grid de empleados, cuando se eliminaron TODOS los registros a procesar
                            TClientDataSet( DataSet).CancelUpdates;
                       end;
                  end;

            end;
     end;

end;

function TWizIMSSImportarCreditos.EjecutarWizard: Boolean;
begin
       try
            with dmCliente do
            begin
             //Funcion que importa los creditos infonavit no registrados en Tress. (Primer dia del bimestre, Dataset) Se encuentra en dmCliente
             ImportarCreditosInfonavit( FirstDayOfBimestre( EncodeDate( YearInfo.Value,
                                        MesBimestre(cbBimestre.Indice), 1 ) ),
                                        rbFechaOtorga.Checked, dFechaVariableImporta.Valor,
                                        txtImportaObserva.Text,
                                        TClientDataSet( DataSet ) );


             end;
       except
             on Error: Exception do
             begin
                   ZetaDialogo.ZError( 'Error al importar cr�ditos INFONAVIT', Error.Message, 0 );
             end;
       end;
end;


procedure TWizIMSSImportarCreditos.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          Clear;
          if ( rbFechaOtorga.Checked ) then
               AddBoolean( 'Fecha de otorgamiento', rbFechaOtorga.Checked )
          else if ( rbFechaVariable.Checked ) then
          begin
               AddDate( 'Fecha', dFechaVariableImporta.Valor )
          end;
          AddString( 'Observaciones', txtImportaObserva.Text );
          AddString( 'Bimestre', ObtieneElemento( cbBimestre.ListaFija, cbBimestre.Indice - cbBimestre.Offset) );
          AddInteger( 'A�o', YearInfo.Value );
          if ( DataSet <> nil ) and ( DataSet.Active ) and ( Wizard.EsPaginaActual( Ejecucion )) then
             AddInteger ( 'Cantidad de empleados', DataSet.RecordCount);
     end;
end;


//Parametro: aDataset
function ShowWizard( aDataSet: TClientDataSet): Boolean;
begin
     with TWizIMSSImportarCreditos.Create( Application ) do
     begin
          try
             //recibe el TressShell.dsCredNoActivosTress.
             DataSet := aDataSet;
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;

end.
