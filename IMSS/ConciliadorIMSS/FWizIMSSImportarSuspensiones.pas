unit FWizIMSSImportarSuspensiones;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl,
  ZcxBaseWizard, ZetaCXWizard, ZetaClientDataSet, DBClient, ZetaFecha;

type
  TWizIMSSImportarSuspensiones = class(TcxBaseWizard)
    lblFechaSuspension: TcxLabel;
    dFechaSuspension: TZetaFecha;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    DataSet: TClientDataSet;
    lEsReadOnly: Boolean;
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;

  public
    { Public declarations }
  end;

var
   WizIMSSImportarSuspensiones: TWizIMSSImportarSuspensiones;
   function ShowWizard( aDataSet: TClientDataSet): Boolean;

implementation

{$R *.dfm}

uses
    ZetaDialogo, ZBasicoSelectGrid_DevEx, ZetaCommonTools,
    ZetaRegistryCliente, DCliente, ZetaCommonClasses,
    FIMSSVerificaImportarSuspensionesGridSelect;

procedure TWizIMSSImportarSuspensiones.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if ( lok ) then
        TClientDataSet(DataSet).CancelUpdates;
end;

procedure TWizIMSSImportarSuspensiones.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                  //si no hay registros a procesar despues de aplicar los filtros en la seleccion de par�metros, no avanzar
                  if ( DataSet.IsEmpty ) then
                  begin
                       ZetaDialogo.ZError( 'Error al importar reinicios de descuento', 'Lista de reinicio de descuentos vac�a', 0 );
                       CanMove := False;
                  end
                  else
                  begin
                       CargaParametros;
                       //si hay registros a procesar, abrir el grid de empleados para filtrar
                       CanMove := ZBasicoSelectGrid_DevEx.GridSelectBasico(
                               TZetaClientDataSet( DataSet ), TIMSSVerificaImportarSuspensionesGridSelect, FALSE );
                       //si no hay registros a procesar, despues de filtrarlos por el grid de empleados, no avanzar
                       if ( DataSet.IsEmpty ) then
                       begin
                            ZetaDialogo.ZError( 'Error al importar reinicios de descuento', 'Lista de reinicio de descuentos vac�a', 0 );
                            CanMove := False;
                            //cancelar los cambios realizados por el grid de empleados, cuando se eliminaron TODOS los registros a procesar
                            TClientDataSet( DataSet ).CancelUpdates;
                       end;

                  end;

            end;
     end;

end;

function TWizIMSSImportarSuspensiones.EjecutarWizard: Boolean;
begin
     inherited;
     try
     with dmCliente do
     begin
          //Funcion que importa los creditos infonavit no registrados en Tress. (Primer dia del bimestre, Dataset) Se encuentra en dmCliente
          SuspenderInfonavit( dFechaSuspension.Valor,
                              TClientDataSet( DataSet ) );
     end
     except
           on Error: Exception do
           begin

           end;
     end;


end;


procedure TWizIMSSImportarSuspensiones.FormCreate(Sender: TObject);
begin
     inherited;
     dFechaSuspension.Valor :=  LastDayOfBimestre( EncodeDate(  ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSYear),
                             ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes) , 1 ) ) + 1 ;

     HelpContext := H_CONCIL_WIZ_IMPORTAR_SUSPENSIONES;
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizIMSSImportarSuspensiones.FormDestroy(Sender: TObject);
begin
     inherited;
     TClientDataSet( DataSet ).ReadOnly:= lEsReadOnly;
end;

procedure TWizIMSSImportarSuspensiones.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Se suspender�n los descuentos del pr�stamo de INFONAVIT de los empleados indicados en los par�metros.';

     lEsReadOnly := TClientDataSet( DataSet).ReadOnly;
     TClientDataSet( DataSet).ReadOnly:= FALSE;
     TClientDataSet( DataSet ).LogChanges := True;
end;

procedure TWizIMSSImportarSuspensiones.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          Clear;
          AddDate( 'Fecha suspensi�n', dFechaSuspension.Valor);
          if ( DataSet <> nil ) and ( DataSet.Active )  and ( Wizard.EsPaginaActual( Ejecucion )) then
             AddInteger ( 'Cantidad de empleados', DataSet.RecordCount);
     end;
end;


//Parametro: aDataset
function ShowWizard( aDataSet: TClientDataSet): Boolean;
begin
     with TWizIMSSImportarSuspensiones.Create( Application ) do
     begin
          try
             //recibe el TressShell.dsCreditosInactivosImss.DataSet
             DataSet := aDataSet;
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;
end.
