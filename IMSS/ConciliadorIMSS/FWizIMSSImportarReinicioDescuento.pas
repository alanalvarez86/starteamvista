unit FWizIMSSImportarReinicioDescuento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, Vcl.StdCtrls, Vcl.Mask,
  ZetaFecha, ZcxBaseWizard, DBClient, ZetaClientDataSet;

type
  TWizIMSSImportarReinicioDescuento = class(TcxBaseWizard)
    lblFechaReinicio: TcxLabel;
    dFechaReinicio: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
     DataSet: TClientDataSet;
     lEsReadOnly: Boolean;
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
   WizIMSSImportarReinicioDescuento: TWizIMSSImportarReinicioDescuento;
   function ShowWizard( aDataSet: TClientDataSet): Boolean;

implementation

{$R *.dfm}


uses
    DCliente,
    ZetaDialogo, ZetaCommonTools, ZetaRegistryCliente,
    ZetaCommonClasses,
    ZBasicoSelectGrid_DevEx, FIMSSVerificaReinicioDescuentosGridSelect,
    ZetaServerDataSet;



procedure TWizIMSSImportarReinicioDescuento.FormCreate(Sender: TObject);
begin
     inherited;
     dFechaReinicio.Valor :=  LastDayOfBimestre( EncodeDate(  ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSYear),
                                ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes) , 1 ) ) + 1 ;

     HelpContext := H_CONCIL_WIZ_IMPORTAR_REINICIO;

     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;

end;

procedure TWizIMSSImportarReinicioDescuento.FormDestroy(Sender: TObject);
begin
     inherited;
     TClientDataSet( DataSet ).ReadOnly:= lEsReadOnly;
end;

procedure TWizIMSSImportarReinicioDescuento.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Se reiniciar�n los descuentos del pr�stamo de INFONAVIT de los empleados indicados en los par�metros.';

     lEsReadOnly := TClientDataSet( DataSet).ReadOnly;
     TClientDataSet( DataSet).ReadOnly:= FALSE;
     TClientDataSet( DataSet ).LogChanges := True;

end;

function TWizIMSSImportarReinicioDescuento.EjecutarWizard: Boolean;
begin
     inherited;
     try
            with dmCliente do
            begin
                 //Funcion que importa los creditos infonavit no registrados en Tress. (Primer dia del bimestre, Dataset) Se encuentra en dmCliente
                 ReiniciarInfonavit( dFechaReinicio.Valor, TClientDataSet( DataSet ) );
            end;
     except
           on Error: Exception do
           begin
                 ZetaDialogo.ZError( 'Error al importar reinicios de descuentos de cr�ditos Infonavit', Error.Message, 0 );
           end;
     end;


end;

procedure TWizIMSSImportarReinicioDescuento.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if  ( lOk ) then
         TClientDataSet( DataSet ).CancelUpdates;

end;

procedure TWizIMSSImportarReinicioDescuento.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                  //si no hay registros a procesar despues de aplicar los filtros en la seleccion de par�metros, no avanzar
                  if ( DataSet.IsEmpty ) then
                  begin
                       ZetaDialogo.ZError( 'Error al importar reinicios de descuento', 'Lista de reinicio de descuentos vac�a', 0 );
                       CanMove := False;
                  end
                  else
                  begin
                       CargaParametros;
                       //si hay registros a procesar, abrir el grid de empleados para filtrar
                       CanMove := ZBasicoSelectGrid_DevEx.GridSelectBasico(
                               TZetaClientDataSet( DataSet ), TIMSSVerificaReinicioDescuentosGridSelect, FALSE );
                       //si no hay registros a procesar, despues de filtrarlos por el grid de empleados, no avanzar
                       if ( DataSet.IsEmpty ) then
                       begin
                            ZetaDialogo.ZError( 'Error al importar reinicios de descuento', 'Lista de reinicio de descuentos vac�a', 0 );
                            CanMove := False;
                            //cancelar los cambios realizados por el grid de empleados, cuando se eliminaron TODOS los registros a procesar
                            TClientDataSet( DataSet ).CancelUpdates;


                       end;
                  end;

            end;
     end;

end;

procedure TWizIMSSImportarReinicioDescuento.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          Clear;
          AddDate( 'Fecha de reinicio' , dFechaReinicio.Valor);
          if ( DataSet <> nil ) and ( DataSet.Active ) and ( Wizard.EsPaginaActual( Ejecucion )) then
             AddInteger ( 'Cantidad de empleados', DataSet.RecordCount);
     end;
end;

//Parametro: aDataset
function ShowWizard( aDataSet: TClientDataSet): Boolean;
begin
     with TWizIMSSImportarReinicioDescuento.Create( Application ) do
     begin
          try
             //recibe el TressShell.dsCredSuspendidosTress.DataSet
             DataSet := aDataSet;
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;

end.
