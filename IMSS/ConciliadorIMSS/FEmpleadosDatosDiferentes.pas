unit FEmpleadosDatosDiferentes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxTextEdit, Vcl.Mask, ZetaNumero, Vcl.Buttons, cxContainer,
  cxCheckBox, cxGroupBox, cxButtons;

type
  TEmpleadosDatosDiferentes = class(TBaseGridLectura_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    TRESS: TcxGridDBColumn;
    CD_IMSS: TcxGridDBColumn;
    DIFERENCIA: TcxGridDBColumn;
    gbMensual: TcxGroupBox;
    Label11: TLabel;
    BtnFiltro1: TcxButton;
    cbLM_DIAS: TcxCheckBox;
    cbLM_EYM_FIJ: TcxCheckBox;
    cbLM_EYM_EXC: TcxCheckBox;
    cbLM_EYM_DIN: TcxCheckBox;
    cbLM_EYM_ESP: TcxCheckBox;
    cbLM_INV_VID: TcxCheckBox;
    cbLM_RIESGOS: TcxCheckBox;
    cbLM_GUARDER: TcxCheckBox;
    cbLM_BASE: TcxCheckBox;
    nMontos: TZetaNumero;
    cbLM_INCAPACI: TcxCheckBox;
    cbLM_AUSENCI: TcxCheckBox;
    cbLE_APO_VOL: TcxCheckBox;
    gbBimestral: TcxGroupBox;
    Label12: TLabel;
    BtnFiltro2: TcxButton;
    cbLM_DIAS2: TcxCheckBox;
    cbLM_RETIRO: TcxCheckBox;
    cbLM_CES_VEJ: TcxCheckBox;
    cbLM_INF_AMO: TcxCheckBox;
    cbLM_INF_PAT: TcxCheckBox;
    cbLM_BASE2: TcxCheckBox;
    nMontos2: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbLM_DIASClick(Sender: TObject);
    procedure nMontosExit(Sender: TObject);
    procedure BtnFiltro1Click(Sender: TObject);

  private
    { Private declarations }
    lMensual: Boolean;
    procedure FiltraDataSet;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;
  public
    { Public declarations }
    end;

const
     K_NOMBRE_FORMA = 'empleados con datos o montos diferentes';

var
  EmpleadosDatosDiferentes: TEmpleadosDatosDiferentes;

implementation

{$R *.dfm}


uses
    FTressShell,
    ZetaRegistryCliente, ZetaCommonTools, ZetaCommonClasses,
    DCliente, DConciliador;

procedure TEmpleadosDatosDiferentes.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsDatosEmpleados.DataSet;
end;


function TEmpleadosDatosDiferentes.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosDatosDiferentes.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosDatosDiferentes.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TEmpleadosDatosDiferentes.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TEmpleadosDatosDiferentes.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n empleado con datos o montos diferentes'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

     ZetaDBGridDBTableView.OptionsData.Editing := False;
     HelpContext := H_CONCIL_EM_DIF_DAT_MONT;
end;

procedure TEmpleadosDatosDiferentes.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
     lMensual :=  ( ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSClaseEmision) = Ord(eeCMensual) );
     gbMensual.Visible := lMensual;
     gbBimestral.Visible := NOT gbMensual.Visible;

     cbLM_DIAS.Checked := TRUE;
     cbLM_BASE.Checked := TRUE;
     cbLM_EYM_FIJ.Checked := FALSE;
     cbLM_EYM_EXC.Checked := FALSE;
     cbLM_EYM_DIN.Checked := FALSE;
     cbLM_EYM_ESP.Checked := FALSE;
     cbLM_INV_VID.Checked := FALSE;
     cbLM_RIESGOS.Checked := FALSE;
     cbLM_GUARDER.Checked := FALSE;

     cbLM_DIAS2.Checked := TRUE;
     cbLM_BASE2.Checked := TRUE;
     cbLM_RETIRO.Checked := FALSE;
     cbLM_CES_VEJ.Checked := FALSE;
     cbLM_INF_AMO.Checked := FALSE;
     cbLM_INF_PAT.Checked := FALSE;
     cbLE_APO_VOL.Checked := FALSE;

     FiltraDataSet;
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;


procedure TEmpleadosDatosDiferentes.nMontosExit(Sender: TObject);
begin
     inherited;
     FiltraDataSet;
end;

procedure TEmpleadosDatosDiferentes.Refresh;
begin

end;


procedure TEmpleadosDatosDiferentes.BtnFiltro1Click(Sender: TObject);
begin
     inherited;
     //with GridActivo do
     with ZetaDBGridDBTableView do
     begin
          if Visible and Enabled then
             SetFocus;
     end;
end;

procedure TEmpleadosDatosDiferentes.cbLM_DIASClick(Sender: TObject);
begin
     inherited;
     FiltraDataSet;
end;


procedure TEmpleadosDatosDiferentes.FiltraDataSet;
 var
    sFiltro : string;

 procedure GetFiltro( const lChecked: Boolean; const sCampo: string );
 begin
      if lChecked then
         sFiltro := ConcatString( sFiltro, 'DESCRIPCION like ' +  Comillas( '%' +sCampo+ '%' ) , ' OR ' );
 end;

 procedure GetDiferencia( const fValor: TPesos );
 begin
      if StrLleno( sFiltro ) then
         sFiltro := Parentesis( sFiltro );
      sFiltro := ConcatFiltros( sFiltro,
                                Parentesis( 'DIFERENCIA>' +  FloatToStr(fValor) ) );

 end;

begin
     sFiltro := VACIO;

     if ( ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSClaseEmision) = Ord(eeCMensual) )  then
    begin

          GetFiltro( cbLM_DIAS.Checked,  K_Dias );
          GetFiltro( cbLM_BASE.Checked, K_Integrado );
          GetFiltro( cbLM_EYM_FIJ.Checked,  K_CUOTA_FIJA );
          GetFiltro( cbLM_EYM_EXC.Checked,  K_Excedente );
          GetFiltro( cbLM_EYM_DIN.Checked,  K_Dinero );
          GetFiltro( cbLM_EYM_ESP.Checked,  K_Especie  );
          GetFiltro( cbLM_INV_VID.Checked,  K_Riesgos  );
          GetFiltro( cbLM_RIESGOS.Checked,  K_Invalidez  );
          GetFiltro( cbLM_GUARDER.Checked,  K_Guarderias );
          GetFiltro( cbLM_INCAPACI.Checked, K_Incapacidad );
          GetFiltro( cbLM_AUSENCI.Checked, K_Ausentismo  );
          // SOP 4527
          // Aportaciones voluntarias se calculan mensualmente
          GetFiltro( cbLE_APO_VOL.Checked, K_Voluntaria );
          // ----- -----

          GetDiferencia( nMontos.Valor );
     end
     else
     begin
          GetFiltro( cbLM_DIAS2.Checked, K_Dias );
          GetFiltro( cbLM_BASE2.Checked, K_Integrado );
          GetFiltro( cbLM_RETIRO.Checked, K_RETIRO );
          GetFiltro( cbLM_CES_VEJ.Checked, K_Cesantia );
          GetFiltro( cbLM_INF_AMO.Checked, k_amortiza );
          GetFiltro( cbLM_INF_PAT.Checked, K_Infonavit );
          // SOP 4527
          // Aportaciones voluntarias se calculan mensualmente
          // GetFiltro( cbLE_APO_VOL.Checked, K_Voluntaria );
          // ----- -----

          GetDiferencia( nMontos2.Valor );
     end;

     //with GridDatosEmpleados.DataSource.DataSet do
     if ( ZetaDBGridDBTableView.DataController.DataSource.DataSet  <> nil ) then
     with ZetaDBGridDBTableView.DataController.DataSource.DataSet do
     begin
          Filtered := FALSE;
          Filter := sFiltro;
          Filtered := StrLleno(sFiltro);
     end;

end;



end.
