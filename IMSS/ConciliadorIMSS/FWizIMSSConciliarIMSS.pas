unit FWizIMSSConciliarIMSS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FWizIMSSConciliarBase, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.Menus, cxClasses,
  cxShellBrowserDialog, Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Vcl.StdCtrls,
  ZetaKeyCombo, Vcl.Mask, ZetaNumero, cxTextEdit, cxButtons, cxMaskEdit,
  cxDropDownEdit, ZetaCXStateComboBox, cxSpinEdit, cxRadioGroup,
  DCliente, ZcxBaseWizard, cxProgressBar;

type
  TWiZIMSSConciliarIMSS = class(TWizIMSSConciliarBase)
    cxLabel2: TcxLabel;
    btnDirectorioIMSS: TcxButton;
    DirectorioIMSS: TcxTextEdit;
    ShellBrowserDialogDirectorioIMSS: TcxShellBrowserDialog;

    procedure btnDirectorioIMSSClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }

  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  end;
var
  WiZIMSSConciliarIMSS: TWiZIMSSConciliarIMSS;


implementation

{$R *.dfm}

uses
    ZetaRegistryCliente, ZetaCommonTools, ZetaCommonLists, ZetaCommonClasses,
    ZetaDialogo, FTressShell;

procedure TWiZIMSSConciliarIMSS.FormCreate(Sender: TObject);
begin
     inherited;
     try
        //directorio
        DirectorioIMSS.Text :=  ClientRegistry.ConciliadorIMSSDirectorioIMSS;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zInformation( '', 'Error al leer configuraci�n de valores activos.', 0 );
           end;
     end;

     HelpContext := H_CONCIL_IMSS;
end;



procedure TWiZIMSSConciliarIMSS.FormShow(Sender: TObject);
begin
  inherited;
     Advertencia.Caption := 'Al aplicar el proceso se comparar� la informaci�n que se tienen '
                 + 'en Sistema TRESS con la que se gener� en IMSS, lo cual arrojar� diferencias si hubiese.';
end;

procedure TWiZIMSSConciliarIMSS.btnDirectorioIMSSClick(Sender: TObject);
begin
     inherited;
     with ShellBrowserDialogDirectorioIMSS do
     begin
           if ( DirectorioIMSS.Text = '' ) then
              ShellBrowserDialogDirectorioIMSS.Path := VerificaDir( ExtractFilePath(Application.ExeName) )
           else
               ShellBrowserDialogDirectorioIMSS.Path := DirectorioIMSS.Text;

          if Execute then
                begin
                      try
                         DirectorioIMSS.Text := ShellBrowserDialogDirectorioIMSS.Path;
                      except
                            on Error: Exception do
                            begin
                                 ZetaDialogo.zInformation( '', 'No se tienen los permisos requeridos para guardar esta configuraci�n, firmarse con otro usuario.', 0 );
                            end;
                      end;
                end;
     end;
end;

procedure TWiZIMSSConciliarIMSS.CargaParametros;
begin
     //inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'Directorio', DirectorioIMSS.Text );
     end;

     with Descripciones do
     begin
          AddString( 'Directorio', DirectorioIMSS.Text );
     end;
     inherited;

end;

function TWiZIMSSConciliarIMSS.EjecutarWizard: Boolean;
begin
     TressShell.SetProgressbar( Self.ProgressBar);
     TressShell.ConciliarProceso(Year.Value, Mes.Indice, Tipo.Indice, Patron.Llave, DirectorioIMSS.Text, tcIDSE, GetEmision);
end;



procedure TWiZIMSSConciliarIMSS.WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
  var CanMove: Boolean);
begin
  inherited;
      with Wizard do
      begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
             begin
                  if ( DirectorioIMSS.Text = '') then
                  begin
                     CanMove := Error( 'El directorio no puede quedar vac�o', btnDirectorioIMSS)
                  end
             end;

     end;

end;



end.
