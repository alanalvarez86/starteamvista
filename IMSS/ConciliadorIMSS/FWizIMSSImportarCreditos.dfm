inherited WizIMSSImportarCreditos: TWizIMSSImportarCreditos
  Left = 0
  Top = 0
  Caption = 'Importar Cr'#233'ditos'
  ClientHeight = 399
  ClientWidth = 531
  Font.Name = 'Tahoma'
  OldCreateOrder = False
  ExplicitWidth = 537
  ExplicitHeight = 427
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 531
    Height = 399
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      7249000002FF494441545847C557BF8B134114CE6EA29E8AC7893944104110AE
      91FB036CD26537476057B9B81B2DCE46D0426C1504EDAE5088267736160A161E
      082A88D899E2504CB2213F3C102B0B6B6D2D44E2F7D699383BF736C95D42FCE0
      6327336FDEF7EDDB799B24D1EBF5FE2BD9C969929D9C265970817194A89FB7FD
      A09873C5C7F140895B2B8E51F3B3A614C2D8088A79438444D02CE64A816FF788
      3416D3BB83141CC4E6723625C223E2639BE0C4E2D8F0ADFDB40762ABBA81B14C
      104880B0B65508A9E27EDB9DC59C49E3C0B393E1D5B79E8C6522F0B261220969
      8043B9E39CAA74DC377D13C55CB256583ADCF0EDEFBB3611785612C1FD0336D4
      40D7ED454DE4530DCFBAAE8B4B8E66423350EE9EDB5BEEBA8B10BA81EB064437
      436195C244D3B74C1CCCA3BAB0CA812610F0F74E60A2DC758E09C1A8581CD54A
      F8F65729C831D604160D4A526EBB4F599161142690E79D2AC89135516A39A94A
      D7A9B3C947210C7CB86AA79BBEFD8813D5193171BB9A49407C5D4B4ACF5BE517
      505D57E8ACBF7AE898D5954C0A9DF09213E4D83751699F3D2113D1F85E900F9F
      A70E54C9A07740E59373458AE3AC5CA31B4007CC500C5E4EEF39B13822FE164E
      BB5B7AD07116280126FBE2D4091C651B625F5ECEA1F469B4E21CF6FF520586B1
      E1D99F136B817B466846D04F8E170D8267F1AE988163A3DC724F427C518DA95D
      583AD2F0B29795E42DB0AA92C470FD67C0B3BE819722621C75E8EB54FE8F17ED
      793CFF1F9498CE8108DD06C46E4A03A8DA63313DD840CBB3CCAD42C6E0D690C8
      A86632264437D8C41A682D368E13184488EEC19D1F44190B32E9580624383195
      1036213A8F3B4FA2A437912C72F0C63640404F43248BD25A61E9116C609CC437
      DF013C8E7D3874A791A42993A99C880109B4D6219C8134B1E6E716EABEBD8C0A
      BC4082D8769BA801B1E1B7E036319ED633B17D1BF003E6AD8CDB89014D60083D
      FB27CEC873DAAB12957BADC6D19C90890705A99B26C9910C20B0A26F9C14D141
      77844C3CD0E7C7F16BE7AE2CE3A4888E5AC5FF8EB95044EFF369939D9C26D9C9
      E9B197F80309045C16783DF6BE0000000049454E44AE426082}
    ExplicitWidth = 531
    ExplicitHeight = 399
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso permite importar los cr'#233'ditos de INFONAVIT que no s' +
        'e tienen registrados en Sistema TRESS.'
      Header.Title = 'Importaci'#243'n de cr'#233'ditos a Sistema TRESS'
      ExplicitWidth = 509
      ExplicitHeight = 259
      object lblFechaInicio: TcxLabel
        Left = 36
        Top = 56
        Hint = ''
        Caption = 'Fecha de Inicio:'
        ParentFont = False
        Transparent = True
      end
      object lblObservaciones: TcxLabel
        Left = 37
        Top = 110
        Hint = ''
        Caption = 'Observaciones:'
        ParentFont = False
        Transparent = True
      end
      object GroupBox5: TcxGroupBox
        Left = 122
        Top = 39
        Hint = ''
        TabOrder = 0
        Height = 50
        Width = 350
        object rbFechaOtorga: TcxRadioButton
          Left = 17
          Top = 16
          Width = 141
          Height = 17
          Caption = 'Fecha de Otorgamiento'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = rbFechaOtorgaClick
          Transparent = True
        end
        object rbFechaVariable: TcxRadioButton
          Left = 164
          Top = 16
          Width = 61
          Height = 17
          Caption = 'Fecha:'
          TabOrder = 1
          OnClick = rbFechaVariableClick
          Transparent = True
        end
        object dFechaVariableImporta: TZetaFecha
          Left = 222
          Top = 13
          Width = 115
          Height = 22
          Cursor = crArrow
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Text = '23/abr/12'
          Valor = 41022.000000000000000000
        end
      end
      object lblBimestreInfo: TcxLabel
        Left = 71
        Top = 147
        Hint = ''
        Caption = 'Bimestre:'
        ParentFont = False
        Transparent = True
      end
      object cbBimestre: TcxStateComboBox
        Left = 122
        Top = 145
        Hint = ''
        AutoSize = False
        Properties.DropDownListStyle = lsFixedList
        Properties.ImmediateDropDownWhenKeyPressed = False
        Properties.IncrementalSearch = False
        TabOrder = 2
        ListaFija = lfBimestres
        ListaVariable = lvPuesto
        EsconderVacios = False
        LlaveNumerica = True
        MaxItems = 10
        Offset = 1
        Height = 21
        Width = 130
      end
      object lblYearInfo: TcxLabel
        Left = 316
        Top = 147
        Hint = ''
        Caption = 'A'#241'o:'
        ParentFont = False
        Transparent = True
      end
      object YearInfo: TcxSpinEdit
        Left = 344
        Top = 145
        Hint = 'Seleccionar el a'#241'o'
        ParentShowHint = False
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taBottomJustify
        Properties.AssignedValues.DisplayFormat = True
        Properties.MaxValue = 2050.000000000000000000
        Properties.MinValue = 1899.000000000000000000
        Properties.SpinButtons.Position = sbpHorzLeftRight
        ShowHint = True
        TabOrder = 3
        Value = 1899
        Width = 90
      end
      object txtImportaObserva: TZetaEdit
        Left = 122
        Top = 108
        Width = 350
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 50
        ParentFont = False
        TabOrder = 1
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 509
      ExplicitHeight = 259
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 509
        ExplicitHeight = 164
        Height = 164
        Width = 509
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 509
        Width = 509
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 441
          ExplicitHeight = 74
          Width = 441
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 24
    Top = 72
  end
end
