unit FDiferenciasDatosINFONAVIT;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.Mask, ZetaFecha, ZetaKeyCombo, cxContainer, cxGroupBox,
  cxCheckBox, cxLabel, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  ZetaCXStateComboBox, cxCalendar;

type
  TDiferenciasDatosINFONAVIT = class(TBaseGridLectura_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    STATUS_EMP: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    TRESS: TcxGridDBColumn;
    CD_IMSS: TcxGridDBColumn;
    CB_FEC_ING: TcxGridDBColumn;
    CB_FEC_BAJ: TcxGridDBColumn;
    gbFiltrosInfo: TcxGroupBox;
    lblDiferencias: TcxLabel;
    GroupBox4: TcxGroupBox;
    lblFechaFiltro: TcxLabel;
    zFechaFiltro: TZetaFecha;
    lFiltrarPorInicio: TcxCheckBox;
    cbDiferenciasInfonavit: TcxStateComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbDiferenciasInfonavitChange(Sender: TObject);
    procedure cbDiferenciasInfonavitClick(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaContFiltrosInfo( const lSetVisibles: Boolean);
    procedure LimpiaFiltros;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;

  public
    { Public declarations }
  end;


const
     K_NOMBRE_FORMA = 'diferencias en datos INFONAVIT';

var
  DiferenciasDatosINFONAVIT: TDiferenciasDatosINFONAVIT;

implementation

{$R *.dfm}


uses
     FTressShell,
     ZetaCommonTools, ZetaCommonClasses,
     DConciliador;

procedure TDiferenciasDatosINFONAVIT.cbDiferenciasInfonavitChange(
  Sender: TObject);
var
   sFiltro: String;
begin
     inherited;
     HabilitaContFiltrosInfo( FALSE );
     LimpiaFiltros;

     with TressShell do
     begin
          DiferenciasFiltrarFecha := lFiltrarPorInicio.Checked;
          DiferenciasFechaFiltro := zFechaFiltro.Valor;
     end;
end;

procedure TDiferenciasDatosINFONAVIT.LimpiaFiltros;
var
   sFiltro: String;
begin
     //if ( GridInfonavit.DataSource.DataSet <> NIL ) then
     if ( DataSource.DataSet <> NIL ) then
     begin
          //with GridInfonavit.DataSource.DataSet do
          with ZetaDBGridDBTableView.DataController.DataSource.DataSet do
          begin
               if Active then
               begin
                    case cbDiferenciasInfonavit.ItemIndex of
                         0: sFiltro:= VACIO;
                         1: sFiltro:= Format( 'DESCRIPCION = %s ', [EntreComillas( K_INICIO_CREDITO )] );
                         2: sFiltro:= Format( 'DESCRIPCION = %s ', [EntreComillas( K_NUMERO_CREDITO ) ] );
                         3: sFiltro:= Format( 'DESCRIPCION = %s ', [EntreComillas( K_TIPO_DESCUENTO ) ]);
                         4: sFiltro:= Format( 'DESCRIPCION = %s ', [EntreComillas( K_VALOR_DESCUENTO )] );
                    end;

                    Filtered := FALSE;
                    if lFiltrarPorInicio.Checked then
                       sFiltro:= ConcatFiltros(sFiltro, Format('CB_INF_INI >= %s',[EntreComillas( FechaAsStr(zFechaFiltro.Valor) ) ] ));
                    Filter := sFiltro;
                    Filtered := StrLleno(sFiltro);
               end;
          end;
     end;
end;

procedure TDiferenciasDatosINFONAVIT.cbDiferenciasInfonavitClick(
  Sender: TObject);
begin
     inherited;
     TressShell.DiferenciasFiltro := cbDiferenciasInfonavit.Indice;
end;

procedure TDiferenciasDatosINFONAVIT.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsInfonavit.DataSet;
end;


procedure TDiferenciasDatosINFONAVIT.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n empleado con error en datos de INFONAVIT'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

     ZetaDBGridDBTableView.OptionsData.Editing := False;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

     {SUA 3.2.5: INFONAVIT estar� revisando los cr�ditos que se otorgaron a partir del 01/Feb/2008
     y si el empleado tuvo cambios en su salario base, el factor de pago se ajustar� conforme a su nuevo salario}
     zFechaFiltro.Valor:= EncodeDate(2008,02,1);

     HelpContext := H_CONCIL_DIF_INFONAVIT;
end;

procedure TDiferenciasDatosINFONAVIT.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount );
     ZetaDBGridDBTableView.ApplyBestFit();
     LimpiaFiltros;
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

function TDiferenciasDatosINFONAVIT.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TDiferenciasDatosINFONAVIT.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TDiferenciasDatosINFONAVIT.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TDiferenciasDatosINFONAVIT.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TDiferenciasDatosINFONAVIT.Refresh;
begin

end;

procedure TDiferenciasDatosINFONAVIT.HabilitaContFiltrosInfo( const lSetVisibles: Boolean);
var
   lHayRegistros: Boolean;
begin
     //with dsInfonavit do
     with TressShell.dsInfonavit do
          lHayRegistros:= ( not lSetVisibles ) or ( ( DataSet <> Nil ) and not ( DataSet.IsEmpty  ) );
     //lblFechaFiltro.Enabled:= ListViewInfo.Enabled and lFiltrarPorInicio.Checked and lHayRegistros;
     lblFechaFiltro.Enabled:= lFiltrarPorInicio.Checked and lHayRegistros;
     //zFechaFiltro.Enabled:= ListViewInfo.Enabled and lFiltrarPorInicio.Checked and lHayRegistros;
     zFechaFiltro.Enabled:= lFiltrarPorInicio.Checked and lHayRegistros;
     //gbFiltrosInfo.Visible:= ListViewInfo.Enabled and lHayRegistros;
     gbFiltrosInfo.Visible:= lHayRegistros;
     //PanelActualizaInfo.Visible:= ListViewInfo.Enabled and lHayRegistros;  //se usa en el wizard
end;


end.
