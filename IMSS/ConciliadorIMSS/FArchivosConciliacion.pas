unit FArchivosConciliacion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  TressMorado2013, cxClasses, cxShellBrowserDialog, cxGroupBox, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxLabel, cxTextEdit;

type
  TArchivosConciliacion = class(TBaseConsulta)
    gbDatosSua: TcxGroupBox;
    gbDatosIMSS: TcxGroupBox;
    ShellBrowserDialogDirectorioIMSS: TcxShellBrowserDialog;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    btnBaseDatosSUA: TcxButton;
    btnDirectorioIMSS: TcxButton;
    BaseDatosSUA: TcxTextEdit;
    DirectorioIMSS: TcxTextEdit;
    OpenDialog: TOpenDialog;
    procedure btnBaseDatosSUAClick(Sender: TObject);
    procedure btnDirectorioIMSSClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BaseDatosSUAPropertiesEditValueChanged(Sender: TObject);
    procedure DirectorioIMSSPropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Connect; override;
    procedure Refresh; override;

  public
    { Public declarations }
  end;


const
     K_NOMBRE_FORMA = 'archivos de conciliación';

var
  ArchivosConciliacion: TArchivosConciliacion;

implementation

{$R *.dfm}

 uses
     ZetaRegistryCliente, ZetaCommonTools, DCliente, ZetaDialogo, FTressShell,
     ZetaCommonClasses;



procedure TArchivosConciliacion.Refresh;
begin
     BaseDatosSUA.Text := ClientRegistry.ConciliadorIMSSBaseDatosSUA;
     DirectorioIMSS.Text :=  ClientRegistry.ConciliadorIMSSDirectorioIMSS;
end;


 procedure TArchivosConciliacion.btnDirectorioIMSSClick(Sender: TObject);
begin
      with ShellBrowserDialogDirectorioIMSS do
      begin
           if ( DirectorioIMSS.Text = '' ) then
              ShellBrowserDialogDirectorioIMSS.Path := VerificaDir( ExtractFilePath(Application.ExeName) )
           else
               ShellBrowserDialogDirectorioIMSS.Path := DirectorioIMSS.Text;

          if Execute then
                begin
                      try
                         ClientRegistry.ConciliadorIMSSDirectorioIMSS := ShellBrowserDialogDirectorioIMSS.Path;
                         DirectorioIMSS.Text := ShellBrowserDialogDirectorioIMSS.Path;
                      except
                            on Error: Exception do
                            begin
                                 ZetaDialogo.zInformation( '', 'No se tienen los permisos requeridos para guardar esta configuración, firmarse con otro usuario.', 0 );
                            end;
                      end;
                end;
      end;
end;


procedure TArchivosConciliacion.BaseDatosSUAPropertiesEditValueChanged(
  Sender: TObject);
begin
     TressShell.RefrescaConfiguracionActivos;
end;

procedure TArchivosConciliacion.btnBaseDatosSUAClick(Sender: TObject);
begin
     with OpenDialog do
     begin
          if ( BaseDatosSUA.Text = '' )  then
             InitialDir :=  VerificaDir( ExtractFilePath(Application.ExeName) )
          else
          begin
               FileName := BaseDatosSUA.Text;
               InitialDir := ExtractFilePath( BaseDatosSUA.Text );
          end;

          if Execute then
          begin
               try
                  if FileExists ( FileName ) then
                  begin
                       ClientRegistry.ConciliadorIMSSBaseDatosSUA := FileName;
                       BaseDatosSUA.Text := FileName;
                  end
                  else
                      ZetaDialogo.zInformation( '', 'El archivo seleccionado no existe.', 0 );

               except
                     on Error: Exception do
                     begin
                          ZetaDialogo.zInformation( '', 'No se tienen los permisos requeridos para guardar esta configuración, firmarse con otro usuario.', 0 );
                     end;
               end;
          end;
     end;
end;

procedure TArchivosConciliacion.Connect;
 begin

 end;


function TArchivosConciliacion.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TArchivosConciliacion.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TArchivosConciliacion.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TArchivosConciliacion.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TArchivosConciliacion.DirectorioIMSSPropertiesEditValueChanged(Sender: TObject);
begin
     TressShell.RefrescaConfiguracionActivos;
end;

procedure TArchivosConciliacion.FormCreate(Sender: TObject);
begin
     BaseDatosSUA.Text := ClientRegistry.ConciliadorIMSSBaseDatosSUA;
     DirectorioIMSS.Text :=  ClientRegistry.ConciliadorIMSSDirectorioIMSS;
     HelpContext := H_CONCIL_ARCHIVOS_CONCILIACION;
end;

end.
