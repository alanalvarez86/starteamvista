unit FIMSSVerificaDiferenciasDatosINFONAVITGridSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls;

type
  TIMSSVerificaDiferenciasDatosINFONAVITGridSelect = class(TBasicoGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    STATUS_EMP: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    TRESS: TcxGridDBColumn;
    CD_IMSS: TcxGridDBColumn;
    CB_FEC_ING: TcxGridDBColumn;
    CB_FEC_BAJ: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
   IMSSVerificaDiferenciasDatosINFONAVITGridSelect: TIMSSVerificaDiferenciasDatosINFONAVITGridSelect;

implementation

{$R *.dfm}

procedure TIMSSVerificaDiferenciasDatosINFONAVITGridSelect.FormCreate(
  Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= False;
end;

procedure TIMSSVerificaDiferenciasDatosINFONAVITGridSelect.FormShow(
  Sender: TObject);
begin
     inherited;
     (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).Caption := 'N�mero';
     (ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO')).MinWidth := 70;
end;

end.
