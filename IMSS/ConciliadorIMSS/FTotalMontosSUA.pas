unit FTotalMontosSUA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, DB, cxDBData, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, StdCtrls, cxButtons, ExtCtrls,
  ActnList, ComCtrls, ZetaStateComboBox, System.Actions,  ZBaseDlgModal_DevEx,
  ZetaRegistryServer, cxTextEdit, cxPCdxBarPopupMenu, cxPC, FEditMontosConceptos_DevEx,ZetaRegistryCliente;

type
  TTotalMontosSUA = class(TBaseGridLectura_DevEx)
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    TRESS: TcxGridDBColumn;
    CD_IMSS: TcxGridDBColumn;
    Footer: TPanel;
    Configura_DevEx: TcxButton;
    CB_CODIGO: TcxGridDBColumn;
    Exportar_DevEx: TcxButton;
    SaveDialog: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Configura_DevExClick(Sender: TObject);
    procedure Exportar_DevExClick(Sender: TObject);

  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;

  public
    { Public declarations }
  end;


const
     K_NOMBRE_FORMA = 'el total montos SUA';

var
  TotalMontosSUA: TTotalMontosSUA;

implementation


{$R *.dfm}


uses
     FTressShell, ZetaCommonClasses,ZetaCommonTools,ZetaClientTools,ZetaDialogo,dCliente;


procedure TTotalMontosSUA.FormCreate(Sender: TObject);
begin
  inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontraron diferencias'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

      ZetaDBGridDBTableView.OptionsData.Editing := False;
     // HelpContext := H_CONCIL_DIF_TOTAL;
 end;


procedure TTotalMontosSUA.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
     Exportar_DevEx.Enabled :=  TressShell.ConciliacionTerminada;
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

procedure TTotalMontosSUA.Configura_DevExClick(Sender: TObject);
{$IFDEF FROMEX}
var
   EditMontosConceptos_DevEx : TZetaDlgModal_DevEx;
{$ENDIF}
begin
{$IFDEF FROMEX}
     inherited;
     EditMontosConceptos_DevEx := TEditMontosConceptos_DevEx.Create( self );
     try
        EditMontosConceptos_DevEx.ShowModal;
     finally
            FreeAndNil( EditMontosConceptos_DevEx );
     end;
{$ENDIF}
end;


function TTotalMontosSUA.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TTotalMontosSUA.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TTotalMontosSUA.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TTotalMontosSUA.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TTotalMontosSUA.Refresh;
begin

end;


procedure TTotalMontosSUA.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsDatosEmpleadosTodos.DataSet;
end;

procedure TTotalMontosSUA.Exportar_DevExClick(Sender: TObject);
  var
   sFileName: String;
   oCursor: TCursor;

   function ValidaConceptos : Boolean;
   begin
        Result := ((StrLleno(dmCliente.ImssSubtotal) ) and
                   (StrLleno(dmCliente.Retiro)) and
                   (StrLleno(dmCliente.CesantiaYVejez)) and
                   (StrLleno(dmCliente.InfonavitPatronal)) and
                   (StrLleno(dmCliente.Amortizacion)))
   end;
begin
     inherited;
     dmcliente.CargaMontosConceptos;
     sFileName := Format( '%sSUA-MONTOS-P%s-%d-%d-%d%d.dat', [ ExtractFilePath( Application.ExeName ), ClientRegistry.ConciliadorIMSSPatron,
                          ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSYear), ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes),
                          ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSTipo),
                          ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSClaseEmision)] );

     with SaveDialog do
     begin
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          if ( ValidaConceptos ) then
          begin
               if Execute then
               begin
                    sFileName := FileName;
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                        TressShell.GenerarResultadosMontosSUA;
                        TressShell.ResultadosMontosSUA.SaveToFile( sFileName );

                    finally
                           Screen.Cursor := oCursor;
                    end;
                    if ZetaDialogo.zConfirm( Caption, 'El archivo ' + sFileName + ' fu� creado' + CR_LF + '� Desea verlo ?', 0, mbYes ) then
                    begin
                         ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sFileName, ExtractFileDir( sFileName ), SW_SHOWDEFAULT );
                    end;
               end;
          end
          else
               ZetaDialogo.ZError( 'Guardar Excepciones de Montos',
                                   'No es posible exportar los montos a excepciones de n�mina, No est�n configurados correctamente los conceptos en la empresa activa.',
                                   0);

     end;
end;



end.
