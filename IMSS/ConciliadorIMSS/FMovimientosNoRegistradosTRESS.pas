unit FMovimientosNoRegistradosTRESS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxCalendar;

type
  TMovimientosNoRegistradosTRESS = class(TBaseGridLectura_DevEx)
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    LM_TIPO_MOV: TcxGridDBColumn;
    LM_FECHA: TcxGridDBColumn;
    LM_BASE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;

  public
    { Public declarations }
  end;


const
     K_NOMBRE_FORMA = 'movimientos no registrados en TRESS';
var
  MovimientosNoRegistradosTRESS: TMovimientosNoRegistradosTRESS;

implementation

{$R *.dfm}

uses
     FTressShell, ZetaCommonClasses;


procedure TMovimientosNoRegistradosTRESS.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsMovtosImss.DataSet;
end;

procedure TMovimientosNoRegistradosTRESS.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n movimiento que no est� registrado en Tress'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';
     ZetaDBGridDBTableView.OptionsData.Editing := False;
     HelpContext := H_CONCIL_MOV_NO_REG_TRESS;
end;

procedure TMovimientosNoRegistradosTRESS.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('PRETTYNAME'), K_SIN_TIPO , '', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     ZetaDBGridDBTableView.ApplyBestFit();
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

function TMovimientosNoRegistradosTRESS.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TMovimientosNoRegistradosTRESS.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TMovimientosNoRegistradosTRESS.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TMovimientosNoRegistradosTRESS.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TMovimientosNoRegistradosTRESS.Refresh;
begin

end;

end.
