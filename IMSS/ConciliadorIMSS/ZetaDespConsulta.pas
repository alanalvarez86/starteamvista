unit ZetaDespConsulta;

interface

uses
    ZBaseConsulta, ZAccesosTress;
    
type
  eFormaConsulta = ( efcNinguna,
                     efcDiferenciasTotales,
                     efcEmpleadosDatosDiferentes,
                     efcDiferenciasCURP,
                     efcEmpNoRegistradosIMSS,
                     efcEmpNoRegistradosTRESS,
                     efcMovtoNoRegistradosIMSS,
                     efcMovtoNoRegistradosTRESS,
                     efcDiferenciasDatosINFONAVIT,
                     efcCreditosNoRegistradosTRESS,
                     efcCreditosNoRegistradosINFONAVIT,
                     efcCreditosSuspendidosTRESS,
                     efcArchivosConciliacion
                     {$ifdef FROMEX}
                     ,efcEmpleadosSua
                     {$ENDIF}
     );
    TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;


  function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;


implementation

uses
    FDiferenciasTotales,
    FEmpleadosDatosDiferentes,
    FDiferenciasCURP,
    FEmpleadosNoRegistradosIMSS,
    FEmpleadosNoRegistradosTRESS,
    FMovimientosNoRegistradosIMSS,
    FMovimientosNoRegistradosTRESS,
    FDiferenciasDatosINFONAVIT,
    FCreditosNoRegistradosTRESS,
    FCreditosNoRegistradosINFONAVIT,
    FCreditosSuspendidosTRESS,
    FArchivosConciliacion
{$ifdef FROMEX}
    ,FTotalMontosSUA
{$ENDIF}
     ;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     case Forma of
       efcDiferenciasTotales                 : Result := Consulta( D_IMSS, TDiferenciasTotales );
       efcEmpleadosDatosDiferentes           : Result := Consulta( D_IMSS, TEmpleadosDatosDiferentes );
       efcDiferenciasCURP                    : Result := Consulta( D_IMSS, TDiferenciasCURP );
       efcEmpNoRegistradosIMSS               : Result := Consulta( D_IMSS, TEmpleadosNoRegistradosIMSS );
       efcEmpNoRegistradosTRESS              : Result := Consulta( D_IMSS, TEmpleadosNoRegistradosTRESS );
       efcMovtoNoRegistradosIMSS             : Result := Consulta( D_IMSS, TMovimientosNoRegistradosIMSS );
       efcMovtoNoRegistradosTRESS            : Result := Consulta( D_IMSS, TMovimientosNoRegistradosTRESS );
       efcDiferenciasDatosINFONAVIT          : Result := Consulta( D_IMSS, TDiferenciasDatosINFONAVIT );
       efcCreditosNoRegistradosTRESS         : Result := Consulta( D_IMSS, TCreditosNoRegistradosTRESS );
       efcCreditosNoRegistradosINFONAVIT     : Result := Consulta( D_IMSS, TCreditosNoRegistradosINFONAVIT );
       efcCreditosSuspendidosTRESS           : Result := Consulta( D_IMSS, TCreditosSuspendidosTRESS );
       efcArchivosConciliacion               : Result := Consulta( D_IMSS, TArchivosConciliacion );
{$IFDEF FROMEX}
       efcEmpleadosSua                       : Result := Consulta( D_IMSS, TTotalMontosSUA );
{$ENDIF}

     else
         Result := Consulta( 0, nil );
     end;
end;

end.
