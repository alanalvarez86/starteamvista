inherited MovimientosNoRegistradosIMSS: TMovimientosNoRegistradosIMSS
  Left = 0
  Top = 0
  Caption = 'Movimientos no registrados en IMSS'
  ClientHeight = 300
  ClientWidth = 635
  OldCreateOrder = False
  ExplicitWidth = 635
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 635
    ExplicitWidth = 635
    inherited ValorActivo2: TPanel
      Width = 376
      ExplicitWidth = 376
      inherited textoValorActivo2: TLabel
        Width = 370
        ExplicitLeft = 290
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 635
    Height = 281
    ExplicitWidth = 635
    ExplicitHeight = 281
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.Editing = True
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 64
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = '# Seguro Social'
        DataBinding.FieldName = 'CB_SEGSOC'
        Width = 64
      end
      object LM_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'LM_FECHA'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyy'
        Width = 64
      end
      object LM_CLAVE: TcxGridDBColumn
        Caption = 'Clave'
        DataBinding.FieldName = 'LM_CLAVE'
        Width = 35
      end
      object LM_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'LM_DIAS'
        Width = 40
      end
      object LM_INCAPAC: TcxGridDBColumn
        Caption = 'INC'
        DataBinding.FieldName = 'LM_INCAPAC'
        Width = 24
      end
      object LM_AUSENCI: TcxGridDBColumn
        Caption = 'AUS'
        DataBinding.FieldName = 'LM_AUSENCI'
        Width = 27
      end
      object LM_BASE: TcxGridDBColumn
        Caption = 'Base'
        DataBinding.FieldName = 'LM_BASE'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
      end
      object LM_EYM_FIJ: TcxGridDBColumn
        Caption = 'EYM Fijo'
        DataBinding.FieldName = 'LM_EYM_FIJ'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
      end
      object LM_EYM_EXC: TcxGridDBColumn
        Caption = 'EYM Excede'
        DataBinding.FieldName = 'LM_EYM_EXC'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Width = 87
      end
      object LM_EYM_DIN: TcxGridDBColumn
        Caption = 'EYM Dinero'
        DataBinding.FieldName = 'LM_EYM_DIN'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
      end
      object LM_EYM_ESP: TcxGridDBColumn
        Caption = 'EYM Especie'
        DataBinding.FieldName = 'LM_EYM_ESP'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
      end
      object LM_RIESGOS: TcxGridDBColumn
        Caption = 'Riesgos'
        DataBinding.FieldName = 'LM_RIESGOS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Width = 69
      end
      object LM_INV_VID: TcxGridDBColumn
        Caption = 'Inv. y Vida'
        DataBinding.FieldName = 'LM_INV_VID'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Width = 87
      end
      object LM_GUARDER: TcxGridDBColumn
        Caption = 'Guarder'#237'as'
        DataBinding.FieldName = 'LM_GUARDER'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
      end
      object LM_RETIRO: TcxGridDBColumn
        Caption = 'Retiro'
        DataBinding.FieldName = 'LM_RETIRO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
      end
      object LM_CES_VEJ: TcxGridDBColumn
        Caption = 'Cesant'#237'a y Vejez'
        DataBinding.FieldName = 'LM_CES_VEJ'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        Width = 70
      end
      object LM_INF_PAT: TcxGridDBColumn
        Caption = 'INFONAVIT'
        DataBinding.FieldName = 'LM_INF_PAT'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
      end
      object LM_INF_AMO: TcxGridDBColumn
        Caption = 'Amortizaci'#243'n'
        DataBinding.FieldName = 'LM_INF_AMO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
