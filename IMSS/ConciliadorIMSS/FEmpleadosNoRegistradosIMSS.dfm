inherited EmpleadosNoRegistradosIMSS: TEmpleadosNoRegistradosIMSS
  Left = 0
  Top = 0
  Caption = 'Empleados no registrados en IMSS'
  ClientHeight = 297
  ClientWidth = 635
  OldCreateOrder = False
  ExplicitWidth = 635
  ExplicitHeight = 297
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 635
    ExplicitWidth = 635
    inherited ValorActivo2: TPanel
      Width = 376
      ExplicitWidth = 376
      inherited textoValorActivo2: TLabel
        Width = 370
        ExplicitLeft = 290
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 635
    Height = 278
    ParentFont = False
    ExplicitTop = 22
    ExplicitWidth = 635
    ExplicitHeight = 278
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.Editing = True
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 300
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = '# Seguro Social'
        DataBinding.FieldName = 'CB_SEGSOC'
        Width = 130
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
