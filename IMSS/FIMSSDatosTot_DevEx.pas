unit FIMSSDatosTot_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, StdCtrls, ExtCtrls, ComCtrls,
     ZetaDBTextBox, DBCtrls, ZBaseGridLecturaImss_DevEx, cxGraphics,
     cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
      TressMorado2013, dxSkinsDefaultPainters,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
     ZetaStateComboBox, cxGridLevel, cxClasses, cxGridCustomView,
     cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
     ZetaCXGrid, cxPCdxBarPopupMenu, cxContainer, cxGroupBox, cxPC,
  dxBarBuiltInMenu, System.Actions;

type
  TIMSSDatosTot_DevEx = class(TBaseGridLecturaImss_DevEx)
    PageControl1: TcxPageControl;
    TabSheet1: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    GBResumenMensual: TcxGroupBox;
    Label20: TLabel;
    LS_TOT_MES: TZetaDBTextBox;
    Label19: TLabel;
    LS_NUM_TRA: TZetaDBTextBox;
    LS_DIAS_CO: TZetaDBTextBox;
    Label5: TLabel;
    LS_FAC_ACT: TZetaDBTextBox;
    Label24: TLabel;
    Label18: TLabel;
    LS_FAC_REC: TZetaDBTextBox;
    GBCuentaInd: TcxGroupBox;
    Label22: TLabel;
    LS_RETIRO: TZetaDBTextBox;
    Label14: TLabel;
    LS_CES_VEJ: TZetaDBTextBox;
    LS_SUB_RET: TZetaDBTextBox;
    Label21: TLabel;
    Label26: TLabel;
    LS_ACT_RET: TZetaDBTextBox;
    Label23: TLabel;
    LS_REC_RET: TZetaDBTextBox;
    Label25: TLabel;
    LS_APO_VOL: TZetaDBTextBox;
    Label33: TLabel;
    LS_TOT_RET: TZetaDBTextBox;
    GBCuentaInfonavit: TcxGroupBox;
    Label6: TLabel;
    LS_INF_NAC: TZetaDBTextBox;
    Label8: TLabel;
    LS_INF_ACR: TZetaDBTextBox;
    Label9: TLabel;
    LS_INF_AMO: TZetaDBTextBox;
    Label29: TLabel;
    LS_SUB_INF: TZetaDBTextBox;
    Label30: TLabel;
    LS_ACT_INF: TZetaDBTextBox;
    Label31: TLabel;
    LS_REC_INF: TZetaDBTextBox;
    Label32: TLabel;
    LS_TOT_INF: TZetaDBTextBox;
    GBResumenBimestral: TcxGroupBox;
    LS_INF_NUM: TZetaDBTextBox;
    Label16: TLabel;
    LS_NUM_BIM: TZetaDBTextBox;
    Label27: TLabel;
    LS_DIAS_BM: TZetaDBTextBox;
    Label28: TLabel;
    GBCuentaImss: TcxGroupBox;
    Label1: TLabel;
    LS_EYM_FIJ: TZetaDBTextBox;
    Label2: TLabel;
    LS_EYM_EXC: TZetaDBTextBox;
    Label3: TLabel;
    LS_EYM_DIN: TZetaDBTextBox;
    LS_EYM_ESP: TZetaDBTextBox;
    Label4: TLabel;
    Label7: TLabel;
    LS_GUARDER: TZetaDBTextBox;
    Label10: TLabel;
    LS_RIESGOS: TZetaDBTextBox;
    Label11: TLabel;
    LS_INV_VID: TZetaDBTextBox;
    Label12: TLabel;
    LS_SUB_IMS: TZetaDBTextBox;
    Label13: TLabel;
    LS_ACT_IMS: TZetaDBTextBox;
    Label15: TLabel;
    LS_REC_IMS: TZetaDBTextBox;
    Label17: TLabel;
    LS_TOT_IMS: TZetaDBTextBox;
    Label34: TLabel;
    Label35: TLabel;
    LS_IMSS_OB: TZetaDBTextBox;
    LS_IMSS_PA: TZetaDBTextBox;
    GBDecreto20: TcxGroupBox;
    LS_DES_20: TDBCheckBox;
    LS_FINAN: TDBCheckBox;
    lblParcialidad: TLabel;
    LS_PARCIAL: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure GenericoMouseDown(Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Integer);
    procedure GBDblClick(Sender: TObject);

  private
    { Private declarations }
    FDecreto20, FParcialidades: Boolean;
    procedure HabilitaContolesImss;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Imprimir; override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
  end;

var
  IMSSDatosTot_DevEx: TIMSSDatosTot_DevEx;

implementation

uses DIMSS,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TIMSSDatosTot_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo2 := stIMSS;
     HelpContext := H40411_Totales_IMSS;
end;

procedure TIMSSDatosTot_DevEx.Connect;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosTot.Conectar;
          DataSource.DataSet:= cdsIMSSDatosTot;
     end;
     HabilitaContolesImss;
end;

procedure TIMSSDatosTot_DevEx.Refresh;
begin
     dmIMSS.cdsIMSSDatosTot.Refrescar;
end;

procedure TIMSSDatosTot_DevEx.Borrar;
begin
     dmIMSS.cdsIMSSDatosTot.Borrar;
end;

procedure TIMSSDatosTot_DevEx.Modificar;
begin
     dmImss.cdsIMSSDatosTot.Modificar;
end;

function TIMSSDatosTot_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar A Los Totales De IMSS';
end;

function TIMSSDatosTot_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:=( FDecreto20 or FParcialidades );
     if not Result then
        sMensaje := 'No Se Pueden Modificar Los Totales De IMSS';
end;

procedure TIMSSDatosTot_DevEx.HabilitaContolesImss;

    procedure AlineaControles;
    const
         aTopParcialidad: array[FALSE..TRUE] of Integer = (66,42);
    begin
         lblParcialidad.Top:= aTopParcialidad[( not FDecreto20 ) and FParcialidades];
         LS_PARCIAL.Top:= (lblParcialidad.Top - 2);
    end;

begin
     dmImss.AplicaDecreto20( FDecreto20, FParcialidades );
     GBDecreto20.Visible:= FDecreto20 or FParcialidades;
     lblParcialidad.Visible:= FParcialidades;
     LS_PARCIAL.Visible:= FParcialidades;

     if ( GBDecreto20.Visible ) then
     begin
          LS_DES_20.Visible:= FDecreto20;
          LS_FINAN.Visible:= FDecreto20;
          AlineaControles;
     end;
end;

procedure TIMSSDatosTot_DevEx.GenericoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     inherited;
     if ( ssDouble in Shift ) then
        DoEdit;
end;

procedure TIMSSDatosTot_DevEx.GBDblClick(Sender: TObject);
begin
     inherited;
     DoEdit;
end;

procedure TIMSSDatosTot_DevEx.Imprimir;
begin
      if zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

end.
