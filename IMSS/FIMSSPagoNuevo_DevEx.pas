unit FIMSSPagoNuevo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, DBCtrls, StdCtrls,
     ZBaseEdicion_DevEx, ZetaKeyLookup_DevEx, ZetaKeyCombo,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
     dxSkinsCore,  TressMorado2013,
     dxSkinsDefaultPainters, dxSkinsdxBarPainter, cxControls,
     dxBarExtItems, dxBar, cxClasses,
     ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TIMSSPagoNuevo_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    LS_PATRON: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LS_MONTH: TZetaDBKeyCombo;
    LS_YEAR: TZetaDBKeyCombo;
    LS_TIPO: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  IMSSPagoNuevo_DevEx: TIMSSPagoNuevo_DevEx;

implementation

uses DCliente,
     DCatalogos,
     DIMSS,
     ZetaCommonClasses,
     ZetaCommonLists,
     FTressShell;

{$R *.DFM}

{ TIMSSPagoNuevo }

procedure TIMSSPagoNuevo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := LS_PATRON;
     HelpContext := H40431_Pago_IMSS_nuevo;
     LS_PATRON.LookupDataset := dmCatalogos.cdsRPatron;
end;

procedure TIMSSPagoNuevo_DevEx.Connect;
begin
     dmCatalogos.cdsRPatron.Conectar;
     with dmImss do
     begin
          cdsIMSSDatosTot.Conectar;
          DataSource.DataSet := cdsIMSSDatosTot;
     end;
end;

procedure TIMSSPagoNuevo_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          with dmIMSS.cdsIMSSDatosTot do
          begin
               IMSSMes := FieldByName( 'LS_MONTH' ).AsInteger;
               IMSSYear := FieldByName( 'LS_YEAR' ).AsInteger;
               IMSSTipo := eTipoLiqImss( FieldByName( 'LS_TIPO' ).AsInteger );
          end;
     end;
     TressShell.RefrescaIMSS;
end;

end.




