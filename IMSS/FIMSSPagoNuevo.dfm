inherited IMSSPagoNuevo: TIMSSPagoNuevo
  ActiveControl = LS_PATRON
  Caption = 'Pago de IMSS Nuevo'
  ClientHeight = 206
  ClientWidth = 451
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 12
    Top = 64
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = '&Registro Patronal:'
    FocusControl = LS_PATRON
  end
  object Label2: TLabel [1]
    Left = 74
    Top = 88
    Width = 22
    Height = 13
    Alignment = taRightJustify
    Caption = '&A�o:'
    FocusControl = LS_YEAR
  end
  object Label5: TLabel [2]
    Left = 73
    Top = 111
    Width = 23
    Height = 13
    Alignment = taRightJustify
    Caption = '&Mes:'
    FocusControl = LS_MONTH
  end
  object Label6: TLabel [3]
    Left = 72
    Top = 135
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = '&Tipo:'
    FocusControl = LS_TIPO
  end
  inherited PanelBotones: TPanel
    Top = 170
    Width = 451
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 283
    end
    inherited Cancelar: TBitBtn
      Left = 368
    end
  end
  inherited PanelSuperior: TPanel
    Width = 451
    TabOrder = 5
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 451
    TabOrder = 6
    inherited ValorActivo2: TPanel
      Width = 125
    end
  end
  object LS_PATRON: TZetaDBKeyLookup [7]
    Left = 101
    Top = 60
    Width = 340
    Height = 21
    LookupDataset = dmCatalogos.cdsRPatron
    Opcional = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    DataField = 'LS_PATRON'
    DataSource = DataSource
  end
  object LS_MONTH: TZetaDBKeyCombo [8]
    Left = 101
    Top = 107
    Width = 100
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    ListaFija = lfMeses
    ListaVariable = lvPuesto
    Offset = 1
    Opcional = False
    DataField = 'LS_MONTH'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object LS_YEAR: TZetaDBKeyCombo [9]
    Left = 101
    Top = 84
    Width = 100
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    ListaFija = lfYears
    ListaVariable = lvPuesto
    Offset = 1994
    Opcional = False
    DataField = 'LS_YEAR'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object LS_TIPO: TZetaDBKeyCombo [10]
    Left = 101
    Top = 131
    Width = 100
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    ListaFija = lfTipoLiqIMSS
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'LS_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 236
    Top = 89
  end
end
