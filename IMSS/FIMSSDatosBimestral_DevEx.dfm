inherited TIMSSDatosBimestral_DevEx: TTIMSSDatosBimestral_DevEx
  Left = 125
  Top = 131
  Caption = 'IMSS: Datos Bimestrales por Empleado'
  ClientHeight = 339
  ClientWidth = 658
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Top = 30
    Width = 658
    inherited Slider: TSplitter
      Left = 313
    end
    inherited ValorActivo1: TPanel
      Width = 297
      inherited textoValorActivo1: TLabel
        Width = 291
      end
    end
    inherited ValorActivo2: TPanel
      Left = 316
      Width = 342
      inherited textoValorActivo2: TLabel
        Width = 336
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 49
    Width = 658
    Height = 112
    Align = alTop
    TabOrder = 3
    object Label8: TLabel
      Left = 239
      Top = 91
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as Cotizados:'
    end
    object LE_DIAS_BM: TZetaDBTextBox
      Left = 319
      Top = 89
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_DIAS_BM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_DIAS_BM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 4
      Width = 209
      Height = 100
      Caption = ' Cuenta Individual '
      TabOrder = 0
      object Label2: TLabel
        Left = 86
        Top = 18
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Retiro:'
      end
      object LE_RETIRO: TZetaDBTextBox
        Left = 121
        Top = 16
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_RETIRO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_RETIRO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label1: TLabel
        Left = 34
        Top = 37
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cesant'#237'a y Vejez:'
      end
      object LE_CES_VEJ: TZetaDBTextBox
        Left = 121
        Top = 35
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_CES_VEJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_CES_VEJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 13
        Top = 73
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aportaci'#243'n Voluntaria:'
        Visible = False
      end
      object LE_APO_VOL: TZetaDBTextBox
        Left = 121
        Top = 73
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_APO_VOL'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_APO_VOL'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label4: TLabel
        Left = 65
        Top = 54
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total RCV:'
      end
      object LE_TOT_RET: TZetaDBTextBox
        Left = 121
        Top = 54
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_TOT_RET'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_TOT_RET'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object GroupBox2: TGroupBox
      Left = 218
      Top = 4
      Width = 192
      Height = 80
      Caption = ' Cuenta INFONAVIT '
      TabOrder = 1
      object Label6: TLabel
        Left = 55
        Top = 18
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = 'Patronal:'
      end
      object LE_INF_PAT: TZetaDBTextBox
        Left = 101
        Top = 16
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_INF_PAT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_INF_PAT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 34
        Top = 37
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Amortizaci'#243'n:'
      end
      object LE_INF_AMO: TZetaDBTextBox
        Left = 101
        Top = 35
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_INF_AMO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_INF_AMO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 10
        Top = 56
        Width = 87
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total INFONAVIT:'
      end
      object LE_TOT_INF: TZetaDBTextBox
        Left = 101
        Top = 54
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_TOT_INF'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_TOT_INF'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 161
    Width = 658
    Height = 178
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = dsLiq_Mov
      object LM_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'LM_FECHA'
        MinWidth = 75
        Width = 75
        Visible = True
      end
      object LM_CLAVE: TcxGridDBColumn
        Caption = 'Clave'
        DataBinding.FieldName = 'LM_CLAVE'
        MinWidth = 75
        Width = 75
      end
      object LM_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'LM_DIAS'
        MinWidth = 60
        Width = 60
      end
      object LM_INCAPAC: TcxGridDBColumn
        Caption = 'INC'
        DataBinding.FieldName = 'LM_INCAPAC'
        MinWidth = 50
        Width = 60
      end
      object LM_AUSENCI: TcxGridDBColumn
        Caption = 'AUS'
        DataBinding.FieldName = 'LM_AUSENCI'
        MinWidth = 50
        Width = 50
      end
      object LM_BASE: TcxGridDBColumn
        Caption = 'Base'
        DataBinding.FieldName = 'LM_BASE'
        MinWidth = 60
        Width = 60
      end
      object LM_RETIRO: TcxGridDBColumn
        Caption = 'Retiro'
        DataBinding.FieldName = 'LM_RETIRO'
        MinWidth = 60
        Width = 60
      end
      object LM_CES_VEJ: TcxGridDBColumn
        Caption = 'Cesant'#237'a y Vejez'
        DataBinding.FieldName = 'LM_CES_VEJ'
        MinWidth = 100
        Width = 100
      end
      object LM_INF_PAT: TcxGridDBColumn
        Caption = 'INFONAVIT'
        DataBinding.FieldName = 'LM_INF_PAT'
        MinWidth = 80
      end
      object LM_INF_AMO: TcxGridDBColumn
        Caption = 'Amortizaci'#243'n'
        DataBinding.FieldName = 'LM_INF_AMO'
        MinWidth = 95
        Width = 95
      end
    end
  end
  inherited IMSSPanel: TPanel
    Top = 0
    Width = 658
    inherited IMSSYearUpDown: TUpDown
      Hint = 'Cambiar a'#241'o'
      ParentShowHint = False
      ShowHint = True
    end
  end
  inherited DataSource: TDataSource
    Left = 464
    Top = 88
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsLiq_Mov: TDataSource
    Left = 536
    Top = 80
  end
end
