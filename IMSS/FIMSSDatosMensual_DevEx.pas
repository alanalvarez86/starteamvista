unit FIMSSDatosMensual_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, ExtCtrls,
     Controls, Forms, Dialogs, Db, Grids, DBGrids, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox, ZBaseGridLecturaImss_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList, ComCtrls,
  ZetaStateComboBox, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid;

type
  TIMSSDatosMensual_DevEx = class(TBaseGridLecturaImss_DevEx)
    dsLiq_Mov: TDataSource;
    Panel1: TPanel;
    LE_INV_VID: TZetaDBTextBox;
    LE_RIESGOS: TZetaDBTextBox;
    LE_GUARDER: TZetaDBTextBox;
    LE_TOT_IMS: TZetaDBTextBox;
    LE_DIAS_CO: TZetaDBTextBox;
    Label1: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label10: TLabel;
    LE_IMSS_OB: TZetaDBTextBox;
    Label11: TLabel;
    LE_IMSS_PA: TZetaDBTextBox;
    Label12: TLabel;
    LE_APO_VOL: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
  end;

var
  IMSSDatosMensual_DevEx: TIMSSDatosMensual_DevEx;

implementation

uses DIMSS,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

{ TIMSSDatosMensual }

procedure TIMSSDatosMensual_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stIMSS;
     HelpContext := H40412_IMSS_mensual_empleado;
end;

procedure TIMSSDatosMensual_DevEx.Connect;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosMen.Conectar;
          cdsMovMen.Conectar;
          DataSource.DataSet:= cdsIMSSDatosMen;
          dsLiq_Mov.DataSet:= cdsMovMen;
     end;
end;

procedure TIMSSDatosMensual_DevEx.Refresh;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosMen.Refrescar;
          cdsMovMen.Refrescar;
     end;
end;

function TIMSSDatosMensual_DevEx.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No Se Puede Agregar A Datos Mensuales IMSS';
end;

function TIMSSDatosMensual_DevEx.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No Se Puede Borrar En Datos Mensuales IMSS';
end;

function TIMSSDatosMensual_DevEx.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No Se Puede Modificar En Datos Mensuales IMSS';
end;

procedure TIMSSDatosMensual_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;

end;

end.
