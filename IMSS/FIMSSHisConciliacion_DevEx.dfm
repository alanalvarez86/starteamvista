inherited IMSSHisConciliacion_DevEx: TIMSSHisConciliacion_DevEx
  Left = 38
  Top = 214
  Caption = 'IMSS: Conciliaci'#243'n INFONAVIT por Empleado '
  ClientHeight = 347
  ClientWidth = 1502
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Top = 30
    Width = 1502
    inherited Slider: TSplitter
      Left = 313
    end
    inherited ValorActivo1: TPanel
      Width = 297
      inherited textoValorActivo1: TLabel
        Width = 291
      end
    end
    inherited ValorActivo2: TPanel
      Left = 316
      Width = 1186
      inherited textoValorActivo2: TLabel
        Width = 1180
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 49
    Width = 1502
    Height = 169
    Align = alTop
    TabOrder = 3
    object Label8: TLabel
      Left = 39
      Top = 91
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as Cotizados:'
    end
    object LE_DIAS_BM: TZetaDBTextBox
      Left = 119
      Top = 89
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_DIAS_BM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_DIAS_BM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object GroupBox2: TGroupBox
      Left = 18
      Top = 4
      Width = 192
      Height = 80
      Caption = ' Cuenta INFONAVIT '
      TabOrder = 0
      object Label6: TLabel
        Left = 55
        Top = 18
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = 'Patronal:'
      end
      object LE_INF_PAT: TZetaDBTextBox
        Left = 101
        Top = 16
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_INF_PAT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_INF_PAT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 34
        Top = 37
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Amortizaci'#243'n:'
      end
      object LE_INF_AMO: TZetaDBTextBox
        Left = 101
        Top = 35
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_INF_AMO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_INF_AMO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 10
        Top = 56
        Width = 87
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total INFONAVIT:'
      end
      object LE_TOT_INF: TZetaDBTextBox
        Left = 101
        Top = 54
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_TOT_INF'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_TOT_INF'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object gbConciliacionBimestral: TGroupBox
      Left = 224
      Top = 4
      Width = 449
      Height = 157
      Caption = 'Conciliaci'#243'n INFONAVIT:'
      TabOrder = 1
      object LE_BIM_CALC: TZetaDBTextBox
        Left = 128
        Top = 128
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_BIM_CALC'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_BIM_CALC'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 120
        Top = 96
        Width = 3
        Height = 13
      end
      object Label10: TLabel
        Left = 10
        Top = 128
        Width = 113
        Height = 13
        Caption = 'Amortizaci'#243'n Calculada:'
      end
      object Label11: TLabel
        Left = 28
        Top = 20
        Width = 95
        Height = 13
        Caption = 'Acumulado N'#243'mina:'
      end
      object LE_ACUMULA: TZetaDBTextBox
        Left = 128
        Top = 20
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_ACUMULA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_ACUMULA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object GroupBox4: TGroupBox
        Left = 16
        Top = 40
        Width = 201
        Height = 81
        Caption = 'Descuentos por Bimestre:'
        TabOrder = 0
        object Label12: TLabel
          Left = 66
          Top = 16
          Width = 39
          Height = 13
          Caption = 'Anterior:'
        end
        object Label13: TLabel
          Left = 72
          Top = 35
          Width = 33
          Height = 13
          Caption = 'Actual:'
        end
        object Label14: TLabel
          Left = 58
          Top = 56
          Width = 47
          Height = 13
          Caption = 'Siguiente:'
        end
        object LE_BIM_ANT: TZetaDBTextBox
          Left = 112
          Top = 16
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LE_BIM_ANT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_BIM_ANT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LE_BIM_ACTUAL: TZetaDBTextBox
          Left = 112
          Top = 35
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LE_BIM_ACTUAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_BIM_ACTUAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LE_BIM_SIG: TZetaDBTextBox
          Left = 112
          Top = 56
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LE_BIM_SIG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_BIM_SIG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label15: TLabel
          Left = 16
          Top = 16
          Width = 20
          Height = 13
          Caption = '(+/-)'
        end
        object Label16: TLabel
          Left = 16
          Top = 35
          Width = 18
          Height = 13
          Caption = '( = )'
        end
        object Label17: TLabel
          Left = 16
          Top = 56
          Width = 20
          Height = 13
          Caption = '(+/-)'
        end
      end
      object GroupBox5: TGroupBox
        Left = 224
        Top = 14
        Width = 217
        Height = 129
        TabOrder = 1
        object Label18: TLabel
          Left = 6
          Top = 16
          Width = 115
          Height = 13
          Caption = 'Diferencia por Provisi'#243'n:'
        end
        object LE_DIF_PROV: TZetaDBTextBox
          Left = 125
          Top = 16
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LE_DIF_PROV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_DIF_PROV'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label19: TLabel
          Left = 17
          Top = 35
          Width = 104
          Height = 13
          Caption = 'Vacaciones Pagadas:'
        end
        object LE_VA_PAGO: TZetaDBTextBox
          Left = 125
          Top = 35
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LE_VA_PAGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_VA_PAGO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label20: TLabel
          Left = 17
          Top = 56
          Width = 104
          Height = 13
          Caption = 'Vacaciones Gozadas:'
        end
        object LE_VA_GOZO: TZetaDBTextBox
          Left = 125
          Top = 56
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LE_VA_GOZO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_VA_GOZO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LE_PROV: TZetaDBTextBox
          Left = 125
          Top = 76
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LE_PROV'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_PROV'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label21: TLabel
          Left = 19
          Top = 76
          Width = 102
          Height = 13
          Caption = 'Prov. de Vacaciones:'
        end
      end
    end
    object gbConciliacionSoloAcumula: TGroupBox
      Left = 224
      Top = 4
      Width = 465
      Height = 116
      Caption = 'Conciliaci'#243'n INFONAVIT:'
      TabOrder = 2
      object Label2: TLabel
        Left = 87
        Top = 85
        Width = 51
        Height = 13
        Caption = 'Diferencia:'
      end
      object LE_DIF_ACUM: TZetaDBTextBox
        Left = 142
        Top = 83
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_DIF_ACUM'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_DIF_ACUM'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object GroupBox7: TGroupBox
        Left = 240
        Top = 16
        Width = 217
        Height = 85
        TabOrder = 0
        object Label28: TLabel
          Left = 17
          Top = 14
          Width = 104
          Height = 13
          Caption = 'Vacaciones Pagadas:'
        end
        object difLE_VA_PAGO: TZetaDBTextBox
          Left = 125
          Top = 14
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'difLE_VA_PAGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_VA_PAGO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label29: TLabel
          Left = 17
          Top = 35
          Width = 104
          Height = 13
          Caption = 'Vacaciones Gozadas:'
        end
        object difLE_VA_GOZO: TZetaDBTextBox
          Left = 125
          Top = 35
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'difLE_VA_GOZO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_VA_GOZO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object difLE_PROV: TZetaDBTextBox
          Left = 125
          Top = 55
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'difLE_PROV'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_PROV'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label30: TLabel
          Left = 19
          Top = 55
          Width = 102
          Height = 13
          Caption = 'Prov. de Vacaciones:'
        end
      end
      object GroupBox6: TGroupBox
        Left = 8
        Top = 16
        Width = 225
        Height = 60
        TabOrder = 1
        object Label1: TLabel
          Left = 128
          Top = 38
          Width = 3
          Height = 13
        end
        object Label3: TLabel
          Left = 36
          Top = 35
          Width = 95
          Height = 13
          Caption = 'Acumulado N'#243'mina:'
        end
        object difLE_ACUMULA: TZetaDBTextBox
          Left = 134
          Top = 35
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'difLE_ACUMULA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_ACUMULA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 18
          Top = 14
          Width = 113
          Height = 13
          Caption = 'Amortizaci'#243'n Calculada:'
        end
        object difLE_INF_AMO: TZetaDBTextBox
          Left = 134
          Top = 14
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'difLE_INF_AMO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LE_INF_AMO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label22: TLabel
          Left = 8
          Top = 35
          Width = 9
          Height = 13
          Caption = '(-)'
        end
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 218
    Width = 1502
    Height = 129
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = dsLiq_Emp
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Registro Patronal'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 100
        Width = 129
      end
      object LE_DIAS_BMField: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'LE_DIAS_BM'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 50
        Width = 50
      end
      object LE_TOT_INFfield: TcxGridDBColumn
        Caption = 'INFONAVIT'
        DataBinding.FieldName = 'LE_TOT_INF'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 80
      end
      object LE_INF_AMOfield: TcxGridDBColumn
        Caption = 'Amortizaci'#243'n'
        DataBinding.FieldName = 'LE_INF_AMO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 90
        Width = 90
      end
    end
  end
  inherited IMSSPanel: TPanel
    Top = 0
    Width = 1502
  end
  inherited DataSource: TDataSource
    Left = 264
    Top = 144
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsLiq_Emp: TDataSource
    Left = 312
    Top = 136
  end
end