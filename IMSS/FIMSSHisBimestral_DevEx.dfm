inherited IMSSHisBimestral_DevEx: TIMSSHisBimestral_DevEx
  Left = 208
  Top = 139
  Caption = 'IMSS: Historia Bimestral por Empleado'
  ClientHeight = 350
  ClientWidth = 676
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Top = 30
    Width = 676
    inherited Slider: TSplitter
      Left = 362
    end
    inherited ValorActivo1: TPanel
      Width = 346
      inherited textoValorActivo1: TLabel
        Width = 340
      end
    end
    inherited ValorActivo2: TPanel
      Left = 365
      Width = 311
      inherited textoValorActivo2: TLabel
        Width = 305
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 49
    Width = 676
    Height = 82
    Align = alTop
    TabOrder = 3
    object LS_TOT_RET: TZetaDBTextBox
      Left = 345
      Top = 12
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_TOT_RET'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_TOT_RET'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LS_TOT_INF: TZetaDBTextBox
      Left = 345
      Top = 31
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_TOT_INF'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_TOT_INF'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label3: TLabel
      Left = 255
      Top = 33
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total INFONAVIT:'
    end
    object Label1: TLabel
      Left = 290
      Top = 14
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total RCV:'
    end
    object LS_DIAS_BM: TZetaDBTextBox
      Left = 150
      Top = 50
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_DIAS_BM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_DIAS_BM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 72
      Top = 52
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as Cotizados:'
    end
    object Label2: TLabel
      Left = 88
      Top = 33
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Asegurados:'
    end
    object LS_NUM_BIM: TZetaDBTextBox
      Left = 150
      Top = 31
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_NUM_BIM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_NUM_BIM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LS_INF_NUM: TZetaDBTextBox
      Left = 150
      Top = 12
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_INF_NUM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_INF_NUM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label5: TLabel
      Left = 88
      Top = 14
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Acreditados:'
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 131
    Width = 676
    Height = 219
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = dsLiq_Emp
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 70
        Width = 80
      end
      object PrettyName: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PrettyName'
        MinWidth = 100
        Width = 297
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = 'NSS'
        DataBinding.FieldName = 'CB_SEGSOC'
        MinWidth = 60
        Width = 73
      end
      object LE_TOT_RET: TcxGridDBColumn
        Caption = 'Total RCV'
        DataBinding.FieldName = 'LE_TOT_RET'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 70
      end
      object LE_TOT_INF: TcxGridDBColumn
        Caption = 'Total INFONAVIT'
        DataBinding.FieldName = 'LE_TOT_INF'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 110
        Width = 110
      end
      object LE_DIAS_BM: TcxGridDBColumn
        Caption = 'D'#237'as Cotizados'
        DataBinding.FieldName = 'LE_DIAS_BM'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 100
        Width = 105
      end
    end
  end
  inherited IMSSPanel: TPanel
    Top = 0
    Width = 676
  end
  inherited DataSource: TDataSource
    Left = 480
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsLiq_Emp: TDataSource
    Left = 512
    Top = 56
  end
end