unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBClient,Variants,
     {$ifndef DOS_CAPAS}
     Kiosco_TLB,
    {$else}
     DServerKiosco,
     {$endif}
     DBasicoCliente,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet,
     ZetaCommonTools,CheckLst,
     qrprntr;

type
  TdmCliente = class(TBasicoCliente)
    cdsEmpleadoLookUp: TZetaLookupDataSet;
    cdsEmpresasKiosco: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsEmpleadoLookUpLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsEmpleadoLookUpLookupKey(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter, sKey: String;
      var sDescription: String);
    procedure cdsEmpresasKioscoAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FEmpresaPass: string;
    {$ifdef DOS_CAPAS}
    FServerKiosco: TdmServerKiosco;
    {$else}
    FServerKiosco: IdmServerKioscoDisp;
    FQrPreview: TQRPreview;
    function GetServerKiosco: IdmServerKioscoDisp;
    {$endif}
    procedure SetEmpresaPass( const Value: string );
  protected
    { Protected declarations }
  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerKiosco: TdmServerKiosco read FServerKiosco;
    {$else}
    property ServerKiosco: IdmServerKioscoDisp read GetServerKiosco;
    {$endif}
    property QrPreview: TQrPreview read FQrPreview write FQrPreview;
    property EmpresaPass : string read FEmpresaPass write SetEmpresaPass;
    function ModoTress: Boolean;
    function GetListaEmpleados( const sFiltro: String ): String;
    function InitComparteCompany: Integer;
    function IsOk( const iValue: Integer ): Boolean;
    function ComputerName: string;
    function ListaCompanies(Lista: TStrings): Boolean;
    procedure CargaListaEmpresas(var Lista: TCheckListBox);
     //Para Compilar en 2.8
    //procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
    procedure GetEmpleadosBuscados(const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    function GetURLPantalla( const sURL : string ): string;
    function GetEmpresas: OleVariant;
  end;

var
  dmCliente: TdmCliente;

implementation

uses DSistema,
     ZetaBuscaEmpleado,
     ZetaRegistryCliente,
     ZBaseEdicion;

const
     K_ARROBA_SERVIDOR = '@SERVIDOR';

{$R *.DFM}

{ ***** TdmCliente ***** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     {$ifdef DOS_CAPAS}
     FServerKiosco := TdmServerKiosco.Create( Self );
     {$endif}
     TipoCompany := tcOtro;
     FEmpresaPass:= VACIO;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerKiosco );
     {$endif}
     inherited;
end;

function TdmCliente.ModoTress: Boolean;
begin
     Result := False;
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerKiosco: IdmServerKioscoDisp;
begin
     Result := IdmServerKioscoDisp( CreaServidor( CLASS_dmServerKiosco, FServerKiosco ) );
end;
{$endif}
 //Para Compilar en 2.8
//procedure TdmCliente.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
procedure TdmCliente.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
begin
     {
     if ( enFolio in Entidades ) then
        cdsFolios.SetDataChange;
     }
end;

function TdmCliente.GetListaEmpleados(const sFiltro: string): string;
begin
     Result := '';
end;

function TdmCliente.InitComparteCompany: Integer;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := ServerKiosco.GetComparte;
               ResetDataChange;
          end;
          if Active then
             Result := RecordCount
          else
              Result := 0;
     end;
end;

procedure TdmCliente.MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     { Se requiere condicionar para que al Hacer un }
     { cdsNomParam.Cancel no ocurra un Invalid BLOB Handle }
     if Assigned( Sender ) and Assigned( Sender.Dataset ) then
     begin
          if Sender.Dataset.State in [ dsBrowse ] then
             Text := Sender.AsString;
     end;
end;

function TdmCliente.IsOk( const iValue: Integer ): Boolean;
begin
     Result := ( iValue = 0 );
end;

function TdmCliente.ComputerName: string;
begin
     Result := ClientRegistry.ComputerName;
end;

function TdmCliente.ListaCompanies( Lista: TStrings ): Boolean;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             with cdsCompany do
             begin
                  Data := Servidor.GetCompanys( Usuario, ord( tc3Datos ) );
                  while not Eof do
                  begin
                       Add( Format( '%s=%s', [ FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ) );
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
          Result := ( Count > 0 );
     end;
end;

procedure TdmCliente.CargaListaEmpresas(var Lista: TCheckListBox);
 var
    sDigito:Char;
    DigitoActual:string;
    AddConfidencial:Boolean;
    AddSinConfidencial:Boolean;

    function ExisteDigitoEmpresa (sDigito: string ; const EsSinConfidencial:Boolean):Boolean;
    var
       i:Integer;
    begin
         with Lista do
         begin
              Result := False;
              i := 0;
              While( ( i < Items.Count ) )  do
              begin
                   if ( Chr( Integer( Items.Objects[i] ) ) = sDigito ) then
                   begin
                        //Quitar la base de datos del mismo Digito y agregar
                        If EsSinConfidencial and not AddSinConfidencial then
                        begin
                             with Items do
                             begin
                                  BeginUpdate;
                                  try
                                     Delete(i);
                                  finally
                                         EndUpdate;
                                  end;
                             end;
                             Result := False;
                        end
                        else
                            Result := ( ( not EsSinConfidencial and AddConfidencial ) or AddSinConfidencial);
                   end;
                   i:= i+1;
              end;
         end;
    end;
begin
     with Lista.Items do
     begin
          BeginUpdate;
          try
             Clear;
             DigitoActual:= VACIO;
             with dmCliente.cdsCompany do
             begin
                  AddConfidencial:= False;
                  AddSinConfidencial := False;
                  Data := dmCliente.Servidor.GetCompanys( 0, Ord( tc3Datos ) );
                  while not Eof do
                  begin
                       if StrLleno( FieldByName( 'CM_DIGITO' ).AsString ) and ( not ExisteDigitoEmpresa(FieldByName( 'CM_DIGITO' ).AsString,StrVacio( FieldByName( 'CM_NIVEL0' ).AsString ) ) ) then
                       begin
                            if DigitoActual <> FieldByName( 'CM_DIGITO' ).AsString then
                            begin
                                 DigitoActual := FieldByName( 'CM_DIGITO' ).AsString;
                                 AddSinConfidencial := False;
                                 AddConfidencial := False;
                            end;
                            if not AddSinConfidencial then
                            begin
                                 sDigito := FieldByName( 'CM_DIGITO' ).AsString[1];
                                 AddObject( Format( '%s=%s', [ FieldByName( 'CM_DIGITO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ), TObject( ORD( sDigito ) ) );
                                 if StrVacio( FieldByName( 'CM_NIVEL0' ).AsString )then
                                    AddSinConfidencial := True
                                 else
                                     AddConfidencial := True;
                            end;
                       end;
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;     
end;


procedure TdmCliente.cdsEmpleadoLookUpLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
begin
     inherited;
     lOk := ZetaBuscaEmpleado.BuscaEmpleadoDialogo( sFilter, sKey, sDescription );
end;

procedure TdmCliente.cdsEmpleadoLookUpLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
var
   Datos: OleVariant;
begin
     if ( FServidor = nil ) then
        lOk := Servidor.GetLookupEmpleado( Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( eLookEmpGeneral ) )
     else
        lOk := FServidor.GetLookupEmpleado( Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( eLookEmpGeneral ) );
     if lOk then
     begin
          with cdsEmpleadoLookUp do
          begin
               Init;
               Data := Datos;
               sDescription := GetDescription;
          end;
     end
     else
     begin
          with cdsEmpleadoLookUp do
          begin
               Init;
               Data := NULL;
               sDescription := VACIO;
          end;

     end;
end;

procedure TdmCliente.GetEmpleadosBuscados( const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet );
begin
     with oDataSet do
     begin
          if ( FServidor = nil ) then
             Data := Servidor.GetEmpleadosBuscados( oEmpresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca )
          else
             Data := FServidor.GetEmpleadosBuscados( oEmpresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca );
     end;
end;

procedure TdmCliente.SetEmpresaPass( const Value: string );
begin
     FEmpresaPass := Value;
     if  FEmpresaPass <> '' then
     begin
          FindCompany( FEmpresaPass );
          SetCompany;
     end;
end;


function TdmCliente.GetURLPantalla(const sURL: string): string;
begin
     Result := sURL;
     //Result := StrTransAll( Result, K_ARROBA_SERVIDOR, GlobalComparte.GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ ) );
end;


procedure TdmCliente.cdsEmpresasKioscoAlAdquirirDatos(Sender: TObject);
begin
     cdsEmpresasKiosco.Data:= Servidor.GetCompanys( Usuario, ord( tc3Datos ) );
end;

function TdmCliente.GetEmpresas: OleVariant;
begin
     Result:= Servidor.GetCompanys( Usuario, ord( tc3Datos ) );
end;

end.
