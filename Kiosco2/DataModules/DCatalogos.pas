unit DCatalogos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     {$ifndef VER130}Variants,{$endif}
     ZetaCommonClasses,
     {$ifdef DOS_CAPAS}
     DServerCatalogos,
     {$else}
     Catalogos_TLB,
     {$endif}
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaClientDataSet;

type
  TdmCatalogos = class(TDataModule)
    cdsCondiciones: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsAfterPost(DataSet: TDataSet);
    procedure cdsBeforePost(DataSet: TDataSet; const sCampo: String);
    {$ifdef VER130}
    procedure cdsReconcileError( DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction );
    {$else}
    procedure cdsReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction );
    {$endif}
    procedure MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
    procedure cdsCondicionesAlModificar(Sender: TObject);
    procedure cdsCondicionesBeforeEdit(DataSet: TDataSet);
    procedure cdsCondicionesBeforePost(DataSet: TDataSet);
    procedure cdsCondicionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsCondicionesNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    function GetServerCatalogo: TdmServerCatalogos;
    {$else}
    FServerCatalogo: IdmServerCatalogosDisp;
    function GetServerCatalogo: IdmServerCatalogosDisp;
    {$endif}
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerCatalogo: TdmServerCatalogo read GetServerCatalogo;
    {$else}
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogo;
    {$endif}
  public
    { Public declarations }
     //Para Compilar en 2.8
    //procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
  end;

var
  dmCatalogos: TdmCatalogos;

implementation

uses DCliente,
     DSistema,
     FEditCatCondiciones,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZBaseEdicion,
     ZReconcile,
     ZetaDialogo;

{$R *.DFM}

procedure TdmCatalogos.DataModuleCreate(Sender: TObject);
begin
     cdsCondiciones.OnReconcileError := cdsReconcileError;
end;

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogo: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmCatalogos.GetServerCatalogo: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServerCatalogo ) );
end;
{$endif}
 //Para Compilar en 2.8
//procedure TdmCatalogos.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
procedure TdmCatalogos.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
begin
     {
     if ( enFolio in Entidades ) then
        cdsFolios.SetDataChange;
     }
end;

procedure TdmCatalogos.MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     { Se requiere condicionar para que al Hacer un }
     { cdsNomParam.Cancel no ocurra un Invalid BLOB Handle }
     if Assigned( Sender ) and Assigned( Sender.Dataset ) then
     begin
          if Sender.Dataset.State in [ dsBrowse ] then
             Text := Sender.AsString;
     end;
end;

procedure TdmCatalogos.cdsBeforePost( DataSet: TDataSet; const sCampo: String );
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( sCampo ).AsString ) then
             DataBaseError( 'EL C�digo No Puede Quedar Vac�o' );
     end;
end;

procedure TdmCatalogos.cdsAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
          end;
     end;
end;

{$ifdef VER130}
procedure TdmCatalogos.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
procedure TdmCatalogos.cdsReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction );
{$endif}
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

{ **** cdsCondiciones **** }

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCondicionesAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( EditCatCondiciones, TEditCatCondiciones );
end;

procedure TdmCatalogos.cdsCondicionesBeforeEdit(DataSet: TDataSet);
begin
     with DataSet.FieldByName( 'QU_NIVEL' ) do
     begin
          if ( AsInteger > 0 ) and ( AsInteger < Ord( dmCliente.GetDatosUsuarioActivo.Nivel ) ) then
          begin
               DataBaseError( 'El Nivel Que Tiene Como Usuario' + CR_LF + 'No Le Permite Modificar/Borrar Esta Condici�n !!!' );
          end;
     end;
end;

procedure TdmCatalogos.cdsCondicionesBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'QU_CODIGO' );
     DataSet.FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmCatalogos.cdsCondicionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_GRALES_CONDICIONES, iRight );
end;

procedure TdmCatalogos.cdsCondicionesNewRecord(DataSet: TDataSet);
begin
     DataSet.FieldByName( 'QU_NIVEL' ).AsInteger := Ord( nuPrioridadMinima );
end;

end.
