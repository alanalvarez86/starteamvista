unit DDiccionario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
  DBaseDiccionario,
  ZetaTipoEntidad,
  ZetaCommonLists,
  ZetaClientDataSet, cxChecklistBox;

type
  TdmDiccionario = class(TdmBaseDiccionario)
    procedure cdsDiccionAlModificar(Sender: TObject);
    procedure cdsDiccionAfterPost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    //procedure AgregandoRelacion(Sender: TObject);
  protected
    { Protected declarations }
    function ChecaDerechoConfidencial: Boolean; override;
    //procedure SetAgregaRelacion;override;
  public
    { Public declarations }
    function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer; override;
    procedure GetListaClasifiModulo( oLista : TStrings; const lMigracion : Boolean = TRUE );override;
    procedure CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings);overload;
    procedure CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TcxChecklistBox );overload;
    procedure SetLookupNames; override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation

uses ZBaseEdicion,
     ZetaEntidad,
     ZGlobalTress,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZAccesosTress,
     FEditDiccion,
     DEntidadesTress,
     DGlobal,
     DCliente;

{$R *.DFM}


procedure TdmDiccionario.DataModuleCreate(Sender: TObject);
begin
     inherited;
     //ClasifiDefault := crWorkFlow;
end;

procedure TdmDiccionario.CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings );
var
   oCampo: TObjetoString;
begin
     oLista.Clear;
     GetListaDatosTablas( oEntidad, lTodos );
     with cdsBuscaPorTabla do
     begin
          while not Eof do
          begin
               oCampo := TObjetoString.Create;
               oCampo.Campo := FieldByName( 'DI_NOMBRE' ).AsString;
               oLista.AddObject( FieldByName( 'DI_TITULO' ).AsString, oCampo );
               Next;
          end;
     end;
end;

procedure TdmDiccionario.CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TcxChecklistBox );
var
   oCampo: TObjetoString;
begin
     oLista.Clear;
     GetListaDatosTablas( oEntidad, lTodos );
     with cdsBuscaPorTabla do
     begin
          while not Eof do
          begin
               oCampo := TObjetoString.Create;
               oCampo.Campo := FieldByName( 'DI_NOMBRE' ).AsString;
               //oLista.AddObject( FieldByName( 'DI_TITULO' ).AsString, oCampo );
               with oLista.items.Add do
               begin
                  text := FieldByName( 'DI_TITULO' ).AsString;
                  Tag := integer(oCampo);
               end;
               Next;
          end;
     end;
end;
procedure TdmDiccionario.cdsDiccionAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion.ShowFormaEdicion( EditDiccion, TEditDiccion );
end;

procedure TdmDiccionario.cdsDiccionAfterPost(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     inherited;
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
             Reconcile ( ServerDiccionario.GrabaDiccion( dmCliente.Empresa,Delta, ErrorCount ) );
     end;
end;
{$ifdef FALSE}
procedure TdmDiccionario.AgregandoRelacion(Sender: TObject);
begin
     {CV: Ahorita el Sender siempre es la Entidad enCONTEO}
end;
{$ENDIF}

{$ifdef FALSE}
procedure TdmDiccionario.SetAgregaRelacion;
begin
     {CV: Ahorita la unica Entidad que tiene un evento AgregaRelacion es
     enConteo, en un futuro, si alguna otra cae en ese caso, se haria una
     lista de entidades, y el siguiente codigo estaria dentro de un FOR}
     with dmEntidadesShell.GetEntidad(enConteo) do
     begin
          AlAgregarRelacion := AgregandoRelacion;
     end;
end;
{$ENDIF}

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     {
     dmTablas.SetLookupNames;
     }
end;

function TdmDiccionario.ChecaDerechoConfidencial: Boolean;
begin
     Result := ZAccesosMgr.Revisa( D_REPORTES_CONFIDENCIALES );
end;

function TdmDiccionario.GetDerechosClasifi( eClasifi: eClasifiReporte ): Integer;
begin
     Result := 0;
{
     case eClasifi of
          crWorkFlow: Result := D_REPORTES_WORKFLOW;
          crPortal: Result := 0;
          crSistema: Result := D_REPORTES_SISTEMA;
          crFavoritos: Result := D_REPORTES_FAVORITOS;
     else
         Result := 0;
     end;  }
end;

procedure TdmDiccionario.GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);
begin
     inherited GetListaClasifiModulo( oLista, lMigracion );
    // AgregaClasifi( oLista, crWorkFlow );
   //  AgregaClasifi( oLista, crSistema );
end;

end.
