{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: QuickReport 2.0 Delphi 1.0/2.0/3.0                      ::
  ::                                                         ::
  :: QRPREV - QuickReport standard preview form              ::
  ::                                                         ::
  :: Copyright (c) 1997 QuSoft AS                            ::
  :: All Rights Reserved                                     ::
  ::                                                         ::
  :: web: http://www.qusoft.no   mail: support@qusoft.no     ::
  ::                             fax: +47 22 41 74 91        ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

unit fPreview;

interface

{$define EZM}

uses
{$ifdef win32}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, QRPrntr, QuickRpt, ToolWin,
  ComCtrls, fcButton, fcImgBtn, fcShapeBtn, fcpanel;
{$else}
  Wintypes, WinProcs, Sysutils, Messages, Classes, Controls, StdCtrls, ExtCtrls,
  Buttons, QRPrntr, Graphics, Forms, Dialogs, QR2const, QuickRpt;
{$endif}

type
  TPreview = class(TForm)
    StatusPanel: TPanel;
    Panel1: TPanel;
    Status: TLabel;
    QRPreview: TQRPreview;
    Timer: TTimer;
    Toolbar: TPanel;
    ZoomToFit: TSpeedButton;
    ZoomTo100: TSpeedButton;
    ZoomToWidth: TSpeedButton;
    FirstPage: TSpeedButton;
    PrevPage: TSpeedButton;
    NextPage: TSpeedButton;
    LastPage: TSpeedButton;
    Print: TSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ZoomToFitClick(Sender: TObject);
    procedure ZoomTo100Click(Sender: TObject);
    procedure ZoomToWidthClick(Sender: TObject);
    procedure FirstPageClick(Sender: TObject);
    procedure PrevPageClick(Sender: TObject);
    procedure NextPageClick(Sender: TObject);
    procedure LastPageClick(Sender: TObject);
    procedure PrintClick(Sender: TObject);
    procedure ExitClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure QRPreviewPageAvailable(Sender: TObject; PageNum: Integer);
{$ifdef EZM}
    procedure QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
{$endif}
  private
    FQRPrinter : TQRPrinter;
    FReporte : TQuickRep;
    FShowGrafica: Boolean;
    procedure TimerStart;
    procedure TimerStop;
  public
    constructor CreatePreview(AOwner : TComponent; oParent: TWinControl; aQRPrinter : TQRPrinter; oReport : TQuickRep ); virtual;
    procedure UpdateInfo;
    procedure CierraPreview;

    property QRPrinter : TQRPrinter read FQRPrinter write FQRPrinter;
    property Reporte : TQuickRep read FReporte write FReporte;
    property ShowGrafica: Boolean read FShowGrafica write FShowGrafica;
  end;

implementation

uses FRangoPaginas,
     FKioscoRegistry,
     FDlgInformativo,
     ZetaSystemWorking,
     DCliente,
     ZetaDialogo,
     ZetaCommonClasses;
{$R *.DFM}


procedure TPreview.FormCreate(Sender: TObject);
begin
     FirstPage.Caption := 'Primer' + CR_LF + 'P�gina';
     LastPage.Caption := 'Ultima' + CR_LF + 'P�gina';
     PrevPage.Caption := 'Anterior';
     NextPage.Caption := 'Siguiente';

     ZoomToFit.Caption := 'P�gina Completa';
     ZoomToWidth.Caption := 'Ancho P�gina';
     ZoomTo100.Caption := 'P�gina al 100%';

     if KioskoRegistry.SoloBotonImprimir then
     begin
          ZoomToFit.Visible := FALSE;
          ZoomToWidth.Visible := FALSE;
          ZoomTo100.Visible := FALSE;
          FirstPage.Visible := FALSE;
          PrevPage.Visible := FALSE;
          NextPage.Visible := FALSE;
          LastPage.Visible := FALSE;
     end;     
end;


constructor TPreview.CreatePreview(AOwner : TComponent; oParent: TWinControl; aQRPrinter : TQRPrinter; oReport : TQuickRep);
begin
     inherited Create(AOwner);
     Parent := oParent;
     QRPrinter := aQRPrinter;
     FReporte := oReport;
     QRPreview.QRPrinter := aQRPrinter;
     if QRPrinter <> nil then Caption := QRPrinter.Title;
     ZoomToWidth.Down := True;
     WindowState := wsMaximized;
end;

procedure TPreview.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     TimerStop;
     Action := caFree;
end;

procedure TPreview.UpdateInfo;
begin
{$ifdef EZM}
     if FQrPrinter <> NIL then
        Status.Caption := 'P�gina ' + IntToStr(QRPreview.PageNumber) +
                          ' de ' + IntToStr(QRPreview.QRPrinter.PageCount);
{$else}
     Status.Caption := LoadStr(SqrPage) + ' ' + IntToStr(QRPreview.PageNumber) + ' ' +
                       LoadStr(SqrOf) + ' ' + IntToStr(QRPreview.QRPrinter.PageCount);
{$endif}
end;

procedure TPreview.ZoomToFitClick(Sender: TObject);
begin
     TimerStop;
     Application.ProcessMessages;
     QRPreview.ZoomToFit;
     TimerStart;
end;

procedure TPreview.ZoomTo100Click(Sender: TObject);
begin
     TimerStop;
     Application.ProcessMessages;
     QRPreview.Zoom := 100;
     TimerStart;
end;

procedure TPreview.ZoomToWidthClick(Sender: TObject);
begin
     TimerStop;
     Application.ProcessMessages;
     QRPreview.ZoomToWidth;
     TimerStart;
end;

procedure TPreview.FirstPageClick(Sender: TObject);
begin
     TimerStop;
     QRPreview.PageNumber := 1;
     UpdateInfo;
     TimerStart;
end;

procedure TPreview.PrevPageClick(Sender: TObject);
begin
     TimerStop;
     QRPreview.PageNumber := QRPreview.PageNumber - 1;
     UpdateInfo;
     TimerStart;
end;

procedure TPreview.NextPageClick(Sender: TObject);
begin
     TimerStop;
     QRPreview.PageNumber := QRPreview.PageNumber + 1;
     UpdateInfo;
     TimerStart;
end;

procedure TPreview.LastPageClick(Sender: TObject);
begin
     TimerStop;
     QRPreview.PageNumber := QRPrinter.PageCount;
     UpdateInfo;
     TimerStart;
end;

procedure TPreview.PrintClick(Sender: TObject);
begin
     try
        TimerStop;
        QrPreview.QrPrinter.Print;
        TimerStart;
        FDlgInformativo.MuestraDialogoInf( 'El reporte ha sido impreso con �xito' );
     except
           raise;
     end;
end;

procedure TPreview.ExitClick(Sender: TObject);
begin
     Close;
end;

procedure TPreview.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_Next : if Shift=[ssCtrl] then
                LastPageClick(Self)
              else
                NextPageClick(Self);
    VK_Prior : if Shift=[ssCtrl] then
                 FirstPageClick(Self)
               else
                 PrevPageClick(Self);
    VK_Home : FirstPageClick(Self);
    VK_End : LastPageClick(Self);
  end;
end;

procedure TPreview.QRPreviewPageAvailable(Sender: TObject;
  PageNum: Integer);
begin
     UpdateInfo;
end;

{$ifdef EZM}
procedure TPreview.QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     TimerStop;
     with QrPreview do
     begin
          case Button of
               mbLeft: if ( Zoom < 300 ) then Zoom := Zoom + 10;
               mbRight: if ( Zoom > 0 ) then Zoom := Zoom - 10;
          end;
     end;
     TimerStart;
end;
{$endif}

procedure TPreview.FormResize(Sender: TObject);
begin
     WindowState := wsMaximized;
end;

procedure TPreview.FormShow(Sender: TObject);
begin
     Timer.Interval := 30000;
     Timer.Interval := dmCliente.TimeoutDatos+10000;
     TimerStart;
     QRPreview.Zoom := 150;

end;


procedure TPreview.TimerStart;
begin
     TimerStop;
     with Timer do
     begin
          Enabled := True;
     end;
end;

procedure TPreview.TimerStop;
begin
     with Timer do
     begin
          Enabled := False;
     end;
end;

procedure TPreview.TimerTimer(Sender: TObject);
begin
     TimerStop;
     Close;
end;

procedure TPreview.CierraPreview;
begin
     Visible := FALSE;
     Timer.Interval := 10;
     TimerStart;
     //Sleep(1000);
     Application.ProcessMessages;
end;

end.
