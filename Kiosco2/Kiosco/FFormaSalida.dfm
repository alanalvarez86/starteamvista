inherited PideSalida: TPideSalida
  Left = 406
  Caption = 'PideSalida'
  ClientHeight = 386
  OldCreateOrder = True
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited fcImagenFondo: TfcImager
    Height = 274
  end
  inherited PanelTeclado: TfcPanel
    Height = 274
    inherited fcNumero2: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero3: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero5: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero8: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero9: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero0: TfcShapeBtn
      NumGlyphs = 1
    end
  end
  inherited fcPanel1: TfcPanel
    inherited PanelMensaje: TfcShapeBtn
      NumGlyphs = 0
    end
    inherited PanelClave: TfcShapeBtn
      NumGlyphs = 1
    end
  end
  object TimerSalida: TTimer
    Enabled = False
    OnTimer = TimerSalidaTimer
    Left = 23
    Top = 28
  end
end
