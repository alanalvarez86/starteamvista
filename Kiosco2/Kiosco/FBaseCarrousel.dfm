object BaseCarrousel: TBaseCarrousel
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'BaseCarrousel'
  ClientHeight = 129
  ClientWidth = 207
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Timer: TTimer
    Enabled = False
    OnTimer = TimerTimer
    Left = 4
    Top = 4
  end
  object TimerTeclado: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = TimerTecladoTimer
    Left = 36
    Top = 4
  end
  object TimerSensorBiometrico: TTimer
    Enabled = False
    OnTimer = TimerSensorBiometricoTimer
    Left = 104
    Top = 4
  end
  object TimerSync: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = TimerSyncTimer
    Left = 136
    Top = 4
  end
end
