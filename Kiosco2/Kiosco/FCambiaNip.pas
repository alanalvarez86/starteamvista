unit FCAmbiaNip;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FCambiaClave, ExtCtrls, StdCtrls, fcButton, fcImgBtn, fcShapeBtn,
  fcpanel, fcImager;

type
  TCambiaNIP = class(TCambiaClave)
    procedure fcCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edTextoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure AlSalir;override;
    procedure AceptaClave;override;

  public
    { Public declarations }
  end;

var
  CambiaNIP: TCambiaNIP;

function CambiaNIPEmpleado( const iMaxDigitos: Integer; FParent: TWinControl = NIL ): Boolean;

implementation
uses
    DCliente,
    FBaseKiosco,
    FDlgInformativo,
    ZetaCommonClasses;

{$R *.dfm}

function CambiaNIPEmpleado( const iMaxDigitos: Integer; FParent: TWinControl = NIL ): Boolean;
begin
     if Assigned( CambiaNIP ) then
     begin
          CambiaNiP.Close;
     end
     else
     begin
          CambiaNIP := TCambiaNIP.Create( Application );
          if ( FParent <> NIL ) then
          begin
               CambiaNIP.Parent := FParent;
               CambiaNIP.OnKeyDown := TBaseKiosco( FParent.Parent ).FormKeyDown;
          end;
     end;
     with CambiaNIP do
     begin
          MaximoDigitos := iMaxDigitos;
          Clear;
          Mensaje := 'Por favor capture su nueva clave';
          Mascara := '*';
          Show;

          Result := True;
     end;
end;

procedure TCambiaNIP.AlSalir;
begin
     inherited AlSalir;
end;


procedure TCambiaNIP.AceptaClave;
begin
     if PrimeraCaptura then
     begin
          Mensaje := 'Por favor confirme la clave capturada';
          PrimeraCaptura := False;
          PrimeraClave := Valor;
          Clear;
          SetControles;
     end
     else
     begin
          if ( Length( Valor ) = MaximoDigitos ) then
          begin
               if ( Self.Valor = Self.PrimeraClave ) then
               begin
                    if dmCliente.EscribeNIPEmpleado( Valor ) then
                       FDlgInformativo.MuestraDialogoInf( 'Su clave ha sido cambiada' )
                    else
                        FDlgInformativo.MuestraDialogoInf( 'Su clave no pudo actualizarse' + CR_LF +
                                                           'Consulte a su Administrador o Supervisor' );
                    Close;
               end
               else
               begin
                    Clear;
                    Problema := 'Clave diferente. Por favor intente de nuevo';
                    SetControles;
               end;
          end
          else
          begin
               Clear;
               Problema := 'La longitud no es correcta. Por favor intente de nuevo';
               SetControles;
          end;
     end;
end;


procedure TCambiaNIP.fcCancelarClick(Sender: TObject);
begin
     inherited;
     Hide;
end;

procedure TCambiaNIP.FormShow(Sender: TObject);
var
   Xc, Yc: Real;
   Xci, Yci: Real;
begin
     inherited;
     Xc := Parent.Width / 2;
     Yc := Parent.Height / 2;

     Xci := Width / 2;
     Yci := Height / 2;
     Left := Round( Xc - Xci );
     Top := Round( Yc - Yci );
end;

procedure TCambiaNIP.edTextoChange(Sender: TObject);
 var
    Key: Word;
begin
     inherited;
     if Assigned( OnKeyDown ) then
        OnKeyDown( Self,  Key, [] );

end;

procedure TCambiaNIP.FormCreate(Sender: TObject);
begin
     inherited;
     ShowNipHtml := FALSE;
end;

end.
