unit FServidorName;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  fcImage, fcimageform, StdCtrls, fcButton, fcImgBtn,
  fcShapeBtn;

type
  TServidorName = class(TForm)
    fcImageForm1: TfcImageForm;
    Label1: TLabel;
    edServidor: TEdit;
    btnEscribir: TfcShapeBtn;
    btnCancelar: TfcShapeBtn;
    lblError: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEscribirClick(Sender: TObject);
    procedure edServidorChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FProcessServer: String;
    procedure SetControles(const lVerifica: Boolean);
  public
    { Public declarations }
    property ProcessServer: String read FProcessServer write FProcessServer;
  end;

function CapturaServidor( var sServidor: String ): Boolean;

implementation

uses FKioscoRegistry,
     ZetaCommonTools;

function CapturaServidor( var sServidor: String ): Boolean;
var
  ServidorName: TServidorName;
begin
     Result := False;
     ServidorName := TServidorName.Create( Application );
     try
        with ServidorName do
        begin
             ProcessServer := KioskoRegistry.ComputerName; //sServidor;
             {$ifdef TRESS_DELPHIXE5_UP}
             {$ifdef WIN32}
             if ( not KioskoRegistry.KioskoConfigurado ) and GetIsWindowsVista and KioskoRegistry.GetVirtualStore then
                ProcessServer := KioskoRegistry.ComputerName;
             {$endif}
             {$endif}
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  Result := True;
                  sServidor := ProcessServer;
                  Close;
             end;
        end;
     finally
            FreeAndNil( ServidorName );
     end;
end;

{$R *.DFM}

procedure TServidorName.FormShow(Sender: TObject);
begin
     edServidor.Text := ProcessServer;
end;

procedure TServidorName.SetControles( const lVerifica: Boolean );
begin
     lblError.Visible := lVerifica;
end;

procedure TServidorName.edServidorChange(Sender: TObject);
begin
     SetControles( False );
end;

procedure TServidorName.btnEscribirClick(Sender: TObject);
begin
     ProcessServer := edServidor.Text;
     if ( Length( ProcessServer ) > 0 ) then
          ModalResult := mrOk
     else
         SetControles( True );
end;

procedure TServidorName.btnCancelarClick(Sender: TObject);
begin
     Close;
end;

end.
