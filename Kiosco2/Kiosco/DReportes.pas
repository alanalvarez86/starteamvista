unit DReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient, FileCtrl,
  ZetaCommonLists, ZetaCommonClasses,
  ZQRReporteListado,
  DReportesGenerador,
  {$IFDEF TRESS_DELPHIXE5_UP}
  DEmailService,
  {$ELSE}
  DEmailServiceD7,
  {$ENDIF}
  Variants,
  ZetaASCIIFile,
  Printers,
  ZReportTools;

type
  TdmReportes = class(TdmReportGenerator)
    cdsReportesConsulta: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsReportesConsultaAlAdquirirDatos(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    QRReporteListado : TQrReporteListado;
    FUsuariosHTML: TStrings;
    FHost: String;
    FPuerto:Integer;
    FAuthMethod: eAuthTressEmail;
    FUserID: String;
    FPassword:string;
    FFromAddress: String;
    FFromName: String;
    FErrorMail: String;
    FArchivoHTML: String;
    FBitacoraMail: TAsciiLog;
    FImpresoraDisponible: Boolean;
    FMensajeImpresoraOcupada: String;
    dmEmailService : TdmEmailService;
    function GetSobreTotales: Boolean;
    procedure ModificaListaCampos(Lista: TStrings);
    procedure ModificaListas;
    function ConectaGlobal: Boolean;
    function ConectaHost: Boolean;
    procedure AgregaAttachments( oLista : TStrings );
    procedure AgregaEmailUsuario;
    function ConectaUsuariosCorreo: Boolean;
    procedure LogEmailEnd;
    procedure LogEmailInit;
    procedure LogEmail( const sTexto: String );
    procedure EnviarReporteEmail( const lMsgNormal : Boolean );

  protected
    FMandaloPreview : boolean;
    procedure DoOnGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoOnGetReportes( const lResultado: Boolean );override;

    function PreparaPlantilla(var sError: WideString): Boolean;override;
    procedure DesPreparaPlantilla;override;

    procedure PreparaReporte;
    procedure DesPreparaReporte;
    procedure LogError(const sTexto: String; const lEnviar: Boolean);override;
    procedure LogErrorEmail(const sTexto: String; const lEnviar: Boolean);
    procedure DoAfterCargaActivos;override;
    function GetEmpresa: Olevariant;override;
    function GetReportes( const iReporte: Integer ): Boolean;override;
    procedure SendEmail(const Tipo: eEmailType; const lMsgNormal : Boolean; oUsuarios: TStrings);
    procedure DoAfterGetResultado( const lResultado : Boolean; const Error : string );override;
    function GetDatosImpresion: TDatosImpresion;override;
  public
    { Public declarations }
    function GeneraUnReporte( const iReporte: Integer; const Preview: Boolean ): Boolean;
    function ImprimeUnReporte( const iReporte: Integer ): Boolean;
    function DirectorioPlantillas: string;override;
    procedure DespreparaEnvioCorreo(const lExito: Boolean );
  end;

var
  dmReportes: TdmReportes;

implementation

uses DCliente,
     DGlobal,
     FDlgInformativo,
     ZetaSystemWorking,
     ZetaCommonTools,
     ZFuncsCliente,
     ZGlobalTress,
     ZAccesosTress,
     ZReporteAscii,
     ZetaDialogo,
     DMailMerge,
     FToolsKiosco,
     FKioscoRegistry;
{$R *.DFM}

const
     {$ifdef MISDATOS}
     K_NOMBRE_BITACORA = 'ReportesMisDatos.LOG';
     {$else}
     K_NOMBRE_BITACORA = 'ReportesKiosco.LOG';
     {$endif}

{ TdmReportes }

procedure TdmReportes.DataModuleCreate(Sender: TObject);
begin
     SetLogFileName( ExtractFilePath( Application.ExeName ) + K_NOMBRE_BITACORA );
     inherited;
     FMostrarError := TRUE;
     Global.Conectar;
     dmEmailService := TdmEmailService.Create( self );
     FUsuariosHTML := TStringList.Create;
     FBitacoraMail:= TAsciiLog.Create;;
end;

procedure TdmReportes.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil(FBitacoraMail);
     FreeAndNil(FUsuariosHTML);
     FreeAndNil(dmEmailService);
end;

procedure TdmReportes.EnviarReporteEmail( const lMsgNormal : Boolean );
begin
     LogEmailInit;
     try
        if ( ConectaGlobal ) and ( ConectaUsuariosCorreo ) then
        begin
             {Agrega a usuario correspondiente}
                //FUsuariosHTML.AddObject( 'jbasurto@tress.com.mx', TObject( 0 ) );
              FUsuariosHTML.Clear;
              with dmCliente.cdsSuscripUsuarios do
              begin
                   while not ( Eof ) do
                   begin
                        AgregaEmailUsuario;
                        Next;
                   end;
              end;
              if ( DatosImpresion.Tipo in [tfHTML,tfHTM] ) AND UnSoloHTML then
                SendEmail( emtHtml, lMsgNormal, FUsuariosHTML )
              else
                 SendEmail( emtTexto, lMsgNormal, FUsuariosHTML );

              { Se cambio a que se hiciera con el thread : BorraArchivos;  }

        end;
     finally
            LogEmailEnd;
            FBitacoraMail.Close;
     end;
end;

function TdmReportes.GeneraUnReporte(const iReporte: Integer; const Preview: Boolean ): Boolean;
begin
     ZetaSystemWorking.InitAnimation( 'Procesando reporte.....' );
     try
        FMandaloPreview := Preview;
        Result := GetResultado( iReporte, TRUE );
     finally
            ZetaSystemWorking.EndAnimation
     end;
end;

function TdmReportes.ImprimeUnReporte( const iReporte: Integer ): Boolean;
begin
     ZetaSystemWorking.InitAnimation( 'Imprimiendo reporte.....' );
     try
        FMandaloPreview := FALSE;
        Result := ( GetResultado( iReporte, TRUE ) and FImpresoraDisponible );
        if ( Result ) then
        begin
             if ( DatosImpresion.Tipo <> tfImpresora ) then
                EnviarReporteEmail( Result )
             else
                 FDlgInformativo.MuestraDialogoInf( 'El reporte ha sido impreso con �xito' );
        end
        else
            FDlgInformativo.MuestraDialogoInf( FMensajeImpresoraOcupada );
     finally
            ZetaSystemWorking.EndAnimation
     end;
end;

function TdmReportes.GetSobreTotales: Boolean;
var
   i : integer;
begin
     Result := FALSE;
     for i := 0 to Campos.Count - 1 do
     begin
          with TCampoListado( Campos.Objects[ i ] ) do
          begin
               Result := Result OR (Operacion = ocSobreTotales);
          end;
     end;
end;

function TdmReportes.PreparaPlantilla( var sError : WideString ) : Boolean;
 var
    lHayImagenes : Boolean;
    sDirectorio : string;
begin
     {$ifdef KIOSCO2}
     PreparaReporte;
     {$else}
     ZQRReporteListado.PreparaReporte;
     {$endif}

     sDirectorio := ExtractFilePath( DatosImpresion.Archivo );
     Result := DirectoryExists( sDirectorio );
     if NOT Result then
        sError := Format( 'El Directorio de Plantillas " %s " No Existe', [ sDirectorio ] );

     if Result then
     begin
          DoOnGetDatosImpresion;

          with QRReporteListado do
          begin
               Init( FSQLAgente,
                     DatosImpresion,
                     Parametros.Count,
                     lHayImagenes );
               ContieneImagenes := lHayImagenes;
          end;
     end;
end;

function TdmReportes.GetDatosImpresion: TDatosImpresion;
var
   fPrinter: TPrinter;
begin
     Result:= inherited GetDatosImpresion;
     fPrinter := TPrinter.Create;
     try
        Result.Impresora:= fPrinter.Printers.IndexOf( cdsReporte['RE_PRINTER'] );
     finally
            FreeAndNil(fPrinter);
     end;
end;

procedure TdmReportes.DesPreparaPlantilla;
begin
     {$ifndef KIOSCO2}
     ZQrReporteListado.DesPreparaReporte;
     {$ENDIF}
end;

procedure TdmReportes.DoOnGetReportes( const lResultado: Boolean );
const
     K_SIN_RESTRICCION = -1;
begin
     if lResultado then
     begin
          with cdsReporte, dmCliente do
          begin
               //La Clasificaci�n de la empresa se toma de acuerdo a la propiedad del bot�n UsaEmpresaEmpleado
               if ( ( ( not UsaEmpresaEmpleado ) and ( ( ClasifiEmpresaDefault = K_SIN_RESTRICCION ) or ( ClasifiEmpresaDefault = FieldByName('RE_CLASIFI').AsInteger ) ) )
                   or ( ( UsaEmpresaEmpleado )  and  ( ( ClasifiEmpresaEmpleado = K_SIN_RESTRICCION ) or ( ClasifiEmpresaEmpleado = FieldByName('RE_CLASIFI').AsInteger ) ) ) ) then
               begin
                    if NOT(eTipoReporte( FieldByName('RE_TIPO').AsInteger ) in [trForma, trListado] ) then
                    begin
                         Raise Exception.Create( 'Solamente se Permite Obtener Listados y/o Formas' );
                    end;
               end
               else
                   Raise Exception.Create( 'No se permite obtener reportes de �sta clasificaci�n' );
          end;
     end;
end;

procedure TdmReportes.DoOnGetResultado( const lResultado : Boolean; const Error : string );
var
   eTipo: eTipoReporte;

   procedure GeneraMailMerge;
   var
       oReporteAscii: TReporteAscii;
   begin
        oReporteAscii := TReporteAscii.Create;

        try
           with oReporteAscii do
           begin
                if GeneraAscii( cdsResultados,
                                DatosImpresion,
                                Campos,
                                Grupos,
                                zStrToBool( cdsReporte.FieldByName('RE_SOLOT').AsString ),
                                FALSE ) then
                begin
                     with TdmMailMerge.Create(self) do
                     begin
                          with DatosImpresion do
                               if FileExists( Archivo ) then
                                  Imprime( Archivo,Exportacion )
                               else
                                   ZError('Impresi�n','El Documento ' + Archivo + ' No Ha Sido Encontrado.', 0);
                          Free;
                     end;
                end;
           end;
        finally
               FreeAndNil( oReporteAscii );
        end;

   end;

begin
     if cdsResultados.Active AND Not cdsResultados.IsEmpty then
     begin
          FImpresoraDisponible := FMandaloPreview or FToolsKiosco.ImpresoraDisponibe( DatosImpresion.Impresora, FMensajeImpresoraOcupada );
          if FImpresoraDisponible then
          begin
               cdsResultados.First;

               ModificaListas;

               ZFuncsCliente.RegistraFuncionesCliente;
               //Asignacion de Parametros del Reporte,
               //para poder evaluar la funcion PARAM() en el Cliente;
               ZFuncsCliente.ParametrosReporte := FSQLAgente.Parametros;

               with cdsReporte do
               begin
                    eTipo := eTipoReporte( FieldByName('RE_TIPO').AsInteger );
                    if ( eTipo = trForma ) then
                    begin
                         QRReporteListado.GeneraForma( FieldByName('RE_NOMBRE').AsString,
                                                       FMandaloPreview, {Mandarlo a Preview}
                                                       cdsResultados )
                    end
                    else if ( eTipo = trListado ) then
                    begin
                         if ( eTipoFormato( FieldByName('RE_PFILE').AsInteger ) = tfMailMerge ) then
                         begin
                              GeneraMailMerge;
                         end
                         else
                             QRReporteListado.GeneraListado( FieldByName('RE_NOMBRE').AsString,
                                                             FMandaloPreview, {Mandarlo a Preview}
                                                             zStrToBool( FieldByName('RE_VERTICA').AsString ),
                                                             zStrToBool( FieldByName('RE_SOLOT').AsString ),
                                                             GetSobreTotales,
                                                             cdsResultados,
                                                             Campos,
                                                             Grupos );
                    end;
               end;
          end;
     end;
end;

procedure TdmReportes.ModificaListaCampos( Lista : TStrings );
 var i : integer;
begin
     for i:=0 to Lista.Count -1 do
        with TCampoOpciones(Lista.Objects[i]) do
             if PosAgente >= 0 then
             begin
                  SQLColumna := FSQLAgente.GetColumna(PosAgente);
                  with FSQLAgente.GetColumna(PosAgente) do
                  begin
                       TipoImp := TipoFormula;
                       OpImp := Totalizacion;
                  end;
             end;
end;

procedure TdmReportes.ModificaListas;
 var i : integer;
begin
     ModificaListaCampos( Campos );
     for i:= 0 to Grupos.Count - 1 do
     begin
          with TGrupoOpciones( Grupos.Objects[i]) do
          begin
               ModificaListaCampos( ListaEncabezado );
               ModificaListaCampos( ListaPie );
          end;
     end;
end;

procedure TdmReportes.PreparaReporte;
begin
     if Assigned( QRReporteListado ) then
        DesPreparaReporte;
     if QRReporteListado = NIL then
        QRReporteListado := TQRReporteListado.Create( Application );
end;

procedure TdmReportes.DesPreparaReporte;
begin
     try
        QRReporteListado.Free;
     finally
            QRReporteListado := NIL;
     end;
end;

procedure TdmReportes.LogError(const sTexto: String; const lEnviar: Boolean );
begin
     if FMostrarError then
        FDlgInformativo.MuestraDialogoInf( sTexto );
end;

procedure TdmReportes.LogErrorEmail(const sTexto: String; const lEnviar: Boolean);
begin
     LogError(sTexto,lEnviar);
end;

function TdmReportes.GetEmpresa: Olevariant;
 var
    sCompany: string;
begin
     with dmCliente do
     begin
          sCompany := cdsCompany.FieldByName('CM_CODIGO').AsString;
          try
             FindCompany( dmCliente.EmpresaReportes );
             //GetEmpresa se manda llamar cuando se conecta el reporte tomando como Empresa la default, es por eso que aqu� se sabe que clasificaci�n
             //tiene la empresa Default.
             dmCliente.ClasifiEmpresaDefault:= cdsCompany.FieldByName('CM_KCLASIFI').AsInteger;
             Result := SetCompany;
          finally
                 FindCompany( sCompany );
                 SetCompany;
          end;
     end;
end;

procedure TdmReportes.DoAfterCargaActivos;
begin
     inherited;
     with dmCliente do
     begin
          if Assigned( ParametrosURL ) and ( ParametrosURL.Count > 0 ) then
          begin
               SetDatosPeriodoActivo( ParametrosURL.ParamByName('Numero').AsInteger,
                                      eTipoPeriodo( ParametrosURL.ParamByName('Tipo').AsInteger ),
                                      ParametrosURL.ParamByName('Year').AsInteger );

               Params.AddInteger( 'Year', ParametrosURL.ParamByName('Year').AsInteger );
               Params.AddInteger( 'Tipo', ParametrosURL.ParamByName('Tipo').AsInteger );
               Params.AddInteger( 'Numero', ParametrosURL.ParamByName('Numero').AsInteger);
               Params.AddBoolean( 'GeneraBitacora', True );

          end;
     end;
end;

function TdmReportes.GetReportes( const iReporte: Integer ): Boolean;
var
   oCampoRep: OleVariant;
begin
     CodigoReporte := iReporte;
     cdsReporte.Data := ServerReportes.GetEditReportes( GetEmpresa, iReporte, oCampoRep );
     if ( dmCliente.UsaEmpresaEmpleado ) and ( dmCliente.GetDatosEmpresaActiva.Codigo <> dmCliente.EmpresaReportes ) then
     begin
          cdsReportesConsulta.Data := ServerReportes.GetLookUpReportes( dmCliente.Empresa, TRUE );
          with cdsReportesConsulta do
          begin
               if Locate( 'RE_NOMBRE', cdsReporte.FieldByName('RE_NOMBRE').AsString, [] ) then
               begin
                    cdsReporte.Data := ServerReportes.GetEditReportes( dmCliente.Empresa,  FieldByName('RE_CODIGO').AsInteger, oCampoRep )
               end;
          end;
     end;
     Result := not cdsReporte.IsEmpty;
     if Result then
     begin
          cdsCampoRep.Data := oCampoRep;
          with cdsReporte do
          begin
               NombreReporte := FieldByName( 'RE_NOMBRE' ).AsString;
               SoloTotales := ZetaCommonTools.zStrToBool( FieldByName( 'RE_SOLOT' ).AsString );
          end;
     end;
end;

 

procedure TdmReportes.cdsReportesConsultaAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsReportesConsulta.Data := ServerReportes.GetLookUpReportes( dmCliente.Empresa, TRUE );
end;


function TdmReportes.DirectorioPlantillas: string;
begin
     {.$ifdef RDD}
     //with dmCliente do
          //Result := VerificaDir( ServerReportes.DirectorioPlantillas(Empresa) );
     {.$else}
     Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
     {.$endif}
end;

procedure TdmReportes.SendEmail( const Tipo: eEmailType;
                                 const lMsgNormal : Boolean;
                                 oUsuarios: TStrings );
var
   sTipo: String;
   sErrorMsg : string;
begin
     if ( oUsuarios.Count > 0 ) then
     begin
          with dmEmailService do
          begin
               ConectaHost;

               SubType := Tipo;
               Subject := 'Reporte  - '+ NombreReporte;


               ToAddress.AddStrings( oUsuarios );

               MessageText.Add( 'Reporte Enviado desde Kiosco: '+ Subject );
               MessageText.Add( 'Empleado: #'+ IntToStr(dmCliente.Empleado) +': '+ dmCliente.EmpleadoNombre );

               if ( SubType = emtHtml ) AND FileExists( FArchivoHTML )  then
               begin
                    MessageText.LoadFromFile( FArchivoHTML );
               end
               else
               begin
                    AgregaAttachments( Attachments );
               end;

               if Validate( sErrorMsg ) then
               begin
                    try
                       if SendEmail( sErrorMsg, DatosImpresion.Exportacion ) then
                       begin
                            if ( Tipo = emtHtml ) then
                               sTipo := 'en Formato HTML '
                            else
                                sTipo := 'con Archivo Adjunto ';

                            if lMsgNormal then
                            begin
                                 { No se usa ReportesKiosco.log por l�gica en DReportesGenerador que cierra y abre la bit�cora al crear varias instancias }

                                 LogEmail( Format( 'Empleado: #%d: Reporte # %d Procesado %s A los Usuarios: %s', [ dmCliente.Empleado, CodigoReporte, sTipo, CR_LF + oUsuarios.Text ] ) );

                                 FDlgInformativo.MuestraDialogoInf( Format( 'El Reporte <%s> ' +
                                                                      'ha sido enviado.' + CR_LF +
                                                                      KioskoRegistry.MensajeEmail, [ NombreReporte ] ) );

                            end;
                       end
                       else
                       begin
                            LogErrorEmail( 'Error al Tratar de Enviar Correo ' + CR_LF + sErrorMsg, TRUE );
                       end;

                    except
                          on Error: Exception do
                          begin
                               LogErrorEmail( 'Se encontr� un Error: Servidor Destinatario no responde', TRUE );
                          end;
                    end;

               end
               else
                   LogErrorEmail( 'Se encontr� un Error ' + CR_LF + sErrorMsg, TRUE );

          end;
     end
     else
         LogErrorEmail( 'Se encontr� un Error ' + CR_LF + 'No se encontro usuarios con correo electr�nico v�lidos', TRUE );
end;


procedure TdmReportes.DespreparaEnvioCorreo(const lExito: Boolean );
begin
     //LogEmailEnd;
     if ( not lExito ) then
     begin
          LogEmail('Correo no enviado: Verifique su direcci�n de correo y/o el correcto funcionamiento de su Servidor de Correos.' );
          if StrLleno( dmEmailService.ErrorMsg ) then
             LogEmail( dmEmailService.ErrorMsg );
     end;
     FBitacoraMail.Close;
end;


function TdmReportes.ConectaGlobal: Boolean;
begin
     with Global do
     begin
          Conectar;
          FHost := Global.GetGlobalString( K_GLOBAL_EMAIL_HOST );
          FPuerto := Global.GetGlobalInteger( K_GLOBAL_EMAIL_PORT );
          FAuthMethod :=  eAuthTressEmail( Global.GetGlobalInteger(K_GLOBAL_EMAIL_AUTH) -1  );
          Result := ZetaCommonTools.StrLleno( FHost );
          if not Result then
             LogError( 'Datos Globales No Configurados' + CR_LF + 'Favor De Indicar El Servidor De Correos En El Cat�logo de Globales', TRUE );
          FUserID := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_USERID ), 'ReportesViaEmail' );
          FPassword := Global.GetGlobalString( K_GLOBAL_EMAIL_PSWD );
          FFromAddress := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMADRESS ), 'ReportesViaEmail@dominio.com' );
          FFromName := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMNAME ), 'Sistema TRESS-M�dulo Reportes V�a Email' );
          FErrorMail := Global.GetGlobalString( K_GLOBAL_EMAIL_MSGERROR );
     end;
end;

function TdmReportes.ConectaHost: Boolean;
begin
     with dmEmailService do
     begin
          NewEMail;
          MailServer := FHost;
          if FPuerto > 0 then
             Port := FPuerto;
          User := FUserId;
          Password := FPassword;
          FromAddress := FFromAddress;
          FromName := FFromName;
          AuthMethod := FAuthMethod;
          Result := TRUE;
     end;
end;

procedure TdmReportes.AgregaAttachments(oLista: TStrings);
 var
    oFile: TSearchRec;
    sExt, sDir : string;
    iPageCount : integer;
begin
     iPageCount := 0;

     if DatosImpresion.Tipo IN [ tfHTML,tfHTM,tfTXT,tfWMF,tfEMF,tfBMP,tfPNG,tfJPEG,tfSVG ] then
     begin
          sExt := ExtractFileExt( FArchivoHTML );
          sDir := ExtractFilePath( FArchivoHTML );
          if FindFirst( Copy( FArchivoHTML, 1, Pos( sExt, FArchivoHTML )-1 ) + '*' + sExt, faArchive, oFile) = 0 then
          begin
               oLista.Add( sDir + oFile.Name );
               Inc( iPageCount );
               while ( iPageCount <= FPageCount) AND ( FindNext( oFile ) = 0 ) do
               begin
                    oLista.Add( sDir + oFile.Name );
                    Inc( iPageCount );
               end;
          end;
     end
     else
         oLista.Add( FArchivoHTML );
end;


procedure TdmReportes.DoAfterGetResultado(const lResultado: Boolean; const Error: string);
begin
     inherited;
     if lResultado then
     begin
          FArchivoHTML := DatosImpresion.Exportacion;
          //ModificaImagenHtml;
     end;
end;

procedure TdmReportes.AgregaEmailUsuario;
var
   sUsuario: String;
begin
     with cdsUsuarios do
     begin
          if Locate( 'US_CODIGO', VarArrayOF( [ dmCliente.cdsSuscripUsuarios.FieldByName( 'US_CODIGO' ).AsInteger ] ), [] ) then
          begin
               sUsuario := FieldByName( 'US_EMAIL' ).AsString;
               if ( Pos ( '@', sUsuario ) > 0 ) and ( Pos ( '.', sUsuario ) > 0 ) then
                  FUsuariosHTML.AddObject( sUsuario, TObject( 0 ) )
               else
                   LogEmail( Format( 'La direcci�n %s no es v�lida', [ EntreComillas( sUsuario ) ] ) );
          end;
     end;
end;

function TdmReportes.ConectaUsuariosCorreo: Boolean;
var
   oUsuarios: OleVariant;
   sMensaje: string;
begin
     with dmCliente do
     begin
          GetUsuariosSuscritos( Empresa[P_CODIGO], Confidencialidad, zStrToBool( cdsCompany.FieldByName('CM_KCONFI').AsString ), oUsuarios );
     end;

     cdsUsuarios.Data:= oUsuarios;

     Result := not ( dmCliente.cdsSuscripUsuarios.IsEmpty OR cdsUsuarios.IsEmpty );

     if NOT RevisaUsuariosInactivos( sMensaje ) then
        LogEmail( 'Los siguientes usuarios est�n inactivos, los correos a esos usuarios no se enviaron: ' + CR_LF + sMensaje );

     if ( not Result ) then
     begin
           LogError( 'Correo Electr�nico sin configurar '+ CR_LF + 'Verifique los Usuarios Asignados a la Empresa.', TRUE );
           LogEmail( Format( 'Correo Electr�nico sin configurar '+ CR_LF + 'Verifique los Usuarios Asignados a la Empresa %s', [dmCliente.Empresa[P_CODIGO]] ) );
     end;
end;

procedure TdmReportes.LogEmailEnd;
const
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
begin
     with FBitacoraMail do
     begin
          if not Used then
          begin
               with Application do
               begin
                    Init( ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + 'BitacoraCorreos.log' );
               end;
          end;
     end;
     LogEmail( Format( 'Fin de env�o de correo:      %s', [ FormatDateTime( K_DIA_HORA, Now ) ] ) );
end;

procedure TdmReportes.LogEmailInit;
const
     K_RELLENO = 10;
begin
     with FBitacoraMail do
     begin
          if not Used then
          begin
               with Application do
               begin
                    Init( ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + 'BitacoraCorreos.log' );
               end;
          end;
          WriteTimeStamp( CR_LF + StringOfChar( '*', K_RELLENO ) + ' Inicio %s' + StringOfChar( '*', K_RELLENO )  );
     end;
end;

procedure TdmReportes.LogEmail(const sTexto: String);
begin
     try
        with FBitacoraMail do
        begin
             if not Used then
             begin
                  with Application do
                  begin
                       Init( ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + 'BitacoraCorreos.log' );
                  end;
             end;
             WriteTexto( sTexto );
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error al escribir a bit�cora'+ CR_LF + Error.Message, TRUE );
           end;
     end;
end;
end.

