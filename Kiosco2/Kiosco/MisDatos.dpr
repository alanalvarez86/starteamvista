program MisDatos;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Forms,
  sysutils,
  ZetaDialogo,
  FFormaSalida,
  FFormaProblema,
  DCliente in 'DCliente.pas',
  FBaseCarrousel in 'FBaseCarrousel.pas' {BaseCarrousel},
  ZetaSystemWorking in 'ZetaSystemWorking.pas' {ZSystemWorking},
  FKeyboardBase in 'FKeyboardBase.pas' {KeyboardBase},
  FKeyboardNumerico in 'FKeyboardNumerico.pas' {KeyboardNumerico},
  DGlobalComparte in '..\DataModules\DGlobalComparte.pas',
  DBaseDiccionario in '..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DExportEngine in '..\..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  dmailmerge in '..\..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DReportesGenerador in '..\..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  try
     dmCliente := TdmCliente.Create( Application );
     try
        if not dmCliente.Sentinel_EsProfesional_MSSQL then
        begin
             try
                if FindCmdLineSwitch( 'CONFIG', [ '/', '-' ], True ) then
                begin
                     dmCliente.ConfiguracionKiosco;
                end
                else
                begin
                     with dmCliente do
                     begin
                          if Init then
                             StartShow;
                     end;
                end;
                Application.Title := 'TressMisDatos';
                Application.Run;
             except
                   on Error: Exception do
                   begin
                        dmCliente.MuestraFormaError( Error );
                   end;
             end;
        end
        else
            ZetaDialogo.ZError('Mis Datos','M�dulo no disponible para la licencia Profesional de Sistema TRESS',0);
        finally
               dmCliente.Free;
        end;
  except
        on Error: Exception do
        begin
             FFormaProblema.MuestraExcepcion( Error );
        end;
  end;
end.
