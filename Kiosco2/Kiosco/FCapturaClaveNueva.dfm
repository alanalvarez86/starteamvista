object CapturaClaveNueva: TCapturaClaveNueva
  Left = 231
  Top = 85
  BorderIcons = []
  BorderStyle = bsNone
  BorderWidth = 5
  Caption = 'CapturaClaveNueva'
  ClientHeight = 514
  ClientWidth = 689
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelTeclado: TfcPanel
    Left = 0
    Top = 0
    Width = 689
    Height = 514
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvNone
    BevelWidth = 5
    BorderWidth = 2
    ParentColor = True
    TabOrder = 0
    object panelTexto: TfcPanel
      Left = 7
      Top = 7
      Width = 675
      Height = 93
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
      BevelWidth = 5
      Caption = 'Su clave de acceso no ha sido especificada'
      Color = 11974253
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object panelBotones: TfcPanel
      Left = 7
      Top = 100
      Width = 675
      Height = 407
      Align = alClient
      BevelInner = bvLowered
      BevelOuter = bvNone
      BevelWidth = 5
      ParentColor = True
      TabOrder = 1
      object fcLabel1: TfcLabel
        Left = 120
        Top = 24
        Width = 441
        Height = 29
        Caption = 'Por favor capture su fecha de nacimiento:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaTop
      end
      object fcLBLDia: TfcLabel
        Left = 29
        Top = 96
        Width = 43
        Height = 29
        Caption = 'D'#237'a:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaTop
      end
      object fcLBLMes: TfcLabel
        Left = 19
        Top = 180
        Width = 53
        Height = 29
        Caption = 'Mes:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaTop
      end
      object fcLBLYear: TfcLabel
        Left = 23
        Top = 267
        Width = 49
        Height = 29
        Caption = 'A'#241'o:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaTop
      end
      object FechaAsText: TLabel
        Left = 288
        Top = 263
        Width = 370
        Height = 32
        Alignment = taCenter
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -27
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object fcOK: TfcShapeBtn
        Left = 147
        Top = 314
        Width = 163
        Height = 81
        Caption = 'OK'
        Color = clGreen
        Default = True
        DitherColor = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -32
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        Layout = blGlyphTop
        NumGlyphs = 2
        ParentClipping = False
        ParentFont = False
        RoundRectBias = 25
        ShadeStyle = fbsHighlight
        Shape = bsRoundRect
        TabOrder = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        OnClick = fcOKClick
      end
      object fcCancelar: TfcShapeBtn
        Left = 363
        Top = 314
        Width = 163
        Height = 81
        Cancel = True
        Caption = 'Cancelar'
        Color = clRed
        DitherColor = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -32
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Kind = bkCancel
        Layout = blGlyphTop
        ModalResult = 2
        ParentClipping = False
        ParentFont = False
        RoundRectBias = 25
        ShadeStyle = fbsHighlight
        Shape = bsRoundRect
        TabOrder = 1
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
      end
      object fcMeses: TfcButtonGroup
        Left = 79
        Top = 160
        Width = 585
        Height = 65
        AutoBold = True
        BevelOuter = bvNone
        ButtonClassName = 'TfcShapeBtn'
        ClickStyle = bcsRadioGroup
        ControlSpacing = 1
        Columns = 1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Layout = loHorizontal
        MaxControlSize = 0
        TabOrder = 2
        Transparent = False
        object fcMes1: TfcShapeBtn
          Left = 0
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Ene'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 0
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes1Click
        end
        object fcMes2: TfcShapeBtn
          Left = 49
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Feb'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 1
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes2Click
        end
        object fcMes3: TfcShapeBtn
          Left = 98
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Mar'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 2
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes3Click
        end
        object fcMes4: TfcShapeBtn
          Left = 147
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Abr'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 3
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes4Click
        end
        object fcMes5: TfcShapeBtn
          Left = 196
          Top = 0
          Width = 48
          Height = 65
          Caption = 'May'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 4
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes5Click
        end
        object fcMes6: TfcShapeBtn
          Left = 245
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Jun'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 5
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes6Click
        end
        object fcMes7: TfcShapeBtn
          Left = 294
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Jul'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 6
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes7Click
        end
        object fcMes8: TfcShapeBtn
          Left = 343
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Ago'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 7
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes8Click
        end
        object fcMes9: TfcShapeBtn
          Left = 392
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Sep'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 8
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes9Click
        end
        object fcMes10: TfcShapeBtn
          Left = 441
          Top = 0
          Width = 48
          Height = 65
          Caption = 'Oct'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 9
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes10Click
        end
        object fcMes11: TfcShapeBtn
          Left = 490
          Top = 0
          Width = 47
          Height = 65
          Caption = 'Nov'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 10
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes11Click
        end
        object fcMes12: TfcShapeBtn
          Left = 538
          Top = 0
          Width = 47
          Height = 65
          Caption = 'Dic'
          Color = clAqua
          DitherColor = clWhite
          GroupIndex = 1
          NumGlyphs = 0
          ParentClipping = True
          RoundRectBias = 25
          ShadeStyle = fbsHighlight
          Shape = bsRoundRect
          TabOrder = 11
          TextOptions.Alignment = taCenter
          TextOptions.VAlignment = vaVCenter
          OnClick = fcMes12Click
        end
      end
      object edDia: TEdit
        Left = 139
        Top = 88
        Width = 46
        Height = 45
        BevelInner = bvNone
        BevelOuter = bvNone
        BiDiMode = bdLeftToRight
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -32
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentBiDiMode = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        Text = '1'
      end
      object edYear: TEdit
        Left = 136
        Top = 259
        Width = 96
        Height = 45
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -32
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        Text = '1970'
      end
      object btnDiaUP: TfcShapeBtn
        Left = 84
        Top = 88
        Width = 51
        Height = 45
        Color = clBlue
        DitherColor = clWhite
        NumGlyphs = 0
        ParentClipping = False
        RoundRectBias = 25
        ShadeStyle = fbsHighlight
        Shape = bsArrow
        TabOrder = 5
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        OnClick = btnDiaUPClick
      end
      object btnDiaDOWN: TfcShapeBtn
        Left = 190
        Top = 88
        Width = 51
        Height = 45
        Color = clRed
        DitherColor = clWhite
        Orientation = soDown
        ParentClipping = False
        RoundRectBias = 25
        ShadeStyle = fbsHighlight
        Shape = bsArrow
        TabOrder = 6
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        OnClick = btnDiaDOWNClick
      end
      object btnYearUP: TfcShapeBtn
        Left = 84
        Top = 257
        Width = 51
        Height = 45
        Color = clBlue
        DitherColor = clWhite
        ParentClipping = False
        RoundRectBias = 25
        ShadeStyle = fbsHighlight
        Shape = bsArrow
        TabOrder = 7
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        OnClick = btnYearUPClick
      end
      object btnYearDOWN: TfcShapeBtn
        Left = 239
        Top = 257
        Width = 51
        Height = 45
        Color = clRed
        DitherColor = clWhite
        NumGlyphs = 0
        Orientation = soDown
        ParentClipping = False
        RoundRectBias = 25
        ShadeStyle = fbsHighlight
        Shape = bsArrow
        TabOrder = 8
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        OnClick = btnYearDOWNClick
      end
    end
  end
  object TimerSalida: TTimer
    Enabled = False
    OnTimer = TimerSalidaTimer
    Left = 23
    Top = 28
  end
end
