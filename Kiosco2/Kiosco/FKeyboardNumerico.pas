unit FKeyboardNumerico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FKeyboardBase, StdCtrls, fcButton, fcImgBtn, fcShapeBtn,
  ExtCtrls, fcpanel, fcImager;

type
  TKeyboardNumerico = class(TKeyboardBase)
    fcNumero1: TfcShapeBtn;
    fcNumero2: TfcShapeBtn;
    fcNumero3: TfcShapeBtn;
    fcNumero4: TfcShapeBtn;
    fcNumero5: TfcShapeBtn;
    fcNumero6: TfcShapeBtn;
    fcNumero7: TfcShapeBtn;
    fcNumero8: TfcShapeBtn;
    fcNumero9: TfcShapeBtn;
    fcNumero0: TfcShapeBtn;
    procedure FormCreate(Sender: TObject);
  private
    FMaximoDigitos: Byte;
    { Private declarations }
  public
    { Public declarations }
    property MaximoDigitos: Byte read FMaximoDigitos write FMaximoDigitos;
  end;

var
  KeyboardNumerico: TKeyboardNumerico;

implementation

{$R *.dfm}

procedure TKeyboardNumerico.FormCreate(Sender: TObject);
begin
     //Esto es para que no se grabe antes de tiempo.
     FMaximoDigitos := 100;
     inherited;
end;

end.
