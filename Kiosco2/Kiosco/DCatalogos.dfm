object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  Left = 270
  Top = 209
  Height = 479
  Width = 741
  object cdsCondiciones: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    Left = 48
    Top = 128
  end
  object cdsNomParamLookUP: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    Params = <>
    UsaCache = True
    Left = 272
    Top = 112
  end
end
