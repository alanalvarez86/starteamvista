unit FFormaSalida;

interface

uses Windows, Messages, SysUtils, {$Ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, fcButton, fcImgBtn, fcShapeBtn, fcpanel,
     FKeyboardNumerico, StdCtrls, FKeyboardBase, fcImager;

type
  TPideSalida = class(TKeyboardNumerico)
    TimerSalida: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure fcOKClick(Sender: TObject);
    procedure fcCancelarClick(Sender: TObject);
    procedure TimerSalidaTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    FClave: String;
    FIntentos: Integer;
    FIntentosMaximos: Integer;
    procedure SetClave(const Value: String);
    procedure ResetTimerSalida;
  protected
   { Protected declarations }
    procedure AlCapturar; override;
    procedure AlSalir; override;
  public
    { Public declarations }
    property Clave: String read FClave write SetClave;
    property IntentosMaximos: Integer read FIntentosMaximos write FIntentosMaximos;
  end;

function PreguntaSiPuedeSalir( const sClave: String ): Boolean;

implementation

uses DCliente,
     FKioscoRegistry,
     ZetaSystemWorking;

{$R *.dfm}

var
  PideSalida: TPideSalida;

function PreguntaSiPuedeSalir( const sClave: String ): Boolean;
begin
     Result := False;
     PideSalida := TPideSalida.Create( Application );
     try
        with PideSalida do
        begin
             Clave := sClave;
             Mascara := '*';
             //WindowState := wsMaximized;
             ShowModal;
             if IsOk then
             begin
                  Result := True;
             end;
        end;
     finally
            FreeandNil( PideSalida );
     end;
end;

{ ***** TPideSalida ***** }

procedure TPideSalida.FormCreate(Sender: TObject);
begin
     inherited;
     FIntentos := 0;
     FIntentosMaximos := KioskoRegistry.Intentos;
end;

procedure TPideSalida.FormShow(Sender: TObject);
begin
     inherited;
     FIntentos := 0;
end;

procedure TPideSalida.SetClave(const Value: String);
begin
     FClave := AnsiUpperCase( Value );
end;

procedure TPideSalida.fcOKClick(Sender: TObject);
begin
     inherited;
     if ( Valor = Clave ) then
        ModalResult := mrOK
     else
     begin
          Inc( FIntentos );
          if ( IntentosMaximos > 0 ) and ( FIntentos >= IntentosMaximos ) then
          begin
               ModalResult := mrCancel;
          end
          else
          begin
               with PanelMensaje do
               begin
                    Caption := '� Clave Incorrecta ! Intente de nuevo';
                    Font.Color := ColorError;
               end;
               Clear;
          end;
     end;
end;

procedure TPideSalida.fcCancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TPideSalida.ResetTimerSalida;
begin
     with TimerSalida do
     begin
          Enabled := False;
          Interval := dmCliente.TimeoutDatos;
          Enabled := True;
     end;
end;

procedure TPideSalida.TimerSalidaTimer(Sender: TObject);
begin
     inherited;
     ModalResult := mrCancel;
end;

procedure TPideSalida.FormActivate(Sender: TObject);
begin
     inherited;
     PanelMensaje.Font.Color := ColorNormal;
     PanelMensaje.Caption := 'Capture la clave de salida';
end;

procedure TPideSalida.AlCapturar;
begin
     inherited AlCapturar;
     ResetTimerSalida;
end;

procedure TPideSalida.FormResize(Sender: TObject);
begin
     inherited;
     with PanelTeclado do
     begin
          if ( Self.Height > Height ) then
             Top := Trunc( ( Self.Height - Height ) / 2 );
          if ( Self.Width > Width ) then
             Left := Trunc( ( Self.Width - Width ) / 2 );
     end;
end;

procedure TPideSalida.AlSalir;
begin
     inherited AlSalir;
     dmCliente.InterceptaMensajes := True;
end;

end.
