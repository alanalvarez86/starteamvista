inherited KeyboardAlphaNum: TKeyboardAlphaNum
  Left = 210
  Top = 123
  Caption = 'KeyboardAlphaNum'
  ClientHeight = 488
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited fcImagenFondo: TfcImager
    Height = 380
  end
  inherited fcPanel2: TfcPanel
    Height = 380
    inherited fcPanel1: TfcPanel
      inherited PanelMensaje: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited PanelClave: TfcShapeBtn
        NumGlyphs = 0
      end
    end
    inherited PanelTeclado: TfcPanel
      Height = 264
    end
  end
  object PanelBotones: TfcPanel
    Left = 0
    Top = 380
    Width = 646
    Height = 108
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    BevelWidth = 5
    Color = 11974253
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object fcOK: TfcShapeBtn
      Left = 142
      Top = 11
      Width = 163
      Height = 81
      Color = clGreen
      Default = True
      DitherColor = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Kind = bkOK
      Layout = blGlyphTop
      ModalResult = 1
      ParentClipping = False
      ParentFont = False
      RoundRectBias = 25
      ShadeStyle = fbsHighlight
      Shape = bsRoundRect
      TabOrder = 0
      TextOptions.Alignment = taCenter
      TextOptions.VAlignment = vaVCenter
    end
  end
end
