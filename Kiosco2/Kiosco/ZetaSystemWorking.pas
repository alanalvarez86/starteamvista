unit ZetaSystemWorking;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
     fcimageform, fcLabel, vcl.fcimage;

type
  TZSystemWorking = class(TForm)
    fcImageForm1: TfcImageForm;
    fcTexto: TfcLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetMensaje( const Value: String );
    procedure WriteMensaje(const Value: String);
  end;

procedure InitAnimation( const sMensaje: String );
procedure EndAnimation;

var
  ZSystemWorking: TZSystemWorking;

implementation

{$R *.DFM}

procedure InitAnimation( const sMensaje: String );
begin
     ZSystemWorking := TZSystemWorking.Create( Application );
     with ZSystemWorking do
     begin
          SetMensaje( sMensaje );
          Show;
          Update;
     end;
end;

procedure EndAnimation;
begin
     if ( ZSystemWorking <> nil ) then
     begin
          with ZSystemWorking do
          begin
               Close;
               Free;
          end;
          ZSystemWorking := nil;
     end;
end;

{ ********* TZSystemWorking ********* }

procedure TZSystemWorking.SetMensaje( const Value: String );
begin
     fcTexto.Caption := Value;
end;

procedure TZSystemWorking.WriteMensaje( const Value: String );
begin
     SetMensaje( Value );
     Application.ProcessMessages;
end;

end.
