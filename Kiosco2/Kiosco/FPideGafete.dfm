inherited PideGafete: TPideGafete
  Left = 249
  Top = 106
  Caption = 'PideGafete'
  ClientHeight = 492
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelPrincipal: TfcPanel
    Height = 384
    inherited panelTexto: TfcPanel
      Height = 68
    end
    inherited PanelTeclado: TfcPanel
      Top = 75
      Height = 302
      inherited fcClear: TfcShapeBtn
        Top = 232
        TabOrder = 1
      end
      inherited fcBackspace: TfcShapeBtn
        Top = 232
        TabOrder = 40
      end
      inherited edTexto: TEdit
        TabOrder = 0
        Visible = False
      end
      inherited fcCancelar: TfcShapeBtn
        Left = 336
        Top = 240
      end
      inherited fcShapeBtn38: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn35: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn33: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn32: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn28: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn27: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn26: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn23: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn21: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn20: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn18: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn15: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn13: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn12: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn6: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn4: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcShapeBtn3: TfcShapeBtn
        NumGlyphs = 1
      end
      object fcPanelInfo: TfcPanel
        Left = 5
        Top = 5
        Width = 652
        Height = 73
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvSpace
        BevelWidth = 5
        Caption = 'Pasa tu gafete o captura tu n'#250'mero de empleado'
        Color = clSilver
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
  end
  inherited PanelBotones: TfcPanel
    Top = 384
  end
  object TimerSalida: TTimer
    Enabled = False
    OnTimer = TimerSalidaTimer
    Left = 23
    Top = 28
  end
end
