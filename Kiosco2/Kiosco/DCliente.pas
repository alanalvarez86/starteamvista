unit DCliente;

interface
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, OleCtrls, SHDocVw, ExtCtrls,
     qrprntr,
     FBaseKiosco,
     FBaseCarrousel,
     {$ifdef DOS_CAPAS}
     DServerKiosco,
     DServerReportes,
     {$else}
     ReporteadorDD_TLB,
     Kiosco_TLB,
     {$endif}
     DBasicoCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet,
     FAutoClasses;

const
     K_CAMPO_DIGITO = 'CM_DIGITO';
     K_ARROBA_KIOSCO = '@KIOSCO';
     K_ARROBA_SERVIDOR = '@SERVIDOR';
     K_ARROBA_PANTALLA = '@PANTALLA';
     K_MIL = 1000;
     //K_CAMPO_DIGITO = 'CM_ALIAS'; { Ojo si se usa COMPANYS.CM_ALIAS en el OleVariant de Empresa Activa }

     // (JB) MEnsajes de error de impresion de reportes
     K_ERR_DOC_IMPRESO            = 'Este documento ya fue impreso';
     K_ERR_DOC_IMPRESO_LIM_PER    = 'Se ha Excedido el L�mite de Impresiones para este Periodo de N�mina';

type
  TPideNIPReply = ( pnrOK, pnrCancel, pnrChangePassword );
  TdmCliente = class(TBasicoCliente)
    cdsBotones: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsPeriodoActivo: TZetaClientDataSet;
    cdsReportes: TZetaClientDataSet;
    cdsCarrusel: TZetaClientDataSet;
    cdsPantalla: TZetaClientDataSet;
    cdsEstilo: TZetaClientDataSet;
    cdsShow: TZetaClientDataSet;
    TimerCarrousel: TTimer;
    cdsKioscos: TZetaClientDataSet;
    cdsAccesoEmp: TZetaClientDataSet;
    cdsSuscripUsuarios: TZetaClientDataSet;
    cdsSesionImpReportes: TZetaClientDataSet; // (JB) Contador de reportes
    cdsKioscoImpresiones: TZetaClientDataSet;
    cdsHuellas: TZetaClientDataSet; //BIOMETRICO
    procedure cdsHuellasAlAdquirirDatos(Sender: TObject); //BIOMETRICO
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsEmpleadoCalcFields(DataSet: TDataSet);
    procedure TimerCarrouselTimer(Sender: TObject);
    procedure cdsKioscosAlAdquirirDatos(Sender: TObject);
    procedure cdsAccesoEmpAlAdquirirDatos(Sender: TObject);
    procedure cdsKioscoImpresionesAlEnviarDatos(Sender: TObject);
    procedure cdsKioscoImpresionesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
  private
    { Private declarations }

    FFormaDatos: TBaseKiosco;
    FFormaInfo: TBaseCarrousel;
    FListaFormas: TList;
    FListaBrowsers: TStringList;
    FPosicion: Integer;
    FBotones: TListaBotones;

    FEmpleadoNombre: String;
    FEmpresaNombre: String;
    FEmpleadoActivo: TNumEmp;
    FEmpleadoNIP: String;
    FEmpleadoNatalicio: TDate;
    FTipoNomina: eTipoPeriodo;
    FEmpleadoConfiden: String;
    FParametros: TZetaParams;
    FParamsBitacora: TZetaParams;
    {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
    FTimeoutDefault : Integer;}
    FTimeoutDatos : Integer;
    FCountConsultas: Integer;
    FNumeroEmpleado : String;
    FMensajeError: String;
    FSalirSistema: Boolean;
    FKioscoIniciado: Boolean;
    FInterceptaMensajes: Boolean;
    FMuestraFormaPideGafete: Boolean;
    FAdministrador: Boolean;
    FPideNip : Boolean;
    FPanelReportes: TWinControl;
    FQrPreview: TQRPreview;
    FParametrosURL :  TZetaParams;
//    FParamsReporte : String;
    FUsaEmpresaEmpleado: Boolean;
    FClasifiEmpresaEmpleado: Integer;
    FClasifiEmpresaDefault: Integer;
    FUltimoReporte: String;
    FUltimoRepFormato: eTipoFormato;
    FLimiteImpresiones:Integer;
    {$ifdef Antes}
    FId: string;
    {$endif}
    {$ifdef DOS_CAPAS}
    FServerKiosco: TdmServerKiosco;
    FServerReportes: TdmServerReportes;
    {$else}
    FServerKiosco: IdmServerKiosco;
    FServerReportes : IdmServerReporteadorDD;
    FTipoGafete: eTipoGafete; 
    FPrimeraVez: Boolean; //BIOMETRICO
    FSync: Boolean; //BIOMETRICO
    FSiguienteHora: String;
    FFechaActual : TDate;
    function GetServerKiosco: IdmServerKioscoDisp;
    function GetServerReportes: IdmServerReporteadorDDDisp;
    {$endif}
    function GetIMSSMes: Integer;
    function GetIMSSPatron: String;
    function GetIMSSTipo: eTipoLiqIMSS;
    function GetIMSSYear: Integer;
    function SalirDelSistema: Boolean;
    function GetEmpresaReportes: string;
    function GetAuto: OleVariant;
    procedure InitAutorizacion;
    procedure IniciarcdsSesionImpReportes;
    procedure EscribeBitacoraInfo( const eClase: eClaseBitacora; const eTipoBitKiosco: eTipoBitacora; const sMarco, sTexto: String );
    function TransformaTipoGafete( const iTipoGafeteRegistry : Integer ) : eTipoGafete; //BIOMETRICO
  protected
    { Protected declarations }
    property Parametros: TZetaParams read FParametros;
    function ValidaGafete( sGafete: String; var sError: String; var lHuboErrores: Boolean ): Boolean;
    function PideNIPEmpleado(FParent: TWinControl = NIL): Boolean;

    procedure CompanyFind( const sDigito: String );
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo( Parametros: TZetaParams );
    procedure CargaActivosSistema( Parametros: TZetaParams );
    procedure ShowConfigure( const sShow: String );
    procedure ShowLoad( const sShow: String );
  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerKiosco: TdmServerKiosco read FServerKiosco;
    {$else}
    property ServerReportes: IdmServerReporteadorDDDisp read GetServerReportes;
    property ServerKiosco: IdmServerKioscoDisp read GetServerKiosco;
    {$endif}
    property EmpleadoActivo: TNumEmp read FEmpleadoActivo;
    property Empleado: TNumEmp read FEmpleadoActivo;
    property EmpleadoNombre: String read FEmpleadoNombre;
    property EmpleadoNIP: String read FEmpleadoNIP;
    property EmpleadoNatalicio: TDate read FEmpleadoNatalicio;
    property EmpresaNombre: String read FEmpresaNombre;
    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;
    property TipoNomina: eTipoPeriodo read FTipoNomina;
    property KioscoIniciado: Boolean read FKioscoIniciado;
    property NumeroEmpleado: String read FNumeroEmpleado write FNumeroEmpleado;
    property FormaDatos : TBaseKiosco read FFormaDatos;
    property TimeoutDatos : Integer read FTimeoutDatos;
    property PideNip: Boolean read FPideNip;
    property ParametrosURL : TZetaParams read FParametrosURL write FParametrosURL;
//    property ParamsReporte: String read FParamsReporte write FParamsReporte;
    property ParamsBitacora: TZetaParams read FParamsBitacora write FParamsBitacora;
    Property UsaEmpresaEmpleado : Boolean  read FUsaEmpresaEmpleado write FUsaEmpresaEmpleado;
    property ClasifiEmpresaDefault: Integer read FClasifiEmpresaDefault write FClasifiEmpresaDefault ;
    property ClasifiEmpresaEmpleado:Integer read FClasifiEmpresaEmpleado write FClasifiEmpresaEmpleado;
    property EmpleadoConfiden: String read FEmpleadoConfiden;
    {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
    property TimeoutDefault : Integer read FTimeoutDefault;}
    property MensajeError: String read FMensajeError write FMensajeError;
    property SalirSistema: Boolean read FSalirSistema write FSalirSistema;
    property InterceptaMensajes: Boolean read FInterceptaMensajes write FInterceptaMensajes;
    property MuestraFormaPideGafete: Boolean read FMuestraFormaPideGafete write FMuestraFormaPideGafete;
    property PanelReportes: TWinControl read FPanelReportes write FPanelReportes;
    property QrPreview: TQrPreview read FQrPreview write FQrPreview;
    property EmpresaReportes: string read GetEmpresaReportes;
    property UltimoRepFormato: eTipoFormato read FUltimoRepFormato;

    property TipoGafete: eTipoGafete read FTipoGafete; //BIOMETRICO
    property PrimeraVez: Boolean read FPrimeraVez write FPrimeraVez; //BIOMETRICO
    property Sync: Boolean read FSync write FSync;

    function GetPantalla( const sPantalla: String ): TBaseKiosco;
    function BuildValoresActivos: String;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function Init: Boolean;
    function PosicionaEmpleado(const sDigito, sCredencial: String; const iEmpleado: TNumEmp; var sTexto: String; var lHuboErrores: Boolean ): Boolean;
    function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String; override;
    function EsBiometrico: Boolean;
    function FileTemplates: String;
    function GafeteAdmin: String;

    //function GetValorActivo( const sValor: String ): String;override;
    function VerificaCodigoKiosco: Boolean;
    function GetURLKiosco( const sURL : string; const eAccion: eAccionBoton = abContenido ): string;
    function CambiaNIPEmpleado(FParent: TWinControl = NIL): Boolean;
    function EscribeNIPEmpleado(const sClave: String): Boolean;

    function GeneraReporte(const iReporte: integer ): Boolean;
    function ImprimeReporte(const iReporte: integer ): Boolean;
    function PuedeImprimirReporte( const iReporte: Integer; const sParamsReporte: String; var sMensaje: String ): Boolean;
    function GetParamsReporte: String;

    procedure MuestraForma( const iPos: Integer);
    procedure MuestraBrowser( const iPos: Integer );
    procedure MuestraBrowserSiguiente;
    procedure StartShow;
    procedure MuestraFormaError( const Error: Exception );
    procedure MuestraErrorcito( const sError: String );
    procedure MuestraPideGafete;overload;
    procedure MuestraPideGafete( const sGafeteTeclado: string );overload;

    procedure GeneraBitacora( const sMensajeError:String );
    procedure ConfiguracionKiosco;
    procedure CompanyLoad;
    procedure CargaActivosTodos( Parametros: TZetaParams );override;
    procedure SetDatosPeriodoActivo( iNumero: integer; eTipo: eTipoPeriodo; iYear: integer );

    procedure CargaReportes;
    procedure AgregaSesionImpReportes( const iReporte: Integer );
    procedure GetBotones(const sPantalla : String );
    procedure GetCarrusel(const sCarrusel: String );
    procedure GetEstilo(const sEstilo: String );
    procedure EscribeBitacora( const eClase: eClaseBitacora; const eTipoBitKiosco: eTipoBitacora = tbNormal; const oBoton: TBotonKiosco = NIL );
    procedure BitacoraImprimirReporte( const iReporte: Integer; const sMarco: String; const lImprimeOK: Boolean );
    procedure GetUsuariosSuscritos( const sEmpresa,sConfiden: String; const lPorConfiden: Boolean; var oUsuarios: OleVariant );
    procedure DespreparaEnvioCorreo(const lExito: Boolean );
    procedure AgregaImpresionNomina( const iReporte: Integer );
    function ValidaLimiteNomina:Boolean;
    procedure InitTempDataset( DataSet : TClientDataset );
    procedure IniciarcdsKioscoImpresiones;
    function RevisaLimiteNomina:Boolean;
    function GetCodigoKioscoID: string;
    function GetBioMessage(error: Integer): String;
    procedure ReiniciaHuellas;                   //BIOMETRICO
    procedure ReportaHuella( sLista : String ); // BIOMETRICO
    procedure ValidacionSincronizacion;         // BIOMETRICO
    procedure SetSiguienteHora;                 // BIOMETRICO
    function GetMinutos( sHoras : String ) : Integer; //BIOMETRICO
    function GetHoras( sHoras : String ) : Integer; //BIOMETRICO

  end;

var
  dmCliente: TdmCliente;

implementation

uses DReportes,
     DDiccionario,
     DGlobal,
     DGlobalComparte,
//     DCapturaAsistencia, //AV 21 Junio 2007
     ZGlobalTress,
     ZetaCommonTools,
     ZetaSystemWorking,
     ZetaDialogo,
     ZetaWinApiTools,
     FKioscoRegistry,
     FServidorName,
     FConfiguraKiosco,
     FCambiaClave,
     FCapturaClaveNueva,
     FFormaProblema,
     FFormaError,
     //FPideGafete,
     FPideNIP,
     FCambiaNip,
     FValoresActivos,
     ZReportConst,
     FDlgInformativo,
     Variants, DReportesGenerador, ZReportTools, DateUtils;

{$R *.DFM}

const
     K_LEN_ANTIGUO = 40;
     K_INVALIDO = 'Gafete inv�lido';
     K_GUION = ' - ';
     K_ERROR_REPORTE = 'Error al generar reporte # %d';

{ ******** TdmCliente ******** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     Randomize;
     {$ifdef Antes}
     FId := IntToStr( Random(High(Integer)) );
     {$endif}
     FKioscoIniciado := False;
     inherited;

     FKioscoRegistry.InitKioskoRegistry;

     {$ifdef DOS_CAPAS}
     FServerKiosco := TdmServerKiosco.Create( self );
     FServerReportes := TdmServerReportes.Create( self );
     {$endif}
     if dmDiccionario = NIL then
        dmDiccionario := TdmDiccionario.Create( Self );

//      if dmCapturaAsistencia = NIL then
//        dmCapturaAsistencia := TdmCapturaAsistencia.Create( Self );

     GlobalComparte := TdmGlobalComparte.Create;
     GlobalComparte.Conectar;

     FParametros:= TZetaParams.Create;
     FParamsBitacora:= TZetaParams.Create;
     FParametrosURL := TZetaParams.Create;
     Usuario := 999;

     FFormaDatos := nil;
     FFormaInfo := nil;
     FListaFormas := TList.Create;
     FListaBrowsers := TStringList.Create;
     FPosicion := 0;
     MuestraFormaPideGafete := True;
     InterceptaMensajes := True;
     FTipoGafete := egTress;
     FBotones := TListaBotones.Create ( FFormaDatos );
     IniciarcdsSesionImpReportes;
     FLimiteImpresiones :=  StrToIntDef( GlobalComparte.GetGlobalString( K_KIOSCO_LIMITE_IMPRESIONES ),0);
     IniciarcdsKioscoImpresiones;

     if ( EsBiometrico ) then
        SetSiguienteHora;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( dmReportes );

     TimerCarrousel.Enabled := False;
     FKioscoRegistry.ClearKioskoRegistry;

     FreeAndNil( FListaFormas );
     FreeAndNil( FListaBrowsers );
     FreeAndNil( FParametros );
     FreeAndNil( FParamsBitacora );
     FreeAndNil( FParametrosURL );
     FreeAndNil( GlobalComparte );

     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerKiosco );
     {$endif}
     inherited;
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerKiosco: IdmServerKioscoDisp;
begin
     Result := IdmServerKioscoDisp( CreaServidor( Class_dmServerKiosco, FServerKiosco ) );
end; 

function TdmCliente.GetServerReportes: IdmServerReporteadorDDDisp;
begin
     Result:= IdmServerReporteadorDDDisp( CreaServidor( CLASS_dmServerReporteadorDD, FServerReportes ) );
end;
{$endif}

{ ***** M�todos ******** }

function TdmCliente.PosicionaEmpleado(const sDigito, sCredencial: String; const iEmpleado: TNumEmp; var sTexto: String; var lHuboErrores: Boolean ): Boolean;
begin
     lHuboErrores := False;
     FInterceptaMensajes := False;
     Result := False;
     try
        CompanyFind( sDigito );
        if EmpresaAbierta then
        begin
             with cdsEmpleado do
             begin
                  Data := ServerKiosco.KioscoEmpleado( Empresa, iEmpleado );
                  if IsEmpty then
                  begin
                       sTexto := Format( 'Empleado %d No Existe', [ iEmpleado ] );
                  end
                  else
                  begin
                       if ( FTipoGafete = egTress ) then
                       begin
                            Result := ( sCredencial = FieldByName( 'CB_CREDENC' ).AsString );
                            if NOT Result then
                                sTexto := 'Letra De Credencial Incorrecta'
                       end;

                       if( Result or ( FTipoGafete <> egTress ) )then
                       begin
                            cdsSesionImpReportes.EmptyDataSet; // (JB) Limpia el listado de reportes impresos que se tenia registrado anteriormente
                            Result := zStrToBool( FieldByName('CB_ACTIVO').AsString ) or
                                      ( FieldByName('CB_FEC_BAJ').AsDateTime > Date );
                            if NOT Result then
                               sTexto := K_INVALIDO;
                       end;

                       if Result then
                       begin
                            FInterceptaMensajes := True;
                            FEmpleadoActivo := iEmpleado;
                            FEmpleadoNombre := FieldByName( 'PRETTYNAME' ).AsString;
                            FEmpleadoNIP := FieldByName('CB_PASSWRD').AsString;
                            FEmpleadoNatalicio := FieldByName( 'CB_FEC_NAC' ).AsDateTime;
                            FTipoNomina := eTipoPeriodo( FieldByName( 'CB_NOMINA' ).AsInteger );
                            FEmpleadoConfiden:= FieldByName('CB_NIVEL0').AsString;
                            FFechaActual := StrAsFecha(FechaAsStr(Now));
                       end;
                  end;
             end;
        end
        else
            sTexto := Format( 'No Hay Empresas Con D�gito %s', [ sDigito ] );
     except
           on Error: Exception do
           begin
                lHuboErrores := True;
                Result := False;
                sTexto := Format( 'Error Al Buscar Empleado %d: %s', [ iEmpleado, Error.Message ] );
                MuestraErrorcito( sTexto );
           end;
     end;
end;

procedure TdmCliente.CompanyFind( const sDigito: String );
begin
     EmpresaAbierta := False;
     with cdsCompany do
     begin
          if Locate( K_CAMPO_DIGITO, sDigito, [] ) then
          begin
               SetCompany;
               EmpresaAbierta := True;
               FEmpresaNombre := FieldByName( 'CM_NOMBRE' ).AsString;
               //CompanyFind se manda llamar solamente cuando se conecta el empleado, aqui se asigna la clasificaci�n de acuerdo a a la empresa.
               FClasifiEmpresaEmpleado := FieldByName( 'CM_KCLASIFI').AsInteger;
          end;
     end;
end;

procedure TdmCliente.CompanyLoad;
begin
     cdsCompany.Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := TheMonth(Date);
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := tlOrdinaria;
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := TheYear( Date );
end;

procedure TdmCliente.CargaActivosSistema;
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo',  EmpleadoActivo );
          AddString( 'NombreUsuario', 'REP_KIOSKO' );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', ImssPatron );
          AddInteger( 'IMSSYear', ImssYear );
          AddInteger( 'IMSSMes', ImssMes );
          AddInteger( 'IMSSTipo', Ord( ImssTipo ) );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with cdsPeriodoActivo do
     begin
          if NOT Active or IsEmpty or ( FieldByName('PE_TIPO').AsInteger <> Ord( FTipoNomina ) ) or (StrAsFecha(FechaAsStr(Now)) <> FFechaActual )  then
             Data := Servidor.GetPeriodoInicial( Empresa, TheYear(Date), Ord( FTipoNomina ) );

          with Parametros do
          begin
               AddInteger( 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
               AddInteger( 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
               AddInteger( 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
          end;
     end;
end;

procedure TdmCliente.CargaActivosTodos( Parametros: TZetaParams );
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          with cdsPeriodoActivo do
          begin
               Year := FieldByName( 'PE_YEAR' ).AsInteger;
               Tipo := eTipoPeriodo(FieldByName( 'PE_TIPO' ).AsInteger);
               Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;


{ ******** Datos Personales ********** }
procedure TdmCliente.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsEmpleado do
     begin
          CreateCalculated( 'CB_ANTIGUO', ftString, K_LEN_ANTIGUO );
          CreateCalculated( 'CB_VENCE', ftDate, 0 );
     end;
end;

procedure TdmCliente.cdsEmpleadoCalcFields(DataSet: TDataSet);

function CalculaVencimiento( const dFecha: TDateTime; const iDuracion: Integer ): TDateTime;
//Tomada de D:\3Win_20\Datamodules\DRecursos.pas
begin
     if ( iDuracion = 0 ) then
         Result := 0
     else
         Result := dFecha + iDuracion - 1;
end;

begin
     inherited;
     with cdsEmpleado do
     begin
          FieldByName( 'CB_ANTIGUO' ).AsString := Copy( ZetaCommonTools.TiempoDias( Date - FieldByName( 'CB_FEC_ANT' ).AsDateTime + 1, etDias ), 1, K_LEN_ANTIGUO );
          FieldByName( 'CB_VENCE' ).AsDateTime := CalculaVencimiento( FieldByName( 'CB_FEC_CON').AsDateTime, FieldByName( 'TB_DIAS').AsInteger );
     end;
end;

procedure TdmCliente.CargaReportes;
begin
     cdsReportes.Data :=  ServerReportes.GetReportes( Empresa, ord(crKiosco), ord(crFavoritos), ord(crSuscripciones), VACIO , TRUE );
end;

function TdmCliente.GetEmpresaReportes: string;
begin
     Result := cdsShow.FieldByName('CM_CODIGO').AsString;
end;

procedure TdmCliente.InitAutorizacion;
begin
     Autorizacion.Cargar( GetAuto );
end;

function TdmCliente.GetAuto: OleVariant;
begin
     Result:= Servidor.GetAuto;
end;

function TdmCliente.GetParamsReporte: String;

   function GetParametroTexto( const sName:string ): String;
   begin
        if Assigned( FParametrosURL.FindParam( sName ) ) then
           Result := FParametrosURL.ParamByName( sName ).AsString
        else
            Result := VACIO;
   end;

begin
     Result := VACIO;
     if Assigned( FParametrosURL ) then
     begin
          Result := GetParametroTexto( 'Year' ) +
                    GetParametroTexto( 'Tipo' ) +
                    GetParametroTexto( 'Numero' );
     end;
end;

function TdmCliente.ValidaLimiteNomina:Boolean;
begin
     Result := ( FLimiteImpresiones > 0 ) and ( GetParamsReporte <> VACIO  );
end;

function TdmCliente.RevisaLimiteNomina:Boolean;
var
   iImpresiones:Integer;
begin
     Result := True;
     if ValidaLimiteNomina then
     begin
          try
             ServerKiosco.GetImpresionesEmpleado( EmpleadoActivo,GetDatosEmpresaActiva.Codigo,FParametrosURL.VarValues,iImpresiones );
          except
                on E: Exception do
                begin
                     iImpresiones := 0
                end;
          end;
          Result := iImpresiones < FLimiteImpresiones;
     end;
end;

function TdmCliente.PuedeImprimirReporte( const iReporte: Integer; const sParamsReporte:String;
                                          var sMensaje: String ): Boolean;
begin
     Result := True;
     if KioskoRegistry.LimitarImpresion then
     begin
          Result := not cdsSesionImpReportes.Locate('EMPRESA;CB_CODIGO;RE_CODIGO;PARAMS', VarArrayOf( [ FEmpresaNombre, IntToStr( FEmpleadoActivo ), IntToStr( iReporte ), sParamsReporte ] ) , [] );
     end;
     if ( not Result ) then
        sMensaje := K_ERR_DOC_IMPRESO
     else
     begin
          Result := RevisaLimiteNomina;
          if( Not Result )then
              sMensaje := K_ERR_DOC_IMPRESO_LIM_PER;
     end;
end;

procedure TdmCliente.AgregaSesionImpReportes( const iReporte: Integer );
begin
     with cdsSesionImpReportes do
     begin
          Append;
          FieldByName( 'EMPRESA' ).AsString := FEmpresaNombre;
          FieldByName( 'CB_CODIGO' ).AsInteger := FEmpleadoActivo;
          FieldByName( 'RE_CODIGO' ).AsInteger := iReporte;
          FieldByName( 'PARAMS' ).AsString := GetParamsReporte;{FParamsReporte;}
          Post;
     end;
end;

procedure TdmCliente.AgregaImpresionNomina( const iReporte: Integer );
begin
     if ValidaLimiteNomina then
     begin
          with cdsKioscoImpresiones do
          begin
               Append;
               FieldByName( 'CM_CODIGO').AsString := GetDatosEmpresaActiva.Codigo;
               FieldByName( 'CB_CODIGO').AsInteger := FEmpleadoActivo;
               FieldByName( 'RE_CODIGO').AsInteger := iReporte;
               FieldByName( 'PE_TIPO').AsInteger := Ord(GetDatosPeriodoActivo.Tipo);
               FieldByName( 'PE_YEAR').AsInteger := GetDatosPeriodoActivo.Year;
               FieldByName( 'PE_NUMERO').AsInteger := GetDatosPeriodoActivo.Numero;
               FieldByName( 'KI_FECHA').AsDateTime := FechaDefault;
               FieldByName( 'KI_KIOSCO').AsString := ZetaWinAPITools.GetComputerName;
               FieldByName( 'KI_HORA' ).AsString := FormatDateTime('hh:nn:ss',Now);
               Post;
               Enviar;
          end;
     end;
end;

function TdmCliente.GeneraReporte( const iReporte: Integer ): Boolean;
begin
     if not Assigned( dmReportes ) then
        dmReportes := TdmReportes.Create( self );

     with dmReportes do
     begin
          Result := GeneraUnReporte( iReporte, TRUE );
          FUltimoReporte:= dmReportes.cdsReporte.FieldByName('RE_NOMBRE').AsString;
          FUltimoRepFormato := dmReportes.DatosImpresion.Tipo;
     end;
end;

function TdmCliente.ImprimeReporte( const iReporte: integer ): Boolean;
var
   sMensaje: String;
begin
     Result := FALSE;
     if not Assigned( dmReportes ) then
        dmReportes := TdmReportes.Create( self );

     with dmReportes do
     begin
          if PuedeImprimirReporte( iReporte, GetParamsReporte, sMensaje ) then
          begin
               Result := ImprimeUnReporte( iReporte );
               FUltimoReporte:= dmReportes.cdsReporte.FieldByName('RE_NOMBRE').AsString;
               FUltimoRepFormato := dmReportes.DatosImpresion.Tipo;
               if Result then
               begin
                    AgregaSesionImpReportes( iReporte );
                    AgregaImpresionNomina(iReporte);
               end;
          end
          else
              FDlgInformativo.MuestraDialogoInf( sMensaje );   // No se permite volver a imprimir el mismo reporte en la misma sesi�n
     end;
end;

procedure TdmCliente.DespreparaEnvioCorreo(const lExito: Boolean );
begin
     dmReportes.DespreparaEnvioCorreo(lExito);
end;

function TdmCliente.GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin
     Result := dmDiccionario.GetValorActivo( eValor );
end;

procedure TdmCliente.GetBotones(const sPantalla: String);
{$ifdef MISDATOS}
const
     MD_QTY_MISDATOS = 1;
     //MD_QTY_REPORTES = 1;
var
   iMisDatos: Integer;
   //iReportes: Integer;
   TipoAccion: eAccionBoton;
{$endif}
begin
    with cdsBotones do
    begin
         Data := ServerKiosco.GetBotones( sPantalla );
         {$ifdef MISDATOS}
         if ( not EOF ) then   // Solo se permite una cantidad y cierto tipo de botones
         begin
              First;
              //iReportes := 0;
              iMisDatos := 0;
              while ( not EOF ) do
              begin
                   TipoAccion := eAccionBoton( FieldByName( 'KB_ACCION' ).AsInteger );
                   if ( not ( TipoAccion in [ abMisDatos, {abReporte,} abCambiaNIP, abRegresar ] ) ) or
                      ( ( TipoAccion = abMisDatos ) and ( iMisDatos > MD_QTY_MISDATOS ) ) or
                      //( ( TipoAccion = abReporte ) and ( iReportes > MD_QTY_REPORTES ) ) or
                      ( ( TipoAccion = abCambiaNIP ) and ( not FPideNIP ) ) then
                      delete
                   else
                   begin
                        {
                        if ( TipoAccion = abReporte ) then
                           Inc( iReportes );
                        }
                        if ( TipoAccion = abMisDatos ) then
                           Inc( iMisDatos );
                        Next;
                   end;
              end;
              First;
         end;
         {$endif}
    end;
end;

procedure TdmCliente.GetCarrusel(const sCarrusel: String );
{$ifdef MISDATOS}
const
     MD_QTY_CARRUSEL = 2;
{$endif}
begin
     with cdsCarrusel do
     begin
          Data := ServerKiosco.GetCarrusel( sCarrusel );
          {$ifdef MISDATOS}
          if ( not EOF ) and ( RecordCount > MD_QTY_CARRUSEL ) then   // Solo se permite una p�gina
          begin
               Last;
               while ( not EOF ) and ( RecordCount > MD_QTY_CARRUSEL ) do
               begin
                    Delete;
               end;
          end;
          {$endif}
     end;
end;

procedure TdmCliente.GetEstilo(const sEstilo: String );
begin
     with cdsEstilo do
     begin
          Data := ServerKiosco.GetEstilo( sEstilo );
     end;
end;

function TdmCliente.GetPantalla(const sPantalla: String ): TBaseKiosco;
begin
     with cdsPantalla do
     begin
          Data := ServerKiosco.GetPantalla( sPantalla );
          if Eof then
             Result := nil
          else
          begin
               Application.CreateForm( TBaseKiosco, Result );
          end;
     end;
end;

procedure TdmCliente.ShowLoad( const sShow: String );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with cdsShow do
        begin
             ZetaSystemWorking.InitAnimation( 'Iniciando Carrousel' );
             Application.ProcessMessages;
             try
                Data := ServerKiosco.GetShow( sShow );
             finally
                    ZetaSystemWorking.EndAnimation;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;

procedure TdmCliente.ShowConfigure( const sShow: String );
var
   oCursor: TCursor;
   lPuedeSalir: Boolean;
   {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
   sFormaInfo,
   }
   sFormaDatos : String;
begin
     FInterceptaMensajes := True;
     FMuestraFormaPideGafete := True;
     FTipoGafete := TransformaTipoGafete( KioskoRegistry.TipoGafete ); //BIOMETRICO
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ( cdsShow.Active ) and ( cdsShow.RecordCount > 0 ) then
        begin
             // Antes de crear cualquier forma
             with cdsShow do
             begin
                  {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
                  sFormaInfo      := FieldByName( 'KW_SCR_INF' ).AsString;
                  FTimeoutDefault := FieldByName( 'KW_TIM_INF' ).AsInteger;
                  }
                  sFormaDatos     := FieldByName( 'KW_SCR_DAT' ).AsString;
                  FTimeoutDatos   := FieldByName( 'KW_TIM_DAT' ).AsInteger * K_MIL;
                  FPideNip        := zStrToBool( FieldByName( 'KW_GET_NIP' ).AsString );
             end;
             {CV: Build4 se descontinuo el uso de los campos KW_SCR_INF y KW_TIM_INF
             cdsPantalla.Data := ServerKiosco.GetPantalla( sFormaInfo );
             }
             FFormaInfo := TBaseCarrousel.Create( Application );

             if ( sFormaDatos <> '' ) then
             begin
                 FFormaDatos := GetPantalla( sFormaDatos );
             end
             else
             begin
                  FFormaProblema.MuestraError( Format( 'El carrusel "%s" no tiene asignado ning�n Marco de Informaci�n', [sShow] ) );
             end;
             if ( FFormaDatos <> NIL ) then
             begin
                  if FFormaInfo.PreparaCarrusel( sShow ) then
                     repeat
                        FFormaInfo.ShowCarrusel;
                        lPuedeSalir := FAdministrador;
                        if not lPuedeSalir then
                        begin
                             //lPuedeSalir := FFormaSalida.PreguntaSiPuedeSalir( KioskoRegistry.PasswordSalida )
                             with KioskoRegistry do
                                  lPuedeSalir := ( PideAdminNIP( Intentos, MaxDigitos, PasswordSalida ) = pnrOK )
                        end;
                     until( lPuedeSalir )
                  else
                  begin
                       FreeAndNil( FFormaDatos ); //Se tiene que destruir para que no procese los mensajes;
                       FFormaProblema.MuestraError( Format( 'El carrusel "%s" tiene la lista de p�ginas web vac�a', [sShow] ) );
                  end;
             end;
        end
        else
            FFormaProblema.MuestraError( 'Posibles causas:' + CR_LF +
                                         {$ifdef MISDATOS}
                                         '1) No se ha configurado MisDatos - Revise configuraci�n global' + CR_LF +
                                         '2) El servidor de base de datos no est� activo' );
                                         {$else}
                                         '1) No se ha asignado el c�digo de kiosco (Ejecutar con el par�metro \CONFIG)' + CR_LF +
                                         Format( '2) El c�digo "%s" de carrusel no existe', [sShow] ) + CR_LF +
                                         '3) El servidor de base de datos no est� activo' );
                                         {$endif}
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmCliente.GeneraBitacora( const sMensajeError:String );
const
     {$ifdef MISDATOS}
     K_FILENAME = 'MD';
     {$else}
     K_FILENAME = 'K2';
     {$endif}
var
   oFile: TextFile;
   sFileName, sFecha, sHoras: String;
   sHora, sMinuto, sSeg, sMsec: Word;
begin
     sFecha := ZetaCommonTools.FechaCorta(Date);
     sFecha := StrTransAll( sFecha, '/', '' );
     DecodeTime( Time, sHora, sMinuto, sSeg, sMsec );
     sHoras := InttoStr(sHora) + ':' + InttoStr(sMinuto) + ':' + InttoStr(sSeg);
     sFileName := K_FILENAME + '-' + sFecha + '.txt';
     try
        {$I-}
        AssignFile( oFile, sFileName );
        if FileExists( sFileName ) then
           Append( oFile )
        else
            Rewrite( oFile );
        {$ifdef MISDATOS}
        Writeln( oFile, '************** Errores para MisDatos( ' + DateToStr( Date ) +' '+ sHoras + ') ********************' );
        {$else}
        Writeln( oFile, '************** Errores para KioscoII( ' + DateToStr( Date ) +' '+ sHoras + ') ********************' );
        {$endif}
        Writeln( oFile, sMensajeError );
        Writeln( '______________________________________________________________________________________________________' );
        CloseFile( oFile );
        {$I+}
     except on E: Exception do
            ZetaDialogo.ZError( 'Error al escribir en bit�cora', E.Message, 0 );
     end;
end;

procedure TdmCliente.StartShow;
var
   sShow: String;
begin
     ZetaSystemWorking.InitAnimation( 'Iniciando Carrousel' );
     Application.ProcessMessages;
     try
        CompanyLoad;
     finally
            ZetaSystemWorking.EndAnimation;
     end;
     sShow := KioskoRegistry.CodigoKiosco;
     ShowLoad( sShow );
     // Antes no, debido a que ShowLoad() carga el Show
     FKioscoIniciado := True;
     InitAutorizacion;
     ShowConfigure( sShow );

end;

procedure TdmCliente.MuestraForma( const iPos: Integer);
var
   //FormaInfo: TBaseKiosco;
   FormaInfo: TBaseCarrousel;
begin
    //FormaInfo := TBaseKiosco( FListaFormas.Items[ iPos ] );
    FormaInfo := TBaseCarrousel( FListaFormas.Items[ iPos ] );
    FormaInfo.ShowContenido;
end;

procedure TdmCliente.MuestraBrowser( const iPos: Integer);
begin
     with TimerCarrousel do
     begin
          Enabled := FALSE;
          Interval := 3000;
          Enabled := TRUE;
     end;
     TWebBrowser( FListaBrowsers.Objects[ iPos ] ).Show;
end;

procedure TdmCliente.MuestraBrowserSiguiente;
var
   iAnterior : Integer;
begin
     iAnterior := FPosicion;
     FPosicion := FPosicion + 1;
     if ( FPosicion >= FListaBrowsers.Count ) then
        FPosicion := 0;
     MuestraBrowser( FPosicion );
     // Primero muestro el nuevo y luego escondo el actual
     // para que se vea mejor la transici�n entre browsers,
     // evitando que se vea el fondo gris de la forma
     TWebBrowser( FListaBrowsers.Objects[ iAnterior ] ).Hide;
end;

function TdmCliente.Init: Boolean;
var
   lOk: Boolean;
   sServidor: String;
begin
     lOk := False;
     with KioskoRegistry do
     begin
          if KioskoConfigurado then
             lOk := True
          else
          begin
               if KioskoRegistry.CanWrite then
               begin
                    if not KioskoRegistry.ClienteConfigurado then
                    begin
                         sServidor := ComputerName;
                         if FServidorName.CapturaServidor( sServidor ) then
                            ComputerName := sServidor;
                    end;
                    if FConfiguraKiosco.ConfiguraKiosco then
                       lOk := KioskoConfigurado;
               end
               else
               begin
                    {
                    No Se puede configurar el Registry debido a que se tiene
                    �nicamente derechos de lectura sobre el mismo
                    }
                    FFormaProblema.MuestraError( 'No se ha configurado este Kiosco' );
               end;
          end;
     end;
     Result := lOk;
end;

procedure TdmCliente.ConfiguracionKiosco;
var
   sServidor: String;
begin
     if KioskoRegistry.CanWrite then
     begin
          if FServidorName.CapturaServidor( sServidor ) then
          begin
               KioskoRegistry.ComputerName := sServidor;
               with cdsKioscos do
               begin
                    Conectar;
               end;
               if cdsKioscos.Active then
               begin
                    FConfiguraKiosco.ConfiguraKiosco;
               end;
          end;
     end
     else
     begin
          {
          No Se puede configurar el Registry debido a que se tiene
          �nicamente derechos de lectura sobre el mismo
          }
          FFormaProblema.MuestraError( 'No se puede configurar la aplicaci�n' + CR_LF +
                                       'Ejecute la aplicaci�n como usuario administrador' );
     end;
end;

function TdmCliente.SalirDelSistema: Boolean;
begin
     //Result := FFormaSalida.PreguntaSiPuedeSalir( KioskoRegistry.PasswordSalida );
     with KioskoRegistry do
          Result := ( PideAdminNIP( Intentos, MaxDigitos, PasswordSalida ) = pnrOK )
end;

procedure TdmCliente.TimerCarrouselTimer(Sender: TObject);
begin
     inherited;
     MuestraBrowserSiguiente;
end;

function TdmCliente.BuildValoresActivos: String;
begin
     Result := ServerKiosco.GetValoresActivos( GetCodigoKioscoID, EmpleadoActivo, cdsCompany.FieldByName( 'CM_CODIGO' ).AsString );
end;

function TdmCliente.VerificaCodigoKiosco: Boolean;
begin
     Result := ( strLleno( KioskoRegistry.CodigoKiosco ) );
end;

procedure TdmCliente.MuestraFormaError( const Error: Exception );
var
   lSalir: Boolean;
begin
     if KioscoIniciado then
        lSalir := FFormaError.MuestraExcepcion( KioskoRegistry.PasswordSalida, Error )
     else
         lSalir := FFormaProblema.MuestraExcepcion( Error );
     if lSalir then
     begin
          Application.OnMessage := nil;
          Application.Terminate;
     end;
end;

procedure TdmCliente.MuestraErrorcito( const sError: String );
begin
     GeneraBitacora( sError );
     FDlgInformativo.MuestraDialogoInf( sError );
end;

{L�gica para capturar la clave del empleado ( CB_PASSWRD ) se usara en un futuro}
{function TdmCliente.ActualizaClaveEmpleado: Boolean;
const
     K_MENSAJE_NORMAL = 'Captura tu clave de empleado';
     K_MENSAJE_CONFIRMA = 'Confirmar clave anterior';
var
   sProblema, sClave, sClaveAnt, sClaveConf: String;
   lSinClave, lLoop: Boolean;
begin
     Result := False;
     if( StrVacio( cdsEmpleado.FieldbyName('CB_PASSWRD').AsString ) )then
     begin
          lSinClave := True;
          repeat
                if lSinClave then
                begin
                     lLoop := FCambiaClave.PideClaveEmpleado( K_MENSAJE_NORMAL, sClave  );
                     sClaveAnt := sClave;
                end
                else
                begin
                     lLoop := FCambiaClave.PideClaveEmpleadoOtraVez( sProblema, K_MENSAJE_CONFIRMA, sClave );
                     sClaveConf  := sClave;
                end;
                if( StrVacio( sClave ) )then
                    sProblema := ' La clave de empleado no puede quedar vac�a'
                else
                    if not lSinclave then
                    begin
                         if ( sClaveAnt <> sClaveConf )then
                         begin
                              lSinClave := False;
                              sProblema := 'La confirmaci�n no coincide con la clave original';
                         end;
                    end
                    else
                    begin
                         if not lSinClave then
                         begin
                              try
                                 ServerPortalTress.SetClaveEmpleadoKiosko( Empresa, EmpleadoActivo, sClave );
                                 lLoop := False;
                                 Result := True;
                              except on Error: Exception do
                                     begin
                                          lLoop := True;
                                          sProblema := ' Error al actualizar la clave del empleado - Por favor intente m�s tarde';
                                     end;
                              end;
                         end;
                         lSinClave := False;
                    end;
          until not lLoop;
     end;
end;}

function TdmCliente.ValidaGafete( sGafete: String; var sError: String; var lHuboErrores: Boolean ): Boolean;
const
     K_GAFETE_DESCONOCIDO = 'Formato desconocido';
     K_VACIO_PROX = 'Gafete Inv�lido';
     K_VACIO = 'Gafete Vac�o';
     K_MAX_CONSULTAS_DEMO = 10;
var
   sDigito, sCredencial, sDigitosValidos: String;
   iEmpleado: Integer;
begin
     FInterceptaMensajes := False;
     Result := False;
     if( ZetaCommonTools.StrVacio( sGafete ) )then
     begin
          if ( FTipoGafete = egProximidad ) then
             sError := K_VACIO_PROX
          else
              sError := K_VACIO;
     end
     else
     begin
          //CV: 12/Abril/2006
          //CV: Cambio que se efect�o para Electrolux
          with KioskoRegistry do
          begin
               if ( FTipoGafete = egTress ) then
                  sGafete := Trim(DigitoEmpresa) + sGafete + Trim(LetraGafete);
               if ( FTipoGafete = egProximidad ) then
                  sGafete := Trim(DigitoEmpresa) + sGafete + Trim(LetraGafete);

               sDigitosValidos := EmpresasAutorizadas;

               //Si hay un tipo de credencial <> a TRESS (Vacio) veamos al personalizado
               {if TipoGafete <> VACIO then
               begin
                    sGafete := DmCapturaAsistencia.obtenerGafeteTRESS( sGafete, Trim(DigitoEmpresa), Trim(LetraGafete) );
               end
               else
               begin
                  sGafete := Trim(DigitoEmpresa) + sGafete + Trim(LetraGafete);
               end;}
          end;
          try
             sDigito := Copy( sGafete, 1, 1 );
             if StrLleno(sDigitosValidos) and (Pos(sDigito, sDigitosValidos) = 0) then
                sError := 'El Empleado no est� autorizado para utilizar ' + {$ifdef MISDATOS}'MisDatos'{$else}'este Kiosco'{$endif}
             else
             begin
                  iEmpleado := 0;
                  try
                     case FTipoGafete of
                          egProximidad: iEmpleado := StrToInt( Copy( sGafete, 2, Length( sGafete ) ) );
                          egTress: iEmpleado := StrToInt( Copy( sGafete, 2, Length(sGafete) - 2 ) );
                          egBiometrico: iEmpleado := StrToInt( Copy( sGafete, 2, Length(sGafete) - 1 ) );
                     end;
                  except
                        on Error: Exception do
                        begin
                             sError := K_INVALIDO;
                             exit;
                        end;
                  end;
                  case FTipoGafete of
                       egProximidad: sCredencial := VACIO;
                       egTress: sCredencial := Copy( sGafete, Length( sGafete ), 1 );
                       egBiometrico: sCredencial := sGafete;
                  end;

                  Result := PosicionaEmpleado( sDigito, sCredencial, iEmpleado, sError, lHuboErrores );
                  FInterceptaMensajes := Result;
             end;
          except
                on Error: Exception do
                begin
                     sError := K_GAFETE_DESCONOCIDO;
                end;
          end;

          { En modo demo el m�ximo numero de consultas es de 10 }
          if ( Result ) and ( Autorizacion.EsDemo {$ifndef MISDATOS}or ( not Autorizacion.OkModulo( okKiosko ) ){$endif} ) then
          begin
               Result:= ( FCountConsultas < K_MAX_CONSULTAS_DEMO );
               if ( not Result ) then
               begin
                    sError:= 'Se excedi� el # m�ximo de consultas en modo demo';
               end
               else
                   Inc( FCountConsultas );
          end;
     end;
end;

procedure TdmCliente.MuestraPideGafete;
begin
     MuestraPideGafete( VACIO );
end;

{$IFDEF PIDEGAFETE}
procedure TdmCliente.MuestraPideGafete( const sGafeteTeclado: string );
var
   sGafete, sProblema, sClave: String;
   lLoop, lPrimero, lEmpleado, lHuboErrores, lOk: Boolean;
   oCursor: TCursor;
begin
     FInterceptaMensajes := False;
     lPrimero := True;
     FAdministrador := False;
     lEmpleado := False;
     sGafete := sGafeteTeclado;

     repeat
           if StrVacio( sGafete ) then
           begin
                if lPrimero then
                   lLoop := FPideGafete.PideElGafete( sGafete )
                else
                    lLoop := FPideGafete.PideElGafeteOtraVez( sProblema, sGafete );
                lPrimero := False;
           end
           else lLoop := TRUE;
           
           if lLoop then
           begin
                if ( sGafete = KioskoRegistry.GafeteAdministrador ) then
                begin
                     { Se ley� el gafete del Administrador del Kiosko }
                     FAdministrador := True;
                     lLoop := False;
                end
                else
                begin
                     oCursor := Screen.Cursor;
                     Screen.Cursor := crHourglass;
                     try
                        if ValidaGafete( sGafete, sProblema, lHuboErrores ) then
                        begin
                             { Se ley� el gafete de un empleado }
                             lEmpleado := True;
                             lLoop := False;


                        end
                        else
                            sGafete := VACIO;
                        if( lHuboErrores ) then
                            lLoop := False;
                     finally
                            Screen.Cursor := oCursor;
                     end;
                end;
           end;
     until not lLoop;
     if FAdministrador then
     begin
          { Pedir el NIP del Administrador para Salir }
          if SalirDelSistema then
          begin
               Application.OnMessage := nil;
               Application.Terminate;
          end;
     end
     else
     begin
          if lEmpleado then
          begin
               lOk := False;
               sProblema := '';
               if ZetaCommonTools.StrLleno( Self.EmpleadoNIP )then
               begin
                    { Pedir el NIP del Empleado }
                    lOk := PideNIPEmpleado;
               end
               else
               begin
                    if CapturarClaveNueva( Self.EmpleadoNatalicio, sClave ) then
                    begin
                         lOk := Self.EscribeNIPEmpleado( sClave );
                    end;
               end;
               {
                  FDlgInformativo.MuestraDialogoInf( 'Es necesario activar su gafete para poder realizar consultas de informaci�n personal' );
               }
               if lOk then
               begin
                    { Valores Activos }
                    if FValoresActivos.EnviaValoresActivos( BuildValoresActivos, sProblema ) then
                    begin
                         { Llamar Forma de Datos }
                         FormaDatos.ShowPrimerBoton;
                    end
                    else
                    begin
                         FDlgInformativo.MuestraDialogoInf( sProblema );
                    end;
               end;
          end;
     end;
end;
{$ELSE}
procedure TdmCliente.MuestraPideGafete( const sGafeteTeclado: string );
var
   sGafete, sProblema, sClave: String;
   lEmpleado, lHuboErrores, lOk: Boolean;
   oCursor: TCursor;
   oProximidad: TZetaClientDataSet;
   oBiometrico: TZetaClientDataSet;
begin
     FInterceptaMensajes := False;
     FAdministrador := False;
     lEmpleado := False;
     sGafete := sGafeteTeclado;

     if ( sGafete = KioskoRegistry.GafeteAdministrador ) then
     begin
          { Se ley� el gafete del Administrador del Kiosko }
          FAdministrador := True;
     end
     else
     begin
          if ( FTipoGafete = egProximidad ) then
          begin
               oProximidad := TZetaClientDataSet.Create( Self );
               try
                  oProximidad.Data := ServerKiosco.GetProximidadEmpleado( Trim( sGafeteTeclado ) );
                  if( not oProximidad.Eof )then
                  begin
                       with oProximidad do
                            sGafete := FieldByName( 'CM_DIGITO' ).AsString + FieldByName( 'CB_CODIGO' ).AsString;
                  end
                  else
                      sGafete := VACIO;
               finally
                      FreeAndNil( oProximidad );
               end;
          end;

          if ( FTipoGafete = egBiometrico ) then
          begin
                oBiometrico := TZetaClientDataSet.Create( Self );
               try
                  oBiometrico.Data := ServerKiosco.GetBiometricoEmpleado( StrToIntDef( sGafeteTeclado, 0 ) );
                  if( not oBiometrico.Eof )then
                  begin
                       with oBiometrico do
                            sGafete := FieldByName( 'CM_DIGITO' ).AsString + FieldByName( 'CB_CODIGO' ).AsString;
                  end
                  else
                      sGafete := VACIO;
               finally
                      FreeAndNil( oBiometrico );
               end;
          end;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if ValidaGafete( sGafete, sProblema, lHuboErrores ) then
             begin
                  { Se ley� el gafete de un empleado }
                  lEmpleado := True;
                  EscribeBitacora( clbLogin );
                  cdsAccesoEmp.Refrescar;
             end
             else
                 FDlgInformativo.MuestraDialogoInf( sProblema );

          finally
                 Screen.Cursor := oCursor;
          end;
     end;
     if FAdministrador then
     begin
          { Pedir el NIP del Administrador para Salir }
          if (NOT FPideNip) OR SalirDelSistema then
          begin
               Application.OnMessage := nil;
               FFormaInfo.DetieneLector;
               Application.Terminate;
          end;
     end
     else
     begin
          if lEmpleado then
          begin
               lOk := False;
               sProblema := '';
               if FPideNip then
               begin
                    if ZetaCommonTools.StrLleno( Self.EmpleadoNIP ) then
                    begin
                         { Pedir el NIP del Empleado }
                         lOk := PideNIPEmpleado;
                    end
                    else
                    begin
                         if CapturarClaveNueva( Self.EmpleadoNatalicio, sClave ) then
                         begin
                              lOk := Self.EscribeNIPEmpleado( sClave );
                         end;
                    end;
               end
               else
                   lOk := TRUE;

               if lOk then
               begin
                    { Valores Activos }
                    if FValoresActivos.EnviaValoresActivos( BuildValoresActivos, sProblema ) then
                    begin
                         //TODO : Mostrar Encuesta


                         { Llamar Forma de Datos }
                         with FormaDatos do
                         begin
                              if KioskoRegistry.MostrarEncuestas then
                                 ShowEncuesta
                              else
                                  ShowPrimerBoton;
                              { No es necesario ya lo hace el disable controls
                              CleanPantallaEmpAnterior; }
                         end;
                    end
                    else
                    begin
                         FDlgInformativo.MuestraDialogoInf( sProblema );
                    end;
                    EscribeBitacora( clbLogOut );
               end;
          end;
     end;
end;
{$ENDIF}

function TdmCliente.EscribeNIPEmpleado( const sClave: String ): Boolean;
var
   oCursor: TCursor;
begin
     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           ServerKiosco.SetClaveEmpleadoKiosco( Empresa, EmpleadoActivo, sClave );
           Self.FEmpleadoNIP := sClave;
           Result := True;
        except
              on Error: Exception do
              begin
                   Error.Message := ' Error al actualizar la clave del empleado: ' + Error.Message;
                   raise;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


function TdmCliente.PideNIPEmpleado( FParent: TWinControl ): Boolean;
var
   sClaveNueva: String;
begin
     case PideClaveNIP( KioskoRegistry.Intentos, KioskoRegistry.MaxDigitos, EmpleadoNIP ) of
          pnrOK: Result := True;
          pnrChangePassword:
          begin
               Result := False;
               if CambiaClaveEmpleado( sClaveNueva, KioskoRegistry.MaxDigitos  ) then
               begin
                    Result := EscribeNIPEmpleado( sClaveNueva );
                    if Result then
                       FDlgInformativo.MuestraDialogoInf( 'Su clave ha sido cambiada' );
               end;
          end;
     else
         Result := False;
     end;
end;

function TdmCliente.CambiaNIPEmpleado(FParent: TWinControl = NIL): Boolean;

begin
     Result := FCambiaNip.CambiaNIPEmpleado( KioskoRegistry.MaxDigitos, FParent );
end;


{ ****** cdsKioscos ***********}

procedure TdmCliente.cdsKioscosAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsKioscos.Data := ServerKiosco.GetKioscos;
end;

function TdmCliente.GetCodigoKioscoID: string;
begin
     {$ifdef Antes}
     with KioskoRegistry do
          Result := FID + '_' + CodigoKiosco;
     {$else}
     Result := GetComputerName;
     {$endif}
end;

function TdmCliente.GetURLKiosco( const sURL : string; const eAccion: eAccionBoton = abContenido ): string;

begin
     Result := sURL;
     case eAccion of
          abMisdatos:
          begin
               Result := StrTransAll( ZetaCommonTools.VerificaDirURL(GlobalComparte.GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ )) +
                         GlobalComparte.GetGlobalString( K_KIOSCO_PAGINA_PANTALLA ), K_ARROBA_PANTALLA, Result );
          end;
     end;

     Result := StrTransAll( Result, K_ARROBA_KIOSCO, GetCodigoKioscoID );
     Result := StrTransAll( Result, K_ARROBA_SERVIDOR, GlobalComparte.GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ ) );

end;

procedure TdmCliente.SetDatosPeriodoActivo( iNumero: integer; eTipo: eTipoPeriodo; iYear: integer );
 var
    Datos: Olevariant;
begin
     with cdsPeriodoActivo do
     begin
          if Servidor.GetPeriodo(Empresa, iYear, Ord( eTipo ), iNumero, Datos ) then
          begin
               Data := Datos;

               with Parametros do
               begin
                    AddInteger( 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
                    AddInteger( 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
                    AddInteger( 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
               end;

          end
          else
          begin
               Raise Exception.Create('Error al cargar el periodo solicitado');
          end;
     end;
end;

procedure TdmCliente.EscribeBitacora( const eClase: eClaseBitacora; const eTipoBitKiosco: eTipoBitacora; const oBoton: TBotonKiosco );
const
     K_CIERRA = '>';
     K_ABRE = '<';
var
   sTexto, sMarco: String;
begin
     with oBoton do
     begin
          sTexto:= ObtieneElemento( lfClaseBitacora, Ord( eClase ) );
          if ( oBoton <> Nil ) then
               sMarco:= Marco;
          case eClase of
               clbLogin, clbLogOut, clbCambioNIP:
               begin
                    sTexto:= K_ABRE + sTexto + K_CIERRA;
               end;
               clbMisDatos:
               begin
                    sTexto:= sTexto + K_GUION + Letrero ;
               end;
               clbContenido:
               begin
                    sTexto:= sTexto + K_GUION + Letrero + K_GUION + URL ;
               end;
               clbReporte:
               begin
                    case eTipoBitKiosco of
                         tbNormal: sTexto:= sTexto + K_GUION + IntToStr( Reporte ) + K_GUION + FUltimoReporte ;
                         tbError: sTexto:= Format( K_ERROR_REPORTE, [Reporte] );
                    end;
               end;
               clbErrorBrowser:
               begin
                    if oBoton <> Nil then
                       sTexto:=  Letrero + K_GUION;
                    sTexto:= 'Error al consultar los datos del empleado ' + CR_LF + sTexto + ' Se reinici� el Navegador.  ';
               end;
          end;
     end;
     EscribeBitacoraInfo( eClase, eTipoBitKiosco, sMarco, sTexto );
end;

procedure TdmCliente.EscribeBitacoraInfo( const eClase: eClaseBitacora; const eTipoBitKiosco: eTipoBitacora; const sMarco, sTexto: String );
begin
     with ParamsBitacora do
     begin
          AddInteger( 'Clase', Ord( eClase ) );
          AddInteger( 'Tipo', Ord( eTipoBitKiosco ) );
          AddString( 'Empresa', cdsCompany.FieldByName( 'CM_CODIGO' ).AsString );
          AddString( 'Kiosco', ZetaWinAPITools.GetComputerName ); //Kiosco local
          AddInteger( 'Empleado', Empleado );
          AddString( 'Marco', sMarco );
          AddString( 'Texto', sTexto );
     end;
     ServerKiosco.GrabaBitacora( ParamsBitacora.VarValues );
end;

procedure TdmCliente.BitacoraImprimirReporte( const iReporte: Integer; const sMarco: String; const lImprimeOK: Boolean );
var
   eTipoBit: eTipoBitacora;
   sTexto: String;
begin
     sTexto:= ObtieneElemento( lfClaseBitacora, Ord( clbReporte ) );
     if lImprimeOK then
     begin
          eTipoBit:= tbNormal;
          sTexto := sTexto + K_GUION + IntToStr( iReporte ) + K_GUION + FUltimoReporte;
     end
     else
     begin
          eTipoBit:= tbError;
          sTexto:= Format( K_ERROR_REPORTE, [iReporte] );
     end;
     dmCliente.EscribeBitacoraInfo( clbReporte, eTipoBit, sMarco, sTexto );
end;

procedure TdmCliente.cdsAccesoEmpAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsAccesoEmp.Data:= ServerKiosco.GetAccesoEmp( Empresa, Empleado );
     {$ifdef MISDATOS}
     cdsAccesoEmp.EmptyDataset;  // Se utiliza el mecanismo de Restricci�n botones para evitar que se agregue funcionalidad no autorizada
     {$endif}
end;

procedure TdmCliente.GetUsuariosSuscritos( const sEmpresa,sConfiden: String; const lPorConfiden: Boolean; var oUsuarios: OleVariant );
begin
     cdsSuscripUsuarios.Data:= ServerKiosco.GetUsuariosSuscritos( Empresa[P_CODIGO],
                                                     EmpleadoConfiden,
                                                     zStrToBool( cdsCompany.FieldByName('CM_KCONFI').AsString ),
                                                     oUsuarios );
end;

procedure TdmCliente.InitTempDataset( DataSet : TClientDataset );
begin
     with Dataset do
     begin
          Active := False;
          Filtered := FALSE;
          Filter := '';
          IndexFieldNames := '';
          while FieldCount > 0 do
          begin
               Fields[ FieldCount - 1 ].DataSet := nil;
          end;
          FieldDefs.Clear;
     end;
end;

procedure TdmCliente.IniciarcdsSesionImpReportes;
begin
     InitTempDataSet(cdsSesionImpReportes);
     with cdsSesionImpReportes do
     begin
          AddStringField('EMPRESA', 100);
          AddIntegerField('CB_CODIGO');
          AddIntegerField('RE_CODIGO');
          AddStringField('PARAMS', 100);
          CreateTempDataset;
     end;
end;

procedure TdmCliente.IniciarcdsKioscoImpresiones;
begin
     InitTempDataSet(cdsKioscoImpresiones);
     with cdsKioscoImpresiones do
     begin
          AddStringField('CM_CODIGO', 10);
          AddIntegerField('CB_CODIGO');
          AddIntegerField('RE_CODIGO');
          AddIntegerField('PE_TIPO');
          AddIntegerField('PE_YEAR');
          AddIntegerField('PE_NUMERO');
          AddDateField('KI_FECHA');
          AddStringField('KI_KIOSCO',100);
          AddStringField('KI_HORA', 8);
          CreateTempDataset;
     end;
end;

procedure TdmCliente.cdsKioscoImpresionesAlEnviarDatos(Sender: TObject);
var
   iErrorCount: Integer;
begin
     inherited;
     with cdsKioscoImpresiones do
     begin
          PostData;
          Reconcile(ServerKiosco.GrabaTabla(Tag,Delta,iErrorCount));
     end;
end;

procedure TdmCliente.cdsKioscoImpresionesReconcileError(DataSet: TCustomClientDataSet;
          E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     inherited;
     Action := raCancel;
end;

//BIOMETRICO
function TdmCliente.TransformaTipoGafete( const iTipoGafeteRegistry : Integer ) : eTipoGafete;
begin
     Result := egTress;
     if ( iTipoGafeteRegistry = 1 ) then Result := egProximidad;
     if ( iTipoGafeteRegistry = 2 ) then Result := egBiometrico;
end;

// BIOMETRICO
function TdmCliente.EsBiometrico: Boolean;
var
   lBiometrico : Boolean;
begin
     lBiometrico := ( FTipoGafete = egBiometrico );
     Result := ( lBiometrico );
end;

// BIOMETRICO
function TdmCliente.FileTemplates: String;
begin
     Result := KioskoRegistry.RutaHuellas;
end;

// BIOMETRICO
procedure TdmCliente.ReiniciaHuellas;
var
   oParametros: TZetaParams;
begin
     try
        oParametros:= TZetaParams.Create;
        oParametros.AddString('ComputerName', ZetaWinAPITools.GetComputerName );
        ServerKiosco.ReportaHuella( oParametros.VarValues );
     finally
            FreeAndNil(oParametros);
     end;
end;

// BIOMETRICO
procedure TdmCliente.cdsHuellasAlAdquirirDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   oParametros: TZetaParams;
{$endif}
begin
     inherited;
{$ifndef DOS_CAPAS}
     try
        oParametros:= TZetaParams.Create;
        with oParametros do
        begin
             {$ifdef ANTES}
             AddString('ComputerName', ZetaWinAPITools.GetComputerName );
             {$else}
             AddString('ComputerName', ZetaWinAPITools.GetComputerName );
             AddString('KioscoId', KioskoRegistry.IdKiosco );
                 {$ifdef MISDATOS}
                 AddInteger('TipoKiosco', Ord( dpMisDatos ) );  //Enumerado que se agrego en ZetaCommonLists
                 {$else}
                 AddInteger('TipoKiosco', Ord( dpKiosco ) );
                 {$endif}
             {$endif}
             cdsHuellas.Data := ServerKiosco.ObtieneTemplates( oParametros.VarValues );
        end;
     finally
            FreeAndNil(oParametros);
     end;
{$endif}
end;

function TdmCliente.GetBioMessage(error: Integer): String;
begin
    case error of
        0: Result := 'Exito!';
        -1: Result := 'Error de imagen de huella!';
        -2: Result := 'Huella inv�lida!';
        -3: Result := 'Error de n�mero biom�trico!';
        -4: Result := 'Error de archivo de dispositivo!';
        -6: Result := 'Error de almacenamiento de dispositivo!';
        -7: Result := 'Error de sensor de dispositivo!';
        -8: Result := 'Orden de enrolamiento incorrecto!';
        -9: Result := 'No puede analizar las tres capturas, intente de nuevo!';
        -11: Result := 'La imagen no es un dedo!';
        -100: Result := 'No hay dispositivo biom�trico!';
        -101: Result := 'No puede abrir archivo de dispositivo local!';
        -102: Result := 'Ya ha enrolado esa huella!';
        -103: Result := 'Error de identificaci�n de huella!';
        -104: Result := 'Error de verificaci�n de huella!';
        -105: Result := 'No puede abrir la imagen de huella!';
        -106: Result := 'No puede crear imagen de huella!';
        -107: Result := 'No puede abrir huella!';
        -108: Result := 'No puede crear huella!';
        else Result := 'Error desconocido! (Numero =' + IntToStr(error) + ')';
    end;
end;

// BIOMETRICO
procedure TdmCliente.ReportaHuella( sLista : String );
{$ifndef DOS_CAPAS}
var
   oParametros: TZetaParams;
{$endif}
begin
     inherited;
{$ifndef DOS_CAPAS}
     try
        oParametros:= TZetaParams.Create;
        with oParametros do
        begin
             AddString('ComputerName', ZetaWinAPITools.GetComputerName );
             AddString('HuellaId', sLista );
             ServerKiosco.ReportaHuella( oParametros.VarValues );
        end;
     finally
            FreeAndNil(oParametros);
     end;
{$endif}
end;

function TdmCliente.GafeteAdmin: String;
begin
     Result := KioskoRegistry.GafeteAdministrador;
end;

procedure TdmCliente.SetSiguienteHora;
var
   iHoras, iMinutos, iTotal : Integer;
   dSiguiente : TDateTime;
begin
     iHoras := GetHoras( KioskoRegistry.HorasSincronizacion );
     iMinutos := GetMinutos( KioskoRegistry.HorasSincronizacion );
     iTotal := iHoras + iMinutos;
     if ( iTotal > 0 ) then
     begin
     dSiguiente := Now;
     dSiguiente := IncHour( dSiguiente, iHoras );
     dSiguiente := IncMinute( dSiguiente, iMinutos );
     end
     else
     begin
          dSiguiente := Now;
          dSiguiente := IncHour( dSiguiente, 1 );
          dSiguiente := IncMinute( dSiguiente, 0 );
     end;

     FSiguienteHora := FormatDateTime( 'hhmm', dSiguiente );
end;

procedure TdmCliente.ValidacionSincronizacion;
begin
     if ( FormatDateTime( 'hhmm', ( now ) ) >= FSiguienteHora ) then
     begin
          SetSiguienteHora;
          FSync := True;  
     end;
end;

function TdmCliente.GetHoras( sHoras : String ) : Integer;
var
   resultado : integer;
begin
     if ( Length( sHoras ) < 2 ) then
          resultado := -1
     else
     begin
          resultado := StrToIntDef( Copy( sHoras, 1, 2 ), 0 );
          if ( resultado < 0 ) or ( resultado > 24 ) then
               resultado := -1;
     end;
     
     result := resultado;
end;

function TdmCliente.GetMinutos( sHoras : String ) : Integer;
var
   resultado : integer;
begin
     if ( Length( sHoras ) < 5 ) then
          resultado := -1
     else
     begin
          resultado := StrToIntDef( Copy( sHoras, 4, 2 ), 0 );
          if ( resultado < 0 ) or ( resultado >= 60 ) then
               resultado := -1;
     end;
     
     result := resultado;
end;

end.
