unit FCapturaClaveNueva;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, StdCtrls, 
     fcClearPanel, fcButtonGroup, fcLabel, fcButton, fcImgBtn, fcShapeBtn, fcpanel,
     ZetaCommonClasses,
     DCliente;

type
  TCapturaClaveNueva = class(TForm)
    panelTeclado: TfcPanel;
    panelTexto: TfcPanel;
    panelBotones: TfcPanel;
    fcOK: TfcShapeBtn;
    fcCancelar: TfcShapeBtn;
    fcLabel1: TfcLabel;
    fcLBLDia: TfcLabel;
    fcLBLMes: TfcLabel;
    fcLBLYear: TfcLabel;
    fcMeses: TfcButtonGroup;
    fcMes1: TfcShapeBtn;
    fcMes2: TfcShapeBtn;
    fcMes3: TfcShapeBtn;
    fcMes4: TfcShapeBtn;
    fcMes5: TfcShapeBtn;
    fcMes6: TfcShapeBtn;
    fcMes7: TfcShapeBtn;
    fcMes8: TfcShapeBtn;
    fcMes9: TfcShapeBtn;
    fcMes10: TfcShapeBtn;
    fcMes11: TfcShapeBtn;
    fcMes12: TfcShapeBtn;
    FechaAsText: TLabel;
    edDia: TEdit;
    edYear: TEdit;
    TimerSalida: TTimer;
    btnDiaUP: TfcShapeBtn;
    btnDiaDOWN: TfcShapeBtn;
    btnYearUP: TfcShapeBtn;
    btnYearDOWN: TfcShapeBtn;
    procedure FormShow(Sender: TObject);
    procedure fcMes1Click(Sender: TObject);
    procedure fcMes2Click(Sender: TObject);
    procedure fcMes3Click(Sender: TObject);
    procedure fcMes4Click(Sender: TObject);
    procedure fcMes5Click(Sender: TObject);
    procedure fcMes6Click(Sender: TObject);
    procedure fcMes7Click(Sender: TObject);
    procedure fcMes8Click(Sender: TObject);
    procedure fcMes9Click(Sender: TObject);
    procedure fcMes10Click(Sender: TObject);
    procedure fcMes11Click(Sender: TObject);
    procedure fcMes12Click(Sender: TObject);
    procedure fcOKClick(Sender: TObject);
    procedure TimerSalidaTimer(Sender: TObject);
    procedure btnDiaUPClick(Sender: TObject);
    procedure btnDiaDOWNClick(Sender: TObject);
    procedure btnYearUPClick(Sender: TObject);
    procedure btnYearDOWNClick(Sender: TObject);
  private
    { Private declarations }
    FNatalicio: TDate;
    FDay: Integer;
    FMonth: Integer;
    FYear: Integer;
    FMensaje: String;
    FProblema: String;
    function CheckDateValue: Boolean;
    function GetDateValue: TDate;
    procedure ResetTimerSalida;
    procedure ShowDateValue;
    procedure SetControles;
    procedure SetProblema(const Value: String);
    procedure SetDay(const Value: Integer);
    procedure SetMonth(const Value: Integer);
    procedure SetYear(const Value: Integer);
  protected
    { Protected declarations }
    property Problema: String read FProblema write SetProblema;
  public
    { Public declarations }
    property Day: Integer read FDay write SetDay;
    property Month: Integer read FMonth write SetMonth;
    property Year: Integer read FYear write SetYear;
    property Mensaje: String read FMensaje write FMensaje;
    property Natalicio: TDate read FNatalicio write FNatalicio;
  end;

function CapturarClaveNueva( const dNatalicio: TDateTime; var sClave: String ): Boolean;

implementation

uses FCambiaClave,
     FKioscoRegistry,
     ZetaCommonLists,
     ZetaCommonTools;

var
  CapturaClaveNueva: TCapturaClaveNueva;
const
     K_DAY_MAX = 31;
     K_DAY_MIN = 1;
     K_YEAR_MAX = 2050;
     K_YEAR_MIN = 1920;

{$R *.dfm}

function CapturarClaveNueva( const dNatalicio: TDateTime; var sClave: String ): Boolean;
begin
     Result := False;
     if not Assigned( CapturaClaveNueva ) then
        CapturaClaveNueva := TCapturaClaveNueva.Create( Application );
     with CapturaClaveNueva do
     begin
          Mensaje := 'Su clave de acceso no ha sido especificada';
          Natalicio := dNatalicio;
          ShowModal;
          if ( ModalResult = mrOk ) then
          begin
               with KioskoRegistry do
                    Result := FCambiaClave.CambiaClaveEmpleado( sClave, MaxDigitos );
          end;
     end;
end;

{ ********** TCapturaClaveNueva ********* }

procedure TCapturaClaveNueva.FormShow(Sender: TObject);
begin
     FYear := ZetaCommonTools.TheYear( Date ) - 25;
     FMonth := 0;
     FDay := 15;
     fcMes1.Down := False;
     fcMes2.Down := False;
     fcMes3.Down := False;
     fcMes4.Down := False;
     fcMes5.Down := False;
     fcMes6.Down := False;
     fcMes7.Down := False;
     fcMes8.Down := False;
     fcMes9.Down := False;
     fcMes10.Down := False;
     fcMes11.Down := False;
     fcMes12.Down := False;
     SetControles;
end;

procedure TCapturaClaveNueva.ShowDateValue;
begin
     with FechaAsText do
     begin
          if CheckDateValue then
             Caption := Format( '%d de %s de %d', [ Self.Day, ZetaCommonLists.ObtieneElemento( lfMeses, Self.Month - 1 ), Self.Year ] )
          else
              Caption := '';
     end;
end;

function TCapturaClaveNueva.CheckDateValue: Boolean;
begin
     if ( Self.Month > 0 ) then
     begin
          Result := ZetaCommonTools.DayIsValid( Self.Year, Self.Month, Self.Day )
     end
     else
         Result := False;
end;

function TCapturaClaveNueva.GetDateValue: TDate;
begin
     if CheckDateValue then
        Result := EncodeDate( Self.Year, Self.Month, Self.Day )
     else
         Result := NullDateTime;
end;

procedure TCapturaClaveNueva.SetDay(const Value: Integer);
begin
     if ( FDay <> Value ) then
     begin
          FDay := Value;
          Problema := '';
          SetControles;
     end;
end;

procedure TCapturaClaveNueva.SetMonth(const Value: Integer);
begin
     if ( FMonth <> Value ) then
     begin
          FMonth := Value;
          Problema := '';
          SetControles;
     end;
end;

procedure TCapturaClaveNueva.SetYear(const Value: Integer);
begin
     if ( FYear <> Value ) then
     begin
          FYear := Value;
          Problema := '';
          SetControles;
     end;
end;

procedure TCapturaClaveNueva.SetControles;
begin
     with PanelTexto do
     begin
          if ZetaCommonTools.StrLleno( Problema ) then
          begin
               Font.Color := clRed;
               Caption := Self.Problema;
          end
          else
          begin
               Font.Color := clBlack;
               Caption := Mensaje;
          end;
     end;
     edDia.Text := Format( '%2.d', [ Self.Day ] );
     edYear.Text := Format( '%2.d', [ Self.Year ] );
     btnDiaUP.Enabled := ( Self.Day < K_DAY_MAX );
     btnDiaDOWN.Enabled := (Self.Day > K_DAY_MIN );
     btnYearUP.Enabled := ( Self.Year < K_YEAR_MAX );
     btnYearDOWN.Enabled := (Self.Year > K_YEAR_MIN );
     ShowDateValue;
     ResetTimerSalida;
     Application.ProcessMessages;
end;

procedure TCapturaClaveNueva.SetProblema(const Value: String);
begin
     if ( FProblema <> Value ) then
     begin
          FProblema := Value;
     end;
end;

procedure TCapturaClaveNueva.ResetTimerSalida;
begin
     with TimerSalida do
     begin
          Enabled := False;
          Interval := dmCliente.TimeoutDatos;
          Enabled := True;
     end;
end;

procedure TCapturaClaveNueva.TimerSalidaTimer(Sender: TObject);
begin
     inherited;
     ModalResult := mrCancel;
end;

{ ********* Eventos ********* }

procedure TCapturaClaveNueva.fcMes1Click(Sender: TObject);
begin
     Self.Month := 1;
end;

procedure TCapturaClaveNueva.fcMes2Click(Sender: TObject);
begin
     Self.Month := 2;
end;

procedure TCapturaClaveNueva.fcMes3Click(Sender: TObject);
begin
     Self.Month := 3;
end;

procedure TCapturaClaveNueva.fcMes4Click(Sender: TObject);
begin
     Self.Month := 4;
end;

procedure TCapturaClaveNueva.fcMes5Click(Sender: TObject);
begin
     Self.Month := 5;
end;

procedure TCapturaClaveNueva.fcMes6Click(Sender: TObject);
begin
     Self.Month := 6;
end;

procedure TCapturaClaveNueva.fcMes7Click(Sender: TObject);
begin
     Self.Month := 7;
end;

procedure TCapturaClaveNueva.fcMes8Click(Sender: TObject);
begin
     Self.Month := 8;
end;

procedure TCapturaClaveNueva.fcMes9Click(Sender: TObject);
begin
     Self.Month := 9;
end;

procedure TCapturaClaveNueva.fcMes10Click(Sender: TObject);
begin
     Self.Month := 10;
end;

procedure TCapturaClaveNueva.fcMes11Click(Sender: TObject);
begin
     Self.Month := 11;
end;

procedure TCapturaClaveNueva.fcMes12Click(Sender: TObject);
begin
     Self.Month := 12;
end;

procedure TCapturaClaveNueva.btnDiaUPClick(Sender: TObject);
begin
     if ( Self.Day < K_DAY_MAX ) then
     begin
          Self.Day := Self.Day + 1;
     end;
end;

procedure TCapturaClaveNueva.btnDiaDOWNClick(Sender: TObject);
begin
     if ( Self.Day > K_DAY_MIN ) then
     begin
          Self.Day := Self.Day - 1;
     end;
end;

procedure TCapturaClaveNueva.btnYearUPClick(Sender: TObject);
begin
     if ( Self.Year < K_YEAR_MAX ) then
     begin
          Self.Year := Self.Year + 1;
     end;
end;

procedure TCapturaClaveNueva.btnYearDOWNClick(Sender: TObject);
begin
     if ( Self.Year > K_YEAR_MIN ) then
     begin
          Self.Year := Self.Year - 1;
     end;
end;

procedure TCapturaClaveNueva.fcOKClick(Sender: TObject);
begin
     if CheckDateValue then
     begin
          if ( Self.Natalicio <> Self.GetDateValue ) then
          begin
               Problema := 'Fecha Incorrecta';
               SetControles;
          end
          else
          begin
               ModalResult := mrOk;
          end;
     end
     else
     begin
          Problema := 'Fecha Inv�lida';
          SetControles;
    end;
end;

end.
