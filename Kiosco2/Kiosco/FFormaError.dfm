inherited FormaError: TFormaError
  Left = 544
  Top = 101
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'FormaError'
  ClientHeight = 391
  ClientWidth = 217
  OldCreateOrder = True
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited fcImagenFondo: TfcImager
    Width = 217
    Height = 279
  end
  inherited PanelTeclado: TfcPanel
    Width = 217
    Height = 279
    inherited fcNumero2: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero3: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero5: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero8: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero9: TfcShapeBtn
      NumGlyphs = 1
    end
    inherited fcNumero0: TfcShapeBtn
      NumGlyphs = 1
    end
  end
  inherited fcPanel1: TfcPanel
    Width = 217
    inherited PanelMensaje: TfcShapeBtn
      NumGlyphs = 0
    end
    inherited PanelClave: TfcShapeBtn
      NumGlyphs = 1
    end
  end
end
