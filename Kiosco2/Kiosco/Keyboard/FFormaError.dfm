inherited FormaError: TFormaError
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'FormaError'
  ClientHeight = 752
  ClientWidth = 853
  OldCreateOrder = True
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited panelTeclado: TfcPanel
    Left = 96
    Top = 32
    Height = 705
    Align = alNone
    BevelInner = bvNone
    inherited panelTexto: TfcPanel
      Left = 2
      Top = 230
      Width = 672
      Align = alBottom
    end
    inherited panelBotones: TfcPanel
      Left = 2
      Top = 299
      Width = 672
      Height = 404
      Align = alBottom
      inherited fcOK: TfcShapeBtn
        Caption = 'Salir'
        Default = False
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
          F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
          000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
          338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
          45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
          3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
          F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
          000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
          338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
          4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
          8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
          333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
          0000}
        Kind = bkCustom
        ModalResult = 0
        OnClick = fcOKClick
      end
      inherited fcCancelar: TfcShapeBtn
        Caption = 'Reintentar'
        Color = 8388863
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333333333333333333333333333333333333333333333FF333333333333
          3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
          E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
          E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
          E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
          000033333373FF77777733333330003333333333333777333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        Kind = bkCustom
        ModalResult = 4
      end
      inherited fcLetraZ: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraX: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraC: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraV: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraB: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraN: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraM: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraL: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraK: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraJ: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraH: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraG: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraF: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraD: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraS: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraA: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraQ: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraW: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraE: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraR: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraT: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraY: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraU: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraI: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetra0: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcLetraP: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumber0: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber9: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber8: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber7: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumber6: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumber5: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber4: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumber3: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber2: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber1: TfcShapeBtn
        NumGlyphs = 0
      end
    end
    object gbError: TfcGroupBox
      Left = 2
      Top = 2
      Width = 672
      Height = 174
      Align = alClient
      Caption = '� Se encontr� un error !'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object memoError: TMemo
        Left = 2
        Top = 31
        Width = 668
        Height = 141
        Align = alClient
        BorderStyle = bsNone
        Color = 8421440
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object panelInfo: TfcPanel
      Left = 2
      Top = 176
      Width = 672
      Height = 54
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Capture la clave de Salida'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 3
    end
  end
end
