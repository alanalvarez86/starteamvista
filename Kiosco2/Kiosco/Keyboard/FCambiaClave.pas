unit FCambiaClave;

interface

uses Windows, Messages, SysUtils, {$Ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, fcButton, fcImgBtn, fcShapeBtn, fcpanel,
     FKeyboardBase, StdCtrls, OleCtrls, SHDocVw;

type
  TCambiaClave = class(TKeyboardBase)
    fcPanelInfo: TfcPanel;
    TimerSalida: TTimer;
    procedure FormShow(Sender: TObject);
    procedure TimerSalidaTimer(Sender: TObject);
    procedure fcOKClick(Sender: TObject);
  private
    { Private declarations }
    FProblema: String;
    FMensaje: String;
    FPrimeraCaptura: Boolean;
    FPrimeraClave: String;
    procedure ResetTimerSalida;
    procedure SetControles;
  protected
    { Protected declarations }
    property PrimeraCaptura: Boolean read FPrimeraCaptura write FPrimeraCaptura;
    property PrimeraClave: String read FPrimeraClave write FPrimeraClave;
    property Problema: String read FProblema write FProblema;
    procedure AlCapturar; override;
    procedure AlSalir; override;
    procedure InicializaControles; override;
  public
    { Public declarations }
    property Mensaje: String read FMensaje write FMensaje;
  end;

function CambiaClaveEmpleado( var sClave: String ): Boolean;

implementation

uses DCliente,
     ZetaCommonTools;

var
   CambiaClave: TCambiaClave;

{$R *.dfm}

function CambiaClaveEmpleado( var sClave: String ): Boolean;
begin
     Result := False;
     if not Assigned( CambiaClave ) then
        CambiaClave := TCambiaClave.Create( Application );
     with CambiaClave do
     begin
          Mensaje := 'Por favor capture su nueva clave';
          Mascara := '*';
          ShowModal;
          if IsOk then
          begin
               Result := True;
               sClave := Valor;
          end;
     end;
end;

{****** TPideGafete ***********}

procedure TCambiaClave.FormShow(Sender: TObject);
begin
     inherited;
     InicializaControles;
     Clear;
     SetControles;
     FProblema := '';
     FPrimeraCaptura := True;
     FPrimeraClave := '';
end;

procedure TCambiaClave.SetControles;
begin
     with fcPanelInfo do
     begin
          if ZetaCommonTools.StrLleno( Problema ) then
          begin
               Font.Color := clRed;
               Caption := Self.Problema;
          end
          else
          begin
               Font.Color := clBlack;
               Caption := Mensaje;
          end;
     end;
     ResetTimerSalida;
     Application.ProcessMessages;
end;

procedure TCambiaClave.ResetTimerSalida;
begin
     with TimerSalida do
     begin
          Enabled := False;
          Interval := dmCliente.TimeoutDatos;
          Enabled := True;
     end;
end;

procedure TCambiaClave.TimerSalidaTimer(Sender: TObject);
begin
     inherited;
     ModalResult := mrCancel;
end;

procedure TCambiaClave.InicializaControles;
begin
     with edTexto do
     begin
          Visible:= True;
          ActiveControl := edTexto;
          Width := 0;
          Height := 0;
     end;
end;

procedure TCambiaClave.AlCapturar;
begin
     inherited AlCapturar;
     ResetTimerSalida;
     Problema := '';
     SetControles;
end;

procedure TCambiaClave.AlSalir;
begin
     inherited AlSalir;
     dmCliente.InterceptaMensajes := True;
end;

procedure TCambiaClave.fcOKClick(Sender: TObject);
begin
     inherited;
     if PrimeraCaptura then
     begin
          Mensaje := 'Por favor confirme la clave capturada';
          PrimeraCaptura := False;
          PrimeraClave := Valor;
          Clear;
          SetControles;
     end
     else
     begin
          if ( Self.Valor = Self.PrimeraClave ) then
          begin
               ModalResult := mrOk;
          end
          else
          begin
               Clear;
               Problema := 'Clave diferente. Por favor intente de nuevo';
               SetControles;
          end;
     end;
end;

end.
