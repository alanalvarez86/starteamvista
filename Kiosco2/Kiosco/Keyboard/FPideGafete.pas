unit FPideGafete;

interface

uses Windows, Messages, SysUtils, {$Ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, fcButton, fcImgBtn, fcShapeBtn, fcpanel,
     FKeyboardBase, StdCtrls, OleCtrls, SHDocVw;

type
  TPideGafete = class(TKeyboardBase)
    fcPanelInfo: TfcPanel;
    TimerSalida: TTimer;
    procedure FormShow(Sender: TObject);
    procedure TimerSalidaTimer(Sender: TObject);
  private
    { Private declarations }
    FProblema: String;
    procedure ResetTimerSalida;
    procedure SetControles;
  protected
    { Protected declarations }
    procedure AlCapturar; override;
    procedure AlSalir; override;
  public
    { Public declarations }
    property Problema: String read FProblema write FProblema;
  end;

function PideElGafeteOtraVez( const sProblema: String; var sGafete: String ): Boolean;
function PideElGafete( var sGafete: String ): Boolean;

implementation

uses DCliente,
     ZetaCommonTools;

var
  PideGafete: TPideGafete;

{$R *.dfm}

function PideElGafeteOtraVez( const sProblema: String; var sGafete: String ): Boolean;
begin
     Result := False;
     if not Assigned( PideGafete )then
        PideGafete := TPideGafete.Create( Application );
     with PideGafete do
     begin
          Clear;
          Problema := sProblema;
          ShowModal;
          if IsOk then
          begin
               Result := True;
               sGafete := Valor;
          end;
     end;
end;

function PideElGafete( var sGafete: String ): Boolean;
begin
     Result := PideElGafeteOtraVez( '', sGafete );
end;

{****** TPideGafete ***********}

procedure TPideGafete.FormShow(Sender: TObject);
begin
     inherited;
     SetControles;
end;

procedure TPideGafete.SetControles;
const
     K_MENSAJE_NORMAL = 'Pasa tu gafete o captura tu n�mero de empleado';
begin
     with fcPanelInfo do
     begin
          if ZetaCommonTools.StrLleno( Problema ) then
          begin
               Font.Color := clRed;
               Caption := Self.Problema;
          end
          else
          begin
               Font.Color := clBlack;
               Caption := K_MENSAJE_NORMAL;
          end;
     end;
     ResetTimerSalida;
end;

procedure TPideGafete.ResetTimerSalida;
begin
     with TimerSalida do
     begin
          Enabled := False;
          Interval := dmCliente.TimeoutDatos;
          Enabled := True;
     end;
end;

procedure TPideGafete.TimerSalidaTimer(Sender: TObject);
begin
     inherited;
     ModalResult := mrCancel;
end;

procedure TPideGafete.AlCapturar;
begin
     inherited AlCapturar;
     ResetTimerSalida;
     Problema := '';
     SetControles;
end;

procedure TPideGafete.AlSalir;
begin
     dmCliente.InterceptaMensajes := True;
end;

end.
