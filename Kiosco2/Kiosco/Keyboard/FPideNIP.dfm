inherited PideNIP: TPideNIP
  Caption = 'PideNIP'
  ClientHeight = 694
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited panelTeclado: TfcPanel
    Height = 694
    inherited panelTexto: TfcPanel
      Top = 174
    end
    inherited panelBotones: TfcPanel
      Top = 243
      Height = 444
      inherited fcOK: TfcShapeBtn
        Left = 163
        Kind = bkCustom
        ModalResult = 0
        OnClick = fcOKClick
      end
      inherited fcCancelar: TfcShapeBtn
        Left = 339
      end
      inherited fcLetraX: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraC: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraB: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraJ: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraH: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraF: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraA: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraW: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraE: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraT: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraI: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetra0: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcLetraP: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber7: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber6: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber4: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumber1: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited edTexto: TEdit
        Left = 8
        Top = 288
      end
      object fcCambiarClave: TfcShapeBtn
        Left = 258
        Top = 393
        Width = 147
        Height = 37
        Caption = 'Cambiar Clave'
        Color = clBlue
        DitherColor = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          777777777777777000777777777777033007777777777030B30777777777030B
          0B077777770030B0B000770000330B0B08F070333300B0B0FFF0037B7B3B0B08
          F8F007BB3B73B0FFFFF00BB3B3BB00F8F8F088303B3B70FFFFF0070703B3B0F8
          F8F00B803B3B70CCCCC070BB8BB7000000007700800077777777}
        ParentClipping = False
        ParentFont = False
        RoundRectBias = 25
        ShadeStyle = fbsHighlight
        Shape = bsRoundRect
        TabOrder = 41
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        OnClick = fcCambiarClaveClick
      end
    end
    object fcPanelDatos: TfcPanel
      Left = 7
      Top = 7
      Width = 662
      Height = 114
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
      BevelWidth = 5
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 2
      object EmpleadoNumero: TfcLabel
        Left = 10
        Top = 38
        Width = 640
        Height = 31
        AutoSize = False
        Caption = 'EmpleadoNumero'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clYellow
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TextOptions.Alignment = taCenter
        TextOptions.Style = fclsOutline
        TextOptions.VAlignment = vaTop
      end
      object EmpleadoNombre: TfcLabel
        Left = 10
        Top = 69
        Width = 640
        Height = 31
        AutoSize = False
        Caption = 'EmpleadoNombre'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TextOptions.Alignment = taCenter
        TextOptions.Style = fclsOutline
        TextOptions.VAlignment = vaTop
      end
      object EmpresaNombre: TfcLabel
        Left = 10
        Top = 10
        Width = 640
        Height = 31
        AutoSize = False
        Caption = 'EmpresaNombre'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TextOptions.Alignment = taCenter
        TextOptions.Style = fclsOutline
        TextOptions.VAlignment = vaTop
      end
    end
    object fcPanelPrompt: TfcPanel
      Left = 7
      Top = 121
      Width = 662
      Height = 53
      Align = alTop
      BevelInner = bvSpace
      BevelOuter = bvNone
      BevelWidth = 5
      BorderWidth = 5
      Caption = 'Capture su clave de acceso'
      Color = clSilver
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -32
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
  end
  object TimerSalida: TTimer
    Enabled = False
    OnTimer = TimerSalidaTimer
    Left = 15
    Top = 12
  end
end
