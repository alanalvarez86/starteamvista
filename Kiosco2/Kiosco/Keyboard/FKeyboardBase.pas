unit FKeyboardBase;

interface

uses
  Windows, Messages, SysUtils,
  {$Ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
  Dialogs, fcImage, fcimageform, ExtCtrls, fcpanel, fcButton, fcImgBtn,
  fcShapeBtn, StdCtrls, fcCombo, fcCalcEdit;

type
  TKeyboardBase = class(TForm)
    panelTeclado: TfcPanel;
    panelTexto: TfcPanel;
    panelBotones: TfcPanel;
    fcOK: TfcShapeBtn;
    fcCancelar: TfcShapeBtn;
    fcClear: TfcShapeBtn;
    fcLetraZ: TfcShapeBtn;
    fcLetraX: TfcShapeBtn;
    fcLetraC: TfcShapeBtn;
    fcLetraV: TfcShapeBtn;
    fcLetraB: TfcShapeBtn;
    fcLetraN: TfcShapeBtn;
    fcLetraM: TfcShapeBtn;
    fcBackspace: TfcShapeBtn;
    fcLetraL: TfcShapeBtn;
    fcLetraK: TfcShapeBtn;
    fcLetraJ: TfcShapeBtn;
    fcLetraH: TfcShapeBtn;
    fcLetraG: TfcShapeBtn;
    fcLetraF: TfcShapeBtn;
    fcLetraD: TfcShapeBtn;
    fcLetraS: TfcShapeBtn;
    fcLetraA: TfcShapeBtn;
    fcLetraQ: TfcShapeBtn;
    fcLetraW: TfcShapeBtn;
    fcLetraE: TfcShapeBtn;
    fcLetraR: TfcShapeBtn;
    fcLetraT: TfcShapeBtn;
    fcLetraY: TfcShapeBtn;
    fcLetraU: TfcShapeBtn;
    fcLetraI: TfcShapeBtn;
    fcLetra0: TfcShapeBtn;
    fcLetraP: TfcShapeBtn;
    fcNumber0: TfcShapeBtn;
    fcNumber9: TfcShapeBtn;
    fcNumber8: TfcShapeBtn;
    fcNumber7: TfcShapeBtn;
    fcNumber6: TfcShapeBtn;
    fcNumber5: TfcShapeBtn;
    fcNumber4: TfcShapeBtn;
    fcNumber3: TfcShapeBtn;
    fcNumber2: TfcShapeBtn;
    fcNumber1: TfcShapeBtn;
    edTexto: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure fcClearClick(Sender: TObject);
    procedure fcBackspaceClick(Sender: TObject);
    procedure AlOprimirUnaTecla(Sender: TObject);
    procedure edTextoChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    //FValor: String;
    FMascara: Char;
    function GetValor: String;
    function GetIsOk: Boolean;
    procedure SetValor(const Value: String);
  protected
    { Protected declarations }
    function HayMascara: Boolean;
    procedure AgregaCaracteres(const sValor: String ); virtual;
    procedure InicializaControles; virtual;
    procedure AlCapturar; virtual;
    procedure AlSalir; virtual;
    procedure Clear;
    procedure ShowValor;
  public
    { Public declarations }
    property Valor: String read GetValor write SetValor;
    property IsOk: Boolean read GetIsOk;
    property Mascara: Char read FMascara write FMascara;
  end;

var
  KeyboardBase: TKeyboardBase;

implementation

uses DCliente;

{$R *.dfm}

{ TKeyboardBase }

const
     MASCARA_NULA = Chr( 0 );

procedure TKeyboardBase.FormCreate(Sender: TObject);
begin
     FMascara := MASCARA_NULA;
     edTexto.Clear;
     //FValor := '';
end;

procedure TKeyboardBase.FormShow(Sender: TObject);
begin
     ShowValor;
     InicializaControles;
end;

procedure TKeyboardBase.InicializaControles;
begin
     with edTexto do
     begin
          Visible:= True;
          ActiveControl := edTexto;
          Width := 0;
          Height := 0;
     end;
end;

function TKeyboardBase.GetValor: String;
begin
     //Result := FValor;
     Result := edTexto.Text;
end;

procedure TKeyboardBase.SetValor(const Value: String);
begin
     if ( Value <> Valor ) then
     begin
          //FValor := AnsiUpperCase( Value );
          edTexto.Text := AnsiUpperCase( Value );
          ShowValor;
     end;
end;

procedure TKeyboardBase.ShowValor;
var
   sValor: String;
begin
     sValor := edTexto.Text;
     with panelTexto do
     begin
          if ( Length( sValor ) > 0 ) then
          begin
               if HayMascara then
               begin
                    Caption := StringOfChar( Mascara, Length( sValor ) );
               end
               else
               begin
                    Caption := sValor;
               end;
               fcOK.Enabled := True;
          end
          else
          begin
               Caption := '';
               fcOK.Enabled := False;
          end;
     end;
end;

procedure TKeyboardBase.AgregaCaracteres( const sValor: String );
begin
     //FValor := FValor + sValor;
     edTexto.Text := edTexto.Text + sValor;
     ShowValor;
end;

procedure TKeyboardBase.Clear;
begin
     edTexto.Clear;
     //FValor := '';
     ShowValor;
end;

procedure TKeyboardBase.AlCapturar;
begin
end;

procedure TKeyboardBase.AlSalir;
begin
end;

procedure TKeyboardBase.fcClearClick(Sender: TObject);
begin
     Clear;
     AlCapturar;
end;

procedure TKeyboardBase.fcBackspaceClick(Sender: TObject);
begin
     if ( Length( Valor ) > 0 ) then
     begin
          //FValor := Copy( FValor, 1, ( Length( FValor ) - 1 ) );
          edTexto.Text := Copy( edTexto.Text, 1, ( Length( edTexto.Text ) - 1 ) );
          ShowValor;
     end;
     AlCapturar;
end;

function TKeyboardBase.GetIsOk: Boolean;
begin
     Result := ( ModalResult = mrOK );
end;

function TKeyboardBase.HayMascara: Boolean;
begin
     Result := ( FMascara <> MASCARA_NULA );
end;

procedure TKeyboardBase.AlOprimirUnaTecla(Sender: TObject);
begin
     AgregaCaracteres( TfcShapeBtn( Sender ).Caption );
     AlCapturar;
end;

procedure TKeyboardBase.edTextoChange(Sender: TObject);
begin
     ShowValor;
     AlCapturar;
end;

procedure TKeyboardBase.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     AlSalir;
end;

end.
