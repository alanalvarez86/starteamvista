inherited PideGafete: TPideGafete
  Caption = 'PideGafete'
  ClientHeight = 552
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited panelTeclado: TfcPanel
    Height = 552
    inherited panelTexto: TfcPanel
      Top = 80
      Height = 68
    end
    inherited panelBotones: TfcPanel
      Top = 148
      Height = 397
      inherited fcOK: TfcShapeBtn
        Top = 299
        Kind = bkCustom
        TabOrder = 38
      end
      inherited fcCancelar: TfcShapeBtn
        Top = 299
        TabOrder = 40
      end
      inherited fcClear: TfcShapeBtn
        Top = 232
        TabOrder = 39
      end
      inherited fcLetraZ: TfcShapeBtn
        Top = 232
        NumGlyphs = 1
        TabOrder = 1
      end
      inherited fcLetraX: TfcShapeBtn
        Top = 232
        NumGlyphs = 0
        TabOrder = 2
      end
      inherited fcLetraC: TfcShapeBtn
        Top = 232
        NumGlyphs = 0
        TabOrder = 3
      end
      inherited fcLetraV: TfcShapeBtn
        Top = 232
        NumGlyphs = 1
        TabOrder = 4
      end
      inherited fcLetraB: TfcShapeBtn
        Top = 232
        NumGlyphs = 0
        TabOrder = 5
      end
      inherited fcLetraN: TfcShapeBtn
        Top = 232
        NumGlyphs = 1
        TabOrder = 6
      end
      inherited fcLetraM: TfcShapeBtn
        Top = 232
        NumGlyphs = 1
        TabOrder = 7
      end
      inherited fcBackspace: TfcShapeBtn
        Top = 232
        TabOrder = 8
      end
      inherited fcLetraL: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
        TabOrder = 9
      end
      inherited fcLetraK: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
        TabOrder = 10
      end
      inherited fcLetraJ: TfcShapeBtn
        Top = 162
        NumGlyphs = 0
        TabOrder = 11
      end
      inherited fcLetraH: TfcShapeBtn
        Top = 162
        NumGlyphs = 0
        TabOrder = 12
      end
      inherited fcLetraG: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
        TabOrder = 13
      end
      inherited fcLetraF: TfcShapeBtn
        Top = 162
        NumGlyphs = 0
        TabOrder = 14
      end
      inherited fcLetraD: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
        TabOrder = 15
      end
      inherited fcLetraS: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
        TabOrder = 16
      end
      inherited fcLetraA: TfcShapeBtn
        Top = 162
        NumGlyphs = 0
        TabOrder = 17
      end
      inherited fcLetraQ: TfcShapeBtn
        Top = 90
        NumGlyphs = 1
        TabOrder = 18
      end
      inherited fcLetraW: TfcShapeBtn
        Top = 90
        NumGlyphs = 0
        TabOrder = 19
      end
      inherited fcLetraE: TfcShapeBtn
        Top = 90
        NumGlyphs = 0
        TabOrder = 20
      end
      inherited fcLetraR: TfcShapeBtn
        Top = 90
        NumGlyphs = 1
        TabOrder = 21
      end
      inherited fcLetraT: TfcShapeBtn
        Top = 90
        NumGlyphs = 0
        TabOrder = 22
      end
      inherited fcLetraY: TfcShapeBtn
        Top = 90
        NumGlyphs = 1
        TabOrder = 23
      end
      inherited fcLetraU: TfcShapeBtn
        Top = 90
        NumGlyphs = 1
        TabOrder = 24
      end
      inherited fcLetraI: TfcShapeBtn
        Top = 90
        NumGlyphs = 0
        TabOrder = 25
      end
      inherited fcLetra0: TfcShapeBtn
        Top = 90
        NumGlyphs = 0
        TabOrder = 26
      end
      inherited fcLetraP: TfcShapeBtn
        Top = 90
        NumGlyphs = 0
        TabOrder = 27
      end
      inherited fcNumber0: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
        TabOrder = 28
      end
      inherited fcNumber9: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
        TabOrder = 29
      end
      inherited fcNumber8: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
        TabOrder = 30
      end
      inherited fcNumber7: TfcShapeBtn
        Top = 18
        NumGlyphs = 0
        TabOrder = 31
      end
      inherited fcNumber6: TfcShapeBtn
        Top = 18
        NumGlyphs = 0
        TabOrder = 32
      end
      inherited fcNumber5: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
        TabOrder = 33
      end
      inherited fcNumber4: TfcShapeBtn
        Top = 18
        NumGlyphs = 0
        TabOrder = 34
      end
      inherited fcNumber3: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
        TabOrder = 35
      end
      inherited fcNumber2: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
        TabOrder = 36
      end
      inherited fcNumber1: TfcShapeBtn
        Top = 18
        NumGlyphs = 0
        TabOrder = 37
      end
      inherited edTexto: TEdit
        TabOrder = 0
        Visible = False
      end
    end
    object fcPanelInfo: TfcPanel
      Left = 7
      Top = 7
      Width = 662
      Height = 73
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvSpace
      BevelWidth = 5
      Caption = 'Pasa tu gafete o captura tu n�mero de empleado'
      Color = clSilver
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object TimerSalida: TTimer
    Enabled = False
    OnTimer = TimerSalidaTimer
    Left = 23
    Top = 28
  end
end
