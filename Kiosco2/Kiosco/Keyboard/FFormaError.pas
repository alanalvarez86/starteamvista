unit FFormaError;

interface

uses Windows, Messages, SysUtils,{$Ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, StdCtrls, fcButton, fcImgBtn, fcShapeBtn, fcpanel,
     FKeyboardBase;

type
  TFormaError = class(TKeyboardBase)
    gbError: TfcGroupBox;
    memoError: TMemo;
    panelInfo: TfcPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure fcOKClick(Sender: TObject);
    procedure fcCancelarClick(Sender: TObject);
  private
    { Private declarations }
    FClave: String;
    FIntentos: Integer;
    FIntentosMaximos: Integer;
    procedure SetClave(const Value: String);
    function GetReintentar: Boolean;
    procedure SetErrorMsg(const Value: String);
  public
    { Public declarations }
    property ErrorMsg: String write SetErrorMsg;
    property Reintentar: Boolean read GetReintentar;
    property Clave: String read FClave write SetClave;
    property IntentosMaximos: Integer read FIntentosMaximos write FIntentosMaximos;
  end;

function MuestraError( const sClave, sError: String ): Boolean;
function MuestraExcepcion( const sClave: String; Error: Exception ): Boolean;

implementation

uses DCliente;

{$R *.dfm}

var
  FormaError: TFormaError;

function MuestraError( const sClave, sError: String ): Boolean;
begin
     Result := False;
     if not Assigned( FormaError ) then
        FormaError := TFormaError.Create( Application );
     with FormaError do
     begin
          Clave := sClave;
          Mascara := '*';
          ErrorMsg := sError;
          WindowState := wsMaximized;
          ShowModal;
          if IsOk then
          begin
               Result := True;
          end;
     end;
end;

function MuestraExcepcion( const sClave: String; Error: Exception ): Boolean;
begin
     Result := MuestraError( sClave, Error.Message );
end;

{ ************ TFormaError ********** }

procedure TFormaError.FormCreate(Sender: TObject);
begin
     inherited;
     FIntentos := 0;
     FIntentosMaximos := 0;
end;

procedure TFormaError.FormShow(Sender: TObject);
begin
     inherited;
     FIntentos := 0;
end;

procedure TFormaError.FormResize(Sender: TObject);
begin
     inherited;
     with PanelTeclado do
     begin
          if ( Self.Height > Height ) then
             Top := Trunc( ( Self.Height - Height ) / 2 );
          if ( Self.Width > Width ) then
             Left := Trunc( ( Self.Width - Width ) / 2 );
     end;
end;

function TFormaError.GetReintentar: Boolean;
begin
     Result := ( ModalResult = mrRetry );
end;

procedure TFormaError.SetClave(const Value: String);
begin
     FClave := AnsiUpperCase( Value );
end;

procedure TFormaError.SetErrorMsg(const Value: String);
begin
     with memoError.Lines do
     begin
          Clear;
          BeginUpdate;
          try
             Add( Value );
             dmCliente.GeneraBitacora( Value );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TFormaError.fcOKClick(Sender: TObject);
begin
     inherited;
     if ( Valor = Clave ) then
        ModalResult := mrOK
     else
     begin
          Inc( FIntentos );
          if ( IntentosMaximos > 0 ) and ( FIntentos >= IntentosMaximos ) then
          begin
               ModalResult := mrCancel;
          end
          else
          begin
               with panelInfo do
               begin
                    Caption := '� Clave Incorrecta ! Intente de nuevo';
                    Font.Color := clRed;
               end;
               Clear;
          end;
     end;
end;

procedure TFormaError.fcCancelarClick(Sender: TObject);
begin
     inherited;
     dmCliente.StartShow;
end;

end.
