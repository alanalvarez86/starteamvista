inherited PideSalida: TPideSalida
  Caption = 'PideSalida'
  ClientHeight = 552
  OldCreateOrder = True
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited panelTeclado: TfcPanel
    Height = 552
    Align = alNone
    inherited panelTexto: TfcPanel
      Top = 80
      Height = 68
    end
    inherited panelBotones: TfcPanel
      Top = 148
      Height = 397
      inherited fcOK: TfcShapeBtn
        Top = 299
        Caption = 'Salir'
        Default = False
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
          F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
          000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
          338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
          45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
          3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
          F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
          000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
          338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
          4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
          8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
          333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
          0000}
        Kind = bkCustom
        ModalResult = 0
        OnClick = fcOKClick
      end
      inherited fcCancelar: TfcShapeBtn
        Top = 299
        Cancel = False
        Caption = 'Regresar'
        Color = 8388863
        Kind = bkRetry
        ModalResult = 4
      end
      inherited fcClear: TfcShapeBtn
        Top = 232
      end
      inherited fcLetraZ: TfcShapeBtn
        Top = 232
        NumGlyphs = 1
      end
      inherited fcLetraX: TfcShapeBtn
        Top = 232
      end
      inherited fcLetraC: TfcShapeBtn
        Top = 232
      end
      inherited fcLetraV: TfcShapeBtn
        Top = 232
        NumGlyphs = 1
      end
      inherited fcLetraB: TfcShapeBtn
        Top = 232
      end
      inherited fcLetraN: TfcShapeBtn
        Top = 232
        NumGlyphs = 1
      end
      inherited fcLetraM: TfcShapeBtn
        Top = 232
        NumGlyphs = 1
      end
      inherited fcBackspace: TfcShapeBtn
        Top = 232
      end
      inherited fcLetraL: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
      end
      inherited fcLetraK: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
      end
      inherited fcLetraJ: TfcShapeBtn
        Top = 162
      end
      inherited fcLetraH: TfcShapeBtn
        Top = 162
      end
      inherited fcLetraG: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
      end
      inherited fcLetraF: TfcShapeBtn
        Top = 162
      end
      inherited fcLetraD: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
      end
      inherited fcLetraS: TfcShapeBtn
        Top = 162
        NumGlyphs = 1
      end
      inherited fcLetraA: TfcShapeBtn
        Top = 162
      end
      inherited fcLetraQ: TfcShapeBtn
        Top = 90
        NumGlyphs = 1
      end
      inherited fcLetraW: TfcShapeBtn
        Top = 90
      end
      inherited fcLetraE: TfcShapeBtn
        Top = 90
      end
      inherited fcLetraR: TfcShapeBtn
        Top = 90
        NumGlyphs = 1
      end
      inherited fcLetraT: TfcShapeBtn
        Top = 90
      end
      inherited fcLetraY: TfcShapeBtn
        Top = 90
        NumGlyphs = 1
      end
      inherited fcLetraU: TfcShapeBtn
        Top = 90
        NumGlyphs = 1
      end
      inherited fcLetraI: TfcShapeBtn
        Top = 90
      end
      inherited fcLetra0: TfcShapeBtn
        Top = 90
      end
      inherited fcLetraP: TfcShapeBtn
        Top = 90
      end
      inherited fcNumber0: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
      end
      inherited fcNumber9: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
      end
      inherited fcNumber8: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
      end
      inherited fcNumber7: TfcShapeBtn
        Top = 18
      end
      inherited fcNumber6: TfcShapeBtn
        Top = 18
      end
      inherited fcNumber5: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
      end
      inherited fcNumber4: TfcShapeBtn
        Top = 18
      end
      inherited fcNumber3: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
      end
      inherited fcNumber2: TfcShapeBtn
        Top = 18
        NumGlyphs = 1
      end
      inherited fcNumber1: TfcShapeBtn
        Top = 18
      end
    end
    object panelInfo: TfcPanel
      Left = 7
      Top = 7
      Width = 662
      Height = 73
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvSpace
      BevelWidth = 5
      Caption = 'Capture la clave de salida'
      Color = clSilver
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -24
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object TimerSalida: TTimer
    Enabled = False
    OnTimer = TimerSalidaTimer
    Left = 23
    Top = 28
  end
end
