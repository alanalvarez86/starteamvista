unit FPideNIP;

interface

uses Windows, Messages, SysUtils, {$Ifndef VER130}Variants,{$endif} Classes,
     Graphics, Controls, Forms, StdCtrls, Dialogs, ExtCtrls,
     fcButton, fcImgBtn, fcShapeBtn, fcpanel, fcLabel,
     FKeyboardBase,
     DCliente;

type
  TPideNIP = class(TKeyboardBase)
    fcPanelDatos: TfcPanel;
    fcPanelPrompt: TfcPanel;
    EmpleadoNumero: TfcLabel;
    EmpleadoNombre: TfcLabel;
    EmpresaNombre: TfcLabel;
    TimerSalida: TTimer;
    fcCambiarClave: TfcShapeBtn;
    procedure FormShow(Sender: TObject);
    procedure fcOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerSalidaTimer(Sender: TObject);
    procedure AlOprimirUnaTecla(Sender: TObject);
    procedure fcClearClick(Sender: TObject);
    procedure fcBackspaceClick(Sender: TObject);
    procedure fcCambiarClaveClick(Sender: TObject);
  private
    { Private declarations }
    FNombre: String;
    FNIP: String;
    FNumero: Integer;
    FEmpresa: String;
    FIntentos: Integer;
    FIntentosMaximos: Integer;
    procedure SetNIP(const Value: String);
    procedure ResetTimerSalida;
    function GetChangePassword: Boolean;
    procedure CheckNIP(Resultado: TModalResult);
  protected
    procedure AlSalir; override;
    procedure InicializaControles; override;
  public
    { Public declarations }
    property ChangePassword: Boolean read GetChangePassword;
    property Numero: Integer read FNumero write FNumero;
    property Nombre: String read FNombre write FNombre;
    property NIP: String read FNIP write SetNIP;
    property Empresa: String read FEmpresa write FEmpresa;
    property IntentosMaximos: Integer read FIntentosMaximos write FIntentosMaximos;
  end;

function PideClaveNIP( const iEmpleado, iIntentos: Integer; const sEmpresa, sPrettyName, sClave: String ): TPideNIPReply;

implementation

{$R *.dfm}

var
  PideNIP: TPideNIP;

function PideClaveNIP( const iEmpleado, iIntentos: Integer; const sEmpresa, sPrettyName, sClave: String ): TPideNIPReply;
begin
     Result := pnrCancel;
     if not Assigned( PideNIP ) then
        PideNIP := TPideNIP.Create( Application );
     with PideNIP do
     begin
          Clear;
          Numero := iEmpleado;
          Nombre := sPrettyName;
          NIP := sClave;
          Empresa := sEmpresa;
          Mascara := '*';
          IntentosMaximos := iIntentos;
          ShowModal;
          if IsOk then
          begin
               Result := pnrOk;
          end
          else
          begin
               if ChangePassword then
               begin
                    Result := pnrChangePassword;
               end
          end;
     end;
end;

{ ******** TPideNIP ********* }

procedure TPideNIP.FormCreate(Sender: TObject);
begin
     inherited;
     FIntentos := 0;
     FIntentosMaximos := 0;
end;

procedure TPideNIP.FormShow(Sender: TObject);
begin
     inherited;
     EmpleadoNumero.Caption := Format( '# %d', [ Numero ] );
     EmpleadoNombre.Caption := Nombre;
     EmpresaNombre.Caption := Empresa;
     FIntentos := 0;
     //InicializaControles;
end;

procedure TPideNIP.InicializaControles;
begin
     inherited InicializaControles;
     Valor := '';
     panelTexto.Caption := Valor;
     ResetTimerSalida;
     with fcPanelPrompt do
     begin
          Font.Color := clBlack;
          Caption := 'Capture su clave de acceso';
     end;
end;

procedure TPideNIP.SetNIP(const Value: String);
begin
     FNIP := AnsiUpperCase( Value );
end;

function TPideNIP.GetChangePassword: Boolean;
begin
     Result := ( ModalResult = mrRetry );
end;

procedure TPideNIP.CheckNIP( Resultado: TModalResult );
begin
     ResetTimerSalida;
     if ( Valor = NIP ) then
        ModalResult := Resultado
     else
     begin
          Inc( FIntentos );
          if ( IntentosMaximos > 0 ) and ( FIntentos >= IntentosMaximos ) then
          begin
               TimerSalida.Enabled := False;
               ModalResult := mrCancel;
          end
          else
          begin
               with fcPanelPrompt do
               begin
                    Caption := '� Clave Incorrecta ! Intente de nuevo';
                    Font.Color := clRed;
               end;
               Clear;
          end;
     end;
end;

procedure TPideNIP.fcOKClick(Sender: TObject);
begin
     inherited;
     CheckNIP( mrOK );
end;

procedure TPideNIP.fcCambiarClaveClick(Sender: TObject);
begin
     inherited;
     CheckNIP( mrRetry );
end;

procedure TPideNIP.TimerSalidaTimer(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TPideNIP.ResetTimerSalida;
begin
     with TimerSalida do
     begin
          Enabled := False;
          Interval := dmCliente.TimeoutDatos;
          Enabled := True;
     end;
end;

procedure TPideNIP.AlOprimirUnaTecla(Sender: TObject);
begin
     inherited;
     ResetTimerSalida;
end;

procedure TPideNIP.fcClearClick(Sender: TObject);
begin
     inherited;
     ResetTimerSalida;
end;

procedure TPideNIP.fcBackspaceClick(Sender: TObject);
begin
     inherited;
     ResetTimerSalida;
end;

procedure TPideNIP.AlSalir;
begin
     inherited AlSalir;
     //dmCliente.
end;

end.
