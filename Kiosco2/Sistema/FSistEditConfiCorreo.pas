unit FSistEditConfiCorreo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Mask, Forms, Dialogs, ExtCtrls,
     Buttons, DBCtrls, StdCtrls, Db,
     ZBaseEdicion,
     ZetaKeyCombo,
     ZetaEdit,
     ZetaKeyLookup, ZetaSmartLists, ZetaDBTextBox;

type
  TSistEditConfiCorreo = class(TBaseEdicion)
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    lbListaUsuarios: TLabel;
    CM_KUSERS: TDBEdit;
    bUsuarios: TSpeedButton;
    CM_KCONFI: TDBCheckBox;
    btnConfigurarConfidencialidad: TBitBtn;
    ZetaDBTextBox1: TZetaDBTextBox;
    CM_NOMBRE: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure btnConfigurarConfidencialidadClick(Sender: TObject);
    procedure bUsuariosClick(Sender: TObject);
    procedure CM_KCONFIClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaControlesConfiden;
  protected
    { Protected declarations }
    procedure Connect; override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
  end;

var
  SistEditConfiCorreo: TSistEditConfiCorreo;

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaCommonTools;

procedure TSistEditConfiCorreo.FormCreate(Sender: TObject);
begin
     inherited;
     { No valida derechos de acceso
     IndexDerechos := D_SIST_DATOS_EMPRESAS;}
     FirstControl := CM_KUSERS;
end;

procedure TSistEditConfiCorreo.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresasKiosco.Conectar;
          cdsNivel0.Conectar;
          Datasource.Dataset := cdsEmpresasKiosco;
          CM_KCONFI.Enabled := ( not cdsNivel0.IsEmpty );
          btnConfigurarConfidencialidad.Enabled:= CM_KCONFI.Checked and CM_KCONFI.Enabled;
     end;
end;

procedure TSistEditConfiCorreo.btnConfigurarConfidencialidadClick( Sender: TObject);
begin
     inherited;
     dmSistema.ConfigurarConfidencialidad;
end;

procedure TSistEditConfiCorreo.bUsuariosClick(Sender: TObject);
var
    sKey, sDescription: String;
begin
     inherited;
     dmSistema.cdsUsuarios.Conectar;
     if ( dmSistema.cdsUsuarios.Search( VACIO, sKey, sDescription  ) ) then
     begin
          with dmSistema.cdsEmpresasKiosco do
          begin
               if not ( State in [dsEdit,dsInsert] ) then
                  Edit;
               FieldByName('CM_KUSERS').AsString := ConcatString( CM_KUSERS.Text,  sKey, ',' );
          end;
     end;
end;

procedure TSistEditConfiCorreo.CM_KCONFIClick(Sender: TObject);
begin
     inherited;
     HabilitaControlesConfiden;
end;

function TSistEditConfiCorreo.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= FALSE;
     sMensaje:= 'No se puede agregar empresas desde la opci�n de configurar correo';
end;

function TSistEditConfiCorreo.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= FALSE;
     sMensaje:= 'No se puede borrar empresas desde la opci�n de configurar correo';
end;

procedure TSistEditConfiCorreo.HabilitaControlesConfiden;
begin
     lbListaUsuarios.Enabled:= not ( CM_KCONFI.Enabled and CM_KCONFI.Checked );
     CM_KUSERS.Enabled:= lbListaUsuarios.Enabled;
     bUsuarios.Enabled:= lbListaUsuarios.Enabled;
     btnConfigurarConfidencialidad.Enabled:= CM_KCONFI.Enabled and CM_KCONFI.Checked;
end;

procedure TSistEditConfiCorreo.FormShow(Sender: TObject);
begin
     inherited;
     HabilitaControlesConfiden;
end;

end.
