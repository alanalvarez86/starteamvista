inherited GridConfidencialidadCorreo: TGridConfidencialidadCorreo
  Caption = 'Direcci'#243'n de Correo por Confidencialidad'
  ClientHeight = 250
  ClientWidth = 422
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 214
    Width = 422
    inherited OK: TBitBtn
      Left = 254
    end
    inherited Cancelar: TBitBtn
      Left = 339
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 422
    inherited ValorActivo2: TPanel
      Width = 96
    end
  end
  inherited PanelSuperior: TPanel
    Width = 422
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 422
    Height = 163
    Columns = <
      item
        Expanded = False
        FieldName = 'TB_CODIGO'
        Title.Caption = 'Confidencialidad'
        Width = 103
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Lista de Usuarios'
        Width = 300
        Visible = True
      end>
  end
end
