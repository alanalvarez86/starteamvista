unit FSistConfiguracionCorreo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TSistConfiguracionCorreo = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  SistConfiguracionCorreo: TSistConfiguracionCorreo;

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonClasses;

{ TSistEmpresas }

procedure TSistConfiguracionCorreo.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext := H80811_Empresas;
end;

procedure TSistConfiguracionCorreo.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresasKiosco.Conectar;
          DataSource.DataSet:= cdsEmpresasKiosco;
     end;
end;

procedure TSistConfiguracionCorreo.Refresh;
begin
     dmSistema.cdsEmpresasKiosco.Refrescar;
end;

procedure TSistConfiguracionCorreo.Modificar;
begin
     dmSistema.cdsEmpresasKiosco.Modificar;
end;


function TSistConfiguracionCorreo.PuedeAgregar( var sMensaje: String): Boolean;
begin
     Result:= FALSE;
      sMensaje:= 'No se puede agregar empresas desde la opci�n de configurar correo';
end;

function TSistConfiguracionCorreo.PuedeBorrar( var sMensaje: String): Boolean;
begin
     Result:= FALSE;
     sMensaje:= 'No se puede borrar empresas desde la opci�n de configurar correo';
end;

end.

