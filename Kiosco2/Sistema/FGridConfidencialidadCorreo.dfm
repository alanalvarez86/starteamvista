inherited GridConfidencialidadCorreo: TGridConfidencialidadCorreo
  Caption = 'Direcci'#243'n de Correo por Confidencialidad'
  ClientHeight = 229
  ClientWidth = 433
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 193
    Width = 433
    inherited OK: TBitBtn
      Left = 265
    end
    inherited Cancelar: TBitBtn
      Left = 350
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 433
    inherited ValorActivo2: TPanel
      Width = 107
    end
  end
  inherited PanelSuperior: TPanel
    Width = 433
    inherited BuscarBtn: TSpeedButton
      Enabled = False
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 433
    Height = 142
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgCancelOnExit]
    Columns = <
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        ReadOnly = True
        Title.Caption = 'Confidencialidad'
        Width = 181
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KEM_USERS'
        Title.Caption = 'Lista de Usuarios'
        Width = 200
        Visible = True
      end>
  end
end
