unit FSistEditBitacoraKiosco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, StdCtrls, DBCtrls, ExtCtrls, ZetaDBTextBox,
  Buttons, DB;

type
  TSistEditBitacoraKiosco = class(TZetaDlgModal)
    Panel1: TPanel;
    lbFecha: TLabel;
    BI_FECHA: TZetaDBTextBox;
    lbHora: TLabel;
    BI_HORA: TZetaDBTextBox;
    lbTipo: TLabel;
    BI_TIPO: TZetaDBTextBox;
    lbKiosco: TLabel;
    BI_KIOSCO: TZetaDBTextBox;
    MensajeLBL: TLabel;
    lbEmpleado: TLabel;
    CB_CODIGO: TZetaDBTextBox;
    lbAccion: TLabel;
    BI_ACCION: TZetaDBTextBox;
    Imagen: TImage;
    lbMarco: TLabel;
    BI_UBICA: TZetaDBTextBox;
    BI_TEXTO: TDBMemo;
    DataSource: TDataSource;
    Label1: TLabel;
    CM_CODIGO: TZetaDBTextBox;
    DBNavigator: TDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
     procedure Conectar( Dataset: TDataset );
  end;

var
  SistEditBitacoraKiosco: TSistEditBitacoraKiosco;

procedure ShowLogDetail( Dataset: TDataset );

implementation

{$R *.dfm}

procedure ShowLogDetail( Dataset: TDataset );
begin
     if ( SistEditBitacoraKiosco = nil ) then
        SistEditBitacoraKiosco := TSistEditBitacoraKiosco.Create( Application );
     with SistEditBitacoraKiosco do
     begin
          Conectar( Dataset );
          ShowModal;
     end;
end;

procedure TSistEditBitacoraKiosco.Conectar(Dataset: TDataset);
begin
     DataSource.DataSet := Dataset;
end;

procedure TSistEditBitacoraKiosco.FormClose(Sender: TObject; var Action: TCloseAction);
begin
      inherited;
      DataSource.DataSet := nil;
end;

end.
