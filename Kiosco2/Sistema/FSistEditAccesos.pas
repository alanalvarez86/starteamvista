unit FSistEditAccesos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ImgList, Buttons, StdCtrls, ComCtrls, ExtCtrls,
     ZetaCommonClasses,
     ZBaseEditAccesos,
     ZetaDBTextBox;

const
     K_DERECHO_SUSPENDER = K_DERECHO_ADICIONAL9;
     K_DERECHO_REINICIAR = K_DERECHO_ADICIONAL10;
     K_DERECHO_CANCELAR = K_DERECHO_ADICIONAL11;
     K_DERECHO_REACTIVAR = K_DERECHO_ADICIONAL12;

type
  TSistEditAccesos = class(TZetaEditAccesos)
    procedure FormCreate(Sender: TObject);
  private
    procedure AddDerechoCancelar;
    procedure AddDerechoReiniciar;
    procedure AddDerechoSuspender;
    { Private declarations }
  protected
    { Protected declarations }
    function GetTipoDerecho(Nodo: TTreeNode): eDerecho; override;
    function TextoEspecial(const sText: String): Boolean; override;
    function NodoEspecial(const iIndex: Integer): eTipoNodo;override;
    procedure ArbolDefinir; override;
  public
    { Public declarations }
  end;

var
  SistEditAccesos: TSistEditAccesos;

implementation

uses ZHelpContext,
     ZAccesosTress,
     ZetaCommonLists;

const
     D_TEXT_DATOS_CONFI = 'Ver Datos Confidenciales';
     D_TEXT_CANCELAR_PROCESOS = 'Cancelar Procesos';
     D_TEXT_CANDIDATURAS = 'Ver Candidaturas';



{$R *.DFM}

procedure TSistEditAccesos.FormCreate(Sender: TObject);
begin
     inherited;
     {
     GeneraDerechosPorCodigo( 'D:\Temp\derechos.pas' );
     }
     ArbolConstruir;
     HelpContext := H_SIST_DATOS_GRUPOS_ACCESOS;
end;


procedure TSistEditAccesos.AddDerechoSuspender;
begin
     AddDerecho( 'Suspender' );
end;

procedure TSistEditAccesos.AddDerechoReiniciar;
begin
     AddDerecho( 'Reiniciar' );
end;

procedure TSistEditAccesos.AddDerechoCancelar;
begin
     AddDerecho( 'Cancelar' );
end;

procedure TSistEditAccesos.ArbolDefinir;
begin
     { *** Historial *** }{

     AddElemento( 0, D_HISTORIAL, 'Historial' );
        AddElemento( 1, D_HISTORIAL_PROCESOS, 'Procesos' );
           AddDerechoConsulta;
           AddDerechoEdicion;
           AddDerechoImpresion;
           AddDerechoSuspender;
           AddDerechoReiniciar;
           AddDerechoCancelar;
        AddElemento( 1, D_HISTORIAL_TAREAS, 'Tareas' );
           AddDerechoConsulta;
           AddDerechoEdicion;
           AddDerechoImpresion;
           AddDerecho( 'Reasignar' );
        AddElemento( 1, D_HISTORIAL_CORREOS, 'Correos' );
           AddDerechoConsulta;
           AddDerechoEdicion;
           AddDerechoImpresion;
     { *** Consultas ***
     AddElemento( 0, D_CONSULTAS, 'Consultas' );
        AddElemento( 1, D_CONS_REPORTES, 'Reportes' );
           AddElementoYDerechos( 2, D_REPORTES_WORKFLOW, ZetaCommonLists.ObtieneElemento( lfClasifiReporte, Ord( crWorkFlow ) ) );
           AddElementoYDerechos( 2, D_REPORTES_PORTAL, ZetaCommonLists.ObtieneElemento( lfClasifiReporte, Ord( crPortal ) ) );
           AddElementoYDerechos( 2, D_REPORTES_SISTEMA, ZetaCommonLists.ObtieneElemento( lfClasifiReporte, Ord( crSistema ) ) );
           AddElemento( 2, D_REPORTES_CONFIDENCIALES, 'Ver Datos Confidenciales' );
        AddElemento( 1, D_CONS_SQL, 'SQL' );
           AddDerechoConsulta;
           AddDerechoImpresion;
        AddElemento( 1, D_CONS_BITACORA, 'Bitácora / Procesos' );
           AddDerechoConsulta;
           AddDerecho( 'Cancelar Procesos' );
           AddDerechoImpresion;
        AddElemento( 1, D_CONS_EVENT_VIEWER, 'Eventos de Windows' );
           AddDerechoConsulta;
                                  }
     { *** Catálogos *** }

     AddElemento( 0, D_CATALOGOS, 'Catálogos' );
        AddElemento( 1, D_CAT_MODELOS, 'Modelos' );
           AddDerechoConsulta;
           AddDerechoAlta;
           AddDerechoBaja;
           AddDerechoEdicion;
           AddDerechoImpresion;
           AddDerechoSuspender;
           AddDerechoReiniciar;
           AddDerechoCancelar;
           AddDerecho( 'Reactivar' );
        AddElementoYDerechos( 1, D_CAT_PASOS_MODELOS, 'Pasos De Modelos' );
        AddElementoYDerechos( 1, D_CAT_ROLES, 'Roles' );
        AddElemento( 1, D_CAT_ACCIONES, 'Acciones' );
           AddDerechoConsulta;
           AddDerechoEdicion;
           AddDerechoImpresion;
        AddElemento( 1, D_CAT_CONEXIONES, 'Conexiones' );
           AddDerechoConsulta;
           AddDerechoEdicion;
           AddDerechoImpresion;
        AddElemento( 1, D_CATALOGOS_GENERALES, 'Generales' );
           AddElementoYDerechos( 2, D_CAT_GRALES_CONDICIONES, 'Condiciones' );
        AddElemento( 1, D_CATALOGOS_CONFIGURACION, 'Configuración' );
           AddElementoYDerechos( 2, D_CAT_CONFI_DICCIONARIO, 'Diccionario' );
           AddElemento( 2, D_CAT_CONFI_GLOBALES, 'Globales de Empresa' );
              AddDerechoConsulta;
              AddDerechoEdicion;
              AddDerechoImpresion;

     { *** Sistema *** }

     AddElemento( 0, D_SISTEMA, 'Sistema' );
        AddElemento( 1, D_SIST_DATOS, 'Datos' );
           {
           AddElementoYDerechos( 2, D_SIST_DATOS_EMPRESAS, 'Empresas' );
           }
           AddElementoYDerechos( 2, D_SIST_DATOS_GRUPOS, 'Grupos de Usuarios' );
           AddElementoYDerechos( 2, D_SIST_DATOS_USUARIOS, 'Usuarios' );
           AddElementoYDerechos( 2, D_SIST_DATOS_IMPRESORAS, 'Impresoras' );
        AddElemento( 1, D_SIST_PRENDE_USUARIOS, 'Activar Usuarios' );
        AddElemento( 1, D_SIST_APAGA_USUARIOS, 'Bloquear Usuarios' );
end;

function TSistEditAccesos.GetTipoDerecho(Nodo: TTreeNode): eDerecho;
begin
     with Nodo do
     begin
          if ( Text = D_TEXT_DATOS_CONFI ) then
              Result := edBorraSistKardex
          else
          if ( Text = D_TEXT_CANCELAR_PROCESOS ) then
             Result := edBaja
          else
          if ( Text = D_TEXT_CANDIDATURAS ) then
             Result := edConsulta
          else
          if ( Text = 'Suspender' ) then
             Result := edAdicional9
          else
          if ( Text = 'Reiniciar' ) then
             Result := edAdicional10
          else
          if ( Text = 'Cancelar' ) then
             Result := edAdicional11
          else
          if ( Text = 'Reactivar' ) then
             Result := edAdicional12
          else
          if (Text = 'Reasignar') then
             Result := edAdicional9
          else
              Result := inherited GetTipoDerecho( Nodo );
     end;
end;

function TSistEditAccesos.NodoEspecial(const iIndex: Integer): eTipoNodo;
begin
     case iIndex of
          D_REPORTES_CONFIDENCIALES: Result := tpEspecial; { Reportes \ Datos Confidenciales }
          D_SIST_PRENDE_USUARIOS: Result := tpEspecial; { Sistema\Usuarios\Activa Todos }
          D_SIST_APAGA_USUARIOS: Result := tpEspecial; { Sistema\Usuarios\Suspende Todos }
          else Result := inherited NodoEspecial( iIndex );
     end;
end;

function TSistEditAccesos.TextoEspecial(const sText: String): Boolean;
begin
     Result := ( sText = 'Registro' ) or
               {( sText = 'Procesos' ) or
               {( sText = 'Suspender' ) or
               ( sText = 'Reiniciar' ) or
               ( sText = 'Cancelar' ) or
               ( sText = 'Reactivar' ) or
               ( sText = 'Reasignar') or}
               ( sText = D_TEXT_CANDIDATURAS ) or
               inherited TextoEspecial( sText );
end;

end.
