unit FGridConfidencialidadCorreo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridEdicion, DB, Grids, DBGrids, ZetaDBGrid, DBCtrls,
  ZetaSmartLists, Buttons, ExtCtrls, StdCtrls;

type
  TGridConfidencialidadCorreo = class(TBaseGridEdicion)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaUsuario;
  protected
    function PuedeAgregar: Boolean; override;
    function PuedeBorrar: Boolean; override;
    procedure Connect; override;
    procedure Buscar; override;
  public
    { Public declarations }
  end;

var
  GridConfidencialidadCorreo: TGridConfidencialidadCorreo;

implementation

uses DSistema,
     DKiosco,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.dfm}

{ TGridConfidencialidadCorreo }

procedure TGridConfidencialidadCorreo.Buscar;
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'KEM_USERS' ) then
             BuscaUsuario;
     end;
end;

procedure TGridConfidencialidadCorreo.BuscaUsuario;
var
   sKey, sDescription: String;
begin
     with dmSistema do
     begin
          cdsUsuarios.Conectar;
          if ( cdsUsuarios.Search( VACIO, sKey, sDescription  ) ) then
          begin
               with dmKiosco.cdsConfiUsuarios do
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                    begin
                         Edit;
                    end;
                    FieldByName('KEM_USERS').AsString := ConcatString( ZetaDBGrid.SelectedField.AsString,  sKey, ',' );
               end;

          end;
     end;
end;

procedure TGridConfidencialidadCorreo.Connect;
begin
     inherited;
     with dmKiosco,dmSistema do
     begin
          cdsUsuarios.Conectar;
          cdsConfiUsuarios.Refrescar;
          DataSource.DataSet := cdsConfiUsuarios;
     end;
end;

function TGridConfidencialidadCorreo.PuedeAgregar: Boolean;
begin
     Result:= False;
end;

function TGridConfidencialidadCorreo.PuedeBorrar: Boolean;
begin
     Result:= False;
end;

procedure TGridConfidencialidadCorreo.FormCreate(Sender: TObject);
begin
     inherited;
     PermiteAltas := FALSE;
end;


end.
