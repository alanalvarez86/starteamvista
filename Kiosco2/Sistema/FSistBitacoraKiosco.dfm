inherited SistBitacoraKiosco: TSistBitacoraKiosco
  Left = 165
  Top = 266
  ClientHeight = 417
  ClientWidth = 873
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 873
    inherited ValorActivo2: TPanel
      Width = 531
    end
  end
  inherited PanelFiltros: TPanel
    Width = 873
    Height = 134
    inherited lbUsuario: TLabel
      Left = 3
      Top = 102
      Width = 50
      Caption = 'E&mpleado:'
      Enabled = False
    end
    inherited Label5: TLabel
      Left = 181
      Width = 36
      Caption = '&Acci'#243'n:'
      FocusControl = ClaseBit
    end
    object lbEmpresa: TLabel [3]
      Left = 9
      Top = 78
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = '&Empresa:'
      FocusControl = Empresa
    end
    inherited Refrescar: TBitBtn
      Left = 371
      TabOrder = 7
    end
    inherited Usuario: TZetaKeyLookup
      Top = 98
      Enabled = False
      LookupDataset = nil
      TabOrder = 4
    end
    inherited GroupBox1: TGroupBox
      Left = 56
      Width = 374
      Caption = 'Fecha:'
      inherited Label4: TLabel
        Left = 35
      end
      inherited Label3: TLabel
        Left = 214
        FocusControl = FechaFinal
      end
      inherited FechaInicial: TZetaFecha
        Left = 68
      end
      inherited FechaFinal: TZetaFecha
        Left = 243
      end
      inherited RBDeMovimiento: TRadioButton
        Left = 7
        Width = 17
        Caption = ''
        Visible = False
      end
      inherited RBDeCaptura: TRadioButton
        Left = 7
        Width = 25
        Caption = ''
        Visible = False
      end
    end
    inherited ClaseBit: TComboBox
      Left = 221
      TabOrder = 6
    end
    object Empresa: TZetaKeyLookup
      Left = 56
      Top = 74
      Width = 308
      Height = 21
      LookupDataset = dmSistema.cdsEmpresasLookUp
      TabOrder = 3
      TabStop = True
      WidthLlave = 50
      OnExit = EmpresaExit
      OnValidKey = EmpresaValidKey
      OnValidLookup = EmpresaValidLookup
    end
    object BtnTemp: TButton
      Left = 5
      Top = 12
      Width = 0
      Height = 25
      Caption = 'h'
      TabOrder = 5
      OnEnter = BtnTempEnter
    end
  end
  inherited BitacoraPG: TPageControl
    Top = 153
    Width = 873
    Height = 264
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 153
    Width = 873
    Height = 264
    Columns = <
      item
        Expanded = False
        FieldName = 'BI_FECHA'
        Title.Caption = 'Fecha'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_HORA'
        Title.Caption = 'Hora'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_TIPO'
        Title.Caption = 'Tipo'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'Empleado'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_TEXTO'
        Title.Caption = 'Descripci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_FEC_MOV'
        Title.Caption = 'Fecha Mov.'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'BI_NUMERO'
        Title.Caption = 'Proceso'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'BI_KIOSCO'
        Title.Caption = 'Kiosco'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_UBICA'
        Title.Caption = 'Marco'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_CODIGO'
        Title.Caption = 'Empresa'
        Width = 60
        Visible = True
      end>
  end
end
