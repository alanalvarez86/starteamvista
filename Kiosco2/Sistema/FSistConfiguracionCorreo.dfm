inherited SistConfiguracionCorreo: TSistConfiguracionCorreo
  Left = 310
  Top = 185
  Caption = 'Empresas'
  ClientWidth = 571
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 571
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 245
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 571
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CM_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_ALIAS'
        Title.Caption = 'Alias'
        Width = 145
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_NIVEL0'
        Title.Caption = 'Confidencialidad'
        Width = 85
        Visible = True
      end>
  end
end
