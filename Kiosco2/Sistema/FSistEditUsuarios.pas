unit FSistEditUsuarios;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, DBCtrls, Mask, ComCtrls, ExtCtrls, Buttons,
     FSistBaseEditUsuarios,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaKeyLookup,
     ZetaNumero, ZetaSmartLists;

type
  TSistEditUsuarios = class(TSistBaseEditUsuarios)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistEditUsuarios: TSistEditUsuarios;

implementation

uses DSistema,
     ZHelpContext,
     ZAccesosTress;

{$R *.DFM}

procedure TSistEditUsuarios.FormCreate(Sender: TObject);
begin
     inherited;
     Operacion.TabVisible := FALSE;
     HelpContext := H_CAT_USUARIOS;
    // IndexDerechos := D_CAT_USUARIOS;
end;

end.
