unit FSistBitacoraKiosco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FBaseSistBitacora, DB, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  StdCtrls, Mask, ZetaFecha, ZetaKeyLookup, Buttons, ZetaKeyCombo, ExtCtrls;

type
  TSistBitacoraKiosco = class(TBaseSistBitacora)
    Empresa: TZetaKeyLookup;
    lbEmpresa: TLabel;
    BtnTemp: TButton;
    procedure EmpresaExit(Sender: TObject);
    procedure EmpresaValidLookup(Sender: TObject);
    procedure EmpresaValidKey(Sender: TObject);
    procedure BtnTempEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    
  private
    { Private declarations }
    FEmpresa: String;
    procedure SetControls( const lEnabled: Boolean );
    procedure SetControlEmpleado;
  public
    { Public declarations }
  protected
    procedure Connect; override;
    procedure Modificar; override;
    procedure Refresh; override;
    procedure RefrescaDatos; override;
    procedure LlenaClaseBit; override;
  end;

var
  SistBitacoraKiosco: TSistBitacoraKiosco;

implementation

{$R *.dfm}

uses DCliente,
     DSistema,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     DKiosco;

{ TSistBitacoraKiosco }

procedure TSistBitacoraKiosco.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresasKiosco.Conectar;
          Empresa.LookupDataSet:= cdsEmpresasKiosco;

          cdsEmpleadosKiosco.Conectar;
          Self.Usuario.LookupDataSet := cdsEmpleadosKiosco;
     end;

     with dmKiosco do
     begin
          DataSource.DataSet := cdsBitacora;
     end;
end;

procedure TSistBitacoraKiosco.LlenaClaseBit;
var
   i: Integer;
begin
     with ClaseBit do
     begin
          Sorted := FALSE;
          for i:= 0 to Ord(High(eClaseBitacora)) do
              Items.AddObject( ObtieneElemento(lfClaseBitacora,i), TObject( i ) );
          ItemIndex := 0;
          Sorted := TRUE;
     end;
end;

procedure TSistBitacoraKiosco.Modificar;
begin
     inherited;
     dmKiosco.cdsBitacora.Modificar;
end;

procedure TSistBitacoraKiosco.RefrescaDatos;
begin
     dmKiosco.RefrescarBitacora( FParametros );
end;

procedure TSistBitacoraKiosco.Refresh;
begin
     with FParametros do
     begin
          AddInteger( 'EmpleadoKiosco', Usuario.Valor  );
          with ClaseBit do
               AddInteger( 'Accion', Integer( Items.Objects[ItemIndex] ) );
          AddString( 'Empresa', Empresa.Llave );
     end;
     inherited Refresh;
end;


procedure TSistBitacoraKiosco.EmpresaExit(Sender: TObject);
begin
     inherited;
     { Si FEmpresa es diferente a la llave significa que no es v�lido el lookup }
     if ( FEmpresa <> Empresa.Llave ) then
          SetControls( FALSE );

end;

procedure TSistBitacoraKiosco.EmpresaValidLookup(Sender: TObject);
begin
     inherited;
     { Guarda la �ltima llave correcta }
     FEmpresa:= Empresa.Llave;
     Usuario.Valor:= 0;
end;

procedure TSistBitacoraKiosco.SetControls(const lEnabled: Boolean);
begin
     lbUsuario.Enabled:= lEnabled;
     Usuario.Enabled:= lEnabled;
     if ( lEnabled ) then
     begin
          Usuario.SetFocus
     end
     else
         SetControlEmpleado;
end;



procedure TSistBitacoraKiosco.EmpresaValidKey(Sender: TObject);
begin
     inherited;
     { Para ZetaKeyLookup la llave vac�a es v�lida }
     if ( StrLleno( Empresa.Llave ) ) then
        SetControls( TRUE );
end;

procedure TSistBitacoraKiosco.SetControlEmpleado;
begin
     Usuario.SetLlaveDescripcion( VACIO, VACIO );
end;

procedure TSistBitacoraKiosco.BtnTempEnter(Sender: TObject);
begin
      inherited;
       if ( Usuario.Enabled ) or strVacio( Empresa.Llave ) then
         Refrescar.SetFocus;
end;



procedure TSistBitacoraKiosco.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := False;
end;

end.
