unit FEditBotones;

interface
//KE_CODIGO,KE_NOMBRE,KE_COLOR,KE_F_NAME,KE_F_SIZE,KE_F_COLR,KE_F_BOLD,KE_F_ITAL,KE_F_SUBR

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Mask, DBCtrls, StdCtrls, Db, ExtCtrls, Buttons,
     ZBaseEdicion,
     ZetaNumero,
     ZetaEdit,
     ZetaSmartLists,
     ZetaKeyLookup,
     ZetaDBTextBox,
     ZetaKeyCombo, ExtDlgs;

type
  TTOEditBotones = class(TBaseEdicion)
    TB_CODIGOlbl: TLabel;
    KS_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    KB_TEXTO: TDBEdit;
    KB_BITMAP: TDBEdit;
    Label5: TLabel;
    KB_ACCION: TZetaDBKeyCombo;
    Label6: TLabel;
    lbKB_URL: TLabel;
    KB_URL: TDBEdit;
    KB_SCREEN: TZetaDBKeyLookup;
    lbKB_SCREEN: TLabel;
    LBKB_REPORTE: TLabel;
    KB_REPORTE: TZetaDBNumero;
    lbSepara: TLabel;
    KB_SEPARA: TZetaDBNumero;
    Label2: TLabel;
    KB_LUGAR: TZetaDBKeyCombo;
    KB_ALTURA: TZetaDBNumero;
    Label3: TLabel;
    Label4: TLabel;
    KB_ORDEN: TZetaDBNumero;
    Label7: TLabel;
    KB_POS_BIT: TZetaDBKeyCombo;
    bBuscaImagen: TSpeedButton;
    OpenPictureDialog1: TOpenPictureDialog;
    bReportes: TSpeedButton;
    bConfiguraRestric: TBitBtn;
    KB_RESTRIC: TDBCheckBox;
    KB_USA_EMPL: TDBCheckBox;
    KB_IMP_DIR: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure KB_ACCIONChange(Sender: TObject);
    procedure bBuscaImagenClick(Sender: TObject);
    procedure KB_LUGARChange(Sender: TObject);
    procedure bReportesClick(Sender: TObject);
    procedure bConfiguraRestricClick(Sender: TObject);
    procedure KB_RESTRICClick(Sender: TObject);
  private
    procedure SetControls;
    procedure SetControlesRestriccion;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure EscribirCambios;override;
    procedure DoCancelChanges;override;

  public
    { Public declarations }
  end;

var
  EdicionBotones: TTOEditBotones;

implementation

uses DKiosco,
     DSistema,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaBuscador,
     FEditAccesoGrupos;

{$R *.DFM}




procedure TTOEditBotones.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 0;
     IndexDerechos := 0; //D_PORTAL_CAT_NOTICIAS_TIPO;
     with dmKiosco do
     begin
          KB_SCREEN.LookupDataset := cdsMarcosLookup;
     end;
     KB_ACCION.ListaFija := lfAccionBoton;
     KB_LUGAR.ListaFija := lfLugarBoton;
     KB_POS_BIT.ListaFija := lfPosicionIcono;
     FirstControl := KB_TEXTO;
end;

procedure TTOEditBotones.Connect;
begin
     with dmKiosco do
     begin
          cdsMarcosLookup.Refrescar;
          DataSource.DataSet := cdsBotones;
     end;
end;

procedure TTOEditBotones.FormShow(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TTOEditBotones.SetControls;
const
     K_PANTALLA = 'Pantalla:';
     K_PAGINA_WEB = 'P�gina web:';
 var
    eAccion: eAccionBoton;
begin
     inherited;
     eAccion := eAccionBoton( KB_ACCION.Valor );

     KB_URL.Enabled := eAccion in [abContenido,abMisDatos];
     lbKB_URL.Enabled := KB_URL.Enabled;

     if KB_URL.Enabled then
     begin
          if ( eAccion in [abMisDatos] )then
              lbKB_URL.Caption := K_PANTALLA
          else
              if ( eAccion in [abContenido] )then
                lbKB_URL.Caption := K_PAGINA_WEB;
     end;

     KB_REPORTE.Enabled := ( eAccion = abReporte );
     lbKB_REPORTE.Enabled := KB_REPORTE.Enabled;
     KB_IMP_DIR.Enabled := KB_REPORTE.Enabled;
     bReportes.Enabled := KB_REPORTE.Enabled;
     KB_USA_EMPL.Enabled:= KB_REPORTE.Enabled;

     KB_SCREEN.Enabled := ( eAccion = abPantalla );
     lbKB_SCREEN.Enabled := KB_SCREEN.Enabled;

     KB_LUGARChange(Nil);

     SetControlesRestriccion;

end;

procedure TTOEditBotones.SetControlesRestriccion;
var
    eAccion: eAccionBoton;
begin
     eAccion := eAccionBoton( KB_ACCION.Valor );
     KB_RESTRIC.Enabled:= not( eAccion in [abRegresar]);
     bConfiguraRestric.Enabled:= KB_RESTRIC.Enabled and KB_RESTRIC.Checked;
end;

procedure TTOEditBotones.KB_ACCIONChange(Sender: TObject);
begin
     inherited;
     SetControls;
end;


procedure TTOEditBotones.EscribirCambios;
var
   oCursor: TCursor;
begin
     with ClientDataset do
     begin
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  Enviar;
               finally
                      Cursor := oCursor;
               end;
          end;
     end;
end;


procedure TTOEditBotones.bBuscaImagenClick(Sender: TObject);
begin
     inherited;
     with ClientDataset do
     begin
          if ( State = dsBrowse ) then Edit;
             with FieldByName('KB_BITMAP') do
                  AsString := OpenDialogImagen( AsString, 'Bitmaps (*.bmp)|*.bmp', 'bmp' );
     end;
end;

procedure TTOEditBotones.KB_LUGARChange(Sender: TObject);
begin
     inherited;
     KB_SEPARA.Enabled :=  ( eLugarBoton( KB_LUGAR.ItemIndex ) = lbEspaciado );
     lbSepara.Enabled := KB_SEPARA.Enabled;
end;

procedure TTOEditBotones.bReportesClick(Sender: TObject);
 var
    sKey, sDescription: String;
begin
     inherited;
     with dmKiosco do
     begin
          if ( ClientDataset.State = dsBrowse ) then ClientDataset.Edit;

          if ( cdsSuscripReportes.Search( VACIO, sKey, sDescription ) ) then
          begin
               ClientDataset.FieldByName('KB_REPORTE').AsInteger := StrToInt( sKey );
          end;
     end;
end;

procedure TTOEditBotones.bConfiguraRestricClick(Sender: TObject);
begin
     inherited;
     if ( EditAccesoGrupos = NIL ) then
        EditAccesoGrupos:= TEditAccesoGrupos.Create(self);

     EditAccesoGrupos.ShowModal;
end;

procedure TTOEditBotones.KB_RESTRICClick(Sender: TObject);
begin
     inherited;
     SetControlesRestriccion;
end;


procedure TTOEditBotones.DoCancelChanges;
begin
     inherited;
     {�sto es para solucionar el problema de que no tomaba el itemindex de KB_ACCION.Valor correctamente al cancelar}
     SetControls;
end;

end.
