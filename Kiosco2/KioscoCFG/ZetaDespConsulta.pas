unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,
                     efcHistProcesos,
                     efcHistTareas,
                     efcHistCorreos,
                     efcCatModelo,
                     efcCatPasosModelo,
                     efcCatUsuarios,
                     efcCatRoles,
                     efcCatAcciones,
                     efcCatCondiciones,
                     efcReportes,
                     efcQueryGral,
                     efcDiccion,
                     efcSistGlobales,
                     efcSistBitacora,
                     efcSistProcesos,
                     efcSistEmpresas,
                     efcSistGrupos,
                     efcSistUsuarios,
                     efcImpresoras,
                     efcEventViewer,
                     efcConexiones );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress{
     FHistProcesos,
     FHistTareas,
     FHistCorreos,
     FCatModelos,
     FCatPasosModelo,
     FCatCondiciones,
     FCatUsuarios,
     FCatRoles,
     FCatAcciones,
     FReportes,
     FQueryGral,
     FSistGlobales,
     FDiccion,
     FSistEmpresas,
     FSistGrupos,
     FSistUsuarios,
     FSistBitacora,
     FSistProcesos,
     FImpresoras,
     FEventLogViewer,
     FCatConexiones  }
     ;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
{
     case Forma of
       efcHistProcesos           : Result := Consulta( D_HISTORIAL_PROCESOS, THistProcesos );
       efcHistTareas             : Result := Consulta( D_HISTORIAL_TAREAS, THistTareas );
       efcHistCorreos            : Result := Consulta( D_HISTORIAL_CORREOS, THistCorreos );
       efcReportes               : Result := Consulta( D_CONS_REPORTES, TReportes );
       efcCatModelo              : Result := Consulta( D_CAT_MODELOS, TCatModelos );
       efcCatPasosModelo         : Result := Consulta( D_CAT_PASOS_MODELOS, TCatPasosModelo );
       efcCatUsuarios            : Result := Consulta( D_CAT_USUARIOS, TCatUsuarios );
       efcCatRoles               : Result := Consulta( D_CAT_ROLES, TCatRoles );
       efcCatAcciones            : Result := Consulta( D_CAT_ACCIONES, TCatAcciones );
       efcCatCondiciones         : Result := Consulta( D_CAT_GRALES_CONDICIONES, TCatCondiciones );
       efcQueryGral              : Result := Consulta( D_CONS_SQL, TQueryGral );
       efcDiccion                : Result := Consulta( D_CAT_CONFI_DICCIONARIO, TDiccion );
       efcSistGlobales           : Result := Consulta( D_CAT_CONFI_GLOBALES, TSistGlobales );
       efcSistBitacora           : Result := Consulta( D_CONS_BITACORA, TSistBitacora );
       efcSistProcesos           : Result := Consulta( D_CONS_BITACORA, TSistProcesos );
       efcSistEmpresas           : Result := Consulta( D_SIST_DATOS_EMPRESAS, TSistEmpresas );
       efcSistGrupos             : Result := Consulta( D_SIST_DATOS_GRUPOS, TSistGrupos );
       efcSistUsuarios           : Result := Consulta( D_SIST_DATOS_USUARIOS, TSistUsuarios );
       efcImpresoras             : Result := Consulta( D_SIST_DATOS_IMPRESORAS, TImpresoras );
//       efcEventViewer            : Result := Consulta( D_CONS_EVENT_VIEWER, TEventLogViewer );
       efcConexiones             : Result := Consulta( D_CAT_CONEXIONES, TCatConexiones);
     else
         Result := Consulta( 0, nil );
     end;    }
end;

end.
