unit FEncuestas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls, Mask,
  ZetaFecha, ZetaCommonClasses, Buttons;

type
  TTOEncuestas = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    PanelFiltros: TPanel;
    Label3: TLabel;
    cbStatus: TComboBox;
    EdadMinimaLBL: TLabel;
    FechaInicial: TZetaFecha;
    EdadMaximaLBL: TLabel;
    FechaFinal: TZetaFecha;
    FILTRAR: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure cbStatusChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FParametros: TZetaParams;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  TOEncuestas: TTOEncuestas;

implementation

uses dKiosco,
     ZetaBuscador,
     ZetaCommonTools,
     ZetaCommonLists;

{$R *.DFM}

{ TCarruseles }

procedure TTOEncuestas.FormCreate(Sender: TObject);
begin
     inherited;
     FParametros:= TZetaParams.Create;
     CanLookup := True;
     HelpContext := 32000;
     IndexDerechos := 0;
     with cbStatus do
     begin
          ZetaCommonLists.LlenaLista( lfStatusEncuesta, Items );
          Items.Insert( 0, '< Todos >' );
          ItemIndex := 0;
     end;
     FechaInicial.Valor := ZetaCommonTools.FirstDayOfYear( TheYear( Date ) );
     FechaFinal.Valor := ZetaCommonTools.LastDayOfYear( TheYear( Date ) );
end;

procedure TTOEncuestas.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FParametros );
     inherited;
end;

procedure TTOEncuestas.Connect;
begin
     with dmKiosco do
     begin
          Refresh;
          DataSource.DataSet:= cdsEncuestas;
     end;
end;

procedure TTOEncuestas.Refresh;
begin
     with FParametros do
     begin
          AddInteger( 'Status', CBStatus.ItemIndex - 1 );
          AddDate( 'FechaInicial', FechaInicial.Valor );
          AddDate( 'FechaFinal', FechaFinal.Valor );
     end;
     dmKiosco.RefrescarEncuestas( FParametros )
end;

procedure TTOEncuestas.Agregar;
begin
     dmKiosco.cdsEncuestas.Agregar;
end;

procedure TTOEncuestas.Borrar;
begin
     dmKiosco.cdsEncuestas.Borrar;
end;

procedure TTOEncuestas.Modificar;
begin
     dmKiosco.cdsEncuestas.Modificar;
//     ZetaDBGrid.Repaint;
end;

procedure TTOEncuestas.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Encuestas', 'ENC_CODIGO', dmKiosco.cdsEncuestas );
end;

procedure TTOEncuestas.cbStatusChange(Sender: TObject);
begin
     inherited;
     self.Refresh;
end;

end.
