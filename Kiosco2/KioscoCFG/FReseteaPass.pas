unit FReseteaPass;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo, Mask,
  ZetaNumero, ZetaKeyLookup;

type
  TReseteaPass = class(TZetaDlgModal)
    eEmpresa: TZetaKeyCombo;
    Label7: TLabel;
    Label1: TLabel;
    eEmpleado: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure eEmpresaChange(Sender: TObject);
  private
    function GetEmpleado: integer;
    function GetEmpresa: string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReseteaPass: TReseteaPass;

implementation
uses
    ZetaDialogo,
    ZetaCommonClasses,
    DKiosco,
    DCliente;

{$R *.dfm}

procedure TReseteaPass.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          ListaCompanies( eEmpresa.Lista );
          eEmpleado.LookupDataset := cdsEmpleadoLookUp;
     end;

end;

procedure TReseteaPass.FormShow(Sender: TObject);
begin
     dmCliente.EmpresaPass := VACIO;      
     inherited;
     eEmpresa.ItemIndex := 0;
     eEmpleado.Valor := 0;
     dmCliente.EmpresaPass := GetEmpresa;
end;

function TReseteaPass.GetEmpresa: string;
begin
     with eEmpresa do
     begin
          if ( ItemIndex < 0 ) then
          begin
               ZetaDialogo.ZError( Caption, 'Favor de seleccionar una empresa', 0 );
               Abort;
          end
          else
              Result := Lista.Names[ItemIndex];
     end;
end;

function TReseteaPass.GetEmpleado: integer;
begin
     Result := 0;
     with eEmpleado do
     begin
          if ( Valor = 0 ) then
          begin
               ZetaDialogo.ZError( Caption, 'Favor de indicar el n�mero de empleado', 0 );
               Abort;
          end
          else
              Result := Valor;
     end;
end;

procedure TReseteaPass.OKClick(Sender: TObject);
begin
     inherited;
     dmCliente.EmpresaPass := GetEmpresa;
     try
        ModalResult := mrNone;
        dmKiosco.ReseteaPassword( GetEmpresa, GetEmpleado );
        ModalResult := mrOK;
     finally
            dmCliente.EmpresaPass := VACIO;
     end;
end;


procedure TReseteaPass.eEmpresaChange(Sender: TObject);
begin
     inherited;
     dmCliente.EmpresaPass := GetEmpresa;
     eEmpleado.ResetMemory;
     eEmpleado.Llave := VACIO;
end;
end.
