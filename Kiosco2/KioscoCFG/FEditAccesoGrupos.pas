unit FEditAccesoGrupos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZBaseDlgModal, ZetaSmartLists;

type
  TEditAccesoGrupos = class(TZetaDlgModal)
    Grupos: TCheckListBox;
    Prender: TBitBtn;
    Apagar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure GruposClickCheck(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
  public
    { Public declarations }
  end;

var
  EditAccesoGrupos: TEditAccesoGrupos;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema,
     DKiosco;

{$R *.DFM}

procedure TEditAccesoGrupos.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TEditAccesoGrupos.FormShow(Sender: TObject);
const
     K_CAPTION = 'Asignar grupo a bot�n #';
begin
     inherited;
     Self.Caption:= K_CAPTION + dmKiosco.cdsBotones.FieldByName('KB_ORDEN').AsString;
     Cargar;
     OK.Enabled := False;
     ActiveControl := Grupos;
end;

procedure TEditAccesoGrupos.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TEditAccesoGrupos.Cargar;
var
   i: Integer;
begin
     dmKiosco.CargaListaGrupos( FLista );
     with Grupos do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       Items.Add( FLista[i] );
                       Checked[ i ] := ( Integer( Objects[ i ] ) > 0 );
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TEditAccesoGrupos.Descargar;
var
   i: Integer;
begin
     with Grupos do
     begin
          with Items do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if Checked[ i ] then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmKiosco.DescargaListaGrupos( FLista );
end;

procedure TEditAccesoGrupos.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Grupos do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
     GruposClickCheck( Grupos );
end;

procedure TEditAccesoGrupos.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TEditAccesoGrupos.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TEditAccesoGrupos.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TEditAccesoGrupos.GruposClickCheck(Sender: TObject);
begin
     inherited;
     with OK do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

end.
