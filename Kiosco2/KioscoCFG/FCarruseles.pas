unit FCarruseles;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TTOCarruseles = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  TOCarruseles: TTOCarruseles;

implementation

uses DKiosco,
     ZetaBuscador;

{$R *.DFM}

{ TCarruseles }

procedure TTOCarruseles.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := 0;
     IndexDerechos := 0;//D_PORTAL_INFO_DOCUMENTOS;
end;

procedure TTOCarruseles.Connect;
begin
     with dmKiosco do
     begin
          cdsCarruseles.Conectar;
          DataSource.DataSet:= cdsCarruseles;
     end;
end;

procedure TTOCarruseles.Agregar;
begin
     dmKiosco.cdsCarruseles.Agregar;
end;

procedure TTOCarruseles.Borrar;
begin
     dmKiosco.cdsCarruseles.Borrar;
end;

procedure TTOCarruseles.Modificar;
begin
     dmKiosco.cdsCarruseles.Modificar;
end;

procedure TTOCarruseles.Refresh;
begin
     dmKiosco.cdsCarruseles.Refrescar;
end;

procedure TTOCarruseles.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Folio', 'Carrusel', 'KW_CODIGO', dmKiosco.cdsCarruseles );
end;

end.
