unit FKiosko;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, ComCtrls;

type
  TKiosko = class(TBaseConsulta)
    PageControl: TPageControl;
    Pantallas: TTabSheet;
    DBGridPantallas: TDBGrid;
    Secuencias: TTabSheet;
    DBGridSecuencias: TDBGrid;
    dsSecuencias: TDataSource;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function EsPantallas: Boolean;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Kiosko: TKiosko;

implementation

uses DPortal;

{$R *.DFM}

procedure TKiosko.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
     // PENDIENTE : HelpContext := H60612_Clasificaciones;
end;

procedure TKiosko.Connect;
begin
     with dmKiosco do
     begin
          cdsPantallas.Conectar;
          cdsSecuencias.Conectar;
          DataSource.DataSet:= cdsPantallas;
          dsSecuencias.Dataset := cdsSecuencias;
     end;
end;

procedure TKiosko.Refresh;
begin
     with dmKiosco do
     begin
          cdsPantallas.Refrescar;
          cdsSecuencias.Refrescar;
     end;
end;

function TKiosko.EsPantallas: Boolean;
begin
     Result := ( PageControl.ActivePage.PageIndex = Pantallas.PageIndex );
end;

procedure TKiosko.Agregar;
begin
     with dmKiosco do
     begin
          if EsPantallas then
             cdsPantallas.Agregar
          else
              cdsSecuencias.Agregar;
     end;
end;

procedure TKiosko.Borrar;
begin
     with dmKiosco do
     begin
          if EsPantallas then
             cdsPantallas.Borrar
          else
              cdsSecuencias.Borrar;
     end;
end;

procedure TKiosko.Modificar;
begin
     with dmKiosco do
     begin
          if EsPantallas then
             cdsPantallas.Modificar
          else
              cdsSecuencias.Modificar;
     end;
end;

end.
