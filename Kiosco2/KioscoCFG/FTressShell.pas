unit FTressShell;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, Buttons, ToolWin, ExtCtrls, StdCtrls, Menus, ActnList, ShellAPI,
     fcOutlookList, fcButton, fcImgBtn, fcShapeBtn, fcClearPanel, fcButtonGroup, fcOutlookBar, ImgList,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaSmartLists,
     ZBaseConsulta,
     ZBaseShell;

type
  TTressShell = class(TBaseShell)
    PanelOpciones: TPanel;
    PanelPrincipal: TPanel;
    PanelBotones: TPanel;
    BarraBotones: TToolBar;
    BtnAgregar: TZetaSpeedButton;
    BtnBorrar: TZetaSpeedButton;
    BtnModificar: TZetaSpeedButton;
    ToolButton1: TToolButton;
    BtnRefrescar: TZetaSpeedButton;
    BtnImprimir: TZetaSpeedButton;
    PanelConsulta: TPanel;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _A_ImprimirForma: TAction;
    _A_ConfigurarImpresora: TAction;
    _A_Servidor: TAction;
    _A_SalirSistema: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    _E_Refrescar: TAction;
    _H_Contenido: TAction;
    _H_Glosario: TAction;
    _H_UsoTeclado: TAction;
    _H_ComoUsarAyuda: TAction;
    _H_AcercaDe: TAction;
    ShellMenu: TMainMenu;
    Archivo: TMenuItem;
    ArchivoImprimir: TMenuItem;
    ArchivoImprimirForma: TMenuItem;
    ArchivoImpresora: TMenuItem;
    N7: TMenuItem;
    ArchivoServidor: TMenuItem;
    N5: TMenuItem;
    ArchivoSalir: TMenuItem;
    Editar: TMenuItem;
    EditarAgregar: TMenuItem;
    EditarBorrar: TMenuItem;
    EditarModificar: TMenuItem;
    EditarSeparador2: TMenuItem;
    EditarRefrescar: TMenuItem;
    Ayuda: TMenuItem;
    AyudaContenido: TMenuItem;
    AyudaGlosario: TMenuItem;
    AyudaCombinacionesTeclas: TMenuItem;
    AyudaUsandoAyuda: TMenuItem;
    AyudaSeparador2: TMenuItem;
    AyudaAcercaDe: TMenuItem;
    EscogeImpresora: TPrinterSetupDialog;
    Splitter: TSplitter;
    PanelHorizontal: TPanel;
    LblFormato: TLabel;
    _E_BuscarCodigo: TAction;
    BtnBuscarCodigo: TZetaSpeedButton;
    N1: TMenuItem;
    EditarBuscarCodigo: TMenuItem;
    fcOutlookBar: TfcOutlookBar;
    Imagenes: TImageList;
    fcKioscoList: TfcOutlookList;
    fcKiosco: TfcShapeBtn;
    ReseteaPassword: TAction;
    ConfiguraBitacora: TAction;
    Exportar: TZetaSpeedButton;
    _A_Exportar: TAction;
    ToolButton2: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );
    procedure FormDestroy(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _A_ImprimirFormaExecute(Sender: TObject);
    procedure _A_ConfigurarImpresoraExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _H_ContenidoExecute(Sender: TObject);
    procedure _H_GlosarioExecute(Sender: TObject);
    procedure _H_UsoTecladoExecute(Sender: TObject);
    procedure _H_ComoUsarAyudaExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _E_BuscarCodigoExecute(Sender: TObject);
    procedure fcKioscoListItems0Click(OutlookList: TfcCustomOutlookList;
      Item: TfcOutlookListItem);
    procedure fcKioscoListItems1Click(OutlookList: TfcCustomOutlookList;
      Item: TfcOutlookListItem);
    procedure fcKioscoListItems2Click(OutlookList: TfcCustomOutlookList;
      Item: TfcOutlookListItem);
    procedure fcKioscoListItems3Click(OutlookList: TfcCustomOutlookList;
      Item: TfcOutlookListItem);
    procedure fcKioscoListItems4Click(OutlookList: TfcCustomOutlookList;
      Item: TfcOutlookListItem);
    procedure ReseteaPasswordExecute(Sender: TObject);
    procedure fcKioscoListItems5Click(OutlookList: TfcCustomOutlookList;
      Item: TfcOutlookListItem);
    procedure ConfiguraBitacoraExecute(Sender: TObject);
    procedure _A_ExportarExecute(Sender: TObject);
    procedure _E_BuscarCodigoUpdate(Sender: TObject);
    procedure fcKioscoListItems6Click(OutlookList: TfcCustomOutlookList;
      Item: TfcOutlookListItem);
    procedure fcKioscoListItems7Click(OutlookList: TfcCustomOutlookList;
      Item: TfcOutlookListItem);
  private
    { Private declarations }
    FFormaActiva: TBaseConsulta;
    function GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean ): TBaseConsulta;
    procedure ActivaFormaPanel(InstanceClass: TBaseConsultaClass; const InstanceName: String; const IndexRights: Integer = 0 );
    procedure RevisaDerechos;
    procedure SetFormaActiva(Forma: TBaseConsulta);
  protected
    { Protected declarations }
    procedure AbreEmpresa(const lDefault: Boolean); override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
  public
    { Public declarations }
    property FormaActiva: TBaseConsulta read FFormaActiva;
    function ExecuteFile(const FileName, Params,DefaultDir: String): THandle;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
  end;

var
  TressShell: TTressShell;

implementation

uses DCliente,
     DKiosco,
     DGlobal,
     DCatalogos,
     DDiccionario,
     DReportes,
     DSistema,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaDialogo,
     ZAccesosMgr,
     ZetaAcercaDe,
     {$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor,
     {$else}
     FBuscaServidor,
     {$endif}
     FCarruseles,
     FEstilosBotones,
     FMarcos,
     FConfiguracion,
     FReseteaPass,
     FSistBitacoraKiosco,
     FGlobalBitacora,
     FSistConfiguracionCorreo,
     FEncuestas;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( self );
     dmKiosco := TdmKiosco.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     with Application do
     begin
          OnException := ManejaExcepcion;
          UpdateFormatSettings := FALSE;
     end;
     {
     // PENDIENTE: Cuando se programen derechos de acceso hay que poner esta bandera en TRUE;
     }
     ZAccesosMgr.SetValidacion( FALSE );
     WindowState:= wsMaximized;

    // DialogoImagenes := TDialogoImagenes.Create( Self );
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     //FreeAndNil( DialogoImagenes );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmReportes );
     FreeAndNil( dmSistema );
     FreeAndNil( dmKiosco );
     FreeAndNil( dmCliente );
     FreeAndNil( Global );
     ZetaRegistryCliente.ClearClientRegistry;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
     DZetaServerProvider.FreeAll;
{$endif}
end;

procedure TTressShell.RevisaDerechos;
begin
     Tag := 0;
end;

procedure TTressShell.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZetaDialogo.ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

procedure TTressShell.AbreEmpresa(const lDefault: Boolean);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with dmCliente do
        begin
             InitComparteCompany;
             SetCompany;
        end;
        DoOpenAll;
     finally
        Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        RevisaDerechos;
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error Al Abrir Datos', Error.Message, 0 );
                DoCloseAll;
           end;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     inherited DoCloseAll;
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     NotifyDataChange( Entidades, stNinguno );
end;

procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
begin
     dmCliente.NotifyDataChange( Entidades, Estado );
     dmCatalogos.NotifyDataChange( Entidades, Estado );
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
     Editar.Enabled := EmpresaAbierta;
end;

{ Activaci�n de Formas   }


function TTressShell.GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean): TBaseConsulta;
begin
     lNueva := FALSE;
     Result := TBaseConsulta( FindComponent( InstanceName ) );
     if not Assigned( Result ) then
     begin
          try
             Result := InstanceClass.Create( Self ) as TBaseConsulta;
             with Result do
             begin
                  Name := InstanceName;
                  Parent := Self.PanelConsulta;
             end;
             lNueva := TRUE;
          except
             Result := nil;
          end;
     end;
end;

procedure TTressShell.ActivaFormaPanel( InstanceClass: TBaseConsultaClass; const InstanceName: String; const IndexRights: Integer = 0 );
var
   Forma: TBaseConsulta;
   lNueva : Boolean;
begin
     Forma := GetForma( InstanceClass, InstanceName, lNueva );
     if Assigned( Forma ) then
     begin
          with Forma do
          begin
               if ( IndexDerechos = 0 ) then
                  IndexDerechos := IndexRights;
               if ZAccesosMgr.Revisa( IndexDerechos ) then
               begin
                    try
                       if lNueva then
                          DoConnect
                       else
                          Reconnect;
                       Show;
                       SetFormaActiva( Forma );
                    except
                          on Error: Exception do
                          begin
                               Application.HandleException( Error );
                               Free;
                          end;
                    end;
               end
               else
               begin
                    ZetaDialogo.zInformation( '� Atenci�n !', Format( 'No Tiene Derechos De Consulta Sobre %s', [ Caption ] ), 0 );
                    Free;
               end;
          end;
     end
     else
          zError( '� Error Al Crear Forma !', '� Ventana No Pudo Ser Creada !', 0 );
end;

procedure TTressShell.SetFormaActiva(Forma: TBaseConsulta);
begin
     if ( FFormaActiva <> Forma ) then
     begin
          if ( Assigned( FFormaActiva ) ) then
             FFormaActiva.DoDisconnect;  { Cerrar Datos de Forma Anterior }
          FFormaActiva := Forma;
          if ( Assigned( FFormaActiva ) ) then
          begin
               with FFormaActiva do
               begin
                    _E_Agregar.Enabled := CheckDerechos( K_DERECHO_ALTA );
                    _E_Borrar.Enabled := CheckDerechos( K_DERECHO_BAJA );
                    _E_Modificar.Enabled := CheckDerechos( K_DERECHO_CAMBIO );
                    _A_Imprimir.Enabled := CheckDerechos( K_DERECHO_IMPRESION );
                    HelpContext:= FFormaActiva.HelpContext;
                    SetFocus;
               end;
          end
          else
          begin
               _E_Agregar.Enabled := FALSE;
               _E_Borrar.Enabled := FALSE;
               _E_Modificar.Enabled := FALSE;
               _A_Imprimir.Enabled := FALSE;
          end;
     end;
end;

{ Action List }

procedure TTressShell._A_ImprimirFormaExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._A_ConfigurarImpresoraExecute(Sender: TObject);
begin
     EscogeImpresora.Execute;
end;

procedure TTressShell._A_ServidorExecute(Sender: TObject);
var
   lOk: Boolean;
   {$ifndef DOS_CAPAS}
   sComputerName: String;
   {$endif}
begin
     inherited;
     {$ifdef DOS_CAPAS}
     lOk := ZetaRegistryServerEditor.EspecificaComparte;
     {$else}
     sComputerName := ClientRegistry.ComputerName;
     with TFindServidor.Create( Application ) do
     begin
          try
             Servidor := sComputerName;
             if ( ShowModal = mrOk ) then
             begin
                  lOk := True;
                  sComputerName := Servidor;
             end
             else
                 lOk := False;
          finally
                 Free;
          end;
     end;
     {$endif}
     if lOk then
     begin
          Application.ProcessMessages;
          {$ifndef DOS_CAPAS}
          ClientRegistry.ComputerName := sComputerName;
          {$endif}
          ZetaDialogo.ZInformation( '� Atenci�n !',
                                    'Para Conectarse Al Nuevo Servidor' +
                                    CR_LF +
                                   'Es Necesario Volver A Entrar Al Sistema', 0 );
          Close;
          Application.Terminate;
     end;
end;

procedure TTressShell._A_SalirSistemaExecute(Sender: TObject);
begin
     Close;
end;

procedure TTressShell._H_ContenidoExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._H_GlosarioExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._H_UsoTecladoExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._H_ComoUsarAyudaExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._H_AcercaDeExecute(Sender: TObject);
begin
     with TZAcercaDe.Create( Self ) do
     begin
          try
             ShowModal;
          finally
             Free;
          end;
     end;
end;

procedure TTressShell._E_AgregarExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoInsert;
     end;
end;

procedure TTressShell._E_BorrarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoDelete;
     end;
end;

procedure TTressShell._E_ModificarExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoEdit;
     end;
end;

procedure TTressShell._E_RefrescarExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoRefresh;
     end;
end;

procedure TTressShell._A_ImprimirExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoPrint;
     end;
end;

procedure TTressShell._E_BuscarCodigoUpdate(Sender: TObject);
begin
     TAction( Sender ).Enabled := Assigned( FFormaActiva ) and FormaActiva.CanLookup;
end;


procedure TTressShell._A_ExportarExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FormaActiva.DoExportar;
     end;
end;

procedure TTressShell._E_BuscarCodigoExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoLookup;
     end;
end;

function TTressShell.ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;


procedure TTressShell.fcKioscoListItems0Click(OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
     ZAccesosMgr.SetValidacion( FALSE );
     try
        ActivaFormaPanel( TTOCarruseles, 'Carruseles' );
     finally
            ZAccesosMgr.SetValidacion( FALSE );
     end;
end;

procedure TTressShell.fcKioscoListItems1Click(OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
     ZAccesosMgr.SetValidacion( FALSE );
     try
        ActivaFormaPanel( TTOMarcos, 'MarcosInformacion' );
     finally
            ZAccesosMgr.SetValidacion( FALSE );
     end;
end;

procedure TTressShell.fcKioscoListItems2Click( OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
     ZAccesosMgr.SetValidacion( FALSE );
     try
        ActivaFormaPanel( TTOEstilosBotones, 'EstiloBotones' );
     finally
            ZAccesosMgr.SetValidacion( FALSE );
     end;
end;

procedure TTressShell.fcKioscoListItems3Click(  OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
     inherited;
      ZAccesosMgr.SetValidacion( FALSE );
     try
        ActivaFormaPanel( TTOEncuestas, 'Encuestas' );
     finally
            ZAccesosMgr.SetValidacion( FALSE );
     end;
end;

procedure TTressShell.fcKioscoListItems4Click( OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
     dmKiosco.EditarGlobalesKiosco;
end;

procedure TTressShell.ReseteaPasswordExecute(Sender: TObject);
begin
     if ( ReseteaPass = NIL ) then
        ReseteaPass := TReseteaPass.Create( self );

     ReseteaPass.ShowModal;

end;

procedure TTressShell.ConfiguraBitacoraExecute(Sender: TObject);
begin
     if ( GlobalBitacora = NIL ) then
        GlobalBitacora:= TGlobalBitacora.Create(self);

     GlobalBitacora.ShowModal;
end;

procedure TTressShell.fcKioscoListItems5Click( OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
      ZAccesosMgr.SetValidacion( FALSE );
     try
        ActivaFormaPanel( TConfiguracion, 'Configuracion' );
     finally
            ZAccesosMgr.SetValidacion( FALSE );
     end;
end;

procedure TTressShell.fcKioscoListItems6Click( OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
     ActivaFormaPanel( TSistBitacoraKiosco, 'SistBitacoraKiosco' );
end;

procedure TTressShell.fcKioscoListItems7Click(
  OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
      ActivaFormaPanel( TSistConfiguracionCorreo, 'SistConfiguracionCorreo' );
end;

end.
