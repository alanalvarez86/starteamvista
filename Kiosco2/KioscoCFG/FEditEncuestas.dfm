inherited EdicionEncuestas: TEdicionEncuestas
  Left = 348
  Top = 228
  Width = 550
  Height = 577
  BorderStyle = bsSizeable
  Caption = 'Encuesta'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 503
    Width = 534
    inherited OK: TBitBtn
      Left = 373
    end
    inherited Cancelar: TBitBtn
      Left = 452
    end
  end
  inherited PanelSuperior: TPanel
    Width = 534
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 534
    inherited ValorActivo2: TPanel
      Width = 208
    end
  end
  inherited Panel1: TPanel
    Width = 534
    Height = 118
    object Label1: TLabel
      Left = 65
      Top = 93
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
      FocusControl = ENC_NOMBRE
    end
    object Label2: TLabel
      Left = 52
      Top = 51
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pregunta:'
      FocusControl = ENC_NOMBRE
    end
    object TB_ELEMENTlbl: TLabel
      Left = 58
      Top = 28
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
      FocusControl = ENC_NOMBRE
    end
    object Label6: TLabel
      Left = 62
      Top = 4
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
      FocusControl = ENC_NOMBRE
    end
    object ZetaDBTextBox1: TZetaDBTextBox
      Left = 101
      Top = 1
      Width = 76
      Height = 17
      AutoSize = False
      Caption = 'ZetaDBTextBox1'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ENC_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ENC_NOMBRE: TDBEdit
      Left = 101
      Top = 24
      Width = 396
      Height = 21
      DataField = 'ENC_NOMBRE'
      DataSource = DataSource
      TabOrder = 0
    end
    object ENC_STATUS: TZetaDBKeyCombo
      Left = 101
      Top = 91
      Width = 145
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      ListaFija = lfStatusEncuesta
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'ENC_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object ENC_PREGUN: TDBMemo
      Left = 101
      Top = 48
      Width = 396
      Height = 39
      DataField = 'ENC_PREGUN'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  inherited PageControl: TPageControl
    Top = 169
    Width = 534
    Height = 334
    inherited Tabla: TTabSheet
      Caption = 'Configuraci'#243'n'
      inherited GridRenglones: TZetaDBGrid
        Top = 170
        Width = 497
        Height = 136
        TabOrder = 3
        Columns = <
          item
            Expanded = False
            FieldName = 'OP_ORDEN'
            ReadOnly = True
            Title.Caption = 'Orden'
            Width = 39
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OP_TITULO'
            Title.Caption = 'T'#237'tulo'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OP_DESCRIP'
            Title.Caption = 'Descripci'#243'n'
            Width = 280
            Visible = True
          end
          item
            Expanded = False
            Title.Caption = 'Refrescar'
            Visible = False
          end>
      end
      inherited Panel2: TPanel
        Top = 137
        Width = 526
        Height = 33
        TabOrder = 2
        inherited BBAgregar: TBitBtn
          Caption = 'Agregar Opci'#243'n'
        end
        inherited BBBorrar: TBitBtn
          Caption = 'Borrar Opci'#243'n'
        end
        inherited BBModificar: TBitBtn
          Caption = 'Modificar Opci'#243'n'
        end
      end
      object Panel3: TPanel
        Left = 497
        Top = 170
        Width = 29
        Height = 136
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
        object bArriba: TZetaSmartListsButton
          Left = 3
          Top = 49
          Width = 25
          Height = 25
          Hint = 'Subir Bot'#243'n'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333090333333333333309033333333333330903333333333333090333
            3333333333090333333333300009000033333330999999903333333309999903
            3333333309999903333333333099903333333333309990333333333333090333
            3333333333090333333333333330333333333333333033333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = bArribaClick
          Tipo = bsSubir
        end
        object bAbajo: TZetaSmartListsButton
          Left = 3
          Top = 73
          Width = 25
          Height = 25
          Hint = 'Bajar Boton'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            3333333333303333333333333309033333333333330903333333333330999033
            3333333330999033333333330999990333333333099999033333333099999990
            3333333000090000333333333309033333333333330903333333333333090333
            3333333333090333333333333309033333333333330003333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = bAbajoClick
          Tipo = bsBajar
        end
      end
      object GBVigencia: TGroupBox
        Left = 0
        Top = 0
        Width = 526
        Height = 58
        Align = alTop
        Caption = ' Vigencia '
        TabOrder = 0
        object Label3: TLabel
          Left = 64
          Top = 13
          Width = 28
          Height = 13
          Caption = 'Inicia:'
        end
        object Label4: TLabel
          Left = 50
          Top = 35
          Width = 41
          Height = 13
          Caption = 'Termina:'
        end
        object ENC_FECINI: TZetaDBFecha
          Left = 98
          Top = 9
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '04/Oct/11'
          Valor = 40820.000000000000000000
          DataField = 'ENC_FECINI'
          DataSource = DataSource
        end
        object ENC_FECFIN: TZetaDBFecha
          Left = 98
          Top = 33
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '04/Oct/11'
          Valor = 40820.000000000000000000
          DataField = 'ENC_FECFIN'
          DataSource = DataSource
        end
        object ENC_HORINI: TZetaDBHora
          Left = 222
          Top = 9
          Width = 42
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 1
          Text = '    '
          Tope = 24
          Valor = '    '
          DataField = 'ENC_HORINI'
          DataSource = DataSource
        end
        object ENC_HORFIN: TZetaDBHora
          Left = 222
          Top = 34
          Width = 42
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 3
          Text = '    '
          Tope = 24
          Valor = '    '
          DataField = 'ENC_HORFIN'
          DataSource = DataSource
        end
      end
      object GBOpciones: TGroupBox
        Left = 0
        Top = 58
        Width = 526
        Height = 79
        Align = alTop
        TabOrder = 1
        object Label5: TLabel
          Left = 27
          Top = 54
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Confirmaci'#243'n:'
          FocusControl = ENC_NOMBRE
        end
        object ENC_MSJCON: TZetaDBEdit
          Left = 97
          Top = 51
          Width = 352
          Height = 21
          TabOrder = 3
          Text = 'ENC_MSJC0N'
          DataField = 'ENC_MSJCON'
          DataSource = DataSource
        end
        object ENC_VOTOSE: TDBCheckBox
          Left = 271
          Top = 28
          Width = 97
          Height = 17
          Caption = 'Voto secreto'
          DataField = 'ENC_VOTOSE'
          DataSource = DataSource
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Visible = False
        end
        object ENC_OPCION: TDBCheckBox
          Left = 98
          Top = 28
          Width = 97
          Height = 17
          Caption = 'Voto obligatorio'
          DataField = 'ENC_OPCION'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object ENC_MRESUL: TDBCheckBox
          Left = 98
          Top = 11
          Width = 186
          Height = 17
          Caption = 'Mostrar resultados de la votaci'#243'n'
          DataField = 'ENC_MRESUL'
          DataSource = DataSource
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
    object Filtros: TTabSheet
      Caption = 'Filtros'
      ImageIndex = 2
      object GBKioscos: TGroupBox
        Left = 0
        Top = 193
        Width = 534
        Height = 175
        Align = alTop
        Caption = ' Kioscos Excluidos '
        TabOrder = 2
        object Label8: TLabel
          Left = 27
          Top = 17
          Width = 35
          Height = 13
          Caption = 'Kiosco:'
        end
        object Label9: TLabel
          Left = 14
          Top = 41
          Width = 48
          Height = 13
          Caption = 'Excluidos:'
        end
        object btnExcluir: TZetaSmartListsButton
          Left = 288
          Top = 14
          Width = 22
          Height = 21
          Hint = 'Excluir Kiosco'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            3333333333303333333333333309033333333333330903333333333330999033
            3333333330999033333333330999990333333333099999033333333099999990
            3333333000090000333333333309033333333333330903333333333333090333
            3333333333090333333333333309033333333333330003333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = btnExcluirClick
          Tipo = bsBajar
        end
        object btnRemoveExcluido: TSpeedButton
          Left = 286
          Top = 43
          Width = 24
          Height = 24
          Hint = 'Remover Excluido'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnRemoveExcluidoClick
        end
        object KioscoAdd: TZetaEdit
          Left = 68
          Top = 14
          Width = 213
          Height = 21
          TabOrder = 0
        end
        object KioscoExcluidos: TListBox
          Left = 68
          Top = 39
          Width = 213
          Height = 74
          ItemHeight = 13
          TabOrder = 1
        end
      end
      object GBEmpresas: TGroupBox
        Left = 0
        Top = 81
        Width = 526
        Height = 112
        Align = alTop
        Caption = ' Empresas a encuestar '
        TabOrder = 1
        object Label7: TLabel
          Left = 6
          Top = 17
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Seleccionar:'
          FocusControl = ENC_NOMBRE
        end
        object chkEmpresas: TCheckListBox
          Left = 69
          Top = 15
          Width = 460
          Height = 85
          OnClickCheck = chkEmpresasClickCheck
          Columns = 2
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object GBScripts: TGroupBox
        Left = 0
        Top = 0
        Width = 526
        Height = 81
        Align = alTop
        Caption = ' Script para filtrar empleados '
        TabOrder = 0
        object ENC_FILTRO: TDBMemo
          Left = 2
          Top = 15
          Width = 530
          Height = 64
          Align = alClient
          DataField = 'ENC_FILTRO'
          DataSource = DataSource
          TabOrder = 0
        end
      end
    end
    object Resultados: TTabSheet
      Caption = 'Resultados'
      ImageIndex = 3
      object GroupBox7: TGroupBox
        Left = 0
        Top = 0
        Width = 534
        Height = 41
        Align = alTop
        Caption = ' Totales '
        TabOrder = 0
        object Label10: TLabel
          Left = 60
          Top = 17
          Width = 33
          Height = 13
          Caption = 'Votos: '
        end
        object Label13: TLabel
          Left = 195
          Top = 17
          Width = 87
          Height = 13
          Caption = 'Opci'#243'n Ganadora:'
        end
        object VOTOS: TZetaDBTextBox
          Left = 96
          Top = 16
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'VOTOS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'VOTOS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object OPCION: TZetaDBTextBox
          Left = 288
          Top = 16
          Width = 169
          Height = 17
          AutoSize = False
          Caption = 'OPCION'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'OPCION'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GridResultados: TZetaDBGrid
        Left = 0
        Top = 41
        Width = 534
        Height = 269
        Align = alClient
        DataSource = dsRenglon
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = GridRenglonesTitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'OP_ORDEN'
            Title.Caption = 'Orden'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OP_TITULO'
            Title.Caption = 'T'#237'tulo'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OP_DESCRIP'
            Title.Caption = 'Descripci'#243'n'
            Width = 210
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VOTOS'
            Title.Caption = 'Votos'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OP_AVANCE'
            Title.Caption = 'Porcentaje'
            Width = 55
            Visible = True
          end>
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 412
    Top = 33
  end
  inherited dsRenglon: TDataSource
    Left = 415
  end
end
