inherited TOEditEstilosBotones: TTOEditEstilosBotones
  Left = 365
  Top = 229
  Caption = 'TOEditEstilosBotones'
  ClientHeight = 333
  ClientWidth = 436
  PixelsPerInch = 96
  TextHeight = 13
  object TB_CODIGOlbl: TLabel [0]
    Left = 55
    Top = 46
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
    FocusControl = KE_CODIGO
  end
  object TB_ELEMENTlbl: TLabel [1]
    Left = 51
    Top = 70
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
    FocusControl = KE_NOMBRE
  end
  object Label1: TLabel [2]
    Left = 64
    Top = 118
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Color:'
    FocusControl = KE_NOMBRE
  end
  object Label12: TLabel [3]
    Left = 6
    Top = 94
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Imagen del fondo:'
    FocusControl = KE_BITMAP
  end
  object bBuscaImagen: TSpeedButton [4]
    Left = 408
    Top = 89
    Width = 22
    Height = 22
    Glyph.Data = {
      36050000424D3605000000000000360400002800000010000000100000000100
      0800000000000001000000000000000000000001000000010000000000000101
      0100020202000303030004040400050505000606060007070700080808000909
      09000A0A0A000B0B0B000C0C0C000D0D0D000E0E0E000F0F0F00101010001111
      1100121212001313130014141400151515001616160017171700181818001919
      19001A1A1A001B1B1B001C1C1C001D1D1D001E1E1E001F1F1F00202020002121
      2100222222002323230024242400252525002626260027272700282828002929
      29002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F2F00303030003131
      3100323232003333330034343400353535003636360037373700383838003939
      39003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F3F00404040004141
      4100424242004343430044444400454545004646460047474700484848004949
      49004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F4F00505050005151
      5100525252005353530054545400555555005656560057575700585858005959
      59005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F5F00606060006161
      6100626262006363630064646400656565006666660067676700686868006969
      69006A6A6A0074647400944D9400BB32BB00E315E300F408F400FB03FB00FD01
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
      FE00FE00FE00FE00FE00FD01FE00FC03FE00FB05FE00F908FE00F50EFD00F015
      FC00E821FA00DD32F600D046F100BF61E900AF7EDF00A78DD500A19ECA00A1A6
      BF00A2AEB300AFAFAF00B5B5B500BBBBBB00C0C0C000C4C4C400C8C9C900CED0
      D000D3D6D600D6DDDD00D9E4E500DCE9EB00E0EFF000E3F2F400E5F5F600E5F6
      F800E6F7F900E5F8FA00E4F8FA00E1F8FA00DDF8FB00DAF8FB00D7F7FB00D4F7
      FB00CFF6FA00CBF6FA00C8F6FB00C4F6FB00BFF7FB00B6F8FC00B1F8FD00ACF8
      FD00A7F9FD00A4F9FE00A1F9FE009FF8FE009EF7FD009DF6FD009CF5FD009AF4
      FD0099F3FC0098F1FC0096F0FC0095ECFB0093EBFB0092EAFB008FE9FB008EE8
      FB008CE7FB008BE6FB0089E4FB0086E3FB0083E1FB0080DFFB007EDEFB007CDD
      FA007ADBFA0078DAFA0076D9FA0074D7F90072D6F90070D5F9006ED4F9006CD2
      F80069D0F70066CDF40062CAF0005FC8EE0059C4EA0053C0E6004CBBE30049B8
      E00043B5DD003FB1DB003AAED80031A8D30025A0CD001495C4000B8FC000088D
      BE00088DBE00078DBE00078DBE00078DBE00078DBE00078DBE0086FCFCFCFCFC
      FCFCFCFCFCFCFC868686FCF6EDDEEBEBEBEBEBEBEBEBF4F78686FCF0F5D5E8E8
      E8E8E8E8E8E8F3C4FC86FCE6FCC9E2E2E2E2E2E2E2E2F2C4FC86FCE1F7D5D7DD
      DEDEDDDEDEDDF1C4F786FCDEF2EEC9D8D8D8D8D8D8D8F0C4C4FCFCDAE3F6BBC4
      C4C4C4C4C4C4D5BBC2FCFCD4D4F7FCFCFCFCFCFCFCFCFCFCFCFCFCD0D1D1D0CF
      D1D0D1D0D1D1F9868686FCBBCCCCCCCCCCCCCCCCCCCCF986868686FCBBCBCBCB
      FCFCFCFCFCFC868686868686FCFCFCFC86868686868686868686868686868686
      8686868686868686868686868686868686868686868686868686868686868686
      8686868686868686868686868686868686868686868686868686}
    OnClick = bBuscaImagenClick
  end
  inherited PanelBotones: TPanel
    Top = 297
    Width = 436
    TabOrder = 6
    inherited OK: TBitBtn
      Left = 268
    end
    inherited Cancelar: TBitBtn
      Left = 353
    end
  end
  inherited PanelSuperior: TPanel
    Width = 436
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 436
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 110
    end
  end
  object KE_CODIGO: TZetaDBEdit [8]
    Left = 94
    Top = 42
    Width = 75
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
    ConfirmEdit = True
    DataField = 'KE_CODIGO'
    DataSource = DataSource
  end
  object KE_NOMBRE: TDBEdit [9]
    Left = 94
    Top = 66
    Width = 216
    Height = 21
    DataField = 'KE_NOMBRE'
    DataSource = DataSource
    TabOrder = 3
  end
  object KE_COLOR: TfcColorCombo [10]
    Left = 94
    Top = 114
    Width = 179
    Height = 21
    DataField = 'KE_COLOR'
    DataSource = DataSource
    AutoDropDown = True
    ColorDialog = ColorDialog
    ColorDialogOptions = [cdoEnabled, cdoPreventFullOpen, cdoSolidColor, cdoAnyColor]
    ColorListOptions.Font.Charset = DEFAULT_CHARSET
    ColorListOptions.Font.Color = clWindowText
    ColorListOptions.Font.Height = -11
    ColorListOptions.Font.Name = 'MS Sans Serif'
    ColorListOptions.Font.Style = []
    ColorListOptions.Options = [ccoShowSystemColors, ccoShowCustomColors, ccoShowStandardColors, ccoShowColorNames]
    DropDownCount = 8
    ReadOnly = False
    SelectedColor = 268435455
    TabOrder = 4
  end
  object GroupBox1: TGroupBox [11]
    Left = 49
    Top = 144
    Width = 265
    Height = 145
    Caption = ' Font '
    TabOrder = 5
    object Label2: TLabel
      Left = 7
      Top = 24
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
      FocusControl = KE_NOMBRE
    end
    object SpeedButton1: TSpeedButton
      Left = 232
      Top = 20
      Width = 21
      Height = 21
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333FFF33FFFFF33333300033000
        00333337773377777333333330333300033333337FF33777F333333330733300
        0333333377FFF777F33333333700000073333333777777773333333333033000
        3333333337FF777F333333333307300033333333377F777F3333333333703007
        33333333377F7773333333333330000333333333337777F33333333333300003
        33333333337777F3333333333337007333333333337777333333333333330033
        3333333333377333333333333333033333333333333733333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = SpeedButton1Click
    end
    object Label3: TLabel
      Left = 5
      Top = 48
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tama'#241'o:'
    end
    object Label4: TLabel
      Left = 20
      Top = 71
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Color:'
      FocusControl = KE_NOMBRE
    end
    object KE_F_NAME: TfcFontCombo
      Left = 50
      Top = 20
      Width = 179
      Height = 21
      ButtonStyle = cbsDownArrow
      ImmediateHints = False
      ReadOnly = False
      ShowFontHint = False
      TabOrder = 0
      TreeOptions = [tvoExpandOnDblClk, tvoExpandButtons3D, tvoHideSelection, tvoRowSelect, tvoShowButtons, tvoToolTips]
      FontSelections = fcRasterFonts
    end
    object KE_F_SIZE: TZetaDBNumero
      Left = 50
      Top = 44
      Width = 46
      Height = 21
      Mascara = mnEmpleado
      TabOrder = 1
      DataField = 'KE_F_SIZE'
      DataSource = DataSource
    end
    object KE_F_COLR: TfcColorCombo
      Left = 50
      Top = 67
      Width = 179
      Height = 21
      DataField = 'KE_F_COLR'
      DataSource = DataSource
      AutoDropDown = True
      ColorDialog = ColorDialog
      ColorDialogOptions = [cdoEnabled, cdoPreventFullOpen, cdoSolidColor, cdoAnyColor]
      ColorListOptions.Font.Charset = DEFAULT_CHARSET
      ColorListOptions.Font.Color = clWindowText
      ColorListOptions.Font.Height = -11
      ColorListOptions.Font.Name = 'MS Sans Serif'
      ColorListOptions.Font.Style = []
      ColorListOptions.Options = [ccoShowSystemColors, ccoShowStandardColors, ccoShowColorNames]
      DropDownCount = 8
      ReadOnly = False
      SelectedColor = 268435455
      TabOrder = 2
    end
    object KE_F_BOLD: TDBCheckBox
      Left = 50
      Top = 90
      Width = 97
      Height = 17
      Caption = 'Negrita'
      DataField = 'KE_F_BOLD'
      DataSource = DataSource
      TabOrder = 3
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object KE_F_ITAL: TDBCheckBox
      Left = 50
      Top = 106
      Width = 97
      Height = 17
      Caption = 'It'#225'lico'
      DataField = 'KE_F_ITAL'
      DataSource = DataSource
      TabOrder = 4
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object KE_F_SUBR: TDBCheckBox
      Left = 50
      Top = 122
      Width = 97
      Height = 17
      Caption = 'Subrayado'
      DataField = 'KE_F_SUBR'
      DataSource = DataSource
      TabOrder = 5
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  object KE_BITMAP: TDBEdit [12]
    Left = 94
    Top = 90
    Width = 310
    Height = 21
    DataField = 'KE_BITMAP'
    DataSource = DataSource
    TabOrder = 7
  end
  inherited DataSource: TDataSource
    Left = 229
    Top = 25
  end
  object ColorDialog: TColorDialog
    Options = [cdFullOpen, cdSolidColor, cdAnyColor]
    Left = 8
    Top = 24
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 8
  end
end
