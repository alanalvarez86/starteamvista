unit FLookupReporte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ImgList, Db, Grids, DBGrids,
  ZetaDBGrid, ZetaKeyCombo,
  ZetaCommonClasses,
  ZetaMessages,
  ZetaCommonLists;


type
  TFormaLookupReporte = class(TZetaDlgModal)
    PanelSuperior: TPanel;
    lbClasificacion: TLabel;
    BBusca: TSpeedButton;
    lbBusca: TLabel;
    CbTablas: TZetaKeyCombo;
    Panel1: TPanel;
    EBuscaNombre: TEdit;
    DBGrid: TZetaDBGrid;
    DataSource: TDataSource;
    ListaSmallImages: TImageList;
    Label7: TLabel;
    CM_CODIGO: TZetaKeyCombo;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure EBuscaNombreChange(Sender: TObject);
    procedure DBGridTitleClick(Column: TColumn);
    procedure OKClick(Sender: TObject);
    procedure CbTablasChange(Sender: TObject);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CM_CODIGOChange(Sender: TObject);
  private
    { Private declarations }
    FNumReporte : Integer;
    FNomReporte : String;
    FFiltro : string;
    //procedure Refresh;
    procedure FiltraReporte;
    procedure LimpiaGrid;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function GetClasificacion: eClasifiReporte;
   // function PuedeConsultar: Boolean;
  protected
    property ClasificacionActiva: eClasifiReporte read GetClasificacion;
  public
    { Public declarations }
    property NumReporte : Integer read FNumReporte;
    property NombreReporte : String read FNomReporte;
    property Filtro: string read FFiltro write FFiltro;
  end;

var
  FormaLookupReporte: TFormaLookupReporte;

implementation

uses //DReportes,
     DDiccionario,
     DCliente,
     DKiosco,
     ZetaDialogo,
     ZAccesosMgr,
     ZReportModulo,
     ZetaClientTools;

{$R *.DFM}

procedure TFormaLookupReporte.FormShow(Sender: TObject);
begin
     inherited;
     if DataSource.DataSet = NIL then
     begin
       DataSource.DataSet := dmKiosco.cdsSuscripReportes;

       {with dmDiccionario do
            CBTablas.ItemIndex:= GetPosClasifi( CBTablas.Items, ClasifiDefault );}

     end;

     CBTablas.ItemIndex:= dmDiccionario.GetPosClasifi( CBTablas.Items, dmDiccionario.ClasifiDefault );

     CBTablasChange(self);

     //Refresh;
end;

{$ifdef COMENTARIOS}
procedure TFormaLookupReporte.Refresh;
begin
     Screen.Cursor := crHourGlass;
     try
        with CbTablas do
             dmReportes.ClasifActivo := ClasificacionActiva;
        dmReportes.cdsSuscripReportes.Refrescar;
        dmReportes.cdsSuscripReportes.Filtered := FALSE;
        ZetaClientTools.OrdenarPor(dmReportes.cdsSuscripReportes,'RE_NOMBRE');
        LimpiaGrid;
        FiltraReporte;
     finally
            Screen.Cursor := crDefault;
     end;
end;
{$endif}
{   AP(20/Nov/2007): Ya no se usa
procedure TFormaLookupReporte.Refresh;
var
   oCursor : TCursor;
   FClasifiTemp: integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        if PuedeConsultar then
        begin
             with CbTablas do
             begin
                  with dmReportes do
                  begin
                       dmReportes.ClasifActivo := ClasificacionActiva;
                       Clasificaciones := Items; //CBTablas.ITems;
                       try
                          cdsSuscripReportes.Refrescar;
                          cdsSuscripReportes.Filtered := FALSE;
                          ZetaClientTools.OrdenarPor(cdsSuscripReportes,'RE_NOMBRE');
                       except
                             On E:Exception do
                             begin
                                  cdsSuscripReportes.EmptyDataset;
                                  ZError(Caption, E.Message, 0);
                             end;
                       end;
                  end;
             end;
             //FClasifiTemp:= CbTablas.ItemIndex;
        end;
        {else
        begin
             if ( FClasifiTemp > -1 ) then CbTablas.ItemIndex:= FClasifiTemp;
        end;
        LimpiaGrid;
        FiltraReporte;

     finally
            Screen.Cursor := oCursor;
     end;
end;  }

procedure TFormaLookupReporte.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H50051_Explorador_reportes;
     ListaSmallImages.ResourceLoad(rtBitmap, 'EXPLORERSMALL', clOlive);
     //dmDiccionario.GetListaClasifi( CBTablas.Items );
     with dmCliente do
     begin
          ListaCompanies( CM_CODIGO.Lista );
     end;
end;

procedure TFormaLookupReporte.FiltraReporte;
begin
     with dmKiosco.cdsSuscripReportes do
     begin
          Filtered := TRUE;
          if NOT BBusca.Down then
             Filter := '(RE_TIPO=0 or RE_TIPO=1)'
          else Filter := '(RE_TIPO=0 or RE_TIPO=1) AND '+
                         'UPPER(RE_NOMBRE) LIKE '+ chr(39)+'%'+
                         UpperCase(EBuscaNombre.Text) + '%' + chr(39);
     end;
     LimpiaGrid;
end;

procedure TFormaLookupReporte.BBuscaClick(Sender: TObject);
begin
  inherited;
  FiltraReporte;
end;

procedure TFormaLookupReporte.EBuscaNombreChange(Sender: TObject);
begin
  inherited;
  BBusca.Down := Length(EBuscaNombre.Text)>0;
  FiltraReporte;
end;

procedure TFormaLookupReporte.LimpiaGrid;
begin
     dbGrid.SelectedRows.Clear;
end;

procedure TFormaLookupReporte.DBGridTitleClick(Column: TColumn);
begin
     inherited;
     with dmKiosco do
          case Column.Index of
               1:ZetaClientTools.OrdenarPor(cdsSuscripReportes,DBGrid.Columns[3].FieldName);
               else ZetaClientTools.OrdenarPor(cdsSuscripReportes,Column.FieldName )
          end;
end;

procedure TFormaLookupReporte.OKClick(Sender: TObject);
begin
  inherited;
  with dmKiosco.cdsSuscripReportes do
  begin
    FNumReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
    FNomReporte := FieldByName( 'RE_NOMBRE' ).AsString;
    ModalResult := mrOK;
  end;
end;

procedure TFormaLookupReporte.CbTablasChange(Sender: TObject);
begin
     inherited;
//     Refresh;
end;

procedure TFormaLookupReporte.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     if ( DataCol = 0 ) then
        ListaSmallImages.Draw( DBGrid.Canvas, Rect.Left, Rect.Top + 1,
                               DataSource.DataSet.FieldByName('RE_TIPO').AsInteger )
end;

procedure TFormaLookupReporte.WMExaminar(var Message: TMessage);
begin
    OKClick( self );
end;

procedure TFormaLookupReporte.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  // Para que el explorador de reportes vuelva a hacer el Query
  dmKiosco.cdsSuscripReportes.SetDataChange;
  inherited;
end;

{ AP(20/Nov/2007): Ya no se usa
function TFormaLookupReporte.PuedeConsultar: Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho(dmDiccionario.GetDerechosClasifi(ClasificacionActiva), K_DERECHO_CONSULTA );
     if not Result then
        ZInformation( Caption, 'Usuario no Tiene Derecho para Consultar en esta Clasificación', 0 );
end;     }

function TFormaLookupReporte.GetClasificacion: eClasifiReporte;
begin
     with CBTablas do
          Result := eClasifiReporte(Items.Objects[ItemIndex]);
end;

procedure TFormaLookupReporte.CM_CODIGOChange(Sender: TObject);
begin
     inherited;
     with CM_CODIGO,Lista do
          dmKiosco.RefrescaReportes( Names[ItemIndex] );
end;


end.
