inherited EdicionCarruseles: TEdicionCarruseles
  Left = 389
  Top = 214
  Width = 512
  Height = 480
  BorderStyle = bsSizeable
  Caption = 'Carrusel'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 410
    Width = 504
    inherited OK: TBitBtn
      Left = 346
    end
    inherited Cancelar: TBitBtn
      Left = 425
    end
  end
  inherited PanelSuperior: TPanel
    Width = 504
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 504
    inherited ValorActivo2: TPanel
      Width = 178
    end
  end
  inherited Panel1: TPanel
    Width = 504
    Height = 150
    object Label1: TLabel
      Left = 118
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 114
      Top = 31
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
    end
    object Label3: TLabel
      Left = 79
      Top = 54
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'Marco principal:'
    end
    object Label4: TLabel
      Left = 106
      Top = 99
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pedir NIP:'
    end
    object Label5: TLabel
      Left = 53
      Top = 122
      Width = 101
      Height = 13
      Alignment = taRightJustify
      Caption = 'L'#237'mite de inactividad:'
    end
    object Label6: TLabel
      Left = 224
      Top = 122
      Width = 48
      Height = 13
      Caption = 'Segundos'
    end
    object Label7: TLabel
      Left = 45
      Top = 77
      Width = 109
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa para reportes:'
    end
    object KW_CODIGO: TZetaDBEdit
      Left = 158
      Top = 4
      Width = 73
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      Text = 'KW_CODIGO'
      ConfirmEdit = True
      DataField = 'KW_CODIGO'
      DataSource = DataSource
    end
    object KW_NOMBRE: TDBEdit
      Left = 158
      Top = 27
      Width = 177
      Height = 21
      DataField = 'KW_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
    object KW_SCR_DAT: TZetaDBKeyLookup
      Left = 158
      Top = 50
      Width = 300
      Height = 21
      TabOrder = 2
      TabStop = True
      WidthLlave = 60
      DataField = 'KW_SCR_DAT'
      DataSource = DataSource
    end
    object KW_GET_NIP: TDBCheckBox
      Left = 158
      Top = 97
      Width = 97
      Height = 17
      DataField = 'KW_GET_NIP'
      DataSource = DataSource
      TabOrder = 4
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object KW_TIM_DAT: TZetaDBNumero
      Left = 158
      Top = 118
      Width = 60
      Height = 21
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
      DataField = 'KW_TIM_DAT'
      DataSource = DataSource
    end
    object CM_CODIGO: TZetaDBKeyCombo
      Left = 158
      Top = 73
      Width = 276
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      DataField = 'CM_CODIGO'
      DataSource = DataSource
      LlaveNumerica = False
    end
  end
  inherited PageControl: TPageControl
    Top = 201
    Width = 504
    Height = 209
    inherited Tabla: TTabSheet
      inherited GridRenglones: TZetaDBGrid
        Width = 467
        Height = 152
        Columns = <
          item
            Expanded = False
            FieldName = 'KI_ORDEN'
            ReadOnly = True
            Title.Caption = 'Orden'
            Width = 39
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KI_URL'
            Title.Caption = 'P'#225'gina web'
            Width = 260
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KI_TIMEOUT'
            Title.Caption = 'Duraci'#243'n'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KI_REFRESH'
            Title.Caption = 'Refrescar'
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 496
      end
      object Panel3: TPanel
        Left = 467
        Top = 29
        Width = 29
        Height = 152
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object bArriba: TZetaSmartListsButton
          Left = 3
          Top = 49
          Width = 25
          Height = 25
          Hint = 'Subir Bot'#243'n'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333090333333333333309033333333333330903333333333333090333
            3333333333090333333333300009000033333330999999903333333309999903
            3333333309999903333333333099903333333333309990333333333333090333
            3333333333090333333333333330333333333333333033333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = bArribaClick
          Tipo = bsSubir
        end
        object bAbajo: TZetaSmartListsButton
          Left = 3
          Top = 73
          Width = 25
          Height = 25
          Hint = 'Bajar Boton'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            3333333333303333333333333309033333333333330903333333333330999033
            3333333330999033333333330999990333333333099999033333333099999990
            3333333000090000333333333309033333333333330903333333333333090333
            3333333333090333333333333309033333333333330003333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = bAbajoClick
          Tipo = bsBajar
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 412
    Top = 33
  end
  inherited dsRenglon: TDataSource
    Left = 415
  end
end
