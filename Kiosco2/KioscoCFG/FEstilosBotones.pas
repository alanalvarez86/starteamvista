unit FEstilosBotones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TTOEstilosBotones = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  TOEstilosBotones: TTOEstilosBotones;

implementation

uses dKiosco,
     ZetaBuscador;

{$R *.DFM}

{ TCarruseles }

procedure TTOEstilosBotones.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := 0;
     IndexDerechos := 0;//D_PORTAL_INFO_DOCUMENTOS;
end;

procedure TTOEstilosBotones.Connect;
begin
     with dmKiosco do
     begin
          cdsEstilosBotones.Conectar;
          DataSource.DataSet:= cdsEstilosBotones;
     end;
end;

procedure TTOEstilosBotones.Agregar;
begin
     dmKiosco.cdsEstilosBotones.Agregar;
end;

procedure TTOEstilosBotones.Borrar;
begin
     dmKiosco.cdsEstilosBotones.Borrar;
end;

procedure TTOEstilosBotones.Modificar;
begin
     dmKiosco.cdsEstilosBotones.Modificar;
     ZetaDBGrid.Repaint;
end;

procedure TTOEstilosBotones.Refresh;
begin
     dmKiosco.cdsEstilosBotones.Refrescar;
end;

procedure TTOEstilosBotones.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Folio', 'Estilos de botones', 'KE_CODIGO', dmKiosco.cdsEstilosBotones );
end;

end.
