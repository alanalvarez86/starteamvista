unit FGlobalKioscoEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGlobal, StdCtrls, Buttons, ExtCtrls, Mask, ZetaNumero;

type
  TGlobalKioscoEdit = class(TBaseGlobal)
    Label1: TLabel;
    Label2: TLabel;
    eURLDirectorioRaiz: TEdit;
    Label3: TLabel;
    eURLPantalla: TEdit;
    eURLValoresActivos: TEdit;
    LimiteImpr: TZetaNumero;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalKioscoEdit: TGlobalKioscoEdit;

implementation
uses
    ZGlobalTress,
    ZetaCommonTools;

const
     K_KIOSCO_DIRECTORIO_RAIZ_DEFAULT = 'http://NombreServidor/DirectorioVirtual/';
     K_KIOSCO_VALORES_ACTIVOS_URL_DEFAULT = 'ValoresActivos.aspx?Kiosco=%s&Empleado=%d&Empresa=%s';
     K_KIOSCO_PAGINA_PANTALLA_DEFAULT = 'Motor.aspx?Kiosco=@Kiosco&Forma=@Pantalla';
{$R *.dfm}

procedure TGlobalKioscoEdit.FormCreate(Sender: TObject);
begin
     eURLDirectorioRaiz.Tag := K_KIOSCO_DIRECTORIO_RAIZ;
     eURLValoresActivos.Tag := K_KIOSCO_VALORES_ACTIVOS_URL;
     eURLPantalla.Tag := K_KIOSCO_PAGINA_PANTALLA;
     LimiteImpr.Tag := K_KIOSCO_LIMITE_IMPRESIONES;
     inherited;
end;

procedure TGlobalKioscoEdit.FormShow(Sender: TObject);
begin
     inherited;
     if ZetaCommonTools.StrVacio(eURLDirectorioRaiz.Text) then
        eURLDirectorioRaiz.Text := K_KIOSCO_DIRECTORIO_RAIZ_DEFAULT;

     if ZetaCommonTools.StrVacio(eURLValoresActivos.Text) then
        eURLValoresActivos.Text := K_KIOSCO_VALORES_ACTIVOS_URL_DEFAULT;
        
     if ZetaCommonTools.StrVacio(eURLPantalla.Text) then
        eURLPantalla.Text := K_KIOSCO_PAGINA_PANTALLA_DEFAULT;
end;

end.
