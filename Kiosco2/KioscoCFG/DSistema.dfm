inherited dmSistema: TdmSistema
  OldCreateOrder = True
  Left = 280
  Top = 204
  inherited cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  inherited cdsImpresoras: TZetaClientDataSet
    Left = 112
  end
  object cdsEmpleadosKiosco: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Empleados'
    LookupDescriptionField = 'PRETTYNAME'
    LookupKeyField = 'CB_CODIGO'
    OnLookupKey = cdsEmpleadosKioscoLookupKey
    OnLookupSearch = cdsEmpleadosKioscoLookupSearch
    Left = 592
    Top = 72
  end
  object cdsEmpresasKiosco: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsEmpresasKioscoBeforePost
    AlAdquirirDatos = cdsEmpresasKioscoAlAdquirirDatos
    AlEnviarDatos = cdsEmpresasKioscoAlEnviarDatos
    AlModificar = cdsEmpresasKioscoAlModificar
    LookupName = 'Empresas Kiosco'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    OnGetRights = cdsEmpresasKioscoGetRights
    Left = 584
    Top = 136
  end
  object cdsNivel0: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsNivel0AlAdquirirDatos
    UsaCache = True
    LookupName = 'Confidencialidad'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 368
    Top = 152
  end
end
