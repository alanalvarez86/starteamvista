unit DSistema;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBaseSistema, ZetaServerDataSet, DB, DBClient, ZetaClientDataSet;

type
  TdmSistema = class(TdmBaseSistema)
    cdsEmpleadosKiosco: TZetaLookupDataSet;
    cdsEmpresasKiosco: TZetaLookupDataSet;
    cdsNivel0: TZetaLookupDataSet;
    procedure cdsEmpresasKioscoAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpleadosKioscoLookupKey(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter, sKey: String;
      var sDescription: String);
    procedure cdsEmpleadosKioscoLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsEmpresasKioscoGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEmpresasKioscoAlModificar(Sender: TObject);
    procedure cdsEmpresasKioscoAlEnviarDatos(Sender: TObject);
    procedure cdsEmpresasKioscoBeforePost(DataSet: TDataSet);
    procedure cdsNivel0AlAdquirirDatos(Sender: TObject);
    procedure cdsUsuariosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
  private
    { Private declarations }
    function BuildEmpresasKiosco( const sEmpresa: String) :OleVariant;
  public
    { Public declarations }
    function CargaListaSupervisores( Lista: TStrings; MostrarActivos: Boolean  ): String;
    procedure DescargaListaSupervisores( Lista: TStrings );
    procedure ConfigurarConfidencialidad;
  end;

var
  dmSistema: TdmSistema;

implementation

uses
    DCliente,
    ZetaCommonTools,
    ZetaCommonLists,
    ZetaBuscaEmpleado,
    ZetaCommonClasses,
    FSistEditConfiCorreo,
    FGridConfidencialidadCorreo,
    ZBaseEdicion,
    ZBaseGridEdicion;

{$R *.dfm}

procedure TdmSistema.cdsEmpresasKioscoAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     { Se hace la llamada a GetEmpresas de ServerSistema, debido a que puede grabar y se desea las claves decodificadas }
     with dmCliente do
          Self.cdsEmpresasKiosco.Data:= ServerSistema.GetEmpresas( Ord( tc3Datos ), dmCliente.GetGrupoActivo );
end;

procedure TdmSistema.cdsEmpleadosKioscoLookupKey( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
var
   Datos: OleVariant;
   sEmpresa: String;
begin
     sEmpresa := cdsEmpresasKiosco.FieldByName('CM_CODIGO').AsString;
     if strLleno( sEmpresa ) then
     begin
          lOk := dmCliente.Servidor.GetLookupEmpleado( BuildEmpresasKiosco( sEmpresa ), StrToIntDef( sKey, 0 ), Datos, Ord( eLookEmpGeneral ) );
          if lOk then
          begin
               with cdsEmpleadosKiosco do
               begin
                    Init;
                    Data := Datos;
                    sDescription := GetDescription;
               end;
          end;
     end;
end;

procedure TdmSistema.cdsEmpleadosKioscoLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
var
   sEmpresa: String;
begin
     sEmpresa := cdsEmpresasKiosco.FieldByName('CM_CODIGO').AsString;
     if strLleno( sEmpresa ) then
        lOk := ZetaBuscaEmpleado.BuscaEmpleadoDialogoDataSet( sFilter, sKey, sDescription, cdsEmpleadosKiosco, BuildEmpresasKiosco( sEmpresa ) );
end;

function TdmSistema.BuildEmpresasKiosco( const sEmpresa: String): OleVariant;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5; }

begin
     with cdsEmpresasKiosco do
     begin
          Locate( 'CM_CODIGO', sEmpresa, [loCaseInsensitive] );
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                          FieldByName( 'CM_USRNAME' ).AsString,
                          FieldByName( 'CM_PASSWRD' ).AsString,
                          Self.Usuario,
                          FieldByName( 'CM_NIVEL0' ).AsString,
                          FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;


function TdmSistema.CargaListaSupervisores(Lista: TStrings;
  MostrarActivos: Boolean): String;
begin
     //No borrar, se dejo por compatibilidad
end;

procedure TdmSistema.DescargaListaSupervisores(Lista: TStrings);
begin
     //No borrar, se dejo por compatibilidad
end;

procedure TdmSistema.cdsEmpresasKioscoGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
    inherited;
    lHasRights:= FALSE;
end;

procedure TdmSistema.cdsEmpresasKioscoAlModificar(Sender: TObject);
begin
     inherited;
     ShowFormaEdicion( SistEditConfiCorreo, TSistEditConfiCorreo);
end;

procedure TdmSistema.ConfigurarConfidencialidad;
begin
     ZBaseGridEdicion.ShowGridEdicion( GridConfidencialidadCorreo, TGridConfidencialidadCorreo, TRUE );
end;

procedure TdmSistema.cdsEmpresasKioscoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     inherited;
     ErrorCount := 0;
     with cdsEmpresasKiosco do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerSistema.GrabaEmpresas( Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmSistema.cdsEmpresasKioscoBeforePost(DataSet: TDataSet);
begin
     inherited;
     if not ValidaListaNumeros( DataSet.FieldByName('CM_KUSERS').AsString ) then
     begin
          DataBaseError( 'La lista de usuarios no es una lista v�lida' );
     end;

     if ( cdsNivel0.IsEmpty ) and ( zStrToBool ( cdsEmpresasKiosco.FieldByName('CM_KCONFI').AsString ) ) then
     begin
          cdsEmpresasKiosco.FieldByName('CM_KCONFI').AsString:= K_GLOBAL_NO;
     end;
end;



procedure TdmSistema.cdsNivel0AlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsNivel0.Data := ServerSistema.GetNivel0;
end;

procedure TdmSistema.cdsUsuariosGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     inherited;
     lHasRights:= FALSE;
end;

end.
