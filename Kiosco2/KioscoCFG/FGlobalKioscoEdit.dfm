inherited GlobalKioscoEdit: TGlobalKioscoEdit
  Left = 330
  Top = 252
  Caption = 'Globales para Kiosco'
  ClientHeight = 155
  ClientWidth = 641
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 104
    Top = 14
    Width = 116
    Height = 13
    Alignment = taRightJustify
    Caption = 'URL para directorio ra'#237'z:'
  end
  object Label2: TLabel [1]
    Left = 97
    Top = 38
    Width = 123
    Height = 13
    Alignment = taRightJustify
    Caption = 'URL para valores activos:'
  end
  object Label3: TLabel [2]
    Left = 131
    Top = 62
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'URL para pantalla:'
  end
  object Label4: TLabel [3]
    Left = 18
    Top = 85
    Width = 202
    Height = 13
    Caption = 'L'#237'mite Impresiones por Periodo de N'#243'mina:'
  end
  inherited PanelBotones: TPanel
    Top = 119
    Width = 641
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 473
    end
    inherited Cancelar: TBitBtn
      Left = 558
    end
  end
  object eURLDirectorioRaiz: TEdit
    Left = 226
    Top = 10
    Width = 386
    Height = 21
    TabOrder = 0
  end
  object eURLPantalla: TEdit
    Left = 226
    Top = 58
    Width = 386
    Height = 21
    TabOrder = 2
  end
  object eURLValoresActivos: TEdit
    Left = 226
    Top = 34
    Width = 386
    Height = 21
    TabOrder = 1
  end
  object LimiteImpr: TZetaNumero
    Left = 226
    Top = 82
    Width = 45
    Height = 21
    Mascara = mnDias
    TabOrder = 3
    Text = '0'
  end
end
