inherited Kiosko: TKiosko
  Left = 312
  Top = 251
  Caption = 'Configuraci'#243'n Del Kiosko'
  ClientWidth = 663
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 663
    inherited ValorActivo2: TPanel
      Width = 404
    end
  end
  object PageControl: TPageControl [1]
    Left = 0
    Top = 19
    Width = 663
    Height = 254
    ActivePage = Pantallas
    Align = alClient
    TabOrder = 1
    object Pantallas: TTabSheet
      Caption = 'Pantallas'
      object DBGridPantallas: TDBGrid
        Left = 0
        Top = 0
        Width = 655
        Height = 226
        Align = alClient
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'PN_CODIGO'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PN_DESCRIP'
            Title.Caption = 'Descripci'#243'n'
            Width = 317
            Visible = True
          end>
      end
    end
    object Secuencias: TTabSheet
      Caption = 'Secuencias'
      object DBGridSecuencias: TDBGrid
        Left = 0
        Top = 0
        Width = 655
        Height = 226
        Align = alClient
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SQ_CODIGO'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SQ_DESCRIP'
            Title.Caption = 'Descripci'#243'n'
            Width = 356
            Visible = True
          end>
      end
    end
  end
  object dsSecuencias: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 160
    Top = 104
  end
end
