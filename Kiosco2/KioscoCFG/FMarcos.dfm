inherited TOMarcos: TTOMarcos
  Left = 401
  Top = 245
  Caption = 'Marcos de informaci'#243'n'
  ClientWidth = 675
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 675
    inherited ValorActivo2: TPanel
      Width = 416
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 675
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'KS_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KS_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 145
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KS_PB_SIZE'
        Title.Caption = 'Panel botones'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KS_NOR_NOM'
        Title.Caption = 'Bot'#243'n normal'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KS_SEL_NOM'
        Title.Caption = 'Bot'#243'n seleccionado'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KS_PF_URL'
        Title.Caption = 'Encabezado'
        Visible = True
      end>
  end
end
