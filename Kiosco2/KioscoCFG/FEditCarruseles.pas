unit FEditCarruseles;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicionRenglon, DB, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls, ZetaEdit,
  ZetaKeyLookup, Mask, ZetaNumero, ZetaKeyCombo,
  ZetaCommonLists,
  DKiosco;

type
  TEdicionCarruseles = class(TBaseEdicionRenglon)
    KW_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    KW_NOMBRE: TDBEdit;
    Label3: TLabel;
    KW_SCR_DAT: TZetaDBKeyLookup;
    Label4: TLabel;
    KW_GET_NIP: TDBCheckBox;
    Label5: TLabel;
    KW_TIM_DAT: TZetaDBNumero;
    Label6: TLabel;
    Label7: TLabel;
    CM_CODIGO: TZetaDBKeyCombo;
    Panel3: TPanel;
    bArriba: TZetaSmartListsButton;
    bAbajo: TZetaSmartListsButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bArribaClick(Sender: TObject);
    procedure bAbajoClick(Sender: TObject);
  private
    { Private declarations }
    procedure SeleccionaPrimerColumna;
    procedure SeleccionaSiguienteRenglon;
    procedure SubirBajarBoton(eAccion: eSubeBaja);
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoCancelChanges;override;
    procedure KeyPress(var Key: Char);override;
    function PosicionaSiguienteColumna: Integer;override;
  public
    { Public declarations }
  end;

var
  EdicionCarruseles: TEdicionCarruseles;

implementation

uses
    ZetaDialogo,
    DCliente;

{$R *.dfm}

const
     PRIMER_COLUMNA = 0;

procedure TEdicionCarruseles.FormCreate(Sender: TObject);
 var
    i: integer;
begin
     inherited;
     IndexDerechos := 0; //D_CAT_CAPA_SESIONES;
     HelpContext:= 0;//H60647_Sesiones;
     FirstControl := KW_CODIGO;

     with dmCliente do
     begin
          ListaCompanies( CM_CODIGO.Lista );
     end;

     with dmKiosco do
     begin
          KW_SCR_DAT.LookupDataset := cdsMarcosInformacion;
     end;

     for i:= 0 to PageControl.PageCount - 1 do
     begin
          PageControl.Pages[i].TabVisible := FALSE;
     end;


end;

procedure TEdicionCarruseles.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePAge := Tabla;
end;

procedure TEdicionCarruseles.Connect;
begin
     with dmKiosco do
     begin
          DataSource.DataSet:= cdsCarruseles;
          {if ( cdsCarruseles.State = dsInsert ) then
             cdsCarruselesInfo.EmptyDataSet
          else}
          cdsCarruselesInfo.Refrescar;
          dsRenglon.DataSet := cdsCarruselesInfo;
     end;
end;

procedure TEdicionCarruseles.DoCancelChanges;
begin
     dmKiosco.cdsCarruselesInfo.CancelUpdates;
     inherited DoCancelChanges;
end;

procedure TEdicionCarruseles.SeleccionaSiguienteRenglon;
begin
     with dmKiosco.cdsCarruselesInfo do
     begin
          Next;
          if Eof then
          begin
               SeleccionaPrimerColumna;
               Agregar;
          end
          else
              GridRenglones.SelectedIndex := 0;
     end;
end;


procedure TEdicionCarruseles.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
               if ( GridRenglones.SelectedIndex = 0 ) then
                  SeleccionaSiguienteRenglon;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   SeleccionaPrimerColumna;
              end;
     end
     else if ActiveControl.TabStop then
          inherited KeyPress( Key );
end;

function TEdicionCarruseles.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with GridRenglones do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
               Result := 0
          else
              Result := i;
     end;
end;

procedure TEdicionCarruseles.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := GridRenglones;
     GridRenglones.SelectedField := GridRenglones.Columns[ PRIMER_COLUMNA ].Field;   // Posicionar en la primer columna
end;


procedure TEdicionCarruseles.bArribaClick(Sender: TObject);
begin
     inherited;
     SubirBajarBoton(eSubir);
end;

procedure TEdicionCarruseles.bAbajoClick(Sender: TObject);
begin
     inherited;
     SubirBajarBoton(eBajar);
end;

procedure TEdicionCarruseles.SubirBajarBoton(eAccion: eSubeBaja);
const
     K_PRIMERO = 1;
var
   iPosActual, iNuevaPos: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmKiosco do
        begin
             iPosActual := dmKiosco.cdsCarruselesInfo.RecNo;
             case( eAccion ) of
                   eSubir: begin
                                if( iPosActual = K_PRIMERO )then
                                    ZetaDialogo.ZError( self.Caption, '� Es el inicio de la lista de p�ginas !', 0 )
                                else
                                begin
                                     iNuevaPos := iPosActual - 1;
                                     CambiaOrdenPaginas( iPosActual, iNuevaPos );
                                end;
                           end;
                   eBajar: begin
                                if( iPosActual = cdsCarruselesInfo.RecordCount  )then
                                    ZetaDialogo.ZError( self.Caption, '� Es el final de la lista de p�ginas !', 0 )
                                else
                                begin
                                     iNuevaPos := iPosActual + 1;
                                     CambiaOrdenPaginas( iPosActual, iNuevaPos );
                                end;
                           end;
             else
                 ZetaDialogo.ZError( self.Caption, '� Operaci�n no v�lida !', 0 )
             end;
        end;
     finally
             Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;
end.
