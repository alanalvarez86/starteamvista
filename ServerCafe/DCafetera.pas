unit DCafetera;

interface

{.$DEFINE DEBUGSENTINEL}  //(@am): Se pasa esta directiva a la config. del proyecto

uses
  FAutoServer, FAutoClasses, FCoffeShop, ZetaCommonClasses, DZetaServerProvider, DServerBase, ZetaCommonLists, ServidorSOAPIntf,
  SysUtils, Data.DB, System.Classes, Datasnap.DBClient,
  Chilkat_v9_5_0_TLB, Windows;

type
  TdmCafetera = class(TdmServerBase)
    adsTotales: TClientDataSet;
    adsDetalle: TClientDataSet;
    adsIdInvita: TClientDataSet;
    adsAcceso: TClientDataSet;
    adsHuellas: TClientDataSet;
    adsReportaHuellas: TClientDataSet;
    adsReiniciaHuellas: TClientDataSet;
    adsAccesoAC_KIND: TIntegerField;
    adsAccesoAC_LABEL: TStringField;
    adsAccesoAC_VALUE: TStringField;
    adsAccesoAC_TIPO: TIntegerField;
    adsAccesoAC_OK: TBooleanField;
    adsAccesoAC_BLOB: TBlobField; // BIOMETRICO
    adsConfiguracion: TClientDataSet;
    adsCalendario: TClientDataSet; 
    adsImportarConfig: TClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

    // procedure PaletitasActions0Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
    procedure AutorizarChecadaComida(DS: TClientDataSet; var ClientParams: TZetaParams);
    // procedure PaletitasActions1Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
    procedure LeeChecadasCafeteria(ClientParams: TZetaParams);
    // procedure PaletitasActions2Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
    procedure LeerListaInvitadores(ClientParams: TZetaParams);
    // procedure PaletitasActions3Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
    procedure ValidarAcceso(DS: TClientDataSet; ClientParams: TZetaParams);
    // procedure PaletitasActions4Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
    procedure ObtenerHuellas(ClientParams: TZetaParams);
    // procedure PaletitasActions5Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
    procedure BorrarRelacionHuellas(ClientParams: TZetaParams);
    // procedure PaletitasActions6Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
    procedure ReportarHuellas(ClientParams: TZetaParams);

    procedure GetCambiosConfig(ClientParams: TZetaParams); // US 13921
    procedure ActualizarConfig(ClientParams: TZetaParams); // US 13921
    procedure ImportarConfig(ClientParams: TZetaParams); //14004
    procedure BuscaCafeteria(ClientParams: TZetaParams); // US 14639
    procedure VerificarSincronizacion(ClientParams: TZetaParams);

    procedure ObtenerLog(Params: TZetaParams);
    procedure ReiniciarReglas(Params: TZetaParams);
    procedure GetParametros(Params: TZetaParams);
    procedure SetParametros(Params: TZetaParams);
    procedure CodedStream(Params: TSOAPDataSet);
  private
    { Private declarations }
    oZetaProvider : TdmZetaServerProvider;
    oAutoServer   : TAutoServer;
    FEmpleadoCtr  : Word;
    FList         : TList;
    FEmpresa      : TCafeteria;
    FImportandoFueraDeLinea : Boolean;
    FChecadaAcceso: TChecadaAcceso;
    FChecadaComida: TChecadaComida;
    FAutoCafeteria: Boolean;
    FAutoAccesos  : Boolean;
    FQueryEmpID   : TZetaCursor;
    {$IFNDEF INTERBASE}
    FQueryEmpBio: TZetaCursor; // BIOMETRICO
    {$ENDIF}
    FOptimizacionCafeteria: Boolean;
    FOptimizacionCaseta   : Boolean;
    {$IFNDEF INTERBASE}
    FBitacora        : TZetaCursor;
    FProviderBitacora: TdmZetaServerProvider;
    {$ENDIF}
    // Encriptaci�n.
    FCrypt: TChilkatCrypt2;
    FLockCrypt: TRTLCriticalSection;
    function GetEmpresa(Index: Integer): TCafeteria;
    function HayOptimizacion(const eTipo: eLstDispositivos): Boolean;
    procedure BuildListaInvitadores;
    procedure GetDispositivo(oCafeteria: TCafeteria; ListaDispositivo: TStringList; const eTipo: eLstDispositivos);
    procedure CheckStructure(Source, Target: TDataSet);
  protected
    { Protected declarations }
    property ChecadaAcceso        : TChecadaAcceso read FChecadaAcceso;
    property ChecadaComida        : TChecadaComida read FChecadaComida;
    property OptimizacionCafeteria: Boolean read FOptimizacionCafeteria;
    property OptimizacionCaseta   : Boolean read FOptimizacionCaseta;
    property ImportandoFueraDeLinea   : Boolean read FImportandoFueraDeLinea write FImportandoFueraDeLinea;
    function AddEmpresa: TCafeteria;
    function AutorizaComida: Boolean;
    function FindEmpresa(Checada: TChecada): TCafeteria;
    function GetQuery: TDataSet; override;
    function GetScript(const iScript: Integer): string; override;
    function SetEmpresaActiva(Checada: TChecada): Boolean;
    function UsaAccesos: Boolean;
    function UsaCafeteria: Boolean;
    procedure AutorizaAcceso(Dataset: TDataSet; var lExisteEmp: Boolean);
    procedure Clear;
    procedure DelEmpresa(const Index: Integer);
    procedure EscribeAcceso;
    procedure EscribeComida;
    procedure LeeChecadas(const dInicial, dFinal: TDateTime; const sTipos, sReloj: string; const lTotales: Boolean;
      var sDetalle: string);
    procedure SetTipoChecadaAcceso;
    procedure EscribeBitacoraError(oChecadaComida: TChecadaComida; const MsgID: Integer; const sChecada: string;
      const sMensaje: string = VACIO; const iTiempo: Integer = 0);
  public
    { Public declarations }
    // Encriptaci�n.
    property Crypt        : TChilkatCrypt2 read FCrypt write FCrypt;
    property Empresa[Index: Integer]: TCafeteria read GetEmpresa;
    property AutoCafeteria          : Boolean read FAutoCafeteria;
    property AutoAccesos            : Boolean read FAutoAccesos;
    property ZetaProvider           : TdmZetaServerProvider read oZetaProvider;
    function Count: Integer;
    function ImportaAccesos(var sDatos: string): Boolean;
    function ImportaCafeteria(var sDatos: string; MsgID: Integer): Boolean;
    function Init(CallBack: TCallBack): Boolean;
    function GetListaEmpresas: Boolean;
    {$IFNDEF INTERBASE}
    function GetHuellas(sComputer, sIdCafeteria: string; iTipoEstacion: integer): Boolean; // BIOMETRICO
    {$ENDIF}
    procedure Close;
    procedure ConnectQuery(const sSQL: string); override;
    procedure Connect; override;
    procedure Disconnect; override;
    procedure Llena(Lista: TStrings);
    procedure IniciarZonaCriticaCrypt;
    procedure TerminarZonaCriticaCrypt;
    function Encrypt (sValor: widestring): WideString;
    function Decrypt (sValor: widestring): WideString;
  end;

var
  dmCafetera: TdmCafetera;

implementation

uses
  ZetaCommonTools, ZetaServerTools,
  {$IFDEF DEBUGSENTINEL}
  ZetaSQLBroker, ZetaLicenseMgr,
  {$ENDIF}
  FCafetera, CafeteraConsts, CafeteraUtils, DServicio;

type
  eStatusChecada = (echOK, echError);

const
  Q_EMP_ID           = 1;
  Q_INVITA_ID        = 2;
  Q_LEE_EMPRESAS     = 3;
  Q_AGREGA_BITACORA  = 4;
  Q_GET_DISPOSITIVOS = 5;
  Q_TOTAL_DISPOSIT   = 6;
  Q_CONFIG_CAF       = 10; // US 13921
  Q_CALEND_CAF       = 11; // US 13921
  Q_UPD_CONFIG       = 12; // US 13921
  Q_GET_DISP         = 13; // US 14004
  Q_GET_CONFIG       = 14; // US 14004
  Q_INSERTAR_DISP    = 15; // US 14004
  Q_INSERTAR_CONFIG  = 16; // US 14004
  Q_INSERTAR_CALEND  = 17; // US 14004
  Q_ACTUALIZAR_CONFIG = 18; // US 14628
  Q_BORRAR_CALEND    = 19; // US 14628
  Q_OBTENER_SINC     = 20;
  {$IFNDEF INTERBASE}
  Q_EMP_BIO                = 7; // BIOMETRICO
  Q_HUELLAS                = 8; // BIOMETRICO
  Q_HUELLA_REPORTA_INSERTA = 9;
  {$ENDIF}
  K_ACTION_VALIDA_CAFE   = 0;
  K_ACTION_VALIDA_ACCESO = 3;

  {$R *.DFM}
  { ********** TdmComparte ************ }

procedure TdmCafetera.DataModuleCreate(Sender: TObject);
var
  I: Integer;
begin
  inherited;

  FImportandoFueraDeLinea := FALSE;

  oZetaProvider := TdmZetaServerProvider.Create(Self);
  with oZetaProvider do begin
    FQueryEmpID := CreateQuery;
  end;
  {$IFNDEF INTERBASE}
  // BIOMETRICO
  // oZetaProvider := TdmZetaServerProvider.Create(Self);   //Comentado RCM
  with oZetaProvider do begin
    FQueryEmpBio := CreateQuery;
  end;
  {$ENDIF}
  FList := TList.Create;

  FChecadaAcceso := TChecadaAcceso.Create;
  with FChecadaAcceso do begin
    Provider   := oZetaProvider;
    QueryEmpID := FQueryEmpID;
  end;

  FChecadaComida := TChecadaComida.Create;
  with FChecadaComida do begin
    Provider   := oZetaProvider;
   QueryEmpID := FQueryEmpID;
    {$IFNDEF INTERBASE}
   QueryEmpBio := FQueryEmpBio; // BIOMETRICO
    {$ENDIF}
  end;

  oAutoServer := TAutoServer.Create;
  with oAutoServer do begin
    {$IFDEF INTERBASE}
    { GA: Se quit� porque 2.0.90 resolver� la necesidad de tener servidor }
    { de Cafeter�a compatible con 1.3 y 2.0 }
    {
      VersionMinima := '1.3'; }
    {
      ER: Se cambi� por 'atProfesional' por que solo aplica en versi�n profesional
      AppType := atAmbas; }
    AppType := atProfesional;
    SQLType := engInterbase;
    {$ENDIF}
    {$IFDEF MSSQL}
    AppType := atCorporativa;
    SQLType := engMSSQL;
    {$ENDIF}
  end;
  FEmpleadoCtr := 0;

  try
    {$IFNDEF INTERBASE}
    FProviderBitacora               := nil; 
    FBitacora                       := nil; 
    {$ENDIF}
  except
    on e: Exception do begin
      raise;
    end;
  end;

  for I := 0 to ComponentCount - 1 do
    if Components[I].InheritsFrom(TCustomClientDataSet) then
      with TCustomClientDataSet(Components[I]) do begin
        if (FieldDefs.Count = 0) and (Fields.Count = 0) then begin
          FieldDefs.Add('DUMMY', ftInteger);
        end;
        CreateDataSet;
        LogChanges := False;
        Close;
      end;
  adsAcceso.CreateDataSet;

  // Encriptaci�n.
  Crypt := TChilkatCrypt2.Create(Self);
  FCrypt.UnlockComponent('TRESSCCrypt_pnFywPZSRHjw');
  FCrypt.SecretKey := FCrypt.GenerateSecretKey('cafeteria');

  InitializeCriticalSection(FLockCrypt);
end;

procedure TdmCafetera.DataModuleDestroy(Sender: TObject);
begin
  Close;
  // Liberar Bitacora
  {$IFNDEF INTERBASE}
  FreeAndNil(FBitacora);
  FreeAndNil(FProviderBitacora);
  {$ENDIF}
  FreeAndNil(FQueryEmpID);
  {$IFNDEF INTERBASE}
  FreeAndNil(FQueryEmpBio); // BIOMETRICO
  {$ENDIF}
  FreeAndNil(oAutoServer);
  FreeAndNil(FChecadaComida);
  FreeAndNil(FChecadaAcceso);
  FreeAndNil(FList);
  FreeAndNil(oZetaProvider);
  FreeAndNil (FCrypt);
  DeleteCriticalSection(FLockCrypt);
  inherited;
end;

function TdmCafetera.GetScript(const iScript: Integer): string;
begin
  case iScript of
    Q_EMP_ID:
      Result := 'select A.CB_CODIGO, A.IV_CODIGO, A.CM_CODIGO, B.CM_DIGITO ' + 'from EMP_ID A ' +
        'left outer join COMPANY B on ( B.CM_CODIGO = A.CM_CODIGO ) ' + 'where ( A.ID_NUMERO = :GAFETE ) ' +
        'and ( B.CM_DIGITO <> '''' ) and ( B.CM_DIGITO is not NULL )';
    {$IFDEF INTERBASE}
    Q_INVITA_ID:
      Result := 'select ID_NUMERO from EMP_ID where ( IV_CODIGO > 0 )';
    {$ELSE}
    Q_INVITA_ID:
      Result := 'SELECT  CONVERT(varchar(30), ID_NUMERO) AS ID_NUMERO, IV_TIPO FROM V_INVITADORES';
    // Invitador: [ 1 Proximidad | 2 Biometrico ] Modificacion de BIOMETRICO
    {$ENDIF}
    Q_LEE_EMPRESAS:
      Result := 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_DATOS, CM_DIGITO, CM_USACAFE, CM_USACASE ' +
        'from COMPANY where ( CM_USACASE = ''%s'' ) or ( CM_USACAFE = ''%s'' )';
    Q_AGREGA_BITACORA:
      Result := 'Insert into BITCAFE ' + '(CB_CODIGO,IV_CODIGO,BC_EMPRESA,BC_CREDENC,BC_FECHA,BC_HORA,BC_MANUAL, ' +
        'BC_RELOJ,BC_TIPO,BC_COMIDAS,BC_EXTRAS,BC_REG_EXT,CL_CODIGO,BC_TGAFETE, ' +
        'BC_TIEMPO,BC_STATUS,BC_MENSAJE, BC_RESPUES, BC_CHECADA) ' + 'Values' +
        '(:CB_CODIGO,:IV_CODIGO,:BC_EMPRESA,:BC_CREDENC,:BC_FECHA,:BC_HORA,:BC_MANUAL, ' +
        ':BC_RELOJ,:BC_TIPO,:BC_COMIDAS,:BC_EXTRAS,:BC_REG_EXT,:CL_CODIGO,:BC_TGAFETE, ' +
        ':BC_TIEMPO,:BC_STATUS,:BC_MENSAJE,:BC_RESPUES, :BC_CHECADA)';
    Q_GET_DISPOSITIVOS:
      Result := 'select * from DISXCOM where (CM_CODIGO = ''%s'' and DI_TIPO = %d)';
    Q_TOTAL_DISPOSIT:
      Result := 'select count(*) as Total from DISXCOM where DI_TIPO = %d';
    {$IFNDEF INTERBASE}
    Q_EMP_BIO:
      Result := 'SELECT E.CB_CODIGO, E.IV_CODIGO, E.CM_CODIGO, C.CM_DIGITO, E.ID_NUMERO ' +
        'FROM EMP_BIO AS E left outer join COMPANY AS C ON E.CM_CODIGO = C.CM_CODIGO ' +
        'WHERE (E.ID_NUMERO = :GAFETE) AND (C.CM_DIGITO <> '''') AND (C.CM_DIGITO IS NOT NULL)';
    // BIOMETRICO
    Q_HUELLAS:
      Result := 'SELECT V.HU_ID, V.ID_NUMERO, V.CM_CODIGO, V.CB_CODIGO, V.HU_INDICE, V.HU_HUELLA, V.IV_CODIGO, V.HU_TIMEST, V.TIPO, C.CH_CODIGO ' +
                'FROM V_HUELLAS AS V LEFT OUTER JOIN COM_HUELLA AS C ON V.HU_ID = C.HU_ID AND C.CH_CODIGO = ''%s'' WHERE (C.CH_CODIGO IS NULL) %s' +
                // DES SP: 13301 EP III - Recarga de huellas
                // DES US #13279: Obtener huellas solo de empleados activos.
                // Para esta consulta se utiliza la nueva vista (versi�n 2016): VEMPL_BAJA.
                // Correcci�n de bug #14344: BRADY - Posterior a la actualizaci�n 2016, ya no se reconocen todas las huellas en cafeter�a.
                //    Se agrega filtro de empresa.
                ' AND V.CB_CODIGO NOT IN (SELECT V2.CB_CODIGO FROM VEMPL_BAJA V2 WHERE V.CM_CODIGO = V2.CM_CODIGO)';
    Q_HUELLA_REPORTA_INSERTA:
      Result := 'exec SP_REPORTAHUELLA ''%s'', %s';
    {$ENDIF}
    Q_CONFIG_CAF: // US 13921
      Result := 'SELECT CF_NOMBRE, DI_TIPO, CF_TIP_COM, CF_FOTO, CF_COM, CF_SIG_COM, CF_TECLADO, CF_DEF_COM, ' +
             'CF_DEF_CRE, CF_PRE_TIP, CF_PRE_QTY, CF_BANNER, CF_VELOCI, CF_T_ESP, CF_CALEND, CF_GAFETE, CF_CLAVE, CF_A_PRINT, CF_A_CANC, ' +
             'CF_TCOM_1, CF_TCOM_2, CF_TCOM_3, CF_TCOM_4, CF_TCOM_5, CF_TCOM_6, CF_TCOM_7, CF_TCOM_8, CF_TCOM_9, CF_REINICI, CF_CHECK_S, ' +
             'CF_SINC, CF_CAMBIOS ' +
             'FROM CAF_CONFIG WHERE (CF_NOMBRE = ''%s'' AND DI_TIPO = %d)';
    Q_CALEND_CAF: // US 13921
      Result := 'SELECT CF_NOMBRE, DI_TIPO, CC_REG_ID, HORA, ACCION, CONTADOR, SEMANA, SYNC ' +
             'FROM CAF_CALEND WHERE (CF_NOMBRE = ''%s'' AND DI_TIPO = %d)';
    Q_UPD_CONFIG: Result := 'UPDATE CAF_CONFIG set CF_CAMBIOS = ''N'' WHERE (CF_NOMBRE = ''%s'' ) and (DI_TIPO = %d)'; // US 13921
    Q_INSERTAR_DISP:
      Result := 'INSERT INTO DISPOSIT (DI_NOMBRE, DI_TIPO) VALUES (:DI_NOMBRE, :DI_TIPO)';
    Q_GET_DISP:
      Result := 'SELECT * FROM DISPOSIT WHERE (DI_NOMBRE = ''%s'' ) and (DI_TIPO = %d)';
    Q_GET_CONFIG:
      Result := 'SELECT * FROM CAF_CONFIG WHERE (CF_NOMBRE = ''%s'' ) and (DI_TIPO = %d)';
    Q_INSERTAR_CONFIG:
      Result := 'INSERT INTO CAF_CONFIG '  +
             '(CF_NOMBRE, DI_TIPO, CF_TIP_COM, ' +
             'CF_A_PRINT, CF_A_CANC, CF_FOTO, CF_COM, CF_SIG_COM, CF_TECLADO, CF_DEF_COM, CF_DEF_CRE, ' +
             'CF_PRE_TIP, CF_PRE_QTY, CF_BANNER, CF_VELOCI, CF_T_ESP, CF_CHECK_S, ' +
             'CF_TCOM_1, CF_TCOM_2, CF_TCOM_3, CF_TCOM_4, CF_TCOM_5, CF_TCOM_6, CF_TCOM_7, CF_TCOM_8, CF_TCOM_9, ' +
             'CF_GAFETE, CF_CLAVE, CF_REINICI) VALUES ' +
             '(:CF_NOMBRE, :DI_TIPO, :CF_TIP_COM, ' +
             ':CF_A_PRINT, :CF_A_CANC, :CF_FOTO, :CF_COM, :CF_SIG_COM, :CF_TECLADO, :CF_DEF_COM, :CF_DEF_CRE, ' +
             ':CF_PRE_TIP, :CF_PRE_QTY, :CF_BANNER, :CF_VELOCI, :CF_T_ESP, :CF_CHECK_S, ' +
             ':CF_TCOM_1, :CF_TCOM_2, :CF_TCOM_3, :CF_TCOM_4, :CF_TCOM_5, :CF_TCOM_6, :CF_TCOM_7, :CF_TCOM_8, :CF_TCOM_9, ' +
             ':CF_GAFETE, :CF_CLAVE, :CF_REINICI)';
    Q_ACTUALIZAR_CONFIG:
      Result := 'UPDATE CAF_CONFIG '  +
             'SET CF_TIP_COM = :CF_TIP_COM, CF_A_PRINT = :CF_A_PRINT, CF_A_CANC = :CF_A_CANC,' +
             'CF_FOTO = :CF_FOTO, CF_COM = :CF_COM, CF_SIG_COM = :CF_SIG_COM, CF_TECLADO = :CF_TECLADO, CF_DEF_COM = :CF_DEF_COM, CF_DEF_CRE = :CF_DEF_CRE, ' +
             'CF_PRE_TIP = :CF_PRE_TIP, CF_PRE_QTY = :CF_PRE_QTY, CF_BANNER = :CF_BANNER, CF_VELOCI = :CF_VELOCI, CF_T_ESP = :CF_T_ESP, CF_CHECK_S = :CF_CHECK_S, ' +
             'CF_TCOM_1 = :CF_TCOM_1, CF_TCOM_2 = :CF_TCOM_2, CF_TCOM_3 = :CF_TCOM_3, CF_TCOM_4 = :CF_TCOM_4, CF_TCOM_5 = :CF_TCOM_5, CF_TCOM_6 = :CF_TCOM_6, ' +
             'CF_TCOM_7 = :CF_TCOM_7, CF_TCOM_8 = :CF_TCOM_8, CF_TCOM_9 = :CF_TCOM_9, CF_GAFETE = :CF_GAFETE, CF_CLAVE = :CF_CLAVE, CF_REINICI = :CF_REINICI ' +
             'WHERE CF_NOMBRE = :CF_NOMBRE AND DI_TIPO = :DI_TIPO';
    Q_INSERTAR_CALEND:
      Result := 'INSERT INTO CAF_CALEND (CF_NOMBRE, DI_TIPO, HORA, ACCION, CONTADOR, SEMANA, SYNC) VALUES (:CF_NOMBRE, :DI_TIPO, :HORA, :ACCION, ' +
             ':CONTADOR, :SEMANA, :SYNC)';
    Q_BORRAR_CALEND:
      Result := 'DELETE FROM CAF_CALEND WHERE CF_NOMBRE = :CF_NOMBRE AND DI_TIPO = :DI_TIPO';
    Q_OBTENER_SINC:
      Result := 'SELECT CF_NOMBRE FROM CAF_CONFIG WHERE CF_CAMBIOS =  ''%s''  AND (CF_NOMBRE = ''%s'' ) AND (DI_TIPO = %d)';
    else
      inherited GetScript(iScript);
  end;
end;

function TdmCafetera.GetQuery: TDataSet;
begin
  Result := oZetaProvider.qryMaster;
end;

procedure TdmCafetera.Connect;
begin
  inherited;
  with oZetaProvider do begin
    EmpresaActiva := Comparte;
    PreparaQuery(FQueryEmpID, GetScript(Q_EMP_ID));
    {$IFNDEF INTERBASE}
    PreparaQuery(FQueryEmpBio, GetScript(Q_EMP_BIO)); // BIOMETRICO
    {$ENDIF}
  end;
  Connected := True
end;

procedure TdmCafetera.Disconnect;
begin
  inherited;
  Connected := False
end;

procedure TdmCafetera.ConnectQuery(const sSQL: string);
begin
  with oZetaProvider.qryMaster do begin
    Active      := False;
    CommandText := ZetaCommonTools.BorraCReturn(sSQL);
    Active      := True;
  end;
end;

{ Mantenimiento de la Lista de Empresas }

procedure TdmCafetera.Clear;
var
  I: Integer;
begin
  for I := (Count - 1) downto 0 do begin
    DelEmpresa(I);
  end;
  FList.Clear;
end;

function TdmCafetera.Count: Integer;
begin
  Result := FList.Count;
end;

function TdmCafetera.GetEmpresa(Index: Integer): TCafeteria;
begin
  if (Index >= 0) and (Index < Self.Count) then
    Result := TCafeteria(FList[Index])
  else
    Result := NIL;
end;

function TdmCafetera.FindEmpresa(Checada: TChecada): TCafeteria;
var
  I: Integer;
begin
  Result := NIL;
  for I  := 0 to (Count - 1) do begin
    if Checada.EmpresaOK(Empresa[I]) then begin
      Result := Empresa[I];
      Break;
    end;
  end;
end;

function TdmCafetera.AddEmpresa: TCafeteria;
begin
  Result := TCafeteria.Create;
  FList.Add(Result);
  with Result do begin
    ChecadaAcceso := Self.ChecadaAcceso;
    ChecadaComida := Self.ChecadaComida;
  end;
end;

procedure TdmCafetera.DelEmpresa(const Index: Integer);
begin
  Empresa[Index].Free;
  FList.Delete(Index);
end;

procedure TdmCafetera.Llena(Lista: TStrings);
var
  I: Integer;
begin
  with Lista do begin
    try
      BeginUpdate;
      Clear;
      for I := 0 to (Self.Count - 1) do
        Add(Empresa[I].Nombre)
    finally
      EndUpdate;
    end;
  end;
end;

{ Procesamiento de Checadas }

function TdmCafetera.GetListaEmpresas: Boolean;
var
  oCafeteria: TCafeteria;
begin
  Result := False;
  try
    with oZetaProvider.CreateQuery(Format(GetScript(Q_LEE_EMPRESAS), [K_GLOBAL_SI, K_GLOBAL_SI])) do begin
      try
        Active := True;
        while not Eof do begin
          oCafeteria := AddEmpresa;
          with oCafeteria do begin
            Codigo := FieldByName('CM_CODIGO').AsString;
            Nombre := FieldByName('CM_NOMBRE').AsString;
            // AliasName := FieldByName( 'CM_ALIAS' ).AsString;
            AliasName := FieldByName('CM_DATOS').AsString;
            UserName  := FieldByName('CM_USRNAME').AsString;
            Password  := FieldByName('CM_PASSWRD').AsString;
            Datos     := FieldByName('CM_DATOS').AsString;
            Digito    := FieldByName('CM_DIGITO').AsString;

            UsaAccesos   := ZetaCommonTools.zStrToBool(FieldByName('CM_USACASE').AsString);
            UsaCafeteria := ZetaCommonTools.zStrToBool(FieldByName('CM_USACAFE').AsString);
            GetDispositivo(oCafeteria, ListaDispCafeteria, dpCafeteria);
            CuantosCafeteria := ListaDispCafeteria.Count;
            GetDispositivo(oCafeteria, ListaDispAccesos, dpCaseta);
            CuantosAccesos := ListaDispAccesos.Count;
          end;
          Result := True;
          Next;
        end;
        Active := False;
      finally
        Free;
      end;
    end;
  except
    on e: Exception do begin
      Clear;
      raise
    end;
  end;
  if Result then begin
    FOptimizacionCafeteria := HayOptimizacion(dpCafeteria);
    FOptimizacionCaseta    := HayOptimizacion(dpCaseta);
  end;
end;

{ ACL. 28.Feb.09. Optimiza cuando no existen dispositivos }
function TdmCafetera.HayOptimizacion(const eTipo: eLstDispositivos): Boolean;
var
  FCount: TZetaCursor;
begin
  with oZetaProvider do begin
    FCount := oZetaProvider.CreateQuery(Format(GetScript(Q_TOTAL_DISPOSIT), [ord(eTipo)]));
    try
      with FCount do begin
        Active := True;
        Result := FCount.FieldByName('Total').AsInteger = 0;
        Active := False;
      end;
    finally
      FreeAndNil(FCount);
    end;
  end;
end;

procedure TdmCafetera.GetDispositivo(oCafeteria: TCafeteria; ListaDispositivo: TStringList; const eTipo: eLstDispositivos);
var
  FListaDisp: TZetaCursor;
begin
  with oZetaProvider do begin
    FListaDisp := CreateQuery(Format(GetScript(Q_GET_DISPOSITIVOS), [oCafeteria.Codigo, ord(eTipo)]));
    try
      with FListaDisp do begin
        Active := True;
        while not Eof do begin
          ListaDispositivo.Add(FieldByName('DI_NOMBRE').AsString);
          Next;
        end;
        Active := False;
      end;
    finally
      FreeAndNil(FListaDisp);
    end;
  end;
end;

procedure TdmCafetera.CheckStructure(Source, Target: TDataSet);
var
  I: Integer;
begin
  if Source.FieldDefs.Count <> Target.FieldDefs.Count then begin
    Target.Close;
    CopyStructure(Source, Target);
    for I                       := 0 to Target.FieldCount - 1 do
      Target.Fields[I].ReadOnly := False;
    TClientDataSet(Target).CreateDataSet;
  end;

end;

function TdmCafetera.Init(CallBack: TCallBack): Boolean;
var
  I, j         : Integer;
  lHayRepetidas: Boolean;
  {$IFDEF DEBUGSENTINEL}
  oLicenseMgr: TLicenseMgr;
  {$ENDIF}
begin
  ChecadaComida.Init;
  ChecadaAcceso.Init;
  if (Count > 0) then begin
    for I := 0 to (Count - 1) do begin
      try
        Empresa[I].Init(CallBack);
      except
        on e: Exception do begin
          e.Message := 'Error Al Accesar ' + Empresa[I].Nombre + CR_LF + ExceptionAsString(e);
          Close;
          raise
        end;
      end;
    end;
    lHayRepetidas := False;

    I := 0;
    while (I < Count) do begin
      for j := (Count - 1) downto (I + 1) do begin
        { Si hay d�gitos de empresa repetidos, borra y avisa }
        with Empresa[j] do begin
          if not Empresa[j].UsaCafeteria and (Empresa[I].Digito = Digito) then begin
            lHayRepetidas := True;
            CafeServer.LogMessage(Format('%s: D�gito De Empresa ( %s ) Igual Que %s', [Nombre, Digito, Empresa[I].Nombre]));
            Close;
            DelEmpresa(j);
          end;
        end;
      end;
      Inc(I);
    end;
    if lHayRepetidas then
      CafeServer.LogMessage('Hay Compa��as Con D�gitos Repetidos');
    Result := True;
  end else
    raise Exception.Create('Lista de Compa��as Vac�a');

  {$IFDEF DEBUGSENTINEL}
  oLicenseMgr := TLicenseMgr.Create(oZetaProvider);
  try
    try
      ZetaSQLBroker.ReadAutoServer(oAutoServer, oLicenseMgr.AutoGetData, oLicenseMgr.AutoSetData);
    except
      // Excepcion silenciosa
    end;
  finally
    FreeAndNil(oLicenseMgr);
  end;
  {$ENDIF}
  with oAutoServer do begin
    {$IFNDEF DEBUGSENTINEL}
    Cargar;
    {$ENDIF}
    FAutoCafeteria := not EsDemo and OkModulo(okCafeteria, True);
    FAutoAccesos   := not EsDemo and OkModulo(okAccesos, True);
  end;
  FEmpleadoCtr := 0;
end;

procedure TdmCafetera.Close;
var
  I: Integer;
begin
  for I := (Count - 1) downto 0 do begin
    Empresa[I].Close;
  end;
  FEmpresa := NIL;
  Clear;
end;

function TdmCafetera.SetEmpresaActiva(Checada: TChecada): Boolean;
var
  lOptimizacion: Boolean;
begin

  lOptimizacion := OptimizacionCafeteria and OptimizacionCaseta;
  if not lOptimizacion then begin
    if Checada is TChecadaComida then
      lOptimizacion := OptimizacionCafeteria;

    if Checada is TChecadaAcceso then
      lOptimizacion := OptimizacionCaseta;
  end;



  with Checada do begin
    if lOptimizacion then begin
      if not Assigned(FEmpresa) or not Checada.EmpresaOK(FEmpresa) then
      begin

        FEmpresa := FindEmpresa(Checada);

      end;
    end
    else
    begin

      FEmpresa := FindEmpresa(Checada);

    end;
  end;
  Result := Assigned(FEmpresa);



  if Result then
  begin

    FEmpresa.ReconectarEmpresa;

  end;



end;

function TdmCafetera.UsaCafeteria: Boolean;
begin
  Result := FEmpresa.UsaCafeteria;
end;

function TdmCafetera.UsaAccesos: Boolean;
begin
  Result := FEmpresa.UsaAccesos;
end;

procedure TdmCafetera.AutorizaAcceso(Dataset: TDataSet; var lExisteEmp: Boolean);
begin
  FEmpresa.AutorizaAcceso(Dataset, lExisteEmp);
end;

function TdmCafetera.AutorizaComida: Boolean;
begin
  Result := FEmpresa.AutorizaComida;
end;

procedure TdmCafetera.SetTipoChecadaAcceso;
begin
  FEmpresa.SetTipoChecadaAcceso;
end;

procedure TdmCafetera.EscribeAcceso;
begin
  FEmpresa.EscribeAcceso;
end;

procedure TdmCafetera.EscribeComida;
begin
  if AutoCafeteria then begin
    FEmpresa.EscribeComida;
  end;
end;

procedure TdmCafetera.LeeChecadas(const dInicial, dFinal: TDateTime; const sTipos, sReloj: string; const lTotales: Boolean;
  var sDetalle: string);
var
  I: Integer;
begin

  if Connected then begin
    adsDetalle.Open;
    adsDetalle.EmptyDataSet;
    adsTotales.Open;
    adsTotales.EmptyDataSet;

    for I := 0 to (Count - 1) do begin
      with Empresa[I] do begin
        Empresa[I].ReconectarEmpresa;
        if UsaCafeteria then begin
          BuildTotales(dInicial, dFinal, sTipos, sReloj, adsTotales);
          if not lTotales then
            BuildDetalles(dInicial, dFinal, sTipos, sReloj, adsDetalle);
        end;
      end;
    end;
    adsDetalle.Close;
    adsTotales.Close;
  end;
end;

function TdmCafetera.ImportaAccesos(var sDatos: string): Boolean;
begin
  Result := False;
  if Connected then begin
    try
      if ChecadaAcceso.SetChecada(sDatos, ZetaCommonTools.TheYear(Now)) then begin
        if SetEmpresaActiva(ChecadaAcceso) then begin
          if UsaAccesos then begin
            try
              SetTipoChecadaAcceso;
              try
                if AutoAccesos then begin
                  EscribeAcceso;
                end;
                Result := True;
              except
                on e: Exception do begin
                  sDatos := 'Error Al Grabar Checada: ' + ExceptionAsString(e);
                end;
              end;
            except
              on e: Exception do begin
                sDatos := 'Error Al Determinar Tipo: ' + ExceptionAsString(e);
              end;
            end;
          end
          else
            sDatos := Format('La Empresa Con Letra %s No Usa Caseta', [ChecadaAcceso.Empresa]);
        end
        else
          sDatos := Format('No Hay Empresa Con Letra %s', [ChecadaAcceso.Empresa]);
      end
      else
        sDatos := 'Formato Inv�lido: ' + sDatos;
    except
      on e: Exception do begin
        sDatos := 'Error Al Importar Checada: ' + ExceptionAsString(e);
      end;
    end;
  end
  else
    sDatos := 'Empresas Desconectadas';
end;

function TdmCafetera.ImportaCafeteria(var sDatos: string; MsgID: Integer): Boolean;
var
  lYaEscribio: Boolean;
  sChecada   : string;
begin
  Result      := False;
  lYaEscribio := False;
  sChecada    := sDatos;

  try
	IniciarZonaCritica() ;

	  if Connected then begin
		try
		  if AnsiPos('|', sDatos) = 0 then begin
			if ChecadaComida.SetChecada(sDatos, ZetaCommonTools.TheYear(Now)) then begin
			  if SetEmpresaActiva(ChecadaComida) then begin
				if UsaCafeteria then begin
				  try
					AutorizaComida;
					try
            ChecadaComida.EsFueraLinea := dmCafetera.ImportandoFueraDeLinea;
					  EscribeComida;
            ChecadaComida.EsFueraLinea := FALSE;
					  {$IFDEF BOSE}
					  with ChecadaComida do
						FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 1, Fecha, Hora, Reloj, Copy(Respuesta, 1, 50));
					  {$ENDIF}
					  Result := True;
					except
					  on e: Exception do begin
						sDatos := 'Error Al Grabar Checada: ' + ExceptionAsString(e);
					  end;
					end;
				  except
					on e: Exception do begin
					  sDatos := 'Error Al Autorizar Checada: ' + ExceptionAsString(e);
					end;
				  end;
				end else begin
				  sDatos := Format('La Empresa Con Letra %s No Usa Cafeter�a', [ChecadaComida.Empresa]);
				  {$IFDEF BOSE}
				  with ChecadaComida do
					FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj, Copy(sDatos, 1, 50));
				  {$ENDIF}
				end;
			  end else begin
				sDatos := Format('No Hay Empresa Con Letra %s', [ChecadaComida.Empresa]);
				{$IFDEF BOSE}
				with ChecadaComida do
				  FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj, Copy(sDatos, 1, 50));
				{$ENDIF}
			  end;
			end else begin
			  sDatos := 'Formato Inv�lido: ' + sDatos;
			  {$IFDEF BOSE}
			  with ChecadaComida do
				FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj, Copy(sDatos, 1, 50));
			  {$ENDIF}
			end;
		  end else begin
			with ChecadaComida do begin
			  {$IFDEF BOSE}
			  if AnsiPos('Reinicio de Contador', sDatos) > 0 then begin
				// iEmpleado, iRegla, iSegundos, iStatus, dFecha, sHora, sEstacion, sMensaje
				FEmpresa.EscribeBitacoraError(0, 0, StrToIntDef(Copy(sDatos, 6, 10), 0), 0, StrToFecha(Copy(sDatos, 16, 8)),
				  Copy(sDatos, 24, 4), Copy(sDatos, 2, 4), Copy(sDatos, 28, 23), 2);
			  end else begin
				FEmpresa.EscribeBitacoraError(0, 0, StrToIntDef(Copy(sDatos, 6, 10), 0), 0, StrToFecha(Copy(sDatos, 16, 8)),
				  Copy(sDatos, 24, 4), Copy(sDatos, 2, 4), Copy(sDatos, 28, 23), 1);
			  end;
			  {$ENDIF}
			  Reloj      := Copy(sDatos, 2, 4);
			  Hora       := Copy(sDatos, 24, 4);
			  Fecha      := StrToFecha(Copy(sDatos, 16, 8));
			  Respuesta  := '';
			  TipoComida := ' ';
			  Empresa    := '';
			end;
			EscribeBitacoraError(ChecadaComida, 2000, Copy(sDatos, 28, 23), sChecada, StrToIntDef(Copy(sDatos, 6, 10), 0));
			lYaEscribio := True;

		  end;
		except
		  on e: Exception do begin
			sDatos := 'Error Al Importar Checada: ' + ExceptionAsString(e);
			{$IFDEF BOSE}
			with ChecadaComida do
			  FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj, Copy(sDatos, 1, 50));
			{$ENDIF}
		  end;
		end;
	  end else begin
		sDatos := 'Empresas Desconectadas';
		{$IFDEF BOSE}
		with ChecadaComida do
		  FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj, Copy(sDatos, 1, 50));
		{$ENDIF}
	  end;
	  if not lYaEscribio then begin
		if not Result then
		  MsgID := MsgID + 1;
		EscribeBitacoraError(ChecadaComida, MsgID, sChecada, sDatos);
	  end;

  finally
        TerminarZonaCritica;
  end;

end;

// procedure PaletitasActions0Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
procedure TdmCafetera.AutorizarChecadaComida(DS: TClientDataSet; var ClientParams: TZetaParams);
var
  sGafete, sTipo, sReloj, sLetrero, sComidas, sHora, sFoto, sChecada: string;
  iComidas: Integer;
  lComidas, lMuestraFoto                  : Boolean;
  dValor                                  : TDateTime;
  eStatus                                 : eStatusChecada;
  eGafete                                 : eTipoGafete;
  wHora, wMinuto, wSegundos, wMiliSegundos: Word;

  procedure WriteError(const sMensaje: string);
  begin
    sLetrero := sMensaje;
    eStatus  := echError;
  end;

begin

try
    IniciarZonaCritica; // Evitar que Start o Restart interfieran mientras se procesa checada

  with ClientParams do begin
    sGafete      := ParamByName('Gafete').AsString;
    sChecada     := sGafete;
    dValor       := ParamByName('FechaHora').AsDateTime;
    iComidas     := ParamByName('NumComidas').AsInteger;
    sTipo        := ParamByName('TipoComida').AsString;
    sReloj       := ParamByName('Letrero').AsString;
    lComidas     := ZetaCommonTools.zStrToBool(ParamByName('Comidas').AsString);
    eGafete      := eTipoGafete(ParamByName('TipoGafete').AsInteger);
    lMuestraFoto := ZetaCommonTools.zStrToBool(ParamByName('Foto').AsString);
  end;
  if Connected then begin

          try
            with ChecadaComida do begin
              Init;
              TipoGafete := eGafete;
              if SetGafete(sGafete) then begin
                FechaHora  := dValor;
                TipoComida := sTipo[1];
                NumComidas := iComidas;
                Reloj      := sReloj;
                eStatus    := echOK;
              end else begin
                DecodeTime(Fecha, wHora, wMinuto, wSegundos, wMiliSegundos);
                sHora := PadLCar(InttoStr(wHora), 2, '0') + PadLCar(InttoStr(wMinuto), 2, '0');
                {$IFDEF BOSE}
                with ChecadaComida do
                  FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Trunc(Fecha), sHora, Copy(sReloj, 1, 4), Copy(Respuesta, 1, 50));
                {$ENDIF}
                WriteError(Respuesta);
              end;

            end;
          except
            on e: Exception do begin
              WriteError('Error Al Decodificar Checada: ' + ExceptionAsString(e));
            end;
          end;
          if (eStatus = echOK) then
          begin

            if SetEmpresaActiva(ChecadaComida) then begin

              if UsaCafeteria then begin
                try
                  if AutorizaComida then begin
                    sLetrero := ChecadaComida.Respuesta;
                    try
                      EscribeComida;

                      {$IFDEF BOSE}
                      with ChecadaComida do
                        FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 1, Fecha, Hora, Reloj, Copy(sLetrero, 1, 50));
                      {$ENDIF}
                    except
                      on e: Exception do begin
                        {$IFDEF BOSE}
                        with ChecadaComida do
                          FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj, Copy('Error Al Grabar Checada: ' +
                            ExceptionAsString(e), 1, 50));
                        {$ENDIF}
                        WriteError('Error Al Grabar Checada: ' + ExceptionAsString(e));

                      end;
                    end;
                  end else begin
                    WriteError(ChecadaComida.Respuesta);
                    {$IFDEF BOSE}
                    with ChecadaComida do
                      FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj, Copy(Respuesta, 1, 50));
                    {$ENDIF}
                  end; // else
                except
                  on e: Exception do begin
                    {$IFDEF BOSE}
                    with ChecadaComida do
                      FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj,
                        Copy('Error Al Autorizar Checada: ' + ExceptionAsString(e), 1, 50));
                    {$ENDIF}
                    WriteError('Error Al Autorizar Checada: ' + ExceptionAsString(e));
                  end;
                end;
                with FEmpresa do begin
                  try
                    BuildDatasetsCafe(lComidas, lMuestraFoto);
                    CloneDataSet(GetDatasetEmpleado, DS);
                    // Action[K_ACTION_VALIDA_CAFE].Dataset := GetDatasetEmpleado;
                    try
                      if lComidas then begin
                        sComidas := DataSetToXML(GetDatasetHistorial)
                      end
                      else
                        sComidas := VACIO;
                    except
                      on e: Exception do begin
                        {$IFDEF BOSE}
                        with ChecadaComida do
                          FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj,
                            Copy('Error Al Leer Historial: ' + ExceptionAsString(e), 1, 50));
                        {$ENDIF}
                        WriteError('Error Al Leer Historial: ' + ExceptionAsString(e));
                        sComidas := VACIO;
                      end;
                    end;
                    try
                        sFoto := VACIO;
                        if GetDatasetFotoEmp.Active then
                        begin
                            if (lMuestraFoto) and (GetDatasetFotoEmp.FieldDefs.Count > 0) then
                            begin
                                sFoto := DataSetToXML(GetDatasetFotoEmp);
                            end
                        end
                    except
                      on e: Exception do
                      begin
                        WriteError('Error al leer foto de empleado: ' + ExceptionAsString(e));
                        sFoto := VACIO;
                      end;
                    end;
                  except
                    on e: Exception do begin
                      {$IFDEF BOSE}
                      with ChecadaComida do
                        FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj,
                          Copy('Error Al Leer Empleado: ' + ExceptionAsString(e), 1, 50));
                      {$ENDIF}
                      WriteError('Error Al Leer Empleado: ' + ExceptionAsString(e));
                      // Action[K_ACTION_VALIDA_CAFE].Dataset := adsEmpDummy;
                      sComidas := VACIO;
                      sFoto    := VACIO;
                    end;
                  end;
                end;
              end else begin
                WriteError(Format('Empresa %s No Usa Cafeter�a', [ChecadaComida.Empresa]));
                // Action[K_ACTION_VALIDA_CAFE].Dataset := adsEmpDummy;
                sComidas := VACIO;
                sFoto    := VACIO;
                {$IFDEF BOSE}
                with ChecadaComida do
                  FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj,
                    Copy(Format('Empresa %s No Usa Cafeter�a', [ChecadaComida.Empresa]), 1, 50));
                {$ENDIF}
              end;
            end else begin
              WriteError(Format('No Hay Empresa Con Letra %s', [ChecadaComida.Empresa]));
              // Action[K_ACTION_VALIDA_CAFE].Dataset := adsEmpDummy;
              sComidas := VACIO;
              sFoto    := VACIO;
              {$IFDEF BOSE}
              with ChecadaComida do
                FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj,
                  Copy(Format('No Hay Empresa Con Letra %s', [ChecadaComida.Empresa]), 1, 50));
              {$ENDIF}
            end;

          end else begin
            // Action[K_ACTION_VALIDA_CAFE].Dataset := adsEmpDummy;

            sComidas := '';
            sFoto    := VACIO;
          end;


  end else begin

    // Action[K_ACTION_VALIDA_CAFE].Dataset := adsEmpDummy;
    {$IFDEF BOSE}
    with ChecadaComida do
      FEmpresa.EscribeBitacoraError(Empleado, Regla, 0, 0, Fecha, Hora, Reloj, K_NO_DISPONIBLE);
    {$ENDIF}
    WriteError(K_NO_DISPONIBLE);
  end;
  try
    EscribeBitacoraError(ChecadaComida, ord(eStatus), sChecada, sLetrero);
  except
    on e: Exception do
      WriteError('Error de Escritura: ' + ExceptionAsString(e));
  end;

  with ClientParams do begin
    ParamByName('Gafete').AsString  := sGafete;
    ParamByName('Letrero').AsString := sLetrero;
    ParamByName('Status').AsInteger := ord(eStatus);
    ParamByName('Comidas').Value    := sComidas;
    ParamByName('Foto').Value       := sFoto;
  end;

  finally
        TerminarZonaCritica;
    end;

end;

// procedure PaletitasActions1Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
procedure TdmCafetera.LeeChecadasCafeteria(ClientParams: TZetaParams);
var
  sDetalle, sTotales: string;
  lTotales          : Boolean;
begin

  try
      IniciarZonaCritica;

      with ClientParams do
        try
          lTotales := ZetaCommonTools.zStrToBool(ParamByName('Detalle').AsString);
          LeeChecadas(ParamByName('FechaInicial').AsDateTime, ParamByName('FechaFinal').AsDateTime,
            ParamByName('TipoComidas').AsString, ParamByName('Reloj').AsString, lTotales, sDetalle);
          if lTotales then
            sDetalle := ''
          else begin
            sDetalle := DataSetToXML(adsDetalle);
          end;
          sTotales := DataSetToXML(adsTotales);

          ParamByName('Totales').AsString := sTotales;
          ParamByName('Detalle').AsString := sDetalle;

        finally
          adsDetalle.Close;
          adsTotales.Close;
        end;

  finally
         TerminarZonaCritica;
  end;
end;

// procedure PaletitasActions2Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
procedure TdmCafetera.LeerListaInvitadores(ClientParams: TZetaParams);
const
  K_MESS_ERROR = 'Error Al Leer Lista de Invitadores: ';
var
  sLetrero: string;
  sDataSet: string;
begin
  sLetrero := VACIO;
  sDataSet := VACIO;
  if Connected then begin
    try
      BuildListaInvitadores;
      sDataSet := DataSetToXML(adsIdInvita);
    except
      on e: Exception do begin
        sLetrero := K_MESS_ERROR + ExceptionAsString(e);
      end;
    end;
  end else begin
    sLetrero := K_MESS_ERROR + 'Empresas Desconectadas';
  end;
  with ClientParams do begin
    ParamByName('IdInvita').AsString := sDataSet;
    ParamByName('Letrero').AsString := sLetrero;
  end;
end;

procedure TdmCafetera.BuildListaInvitadores;
var
  FIdInvita: TZetaCursor;

  procedure AgregaIdInvita(const sId: string{$IFNDEF INTERBASE}; const iTipo: Integer{$ENDIF} );
  begin
    with adsIdInvita do begin
      Append;
      FieldByName('ID_NUMERO').AsString := sId;
      {$IFNDEF INTERBASE}
      FieldByName('IV_TIPO').AsInteger := iTipo; // BIOMETRICO
      {$ENDIF}
      Post;
    end;
  end;

begin
  with adsIdInvita do begin
    Active := True;
    EmptyDataSet;
  end;
  with oZetaProvider do begin
    FIdInvita := CreateQuery(GetScript(Q_INVITA_ID));
    try
      with FIdInvita do begin
        Active := True;
        CheckStructure(FIdInvita, adsIdInvita);
        while (not Eof) do begin
          AgregaIdInvita(FieldByName('ID_NUMERO').AsString{$IFNDEF INTERBASE}, FieldByName('IV_TIPO').AsInteger{$ENDIF} );
          Next;
        end;
        Active := False;
      end;
    finally
      FreeAndNil(FIdInvita);
    end;
  end;
end;

// procedure PaletitasActions3Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
procedure TdmCafetera.ValidarAcceso(DS: TClientDataSet; ClientParams: TZetaParams);
const
  K_MAX_DEMO = 10;
var
  sGafete   : string;
  sCaseta   : string;
  sLetrero  : string;
  eTipo     : eTipoChecadas;
  dValor    : TDateTime;
  eStatus   : eStatusChecada;
  eGafete   : eTipoGafete;
  lExisteEmp: Boolean;

  procedure WriteError(const sMensaje: string);
  begin
    sLetrero := sMensaje;
    eStatus  := echError;
  end;

begin


  try
      IniciarZonaCritica;

        { Validar Acceso }
        with ClientParams do begin
          sGafete := ParamByName('Gafete').AsString;
          dValor  := ParamByName('FechaHora').AsDateTime;
          eTipo   := eTipoChecadas(ParamByName('Tipo').AsInteger);
          sCaseta := ParamByName('Caseta').AsString;
          eGafete := eTipoGafete(ParamByName('TipoGafete').AsInteger);
        end;
        with adsAcceso do begin
          Active := True;
          EmptyDataSet;
        end;
        if Connected then
        begin
              try
                with ChecadaAcceso do begin
                  Init;
                  TipoGafete := eGafete;
                  if SetGafete(sGafete) then begin
                    FechaHora := dValor;
                    Caseta    := sCaseta;
                    Tipo      := eTipo;
                    eStatus   := echOK;
                  end
                  else
                    WriteError(Respuesta);
                end;
              except
                on e: Exception do begin
                  WriteError('Error Al Decodificar Checada: ' + ExceptionAsString(e));
                end;
              end;
              if (eStatus = echOK) then begin
                if SetEmpresaActiva(ChecadaAcceso) then begin
                  if UsaAccesos then begin
                    try
                      Inc(FEmpleadoCtr);
                      if AutoAccesos or (FEmpleadoCtr <= K_MAX_DEMO) then begin
                        AutorizaAcceso(adsAcceso, lExisteEmp);
                        if lExisteEmp and ChecadaAcceso.Passed then begin
                          try
                            EscribeAcceso;
                          except
                            on e: Exception do begin
                              WriteError('Error Al Grabar Checada: ' + ExceptionAsString(e));
                            end;
                          end;
                        end else begin
                          WriteError(ChecadaAcceso.Respuesta);
                        end;
                      end else begin
                        WriteError(Format('DEMO: L�mite de %d Empleado Excedido', [K_MAX_DEMO]));
                        ChecadaAcceso.Respuesta := 'MODO DEMO';
                      end;
                      //
                      if (eStatus = echOK) and ChecadaAcceso.Passed then
                        sLetrero := ChecadaAcceso.Respuesta
                      else
                        WriteError(ChecadaAcceso.Respuesta);
                    except
                      on e: Exception do begin
                        WriteError('Error Al Autorizar Checada: ' + ExceptionAsString(e));
                      end;
                    end;
                    //
                  end else begin
                    WriteError(Format('Empresa %s No Usa Caseta', [ChecadaAcceso.Empresa]));
                  end;
                end else begin
                  WriteError(Format('No Hay Empresa Con Letra %s', [ChecadaAcceso.Empresa]));
                end;
              end;


        end else begin
          WriteError(K_NO_DISPONIBLE); // 'Empresas desconectadas'
        end;
        try
          DS.XMLData := adsAcceso.XMLData;
        except
          on e: Exception do begin
            WriteError('Error de Escritura: ' + ExceptionAsString(e));
          end;
        end;
        // Action[K_ACTION_VALIDA_ACCESO].Dataset := adsAcceso;
        with ClientParams do begin
          ParamByName('Gafete').AsString  := sGafete;
          ParamByName('Tipo').AsInteger   := ord(ChecadaAcceso.Tipo);
          ParamByName('Status').AsInteger := ord(eStatus);
          ParamByName('Caseta').AsString  := sLetrero;
        end;

  finally
        TerminarZonaCritica;
  end;


end;

procedure TdmCafetera.EscribeBitacoraError(oChecadaComida: TChecadaComida; const MsgID: Integer; const sChecada: string;
  const sMensaje: string = VACIO; const iTiempo: Integer = 0);
begin
  {$IFNDEF INTERBASE}
  if ( FProviderBitacora <> nil)  then
  begin
      FreeAndNIl( FProviderBitacora );
  end;

  try
    {$IFNDEF INTERBASE}
    FProviderBitacora               := GetZetaProvider(NIL);
    FProviderBitacora.EmpresaActiva := FProviderBitacora.Comparte;
    FBitacora                       := FProviderBitacora.CreateQuery(GetScript(Q_AGREGA_BITACORA));
    {$ENDIF}
  except
    on e: Exception do begin
      raise;
    end;
  end;

  try
          with FProviderBitacora do begin
          try
            //EmpiezaTransaccion;
            with oChecadaComida do begin
              // Pasar Parametros
              ParamAsInteger(FBitacora, 'CB_CODIGO', Empleado);
              ParamAsInteger(FBitacora, 'IV_CODIGO', Invitador);
              ParamAsString(FBitacora, 'BC_EMPRESA', Empresa);
              ParamAsString(FBitacora, 'BC_CREDENC', Credencial);
              ParamAsDate(FBitacora, 'BC_FECHA', Trunc(Fecha));
              ParamAsString(FBitacora, 'BC_HORA', Hora);
              ParamAsString(FBitacora, 'BC_RELOJ', Reloj);
              ParamAsString(FBitacora, 'BC_TIPO', TipoComida);
              ParamAsInteger(FBitacora, 'CL_CODIGO', Regla);
              ParamAsInteger(FBitacora, 'BC_COMIDAS', NumComidas);
              ParamAsString(FBitacora, 'BC_MENSAJE', Copy(sMensaje, 1, 50));
              ParamAsString(FBitacora, 'BC_RESPUES', Copy(Respuesta, 1, 50));
              ParamAsString(FBitacora, 'BC_CHECADA', Copy(sChecada, 1, 50));
              ParamAsInteger(FBitacora, 'BC_TGAFETE', ord(TipoGafete));

              ParamAsInteger(FBitacora, 'BC_EXTRAS', 0); // PENDIENTE
              ParamAsString(FBitacora, 'BC_REG_EXT', zBoolToStr(Extras)); // PENDIENTE
              ParamAsString(FBitacora, 'BC_MANUAL', zBoolToStr(EsManual)); // PENDIENTE
              ParamAsInteger(FBitacora, 'BC_TIEMPO', iTiempo);
              ParamAsInteger(FBitacora, 'BC_STATUS', MsgID);
            end;

            try
              Ejecuta(FBitacora);
            except
              on e: Exception do begin
                raise;
              end;
            end;

            //TerminaTransaccion(True);
          except
            on e: Exception do begin
              //TerminaTransaccion(False);
              raise;
            end;
          end;
        end;
  finally
      
      try
        FProviderBitacora.adoComparte.Connected := FALSE; 
        except
            on e: Exception do begin
              //Se come la excepcion 
            end;
      end;
      FreeAndNil( FBitacora ) ;
      FreeAndNIl( FProviderBitacora );
  end;              
  {$ENDIF}
end;

// BIOMETRICO
// procedure PaletitasActions4Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
procedure TdmCafetera.ObtenerHuellas(ClientParams: TZetaParams);
{$IFNDEF INTERBASE}
var
  sDataSet, sLetrero, sComputer: string;
const
  K_MESS_ERROR = 'Error al obtener las huellas de los empleados: ';
  {$ENDIF}
begin
  {$IFNDEF INTERBASE}
  sDataSet := VACIO;

  if Connected then begin
    try
      GetHuellas(ClientParams.ParamByName('Computer').AsString, ClientParams.ParamByName( 'idCafeteria' ).AsString, ClientParams.ParamByName( 'TipoEstacion' ).AsInteger);
      // Trae huellas de empleados / Invitadores
      sDataSet := DataSetToXML(adsHuellas);
    except
      on e: Exception do begin
        sLetrero := K_MESS_ERROR + ExceptionAsString(e);
      end;
    end;
  end else begin
    sLetrero := K_MESS_ERROR + 'No hay informaci�n de huellas';
  end;
  with ClientParams do begin
    ParamByName('Huellas').AsString  := sDataSet;
    ParamByName('Letrero').AsString  := sLetrero;
    ParamByName('Computer').AsString := sComputer;
  end;
  {$ENDIF}
end;

{$IFNDEF INTERBASE}

// BIOMETRICO
function TdmCafetera.GetHuellas(sComputer, sIdCafeteria: string; iTipoEstacion: integer): Boolean;
var
  FHuellas: TZetaCursor;
  sFiltro: string;

  procedure AgregaHuella(iBiometrico: Integer; sEmpresa: string; iEmpleado, iDedo: Integer; sHuella: string;
    iInvitador, iIdHuella: Integer);
  begin
    with adsHuellas do begin
      Open;

      Append;

      FieldByName('ID_NUMERO').AsInteger := iBiometrico;
      FieldByName('CM_CODIGO').AsString  := sEmpresa;
      FieldByName('CB_CODIGO').AsInteger := iEmpleado;
      FieldByName('HU_INDICE').AsInteger := iDedo;
      FieldByName('HU_HUELLA').AsString  := sHuella;
      FieldByName('IV_CODIGO').AsInteger := iInvitador;
      FieldByName('HU_ID').AsInteger     := iIdHuella;

      Post;
    end;
  end;

begin
  Result := True;
  with adsHuellas do begin
    Active := True;
    EmptyDataSet;
  end;
  with oZetaProvider do begin
    EmpresaActiva := Comparte;
    sFiltro := VACIO;
    if not FOptimizacionCafeteria then
       sFiltro := Format( 'and V.CM_CODIGO in ( select DI.CM_CODIGO from DISXCOM DI where DI.DI_NOMBRE = ''%s'' and DI.DI_TIPO = %d )', [ sIdCafeteria, Ord( iTipoEstacion )  ] );
    FHuellas      := CreateQuery(Format(GetScript(Q_HUELLAS), [sComputer, sFiltro]));
    try
      with FHuellas do begin
        Active := True;
        CheckStructure(FHuellas, adsHuellas);
        while (not Eof) do begin
          AgregaHuella(FieldByName('ID_NUMERO').AsInteger, FieldByName('CM_CODIGO').AsString, FieldByName('CB_CODIGO').AsInteger,
            FieldByName('HU_INDICE').AsInteger, FieldByName('HU_HUELLA').AsString, FieldByName('IV_CODIGO').AsInteger,
            FieldByName('HU_ID').AsInteger);
          Next;
        end;
        Active := False;
      end;
    finally
      FreeAndNil(FHuellas);
    end;
  end;
end;
{$ENDIF}

// procedure PaletitasActions5Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
procedure TdmCafetera.BorrarRelacionHuellas(ClientParams: TZetaParams);
begin
  {$IFNDEF INTERBASE}
  if Connected then begin
    try
      with oZetaProvider do begin
        EmpresaActiva := Comparte;
        EjecutaAndFree('delete from COM_HUELLA where CH_CODIGO = ''' + ClientParams.ParamByName('Computer').AsString + '''')
      end;
    except
      ; // Excepcion silenciosa
    end;
  end;
  {$ENDIF}
end;

// procedure PaletitasActions6Action(Sender: TObject; ADataSet: TDataSet; ClientParams: TAstaParamList);
procedure TdmCafetera.ReportarHuellas(ClientParams: TZetaParams);
{$IFNDEF INTERBASE}
var
  sComputer, sMensaje: string;
  sHuellas           : TStringList;
  I                  : Integer;
  {$ENDIF}
begin
  {$IFNDEF INTERBASE}
  // Reporta Huellas OK
  sMensaje := VACIO;
  if Connected then begin
    try
      try
        sHuellas               := TStringList.Create;
        sHuellas.Delimiter     := ',';
        sHuellas.DelimitedText := ClientParams.ParamByName('Lista').AsString;
        sComputer              := ClientParams.ParamByName('Computer').AsString;

        if (sHuellas.Count > 0) then
          with oZetaProvider do begin
            for I := 0 to sHuellas.Count - 1 do begin
              EjecutaAndFree(Format(GetScript(Q_HUELLA_REPORTA_INSERTA), [sComputer, sHuellas[I]]));
            end;
          end;
      except
        on e: Exception do begin
          sMensaje := ExceptionAsString(e);
        end;
      end;
    finally
      FreeAndNil(sHuellas);
    end;
  end;
  {$ENDIF}
end;

// US 14639
procedure TdmCafetera.BuscaCafeteria(ClientParams: TZetaParams);
var
   FConfig: TZetaCursor;
begin
     with ClientParams do
     begin
          with oZetaProvider do
          begin
               FConfig := CreateQuery(Format(GetScript(Q_CONFIG_CAF),[ParamByName('CF_NOMBRE').AsString, ParamByName('DI_TIPO').AsInteger]));
               try
                  FConfig.Active := True;
                  ParamByName('STATUS').AsBoolean := Not FConfig.IsEmpty;
                  FConfig.Active := FALSE;
             finally
                    FreeAndNil(FConfig);
             end;
        end;
     end;
end;



// US 13921
procedure TdmCafetera.ActualizarConfig(ClientParams: TZetaParams);
var
   FCambios: TZetaCursor;
begin
     with ClientParams do
     begin
          with oZetaProvider do
          begin
               EmpresaActiva := Comparte;
               EmpiezaTransaccion;               
               try
	              FCambios := CreateQuery(Format(GetScript(Q_UPD_CONFIG),[ParamByName('CF_NOMBRE').AsString, ParamByName('DI_TIPO').AsInteger]));
                  Ejecuta( FCambios );
                  TerminaTransaccion( TRUE );
                  ParamByName('STATUS').AsBoolean := True;
               finally
                      FreeAndNil( FCambios );
               end;
          end;
     end;
end;

// US 13921
procedure TdmCafetera.GetCambiosConfig(ClientParams: TZetaParams);
var
   sNombre, sConfig, sCalend: string;
   iTipo, I: Integer;
   FConfig, FCalend: TZetaCursor;
begin
     try
        IniciarZonaCritica;

        with ClientParams do
        begin
             sNombre := ParamByName('CF_NOMBRE').AsString;
             iTipo := ParamByName('DI_TIPO').AsInteger;
        end;

        adsConfiguracion.Open;
        adsConfiguracion.EmptyDataSet;
        adsCalendario.Open;
        adsCalendario.EmptyDataSet;

        with oZetaProvider do
        begin
             FConfig := CreateQuery(Format(GetScript(Q_CONFIG_CAF),[sNombre, iTipo]));
             try
                FConfig.Active := True;
                if Not FConfig.IsEmpty then
                begin
                     try
                        FCalend := CreateQuery(Format(GetScript(Q_CALEND_CAF),[sNombre, iTipo]));
                        FCalend.Active := True;
                        CloneDataSet(FCalend, adsCalendario);
                        CloneDataSet(FConfig, adsConfiguracion);
                        FCalend.Active := False;
                     finally
                            FreeAndNil(FCalend);
                     end;
                end;
                FConfig.Active := FALSE;
             finally
                    FreeAndNil(FConfig);
             end;
        end;

        sConfig := DataSetToXML(adsConfiguracion);
        sCalend := DataSetToXML(adsCalendario);

        with ClientParams do
        begin
             ParamByName('Totales').AsString := sCalend;
             ParamByName('Detalle').AsString := sConfig;
        end;

        adsConfiguracion.Close;
        adsCalendario.Close;
     finally
            TerminarZonaCritica;
     end;
end;

//
procedure TdmCafetera.VerificarSincronizacion(ClientParams: TZetaParams);
var
   lSincronizar : Boolean;
   sNombre : String;
   iTipo : Integer;
   FDisp: TZetaCursor;
begin
     try
        IniciarZonaCritica;

        with ClientParams do
        begin
             sNombre := ParamByName('CF_NOMBRE').AsString;
             iTipo := ParamByName('DI_TIPO').AsInteger;
        end;

        lSincronizar := False;
        with oZetaProvider do
        begin
             FDisp := CreateQuery(Format(GetScript(Q_OBTENER_SINC), [K_GLOBAL_SI, sNombre, iTipo ]));
             try
                FDisp.Active := True;
                if Not FDisp.IsEmpty then
                begin
                     lSincronizar := True;
                end;

                FDisp.Active := FALSE;

                ClientParams.ParamByName('Detalle').AsString := zBoolToStr(lSincronizar);
             finally
                    FreeAndNil(FDisp);
             end;
        end;
     finally
            TerminarZonaCritica;
     end;
end;

//US 14004 Importar Configuracion de Cafeteria
procedure TdmCafetera.ImportarConfig(ClientParams: TZetaParams);
var
   sNombre, sConfig, sCalend: string;
   iTipo: Integer;
   dsConf: TClientDataSet;
   dsCalend : TDataSet;
   oConfigParams: TZetaParams;
   FConfig, FCalend, FDisp: TZetaCursor;
   lImportar : Boolean;

begin
     try
        IniciarZonaCritica;

        dsConf := TClientDataSet.Create(nil);
        oConfigParams := TZetaParams.Create;

        with ClientParams do
        begin
             sNombre := ParamByName('CF_NOMBRE').AsString;
             iTipo := ParamByName('DI_TIPO').AsInteger;
             XMLToParams(ParamByName('Detalle').AsString, dsConf.Params);
             oConfigParams.Assign(dsConf.Params);
             dsCalend := XMLToDataSet(ParamByName('Totales').AsString);
        end;

        lImportar := False;
        with oZetaProvider do
        begin
             FDisp := CreateQuery(Format(GetScript(Q_GET_DISP), [sNombre, iTipo ]));
             try
                FDisp.Active := True;
                if FDisp.IsEmpty then
                begin
                     lImportar := True;
                     FDisp := CreateQuery(GetScript(Q_INSERTAR_DISP));
                     ParamAsInteger(FDisp, 'DI_TIPO', iTipo);
                     ParamAsString(FDisp, 'DI_NOMBRE', sNombre);
                     Ejecuta(FDisp);
                end
                else
                begin
                     FConfig := CreateQuery(Format(GetScript(Q_GET_CONFIG), [sNombre, iTipo ]));
                     FConfig.Active := True;
                     if FConfig.IsEmpty then
                        lImportar := True;
                end;

                try
                   if lImportar then
                      FConfig := CreateQuery(GetScript(Q_INSERTAR_CONFIG))
                   else
                   begin
                        FConfig := CreateQuery(GetScript(Q_ACTUALIZAR_CONFIG));
                        FCalend := CreateQuery(Format(GetScript(Q_BORRAR_CALEND), [ sNombre, iTipo ]));
                        lImportar := True;
                        Ejecuta(FCalend);
                   end;

                   FCalend := CreateQuery(GetScript(Q_INSERTAR_CALEND));
                   ParamAsString(FConfig, 'CF_NOMBRE', sNombre);
                   ParamAsInteger(FConfig, 'DI_TIPO', iTipo);
                   ParamAsInteger(FConfig, 'CF_TIP_COM', oConfigParams.ParamByName('TipoComida').AsInteger);
                   ParamAsString(FConfig, 'CF_FOTO', zBoolToStr ( oConfigParams.ParamByName('MostrarFoto').AsBoolean ));
                   ParamAsString(FConfig, 'CF_COM', zBoolToStr( oConfigParams.ParamByName('MostrarComida').AsBoolean ));
                   ParamAsString(FConfig, 'CF_SIG_COM', zBoolToStr( oConfigParams.ParamByName('MostrarSigComida').AsBoolean ));
                   ParamAsString(FConfig, 'CF_TECLADO', zBoolToStr(  oConfigParams.ParamByName('UsaTeclado').AsBoolean ));
                   ParamAsString(FConfig, 'CF_DEF_COM', oConfigParams.ParamByName('DefaultEmpresa').AsString);
                   ParamAsString(FConfig, 'CF_DEF_CRE', oConfigParams.ParamByName('DefaultCredencial').AsString);
                   ParamAsString(FConfig, 'CF_PRE_TIP', zBoolToStr( oConfigParams.ParamByName('PreguntaTipo').AsBoolean ));
                   ParamAsString(FConfig, 'CF_PRE_QTY', zBoolToStr( oConfigParams.ParamByName('PreguntaQty').AsBoolean ));
                   ParamAsString(FConfig, 'CF_BANNER', oConfigParams.ParamByName('Banner').AsString);
                   ParamAsInteger(FConfig, 'CF_VELOCI', oConfigParams.ParamByName('Velocidad').AsInteger);
                   ParamAsInteger(FConfig, 'CF_T_ESP', oConfigParams.ParamByName('TiempoEspera').AsInteger);
                   ParamAsString(FConfig, 'CF_GAFETE', oConfigParams.ParamByName('GafeteAdmon').AsString);
                   ParamAsString(FConfig, 'CF_CLAVE', oConfigParams.ParamByName('ClaveAdmon').AsString);
                   ParamAsString(FConfig, 'CF_A_PRINT', zBoolToStr( oConfigParams.ParamByName('ConcesionImprimir').AsBoolean ));
                   ParamAsString(FConfig, 'CF_A_CANC', zBoolToStr( oConfigParams.ParamByName('ConcesionCancelar').AsBoolean ));
                   ParamAsString(FConfig, 'CF_TCOM_1', oConfigParams.ParamByName('TipoComida1').AsString);
                   ParamAsString(FConfig, 'CF_TCOM_2', oConfigParams.ParamByName('TipoComida2').AsString);
                   ParamAsString(FConfig, 'CF_TCOM_3', oConfigParams.ParamByName('TipoComida3').AsString);
                   ParamAsString(FConfig, 'CF_TCOM_4', oConfigParams.ParamByName('TipoComida4').AsString);
                   ParamAsString(FConfig, 'CF_TCOM_5', oConfigParams.ParamByName('TipoComida5').AsString);
                   ParamAsString(FConfig, 'CF_TCOM_6', oConfigParams.ParamByName('TipoComida6').AsString);
                   ParamAsString(FConfig, 'CF_TCOM_7', oConfigParams.ParamByName('TipoComida7').AsString);
                   ParamAsString(FConfig, 'CF_TCOM_8', oConfigParams.ParamByName('TipoComida8').AsString);
                   ParamAsString(FConfig, 'CF_TCOM_9', oConfigParams.ParamByName('TipoComida9').AsString);
                   ParamAsString(FConfig, 'CF_REINICI', zBoolToStr( oConfigParams.ParamByName('ReiniciaMediaNoche').AsBoolean ));
                   ParamAsInteger(FConfig, 'CF_CHECK_S', oConfigParams.ParamByName('ChecadaSimultanea').AsInteger);

                   Ejecuta(FConfig);

                   while not dsCalend.EOF do
                   begin
                        ParamAsString(FCalend, 'CF_NOMBRE', sNombre);
                        ParamAsInteger(FCalend, 'DI_TIPO', iTipo);
                        ParamAsString(FCalend, 'HORA', dsCalend.FieldByName('HORA').AsString );
                        ParamAsInteger(FCalend, 'ACCION', dsCalend.FieldByName('ACCION').AsInteger );
                        ParamAsString(FCalend, 'CONTADOR', dsCalend.FieldByName('CONTADOR').AsString );
                        ParamAsInteger(FCalend, 'SEMANA', dsCalend.FieldByName('SEMANA').AsInteger );
                        ParamAsString(FCalend, 'SYNC', dsCalend.FieldByName('SYNC').AsString );
                        Ejecuta(FCalend);
                        dsCalend.Next;
                    end;

                finally
                       FreeAndNil(FConfig);
                       FreeAndNil(FCalend);
                end;
                FDisp.Active := FALSE;

                ClientParams.ParamByName('Detalle').AsString := zBoolToStr(lImportar);
             finally
                    FreeAndNil(FDisp);
             end;
        end;
     finally
            FreeAndNil( oConfigParams );
            FreeAndNil( dsConf );
            FreeAndNil( dsCalend );
            TerminarZonaCritica;
     end;
end;

procedure TdmCafetera.ObtenerLog(Params: TZetaParams);
var
  oTexto: TStringList;
begin
  oTexto := TStringList.Create;
  try
    if FileExists(CafeServer.Log.FileName) then
      oTexto.LoadFromFile(CafeServer.Log.FileName);
    Params.AddMemo('LOG', oTexto.Text);
  except
    on e: Exception do
      Params.AddMemo('LOG', ExceptionAsString(e));
  end;
  FreeAndNil(oTexto);
end;

procedure TdmCafetera.ReiniciarReglas(Params: TZetaParams);
begin
  CafeServer.ReiniciarReglas;//CafeServer.Restart;
  Params.AddBoolean('Status', CafeServer.Connected);
end;

procedure TdmCafetera.GetParametros(Params: TZetaParams);
begin
  Params.AddDateTime(K_IDENT_HORA_REINICIO, CafeServer.GetGlobal(K_IDENT_HORA_REINICIO, K_CAFETERA_HORA_REINICIO));
  if IsDesktopMode or (Cafetera_Servicio = nil )  then
    Params.AddString(K_IDENT_INSTANCIA, SERVICE_BASE_NAME + 'UI')
  else
    Params.AddString(K_IDENT_INSTANCIA, Cafetera_Servicio.Name);
end;

procedure TdmCafetera.SetParametros(Params: TZetaParams);
begin
  // Seguardan los valores que env�a el cliente, el hilo que se ejecuta detecta el cambio en el Registry y actualiza los objetos
  CafeServer.SetGlobal(K_IDENT_PUERTO       , Params.ParamByName(K_IDENT_PUERTO).AsString);
  CafeServer.SetGlobal(K_IDENT_HORA_REINICIO, Params.ParamByName(K_IDENT_HORA_REINICIO).AsDateTime);
end;

procedure TdmCafetera.CodedStream(Params: TSOAPDataSet);
var
  FLista  : TStrings;
  FErrores: TStrings;
  I       : Integer;
  sDatos  : string;

  procedure EnviaErrores;
  begin
    if (FErrores.Count > 0) then begin
      Params.MsgID   := K_ERROR_REPLY;
      Params.Dataset := FErrores.Text;
      GuardaCafeTemporal(FErrores);
    end else begin
      Params.Dataset := '';
    end;
  end;

begin
  if not Connected then begin
    Params.MsgID   := K_ERROR_REPLY;
    Params.Dataset := K_NO_DISPONIBLE;
    Exit;
  end;

  case Params.MsgID of
    K_CAFETERA_FUERA_DE_LINEA: { Transferencia de Archivo de Lecturas de Cafeter�a } begin
        FLista   := TStringList.Create;
        FErrores := TStringList.Create;
        dmCafetera.ImportandoFueraDeLinea := TRUE;
        with FLista do
          try
            Text     := Params.Dataset;
            for I    := 0 to (Count - 1) do begin
              sDatos := Strings[I];
              if not dmCafetera.ImportaCafeteria(sDatos, Params.MsgID) then begin
                CafeServer.LogMessage(StrTransall(sDatos, '%', '%%'));
                { ACL240709. Validacion para cuando el gafete tiene % }
                FErrores.Add(sDatos);
              end;
            end;
            EnviaErrores;
          finally
            Free;
            FErrores.Free;
            dmCafetera.ImportandoFueraDeLinea := FALSE;
          end;
          dmCafetera.ImportandoFueraDeLinea := FALSE;
      end;
    K_ACCESOS_FUERA_DE_LINEA: { Transferencia de Archivo de Lecturas de Accesos } begin
        FLista   := TStringList.Create;
        FErrores := TStringList.Create;
        with FLista do
          try
            Text     := Params.Dataset;
            for I    := 0 to (Count - 1) do begin
              sDatos := Strings[I];
              if not dmCafetera.ImportaAccesos(sDatos) then begin
                CafeServer.LogMessage(sDatos);
                FErrores.Add(sDatos);
              end;
            end;
            EnviaErrores;
          finally
            Free;
            FErrores.Free;
          end;
      end;
    K_CAFETERA_DEMO: { Mensaje  de Regreso para el Modo DEMO } begin
        if dmCafetera.AutoCafeteria then
          Params.Dataset := VACIO
        else
          Params.Dataset := K_ES_DEMO;
      end;
    K_ACCESOS_DEMO: begin
        if dmCafetera.AutoAccesos then
          Params.Dataset := VACIO
        else
          Params.Dataset := K_ES_DEMO;
      end;
  end;
end;


procedure TdmCafetera.IniciarZonaCriticaCrypt;
begin
  EnterCriticalSection(FLockCrypt);
end;

procedure TdmCafetera.TerminarZonaCriticaCrypt;
begin
  LeaveCriticalSection(FLockCrypt);
end;

function TdmCafetera.Encrypt (sValor: widestring): WideString;
begin
      Result := FCrypt.EncryptStringENC(sValor);
end;

function TdmCafetera.Decrypt (sValor: widestring): WideString;
begin
      Result := FCrypt.DecryptStringENC(sValor);
end;

end.
