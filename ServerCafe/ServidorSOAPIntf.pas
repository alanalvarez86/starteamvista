{ Invokable interface IServidorSOAP }

unit ServidorSOAPIntf;

interface

uses Soap.InvokeRegistry, System.Types, Soap.XSBuiltIns;

type

  TSOAPDataSet = class(TRemotable)
  private
    FMsgID  : Integer;
    FParams : WideString;
    FDataSet: WideString;
  published
    property MsgID  : Integer    read FMsgID   write FMsgID;
    property Params : WideString read FParams  write FParams;
    property DataSet: WideString read FDataSet write FDataSet;
  end;

  { Invokable interfaces must derive from IInvokable }
  ICafeteraSOAPWebService = interface(IInvokable)
    ['{9B3E0385-0A55-4A2A-A888-2DF1DC8DCF9A}']
    //E16890ED-A2B7-8E7D-60F9-C496112E18FE

    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function Ping: Boolean; stdcall;
    function SendCodedMessage(const Params: TSOAPDataSet): TSOAPDataSet; stdcall;
    function SendCodedMessageXMLResponse(const Params: TSOAPDataSet): WideString; stdcall;
    function SendCodedMessageByte(const inParams: TSOAPDataSet; var aDetalle, aTotales : TByteDynArray ): Boolean; stdcall;
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(ICafeteraSOAPWebService));

end.
