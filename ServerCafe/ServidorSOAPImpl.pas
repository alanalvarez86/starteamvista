{ Invokable implementation File for TServidorSOAP which implements IServidorSOAP }

unit ServidorSOAPImpl;

interface

uses
  Soap.InvokeRegistry, System.Types, Soap.XSBuiltIns, ServidorSOAPIntf, Chilkat_v9_5_0_TLB;

type

  { TServidorSOAP }
  TCafeteraSOAPWebService = class(TInvokableClass, ICafeteraSOAPWebService)
  public
    function Ping: Boolean; stdcall;
    function SendCodedMessage(const Params: TSOAPDataSet): TSOAPDataSet; stdcall;
    function SendCodedMessageXMLResponse(const Params: TSOAPDataSet): WideString; stdcall;
    function SendCodedMessageByte(const  inParams: TSOAPDataSet; var aDetalle, aTotales : TByteDynArray ): Boolean; stdcall;
  end;

implementation

uses
  Classes, DB, DBClient, SysUtils, Windows, DCafetera, ZetaCommonClasses, ZetaServerTools, CafeteraConsts, CafeteraUtils;

{ TCafeteraSOAPWebService }


function TCafeteraSOAPWebService.Ping: Boolean;
begin
  Result := dmCafetera.Connected;
end;

function TCafeteraSOAPWebService.SendCodedMessage(const Params: TSOAPDataSet): TSOAPDataSet;
var
  oParams: TZetaParams;
  DS: TClientDataSet;
begin

  try

    Result := Params;

    // Encriptación.
    Result.Params  := Result.Params;
    // Result.DataSet := dmCafetera.Crypt.DecryptStringENC(Result.DataSet);
    dmCafetera.IniciarZonaCriticaCrypt;
    Result.DataSet := dmCafetera.Decrypt(Result.DataSet);
    dmCafetera.TerminarZonaCriticaCrypt;

    {$ifdef ANTES}
    Result.Params  := Decrypt(Result.Params);
    Result.DataSet := Decrypt(Result.DataSet);
    {$endif}

    case Params.MsgID of
      K_CAFETERA_FUERA_DE_LINEA:
        begin
          DS := nil;
          oParams := nil;
        end
      else begin
        DS := TClientDataSet.Create(nil);
        XMLToParams(Params.Params, DS.Params);
        oParams := TZetaParams.Create;
        oParams.Assign(DS.Params);
      end;
    end;

    case Params.MsgID of
      K_AUTORIZAR_CHECADA_COMIDA:
        dmCafetera.AutorizarChecadaComida(DS, oParams);
      K_LEE_CHECADAS_CAFETERIA:
        begin
        dmCafetera.LeeChecadasCafeteria(oParams);
        end;
      K_LEE_LISTA_INVITADORES:
        dmCafetera.LeerListaInvitadores(oParams);
      K_VALIDAR_ACCESO:
        begin
          oParams.AddInteger('Status', 0);
          dmCafetera.ValidarAcceso(DS, oParams);
        end;
      K_OBTENER_HUELLAS:
        dmCafetera.ObtenerHuellas(oParams);
      K_BORRAR_RELACION_HUELLAS:
        dmCafetera.BorrarRelacionHuellas(oParams);
      K_OBTENER_LOG:
        begin
          oParams.Clear;
          dmCafetera.ObtenerLog(oParams);
        end;
      K_RESTART:
        dmCafetera.ReiniciarReglas(oParams);
      K_GET_PARAMETROS:
        begin
          dmCafetera.GetParametros(oParams);
        end;
      K_SET_PARAMETROS:
        dmCafetera.SetParametros(oParams);
      K_CAFETERA_FUERA_DE_LINEA, K_ACCESOS_FUERA_DE_LINEA, K_CAFETERA_DEMO, K_ACCESOS_DEMO:
        dmCafetera.CodedStream(Params);
      K_ACTUALIZA_CONFIG:
          dmCafetera.ActualizarConfig(oParams);
      K_VERIFICAR_CONFIG:
          dmCafetera.BuscaCafeteria(oParams);
    end;

    Params.Params := ParamsToXML(oParams);
    case Params.MsgID of
      K_CAFETERA_DEMO, K_ACCESOS_DEMO:
        ; // No tocar el DataSet
      else
      begin
        if Assigned(DS) then
          Params.DataSet := DS.XMLData
        else
          Params.DataSet := '';
      end;
    end;

    {$ifdef ANTES}
    Result.Params  := Encrypt(Result.Params);
    Result.DataSet := Encrypt(Result.DataSet);
    {$endif}

    // Encriptación.
    Result.Params  := (Result.Params);
    // Result.DataSet := dmCafetera.crypt.EncryptStringENC(Result.DataSet);
    dmCafetera.IniciarZonaCriticaCrypt;
    Result.DataSet := dmCafetera.Encrypt(Result.DataSet);
    dmCafetera.TerminarZonaCriticaCrypt;

  finally
    // Destruir objetos.
    FreeAndNil(oParams);
    FreeAndNil(DS);
  end;

end;

function TCafeteraSOAPWebService.SendCodedMessageByte(const inParams: TSOAPDataSet; var aDetalle, aTotales : TByteDynArray ): Boolean;
var
  input : AnsiString;
  oParams: TZetaParams;
  DS: TClientDataSet;
  soapDataset : TSOAPDataSet;
  sl : TStringList;
begin
  input := '';

  soapDataset :=  TSOAPDataSet.Create;
  soapDataSet.MsgID :=  inParams.MsgID;

  {$ifdef ANTES}
  soapDataSet.Params  := Decrypt(inParams.Params);
  soapDataSet.DataSet := Decrypt(inParams.DataSet);
  {$else}
  soapDataSet.Params  := inParams.Params;
  soapDataSet.DataSet := inParams.DataSet;
  {$endif}

  case soapDataset.MsgID of
    K_CAFETERA_FUERA_DE_LINEA:
      begin
        DS := nil;
        oParams := nil;
      end
    else begin
      DS := TClientDataSet.Create(nil);
      XMLToParams(soapDataset.Params, DS.Params);
      oParams := TZetaParams.Create;
      oParams.Assign(DS.Params);
    end;
  end;

  case soapDataset.MsgID of
    K_AUTORIZAR_CHECADA_COMIDA:
      dmCafetera.AutorizarChecadaComida(DS, oParams);
    K_LEE_CHECADAS_CAFETERIA:
      begin
      dmCafetera.LeeChecadasCafeteria(oParams);
      end;
    K_LEE_LISTA_INVITADORES:
      dmCafetera.LeerListaInvitadores(oParams);
    K_VALIDAR_ACCESO:
      begin
        oParams.AddInteger('Status', 0);
        dmCafetera.ValidarAcceso(DS, oParams);
      end;
    K_OBTENER_HUELLAS:
      dmCafetera.ObtenerHuellas(oParams);
    K_BORRAR_RELACION_HUELLAS:
      dmCafetera.BorrarRelacionHuellas(oParams);
    K_OBTENER_LOG:
      begin
        oParams.Clear;
        dmCafetera.ObtenerLog(oParams);
      end;
    K_RESTART:
      dmCafetera.ReiniciarReglas(oParams);
    K_GET_PARAMETROS:
      begin
        dmCafetera.GetParametros(oParams);
      end;
    K_SET_PARAMETROS:
      dmCafetera.SetParametros(oParams);
    K_CAFETERA_FUERA_DE_LINEA, K_ACCESOS_FUERA_DE_LINEA, K_CAFETERA_DEMO, K_ACCESOS_DEMO:
      dmCafetera.CodedStream(soapDataset);
    K_SINCRONIZA_CONFIG:
      dmCafetera.GetCambiosConfig(oParams);
    K_IMPORTAR_CONFIG:
      dmCafetera.ImportarConfig(oParams);
    K_VERIFICAR_SINC:
      dmCafetera.VerificarSincronizacion(oParams);
  end;



  if ( soapDataset.MsgID = K_OBTENER_HUELLAS)   then
  begin
    {$ifdef ANTES}
    input := Encrypt( oParams.ParamByName('Huellas').AsString );
    {$else}
    input :=  oParams.ParamByName('Huellas').AsString;
    {$endif}
    SetLength(aDetalle, Length(input));
    Move(input[1], aDetalle[0], Length(input));

    input := Encrypt( oParams.ParamByName('Letrero').AsString );
    SetLength(aTotales, Length(input));
    Move(input[1], aTotales[0], Length(input));
  end
  else
  begin


  {$ifdef ANTES}
  input := Encrypt( oParams.ParamByName('Detalle').AsString );
  {$else}
  input := oParams.ParamByName('Detalle').AsString;
  {$endif}
  SetLength(aDetalle, Length(input));
  Move(input[1], aDetalle[0], Length(input));

  {$ifdef ANTES}
  input := Encrypt( oParams.ParamByName('Totales').AsString );
  {$else}
  input := oParams.ParamByName('Totales').AsString;
  {$endif}
  SetLength(aTotales, Length(input));
  Move(input[1], aTotales[0], Length(input));
  end;


  FreeAndNil( oParams );
  FreeAndNil( soapDataset );

  Result := True;

end;

function TCafeteraSOAPWebService.SendCodedMessageXMLResponse(
  const Params: TSOAPDataSet): WideString;
var
  oParams: TZetaParams;
  DS: TClientDataSet;
  soapDataset : TSOAPDataSet;
begin

  soapDataset := Params;

{NO CRYPT
  Result.Params  := Decrypt(Result.Params);
  Result.DataSet := Decrypt(Result.DataSet);
  }

  case Params.MsgID of
    K_CAFETERA_FUERA_DE_LINEA:
      begin
        DS := nil;
        oParams := nil;
      end
    else begin
      DS := TClientDataSet.Create(nil);
      XMLToParams(Params.Params, DS.Params);
      oParams := TZetaParams.Create;
      oParams.Assign(DS.Params);
    end;
  end;

  case Params.MsgID of
    K_AUTORIZAR_CHECADA_COMIDA:
      dmCafetera.AutorizarChecadaComida(DS, oParams);
    K_LEE_CHECADAS_CAFETERIA:
      begin
      dmCafetera.LeeChecadasCafeteria(oParams);
      end;
    K_LEE_LISTA_INVITADORES:
      dmCafetera.LeerListaInvitadores(oParams);
    K_VALIDAR_ACCESO:
      begin
        oParams.AddInteger('Status', 0);
        dmCafetera.ValidarAcceso(DS, oParams);
      end;
    K_OBTENER_HUELLAS:
      dmCafetera.ObtenerHuellas(oParams);
    K_BORRAR_RELACION_HUELLAS:
      dmCafetera.BorrarRelacionHuellas(oParams);
    K_OBTENER_LOG:
      begin
        oParams.Clear;
        dmCafetera.ObtenerLog(oParams);
      end;
    K_RESTART:
      dmCafetera.ReiniciarReglas(oParams);
    K_GET_PARAMETROS:
      begin
        dmCafetera.GetParametros(oParams);
      end;
    K_SET_PARAMETROS:
      dmCafetera.SetParametros(oParams);
    K_CAFETERA_FUERA_DE_LINEA, K_ACCESOS_FUERA_DE_LINEA, K_CAFETERA_DEMO, K_ACCESOS_DEMO:
      dmCafetera.CodedStream(Params);
  end;

  Params.Params := ParamsToXML(oParams);
  case Params.MsgID of
    K_CAFETERA_DEMO, K_ACCESOS_DEMO:
      ; // No tocar el DataSet
    else begin
      if Assigned(DS) then
        Params.DataSet := DS.XMLData
      else
        Params.DataSet := '';
    end;
  end;

  {    property MsgID  : Integer    read FMsgID   write FMsgID;
    property Params : WideString read FParams  write FParams;
    property DataSet: WideString read FDataSet write FDataSet;}

  Result := '<?xml version="1.0" ?>' + CR_LF;
  Result := Result + '<Respuesta>'+ CR_LF;
  Result := Result + Format( '<MsgID>%d</MsgID>', [ soapDataset.MsgID ]) +  CR_LF;
  Result := Result + '<Params><![CDATA['+ CR_LF;
  Result := Result + soapDataset.Params + CR_LF;
  Result := Result + ']]></Params>'+ CR_LF;
  Result := Result + '<DataSet><![CDATA['+ CR_LF;
  Result := Result + soapDataset.DataSet + CR_LF;
  Result := Result + ']]></DataSet>'+ CR_LF;
  Result := Result + '</Respuesta>'+ CR_LF;

  {Result.Params  := Encrypt(Result.Params);
  Result.DataSet := Encrypt(Result.DataSet);}

  FreeAndNil(oParams);
  FreeAndNil(DS);
end;

initialization

  { Invokable classes must be registered }
  InvRegistry.RegisterInvokableClass(TCafeteraSOAPWebService);

end.
