// RCM Octubre de 2013
unit XMLMiniTools;

interface

uses
  SysUtils, Windows, Classes, DB, DBClient, TypInfo, Variants;


procedure CopyStructure(SourceDataset, DestDataset: TDataset; doAdd: Boolean = False);
procedure CloneDataSet(Source: TDataSet; Target: TDataSet);
function DataSetToXML(DS: TDataSet): string;
procedure XMLToDataSet(XML: string; DS: TDataSet); overload;
function XMLToDataSet(XML: string): TDataSet; overload;
function ParamsToXML(Params: TParams): string;
procedure XMLToParams(XML: string; AParams: TParams);
function SortClientDataSet(ClientDataSet: TClientDataSet; const FieldName: String; IdxOptions: TIndexOptions): Boolean;

procedure DisplayParams(Params: TParams);
procedure DisplayFields(DS: TDataSet);

//================================================================================================================================
implementation

procedure SetProp(Name: string; Source, Result: TComponent);
var
  V       : Variant;
  PropInfo: PPropInfo;
begin
  PropInfo := TypInfo.GetPropInfo(Source, Name);
  if PropInfo <> nil then
    try
      V := TypInfo.GetPropValue(Source,Name);
      if not VarIsNull(V) then
        TypInfo.SetPropValue(Result, Name, V);
    except
      ; //just kill exception
    end;
end;

function CloneField(Source: TField; AOwner: TComponent): TField;
var
  i: Integer;
const
  CloneProperty: array [0 .. 18] of String = ('EditMask', 'FixedChar', 'Size', 'Transliterate', 'DisplayFormat', 'EditFormat',
    'Currency', 'MaxValue', 'MinValue', 'Precision', 'DisplayValues', 'BlobType', 'ObjectType', 'IncludeObjectField',
    'ReferenceTableName', 'Active', 'Expression', 'GroupingLevel', 'IndexName');
begin
  Result                        := TFieldClass(Source.ClassType).Create(AOwner);
  Result.Alignment              := Source.Alignment;
  Result.AutoGenerateValue      := Source.AutoGenerateValue;
  Result.CustomConstraint       := Source.CustomConstraint;
  Result.ConstraintErrorMessage := Source.ConstraintErrorMessage;
  Result.DefaultExpression      := Source.DefaultExpression;
  Result.DisplayLabel           := Source.DisplayLabel;
  Result.DisplayWidth           := Source.DisplayWidth;
  Result.FieldKind              := Source.FieldKind;
  Result.FieldName              := Source.FieldName;
  Result.ImportedConstraint     := Source.ImportedConstraint;
  Result.LookupDataSet          := Source.LookupDataSet;
  Result.LookupKeyFields        := Source.LookupKeyFields;
  Result.LookupResultField      := Source.LookupResultField;
  Result.KeyFields              := Source.KeyFields;
  Result.LookupCache            := Source.LookupCache;
  Result.ProviderFlags          := Source.ProviderFlags;
  Result.ReadOnly               := Source.ReadOnly;
  Result.Required               := Source.Required;
  Result.Visible                := Source.Visible;

  for i := Low(CloneProperty) to High(CloneProperty) do begin
    SetProp(CloneProperty[i], Source, Result);
  end;
end;

procedure CopyStructure(SourceDataset, DestDataset: TDataset; doAdd: Boolean = False);
var
  I: Integer;
  Fld : TField;
begin
  if not doAdd then begin
    DestDataset.FieldDefs.Clear;
    DestDataset.Fields.Clear;
  end;
  for I := 0 to SourceDataset.Fields.Count - 1 do begin
    if Assigned(DestDataset.Fields.FindField(SourceDataset.Fields[I].FieldName)) then
      Continue;
    Fld         := CloneField(SourceDataset.Fields[I], DestDataset.Fields.Dataset);
    Fld.Dataset := DestDataset.Fields.Dataset;
  end;
end;

procedure CloneDataSet(Source: TDataSet; Target: TDataSet);
var
  I: Integer;
begin
  with Target as TCustomClientDataSet do begin
    Close;
    CopyStructure(Source, Target);
    CreateDataSet;
    LogChanges := False;
    Open;
  end;
  Source.Open;
  Source.First;
  while not Source.Eof do begin
    Target.Insert;
    for I := 0 to Target.Fields.Count - 1 do begin
      if Target.Fields[I].ReadOnly then
        Target.Fields[I].ReadOnly := False;
      Target.Fields[I].Value := Source.Fields[I].Value;
    end;
    Target.Post;
    Source.Next;
  end;
  Source.First;
  Target.First;
end;

function DataSetToXML(DS: TDataSet): string;
var
  Tmp    : TStringStream;
  s      : string;
  CDS    : TClientDataSet;
  FreeCDS: Boolean;
begin
  Tmp := nil;
  Result := '';
  FreeCDS := False;
  if DS.InheritsFrom(TClientDataSet) then begin
    CDS := DS as TClientDataSet;
    CDS.Open;
  end else begin
    CDS := TClientDataSet.Create(nil);
    FreeCDS := True;
    CloneDataSet(DS, CDS);
  end;
  try
    Tmp := TStringStream.Create(s);
    CDS.SaveToStream(Tmp, dfXMLUTF8);
    Result := Tmp.DataString;
  except
    on e:Exception do
      Windows.MessageBoxA(GetDesktopWindow, PAnsiChar(e.ClassName + ': ' + e.Message), 'DataSetToXML', 0);
  end;
  FreeAndNil(Tmp);
  if FreeCDS then
    FreeAndNil(CDS)
  else
    CDS.First;
end;

procedure XMLToDataSet(XML: string; DS: TDataSet);
var
  tmp: TStringStream;
begin
  tmp := nil;
  with DS as TClientDataSet do try
    Open;
    EmptyDataSet;
    Close;
//    FieldDefs.Clear;
    tmp := TStringStream.Create(XML);
    tmp.Seek(0, soBeginning);
    LoadFromStream(tmp);
    Open;
  except
    on e:Exception do
      Windows.MessageBoxA(GetDesktopWindow, PAnsiChar(e.ClassName + ': ' + e.Message), 'XMLToDataSet', 0);
  end;
  FreeAndNil(tmp)
end;

function XMLToDataSet(XML: string): TDataSet;
var
  Tmp: TStringStream;
begin
  Tmp := nil;
  Result := TClientDataSet.Create(nil);
  with Result as TClientDataSet do try
    FieldDefs.Add('Dummy', ftInteger);
    CreateDataSet;
    Tmp := TStringStream.Create(XML);
    Tmp.Seek(0, soBeginning);
    LoadFromStream(Tmp);
    Open;
  except
    on e:Exception do
      Windows.MessageBoxA(GetDesktopWindow, PAnsiChar(e.ClassName + ': ' + e.Message), 'XMLToDataSet', 0);
  end;
  FreeAndNil(Tmp);
end;

function ParamsToXML(Params: TParams): string;
var
  CDS : TClientDataSet;
  I   : Integer;
  Tipo: TFieldType;
begin
  Result := '';
  CDS := TClientDataSet.Create(nil);
  try
    for I := 0 to Params.Count - 1 do begin
      Tipo := Params[I].DataType;
      if Tipo = ftWideString then
        Tipo := ftString;
      if (Tipo = ftString) and (Params[I].Size = 0) then
        if Length(Params[I].AsString) = 0 then
          // ToDo: Esto puede causar p�rdida de datos si se guardan cadenas de mas de 40 caracteres en los campos
          Params[I].Size := 40
        else
          Params[I].Size := Length(Params[I].AsString);
      CDS.FieldDefs.Add(Params[I].Name, Tipo, Params[I].Size);
    end;
    CDS.CreateDataSet;
    CDS.Open;
    CDS.Append;
    for I := 0 to Params.Count - 1 do
      CDS.Fields[I].Value := Params[I].Value;
    CDS.Post;
    Result := CDS.XMLData; // DataSetToXML(CDS)
  except
    on e:Exception do
      Windows.MessageBoxA(GetDesktopWindow, PAnsiChar(e.ClassName + ': ' + e.Message), 'ParamsToXML', 0);
  end;
  FreeAndNil(CDS);
end;

procedure XMLToParams(XML: string; AParams: TParams);
var
  CDS  : TClientDataSet;
  I    : Integer;
  Param: TParam;
begin
  CDS := TClientDataSet.Create(nil);
  CDS.FieldDefs.Add('Dummy', ftInteger);
  CDS.CreateDataSet;
  XMLToDataSet(XML, CDS);
  for I := 0 to CDS.Fields.Count - 1 do begin
    Param          := CDS.Params.Add as TParam;
    Param.Name     := CDS.FieldDefs[I].Name;
    Param.DataType := CDS.FieldDefs[I].DataType;
    Param.Size     := CDS.FieldDefs[I].Size;
  end;
  CDS.First;
  for I := 0 to CDS.FieldDefs.Count - 1 do
    CDS.Params[I].Value := CDS.Fields[I].Value;
  AParams.Assign(CDS.Params);
  FreeAndNil(CDS);
end;

function SortClientDataSet(ClientDataSet: TClientDataSet; const FieldName: String; IdxOptions: TIndexOptions): Boolean;
var
  i: Integer;
  NewIndexName: String;
  Field: TField;
begin
  Result := False;
  try
    Field := ClientDataSet.Fields.FindField(FieldName);
    if Field = nil then
      Exit;
    if (Field is TObjectField) or (Field is TBlobField) or (Field is TAggregateField) or (Field is TVariantField) or (Field is TBinaryField) then
      Exit;

    ClientDataSet.IndexDefs.Update;
    if ixDescending in IdxOptions then
      NewIndexName := FieldName + '__IdxD'
    else
      NewIndexName := FieldName + '__IdxA';

    for i := 0 to Pred(ClientDataSet.IndexDefs.Count) do
      if ClientDataSet.IndexDefs[i].Name = NewIndexName then begin
        Result := True;
        Break
      end;
    if not Result then begin
      ClientDataset.IndexDefs.Add(NewIndexName, FieldName, IdxOptions);
    end;
    ClientDataSet.IndexName := NewIndexName;
    Result := True;
  except
    on e:Exception do
      DataBaseError('Error al ordenar DataSet. ' + e.ClassName + ': ' + e.Message, ClientDataSet);
  end;
end;

procedure DisplayParams(Params: TParams);
var
  I: Integer;
  s: string;
begin
  s := '';
  for I := 0 to Params.Count - 1 do begin
    if s <> '' then
      s := s + sLineBreak;
    with Params[I] do
      s := s + Format('%d) %s: %s [%d]= "%s"', [I, Name, FieldTypeNames[DataType], Size, AsString])
  end;
  Windows.MessageBoxA(GetDesktopWindow, PAnsiChar(s), PAnsiChar('DisplayParams: ' + Params.ClassName), 0);
end;

procedure DisplayFields(DS: TDataSet);
var
  I, R: Integer;
  s: string;
begin
  s := '';
  R := 0;
  DS.First;
  while not DS.Eof do begin
    Inc(R);
    s := s + Format('Registro [%d]', [R]) + StringOfChar('-', 50) + sLineBreak;
    for I := 0 to DS.FieldCount - 1 do
      with DS.Fields[I] do
        s := s + Format('    %d) %s: %s [%d]= "%s"', [I, Name, FieldTypeNames[DataType], Size, AsString]) + sLineBreak;
    DS.Next;
  end;
  DS.First;
  Windows.MessageBoxA(GetDesktopWindow, PAnsiChar(s), 'DisplayFields', 0);
end;

end.
