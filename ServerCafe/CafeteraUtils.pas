unit CafeteraUtils;

interface

uses
  SysUtils, DB, DBClient, Classes, TypInfo, Variants, Dialogs, Registry, Windows, Controls, Xml.XMLDoc;

var
  IsDesktopMode: Boolean;
  IsInstalling : Boolean;
  ServiceName  : string;

  function IsServiceRunning(Instance: string): Boolean;
  function GetSpecialFolderPath(Folder: integer): string;
  function ApplicationIni: string;

  // Trabajo con DataSets
  procedure CopyStructure(SourceDataset, DestDataset: TDataset; doAdd: Boolean = False);
  procedure CloneDataSet(Source: TDataset; Target: TDataset);
  function DataSetToXML(DS: TDataset): string;
  procedure XMLToDataSet(XML: string; DS: TDataset); overload;
  function XMLToDataSet(XML: string): TDataset; overload;
  function XMLToDataSetUsingTmp(XML: string): TDataset;
  function ParamsToXML(Params: TParams): string;
  procedure XMLToParams(XML: string; AParams: TParams);
  function SortClientDataSet(ClientDataSet: TClientDataSet; const FieldName: string; IdxOptions: TIndexOptions): Boolean;
  procedure DisplayParams(Params: TParams);
  procedure DisplayFields(DS: TDataset);

  // Registro de Windows
  function GetRegistryString(const sKey, sName: string; var sValue: string): Boolean;
  function GetRegistryDateTime(const sKey, sName: string; var dValue: TDateTime): Boolean;
  function SetRegistryString(const sKey, sName, dValue: string): Boolean;
  function SetRegistryDateTime(const sKey, sName: string; const dValue: TDateTime): Boolean;

  function TressInstalado: Boolean;
  function WriteEventLog(AEntry: string; AServer: string = ''; ASource: string = 'ELog';
    AEventType: word = EVENTLOG_INFORMATION_TYPE; AEventId: word = 0; AEventCategory: word = 0): Boolean;
  procedure EnableControls(Container: TWinControl; Enable: Boolean);
  function ExceptionAsString(e: Exception): string;
  function StartProcess(ExeName: string; CmdLineArgs: string = ''; ShowWindow: Boolean = False; WaitForFinish: Boolean = False): integer;
  function GetInstanceParam(UseUnderscore: Boolean = False): string;
  function GetInstanceName: string;

implementation

uses
  SvcMgr, Forms, WinSvc, SHFolder, ZetaRegistryServer, ZetaDialogo, CafeteraConsts;

const
  NotAllowed: array[0..3] of string = ('/', '\', '.', ' ');

function IsServiceRunning(Instance: string): Boolean;
var
  Svc   : integer;
  SvcMgr: integer;
  ServSt: TServiceStatus;
begin
  Result := False;
  SvcMgr := OpenSCManager(nil, nil, SC_MANAGER_CONNECT);
  if SvcMgr = 0 then
    Exit;
  try
    Svc := OpenService(SvcMgr, PChar(Instance), SERVICE_QUERY_STATUS);
    if Svc = 0 then
      Exit;
    try
      if not QueryServiceStatus(Svc, ServSt) then begin
        Exit;
      end;
      Result := (ServSt.dwCurrentState = SERVICE_RUNNING) or (ServSt.dwCurrentState = SERVICE_START_PENDING);
    finally
      CloseServiceHandle(Svc);
    end;
  finally
    CloseServiceHandle(SvcMgr);
  end;
end;

{* Regresa el nombre del archivo ini del ejecutable, la carpeta depende del sistema operativo (CSIDL_COMMON_APPDATA).
   @author // http://delphi.about.com/od/kbwinshell/a/SHGetFolderPath.htm }
function ApplicationIni: string;
begin
  // All Users\Application Data\App\<App>.ini
  Result := IncludeTrailingPathDelimiter(GetSpecialFolderPath(CSIDL_COMMON_APPDATA)) + 'Grupo Tress\' +
    ChangeFileExt(ExtractFileName(ParamStr(0)), '');
  if ForceDirectories(Result) then
    Result := Result + '\' + ChangeFileExt(ExtractFileName(ParamStr(0)), '');
  Result   := Result + '.ini';
end;

function GetSpecialFolderPath(Folder: integer): string;
const
  SHGFP_TYPE_CURRENT = 0;
var
  Path: array [0 .. MAX_PATH] of char;
begin
  if Succeeded(SHGetFolderPath(0, Folder, 0, SHGFP_TYPE_CURRENT, @Path[0])) then
    Result := Path
  else
    Result := ExcludeTrailingPathDelimiter(ExtractFileDir(ParamStr(0)));
end;

procedure SetProp(Name: string; Source, Result: TComponent);
var
  V       : Variant;
  PropInfo: PPropInfo;
begin
  PropInfo := TypInfo.GetPropInfo(Source, Name);
  if PropInfo <> nil then
    try
      V := TypInfo.GetPropValue(Source, Name);
      if not VarIsNull(V) then
        TypInfo.SetPropValue(Result, Name, V);
    except
      ; // just kill exception
    end;
end;

function CloneField(Source: TField; AOwner: TComponent): TField;
var
  i: integer;
const
  CloneProperty: array [0 .. 18] of string = ('EditMask', 'FixedChar', 'Size', 'Transliterate', 'DisplayFormat', 'EditFormat',
    'Currency', 'MaxValue', 'MinValue', 'Precision', 'DisplayValues', 'BlobType', 'ObjectType', 'IncludeObjectField',
    'ReferenceTableName', 'Active', 'Expression', 'GroupingLevel', 'IndexName');
begin
  Result                        := TFieldClass(Source.ClassType).Create(AOwner);
  Result.Alignment              := Source.Alignment;
  Result.AutoGenerateValue      := Source.AutoGenerateValue;
  Result.CustomConstraint       := Source.CustomConstraint;
  Result.ConstraintErrorMessage := Source.ConstraintErrorMessage;
  Result.DefaultExpression      := Source.DefaultExpression;
  Result.DisplayLabel           := Source.DisplayLabel;
  Result.DisplayWidth           := Source.DisplayWidth;
  Result.FieldKind              := Source.FieldKind;
  Result.FieldName              := Source.FieldName;
  Result.ImportedConstraint     := Source.ImportedConstraint;
  Result.LookupDataSet          := Source.LookupDataSet;
  Result.LookupKeyFields        := Source.LookupKeyFields;
  Result.LookupResultField      := Source.LookupResultField;
  Result.KeyFields              := Source.KeyFields;
  Result.LookupCache            := Source.LookupCache;
  Result.ProviderFlags          := Source.ProviderFlags;
  Result.ReadOnly               := Source.ReadOnly;
  Result.Required               := Source.Required;
  Result.Visible                := Source.Visible;

  for i := Low(CloneProperty) to High(CloneProperty) do begin
    SetProp(CloneProperty[i], Source, Result);
  end;
end;

procedure CopyStructure(SourceDataset, DestDataset: TDataset; doAdd: Boolean = False);
var
  i  : integer;
  Fld: TField;
begin
  if not doAdd then begin
    DestDataset.FieldDefs.Clear;
    DestDataset.Fields.Clear;
  end;
  for i := 0 to SourceDataset.Fields.Count - 1 do begin
    if Assigned(DestDataset.Fields.FindField(SourceDataset.Fields[i].FieldName)) then
      Continue;
    Fld         := CloneField(SourceDataset.Fields[i], DestDataset.Fields.Dataset);
    Fld.Dataset := DestDataset.Fields.Dataset;
  end;
end;

procedure CloneDataSet(Source: TDataset; Target: TDataset);
var
  i: integer;
begin
  with Target as TCustomClientDataSet do begin
    Close;
    CopyStructure(Source, Target);
    CreateDataSet;
    LogChanges := False;
    Open;
  end;
  Source.Open;
  Source.First;
  while not Source.Eof do begin
    Target.Insert;
    for i := 0 to Target.Fields.Count - 1 do begin
      if Target.Fields[i].ReadOnly then
        Target.Fields[i].ReadOnly := False;
      Target.Fields[i].Value      := Source.Fields[i].Value;
    end;
    Target.Post;
    Source.Next;
  end;
  Source.First;
  Target.First;
end;

function DataSetToXML(DS: TDataset): string;
var
  Tmp    : TStringStream;
  s      : string;
  CDS    : TClientDataSet;
  FreeCDS: Boolean;
begin
  Tmp     := nil;
  Result  := '';
  FreeCDS := False;
  if DS.InheritsFrom(TClientDataSet) then begin
    CDS := DS as TClientDataSet;
    CDS.Open;
  end else begin
    CDS     := TClientDataSet.Create(nil);
    FreeCDS := True;
    CloneDataSet(DS, CDS);
  end;
  try
    Tmp := TStringStream.Create(s);
    CDS.MergeChangeLog;
    CDS.SaveToStream(Tmp, dfXMLUTF8);
    Result := Tmp.DataString;
    Result := FormatXMLData( Result );

  finally
    FreeAndNil(Tmp);
    if FreeCDS then
      FreeAndNil(CDS)
    else
      CDS.First;
  end;
end;

procedure XMLToDataSet(XML: string; DS: TDataset);
var
  Tmp: TStringStream;
begin
  Tmp := nil;
  with DS as TClientDataSet do try
    Open;
    EmptyDataSet;
    Close;
    FieldDefs.Clear;
    Tmp := TStringStream.Create(XML);
    Tmp.Seek(0, soBeginning);
    LoadFromStream(Tmp);
    Open;
  finally
    FreeAndNil(Tmp)
  end;
end;

function XMLToDataSet(XML: string): TDataset;
var
  Tmp: TStringStream;
begin
  Tmp    := nil;
  Result := TClientDataSet.Create(nil);
  with Result as TClientDataSet do
    try
      FieldDefs.Add('Dummy', ftInteger);
      CreateDataSet;
      Tmp := TStringStream.Create(XML);
      Tmp.Seek(0,soBeginning);
      LoadFromStream(Tmp);
      Open;
    finally
      FreeAndNil(Tmp);
    end;
end;

function XMLToDataSetUsingTmp(XML: string): TDataset;
var
  tmpFile : TextFile;
  Tmp: TStringStream;
begin
  Tmp    := nil;
  AssignFile(tmpFile, 'TEMP_REP.XML');
  ReWrite(tmpFile);
  Write( tmpFile, XML);
  CloseFile( tmpFile );


  Result := TClientDataSet.Create(nil);
  with Result as TClientDataSet do
    try
      FieldDefs.Add('Dummy', ftInteger);
      CreateDataSet;
      Tmp := TStringStream.Create(XML);
      Tmp.Seek(0,soBeginning);
      LoadFromFile('TEMP_REP.XML');
      Open;
    finally
      FreeAndNil(Tmp);
    end;
end;


function ParamsToXML(Params: TParams): string;
var
  CDS : TClientDataSet;
  i   : integer;
  Tipo: TFieldType;
begin
  Result := '';
  if not Assigned(Params) then
    Exit;

  CDS := TClientDataSet.Create(nil);
  try
    i := 0;
    while i < Params.Count do begin
      if (UpperCase(Params[i].Name) = 'DUMMY') and (Params.Count > 1) then begin
        Params.Delete(i);
        Continue;
      end;
      Tipo := Params[i].DataType;
      if Tipo = ftWideString then
        Tipo := ftString;
      if (Tipo = ftString) then begin
        if Params[i].Size = 0 then
          Params[i].Size := 40;
        if (Length(Params[i].AsString) > Params[i].Size) then
          Params[i].Size := Length(Params[i].AsString);
      end;
      CDS.FieldDefs.Add(Params[i].Name, Tipo, Params[i].Size);
      Inc(i);
    end;
    CDS.CreateDataSet;
    CDS.Open;
    CDS.Append;
    for i := 0 to CDS.Fields.Count - 1 do
      CDS.Fields[i].Value := Params[i].Value;
    CDS.Post;
    Result := CDS.XMLData; // DataSetToXML(CDS)
  finally
    FreeAndNil(CDS);
  end;
end;

procedure XMLToParams(XML: string; AParams: TParams);
var
  CDS  : TClientDataSet;
  i    : integer;
  Param: TParam;
begin
  CDS := TClientDataSet.Create(nil);
  CDS.FieldDefs.Add('Dummy', ftInteger);
  CDS.CreateDataSet;
  XMLToDataSet(XML, CDS);
  for i := 0 to CDS.Fields.Count - 1 do begin
    Param          := CDS.Params.Add as TParam;
    Param.Name     := CDS.FieldDefs[i].Name;
    Param.DataType := CDS.FieldDefs[i].DataType;
    Param.Size     := CDS.FieldDefs[i].Size;
  end;
  CDS.First;
  for i := 0 to CDS.FieldDefs.Count - 1 do
    CDS.Params[i].Value := CDS.Fields[i].Value;
  AParams.Assign(CDS.Params);
  FreeAndNil(CDS);
end;

function SortClientDataSet(ClientDataSet: TClientDataSet; const FieldName: string; IdxOptions: TIndexOptions): Boolean;
var
  i           : integer;
  NewIndexName: string;
  Field       : TField;
begin
  Result := False;
  try
    Field := ClientDataSet.Fields.FindField(FieldName);
    if Field = nil then
      Exit;
    if (Field is TObjectField) or (Field is TBlobField) or (Field is TAggregateField) or (Field is TVariantField) or
      (Field is TBinaryField) then
      Exit;

    ClientDataSet.IndexDefs.Update;
    if ixDescending in IdxOptions then
      NewIndexName := FieldName + '__IdxD'
    else
      NewIndexName := FieldName + '__IdxA';

    for i := 0 to Pred(ClientDataSet.IndexDefs.Count) do
      if ClientDataSet.IndexDefs[i].Name = NewIndexName then begin
        Result := True;
        Break
      end;
    if not Result then begin
      ClientDataSet.IndexDefs.Add(NewIndexName, FieldName, IdxOptions);
    end;
    ClientDataSet.IndexName := NewIndexName;
    Result                  := True;
  finally

  end;
end;

procedure DisplayParams(Params: TParams);
var
  i: integer;
  s: string;
begin
  s := '';
  for i := 0 to Params.Count - 1 do begin
    if s <> '' then
      s := s + sLineBreak;
    with Params[i] do
      s := s + Format('%d) %s: %s [%d]= "%s"', [i, Name, FieldTypeNames[DataType], Size, AsString])
  end;
  MessageDlg(Format('DisplayParams(%s) %s', [Params.ClassName, sLineBreak + s]), mtInformation, [mbOK], 0);
end;

procedure DisplayFields(DS: TDataset);
var
  i, R: integer;
  s   : string;
begin
  s := '';
  R := 0;
  DS.First;
  while not DS.Eof do begin
    Inc(R);
    s := s + Format('Registro [%d]', [R]) + StringOfChar('-', 50) + sLineBreak;
    for i := 0 to DS.FieldCount - 1 do
      with DS.Fields[i] do
        s := s + Format('    %d) %s: %s [%d]= "%s"', [i, Name, FieldTypeNames[DataType], Size, AsString]) + sLineBreak;
    DS.Next;
  end;
  DS.First;
  MessageDlg(Format('DisplayFields() %s', [sLineBreak + s]), mtInformation, [mbOK], 0);
end;

function GetRegistryString(const sKey, sName: string; var sValue: string): Boolean;
begin
  Result := False;
  sValue := '';
  with TRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      Result  := OpenKey(sKey, False) and ValueExists(sName);
      if Result then begin
        sValue := ReadString(sName);
      end
    finally
      Free;
    end;
  end;
end;

function GetRegistryDateTime(const sKey, sName: string; var dValue: TDateTime): Boolean;
begin
  Result := False;
  dValue := 0;
  with TRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      Result  := OpenKey(sKey, False) and ValueExists(sName);
      if Result then begin
        dValue := ReadDateTime(sName);
      end
    finally
      Free;
    end;
  end;
end;

function SetRegistryString(const sKey, sName, dValue: string): Boolean;
begin
  Result := False;
  with TRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey(sKey, True) then begin
        WriteString(sName, dValue);
        Result := True;
      end
    finally
      Free;
    end;
  end;
end;

function SetRegistryDateTime(const sKey, sName: string; const dValue: TDateTime): Boolean;
begin
  Result := False;
  with TRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey(sKey, True) then begin
        WriteDateTime(sName, dValue);
        Result := True;
      end
    finally
      Free;
    end;
  end;
end;

function TressInstalado: Boolean;
var
  FRegistry: TZetaRegistryServer;
begin
  try
    FRegistry := TZetaRegistryServer.Create(False);
    Result    := Trim(FRegistry.DataBase) <> ''
  except
    Result := False;
  end;
  FreeAndNil(FRegistry);
end;

// Fuente: http://edn.embarcadero.com/article/40404
function WriteEventLog(AEntry: string; AServer: string = ''; ASource: string = 'ELog';
  AEventType: word = EVENTLOG_INFORMATION_TYPE; AEventId: word = 0; AEventCategory: word = 0): Boolean;
var
  EventLog: integer;
  P       : Pointer;
begin
  Result := False;
  P      := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(AEntry);
  if Length(AServer) = 0 then // Write to the local machine
    EventLog := RegisterEventSourceA(nil, PAnsiChar(ASource))
  else // Write to a remote machine
    EventLog := RegisterEventSourceA(PAnsiChar(AServer), PAnsiChar(ASource));
  if EventLog <> 0 then
    try
      ReportEvent(EventLog, // event log handle
        AEventType,         // event type
        AEventCategory,     // category zero
        AEventId,           // event identifier
        nil,                // no user security identifier
        1,                  // one substitution string
        0,                  // no data
        @P,                 // pointer to string array
        nil);               // pointer to data
      Result := True;
    finally
      DeregisterEventSource(EventLog);
    end;
end;

procedure EnableControls(Container: TWinControl; Enable: Boolean);
var
  I: Integer;
  ChildControl: TControl;
begin
  for I:= 0 to Container.ControlCount -1 do begin
    ChildControl := Container.Controls[I];
    ChildControl.Enabled := Enable;
    if ChildControl.InheritsFrom(TWinControl) then
      EnableControls(ChildControl as TWinControl, Enable)
  end;
  Container.Enabled := Enable;
end;

function ExceptionAsString(e: Exception): string;
begin
  Result := e.ClassName + ': ' + e.Message;
end;

//Fuente: http://stackoverflow.com/questions/4100840/createprocess-returns-immediately-but-only-if-the-started-process-is-hidden
function StartProcess(ExeName: string; CmdLineArgs: string = ''; ShowWindow: Boolean = False; WaitForFinish: Boolean = False): integer;
var
  StartInfo: TStartupInfo;
  ProcInfo : TProcessInformation;
begin
  FillChar(StartInfo, SizeOf(TStartupInfo), #0);
  FillChar(ProcInfo, SizeOf(TProcessInformation), #0);
  StartInfo.cb := SizeOf(TStartupInfo);

  if not(ShowWindow) then begin
    StartInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartInfo.wShowWindow := SW_HIDE;
  end;

  try
    CreateProcess(nil, PChar(ExeName + ' ' + CmdLineArgs), nil, nil, False, CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS, nil,
      nil, StartInfo, ProcInfo);
    Result := ProcInfo.dwProcessId;

    if WaitForFinish then
      try
        WaitForSingleObject(ProcInfo.hProcess, INFINITE);
      except
        on e: Exception do
          if IsDesktopMode then
            ZetaDialogo.ZExcepcion('StartProcess()', 'Error al iniciar proceso.', e, 0)
          else
            WriteEventLog(ExceptionAsString(e))
      end;

    CloseHandle(ProcInfo.hProcess);
    CloseHandle(ProcInfo.hThread);
  except
    Result := 0;
  end;
end;

{* Extraer el nombre de la instancia, si es el mismo que el nombre base, se conserva el base. @author Ricardo Carrillo}
function GetInstanceParam(UseUnderscore: Boolean = False): string;
begin
  if FindCmdLineSwitch('INSTANCIA', Result) then begin
    if UpperCase(Result) = UpperCase(SERVICE_BASE_NAME) then
      Result := '';
    if UseUnderscore and (Result <> '') then
      Result := '_' + Result;
  end;
end;

function GetInstanceName: string;
var
  i         : integer;
begin
  Result := SERVICE_BASE_NAME + GetInstanceParam(True);

  // Reemplazar caracteres ilegales
  for i := 0 to High(NotAllowed) do
    Result := StringReplace(Result, NotAllowed[i], '_', [rfReplaceAll]);

  // Longitud m�xima
  Result := Copy(Result, 1, 256);

  if (Win32Platform <> VER_PLATFORM_WIN32_NT) or FindCmdLineSwitch('GUI') then begin
    IsInstalling  := False;
    IsDesktopMode := True;
  end else begin
    IsInstalling  := FindCmdLineSwitch('INSTALL') or FindCmdLineSwitch('UNINSTALL');
    IsDesktopMode := not IsInstalling and not IsServiceRunning(Result);
  end;
  ServiceName := Result;
end;

end.
