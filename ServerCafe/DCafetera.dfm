inherited dmCafetera: TdmCafetera
  OldCreateOrder = True
  Height = 263
  Width = 332
  object adsTotales: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 8
  end
  object adsDetalle: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 56
  end
  object adsIdInvita: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 104
  end
  object adsAcceso: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 120
    Top = 8
    object adsAccesoAC_KIND: TIntegerField
      FieldName = 'AC_KIND'
    end
    object adsAccesoAC_LABEL: TStringField
      FieldName = 'AC_LABEL'
      Size = 40
    end
    object adsAccesoAC_VALUE: TStringField
      FieldName = 'AC_VALUE'
      Size = 40
    end
    object adsAccesoAC_TIPO: TIntegerField
      FieldName = 'AC_TIPO'
    end
    object adsAccesoAC_OK: TBooleanField
      FieldName = 'AC_OK'
    end
    object adsAccesoAC_BLOB: TBlobField
      FieldName = 'AC_BLOB'
    end
  end
  object adsHuellas: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 120
    Top = 56
  end
  object adsReportaHuellas: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 120
    Top = 104
  end
  object adsReiniciaHuellas: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 208
    Top = 8
  end
  object adsConfiguracion: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 208
    Top = 64
  end
  object adsCalendario: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 208
    Top = 120
  end
  object adsImportarConfig: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 216
    Top = 176
  end
end
