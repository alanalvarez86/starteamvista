unit AstaSMLauncherUtils;

interface

uses Windows,classes,ExecFile,registry;

type
  TProcessNode = class(TComponent)
  private
    { Private declarations }
  public
    { Public declarations }
    ProcessName : string;
    ProcessHandle : integer;
    exe : TExecFile;
    AutoStart: boolean;
    ReStart: boolean;
    StartOnce: boolean;
    NextProcess : TProcessNode;
    PrevProcess : TProcessNode;
    LastExitCode : integer;
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
  end;

type
  TProcessList = class(TComponent)
  private
    { Private declarations }
  public
    { Public declarations }
    FirstProcess : TProcessNode;
    ProcessCount : integer;
    function StartProcess(Name,FullPath,Arguments:string;AutoStart,ReStart: boolean):integer;
    function ExitCode(Name:string):integer;
    function StopProcess(Name: string):integer;
    function UpdateExitStatus:boolean;
    procedure Restart;
    procedure InitalProcessStart;
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
  end;

type
  TProcessMonitor = class(TThread)
  // This class is used to monitor the processes
  // and to restart if needed
  protected
    delay : integer;
    terminated : boolean;
    constructor Create;
  public
    procedure Execute; override;
  end;

type
  TProcessManager = class(TComponent)
  private
    { Private declarations }
  public
    { Public declarations }
//    Sockets : TSockets;
    UpdateForm: pointer;
    ProcessList : TProcessList;
    ProcessMonitor: TProcessMonitor;
    ClientConnected: string;
    function Start(ProcName:string):integer;
    function StartProcess(Name,FullPath,Arguments:string;AutoStart,ReStart: boolean):integer;
    function StopProcess(Name: string):integer;
    function ExitCode(Name: string):integer;
    function UpdateExitStatus:boolean;
    procedure CheckStatus;
    procedure Restart;
    procedure InitialProcessStart;
    procedure InitializeSocket;
    procedure StartSocket;
    procedure StopSocket;
    procedure ProcessMessage(msg: string;Socket: integer);
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
  end;

var
  ProcessManager: TProcessManager;

function getFld(s : string; fld : integer; separator:char):string;
function FileNameFromPath(Path : string):string;
function OneWord(word : string):boolean;

procedure GetExeListFromRegistry(ExeList : TStrings;ShowIfRunning: boolean);
procedure PutExeIntoRegistry(Name, FullPath, Arguments : string; Start, ReStart : boolean);
procedure GetExeFromRegistry(ExeName : string;var FullPath, Arguments : String;var AutoStart,ReStart : boolean);
procedure DeleteExeFromRegistry(Name:String);
function isRunning(Name:String):boolean;
procedure SetProcessFlags(ProcName:string;AutoStart,AutoReStart:boolean);
procedure startOnce(ProcName:String);
procedure SessionAvailable(Sender: TObject; Socket: integer);
//procedure ReceiveData(Sender: TObject; Socket: integer);
function  unscramble(msg: string):string;
function stripArguments(var Msg: string):string;
function stripNextCommand(var Msg: string):string;

const
  AstaRegKey = 'SOFTWARE\Asta';
  ServiceManagerKey = 'ServiceManager';

  ClientExeKey = 'ClientEXES';
  ClientNameKey = 'Name';
  ClientFullPathKey = 'FullPath';
  ClientArgumentsKey = 'CommandLineArguments';
  ClientAutoStartKey = 'AutoStart';
  ClientAutoReStartKey = 'AutoReStart';
  ClientFirstKey = '_First_';
  ClientLastKey = '_Last_';
  ClientNextKey = 'Next';

  Tab = CHAR(9);

  StoppedProcessExitCode = 101;
  HaltProcessExitCode = -1073741510;
  EndedProcessExitCode = 0;
  RunningProcessExitCode = 259;
  NotStartedProcessExitCode = -1;

  AstaControlPort = '5001';
  CommandStart = char(111);

  MsgStartExe = 'Flip';
  MsgStopExe = 'Flop';
  HKEY_ASTA_APP_MANAGER = HKEY_LOCAL_MACHINE;


implementation

uses sysUtils,MMSystem,AstaServiceMgr,Forms;

function TProcessManager.StartProcess(Name,FullPath,Arguments:string;AutoStart,ReStart: boolean):integer;
begin
  result := ProcessList.StartProcess(Name,FullPath,Arguments,AutoStart,ReStart);
end;

constructor TProcessNode.Create;
begin
  ProcessName := '';
  ProcessHandle := -1;
  LastExitCode := -1;
  exe := TExecFile.Create(Self);
  NextProcess := nil;
  PrevProcess := nil;
  AutoStart := false;
  ReStart := false;
  StartOnce := false;
end;

destructor TProcessNode.Destroy;
begin
  exe.free;
end;

constructor TProcessList.Create(AOwner: TComponent);
begin
  ProcessCount := 0;
end;

destructor TProcessList.Destroy;
var
  stepProcess,thisProcess: TProcessNode;
begin
  if FirstProcess<>nil then begin
    stepProcess := FirstProcess;
    while stepProcess<>nil do begin
      thisProcess := stepProcess;
      stepProcess := stepProcess.NextProcess;
      thisProcess.free;
    end;
  end;
end;

function TProcessManager.ExitCode(Name: string):integer;
begin
  result := ProcessList.ExitCode(Name);
end;

function TProcessList.ExitCode(Name:string):integer;
var
  stepProcess : TProcessNode;
begin
  stepProcess := FirstProcess;
  result := -1;
  while stepProcess<>nil do begin
    if stepProcess.ProcessName=Name then begin
      result := stepProcess.exe.ExitCode;
      stepProcess := nil;
    end else
      stepProcess := stepProcess.NextProcess;
  end;
end;

function TProcessList.StartProcess(Name,FullPath,Arguments:string;AutoStart,ReStart: boolean):integer;
var
  newProcess: boolean;
  stepProcess: TProcessNode;
  exitCode: integer;
begin
  result := -1;
  stepProcess := nil;
  if ProcessCount=0 then begin
    newProcess := TRUE;
    FirstProcess := TProcessNode.Create(Self);
    FirstProcess.ProcessName := Name;
    FirstProcess.exe.CommandLine := FullPath;
    FirstProcess.exe.Parameters := Arguments;
    FirstProcess.exe.Execute;
    FirstProcess.AutoStart := AutoStart;
    FirstProcess.ReStart := ReStart;
    result := FirstProcess.exe.ProcessHandle;
    ProcessCount := ProcessCount + 1;
  end else begin
    newProcess := TRUE;
    stepProcess := FirstProcess;
   (*********** rc Added this code-2-29-2000 ***************)
    while (stepProcess <> NIL) do
    begin
      if (stepProcess.ProcessName = Name) then
      begin
        newProcess := FALSE;
        break;
      end; { if }

      stepProcess := stepProcess.NextProcess;
    end; { while }
    //end rc add
    if (FirstProcess<>nil) and (FirstProcess.ProcessName=Name) then begin
      newProcess := FALSE
    end else begin
      stepProcess := FirstProcess;
      while (stepProcess.NextProcess<>nil) and newProcess do begin
        if stepProcess.ProcessName=Name then
          newProcess := false
        else
          stepProcess := stepProcess.NextProcess;
      end;
      if newProcess then begin
        stepProcess.NextProcess := TProcessNode.Create(self);
        stepProcess := stepProcess.NextProcess;
        stepProcess.ProcessName := Name;
        stepProcess.exe.CommandLine := FullPath;
        stepProcess.exe.Parameters := Arguments;
        stepProcess.AutoStart := AutoStart;
        stepProcess.ReStart := ReStart;
        stepProcess.exe.Execute;
        result := stepProcess.exe.ProcessHandle;
        ProcessCount := ProcessCount + 1;
      end;
    end;
  end;
  if not NewProcess then begin
    ExitCode := stepProcess.exe.ExitCode;
    if ExitCode<>RunningProcessExitCode then begin
      stepProcess.exe.CommandLine := FullPath;
      stepProcess.exe.Parameters := Arguments;
      stepProcess.AutoStart := AutoStart;
      stepProcess.ReStart := ReStart;
      stepProcess.exe.Execute;
      result := FirstProcess.exe.ProcessHandle;
    end;
  end;
end;

function TProcessManager.StopProcess(Name: string):integer;
begin
  result := ProcessList.StopProcess(Name);
end;

function TProcessList.StopProcess(Name: string):integer;
var
  stepProcess : TProcessNode;
begin
  result := -1;
  if FirstProcess<>nil then begin
    stepProcess := FirstProcess;
    while (stepProcess<>nil) do begin
      if stepProcess.ProcessName=Name then begin
        if stepProcess.exe.Terminate then
          result := 1;
        stepProcess := nil;
      end else
        stepProcess := stepProcess.NextProcess;
    end;
  end;
end;

constructor TProcessManager.Create(AOwner: TComponent);
begin
  ProcessList := TProcessList.Create(self);
  UpdateForm := nil;
  ProcessMonitor := TProcessMonitor.Create;
//  Sockets := frmASM.sktOne;
//  InitializeSocket;
//  StartSocket;
end;

destructor TProcessManager.Destroy;
begin
  ProcessList.free;
  if ProcessMonitor<>nil then begin
    ProcessMonitor.Terminated := true;
    ProcessMonitor.Terminate;
    ProcessMonitor.WaitFor;
  end;
end;

procedure GetExeListFromRegistry(ExeList : TStrings;
          ShowIfRunning: boolean);
var
  Reg: TRegistry;
  NextFile: string;
  Auto: string;
  ExitCode: integer;
  Line: string;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_ASTA_APP_MANAGER;

  ExeList.Clear;

  Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey, TRUE);
  NextFile := Reg.ReadString(ClientFirstKey);
  Reg.CloseKey;
  while length(trim(NextFile))>0 do begin
    Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+NextFile, TRUE);

    if Reg.ReadBool(ClientAutoStartKey) then
      Auto := 'AutoStart'
    else
      Auto := '';

    if Reg.ReadBool(ClientAutoReStartKey) then begin
      if length(Auto)>0 then
        Auto := Auto + Tab;
      Auto := Auto + 'AutoReStart';
    end;

    Line := NextFile + Tab;
    if ShowIfRunning then begin
      ExitCode := ProcessManager.ExitCode(NextFile);
      if ExitCode=RunningProcessExitCode then
        Line := Line + 'running' + Tab
      else if ExitCode=HaltProcessExitCode then
        Line := Line + 'halted' + Tab
      else if ExitCode=StoppedProcessExitCode then
        Line := Line + 'stopped' + Tab
      else if ExitCode=EndedProcessExitCode then
        Line := Line + 'exited' + Tab
      else if ExitCode=NotStartedProcessExitCode then
        Line := Line + 'not_started' + Tab
      else
        Line := Line + inttostr(ExitCode) + Tab
    end;

    Line := Line + Reg.ReadString(ClientFullPathKey) + Tab
     + Reg.ReadString(ClientArgumentsKey) + Tab
     + Auto;
    ExeList.Add(Line);
    NextFile := Reg.ReadString(ClientNextKey);
    Reg.CloseKey;
  end;
  Reg.free;
end;

procedure PutExeIntoRegistry(Name, FullPath, Arguments : string; Start, ReStart : boolean);
var
  Reg: TRegistry;
  LastFile: string;
begin

  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_ASTA_APP_MANAGER;

  Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey, TRUE);

  try
    LastFile := Reg.ReadString(ClientLastKey);
  except
  else
    LastFile := Name;
    Reg.WriteString(ClientFirstKey,Name);
  end;

  if length(trim(LastFile))=0 then begin
    LastFile := Name;
    Reg.WriteString(ClientFirstKey,Name);
  end;

  Reg.CloseKey;

  if Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+Name, FALSE) then begin
    // Key already exists, just update values
    Reg.CloseKey;
    Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+Name, TRUE);
    Reg.WriteString(ClientFullPathKey,FullPath);
    Reg.WriteString(ClientArgumentsKey,Arguments);
    Reg.WriteBool(ClientAutoStartKey,Start);
    Reg.WriteBool(ClientAutoReStartKey,Restart);
  end else begin
    // New Key is required
    Reg.CloseKey;
    // Set Lastkey Pointer
    Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey, TRUE);
    Reg.WriteString(ClientLastKey,Name);
    Reg.CloseKey;
    // Create Key set values
    Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+Name, TRUE);
    Reg.WriteString(ClientFullPathKey,FullPath);
    Reg.WriteString(ClientArgumentsKey,Arguments);
    Reg.WriteBool(ClientAutoStartKey,Start);
    Reg.WriteBool(ClientAutoReStartKey,Restart);
    Reg.WriteString(ClientNextKey,'');
    Reg.CloseKey;
    // If Name and Lastfile are identical then this is the only key
    if Name<>LastFile then begin
      // If other keys, find lastkey and set Next Key for linked list
      Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+LastFile,TRUE);
      Reg.WriteString(ClientNextKey,Name);
      Reg.CloseKey;
    end;
  end;
  Reg.free;
end;

procedure DeleteExeFromRegistry(Name:String);
var
  Reg: TRegistry;
  NextFile: string;
  StepThroughFiles: string;
  LastFile: string;
  ExitNow : boolean;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_ASTA_APP_MANAGER;
  if Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+Name,FALSE) then begin
    NextFile := Reg.ReadString(ClientNextKey);
    Reg.CloseKey;
    Reg.DeleteKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+Name);
    Reg.CloseKey;
    if Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey,FALSE) then begin
      StepThroughFiles := Reg.ReadString(ClientFirstKey);
      LastFile := Reg.ReadString(ClientFirstKey);
      Reg.CloseKey;
      ExitNow := FALSE;
      while not ExitNow do begin
        Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+StepThroughFiles,FALSE);
        if Reg.ReadString(ClientNextKey)=Name then begin
          Reg.WriteString(ClientNextKey,NextFile);
          if LastFile=Name then begin
            Reg.CloseKey;
            Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey,FALSE);
            Reg.WriteString(ClientLastKey,StepThroughFiles);
            Reg.CloseKey;
            Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+StepThroughFiles,FALSE);
          end;
          ExitNow := TRUE;
        end;
        StepThroughFiles := Reg.ReadString(ClientNextKey);
        if length(trim(StepThroughFiles))=0 then
          ExitNow := TRUE;
        Reg.CloseKey;
      end;

      Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey,FALSE);
      if Reg.ReadString(ClientFirstKey)=Name then
        Reg.WriteString(ClientFirstKey,NextFile);
      if Reg.ReadString(ClientLastKey)=Name then
        Reg.WriteString(ClientLastKey,'');
      Reg.CloseKey;
    end;
  end;
  Reg.free;
end;


function FileNameFromPath(Path : string):string;
var
  i: integer;
  pt: string;
begin
  // Scans path from right end, stops on "\" or ":"
  result := '';
  pt := trim(Path);
  i := length(pt);
  while i>0 do begin
    if (pt[i]='\') or (pt[i]=':') then
      i := 0
    else
      result := pt[i] + result;
    i := i - 1;
  end;
end;

function OneWord(word : string):boolean;
var
  i: integer;
  wt: string;
begin
  // Scans word returns false if contains space or tab
  result := TRUE;
  wt := trim(word);
  i := length(wt);
  while i>0 do begin
    if (wt[i]=' ') or (wt[i]=Tab) then begin
      result := FALSE;
      i := 0;
    end;
    i := i - 1;
  end;
end;

procedure GetExeFromRegistry(ExeName : string;var FullPath, Arguments : String;var AutoStart,ReStart : boolean);
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_ASTA_APP_MANAGER;

  Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+ExeName, TRUE);
  FullPath := Reg.ReadString(ClientFullPathKey);
  Arguments := Reg.ReadString(ClientArgumentsKey);
  AutoStart := Reg.ReadBool(ClientAutoStartKey);
  ReStart := Reg.ReadBool(ClientAutoReStartKey);
  Reg.CloseKey;
  Reg.Free;
end;

function getFld(s : string; fld : integer; separator:char):string;
var
  i: integer;
  fldpos: integer;
begin
  // Scans word returns false if contains space or tab
  result := '';
  i := 1;
  fldpos := 1;
  while i<=length(s) do begin
    if S[i]=separator then
      fldpos := fldpos + 1
    else
      if fldpos=fld then
        result := result + s[i];
    if fldpos>fld then
      i := length(s);
    i := i + 1;
  end;
end;

constructor TProcessMonitor.Create;
begin
  delay := 4000;
  terminated := FALSE;
  FreeOnTerminate := TRUE;
  inherited Create(false);
end;

procedure TProcessMonitor.Execute;
var
  lStartTime : integer;
begin
  while not terminated do begin
    lStartTime := timeGetTime;
    // Loop until delay time
    while timeGetTime - lStartTime < delay do begin
      Application.ProcessMessages;
      sleep(1);
    end;
    // Check Status of
    Synchronize(ProcessManager.CheckStatus);

    lStartTime := timeGetTime;
    while timeGetTime - lStartTime < delay do begin
      Application.ProcessMessages;
      sleep(1);
    end;
    // Re-Start Programs
    Synchronize(ProcessManager.ReStart);
  end;
end;

procedure TProcessManager.CheckStatus;
begin
  if UpdateForm<>nil then
    TfrmASM(UpdateForm).RefreshIfChange;
end;

procedure TProcessManager.Restart;
begin
  ProcessList.Restart;
end;

procedure TProcessList.Restart;
var
  stepProcess : TProcessNode;
begin
  if FirstProcess<>nil then begin
    stepProcess := FirstProcess;
    while (stepProcess<>nil) do begin
      if (stepProcess.StartOnce
      and (stepProcess.exe.ExitCode<>RunningProcessExitCode))
      or (stepProcess.ReStart
      and (stepProcess.exe.ExitCode<>RunningProcessExitCode)
      and (stepProcess.exe.ExitCode<>NotStartedProcessExitCode)
      and (stepProcess.exe.ExitCode<>StoppedProcessExitCode)) then begin
          ProcessManager.Start(stepProcess.ProcessName);
          stepProcess.StartOnce := FALSE;
          stepProcess := nil;
      end else
        stepProcess := stepProcess.NextProcess;
    end;
  end;
end;

function TProcessManager.UpdateExitStatus:boolean;
begin
  Result := ProcessList.UpdateExitStatus;
end;

function TProcessList.UpdateExitStatus:boolean;
var
  stepProcess : TProcessNode;
begin
  result := false;
  if FirstProcess<>nil then begin
    stepProcess := FirstProcess;
    while (stepProcess<>nil) do begin
      if stepProcess.LastExitCode<>stepProcess.exe.ExitCode then begin
        stepProcess.LastExitCode := stepProcess.exe.ExitCode;
        result := TRUE;
      end;
      stepProcess := stepProcess.NextProcess;
    end;
  end;
end;

function isRunning(Name:String):boolean;
begin
  if ProcessManager.ExitCode(Name)=RunningProcessExitCode then
    result := TRUE
  else
    result := FALSE;
end;

procedure SetProcessFlags(ProcName:string;AutoStart,AutoReStart:boolean);
var
  stepProcess : TProcessNode;
begin with ProcessManager.ProcessList do begin
  if FirstProcess<>nil then begin
    stepProcess := FirstProcess;
    while (stepProcess<>nil) do begin
      if stepProcess.ProcessName=ProcName then begin
        stepProcess.AutoStart := AutoStart;
        stepProcess.ReStart   := AutoReStart;
        stepProcess := nil;
      end else
        stepProcess := stepProcess.NextProcess;
    end;
  end;
end;end;

procedure startOnce(ProcName:String);
var
  stepProcess: TProcessNode;
  notStarted: boolean;
  FullPath : string;
  Arguments : string;
  AutoStart : boolean;
  AutoReStart : boolean;
begin with ProcessManager.ProcessList do begin
  notStarted := TRUE;
  if FirstProcess<>nil then begin
    stepProcess := FirstProcess;
    while (stepProcess<>nil) do begin
      if stepProcess.ProcessName=ProcName then begin
        stepProcess.StartOnce := TRUE;
        stepProcess := nil;
        notStarted := false;
      end else
        stepProcess := stepProcess.NextProcess;
    end;
  end;
  if notStarted then begin
    GetExeFromRegistry(ProcName,FullPath,Arguments,AutoStart,AutoReStart);
    ProcessManager.StartProcess(ProcName,FullPath,Arguments,AutoStart,AutoReStart);
  end;
end;end;

function TProcessManager.Start(ProcName:string):integer;
var
  FullPath : string;
  Arguments : string;
  AutoStart : boolean;
  AutoReStart : boolean;
begin
  GetExeFromRegistry(ProcName,FullPath,Arguments,AutoStart,AutoReStart);
  Result := StartProcess(ProcName,FullPath,Arguments,AutoStart,AutoReStart);
end;

procedure TProcessManager.InitialProcessStart;
begin
  ProcessList.InitalProcessStart;
end;

procedure TProcessList.InitalProcessStart;
var
  Reg: TRegistry;
  NextFile: string;
begin
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_ASTA_APP_MANAGER;

  Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey, TRUE);
  NextFile := Reg.ReadString(ClientFirstKey);
  Reg.CloseKey;
  while length(trim(NextFile))>0 do begin
    Reg.OpenKey(AstaRegKey+'\'+ServiceManagerKey+'\'+ClientExeKey+'\'+NextFile, TRUE);
    if Reg.ReadBool(ClientAutoStartKey) then
      ProcessManager.Start(NextFile);
    NextFile := Reg.ReadString(ClientNextKey);
    Reg.CloseKey;
  end;
  Reg.free;
end;

procedure TProcessManager.InitializeSocket;
var
  Prt : string;
begin
{  Sockets.IPAddr := Sockets.GetLocalIPAddr;
  Sockets.Port := AstaControlPort;
  Prt := Sockets.Port;}
end;

procedure TProcessManager.StartSocket;
begin;
//  Sockets.SListen;
end;

procedure TProcessManager.StopSocket;
begin;
end;

procedure SessionAvailable(Sender: TObject; Socket: integer);
{var
  new_client: TSocket;
 sPort: string;}
begin
//  new_client := ProcessManager.Sockets.SAccept;
end;

{procedure ReceiveData(Sender: TObject; Socket: integer);
var
  Buf: string;
  MsgLength: integer;
begin
  ProcessManager.Sockets.SocketNumber := Socket;
  buf := ProcessManager.Sockets.Text;
  MsgLength := (integer(buf[2])-1) * 255;
  MsgLength := MsgLength + integer(buf[3]) - 1;
  if (length(buf)>3) and (buf[1]=CommandStart) then
    if length(buf)-3=MsgLength then
      ProcessManager.ProcessMessage(buf,Socket);
end;
}
procedure TProcessManager.ProcessMessage(msg: string;Socket: integer);
var
  PlainMsg: string;
  Command: string;
  Arguments: string;
begin
  PlainMsg := unscramble(msg);
  while length(PlainMsg)>0 do begin
    Command   := stripNextCommand(PlainMsg);
    Arguments := stripArguments(PlainMsg);
    if Command=MsgStartExe then begin
      Start(Arguments);
    end else if Command=MsgStopExe then begin
      StopProcess(Arguments);
    end;
  end;
end;

function stripArguments(var Msg: string):string;
var
  i : integer;
  temp : string;
  skip : integer;
begin
  result := '';
  i := 1;
  skip := 0;
  while i <= length(Msg) do begin
    if Msg[i]=';' then begin
      i := length(msg);
      skip := skip + 1;
    end else if pos(Msg[i],'(")')=0 then begin
      result := result + Msg[i];
      skip := skip + 1;
    end else begin
      skip := skip + 1;
    end;
    i := i + 1;
  end;
  temp := Msg;
  Msg := '';
  if skip<length(temp) then
    for i:=skip+1 to length(temp) do begin
      Msg := Msg + temp[i];
    end;
end;

function stripNextCommand(var Msg: string):string;
var
  i : integer;
  temp : string;
begin
  result := '';
  i := 1;
  while i <= length(Msg) do begin
    if (Msg[i]<>';') and (Msg[i]<>'(') then begin
      result := result + Msg[i];
    end else begin
      i := length(msg);
    end;
    i := i + 1;
  end;
  temp := Msg;
  Msg := '';
  for i:=length(result)+1 to length(temp) do begin
    Msg := Msg + temp[i];
  end;
end;

function unscramble(msg: string):string;
var
  i : integer;
begin
  result := '';
  for i := 4 to length(msg) do 
    result := result + msg[i];
end;

end.
