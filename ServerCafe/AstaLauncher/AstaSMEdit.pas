unit AstaSMEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TfrmService = class(TForm)
    Label2: TLabel;
    Label3: TLabel;
    cbxAutoStart: TCheckBox;
    cbxReStart: TCheckBox;
    ebxName: TEdit;
    ebxFullPath: TEdit;
    ebxArguments: TEdit;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    lastFullPath : string;
    lastArguments : string;
    lastAutoStart : boolean;
    lastReStart : boolean;
  public
    { Public declarations }
    procedure saveLast(FullPath,Arguments:string;AutoStart,ReStart:boolean);
    procedure SaveChange;
  end;

var
  frmService: TfrmService;

implementation

{$R *.DFM}

uses AstaSMUtils, AstaServiceMgr, MMSystem;

procedure TfrmService.saveLast(FullPath,Arguments:string;AutoStart,ReStart:boolean);
begin
  lastFullPath  := FullPath;
  lastArguments := Arguments;
  lastAutoStart := AutoStart;
  lastReStart   := ReStart;
end;

procedure TfrmService.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmService.Button1Click(Sender: TObject);
begin
  SaveChange;
end;

procedure TfrmService.SaveChange;
begin
  if oneWord(ebxName.Text) then begin
    PutExeIntoRegistry(ebxName.Text,ebxFullPath.Text
      ,ebxArguments.Text,cbxAutoStart.Checked,cbxReStart.Checked);

    // if process flags are changing then reset them
    if ( (lastAutoStart<>cbxAutoStart.checked)
    or (lastReStart<>cbxReStart.checked) ) then
      SetProcessFlags(ebxName.Text,cbxAutoStart.checked,cbxReStart.checked);

    // if process is running, and path or options is changed then shut it down and restart
    if isRunning(ebxName.Text) then begin
      if ( (lastFullPath<>ebxFullPath.Text) or (lastArguments<>ebxArguments.Text) ) then begin
        AstaSMUtils.ProcessManager.StopProcess(ebxName.Text);

        frmASM.RefreshIfChange;

        StartOnce(ebxName.Text);
      end;
    end else begin

    // if process is not running but AutoStart is turned on then start it
      if not lastAutoStart and cbxAutoStart.Checked then begin
        AstaSMUtils.ProcessManager.StartProcess(ebxName.Text,ebxFullPath.Text
          ,ebxArguments.Text,cbxAutoStart.Checked,cbxReStart.Checked)
      end;

    end;
  end;
  frmASM.RefreshList;
//  Close;
end;

procedure TfrmService.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
CanClose:=True;
   if ModalResult = mrCancel then
      case MessageDlg('Save the changes?',
                     mtConfirmation,[mbYes,mbNo,mbCancel],0) of
         mrYes:
            ModalResult := mrOK;
         mrCancel:
            CanClose := false;
      end;
end;

procedure TfrmService.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   if ModalResult = mrOK then
      SaveChange;

end;

procedure TfrmService.BitBtn1Click(Sender: TObject);
begin
close;
end;

end.
