program AstaServerLauncherNTS;

uses
  SvcMgr,
  AAMService in 'AAMService.pas' {AstaAppManager: TService},
  ExecFile in 'ExecFile.pas',
  AstaSMConst in 'AstaSMConst.pas',
  AstaSMUtils in 'AstaSMUtils.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Asta Server Launcher Service';
  Application.CreateForm(TAstaAppManager, AstaAppManager);
  Application.Run;
end.
