unit AstaSMConst;

interface
const

 // Asta Service Manager Constants
  AstaRegKey = 'SOFTWARE\Asta';
  ServiceManagerKey = 'ServiceManager';

  ClientExeKey = 'ClientEXES';
  ClientNameKey = 'Name';
  ClientFullPathKey = 'FullPath';
  ClientArgumentsKey = 'CommandLineArguments';
  ClientAutoStartKey = 'AutoStart';
  ClientAutoReStartKey = 'AutoReStart';
  ClientFirstKey = '_First_';
  ClientLastKey = '_Last_';
  ClientNextKey = 'Next';

  Tab = CHAR(9);

  StoppedProcessExitCode = 101;
  HaltProcessExitCode = -1073741510;
  EndedProcessExitCode = 0;
  RunningProcessExitCode = 259;
  NotStartedProcessExitCode = -1;

  AstaControlPort = '5001';
  CommandStart = char(111);

  MsgStartExe = 'Flip';
  MsgStopExe = 'Flop';

implementation

end.
 