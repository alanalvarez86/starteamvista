unit FCapturaManual;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls,
     DCafetera,
     FCoffeShop,
     ZetaHora,
     ZetaFecha,
     ZetaNumero,
     ZetaDBTextBox;

type
  TCapturaManual = class(TForm)
    PanelInferior: TPanel;
    OK: TBitBtn;
    Salir: TBitBtn;
    RespuestaGB: TGroupBox;
    Respuesta: TLabel;
    PanelControles: TPanel;
    Empleado: TZetaNumero;
    EmpleadoLBL: TLabel;
    FechaLBL: TLabel;
    Fecha: TZetaFecha;
    Hora: TZetaHora;
    HoraLBL: TLabel;
    ComidasLBL: TLabel;
    Comidas: TZetaNumero;
    TipoComida: TComboBox;
    TipoComidaLBL: TLabel;
    InvitadorLBL: TLabel;
    EsInvitacion: TCheckBox;
    Invitador: TZetaNumero;
    Tiempo: TLabel;
    PanelSuperior: TPanel;
    EmpresaLBL: TLabel;
    Empresa: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EmpleadoChange(Sender: TObject);
    procedure EsInvitacionClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FCafetera: TdmCafetera;
    FChanged: Boolean;
    procedure SetChanged( const Value: Boolean );
    procedure SetControls;
    procedure Elapsed( const dStart: TDateTime );
  public
    { Public declarations }
    property Cafetera: TdmCafetera read FCafetera write FCafetera;
  end;

var
  CapturaManual: TCapturaManual;

implementation

uses ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

procedure TCapturaManual.FormCreate(Sender: TObject);
begin
     Fecha.Valor := Now;
     Hora.Valor := FormatDateTime( 'hhmm', Now );
     Comidas.Valor := 1;
     TipoComida.ItemIndex := 0;
     FChanged := True;
end;

procedure TCapturaManual.FormShow(Sender: TObject);
begin
     Cafetera.Llena( Empresa.Items );
     with Empresa do
     begin
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
     SetChanged( False );
     SetControls;
end;

procedure TCapturaManual.SetChanged( const Value: Boolean );
begin
     if ( FChanged <> Value ) then
     begin
          FChanged := Value;
          if FChanged then
          begin
               Ok.Enabled := True;
          end
          else
          begin
               Ok.Enabled := False;
          end;
     end;
end;

procedure TCapturaManual.SetControls;
begin
     if EsInvitacion.Checked then
     begin
          Empleado.Clear;
          Empleado.Enabled := False;
          EmpleadoLBL.Enabled := False;
          Invitador.Enabled := True;
          InvitadorLBL.Enabled := True;
     end
     else
     begin
          Empleado.Enabled := True;
          EmpleadoLBL.Enabled := True;
          Invitador.Clear;
          Invitador.Enabled := False;
          InvitadorLBL.Enabled := False;
     end;
end;

procedure TCapturaManual.EsInvitacionClick(Sender: TObject);
begin
     SetChanged( True );
     SetControls;
     if EsInvitacion.Checked then
        ActiveControl := Invitador
     else
         ActiveControl := Empleado;
end;

procedure TCapturaManual.EmpleadoChange(Sender: TObject);
begin
     SetChanged( True );
end;

procedure TCapturaManual.Elapsed( const dStart: TDateTime );

function TimeInSeconds( dTime: TDateTime ): Extended;
var
   wHour, wMin, wSec, wMSec: Word;
begin
     DecodeTime( dTime, wHour, wMin, wSec, wMSec );
     Result := ( 3600 * wHour ) + ( 60 * wMin ) + wSec + ( wMSec / 1000 );
end;

begin
     Tiempo.Caption := FormatFloat( '###,##0.00', TimeInSeconds( Time ) - TimeInSeconds( dStart ) ) + ' Segs.';
end;

procedure TCapturaManual.OKClick(Sender: TObject);
var
   oCursor: TCursor;
   lAutorizada: Boolean;
   dStart: TDateTime;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     dStart := Time;
     try
        with Cafetera do
        begin
             lAutorizada := False;
             try
                with ChecadaComida do
                begin
                     Empresa := Cafeteria[ Self.Empresa.ItemIndex ].Digito;
                     if Self.EsInvitacion.Checked then
                     begin
                          EsInvitacion := True;
                          Empleado := 0;
                          Invitador := Self.Invitador.ValorEntero;
                          Credencial := 'A';
                     end
                     else
                     begin
                          EsInvitacion := False;
                          Empleado := Self.Empleado.ValorEntero;
                          Invitador := 0;
                          Credencial := ' '; {Para Que Se Lea Automáticamente El # de Credencial}
                     end;
                     EsManual := True;
                     Fecha := Self.Fecha.Valor;
                     Hora := Self.Hora.Valor;
                     TipoComida := Chr( 49 + Self.TipoComida.ItemIndex );
                     NumComidas := Self.Comidas.ValorEntero;
                     Reloj := 'MANU';
                end;
             except
                   on e: Exception do
                   begin
                        ZetaDialogo.zExcepcion( Caption, '¡ Error Al Codificar Checada Manual !', e, 0 );
                   end;
             end;
             if SetCafeteriaActiva then
             begin
                  try
                     lAutorizada := AutorizaChecada;
                     if lAutorizada then
                     begin
                          try
                             EscribeChecada;
                          except
                                on e: Exception do
                                begin
                                     ZetaDialogo.zExcepcion( Caption, '¡ Error Al Grabar Checada Manual !', e, 0 );
                                end;
                          end;
                     end;
                  except
                        on e: Exception do
                        begin
                             ZetaDialogo.zExcepcion( Caption, '¡ Error Al Autorizar Checada Manual !', e, 0 );
                        end;
                  end;
                  with Respuesta do
                  begin
                       Caption := ChecadaComida.Respuesta;
                       Elapsed( dStart );
                       if lAutorizada then
                       begin
                            Font.Color := clBlack;
                            SetChanged( False );
                       end
                       else
                       begin
                            Font.Color := clRed;
                       end;
                  end;
             end
             else
                 ZetaDialogo.zError( Caption, Format( '¡ No Hay Empresa Con Letra %s !', [ ChecadaComida.Empresa ] ), 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
