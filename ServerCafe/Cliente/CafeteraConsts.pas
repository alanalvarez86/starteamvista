unit CafeteraConsts;

interface

const
  // ESTADO_CONEXION: array[Boolean] of string = ('Fuera de l�nea', 'En L�nea');
  ESTADO_CONEXION: array[0..2] of string = ('Fuera de l�nea', 'En l�nea', 'Sincronizaci�n Pendiente');

  // Constantes de CafeteraServicio, Tress Caseta y Tress Cafeteria
  SERVICE_BASE_NAME   = 'CafeteraServicio';
  SERVICE_DISPLAYNAME = 'Servidor de Cafeter�a/Caseta';
  {$IFDEF SERVERCAFE}
  SERVICE_DESCRIPTION = 'Provee servicios de validaci�n de reglas para los clientes Tress Cafeter�a y Tress Caseta';
  {$ENDIF}

  //ACCESOS_REGISTRY_PATH = 'Software\Grupo Tress\TressAccesos';
  //CAFETERIA_REGISTRY_PATH = 'Software\Grupo Tress\TressCafe';

  {$IFDEF SERVERCAFE}
  REG_KEY = 'Software\Grupo Tress\' + SERVICE_BASE_NAME;
  {$ENDIF}
  {$IFDEF SERVERCAFEUI}
  REG_KEY = 'Software\Grupo Tress\' + SERVICE_BASE_NAME + 'UI';
  {$ENDIF}
  {$IFDEF ACCESOS}
  REG_KEY = 'Software\Grupo Tress\TressAccesos';
  {$ENDIF}
  {$IFDEF CAFE}
  REG_KEY = 'Software\Grupo Tress\TressCafe';
  {$ENDIF}

  K_IDENT_SERVIDOR      = 'Servidor';  // Nombre de la clave en el registro
  K_IDENT_INSTANCIA     = 'Instancia'; // Nombre de la instancia en el registro
  K_IDENT_PUERTO        = 'Puerto';    // Nombre de la clave en el registro
  K_IDENT_HORA_REINICIO = 'Hora';      // Nombre de la clave en el registro, Hora de reinicio de reglas
  K_IDENT_INTERVALO     = 'Intervalo'; // Nombre de la clave en el registro, Intervalo de actualizaci�n

  K_CAFETERA_SERVIDOR  = 'localhost';      // Nombre predeterminado del servidor CafeteraServicio
  K_CAFETERA_PUERTO    = 33100;            // Puerto TCP predeterminado del servidor CafeteraServicio
  K_CAFETERA_HORA_REINICIO: TDateTime = 0; // 00:00 Hora predeterminada de reinicio de reglas del servidor CafeteraServicio
  K_CAFETERA_INTERVALO = 30;               // Actualizar estado EN SEGUNDOS
  K_CAFETERA_FUERA_LINEA_TEXT = 0;
  K_CAFETERA_EN_LINEA_TEXT    = 1;
  K_CAFETERA_PENDIENTE_TEXT   = 2;

  P_INSTANCIA  = 'INSTANCIA';
  P_PUERTO     = 'PUERTO';
  P_HORA       = 'HORA';
  P_AUTORESET  = 'AUTORESET';

  K_AUTORIZAR_CHECADA_COMIDA = 0;
  K_LEE_CHECADAS_CAFETERIA   = 1;
  K_LEE_LISTA_INVITADORES    = 2;
  K_VALIDAR_ACCESO           = 3;
  K_OBTENER_HUELLAS          = 4;
  K_BORRAR_RELACION_HUELLAS  = 5;
  K_REPORTAR_HUELLAS         = 6;
  K_OBTENER_LOG              = 7;
  K_RESTART                  = 8;
  K_SET_PARAMETROS           = 9;
  K_GET_PARAMETROS           = 10;
  K_SINCRONIZA_CONFIG        = 11;
  K_ACTUALIZA_CONFIG         = 12;
  K_IMPORTAR_CONFIG          = 13;
  K_VERIFICAR_CONFIG         = 14;
  K_VERIFICAR_SINC           = 15;

  K_CAFETERA_FUERA_DE_LINEA = 1000;
  K_ACCESOS_FUERA_DE_LINEA  = 1100;
  K_ERROR_REPLY             = 1005;
  K_CAFETERA_DEMO           = 1500;
  K_ACCESOS_DEMO            = 1600;

  K_FRECUENCIA_DIARIA = 1;
  K_FRECUENCIA_MINUTOS = 2;

  K_ES_DEMO       = 'DEMO';
  K_NO_DISPONIBLE = 'Servicio No Disponible';
  K_NO_TRESS      = 'Sistema Tress debe estar insladado para poder ejecutar esta aplicaci�n.';

implementation

end.
