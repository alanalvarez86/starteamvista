// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost:33100/wsdl/ICafeteraSOAPWebService
// >Import : http://localhost:33100/wsdl/ICafeteraSOAPWebService>0
// Version  : 1.0
// (11/6/2013 11:41:12 AM - - $Rev: 52705 $)
// ************************************************************************ //

unit CafeteraWSDL;

interface

uses
  InvokeRegistry, SOAPHTTPClient,  Soap.SOAPHTTPTrans,  Soap.OpConvertOptions,  Types, XSBuiltIns,  IdHTTP, IdIOHandlerSocket, IdSSLOpenSSL, wininet,  Soap.WebNode,  System.Classes;

const
K_TIMEOUT_CONNECT_DEF = 15000; // 15 segundos
K_TIMEOUT_TRANSACTION_DEF = 300000; //5 minutos

K_TIMEOUT_CONNECT_REP_DEF = 360000;    //6 minutos
K_TIMEOUT_TRANSACTION_REP_DEF = 3600000; //60 minutos


var
K_TIMEOUT_CONNECT : integer  = K_TIMEOUT_CONNECT_DEF;
K_TIMEOUT_TRANSACTION  : integer = K_TIMEOUT_TRANSACTION_DEF;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]


  TWebServiceConfigSOAP = class( TObject )
public
  FHTTPRIO: THTTPRIO;

  procedure BeforeExecute(const MethodName: string; SOAPRequest: TStream);
end;



  TSOAPDataSet = class; { "urn:ServidorSOAPIntf"[GblCplx] }

// ************************************************************************ //
  // XML       : TSOAPDataSet, global, <complexType>
  // Namespace : urn:ServidorSOAPIntf
  // ************************************************************************ //
  TSOAPDataSet = class(TRemotable)
  private
    FMsgID  : Integer;
    FParams : WideString;
    FDataSet: WideString;
  published
    property MsgID  : Integer    read FMsgID   write FMsgID;
    property Params : WideString read FParams  write FParams;
    property DataSet: WideString read FDataSet write FDataSet;
  end;

// ************************************************************************ //
  // Namespace : urn:ServidorSOAPIntf-ICafeteraSOAPWebService
  // soapAction: urn:ServidorSOAPIntf-ICafeteraSOAPWebService#ValidarAcceso
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // use       : encoded
  // binding   : ICafeteraSOAPWebServicebinding
  // service   : ICafeteraSOAPWebServiceservice
  // port      : ICafeteraSOAPWebServicePort
  // URL       : http://localhost:33100/soap/ICafeteraSOAPWebService
  // ************************************************************************ //
  ICafeteraSOAPWebService = interface(IInvokable)
    ['{9B3E0385-0A55-4A2A-A888-2DF1DC8DCF9A}']
    function Ping: Boolean; stdcall;
    function SendCodedMessage(const Params: TSOAPDataSet): TSOAPDataSet; stdcall;
    function SendCodedMessageXMLResponse(const Params: TSOAPDataSet): WideString; stdcall;
    function SendCodedMessageByte(const  inParams: TSOAPDataSet; var aDetalle, aTotales : TByteDynArray ): Boolean; stdcall;
  end;

function GetICafeteraSOAPWebService(UseWSDL: Boolean = System.False; Addr: string = '';
  HTTPRIO: THTTPRIO = nil): ICafeteraSOAPWebService;

implementation

uses
  SysUtils;

function GetICafeteraSOAPWebService(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): ICafeteraSOAPWebService;
const
  defWSDL = 'http://localhost:33100/wsdl/ICafeteraSOAPWebService';
  defURL  = 'http://localhost:33100/soap/ICafeteraSOAPWebService';
  defSvc  = 'ICafeteraSOAPWebServiceservice';
  defPrt  = 'ICafeteraSOAPWebServicePort';
var
  RIO: THTTPRIO;
  wsConfig : TWebServiceConfigSOAP;
begin
  Result := nil;
    wsConfig  :=   TWebServiceConfigSOAP.Create;

  if (Addr = '') then begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try

   //[soIgnoreInvalidCerts,soAutoCheckAccessPointViaUDDI]


    RIO.HTTPWebNode.InvokeOptions := RIO.HTTPWebNode.InvokeOptions + [ soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI ];
    RIO.Converter.Options := RIO.Converter.Options + [soUTF8InHeader];
    RIO.HTTPWebNode.UseUTF8InHeader := True;

    Result := (RIO as ICafeteraSOAPWebService);
    if UseWSDL then begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end
    else
      RIO.URL := Addr;


    wsConfig.FHTTPRIO := RIO;
    RIO.OnBeforeExecute := wsConfig.BeforeExecute;
    //RIO.OnBeforeExecute :=  wsConfig.HTTPRIOBeforeExecute;

  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;


end;

{ TWebServiceConfigSOAP }

procedure TWebServiceConfigSOAP.BeforeExecute(const MethodName: string; SOAPRequest: TStream);
var
    iTimeOUt, iTimeOutTx : integer;
    SecurityFlags: DWord;
    SecurityFlagsLen: DWord;
    Request: HINTERNET;

begin
    iTimeOut := K_TIMEOUT_CONNECT;
    iTimeOutTx := K_TIMEOUT_TRANSACTION;

    InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@iTimeOut),
    SizeOf(iTimeOut));

    InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@iTimeOutTx),
    SizeOf(iTimeOutTx));

    InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@iTimeOutTx),
    SizeOf(iTimeOutTx));
end;


initialization
  { ICafeteraSOAPWebService }
  InvRegistry.RegisterInterface(TypeInfo(ICafeteraSOAPWebService), 'urn:ServidorSOAPIntf-ICafeteraSOAPWebService', '');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(ICafeteraSOAPWebService), 'urn:ServidorSOAPIntf-ICafeteraSOAPWebService#%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(ICafeteraSOAPWebService), ioDocument);
  RemClassRegistry.RegisterXSClass(TSOAPDataSet, 'urn:ServidorSOAPIntf', 'TSOAPDataSet');
  K_TIMEOUT_CONNECT := K_TIMEOUT_CONNECT_DEF;
  K_TIMEOUT_TRANSACTION := K_TIMEOUT_TRANSACTION_DEF;

end.
