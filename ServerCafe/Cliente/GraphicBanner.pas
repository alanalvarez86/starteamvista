{ == PBBanner ========================================================== }
{ : This is an example for a banner component showing the use of
  ScrollWindowEx for smooth and flicker-free scrolling.
  @author Dr. Peter Below
  @desc Version 1.0 created 2004-06-13<BR>
  Last modified 2004-06-13<P>
}
{ ====================================================================== }
{$BOOLEVAL OFF}{ Unit depends on shortcut boolean evaluation }

unit GraphicBanner;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Graphics, ExtCtrls;

type

  TCustomBanner = class(TGraphicControl)
  private
    FTimer         : TTimer;
    FBitmap        : TBitmap;
    FOffset        : Integer;
    FStepsPerSecond: Integer;
    FStepWidth     : Integer;
    function GetActive: Boolean;
    procedure SetActive(const Value: Boolean);
    procedure SetStepsPerSecond(const Value: Integer);
    procedure SetStepWidth(const Value: Integer);
    procedure TimerTick(sender: TObject);
  protected
    procedure AnimationStep; virtual;
    procedure CMFontchanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure CMColorchanged(var Message: TMessage); message CM_COLORCHANGED;
    procedure CMTextchanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure Paint; override;
    procedure PaintBitmapBackground; virtual;
    procedure PaintBitmapText; virtual;
    procedure PaintControlBackground; virtual;
    procedure PaintControlImage; virtual;
    procedure PrepareAnimation; virtual;
    procedure ResetTimer;
    procedure ValidateStepsPerSecond(var Value: Integer); virtual;
    procedure ValidateStepWidth(var Value: Integer); virtual;
    property Timer: TTimer read FTimer;
    property Bitmap: TBitmap read FBitmap;
    property Offset: Integer read FOffset write FOffset;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Reset;
    property Active: Boolean read GetActive write SetActive;
    property StepsPerSecond: Integer read FStepsPerSecond write SetStepsPerSecond;
    property StepWidth: Integer read FStepWidth write SetStepWidth;
    procedure SetBounds(ALeft: Integer; ATop: Integer; AWidth: Integer; AHeight: Integer); override;
  end;

  TBanner = class(TCustomBanner)
  published
    property Active;
    property Align;
    property Anchors;
    property Caption;
    property Color;
    property Font;
    property ParentColor;
    property ParentFont;
    property StepsPerSecond;
    property StepWidth;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Samples', [TBanner]);
end;

function CenterVertical(const rect: TRect; h: Integer): Integer;
begin
  Result := (rect.bottom + rect.top - h) div 2;
end;

{ TCustomBanner }
procedure TCustomBanner.AnimationStep;
var
  R, SourceRect, UpdateRect: TRect;
begin
  if not(Assigned(Parent) and Parent.HandleAllocated) then
    Exit;
  R := ClientRect;
  OffsetRect(R, Left, top);
  ScrollWindowEx(Parent.Handle, -StepWidth, 0, @R, @R, 0, @UpdateRect, 0);
  OffsetRect(UpdateRect, -Left, -top);
  with Canvas.Brush do begin
    Color := Self.Color;
    Style := bsSolid;
  end; { with }
  Canvas.FillRect(UpdateRect);
  R        := UpdateRect;
  R.top    := CenterVertical(R, Bitmap.Height);
  R.bottom := R.top + Bitmap.Height;
  Offset   := Offset + StepWidth;
  if Offset >= Bitmap.Width then
    Offset := -Clientwidth
  else begin
    SourceRect := rect(Offset + Clientwidth - StepWidth, 0, Offset + Clientwidth, Bitmap.Height);
    Canvas.CopyRect(R, Bitmap.Canvas, SourceRect);
  end; { else }
end;

procedure TCustomBanner.CMColorchanged(var Message: TMessage);
begin
  inherited;
  PrepareAnimation;
end;

procedure TCustomBanner.CMFontchanged(var Message: TMessage);
begin
  inherited;
  PrepareAnimation;
end;

procedure TCustomBanner.CMTextchanged(var Message: TMessage);
begin
  inherited;
  PrepareAnimation;
end;

constructor TCustomBanner.Create(AOwner: TComponent);
begin
  FBitmap := TBitmap.Create;
  FTimer  := TTimer.Create(nil);
  inherited;
  Caption         := '';
  ControlStyle    := ControlStyle + [csOpaque] - [csReplicatable, csFramed];
  FTimer.Interval := 50;
  FTimer.Enabled  := false;
  FTimer.OnTimer  := TimerTick;
  FStepsPerSecond := 20;
  FOffset         := ClientWidth;
  FStepWidth      := 1;
  Color           := clBtnFace;
  Font.Color      := clBtnText;
  Font.Name       := 'Arial';
  Font.Size       := 14;
  Font.Style      := [fsBold];
end;

destructor TCustomBanner.Destroy;
begin
  FTimer.Free;
  FBitmap.Free;
  inherited;
end;

function TCustomBanner.GetActive: Boolean;
begin
  Result := FTimer.Enabled;
end;

procedure TCustomBanner.Paint;
begin
  PaintControlBackground;
  PaintControlImage;
end;

procedure TCustomBanner.PaintBitmapBackground;
begin
  with Bitmap.Canvas do
    FillRect(Cliprect);
end;

procedure TCustomBanner.PaintBitmapText;
begin
  Bitmap.Canvas.Brush.Style := bsClear;
  Bitmap.Canvas.TextOut(0, 0, Caption);
end;

procedure TCustomBanner.PaintControlBackground;
begin
  Canvas.Brush.Color := Color;
  Canvas.Brush.Style := bsSolid;
  Canvas.FillRect(ClientRect);
end;

procedure TCustomBanner.PaintControlImage;
begin
  Canvas.Draw(-Offset, CenterVertical(ClientRect, Bitmap.Height), Bitmap);
end;

procedure TCustomBanner.PrepareAnimation;
var
  TextSize: TSize;
begin
  if not Assigned(Bitmap) then
    Exit;
  Bitmap.Canvas.Font        := Font;
  TextSize                  := Bitmap.Canvas.TextExtent(Caption);
  Bitmap.Width              := TextSize.cx + 20; // buffer for italic overhang
  Bitmap.Height             := TextSize.cy;
  Bitmap.Canvas.Brush.Color := Color;
  Bitmap.Canvas.Brush.Style := bsSolid;
  PaintBitmapBackground;
  Bitmap.Canvas.Font := Font;
  PaintBitmapText;
  Reset;
end;

procedure TCustomBanner.Reset;
begin
  FOffset := ClientWidth;
  Invalidate;
end;

procedure TCustomBanner.ResetTimer;
begin
  if FTimer.Enabled then begin
    FTimer.Enabled := false;
    FTimer.Enabled := true;
  end; { if }
end;

procedure TCustomBanner.SetActive(const Value: Boolean);
begin
  FTimer.Enabled := Value;
end;

procedure TCustomBanner.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited;
  PrepareAnimation;
end;

procedure TCustomBanner.SetStepsPerSecond(const Value: Integer);
var
  V: Integer;
begin
  V := Value;
  ValidateStepsPerSecond(V);
  if FStepsPerSecond <> V then begin
    FStepsPerSecond := V;
    FTimer.Interval := Round(1000 / V);
    ResetTimer;
  end; { if }
end;

procedure TCustomBanner.SetStepWidth(const Value: Integer);
var
  V: Integer;
begin
  V := Value;
  ValidateStepWidth(V);
  if FStepWidth <> V then
    FStepWidth := V;
end;

procedure TCustomBanner.TimerTick(sender: TObject);
begin
  AnimationStep;
end;

procedure TCustomBanner.ValidateStepsPerSecond(var Value: Integer);
begin
  if Value <= 0 then
    Value := 1;
  if Value > 100 then
    Value := 100;
end;

procedure TCustomBanner.ValidateStepWidth(var Value: Integer);
begin
  if Value <= 0 then
    Value := 1;
  if Value > (Clientwidth div 10) then
    Value := Clientwidth div 10;
end;

end.
