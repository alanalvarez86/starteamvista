unit DBaseCafeCliente;

interface

uses
  FAsciiServer, CafeteraWSDL, SysUtils, Windows, Forms, Controls, Classes, InvokeRegistry, ExtCtrls, DB, DBClient, Rio,
  SOAPHTTPClient, Dialogs,  System.Win.ComObj, Winapi.ActiveX, Types, Chilkat_v9_5_0_TLB;

type
  TdmBaseCafeCliente = class(TDataModule)
    HTTPRIO: THTTPRIO;
    cdsEmpleado: TClientDataSet;
    ConnTimer: TTimer;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ConnTimerTimer(Sender: TObject);
  private
    { Private declarations }
    oAsciiServer       : TAsciiServer;
    fOnClientConnect   : TNotifyEvent;
    fOnClientDisConnect: TNotifyEvent;
    fOnClientError     : TNotifyEvent;
    fActive            : Boolean;
    fOldActive         : Boolean;
    fOldRunning        : Boolean;
    fAddress           : String;
    fPort              : Integer;
    fOnConnectTimer    : TNotifyEvent;
    FEstacion          : String;
    fServerRunning     : Boolean;
    fInstance          : string;
    fWasTimeOut        : Boolean;

    procedure SetActive(const Value: Boolean);
    function GetWSDL: String;
    function GetSOAPUrl: String;
    procedure SetInterval(const Value: Cardinal);
    function GetInterval: Cardinal;
    procedure SetFEstacionGafete(const Value: String);
    procedure Log(const Message: String; FileName: string = '');
  protected
    FirstRun: Boolean;
  public
    { Public declarations }
    SOAPMsg: TSOAPDataSet;
    Params: TParams;
    procedure Init; virtual;
    function GetServer: ICafeteraSOAPWebService;
    procedure ReleaseServer;
    function CreateSOAPMsg(MsgID: Integer = 0; Params: String = ''; DataSet: String = ''): TSOAPDataSet;
    function SendSOAPMsg(var SOAPMsg: TSOAPDataSet; OutParams: TParams = nil; OutDataset: TDataset = nil;  oServerReuse : ICafeteraSOAPWebService = nil ): Boolean;
    function SendSOAPMsgXML_DetalleTotales(var aDetalle, aTotales : TByteDynArray; var SOAPMsg: TSOAPDataSet; OutParams: TParams = nil; OutDataset: TDataset = nil;  oServerReuse : ICafeteraSOAPWebService = nil ): Boolean;
    procedure Update;
    procedure AgregaChecadaServidor(const sMensaje: String);

    function GetGlobal(const sIdent, sDefault: string): string; overload;
    function GetGlobal(const sIdent: string; const iDefault: Integer): Integer; overload;
    function GetGlobal(const sIdent: string; const dDefault: TDateTime): TDateTime; overload;
    function SetGlobal(const sIdent, sValue: string): Boolean; overload;
    function SetGlobal(const sIdent: string; iValue: Integer): Boolean; overload;
    function SetGlobal(const sIdent: string; const dValue: TDateTime): Boolean; overload;

    property Active: Boolean read fActive write SetActive;
    property AsciiServer: TAsciiServer read oAsciiServer;
    property Estacion: String read FEstacion write SetFEstacionGafete;
    property Direccion: String read fAddress write fAddress;
    property Instance: string read fInstance;
    property Interval: Cardinal read GetInterval write SetInterval;
    property Puerto: Integer read fPort write fPort;
    property ServerRunning: Boolean read fServerRunning;

    property OnClientConnect: TNotifyEvent read fOnClientConnect write fOnClientConnect;
    property OnClientDisConnect: TNotifyEvent read fOnClientDisConnect write fOnClientDisConnect;
    property OnClientError: TNotifyEvent read fOnClientError write fOnClientError;
    property OnConnectTimer: TNotifyEvent read fOnConnectTimer write fOnConnectTimer;
  end;

var
  dmBaseCafeCliente: TdmBaseCafeCliente;

implementation

uses
  ZetaServerTools, CafeteraConsts, CafeteraUtils, ZetaCommonTools;

{$R *.dfm}

procedure TdmBaseCafeCliente.DataModuleCreate(Sender: TObject);
var
  I: Integer;
begin
  FirstRun       := True;
  oAsciiServer   := TAsciiServer.Create;
  fActive        := False;
  fOldActive     := False;
  fServerRunning := False;
  fOldRunning    := False;
  fWasTimeOut    := False;
  cdsEmpleado.FieldDefs.Add('FOTO', ftBlob);

  for I := 0 to ComponentCount - 1 do
    if Components[I].InheritsFrom(TCustomClientDataSet) then
      with TCustomClientDataSet(Components[I]) do begin
        if (FieldDefs.Count = 0) and (Fields.Count = 0) then begin
          FieldDefs.Add('DUMMY', ftInteger);
        end;
        CreateDataSet;
        LogChanges := False;
        Close;
      end;
end;

procedure TdmBaseCafeCliente.DataModuleDestroy(Sender: TObject);
begin
  ConnTimer.Enabled := False;
  oAsciiServer.CloseBuffersOnly;
  FreeAndNil(oAsciiServer);
end;

procedure TdmBaseCafeCliente.Init;
begin
  with oAsciiServer do
  begin
    { GA: Primero hay que cerrar bit�coras previas dado que se puede pasar por aqu� varias }
    { veces durante la vida de una sesi�n del cliente de Cafeter�a }
    CloseBuffersOnly;
    InitBuffersOnly;
  end;
end;

function TdmBaseCafeCliente.GetInterval: Cardinal;
begin
  Result := ConnTimer.Interval div 1000
end;

function TdmBaseCafeCliente.GetWSDL: String;
begin
  Result := Format('http://%s:%d/wsdl/ICafeteraSOAPWebService', [fAddress, fPort]);
end;

function TdmBaseCafeCliente.GetSOAPURL: String;
begin
  Result := Format('http://%s:%d/soap/ICafeteraSOAPWebService', [fAddress, fPort]);
end;


function TdmBaseCafeCliente.CreateSOAPMsg(MsgID: Integer = 0; Params: String = ''; DataSet: String = ''): TSOAPDataSet;
var crypt: TChilkatCrypt2;
begin
  Result         := TSOAPDataSet.Create;
  Result.MsgID   := MsgID;

  {$ifdef ANTES}
  Result.Params  := ZetaServerTools.Encrypt(Params);
  Result.DataSet := ZetaServerTools.Encrypt(DataSet);
  {$endif}

  // Encriptaci�n.
  {Result.Params  := Params;
  Result.DataSet := DataSet;}

  crypt := TChilkatCrypt2.Create(Self);
  crypt.UnlockComponent('TRESSCCrypt_pnFywPZSRHjw');
  crypt.SecretKey := crypt.GenerateSecretKey('cafeteria');

  Result.Params  := Params;
  Result.DataSet := crypt.EncryptStringENC(DataSet);

  try
    FreeAndNil (crypt);
  except
      //ToDo: Excepci�n silenciosa
  end;
end;

function TdmBaseCafeCliente.SendSOAPMsg(var SOAPMsg: TSOAPDataSet; OutParams: TParams = nil; OutDataset: TDataset = nil;  oServerReuse : ICafeteraSOAPWebService = nil ): Boolean;
var
  SOAPRes: TSOAPDataSet;
  oServer : ICafeteraSOAPWebService;
  crypt: TChilkatCrypt2;
begin
  Result := False;
  ConnTimer.Enabled := False;
  try
      try

        if ( oServerReuse = nil )  then
        begin
         // CoInitializeEx(nil, COINIT_MULTITHREADED);
          oServer := GetServer
        end
        else
          oServer := oServerReuse;

        SOAPRes := oServer.SendCodedMessage(SOAPMsg);

        {$ifdef ANTES}
        SOAPRes.Params  := Decrypt(SOAPRes.Params);
        SOAPRes.DataSet := Decrypt(SOAPRes.DataSet);
        {$endif}

        // Encriptaci�n.
        crypt := TChilkatCrypt2.Create(Self);
        crypt.UnlockComponent('TRESSCCrypt_pnFywPZSRHjw');
        crypt.SecretKey := crypt.GenerateSecretKey('cafeteria');

        SOAPRes.Params  := SOAPRes.Params;
        SOAPRes.DataSet := crypt.DecryptStringENC(SOAPRes.DataSet);

        if Assigned(OutParams) then
          XMLToParams(SOAPRes.Params, OutParams);
        if Assigned(OutDataset) then
          XMLToDataSet(SOAPRes.DataSet, OutDataSet);
        Result := True;
      except
        on e: Exception do begin

           if Assigned(fOnClientError) then begin
            fOnClientError(e);
           end;

           fWasTimeOut := True;

           {$ifndef SERVERCAFEUI}
           ConnTimerTimer( Self );
           {$endif}

        end;
      end;
  finally
  end;
  ConnTimer.Enabled := True;
  FreeAndNil(SOAPMsg);

  if Result then
    SOAPMsg := SOAPRes
  else
  begin
    SOAPMsg := nil;
    try
      FreeAndNil (crypt);
      FreeAndNil (SOAPRes);
    except
      //ToDo: Excepci�n silenciosa
    end;
  end;

end;

function TdmBaseCafeCliente.SendSOAPMsgXML_DetalleTotales(var aDetalle, aTotales : TByteDynArray; var SOAPMsg: TSOAPDataSet; OutParams: TParams; OutDataset: TDataset;
  oServerReuse: ICafeteraSOAPWebService): Boolean;
var
  SOAPRes: TSOAPDataSet;
  oServer : ICafeteraSOAPWebService;
  sWide : WideString;




begin
  Result := False;
  ConnTimer.Enabled := False;
  try
      try

        if ( oServerReuse = nil )  then
        begin
          //CoInitializeEx(nil, COINIT_MULTITHREADED);
          oServer := GetServer
        end
        else
          oServer := oServerReuse;

        K_TIMEOUT_TRANSACTION := K_TIMEOUT_TRANSACTION_REP_DEF;
        oServer.SendCodedMessageByte(SOAPMsg, aDetalle, aTotales);
        K_TIMEOUT_TRANSACTION := K_TIMEOUT_TRANSACTION_DEF;
        SOAPRes := TSOAPDataSet.Create;
        Result := True;
      except
        on e: Exception do begin

           if Assigned(fOnClientError) then begin
            fOnClientError(e);
           end;

           fWasTimeOut := True;

           {$ifndef SERVERCAFEUI}
           ConnTimerTimer( Self );
           {$endif}
        end;
      end;
  finally
    //FreeAndnil( server );
{    if ( oServerReuse = nil )  then
      CoUninitialize;}
  end;
  ConnTimer.Enabled := True;
  FreeAndNil(SOAPMsg);
  if Result then
    SOAPMsg := SOAPRes
  else begin
    SOAPMsg := nil;
    try
      FreeAndNil(SOAPRes);
    except
      //ToDo: Excepci�n silenciosa
    end;
  end;
end;



procedure TdmBaseCafeCliente.Update;
begin
  FirstRun := True;
  ConnTimerTimer(nil);
end;
function TdmBaseCafeCliente.GetGlobal(const sIdent, sDefault: string): string;
var
  sValor: string;
begin
  if GetRegistryString(REG_KEY, sIdent, sValor) then
    Result := sValor
  else
    Result := sDefault
end;

function TdmBaseCafeCliente.GetGlobal(const sIdent: string; const iDefault: Integer): Integer;
var
  sValor: string;
begin
  if GetRegistryString(REG_KEY, sIdent, sValor) then
    Result := StrToIntDef(sValor, iDefault)
  else
    Result := iDefault
end;

function TdmBaseCafeCliente.GetGlobal(const sIdent: string; const dDefault: TDateTime): TDateTime;
var
  dValor: TDateTime;
begin
  if GetRegistryDateTime(REG_KEY, sIdent, dValor) then
    Result := dValor
  else
    Result := dDefault
end;

function TdmBaseCafeCliente.SetGlobal(const sIdent, sValue: string): Boolean;
begin
  Result := SetRegistryString(REG_KEY, sIdent, sValue);
end;

function TdmBaseCafeCliente.SetGlobal(const sIdent: string; iValue: Integer): Boolean;
begin
  Result := SetRegistryString(REG_KEY, sIdent, IntToStr(iValue));
end;

function TdmBaseCafeCliente.SetGlobal(const sIdent: String; const dValue: TDateTime): Boolean;
begin
  Result := SetRegistryDateTime(REG_KEY, sIdent, dValue);
end;

procedure TdmBaseCafeCliente.SetActive(const Value: Boolean);
begin
  fActive := Value;
  ConnTimerTimer(nil);
end;

procedure TdmBaseCafeCliente.SetFEstacionGafete(const Value: String);
var
  sEstacion: String;
begin
  sEstacion := Value;
  sEstacion := ZetaCommonTools.PadRCar(sEstacion, 4, ' ');
  sEstacion := ZetaCommonTools.PadRCar(sEstacion, 10, '0');
  FEstacion := sEstacion;
end;

function TdmBaseCafeCliente.GetServer: ICafeteraSOAPWebService;
begin
{  with HTTPRIO do begin
    WSDLLocation := GetWSDL;
    Service      := 'ICafeteraSOAPWebServiceservice';
    Port         := 'ICafeteraSOAPWebServicePort';
  end;
  Result := HTTPRIO as ICafeteraSOAPWebService;}

  Result := GetICafeteraSOAPWebService( False, GetSOAPURL );

end;

procedure TdmBaseCafeCliente.SetInterval(const Value: Cardinal);
begin
  ConnTimer.Interval := Value * 1000;
end;

procedure TdmBaseCafeCliente.AgregaChecadaServidor(const sMensaje: String);
begin
  Log(sMensaje);
end;

procedure TdmBaseCafeCliente.ConnTimerTimer(Sender: TObject);
var
  oServer : ICafeteraSOAPWebService;
begin

  Screen.Cursor := crHourGlass;
  ConnTimer.Enabled := False;
  oServer := nil;

  try
//      CoInitializeEx(nil, COINIT_MULTITHREADED);
      try
        oServer := GetServer;
        fActive := oServer.Ping;
        fServerRunning := True;
      except
        fActive := False;
        fServerRunning := False;
      end;
  finally
    //
  end;

  if ( fWasTimeOut )  then
  begin
      fActive := False;
      fServerRunning := False;
      fWasTimeOut := False;
  end;

  ConnTimer.Enabled := True;
  Screen.Cursor := crDefault;

  if (fOldActive <> fActive) or (fOldRunning <> fServerRunning) or FirstRun then begin
    FirstRun   := False;
    fOldActive := fActive;
    fOldRunning := fServerRunning;
    if fActive or fServerRunning then begin
      if Assigned(fOnClientConnect) then begin

        SOAPMsg := CreateSOAPMsg(K_GET_PARAMETROS);
        Params := TParams.Create(Self);
        if SendSOAPMsg(SOAPMsg, Params,nil, oServer) then
          FInstance := Params.ParamByName(K_IDENT_INSTANCIA).AsString
        else
          FInstance := '';
        FreeAndNil(SOAPMsg);
        FreeAndNil(Params);

        fOnClientConnect(Self);
      end;
    end else begin
      if Assigned(fOnClientDisConnect) then
        fOnClientDisConnect(Self);
    end;
  end;
  if Assigned(fOnConnectTimer) then
    fOnConnectTimer(Sender);

  //CoUninitialize;
end;

// Fuente: http://delphi.jmrds.com/?q=node/37
procedure TdmBaseCafeCliente.Log(const Message: String; FileName: string = '');
const
  SEP = Chr(9);
var
  F        : TextFile;
  Mutex    : THandle;
  SearchRec: TSearchRec;
  Msg      : string;
begin
  Msg := FormatDateTime('[dd/mmm/yyyy hh:nn:ss.zzz]', Now) + SEP + Message;
  if FileName = '' then
    Filename := ChangeFileExt(ParamStr(0), '.log');
  Mutex := CreateMutex(nil, False, PChar(StringReplace(ParamStr(0), '\', '/', [rfReplaceAll])));
  if Mutex <> 0 then begin
    WaitForSingleObject(Mutex, INFINITE);
    try
      if SysUtils.FindFirst(Filename, faAnyFile, SearchRec) = 0 then begin
        if SearchRec.Size > (1024 * 1024) then
          MoveFileEx(PChar(Filename), PChar(Filename + '.1'), MOVEFILE_REPLACE_EXISTING);
        SysUtils.FindClose(SearchRec);
      end;
      try
        AssignFile(F, Filename);
        {$I-}
        Append(F);
        if IOResult <> 0 then
          Rewrite(F);
        {$I+}
        if IOResult = 0 then begin
          Writeln(F, Msg);
          CloseFile(F);
        end;
      except
        ; // Excepcion silenciosa
      end;
    finally
      ReleaseMutex(Mutex);
      CloseHandle(Mutex);
    end;
  end;
end;

procedure TdmBaseCafeCliente.ReleaseServer;
begin
end;

end.
