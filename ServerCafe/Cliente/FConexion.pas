unit FConexion;

interface

uses
  ZBaseDlgModal_DevEx, Vcl.Forms, Vcl.Mask, ZetaNumero, Vcl.StdCtrls, ZetaEdit, Vcl.Buttons, System.Classes, Vcl.Controls, Vcl.ExtCtrls,
  Vcl.ComCtrls, ZetaHora, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, Vcl.ImgList, cxButtons;

type
  TConexion = class(TZetaDlgModal_DevEx)
    edtServidor: TZetaEdit;
    edtPuerto: TZetaNumero;
    lblServidor: TLabel;
    Label2: TLabel;
    lblIntervalo: TLabel;
    edtIntervalo: TZetaNumero;
    lblSegundos: TLabel;
    Label5: TLabel;
    GroupBox1: TGroupBox;
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    class function Execute: Boolean;
  end;

var
  Conexion: TConexion;

implementation

uses
  SysUtils, DServerCafeUI, CafeteraConsts, CafeteraUtils, ZetaDialogo;

{$R *.dfm}

class function TConexion.Execute: Boolean;
begin
  with TConexion.Create(Application) do begin
    Result := ShowModal = mrOk;
    Free;
  end;
end;

procedure TConexion.FormCreate(Sender: TObject);
begin
  inherited;
  edtServidor.Text   := ServerCafeUI.Direccion;
  edtPuerto.Valor    := ServerCafeUI.Puerto;
  edtIntervalo.Valor := ServerCafeUI.Interval;
end;

procedure TConexion.OKClick(Sender: TObject);
begin
  inherited;
  if Trim(edtServidor.Text) = '' then begin
    zError(Caption, 'Debe especificar un Nombre de Servidor.', 0);
    edtServidor.SetFocus;
  end else if not((edtPuerto.ValorEntero > 0) and (edtPuerto.ValorEntero < 65536)) then begin
    zError(Caption, 'El Puerto TCP no est� en el rango permitido.', 0);
    edtPuerto.SetFocus;
  end else if not((edtIntervalo.ValorEntero > 0) and (edtIntervalo.ValorEntero < 3600)) then begin
    zError(Caption, 'El Periodo de actualizaci�n es m�ximo una hora.', 0);
    edtIntervalo.SetFocus;
  end else with ServerCafeUI do begin
    SetGlobal(K_IDENT_SERVIDOR, Trim(edtServidor.Text));
    SetGlobal(K_IDENT_PUERTO, edtPuerto.ValorEntero);
    SetGlobal(K_IDENT_INTERVALO, edtIntervalo.ValorEntero);
    Init;
  end;
end;

end.
