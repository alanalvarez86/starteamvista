object ListarChecadas: TListarChecadas
  Left = 252
  Top = 163
  BorderStyle = bsDialog
  Caption = 'Listar Checadas Recibidas'
  ClientHeight = 259
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelInferior: TPanel
    Left = 0
    Top = 224
    Width = 367
    Height = 35
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvNone
    TabOrder = 0
    object Salir: TBitBtn
      Left = 288
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Abandonar Captura'
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Salir'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 367
    Height = 224
    ActivePage = Parametros
    Align = alClient
    TabOrder = 1
    object Parametros: TTabSheet
      Caption = 'Par�metros'
      object FechaLBL: TLabel
        Left = 35
        Top = 7
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha:'
        FocusControl = Fecha
      end
      object ComidasLBL: TLabel
        Left = 25
        Top = 28
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = '&Comidas:'
        FocusControl = ListaTipos
      end
      object EstacionLBL: TLabel
        Left = 24
        Top = 152
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = '&Estaci�n:'
        FocusControl = Estacion
      end
      object Fecha: TZetaFecha
        Left = 71
        Top = 3
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '10/Jul/98'
        Valor = 35986
      end
      object ListaTipos: TCheckListBox
        Left = 71
        Top = 28
        Width = 54
        Height = 122
        BorderStyle = bsNone
        Color = clBtnFace
        ItemHeight = 13
        Items.Strings = (
          'Tipo 1'
          'Tipo 2'
          'Tipo 3'
          'Tipo 4'
          'Tipo 5'
          'Tipo 6'
          'Tipo 7'
          'Tipo 8'
          'Tipo 9')
        TabOrder = 1
      end
      object Estacion: TEdit
        Left = 71
        Top = 148
        Width = 121
        Height = 21
        TabOrder = 2
      end
      object SoloTotales: TCheckBox
        Left = 4
        Top = 170
        Width = 80
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Solo &Totales:'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = SoloTotalesClick
      end
      object Generar: TBitBtn
        Left = 282
        Top = 166
        Width = 75
        Height = 25
        Hint = 'Generar Listas De Comidas'
        Anchors = [akTop, akRight]
        Caption = '&Generar'
        Default = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = GenerarClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
    end
    object Totales: TTabSheet
      Caption = 'Totales'
      ImageIndex = 1
      object GridTotales: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 359
        Height = 196
        Align = alClient
        DataSource = dsTotales
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CF_TIPO'
            Title.Caption = 'Comida Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CF_COMIDAS'
            Title.Caption = 'Comidas'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CF_EXTRAS'
            Title.Caption = 'Extras'
            Visible = True
          end>
      end
    end
    object Comidas: TTabSheet
      Caption = 'Comidas'
      ImageIndex = 2
      object GridDetalle: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 359
        Height = 196
        Align = alClient
        DataSource = dsDetalle
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CB_CODIGO'
            Title.Caption = 'C�digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRETTYNAME'
            Title.Caption = 'Nombre'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CF_FECHA'
            Title.Caption = 'Fecha'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CF_HORA'
            Title.Caption = 'Hora'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CF_TIPO'
            Title.Caption = 'Comida Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CF_COMIDA'
            Title.Caption = 'Comidas'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CF_EXTRAS'
            Title.Caption = 'Extras'
            Visible = True
          end>
      end
    end
  end
  object dsTotales: TDataSource
    Left = 80
    Top = 227
  end
  object dsDetalle: TDataSource
    Left = 112
    Top = 227
  end
end
