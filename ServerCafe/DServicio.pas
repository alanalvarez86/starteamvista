unit DServicio;

interface

uses
  SysUtils, SvcMgr;

type
  TCafetera_Servicio = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServiceAfterInstall(Sender: TService);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceAfterUninstall(Sender: TService);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  Cafetera_Servicio: TCafetera_Servicio;

implementation

uses
  ActiveX, Windows, Registry, ZetaWinAPITools, FCafetera, CafeteraConsts, CafeteraUtils;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  Cafetera_Servicio.Controller(CtrlCode);
end;

function TCafetera_Servicio.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TCafetera_Servicio.ServiceAfterInstall(Sender: TService);
var
  Reg: TRegistry;
  sParams, sValor: string;
  Hora: TDateTime;
begin
  // Registrar los par�metros de inicio del servicio
  Reg := TRegistry.Create(KEY_READ or KEY_WRITE);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    // ServiceName toma valor al principio de la ejecucion del programa en el DPR, el formato es
    //   <CafeteraServicio>[.Instancia]
    if Reg.OpenKey('\SYSTEM\CurrentControlSet\Services\' + ServiceName, False) then begin

      // Este servicio requiere que arranque despu�s de SQL Server, sino se queda trabado tratando de arrancar
      Reg.WriteInteger('DelayedAutostart', 1);

      // Descripcion del servicio
      Reg.WriteString('Description', SERVICE_DESCRIPTION);

      // Guardar los par�metros para ejecutar la instancia del servicio
      sParams := '"' + ParamStr(0) + '"';
      // Nombre de la instancia
      sParams := Format('%s /%s %s', [sParams, P_INSTANCIA, GetInstanceParam()]);
      // Puerto para esta instancia
      Reg.WriteString('ImagePath', sParams);
      Reg.CloseKey;
    end;

    //Guardar los datos espec�ficos para la instancia
    if Reg.OpenKey('\Software\Grupo Tress\' + ServiceName, True) then begin

      if FindCmdLineSwitch(P_PUERTO, sValor) then
        Reg.WriteString(P_PUERTO, IntToStr(StrToIntDef(sValor, K_CAFETERA_PUERTO)))
      else
        Reg.WriteString(P_PUERTO, IntToStr(K_CAFETERA_PUERTO));

      // Hora de reinicio de reglas
      try
        if FindCmdLineSwitch(P_HORA, sValor) then
          Hora := EncodeTime(StrToIntDef(Copy(sValor, 1, 2), 0), StrToIntDef(Copy(sValor, 3, 2), 0), 0, 0)
        else
          Hora := K_CAFETERA_HORA_REINICIO
      except
        Hora := K_CAFETERA_HORA_REINICIO;
      end;
      Reg.WriteDateTime(P_HORA, Hora);

      // AutoReset, obsoleto
      if FindCmdLineSwitch(P_AUTORESET, sValor) then
        Reg.WriteString(P_AUTORESET, sValor);

      Reg.CloseKey;
    end;

  except
    on e: Exception do
      WriteEventLog(ExceptionAsString(e), '', ServiceName)
  end;
  FreeAndNil(Reg);
end;

procedure TCafetera_Servicio.ServiceAfterUninstall(Sender: TService);
var
  Reg: TRegistry;
begin
  // Eliminar los par�metros de inicio del servicio
  Reg := TRegistry.Create(KEY_READ or KEY_WRITE);
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;

    //Guardar los datos espec�ficos para la instancia
    if Reg.OpenKey('\Software\Grupo Tress', True) then begin
      Reg.DeleteKey(ServiceName)
    end;
  except
    on e: Exception do
      WriteEventLog(ExceptionAsString(e), '', ServiceName)
  end;
  FreeAndNil(Reg);
end;

procedure TCafetera_Servicio.ServiceCreate(Sender: TObject);
begin
  // ServiceName toma valor al inicio de la ejecuci�n del programa en el c�digo del DPR
  Name := ServiceName;
  DisplayName := Format('%s (%s)', [SERVICE_DISPLAYNAME, Name]);
end;

procedure TCafetera_Servicio.ServiceExecute(Sender: TService);
begin
  while (not Terminated) and (not IsInstalling) do begin
    ServiceThread.ProcessRequests(false);
    Sleep(1000);
  end;
end;

procedure TCafetera_Servicio.ServiceStart(Sender: TService; var Started: Boolean);
begin
  if IsInstalling then begin
    Exit;
  end;

  CoInitialize(nil);
  try
    CafeServer := TCafeServer.Create(Self);
    CafeServer.RegistryKey := Name;
    CafeServer.Start;
  except
    on e: Exception do begin
      WriteEventLog(ExceptionAsString(e), '', Name);
    end;
  end;
end;

procedure TCafetera_Servicio.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  if IsInstalling then begin
    Exit;
  end;

  try
    FreeAndNil(CafeServer);
  except
    on e: Exception do begin
      WriteEventLog(ExceptionAsString(e), '', Name);
    end;
  end;
  CoUnInitialize;
end;

initialization
  if FindCmdLineSwitch('uninstall') then begin
    StartProcess('net', 'stop ' + GetInstanceName());
  end;

finalization
  if FindCmdLineSwitch('install') then begin
    StartProcess('net', 'start ' + GetInstanceName());
  end;

end.
