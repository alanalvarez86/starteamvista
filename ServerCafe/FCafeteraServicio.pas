unit FCafeteraServicio;

interface

uses
  FBaseServerCafeUI, Windows, Types, Vcl.Forms, Vcl.Controls, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.StdCtrls, Vcl.Buttons, System.Classes,
  Vcl.ImgList, System.Actions, Vcl.ActnList, Vcl.StdActns, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxBarBuiltInMenu,
  cxControls, cxContainer, cxEdit, cxClasses, dxSkinsForm, cxTextEdit, cxMemo,
  cxPC, cxButtons;

type
  TFormaCafeteraServicio = class(TBaseServerCafeUI)
    ProgressBar: TProgressBar;
    Image: TImage;
    procedure FormCreate(Sender: TObject);
    procedure StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init; override;
  end;

var
  FormaCafeteraServicio: TFormaCafeteraServicio;

implementation

uses
  SysUtils, Messages, CafeteraConsts, CafeteraUtils, FCafetera;

{$R *.dfm}

procedure TFormaCafeteraServicio.FormCreate(Sender: TObject);
var
  ProgressBarStyle: Integer;
begin
  inherited;
  StatusBar.Panels[0].Style := psOwnerDraw;
  ProgressBar.Parent        := StatusBar;
  ProgressBarStyle          := GetWindowLong(ProgressBar.Handle, GWL_EXSTYLE);
  ProgressBarStyle          := ProgressBarStyle - WS_EX_STATICEDGE;
  SetWindowLong(ProgressBar.Handle, GWL_EXSTYLE, ProgressBarStyle);
end;

procedure TFormaCafeteraServicio.FormDestroy(Sender: TObject);
begin
  CafeServer.Memo := nil;
  inherited;
end;

procedure TFormaCafeteraServicio.Init;
begin
  try
    CafeServer := TCafeServer.Create(Self);
    if IsDesktopMode then
      CafeServer.RegistryKey := SERVICE_BASE_NAME + 'UI'
    else
      CafeServer.RegistryKey := SERVICE_BASE_NAME;
    if FileExists(CafeServer.Log.FileName) then
    begin
      // Errores.Lines.LoadFromFile(CafeServer.Log.FileName);
      // SendMessage(Errores.Handle, EM_LINESCROLL, 0, Errores.Lines.Count);
      Errores_DevEx.Lines.LoadFromFile(CafeServer.Log.FileName);
      // SendMessage(Errores_DevEx.Handle, EM_LINESCROLL, 0, Errores_DevEx.Lines.Count);
      Errores_DevEx.InnerControl.Perform(EM_LINESCROLL, 0,  Errores_DevEx.Lines.Count);

    end;
    CafeServer.ProgressBar := ProgressBar;
    // CafeServer.Memo        := Errores;
    CafeServer.Memo        := Errores_DevEx;
    CafeServer.PortPanel   := StatusBar.Panels[1];
    CafeServer.StatusPanel := StatusBar.Panels[2];
    CafeServer.Imagen      := Image;
    CafeServer.Start;
  except
    ; // Excepcion silenciosa
  end;
end;

procedure TFormaCafeteraServicio.StatusBarDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
begin
  inherited;
  if Panel = StatusBar.Panels[0] then
    with ProgressBar do begin
      Top    := Rect.Top;
      Left   := Rect.Left;
      Width  := Rect.Right - Rect.Left;
      Height := Rect.Bottom - Rect.Top;
    end;
end;

end.
