unit DReportesGen;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DReportesGenerador, DB, DBClient, ZetaClientDataSet;

type
  TdmReportesGen = class(TdmReportGenerator)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { protected declarations }
    procedure DoBeforeGetResultado;override;
  public
    { Public declarations }
    function ProcesaReporte(const iReporte: integer): Boolean;
  end;

var
  dmReportesGen: TdmReportesGen;

implementation
uses
    ZetaCommonLists,
    ZetaTipoEntidad;


{$R *.dfm}

{ TdmReportGen }

function TdmReportesGen.ProcesaReporte(const iReporte: integer): Boolean;
begin
     Result := GetResultado( iReporte, FALSE );
end;

procedure TdmReportesGen.DataModuleCreate(Sender: TObject);
begin
     inherited;
     SetLogFileName( ExtractFilePath( Application.ExeName ) + 'Reportes.LOG' );
end;

procedure TdmReportesGen.DoBeforeGetResultado;
begin
     with FSQLAgente do
     begin
          AgregaColumna( 'PLAZA.PL_FOLIO',
                         TRUE,
                         enPlazas,
                         tgNumero,
                         7,
                         'PL_FOLIODATASET',
                         ocNinguno,
                         FALSE,
                         0 );
     end;
end;

end.
