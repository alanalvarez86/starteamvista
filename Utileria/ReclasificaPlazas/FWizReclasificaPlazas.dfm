inherited WizPlazas: TWizPlazas
  Left = 175
  Top = -17
  Caption = 'Reclasificaci'#243'n de Plazas'
  ClientHeight = 281
  ClientWidth = 510
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 245
    Width = 510
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Left = 12
      Enabled = True
    end
    inherited Siguiente: TZetaWizardButton
      Left = 89
    end
    inherited Salir: TZetaWizardButton
      Left = 417
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 335
    end
  end
  inherited PageControl: TPageControl
    Width = 510
    Height = 245
    ActivePage = Parametros
    TabStop = False
    object tsReporte: TTabSheet [0]
      ImageIndex = 7
      TabVisible = False
      object GroupBox2: TGroupBox
        Left = 83
        Top = 80
        Width = 337
        Height = 73
        Caption = ' Reporte para obtener las plazas: '
        TabOrder = 0
        object Formato: TZetaKeyLookup
          Left = 16
          Top = 32
          Width = 300
          Height = 21
          Filtro = 'RE_TIPO=0 and RE_ENTIDAD=271'
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    object DatosRegistro: TTabSheet [1]
      Caption = 'DatosRegistro'
      ImageIndex = 7
      TabVisible = False
      object LblFecha: TLabel
        Left = 82
        Top = 49
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha Movimiento:'
      end
      object GBMensaje: TGroupBox
        Left = 82
        Top = 109
        Width = 337
        Height = 81
        Caption = ' Observaciones: '
        Color = clBtnFace
        ParentColor = False
        TabOrder = 0
        object MemoObserva: TMemo
          Left = 2
          Top = 15
          Width = 333
          Height = 64
          Align = alClient
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object FechaCambio: TZetaFecha
        Left = 177
        Top = 44
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '08/Dec/06'
        Valor = 39059.000000000000000000
      end
    end
    inherited Parametros: TTabSheet
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 502
        Height = 235
        Align = alClient
        Caption = ' Indicar solamente los datos que se desean cambiar: '
        TabOrder = 0
        object Label5: TLabel
          Left = 60
          Top = 30
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descripci'#243'n:'
        end
        object Label6: TLabel
          Left = 70
          Top = 54
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Reporta a:'
        end
        object Label16: TLabel
          Left = 75
          Top = 78
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Relaci'#243'n:'
        end
        object PL_CODIGO: TZetaEdit
          Left = 123
          Top = 26
          Width = 60
          Height = 21
          MaxLength = 10
          TabOrder = 0
        end
        object PL_REPORTA: TZetaKeyLookup
          Left = 123
          Top = 50
          Width = 300
          Height = 21
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
        object PL_TIREP: TZetaKeyCombo
          Left = 123
          Top = 74
          Width = 115
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 2
          ListaFija = lfJIPlazaReporta
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object GroupBox3: TGroupBox
          Left = 56
          Top = 98
          Width = 386
          Height = 127
          Caption = ' Vigencia '
          TabOrder = 3
          object Label1: TLabel
            Left = 39
            Top = 15
            Width = 24
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tipo:'
          end
          object lbFechaInicio: TLabel
            Left = 19
            Top = 40
            Width = 28
            Height = 13
            Alignment = taRightJustify
            Caption = 'Inicio:'
          end
          object PL_TIPO: TZetaKeyCombo
            Left = 66
            Top = 11
            Width = 115
            Height = 21
            AutoComplete = False
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            ListaFija = lfJITipoPlaza
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
          end
          object PL_FEC_INI: TZetaFecha
            Left = 66
            Top = 35
            Width = 115
            Height = 22
            Cursor = crArrow
            Enabled = False
            TabOrder = 1
            Text = '12/Dec/05'
            Valor = 38698.000000000000000000
          end
          object cbInicio: TCheckBox
            Left = 51
            Top = 38
            Width = 14
            Height = 17
            TabOrder = 2
            OnClick = cbInicioClick
          end
          object GroupBox8: TGroupBox
            Left = 29
            Top = 62
            Width = 329
            Height = 59
            Caption = ' Cambiar vencimiento: '
            TabOrder = 3
            object lblVence: TLabel
              Left = 73
              Top = 39
              Width = 86
              Height = 13
              Alignment = taRightJustify
              Caption = 'Fecha espec'#237'fica:'
              Enabled = False
            end
            object PL_FEC_FIN: TZetaFecha
              Left = 163
              Top = 34
              Width = 115
              Height = 22
              Cursor = crArrow
              Enabled = False
              TabOrder = 0
              Text = '12/Dec/05'
              Valor = 38698.000000000000000000
            end
            object cbSinVence: TCheckBox
              Left = 8
              Top = 16
              Width = 167
              Height = 17
              Alignment = taLeftJustify
              Caption = 'Dejar sin fecha de vencimiento:'
              Enabled = False
              TabOrder = 1
            end
            object cbVence: TCheckBox
              Left = 115
              Top = -1
              Width = 14
              Height = 17
              TabOrder = 2
              OnClick = cbVenceClick
            end
          end
        end
        object PL_NOMBRE: TZetaEdit
          Left = 185
          Top = 26
          Width = 213
          Height = 21
          TabOrder = 4
        end
      end
    end
    object Contratacion: TTabSheet [3]
      Caption = 'Contratacion'
      ImageIndex = 2
      TabVisible = False
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 502
        Height = 235
        Align = alClient
        Caption = ' Indicar solamente los datos que se desean cambiar: '
        TabOrder = 0
        object Label4: TLabel
          Left = 79
          Top = 51
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clasificaci'#243'n:'
        end
        object Label26: TLabel
          Left = 110
          Top = 75
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Turno:'
        end
        object Label29: TLabel
          Left = 57
          Top = 99
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Registro Patronal:'
        end
        object Label21: TLabel
          Left = 71
          Top = 196
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'Checa Tarjeta:'
        end
        object Arealbl: TLabel
          Left = 116
          Top = 174
          Width = 25
          Height = 13
          Alignment = taRightJustify
          BiDiMode = bdRightToLeft
          Caption = 'Area:'
          ParentBiDiMode = False
        end
        object Label27: TLabel
          Left = 59
          Top = 149
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Contrato:'
        end
        object lblConfidencialidad: TLabel
          Left = 60
          Top = 124
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Confidencialidad:'
        end
        object lblPuesto: TLabel
          Left = 105
          Top = 26
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Puesto:'
        end
        object PL_CLASIFI: TZetaKeyLookup
          Left = 145
          Top = 47
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
        object PL_TURNO: TZetaKeyLookup
          Left = 145
          Top = 71
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
        end
        object PL_PATRON: TZetaKeyLookup
          Left = 145
          Top = 96
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 3
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL0: TZetaKeyLookup
          Left = 145
          Top = 120
          Width = 300
          Height = 21
          Opcional = False
          TabOrder = 4
          TabStop = True
          WidthLlave = 60
        end
        object PL_CONTRAT: TZetaKeyLookup
          Left = 145
          Top = 145
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 5
          TabStop = True
          WidthLlave = 60
        end
        object PL_AREA: TZetaKeyLookup
          Left = 145
          Top = 170
          Width = 300
          Height = 21
          Opcional = False
          TabOrder = 6
          TabStop = True
          WidthLlave = 60
        end
        object PL_CHECA: TZetaKeyCombo
          Left = 145
          Top = 192
          Width = 120
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 7
          ListaFija = lfAccesoRegla
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object PU_CODIGO: TZetaKeyLookup
          Left = 145
          Top = 22
          Width = 300
          Height = 21
          EditarSoloActivos = True
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    object Area: TTabSheet [4]
      Caption = 'Area'
      ImageIndex = 3
      TabVisible = False
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 502
        Height = 235
        Align = alClient
        Caption = ' Indicar solamente los datos que se desean cambiar: '
        TabOrder = 0
        object PL_NIVEL1lbl: TLabel
          Left = 122
          Top = 26
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel1:'
        end
        object PL_NIVEL9lbl: TLabel
          Left = 122
          Top = 206
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel9:'
        end
        object PL_NIVEL8lbl: TLabel
          Left = 122
          Top = 183
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel8:'
        end
        object PL_NIVEL7lbl: TLabel
          Left = 122
          Top = 161
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel7:'
        end
        object PL_NIVEL6lbl: TLabel
          Left = 122
          Top = 138
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel6:'
        end
        object PL_NIVEL5lbl: TLabel
          Left = 122
          Top = 116
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel5:'
        end
        object PL_NIVEL4lbl: TLabel
          Left = 122
          Top = 93
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel4:'
        end
        object PL_NIVEL3lbl: TLabel
          Left = 122
          Top = 71
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel3:'
        end
        object PL_NIVEL2lbl: TLabel
          Left = 122
          Top = 48
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nivel2:'
        end
        object PL_NIVEL1: TZetaKeyLookup
          Left = 156
          Top = 22
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL2: TZetaKeyLookup
          Left = 156
          Top = 44
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL3: TZetaKeyLookup
          Left = 156
          Top = 67
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL4: TZetaKeyLookup
          Left = 156
          Top = 89
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 3
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL5: TZetaKeyLookup
          Left = 156
          Top = 112
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 4
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL6: TZetaKeyLookup
          Left = 156
          Top = 134
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 5
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL7: TZetaKeyLookup
          Left = 156
          Top = 157
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 6
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL8: TZetaKeyLookup
          Left = 156
          Top = 179
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 7
          TabStop = True
          WidthLlave = 60
        end
        object PL_NIVEL9: TZetaKeyLookup
          Left = 156
          Top = 202
          Width = 304
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 8
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    object Salario: TTabSheet [5]
      Caption = 'Salario'
      ImageIndex = 4
      TabVisible = False
      object GroupBox6: TGroupBox
        Left = 0
        Top = 0
        Width = 502
        Height = 235
        Align = alClient
        Caption = ' Indicar solamente los datos que se desean cambiar: '
        TabOrder = 0
        object LblTabulador: TLabel
          Left = 46
          Top = 60
          Width = 107
          Height = 13
          Alignment = taRightJustify
          Caption = ' Salario por Tabulador:'
        end
        object LblSalario: TLabel
          Left = 118
          Top = 84
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Salario:'
        end
        object Label33: TLabel
          Left = 45
          Top = 108
          Width = 108
          Height = 13
          Alignment = taRightJustify
          Caption = 'Promedio de Variables:'
        end
        object Label28: TLabel
          Left = 44
          Top = 132
          Width = 109
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tabla de Prestaciones:'
        end
        object Label31: TLabel
          Left = 70
          Top = 156
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Zona Geogr'#225'fica:'
        end
        object PL_SALARIO: TZetaNumero
          Left = 175
          Top = 80
          Width = 100
          Height = 21
          Enabled = False
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
        end
        object PL_PER_VAR: TZetaNumero
          Left = 175
          Top = 104
          Width = 100
          Height = 21
          Enabled = False
          Mascara = mnPesos
          TabOrder = 1
          Text = '0.00'
        end
        object PL_TABLASS: TZetaKeyLookup
          Left = 155
          Top = 128
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = True
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
        end
        object PL_ZONA_GE: TZetaKeyCombo
          Left = 155
          Top = 152
          Width = 120
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 3
          ListaFija = lfZonaGeografica
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
        object PL_AUTOSAL: TZetaKeyCombo
          Left = 155
          Top = 56
          Width = 120
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 4
          ListaFija = lfAccesoRegla
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object cbCambiaSalario: TCheckBox
          Left = 155
          Top = 82
          Width = 17
          Height = 17
          Hint = 'Prender para cambiar el salario'
          TabOrder = 5
          OnClick = cbCambiaSalarioClick
        end
        object cbCambiaPromedio: TCheckBox
          Left = 155
          Top = 106
          Width = 17
          Height = 17
          Hint = 'Prender para cambiar el promedio de variables'
          TabOrder = 6
          OnClick = cbCambiaPromedioClick
        end
      end
    end
    object Generales: TTabSheet [6]
      Caption = 'Generales'
      ImageIndex = 6
      TabVisible = False
      object GroupBox7: TGroupBox
        Left = 0
        Top = 0
        Width = 502
        Height = 235
        Align = alClient
        Caption = ' Indicar solamente los datos que se desean cambiar: '
        TabOrder = 0
        object Label7: TLabel
          Left = 76
          Top = 75
          Width = 31
          Height = 13
          Caption = 'Ingl'#233's:'
        end
        object Label10: TLabel
          Left = 51
          Top = 148
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'SubCuenta:'
        end
        object Label8: TLabel
          Left = 77
          Top = 124
          Width = 30
          Height = 13
          Caption = 'Texto:'
        end
        object CO_NUMEROLbl: TLabel
          Left = 67
          Top = 99
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object PL_INGLES: TEdit
          Left = 110
          Top = 71
          Width = 325
          Height = 21
          MaxLength = 30
          TabOrder = 0
        end
        object PL_NUMERO: TZetaNumero
          Left = 127
          Top = 95
          Width = 70
          Height = 21
          Enabled = False
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
          UseEnterKey = True
        end
        object PL_TEXTO: TEdit
          Left = 110
          Top = 119
          Width = 325
          Height = 21
          MaxLength = 30
          TabOrder = 2
        end
        object PL_SUB_CTA: TEdit
          Left = 110
          Top = 143
          Width = 325
          Height = 21
          MaxLength = 30
          TabOrder = 3
        end
        object cbNumero: TCheckBox
          Left = 110
          Top = 97
          Width = 16
          Height = 17
          Hint = 'Prender para cambiar el n'#250'mero'
          TabOrder = 4
          OnClick = cbNumeroClick
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 502
        Lines.Strings = (
          ''
          'Se crearan las nuevas plazas con los par'#225'metros capturados'
          ''
          'Presione el bot'#243'n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Top = 168
        Width = 502
        inherited ProgressBar: TProgressBar
          Left = 72
        end
      end
    end
  end
  object DataSource: TDataSource
    Left = 20
    Top = 14
  end
end
