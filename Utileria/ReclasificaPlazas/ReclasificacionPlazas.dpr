program ReclasificacionPlazas;

uses
  Forms,
  ComObj,
  ActiveX,
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  DInterfase in 'DInterfase.pas' {dmInterfase: TDataModule},
  ZBaseImportaShell in '..\..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  DReportesGenerador in '..\..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DReportesGen in 'DReportesGen.pas' {dmReportesGen: TDataModule},
  ZWizardBasico in '..\..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZBaseWizard in '..\..\Tools\ZBaseWizard.pas' {BaseWizard};

{$R *.RES}
{$R WindowsXP.res}

{procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     {if ( ParamCount = 0 ) then
        MuestraSplash;}
     Application.Title := 'Proceso de Reclasificación de Plazas';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if ( ParamCount = 0 ) then
          begin
               if Login( False ) then
               begin
                    //CierraSplash;
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    //CierraSplash;
                    Free;
               end;
          end
          else
          begin
               try
                  ProcesarBatch;
               finally
                  Free;
               end;
          end;
     end;
end.
