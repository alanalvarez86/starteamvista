unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell, Psock, NMsmtp, ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZBaseConsulta,
  ZetaMessages, ZetaNumero, ZetaKeyLookup, ZetaClientDataSet;

type
  TTressShell = class(TBaseImportaShell)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SistemaFechaZFValidDate(Sender: TObject);
  private
    { Private declarations }
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
    procedure SetNombreLogs;
    procedure ConectaLookups;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;

  protected
    { Protected declarations }
    procedure ActivaControles( const lProcesando: Boolean ); override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
    procedure ProcesarArchivo; override;
    procedure CancelarProceso; override;
    procedure AbreEmpresa(const lDefault: Boolean);override;
  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure RefrescaEmpleado;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure CambiaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;

  end;

var
  TressShell: TTressShell;


implementation

uses DCliente, DGlobal, DSistema, DCatalogos, DTablas,
     DRecursos, DProcesos, DInterfase,Dconsultas,
     DReportes,
     FWizReclasificaPlazas,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
{$endif}
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     FCalendario,
     ZGlobalTress;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     inherited;
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmInterfase := TdmInterfase.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmConsultas.Free;
     dmReportes.Free;
     dmInterfase.Free;
     dmProcesos.Free;
     dmRecursos.Free;
     dmTablas.Free;
     dmCatalogos.Free;
     dmSistema.Free;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
{$endif}
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        {***US 11757: Proteccion HTTP SkinController = True
        Evita que el backgound de algunos componentes de la aplicacion
        sea de color blanco
        ***}
        DevEx_SkinController.NativeStyle := TRUE;
        {AV: 2010-11-16 Asigna El Global Usar Validacion Status Activo de Tablas y Catálogos al ZetaClienteDataSet }
        ZetaClientDataSet.GlobalSoloKeysActivos := Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS );
        ZetaClientDataSet.GlobalListaConfidencialidad := dmCliente.Confidencialidad;

        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosAsistencia;
             InitActivosEmpleado;
             InitActivosPeriodo;
        end;
        InitBitacora;
        CargaSistemaActivos;
        ConectaLookups;
     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message );
             DoCloseAll;
        end;
     end;
     ActivaControles( FALSE );
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

procedure TTressShell.ProcesarArchivo;
begin
     ActivaControles( TRUE );
     try
        InitBitacora;
        DoProcesar;
     finally
        EscribeBitacoras;
        ActivaControles( FALSE );
     end;
end;

procedure TTressShell.DoProcesar;
begin
     WizPlazas := TWizPlazas.Create( Application );

     with WizPlazas do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

procedure TTressShell.CancelarProceso;
begin
     dmInterfase.ProcesandoThread := FALSE;
end;

procedure TTressShell.ActivaControles(const lProcesando: Boolean);
begin
     inherited;
     with dmCliente do
     begin
          if ( not ModoBatch ) then
          begin
               if lProcesando and ( BtnDetener.CanFocus ) then
                  BtnDetener.SetFocus
               else if BtnSalir.CanFocus then
                  BtnSalir.SetFocus;
          end;
     end;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     RefrescaSistemaActivos;
end;

procedure TTressShell.SistemaFechaZFValidDate(Sender: TObject);
begin
     inherited;
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre: String;
begin
     with dmCliente do
     begin
          sNombre := GetDatosEmpresaActiva.Nombre + FormatDateTime('ddmmyy-hhnnss',Now) +'.LOG';
          ArchBitacora.Text := ZetaMessages.SetFileNameDefaultPath( 'BIT-' + sNombre );
          ArchErrores.Text := ZetaMessages.SetFileNameDefaultPath( 'ERR-' + sNombre );
     end;
end;

procedure TTressShell.ConectaLookups;
begin
     with dmTablas do
     begin
          cdsSupervisores.Conectar;
     end;
end;
{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmInterfase.HandleProcessEnd( WParam, LParam );
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }

procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := nil;
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

procedure TTressShell.AbreEmpresa(const lDefault: Boolean);
begin
     inherited AbreEmpresa( lDefault );
     SetNombreLogs;
     ProcesarArchivo;
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
    //No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     //No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     //No Implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     //No Implementar
end;

end.
