unit DReportes;

interface

{$DEFINE REPORTING}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient,
  {$ifdef DOS_CAPAS}
     {$ifdef REPORTING}
     DServerReporting,
     {$else}
     DServerCalcNomina,
     {$endif}
  {$else}
     {$ifdef REPORTING}
     Reporteador_TLB,
     {$else}
     DCalcNomina_TLB,
     {$endif}

  {$endif}
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaTipoEntidad,
  ZReportTools,
  DBaseReportes;

{$ifdef REPORTING}
  type
  {$ifdef DOS_CAPAS}
    TdmServerCalcNomina = TdmServerReporting;
  {$else}
    IdmServerCalcNominaDisp = IdmServerReportingDisp;
  {$endif}
{$endif}

type
  TdmReportes = class(TdmBaseReportes)
    cdsSuscripReportesLookup: TZetaLookupDataSet;
    cdsLookupReportesEmpresa: TZetaLookupDataSet;
    procedure cdsCampoRepAlCrearCampos(Sender: TObject);
    procedure cdsLookupReportesLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsSuscripReportesLookupAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerReportesBDE: TdmServerCalcNomina;
    property ServerReportesBDE: TdmServerCalcNomina read GetServerReportesBDE;
{$else}
    FServidorBDE: IdmServerCalcNominaDisp;
    function GetServerReportesBDE: IdmServerCalcNominaDisp;
    property ServerReportesBDE: IdmServerCalcNominaDisp read GetServerReportesBDE;
{$endif}
    procedure GrabaFotos;
  protected
    function GeneraSQL( var oSQLAgente: OleVariant; var ParamList: OleVariant;
                        var sError: WideString): OleVariant;override;
    function ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;override;
    function ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;override;
    procedure GetFormaReporte( const eTipo: eTipoReporte );override;
    procedure AfterGeneraSQL;override;
  public
    { Public declarations }
    //function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;
    procedure CancelaCuentasPoliza;
    procedure LlenaConceptos(eTiposConcepto: TiposConcepto);
    procedure BuscaConcepto;override;
    function DirectorioPlantillas: string;override;
  end;

var
  dmReportes: TdmReportes;

implementation
uses DCliente,
     DGlobal,
     DCatalogos,
     ZReportModulo,
     ZGlobalTress,
     ZAccesosTress,
     ZetaBusqueda,
     ZetaDialogo,
     ZetaCommonTools,
     FPoliza,
     FPolizaConcepto ;

{$R *.DFM}
{$ifdef DOS_CAPAS}
function TdmReportes.GetServerReportesBDE: TdmServerCalcNomina;
begin
     {$ifdef REPORTING}
     Result := DCliente.dmCliente.ServerReporteador;
     {$else}
     Result := DCliente.dmCliente.ServerCalcNomina;
     {$endif}
end;
{$else}
function TdmReportes.GetServerReportesBDE: IdmServerCalcNominaDisp;
//function TdmReportes.GetServerReportesBDE:IdmServerReportingDisp;
begin
     Result := IdmServerCalcNominaDisp( dmCliente.CreaServidor( {$ifdef REPORTING}CLASS_dmServerReporting{$ELSE}CLASS_dmServerCalcNomina{$ENDIF}, FServidorBDE ) );
     //Result := IdmServerReportingDisp(dmCliente.CreaServidor( {$ifdef REPORTING}CLASS_dmServerReporting{$ELSE}CLASS_dmServerCalcNomina{$ENDIF}, FServidorBDE ) );
end;
{$endif}

function TdmReportes.GeneraSQL( var oSQLAgente: OleVariant; var ParamList : OleVariant; var sError : WideString ) : OleVariant;
begin
     Result := ServerReportesBDE.GeneraSQL( dmCliente.Empresa, oSQLAgente, ParamList , sError  );


end;

procedure TdmReportes.AfterGeneraSQL;
begin
     GrabaFotos;
end;

procedure TdmReportes.GrabaFotos;
var
    FBlob: TBlobField;
    iColumna, iEmpleado: integer;
    sTipo, sDirectorio,sDirectorio2: string;
    lExportarImagen: Boolean;
begin
     if ParamList.IndexOf('GRABA_IMAGEN_DISCO') <> -1 then
     begin
          try
             with ParamList do
             begin
                  iColumna := ParamByName('COL_IMAGEN_DISCO').AsInteger;
                  sTipo := ParamByName('TIPO_IMAGEN_DISCO').AsString;
                  sDirectorio := ParamByName('GRABA_IMAGEN_DISCO').AsString;
                  sDirectorio2 := StrTransAll( sDirectorio, '\', '\\');
             end;

             with cdsResultados do
             begin
                  DisableControls;
                  try
                     First;
                     while NOT EOF do
                     begin
                          lExportarImagen := FALSE;
                          iEmpleado := Fields[iColumna].AsInteger;
                          if ( iEmpleado > 0 ) then
                          begin
                               cdsBlobs.Data := ServerReportesBDE.GetFoto( dmCliente.Empresa, iEmpleado, sTipo );
                               FBlob := TBlobField( cdsBlobs.FieldByName( 'CampoBlob' ) );
                               lExportarImagen := ( not FBlob.IsNull );
                               if lExportarImagen then
                                  FBlob.SaveToFile( sDirectorio + IntToStr( iEmpleado ) + '.jpg');
                          end;
                          Edit;
                          if lExportarImagen then
                             Fields[iColumna].AsString := sDirectorio2 + IntToStr( iEmpleado ) + '.jpg'
                          else
                              Fields[iColumna].AsString := VACIO;
                          Post;
                          Next;
                     end;
                  finally
                         MergeChangeLog;
                         First;
                         EnableControls;
                  end;
             end;
          finally
          end;
     end;
end;

procedure TdmReportes.GetFormaReporte( const eTipo : eTipoReporte );
begin
     inherited GetFormaReporte( eTipo );
     case eTipo of
          trPoliza : ShowFormaReporte( Poliza, TPoliza, SoloLectura, SoloImpresion );
          trPolizaConcepto : ShowFormaReporte( PolizaConcepto, TPolizaConcepto, SoloLectura, SoloImpresion );
     end;
end;

{$ifdef COMENTARIOS}

function TdmReportes.GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;
begin
     Result := ZReportModulo.GetDerechosClasifi(eClasifi);
     case eClasifi of
          crEmpleados  : Result:= D_REPORTES_EMPLEADOS;
          crCursos     : Result:= D_REPORTES_CURSOS;
          crAsistencia : Result:= D_REPORTES_ASISTENCIA;
          crNominas    : Result:= D_REPORTES_NOMINA;
          crPagosIMSS  : Result:= D_REPORTES_IMSS;
          crConsultas  : Result:= D_REPORTES_CONSULTAS;
          crCatalogos  : Result:= D_REPORTES_CATALOGOS;
          crTablas     : Result:= D_REPORTES_TABLAS;
          crSupervisor : Result:= D_REPORTES_SUPERVISORES;
          crCafeteria  : Result:= D_REPORTES_CAFETERIA;
          crLabor      : Result:= D_REPORTES_LABOR;
          crMedico     : Result:= D_REPORTES_MEDICOS;
          {$ifdef CARRERA}
          crCarrera    : Result:= D_REPORTES_CARRERA;
          {$endif}
          crMigracion  : Result:= D_REPORTES_MIGRACION

          else Result:= 0;
     end;
end;
{$ENDIF}


function TdmReportes.ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;
begin
     Result := ServerReportesBDE.EvaluaParam( dmCliente.Empresa, oSQLAgente, oParamList, sError );
end;

function TdmReportes.ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;
begin
     //CV: 17-junio-2005
     //La implementacion de esta parte se realizara cuando cambie la llamada de DDiccionario.PruebaFormula
     Result := FALSE;
end;



procedure TdmReportes.CancelaCuentasPoliza;
begin
     if ( cdsCampoRep.ChangeCount > 0 ) then
        cdsCampoRep.CancelUpdates;
end;

procedure TdmReportes.cdsCampoRepAlCrearCampos(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsConceptosLookup.Conectar;
     if dmCatalogos.cdsConceptosLookup.Active then
        cdsCampoRep.CreateSimpleLookup(dmCatalogos.cdsConceptosLookup, 'CO_DESCRIP','CR_CALC');
end;


procedure TdmReportes.LlenaConceptos( eTiposConcepto : TiposConcepto );
const
     K_CARGO = 0;
     K_ABONO = 1;
     aAsiento: array[ false..true ] of integer = ( K_CARGO, K_ABONO );
var
   eTipo: eTipoConcepto;
begin
     with dmCatalogos.cdsConceptosLookup do
     begin
          try
             Conectar;
             if Active then
             begin
                  DisableControls;
                  First;
                  cdsCampoRep.DisableControls;
                  while not Eof do
                  begin
                       eTipo := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
                       if ( eTipo in eTiposConcepto ) then
                       begin
                            if NOT cdsCampoRep.Locate('CR_CALC', FieldByName('CO_NUMERO').AsInteger, []) then
                            begin
                                 cdsCampoRep.Append;
                                 cdsCampoRep.FieldByName( 'CR_CALC' ).AsInteger := FieldByName( 'CO_NUMERO' ).AsInteger;
                                 cdsCampoRep.FieldByName( 'CR_OPER' ).AsInteger := aAsiento[ ( eTipo in [coDeduccion,coObligacion] ) ];
                                 cdsCampoRep.Post;
                            end;
                       end;
                       Next;
                  end;
             end;
          finally
            cdsCampoRep.EnableControls;
            EnableControls;
          end;
     end;
end;

procedure TdmReportes.BuscaConcepto;
var
   sKey, sDescription: String;
begin
     if dmCatalogos.cdsConceptosLookUp.Search( VACIO, sKey, sDescription ) then
     begin
          with cdsCampoRep do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CR_CALC' ).AsString := sKey;
          end;
     end;
end;

procedure TdmReportes.cdsLookupReportesLookupSearch(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
begin
     with Sender do
     begin
          if NOT Filtered then
          begin
               Filter := '(RE_TIPO =0) and (RE_ENTIDAD=271)';
               Filtered := TRUE;
          end;
          lOk := NOT IsEmpty;
          Filtered := FALSE;
     end;

     if lOk then
        lOk := ZetaBusqueda.ShowSearchForm( Sender, sFilter, sKey, sDescription )
     else ZInformation('B�squeda de Listado','No se han Definido Listados de Plazas en el Reporteador',0);
end;

function TdmReportes.DirectorioPlantillas: string;
begin
     {$ifdef RDD}
     with dmCliente do
          Result := VerificaDir( ServerReportes.DirectorioPlantillas(Empresa) );
     {$else}
     Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
     {$endif}
end;

procedure TdmReportes.cdsSuscripReportesLookupAlAdquirirDatos(
  Sender: TObject);
begin
     inherited;
     {$ifdef SUSCRIPCION}
     cdsLookupReportesEmpresa.Data := ServerReportes.GetLookUpReportes(EmpresaReportes, dmDiccionario.VerConfidencial);
     {$endif}
end;

end.
