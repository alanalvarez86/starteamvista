unit ZArbolTress;

interface

uses ComCtrls, Dialogs, SysUtils, DB, ZArbolTools;

function  GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );

implementation

uses ZetaDespConsulta,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     DGlobal,
     DSistema,
     DCliente;
     

function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma   := TRUE;
     Result.Caption   := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma   := FALSE;
     Result.Caption   := sCaption;
     Result.IndexDerechos  := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     with GLobal do
     begin
          Result := Folder( VACIO, 0 );
     end;
end;

procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
   procedure NodoNivel( const iNivel, iNodo: Integer );
   begin
        oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
   end;

begin
end;

procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin
     with oArbolMgr do
     begin
          Configuracion := TRUE;
          oArbolMgr.NumDefault := 0;
          CreaArbolDefault( oArbolMgr );
          Arbol.FullExpand;
     end;
end;

end.
