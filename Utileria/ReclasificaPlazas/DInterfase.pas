unit DInterfase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
  DReportesGen,
  ZetaClientDataSet,
  ZetaCommonLists,
  ZetaCommonClasses;

type
  TdmInterfase = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FHuboCambios, FProcesandoThread : Boolean;
    FSuperActual: String;
    FSuperNuevo: String;
    FObservaciones: String;
    dmReportesGen: TdmReportesGen;
    FParametros: TZetaParams;
    function cdsDataSet: TZetaClientDataSet;
    function GetHeaderTexto(const iPlaza: Integer): String;
    procedure SetNuevosEventos;
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure EscribeBitacora(const sMensaje: String; const iPlaza: Integer = 0 );
    procedure EscribeError(const sMensaje: String; const iPlaza: Integer = 0 );
    procedure ReportaProcessDetail(const iFolio: Integer);
    procedure LeerPlazasAProcesar;
    procedure GrabaCambioSupervisor( const iPlaza: Integer);
    function GetNextOrdenPuesto(const sPuesto: string ): integer;

  public
    { Public declarations }
    property ProcesandoThread : Boolean read FProcesandoThread write FProcesandoThread;
    property SuperActual : String read FSuperActual write FSuperActual;
    property SuperNuevo : String read FSuperNuevo write FSuperNuevo;
    property Observaciones: String read FObservaciones write FObservaciones;
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure Procesa( oParametros: TZetaParams );
  end;

var
  dmInterfase: TdmInterfase;

implementation

uses dProcesos,
     dCliente,
     dRecursos,
     dTablas,
     dConsultas,
     dGlobal,
     dReportes,
     FTressShell,
     ZBaseThreads,
     ZGlobalTress,
     ZetaCommonTools;

{$R *.DFM}

{ TdmInterfase }

procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     SetNuevosEventos;
end;

function TdmInterfase.cdsDataSet: TZetaClientDataSet;
begin
     Result := dmReportesGen.cdsResultados;
end;

{ Manejo de Fin de Proceso - Bit�cora }

procedure TdmInterfase.HandleProcessEnd( const iIndice, iValor: Integer );
var
   oProcessData: TProcessInfo;

   function GetResultadoProceso: String;
   begin

        case oProcessData.Status of
             epsOK: Result := '******* Proceso Terminado *******';
             epsCancelado: Result := '******* Proceso Cancelado *******';
             epsError, epsCatastrofico: Result := '****** Proceso Con Errores ******';
        else
             Result := ZetaCommonClasses.VACIO;
        end;
   end;

begin
     oProcessData := TProcessInfo.Create( nil );
     try
        with dmProcesos do
        begin
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             oProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             ReportaProcessDetail( oProcessData.Folio );
        end;
        EscribeBitacora( GetResultadoProceso );
     finally
        FreeAndNil( oProcessData );
        FProcesandoThread := FALSE;      // Mientras se encuentra la forma de manejar los threads
     end;
end;

procedure TdmInterfase.ReportaProcessDetail( const iFolio: Integer );
const
     K_MENSAJE_BIT = 'Fecha Movimiento: %s - %s';
var
   sMensaje: String;
begin
     with dmConsultas do
     begin
          GetProcessLog( iFolio );
          with cdsLogDetail do
          begin
               if ( not IsEmpty ) then
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         sMensaje := Format( K_MENSAJE_BIT, [ ZetaCommonTools.FechaCorta( FieldByName( 'BI_FEC_MOV' ).AsDateTime ),
                                                              FieldByName( 'BI_TEXTO' ).AsString ] );
                         case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                              tbNormal, tbAdvertencia : EscribeBitacora( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                              tbError, tbErrorGrave : EscribeError( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

{ Utilerias de Bit�cora }

function TdmInterfase.GetHeaderTexto( const iPlaza: Integer ): String;
const
     K_HEADER_PLAZA = 'Plaza No. %d : ';
begin
     Result := VACIO;
     if ( iPlaza > 0 ) then
        Result := Format( K_HEADER_PLAZA, [ iPlaza ] )
     else
        Result := FechaCorta( date ) + '-' + FormatDateTime( 'hh:mm', Now ) + ' : ';
end;

procedure TdmInterfase.EscribeBitacora(const sMensaje: String; const iPlaza: Integer = 0 );
begin
     if ( not FHuboCambios ) and ( iPlaza > 0 ) then       // Para saber si hay que poner un mensaje de que no hubo cambios
        FHuboCambios := TRUE;
     TressShell.EscribeBitacora( GetHeaderTexto( iPlaza ) + sMensaje );
end;

procedure TdmInterfase.EscribeError(const sMensaje: String; const iPlaza: Integer = 0 );
begin
     TressShell.EscribeError( GetHeaderTexto( 0 ) + GetHeaderTexto( iPlaza ) + sMensaje );
end;
{$ifdef antes}
procedure TdmInterfase.LeerPlazasAProcesar;
const
     Q_BUSCA_PLAZAS = 'select PL_FOLIO, CB_CODIGO from PLAZA ' +
                      'where ( PL_NIVEL%d = %s ) order by PL_FOLIO';
begin
     with dmConsultas do
     begin
          SQLText := Format( Q_BUSCA_PLAZAS, [ Global.GetGlobalInteger(K_GLOBAL_NIVEL_SUPERVISOR),
                                               EntreComillas( FSuperActual ) ] );
          cdsQueryGeneral.Refrescar;
     end;
end;
{$else}
procedure TdmInterfase.LeerPlazasAProcesar;
begin
     if dmReportesGen = NIL then
        dmReportesGen := TdmReportesGen.Create( self );

     with dmReportesGen do
     begin
          if NOT ProcesaReporte(FParametros.ParamByName('ReportePlazas').AsInteger) then
          begin
               EscribeError( Format( 'El reporte %d no contiene informaci�n', [FParametros.ParamByName('ReportePlazas').AsInteger] ) );
          end;

     end;
end;
{$endif}

procedure TdmInterfase.Procesa( oParametros: TZetaParams );
begin
     FPArametros := oParametros;
     FHuboCambios := FALSE;
     FProcesandoThread := TRUE;
     EscribeBitacora( '******* Proceso Iniciado *******' );
     LeerPlazasAProcesar;
     Application.ProcessMessages;
     with cdsDataSet do
     begin
          First;
          while ( not EOF ) and FProcesandoThread do
          begin
               GrabaCambioSupervisor( FieldByName( 'PL_FOLIODATASET' ).AsInteger );
               Next;
               Application.ProcessMessages;
          end;
     end;
     if ( not FHuboCambios ) then
        EscribeBitacora( 'No se Encontraron Cambios que Procesar.' )
     else 
        EscribeBitacora( '******* Proceso Terminado *******' );
end;

function TdmInterfase.GetNextOrdenPuesto(const sPuesto: string ): integer;
 const
      Q_NEXT_PUESTO = 'select MAX(PL_ORDEN)+1 ORDEN FROM PLAZA WHERE PU_CODIGO = %s';

begin
     with dmConsultas do
     begin
          SQLText := Format( Q_NEXT_PUESTO, [ EntreComillas( sPuesto ) ] );
          cdsQueryGeneral.Refrescar;
          Result := cdsQueryGeneral.FieldByName('ORDEN').AsInteger;
     end;
end;

procedure TdmInterfase.GrabaCambioSupervisor( const iPlaza: Integer );
const
     K_MENSAJE_PLAZA = 'Se cambi� el campo %s ( De %s a %s )';
var
     sCambios,sField: string;
     i, iEmpleado: integer;
     lCambiaKardex: Boolean;
begin
     dmCliente.SetTipoLookupEmpleado(eLookEmpPlaza);
     with dmRecursos do
     begin
          RefrescaPlaza( iPlaza, FALSE );
          with cdsPlazas do
          begin
               if ( not IsEmpty ) then
               begin
                    try
                       sCambios := VACIO;
                       lCambiaKardex := FALSE;
                       Edit;

                       for i:= 0 to FParametros.Count - 1 do
                       begin
                            sField := FParametros[i].Name;
                            if ( Copy( sField, 1, 3 ) = 'PL_' ) or ( sField = 'PU_CODIGO' ) then
                            begin
                                 if ( sField <> 'PL_REPORTA' ) or
                                    ( ( sField = 'PL_REPORTA' ) and
                                      ( FieldByName('PL_FOLIO').AsInteger <> FParametros[i].AsInteger ) ) then
                                 begin

                                      if ( FieldByName(sField).Value <> FParametros[i].Value ) then
                                      begin
                                           lCambiaKardex := ( sField = 'PU_CODIGO' ) or
                                                            ( sField = 'PL_TURNO' ) or
                                                            ( Pos( 'PL_NIVEL', sField ) <> 0 );
                                           if not lCambiaKardex then
                                           begin
                                                iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                                                if ( sField = 'PL_CLASIFI' ) and ( iEmpleado <> 0 ) then
                                                begin           // Revisa si el empleado tiene tabulador y no aplica el kardex
                                                     if ( not ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( iEmpleado ) ) = VACIO ) ) then
                                                     lCambiaKardex := ( not zStrToBool( dmCliente.cdsEmpleadoLookup.FieldByName( 'CB_AUTOSAL' ).AsString ) );
                                                end;
                                           end;

                                           if (sField = 'PU_CODIGO') then
                                              FieldByName('PL_ORDEN').AsInteger := GetNextOrdenPuesto(FParametros[i].Value);


                                           sCambios := sCambios + Format( K_MENSAJE_PLAZA,
                                                                          [ sField,
                                                                            FieldByName(sField).Value,
                                                                            FParametros[i].Value ] ) +
                                                                            CR_LF;
                                           FieldByName(sField).Value := FParametros[i].Value;
                                      end;
                                 end;
                            end;
                       end;
                       Post;

                       if lCambiaKardex then
                          ModificarPlaza( FParametros.ParamByName('FechaCambio').AsDate,
                                          FParametros.ParamByName('Observaciones').AsString )
                       else
                           ModificarPlaza( NullDateTime );

                       if ( ChangeCount = 0 ) then
                       begin
                            if StrVacio(sCambios) then
                               sCambios := 'Ning�n dato cambi�';
                            EscribeBitacora( sCambios, iPlaza );
                       end;
                    except
                          on Error: Exception do
                          begin
                               EscribeError( Error.Message, iPlaza );
                          end;
                    end;
               end
               else
                   EscribeError( 'No se Encontr� plaza con ese N�mero -  No se puede modificar', iPlaza );
          end;
     end;
end;

procedure TdmInterfase.SetNuevosEventos;
begin
     with dmRecursos.cdsPlazas do
     begin
          OnReconcileError := self.cdsReconcileError;
     end;
end;


{$ifdef VER130}
procedure TdmInterfase.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     EscribeError( E.Message, DataSet.FieldByName( 'CB_CODIGO' ).AsInteger );
     Action := raCancel;
end;
{$else}
procedure TdmInterfase.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     EscribeError( E.Message, DataSet.FieldByName( 'CB_CODIGO' ).AsInteger );
     Action := raCancel;
end;
{$endif}




end.
