inherited dmReportes: TdmReportes
  OldCreateOrder = True
  Left = 899
  Top = 137
  object cdsSuscripReportesLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsSuscripReportesLookupAlAdquirirDatos
    LookupName = 'Reportes'
    LookupDescriptionField = 'RE_NOMBRE'
    LookupKeyField = 'RE_CODIGO'
    Left = 120
    Top = 296
  end
  object cdsLookupReportesEmpresa: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    AlAdquirirDatos = cdsSuscripReportesLookupAlAdquirirDatos
    LookupName = 'Reportes'
    LookupDescriptionField = 'RE_NOMBRE'
    LookupKeyField = 'RE_CODIGO'
    Left = 120
    Top = 352
  end
end
