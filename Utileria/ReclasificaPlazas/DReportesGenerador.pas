unit DReportesGenerador;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DReportesMail.pas                          ::
  :: Descripci�n: Programa principal de TressEMail.exe       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$DEFINE REPORTING}
{.$UNDEF REPORTING}

uses Windows, Messages, SysUtils, Classes, Graphics, ComObj,
     Controls, Forms, Dialogs, Db, DBClient, {Psock, NMsmtp,}
     //FileCtrl,
     {$ifndef VER130}
     Variants,
     {$endif}
     FAutoClasses,
     ZAgenteSQL,
     ZetaWinAPITools,
     ZetaAsciiFile, { GA: 08/Ene/2004: Bajar a clases descendientes para evitar que se genere una bit�cora en todos los casos }
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     {$ifdef DOS_CAPAS}
       {$ifdef TRESS}
           {$ifdef REPORTING}
           DServerReporting,
           {$else}
           //DServerCalcNomina,
           {$endif}
       {$endif}
       {$ifdef SELECCION}DServerSelReportes,{$endif}
       {$ifdef VISITANTES}DServerVisReportes,{$endif}
       {$ifdef TRESS}DServerSuper,{$endif}
       {$ifdef ADUANAS}DServerReporter,{$endif}
       DServerReportes,
       DServerLogin,
       DZetaServerProvider,
     {$else}
       {$ifdef TRESS}
           {$ifdef REPORTING}
           Reporteador_TLB,
           {$else}
           //DCalcNomina_TLB,
           {$endif}
       {$endif}
       {$ifdef SELECCION}SelReportes_TLB,{$endif}
       {$ifdef VISITANTES}VisReportes_TLB,{$endif}
       {$ifdef TRESS}Super_TLB,{$endif}
       {$ifdef ADUANAS}ReportWriter_TLB,{$endif}
       Reportes_TLB,
       Login_TLB,
     {$endif}
     ZReportTools;


{$ifdef REPORTING}
{$ifdef TRESS}
  type
  {$ifdef DOS_CAPAS}
    TdmServerCalcNomina = TdmServerReporting;
  {$else}
    IdmServerCalcNominaDisp = IdmServerReportingDisp;
  {$endif}
{$ENDIF} 
{$endif}

type
  TLogCallBack = procedure( const sText: String ) of object;
  TdmReportGenerator = class(TDataModule)
    cdsSuscripcion: TZetaClientDataSet;
    cdsReporte: TZetaClientDataSet;
    cdsCampoRep: TZetaClientDataSet;
    cdsUsuarios: TZetaClientDataSet;
    cdsResultados: TZetaClientDataSet;
    cdsEmpleados: TZetaClientDataSet;
    cdsPlantilla: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FParams: TZetaParams;
    FParametros: TStrings;
    FCampos: TStrings;
    FOrden: TStrings;
    FFiltros: TStrings;
    FGrupos: TStrings;
    FSoloTotales: Boolean;
    FContieneImagenes: Boolean;
    FBitacora: TAsciiLog;
    FFrecuencia: Integer;
    FParamCount: Integer;
    FEmpresa: String;
    FNombreReporte: String;
    FCodigoReporte: Integer;
    FLogCallBack: TLogCallBack;
    FLogFileName: String;
    FFiltroFormula, FFiltroDescrip : string;
    FDatosImpresion : TDatosImpresion;
    FParamValues: array[ 1..K_MAX_PARAM ] of String;
    {$ifdef DOS_CAPAS}
    function GetServerReportesBDE: {$ifdef TRESS}TdmServerCalcNomina;{$endif}
                                   {$ifdef SELECCION}TdmServerSelReportes;{$endif}
                                   {$ifdef VISITANTES}TdmServerVisReportes;{$endif}
                                   {$ifdef ADUANAS}TdmServerReporter;{$endif}
    function GetServerReportes: TdmServerReportes;
    function GetServerLogin: TdmServerLogin;
       {$ifdef TRESS}
       function GetServerSuper: TdmServerSuper;
       {$endif}
    {$else}
    FServerReportes: IdmServerReportesDisp;
    FServerLogin: IdmServerLoginDisp;
    FServerReportesBDE:{$ifdef TRESS}IdmServerCalcNominaDisp;{$endif}
                       {$ifdef SELECCION}IdmServerSelReportesDisp;{$endif}
                       {$ifdef VISITANTES}IdmServerVisReportesDisp;{$endif}
                       {$ifdef ADUANAS}IdmServerReportWriterDisp;{$endif}
      {$ifdef TRESS}
      FServerSuper: IdmServerSuperDisp;
      {$endif}

    function GetServerReportes: IdmServerReportesDisp;
    
    function GetServerLogin: IdmServerLoginDisp;
    function GetServerReportesBDE: {$ifdef TRESS}IdmServerCalcNominaDisp;{$endif}
                                   {$ifdef SELECCION}IdmServerSelReportesDisp;{$endif}
                                   {$ifdef VISITANTES}IdmServerVisReportesDisp;{$endif}
                                   {$ifdef ADUANAS}IdmServerReportWriterDisp;{$endif}

      {$ifdef TRESS}
      function GetServerSuper: IdmServerSuperDisp;
      {$endif}
    {$endif}
    function AgregaFiltroSupervisor(oSQLAgente: TSQLAgente): Boolean;
    procedure AsignaLista(oLista: TStrings);
    procedure CreaBitacora;
    procedure ResetParamValues;
  protected
    { Protected declarations }
    FSQLAgente: TSQLAgente;
    FAppParams: TStrings;
    FNombresImagenes : TStrings;
    FPageCount : integer;
    FUnSoloHTML : Boolean;
    FMostrarError : Boolean;
    FUsaMismoResultado: Boolean;
    property Empresa: String read FEmpresa write FEmpresa;
    property Frecuencia: Integer read FFrecuencia write FFrecuencia;
    property Params: TZetaParams read FParams;
    property NombreReporte: string read FNombreReporte;
    property CodigoReporte: Integer read FCodigoReporte;


    property SoloTotales: Boolean read FSoloTotales;
    property ContieneImagenes: Boolean read FContieneImagenes write FContieneImagenes;
    {$ifdef DOS_CAPAS}
    property ServerReportesBDE: {$ifdef TRESS}TdmServerCalcNomina{$endif}
                                {$ifdef SELECCION}TdmServerSelReportes{$endif}
                                {$ifdef VISITANTES}TdmServerVisReportes{$endif}
                                {$ifdef ADUANAS}TdmServerReporter{$endif}read GetServerReportesBDE;

    property ServerReportes: TdmServerReportes read GetServerReportes;
    property ServerLogin: TdmServerLogin read GetServerLogin;
       {$ifdef TRESS}
       property ServerSuper: TdmServerSuper read GetServerSuper;
       {$endif}
    {$else}
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
    property ServerLogin: IdmServerLoginDisp read GetServerLogin;
    property ServerReportesBDE: {$ifdef TRESS}IdmServerCalcNominaDisp{$endif}
                                {$ifdef SELECCION}IdmServerSelReportesDisp{$endif}
                                {$ifdef VISITANTES}IdmServerVisReportesDisp{$endif}
                                {$ifdef ADUANAS}IdmServerReportWriterDisp{$endif}read GetServerReportesBDE;
      {$ifdef TRESS}
      property ServerSuper: IdmServerSuperDisp read GetServerSuper;
      {$endif}
    {$endif}
    function EvaluaParametros( var oParams: OleVariant ): Boolean;overload;
    function GeneraSQL( var Error : widestring ): Boolean;
    function GetReportes( const iReporte: Integer ): Boolean;
    function InitAutorizacion( const eModulo: TModulos ): Boolean;
    function PreparaAgente( oParams: OleVariant ): Boolean;
    function PreparaParamsReporte: Boolean;
    procedure AsignaListas;
    procedure Log(const sTexto: String);
    procedure LogError( const sTexto: String; const lEnviar: Boolean = FALSE );virtual;
    procedure SetLogFileName(const sValue: String);

    procedure DoAfterGetResultado( const lResultado: Boolean; const Error : string );dynamic;
    procedure DoBeforeGetResultado;dynamic;
    procedure DoOnGetResultado( const lResultado: Boolean; const Error : string );dynamic;
    procedure DoOnGetDatosImpresion;dynamic;
    procedure DoOnGetReportes( const lResultado: Boolean ); dynamic;
    function GetExtensionDef : string;dynamic;
    function PreparaPlantilla( var sError : WideString ) : Boolean;dynamic;
    procedure DesPreparaPlantilla;dynamic;
    function GetResultado(const iReporte: integer; const lUsaPlantilla: Boolean ): Boolean;
    function GetDatosImpresion: TDatosImpresion;
    function GetEmpresa: Olevariant;virtual;
  public
    { Public declarations }
    property AppParams: TStrings read FAppParams;
    property OnLogCallBack: TLogCallBack read FLogCallBack write FLogCallBack;

    property Campos: TStrings read FCampos;
    property Orden: TStrings read FOrden;
    property Filtros: TStrings read FFiltros;
    property Grupos: TStrings read FGrupos;
    property Parametros: TStrings read FParametros;
    property DatosImpresion: TDatosImpresion read FDatosImpresion write FDatosImpresion;
    property PageCount: integer read FPageCount write FPageCount;
    property UnSoloHTML: Boolean read FUnSoloHTML write FUnSoloHTML;
    property MostrarError: Boolean read FMostrarError write FMostrarError default FALSE;
    function EvaluaParametros( oSQLAgente : TSQLAgente;Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;overload;
    procedure SetParamValue( const iParametro: Integer; const sValor: String );
    function GetPlantilla( const Plantilla: string ): TBlobField;
    function GetLogo( const Logo: string ): TBlobField;

 end;

var
  dmReportGenerator: TdmReportGenerator;

 const
      k_file = 'D:\3win_13\Datos\Fuentes\PlantillasDatos\texto.txt';

{$ifdef RW}
procedure Save( const sMsg: string );overload;
procedure Save( const iMsg: integer );overload;
{$ENDIF}


implementation

uses DBasicoCliente,
     DCliente,
     DCatalogos,
     DGlobal,
     DDiccionario,
     ZetaRegistryCliente,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZetaDialogo,
     ZGlobalTress;

{$R *.DFM}

{$IFDEF RW}
procedure Save( const sMsg: string );
 var
    oLista: TStrings;

begin
     oLista:= TStringList.Create;
     if FileExists( k_file ) then
        oLista.LoadFromFile(  k_file );
     oLista.Add( sMsg );
     oLista.SaveToFile(  k_file );
     oLista.Free;
end;

procedure Save( const iMsg: integer );
begin
     Save('dmReportes- - ' + IntToStr(iMsg));
end;
{$ENDIF}

{ ****** TdmReportGenerator ******* }

procedure TdmReportGenerator.DataModuleCreate(Sender: TObject);
begin
     //CreaBitacora;
     if dmCliente = NIL then
        dmCliente := TdmCliente.Create( Self );

     if dmCatalogos = NIL then
        dmCatalogos := TdmCatalogos.Create( Self );

     if dmDiccionario = NIL then
        dmDiccionario := TdmDiccionario.Create( Self );

     if Global = NIL then
        Global := TdmGlobal.Create;
        
     FSQLAgente := TSQLAgente.Create;
     FParams := TZetaParams.Create;
     FAppParams := TStringList.Create;
     FCampos := TStringList.Create;
     FGrupos := TStringList.Create;
     FOrden := TStringList.Create;
     FFiltros := TStringList.Create;
     FParametros := TStringList.Create;
     FNombresImagenes := TStringList.Create;
     FUsaMismoResultado := FALSE;
     ResetParamValues;
end;

procedure TdmReportGenerator.DataModuleDestroy(Sender: TObject);
begin
     Log( 'Fin de Proceso: '+ FormatDateTime( 'dd/mmm/yyyy hh:mm:ss', Now ) );
     FreeAndNil( FNombresImagenes );
     FreeAndNil( FBitacora );
     FreeAndNil( FCampos );
     FreeAndNil( FOrden );
     FreeAndNil( FGrupos );
     FreeAndNil( FFiltros );
     FreeAndNil( FParametros );
     FreeAndNil( FAppParams );
     FreeAndNil( FParams );
     FreeAndNil( FSQLAgente );
     //FreeAndNil( Global );
     FreeAndNil( dmDiccionario );
     //FreeAndNil( dmCliente );
     FreeAndNil( dmCatalogos );
end;

{$ifdef DOS_CAPAS}
function TdmReportGenerator.GetServerReportes: TdmServerReportes;
begin
     Result := DCliente.dmCliente.ServerReportes;
end;

function TdmReportGenerator.GetServerReportesBDE: {$ifdef TRESS}TdmServerCalcNomina{$endif}{$ifdef SELECCION}TdmServerSelReportes{$endif}{$ifdef VISITANTES}TdmServerVisReportes{$endif}{$ifdef ADUANAS}TdmServerAduanas{$endif};
begin
     {$ifdef REPORTING}
     Result := DCliente.dmCliente.ServerReporteador;
     {$else}
     Result := DCliente.dmCliente.ServerReporteador;
     {$endif}
end;
{$ifdef TRESS}
function TdmReportGenerator.GetServerSuper: TdmServerSuper;
begin
     Result := DCliente.dmCliente.ServerSuper;
end;
{$endif}

function TdmReportGenerator.GetServerLogin: TdmServerLogin;
begin
     Result := DCliente.dmCliente.ServerLogin;
end;

{$else}
function TdmReportGenerator.GetServerReportesBDE: {$ifdef TRESS}IdmServerCalcNominaDisp{$endif}
                                                  {$ifdef SELECCION}IdmServerSelReportesDisp{$endif}
                                                  {$ifdef VISITANTES}IdmServerVisReportesDisp{$endif}
                                                  {$ifdef ADUANAS}IdmServerReportWriterDisp{$endif};
begin
     Result := {$ifdef TRESS}IdmServerCalcNominaDisp( dmCliente.CreaServidor( {$ifdef REPORTING}CLASS_dmServerReporting{$ELSE}CLASS_dmServerCalcNomina{$ENDIF},{$endif}
               {$ifdef SELECCION}IdmServerSelReportesDisp( dmCliente.CreaServidor( CLASS_dmServerSelReportes,{$endif}
               {$ifdef VISITANTES}IdmServerVisReportesDisp( dmCliente.CreaServidor( CLASS_dmServerVisReportes,{$endif}
               {$ifdef ADUANAS}IdmServerReportWriterDisp( dmCliente.CreaServidor( CLASS_dmServerReportWriter,{$endif}
               FServerReportesBDE ) );
end;

{$ifdef TRESS}
function TdmReportGenerator.GetServerSuper: IdmServerSuperDisp;
begin
     Result:= IdmServerSuperDisp( dmCliente.CreaServidor( CLASS_dmServerSuper, FServerSuper ) );
end;
{$endif}

function TdmReportGenerator.GetServerReportes: IdmServerReportesDisp;
begin
     Result := IdmServerReportesDisp( dmCliente.CreaServidor( CLASS_dmServerReportes, FServerReportes ) );
end;

function TdmReportGenerator.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( dmCliente.CreaServidor( CLASS_dmServerLogin, FServerLogin ) );
end;

{$endif}

procedure TdmReportGenerator.SetLogFileName( const sValue: String );
begin
     FLogFileName := sValue;
end;

procedure TdmReportGenerator.CreaBitacora;
begin
     if FBitacora = NIL then
     begin
          FBitacora := TAsciiLog.Create;
          with FBitacora do
          begin
               Init( FLogFileName );
          end;
     end;
end;

function TdmReportGenerator.InitAutorizacion( const eModulo: TModulos ): Boolean;
begin
     Autorizacion.Cargar( ServerLogin.GetAuto );
     begin
          with Autorizacion do
          begin
               Result := OkModulo( eModulo );
          end;
     end;
end;

{CV:21/Septiembre
Se puso el CreaBitacora en este m�todo, porque si el objeto TdmReportGenerator
o una descendencia se crea al principio de la aplicacion y se corre un segundo ejecutable,
el segundo exe marca error de "I/O File", debido a que la primera instancia abre la bitacora.}

procedure TdmReportGenerator.Log(const sTexto: String );
begin
     CreaBitacora;
     FBitacora.WriteTexto( sTexto );
     if Assigned( FLogCallBack ) then
        FLogCallBack( sTexto );
     {
     WriteLn( QuitaAcentos( sTexto ) );
     }
end;

procedure TdmReportGenerator.LogError(const sTexto: String;const lEnviar: Boolean );
begin
     Log( 'ERROR: '+ sTexto );
     Log( '' );
     if FMostrarError then
        ZError( 'Mensaje', sTexto, 0 );
end;

procedure TdmReportGenerator.AsignaLista( oLista: TStrings );
var
   i: Integer;
begin
     with oLista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with TCampoOpciones( Objects[ i ] ) do
               begin
                    if ( PosAgente >= 0 ) then
                    begin
                         SQLColumna := FSQLAgente.GetColumna( PosAgente );
                         TipoImp := SQLColumna.TipoFormula;
                    end;
               end;
          end;
     end;
end;

procedure TdmReportGenerator.AsignaListas;
var
   i: Integer;
begin
     AsignaLista( FCampos );
     with FGrupos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with TGrupoOpciones( Objects[ i ] ) do
               begin
                    AsignaLista( ListaEncabezado );
                    AsignaLista( ListaPie );
               end;
          end;
     end;
end;

function TdmReportGenerator.GetEmpresa: Olevariant;
begin
     Result := dmCliente.Empresa;
end;

function TdmReportGenerator.GetReportes( const iReporte: Integer ): Boolean;
var
   oCampoRep: OleVariant;
begin
     FCodigoReporte := iReporte;
     cdsReporte.Data := ServerReportes.GetEditReportes( GetEmpresa, iReporte, oCampoRep );
     Result := not cdsReporte.IsEmpty;
     if Result then
     begin
          cdsCampoRep.Data := oCampoRep;
          with cdsReporte do
          begin
               FNombreReporte := FieldByName( 'RE_NOMBRE' ).AsString;
               FSoloTotales := ZetaCommonTools.zStrToBool( FieldByName( 'RE_SOLOT' ).AsString );
          end;
     end;
end;

function TdmReportGenerator.EvaluaParametros( var oParams: OleVariant ): Boolean;
const
     P_TITULO = 1;
     P_FORMULA = 2;
     P_TIPO = 3;
var
   oParam, oSQLAgente: OleVariant;
   oColumna: TSQLColumna;
   i, iCount: Integer;
   sError: WideString;
   eTipo: eTipoGlobal;
begin
     Result := TRUE;
     FParametros.Clear;
     with cdsCampoRep do
     begin
          First;
          Filter := Format( 'CR_TIPO = %d', [ Ord( tcParametro ) ] );
          Filtered := TRUE;
          FParamCount := cdsCampoRep.RecordCount;
          while not Eof do
          begin
               AgregaListaObjeto( cdsCampoRep, FParametros );
               Next;
          end;
          Filtered := FALSE;
          Filter := Format( 'CR_TIPO <> %d', [ Ord( tcParametro ) ] );
          Filtered := TRUE;
     end;
     iCount := FParametros.Count;
     oParams := VarArrayCreate( [ 1, K_MAX_PARAM ], varVariant );
     FSQLAgente.Clear;

     if ( iCount > 0 ) then
     begin
          for i := 0 to ( iCount - 1 ) do
          begin
               AgregaSQLColumnasParam( FSQLAgente, TCampoMaster( FParametros.Objects[ i ] ) );
          end;
          oSQLAgente := FSQLAgente.AgenteToVariant;
          Result := ServerReportesBDE.EvaluaParam( dmCliente.Empresa, oSQLAgente, Params.VarValues, sError );
          if Result then
          begin
               FSQLAgente.VariantToAgente( oSQLAgente );
               for i := 0 to ( FParametros.Count - 1 ) do
               begin
                    with TCampoMaster( FParametros.Objects[ i ] ) do
                    begin
                         oParam := VarArrayCreate( [ P_TITULO, P_TIPO ], varVariant );
                         oParam[ P_TITULO ] := Titulo;
                         if ( PosAgente >= 0 ) then
                         begin
                              oColumna := FSQLAgente.GetColumna( PosAgente );
                              eTipo := oColumna.TipoFormula;
                              case eTipo of
                                   tgFecha: oParam[ P_FORMULA ] := StrToDate( oColumna.Formula );
                                   tgBooleano: oParam[ P_FORMULA ] := ZetaCommonTools.zStrToBool( oColumna.Formula );
                              else
                                  oParam[ P_FORMULA ] := oColumna.Formula;
                              end;
                         end
                         else
                         begin
                              eTipo := TipoCampo;
                              oParam[ P_FORMULA ] := Formula;
                         end;
                         oParam[ P_TIPO ] := eTipo;
                         { Cargar Valores Capturados (Si hay ) }
                         iCount := i + 1;
                         if ( iCount >= Low( FParamValues ) ) and ( iCount <= High( FParamValues ) ) then
                         begin
                              if ZetaCommonTools.StrLleno( FParamValues[ iCount ] ) then
                              begin
                                   try
                                      case eTipo of
                                           tgBooleano: oParam[ P_FORMULA ] := ZetaCommonTools.zStrToBool( FParamValues[ iCount ] );
                                           tgFloat: oParam[ P_FORMULA ] := StrToFloat( FParamValues[ iCount ] );
                                           tgNumero: oParam[ P_FORMULA ] := StrToFloat( FParamValues[ iCount ] );
                                           tgFecha: oParam[ P_FORMULA ] := ZetaCommonTools.StrAsFecha( FParamValues[ iCount ] );
                                           tgTexto: oParam[ P_FORMULA ] := FParamValues[ iCount ];
                                      end;
                                   except
                                         on Error: Exception do
                                         begin
                                              Error.Message := Format( 'Error En Valor Del Par�metro %s # %d: %s', [ ZetaCommonLists.ObtieneElemento( lfTipoGlobal, Ord( eTipo ) ), iCount, Error.Message ] );
                                              raise;
                                         end;
                                   end;
                              end;
                         end;
                    end;
                    oParams[ i + 1 ] := oParam;
               end;
          end;
     end;
end;

function TdmReportGenerator.AgregaFiltroSupervisor( oSQLAgente: TSQLAgente ) : Boolean;

function GetParametrosCdsEmpleados: OleVariant;
var
   FParams: TZetaParams;
begin
     FParams := TZetaParams.Create( Self );
     try
        with FParams do
        begin
             AddDate( 'Fecha', dmCliente.FechaDefault );
             AddString( 'RangoLista', '' );
             AddString( 'Condicion', '' );
             AddString( 'Filtro', '' );
             AddInteger( 'FiltroFijo', 0 );
             AddBoolean( 'LaborActivado', FALSE );
             Result := VarValues;
        end;
     finally
            FreeAndNil( FParams );
     end;
end;

begin
     Result := TRUE;
     {$ifdef TRESS}

     if eClasifiReporte( cdsReporte.FieldByName( 'RE_CLASIFI' ).AsInteger ) = crSupervisor then
     begin
          {Solamente se agrega el filtro supervisor si la tabla principal del reporte en
          cuestion, tiene relacion a COLABORA. Si el reporte no tiene relacion hacia
          COLABORA, }
          if FSQLAgente.Entidad in EntidadConCondiciones then
          begin
               dmCliente.Usuario := cdsSuscripcion.FieldByName( 'US_CODIGO' ).AsInteger ;
               cdsEmpleados.Data := ServerSuper.GetEmpleados( dmCliente.Empresa, GetParametroscdsEmpleados );

               oSQLAgente.AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero, 30, 'CB_CODIGO' );
               try
                  AgregaSQLFiltrosEspeciales( oSQLAgente, dmCliente.ConstruyeListaEmpleados( cdsEmpleados, 'COLABORA.CB_CODIGO' ), TRUE );
               except
                     On E:Exception do
                     begin
                          Result := FALSE;
                          LogError( E.Message, TRUE );
                     end;
               end;
               dmCliente.Usuario := 0;
          end;
     end
     {$endif}
end;

function TdmReportGenerator.PreparaAgente( oParams: OleVariant ):Boolean;
var
   eTipo: eTipoCampo;
   oLista: TStrings;
   oCampo: TCampoMaster;
   QU_CODIGO: string;
begin
     FSQLAgente.Clear;
     FSQLAgente.Parametros := oParams;

     DatosImpresion := GetDatosImpresion;
     FFiltroFormula := VACIO;
     FFiltroDescrip := VACIO;

     with cdsReporte do
     begin
          FSQLAgente.Entidad := TipoEntidad( FieldByName( 'RE_ENTIDAD' ).AsInteger );
          AgregaSQLFiltrosEspeciales( FSQLAgente, FieldByName( 'RE_FILTRO' ).AsString, FALSE, FParamCount );
          QU_CODIGO := FieldByName( 'QU_CODIGO' ).AsString;
          if ( Trim( QU_CODIGO ) > '' ) then
          begin
               dmCatalogos.cdsCondiciones.Refrescar;
               if dmCatalogos.cdsCondiciones.Locate( 'QU_CODIGO', VarArrayOf( [ QU_CODIGO ] ), [] ) then
               begin
                    AgregaSQLFiltrosEspeciales( FSQLAgente, dmCatalogos.cdsCondiciones.FieldByName( 'QU_FILTRO' ).AsString, FALSE, FParamCount );
               end;
          end;
     end;

     oLista := NIL;
     FCampos.Clear;
     FGrupos.Clear;
     FOrden.Clear;
     FFiltros.Clear;

     with cdsCampoRep do
     begin
          IndexFieldNames := 'RE_CODIGO;CR_TIPO;CR_POSICIO;CR_SUBPOS';
          First;
          while not Eof do
          begin
               eTipo := eTipoCampo( FieldByName( 'CR_TIPO' ).AsInteger );
               case eTipo of
                    tcCampos: oLista := FCampos;
                    tcGrupos: oLista := FGrupos;
                    tcOrden: oLista := FOrden;
                    tcEncabezado: oLista := TGrupoOpciones( FGrupos.Objects[ FieldByName( 'CR_POSICIO' ).AsInteger ] ).ListaEncabezado;
                    tcPieGrupo: oLista := TGrupoOpciones( FGrupos.Objects[ FieldByName( 'CR_POSICIO' ).AsInteger ] ).ListaPie;
                    tcFiltro: oLista := FFiltros;
                    tcParametro: oLista := FParametros;
               end;
               AgregaListaObjeto( cdsCampoRep, oLista );

               oCampo := TCampoMaster( oLista.Objects[ oLista.Count - 1 ] );
               case eTipo of
                    tcCampos: AgregaSQLColumnas( FSQLAgente, TCampoOpciones( oCampo ), FDatosImpresion, -1, FParamCount );
                    tcEncabezado, tcPieGrupo: AgregaSQLColumnas( FSQLAgente, TCampoOpciones( oCampo ), FDatosImpresion, FieldByName('CR_POSICIO').AsInteger, FParamCount );
                    tcGrupos: AgregaSQLGrupos( FSQLAgente, TGrupoOpciones( oCampo ) );
                    tcOrden: AgregaSQLOrdenes( FSQLAgente, TOrdenOpciones( oCampo ) );
                    tcFiltro: AgregaSQLFiltros( FSQLAgente, TFiltroOpciones( oCampo ), FFiltroFormula, FFiltroDescrip );

                    {
                    tcParametro: AgregaSQLColumnasParam( FSQLAgente, TCampoOpciones( oCampo ) );
                    }
               end;
               Next;
          end;
     end;
     Result := AgregaFiltroSupervisor( FSQLAgente );
end;

function TdmReportGenerator.PreparaParamsReporte:Boolean;
begin
     Result := TRUE;

     ZReportTools.ParametrosReportes( cdsReporte, dmCatalogos.cdsCondiciones, FParams,
                                      FFiltroFormula, FFiltroDescrip,
                                      FSoloTotales, TRUE, FALSE, FContieneImagenes );



          {28-Octubre: CV: Esta llamada se requiere por que los reportes especiales requieren
          agregar ciertos parametros especificos, al FParams. En el Servidor,
          se espera que lleguen estos parametros.}
          ZReportTools.ParametrosEspeciales( TipoEntidad( cdsReporte.FieldByName('RE_ENTIDAD').AsInteger ),
                                             FParams,
                                             FFiltros,
                                             FGrupos );


end;

function TdmReportGenerator.GeneraSQL( var Error : widestring ): Boolean;
var
   oSQLAgente, oParams: OleVariant;
   //Error: WideString;
begin
     oSQLAgente := FSQLAgente.AgenteToVariant;
     oParams := Params.VarValues;
     with cdsResultados do
     begin
          Init;
          Data := ServerReportesBDE.GeneraSQL( dmCliente.Empresa, oSQLAgente, oParams, Error);
          Result := not IsEmpty;
     end;
     if Result then
     begin
          {$ifdef CAROLINA}
          cdsResultados.SaveToFile( ExtractFileDir( ( Application.ExeName ) ) + '\Reportes.cds', dfBinary );
          {$endif}
          FSQLAgente.VariantToAgente( oSQLAgente );
          if not FSoloTotales then
          begin
               FSQLAgente.OrdenaDataset( cdsResultados );
          end;
     end
     else
     begin
          LogError( Error, True )
          {Result := StrLleno( Error );
          if Result then
             LogError( Error, True )
          else
          LogError( Format( 'El Reporte %s No Tiene Datos Que Cumplan Con Los Filtros Especificados', [ FNombreReporte ] ) );}
     end;
end;

procedure TdmReportGenerator.DoAfterGetResultado( const lResultado: Boolean; const Error : string );
begin

end;

procedure TdmReportGenerator.DoBeforeGetResultado;
begin

end;

procedure TdmReportGenerator.DoOnGetResultado(const lResultado: Boolean; const Error : string );
begin

end;

procedure TdmReportGenerator.DoOnGetDatosImpresion;
begin

end;

procedure TdmReportGenerator.DoOnGetReportes( const lResultado: Boolean );
begin
end;

function TdmReportGenerator.GetExtensionDef : string;
begin
     Result := '';
end;


function TdmReportGenerator.GetResultado( const iReporte : integer; const lUsaPlantilla : Boolean ): Boolean;
 var
    oParams : OleVariant;
    Error : wideString;
begin
     Result := GetReportes( iReporte );
     DoOnGetReportes(Result);
     if Result then
     begin
          dmCliente.CargaActivosTodos( Params );
          Result := EvaluaParametros( oParams );
          if Result then
          begin
               Result := PreparaAgente( oParams );
               if Result then
               begin
                    DoBeforeGetResultado;
                    if lUsaPlantilla AND ( eTipoFormato( cdsReporte.FieldByName('RE_PFILE').AsInteger ) <> tfMailMerge ) then
                    begin
                         Result := PreparaPlantilla( Error );
                    end;
                    PreparaParamsReporte;
                    if Result then
                    begin
                         try
                              if NOT FUsaMismoResultado then
                                 Result := GeneraSQL( Error )
                              else
                                  Result := TRUE;
                            DoOnGetResultado( Result, Error );

                            finally
                                   if lUsaPlantilla then
                                      DespreparaPlantilla;

                                   DoAfterGetResultado( Result, Error );
                            end;
                    end
                    else
                        LogError( Format( 'Problemas Al Cargar Plantilla del Reporte #%d' + CR_LF + Error, [ iReporte ] ), TRUE )
               end
               {else
                   LogError( Format( 'Problemas Al Preparar Reporte #%d', [ iReporte ] ), TRUE )}
          end
          else
              LogError( Format( 'Problemas Al Evaluar Los PARAMETROS Del Reporte #%d', [ iReporte ] ), TRUE );
     end
     else
         LogError( Format( 'El Reporte #%d No Existe', [ iReporte ] ), TRUE );

     ResetParamValues;
end;

function TdmReportGenerator.PreparaPlantilla( var sError : WideString ) : Boolean;
begin
     Result := TRUE;
end;

procedure TdmReportGenerator.DesPreparaPlantilla;
begin

end;

function TdmReportGenerator.GetDatosImpresion : TDatosImpresion;
 var
    oDirDefault : string;
    sError : string;
begin
     with Result do
     begin
          with cdsReporte do
          begin
               if eTipoReporte(FieldByName('RE_TIPO').AsInteger) = trForma then
               begin
                    Tipo := ZReportTools.GetTipoFormatoForma(eFormatoFormas(FieldByName('RE_PFILE').AsInteger) );
               end
               else
               begin
                    Tipo := eTipoFormato(FieldByName('RE_PFILE').AsInteger);
               end;
               Archivo := FieldByName('RE_REPORTE').AsString;
               Exportacion := FieldByName('RE_ARCHIVO').AsString;
               Grafica := 'GRAFICA.QR2';
               oDirDefault := zReportTools.DirPlantilla;

               Archivo := GetNombreArchivo( Result, sError );
               Exportacion := GetNombreExportacion( Result, GetExtensionDef, sError );
               Separador := Copy(FieldByName('RE_CFECHA').AsString,1,1);
               //Si la impresora es -1, siempre toma la impresora default de la maquina.
               Impresora := -1;
          end;
     end;
end;

function TdmReportGenerator.EvaluaParametros( oSQLAgente : TSQLAgente;Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;
begin
     Result := TRUE;
end;

procedure TdmReportGenerator.ResetParamValues;
var
   i: Integer;
begin
     for i := Low( FParamValues ) to High( FParamValues ) do
     begin
          FParamValues[ i ] := VACIO;
     end;
end;

procedure TdmReportGenerator.SetParamValue( const iParametro: Integer; const sValor: String );
begin
     if ( iParametro > 0 ) and ( iParametro <= K_MAX_PARAM ) then
     begin
          FParamValues[ iParametro ] := Trim( sValor );
     end;
end;


function TdmReportGenerator.GetPlantilla( const Plantilla: string ): TBlobField;
begin
     cdsPlantilla.Data := ServerReportes.GetPlantilla( Plantilla );
     Result := TBlobField( cdsPlantilla.FieldByName( 'CampoBlob' ) );
end;

function TdmReportGenerator.GetLogo( const Logo: string ): TBlobField;
begin
     cdsPlantilla.Data := ServerReportes.GetPlantilla( Logo );
     Result := TBlobField( cdsPlantilla.FieldByName( 'CampoBlob' ) );
end;
end.

