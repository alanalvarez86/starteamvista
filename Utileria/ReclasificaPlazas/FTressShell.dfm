inherited TressShell: TTressShell
  Left = 284
  Top = 173
  Height = 418
  Caption = 'Proceso de Reclasificaci'#243'n de Plazas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 353
  end
  inherited PageControl: TPageControl
    Top = 41
    Height = 278
    inherited TabBitacora: TTabSheet
      inherited MemBitacora: TMemo
        Top = 37
        Height = 213
        ScrollBars = ssVertical
      end
      inherited PanelBottom: TPanel
        Top = 0
        Height = 37
        Align = alTop
      end
    end
    inherited TabErrores: TTabSheet
      inherited Panel1: TPanel
        Top = 0
        Height = 37
        Align = alTop
      end
      inherited MemErrores: TMemo
        Top = 37
        Height = 205
        ScrollBars = ssVertical
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 319
    Align = alBottom
    inherited BtnProcesar: TBitBtn
      Left = 9
      Hint = 'Ejecutar Reclasificaci'#243'n de Plazas'
      Caption = '&Ejecutar Proceso'
    end
    inherited BtnDetener: TBitBtn
      Left = 144
      Visible = False
    end
    inherited BtnSalir: TBitBtn
      Left = 281
    end
  end
  inherited PanelEncabezado: TPanel
    Height = 41
    Visible = False
    inherited LblArchivo: TLabel
      Left = 12
      Visible = False
    end
    inherited ArchivoSeek: TSpeedButton
      Visible = False
    end
    inherited SistemaFechaDia: TLabel
      Left = 225
      Top = 15
    end
    inherited ArchOrigen: TEdit
      Visible = False
    end
  end
  inherited LogoffTimer: TTimer
    Left = 296
    Top = 64
  end
  inherited ActionList: TActionList
    Left = 346
    Top = 68
  end
  inherited ShellMenu: TMainMenu
    inherited Archivo1: TMenuItem
      inherited CancelarProceso2: TMenuItem
        Visible = False
      end
    end
  end
  inherited OpenDialog: TOpenDialog
    Left = 366
    Top = 74
  end
end
