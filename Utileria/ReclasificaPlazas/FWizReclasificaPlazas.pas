unit FWizReclasificaPlazas;

interface

uses
  Windows, Messages, SysUtils,
  {$ifndef VER130}
  Variants,
  {$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizard, StdCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaEdit,
  ZetaKeyLookup, ComCtrls, Buttons, ZetaWizard, ExtCtrls, DBCtrls,
  ZetaNumero, Grids, DBGrids, ZetaDBGrid, DB;

type
  TWizPlazas = class(TBaseWizard)
    Contratacion: TTabSheet;
    Area: TTabSheet;
    Salario: TTabSheet;
    Generales: TTabSheet;
    DataSource: TDataSource;
    tsReporte: TTabSheet;
    GroupBox2: TGroupBox;
    Formato: TZetaKeyLookup;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    PL_CODIGO: TZetaEdit;
    Label6: TLabel;
    PL_REPORTA: TZetaKeyLookup;
    PL_TIREP: TZetaKeyCombo;
    Label16: TLabel;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    PL_TIPO: TZetaKeyCombo;
    PL_NOMBRE: TZetaEdit;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    PL_CLASIFI: TZetaKeyLookup;
    PL_TURNO: TZetaKeyLookup;
    Label26: TLabel;
    Label29: TLabel;
    PL_PATRON: TZetaKeyLookup;
    PL_NIVEL0: TZetaKeyLookup;
    PL_CONTRAT: TZetaKeyLookup;
    PL_AREA: TZetaKeyLookup;
    Label21: TLabel;
    Arealbl: TLabel;
    Label27: TLabel;
    lblConfidencialidad: TLabel;
    GroupBox5: TGroupBox;
    PL_NIVEL1lbl: TLabel;
    PL_NIVEL1: TZetaKeyLookup;
    PL_NIVEL2: TZetaKeyLookup;
    PL_NIVEL3: TZetaKeyLookup;
    PL_NIVEL4: TZetaKeyLookup;
    PL_NIVEL5: TZetaKeyLookup;
    PL_NIVEL6: TZetaKeyLookup;
    PL_NIVEL7: TZetaKeyLookup;
    PL_NIVEL8: TZetaKeyLookup;
    PL_NIVEL9: TZetaKeyLookup;
    PL_NIVEL9lbl: TLabel;
    PL_NIVEL8lbl: TLabel;
    PL_NIVEL7lbl: TLabel;
    PL_NIVEL6lbl: TLabel;
    PL_NIVEL5lbl: TLabel;
    PL_NIVEL4lbl: TLabel;
    PL_NIVEL3lbl: TLabel;
    PL_NIVEL2lbl: TLabel;
    GroupBox6: TGroupBox;
    LblTabulador: TLabel;
    PL_SALARIO: TZetaNumero;
    LblSalario: TLabel;
    Label33: TLabel;
    PL_PER_VAR: TZetaNumero;
    PL_TABLASS: TZetaKeyLookup;
    Label28: TLabel;
    Label31: TLabel;
    PL_ZONA_GE: TZetaKeyCombo;
    GroupBox7: TGroupBox;
    Label7: TLabel;
    PL_INGLES: TEdit;
    PL_NUMERO: TZetaNumero;
    PL_TEXTO: TEdit;
    PL_SUB_CTA: TEdit;
    Label10: TLabel;
    Label8: TLabel;
    CO_NUMEROLbl: TLabel;
    DatosRegistro: TTabSheet;
    GBMensaje: TGroupBox;
    MemoObserva: TMemo;
    FechaCambio: TZetaFecha;
    LblFecha: TLabel;
    PL_CHECA: TZetaKeyCombo;
    PL_AUTOSAL: TZetaKeyCombo;
    cbCambiaSalario: TCheckBox;
    cbCambiaPromedio: TCheckBox;
    cbNumero: TCheckBox;
    lblPuesto: TLabel;
    PU_CODIGO: TZetaKeyLookup;
    lbFechaInicio: TLabel;
    PL_FEC_INI: TZetaFecha;
    cbInicio: TCheckBox;
    GroupBox8: TGroupBox;
    PL_FEC_FIN: TZetaFecha;
    cbSinVence: TCheckBox;
    lblVence: TLabel;
    cbVence: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;var CanMove: Boolean);
    procedure cbVenceClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PL_AUTOSALClick(Sender: TObject);
    procedure cbCambiaSalarioClick(Sender: TObject);
    procedure cbCambiaPromedioClick(Sender: TObject);
    procedure cbNumeroClick(Sender: TObject);
    procedure cbInicioClick(Sender: TObject);
  private
    { Private declarations }
    procedure Connect;
    procedure DoConnect;
    procedure SetCamposNivel;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure CargaParametros;override;
    function EjecutarWizard: Boolean; override;
  end;

var
  WizPlazas: TWizPlazas;

implementation
uses
    dGlobal,
    dReportes,
    ZGlobalTress,
    ZetaCommonLists,
    ZetaClientTools,
    ZetaCommonTools,
    ZetaCommonClasses,
    dCatalogos,
    dRecursos,
    ZetaBuscaEmpleado,
    ZetaDialogo,
    dSistema,
    dTablas,
    DCliente, DInterfase;

{$R *.dfm}

procedure TWizPlazas.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_EMP_WIZ_PLAZA;
     cbVence.Checked:= False;
     DataSource.DataSet := dmRecursos.cdsListaEmpPlazas;
     with dmCatalogos do
     begin
          PU_CODIGO.LookupDataSet := cdsPuestos;
          PL_AREA.LookupDataSet := cdsAreas;
          PL_CLASIFI.LookupDataSet := cdsClasifi;
          PL_TURNO.LookupDataSet :=  cdsTurnos;
          PL_PATRON.LookupDataSet := cdsRPatron;
          PL_CONTRAT.LookupDataSet := cdsContratos;
          PL_TABLASS.LookupDataset := cdsSSocial;
     end;
     PL_NIVEL0.LookupDataset  := dmSistema.cdsNivel0;
     with dmTablas do
     begin
          PL_NIVEL9.LookupDataset  := cdsNivel9;
          PL_NIVEL8.LookupDataset  := cdsNivel8;
          PL_NIVEL7.LookupDataset  := cdsNivel7;
          PL_NIVEL6.LookupDataset  := cdsNivel6;
          PL_NIVEL5.LookupDataset  := cdsNivel5;
          PL_NIVEL4.LookupDataset  := cdsNivel4;
          PL_NIVEL3.LookupDataset  := cdsNivel3;
          PL_NIVEL2.LookupDataset  := cdsNivel2;
          PL_NIVEL1.LookupDataset  := cdsNivel1;
     end;
     PL_REPORTA.LookUpDataSet := dmRecursos.cdsPlazasLookup;
     Formato.LookupDataset := dmReportes.cdsLookupReportes;
     PL_CHECA.ListaFija := lfReclasificaBool;
     PL_TIREP.ListaFija := lfReclasificaRelacion;
     PL_TIPO.ListaFija := lfReclasificaTipoVigencia;
     PL_AUTOSAL.ListaFija := lfReclasificaBool;
     PL_ZONA_GE.ListaFija := lfReclasificaZonaGeo;
end;

procedure TWizPlazas.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := tsReporte;
     SetCamposNivel;
     DoConnect;
     FechaCambio.Valor := dmCliente.FechaDefault;
     PL_FEC_INI.Valor := dmCliente.FechaDefault;
     PL_FEC_FIN.Valor := dmCliente.FechaDefault + 1;
     PL_TIPO.ItemIndex := 0;
     PL_CHECA.ItemIndex := 0;
     PL_TIREP.ItemIndex := 0;
     PL_REPORTA.Valor := 0;
     PL_AUTOSAL.ItemIndex := 0;
     PL_ZONA_GE.ItemIndex := 0;
end;

procedure TWizPlazas.Connect;
begin
     with dmTablas do
     begin
          if PL_NIVEL1.Visible then cdsNivel1.Conectar;
          if PL_NIVEL2.Visible then cdsNivel2.Conectar;
          if PL_NIVEL3.Visible then cdsNivel3.Conectar;
          if PL_NIVEL4.Visible then cdsNivel4.Conectar;
          if PL_NIVEL5.Visible then cdsNivel5.Conectar;
          if PL_NIVEL6.Visible then cdsNivel6.Conectar;
          if PL_NIVEL7.Visible then cdsNivel7.Conectar;
          if PL_NIVEL8.Visible then cdsNivel8.Conectar;
          if PL_NIVEL9.Visible then cdsNivel9.Conectar;
     end;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmRecursos do
     begin
          cdsPlazasLookup.Conectar;
     end;

     with dmSistema do
     begin
          cdsNivel0.Conectar;
     end;

     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsAreas.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
          cdsContratos.Conectar;
          cdsSSocial.Conectar;
     end;

     dmReportes.cdsLookupReportes.Conectar;
end;

procedure TWizPlazas.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;

end;

procedure TWizPlazas.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, PL_NIVEL1lbl, PL_NIVEL1 );
     SetCampoNivel( 2, iNiveles, PL_NIVEL2lbl, PL_NIVEL2 );
     SetCampoNivel( 3, iNiveles, PL_NIVEL3lbl, PL_NIVEL3 );
     SetCampoNivel( 4, iNiveles, PL_NIVEL4lbl, PL_NIVEL4 );
     SetCampoNivel( 5, iNiveles, PL_NIVEL5lbl, PL_NIVEL5 );
     SetCampoNivel( 6, iNiveles, PL_NIVEL6lbl, PL_NIVEL6 );
     SetCampoNivel( 7, iNiveles, PL_NIVEL7lbl, PL_NIVEL7 );
     SetCampoNivel( 8, iNiveles, PL_NIVEL8lbl, PL_NIVEL8 );
     SetCampoNivel( 9, iNiveles, PL_NIVEL9lbl, PL_NIVEL9 );
end;

procedure TWizPlazas.WizardBeforeMove(Sender: TObject;var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if  Adelante then
          begin
               if EsPaginaActual ( tsReporte ) then
               begin
                    if StrVacio( Formato.Llave ) then
                       CanMove := Error( 'El n�mero de Reporte no puede quedar vac�o', Formato );
               end
               else if EsPaginaActual ( Parametros ) then
               begin
                    PL_NIVEL0.Enabled := dmSistema.HayNivel0;
                    lblConfidencialidad.Enabled := PL_NIVEL0.Enabled;

                     if PL_NIVEL0.Enabled then
                     begin
                         if StrLleno( dmCliente.Confidencialidad ) then
                         begin
                              PL_NIVEL0.Filtro :=  Format( 'TB_CODIGO in %s', [dmCliente.ConfidencialidadListaIN] ) ;
                         end
                         else
                         begin
                              PL_NIVEL0.Filtro := VACIO;
                         end;
                     end;

               end;
          end
     end;
end;

procedure TWizPlazas.CargaParametros;
 procedure AgregaParametroTexto( const sField, sValor: string );
 begin
      if Length(sValor) > 0 then
         ParameterList.AddString( sField, sValor );
 end;

 procedure AgregaParametroInteger( const sField, sValor: string );
 begin
       if StrLleno( sValor ) then
         ParameterList.AddInteger( sField, StrToIntDef( sValor, 0 ) );
 end;

 procedure AgregaParametroListaFija( const sField: string; const iValor: integer );
 begin
      if ( iValor > 0 ) then
         ParameterList.AddInteger( sField, iValor - 1);
 end;

begin
     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger('ReportePlazas', Formato.Valor );
          AddString('Observaciones', MemoObserva.Text);
          AddDate('FechaCAmbio', FechaCambio.Valor );

          //Pesta�a Parametros
          AgregaParametroTexto( 'PL_CODIGO', PL_CODIGO.Text );
          AgregaParametroTexto( 'PL_NOMBRE', PL_NOMBRE.Text );
          AgregaParametroInteger( 'PL_REPORTA', PL_REPORTA.Llave );

          AgregaParametroListaFija( 'PL_TIPO', PL_TIPO.ItemIndex );
          AgregaParametroListaFija( 'PL_TIREP', PL_TIREP.ItemIndex );

          if cbInicio.Checked then
             AddDate( 'PL_FEC_INI', PL_FEC_INI.Valor );

          if cbVence.Checked then
          begin
               if cbSinVence.Checked then
                  AddDate( 'PL_FEC_FIN', NullDateTime )
               else AddDate( 'PL_FEC_FIN', PL_FEC_FIN.Valor );
          end;

          //Pesta�a Contratacion
          AgregaParametroTexto( 'PU_CODIGO', PU_CODIGO.Llave );
          AgregaParametroTexto( 'PL_CLASIFI', PL_CLASIFI.Llave );
          AgregaParametroTexto( 'PL_TURNO', PL_TURNO.Llave );
          AgregaParametroTexto( 'PL_PATRON', PL_PATRON.Llave );
          AgregaParametroTexto( 'PL_NIVEL0', PL_NIVEL0.Llave );
          AgregaParametroTexto( 'PL_CONTRAT', PL_CONTRAT.Llave );
          AgregaParametroTexto( 'PL_AREA', PL_AREA.Llave );

          if PL_CHECA.ItemIndex > ord( erbSinCambio ) then
             AddString( 'PL_CHECA', ZBoolToStr( PL_CHECA.ItemIndex = Ord(erbSi) ) );


          //Pesta�a Area
          if PL_NIVEL1.Visible then AgregaParametroTexto( 'PL_NIVEL1', PL_NIVEL1.Llave );
          if PL_NIVEL2.Visible then AgregaParametroTexto( 'PL_NIVEL2', PL_NIVEL2.Llave );
          if PL_NIVEL3.Visible then AgregaParametroTexto( 'PL_NIVEL3', PL_NIVEL3.Llave );
          if PL_NIVEL4.Visible then AgregaParametroTexto( 'PL_NIVEL4', PL_NIVEL4.Llave );
          if PL_NIVEL5.Visible then AgregaParametroTexto( 'PL_NIVEL5', PL_NIVEL5.Llave );
          if PL_NIVEL6.Visible then AgregaParametroTexto( 'PL_NIVEL6', PL_NIVEL6.Llave );
          if PL_NIVEL7.Visible then AgregaParametroTexto( 'PL_NIVEL7', PL_NIVEL7.Llave );
          if PL_NIVEL8.Visible then AgregaParametroTexto( 'PL_NIVEL8', PL_NIVEL8.Llave );
          if PL_NIVEL9.Visible then AgregaParametroTexto( 'PL_NIVEL9', PL_NIVEL9.Llave );


          //Pesta�a Salario
          if PL_AUTOSAL.ItemIndex > ord( erbSinCambio ) then
             AddString( 'PL_AUTOSAL', ZBoolToStr( PL_AUTOSAL.ItemIndex = Ord(erbSi) ) );
          if ( cbCambiaSalario.Checked ) then
             AddFloat( 'PL_SALARIO',  PL_SALARIO.Valor  );
          if ( cbCambiaPromedio.Checked ) then
             AddFloat( 'PL_PER_VAR',  PL_PER_VAR.Valor  );
          AgregaParametroTexto( 'PL_TABLASS', PL_TABLASS.Llave );

          if PL_ZONA_GE.ItemIndex > ord(rzgSinCambio) then
             AddString( 'PL_ZONA_GE', PL_ZONA_GE.Text );

          AgregaParametroTexto( 'PL_INGLES', PL_INGLES.Text );

          AgregaParametroListaFija( 'PL_NUMERO', PL_NUMERO.ValorEntero  );
          AgregaParametroTexto( 'PL_TEXTO', PL_TEXTO.Text );
          AgregaParametroTexto( 'PL_SUB_CTA', PL_SUB_CTA.Text );
     end;
end;

procedure TWizPlazas.cbVenceClick(Sender: TObject);
begin
     inherited;
     lblVence.Enabled := cbVence.Checked;
     PL_FEC_FIN.Enabled := cbVence.Checked;
     cbSinVence.Enabled := cbVence.Checked;
     
end;

function TWizPlazas.EjecutarWizard: Boolean;
begin
     with dmInterfase do
     begin
          Procesa(ParameterList);
          Result := TRUE;
     end;
end;

procedure TWizPlazas.PL_AUTOSALClick(Sender: TObject);
begin
     inherited;
     {PL_SALARIO.Enabled := NOT PL_AUTOSAL.Checked;
     LblSalario.Enabled := PL_SALARIO.Enabled;}
end;

procedure TWizPlazas.cbCambiaSalarioClick(Sender: TObject);
begin
     inherited;
     PL_SALARIO.Enabled := cbCambiaSalario.Checked;
end;

procedure TWizPlazas.cbCambiaPromedioClick(Sender: TObject);
begin
     inherited;
     PL_PER_VAR.Enabled := cbCambiaPromedio.Checked;
end;

procedure TWizPlazas.cbNumeroClick(Sender: TObject);
begin
     inherited;
     PL_NUMERO.Enabled := cbNumero.Checked;
end;

procedure TWizPlazas.cbInicioClick(Sender: TObject);
begin
     inherited;
     PL_FEC_INI.Enabled := cbInicio.Checked;
end;

end.
