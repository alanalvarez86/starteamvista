program ImportaVacaciones;

uses
  Forms,
  ComObj,
  ActiveX,
  ZetaClientTools,
  DInterfase in '..\DInterfase.pas' {dmInterfase: TDataModule},
  FTressShell in '..\FTressShell.pas' {TressShell},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseImportaShell in '..\..\..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in '..\..\..\Tools\ZetaSplash.pas' {SplashScreen};

{$R *.RES}
{$R WindowsXP.res}

{procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     Application.HelpFile := 'ImportaVacaciones.chm';
     Application.Title := 'Importación de vacaciones - Sistema TRESS';
     //MuestraSplash;
     Application.CreateForm(TTressShell, TressShell);
  with TressShell do
        begin
             if Login( False ) then
             begin
                  //CierraSplash;
                  Show;
                  Update;
                  BeforeRun;
                  Application.Run;
             end
             else
             begin
                  //CierraSplash;
                  Free;
             end;
        end;
end.
