inherited TressShell: TTressShell
  Left = 402
  Top = 226
  Width = 432
  Height = 428
  Caption = 'Importaci'#243'n de vacaciones'
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 351
    Width = 416
  end
  inherited PageControl: TPageControl
    Top = 187
    Width = 416
    Height = 164
    inherited TabBitacora: TTabSheet
      inherited MemBitacora: TMemo
        Width = 408
        Height = 95
      end
      inherited PanelBottom: TPanel
        Top = 95
        Width = 408
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 153
    Width = 416
    inherited BtnSalir: TBitBtn
      Left = 282
    end
  end
  inherited PanelEncabezado: TPanel
    Width = 416
    Height = 153
    inherited LblArchivo: TLabel
      Left = 20
      Caption = '&Archivo Origen:'
    end
    inherited ArchivoSeek: TSpeedButton
      Left = 393
      Top = 6
    end
    object LblSaldogozo: TLabel [3]
      Left = 39
      Top = 77
      Width = 54
      Height = 13
      Caption = '&D'#237'as Pago:'
    end
    object LblPrimavacacional: TLabel [4]
      Left = 9
      Top = 102
      Width = 84
      Height = 13
      Caption = '&Prima vacacional:'
    end
    object LblFechadefault: TLabel [5]
      Left = 25
      Top = 126
      Width = 68
      Height = 13
      Caption = '&Fecha default:'
    end
    object LblTipoImportacion: TLabel [6]
      Left = 15
      Top = 43
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = '&Tipo de Archivo:'
    end
    inherited ArchOrigen: TEdit
      Left = 100
    end
    object RGTipoArchivo: TRadioGroup
      Left = 100
      Top = 28
      Width = 189
      Height = 41
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Historial '
        'Saldo ')
      TabOrder = 1
      OnClick = RGTipoArchivoClick
    end
    object cbprimavacacional: TZetaKeyCombo
      Left = 100
      Top = 98
      Width = 189
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      Items.Strings = (
        'A partir de los d'#237'as de pago'
        'Dejar en cero'
        'Saldar en Aniversarios')
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object cbSaldogozo: TZetaKeyCombo
      Left = 100
      Top = 73
      Width = 189
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        '')
      ListaFija = lfPagoPlanVacacion
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object FechaReferencia: TZetaFecha
      Left = 100
      Top = 122
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 4
      Text = '30/Aug/07'
      Valor = 39324.000000000000000000
    end
  end
  inherited LogoffTimer: TTimer
    Left = 360
    Top = 128
  end
  inherited ActionList: TActionList
    Left = 362
    Top = 100
  end
  inherited ShellMenu: TMainMenu
    Left = 392
    Top = 128
  end
  inherited OpenDialog: TOpenDialog
    Left = 382
    Top = 42
  end
  inherited ArbolImages: TImageList
    Left = 388
    Top = 72
  end
  inherited Email: TNMSMTP
    Left = 392
    Top = 96
  end
end
