program ImportaVacaciones;

uses
  Forms,
  ComObj,
  ActiveX,
  ZetaClientTools,
  FTressShell in '..\FTressShell.pas' {TressShell},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseImportaShell in '..\..\..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in '..\..\..\Tools\ZetaSplash.pas' {SplashScreen},
  DInterfase in '..\DInterfase.pas' {dmInterfase: TDataModule};

{$R *.RES}

procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     MuestraSplash;
     Application.Title := 'Importación de vacaciones - Sistema TRESS';
     Application.CreateForm(TTressShell, TressShell);
  with TressShell do
        begin
             if Login( False ) then
             begin
                  CierraSplash;
                  Show;
                  Update;
                  BeforeRun;
                  Application.Run;
             end
             else
             begin
                  CierraSplash;
                  Free;
             end;
        end;
end.
