unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell, Psock, NMsmtp, ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, ZBaseConsulta;

type
  TTressShell = class(TBaseImportaShell)
    RGTipoArchivo: TRadioGroup;
    LblSaldogozo: TLabel;
    cbprimavacacional: TZetaKeyCombo;
    cbSaldogozo: TZetaKeyCombo;
    FechaReferencia: TZetaFecha;
    LblPrimavacacional: TLabel;
    LblFechadefault: TLabel;
    LblTipoImportacion: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RGTipoArchivoClick(Sender: TObject);
  private
    { Private declarations }
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
    procedure HabilitaControlesSaldo;
    procedure SetNombreLogs;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;

  protected
    { Protected declarations }
    procedure ActivaControles( const lProcesando: Boolean ); override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
  end;

const
     K_HISTORIAL = 0;
     K_SALDO = 1;
     K_DIAS_PAGO = 0;
     K_DEJAR_CERO = 1;
     K_SALDA_ANIV = 2;

var
  TressShell: TTressShell;

implementation

uses DCliente, DGlobal, DSistema, DTablas,
     DRecursos, DConsultas, DProcesos,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     FCalendario,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     {$endif}
     DInterfase,
     DCatalogos;

{$R *.dfm}

{ TTressShell }

procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmSistema := TdmSistema.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmInterfase := TdmInterfase.Create( Self );
     inherited;
     HelpContext := 20121;//Pasos para Importación 
     cbSaldoGozo.ItemIndex := Ord( pvGozo );
     cbPrimaVacacional.ItemIndex := K_DIAS_PAGO;
     RGTipoArchivo.ItemIndex:= K_HISTORIAL;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmInterfase.Free;
     dmCatalogos.Free;
     dmConsultas.Free;
     dmProcesos.Free;
     dmRecursos.Free;
     dmTablas.Free;
     dmSistema.Free;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
end;

procedure TTressShell.RGTipoArchivoClick(Sender: TObject);
begin
     HabilitaControlesSaldo;
end;

procedure TTressShell.DoOpenAll;
begin
     try
         inherited DoOpenAll;
         Global.Conectar;
         {***US 11757: Proteccion HTTP SkinController = True
         Evita que el backgound de algunos componentes de la aplicacion
         sea de color blanco
         ***}
         DevEx_SkinController.NativeStyle := TRUE;
         with dmCliente do
         begin
              InitActivosSistema;
              InitActivosAsistencia;
              InitActivosEmpleado;
              InitActivosPeriodo;
         end;
         InitBitacora;
         CargaSistemaActivos;
      except
         on Error : Exception do
         begin
              ReportaErrorDialogo( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message );
              DoCloseAll;
         end;
      end;
      ActivaControles( FALSE );
      HabilitaControlesSaldo;
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmSistema );
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
          ZetaClientTools.CierraDatasets( dmInterfase );
          {$ifdef DOS_CAPAS}
          ZetaSQLBroker.FreeAll;
          {$endif}
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.DoProcesar;
begin
     with dmInterfase do
     begin
          TipoImportacion  := eTipoImportacion( RGTipoArchivo.ItemIndex );
          IgualaGozo:= ( cbSaldoGozo.ItemIndex = Ord( pvGozo ) );
          SaldoPrimaEnCero:= ( cbPrimaVacacional.ItemIndex = K_DEJAR_CERO );
          SaldarAniversario := ( cbPrimaVacacional.ItemIndex = K_SALDA_ANIV );
          FechaReferencia:= Self.FechaReferencia.Valor;
          Archivo := ArchOrigen.Text;
          Procesa;
     end;
end;

procedure TTressShell.HabilitaControlesSaldo;
var
   lEsSaldo: Boolean;
begin
     lEsSaldo:= ( RGTipoArchivo.ItemIndex = K_SALDO );
     LblPrimaVacacional.Enabled:= lEsSaldo;
     cbPrimaVacacional.Enabled:= lEsSaldo;
     LblFechaDefault.Enabled:= lEsSaldo;
     FechaReferencia.Enabled:= lEsSaldo;
end;

procedure TTressShell.ActivaControles(const lProcesando: Boolean);
begin
     inherited;
     with dmCliente do
     begin
          LblTipoImportacion.Enabled := EmpresaAbierta and ( not lProcesando );
          RGTipoArchivo.Enabled := EmpresaAbierta and ( not lProcesando );
          LblSaldoGozo.Enabled:= EmpresaAbierta and ( not lProcesando );
          cbSaldoGozo.Enabled:= EmpresaAbierta and ( not lProcesando );
          LblPrimaVacacional.Enabled := EmpresaAbierta and ( not lProcesando );
          cbPrimaVacacional.Enabled := EmpresaAbierta and ( not lProcesando );
          LblFechaDefault.Enabled := EmpresaAbierta and ( not lProcesando );
          FechaReferencia.Enabled := EmpresaAbierta and ( not lProcesando );
          if lProcesando and ( BtnDetener.CanFocus ) then
             BtnDetener.SetFocus
          else if EmpresaAbierta and ( ArchOrigen.CanFocus ) then
             ArchOrigen.SetFocus
          else if BtnSalir.CanFocus then
             BtnSalir.SetFocus;
     end;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     FechaReferencia.Valor := dmCliente.FechaDefault;
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     FCalendario.SetDefaultDlgFecha( FechaReferencia.Valor ); { Refresca el Default del Lookup de ZetaFecha }
     SetNombreLogs;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre: String;
begin
     with dmCliente do
     begin
          sNombre :='IMPORTACION.LOG';
          ArchBitacora.Text := ZetaMessages.SetFileNameDefaultPath( 'BIT-' + sNombre );
          ArchErrores.Text := ZetaMessages.SetFileNameDefaultPath( 'ERR-' + sNombre );
     end;
end;

procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
         dmInterfase.HandleProcessEnd( WParam, LParam );
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }

function TTressShell.FormaActiva: TBaseConsulta;
begin
      Result:= Nil;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo,iPeriodoBorrado: Integer);
begin
     //
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
      //
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
    //
end;

procedure TTressShell.ChangeTimerInfo;
begin
     //
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     //
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     //
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     //
end;

procedure TTressShell.RefrescaEmpleado;
begin
     //
end;

procedure TTressShell.RefrescaIMSS;
begin
      //
end;



procedure TTressShell.ReinitPeriodo;
begin
     //
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
      //
end;

procedure TTressShell.SetBloqueo;
begin
      //
end;


procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     //
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
    //No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     //No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     //No Implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     //No Implementar
end;

end.
