unit DSistema;

interface

uses
  System.SysUtils, System.Classes, Sistema_TLB, DBasicoCliente, Data.DB, ZetaCommonClasses, ZetaDialogo, Dialogs,
  Datasnap.DBClient, ZetaAsciiFile, ZetaClientDataSet, Controls, Forms, ZetaCommonLists;

type
  TdmSistema = class(TDataModule)
    cdsSistBaseDatos: TZetaLookupDataSet;
    cdsSistBaseDatosLookUp: TZetaLookupDataSet;
    cdsTareasEmail: TZetaClientDataSet;
    cdsEnvioTareasEmail: TZetaClientDataSet;
  private
    { Private declarations }
  protected
    FServidor: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
  public
    { Public declarations }
    function GrabaTareas(Empresa : OleVariant; Parametros:TZetaParams; var sError : String): Integer;
    function GrabaSuscripciones(Empresa : OleVariant; iFolio: Integer; sFrecuencia, sUsuarios : WideString; iReporte : Integer): String;
    procedure EstablecerFechas(var sError : WideString);
    procedure RefrescarTareas(Empresa: OleVariant);
  end;

var
  dmSistema: TdmSistema;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

function TdmSistema.GrabaTareas(Empresa : OleVariant; Parametros:TZetaParams; var sError : String): Integer;
var
   oDatos, oParametros, sErrores: OleVariant;
begin
     Result := 0;
     oParametros := Parametros.VarValues;
     try
        ServerSistema.GrabaTareaCalendario(Empresa, oParametros, oDatos);
        sError := sErrores;
        Result := oDatos;
     Except
           on Error: Exception do
           begin
                sError := Error.Message;
           end;
     end;
end;

procedure TdmSistema.EstablecerFechas(var sError : WideString);
begin
     ServerSistema.SetEnviosProgramados( sError );
end;


procedure TdmSistema.RefrescarTareas(Empresa: OleVariant);
begin
     cdsEnvioTareasEmail.Data := ServerSistema.GetTareaCalendario( Empresa, 1 );
end;

function TdmSistema.GrabaSuscripciones(Empresa : OleVariant; iFolio: Integer; sFrecuencia, sUsuarios : WideString; iReporte : Integer): String;
var
   oDatos: OleVariant;
   sError: WideString;
begin
     sError := VACIO;
     try
        ServerSistema.ImportarSuscripcionesTressEmailPorTarea(Empresa, iFolio, sFrecuencia, sUsuarios, iReporte, sError);
        Result := sError;
     Except
           on Error: Exception do
           begin
                Result := Error.Message;
           end;
     end;
end;

function TdmSistema.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( BasicoCliente.CreaServidor( CLASS_dmServerSistema, FServidor ) );
end;

end.
