unit DCliente;

interface

uses
  System.SysUtils, System.Classes, DBasicoCliente, Data.DB, Datasnap.DBClient,
     Login_TLB,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaCommonClasses, Bde.DBTables, dSistema;

type
  TdmCliente = class(TBasicoCliente)
  private
    { Private declarations }
  public
    { Public declarations }
    function BuscaCompany( const sCodigo: String ): Boolean;
    function Empresa: OleVariant;
    function ImportarTareas(sEmpresa : String; out Parametros:TZetaParams; var sResultado : String): Integer;
    function ImportarSuscripciones(sFrecuencia, sUsuarios: String; iFolio, iReporte : Integer): String;
    procedure EstablecerFechas(var sError : WideString);
  end;

var
  dmCliente: TdmCliente;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


function TdmCliente.BuscaCompany( const sCodigo: String ): Boolean;
begin
     with cdsCompany do
     begin
          Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
          Result := Locate( 'CM_CODIGO', sCodigo, [] );
     end;
end;

function TdmCliente.Empresa: OleVariant;
begin
     Result := BuildEmpresa( cdsCompany );
end;

function TdmCliente.ImportarTareas(sEmpresa : String; out Parametros:TZetaParams; var sResultado : String): Integer;
var i : integer;
    lExito : Boolean;
    sNombre : String;
begin
     if BuscaCompany(sEmpresa) then
     begin
          dmSistema.RefrescarTareas(Empresa);
          sNombre := Parametros.ParamByName('Nombre').AsString;
          lExito := False;
          i := 1;
          while Not lExito do
          begin
               if dmSistema.cdsEnvioTareasEmail.Locate( 'CA_NOMBRE', sNombre, [] ) then
               begin
                    i := i + 1;
                    sNombre := sNombre + ' ' + IntToStr(i);
               end
               else
                   lExito := True;
          end;

          Parametros.ParamByName('Nombre').AsString := sNombre;
          Result:= dmSistema.GrabaTareas(Empresa, Parametros, sResultado);
     end
     else
     begin
          sResultado := 'Error Empresa';
          Result := 0;
     end;
end;

function TdmCliente.ImportarSuscripciones(sFrecuencia, sUsuarios : String; iFolio, iReporte: Integer): String;
begin
     Result:= dmSistema.GrabaSuscripciones(Empresa, iFolio, sFrecuencia, sUsuarios, iReporte);
end;

procedure TdmCliente.EstablecerFechas(var sError : WideString);
begin
      dmSistema.EstablecerFechas(sError);
end;

end.
