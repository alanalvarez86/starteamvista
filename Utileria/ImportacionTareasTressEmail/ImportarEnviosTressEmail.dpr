﻿program ImportarEnviosTressEmail;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Vcl.Forms,
  Sistema_TLB in '..\..\MTS\Sistema_TLB.pas',
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  Login_TLB in '..\..\MTS\Login_TLB.pas',
  DSistema in 'DSistema.pas' {dmSistema: TDataModule},
  FDetalleRegistro in 'FDetalleRegistro.pas' {DetalleRegistro},
  FImportarTareasTressEmailShell in 'FImportarTareasTressEmailShell.pas' {FImportarTareasTressEmail},
  DImportacion in 'DImportacion.pas' {dmImportacion: TDataModule},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule};

{$R *.res}

{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recurso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\..\Traducciones\Spanish.RES}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.HelpFile := '';
  Application.Title := 'ImportarEnviosTRESSEmail';
  Application.CreateForm(TdmSistema, dmSistema);
  Application.CreateForm(TdmImportacion, dmImportacion);
  Application.CreateForm(TFImportarTareasTressEmail, FImportarTareasTressEmail);
  Application.CreateForm(TdmCliente, dmCliente);
  Application.Run;
end.
