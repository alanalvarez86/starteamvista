object dmSistema: TdmSistema
  OldCreateOrder = False
  Height = 213
  Width = 339
  object cdsSistBaseDatos: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 56
    Top = 32
  end
  object cdsSistBaseDatosLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Base de Datos'
    LookupDescriptionField = 'DB_DESCRIP'
    LookupKeyField = 'DB_CODIGO'
    Left = 56
    Top = 104
  end
  object cdsTareasEmail: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'TASKNAME'
    Params = <>
    Left = 208
    Top = 32
  end
  object cdsEnvioTareasEmail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 216
    Top = 128
  end
end
