unit FDetalleRegistro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, Vcl.ImgList, cxButtons,
  dxGDIPlusClasses, TressMorado2013, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxNavigator, cxDBNavigator, dSistema, Data.DB, cxDBEdit;

type
  TDetalleRegistro = class(TForm)
    ImageButtons: TcxImageList;
    btOk: TcxButton;
    editTaskName: TcxDBTextEdit;
    Label1: TLabel;
    editComment: TcxDBTextEdit;
    Label2: TLabel;
    editStartDate: TcxDBTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    editScheduleType: TcxDBTextEdit;
    Label5: TLabel;
    editDays: TcxDBTextEdit;
    editMonths: TcxDBTextEdit;
    Label6: TLabel;
    editLastRunTime: TcxDBTextEdit;
    Label7: TLabel;
    Label9: TLabel;
    editNextRunTime: TcxDBTextEdit;
    GroupBox1: TGroupBox;
    editEmpresa: TcxDBTextEdit;
    Label8: TLabel;
    editFrec: TcxDBTextEdit;
    Label10: TLabel;
    editUsuarios: TcxDBTextEdit;
    Label11: TLabel;
    Label12: TLabel;
    editFecha: TcxDBTextEdit;
    Label13: TLabel;
    editReportes: TcxDBTextEdit;
    editReportEmp: TcxDBTextEdit;
    Label14: TLabel;
    Panel1: TPanel;
    DevEx_cxDBNavigatorEdicion: TcxDBNavigator;
    DataSource: TDataSource;
    editActivo: TcxDBTextEdit;
    Label15: TLabel;
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
   DetalleRegistro: TDetalleRegistro;

procedure MostrarDetalle;//(sTaskName, sComment, sSchedule, sStartDate, sDays, sMonths, sLastRun, sNextRun, sEmpresa, sFrec, sFecha, sUsuario, sReportes, sReportEmp:String): String;

implementation

{$R *.DFM}

procedure MostrarDetalle;//(sTaskName, sComment, sSchedule, sStartDate, sDays, sMonths, sLastRun, sNextRun, sEmpresa, sFrec, sFecha, sUsuario, sReportes, sReportEmp:String): String;
begin
     with TDetalleRegistro.Create( Application ) do
     begin
          try
             {editTaskName.Text := sTaskName;
             editComment.Text := sComment;
             editScheduleType.Text := sSchedule;
             editStartDate.Text := sStartDate;
             editDays.Text := sDays;
             editMonths.Text := sMonths;
             editLastRunTime.Text := sLastRun;
             editNextRunTime.Text := sNextRun;
             editEmpresa.Text := sEmpresa;
             editFrec.Text := sFrec;
             editFecha.Text := sFecha;
             editUsuarios.Text := sUsuario;
             editReportes.Text := sReportes;
             editReportEmp.Text := sReportEmp;

             Caption := 'Tarea de TressEmail: ' + sTaskName;}
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TDetalleRegistro.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     with dmSistema.cdsTareasEmail do
     begin
          Caption := 'Envio de TressEmail: ' + FieldByName('TASKNAME').AsString;
     end;
end;

procedure TDetalleRegistro.FormShow(Sender: TObject);
begin
     btOK.SetFocus;
     DataSource.DataSet :=  dmSistema.cdsTareasEmail;
end;

end.
