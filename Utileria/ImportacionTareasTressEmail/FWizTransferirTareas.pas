unit FWizTransferirTareas;

interface

uses
 ZetaCommonClasses, Forms, dxWizardControlForm, TressMorado2013, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
 dxSkinsCore, cxContainer, cxEdit, cxProgressBar, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel,
 dxCustomWizardControl, dxWizardControl, System.Classes, Vcl.Controls, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.ExtCtrls, ZetaKeyLookup, Data.DB, cxCheckListBox, Vcl.ImgList, ZetaClientDataSet, Dialogs,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxShellBrowserDialog, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxSkinscxPCPainter, ZetaKeyLookup_DevEx, Windows, Graphics, ZetaCXGrid, ZetaAsciiFile, Variants;


type
  TWizTransferirTareas = class(TdxWizardControlForm)
    dxWizardControl: TdxWizardControl;
    DataSource: TDataSource;
    pgCargaTareas: TdxWizardControlPage;
    btnSeleccionarTodos: TcxButton;
    btnDesmarcarTodas: TcxButton;
    ImageButtons: TcxImageList;
    pgConfirmaTareas: TdxWizardControlPage;
    gridTareasConfirmacionDBTableView: TcxGridDBTableView;
    gridTareasConfirmacionLevel: TcxGridLevel;
    gridTareasConfirmacion: TcxGrid;
    lblBitacora: TLabel;
    btnBitacora: TcxButton;
    cxOpenDialog: TcxShellBrowserDialog;
    DataSourceConfirmacion: TDataSource;
    ENTIDAD: TcxGridDBColumn;
    Frecuencia: TcxGridDBColumn;
    pgEjecucionTransferencia: TdxWizardControlPage;
    lblEstatusHeaderTransfer: TcxLabel;
    pbAvanceTransfer: TcxProgressBar;
    pnlResumen: TPanel;
    lblResumen: TcxLabel;
    btnVerBitacoraTrans: TcxButton;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    lblErrores: TcxLabel;
    lblDiferencias: TcxLabel;
    lblProcesados: TcxLabel;
    gridTareasCalendario: TZetaCXGrid;
    gridTareasCalendarioDBTableView: TcxGridDBTableView;
    NomCol: TcxGridDBColumn;
    FrecCol: TcxGridDBColumn;
    ElegirCol: TcxGridDBColumn;
    gridTareasCalendarioLevel: TcxGridLevel;
    txtBitacora: TcxTextEdit;
    gridTareasCalendarioDBTableViewColumn1: TcxGridDBColumn;
    gridTareasConfirmacionDBTableViewColumn1: TcxGridDBColumn;
    FechaCol: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodasClick(Sender: TObject);
    procedure btnBitacoraClick(Sender: TObject);
    procedure dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure gridTareasCalendarioDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure gridTareasConfirmacionDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure btnVerBitacoraTransClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure SeleccionarTareas(lElegir: Boolean);
    procedure CargarTareasEnGrid(gridTableView : TcxGridDBTableView; DS : TDataSource; DataModule : TDataSet; lElegidas : Boolean);
    function TransferirTablasCatalogos: Boolean;
    procedure HabilitaBotones;
    function Processing: Boolean;
    function Warning( const sError: String; oControl: TWinControl ): Boolean;
    function HayTablaSeleccionada (DataSet: TDataSet; iColumna: Integer): Boolean;

    procedure ApplyMinWidth(gridTableView : TcxGridDBTableView);

  public
    { Public declarations }
  end;

var
  WizTransferirTareas: TWizTransferirTareas;


implementation

uses SysUtils, DBClient, ZetaCommonLists, ShellAPI, DCliente, ZetaDialogo, ZetaCommonTools, DImportacion, DSistema;



{$R *.dfm}

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[0..79] of Char;
begin
     Result := ShellExecute( Application.MainForm.Handle, nil, StrPCopy( zFileName, FileName ),StrPCopy( zParams, Params ),
               StrPCopy( zDir, DefaultDir ),ShowCmd );
end;

function TWizTransferirTareas.Processing: Boolean;
begin
     Result := Not pnlResumen.Visible;
end;

procedure TWizTransferirTareas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if dxWizardControl.Buttons.Finish.Enabled then
        Action := caFree
     else
         Action := caNone;
end;

procedure TWizTransferirTareas.FormCreate(Sender: TObject);
begin
     HelpContext := 3;
end;

procedure TWizTransferirTareas.FormShow(Sender: TObject);
begin
     with gridTareasCalendarioDBTableView do
     begin
          OptionsCustomize.ColumnGrouping := False;
          OptionsView.GroupByBox := False;
          FilterBox.Visible := fvNever;
          FilterBox.CustomizeDialog := False;
     end;
     CargarTareasEnGrid(gridTareasCalendarioDBTableView, DataSource, dmSistema.cdsTareasEmail, False);
end;

procedure TWizTransferirTareas.CargarTareasEnGrid(gridTableView : TcxGridDBTableView; DS : TDataSource; DataModule : TDataSet; lElegidas : Boolean);
var cdsTablasCatalogos : TZetaClientDataSet;
begin
     cdsTablasCatalogos := TZetaClientDataSet.Create(Self);
     with cdsTablasCatalogos do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          AddStringField('TASKNAME', 255);
          AddStringField('COMMENT', 255);
          AddStringField ('SCHEDULE_TYPE', 255);
          AddStringField ('START_TIME', 25);
          AddStringField ('START_DATE', 25);
          AddStringField ('DAYS', 255);
          AddStringField ('MONTHS', 255);
          AddStringField ('TASK_TO_RUN', 1024);
          AddStringField ('EMPRESA', 10);
          AddStringField ('FREC', 2);
          AddStringField ('FECHA', 10);
          AddStringField ('USUARIOS', 255);
          AddStringField ('REPORTES', 600);
          AddIntegerField ('REPORTE_EMP');
          AddStringField ('NEXTRUN', 25);
          AddStringField ('LASTRUN', 25);
          AddBooleanField('ELEGIR');
          AddStringField ('ACTIVO', 2);
          AddStringField ('WHOLE_DATE', 25);
          CreateTempDataset;
          Open;
     end;

     with DataModule do
     begin
         First;
         while not Eof do
         begin
              if FieldByName('Elegir').AsBoolean = lElegidas then
              begin
                   cdsTablasCatalogos.Insert;
                   cdsTablasCatalogos.FieldByName('TASKNAME').AsString := FieldByName('TASKNAME').AsString;
                   cdsTablasCatalogos.FieldByName('COMMENT').AsString := FieldByName('COMMENT').AsString;
                   cdsTablasCatalogos.FieldByName('SCHEDULE_TYPE').AsString := FieldByName('SCHEDULE_TYPE').AsString;
                   cdsTablasCatalogos.FieldByName('START_TIME').AsString := FieldByName('START_TIME').AsString;
                   cdsTablasCatalogos.FieldByName('START_DATE').AsString := FieldByName('START_DATE').AsString;
                   cdsTablasCatalogos.FieldByName('DAYS').AsString := FieldByName('DAYS').AsString;
                   cdsTablasCatalogos.FieldByName('MONTHS').AsString := FieldByName('MONTHS').AsString;
                   cdsTablasCatalogos.FieldByName('TASK_TO_RUN').AsString := FieldByName('TASK_TO_RUN').AsString;
                   cdsTablasCatalogos.FieldByName('EMPRESA').AsString := FieldByName('EMPRESA').AsString;
                   cdsTablasCatalogos.FieldByName('FREC').AsString := FieldByName('FREC').AsString;
                   cdsTablasCatalogos.FieldByName('FECHA').AsString := FieldByName('FECHA').AsString;
                   cdsTablasCatalogos.FieldByName('USUARIOS').AsString := FieldByName('USUARIOS').AsString;
                   cdsTablasCatalogos.FieldByName('REPORTES').AsString := FieldByName('REPORTES').AsString;
                   cdsTablasCatalogos.FieldByName('REPORTE_EMP').AsString := FieldByName('REPORTE_EMP').AsString;
                   cdsTablasCatalogos.FieldByName('NEXTRUN').AsString := FieldByName('NEXTRUN').AsString;
                   cdsTablasCatalogos.FieldByName('LASTRUN').AsString := FieldByName('LASTRUN').AsString;
                   cdsTablasCatalogos.FieldByName('ELEGIR').AsBoolean := False;
                   cdsTablasCatalogos.FieldByName('ACTIVO').AsString := FieldByName('ACTIVO').AsString;
                   cdsTablasCatalogos.FieldByName('WHOLE_DATE').AsString := FieldByName('WHOLE_DATE').AsString;
                   cdsTablasCatalogos.Post;
              end;
              Next;
         end;
     end;

     DS.DataSet := cdsTablasCatalogos;
     cdsTablasCatalogos.IndexFieldNames := 'TASKNAME';
     gridTableView.DataController.DataSource := DS;
     ApplyMinWidth(gridTableView);
     gridTableView.ApplyBestFit();
     gridTableView.DataController.FocusedRowIndex := 0;
     DS.DataSet.First;
end;

procedure TWizTransferirTareas.dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
var
  mensaje : string;
  oCursor: TCursor;
begin
     case AKind of
          wcbkNext:
          begin
               if (dxWizardControl.ActivePage = pgCargaTareas) then
               begin
                    if not HayTablaSeleccionada (DataSource.DataSet, 16) then
                       AHandled := Warning('Debe seleccionar al menos un env�o.', gridTareasCalendario)
                    else
                    begin
                         dxWizardControl.Buttons.Next.Caption := 'Aplicar';
                         CargarTareasEnGrid(gridTareasConfirmacionDBTableView, DataSourceConfirmacion, DataSource.DataSet, True );
                    end;
               end
               else if (dxWizardControl.ActivePage = pgConfirmaTareas) then
               begin
                    pnlResumen.Visible := FALSE;
                    pbAvanceTransfer.Position := 0;
                    Application.ProcessMessages;
                    dxWizardControl.ActivePage := pgEjecucionTransferencia;
                    oCursor := Screen.Cursor;
                    try
                       dxWizardControl.Enabled := FALSE;
                       Screen.Cursor := crHourglass;
                       // Ejecutar Proceso de Transferencia de Tablas y Cat�logos
                       if TransferirTablasCatalogos then
                          pnlResumen.Visible := TRUE;
                    finally
                           dxWizardControl.Enabled := TRUE;
                           Screen.Cursor := oCursor;
                    end;
               end;
        end;
        wcbkCancel:
        begin
             if not Processing then
                  ModalResult := mrCancel;
        end;
        wcbkFinish:  ModalResult := mrOk;
        wcbkBack :
                 if (dxWizardControl.ActivePage = pgConfirmaTareas) then
                 begin
                      dxWizardControl.Buttons.Next.Caption := 'Siguiente';
                 end;
     end;

end;

procedure TWizTransferirTareas.btnSeleccionarTodosClick(Sender: TObject);
begin
     SeleccionarTareas(TRUE);
end;

procedure TWizTransferirTareas.btnVerBitacoraTransClick(Sender: TObject);
begin
     ShellExecute(0, nil, 'notepad', PChar(txtBitacora.Text), nil, SW_SHOWNORMAL);
end;

procedure TWizTransferirTareas.btnDesmarcarTodasClick(Sender: TObject);
begin
     SeleccionarTareas(FALSE);
end;

procedure TWizTransferirTareas.btnBitacoraClick(Sender: TObject);
var sFileName: String;
begin
     sFileName := StrDef (ExtractFileName(txtBitacora.Text), 'ImportarEnviosBitacora.txt');
     cxOpenDialog.Path := ExtractFilePath( Application.ExeName );

     if cxOpenDialog.Execute then
        txtBitacora.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
end;

procedure TWizTransferirTareas.SeleccionarTareas(lElegir: Boolean);
var
   sTablaActual, sTaskName, sFrec, sFecha, sHora: String;
   i : integer;
begin
     gridTareasCalendarioDBTableView.OptionsView.ScrollBars := ssNone;
     with gridTareasCalendarioDBTableView.ViewData do
     begin
          for i:= 0 to RecordCount - 1 do
          begin
               sTaskName := Records[i].Values[NomCol.Index];
               sFrec :=  Records[i].Values[FrecCol.Index];
               sFecha := Records[i].Values[FechaCol.Index];

               with DataSource.DataSet do
               begin
                    Locate('TASKNAME;SCHEDULE_TYPE;WHOLE_DATE',VarArrayOf([sTaskName,sFrec,sFecha]),[]);
                    Edit;
                    FieldByName ('ELEGIR').AsBoolean := lElegir;
                    Post;
               end;
          end;
     end;
     gridTareasCalendarioDBTableView.OptionsView.ScrollBars := ssBoth;
     gridTareasCalendarioDBTableView.DataController.FocusedRowIndex := 0;
     DataSource.DataSet.First;
end;

function TWizTransferirTareas.HayTablaSeleccionada (DataSet: TDataSet; iColumna: Integer): Boolean;
begin
     Result := FALSE;
     with DataSet do
     begin
          First;
          while not Eof do
          begin
               if Fields[iColumna].AsBoolean then
               begin
                    Result := TRUE;
                    Break;
               end;
               Next;
          end;
     end;
end;

procedure TWizTransferirTareas.gridTareasCalendarioDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
var
   AIndex: Integer;
begin
     inherited;
     AIndex := AValueList.FindItemByKind(fviCustom);
     if AIndex <> -1 then
        AValueList.Delete(AIndex);
end;

procedure TWizTransferirTareas.gridTareasConfirmacionDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
var
   AIndex: Integer;
begin
     inherited;
     AIndex := AValueList.FindItemByKind(fviCustom);
     if AIndex <> -1 then
        AValueList.Delete(AIndex);
end;

function TWizTransferirTareas.TransferirTablasCatalogos: Boolean;
const K_NOMBRE = '%s [Reporte: %s]';
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings);
begin
     ListOfStrings.Clear;
     ListOfStrings.Delimiter     := Delimiter;
     ListOfStrings.DelimitedText := Str;
end;

var
   lOk, lExito : Boolean;
   FLog: TTransferLog;
   sError: WideString;
   sFileName, sResultado, sMeses, sNombre, sDiasSemana, sReporte, sSuscrip, sTemp: string;
   Parametros: TZetaParams;
   iCantidad, iRegistros, iDiasMes, iReporteEmp, i, iErrores, iFolio, iConsecutivo: Integer;
   dFechaInicio, dFechaSiguiente, dFechaUltima, dFechaEspecial : TDateTime;
   Lista : TStringList;
   FFormatoFecha: eFormatoImpFecha;
begin
     lOk := FALSE;
     sFileName := txtBitacora.Text;
     HabilitaBotones;
     FLog := TTransferLog.Create;
     try
        dmImportacion.Iniciar(sFileName);
        FLog.Init(sFileName);
        lblEstatusHeaderTransfer.Caption := 'Empezando importaci�n de env�os';
        with DataSourceConfirmacion.DataSet do
        begin
             iRegistros := RecordCount;
             while not Eof do
             begin
                  Lista := TStringList.Create;
                  try
                     Split(',', FieldByName('REPORTES').AsString, Lista);

                     if Lista.Count = 0 then
                        Lista.Text := '0';

                     for i := 0 to Lista.Count-1 do
                     begin
                          Parametros := TZetaParams.Create;
                          try
                             try
                                lExito := True;
                                sReporte := Trim(Lista[i]);
                                iDiasMes := 0;
                                sResultado := VACIO;

                                Parametros.AddInteger( 'Folio', 0);
                                Parametros.AddString( 'Reportes', sReporte );
                                if Lista.Count > 1 then
                                   sNombre := Format(K_NOMBRE, [  FieldByName('TASKNAME').AsString, sReporte ] )
                                else
                                    sNombre :=  FieldByName('TASKNAME').AsString;

                                Parametros.AddString( 'Nombre', sNombre );
                                Parametros.AddString( 'Empresa', FieldByName('EMPRESA').AsString );
                                Parametros.AddString( 'Descripcion', FieldByName('COMMENT').AsString );
                                Parametros.AddString( 'Activo', FieldByName('ACTIVO').AsString );
                                Parametros.AddInteger( 'Frecuencia', dmImportacion.GetFrecuencia ( FieldByName('SCHEDULE_TYPE').AsString ) );
                                Parametros.AddInteger( 'Recurrencia', dmImportacion.GetRecurrencia ( FieldByName('SCHEDULE_TYPE').AsString,
                                                        FieldByName('DAYS').AsString,  FieldByName('MONTHS').AsString ) );

                                try
                                   dFechaInicio := StrToDateTime(FieldByName('WHOLE_DATE').AsString );
                                except on E: Exception do
                                       dFechaInicio := dFechaSiguiente;
                                end;

                                try
                                   dFechaUltima := StrToDateTime(FieldByName('LASTRUN').AsString );
                                except on E: Exception do
                                       dFechaUltima := dFechaSiguiente;
                                end;

                                Parametros.AddString( 'Hora', HoraAsStr(dFechaInicio, 'hhmm'));
                                Parametros.AddDateTime ('FechaInicio', dFechaInicio);
                                Parametros.AddString( 'Meses', dmImportacion.GetMeses(FieldByName('SCHEDULE_TYPE').AsString, FieldByName('MONTHS').AsString) );
                                Parametros.AddString( 'DiasMes', dmImportacion.GetDiasMeses(FieldByName('SCHEDULE_TYPE').AsString, FieldByName('DAYS').AsString, sDiasSemana, iDiasMes ));
                                Parametros.AddString( 'SemanaMes', sDiasSemana );
                                Parametros.AddInteger( 'TipoMes', iDiasMes );

                                if FieldByName('REPORTE_EMP').AsString = VACIO then
                                   iReporteEmp := 0
                                else
                                    iReporteEmp := StrToInt (FieldByName('REPORTE_EMP').AsString);

                                Parametros.AddInteger( 'ReporteEmpleados', iReporteEmp);
                                Parametros.AddString( 'Evaluar', VACIO );
                                Parametros.AddDate( 'FechaEspecial', dFechaEspecial);
                                Parametros.AddString( 'Dias', dmImportacion.GetDias(FieldByName('SCHEDULE_TYPE').AsString, FieldByName('DAYS').AsString));
                                Parametros.AddInteger( 'Salida', 0 ); //Archivo Salida
                                Parametros.AddString( 'SalidaArchivo', VACIO ); //Archivo Salida
                                Parametros.AddInteger( 'Usuario', 0 ); // Usuario Modifico
                                Parametros.AddInteger( 'SuscribirUsuario', 0 ); //Suscripcion Usuario
                                Parametros.AddDate('FechaModifico', Date());

                                Parametros.AddDateTime('FechaSiguiente', dFechaSiguiente);
                                Parametros.AddDateTime('FechaUltima', dFechaUltima );

                                iFolio := dmCliente.ImportarTareas(FieldByName('EMPRESA').AsString, Parametros, sResultado);

                                iCantidad := iCantidad + 1;
                                if (iFolio = 0) then
                                begin
                                     iErrores := iErrores + 1;
                                     lExito := False;
                                end
                                else
                                begin
                                     if iReporteEmp = 0 then
                                     begin
                                          sSuscrip := dmCliente.ImportarSuscripciones(FieldByName('SCHEDULE_TYPE').AsString, FieldByName('USUARIOS').AsString, iFolio, StrToInt(sReporte));
                                          if StrLleno(sSuscrip) then
                                          begin
                                               iErrores := iErrores + 1;
                                               FLog.WriteTexto(dmImportacion.MessageHandler(sSuscrip, False, Parametros, FieldByName('SCHEDULE_TYPE').AsString ));
                                          end;
                                     end;
                                end;

                                FLog.WriteTexto(dmImportacion.MessageHandler(sResultado, lExito, Parametros, FieldByName('SCHEDULE_TYPE').AsString ));

                             except on E: Exception do
                               begin
                                    iCantidad := iCantidad + 1;
                                    iErrores := iErrores + 1;
                                    FLog.WriteTexto(dmImportacion.MessageHandler(E.Message, False, Parametros, FieldByName('SCHEDULE_TYPE').AsString ));
                               end
                             end;
                          finally
                                 FreeAndNil( Parametros );
                          end;
                     end
                  finally
                         FreeAndNil( Lista );
                  end;
                  Next;
                  pbAvanceTransfer.Position := ((iRegistros / 1) * StrToReal('.01')) + pbAvanceTransfer.Position;
             end;
        end;
        lOk := True;
     finally
            with FLog do
            begin
                 Close;
                 Free;
            end;
     end;

     try
        dmCliente.EstablecerFechas(sError);
     except on E: Exception do
            //
     end;


     //Resultados del proceso
     lblProcesados.Caption := IntToStr (iCantidad);
     lblDiferencias.Caption := IntToStr (iCantidad - iErrores);
     lblErrores.Caption := IntToStr (iErrores);
     dxWizardControl.Buttons.Finish.Enabled := True;


     pbAvanceTransfer.Position := 100;
     Result := lOk;
end;

procedure TWizTransferirTareas.HabilitaBotones;
begin
     with dxWizardControl.Buttons do
     begin
          Finish.Enabled := Not Processing;
          Cancel.Enabled := Not Processing;
          Next.Enabled := Not Processing;
          Back.Enabled := False;
     end;
end;

function TWizTransferirTareas.Warning( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZWarning( Caption, sError, 0, mbOK );
     oControl.SetFocus;
     Result := TRUE;
end;

procedure TWizTransferirTareas.ApplyMinWidth(gridTableView : TcxGridDBTableView);
var
   i: Integer;
const
     widthColumnOptions = 35;
begin
     with gridTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;


end.
