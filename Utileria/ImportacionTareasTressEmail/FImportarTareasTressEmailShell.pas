unit FImportarTareasTressEmailShell;

interface

uses
  CheckLst, DB, ZetaRegistryServer, DZetaServerProvider, ZetaClientDataSet,
  WinSvc, TressMorado2013, ComObj, ShellApi,

  // US #7328: Implementar Control de la ayuda en XE5
  // HtmlHelpViewer,
  FHelpManager, SysUtils,

  Forms, System.Classes, dxBevel, ZetaDialogo, ZetaRegistryCliente, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, dxSkinsdxBarPainter, Xml.xmldom, Xml.XMLIntf,
  Xml.Win.msxmldom, Xml.XMLDoc, cxLocalization, Vcl.ImgList, Vcl.Controls,
  dxBar, cxClasses, dxSkinsForm, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid, Vcl.ExtCtrls,

  DImportacion, FWizTransferirTareas,
  Vcl.Menus, Vcl.StdCtrls, cxButtons,ZGridModeTools;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
  end;

  TFImportarTareasTressEmail = class(TForm)
    dxSkinController1: TdxSkinController;
    dxBarManagerbkbk: TdxBarManager;
    btnComparar: TdxBarLargeButton;
    dxBarManagerBar: TdxBar;
    cxLargeImageMenu: TcxImageList;
    DataSource: TDataSource;
    btnRefrescar: TdxBarLargeButton;
    pnlGrid: TPanel;
    cxLocalizer: TcxLocalizer;
    pnlSuperior: TPanel;
    XMLDoc: TXMLDocument;
    gridTareasEmail: TcxGrid;
    cxGridTareasEmailDBTableView: TcxGridDBTableView;
    NombreTarea: TcxGridDBColumn;
    cxGridTareasEmail: TcxGridLevel;
    Empresa: TcxGridDBColumn;
    Fecha: TcxGridDBColumn;
    Usuarios: TcxGridDBColumn;
    Frecuencia: TcxGridDBColumn;
    Reportes: TcxGridDBColumn;
    ReporteEmp: TcxGridDBColumn;
    panelInferior: TPanel;
    Descripcion: TcxGridDBColumn;
    ScheduleType: TcxGridDBColumn;
    HoraInicio: TcxGridDBColumn;
    FechaInicio: TcxGridDBColumn;
    Dias: TcxGridDBColumn;
    Meses: TcxGridDBColumn;
    btnGenerarCSV: TdxBarLargeButton;
    btnSalir: TdxBarLargeButton;
    btnImportarTareas: TdxBarLargeButton;
    LastRun: TcxGridDBColumn;
    NextRun: TcxGridDBColumn;
    Activo: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCompararClick(Sender: TObject);
    procedure cxGridServidorDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    // US #7328: Implementar Control de la ayuda en XE5
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );
    procedure btnGenerarCSVClick(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    procedure btnImportarTareasClick(Sender: TObject);
    procedure cxGridTareasEmailDBTableViewCellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridTareasEmailDBTableViewKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    private
      { Private declarations }
      procedure ApplyMinWidth;
    public
    { Public declarations }
    protected
      // US #7328: Implementar Control de la ayuda en XE5
      FOldHelpEvent: THelpEvent;
      function HTMLHelpHook( Command: Word; Data: THelpEventData; var CallHelp: Boolean ): Boolean;
      procedure ActualizarBotones(lEnabled : Boolean);

      procedure CargaTraducciones;
  end;

var
  FImportarTareasTressEmail: TFImportarTareasTressEmail;

implementation

uses
  Types, Windows, Graphics, ZetaCommonClasses,// ZetaWinAPITools,
  ZetaLicenseMgr, ZetaCommonTools, ZetaServerTools,
  MotorPatchUtils, DBClient, DSistema, FDetalleRegistro, ZetaMessages;


{$R *.dfm}

procedure TFImportarTareasTressEmail.FormCreate(Sender: TObject);
{var
  l: DWORD;}
begin
     HelpContext := 1;
     ZetaRegistryCliente.InitClientRegistry;

     WindowState := wsNormal;
     // US #7328: Implementar Control de la ayuda en XE5
     with Application do
     begin
          OnException := ManejaExcepcion;
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
     end;
end;

// US #7328: Implementar Control de la ayuda en XE5
function TFImportarTareasTressEmail.HTMLHelpHook(Command: Word; Data: THelpEventData; var CallHelp: Boolean): Boolean;
begin
    Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

// US #7328: Implementar Control de la ayuda en XE5
procedure TFImportarTareasTressEmail.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

procedure TFImportarTareasTressEmail.FormShow(Sender: TObject);
begin
     // Traducciones
     CargaTraducciones;

     // dmSistema.cdsSistBaseDatos.Conectar;
     dmSistema.cdsTareasEmail.Conectar;

     // DataSource.DataSet := dmsistema.cdsSistBaseDatos;
     DataSource.DataSet := dmsistema.cdsTareasEmail;

     cxGridTareasEmailDBTableView.DataController.DataSource := DataSource;

     //Desactiva la posibilidad de agrupar
     cxGridTareasEmailDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     cxGridTareasEmailDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     cxGridTareasEmailDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     cxGridTareasEmailDBTableView.FilterBox.CustomizeDialog := TRUE;
     //DevEx: Para que ponga el MinWidth ideal al titulo de las columnas. Posteriormente se aplica el BestFit.
     ApplyMinWidth;
     cxGridTareasEmailDBTableView.ApplyBestFit();
end;

procedure TFImportarTareasTressEmail.btnCompararClick(Sender: TObject);
begin
     dmImportacion.ProcesaArchivoTXT;
     cxGridTareasEmailDBTableView.ApplyBestFit();
end;

procedure TFImportarTareasTressEmail.btnGenerarCSVClick(Sender: TObject);
const
     K_BAT_FILE = 'ImportarEnviosTressEmail.bat';
     K_CODEPAGE = 'chcp 65001';
     K_TXT_FILE = 'ImportarEnviosTressEmail.txt';
     K_PRINT_LIST = 'schtasks /query /fo LIST /v > %s';
var
   StringList: TStringList;
   oCursor: TCursor;
   iResultado : Integer;
begin
     ActualizarBotones(False);
     StringList:=TStringList.Create;
     try
        StringList.Add(K_CODEPAGE);
        StringList.Add(format(K_PRINT_LIST, [K_TXT_FILE]));
        StringList.SaveToFile(K_BAT_FILE);
     finally
            StringList.Free;
     end;

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iResultado := ZetaMessages.WinExecAndWait32( K_BAT_FILE, VACIO, SW_HIDE );
     finally
            Screen.Cursor := oCursor;
     end;

     deletefile(K_BAT_FILE);

     if iResultado = 0 then
     begin
          dmImportacion.ProcesaArchivoTXT;
          dmSistema.cdsTareasEmail.IndexFieldNames := 'TASKNAME';
          with cxGridTareasEmailDBTableView do
          begin
               ApplyBestFit();
               dmSistema.cdsTareasEmail.First;
          end;
          ActualizarBotones(True);

               cxGridTareasEmailDBTableView.Focused := true;
     end;

     //ShellApi.ShellExecute (Application.MainForm.Handle, nil, 'cmd.exe', '/C schtasks /query /fo CSV /v > ImportarTareasTressEmail1.csv', nil, SW_HIDE);
     //ShellApi.ShellExecute (Application.MainForm.Handle, nil, 'cmd.exe', '/C schtasks /query /fo LIST /v > ImportarTareasTressEmail.txt', nil, SW_HIDE);
     //ShellApi.ShellExecute (Application.MainForm.Handle, nil, 'cmd.exe', '/C schtasks /query /fo TABLE /NH /V > ImportarTareasTressEmail1.txt', nil, SW_HIDE);
end;

procedure TFImportarTareasTressEmail.btnImportarTareasClick(Sender: TObject);
var oWizTransferirTareas: TWizTransferirTareas;
begin
     try
        oWizTransferirTareas := TWizTransferirTareas.Create(Self);
        if DataSource.DataSet.State <> dsInactive then
           oWizTransferirTareas.ShowModal
        else
            ZetaDialogo.ZError(Self.Caption, 'No existe ninguna tarea cargada a la herramienta', 0);
    finally
           FreeAndNil (oWizTransferirTareas);
    end;
end;

procedure TFImportarTareasTressEmail.btnRefrescarClick(Sender: TObject);
begin
     dmSistema.cdsTareasEmail.Refrescar;
end;

procedure TFImportarTareasTressEmail.btnSalirClick(Sender: TObject);
begin
     Close;
end;

procedure TFImportarTareasTressEmail.ActualizarBotones(lEnabled : Boolean);
begin
     btnGenerarCSV.Enabled := lEnabled;
     btnComparar.Enabled := lEnabled;
     btnRefrescar.Enabled := lEnabled;
     btnImportarTareas.Enabled := lEnabled;
     btnSalir.Enabled := lEnabled;
end;

procedure TFImportarTareasTressEmail.CargaTraducciones;
begin
     cxLocalizer.Active := True;
     cxLocalizer.Locale := 2058;  // Cardinal para Espa�ol (Mexico)
end;

procedure TFImportarTareasTressEmail.cxGridServidorDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
begin
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if cxGridTareasEmailDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( cxGridTareasEmailDBTableView, AItemIndex, AValueList );
end;

procedure TFImportarTareasTressEmail.cxGridTareasEmailDBTableViewCellDblClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
     FDetalleRegistro.MostrarDetalle;
end;

procedure TFImportarTareasTressEmail.cxGridTareasEmailDBTableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var s: string;
begin
     if Key = VK_RETURN then
        FDetalleRegistro.MostrarDetalle;


end;

procedure TFImportarTareasTressEmail.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions = 35;
begin
     with  cxGridTareasEmailDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

end.
