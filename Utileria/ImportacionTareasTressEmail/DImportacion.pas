unit DImportacion;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Datasnap.DBClient, ZetaClientDataSet, ZetaCommonTools, DSistema, ZetaCommonClasses, ZetaAsciiFile, ZetaCommonLists;

type
  TdmImportacion = class(TDataModule)
    cdsASCII: TZetaClientDataSet;
    cdsASCIIRenglon: TStringField;
    cdsUsuarios: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FDatos, FRenglon: TStringList;
    FNombreArchivo: string;
    FLinea: Integer;

  public
    { Public declarations }

    FFormatoFecha: eFormatoImpFecha;
    procedure ProcesaArchivoTXT;
    function decodificaUTF (sDatos: String): WideString;
    function ObtenerNumeros(const str: string): Integer;
    function GetFrecuencia(sFrec : String) : Integer;
    function GetRecurrencia(sFrec, sDias, sMes : String): Integer;
    function GetMeses(sFrec, sMeses : String) : String;
    function GetDias(sFrec, sDias : String) : String;
    function GetDiasMeses(sFrec, sDias: String;var sDiasSemana :String; var iDiasMes : Integer) : String;
    procedure Iniciar( sFileName : String);
    function MessageHandler(sMessage:String; lError : Boolean; Parametros:TZetaParams; sTipo:String ): String;
  end;

  TTransferLog = class( TASCIILog )
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start( const sFileName: String );
    procedure HandleException( const sMensaje: String; Error: Exception );
  end;

const
     K_TODOS_LOS_MESES = '1,2,3,4,5,6,7,8,9,10,11,12';
     K_TODOS_LOS_DIAS = '1,2,3,4,5,6,7';
     K_CADA_DIA_SEMANA = '0,1,2,3,4';

     K_CADA_DIA = 'EVERY DAY OF THE WEEK';
     K_CADA_DIA_ESP = 'TODOS LOS D�AS DE LA SEMANA';
     K_CADA_SEMANA = 'EVERY WEEK';
     K_CADA_SEMANA_ESP = 'TODOS LOS D�AS DE LA SEMANA';

     K_MENSUAL = 'MONTHLY';
     K_MENSUAL_ESP = 'MENSUALMENTE';
     K_DIARIO = 'DAILY';
     K_DIARIO_ESP = 'DIARIMENTE';
     K_SEMANAL = 'WEEKLY';
     K_SEMANAL_ESP = 'SEMANALMENTE';
     K_HORA = 'HOURLY';
     K_HORA_ESP = 'HORA';
     K_ESPECIAL = 'ONE TIME ONLY';
     K_ESPECIAL_ESP = 'S�LO UNA VEZ';

     K_DIAS_MES = 1;
     K_DIAS_SEMANA = 2;

var
  dmImportacion: TdmImportacion;

  aDias: array[ 1..7 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT' );
  aMeses: array[ 1..12 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC' );
  aWeeksOn: array[ 1..5 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'FIRST', 'SECOND', 'THIRD', 'FOURTH', 'LAST' );

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdmInterfase }

procedure TdmImportacion.DataModuleCreate(Sender: TObject);
begin
     FDatos := TStringList.Create;
end;

function TdmImportacion.decodificaUTF (sDatos: String ): WideString;
var sUTF8: WideString;
begin
     Result := sDatos;

     sUTF8 := UTF8Decode( sDatos );
     if sUTF8 <> VACIO then
        Result := sUTF8;
end;

procedure TdmImportacion.ProcesaArchivoTXT;
const
     // Columnas a obtener.
     K_DOS_PUNTOS = ':';
     K_TASK_TO_RUN =                          'TASK TO RUN';
     K_HOST_NAME =                            'HOSTNAME';
     K_REPEAT_STOP_IF_STILL_RUNNING =         'REPEAT: STOP IF STILL RUNNING';
     K_SCHEDULED_TASK_STATE =                 'SCHEDULED TASK STATE';
     K_TASK_NAME =                            'TASKNAME';
     K_NEXT_RUN_TIME =                        'NEXT RUN TIME';
     K_LAST_RUN_TIME =                        'LAST RUN TIME';
     K_COMMENT =                              'COMMENT';
     K_SCHEDULE_TYPE =                        'SCHEDULE TYPE';
     K_START_TIME =                           'START TIME';
     K_START_DATE =                           'START DATE';
     K_DAYS =                                 'DAYS';
     K_MONTHS =                               'MONTHS';
     K_REPEAT_EVERY =                         'REPEAT: EVERY:';
     K_HOUR         =                         'HOUR';

     //Espa�ol
     K_TASK_TO_RUN_ESP =                      'TAREA QUE SE EJECUTAR�';
     K_HOST_NAME_ESP =                        'NOMBRE DE HOST';
     K_REPEAT_STOP_IF_STILL_RUNNING_ESP =     'REPETIR: DETENER SI A�N SE EJECUTA';
     K_SCHEDULED_TASK_STATE_ESP =             'ESTADO DE TAREA PROGRAMADA';
     K_TASK_NAME_ESP =                        'NOMBRE DE TAREA';
     K_NEXT_RUN_TIME_ESP =                    'HORA PR�XIMA EJECUCI�N';
     K_LAST_RUN_TIME_ESP =                    '�LTIMO TIEMPO DE EJECUCI�N';
     K_COMMENT_ESP =                          'COMENTARIO';
     K_SCHEDULE_TYPE_ESP =                    'TIPO DE PROGRAMACI�N';
     K_START_TIME_ESP =                       'HORA DE INICIO';
     K_START_DATE_ESP =                       'FECHA DE INICIO';
     K_DAYS_ESP =                             'D�AS';
     K_MONTHS_ESP =                           'MESES';
     K_REPEAT_EVERY_ESP =                     'REPETIR: CADA:';
     K_HOUR_ESP =                             'HORA';


var
   i, y: Integer;
   sDatos, sDatosTemporal, sActivo, sFecha: String;
   dFecha: TDate;
   sHorario, sHora: String;
   sEstatusDia: String;
   iPosHostName: Integer;
  MyClass: TComponent;

   procedure IniciarDataSet;
   begin
        with dmSistema.cdsTareasEmail do
        begin
             Init;
              Active:= False;
              Filtered := False;
              Filter := '';
              IndexFieldNames := '';

             AddStringField ('NOMBRETAREA', 255);

             AddStringField ('TASKNAME', 255);
             AddStringField ('COMMENT', 255);
             AddStringField ('SCHEDULE_TYPE', 255);
             AddStringField ('START_TIME', 25);
             AddStringField ('START_DATE', 25);
             AddStringField ('DAYS', 255);
             AddStringField ('MONTHS', 255);
             AddStringField ('WHOLE_DATE', 25);

             AddStringField ('TASK_TO_RUN', 1024);
             // Par�metros de una tarea.
             AddStringField ('EMPRESA', 10);
             AddStringField ('FREC', 2);
             AddStringField ('FECHA', 10);
             AddStringField ('USUARIOS', 255);
             AddStringField ('REPORTES', 600);
             AddIntegerField ('REPORTE_EMP');
             AddStringField ('NEXTRUN', 25);
             AddStringField ('LASTRUN', 25);
             AddStringField ('ELEGIR', 2);
             AddStringField ('ACTIVO', 15);
             CreateTempDataset;
        end;
   end;

   function obtenerParametro (sTask, sNombreParametro: String): String;
   const K_ESPACIO = ' ';
         K_IGUAL = '=';
   var sParametros: String;
   begin
        Result := VACIO;
        if POS (sNombreParametro, sTask) > 0 then
        begin
             {POS (sNombreParametro, sTask);
             Length(sNombreParametro) + 1}
             // POS (K_ESPACIO,)
             sParametros := Copy (stask, POS (sNombreParametro, sTask), Length (sTask));
             if POS (K_ESPACIO, sParametros) > 0 then
             begin
                  Result := Copy (sParametros, POS (sNombreParametro, sParametros), POS (K_ESPACIO, sParametros));
             end
             else
             begin
                  // Result := Copy (sParametros, POS (sNombreParametro, sParametros), Length (sParametros));
                  Result := sParametros;
             end;
        end;

        // Obtener solo el valor del par�metro.
        if POS (K_IGUAL, Result) > 0 then
        begin
             Result := Copy (Result, POS (K_IGUAL, Result) + 1, Length (Result));
        end;
   end;
begin
     FDatos := TStringList.Create;
     IniciarDataSet;
     FNombreArchivo := 'ImportarEnviosTressEmail.txt';
     try
        FDatos.LoadFromFile( FNombreArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                try
                   // Pruebas.
                   // FRenglon.Delimiter := ':';
                   // FRenglon.StrictDelimiter := TRUE;
                   // FRenglon.Delimiter := ',';
                   // ----- -----

                   for i := 0 to ( FDatos.Count - 1 ) do
                   begin
                        try
                           Inc( FLinea );
                           FRenglon.Clear;

                           sDatos := FDatos[ i ];
                           // sDatos := UTF8Decode( sDatos );
                           sDatos := decodificaUTF (sDatos);

                           if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                              delete( sDatos, 1, 1);

                           // sDatos := StrTransAll( sDatos, ':', '":"' );
                           // sDatos := StrTransAll (sDatos, '"', '');

                           sDatos := ansiUpperCase( sDatos );

                           // Pruebas.
                           {FRenglon.StrictDelimiter := TRUE;
                           FRenglon.Delimiter := ',';}
                           // FRenglon.DelimitedText := '"' + sDatos + '"';
                           FRenglon.Text := '"' + sDatos + '"';
                           // FRenglon.DelimitedText := sDatos;
                           // ----- -----

                           {if FRenglon.Count > 1 then
                           begin}

                           // Encontrar �ltima posici�n de HOSTNAME.
                           if ( POS (K_HOST_NAME, FRenglon[0]) > 0 ) or ( POS (K_HOST_NAME_ESP, FRenglon[0]) > 0 ) then
                           begin
                                iPosHostName := i;
                           end;

                           // ----- -----
                           if (POS ('CARPETA', FRenglon[0]) > 0 ) then
                             FFormatoFecha := ifDDMMYYYYs
                           else
                               FFormatoFecha := ifMMDDYYYYs;

                           if (POS (K_TASK_TO_RUN, FRenglon[0]) > 0 ) or ( POS (K_TASK_TO_RUN_ESP, FRenglon[0]) > 0 ) then
                           begin
                                // Recorrer columnas.
                                // Buscar TressEmail.exe.
                                {for  y:=0 to FRenglon.Count-1 do
                                begin}
                                     if ansipos ('TRESSEMAIL.EXE', UpperCase(FRenglon [0])) > 0 then
                                     begin
                                          y := iPosHostName;
                                          dmSistema.cdsTareasEmail.Conectar;
                                          dmSistema.cdsTareasEmail.Insert;

                                          // Se encontr� rengl�n con TressEmail.exe.
                                          // Regresar a �ltima posici�n de la columna HostName para
                                          // iniciar recorrido del resto de valores de la tarea.
                                          // Se est� asumiento que el valor HostName es el primero de cualquier tarea listada.
                                          sDatosTemporal := FDatos [y];

                                          while ( POS (K_REPEAT_STOP_IF_STILL_RUNNING, sDatosTemporal) = 0 ) and ( POS (K_REPEAT_STOP_IF_STILL_RUNNING_ESP, sDatosTemporal) = 0 ) do
                                          begin
                                               sDatosTemporal := FDatos [y];
                                               sDatosTemporal := decodificaUTF (sDatosTemporal);
                                               if (sDatosTemporal <> '') and (Copy( sDatosTemporal, 1,1 ) = #$FEFF) then
                                                  delete( sDatosTemporal, 1, 1);
                                               sDatosTemporal := ansiUpperCase( sDatosTemporal );

                                               if ( POS (K_TASK_NAME, sDatosTemporal) > 0) or ( POS (K_TASK_NAME_ESP, sDatosTemporal) > 0 ) then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('TASKNAME').AsString :=
                                                        StringReplace (TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal))) , '\' , VACIO, [rfReplaceAll, rfIgnoreCase])
                                               end
                                               else if ( POS (K_TASK_TO_RUN, sDatosTemporal) > 0) or (POS (K_TASK_TO_RUN_ESP, sDatosTemporal) > 0) then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('TASK_TO_RUN').AsString :=
                                                        TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));

                                                    dmSistema.cdsTareasEmail.FieldByName('NOMBRETAREA').AsString := sDatosTemporal;
                                                    dmSistema.cdsTareasEmail.FieldByName('EMPRESA').AsString := obtenerParametro (sDatosTemporal, 'EMPRESA');
                                                    dmSistema.cdsTareasEmail.FieldByName('FREC').AsString := obtenerParametro (sDatosTemporal, 'FREC');
                                                    dmSistema.cdsTareasEmail.FieldByName('FECHA').AsString := obtenerParametro (sDatosTemporal, 'FECHA');
                                                    dmSistema.cdsTareasEmail.FieldByName('USUARIOS').AsString := obtenerParametro (sDatosTemporal, 'USUARIOS');
                                                    dmSistema.cdsTareasEmail.FieldByName('REPORTES').AsString := obtenerParametro (sDatosTemporal, 'REPORTES');
                                                    dmSistema.cdsTareasEmail.FieldByName('REPORTE_EMP').AsString := obtenerParametro (sDatosTemporal, 'REPORTE_EMP');
                                               end
                                               else if (POS (K_COMMENT, sDatosTemporal) > 0) or (POS (K_COMMENT_ESP, sDatosTemporal) > 0 ) then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('COMMENT').AsString :=
                                                        TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));
                                               end
                                               {     K_SCHEDULE_TYPE =                        'SCHEDULE TYPE';
                                                    K_START_TIME =                           'START TIME';}
                                               else if ( POS (K_SCHEDULE_TYPE, sDatosTemporal) > 0 ) or (POS (K_SCHEDULE_TYPE_ESP, sDatosTemporal) > 0 ) then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('SCHEDULE_TYPE').AsString :=
                                                        TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));
                                               end
                                               else if ( POS (K_START_TIME, sDatosTemporal) > 0 ) or ( POS (K_START_TIME_ESP, sDatosTemporal) > 0 ) then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('START_TIME').AsString := TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));
                                               end
                                               else if ( POS (K_START_DATE, sDatosTemporal) > 0 ) or ( POS (K_START_DATE_ESP, sDatosTemporal) > 0  )then
                                               begin
                                                    sFecha := TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));                                                  
                                                    try
                                                       dmSistema.cdsTareasEmail.FieldByName('START_DATE').AsString := UpperCase ( FormatDateTime('dd/MMM/yyyy', StrToDateTime (sFecha)));
                                                       dmSistema.cdsTareasEmail.FieldByName('WHOLE_DATE').AsString := sFecha + ' ' + dmSistema.cdsTareasEmail.FieldByName('START_TIME').AsString;    
                                                    except on Error : Exception do
                                                      begin
                                                           dmSistema.cdsTareasEmail.FieldByName('START_DATE').AsString := sFecha; 
                                                           dmSistema.cdsTareasEmail.FieldByName('WHOLE_DATE').AsString := sFecha;
                                                      end;
                                                    end;
                                                    
                                                    
                                               end
                                               else if ( POS (K_DAYS, sDatosTemporal) > 0 ) or (  POS (K_DAYS_ESP, sDatosTemporal) > 0  )then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('DAYS').AsString :=
                                                        TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));
                                               end
                                               else if ( POS (K_MONTHS, sDatosTemporal) > 0 ) or ( POS (K_MONTHS_ESP, sDatosTemporal) > 0 ) then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('MONTHS').AsString :=
                                                        TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));
                                               end
                                               else if ( POS (K_NEXT_RUN_TIME, sDatosTemporal) > 0 ) or ( POS (K_NEXT_RUN_TIME_ESP, sDatosTemporal) > 0 ) then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('NEXTRUN').AsString := TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));
                                                    {try
                                                        dmSistema.cdsTareasEmail.FieldByName('START_DATE').AsString := UpperCase( FormatDateTime('dd/MMM/yyyy', StrToDateTime (dmSistema.cdsTareasEmail.FieldByName('NEXTRUN').AsString)));
                                                        dmSistema.cdsTareasEmail.FieldByName('START_TIME').AsString := FormatDateTime('hh:mm:ss AM/PM', StrToDateTime(dmSistema.cdsTareasEmail.FieldByName('NEXTRUN').AsString));
                                                    except on Error : Exception do
                                                      begin
                                                           dmSistema.cdsTareasEmail.FieldByName('START_DATE').AsString := dmSistema.cdsTareasEmail.FieldByName('NEXTRUN').AsString;
                                                           dmSistema.cdsTareasEmail.FieldByName('START_TIME').AsString := dmSistema.cdsTareasEmail.FieldByName('NEXTRUN').AsString;
                                                      end;
                                                    end;   }
                                               end
                                               else if ( POS (K_LAST_RUN_TIME, sDatosTemporal) > 0 ) or  ( POS (K_LAST_RUN_TIME_ESP, sDatosTemporal) > 0 ) then
                                               begin
                                                    dmSistema.cdsTareasEmail.FieldByName('LASTRUN').AsString :=
                                                        TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal)));
                                               end
                                               else if ( POS (K_SCHEDULED_TASK_STATE, sDatosTemporal) > 0 ) or ( POS (K_SCHEDULED_TASK_STATE_ESP, sDatosTemporal) > 0 )then
                                               begin
                                                    sActivo := (TRIM (Copy (sDatosTemporal, POS (K_DOS_PUNTOS, sDatosTemporal) + 1, Length (sDatosTemporal))));

                                                    if ('ENABLED' = sActivo) or ('HABILITADA' = sActivo) then
                                                       dmSistema.cdsTareasEmail.FieldByName('ACTIVO').AsString := K_GLOBAL_SI
                                                    else
                                                        dmSistema.cdsTareasEmail.FieldByName('ACTIVO').AsString := K_GLOBAL_NO;

                                                    
                                               end
                                               else if ( POS (K_REPEAT_EVERY, sDatosTemporal) > 0 ) or ( POS (K_REPEAT_EVERY_ESP, sDatosTemporal) > 0 ) then
                                               begin
                                                    if (( POS (K_HOUR, sDatosTemporal) > 0 ) AND ( POS (K_HOUR, dmSistema.cdsTareasEmail.FieldByName('SCHEDULE_TYPE').AsString) > 0 )) or
                                                       (( POS (K_HOUR_ESP, sDatosTemporal) > 0 ) AND ( POS (K_HOUR_ESP, dmSistema.cdsTareasEmail.FieldByName('SCHEDULE_TYPE').AsString) > 0 )) then
                                                    begin
                                                         sHora := TRIM (Copy (sDatosTemporal, POS (K_REPEAT_EVERY, sDatosTemporal) + Length(K_REPEAT_EVERY), Length (sDatosTemporal)));
                                                         dmSistema.cdsTareasEmail.FieldByName('DAYS').AsString := Copy(sHora,1,POS(',',sHora)-1);
                                                    end;
                                               end;

                                               y := y + 1;
                                          end;

                                          dmSistema.cdsTareasEmail.Enviar;
                                     end;
                                // end;
                           end;
                           // end; // if FRenglon.Count > 1
                        except
                            on Error : Exception do
                            begin
                                 // EscribeError( Format ('Error en el proceso: %s', [Error.Message]), 0, FLinea );
                                 Error.Message := Error.Message;
                            end;
                        end;
                   end; // for i := 0 to ( FDatos.Count - 1 ) do
                except
                    on Error : Exception do
                       // EscribeError( Format ('Error en el proceso: %s', [Error.Message]), 0, FLinea );
                end;
             finally
                    FreeAndNil (Frenglon);
             end;
        end; // ( FDatos.Count > 0 )
     finally
            FreeAndNIl(FDatos);
     end;
end;


function TdmImportacion.ObtenerNumeros(const str: string): Integer;
var
   i, Count: Integer;
   sCantidad : string;
begin
     SetLength(sCantidad, Length(str));
     Count := 0;
     for i := 1 to Length(str) do
     begin
          if (CharInSet ( str[i], ['0'..'9'] ) ) then
          begin
                 inc(Count);
                 sCantidad[Count] := str[i];
          end
          else
          begin
               inc(Count);
               sCantidad[Count] := ' ';
          end;
     end;
     SetLength(sCantidad, Count);

     sCantidad := Trim(sCantidad);
     if sCantidad = VACIO then
        Result := 1
     else
         Result := StrToInt(sCantidad);
end;

function TdmImportacion.GetFrecuencia(sFrec : String) : Integer;
begin
     if ( sFrec = K_SEMANAL ) or ( sFrec = K_SEMANAL_ESP ) then
        Result := 1
     else if ( sFrec = K_MENSUAL ) or ( sFrec = K_MENSUAL_ESP ) then
          Result := 2
     else if ( POS(K_HORA, sFrec) > 0 ) or  ( POS(K_HORA_ESP, sFrec) > 0 ) then
          Result := 3
     else if ( POS(K_ESPECIAL, sFrec) > 0 ) or ( POS(K_ESPECIAL_ESP, sFrec) > 0 )  then
          Result := 4
     else
         Result := 0;
end;

function TdmImportacion.GetRecurrencia(sFrec, sDias, sMes : String): Integer;
begin
     if ( sFrec = K_DIARIO ) or ( sFrec = K_DIARIO_ESP ) then
        Result :=  ObtenerNumeros(sDias)
     else if ( sFrec = K_SEMANAL ) or ( sFrec = K_MENSUAL) or ( sFrec = K_SEMANAL_ESP ) or ( sFrec = K_MENSUAL_ESP )  then
          Result := ObtenerNumeros(sMes)
     else if ( POS(K_HORA, sFrec) > 0 ) or ( POS(K_HORA_ESP, sFrec) > 0 ) then
          Result := ObtenerNumeros(sDias)
     else
         Result := 0;
end;

function TdmImportacion.GetMeses(sFrec, sMeses : String) : String;
var i: Integer;
begin
     Result := VACIO;
     if ( sFrec = K_MENSUAL ) or ( sFrec = K_MENSUAL_ESP ) then
     begin
          for i := 1 to Length(aMeses) do
          begin
               if POS (aMeses[i], sMeses) > 0 then
                  Result := Result + IntToStr(i) + ',';
          end;

          if Result = VACIO then
             Result := K_TODOS_LOS_MESES
          else
              delete(Result,length(Result),3);
     end;
end;

function TdmImportacion.GetDias(sFrec, sDias : String) : String;
var i: Integer;
begin
     Result := VACIO;
     if ( sFrec = K_SEMANAL ) or ( sFrec = K_MENSUAL ) or ( sFrec = K_SEMANAL_ESP ) or ( sFrec = K_MENSUAL_ESP )then
     begin
          for i := 1 to Length(aDias) do
          begin
               if POS (aDias[i], sDias) > 0 then
                  Result := Result + IntToStr(i) + ',';
          end;

          if ( ( Result = VACIO ) and ( sFrec = K_SEMANAL )) or ( ( Result = VACIO ) and ( sFrec = K_SEMANAL_ESP ))
             or ( POS(K_CADA_DIA,sDias) > 0) or ( POS(K_CADA_DIA_ESP,sDias) > 0) then
             Result := K_TODOS_LOS_DIAS
          else
              delete(Result,length(Result),3);
     end;
end;

function TdmImportacion.GetDiasMeses(sFrec, sDias: String;var sDiasSemana :String; var iDiasMes : Integer) : String;
var i : integer;
begin
     Result := VACIO;
     if ( sFrec = K_MENSUAL ) or ( sFrec = K_MENSUAL_ESP ) then
     begin
          if ( POS(K_CADA_SEMANA, sDias) > 0 ) or ( POS(K_CADA_SEMANA_ESP, sDias) > 0 ) then
          begin
               Result := K_CADA_DIA_SEMANA;
               iDiasMes := K_DIAS_SEMANA;
          end
          else
          begin
               for i := 1 to Length(aWeeksOn) do
               begin
                    if POS (aWeeksOn[i], sDias) > 0 then
                       Result := Result + IntToStr(i-1) + ','
               end;

               delete(Result,length(Result),3);
          end;

          if Result = VACIO then
          begin
               iDiasMes := K_DIAS_MES;
               sDias := StringReplace(sDias, '32', '0', [rfReplaceAll, rfIgnoreCase]);
               sDias := StringReplace(sDias, ' ', '', [rfReplaceAll, rfIgnoreCase]);
               Result := sDias;
          end
          else
          begin
               sDiasSemana := Result;
               Result := VACIO;
          end;
     end;
end;

procedure TdmImportacion.Iniciar( sFileName : String);
begin
     if StrVacio(ExtractFileExt(sFileName)) then
        sFileName := sFileName + '.txt';

     if FileExists( sFileName ) then
        DeleteFile( sFileName );
end;

function TdmImportacion.MessageHandler(sMessage:String; lError : Boolean; Parametros:TZetaParams; sTipo : String): String;
const
     K_MSG_DUPLICATE_ERROR = 'Ya existe el registro del env�o %s con frecuencia %s.';
     K_MSG_EMPRESA_ERROR = 'No fue posible importar el env�o %s, la empresa %s no existe en el COMPARTE.';
     K_MSG_EXITO = 'El env�o %s con frecuencia %s, fue importada con �xito en la empresa %s.';
     K_MSG_EXITO_ERROR = 'El env�o %s con frecuencia %s, fue importada con �xito en la empresa %s, sin embargo hubo un error al obtener al establecer la siguiente fecha de ejecuci�n. Error:  %s';
     K_MSG_GENERICO_ERROR = 'Hubo un error al grabar el env�o %s por: %s';
     K_MSG_FECHAS_ERROR = 'La fecha de inicio no es correcta.';
     K_DUPLICATE_ERROR = 'Cannot insert duplicate key row';
     K_FECHAS_ERROR = 'is not a valid date and time';
     K_EMPRESA_ERROR = 'Error Empresa';
var
   sNombre, sEmpresa : String;
begin
     with Parametros do
     begin
          sNombre := ParamByName('Nombre').AsString;
          sEmpresa := ParamByName('Empresa').AsString;
          if Not lError then
          begin
               if POS(K_DUPLICATE_ERROR, sMessage) > 0 then
                  Result := Format(K_MSG_DUPLICATE_ERROR, [ sNombre, sTipo] )
               else if POS(K_EMPRESA_ERROR, sMessage) > 0 then
                    Result := Format(K_MSG_EMPRESA_ERROR, [ sNombre, sEmpresa ] )
               else if POS(K_FECHAS_ERROR, sMessage) > 0 then
                    Result := Format(K_MSG_GENERICO_ERROR, [ sNombre, K_MSG_FECHAS_ERROR ] )
               else
                   Result := Format(K_MSG_GENERICO_ERROR, [ sNombre, sMessage ] );
          end
          else
          begin
               if StrVacio(sMessage) then
                  Result := Format(K_MSG_EXITO, [ sNombre, sTipo, sEmpresa ])
               else
                   Result := Format(K_MSG_EXITO_ERROR, [ sNombre, sTipo, sEmpresa, sMessage ])
          end;
     end;
end;


{ ******** TTransferLog ********* }

procedure TTransferLog.Start( const sFileName: String );
begin
     Init( sFileName );
end;

procedure TTransferLog.HandleException( const sMensaje: String; Error: Exception );
begin
     WriteTexto( sMensaje );
     WriteTexto( Error.ClassName + ' = ' + Error.Message );
end;

end.
