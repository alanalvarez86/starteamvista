unit Curl;

interface

uses Sysutils, Classes;

type
  TCURL = Pointer;
  PCURL = ^TCURL;

  CURLcode = (
    CURLE_OK = 0,
    CURLE_UNSUPPORTED_PROTOCOL,
    CURLE_FAILED_INIT,
    CURLE_URL_MALFORMAT,
    CURLE_OBSOLETE4,
    CURLE_COULDNT_RESOLVE_PROXY,
    CURLE_COULDNT_RESOLVE_HOST,
    CURLE_COULDNT_CONNECT,
    CURLE_FTP_WEIRD_SERVER_REPLY,
    CURLE_REMOTE_ACCESS_DENIED,
    CURLE_OBSOLETE10,
    CURLE_FTP_WEIRD_PASS_REPLY,
    CURLE_OBSOLETE12,
    CURLE_FTP_WEIRD_PASV_REPLY,
    CURLE_FTP_WEIRD_227_FORMAT,
    CURLE_FTP_CANT_GET_HOST,
    CURLE_OBSOLETE16,
    CURLE_FTP_COULDNT_SET_TYPE,
    CURLE_PARTIAL_FILE,
    CURLE_FTP_COULDNT_RETR_FILE,
    CURLE_OBSOLETE20,
    CURLE_QUOTE_ERROR,
    CURLE_HTTP_RETURNED_ERROR,
    CURLE_WRITE_ERROR,
    CURLE_OBSOLETE24,
    CURLE_UPLOAD_FAILED,
    CURLE_READ_ERROR,
    CURLE_OUT_OF_MEMORY,
    CURLE_OPERATION_TIMEDOUT,
    CURLE_OBSOLETE29,
    CURLE_FTP_PORT_FAILED,
    CURLE_FTP_COULDNT_USE_REST,
    CURLE_OBSOLETE32,
    CURLE_RANGE_ERROR,
    CURLE_HTTP_POST_ERROR,
    CURLE_SSL_CONNECT_ERROR,
    CURLE_BAD_DOWNLOAD_RESUME,
    CURLE_FILE_COULDNT_READ_FILE,
    CURLE_LDAP_CANNOT_BIND,
    CURLE_LDAP_SEARCH_FAILED,
    CURLE_OBSOLETE40,
    CURLE_FUNCTION_NOT_FOUND,
    CURLE_ABORTED_BY_CALLBACK,
    CURLE_BAD_FUNCTION_ARGUMENT,
    CURLE_OBSOLETE44,
    CURLE_INTERFACE_FAILED,
    CURLE_OBSOLETE46,
    CURLE_TOO_MANY_REDIRECTS ,
    CURLE_UNKNOWN_TELNET_OPTION,
    CURLE_TELNET_OPTION_SYNTAX ,
    CURLE_OBSOLETE50,
    CURLE_PEER_FAILED_VERIFICATION,
    CURLE_GOT_NOTHING,
    CURLE_SSL_ENGINE_NOTFOUND,
    CURLE_SSL_ENGINE_SETFAILED,
    CURLE_SEND_ERROR,
    CURLE_RECV_ERROR,
    CURLE_OBSOLETE57,
    CURLE_SSL_CERTPROBLEM,
    CURLE_SSL_CIPHER,
    CURLE_SSL_CACERT,
    CURLE_BAD_CONTENT_ENCODING,
    CURLE_LDAP_INVALID_URL,
    CURLE_FILESIZE_EXCEEDED,
    CURLE_USE_SSL_FAILED,
    CURLE_SEND_FAIL_REWIND,
    CURLE_SSL_ENGINE_INITFAILED,
    CURLE_LOGIN_DENIED,
    CURLE_TFTP_NOTFOUND,
    CURLE_TFTP_PERM,
    CURLE_REMOTE_DISK_FULL,
    CURLE_TFTP_ILLEGAL,
    CURLE_TFTP_UNKNOWNID,
    CURLE_REMOTE_FILE_EXISTS,
    CURLE_TFTP_NOSUCHUSER,
    CURLE_CONV_FAILED,
    CURLE_CONV_REQD,
    CURLE_SSL_CACERT_BADFILE,
    CURLE_REMOTE_FILE_NOT_FOUND,
    CURLE_SSH,
    CURLE_SSL_SHUTDOWN_FAILED,
    CURLE_AGAIN,
    CURLE_SSL_CRL_BADFILE,
    CURLE_SSL_ISSUER_ERROR,
    CURLE_FTP_PRET_FAILED,
    CURLE_RTSP_CSEQ_ERROR,
    CURLE_RTSP_SESSION_ERROR,
    CURLE_FTP_BAD_FILE_LIST,
    CURLE_CHUNK_FAILED,
    CURL_LAST
  );

  CURLoption = (
    CURLOPT_PORT                        = $0003,
    CURLOPT_TIMEOUT                     = $000D,
    CURLOPT_INFILESIZE                  = $000E,
    CURLOPT_LOW_SPEED_LIMIT             = $0013,
    CURLOPT_LOW_SPEED_TIME              = $0014,
    CURLOPT_RESUME_FROM                 = $0015,
    CURLOPT_CRLF                        = $001B,
    CURLOPT_SSLVERSION                  = $0020,
    CURLOPT_TIMECONDITION               = $0021,
    CURLOPT_TIMEVALUE                   = $0022,
    CURLOPT_VERBOSE                     = $0029,
    CURLOPT_HEADER                      = $002A,
    CURLOPT_NOPROGRESS                  = $002B,
    CURLOPT_NOBODY                      = $002C,
    CURLOPT_FAILONERROR                 = $002D,
    CURLOPT_UPLOAD                      = $002E,
    CURLOPT_POST                        = $002F,
    CURLOPT_DIRLISTONLY                 = $0030,
    CURLOPT_APPEND                      = $0032,
    CURLOPT_NETRC                       = $0033,
    CURLOPT_FOLLOWLOCATION              = $0034,
    CURLOPT_TRANSFERTEXT                = $0035,
    CURLOPT_PUT                         = $0036,
    CURLOPT_AUTOREFERER                 = $003A,
    CURLOPT_PROXYPORT                   = $003B,
    CURLOPT_POSTFIELDSIZE               = $003C,
    CURLOPT_HTTPPROXYTUNNEL             = $003D,
    CURLOPT_SSL_VERIFYPEER              = $0040,
    CURLOPT_MAXREDIRS                   = $0044,
    CURLOPT_FILETIME                    = $0045,
    CURLOPT_MAXCONNECTS                 = $0047,
    CURLOPT_CLOSEPOLICY                 = $0048,
    CURLOPT_FRESH_CONNECT               = $004A,
    CURLOPT_FORBID_REUSE                = $004B,
    CURLOPT_CONNECTTIMEOUT              = $004E,
    CURLOPT_HTTPGET                     = $0050,
    CURLOPT_SSL_VERIFYHOST              = $0051,
    CURLOPT_HTTP_VERSION                = $0054,
    CURLOPT_FTP_USE_EPSV                = $0055,
    CURLOPT_SSLENGINE_DEFAULT           = $005A,
    CURLOPT_DNS_USE_GLOBAL_CACHE        = $005B,
    CURLOPT_DNS_CACHE_TIMEOUT           = $005C,
    CURLOPT_COOKIESESSION               = $0060,
    CURLOPT_BUFFERSIZE                  = $0062,
    CURLOPT_NOSIGNAL                    = $0063,
    CURLOPT_PROXYTYPE                   = $0065,
    CURLOPT_UNRESTRICTED_AUTH           = $0069,
    CURLOPT_FTP_USE_EPRT                = $006A,
    CURLOPT_HTTPAUTH                    = $006B,
    CURLOPT_FTP_CREATE_MISSING_DIRS     = $006E,
    CURLOPT_PROXYAUTH                   = $006F,
    CURLOPT_FTP_RESPONSE_TIMEOUT        = $0070,
    CURLOPT_IPRESOLVE                   = $0071,
    CURLOPT_MAXFILESIZE                 = $0072,
    CURLOPT_USE_SSL                     = $0077,
    CURLOPT_TCP_NODELAY                 = $0079,
    CURLOPT_FTPSSLAUTH                  = $0081,
    CURLOPT_IGNORE_CONTENT_LENGTH       = $0088,
    CURLOPT_FTP_SKIP_PASV_IP            = $0089,
    CURLOPT_FTP_FILEMETHOD              = $008A,
    CURLOPT_LOCALPORT                   = $008B,
    CURLOPT_LOCALPORTRANGE              = $008C,
    CURLOPT_CONNECT_ONLY                = $008D,
    CURLOPT_SSL_SESSIONID_CACHE         = $0096,
    CURLOPT_SSH_AUTH_TYPES              = $0097,
    CURLOPT_FTP_SSL_CCC                 = $009A,
    CURLOPT_TIMEOUT_MS                  = $009B,
    CURLOPT_CONNECTTIMEOUT_MS           = $009C,
    CURLOPT_HTTP_TRANSFER_DECODING      = $009D,
    CURLOPT_HTTP_CONTENT_DECODING       = $009E,
    CURLOPT_NEW_FILE_PERMS              = $009F,
    CURLOPT_NEW_DIRECTORY_PERMS         = $00A0,
    CURLOPT_POSTREDIR                   = $00A1,
    CURLOPT_PROXY_TRANSFER_MODE         = $00A6,
    CURLOPT_ADDRESS_SCOPE               = $00AB,
    CURLOPT_CERTINFO                    = $00AC,
    CURLOPT_TFTP_BLKSIZE                = $00B2,
    CURLOPT_SOCKS5_GSSAPI_NEC           = $00B4,
    CURLOPT_PROTOCOLS                   = $00B5,
    CURLOPT_REDIR_PROTOCOLS             = $00B6,
    CURLOPT_FTP_USE_PRET                = $00BC,
    CURLOPT_RTSP_REQUEST                = $00BD,
    CURLOPT_RTSP_CLIENT_CSEQ            = $00C1,
    CURLOPT_RTSP_SERVER_CSEQ            = $00C2,
    CURLOPT_WILDCARDMATCH               = $00C5,
    CURLOPT_FILE                        = $2711,
    CURLOPT_URL                         = $2712,
    CURLOPT_PROXY                       = $2714,
    CURLOPT_USERPWD                     = $2715,
    CURLOPT_PROXYUSERPWD                = $2716,
    CURLOPT_RANGE                       = $2717,
    CURLOPT_INFILE                      = $2719,
    CURLOPT_ERRORBUFFER                 = $271A,
    CURLOPT_POSTFIELDS                  = $271F,
    CURLOPT_REFERER                     = $2720,
    CURLOPT_FTPPORT                     = $2721,
    CURLOPT_USERAGENT                   = $2722,
    CURLOPT_COOKIE                      = $2726,
    CURLOPT_HTTPHEADER                  = $2727,
    CURLOPT_HTTPPOST                    = $2728,
    CURLOPT_SSLCERT                     = $2729,
    CURLOPT_KEYPASSWD                   = $272A,
    CURLOPT_QUOTE                       = $272C,
    CURLOPT_WRITEHEADER                 = $272D,
    CURLOPT_COOKIEFILE                  = $272F,
    CURLOPT_CUSTOMREQUEST               = $2734,
    CURLOPT_STDERR                      = $2735,
    CURLOPT_POSTQUOTE                   = $2737,
    CURLOPT_WRITEINFO                   = $2738,
    CURLOPT_PROGRESSDATA                = $2749,
    CURLOPT_INTERFACE                   = $274E,
    CURLOPT_KRBLEVEL                    = $274F,
    CURLOPT_CAINFO                      = $2751,
    CURLOPT_TELNETOPTIONS               = $2756,
    CURLOPT_RANDOM_FILE                 = $275C,
    CURLOPT_EGDSOCKET                   = $275D,
    CURLOPT_COOKIEJAR                   = $2762,
    CURLOPT_SSL_CIPHER_LIST             = $2763,
    CURLOPT_SSLCERTTYPE                 = $2766,
    CURLOPT_SSLKEY                      = $2767,
    CURLOPT_SSLKEYTYPE                  = $2768,
    CURLOPT_SSLENGINE                   = $2769,
    CURLOPT_PREQUOTE                    = $276D,
    CURLOPT_DEBUGDATA                   = $276F,
    CURLOPT_CAPATH                      = $2771,
    CURLOPT_SHARE                       = $2774,
    CURLOPT_ENCODING                    = $2776,
    CURLOPT_PRIVATE                     = $2777,
    CURLOPT_HTTP200ALIASES              = $2778,
    CURLOPT_SSL_CTX_DATA                = $277D,
    CURLOPT_NETRC_FILE                  = $2786,
    CURLOPT_IOCTLDATA                   = $2793,
    CURLOPT_FTP_ACCOUNT                 = $2796,
    CURLOPT_COOKIELIST                  = $2797,
    CURLOPT_FTP_ALTERNATIVE_TO_USER     = $27A3,
    CURLOPT_SOCKOPTDATA                 = $27A5,
    CURLOPT_SSH_PUBLIC_KEYFILE          = $27A8,
    CURLOPT_SSH_PRIVATE_KEYFILE         = $27A9,
    CURLOPT_SSH_HOST_PUBLIC_KEY_MD5     = $27B2,
    CURLOPT_OPENSOCKETDATA              = $27B4,
    CURLOPT_COPYPOSTFIELDS              = $27B5,
    CURLOPT_SEEKDATA                    = $27B8,
    CURLOPT_CRLFILE                     = $27B9,
    CURLOPT_ISSUERCERT                  = $27BA,
    CURLOPT_USERNAME                    = $27BD,
    CURLOPT_PASSWORD                    = $27BE,
    CURLOPT_PROXYUSERNAME               = $27BF,
    CURLOPT_PROXYPASSWORD               = $27C0,
    CURLOPT_NOPROXY                     = $27C1,
    CURLOPT_SOCKS5_GSSAPI_SERVICE       = $27C3,
    CURLOPT_SSH_KNOWNHOSTS              = $27C7,
    CURLOPT_SSH_KEYDATA                 = $27C9,
    CURLOPT_MAIL_FROM                   = $27CA,
    CURLOPT_MAIL_RCPT                   = $27CB,
    CURLOPT_RTSP_SESSION_ID             = $27CE,
    CURLOPT_RTSP_STREAM_URI             = $27CF,
    CURLOPT_RTSP_TRANSPORT              = $27D0,
    CURLOPT_INTERLEAVEDATA              = $27D3,
    CURLOPT_CHUNK_DATA                  = $27D9,
    CURLOPT_FNMATCH_DATA                = $27DA,
    CURLOPT_RESOLVE                     = $27DB,
    CURLOPT_TLSAUTH_USERNAME            = $27DC,
    CURLOPT_TLSAUTH_PASSWORD            = $27DD,
    CURLOPT_TLSAUTH_TYPE                = $27DE,
    CURLOPT_WRITEFUNCTION               = $4E2B,
    CURLOPT_READFUNCTION                = $4E2C,
    CURLOPT_PROGRESSFUNCTION            = $4E58,
    CURLOPT_HEADERFUNCTION              = $4E6F,
    CURLOPT_DEBUGFUNCTION               = $4E7E,
    CURLOPT_SSL_CTX_FUNCTION            = $4E8C,
    CURLOPT_IOCTLFUNCTION               = $4EA2,
    CURLOPT_CONV_FROM_NETWORK_FUNCTION  = $4EAE,
    CURLOPT_CONV_TO_NETWORK_FUNCTION    = $4EAF,
    CURLOPT_CONV_FROM_UTF8_FUNCTION     = $4EB0,
    CURLOPT_SOCKOPTFUNCTION             = $4EB4,
    CURLOPT_OPENSOCKETFUNCTION          = $4EC3,
    CURLOPT_SEEKFUNCTION                = $4EC7,
    CURLOPT_SSH_KEYFUNCTION             = $4ED8,
    CURLOPT_INTERLEAVEFUNCTION          = $4EE4,
    CURLOPT_CHUNK_BGN_FUNCTION          = $4EE6,
    CURLOPT_CHUNK_END_FUNCTION          = $4EE7,
    CURLOPT_FNMATCH_FUNCTION            = $4EE8,
    CURLOPT_INFILESIZE_LARGE            = $75A3,
    CURLOPT_RESUME_FROM_LARGE           = $75A4,
    CURLOPT_MAXFILESIZE_LARGE           = $75A5,
    CURLOPT_POSTFIELDSIZE_LARGE         = $75A8,
    CURLOPT_MAX_SEND_SPEED_LARGE        = $75C1,
    CURLOPT_MAX_RECV_SPEED_LARGE        = $75C2
  );

  curl_usessl = (
    CURLUSESSL_NONE = 0,
    CURLUSESSL_TRY,
    CURLUSESSL_CONTROL,
    CURLUSESSL_ALL,
    CURLUSESSL_LAST
  );


  curl_infotype = (
   CURLINFO_TEXT = 0,
   CURLINFO_HEADER_IN,
   CURLINFO_HEADER_OUT,
   CURLINFO_DATA_IN,
   CURLINFO_DATA_OUT,
   CURLINFO_SSL_DATA_IN,
   CURLINFO_SSL_DATA_OUT, 
   CURLINFO_END
 ) ;




  pcurl_slist = ^curl_slist;
  curl_slist = record
    data: PAnsiChar;
    next: pcurl_slist;
  end;

  CURLFORMcode = (
    CURL_FORMADD_OK,
    CURL_FORMADD_MEMORY,
    CURL_FORMADD_OPTION_TWICE,
    CURL_FORMADD_NULL,
    CURL_FORMADD_UNKNOWN_OPTION,
    CURL_FORMADD_INCOMPLETE,
    CURL_FORMADD_ILLEGAL_ARRAY,
    CURL_FORMADD_DISABLED,
    CURL_FORMADD_LAST
  );

  CURLformoption = (
    CURLFORM_NOTHING,
    CURLFORM_COPYNAME,
    CURLFORM_PTRNAME,
    CURLFORM_NAMELENGTH,
    CURLFORM_COPYCONTENTS,
    CURLFORM_PTRCONTENTS,
    CURLFORM_CONTENTSLENGTH,
    CURLFORM_FILECONTENT,
    CURLFORM_ARRAY,
    CURLFORM_OBSOLETE,
    CURLFORM_FILE,
    CURLFORM_BUFFER,
    CURLFORM_BUFFERPTR,
    CURLFORM_BUFFERLENGTH,
    CURLFORM_CONTENTTYPE,
    CURLFORM_CONTENTHEADER,
    CURLFORM_FILENAME,
    CURLFORM_END,
    CURLFORM_OBSOLETE2,
    CURLFORM_STREAM,
    CURLFORM_LASTENTRY
  );

  pcurl_httppost = ^curl_httppost;
  ppcurl_httppost = ^pcurl_httppost;
  curl_httppost = record
    next: pcurl_httppost;
    name: PAnsiChar;
    namelength: Longint;
    contents: PAnsiChar;
    contentslength: Longint;
    buffer: PAnsiChar;
    bufferlength: Longint;
    contenttype: PAnsiChar;
    contentheader: pcurl_slist;
    more: pcurl_httppost;
    flags: Longint;
    showfilename: PAnsiChar;
    userp: Pointer;
  end;

const
  libcurl = 'libcurl.dll';

  CURL_GLOBAL_SSL     = 1;
  CURL_GLOBAL_WIN32   = 2;
  CURL_GLOBAL_ALL     = CURL_GLOBAL_SSL or CURL_GLOBAL_WIN32;
  CURL_GLOBAL_NOTHING = 0;
  CURL_GLOBAL_DEFAULT = CURL_GLOBAL_ALL;

  HTTPPOST_FILENAME     =  1;
  HTTPPOST_READFILE     =  2;
  HTTPPOST_PTRNAME      =  4;
  HTTPPOST_PTRCONTENTS  =  8;
  HTTPPOST_BUFFER       = 16;
  HTTPPOST_PTRBUFFER    = 32;
  HTTPPOST_CALLBACK     = 64;


// easy interface
function curl_easy_init: TCURL;
  cdecl; external libcurl;
function curl_easy_setopt(curl: TCURL; option: CURLoption): CURLcode;
  cdecl; varargs; external libcurl;
function curl_easy_perform(curl: TCURL): CURLcode;
  cdecl; external libcurl;
procedure curl_easy_cleanup(curl: TCURL);
  cdecl; external libcurl;
// initialization
function curl_global_init(flags: Longint): CURLcode;
  cdecl; external libcurl;
procedure curl_global_cleanup;
  cdecl; external libcurl;
// curl_slist
function curl_slist_append(list: pcurl_slist; const Str: PAnsiChar): pcurl_slist;
  cdecl; external libcurl;
procedure curl_slist_free_all(list: pcurl_slist);
  cdecl; external libcurl;
// forms
function curl_formadd(httppost, last_post: ppcurl_httppost): CURLFORMcode;
  cdecl; varargs; external libcurl;
procedure curl_formfree(form: pcurl_httppost);
  cdecl; external libcurl;



// Se utiliza para que curl lea de un stream
function CurlReadFromStream(Buffer: PAnsiChar; Size, Count: Integer;
  Stream: TStream): Integer; cdecl;
// Se utiliza para que curl escriba en un stream
function CurlSaveToStream(Buffer: PAnsiChar; Size, Count: Integer;
  Stream: TStream): Integer; cdecl;

function GetCurlError(Codigo:CURLcode): String;


implementation

// Se utiliza para que curl lea de un stream
function CurlReadFromStream(Buffer: PAnsiChar; Size, Count: Integer;
  Stream: TStream): Integer; cdecl;
begin
  Result:= Stream.Read(Buffer^,Size*Count) div Size;
end;

// Se utiliza para que curl escriba en un stream
function CurlSaveToStream(Buffer: PAnsiChar; Size, Count: Integer;
  Stream: TStream): Integer; cdecl;
begin
  Result:= Stream.Write(Buffer^,Size*Count) div Size;
end;

//Se utiliza para obtener el error de la libreria Curl y obtener el mensaje
function GetCurlError(Codigo:CURLcode): String;
begin
   Case Codigo of
    CURLE_OK                      : Result:=('Proceso Exitoso');
    CURLE_UNSUPPORTED_PROTOCOL    : Result:=('El protocolo no es soportado por el servidor,  verifique por favor');
    CURLE_FAILED_INIT             : Result:=('Fallo en la inicializaci�n del objeto de conexion curl');
    CURLE_URL_MALFORMAT           : Result:=('El URL no se ha formateado correctamente  ');
    CURLE_OBSOLETE4               : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_COULDNT_RESOLVE_PROXY   : Result:=('No se pudo resolver proxy. El host proxy dado no se pudo resolver .');
    CURLE_COULDNT_RESOLVE_HOST    : Result:=('La ruta a la que deseas establecer conexi�n no esta disponible, verifica si es correcta en la configuracion');
    CURLE_COULDNT_CONNECT         : Result:=('No se puede establecer conexi�n con el servidor ');
    CURLE_FTP_WEIRD_SERVER_REPLY   : Result:=('Respuesta inesperada del servidor FTP');
    CURLE_REMOTE_ACCESS_DENIED     : Result:=('acceso denegado al servidor');
    CURLE_OBSOLETE10               : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_FTP_WEIRD_PASS_REPLY     : Result:=('Respuesta inesperada del servidor FTP');
    CURLE_OBSOLETE12               : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_FTP_WEIRD_PASV_REPLY     : Result:=('No se pudo obtener un resultado razonable desde el servidor como respuesta a un comando o bien un EPSV o PASV. La respuesta del servidor es err�nea.');
    CURLE_FTP_WEIRD_227_FORMAT     : Result:=('El Servidor FTP regreso una l�nea 227 como  respuesta a un comando PASV.');
    CURLE_FTP_CANT_GET_HOST        : Result:=('Error interno al realizar la busqueda del host usado para la nueva conexion');
    CURLE_OBSOLETE16               : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_FTP_COULDNT_SET_TYPE     : Result:=('Se recibi� un error al intentar establecer el modo de transferencia a binario o ASCII');
    CURLE_PARTIAL_FILE             : Result:=('Un Archivo transferido fue mas corto o mas largo de lo esperado ');
    CURLE_FTP_COULDNT_RETR_FILE    : Result:=('Se encontro una respuesta inesperada a un comando RETR o o Una Transferencia Completa de cero bytes.');
    CURLE_OBSOLETE20               : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_QUOTE_ERROR              : Result:=('Uno de los comandos enviados al servidor devuelve un Codigo 400 o mas');
    CURLE_HTTP_RETURNED_ERROR      : Result:=('Problemas con conexion HTTP verifica la conexion o el estado del servidor');
    CURLE_WRITE_ERROR              : Result:=('Error al intentar escrbir los datos recibidos en un archivo local');
    CURLE_OBSOLETE24               : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_UPLOAD_FAILED            : Result:=('Error al Subir el archivo al servidor');
    CURLE_READ_ERROR               : Result:=('Error al leer un archivo local');
    CURLE_OUT_OF_MEMORY            : Result:=('Error de solicitud de asignacion de memoria');
    CURLE_OPERATION_TIMEDOUT       : Result:=('No se recibe respuesta del servidor,  verifica la conexion a internet o si el servidor se encuentra activo.');
    CURLE_OBSOLETE29               : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_FTP_PORT_FAILED          : Result:=('El comando FTP PORT regreso un error');
    CURLE_FTP_COULDNT_USE_REST     : Result:=('El comando FTP REST regreso un error');
    CURLE_OBSOLETE32              : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_RANGE_ERROR             : Result:=('El servidor no soporta ni acepta peticiones de rango');
    CURLE_HTTP_POST_ERROR         : Result:=('Error al enviar datos, verifica la conexion al servidor, o la conexion a internet');
    CURLE_SSL_CONNECT_ERROR       : Result:=('Se produjo un problema en alguna parte del SSL / TLS, Verifica los certificados,rutas, contrase�as o formato de los archivos ');
    CURLE_BAD_DOWNLOAD_RESUME     : Result:=('La descarga no podr�a reanudarse debido a que el desplazamiento especificado estaba fuera de los l�mites del archivo');
    CURLE_FILE_COULDNT_READ_FILE  : Result:=('El archivo no puede ser leido');
    CURLE_LDAP_CANNOT_BIND      : Result:=('Operaci�n de enlace LDAP fall�');
    CURLE_LDAP_SEARCH_FAILED    : Result:=('B�squeda LDAP fall�');
    CURLE_OBSOLETE40            : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_FUNCTION_NOT_FOUND    : Result:=('funci�n no encontrada');
    CURLE_ABORTED_BY_CALLBACK   : Result:=('Anulada por CallBack. Un callback devuelve "abortar" a curl.');
    CURLE_BAD_FUNCTION_ARGUMENT : Result:=('Error Interno. Una funcion fue invocada con los parametros invalidos');
    CURLE_OBSOLETE44            : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_INTERFACE_FAILED    : Result:=('Error de Interface, Uso especifico de una interfaz de salida podria no ser declarado.');
    CURLE_OBSOLETE46          : Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_TOO_MANY_REDIRECTS  : Result:=('Demasiados redireccionamientos. Al seguir redirecciones, se llega a la cantidad m�xima.');
    CURLE_UNKNOWN_TELNET_OPTION: Result:=('Opcion de telnet desconosida');
    CURLE_TELNET_OPTION_SYNTAX : Result:=('Una opcion de telnet no esta correctamente formada');
    CURLE_OBSOLETE50: Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl');
    CURLE_PEER_FAILED_VERIFICATION: Result:=('Certificado SSL o SSH md5 huella digital del servidor remoto se consider� invalida.');
    CURLE_GOT_NOTHING: Result:=('El servidor regreso una respuesta nula o vacia');
    CURLE_SSL_ENGINE_NOTFOUND: Result:=('No se encontr� el motor de cifrado especificado.');
    CURLE_SSL_ENGINE_SETFAILED: Result:=('No se pudo establecer el motor de cifrado SSL seleccionada por defecto');
    CURLE_SEND_ERROR: Result:=('Fall� de envio de datos a la red');
    CURLE_RECV_ERROR: Result:=('Error al recibir datos de la red,verifique la configuraci�n');
    CURLE_OBSOLETE57: Result:=('Una caracter�stica , el protocolo o la funci�n solicitada no se encontr� integrado en esta version de la libreria Curl.');
    CURLE_SSL_CERTPROBLEM: Result:=('Problema con el certificado del cliente local.');
    CURLE_SSL_CIPHER: Result:=('No se puede usar el cifrado especificado.');
    CURLE_SSL_CACERT: Result:=('Peer certificate, no se puede autenticar con certificados de CA conocidos.');
    CURLE_BAD_CONTENT_ENCODING: Result:=('Codificaci�n de transferencia no reconocida.');
    CURLE_LDAP_INVALID_URL: Result:=('URL LDAP Inv�lida.');
    CURLE_FILESIZE_EXCEEDED: Result:=('Tama�o m�ximo de archivo excedido.');
    CURLE_USE_SSL_FAILED: Result:=('El Nivel FTP SSL solicitado es inv�lido.');
    CURLE_SEND_FAIL_REWIND: Result:=('Operacion de retroceso err�nea.');
    CURLE_SSL_ENGINE_INITFAILED: Result:=('Inicializacion del motor SSL Err�nea.');
    CURLE_LOGIN_DENIED: Result:=('Usuario o llaves incorrectas, verifique la configuraci�n. ');
    CURLE_TFTP_NOTFOUND: Result:=('Archivo no encontrado en el servidor.');
    CURLE_TFTP_PERM: Result:=('Problema de permisos en el servidor TFTP.');
    CURLE_REMOTE_DISK_FULL: Result:=('No hay espacio libre en el servidor');
    CURLE_TFTP_ILLEGAL: Result:=('Operaci�n TFTP no v�lida.');
    CURLE_TFTP_UNKNOWNID: Result:=('FTP TransferID desconocida. ');
    CURLE_REMOTE_FILE_EXISTS: Result:=('El archivo ya existe y no puede ser sobreescrito.');
    CURLE_TFTP_NOSUCHUSER: Result:=('Error Desconocido TFTP, CURLE_TFTP_NOSUCHUSER');
    CURLE_CONV_FAILED: Result:=('Error en el proceso de conversi�n de caracteres .');
    CURLE_CONV_REQD: Result:=('La llamada debe registrar las devoluciones de la llamada(Conversion callbacks)');
    CURLE_SSL_CACERT_BADFILE: Result:=('Error leyendo el certificado SSL CA');
    CURLE_REMOTE_FILE_NOT_FOUND: Result:=('Archivo remoto no encontrado');
    CURLE_SSH: Result:=('Error inesperado durante la sesion SSH');
    CURLE_SSL_SHUTDOWN_FAILED: Result:=('Error al  apagar la conexi�n SSL');
    CURLE_AGAIN: Result:=('El socket no esta listo para enviar datos esperar y reintentar');
    CURLE_SSL_CRL_BADFILE: Result:=('Error al cargar el archivo CRL file.');
    CURLE_SSL_ISSUER_ERROR: Result:=('Error en la configuraci�n SSL');
    CURLE_FTP_PRET_FAILED: Result:=('El servidor FTP no soporta el comando PRET o no soporta el argumento enviado.');
    CURLE_RTSP_CSEQ_ERROR: Result:=('Discrepancia de los n�meros CSeq RTSP(Real Time Streaming Protocol)');
    CURLE_RTSP_SESSION_ERROR: Result:=('Error en los identificadores de sesi�n del RTSP(Real Time Streaming Protocol)');
    CURLE_FTP_BAD_FILE_LIST: Result:=('No se puede convertir la lista de archivos FTP');
    CURLE_CHUNK_FAILED: Result:=('Callback Chunk inform� error.');
    CURL_LAST : Result:=('Error No especificado, CURL_LAST')

  else  Result :=('Error  Desconocido, Verifique su conexion a Internet');
        end;


end;




initialization
  curl_global_init(CURL_GLOBAL_ALL);
finalization
  curl_global_cleanup;
end.
