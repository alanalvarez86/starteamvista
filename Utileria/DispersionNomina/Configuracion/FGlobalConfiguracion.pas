unit FGlobalConfiguracion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal, ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,  TressMorado2013, dxSkinsDefaultPainters,
  ImgList, cxButtons;

type
  TGlobalFormato = class(TBaseGlobal_DevEx)
    FormatoNombre: TEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalFormato: TGlobalFormato;

implementation

{$R *.DFM}

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

procedure TGlobalFormato.FormCreate(Sender: TObject);
begin
     inherited;
    IndexDerechos       := ZAccesosTress.D_CONS_REPORTES;
       FormatoNombre.Tag :=  K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_1;
     HelpContext := 0;
end;

procedure TGlobalFormato.FormShow(Sender: TObject);
begin
  inherited;
//  EnviarDetalleTimbrado.SetFocus;
end;

end.
