object dmInterfase: TdmInterfase
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Left = 2285
  Top = 889
  Height = 381
  Width = 676
  object cdsDatosEmpleado: TZetaClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsDatosEmpleadoAlAdquirirDatos
    AlEnviarDatos = cdsDatosEmpleadoAlEnviarDatos
    AlCrearCampos = cdsDatosEmpleadoAlCrearCampos
    AlBorrar = cdsDatosEmpleadoAlBorrar
    AlModificar = cdsDatosEmpleadoAlModificar
    Left = 112
    Top = 48
  end
  object cdsDatosConexion: TZetaClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsDatosConexionAlAdquirirDatos
    Left = 112
    Top = 104
  end
end
