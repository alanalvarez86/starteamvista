program DispersionNomina;

uses
  Forms,
  MidasLib, 
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBaseImportaShell_DevEx in '..\..\Tools\ZBaseImportaShell_DevEx.pas' {BaseImportaShell_DevEx},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  DInterfase in 'DInterfase.pas' {dmInterfase: TDataModule},
  DTablas in '..\..\DataModules\dTablas.pas' {dmTablas: TDataModule},
  DSistema in '..\..\DataModules\dSistema.pas' {dmSistema: TDataModule},
  FSistGlobales_DevEx in '..\..\Sistema\FSistGlobales_DevEx.pas',
  ZetaCommonLists in '..\..\Componentes\ZetaCommonLists.pas',
  ZBaseGlobal_DevEx in '..\..\Tools\ZBaseGlobal_DevEx.pas' {BaseGlobal_DevEx},
  ZBaseDlgModal_DevEx in '..\..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx};

{$R *.res}
{$R WindowsXP.res}
{$R ..\..\Traducciones\Spanish.RES}
begin
  Application.Initialize;
  Application.Title := 'Dispersi�n de N�mina';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
        if Login( False ) then
        begin
             Show;
             Update;
             BeforeRun;
             Application.Run;
        end
        else
        begin
             Free;
        end;
  end;




end.
