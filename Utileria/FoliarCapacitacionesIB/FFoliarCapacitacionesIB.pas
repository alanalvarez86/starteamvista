unit FFoliarCapacitacionesIB;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, DBTables, ExtCtrls,
     ZetaCommonLists,ZetaRegistryServer,ZetaKeyCombo, DB, DBClient,
  ZetaClientDataSet,ZetaCommonTools,ZetaCommonClasses,ZetaServerTools,ZetaASCIIFile;

type
  TAplicarPatch = class(TForm)
    OpenDialog: TOpenDialog;
    DatabaseGB: TGroupBox;
    PanelBotones: TPanel;
    btnSalir: TBitBtn;
    DatabaseFileLBL: TLabel;
    DatabaseFile: TEdit;
    Usuario: TEdit;
    UsuarioLBL: TLabel;
    PasswordLBL: TLabel;
    Password: TEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    txtBitacora: TMemo;
    dbTress: TDatabase;
    dbComparte: TDatabase;
    AliasLBL: TLabel;
    AliasName: TEdit;
    qryTress: TQuery;
    tqCompany: TQuery;
    btnAplicar: TBitBtn;
    btnDetener: TBitBtn;
    tqDrop: TQuery;
    Panel3: TPanel;
    Conexion: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnConectarseClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnAplicarClick(Sender: TObject);
    procedure btnDetenerClick(Sender: TObject);
  private
    FRegistry : TZetaRegistryServer;
    Detener : Boolean;
    FBitacora: TAsciiLog;
    procedure ConectarBD( Database: TDatabase; const sAlias, sUsuario, sPassword: String );
    procedure LoadControls;
    procedure AplicarPatchPorBasedeDatos;
    procedure CargarEmpresas;
    procedure AddBitacora(Linea: string);
    procedure DespreparaQuery(Query: TQuery);
    procedure EjecutaDDL( BaseDatos: TDatabase; Query: TQuery; const sSQL: String );
    procedure LogInit;
    procedure LogEnd;

    { Private declarations }
  public
    { Public declarations }
    property Registry: TZetaRegistryServer read FRegistry;
    function GetDatabase: String;
    function GetUsuario: String;
    function GetPassword: String;
  end;

function EscogerBaseDeDatos: Boolean;

implementation

{$R *.DFM}

USES
    ZetaDialogo;
var
  AplicarPatch: TAplicarPatch;

const
     K_QueryAlter =   ' ALTER procedure SET_KARCURSO_CONTADOR( Anio Smallint, FolioInicial Integer )   '+ CR_LF+
                      ' as                                                                             '+CR_LF+
	' declare variable Contador integer;                                                           '+ CR_LF+
	' declare variable Curso char(6);                                                              '+CR_LF+
	' declare variable Patron varchar(11);                                                         '+CR_LF+
	' declare variable Establecimiento varchar(6);                                                 '+CR_LF+
	' declare variable Fecha Date;                                                            '+CR_LF+
' begin                                                                                                '+CR_LF+
'	Contador = :FolioInicial - 1;                                                                     '+CR_LF+
'	update KARCURSO set KC_NO_STPS = 0 where extract( year from KC_FEC_TOM)= :Anio;               '+CR_LF+
'                                                                                                     '+CR_LF+
'	for                                                                                           '+CR_LF+
'		select CU_CODIGO, KC_FEC_TOM, CB_PATRON, KC_EST                                       '+CR_LF+
'		from CONTA_KC K                                                                       '+CR_LF+
'		where theYear= :Anio                                                                  '+CR_LF+
'		group by  K.CU_CODIGO,                                                                '+CR_LF+
'			K.KC_FEC_TOM,                                                                 '+CR_LF+
'			K.CB_PATRON,                                                                  '+CR_LF+
'			K.KC_EST                                                                      '+CR_LF+
'		order by  K.CU_CODIGO,                                                                '+CR_LF+
'			K.KC_FEC_TOM,                                                                 '+CR_LF+
'			K.CB_PATRON,                                                                  '+CR_LF+
'			K.KC_EST                                                                      '+CR_LF+
'		into Curso, Fecha, Patron, Establecimiento do                                         '+CR_LF+
'	begin                                                                                         '+CR_LF+
'		Contador = Contador + 1 ;                                                             '+CR_LF+
'		update KARCURSO                                                                       '+CR_LF+
'			set KC_NO_STPS = :Contador                                                    '+CR_LF+
'		where extract( year from KC_FEC_TOM)=:Anio                                            '+CR_LF+
'		and CU_CODIGO = :Curso                                                                '+CR_LF+
'		and KC_FEC_TOM = :Fecha                                                               '+CR_LF+
'		and ( select CB_PATRON from SP_FECHA_KARDEX(KARCURSO.KC_FEC_TOM, KARCURSO.CB_CODIGO))=:Patron '+CR_LF+
'		and KC_EST = :Establecimiento;                                                                '+CR_LF+
'                                                                                                             '+CR_LF+
'	end                                                                                                   '+CR_LF+
'                                                                                                             '+CR_LF+
'	suspend;                                                                                              '+CR_LF+
'end ';

  K_RELLENO = 10;

function EscogerBaseDeDatos: Boolean;
begin
     if not Assigned( AplicarPatch ) then
        AplicarPatch := TAplicarPatch.Create( Application );
     with AplicarPatch do
     begin
          ShowModal;
     end;
end;

{ ************ TEscogerAlias ************** }

procedure TAplicarPatch.LogInit;
begin
     txtBitacora.Lines.Clear;
     with FBitacora do
     begin
          if not Used then
          begin
               with Application do
               begin
                    Init( ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + Title + '.log' );
               end;
          end;
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ) );
          txtBitacora.Lines.Add( Format( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ), [ FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now )]  ) );
     end;
end;

procedure TAplicarPatch.LogEnd;
begin
     with FBitacora do
     begin
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Final %s ' + StringOfChar( '*', K_RELLENO ) );
          Close;
     end;
end;

procedure TAplicarPatch.FormCreate(Sender: TObject);
begin
     inherited;
     FRegistry := TZetaRegistryServer.Create;
     FBitacora := TAsciiLog.Create;
     Detener := False;
end;

procedure TAplicarPatch.FormDestroy(Sender: TObject);
begin
     FRegistry.Free;
     FBitacora.Free;
     inherited;
end;

procedure TAplicarPatch.FormShow(Sender: TObject);
begin
     //ActiveControl := DatabaseFile;
     LoadControls;
end;

procedure TAplicarPatch.LoadControls;
begin
     with Registry do
     begin
          Self.AliasName.Text := AliasComparte;
          Self.DatabaseFile.Text := Database;
          Self.Usuario.Text := UserName;
          Self.Password.Text := Password;
     end;
     try
        ConectarBD(dbComparte,AliasName.Text,Usuario.Text,Password.Text);
        if dbComparte.Connected Then
        begin
             Conexion.Caption := 'Conexi�n Exitosa a BD Comparte';
             btnAplicar.Enabled := True;
             btnDetener.Enabled := False;
        end
        else
            Conexion.Caption := 'Fall� conexi�n a BD Comparte , Configurar el acceso con TressCfg ';
     except
           on Error: Exception do
           begin
                Conexion.Caption := 'Fall� conexi�n a BD Comparte , Configurar el acceso con TressCfg : '+Error.Message;
                btnAplicar.Enabled := False;
                btnDetener.Enabled := False;
                //Application.HandleException( Error );
           end;
     end;
end;

function TAplicarPatch.GetDatabase: String;
begin
     Result := DatabaseFile.Text;
end;

function TAplicarPatch.GetUsuario: String;
begin
     Result := Usuario.Text;
end;

function TAplicarPatch.GetPassword: String;
begin
     Result := Password.Text;
end;

procedure TAplicarPatch.ConectarBD(Database: TDatabase; const sAlias,sUsuario, sPassword: String);
begin
     with Database do
     begin
          Connected := False;
          AliasName := sAlias;

          with Params do
          begin
               Values[ 'USER NAME' ] := sUsuario;
               Values[ 'PASSWORD' ] :=  sPassword ;
          end;
          Connected := True;
     end;
end;

procedure TAplicarPatch.btnConectarseClick(Sender: TObject);
begin
     //ConectarBD(dbComparte,AliasName.Text,Usuario.Text,Password.Text);
end;



procedure TAplicarPatch.btnAplicarClick(Sender: TObject);
begin
     Detener := False;
     btnAplicar.Enabled := False;
     CargarEmpresas;
end;

procedure TAplicarPatch.CargarEmpresas;
begin
     with tqCompany do
     begin
          LogInit;
          Active := True;
          while not Eof and not Detener do
          begin
               AplicarPatchPorBasedeDatos;
               Next;
          end;
          Active := False;
          LogEnd;
          ZetaDialogo.ZInformation('Foliar Capacitaciones STPS','Se ha terminado de Aplicar Patch a Base de Datos',0);
          btnAplicar.Enabled := True;
          btnDetener.Enabled := False;
     end;
end;

procedure TAplicarPatch.AplicarPatchPorBasedeDatos;
begin
      with tqCompany do
      begin
           try
              AddBitacora('--------------------------------------------------');
              AddBitacora(Format('Conectando a Base de Datos: %s',[FieldByName('CM_DATOS').AsString]));
              ConectarBD(dbTress,FieldByName('CM_ALIAS').AsString,FieldByName('CM_USRNAME').AsString,ZetaServerTools.Decrypt( FieldByName('CM_PASSWRD').AsString));
              if dbTress.Connected then
              begin
                   AddBitacora(Format('   Conexi�n Exitosa a Base de Datos: %s ',[FieldByName('CM_DATOS').AsString]));
                   try
                      AddBitacora('   Aplicando Patch a Foliar Capacitaciones STPS...');
                      dbTress.StartTransaction;
                      EjecutaDDL(dbTress,qryTress,K_QueryAlter);
                      dbTress.Commit;
                      AddBitacora('   Patch Aplicado');
                   except
                         on Error: Exception do
                         begin
                              AddBitacora(Format ('   Fall� Al Aplicar Patch: %s',[Error.Message]));
                         end;
                   end;
              end
              else
                  AddBitacora(Format ('Fall� Conexi�n: Alias:%s Datos:%s',[FieldByName('CM_ALIAS').AsString,FieldByName('CM_DATOS').AsString]));
          except
                on Error: Exception do
                begin
                     AddBitacora(Format ('Fall� Conexi�n: Alias:%s Datos:%s Error: %s',[FieldByName('CM_ALIAS').AsString,FieldByName('CM_DATOS').AsString,Error.Message]));
                     //Application.HandleException( Error );
                end;
          end;
     end;
end;

procedure TAplicarPatch.EjecutaDDL( BaseDatos: TDatabase; Query: TQuery; const sSQL: String );

function BorraComentarios: String;
var
   iStart, iEnd: Integer;
   lLoop: Boolean;
begin
     Result := sSQL;
     lLoop := True;
     while lLoop do
     begin
          iStart := Pos( '/*', Result );
          if ( iStart > 0 ) then
          begin
               iEnd := Pos( '*/', Result );
               if ( iEnd = 0 ) then
                  Result := Copy( Result, 1, ( iStart - 1 ) )
               else
               begin
                    iEnd := iEnd + 3;
                    Result := Copy( Result, 1, ( iStart - 1 ) ) + Copy( Result, iEnd, ( Length( Result ) - iEnd + 1 ) );
               end;
          end
          else
              lLoop := False;
     end;
end;

begin
     DespreparaQuery( Query );
     with Query do
     begin
          DatabaseName := BaseDatos.DatabaseName;
          ParamCheck := False;
          with SQL do
          begin
               Clear;
               Text := BorraComentarios;
          end;
          ExecSQL;
     end;
end;


procedure TAplicarPatch.DespreparaQuery( Query: TQuery );
begin
     with Query do
     begin
          Active := False;
          while FieldCount > 0 do
          begin
               Fields[ FieldCount - 1 ].DataSet := nil;
          end;
          FieldDefs.Clear;
          if Prepared then
             Unprepare;
     end;
end;

procedure TAplicarPatch.AddBitacora(Linea:string);
begin
     txtBitacora.Lines.Add(Linea);
     FBitacora.WriteTexto(Linea);
     //WriteLn( Linea );
end;

procedure TAplicarPatch.btnDetenerClick(Sender: TObject);
begin
     Detener := True;
     btnAplicar.Enabled := True;
end;

end.
