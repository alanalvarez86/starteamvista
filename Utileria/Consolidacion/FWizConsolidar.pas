unit FWizConsolidar;

interface

uses
 ZetaCommonClasses, Forms, ZetaFecha, DConsolida, ZetaServerTools, DBasicoCliente, IniFiles,
 dxWizardControlForm, TressMorado2013, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
 dxSkinsCore, cxContainer, cxEdit, cxProgressBar, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel,
 dxCustomWizardControl, dxWizardControl, System.Classes, Vcl.Controls, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Data.DB, cxCheckListBox, Vcl.ImgList, ZetaClientDataSet, Dialogs,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxShellBrowserDialog, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxSkinscxPCPainter, cxMemo, cxCurrencyEdit, cxRadioGroup, Vcl.ComCtrls,
  dxCore, cxDateUtils, cxCalendar, Vcl.Mask, cxGroupBox, ZetaCXGrid;

type
  TWizConsolidar = class(TdxWizardControlForm)
    dxWizardControl: TdxWizardControl;
    pgFinProceso: TdxWizardControlPage;
    pgBasesDatos: TdxWizardControlPage;
    DataSource: TDataSource;
    ImageButtons: TcxImageList;
    cxOpenDialog: TcxShellBrowserDialog;
    DataSourceAlAcumular: TDataSource;
    luBDDestino: TZetaKeyLookup_DevEx;
    pgConfigurar: TdxWizardControlPage;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    pgEjecutar: TdxWizardControlPage;
    lblAcumulados: TcxLabel;
    lblBitacora: TLabel;
    txtBitacora: TcxTextEdit;
    btnBitacora: TcxButton;
    lblConfidencialidad: TcxLabel;
    txtAcumulados: TcxCurrencyEdit;
    pgBDConsolidar: TdxWizardControlPage;
    lblConsolidadora: TcxLabel;
    clbBDFuente: TcxCheckListBox;
    cxLabel11: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel12: TcxLabel;
    fecInicial: TZetaFecha;
    fecFinal: TZetaFecha;
    chbBitacora: TcxCheckBox;
    lblTarjetas: TcxLabel;
    lblBorrar: TcxLabel;
    pgNuevaBaseDatos: TdxWizardControlPage;
    GrupoAvance: TcxGroupBox;
    cxMemoStatus: TcxMemo;
    cxMemoPasos: TcxMemo;
    chbBorrar: TCheckBox;
    pnlResumen: TPanel;
    btnVerBitacora: TcxButton;
    lblResumen: TcxLabel;
    lblEstatus: TcxLabel;
    pbProgreso: TcxProgressBar;
    pgSumarAlAcumular: TdxWizardControlPage;
    gridBDConsolidar: TZetaCXGrid;
    gridBDConsolidarDBTableView: TcxGridDBTableView;
    colBaseDatos: TcxGridDBColumn;
    colDescripcion: TcxGridDBColumn;
    colEmpate: TcxGridDBColumn;
    gridBDConsolidarLevel: TcxGridLevel;
    lblBaseDatos: TcxLabel;
    lblInstruccion: TcxLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure btnBitacoraClick(Sender: TObject);
    procedure btnVerBitacoraClick(Sender: TObject);
    procedure chbBitacoraClick(Sender: TObject);
    procedure agregarGridBDConsolidar;
    procedure gridBDConsolidarDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
  private
    { Private declarations }
    Mutex      : THandle;
    fAvances   : TList;
    oParametros: TZetaParams;
    FBDConsolidadora: TCompanyData;
    FEmpresas: TCompanyList;
    FLimiteEmpleados: Integer;
    FParameterList: TZetaParams;
    FHayError: Boolean;
    FErrores: Integer;
    FProcesoCancelado: Boolean;
    function Processing: Boolean;
    procedure HabilitaBotones;
    function ConsolidarBasesDatos (out mensaje: string): Boolean;
    function CallMeBack( const sMensaje: String; iValor: Integer): Boolean;
    function Warning( const sError: String; oControl: TWinControl ): Boolean;
    procedure CargarBasesDatosFuente (sDBConsolidadora: String);
    procedure CargarBasesDatosConsolidar;
    procedure GuardarInformacionINI;
    function HayBDFuenteSeleccionada: Boolean;
    procedure StartProcess;
  public
    { Public declarations }
    property HayError: Boolean read FHayError write FHayError;
    property ProcesoCancelado: Boolean read FProcesoCancelado write FProcesoCancelado;
    property LimiteEmpleados: Integer read FLimiteEmpleados write FLimiteEmpleados;

    procedure ClearCounter( Sender: TObject; var Continue: Boolean );
  protected
    FSiATodos: Boolean;
    // FParamMsg: TStrings;
    property ParameterList: TZetaParams read FParameterList;
    procedure SetStatus (lTerminado: Boolean);
    function HayErrores: Boolean;
    procedure EnableStopOnError;
    function PuedeContinuar: Boolean;  virtual;
  end;

Const
     K_TERMINADO='  Terminado';
     K_ERROR='  Error';
     K_TAM_BD_300_EMPLEADOS = 629; // Tama�o del archivo de datos para una base de datos de 300 empleados (c�lculo que se hace al crear
                                     // una base de datos desde Tress
     K_ARCHIVO_INI = 'ConsolidacionTRESS.INI';
var
  WizConsolidar: TWizConsolidar;

implementation

uses
  SysUtils, Windows, DMotorPatch, MotorPatchUtils, MotorPatchThreads, DBClient, ZetaCommonLists, ShellAPI,
  DSistema, ZetaDialogo, ZetaCommonTools;

{$R *.dfm}

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[0..79] of Char;
begin
     Result := ShellExecute( Application.MainForm.Handle,
                             nil,
                             StrPCopy( zFileName, FileName ),
                             StrPCopy( zParams, Params ),
                             StrPCopy( zDir, DefaultDir ),
                             ShowCmd );
end;

{ TWizConsolidar }

procedure TWizConsolidar.FormCreate(Sender: TObject);
begin
    HelpContext := 5;
    // FParamMsg := TStringList.Create;
    Mutex := CreateMutex(nil, False, PChar(ClassName + '.Execute'));

    if Mutex = 0 then
        RaiseLastOSError;

    fAvances := TList.Create;
    FEmpresas := TCompanyList.Create;
    FParameterList := TZetaParams.Create;

end;

procedure TWizConsolidar.FormClose(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not Processing;
end;

procedure TWizConsolidar.FormDestroy(Sender: TObject);
begin
    FreeAndNil(oParametros);
    FreeAndNil(fAvances);
    CloseHandle(Mutex);
    FEmpresas.Free;
    FParameterList.Free;
    // FParamMsg.Free;
end;

procedure TWizConsolidar.FormShow(Sender: TObject);
begin
     with dmSistema do
     begin
          cdsSistBaseDatos.Conectar;
          cdsSistBaseDatosLookup.Conectar;
          DataSource.DataSet := cdsSistBaseDatos;
          luBDDestino.LookupDataset := cdsSistBaseDatosLookUp;
     end;

     txtBitacora.Text := Format( '%sConsolidacion.txt', [ ExtractFilePath( Application.ExeName ) ] );
     luBDDestino.SetFocus;

     // A�o actual
     txtAcumulados.Value := TheYear( Now );

     // Primer d�a del a�o
     fecInicial.Valor := FirstDayOfYear(TheYear( Now ));

     // �ltimo d�a del a�o
     fecFinal.Valor := LastDayOfYear(TheYear( Now ));

     //Desactiva la posibilidad de agrupar
    gridBDConsolidarDBTableView.OptionsCustomize.ColumnGrouping := False;
    //Esconde la caja de agrupamiento
    gridBDConsolidarDBTableView.OptionsView.GroupByBox := False;
    //Para que nunca muestre el filterbox inferior
    gridBDConsolidarDBTableView.FilterBox.Visible := fvNever;
    //Para que no aparezca el Custom Dialog
    gridBDConsolidarDBTableView.FilterBox.CustomizeDialog := False;
    //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
    // gridBDConsolidarDBTableView.ApplyBestFit();
end;

procedure TWizConsolidar.gridBDConsolidarDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

function TWizConsolidar.Processing: Boolean;
begin
    Result := (lblEstatus.Caption <> 'Estatus') and
        (Pos ('terminada', lblEstatus.Caption) = 0);
    {(lblEstatus.Caption <> 'Consolidaci�n terminada')
              and (lblEstatus.Caption <> 'Consolidaci�n terminada con errores');}
end;

procedure TWizConsolidar.HabilitaBotones;
begin
  with dxWizardControl.Buttons do
  begin
       Next.Enabled := not Processing;
       Back.Enabled := not Processing;
  end;
end;

procedure TWizConsolidar.btnBitacoraClick(Sender: TObject);
var sFileName: String;
begin
      sFileName := StrDef (ExtractFileName(txtBitacora.Text), 'Consolidacion.txt');
      cxOpenDialog.Path := ExtractFilePath( Application.ExeName );
      if cxOpenDialog.Execute then
        txtBitacora.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
end;

procedure TWizConsolidar.btnVerBitacoraClick(Sender: TObject);
begin
    ShellExecute(0, nil, 'notepad', PChar(txtBitacora.Text), nil, SW_SHOWNORMAL);
end;

procedure TWizConsolidar.dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
const K_ESPACIO = ' ';
var
  mensaje : string;
  oCursor: TCursor;
  bEjecucionNuevaBD: Boolean;
begin
  bEjecucionNuevaBD := FALSE;
  case AKind of
    wcbkNext:
      begin

        if (dxWizardControl.ActivePage = pgBasesDatos) then
        begin
            if luBDDestino.Llave = VACIO then
            begin
                AHandled := Warning('El campo Base de datos no puede quedar vac�o.', luBDDestino);
            end
            else
            begin
                lblConsolidadora.Caption := 'Base de datos de consolidaci�n: ' + luBDDestino.Llave + ': ' + luBDDestino.Descripcion;
                pgNuevaBaseDatos.Enabled := FALSE;
                pgNuevaBaseDatos.PageVisible := FALSE;
                CargarBasesDatosFuente (luBDDestino.Llave );
            end;

        end;

        if (dxWizardControl.ActivePage = pgBDConsolidar) then
        begin
            if not HayBDFuenteSeleccionada then
            begin
                AHandled := Warning('Debe seleccionar al menos una base de datos fuente.', clbBDFuente);
            end
            else
            begin
                // Primero cargar las bases de datos a consolidar en grid para determinar el valor al acumular
                agregarGridBDConsolidar;
                // Cargar bases de datos a Consolidar en objeto FEmpresas;
                // CargarBasesDatosConsolidar; // -> Se hace en el siguiente paso. Debe cargarse el valor Empate.
            end;
        end;

        if (dxWizardControl.ActivePage = pgSumarAlAcumular) then
        begin
            // Cargar bases de datos a Consolidar en objeto FEmpresas;
            CargarBasesDatosConsolidar;
        end;

        if (dxWizardControl.ActivePage = pgConfigurar) then
        begin
            lblAcumulados.Caption := 'Acumulados de n�mina:  ' + txtAcumulados.Text;
            lblTarjetas.Caption := 'Tarjetas desde:  ' + FechaCorta (fecInicial.Valor) + ' al ' + FechaCorta (fecFinal.Valor);
            lblBorrar.Caption := 'Borrar tablas en base consolidada:  ' + BoolAsSiNo (chbBorrar.Checked);
        end;

        if (dxWizardControl.ActivePage = pgEjecutar) then
        begin
            if StrVacio (ExtractFileName (txtBitacora.Text)) then
                AHandled := Warning('Debe proporcionar un nombre de archivo de bit�cora.',  txtBitacora)
            else
            begin
                pnlResumen.Visible := FALSE;
                pbProgreso.Position := 0;
                Application.ProcessMessages;
                dxWizardControl.ActivePage := pgFinProceso;
                oCursor := Screen.Cursor;
                try
                    // Poder cancelar proceso de Consolidaci�n
                    // dxWizardControl.Enabled := FALSE;
                    // ----- -----

                    // oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;

                    // Guardar informaci�n en archivo INI
                    GuardarInformacionINI;

                    // Ejecutar proceso de consolidaci�n de bases de datos
                    if ConsolidarBasesDatos (mensaje) then
                        pnlResumen.Visible := TRUE;
                    // Fin de Proceso
                finally
                    dxWizardControl.Enabled := TRUE;
                    Screen.Cursor := oCursor;
                end;
            end;
        end;

      end;

    wcbkCancel:
      begin
        if not Processing then
          ModalResult := mrCancel
      end;

    wcbkFinish:
    begin


        // if (dxWizardControl.ActivePage = pgFinProceso) then
        // begin
            // AHandled := Warning('�Desea cancelar el proceso de Consolidaci�n?',  txtBitacora)
            if (Processing) then
            begin
                oCursor := Screen.Cursor;
                try
                    Screen.Cursor := crUpArrow;
                    if (ZetaDialogo.ZWarningConfirm(Caption, '�Desea cancelar el proceso de Consolidaci�n?', 0, mbYes)) then
                    begin
                        FProcesoCancelado := TRUE;
                        dmConsolida.ParamMsg.Add( '*** Proceso interrumpido por el usuario ***' );
                    end;
                finally
                    Screen.Cursor := oCursor;
                end;
            end
        // end
        else
            ModalResult := mrOk;
    end;

  end;

  if not bEjecucionNuevaBD then
      HabilitaBotones()
  else
  begin
      with dxWizardControl.Buttons do
      begin
           // dxWizardControl.ActivePage <> pgNuevaBaseDatos then
           Next.Enabled := (dxWizardControl.ActivePage <> pgNuevaBaseDatos);
           Back.Enabled := TRUE;
      end;
  end;

end;

procedure TWizConsolidar.dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage;
  var AAllow: Boolean);
begin
  if ANewPage = pgEjecutar then
    dxWizardControl.Buttons.Next.Caption := 'Aplicar'
  else
    dxWizardControl.Buttons.Next.Caption := 'Siguiente';
end;

function TWizConsolidar.ConsolidarBasesDatos (out mensaje: string): Boolean;
const K_NUM_TABLAS_CONSOLIDAR = 9;
var
   lOk : Boolean;
   FLog: TTransferLog;
begin
     try
         FLog := TTransferLog.Create;
         try
              if StrVacio(ExtractFileExt(txtBitacora.Text)) then
                  FLog.Start( txtBitacora.Text + '.txt' )
              else
                FLog.Start( txtBitacora.Text );

              lblEstatus.Caption := 'Empezando consolidaci�n de bases de datos';

              // Ajustar m�ximo de la barra de progreso a la cantidad de bases de datos a consolidar por el n�mero de tablas que
              // se consolidan (K_NUM_TABLAS_CONSOLIDAR);
              pbProgreso.Properties.Max := (FEmpresas.Count * K_NUM_TABLAS_CONSOLIDAR) + 1;

              // Si se borrar� en base consolidadora, agregar un paso m�s a pbProgreso
              if chbBorrar.Checked then
                pbProgreso.Properties.Max := pbProgreso.Properties.Max + 1;

              // Iniciar Procesos
              StartProcess;

              EnableStopOnError; // ??

              // Ejecutar proceso de dmConsolida.ConsolidarBasesDatos
              // bConsolidarBasesDatos :=
              dmConsolida.ConsolidarBasesDatos (CallMeBack, FLog, FBDConsolidadora.Nombre, FEmpresas, FBDConsolidadora,
                  StrToInt (txtAcumulados.Text), fecInicial.Valor, fecFinal.Valor, chbBorrar.Checked, LimiteEmpleados);

              lOk := True;
         finally
            with FLog do
            begin
                 Close;
                 Free;
            end;
         end;
     except
         on e: Exception do
         begin
              ZetaDialogo.ZError('Error', mensaje, 0);
              lOk := FALSE;
         end;
     end;

     Result := lOk;
end;

function TWizConsolidar.CallMeBack ( const sMensaje: String; iValor: Integer): Boolean;
begin
     lblEstatus.Caption := sMensaje;
     pbProgreso.Position := pbProgreso.Position + iValor;
     Application.ProcessMessages;

     Result := PuedeContinuar;
     // Result := TRUE; // original
end;

procedure TWizConsolidar.chbBitacoraClick(Sender: TObject);
begin
    txtBitacora.Enabled := chbBitacora.Checked;
    btnBitacora.Enabled := chbBitacora.Checked;
end;

function TWizConsolidar.Warning( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZWarning( Caption, sError, 0, mbOK );
     oControl.SetFocus;
     Result := TRUE;
end;

procedure TWizConsolidar.CargarBasesDatosFuente (sDBConsolidadora: String);
var
   appINI : TIniFile;
   i, y: Integer;
   slBDFuente: TStringList;
begin
    appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
    // Definir aqu� la base de datos consolidadora
    if strLleno (sDBConsolidadora) then
    begin
        FBDConsolidadora := TCompanyData.Create;
        with dmSistema.cdsSistBaseDatos do
        begin
            Locate('DB_CODIGO', sDBConsolidadora, []);
            with FBDConsolidadora do
            begin
                Codigo := FieldByName( 'DB_CODIGO' ).AsString;
                Nombre := FieldByName( 'DB_DESCRIP' ).AsString;
                Alias := '';
                UserName := FieldByName( 'DB_USRNAME' ).AsString;
                // Password := ZetaServerTools.Decrypt( FieldByName( 'DB_PASSWRD' ).AsString );
                Password := FieldByName('DB_PASSWRD').AsString;
                Empate := 0; // �Se usa este valor? Consultar.
                Datos := FieldByName ('DB_DATOS').AsString;
            end;
        end;
    end;

    // Cargar bases de datos fuente como lista de check box
    clbBDFuente.Items.Clear;
    with dmSistema.cdsSistBaseDatos do
    begin
        First;
        while not Eof do
        begin
            if (UpperCase (sDBConsolidadora) <> UpperCase (FieldByName('DB_CODIGO').AsString))
            and
            (FieldByName('DB_TIPO').AsInteger in [Ord (tc3Datos), Ord (tc3Prueba), Ord (tcPresupuesto)]) then
            begin
                // �Mostrar solo C�digo o mostrar C�digo y Descripci�n de bases de datos?
                // (clbBDFuente.Items.Add).Text := FieldByName('DB_CODIGO').AsString;
                (clbBDFuente.Items.Add).Text := FieldByName('DB_CODIGO').AsString + ': ' + FieldByName('DB_DESCRIP').AsString;
            end;
            Next;
        end;
    end;

    try
        slBDFuente := TStringList.Create;
        slBDFuente.CommaText := appINI.ReadString(FBDConsolidadora.Codigo,'ORIGEN', '') ;

        for y := 0 to slBDFuente.Count-1 do
        begin
            for i := 0 to clbBDFuente.Items.Count-1 do
            begin
                if Copy(clbBDFuente.Items[i].Text, 0, Pos (': ', clbBDFuente.Items[i].Text)-1)
                      = slBDFuente.Strings[y] then
                begin
                    clbBDFuente.Items[i].Checked := TRUE;
                end;
            end;
        end;
    finally
        appINI.Free;
    end;
end;

procedure TWizConsolidar.CargarBasesDatosConsolidar;
begin
     // Cargar FEmpresas con las Bases de datos seleccionadas a Consolidar
     FEmpresas.Clear;

     DataSourceAlAcumular.DataSet.First;
     while not DataSourceAlAcumular.DataSet.Eof do
     begin
          with dmSistema.cdsSistBaseDatos do
          begin
              Locate('DB_CODIGO', DataSourceAlAcumular.DataSet.FieldByName('BaseDatos').AsString, []);
              with FEmpresas.AgregaEmpresa do
              begin
                  Codigo := FieldByName( 'DB_CODIGO' ).AsString;
                  Nombre := FieldByName( 'DB_DESCRIP' ).AsString;
                  Alias := '';
                  UserName := FieldByName( 'DB_USRNAME' ).AsString;
                  Password := FieldByName('DB_PASSWRD').AsString;
                  Empate := DataSourceAlAcumular.DataSet.FieldByName('Empate').AsInteger;
                  Datos := FieldByName ('DB_DATOS').AsString;
              end;
          end;
          DataSourceAlAcumular.DataSet.Next;
     end;
end;

procedure TWizConsolidar.GuardarInformacionINI;
var appINI : TIniFile;
   i: Integer;
   sBDOrigen: String;
begin
      sBDOrigen := '';
      appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.INI')) ;
      try

         for i := 0 to FEmpresas.Count-1 do
         begin
              if not StrLleno (sBDOrigen) then
                sBDOrigen := FEmpresas.Empresa[i].Codigo
              else
                sBDOrigen := sBDOrigen + ',' + FEmpresas.Empresa[i].Codigo;
              // De cada base de datos origen, colocar su secci�n y llave para valor al acumular (EMPATE)
              appIni.WriteInteger(FEmpresas.Empresa[i].Codigo, 'EMPATE', FEmpresas.Empresa[i].Empate);
         end;

         appIni.WriteString(FBDConsolidadora.Codigo, 'ORIGEN', sBDOrigen);
      finally
        appINI.Free;
      end;
end;

function TWizConsolidar.HayBDFuenteSeleccionada: Boolean;
var i: Integer;
begin
    Result := FALSE;

    for i := 0 to clbBDFuente.Items.Count-1 do
    begin
        if clbBDFuente.Items[i].Checked then
        begin
            Result := TRUE;
            Break;
        end;
    end;
end;

procedure TWizConsolidar.SetStatus (lTerminado: Boolean);
begin
     if lTerminado then
          cxMemoStatus.Lines.Append( K_TERMINADO)
     else
          cxMemoStatus.Lines.Append( K_ERROR);
end;

procedure TWizConsolidar.agregarGridBDConsolidar;
var cdsGridBDConsolidar : TZetaClientDataSet;
    i : Integer;
    appINI : TIniFile;
begin
     appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
     cdsGridBDConsolidar := TZetaClientDataSet.Create(Self);
     with cdsGridBDConsolidar do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          AddStringField('BaseDatos', 20);
          AddStringField('Descripcion', 45);
          AddIntegerField('Empate');
          CreateTempDataset;
          Open;
     end;

     try
         for i:=0 to clbBDFuente.Items.Count-1 do
         begin
            if clbBDFuente.Items[i].Checked then
            begin
                cdsGridBDConsolidar.Insert;
                cdsGridBDConsolidar.FieldByName('BaseDatos').AsString :=
                    Copy(clbBDFuente.Items[i].Text, 0, Pos (': ', clbBDFuente.Items[i].Text)-1);
                cdsGridBDConsolidar.FieldByName('Descripcion').AsString :=
                    Copy (clbBDFuente.Items[i].Text, Pos (': ', clbBDFuente.Items[i].Text)+2, Length(clbBDFuente.Items[i].Text)-1);
                // cdsGridBDConsolidar.FieldByName('Empate').AsInteger := 0;
                // Obtener valor al acumular de cada Base de Datos.
                cdsGridBDConsolidar.FieldByName('Empate').AsInteger :=
                      appINI.ReadInteger(cdsGridBDConsolidar.FieldByName('BaseDatos').AsString,'EMPATE', 0) ;
                cdsGridBDConsolidar.Post;
            end;
         end;
     finally
        appINI.Free;
     end;
     DataSourceAlAcumular.DataSet := cdsGridBDConsolidar;
     gridBDConsolidarDBTableView.DataController.DataSource := DataSourceAlAcumular;

end;

procedure TWizConsolidar.EnableStopOnError;
begin
     FSiATodos := False;
end;

procedure TWizConsolidar.ClearCounter( Sender: TObject; var Continue: Boolean );
begin
     Continue := PuedeContinuar;
end;

procedure TWizConsolidar.StartProcess;
begin
     FHayError := False;
     FProcesoCancelado := False;
     FErrores := 0;
     FSiATodos := True;

     with dmConsolida do
     begin
          EndCallBack := Self.ClearCounter;
     end;
end;

function TWizConsolidar.HayErrores: Boolean;
begin
     with dmConsolida do
     begin
          if ErrorFound and ( ErrorCount > FErrores ) then
          begin
               FErrores := ErrorCount; { Evita que se llame otra vez por el mismo error }
               Result := True;
          end
          else
              Result := False;
     end;
end;

function TWizConsolidar.PuedeContinuar: Boolean;
begin
     Result := not FProcesoCancelado;
     {if Result then
     begin
          if not FSiATodos and HayErrores then
          begin
             case SiNoSiTodos of
                  mrCancel:
                  begin
                       Result := False;
                       // FParamMsg.Add( '*** Proceso interrumpido por el usuario ***' );
                  end;
                  mrAll: FSiATodos := True;
             end;
          end;
     end;}
end;

end.
