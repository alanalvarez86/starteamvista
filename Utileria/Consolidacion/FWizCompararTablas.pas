unit FWizCompararTablas;

interface

uses
 ZetaCommonClasses, Forms, dxWizardControlForm, TressMorado2013, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
 dxSkinsCore, cxContainer, cxEdit, cxProgressBar, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel,
 dxCustomWizardControl, dxWizardControl, System.Classes, Vcl.Controls, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Data.DB, cxCheckListBox, Vcl.ImgList, ZetaClientDataSet, Dialogs,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxShellBrowserDialog, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxSkinscxPCPainter, ZetaCXGrid;

const
    K_DEFAULT_BIT = 'ComparaTablas.txt';

type
  TWizCompararTablas = class(TdxWizardControlForm)
    dxWizardControl: TdxWizardControl;
    pgFinProceso: TdxWizardControlPage;
    pgBasesDatos: TdxWizardControlPage;
    lblFuente: TcxLabel;
    lblDestino: TcxLabel;
    DataSource: TDataSource;
    pgTablasComparar: TdxWizardControlPage;
    btnSeleccionarTodos: TcxButton;
    btnDesmarcarTodas: TcxButton;
    ImageButtons: TcxImageList;
    pgResumen: TdxWizardControlPage;
    gridTablasCatalogosDBTableView: TcxGridDBTableView;
    gridTablasCatalogosLevel: TcxGridLevel;
    gridTablasCatalogos: TcxGrid;
    lblBitacora: TLabel;
    txtBitacora: TcxTextEdit;
    btnBitacora: TcxButton;
    cxOpenDialog: TcxShellBrowserDialog;
    DataSourceGrid: TDataSource;
    Entidad: TcxGridDBColumn;
    pbPreparacion: TcxProgressBar;
    cxLabel1: TcxLabel;
    luBDDestino: TZetaKeyLookup_DevEx;
    luBDFuente: TZetaKeyLookup_DevEx;
    lblEstatus: TcxLabel;
    pbDetalle: TcxProgressBar;
    lblEstatusHeader: TcxLabel;
    pbAvance: TcxProgressBar;
    pnlResumen: TPanel;
    lblResumen: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    lblErrores: TcxLabel;
    lblDiferencias: TcxLabel;
    lblProcesados: TcxLabel;
    btnBitacora_BK: TcxButton;
    cxLabel2: TcxLabel;
    lblFaltantes: TcxLabel;
    gridTablas: TZetaCXGrid;
    gridTablasDBTableView: TcxGridDBTableView;
    Nombre: TcxGridDBColumn;
    Descripcion1: TcxGridDBColumn;
    Elegir: TcxGridDBColumn;
    gridTablasLevel: TcxGridLevel;
    DataSourceGridTablas: TDataSource;
    Descripcion2: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure dxWizardControlPageChanged(Sender: TObject);
    procedure btnBitacora_BKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodasClick(Sender: TObject);
    procedure cambiarSeleccion (elegir: Boolean);
    procedure btnBitacoraClick(Sender: TObject);
    procedure gridTablasDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure gridTablasCatalogosDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
  private
    { Private declarations }
    Mutex      : THandle;
    fAvances   : TList;
    oParametros: TZetaParams;
    // fDataBase  : string;
    function Processing: Boolean;
    // procedure HiloFinalizado(Sender: TObject);
    procedure HabilitaBotones;
    procedure agregarTodasTablasCatalogos;
    // procedure CargarListaTablasCatalogosEnGrid;
    procedure CargarTablasCatalogosEnGrid;
    function CompararTablasCatalogos(out mensaje: string): Boolean;
    function CallMeBack( const sMensaje: String; iValor: Integer ): Boolean;
    function CallMeBackDetalle( iValorMax, iValor: Integer): Boolean;
    procedure limitarTablasSeleccion;
    function Warning( const sError: String; oControl: TWinControl ): Boolean;
    function HayTablaSeleccionada (DataSet: TDataSet; iColumna: Integer): Boolean;
  public
    { Public declarations }
  end;

var
  WizCompararTablas: TWizCompararTablas;

implementation

uses
  SysUtils, Windows, DMotorPatch, MotorPatchUtils, MotorPatchThreads, DBClient, ZetaCommonLists, ShellAPI,
  ZetaRegistryServer, DSistema, DConsolida, ZetaDialogo, ZetaCommonTools;

{$R *.dfm}

{function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle, nil, StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ), StrPCopy( zDir, DefaultDir ), 10 );
end;}

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[0..79] of Char;
begin
     Result := ShellExecute( Application.MainForm.Handle,
                             nil,
                             StrPCopy( zFileName, FileName ),
                             StrPCopy( zParams, Params ),
                             StrPCopy( zDir, DefaultDir ),
                             ShowCmd );
end;

{ TWizCompararTablas }

procedure TWizCompararTablas.FormCreate(Sender: TObject);
begin
  HelpContext := 2;
  Mutex := CreateMutex(nil, False, PChar(ClassName + '.Execute'));
  if Mutex = 0 then
    RaiseLastOSError;

  fAvances := TList.Create;
end;

procedure TWizCompararTablas.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := not Processing;
end;

procedure TWizCompararTablas.FormDestroy(Sender: TObject);
begin
  FreeAndNil(oParametros);
  FreeAndNil(fAvances);
  CloseHandle(Mutex);
end;

procedure TWizCompararTablas.FormShow(Sender: TObject);
begin
     with dmSistema do
     begin
          cdsSistBaseDatos.Conectar;
          cdsSistBaseDatosLookup.Conectar;
          DataSource.DataSet := cdsSistBaseDatos;
          luBDFuente.LookupDataset := cdsSistBaseDatosLookUp;
          luBDDestino.LookupDataset := cdsSistBaseDatosLookUp;
     end;

     txtBitacora.Text := Format( '%s' + K_DEFAULT_BIT, [ ExtractFilePath( Application.ExeName ) ] );


    //Desactiva la posibilidad de agrupar
    gridTablasDBTableView.OptionsCustomize.ColumnGrouping := False;
    //Esconde la caja de agrupamiento
    gridTablasDBTableView.OptionsView.GroupByBox := False;
    //Para que nunca muestre el filterbox inferior
    gridTablasDBTableView.FilterBox.Visible := fvNever;
    //Para que no aparezca el Custom Dialog
    gridTablasDBTableView.FilterBox.CustomizeDialog := False;
    //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
    // gridTablasDBTableView.ApplyBestFit();

    //Desactiva la posibilidad de agrupar
    gridTablasCatalogosDBTableView.OptionsCustomize.ColumnGrouping := False;
    //Esconde la caja de agrupamiento
    gridTablasCatalogosDBTableView.OptionsView.GroupByBox := False;
    //Para que nunca muestre el filterbox inferior
    gridTablasCatalogosDBTableView.FilterBox.Visible := fvNever;
    //Para que no aparezca el Custom Dialog
    gridTablasCatalogosDBTableView.FilterBox.CustomizeDialog := False;
    //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
    // gridTablasCatalogosDBTableView.ApplyBestFit();
end;

procedure TWizCompararTablas.gridTablasCatalogosDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TWizCompararTablas.gridTablasDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

function TWizCompararTablas.Processing: Boolean;
begin
    Result := (lblEstatus.Caption <> 'Estatus') and (lblEstatus.Caption <> 'Comparaci�n Terminada');
end;

procedure TWizCompararTablas.HabilitaBotones;
begin
  with dxWizardControl.Buttons do
  begin
       Next.Enabled := not Processing;
       Back.Enabled := not Processing;
  end;
end;

{procedure TWizCompararTablas.HiloFinalizado(Sender: TObject);
var
  Hilo: TThreadPatchProceso;
begin
  with fAvances do
    Delete(IndexOf(Sender));

  if (Sender is TThreadPatchProgreso) then begin
    ;
  end else begin
    Hilo := TThreadPatchProceso(Sender);
    with Hilo.Params do begin
      lblExitos.Caption       := ParamByName('PRO_EXITOS').AsString;
      lblAdvertencias.Caption := ParamByName('PRO_ADVERTENCIAS').AsString;
      lblErrores.Caption      := ParamByName('PRO_ERRORES').AsString;

      if ParamByName('PRO_ERRORES').AsInteger > 0 then
        lblResumen.Caption := 'La actualizaci�n termin� con errores, favor de revisar la bit�cora del sistema.';
      // else
        // lblResumen.Caption := 'La actualizaci�n se aplic� correctamente a la versi�n: ' + cbVersion.Text;
    end;
    dxWizardControl.ActivePage := dxWizardControlPageResumen;
  end;
  // cxProgressBar1.Position := 100;
  HabilitaBotones();
end;}

{* Generar un archivo de bit�cora. @author Ricardo Carrillo Morales 2014-04-10}
procedure TWizCompararTablas.btnBitacoraClick(Sender: TObject);
var sFileName: String;
begin
      sFileName := StrDef (ExtractFileName(txtBitacora.Text), K_DEFAULT_BIT);
      cxOpenDialog.Path := ExtractFilePath( Application.ExeName );
      /// if not strLleno (ExtractFileExt(sFileName)) then
          // sFileName := sFileName + '.txt';
      if cxOpenDialog.Execute then
        txtBitacora.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
end;

procedure TWizCompararTablas.btnBitacora_BKClick(Sender: TObject);
// var
   // oTexto: TStringList;
   // oCDS: TClientDataSet;
   // eTipo: eTipoBitacora;
   // sArchivo: string;
begin
  // oTexto := TStringList.Create;
  // oCDS := TClientDataSet.Create(Self);
  with oParametros do
  try
     {oTexto.Add(Format('********** Inicio: %s  **********',  [FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now)]));
     oTexto.Add(Format('      Sentencias: %d', [ParamByName('PRO_EXITOS').AsInteger]));
     oTexto.Add(Format('    Advertencias: %d', [ParamByName('PRO_ADVERTENCIAS').AsInteger]));
     oTexto.Add(Format('         Errores: %d', [ParamByName('PRO_ERRORES').AsInteger]));
     oTexto.Add('');
     // Lista de advertencias y errores
     oCDS.Data := dmDBConfig.GetBitacora(ParamByName('DB_PROCESO').AsInteger);
     while not oCDS.Eof do
     begin
          eTipo := eTipoBitacora(oCDS.FieldByName('BI_TIPO').AsInteger);
          if eTipo in [tbAdvertencia, tbError] then begin
             oTexto.Add(Format('%s: %s', [ObtieneElemento(lfTipoBitacora, Ord(eTipo)), oCDS.FieldByName('BI_TEXTO').AsString]));
             if eTipo = tbError then
                oTexto.Add(StringOfChar(' ', 7) + oCDS.FieldByName('BI_DATA').AsString);
          end;
          oCDS.Next;
     end;
     if not oCDS.IsEmpty then
        oTexto.Add('');

    if ParamByName('PRO_ERRORES').AsInteger > 0 then
       oTexto.Add('La actualizaci�n termin� con errores, favor de revisarlos y ejecutar el proceso de nuevo.')
    else
        oTexto.Add(lblResumen.Caption);
    oTexto.Add(Format('********** Final: %s **********',  [FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now)]));
    sArchivo := ExtractFilePath(ParamStr(0)) + 'COMPARTE_Actualiza_' + ParamByName('DB_APLICAVERSION').AsString + '.log';
    oTexto.SaveToFile(sArchivo);}

    // ShellExecute(0, nil, 'notepad', PChar(sArchivo), nil, SW_SHOWNORMAL);
    ShellExecute(0, nil, 'notepad', PChar(txtBitacora.Text), nil, SW_SHOWNORMAL);
  finally
         // FreeAndNil(oTexto)
  end;
end;

procedure TWizCompararTablas.btnDesmarcarTodasClick(Sender: TObject);
begin
    cambiarSeleccion(FALSE);
end;

procedure TWizCompararTablas.btnSeleccionarTodosClick(Sender: TObject);
begin
    cambiarSeleccion(TRUE);
end;

procedure TWizCompararTablas.cambiarSeleccion (elegir: Boolean);
var sTablaActual: String;
begin
    with DataSourceGridTablas.DataSet do
    begin
        sTablaActual := FieldByName('Nombre').AsString;
        gridTablasDBTableView.OptionsView.ScrollBars := ssNone;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName ('Elegir').AsBoolean := elegir;
            Post;
            Next;
        end;
        First;
        gridTablasDBTableView.OptionsView.ScrollBars := ssBoth;
        Locate('Nombre', sTablaActual, []);
    end;
end;

procedure TWizCompararTablas.dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind;
  var AHandled: Boolean);
var
  oCursor: TCursor;
begin
  case AKind of
    wcbkNext:
      begin
        if (dxWizardControl.ActivePage = pgBasesDatos) then
        begin
            if luBDFuente.Llave = VACIO then
            begin
                AHandled := Warning('El campo Fuente no puede quedar vac�o.', luBDFuente);
            end
            else if luBDDestino.Llave = VACIO then
            begin
                AHandled := Warning('El campo Destino no puede quedar vac�o.', luBDDestino);
            end
            else if luBDFuente.Llave = luBDDestino.Llave then
            begin
                AHandled := Warning('La base de datos Destino no puede ser igual a la base de datos Fuente.', luBDDestino);
            end
            else
            begin

                oCursor := Screen.Cursor;
                try
                    // Proceso de inicar empresas y tablas en DConsolida
                    dxWizardControl.Enabled := FALSE;
                    Screen.Cursor := crHourglass;

                    // Inicializar bases de datos seleccionadas
                    dmConsolida.InitEmpresas (luBDFuente.Llave, luBDDestino.Llave);
                    // Inicializar tablas
                    dmConsolida.InitTablas;
                    // Agregar las tablas en el grid
                    agregarTodasTablasCatalogos;
                finally
                    dxWizardControl.Enabled := TRUE;
                    Screen.Cursor := oCursor;
                end;
            end;

        end;

        if (dxWizardControl.ActivePage = pgTablasComparar) then
        begin
            if not HayTablaSeleccionada (DataSourceGridTablas.DataSet, 2) then
                AHandled := Warning('Debe seleccionar al menos una tabla.',  gridTablas)
            else
            begin
                dmConsolida.LimpiarDiferenciasErrores;
                CargarTablasCatalogosEnGrid;
                pbAvance.Position := 0;
                pbDetalle.Position := 0;
            end;
        end;

        if (dxWizardControl.ActivePage = pgResumen) then
            if StrVacio (ExtractFileName (txtBitacora.Text)) then
                AHandled := Warning('Debe proporcionar un nombre de archivo de bit�cora',  txtBitacora);

      end;
    wcbkCancel:
      begin
        if not Processing then
          ModalResult := mrCancel;
      end;

    wcbkFinish:
      ModalResult := mrOk;

  end;
  HabilitaBotones();
end;

procedure TWizCompararTablas.dxWizardControlPageChanged(Sender: TObject);
// var
  // DetallesBD: TDetallesBD;
  // FRegistry : TZetaRegistryServer;
var
  oCursor: TCursor;
  mensaje : string;
begin
    {if (dxWizardControl.ActivePage = dxWizardControlPageCaptura) or (dxWizardControl.ActivePage = dxWizardControlPageAplicar) then begin
      lblVersion.Caption := dmDBConfig.LoadVersion;
      cxLabelVersion.Parent := dxWizardControl.ActivePage;
      cxLabelActualizar.Parent := dxWizardControl.ActivePage;
      lblVersion.Parent := dxWizardControl.ActivePage;
      cbVersion.Parent := dxWizardControl.ActivePage;
      chkDiccionario.Parent := dxWizardControl.ActivePage;
      chkEspeciales.Parent := dxWizardControl.ActivePage;
      cbVersion.Enabled := (dxWizardControl.ActivePage = dxWizardControlPageCaptura);
      chkDiccionario.Enabled := (dxWizardControl.ActivePage = dxWizardControlPageCaptura);
    end;
    if (dxWizardControl.ActivePage = dxWizardControlPageCaptura) then begin
      try
        FRegistry := TZetaRegistryServer.Create;
        fDataBase := FRegistry.DataBase;
        DetallesBD := TDetallesBD.Create(nil, fDataBase);
        cbVersion.Properties.Items.Text := DetallesBD.DBVersiones.Text;
        chkEspeciales.Checked := DetallesBD.DBEspecial;
        chkEspeciales.Enabled := DetallesBD.DBEspecial and (dxWizardControl.ActivePage = dxWizardControlPageCaptura);
      finally
        FreeAndNil(FRegistry);
        FreeAndNil(DetallesBD);
      end;
      cbVersion.ItemIndex := (cbVersion.Properties.Items.Count - 1);
      dxWizardControl.Buttons.Next.Enabled := cbVersion.Properties.Items.Count > 0;
    end;
    }

    // if (dxWizardControl.ActivePage = pgResumen) then
    if (dxWizardControl.ActivePage = pgFinProceso) then
    begin
        pnlResumen.Visible := FALSE;
        Application.ProcessMessages;
        oCursor := Screen.Cursor;
        try
            dxWizardControl.Enabled := FALSE;
            Screen.Cursor := crHourglass;
            // Ejecutar Proceso de Comparaci�n de Tablas y Cat�logos
            if CompararTablasCatalogos(mensaje) then
                pnlResumen.Visible := TRUE;
            // Fin de Proceso
        finally
            dxWizardControl.Enabled := TRUE;
            Screen.Cursor := oCursor;

            // Resultados del proceso
            lblProcesados.Caption := IntToStr (dmConsolida.TablasCountSeleccionadas);
            lblDiferencias.Caption := IntToStr (dmConsolida.TablasConDiferencias);
            lblFaltantes.Caption := IntToStr (dmConsolida.TablasConFaltantes);
            lblErrores.Caption := IntToStr (dmConsolida.TablasConError);
        end;
    end;

    {if (dxWizardControl.ActivePage = pgFinProceso) then
    begin
        lblProcesados.Caption := IntToStr (dmConsolida.TablasCountSeleccionadas);
        lblDiferencias.Caption := IntToStr (dmConsolida.TablasConDiferencias);
        lblErrores.Caption := IntToStr (dmConsolida.TablasConError);
    end;}
end;

procedure TWizCompararTablas.dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage;
  var AAllow: Boolean);
begin
  //AAllow := not Processing;
  // if ANewPage = dxWizardControlPageAplicar then
  if ANewPage = pgResumen then
    dxWizardControl.Buttons.Next.Caption := 'Aplicar'
  else
    dxWizardControl.Buttons.Next.Caption := 'Siguiente';
end;

procedure TWizCompararTablas.agregarTodasTablasCatalogos;
 var cdsTablasCatalogos : TZetaClientDataSet;
     i : Integer;
begin
     cdsTablasCatalogos := TZetaClientDataSet.Create(Self);
     with cdsTablasCatalogos do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          AddStringField('Nombre', 20);
          AddStringField('Descripcion', 45);
          AddBooleanField('Elegir');
          CreateTempDataset;
          Open;
     end;

     for i:=0 to dmConsolida.TablasCount-1 do
     begin
        cdsTablasCatalogos.Insert;
        cdsTablasCatalogos.FieldByName('Nombre').AsString := dmConsolida.Tablas[i].Nombre;
        cdsTablasCatalogos.FieldByName('Descripcion').AsString := dmConsolida.Tablas[i].Descripcion;
        cdsTablasCatalogos.FieldByName('Elegir').AsBoolean := FALSE;
        cdsTablasCatalogos.Post;
     end;

     DataSourceGridTablas.DataSet := cdsTablasCatalogos;
     gridTablasDBTableView.DataController.DataSource := DataSourceGridTablas;

     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     // gridTablasDBTableView.ApplyBestFit();
end;

procedure TWizCompararTablas.CargarTablasCatalogosEnGrid;
var cdsTablasCatalogos : TZetaClientDataSet;
begin
     cdsTablasCatalogos := TZetaClientDataSet.Create(Self);
     with cdsTablasCatalogos do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          AddStringField('Entidad', 20);
          AddStringField('Descripcion', 45);
          CreateTempDataset;
          Open;
     end;

     DataSourceGridTablas.DataSet.First;
     while not DataSourceGridTablas.DataSet.Eof do
     begin
          if DataSourceGridTablas.DataSet.FieldByName('Elegir').AsBoolean then
          begin
              cdsTablasCatalogos.Insert;
              cdsTablasCatalogos.FieldByName('Entidad').AsString := DataSourceGridTablas.DataSet.FieldByName('Nombre').AsString;
              cdsTablasCatalogos.FieldByName('Descripcion').AsString := DataSourceGridTablas.DataSet.FieldByName('Descripcion').AsString;
              cdsTablasCatalogos.Post;
          end;
          DataSourceGridTablas.DataSet.Next;
     end;

     DataSourceGrid.DataSet := cdsTablasCatalogos;
     gridTablasCatalogosDBTableView.DataController.DataSource := DataSourceGrid;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     // gridTablasCatalogosDBTableView.ApplyBestFit();
end;

{procedure TComparaTablas.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   FLog: TTransferLog;
   i: Integer;
begin
     FLog := TTransferLog.Create;
     try
        FLog.Start( LogFileName );
        ShowStatus( 'Empezando Comparaci�n' );
        with dmCompara do
        begin
             with ProgressBar do
             begin
                  Step := 1;
                  Max := TablasCount;
             end;
             if Transferir.Checked then
             begin
                  with Catalogos do
                  begin
                       for i := 0 to ( Items.Count - 1 ) do
                       begin
                            Tablas[ i ].Transfer := Checked[ i ];
                       end;
                  end;
             end;
             if not CompararTablas( CallMeBack, FLog ) then
                ZetaDialogo.zInformation( '� Buenas Noticias !', 'No Se Encontraron Diferencias', 0 );
        end;
        ShowStatus( 'Comparaci�n Terminada' );
        lOk := True;
     finally
            with FLog do
            begin
                 Close;
                 Free;
            end;
     end;
     if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver La Bit�cora ?', 0, mbOk ) then
        ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( LogFileName ), ExtractFilePath( LogFileName ), SW_SHOWDEFAULT );
end;}


function TWizCompararTablas.CompararTablasCatalogos(out mensaje: string): Boolean;
var
   // sArchivo : String;
   lOk, bCompararTablas : Boolean;
   // lErrorXML, lErrorEstructura: Boolean;
   FLog: TTransferLog;
   // i: Integer;
begin
     // lOk := FALSE;
     try

         // ZetaDialogo.ZInformation('Mensaje', 'Iniciando proceso', 0);
         // mensaje := '';

         FLog := TTransferLog.Create;
         try
            if StrVacio(ExtractFileExt(txtBitacora.Text)) then
                FLog.Start( txtBitacora.Text + '.txt' )
            else
              FLog.Start( txtBitacora.Text );

            // ShowStatus( 'Empezando Comparaci�n' );
            lblEstatus.Caption := 'Empezando Comparaci�n';
            // with dmCompara do
            with dmConsolida do
            begin
                 {if Transferir.Checked then
                 begin
                      with Catalogos do
                      begin
                           for i := 0 to ( Items.Count - 1 ) do
                           begin
                                Tablas[ i ].Transfer := Checked[ i ];
                           end;
                      end;
                 end;}

                 // Antes de CompararTablas, limitar selecci�n de tablas
                 limitarTablasSeleccion;

                 // Ajustar m�ximo de la barra de progreso a las tablas que ser�n comparadas.
                 pbAvance.Properties.Max := TablasCountSeleccionadas;

                 bCompararTablas := CompararTablas( CallMeBack, FLog, CallMeBackDetalle );
                 // if not CompararTablas( CallMeBack, FLog, CallMeBackDetalle ) then
                 // begin
                    CallMeBack ('',  dmConsolida.TablasCountSeleccionadas);
                    lblEstatus.Caption := 'Comparaci�n Terminada';

                 if not bCompararTablas then
                    ZetaDialogo.zInformation( 'Proceso finalizado', 'No se encontraron diferencias.', 0 );
                 {end
                 else
                 begin
                    lblEstatus.Caption := 'Comparaci�n Terminada';
                    CallMeBack ('',  dmConsolida.TablasCountSeleccionadas);
                 end;}


            end;
            // ShowStatus( 'Comparaci�n Terminada' );
            // lblEstatus.Caption := 'Comparaci�n Terminada';
            lOk := True;
         finally
                with FLog do
                begin
                     Close;
                     Free;
                end;
         end;
         // if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver La Bit�cora ?', 0, mbOk ) then
            // ExecuteFile ( 'NOTEPAD.EXE', ExtractFileName( txtBitacora.Text ), ExtractFilePath( txtBitacora.Text ), SW_SHOWDEFAULT );
     Except on e:exception do
     begin
          ZetaDialogo.ZError('Error', mensaje, 0);
          lOk := FALSE;
     end;
     end;

     Result := lOk;
end;

function TWizCompararTablas.CallMeBack( const sMensaje: String; iValor: Integer ): Boolean;
begin
     lblEstatus.Caption := sMensaje;
     pbAvance.Position := iValor;
     Application.ProcessMessages;
     Result := TRUE;
     // Result := not Wizard.Cancelado;
end;

function TWizCompararTablas.CallMeBackDetalle( iValorMax, iValor: Integer ): Boolean;
begin
     // if iValorMax > 0 then
        pbDetalle.Properties.Max := iValorMax;
     pbDetalle.Position := iValor;
     Application.ProcessMessages;
     Result := TRUE;
end;

procedure TWizCompararTablas.limitarTablasSeleccion;
var i: Integer;
begin
    gridTablasCatalogosDBTableView.OptionsView.ScrollBars := ssNone;
    for i := 0 to dmConsolida.TablasCount-1 do
    begin
        dmConsolida.Tablas[i].Seleccionada := FALSE;
        DataSourceGrid.DataSet.First;
        while (not DataSourceGrid.DataSet.Eof) and (not dmConsolida.Tablas[i].Seleccionada)do
        begin
            if DataSourceGrid.DataSet.FieldByName('ENTIDAD').AsString = dmConsolida.Tablas[i].Nombre then
            begin
                dmConsolida.Tablas[i].Seleccionada := TRUE;
            end;
            DataSourceGrid.DataSet.Next;
        end;
    end;
    gridTablasCatalogosDBTableView.OptionsView.ScrollBars := ssBoth;
end;

function TWizCompararTablas.Warning( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZWarning( Caption, sError, 0, mbOK );
     oControl.SetFocus;
     Result := TRUE;
end;

function TWizCompararTablas.HayTablaSeleccionada (DataSet: TDataSet; iColumna: Integer): Boolean;
begin
    Result := FALSE;
    with DataSet do
    begin
        First;
        while not Eof do
        begin
            if Fields[iColumna].AsBoolean then
            begin
                Result := TRUE;
                Break;
            end;
            Next;
        end;
    end;
end;

end.
