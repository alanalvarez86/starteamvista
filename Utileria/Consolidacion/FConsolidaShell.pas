unit FConsolidaShell;

interface

uses
  CheckLst, DB, ZetaRegistryServer, FAutoServer, FAutoServerLoader, DZetaServerProvider, ZetaClientDataSet,
  WinSvc, FileCtrl, TressMorado2013, ComObj,

  // US #7328: Implementar Control de la ayuda en XE5
  // HtmlHelpViewer,
  FHelpManager, SysUtils,

  Forms, dxSkinsCore, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, cxDBData, dxSkinsdxBarPainter, cxClasses, Vcl.Dialogs, Vcl.ExtDlgs, Vcl.ImgList,
  Vcl.Controls, cxShellBrowserDialog, dxBar, dxSkinsForm, cxMemo, cxRichEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, cxCheckListBox, cxCheckBox, cxImage, cxMaskEdit, cxDropDownEdit, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxGroupBox, cxLabel, dxGDIPlusClasses, Vcl.ExtCtrls, cxPC, System.Classes, dxBevel, ZetaDialogo, ZetaRegistryCliente,
  cxLocalization, ZetaCXGrid;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
  end;

  TFConsolidacionTRESS = class(TForm)
    dxSkinController1: TdxSkinController;
    dxBarManagerbkbk: TdxBarManager;
    btnComparar: TdxBarLargeButton;
    btnTransferirEmp: TdxBarLargeButton;
    dxBarManagerBar: TdxBar;
    lgbSalir: TdxBarLargeButton;
    dxBevel1: TdxBevel;
    cxLargeImageMenu: TcxImageList;
    DataSource: TDataSource;
    btnConsolidar: TdxBarLargeButton;
    btnTransferirCat: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    btnRefrescar: TdxBarLargeButton;
    pnlGrid: TPanel;
    cxGridServidor: TcxGrid;
    cxGridServidorDBTableView: TcxGridDBTableView;
    colCodigo: TcxGridDBColumn;
    colNombre: TcxGridDBColumn;
    colDatos: TcxGridDBColumn;
    colEmpleados: TcxGridDBColumn;
    colTamanio: TcxGridDBColumn;
    colEspeciales: TcxGridDBColumn;
    colMensaje: TcxGridDBColumn;
    cxGridServidorLevel: TcxGridLevel;
    colTipo: TcxGridDBColumn;
    cxLocalizer: TcxLocalizer;
    procedure lgbSalirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCompararClick(Sender: TObject);
    procedure btnTransferirCatClick(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
    procedure btnTransferirEmpClick(Sender: TObject);
    procedure btnConsolidarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxGridServidorDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    // US #7328: Implementar Control de la ayuda en XE5
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );
    private
      { Private declarations }
      FAutoServer: TAutoServer;
      function GetMaxEmpleados: Integer;
      procedure ApplyMinWidth;
    public
    { Public declarations }
    protected
      // US #7328: Implementar Control de la ayuda en XE5
      FOldHelpEvent: THelpEvent;
      function HTMLHelpHook( Command: Word; Data: THelpEventData; var CallHelp: Boolean ): Boolean;

      procedure CargaTraducciones;
  end;

var
  FConsolidacionTRESS: TFConsolidacionTRESS;

implementation

uses
  Types, Windows, Graphics, ZetaCommonClasses,// ZetaWinAPITools,
  ZetaLicenseMgr, ZetaCommonTools, ZetaServerTools,
  FWizCompararTablas, FWizTransferirTablas, FWizTransferirEmpleados, FWizConsolidar, MotorPatchUtils, DBClient, DSistema;


{$R *.dfm}

procedure TFConsolidacionTRESS.FormCreate(Sender: TObject);
{var
  l: DWORD;}
begin
     HelpContext := 1;

     ZetaRegistryCliente.InitClientRegistry;
     {l := GetWindowLong(Self.Handle, GWL_STYLE);
     l := l and not (WS_MAXIMIZEBOX);
     l := SetWindowLong(Self.Handle, GWL_STYLE, l);}

     FAutoServerLoader.FCalculandoEmpleadosLoader := FALSE;
     FAutoServer := TAutoServer.Create;
     WindowState := wsNormal;
     // US #7328: Implementar Control de la ayuda en XE5
     with Application do
     begin
          OnException := ManejaExcepcion;
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
     end;
end;

// US #7328: Implementar Control de la ayuda en XE5
function TFConsolidacionTRESS.HTMLHelpHook(Command: Word; Data: THelpEventData; var CallHelp: Boolean): Boolean;
begin
    Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

// US #7328: Implementar Control de la ayuda en XE5
procedure TFConsolidacionTRESS.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

procedure TFConsolidacionTRESS.FormDestroy(Sender: TObject);
begin
     FAutoServer.Free;
end;

procedure TFConsolidacionTRESS.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if ( Key <> 0 ) then
     begin
          if Key = VK_F5  then
          begin
               Key := 0;
               dmSistema.cdsSistBaseDatos.Refrescar;
          end;
     end;
end;

procedure TFConsolidacionTRESS.FormShow(Sender: TObject);
begin

    // Traducciones
    CargaTraducciones;

    dmSistema.cdsSistBaseDatos.Conectar;
    DataSource.DataSet := dmsistema.cdsSistBaseDatos;
    cxGridServidorDBTableView.DataController.DataSource := DataSource;

    //Desactiva la posibilidad de agrupar
    cxGridServidorDBTableView.OptionsCustomize.ColumnGrouping := False;
    //Esconde la caja de agrupamiento
    cxGridServidorDBTableView.OptionsView.GroupByBox := False;
    //Para que nunca muestre el filterbox inferior
    cxGridServidorDBTableView.FilterBox.Visible := fvNever;
    //Para que no aparezca el Custom Dialog
    cxGridServidorDBTableView.FilterBox.CustomizeDialog := TRUE;
    //DevEx: Para que ponga el MinWidth ideal al titulo de las columnas. Posteriormente se aplica el BestFit.
    ApplyMinWidth;
    cxGridServidorDBTableView.ApplyBestFit();
end;

procedure TFConsolidacionTRESS.lgbSalirClick(Sender: TObject);
begin
     Close;
end;

procedure TFConsolidacionTRESS.btnRefrescarClick(Sender: TObject);
begin
      dmSistema.cdsSistBaseDatos.Refrescar;
end;

procedure TFConsolidacionTRESS.btnCompararClick(Sender: TObject);
var oWizCompararTablas: TWizCompararTablas;
begin
    try
      oWizCompararTablas := TWizCompararTablas.Create(Self);
      oWizCompararTablas.ShowModal;
    finally
      FreeAndNil (oWizCompararTablas);
    end;
end;

procedure TFConsolidacionTRESS.btnTransferirCatClick(Sender: TObject);
var oWizTransferirTablas: TWizTransferirTablas;
begin
    try
      oWizTransferirTablas := TWizTransferirTablas.Create(Self);
      oWizTransferirTablas.ShowModal;
    finally
      FreeAndNil (oWizTransferirTablas);
    end;
end;

procedure TFConsolidacionTRESS.btnTransferirEmpClick(Sender: TObject);
var oWizTransferirEmpleados: TWizTransferirEmpleados;
begin
    try
      oWizTransferirEmpleados := TWizTransferirEmpleados.Create(Self);
      oWizTransferirEmpleados.ShowModal;
    finally
      FreeAndNil (oWizTransferirEmpleados);
    end;
end;

procedure TFConsolidacionTRESS.btnConsolidarClick(Sender: TObject);
var oWizConsolidaBaseDatos: TWizConsolidar;
begin
    try
      oWizConsolidaBaseDatos := TWizConsolidar.Create(Self);
      oWizConsolidaBaseDatos.LimiteEmpleados := GetMaxEmpleados;
      oWizConsolidaBaseDatos.ShowModal;
    finally
      FreeAndNil (oWizConsolidaBaseDatos);
    end;
end;

function TFConsolidacionTRESS.GetMaxEmpleados: Integer;
begin
     TAutoServerLoader.LoadSentinelInfo( FAutoServer );
     Result := FAutoServer.Empleados;
end;

procedure TFConsolidacionTRESS.CargaTraducciones;
begin
    cxLocalizer.Active := True;
    cxLocalizer.Locale := 2058;  // Cardinal para Espanol (Mexico)
end;

procedure TFConsolidacionTRESS.cxGridServidorDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TFConsolidacionTRESS.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions = 35;
begin
     with  cxGridServidorDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

end.
