unit DSistema;

interface

uses
  System.SysUtils, System.Classes, Sistema_TLB, DBasicoCliente, Data.DB, ZetaCommonClasses, ZetaDialogo, Dialogs,
  Datasnap.DBClient, ZetaClientDataSet, Controls, Forms, ZetaCommonLists;

type
  TdmSistema = class(TDataModule)
    cdsSistBaseDatos: TZetaLookupDataSet;
    cdsSistBaseDatosLookUp: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsSistBaseDatosAlAdquirirDatos(Sender: TObject);
    procedure cdsSistBaseDatosLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsSistBaseDatosLookUpLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: string; var sKey, sDescription: string);
    procedure DB_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsSistBaseDatosAlCrearCampos(Sender: TObject);
  private
    { Private declarations }
    FActualizarBD: Boolean;
  protected
    FServidor: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
  public
    { Public declarations }
    property ActualizarBD: Boolean read FActualizarBD write FActualizarBD;
    function CrearBDEmpleados (Parametros: TZetaParams): Boolean;
    function CrearEstructuraBD(Parametros: TZetaParams): Boolean;
    function CreaInicialesTabla(Parametros: TZetaParams): Boolean;
    function CreaSugeridosTabla(Parametros: TZetaParams): Boolean;
    function CreaDiccionarioTabla(Parametros: TZetaParams): Boolean;
    function EmpGrabarDBSistema (Parametros: TZetaParams): Boolean;
    function GrabaDB_INFO(Parametros: OleVariant): Boolean;
    function ValidarBDEmpleados(sCodigo, sBaseDatos: String; bWizEmpleados: Boolean = FALSE): WideString;
    function GetServer: String;
  end;

var
  dmSistema: TdmSistema;

implementation

uses ZetaBusqueda_DevEx;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmSistema.DataModuleCreate(Sender: TObject);
begin
     FActualizarBD := FALSE;
end;

procedure TdmSistema.cdsSistBaseDatosAlAdquirirDatos(Sender: TObject);
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourGlass;
        cdsSistBaseDatos.Data := ServerSistema.ConsultaConfBD(ord (tc3Datos));
        cdsSistBaseDatosLookUp.Data := cdsSistBaseDatos.Data;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TdmSistema.cdsSistBaseDatosAlCrearCampos(Sender: TObject);
begin
     with cdsSistBaseDatos do
     begin
          with FieldByName( 'DB_TIPO' ) do
          begin
               OnGetText:= DB_TIPOGetText;
               Alignment:= taLeftJustify;
          end;
     end;
end;

procedure TdmSistema.DB_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          Text := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Sender.AsInteger);
     end;
end;

procedure TdmSistema.cdsSistBaseDatosLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsSistBaseDatos do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque el traspaso de Data regresa el
          begin                          // dataset a dsBrowse
               Conectar;
               cdsSistBaseDatosLookUp.Data := Data;
          end;
     end;
end;

procedure TdmSistema.cdsSistBaseDatosLookUpLookupSearch(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: string; var sKey,
  sDescription: string);
begin  
  lOk := ZetaBusqueda_DevEx.ShowSearchForm( Sender,
      Format ('DB_TIPO IN (%d,%d,%d) AND DB_ERROR = '''' ', [ord (tc3Datos), ord (tcPresupuesto), ord (tc3Prueba)]),
      sKey, sDescription, false )
end;

function TdmSistema.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( BasicoCliente.CreaServidor( CLASS_dmServerSistema, FServidor ) );
end;

{ ** Inicio: Creaci�n de nueva base de datos consolidadora ** }

function TdmSistema.CrearBDEmpleados (Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CrearBDEmpleados(Parametros.VarValues);
end;

function TdmSistema.CrearEstructuraBD(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CrearEstructuraBD(Parametros.VarValues);
end;

function TdmSistema.CreaInicialesTabla(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CreaInicialesTabla(Parametros.VarValues);
end;

function TdmSistema.CreaSugeridosTabla(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CreaSugeridosTabla(Parametros.VarValues);
end;

function TdmSistema.CreaDiccionarioTabla(Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.CreaDiccionarioTabla(Parametros.VarValues);
end;

function TdmSistema.EmpGrabarDBSistema (Parametros: TZetaParams): Boolean;
begin
     Result := ServerSistema.EmpGrabarDBSistema(Parametros.VarValues);
end;

function TdmSistema.GrabaDB_INFO(Parametros: OleVariant): Boolean;
begin
     Result := ServerSistema.GrabaDB_INFO(Parametros);
end;

function TdmSistema.ValidarBDEmpleados (sCodigo, sBaseDatos: String; bWizEmpleados: Boolean = FALSE): WideString;
var sMensaje: WideString;
begin

     if bWizEmpleados then
     begin
        ServerSistema.ValidarBDEmpleados(sCodigo, sBaseDatos, sMensaje);
        Result := sMensaje;
     end
     else
     begin
         try
            ServerSistema.ValidarBDEmpleados(sCodigo, sBaseDatos, sMensaje);
            Result := sMensaje;
         except
               Result := VACIO;
         end;
     end;
end;

function TdmSistema.GetServer: String;
begin
    Result := ServerSistema.GetServer;
end;

{ ** Fin:    Creaci�n de nueva base de datos consolidadora ** }

end.
