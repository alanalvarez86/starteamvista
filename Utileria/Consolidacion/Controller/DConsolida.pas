unit DConsolida;

interface

uses
  SysUtils, System.Classes, ZetaAsciiFile, ZetaCommonTools, Db, DBTables,
  FireDAC.Stan.Intf, FireDAC.Phys, FireDAC.Phys.ODBCBase, FireDAC.Phys.ODBC,
  ZetaServerTools, ZetaCommonLists, DZetaServerProvider, DSistema, Variants, ZetaServerDataSet,
  Datasnap.DBClient, Data.Win.ADODB, ZetaCommonClasses, ZetaClientDataSet, MotorPatchUtils;

const
     DBIERR_KEYVIOL = 9729;

type

  TShowProgress = function( const sMensaje: String; iValor: Integer): Boolean of object;
  TShowProgressDetalle = function( iValorMax, iValor: Integer ) : Boolean of object;
  TEndCallBack = procedure( Sender: TObject; var Continue: Boolean ) of object;
  TdmConsolida = class;

  TCompanyData = class( TObject )
  private
    { Private declarations }
    FCodigo: String;
    FNombre: String;
    FAlias: String;
    FDatos: String;
    FUserName: String;
    FPassword: String;
    FEmpate: Integer;
  public
    { Public declarations }
    property Codigo: String read FCodigo write FCodigo;
    property Nombre: String read FNombre write FNombre;
    property Alias: String read FAlias write FAlias;
    property Datos: String read FDatos write FDatos;
    property UserName: String read FUserName write FUserName;
    property Password: String read FPassword write FPassword;
    property Empate: Integer read FEmpate write FEmpate;
  end;

  TCompanyList = class( TObject )
  private
    { Private declarations }
    FLista: TList;
    function GetEmpresa( Index: Integer ): TCompanyData;
  public
    { Public declarations }
    property Empresa[ Index: Integer ]: TCompanyData read GetEmpresa;
    constructor Create;
    destructor Destroy; override;
    function AgregaEmpresa: TCompanyData;
    function Count: Integer;
    procedure Clear;
    procedure LlenaLista( Lista: TStrings );
  end;

  TTransferLog = class( TASCIILog )
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start( const sFileName: String );
    procedure HandleException( const sMensaje: String; Error: Exception );
  end;

  TTablaData = class(TObject)
  private
    { Private declarations }
    FSeleccionada: Boolean;
    // FDiferencias: Boolean;
    FCampos: TStrings;
    FClase: Integer;
    FComparables: TStrings;
    FDescripcion: String;
    FLlaves: TStrings;
    FDifferences: TStrings;
    FError: TStrings;
    FDelta: TStrings;
    FNombre: String;
    FOwner: TList;
    FTransfer: Boolean;
    FLog: TTransferLog;
    FQFindTable: TQuery;
    FQFindTableDS: TDataSet;
    FQFindField: TQuery;
    FQFindFieldDS: TDataSet;
    FQInsertDS: TDataSet;
    FQRead: TQuery;
    FQReadDS: TDataSet;
    FQSeek: TQuery;
    FQSeekDS: TDataSet;
    FdmCompara: TdmConsolida;
    FIgnorar: String;
    FComputedFields: String;
    FDbSourceName: String;
    FUsaDBSYS : Boolean;

    oZetaProviderSource: TdmZetaServerProvider;
    oZetaProviderMaster: TdmZetaServerProvider;
    function BuildPrimaryKeyFilter: String;
    function CompareFieldValues: Boolean;
    function GetFieldDescription( const sFieldName: String ): String;
    function GetNamesList( Lista: TStrings ): String;
    function GetNombreReal: String;
    procedure Connect(DataModule: TdmConsolida);
    procedure GetFieldsDescriptions( Lista: TStrings );
    procedure LoadPrimaryKey;
    procedure HandleException( const sMensaje: String; Error: Exception );
    function camposLlaveAsCommaText (sTabla: String): String;
    function TieneDiferencias: Boolean;
    function TieneError: Boolean;
    function FaltanRegistros: Boolean;

  protected
    { Protected declarations }
    property Ignorar: String read FIgnorar write FIgnorar;
    property ComputedFields: String read FComputedFields write FComputedFields;
    {$ifdef MSSQL}
    {$endif}
    function GetPrimaryKeyValues( Source: TDataSet ): String; dynamic;
    function GetQueryInsert: String; dynamic;
    function GetQueryRead: String; dynamic;
    function GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp; FNivel0: String = '' ): String; dynamic;
    procedure NotifyDelta;
    procedure PrepareQueryInsert;

    procedure TransferFieldValues; dynamic;
    procedure FieldsToParams;dynamic;
    procedure PrepareQuerySeek;virtual;
    function RowExists: Boolean;virtual;
  public
    { Public declarations }
    constructor Create( Owner: TList; const sNombre: String );
    destructor Destroy; override;
    property Seleccionada: Boolean read FSeleccionada write FSeleccionada;
    property Diferencias: Boolean read TieneDiferencias;
    property Error: Boolean read TieneError;
    property MensajeError: TStrings read FError;
    property DiferenciasTS: TStrings read FDifferences;
    property FaltantesTS: TStrings read FDelta;
    property Registros: Boolean read FaltanRegistros;
    property Campos: TStrings read FCampos;
    property CamposComparables: TStrings read FComparables;
    property Clase: Integer read FClase;
    property Descripcion: String read FDescripcion;
    property Llaves: TStrings read FLlaves;
    property Log: TTransferLog read FLog write FLog;
    property Nombre: String read FNombre;
    property NombreReal: String read GetNombreReal;
    property Transfer: Boolean read FTransfer write FTransfer;
    property QueryTable: TQuery read FQFindTable write FQFindTable;
    property QueryTableDS: TDataSet read FQFindTableDS write FQFindTableDS;
    property QueryField: TQuery read FQFindField write FQFindField;
    property QueryFieldDS: TDataSet read FQFindFieldDS write FQFindFieldDS;
    property QueryRead: TQuery read FQRead;
    property QueryReadDS: TDataSet read FQReadDS write FQReadDS;
    property UsaDBSYS: Boolean read FUsaDBSYS write FUsaDBSYS;
    function Comparar (CallBackDetalle: TShowProgressDetalle): Boolean; dynamic;
    function Transferir: Boolean; dynamic;
    procedure GetTableInfo;
    procedure BuildCamposComparables( const sComparables: String );
    // procedure TransferEmployee( const iOldEmp, iNewEmp: TNumEmp ); dynamic;
    procedure TransferEmployee( const iOldEmp, iNewEmp: TNumEmp; sNivel0: String ); dynamic;
  end;

  TNomParam = class( TTablaData )
  private
    { Private declarations }
    FQFolioDS: TDataSet;
  protected
    { Protected declarations }
    procedure TransferFieldValues; override;
  public
    { Public declarations }
    function Comparar(CallBackDetalle: TShowProgressDetalle): Boolean; override;
    procedure BuildPrimaryKeyList;
  end;

  TGlobal = class( TTablaData )
  private
    { Private declarations }
  protected
    { Protected declarations }
    function GetPrimaryKeyValues( Source: TDataSet ): String; override;
  public
    { Public declarations }
  end;

  TCatalogo = class( TTablaData )
  private
    { Private declarations }
    FKardexKeyField: String;
    FFiltro: String;
  protected
    { Protected declarations }
    function GetQueryRead: String; override;
    function GetPrimaryKeyValues( Source: TDataSet ): String; override;
  public
    { Public declarations }
    property KardexKeyField: String read FKardexKeyField write FKardexKeyField;
    property Filtro: String read FFiltro write FFiltro;
  end;

  TColabora = class( TTablaData )
  private
    { Private declarations }
    FNivel0: String;
    FFiltro: String;
    function CambiaNivel0( const sScript: String ): String;
  protected
    { Protected declarations }
    function GetQueryRead: String; override;
    function GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp; FNivel0: String = '' ): String; override;
  public
    { Public declarations }
    property Nivel0: String read FNivel0 write FNivel0;
    property Filtro: String read FFiltro write FFiltro;
  end;

  TdmConsolida = class(TDataModule)
    tqList: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FTablas: TList;
    FNivel0: TStrings;
    FEmpresaActiva: String;
    FEmpresaFuente: String;
    FEmpresas: TCompanyList;
    FTargetAlias: String;
    FTargetUserName: String;
    FTargetPassword: String;
    FColabora: TColabora;
    FDbSourceName: String;
    oZetaProvider: TdmZetaServerProvider;
    FErrorFound: Boolean;
    FErrorCount: Integer;
    FEndCallBack: TEndCallBack;
    function GetEmpresaActiva: TCompanyData;
    function GetEmpresaFuente: TCompanyData;
    function GetEmpresasCount: Integer;
    function GetEmpresas(Index: Integer): TCompanyData;
    function GetTablas(Index: Integer): TTablaData;
    function GetTablasPorNombre(Nombre: String): TTablaData;
    function GetTablasCount: Integer;
    function GetTablasCountSeleccionadas: Integer;
    function GetTablasTransferirCountSeleccionadas: Integer;
    function GetTablasConDiferencias: Integer;
    function GetTablasConFaltantes: Integer;
    function GetTablasConError: Integer;
    procedure ClearTablas;
    procedure InitMovimientos( const lNominas, lIMSS: Boolean; const sFiltro, sNivel0: String );
    procedure LeeListaNivel0(Lista: TStrings);

    function BorraTablas (Bitacora: TTransferLog; oZetaProviderConsolidaTarget: TdmZetaServerProvider ) : Boolean;

    procedure ClearErrorFound;
    procedure SetErrorFound( const lValue: Boolean );

    function LeeTablas (CallBack: TShowProgress; Bitacora: TTransferLog; oZetaProviderConsolidaTarget, oZetaProviderConsolida: TdmZetaServerProvider; Empresa: TCompanyData; const iYear: Integer; const dInicial, dFinal: TDate; const iMaxEmpleados: Integer; const iMaxEmpDBSource: Integer): Boolean;
    function TerminaConsolidacion( Bitacora: TTransferLog; sTableName: String; lExito: Boolean ) : Boolean;
    procedure SetInsertScript( Tabla: String; Source: TDataset; var Target: TDataset;
          oZetaProviderConsolidaTarget: TdmZetaServerProvider; sComputed_Columns: String);
    function DefineTabla( sTabla: String; oFields: TField; sComputed_Columns: String ): Boolean;
    procedure TransfiereDatasets( sTabla: String; qSource, qTarget: TDataSet; sComputed_Columns: String);
    function DefineComputedColumns (sTableName: String; oZetaProviderConsolidaTarget: TdmZetaServerProvider): String;

  protected
    { Protected declarations }
    FParamMsg: TStrings;
    function DoEndCallBack (Bitacora: TTransferLog): Boolean;
  public
    { Public declarations }
    procedure InitTablas;
    property EmpresaActiva: TCompanyData read GetEmpresaActiva;
    property EmpresaFuente: TCompanyData read GetEmpresaFuente;
    property Empresas[ Index: Integer ]: TCompanyData read GetEmpresas;
    property EmpresasCount: Integer read GetEmpresasCount;
    property Nivel0: TStrings read FNivel0;
    property TargetAlias: String read FTargetAlias write FTargetAlias;
    property TargetPassword: String read FTargetPassword write FTargetPassword;
    property TargetUserName: String read FTargetUserName write FTargetUserName;
    procedure InitEmpresas( Fuente, Destino: String );
    function CompararTablas( CallBack: TShowProgress; Bitacora: TTransferLog; CallBackDetalle: TShowProgressDetalle ): Boolean;
    function TransferirTablas( CallBack: TShowProgress; Bitacora: TTransferLog ): Boolean;
    property Tablas[ Index: Integer ]: TTablaData read GetTablas;
    property TablasN[ Nombre: String ]: TTablaData read GetTablasPorNombre;
    property TablasCount: Integer read GetTablasCount;
    property TablasCountSeleccionadas: Integer read GetTablasCountSeleccionadas;
    property TablasTransferirCountSeleccionadas: Integer read GetTablasTransferirCountSeleccionadas;
    property TablasConDiferencias: Integer read GetTablasConDiferencias;
    property TablasConFaltantes: Integer read GetTablasConFaltantes;
    property TablasConError: Integer read GetTablasConError;
    property DBSourceName: String read FDBSourceName;
    procedure LlenaListaCompaniasFuente(Lista: TCompanyList);
    // procedure LeeListaNivel0(Lista: TStrings);
    function TransferirEmpleados( CallBack: TShowProgress; Bitacora: TTransferLog; const iEmpBase: TNumEmp; const lNominas, lIMSS: Boolean; const sFiltro, sNivel0: String ): Boolean;
    // function ConectarDestino: Boolean;
    procedure LimpiarDiferenciasErrores;
    function GetEmpleadoMaximo: Integer;    
	function GetEmpleadoCount(sFiltro: String; var sMensajeError: String): Integer;
    function ConsolidarBasesDatos( CallBack: TShowProgress; const Bitacora: TTransferLog; const sNombre: String; Lista: TCompanyList;
      BaseDatosConsolidadora: TCompanyData; const iYear: Integer; const dInicial, dFinal: TDate; const lBorrarTablas: Boolean; const iMaxEmpleados: Integer ): Boolean;
    property ErrorCount: Integer read FErrorCount;
    property ErrorFound: Boolean read FErrorFound;
    property EndCallBack: TEndCallBack read FEndCallBack write FEndCallBack;
    property ParamMsg: TStrings read FParamMsg write FParamMsg;
  end;

var
  dmConsolida: TdmConsolida;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

const
     K_ANCHO_LINEA = 79;
     K_CONDICION = '@WHERE';
     K_TODOS = 'TODOS';
     K_DELTA_VALUES = '{ %s } <-> { %s }';
     K_ANCHO_REFERENCIA = 8;
     K_VALOR_NULO = 'NULLNULL';
     K_CONCEPTOS_PK = 'CO_NUMERO;CB_CODIGO;MO_REFEREN';
     K_EMPLEADOS_PK = 'CB_CODIGO';
     VACIO = '';
     Q_MAX_NUMEMP = 'select COUNT( * ) as CUANTOS from COLABORA '+
                       'where ( ( select DBO.SP_STATUS_ACT ( %s, COLABORA.CB_CODIGO ) ) = 1 ) ';

{$R *.dfm}

function GetQueryFiltro( const sQuery, sFiltro: String ): String;
begin
     if ZetaCommonTools.StrLleno( sFiltro ) then
        Result := ZetaCommonTools.StrTransAll( sQuery, K_CONDICION, Format( 'where ( %s )', [ sFiltro ] ) )
     else
         Result := ZetaCommonTools.StrTransAll( sQuery, K_CONDICION, VACIO );
end;

{ ********* TCompanyList ********** }

constructor TCompanyList.Create;
begin
     FLista := TList.Create;
end;

destructor TCompanyList.Destroy;
begin
     Clear;
     FLista.Free;
     inherited Destroy;
end;

procedure TCompanyList.Clear;
var
   i: Integer;
begin
     with FLista do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Empresa[ i ].Free;
          end;
          Clear;
     end;
end;

function TCompanyList.Count: Integer;
begin
     Result := FLista.Count;
end;

function TCompanyList.GetEmpresa( Index: Integer ): TCompanyData;
begin
     with FLista do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TCompanyData( Items[ Index ] )
          else
              Result := nil;
     end;
end;

function TCompanyList.AgregaEmpresa: TCompanyData;
var
   oEmpresa: TCompanyData;
begin
     oEmpresa := TCompanyData.Create;
     FLista.Add( oEmpresa );
     Result := oEmpresa;
end;

procedure TCompanyList.LlenaLista( Lista: TStrings );
var
   i: Integer;
begin
     try
        with Lista do
        begin
             BeginUpdate;
             Clear;
        end;
        for i := 0 to ( FLista.Count - 1 ) do
        begin
             with Empresa[ i ] do
             begin
                  Lista.Add( Nombre );
             end;
        end;
     finally
            Lista.EndUpdate;
     end;
end;

{ ******** TTransferLog ********* }

procedure TTransferLog.Start( const sFileName: String );
begin
     if FileExists( sFileName ) then
     begin
          SysUtils.DeleteFile( sFileName );
     end;
     Init( sFileName );
end;

procedure TTransferLog.HandleException( const sMensaje: String; Error: Exception );
begin
     WriteTexto( sMensaje );
     WriteTexto( Error.ClassName + ' = ' + Error.Message );
end;

{ ******** TTablaData ********* }

constructor TTablaData.Create( Owner: TList; const sNombre: String );
begin
     FOwner := Owner;
     FNombre := sNombre;
     FDescripcion := sNombre;
     FTransfer := False;
     FClase := -1;
     FSeleccionada := TRUE;
     // FDiferencias := FALSE;
     FCampos := TStringList.Create;
     FLlaves := TStringList.Create;
     FDifferences := TStringList.Create;
     FError := TStringList.Create;
     FDelta := TStringList.Create;
     FComparables := TStringList.Create;
     FIgnorar := VACIO;
     FComputedFields := VACIO;
     FOwner.Add( Self );

     oZetaProviderSource := TdmZetaServerProvider.Create( nil );
     oZetaProviderMaster := TdmZetaServerProvider.Create( nil );
end;

destructor TTablaData.Destroy;
begin
     FComparables.Free;
     FDelta.Free;
     FDifferences.Free;
     FError.Free;
     FLlaves.Free;
     FCampos.Free;
     FOwner.Remove( Self );
     FQInsertDS.Free;
     FQRead.Free;
     FQReadDS.Free;
     FQSeek.Free;
     FQSeekDS.Free;

     oZetaProviderSource.Free;
     oZetaProviderMaster.Free;

     inherited Destroy;
end;

procedure TTablaData.GetTableInfo;
const
     Q_DICCION_GET_TABLA = 'select EN_TITULO AS DI_TITULO, EN_CODIGO AS DI_CALC from R_ENTIDAD where ( EN_TABLA = :Tabla )';
var
   FDataset: TZetaCursor;
   oCampos: TStringList;
   i: Integer;
begin
     oCampos := TStringList.Create;

     // Hacer que el provider se conecte a la empresa destino (FEmpresaActiva).
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaActiva, []);
     oZetaProviderMaster.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
          0]);

     // Hacer que el provider se conecte a la empresa Fuente (FEmpresaFuente).
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaFuente, []);
     oZetaProviderSource.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
          0]);

     with oZetaProviderMaster do
     begin
          FQFindTableDS := CreateQuery (Q_DICCION_GET_TABLA);
          with FQFindTableDS do
          begin
               Active := False;
               ParamAsString(FQFindTableDS, 'Tabla', Nombre);
               Active := True;
               if not IsEmpty then
               begin
                    FDescripcion := FieldByName( 'DI_TITULO' ).AsString;
                    FClase := FieldByName( 'DI_CALC' ).AsInteger;
               end;
               Active := False;
          end;

          FDataset := CreateQuery( Format ('SELECT * FROM %S', [Self.Nombre]) );
          try
             with FDataset do
             begin
                   Active := TRUE;
                   for i := 0 to Fields.Count - 1 do
                   begin
                        if NoEsCampoLLavePortal (Fields[i].FieldName) then
                            oCampos.Add( Fields[i].FieldName );
                   end;
             end;
          finally
             FreeAndNil( FDataset );
          end;
     end;

     FCampos.CommaText := oCampos.CommaText;
     FLlaves.CommaText := camposLlaveAsCommaText(Self.Nombre);
     GetFieldsDescriptions( FLlaves );
end;


function TTablaData.camposLlaveAsCommaText (sTabla: String): String;
const Q_GET_CAMPOS_LLAVE= 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE ' +
                                        ' WHERE ' +
                                          ' OBJECTPROPERTY(OBJECT_ID(constraint_name), ''IsPrimaryKey'') = 1 ' +
                                          ' AND table_name = ''%s'' ';
var cdsCamposLlave: TDataSet;
begin
    Result := '';

    // Hacer que el provider se conecte a la empresa destino (FEmpresaActiva).
    dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaActiva, []);
    oZetaProviderMaster.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
      dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
      dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
      0]);

    with oZetaProviderMaster do
    begin
        cdsCamposLlave := CreateQuery(Format (Q_GET_CAMPOS_LLAVE, [sTabla]));
        cdsCamposLlave.Active := TRUE;
        cdsCamposLlave.First;
        while not cdsCamposLlave.Eof do
        begin
            if not StrLleno (Result) then
              Result := cdsCamposLlave.FieldByName('COLUMN_NAME').AsString
            else
              Result := Result + ',' + cdsCamposLlave.FieldByName('COLUMN_NAME').AsString;
            cdsCamposLlave.Next;
        end;
    end;
end;

procedure TTablaData.BuildCamposComparables( const sComparables: String );
begin
     if ( sComparables <> VACIO ) then
     begin
          with CamposComparables do
          begin
               CommaText := sComparables;
          end;
          GetFieldsDescriptions( CamposComparables );
     end;
end;

procedure TTablaData.GetFieldsDescriptions( Lista: TStrings );
var
   i: Integer;
   sFieldName: String;
begin
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sFieldName := Strings[ i ];
               Strings[ i ] := Format( '%s=%s', [ sFieldName, GetFieldDescription( sFieldName ) ] );
          end;
     end;
end;

function TTablaData.GetFieldDescription( const sFieldName: String ): String;
const
     Q_DICCION_GET_CAMPO = 'select AT_TITULO AS DI_TITULO from R_ATRIBUTO where ( AT_CAMPO = :Campo ) and ( EN_CODIGO = :Clase )';
begin
     // Hacer que el provider se conecte a la empresa destino (FEmpresaActiva).
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaActiva, []);

     oZetaProviderMaster.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
          0]);

     // try
     with oZetaProviderMaster do
     begin
          FQFindFieldDS := CreateQuery (Q_DICCION_GET_CAMPO);
          with FQFindFieldDS do
           begin
                Active := False;
                ParamAsString(FQFindFieldDS, 'Campo', sFieldName);
                ParamAsInteger(FQFindFieldDS, 'Clase', Clase);
                Active := True;
                if IsEmpty then
                   Result := sFieldName
                else
                    Result := FieldByName( 'DI_TITULO' ).AsString;
                Active := False;
           end;
     end;
     {finally

     end;}
end;

function TTablaData.Comparar (CallBackDetalle: TShowProgressDetalle): Boolean;
var
   i: Integer;
   iPdDetalle: Integer;
   sValue: String;
begin
     Result := False;
     FDifferences.Clear;
     FError.Clear;
     FDelta.Clear;

     // Hacer que el provider se conecte a la empresa fuente
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaFuente, []);
     oZetaProviderSource.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
          0]);

     // Hacer que el provider se conecte a la empresa destino
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaActiva, []);
     oZetaProviderMaster.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
          0]);

      try
        FQReadDS := oZetaProviderSource.CreateQuery (GetQueryRead);
        PrepareQuerySeek;
        try
           with FQReadDS do
           begin
                Active := True;
                iPdDetalle := 0;
                // Progress bar detalle (pbDetalle)
                CallBackDetalle(RecordCount, 0);
                iPdDetalle := iPdDetalle + 1;
                while not Eof do
                begin
                     // Progress bar detalle (pbDetalle)
                     iPdDetalle := iPdDetalle + 1;
                     CallBackDetalle(RecordCount, iPdDetalle);
                     if RowExists then
                     begin
                        if CompareFieldValues then
                            Result := TRUE;
                     end
                     else
                     begin
                          Result := True;
                          NotifyDelta;
                     end;
                     Next;
                end;
                Active := False;
           end;
        except
              On Error : Exception do
              begin
                   {if Transfer then
                      FdmCompara.TxRollBack;}
                   raise;
              end;
        end;
      except
           on Error: Exception do
           begin
                HandleException( 'Error al comparar', Error );
                FError.Text := Error.Message;
           end;
      end;

     if ( FDifferences.Count > 0 ) or ( FDelta.Count > 0 ) then
     begin
          with Log do
          begin
               WriteTexto( StringOfChar( '=', K_ANCHO_LINEA ) );
               WriteTexto( Format( 'TABLA %s ( %s )', [ Nombre, Descripcion ] ) );
               with Llaves do
               begin
                    sValue := 'Llave primaria: ';
                    for i := 0 to ( Count - 1 ) do
                    begin
                         WriteTexto( sValue + Strings[ i ] );
                         sValue := StringOfChar( ' ', Length( sValue ) );
                    end;
               end;
               WriteTexto( StringOfChar( '=', K_ANCHO_LINEA ) );
               with FDifferences do
               begin
                    if ( Count > 0 ) then
                    begin
                         WriteTexto( 'Registros diferentes en base de datos destino ' + Format( K_DELTA_VALUES, [ '<Valor Destino>', '<Valor Fuente>' ] ) );
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                         for i := 0 to ( Count - 1 ) do
                         begin
                              WriteTexto( Strings[ i ] );
                         end;
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                    end;
               end;
               with FDelta do
               begin
                    if ( Count > 0 ) then
                    begin
                         {if Transfer then
                            WriteTexto( 'Registros Agregados A Empresa Maestra' )
                         else}
                             // WriteTexto( 'Registros No Existentes En Empresa Maestra' );
                             WriteTexto( 'Registros no existentes en base de datos destino' );
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                         for i := 0 to ( Count - 1 ) do
                         begin
                              WriteTexto( Strings[ i ] );
                         end;
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                    end;
               end;
               WriteTexto( VACIO );
               WriteTexto( VACIO );
          end;
     end;
end;

procedure TTablaData.PrepareQuerySeek;
var
   sCampos: String;
begin
     if ( FComparables.Count > 0 ) then
        sCampos := GetNamesList( FComparables )
     else
         sCampos := GetNamesList( FLlaves );

     // Hacer que el provider se conecte a la empresa destino (FEmpresaActiva).
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaActiva, []);
     oZetaProviderMaster.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
          dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
          0]);

     FQSeekDS := oZetaProviderMaster.CreateQuery( Format( 'select %s from %s where %s', [ sCampos, Nombre, BuildPrimaryKeyFilter ] ) );
end;

function TTablaData.GetQueryRead: String;
begin
     Result := Format( 'select %s from %s order by %s', [ FCampos.CommaText, Nombre, GetNamesList( FLlaves ) ] );
end;

function TTablaData.GetNamesList( Lista: TStrings ): String;
var
   i: Integer;
begin
     Result := VACIO;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
              Result := Result + Names[ i ] + ','
          end;
     end;
     Result := ZetaCommonTools.CortaUltimo( Trim( Result ) );
end;

function TTablaData.BuildPrimaryKeyFilter: String;
var
   i: Integer;
   sCampo: String;
begin
     Result := VACIO;
     with FLlaves do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sCampo := Names[ i ];
               Result := ZetaCommonTools.ConcatFiltros( Result, Format( '( %s= :%s )', [ sCampo, sCampo ] ) );
          end;
     end;
end;

function TTablaData.CompareFieldValues: Boolean;
var
   i, iPos: Integer;
   sFieldName, sDescription: String;
   Master, Source: TField;
   lNotEqual: Boolean;

procedure AddSingleRow( sMaster, sSource: String );
begin
     sMaster := Trim( sMaster );
     sSource := Trim( sSource );
     with FDifferences do
     begin
          if ( Length( sMaster + sSource ) > 40 ) then
          begin
               Add( Format( '%s ( %s ):', [ sDescription, sFieldName ] ) );
               Add( Format( 'Destino: %s', [ sMaster ] ) );
               Add( Format( 'Fuente:  %s', [ sSource ] ) );
          end
          else
              Add( Format( '%s ( %s ): ', [ sDescription, sFieldName ] ) + Format( K_DELTA_VALUES, [ sMaster, sSource ] ) );
     end;
end;

begin
     Result := False;
     iPos := FDifferences.Count;
     with CamposComparables do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sFieldName := Names[ i ];
               // Agregar aqu� el no considerar el CampoLlavePortal en la Comparaci�n de Tablas y Cat�logos
               if NoEsCampoLLavePortal(sFieldName) then
               begin             
                   sDescription := Values[ sFieldName ];
                   Master := FQSeekDS.FieldByName( sFieldName );
                   Source := FQReadDS.FieldByName( sFieldName );
                   if Assigned( Master ) and Assigned( Source ) then
                   begin
                        lNotEqual := False;
                        case Master.DataType of
                             ftString:
                             begin
                                  if ( Trim (Source.AsString) <> Trim (Master.AsString) ) then
                                  begin
                                       lNotEqual := True;
                                       AddSingleRow( Master.AsString, Source.AsString );
                                  end;
                             end;
                             ftSmallInt, ftInteger, ftWord:
                             begin
                                  if ( Source.AsInteger <> Master.AsInteger ) then
                                  begin
                                       lNotEqual := True;
                                       AddSingleRow( Master.AsString, Source.AsString );
                                  end;
                             end;
                             ftBoolean:
                             begin
                                  if ( Source.AsBoolean <> Master.AsBoolean ) then
                                  begin
                                       lNotEqual := True;
                                       AddSingleRow( ZetaCommonTools.zBoolToStr( Master.AsBoolean ),
                                                     ZetaCommonTools.zBoolToStr( Source.AsBoolean ) );
                                  end;
                             end;
                             ftFloat:
                             begin
                                  if ( Source.AsFloat <> Master.AsFloat ) then
                                  begin
                                       lNotEqual := True;
                                       AddSingleRow( Master.AsString, Source.AsString );
                                  end;
                             end;
                             ftCurrency:
                             begin
                                  if ( Source.AsCurrency <> Master.AsCurrency ) then
                                  begin
                                       lNotEqual := True;
                                       AddSingleRow( Master.AsString, Source.AsString );
                                  end;
                             end;
                             ftDate, ftTime, ftDateTime:
                             begin
                                  if ( Source.AsDateTime <> Master.AsDateTime ) then
                                  begin
                                       lNotEqual := True;
                                       AddSingleRow( FormatDateTime( 'dd/mmm/yyyy', Master.AsDateTime ),
                                                     FormatDateTime( 'dd/mmm/yyyy', Source.AsDateTime ) );
                                  end;
                             end;
                             ftMemo:
                             begin
                                  if ( Source.AsString <> Master.AsString ) then
                                  begin
                                       lNotEqual := True;
                                       AddSingleRow( Master.AsString, Source.AsString );
                                  end;
                             end;
                        end;
                        if lNotEqual then
                        begin
                             Result := True;
                        end;
                   end;
               end; // termina if NoEsCampoLLavePortal(sFieldName)
          end; // termina for
     end; // termina with

     if Result then
     begin
          with FDifferences do
          begin
               Insert( iPos, GetPrimaryKeyValues( FQReadDS ) );
               Insert( iPos, ' ' );
          end;
     end;
end;

function TTablaData.GetPrimaryKeyValues( Source: TDataSet ): String;
var
   i: Integer;
   sFieldName, sValue: String;
begin
     Result := VACIO;
     with FLlaves do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sFieldName := Names[ i ];
               with Source.FieldByName( sFieldName )do
               begin
                    case DataType of
                         ftDate: sValue := FormatDateTime( 'dd/mmm/yyyy', AsDateTime );
                         ftDateTime: sValue := FormatDateTime( 'dd/mmm/yyyy', AsDateTime );
                         ftTime: sValue := FormatDateTime( 'dd/mmm/yyyy', AsDateTime );
                    else
                        sValue := AsString;
                    end;
               end;
               Result := Result + Format( '%s=%s, ', [ sFieldName, sValue ] );
          end;
     end;
     Result := ZetaCommonTools.CortaUltimo( Trim( Result ) );
end;

function TTablaData.RowExists: Boolean;
begin
     with FQSeekDS do
     begin
          Active := False;
          LoadPrimaryKey;
          Active := True;
          Result := not IsEmpty;
     end;
end;

procedure TTablaData.LoadPrimaryKey;
var
   i: Integer;
   sCampo: String;
begin
     with FLlaves do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sCampo := Names[ i ];
               (TADOQuery(FQSeekDS).Parameters).FindParam (sCampo).Value := FQReadDS.FieldByName( sCampo ).Value;
          end;
     end;
end;

procedure TTablaData.TransferFieldValues;
begin
    try
      FieldsToParams;
      with FQInsertDS do
      begin
           oZetaProviderMaster.Ejecuta(FQInsertDS);
      end;
      NotifyDelta;
    except
       on Error: Exception do
       begin
            if NOT ( PK_Violation(Error)) then
            begin
                 HandleException( 'Error al transferir', Error );
            end;
       end;
    end;
end;

procedure TTablaData.FieldsToParams;
var
  i: Integer;
  sFieldName: String;

  function EsCampoValido( oCampo: TField ): Boolean;
  begin
       Result := NoEsCampoLLavePortal( oCampo ) and ( oCampo.DataType <> ftAutoInc ); 
  end;

begin
     with FQReadDS do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               sFieldName := Fields[ i ].FieldName;
               if ( Pos( sFieldName, FIgnorar ) = 0 ) and ( Pos( sFieldName, FComputedFields ) = 0 ) and
                  (EsCampoValido( Fields[i] )) then
               begin
                    (TADOQuery(FQInsertDS).Parameters).FindParam (sFieldName).Value := FQReadDS.FieldByName( sFieldName ).Value;
               end;
          end;
     end;
end;

procedure TTablaData.NotifyDelta;
begin
     FDelta.Add( GetPrimaryKeyValues( FQReadDS ) );
end;

procedure TTablaData.HandleException( const sMensaje: String; Error: Exception );
begin
     Log.HandleException( Format( '%s ( %s ): %s', [ Descripcion, Nombre, sMensaje ] ), Error );
end;

function TTablaData.GetNombreReal: String;
begin
     Result := 'DBO.' + Nombre;
end;

procedure TTablaData.Connect( DataModule: TdmConsolida );
begin
     FdmCompara := Datamodule;
     FDBSourceName := Datamodule.DBSourceName;
end;

function TTablaData.TieneDiferencias: Boolean;
begin
     Result := FALSE;
     if StrLleno (FDifferences.Text) then
        Result := TRUE;
end;

function TTablaData.TieneError: Boolean;
begin
     Result := FALSE;
     if StrLleno (FError.Text) then
        Result := TRUE;
end;

function TTablaData.FaltanRegistros: Boolean;
begin
     Result := FALSE;
     if StrLleno (FDelta.Text) then
        Result := TRUE;
end;

procedure TTablaData.PrepareQueryInsert;
begin
     FQInsertDS:= oZetaProviderMaster.CreateQuery (GetQueryInsert);
end;

function TTablaData.GetQueryInsert: String;
var
   i: Integer;
   sCampos, sValues: String;

  function EsCampoValido( oCampo: TField ): Boolean;
  begin
       Result := NoEsCampoLLavePortal( oCampo ) and ( oCampo.DataType <> ftAutoInc ); 
  end;

begin
     sCampos := VACIO;
     sValues := VACIO;
     with FQReadDS do
     begin
          for i := 0 to ( Fields.Count - 1 ) do
          begin
               if ( Pos( Fields[ i ].FieldName, FIgnorar ) = 0 ) and ( Pos( Fields[ i ].FieldName, FComputedFields ) = 0 ) and
                  (EsCampoValido( Fields[i] )) then
               begin
                    sCampos := sCampos + Fields[ i ].FieldName + ',';
                    sValues := sValues + ':' + Fields[ i ].FieldName + ',';
               end;
          end;

          sCampos := ZetaCommonTools.CortaUltimo( sCampos );
          sValues := ZetaCommonTools.CortaUltimo( sValues );
     end;

     Result := Format( 'insert into %s ( %s ) values ( %s )', [ Nombre, sCampos, sValues ] );
end;

function TTablaData.Transferir: Boolean;
var
   i: Integer;
   sValue: String;
begin
    Result := False;
    // FDifferences.Clear;
    FDelta.Clear; // Se limpia FDelta para volver a informar de registros faltantes pero que se transfieren llegando este paso.
    try
      FQReadDS := oZetaProviderSource.CreateQuery (GetQueryRead);
      PrepareQuerySeek;
      try
         with FQReadDS do
         begin
              Active := True;
              PrepareQueryInsert;
              while not Eof do
              begin
                   if not RowExists then
                   begin
                        Result := True;
                        TransferFieldValues;
                   end;
                   Next;
              end;
              Active := False;
         end;
      except
          On Error : Exception do
          begin
               // if Transfer then // ?? Si se llega ac�, es porque ya se sabe que se trata de una tabla seleccionada para Transferir
                  // FdmCompara.TxRollBack;
               // raise;
          end;
      end;
    except
         on Error: Exception do
         begin
              HandleException( 'Error al transferir', Error );
              FError.Text := Error.Message;
         end;
    end;

     // if ( FDifferences.Count > 0 ) or ( FDelta.Count > 0 ) then
     if FDelta.Count > 0 then
     begin
          with Log do
          begin
               WriteTexto( StringOfChar( '=', K_ANCHO_LINEA ) );
               WriteTexto( Format( 'TABLA %s ( %s )', [ Nombre, Descripcion ] ) );
               with Llaves do
               begin
                    sValue := 'Llave primaria: ';
                    for i := 0 to ( Count - 1 ) do
                    begin
                         WriteTexto( sValue + Strings[ i ] );
                         sValue := StringOfChar( ' ', Length( sValue ) );
                    end;
               end;
               WriteTexto( StringOfChar( '=', K_ANCHO_LINEA ) );
               with FDelta do
               begin
                    if ( Count > 0 ) then
                    begin
                         WriteTexto( 'Registros agregados a base de datos destino' );
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                         for i := 0 to ( Count - 1 ) do
                         begin
                              WriteTexto( Strings[ i ] );
                         end;
                         WriteTexto( StringOfChar( '-', K_ANCHO_LINEA ) );
                    end;
               end;
               WriteTexto( VACIO );
               WriteTexto( VACIO );
          end;
     end;
     Transfer := Result;
end;

procedure TTablaData.TransferEmployee( const iOldEmp, iNewEmp: TNumEmp; sNivel0: String );
begin
    FQFindTableDS := oZetaProviderMaster.CreateQuery (GetQueryTransfer( iOldEmp, iNewEmp, sNivel0 ));
    try
        oZetaProviderMaster.Ejecuta(FQFindTableDS);
    except
       on Error: EDBEngineError do
       begin
            { Si el registro ya existe, hay que seguir transfiriendo }
            with Error do
            begin
               if ( ErrorCount > 0 ) and ( Errors[ 0 ].ErrorCode = DBIERR_KEYVIOL ) then
               begin
                    Log.WriteTexto( Format( 'Registro ya existe en %s: %s', [ Nombre ] ) );
               end
               else
               begin
                    { En caso contrario, se interrumpe el proceso }
                    raise;
               end;
            end;
       end;
       on Error: Exception do
       begin
            raise;
       end;
    end;
end;

function TTablaData.GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp; FNivel0: String = '' ): String;
const
     K_NUM_EMP = 'CB_CODIGO';
     K_NIVEL_0 = 'CB_NIVEL0';
var
   i: Integer;
   sCampos, sValues: String;

begin
     sValues := VACIO;
     with FCampos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Pos( Strings[ i ], FIgnorar ) = 0 ) and ( Pos( Strings[ i ], FComputedFields ) = 0 )
                  and ( Pos( Strings[ i ], 'LLAVE' ) = 0 ) then
               begin
                    sValues := sValues + Strings[ i ] + ',';
               end;
          end;
     end;
     sValues := ZetaCommonTools.CortaUltimo( sValues );
     sCampos := sValues;
     if ( iNewEmp <> iOldEmp ) then
        sCampos := StrTransform( sCampos, K_NUM_EMP, Format( '%d %s', [ iNewEmp, K_NUM_EMP ] ) );

     // Tema del Nivel0
     if ZetaCommonTools.StrLleno( FNivel0 ) then
     begin
        sCampos := StrTransform( sCampos, K_NIVEL_0, Format( '%s %s', [ QuotedStr( FNivel0 ), K_NIVEL_0 ] ) );
     end;

     Result := Format( 'insert into %0:s ( %1:s ) select %2:s from %3:s.%0:s where ( CB_CODIGO = %4:d )', [ NombreReal, sValues, sCampos, FdbSourceName, iOldEmp ] );
end;

{ ********* TNomParam ********** }

procedure TNomParam.BuildPrimaryKeyList;
begin
     with Llaves do
     begin
          Clear;
          Add( 'NP_NOMBRE' );
     end;
     GetFieldsDescriptions( Llaves );
end;

function TNomParam.Comparar(CallBackDetalle: TShowProgressDetalle): Boolean;
begin
     FQFolioDS := oZetaProviderMaster.CreateQuery ('select MAX( NP_FOLIO ) MAXIMO from NOMPARAM');
     Result := inherited Comparar (CallBackDetalle);
end;

procedure TNomParam.TransferFieldValues;
var
   iFolio: Integer;
begin
     with FQFolioDS do
     begin
          Active := True;
          iFolio := Fields[ 0 ].AsInteger;
          Active := False;
     end;
     try
        FieldsToParams;
        (TADOQuery(FQInsertDS).Parameters).FindParam ('NP_FOLIO').Value := iFolio + 1;

        oZetaProviderMaster.Ejecuta(FQInsertDS);

        NotifyDelta;
     except
           on Error: Exception do
           begin
                HandleException( 'Error al transferir', Error );
           end;
     end;
end;

{ ********* TGlobal ************* }

function TGlobal.GetPrimaryKeyValues( Source: TDataSet ): String;
begin
     with Source do
     begin
          Result := Format( '%d .- %s', [ FieldByName( 'GL_CODIGO' ).AsInteger, FieldByName( 'GL_DESCRIP' ).AsString ] );
     end;
end;

{ ********* TCatalogo ************* }

function TCatalogo.GetQueryRead: String;
const
     K_CONDITION = 'KARDEX.%s = %s.%s';
     K_COUNTERS = '( select COUNT(*) from KARDEX where ( %s ) and ' +
                  '( ( select CB_ACTIVO from COLABORA where COLABORA.CB_CODIGO = KARDEX.CB_CODIGO ) = ''S'' ) ) ACTIVOS, '+
                  '( select COUNT(*) from KARDEX where ( %s ) and '+
                  '( ( select CB_ACTIVO from COLABORA where COLABORA.CB_CODIGO = KARDEX.CB_CODIGO ) <> ''S'' ) ) INACTIVOS';
var
   sCondition, sCounters: String;
begin
     Ignorar := 'ACTIVOS;INACTIVOS';
     sCondition := Format( K_CONDITION, [ FKardexKeyField, Nombre, FLlaves.Names[ 0 ] ] );
     sCounters := Format( K_COUNTERS, [ sCondition, sCondition ] );
     Result := GetQueryFiltro( Format( 'select %s,%s from %s %s order by %s', [ FCampos.CommaText, sCounters, NombreReal, K_CONDICION, GetNamesList( FLlaves ) ] ), FFiltro );
end;

function TCatalogo.GetPrimaryKeyValues( Source: TDataSet ): String;
const
     K_MENSAJE = '%s ( Empleados Activos: %.0n, Inactivos: %.0n )';
begin
     with Source do
     begin
          Result := Format( K_MENSAJE, [ inherited GetPrimaryKeyValues( Source ),
                                         FieldByName( 'ACTIVOS' ).AsFloat,
                                         FieldByName( 'INACTIVOS' ).AsFloat ] );
     end;
end;


{ ********* TColabora ************* }

function TColabora.CambiaNivel0( const sScript: String ): String;
const
     K_NIVEL_0 = 'CB_NIVEL0';
begin
     Result := sScript;
     if ZetaCommonTools.StrLleno( FNivel0 ) then
        Result := StrTransform( Result, K_NIVEL_0, Format( '%s %s', [ QuotedStr( FNivel0 ), K_NIVEL_0 ] ) );
end;


function TColabora.GetQueryRead: String;
begin
     if StrLleno (Self.Filtro) then
        Self.Filtro := ' WHERE ' + Self.Filtro;
     Result := Format( 'select %s from %s %s order by %s', [ FCampos.CommaText, Nombre, Self.Filtro, GetNamesList( FLlaves ) ] );
end;

function TColabora.GetQueryTransfer( const iOldEmp, iNewEmp: TNumEmp; FNivel0: String = '' ): String;
begin
     // Result := CambiaNivel0( inherited GetQueryTransfer( iOldEmp, iNewEmp, FNivel0 ) );
     Result := inherited GetQueryTransfer( iOldEmp, iNewEmp, FNivel0 );
end;

{ ******** TdmConsolida ********* }

procedure TdmConsolida.DataModuleCreate(Sender: TObject);
begin
     FTablas := TList.Create;
     FEmpresas := TCompanyList.Create;
     FNivel0 := TStringList.Create;
     oZetaProvider := TdmZetaServerProvider.Create(nil);
     FParamMsg := TStringList.Create;
end;

procedure TdmConsolida.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FNivel0 );
     FreeAndNil( FTablas );
     FreeAndNil( FEmpresas );
     oZetaProvider.Free;
     FParamMsg.Free;
end;

function TdmConsolida.GetEmpresas(Index: Integer): TCompanyData;
begin
     Result := FEmpresas.Empresa[ Index ];
end;

function TdmConsolida.GetEmpresasCount: Integer;
begin
     Result := FEmpresas.Count;
end;

function TdmConsolida.GetTablas( Index: Integer ): TTablaData;
begin
     Result := TTablaData( FTablas.Items[ Index ] );
end;

function TdmConsolida.GetTablasPorNombre( Nombre: String ): TTablaData;
var i: Integer;
begin
     Result := nil;
     for i := 0 to TablasCount-1 do
     begin
        if Tablas[i].Nombre = Nombre then
          Result := TTablaData( FTablas.Items[ i ] );
     end;
end;

function TdmConsolida.GetTablasCount: Integer;
begin
     Result := FTablas.Count;
end;

function TdmConsolida.GetTablasCountSeleccionadas: Integer;
var i: Integer;
begin
     Result := 0;
     for i := 0 to FTablas.Count-1 do
     begin
        if Tablas[i].Seleccionada then
            Result := Result + 1;
     end;
end;

function TdmConsolida.GetTablasTransferirCountSeleccionadas: Integer;
var i: Integer;
begin
     Result := 0;
     for i := 0 to FTablas.Count-1 do
     begin
        if Tablas[i].Transfer then
            Result := Result + 1;
     end;
end;

function TdmConsolida.GetTablasConDiferencias: Integer;
var i: Integer;
begin
     Result := 0;
     for i := 0 to FTablas.Count-1 do
     begin
        if StrLleno (Tablas[i].FDifferences.Text) then
            Result := Result + 1;
     end;
end;

function TdmConsolida.GetTablasConFaltantes: Integer;
var i: Integer;
begin
     Result := 0;
     for i := 0 to FTablas.Count-1 do
     begin
        if StrLleno (Tablas[i].FDelta.Text) then
            Result := Result + 1;
     end;
end;

function TdmConsolida.GetTablasConError: Integer;
var i: Integer;
begin
     Result := 0;
     for i := 0 to FTablas.Count-1 do
     begin
        if StrLleno (Tablas[i].FError.Text) then
            Result := Result + 1;
     end;
end;

procedure TdmConsolida.ClearTablas;
var
   i: Integer;
begin
     for i := ( TablasCount - 1 ) downto 0 do
     begin
          try
            Tablas[ i ].Free;
          except
            // Tablas[i].FError.Clear;
            // Tablas[i].FDifferences.Clear;
          end;
     end;
end;

procedure TdmConsolida.InitEmpresas( Fuente, Destino: String );
var
   sServer: String;
begin
      LlenaListaCompaniasFuente( FEmpresas  );
      FEmpresaFuente := Fuente;
      FEmpresaActiva := Destino;
      LeeListaNivel0( FNivel0 );

     // Asignar Empresa Fuente a propiedad DBSourceName.
     // Hacerlo por medio de un locate a dmSistema.cdsSistBaseDatos y un GetServerDatabase.
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaFuente, []);
     GetServerDatabase(dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString, sServer, FDBSourceName);
end;

procedure TdmConsolida.LlenaListaCompaniasFuente( Lista: TCompanyList  );
var
   FDataset: TZetaCursor;
   SQL: TStrings;
begin
     SQL := TStringList.Create;
     try
         with oZetaProvider do
         begin
              oZetaProvider.EmpresaActiva := Comparte;
              with SQL do
              begin
                    Clear;
                    Add( 'SELECT DB_CODIGO, DB_DESCRIP, '''' AS CM_ALIAS, DB_USRNAME, DB_PASSWRD, 0 AS CM_EMPATE, DB_DATOS ' );
                        Add( ' FROM DB_INFO ORDER BY DB_CODIGO');
              end;
              FDataset := CreateQuery (SQL.Text);
         end;

         Lista.Clear;
         with FDataset do
         begin
             Active := True;
             while not Eof do
             begin
                  with Lista.AgregaEmpresa do
                  begin
                       Codigo := FieldByName( 'DB_CODIGO' ).AsString;
                       Nombre := FieldByName( 'DB_DESCRIP' ).AsString;
                       Alias := FieldByName( 'CM_ALIAS' ).AsString;
                       Datos := FieldByName( 'DB_DATOS' ).AsString;
                       UserName := FieldByName( 'DB_USRNAME' ).AsString;
                       Password := ZetaServerTools.Decrypt( FieldByName( 'DB_PASSWRD' ).AsString );
                       Empate := FieldByName( 'CM_EMPATE' ).AsInteger;
                  end;
                  Next;
             end;
         end;
     finally
          FreeAndNil (SQL);
     end;
end;

procedure TdmConsolida.LeeListaNivel0( Lista: TStrings );
var dsConsulta: TDataSet;
begin
     oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;
     with Lista do
     begin
         try
             BeginUpdate;
             Clear;
             with dsConsulta do
             begin
                 try
                      dsConsulta := oZetaProvider.CreateQuery ('select TB_CODIGO, TB_ELEMENT from NIVEL0 order by TB_CODIGO');
                      Active := TRUE;
                      while not Eof do
                      begin
                          Lista.Add( FieldByName( 'TB_CODIGO' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString );
                          Next;
                      end;
                 finally
                    Active := False;
                 end;
             end;
         finally
            EndUpdate;
            FreeAndNil (dsConsulta);
         end;
     end;
end;

procedure TdmConsolida.InitTablas;

    procedure Add( const sNombre, sComparables: String );
    begin
         with TTablaData.Create( FTablas, sNombre ) do
         begin
              Connect( Self );
              GetTableInfo;
              if ( sComparables = K_TODOS ) then
                 BuildCamposComparables( Campos.CommaText )
              else
                  BuildCamposComparables( sComparables );
         end;
    end;

    procedure Cat( const sNombre, sKardexField, sFiltro, sComparables: String );
    begin
         with TCatalogo.Create( FTablas, sNombre ) do
         begin
              Connect( Self );
              KardexKeyField := sKardexField;
              Filtro := sFiltro;
              GetTableInfo;
              if ( sComparables = K_TODOS ) then
                 BuildCamposComparables( Campos.CommaText )
              else
                  BuildCamposComparables( sComparables );
         end;
    end;

    procedure Nivel( const iNivel: Integer );
    const
         K_NIVEL_FIELD_LIST = 'TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO';
    begin
         Cat( Format( 'NIVEL%d', [ iNivel ] ), Format( 'CB_NIVEL%d', [ iNivel ] ), VACIO, K_NIVEL_FIELD_LIST );
    end;

begin
    ClearTablas;
    { Esta lista est� en orden descendente de dependencias, es decir }
    { las tablas hijas van primero que las padres }
    { Para copiar, es necesario procesarla de abajo para arriba }

    Add( 'CONCEPTO', 'CO_A_PTU,CO_ACTIVO,CO_CALCULA,CO_FORMULA,CO_G_IMSS,CO_G_ISPT,CO_IMP_CAL,CO_IMPRIME,CO_LISTADO,CO_MENSUAL,CO_QUERY,CO_RECIBO,CO_TIPO,CO_X_ISPT,CO_ISN' );
    Add( 'ART_80', VACIO );
    Add( 'T_ART_80', VACIO );
    Add( 'NUMERICA', VACIO );
    Add( 'CAFREGLA', VACIO );
    Add( 'INVITA', VACIO );
    Add( 'ENTNIVEL', VACIO );
    Add( 'ENTRENA', VACIO );
    Add( 'CALCURSO', VACIO );
    Add( 'CUR_REV',VACIO);
    Add( 'CCURSO',VACIO);
    Add( 'CURSOPRE',VACIO);
    Add( 'CURSO', VACIO );
    Add( 'MAESTRO', VACIO );
    Cat( 'CLASIFI', 'CB_CLASIFI', VACIO, 'TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_SALARIO,TB_OP1,TB_OP2,TB_OP3,TB_OP4,TB_OP5' );
    Add( 'CONTRATO', VACIO );
    Add( 'EDOCIVIL', VACIO );
    Add( 'ENTIDAD', VACIO );
    Add( 'PTOFIJAS',VACIO);
    Add( 'PTOTOOLS',VACIO);

    // Se cambia orden de PUESTO
    // Se debe agregar despu�s de PTO_CERT.
    // Revisar l�neas abajo
    // Cat( 'PUESTO', 'CB_PUESTO', VACIO, VACIO );

    Add( 'ESTUDIOS', VACIO );
    Add( 'EVENTO', VACIO );
    Add( 'EXTRA1', VACIO );
    Add( 'EXTRA2', VACIO );
    Add( 'EXTRA3', VACIO );
    Add( 'EXTRA4', VACIO );
    Add( 'LIQ_IMSS', VACIO );
    Add( 'PERIODO', 'PE_USO,PE_INC_BAJ,PE_SOLO_EX,PE_FEC_INI,PE_FEC_FIN,PE_FEC_PAG,'+
                   {$ifdef QUINCENALES}
                   'PE_ASI_INI,PE_ASI_FIN,'+
                   {$endif}
                   'PE_MES,PE_DIAS,PE_DIAS_AC,PE_DIA_MES,PE_POS_MES,PE_PER_MES,PE_PER_TOT,PE_AHORRO,PE_PRESTAM' );
    Add( 'FESTIVO', VACIO );

    Cat( 'TURNO', 'CB_TURNO', VACIO, 'TU_DESCRIP,TU_DIAS,TU_DOBLES,TU_DOMINGO,TU_FESTIVO,TU_HOR_1,TU_HOR_2,TU_HOR_3,TU_HOR_4,TU_HOR_5,TU_HOR_6,TU_HOR_7,TU_HORARIO,TU_JORNADA,TU_NOMINA,TU_RIT_INI,TU_RIT_PAT,TU_TIP_1,TU_TIP_2,' + 'TU_TIP_3,TU_TIP_4,TU_TIP_5,TU_TIP_6,TU_TIP_7,TU_TIP_JOR,TU_INGLES,TU_TEXTO,TU_NUMERO' );
    Add( 'HORARIO', 'HO_DESCRIP,HO_ADD_EAT,HO_CHK_EAT,HO_COMER,HO_DOBLES,HO_EXT_FIJ,HO_IGN_EAT,HO_IN_EAT,HO_IN_TARD,HO_IN_TEMP,HO_INTIME,HO_LASTOUT,HO_MIN_EAT,HO_JORNADA,HO_OU_TARD,HO_OU_TEMP,HO_OUT_EAT,HO_OUTTIME,HO_TIPO,HO_EXT_COM,HO_EXT_MIN,HO_OUT_BRK,HO_IN_BRK' );
    Add( 'ORDFOLIO', VACIO );
    Add( 'FOLIO', VACIO );
    with TGlobal.Create( FTablas, 'GLOBAL' ) do
    begin
        Connect( Self );
        GetTableInfo;
        BuildCamposComparables( 'GL_DESCRIP,GL_FORMULA,GL_TIPO' );
    end;
    Add( 'INCIDEN', VACIO );
    Add( 'LEY_IMSS', VACIO );
    Add( 'MONEDA', VACIO );
    Add( 'MOT_AUTO', VACIO );
    Add( 'MOT_BAJA', VACIO );
    Nivel( 1 );
    Nivel( 2 );
    Nivel( 3 );
    Nivel( 4 );
    Nivel( 5 );
    Nivel( 6 );
    Nivel( 7 );
    Nivel( 8 );
    Nivel( 9 );
    {$ifdef ACS}
    Nivel( 10 );
    Nivel( 11 );
    Nivel( 12 );
    {$endif}
    with TNomParam.Create( FTablas, 'NOMPARAM' ) do
    begin
        Connect( Self );
        GetTableInfo;
        BuildPrimaryKeyList;
        BuildCamposComparables( 'NP_FORMULA,NP_TIPO,NP_ACTIVO' );
    end;
    Add( 'OTRASPER', VACIO );
    Add( 'PRESTACI', VACIO );
    Add( 'SSOCIAL', VACIO );
    Add( 'PRIESGO', VACIO );
    Add( 'RPATRON', VACIO );
    Add( 'QUERYS', VACIO );
    Add( 'TAHORRO', VACIO );
    Add( 'RIESGO', VACIO );
    Add( 'SAL_MIN', VACIO );
    Add( 'SUPER', VACIO );
    Add( 'TCAMBIO', VACIO );
    Add( 'TCURSO', VACIO );
    Add( 'TKARDEX', VACIO );
    Add( 'TPERIODO', VACIO );
    Add( 'TPRESTA', VACIO );
    Add( 'TRANSPOR', VACIO );
    Add( 'VIVE_CON', VACIO );
    Add( 'VIVE_EN', VACIO );
    Add( 'TALLA', VACIO );
    Add( 'MOT_TOOL', VACIO );
    Add( 'TOOL', VACIO );

    Add( 'BRK_HORA', VACIO );
    Add( 'BREAKS', VACIO );
    Add( 'SUP_AREA', VACIO );
    Add( 'AREA', VACIO );
    Add( 'MODULA1', VACIO );
    Add( 'MODULA2', VACIO );
    Add( 'MODULA3', VACIO );
    Add( 'TMUERTO', VACIO );
    Add( 'TOPERA', VACIO );
    Add( 'OPERA', VACIO );
    Add( 'TPARTE', VACIO );
    Add( 'DEFSTEPS', VACIO );
    Add( 'PARTES', VACIO );

    Add( 'ACCREGLA',VACIO);
    Add( 'COLONIA',VACIO);
    Add( 'EXTRA5',VACIO);
    Add( 'EXTRA6',VACIO);
    Add( 'EXTRA7',VACIO);
    Add( 'EXTRA8',VACIO);
    Add( 'EXTRA9',VACIO);
    Add( 'EXTRA10',VACIO);
    Add( 'EXTRA11',VACIO);
    Add( 'EXTRA12',VACIO);
    Add( 'EXTRA13',VACIO);
    Add( 'EXTRA14',VACIO);

    // Cambio en orden de las siguientes 2 tablas
    // Antes: REGLAPREST -> PRESTAXREG
    // Ahora: PRESTAXREG -> REGLAPREST
    Add( 'PRESTAXREG',VACIO);
    Add( 'REGLAPREST',VACIO);

    Add( 'AULA',VACIO);
    Add( 'POL_TIPO',VACIO);
    Add( 'CERNIVEL',VACIO);
    Add( 'PTO_CERT',VACIO);

    // Se cambia orden de PUESTO
    Cat( 'PUESTO', 'CB_PUESTO', VACIO, VACIO );

    Add( 'CERTIFIC',VACIO);
    Add( 'TCTAMOVS',VACIO);
    Add( 'CTABANCO',VACIO);
    Add( 'PROV_CAP',VACIO);
    Add( 'RSOCIAL',VACIO);
    Add( 'OCUPA_NAC',VACIO);
    Add( 'MOT_CHECA',VACIO);
    Add( 'AR_TEM_CUR',VACIO);
    Add( 'ESTABLEC',VACIO);
    Add( 'MUNICIPIO',VACIO);
    Add( 'TPENSION','TP_NOMBRE,TP_INGLES,TP_NUMERO,TP_TEXTO,TP_PERCEP,TP_PRESTA,TP_CO_PENS,TP_CO_DESC,TP_APL_NOR,TP_APL_LIQ,TP_APL_IND');
end;

function TdmConsolida.CompararTablas( CallBack: TShowProgress; Bitacora: TTransferLog;
                        CallBackDetalle: TShowProgressDetalle ): Boolean;
var
   i: Integer;
   valorCallBack: Integer;
   lContinue: Boolean;
begin
     Result := False;
     lContinue := True;
     { La Lista Tablas est� en orden descendente de dependencias, es decir }
     { las tablas hijas van primero que las padres }
     { Es necesario procesarla de abajo para arriba }
     with Bitacora do
     begin
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          WriteTexto( 'Comparaci�n de Tablas' );
          with EmpresaActiva do
          begin
               WriteTexto( 'Base de datos destino: ' + Nombre );
          end;
          with EmpresaFuente do
          begin
               WriteTexto( ' Base de datos fuente: ' + Nombre );
          end;
          WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Empezando comparaci�n' );
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
     end;
     valorCallBack := 0;
     for i := ( TablasCount - 1 ) downto 0 do
     begin
          if Tablas[ i ].Seleccionada then
          begin
              with Tablas[ i ] do
              begin
                   Log := Bitacora;
                   lContinue := CallBack( Format( 'Comparando %s', [ Descripcion ] ), valorCallBack );
                   valorCallBack := valorCallBack + 1;
                   if lContinue then
                   begin
                      if Comparar (CallBackDetalle) then
                          Result := True;
                   end
                   else
                       Break;
              end;
          end;
     end;
     with Bitacora do
     begin
          if lContinue then
          begin
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Comparaci�n terminada' );
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          end
          else
          begin
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Comparaci�n interrumpida' );
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
          end;
     end;
end;

function TdmConsolida.GetEmpresaActiva: TCompanyData;
var i: Integer;
begin
     Result := nil;
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', FEmpresaActiva, []);
     for i := 0 to EmpresasCount-1 do
     begin
        if Empresas[i].Codigo = dmSistema.cdsSistBaseDatos.FieldByName('DB_CODIGO').AsString then
        begin
            Result := Empresas[ i ];
            break;
        end;
     end;
end;

function TdmConsolida.GetEmpresaFuente: TCompanyData;
var i: Integer;
begin
     Result := nil;
     dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', FEmpresaFuente, []);
     for i := 0 to EmpresasCount-1 do
     begin
        if Empresas[i].Codigo = dmSistema.cdsSistBaseDatos.FieldByName('DB_CODIGO').AsString then
        begin
            Result := Empresas[ i ];
            break;
        end;
     end;
end;

procedure TdmConsolida.LimpiarDiferenciasErrores;
var i: Integer;
begin
    for i := 0 to TablasCount-1 do
    begin
        Tablas[i].FDifferences.Clear;
        Tablas[i].FError.Clear;
        Tablas[i].FDelta.Clear;
    end;
end;

function TdmConsolida.TransferirTablas( CallBack: TShowProgress; Bitacora: TTransferLog): Boolean;
var
   i: Integer;
   valorCallBack: Integer;
   lContinue: Boolean;
begin
     Result := False;
     lContinue := True;
     { La Lista Tablas est� en orden descendente de dependencias, es decir }
     { las tablas hijas van primero que las padres }
     { Es necesario procesarla de abajo para arriba }
     with Bitacora do
     begin
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          WriteTexto( 'Transferencia de tablas' );
          with EmpresaActiva do
          begin
               WriteTexto( 'Base de datos destino: ' + Nombre );
          end;
          with EmpresaFuente do
          begin
               WriteTexto( ' Base de datos fuente: ' + Nombre );
          end;
          WriteTexto( FormatDateTime( ' dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Empezando transferencia' );
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
     end;
     valorCallBack := 0;
     for i := ( TablasCount - 1 ) downto 0 do
     begin
          if Tablas[ i ].Transfer then
          begin
              with Tablas[ i ] do
              begin
                   Log := Bitacora;
                   lContinue := CallBack( Format( 'Transfiriendo %s', [ Descripcion ] ), valorCallBack );
                   valorCallBack := valorCallBack + 1;
                   if lContinue then
                   begin
                      if Transferir then
                          Result := True;
                   end
                   else
                       Break;
              end;
          end;
     end;
     with Bitacora do
     begin
          if lContinue then
          begin
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Transferencia terminada' );
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          end
          else
          begin
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Transferencia interrumpida' );
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
          end;
     end;
end;

function TdmConsolida.TransferirEmpleados(CallBack: TShowProgress; Bitacora: TTransferLog; const iEmpBase: TNumEmp; const lNominas, lIMSS: Boolean; const sFiltro, sNivel0: String): Boolean;
const
     aSiNo: array[ False..True ] of pChar = ( 'NO', 'SI' );
     K_CUENTA_EMPLEADOS = 'SELECT COUNT( CB_CODIGO ) CUANTOS FROM COLABORA WHERE ( CB_CODIGO = :Empleado )';
     K_UPDATE_PERIODO = 'UPDATE PERIODO set '+
                        'PERIODO.PE_NUM_EMP = ( select COUNT(*) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ), '+
                        'PERIODO.PE_TOT_PER = ( select SUM( N.NO_PERCEPC ) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ), '+
                        'PERIODO.PE_TOT_DED = ( select SUM( N.NO_DEDUCCI ) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ), '+
                        'PERIODO.PE_TOT_NET = ( select SUM( N.NO_NETO ) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ) '+
                        'where ( PERIODO.PE_STATUS >= %d ) and ( ( select COUNT(*) from NOMINA N where ( N.PE_YEAR = PERIODO.PE_YEAR ) and ( N.PE_TIPO = PERIODO.PE_TIPO ) and ( N.PE_NUMERO = PERIODO.PE_NUMERO ) ) > 0 ) ';
     K_IMSS_MENSUAL = 'update LIQ_IMSS set '+
                      'LIQ_IMSS.LS_NUM_TRA = ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_DIAS_CO = ( select SUM( E.LE_DIAS_CO ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_EYM_DIN = ( select SUM( E.LE_EYM_DIN ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_EYM_ESP = ( select SUM( E.LE_EYM_ESP ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_EYM_EXC = ( select SUM( E.LE_EYM_EXC ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_EYM_FIJ = ( select SUM( E.LE_EYM_FIJ ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_RIESGOS = ( select SUM( E.LE_RIESGOS ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_INV_VID = ( select SUM( E.LE_INV_VID ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_GUARDER = ( select SUM( E.LE_GUARDER ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_IMSS_OB = ( select SUM( E.LE_IMSS_OB ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                      'LIQ_IMSS.LS_IMSS_PA = ( select SUM( E.LE_IMSS_PA ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+

                      // SOP 4527
                      // Aportaciones voluntarias se calcula mensualmente
                      'LIQ_IMSS.LS_APO_VOL = ( select SUM( LE_APO_VOL ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ) '+

                      'where ( ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ) > 0 )';
     K_IMSS_BIMESTRAL = 'update LIQ_IMSS set '+
                        'LIQ_IMSS.LS_NUM_BIM = ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        'LIQ_IMSS.LS_INF_NUM = ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) and ( E.LE_INF_AMO > 0 ) ), '+
                        'LIQ_IMSS.LS_DIAS_BM = ( select SUM( LE_DIAS_BM ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        'LIQ_IMSS.LS_RETIRO =  ( select SUM( LE_RETIRO  ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        'LIQ_IMSS.LS_CES_VEJ = ( select SUM( LE_CES_VEJ ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+

                        // SOP 4527
                        // Aportaciones voluntarias es calculado mensualmente
                        // 'LIQ_IMSS.LS_APO_VOL = ( select SUM( LE_APO_VOL ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+

                        'LIQ_IMSS.LS_INF_AMO = ( select SUM( LE_INF_AMO ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ), '+
                        'LIQ_IMSS.LS_INF_ACR = ( select SUM( LE_INF_PAT ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) and ( E.LE_INF_AMO > 0 ) ), '+
                        'LIQ_IMSS.LS_INF_NAC = ( select SUM( LE_INF_PAT ) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) and ( E.LE_INF_AMO <= 0 ) ) '+
	                'where ( LIQ_IMSS.LS_MONTH in ( 2,4,6,8,10,12 ) ) and ( ( select COUNT(*) from LIQ_EMP E where ( E.LS_PATRON = LIQ_IMSS.LS_PATRON ) and ( E.LS_YEAR = LIQ_IMSS.LS_YEAR ) and ( E.LS_MONTH = LIQ_IMSS.LS_MONTH ) and ( E.LS_TIPO = LIQ_IMSS.LS_TIPO ) ) > 0 )';
     K_IMSS_TOTALES = 'update LIQ_IMSS set '+
                      'LS_SUB_IMS = LS_EYM_FIJ + LS_EYM_EXC + LS_EYM_DIN + LS_EYM_ESP + LS_INV_VID + LS_RIESGOS + LS_GUARDER, '+
                      'LS_SUB_RET = LS_RETIRO + LS_CES_VEJ, '+
                      'LS_SUB_INF = LS_INF_NAC + LS_INF_ACR + LS_INF_AMO, '+
                      'LS_TOT_IMS = LS_SUB_IMS + LS_ACT_IMS + LS_REC_IMS, '+
                      'LS_TOT_RET = LS_SUB_RET + LS_ACT_RET + LS_REC_RET + LS_APO_VOL, '+
                      'LS_TOT_INF = LS_SUB_INF + LS_ACT_INF + LS_REC_INF, '+
                      'LS_TOT_MES = LS_TOT_IMS + LS_TOT_RET + LS_TOT_INF';
var
     i, indexEmpleado: Integer;
     iEmpleado, iNewEmp, iFolio: TNumEmp;
     sValor: String;
     lOk, lContinue: Boolean;
     qryCuenta: TDataSet;

     procedure Recalcular( const sScript, sItem: String );
     begin
          try
             // SetTargetQuery( qryCuenta, sScript );
             // qryCuenta.ExecSQL;
             qryCuenta := FColabora.oZetaProviderMaster.CreateQuery (sScript);
             FColabora.oZetaProviderMaster.Ejecuta(qryCuenta);

             Bitacora.WriteTexto( Format( '%s fueron recalculados', [ sItem ] ) );
          except
                on Error: Exception do
                begin
                     Bitacora.HandleException( Format( 'Error al recalcular %s', [ sItem ] ), Error );
                end;
          end;
     end;

begin
     Result := FALSE;
     
     lContinue := True;
     
     with Bitacora do
     begin
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          WriteTexto( 'Transferencia de empleados' );
          with EmpresaActiva do
          begin
               WriteTexto( '        Base de datos destino: ' + Nombre );
          end;
          with EmpresaFuente do
          begin
               WriteTexto( '         Base de datos fuente: ' + Nombre );
          end;
          WriteTexto( '              # Empleado base: ' + IntToStr( iEmpBase ) );
          WriteTexto( '           Transferir n�minas: ' + aSiNo[ lNominas ] );
          WriteTexto( 'Transferir liquidaciones IMSS: ' + aSiNo[ lIMSS ] );
          WriteTexto( FormatDateTime( ' dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Empezando transferencia' );
          WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
     end;
     try
        // Limpiar tablas de dmConsolida. Podr�an ya estar inicializada con la lista de tablas de otro proceso (comparaci�n o transferencia).
        ClearTablas;
        InitMovimientos( lNominas, lIMSS, sFiltro, sNivel0 );
        lOk := True;
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( Format( 'Error al configurar tablas por transferir', [ sValor ] ), Error );
                lOk := False;
           end;
     end;
     if lOk then
     begin
          sValor := VACIO;
          try
             for i := 0 to ( TablasCount - 1 ) do
             begin
                  with Tablas[ i ] do
                  begin
                       Log := Bitacora;
                       sValor := Nombre;
                  end;
             end;
          except
                on Error: Exception do
                begin
                     Bitacora.HandleException( Format( 'Error al preparar transferencia de la tabla %s', [ sValor ] ), Error );
                     lOk := False;
                end;
          end;
     end;
     if lOk then
     begin
          // Hacer que el provider se conecte a la empresa fuente
          dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaFuente, []);
          FColabora.oZetaProviderSource.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
                dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
                dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
                0]);

          // Hacer que el provider se conecte a la empresa destino
          dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaActiva, []);
          FColabora.oZetaProviderMaster.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
                dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
                dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
                0]);

          qryCuenta := TDataSet(FColabora.oZetaProviderMaster.CreateQuery (K_CUENTA_EMPLEADOS));
          iFolio := iEmpBase;
          FColabora.QueryReadDS := FColabora.oZetaProviderSource.CreateQuery (FColabora.GetQueryRead);
          // FColabora.TransferPrepare;

          try
              with FColabora.QueryReadDS do
              begin
                   Active := True;
                   indexEmpleado := 1;
                   while not Eof and lContinue do
                   begin
                        iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;

                        with qryCuenta do
                        begin
                             Active := False;
                             (TADOQuery(qryCuenta).Parameters).FindParam ('Empleado').Value := iEmpleado;

                             Active := True;
                             if ( Fields[ 0 ].AsInteger > 0 ) then
                             begin
                                  iNewEmp := iFolio;
                                  Inc( iFolio );
                             end
                             else
                                 iNewEmp := iEmpleado;
                             Active := False;
                        end;

                        lContinue := CallBack( Format( 'Transfiriendo %.0n como %.0n', [ iEmpleado / 1, iNewEmp / 1 ] ), indexEmpleado );

                        if lContinue then
                        begin
                             sValor := VACIO;
                             // TxStart;
                             try
                                for i := 0 to ( TablasCount - 1 ) do
                                begin
                                     with Tablas[ i ] do
                                     begin
                                          sValor := Nombre;
                                          TransferEmployee( iEmpleado, iNewEmp, sNivel0 );
                                     end;
                                end;
                                // TxCommit;
                                with Bitacora do
                                begin
                                     WriteTexto( Format( 'Empleado %.0n transferido como %.0n', [ iEmpleado / 1, iNewEmp / 1 ] ) );
                                end;
                             except
                                   on Error: Exception do
                                   begin
                                        // TxRollBack;
                                        Bitacora.HandleException( Format( 'Error al transferir %s del empleado %.0n', [ sValor, iEmpleado / 1] ), Error );                                        
                                        // Result := FALSE;
                                   end;
                             end;
                             indexEmpleado := indexEmpleado + 1;
                             Next;
                        end;
                   end;
                   Active := False;
              end;
          except 
                on Error: Exception do
                begin
                     Bitacora.HandleException( 'Error en transferencia de empleados', Error );
                     lOk := False;    
                     // Result := FALSE;
                end;
          end
     end;
     
     // if lContinue then
     if lContinue and lOk then
     begin
          Recalcular( Format( K_UPDATE_PERIODO, [ Ord( spCalculadaTotal ) ] ), 'Totales de per�odos de n�mina' );
          Recalcular( K_IMSS_MENSUAL, 'Totales mensuales del IMSS' );
          Recalcular( K_IMSS_BIMESTRAL, 'Totales bimestrales del IMSS' );
          Recalcular( K_IMSS_TOTALES, 'Totales del IMSS' );
     end;
     with Bitacora do
     begin      
          if lContinue then
          begin     
               if lOk and not ErrorFound then
                 sValor := ' con �xito.'
               else
                  sValor := ' con errores.';
                  
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Transferencia terminada' + sValor );
               WriteTexto( StringOfChar( '*', K_ANCHO_LINEA ) );
          end
          else
          begin
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
               WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ': Transferencia interrumpida'  );
               WriteTexto( StringOfChar( '!', K_ANCHO_LINEA ) );
          end;
     end;     
     
     Result := lOk;
end;

procedure TdmConsolida.InitMovimientos( const lNominas, lIMSS: Boolean; const sFiltro, sNivel0: String );

    procedure Add( const sNombre: String );
    begin
         with TTablaData.Create( FTablas, sNombre ) do
         begin
              Connect( Self );
              GetTableInfo;
              if sNombre = 'ACUMULA_RS' then
                 Ignorar := 'AC_ID';
              // Obtener lista de Computed Columns para cada tabla en InitMovimientos (proceso de transferir empleados).
              FComputedFields := dmConsolida.DefineComputedColumns (Nombre, oZetaProviderMaster);
         end;
    end;

begin
     {
     Con este query se averiguan las tablas que tienen CB_CODIGO:

     select DISTINCT( RDB$RELATION_NAME) from RDB$RELATION_FIELDS where
     RDB$FIELD_NAME = 'CB_CODIGO' group by RDB$RELATION_NAME

     Es necesario incluirlas en el arreglo A_TABLAS_TRANSFERENCIA
     En el proceso de Transferencia de Empleados de
     D:\3Win_20\MTS\DServerCalcNomina.pas

     OJO con las dependencias: Debe ir ordenado
     primero con las tablas padre y luego las hijas
     }

     FColabora := TColabora.Create( FTablas, 'COLABORA' );
     with FColabora do
     begin
          Connect( Self );
          Filtro := sFiltro;
          Nivel0 := sNivel0;
          Ignorar := 'PRETTYNAME,LLAVE';
          GetTableInfo;
     end;
     Add( 'PARIENTE' );
     Add( 'IMAGEN' );
     Add( 'DOCUMENTO' );
     Add( 'CAF_COME' );
     Add( 'ACUMULA' );
     Add( 'ACUMULA_RS' );
     Add( 'ASIGNA' );
     Add( 'AUSENCIA' );
     Add( 'CHECADAS' );
     Add( 'AHORRO' );
     Add( 'ACAR_ABO' );
     Add( 'PRESTAMO' );
     Add( 'PCAR_ABO' );
     Add( 'ANTESPTO' );
     Add( 'ANTESCUR' );
     Add( 'INCAPACI' );
     Add( 'PERMISO' );
     Add( 'VACACION' );
     Add( 'KARDEX' );
     Add( 'KAR_FIJA' );
     Add( 'KARCURSO' );
     Add( 'AGUINAL' );
     Add( 'REP_AHO' );
     Add( 'COMPARA' );
     Add( 'DECLARA' );
     Add( 'REP_PTU' );
     Add( 'KAR_TOOL' );
     Add( 'CED_EMP' );
     Add( 'KAR_AREA' );
     Add( 'WOFIJA' );
     Add( 'WORKS' );

     if lIMSS then
     begin
          Add( 'LIQ_EMP' );
          Add( 'LIQ_MOV' );
     end;

     if lNominas then
     begin
          Add( 'NOMINA' );
          Add( 'FALTAS' );
          Add( 'MOVIMIEN' );
     end;

     Add( 'KARINF');
     Add('KAR_CERT');
end;

function TdmConsolida.GetEmpleadoMaximo: Integer;
const
    Q_GET_MAX_NUM_EMP = 'SELECT MAX( CB_CODIGO ) MAXIMO FROM COLABORA';
var
    dsConsulta: TDataSet;
    oZetaProviderLocal: TdmZetaServerProvider;
begin
    oZetaProviderLocal := TdmZetaServerProvider.Create(nil);
    try
         dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaActiva, []);
         oZetaProviderLocal.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
               dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
               dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
               0]);
         try
            dsConsulta := oZetaProviderLocal.CreateQuery (Q_GET_MAX_NUM_EMP);
            with dsConsulta do
            begin
                 Active := True;
                 Result := Fields[ 0 ].AsInteger;
                 Active := False;
            end;
         except
               on Error: Exception do
               begin
                    // Application.HandleException( Error );
                    // HandleException( Error );
                    Result := 0;
               end;
         end;
    finally
        FreeAndNil (oZetaProviderLocal);
    end;
end;

function TdmConsolida.GetEmpleadoCount( sFiltro: String; var sMensajeError: String ): Integer;
const
     Q_CUENTA_EMPLEADOS = 'SELECT COUNT( CB_CODIGO ) CUANTOS FROM COLABORA ';
var
   dsConsulta: TDataSet;
    oZetaProviderLocal: TdmZetaServerProvider;
begin
      sMensajeError := '';
      oZetaProviderLocal := TdmZetaServerProvider.Create(nil);
      try
           dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', dmConsolida.FEmpresaFuente, []);
           oZetaProviderLocal.EmpresaActiva := VarArrayOf([dmSistema.cdsSistBaseDatos.FieldByName('DB_DATOS').AsString,
                 dmSistema.cdsSistBaseDatos.FieldByName('DB_USRNAME').AsString,
                 dmSistema.cdsSistBaseDatos.FieldByName('DB_PASSWRD').AsString,
                 0]);
           try
              if StrLleno (sFiltro) then
                sFiltro := ' WHERE ' + sFiltro;

              dsConsulta := oZetaProviderLocal.CreateQuery (Q_CUENTA_EMPLEADOS + sFiltro);

              with dsConsulta do
              begin
                   Active := True;
                   Result := Fields[ 0 ].AsInteger;
                   Active := False;
              end;
           except
                 on Error: Exception do
                 begin
                      SetErrorFound( True );
                      Result := -1;
                      sMensajeError := Error.Message;
                 end;
           end;
      finally
          FreeAndNil (dsConsulta);
          FreeAndNil (oZetaProviderLocal);
      end;
end;

function TdmConsolida.ConsolidarBasesDatos (CallBack: TShowProgress; const Bitacora: TTransferLog; const sNombre: String; Lista: TCompanyList;
            BaseDatosConsolidadora: TCompanyData;
            const iYear: Integer; const dInicial, dFinal: TDate; const lBorrarTablas: Boolean; const iMaxEmpleados: Integer): Boolean;
var
   lOk: Boolean;
   i, iNumEmpDBSource: Integer;
   sValor: String;
   oZetaProviderConsolidaTarget: TdmZetaServerProvider;
   dsSource: TDataSet;
begin
     FParamMsg := TStringList.Create;
     i := 0;
     oZetaProviderConsolidaTarget := TdmZetaServerProvider.Create(nil);

     // Definir empresa activa a oZetaProviderConsolidaTarget
     oZetaProviderConsolidaTarget.EmpresaActiva :=
          VarArrayOf([BaseDatosConsolidadora.Datos, BaseDatosConsolidadora.UserName, BaseDatosConsolidadora.Password, 0]);

     with Bitacora do
     begin
          WriteTexto( 'Consolidaci�n de bases de datos' );
          WriteTexto( 'Base de datos de consolidaci�n: ' + sNombre );
          WriteTexto( GetTimeStamp + ': Iniciando consolidaci�n' );
     end;
     lOk := False;

     ClearErrorFound;

     try
        if lBorrarTablas then
        begin
           lOk := CallBack ('Borrando tablas en base de datos de consolidaci�n', 0);
           if lOk then
              lOk := BorraTablas (Bitacora, oZetaProviderConsolidaTarget);
        end
        else
            lOk := True;
        if lOk then
        begin
             dsSource := oZetaProviderConsolidaTarget.CreateQuery (Format( Q_MAX_NUMEMP, [ DatetoStrSQLC( dInicial ) ] ) );

             with dsSource do
             begin
                  Active := True;
                  iNumEmpDBSource := FieldByName('CUANTOS').AsInteger;
             end;
             with Lista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       // Por cada elemento en Lista, crear un nuevo provider para leer
                       // de la siguiente empresa a consolidar
                       lOk := CallBack ('Consolidando base de datos: ' + Empresa[ i ].Nombre, i + 1);
                       if lOk then
                          lOk := LeeTablas( CallBack, Bitacora, oZetaProviderConsolidaTarget, TdmZetaServerProvider.Create(nil), Empresa[ i ], iYear, dInicial, dFinal, iMaxEmpleados, iNumEmpDBSource );

                       if not lOk then
                       begin
                          // Comentar el siguiente llamado a un CallBack
                          // CallBack ('Errores', i-1);4
                          Break;
                       end;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                SetErrorFound( True );
                Bitacora.HandleException( 'Error al abrir base de datos de consolidaci�n', Error );
           end;
     end;

     with Bitacora do
     begin
          sValor := 'Consolidaci�n terminada %s';
          if lOk and not ErrorFound then
             sValor := Format( sValor, [ 'con �xito.' ] )
          else
              sValor := Format( sValor, [ 'con errores.' ] );

          if StrLleno (FParamMsg.Text) then
              WriteTexto( CR_LF + FParamMsg.Text );

          WriteTexto( GetTimeStamp + ': ' + sValor );
          CallBack (sValor, i);
     end;

     Result := lOk;
end;

function TdmConsolida.BorraTablas ( Bitacora: TTransferLog; oZetaProviderConsolidaTarget: TdmZetaServerProvider ): Boolean;

      procedure BorraTabla(const sTableName: String );
      begin
           try
              oZetaProviderConsolidaTarget.ExecSQL(oZetaProviderConsolidaTarget.EmpresaActiva, 'delete from ' + sTableName);
              Bitacora.WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now )  + ' Tabla ' + sTableName + ' fu� borrada' );

              if not DoEndCallBack (Bitacora) then
                 raise EAbort.Create( 'Proceso interrumpido por el usuario' );
           except
                 on Error: Exception do
                 begin
                      Error.Message := 'Tabla: ' + sTableName + CR_LF + Error.Message;
                      raise;
                 end;
           end;
      end;

begin
    // StartTransaction;
    try
       BorraTabla( 'INCAPACI' );
       BorraTabla( 'VACACION' );
       BorraTabla( 'PERMISO' );
       BorraTabla( 'AUSENCIA' );
       BorraTabla( 'ACUMULA' );
       BorraTabla( 'ACUMULA_RS' );
       BorraTabla( 'KAR_FIJA' );
       BorraTabla( 'KARDEX' );
       BorraTabla( 'COLABORA' );
       // Commit;
       Result := True;
    except
          on Error: EAbort do
          begin
               // RollBack;
               SetErrorFound( True );
               Result := False;
          end;
          on Error: Exception do
          begin
               // RollBack;
               SetErrorFound( True );
               Bitacora.HandleException( 'Error al borrar tablas', Error );
               Result := False;
          end;
    end;
end;

procedure TdmConsolida.ClearErrorFound;
begin
     FErrorFound := False;
     FErrorCount := 0;
end;

procedure TdmConsolida.SetErrorFound( const lValue: Boolean );
begin
     if lValue then
     begin
          FErrorFound := True;
          FErrorCount := FErrorCount + 1;
     end;
end;

function TdmConsolida.LeeTablas(CallBack: TShowProgress; Bitacora: TTransferLog; oZetaProviderConsolidaTarget, oZetaProviderConsolida: TdmZetaServerProvider; Empresa: TCompanyData; const iYear: Integer; const dInicial, dFinal: TDate; const iMaxEmpleados: Integer; const iMaxEmpDBSource: Integer ): Boolean;
var
   iAcumulaEmpresas: Integer;
   lOk: Boolean;
   dsSource: TDataSet;
   dsTarget: TDataSet;

function LeeUnaTabla( const sTableName, sFiltro, sOrden: String ): Boolean;
var
   lHayCBCodigo: Boolean;
   sQuery: String;
   sComputed_Columns: String;
begin
     Result := True;
     sQuery := 'select * from ' + sTableName;
     if ZetaCommonTools.StrLleno( sFiltro ) then
        sQuery := sQuery + ' where ' + sFiltro;
     if ZetaCommonTools.StrLleno( sOrden ) then
        sQuery := sQuery + ' order by ' + sOrden;
     try
        dsSource := oZetaProviderConsolida.CreateQuery(sQuery);

        with dsSource do
        begin
             Active := True;
             lHayCBCodigo := ( FindField( 'CB_CODIGO' ) <> nil );

             // Definir aqu� la lista de Computed Fields de cada tabla.
             // Guardarlos en un commatext y enviarlo a SetInsertScript y TransfiereDatasets.
             // Ambos m�todos, lo mandar�n a DefineTabla( sTabla: String; oFields: TField ) como
             // un tercer par�metro y exluir esos campos
             sComputed_Columns := DefineComputedColumns (sTableName, oZetaProviderConsolidaTarget);

             SetInsertScript( sTableName, dsSource, dsTarget, oZetaProviderConsolidaTarget, sComputed_Columns );

             // TransactionStart;
             // oZetaProviderConsolidaTarget.EmpiezaTransaccion;
             while not Eof and Result do
             begin
                  try
                     // Hacer aqu� el inicio de una transacci�n
                     oZetaProviderConsolidaTarget.EmpiezaTransaccion;

                     TransfiereDatasets( sTableName, dsSource, dsTarget, sComputed_Columns );
                     with dsTarget do
                     begin
                          // Activar dsTarget para obtener n�mero de empleado y sumar
                          // dsTarget.Active := TRUE;
                          if lHayCbCodigo then
                          begin
                               (TADOQuery(dsTarget).Parameters).FindParam ('CB_CODIGO').Value :=
                                  (TADOQuery(dsTarget).Parameters).FindParam ('CB_CODIGO').Value + Empresa.Empate;
                          end;

                          oZetaProviderConsolidaTarget.Ejecuta(dsTarget);
                          // Terminar aqu� la transacci�n
                          oZetaProviderConsolidaTarget.TerminaTransaccion(TRUE);
                     end;
                  except
                        on Error: Exception do
                        begin
                             // Hacer aqui el RollBackTransaccion.
                             // Con que se de un error al transferir una tabla, Result es FALSE y
                             // se deber� indicar 'parcialmente consolidada'. --> Importante.
                             Result := FALSE;
                             oZetaProviderConsolidaTarget.RollBackTransaccion;
                             SetErrorFound( True );
                             Bitacora.HandleException( 'Error al transferir tabla ' + sTableName, Error );
                        end;
                  end;
                  // Result := TransactionCommit( sTableName + ' ' + IntToStr( iCounter ) );
                  // oZetaProviderConsolidaTarget.TerminaTransaccion(TRUE);
                  // Result := TRUE;
                  Next;
             end;
             // TransactionEnd;
             Active := False;
        end;
        Result := TerminaConsolidacion( Bitacora, sTableName, Result );
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( 'Error al consolidar tabla ' + sTableName, Error );
                Result := False;
           end;
     end;
end;

function MergeTable( const sTableName, sKeyName, sFiltro, sOrden: String ): Boolean;
var
   lHayCbCodigo, lYaExiste: Boolean;
   sQuery: String;
   dsDelete: TDataSet;
   sComputed_Columns: String;
begin
     sQuery := 'select * from ' + sTableName;
     if ZetaCommonTools.StrLleno( sFiltro ) then
        sQuery := sQuery + ' where ' + sFiltro;
     if ZetaCommonTools.StrLleno( sOrden ) then
        sQuery := sQuery + ' order by ' + sOrden;
     try
        dsSource := oZetaProviderConsolida.CreateQuery(sQuery);
        dsDelete := oZetaProviderConsolidaTarget.CreateQuery(
                        Format( 'select COUNT(*) from %s where ( %s = :%s )', [ sTableName, sKeyName, sKeyName ] ));
                        // Format( 'select COUNT(CO_NUMERO) from %s where ( %s = :%s )', [ sTableName, sKeyName, sKeyName ] ));
        Result := True;
        // TransactionStart;
        with dsSource do
        begin
             Active := True;
             lHayCbCodigo := ( FindField( 'CB_CODIGO' ) <> nil );

             // Definir aqu� la lista de Computed Fields de cada tabla.
             // Guardarlos en un commatext y enviarlo a SetInsertScript y TransfiereDatasets.
             // Ambos m�todos, lo mandar�n a DefineTabla( sTabla: String; oFields: TField ) como
             // un tercer par�metro y exluir esos campos
             sComputed_Columns := DefineComputedColumns (sTableName, oZetaProviderConsolidaTarget);

             // sComputed_Columns: Nuevo Par�metro
             SetInsertScript( sTableName, dsSource, dsTarget, oZetaProviderConsolidaTarget, sComputed_Columns );
             while not Eof and Result do
             begin
                  (TADOQuery(dsDelete).Parameters).FindParam (sKeyName).Value := FieldByName( sKeyName ).Value;
                  with dsDelete do
                  begin
                       Active := True;
                       if IsEmpty then
                          lYaExiste := False
                       else
                           lYaExiste := ( Fields[ 0 ].AsInteger > 0 );
                       Active := False;
                  end;
                  if not lYaExiste then
                  begin
                       try
                          // Hacer aqu� el inicio de una transacci�n
                          oZetaProviderConsolidaTarget.EmpiezaTransaccion;

                          TransfiereDatasets( sTableName, dsSource, dsTarget, sComputed_Columns );
                          with dsTarget do
                          begin
                               if lHayCbCodigo then
                               begin
                                    (TADOQuery(dsTarget).Parameters).FindParam ('CB_CODIGO').Value :=
                                            dsSource.FieldByName('CB_CODIGO').AsInteger + Empresa.Empate;
                               end;
                               oZetaProviderConsolidaTarget.Ejecuta(dsTarget);
                               // Terminar aqu� la transacci�n
                               oZetaProviderConsolidaTarget.TerminaTransaccion(TRUE);
                          end;
                       except
                             on Error: Exception do
                             begin
                                  // Hacer aqui el RollBackTransaccion.
                                  // Con que se de un error al transferir una tabla, Result es FALSE y
                                  // se deber� indicar 'parcialmente consolidada'. --> Importante.
                                  Result := FALSE;
                                  oZetaProviderConsolidaTarget.RollBackTransaccion;

                                  SetErrorFound( True );
                                  Bitacora.HandleException( 'Error al transferir tabla ' + sTableName, Error );
                             end;
                       end;
                  end;
                  // Result := TransactionCommit( sTableName + ' ' + IntToStr( iCounter ) );
                  Next;
             end;
             Active := False;
        end;
        Result := TerminaConsolidacion( Bitacora, sTableName, Result );
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( 'Error al consolidar tabla ' + sTableName, Error );
                Result := False;
           end;
     end;
end;

   function ObtenerNumEmpleados: Integer;
   var dsTargetLocal: TDataSet;
   begin
        dsTargetLocal := oZetaProviderConsolida.CreateQuery(Format( Q_MAX_NUMEMP, [ DatetoStrSQLC( dInicial ) ] )); // Se necesita otro provider. No debe ser el oZetaProviderConsolida.
        try
            with dsTargetLocal do
            begin
                 Active := True;
                 Result := FieldByName('CUANTOS').AsInteger;
            end;
        finally
            FreeAndNil (dsTargetLocal);
        end;
   end;

begin
     Result := False;

     with Bitacora do
     begin
          WriteTexto( StringOfChar( '*', 50 ) );
          WriteTexto( GetTimeStamp + ': Consolidando ' + Empresa.Nombre );
     end;
     try
        // Al entrar a LeeTablas, definir empresa activa para el provider que se recibe.
        // Asignarle los valores de la empresa --> Empresa: TCompanyData.
        oZetaProviderConsolida.EmpresaActiva := VarArrayOf([Empresa.FDatos, Empresa.FUserName, Empresa.FPassword, 0]);

        iAcumulaEmpresas := ObtenerNumEmpleados + iMaxEmpDBSource;

        if ( iMaxEmpleados >= iAcumulaEmpresas ) then
        begin
             lOk := CallBack ('Consolidando tabla COLABORA', 1);
             if lOk then
                lOk := LeeUnaTabla( 'COLABORA', '', 'CB_CODIGO' );

             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla KARDEX', 1);
                if lOk then
                    lOk := LeeUnaTabla( 'KARDEX', '', 'CB_CODIGO, CB_FECHA, CB_TIPO' );
             end;

             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla KAR_FIJA', 1);
                if lOk then
                    lOk := LeeUnaTabla( 'KAR_FIJA', '', 'CB_CODIGO, CB_FECHA, CB_TIPO, KF_FOLIO' );
             end;

             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla INCAPACI', 1);
                if lOk then
                    lOk := LeeUnaTabla( 'INCAPACI', '', 'CB_CODIGO, IN_FEC_INI' );
             end;

             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla VACACION', 1);
                if lOk then
                    lOk := LeeUnaTabla( 'VACACION', '', 'CB_CODIGO, VA_FEC_INI, VA_TIPO' );
             end;

             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla PERMISO', 1);
                if lOk then
                    lOk := LeeUnaTabla( 'PERMISO', '', 'CB_CODIGO, PM_FEC_INI' );
             end;

             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla AUSENCIA', 1);
                if lOk then
                    lOk := LeeUnaTabla( 'AUSENCIA', Format( '( AU_FECHA >= %s ) and ( AU_FECHA <= %s )', [ DateToStrSQLC( dInicial ), DateToStrSQLC( dFinal ) ] ), 'CB_CODIGO, AU_FECHA' );
             end;

             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla CONCEPTO', 1);
                if lOk then
                    lOk := MergeTable( 'CONCEPTO', 'CO_NUMERO', '', 'CO_NUMERO' );
             end;

             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla ACUMULA', 1);
                if lOk then
                    lOk := LeeUnaTabla( 'ACUMULA', Format( '( AC_YEAR = %d )', [ iYear ] ), 'AC_YEAR, CB_CODIGO, CO_NUMERO' );
             end;
             if lOk then
             begin
                lOk := CallBack ('Consolidando tabla ACUMULA_RS', 1);
                if lOk then
                    lOk := LeeUnaTabla( 'ACUMULA_RS', Format( '( AC_YEAR = %d )', [ iYear ] ), 'AC_YEAR, CB_CODIGO, CO_NUMERO, RS_CODIGO' );
             end;
             Result := lOk;
        end
        else
        begin
             Bitacora.WriteTexto( Format( 'Intentando proceso para %d empleados.' + CR_LF + 'Se excedi� el l�mite de %d empleados autorizados', [ iAcumulaEmpresas, iMaxEmpleados ] ) );
        end;
     except
           on Error: Exception do
           begin
                SetErrorFound( True );
                Bitacora.WriteException(0, 'Error al abrir empresa a consolidar', Error);
                Result := False;
           end;
     end;
end;

function TdmConsolida.TerminaConsolidacion( Bitacora: TTransferLog; sTableName: String; lExito: Boolean ): Boolean;
begin
     if lExito then
     begin
          Bitacora.WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ' Tabla ' + sTableName + ' fu� consolidada' );
          Result := DoEndCallBack (Bitacora);
     end
     else
     begin
          Bitacora.WriteTexto( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ' Tabla ' + sTableName + ' fu� parcialmente consolidada' );
          Result := False;
     end;
end;

procedure TdmConsolida.SetInsertScript( Tabla: String; Source: TDataset; var Target: TDataset;
      oZetaProviderConsolidaTarget: TdmZetaServerProvider; sComputed_Columns: String);
var
   i: Integer;
   sScript, sCampos: String;
begin
     sScript := 'insert into ' + Tabla + ' ( ';
     sCampos := ' ) values ( ';
     with Source do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
              if DefineTabla( Tabla, Fields[ i ], sComputed_Columns ) then
              begin
                   sScript := sScript + Fields[ i ].FieldName + ',';
                   sCampos := sCampos + ':' + Fields[ i ].FieldName + ',';
              end;
          end;
     end;
     sScript := ZetaCommonTools.CortaUltimo( sScript ) + ZetaCommonTools.CortaUltimo( sCampos ) + ' )';
     Target := oZetaProviderConsolidaTarget.CreateQuery (sScript);
end;

function TdmConsolida.DefineTabla( sTabla: String; oFields: TField; sComputed_Columns: String ): Boolean;
begin
     Result := False;
     with oFields do
     begin
          if NoEsCampoLLavePortal( oFields ) then
          begin
               if ( FieldKind = fkData ) then
               begin
                    // Se cambian las siguientes dos condiciones por una sola.
                    // Esto porque ya se trae la lista de Computed Fields por cada tabla.
                    // No debe ser necesario usar directamente el  campo K_PRETTYNAME para la tabla COLABORA.
                    // Como es un Computed Field, este  viene en el string sComputed_Columns.
                    {if ( ( AnsiUpperCase( sTabla ) <> 'COLABORA' ) or ( FieldName <> K_PRETTYNAME ) ) then
                    begin
                         // if ( ( AnsiUpperCase( sTabla ) <> 'KARDEX' ) or ( ( FieldName <> 'CB_TIP_REV' ) and ( FieldName <> 'CB_FEC_SAL' ) ) ) then
                         // No debe ser necesario usar directamente los campos CB_TIP_REV y CB_FEC_SAL para la tabla KARDEX.
                         // Como son Computed Fields, estos vienen en el string sComputed_Columns.
                         if ( ( AnsiUpperCase( sTabla ) <> 'KARDEX' ) or ( ( FieldName <> 'CB_TIP_REV' ) and ( FieldName <> 'CB_FEC_SAL' ) ) ) then
                              Result := True;
                    end;}
                    if Pos (FieldName, sComputed_Columns) = 0 then
                        Result := TRUE;
               end;
          end;
     end;
end;

procedure TdmConsolida.TransfiereDatasets( sTabla: String; qSource, qTarget: TDataSet; sComputed_Columns: String);
var
   i,j: Integer;
begin
     //Checar ZetaTressCFGTools el m�todo SetInsertScript donde se valida que si es SQL no mande
     //el Prettyname
     {
     ZetaCommonTools.TransferDatasets( tqSource, tqTarget );
     }
     j := 0;
     with qSource do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               //MA: Soluci�n #1 Protegiendote si la posicion de los campos cambian
               {oParam :=  tqTarget.Params.FindParam( Fields[i].FieldName );
               if ( oParam <> nil ) then
                  oParam.AssignField( Fields[ i ] );}
               //MA: Soluci�n #2 Solo te valida que el numero de parametros coincida con el numero de campos
               //pero no te protege si los campos cambiaran de posicion.
{              if ( tqTarget.ParamCount > i ) then
                  tqTarget.Params[ i ].AssignField( Fields[ i ] ); }
               if DefineTabla( sTabla, Fields[ i ], sComputed_Columns ) then
               begin
                    (TADOQuery(qTarget)).Parameters[j].Value := Fields[i].Value;
                    j := j + 1;
               end;
          end;
     end;
end;

function TdmConsolida.DefineComputedColumns (sTableName: String; oZetaProviderConsolidaTarget: TdmZetaServerProvider): String;
var dataSet: TDataSet;
begin
    Result := VACIO;
    dataSet := oZetaProviderConsolidaTarget.
        CreateQuery(Format ('SELECT NAME FROM sys.columns WHERE is_computed = 1 AND object_id = OBJECT_ID(''%s'')', [sTableName]));

    try
        dataSet.Active := TRUE;
        dataSet.First;
        while not dataSet.Eof do
        begin
            Result := Result + dataSet.FieldByName('NAME').AsString + ',';
            dataSet.Next;
        end;
        dataSet.Active := FALSE;
    finally
       FreeAndNil (dataSet);
    end;

    Result := ZetaCommonTools.CortaUltimo( Result );
end;

function TdmConsolida.DoEndCallBack (Bitacora: TTransferLog): Boolean;
var
   Continue: Boolean;
begin
     Continue := True;

     if Assigned( FEndCallBack ) then
        FEndCallBack( Self, Continue );

     Result := Continue;

     if not Result then
        // Bitacora.AgregaResumen( '*** Interrupci�n del Usuario ***' );
        Bitacora.WriteTexto ('*** Interrupci�n del usuario ***');
end;

end.

