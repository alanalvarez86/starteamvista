unit DConsolidacion;

	interface

	uses		
		  System.Generics.Collections, DConsolida, SysUtils, Classes, Data.DB, DSistema, Differences, Comparision, ZetaServerTools, ZetaRegistryServer, ZetaClientDataSet, Writer, ModRow, Row;
	
	function GetTables(BaseDB, SlaveDB:String):TZetaClientDataSet;
	function GetDifferences(DataSource:TDataSet; BaseDB, SlaveDB:String):TZetaClientDataSet;
	function GetDiffDetails(Index:Integer; IsInsertionDetails:Boolean; out TableName:String):TZetaClientDataSet;
	procedure GenerateQuery(BaseQryPath, SlaveQryPath:String);		
	function DatasetToStringList(Selected:TDataSet):TStringList;
	function GetConnStr(Database:String):String;
	procedure AddDiffDetail(Detail:TDataSet; Index:Integer; IsInsertionDetail:Boolean);
	procedure FillWithModRows(Detail:TDataSet; const m:TObjectList<TModRow>);
  procedure FillWithNewRows(Detail:TDataSet; const n:TObjectList<TRow>);
	procedure DiffListToFalse;
	function DiffsWereSelected(Selected:TDataSet):Boolean;
	procedure FillSelectedList(Selected:TDataSet);
	function GetSelectedDiffs(Selected:TDataSet):TZetaClientDataSet;

implementation
	
	var diffList:TObjectList<TComparision>;
	var selectList:TObjectList<TComparision>;

	function GetTables(BaseDB, SlaveDB:String):TZetaClientDataSet;
		var 
			i:Integer;
			cds:TZetaClientDataSet;
		begin
			cds:=TZetaClientDataSet.Create(nil);
			with cds do begin
				Active:=False;
				Filtered:=False;
				Filter:='';
				AddStringField('Nombre', 20);
				AddStringField('Descripcion', 45);
				AddBooleanField('Elegir');
				CreateTempDataset;				
				Open;
			end;
			
			dmConsolida.InitEmpresas(BaseDB, SlaveDB);
			dmConsolida.InitTablas;
			for i:=0 to dmConsolida.TablasCount-1 do begin
				cds.Insert;
				cds.FieldByName('Nombre').AsString := dmConsolida.Tablas[i].Nombre;
				cds.FieldByName('Descripcion').AsString := dmConsolida.Tablas[i].Descripcion;
				cds.FieldByName('Elegir').AsBoolean := FALSE;
				cds.Post;
			end;

			result:=cds;
		end;
		
		
		
	function GetDifferences(DataSource:TDataSet; BaseDB, SlaveDB:String):TZetaClientDataSet;
		var
			cds:TZetaClientDataset;
			i:Integer;
		begin			
			cds:=TZetaClientDataset.Create(nil);
  		diffList:=Differences.Get(
			 	DatasetToStringList(DataSource),
			 	GetConnStr(BaseDB),
			 	GetConnStr(SlaveDB));
			
			selectList:=TObjectList<TComparision>.Create;
				 				 
			cds.Active:= False;
			cds.Filtered := False;
			cds.Filter := '';
			cds.AddStringField('Nombre', 20);
			cds.AddIntegerField('DiffCount');
			cds.AddBooleanField('Elegir');
      cds.AddIntegerField('Index');
      cds.CreateTempDataset;
			cds.Open;

			for i:=0 to diffList.Count-1 do begin
				cds.Insert;
				cds.FieldByName('Nombre').AsString:=diffList[i].TableName;
				cds.FieldByName('DiffCount').AsInteger:=diffList[i].DiffCount;
				cds.FieldByName('Elegir').AsBoolean:=difflist[i].IsSelected;
        cds.FieldByName('Index').AsInteger:=i;
				cds.Post;
			end;
			
			result:=cds;
		end;
	
	function GetDiffDetails(Index:Integer; IsInsertionDetails:Boolean; out TableName:String):TZetaClientDataSet;
		var
			cds:TZetaClientDataset;
			tempComp:TComparision;
			i:Integer;
		begin			
			cds:=TZetaClientDataset.Create(nil);
			tempComp:=diffList[Index];
			TableName:=tempComp.TableName;

			cds.Active:= False;
			cds.Filtered := False;
			cds.Filter := '';
			cds.AddStringField('SlaveDesc', 40);
			cds.AddStringField('SlaveCode', 15);
			cds.AddStringField('BaseDesc', 40);
			cds.AddStringField('BaseCode', 15);
			cds.AddBooleanField('Elegir');
      cds.CreateTempDataset;
			cds.Open;

			if IsInsertionDetails then begin
				for i:=0 to tempComp.NewRows.Count-1 do begin
					cds.Insert;
					cds.FieldByName('SlaveDesc').AsString:=tempComp.NewRows[i].Description;
					cds.FieldByName('SlaveCode').AsString:=tempComp.NewRows[i].Code;
					cds.FieldByName('BaseDesc').AsString:='--Registros Faltantes--';
					cds.FieldByName('BaseCode').AsString:='--';					
					cds.FieldByName('Elegir').AsBoolean:=tempComp.NewRows[i].IsSelected;
					cds.Post;
				end;
			end
			else begin
				for i:=0 to tempComp.ModRows.Count-1 do begin
					cds.Insert;
					cds.FieldByName('SlaveDesc').AsString:=tempComp.ModRows[i].Slave.Description;
					cds.FieldByName('SlaveCode').AsString:=tempComp.ModRows[i].Slave.Code;
					cds.FieldByName('BaseDesc').AsString:=tempComp.ModRows[i].Base.Description;
					cds.FieldByName('BaseCode').AsString:=tempComp.ModRows[i].Base.Code;
					cds.FieldByName('Elegir').AsBoolean:=tempComp.ModRows[i].IsSelected;
					cds.Post;
				end;
			end;										
			result:=cds;
		end;

	procedure GenerateQuery(BaseQryPath, SlaveQryPath:String);
		var	
			w:TWriter;			
		begin
			w:=TWriter.Create(diffList);
			w.ToSQLFile(BaseQryPath, SlaveQryPath);
		end;
		
	function DatasetToStringList(Selected:TDataSet):TStringList;
		var
			Comparisions:TStringList;
		begin
				Comparisions:=TStringList.Create;
				Selected.First;
				while (not Selected.Eof)do
				begin					
					if Selected.FieldByName('Elegir').AsBoolean then						
							Comparisions.Add(Selected.FieldByName('Nombre').AsString);
           Selected.Next;
				end;
				result:=Comparisions;
		end;

	function GetConnStr(Database: String): String;
		var	
			FRegistry:TZetaRegistryServer;
			s:String;
		begin 
			s:='Provider=SQLOLEDB.1;';
			try
				FRegistry:=TzetaRegistryServer.Create;
				s:= s + 'Data source=' + FRegistry.ServerDB + ';Database=' + Database + ';uid=' + FRegistry.UserName + ';pwd=' + ZetaServerTools.Decrypt(FRegistry.PasswordRaw) + ';'; 				
				result:=s;
			finally
				FreeAndNil(FRegistry);
			end;
		end;

	procedure AddDiffDetail(Detail:TDataSet; Index:Integer; IsInsertionDetail:Boolean);		
		begin
      if(IsInsertionDetail)then begin
        FillWithNewRows(Detail, diffList[Index].NewRows);
      end
      else begin
        FillWithModRows(Detail, diffList[Index].ModRows);
      end;
		end;

	procedure FillWithModRows(Detail:TDataSet; const m:TObjectList<TModRow>);
		var
			r1:TRow;
			r2:TRow;
			m3:TModRow;
		begin
			m.Clear;
      Detail.First;
			while (not Detail.Eof) do begin
				r1:=TRow.Create(
					Detail.FieldByName('SlaveCode').AsString,
					Detail.FieldByName('SlaveDesc').AsString);

				r2:=TRow.Create(
					Detail.FieldByName('BaseCode').AsString,
					Detail.FieldByName('BaseDesc').AsString);

				m3:=TModRow.Create(r1,r2);
				m3.IsSelected:=Detail.FieldByName('Elegir').AsBoolean;
				m.Add(m3);
				Detail.Next;
			end;
		end;

	procedure FillWithNewRows(Detail:TDataSet; const n:TObjectList<TRow>);
		var
			r:TRow;
		begin
			n.Clear;
      Detail.First;
			while (not Detail.Eof) do begin				
				r:=TRow.Create(
					Detail.FieldByName('SlaveCode').AsString,
					Detail.FieldByName('SlaveDesc').AsString);
				r.IsSelected:=Detail.FieldByName('Elegir').AsBoolean;						
				n.Add(r);								
				Detail.Next;							
			end;
		end;
		
	procedure DiffListToFalse;
		var
			c:TComparision;
			i:Integer;
		begin
			for c in diffList do begin
				c.IsSelected:=false;
				for i:=0 to c.ModRows.Count - 1 do begin
					c.ModRows[i].IsSelected:=false;	
				end;
				for i:=0 to c.NewRows.Count - 1 do begin
					c.NewRows[i].IsSelected:=false;
				end;
			end;
		end;
	function DiffsWereSelected(Selected: TDataSet): Boolean;
		begin		
			selectList:=TObjectList<TComparision>.Create;			
			FillSelectedList(Selected);
			if(selectList.Count > 0) then begin
				result:=true;
			end
			else begin
				result:=false;
			end;									
		end;
		
	procedure FillSelectedList(Selected:TDataSet);
		var
			i, j:Integer;
			c:TComparision;
		begin
			Selected.First;
			while (not Selected.Eof) do begin
				i:=Selected.FieldByName('Index').AsInteger;
				if(Selected.FieldByName('Elegir').AsBoolean) then begin
					diffList[i].IsSelected:=True;
					selectList.Add(diffList[i]);
				end		
				else begin	
					c:=TComparision.Create(diffList[i].TableName);
					c.CodeColumn:=diffList[i].CodeColumn;
					c.DescriptionColumn:=diffList[i].DescriptionColumn;
					c.BaseRows:=diffList[i].BaseRows;
					c.SlaveRows:=diffList[i].SlaveRows;
															
					for j:=0 to diffList[i].NewRows.Count - 1 do begin
						if (diffList[i].NewRows[j].IsSelected) then
							c.NewRows.Add(diffList[i].NewRows[j]);
					end;
					for j:=0 to diffList[i].ModRows.Count - 1 do begin
						if (diffList[i].ModRows[j].IsSelected) then
							c.ModRows.Add(diffList[i].ModRows[j]);
					end;
					
					if(c.DiffCount > 0) then begin
						selectList.Add(c);
					end;					
				end;
				Selected.Next;				
			end;			
		end;

	function GetSelectedDiffs(Selected:TDataSet):TZetaClientDataSet;		
		var
			cds:TZetaClientDataset;
			i:Integer;
		begin	
							
			cds:=TZetaClientDataset.Create(nil);			
			
			cds.Active:= False;
			cds.Filtered := False;
			cds.Filter := '';
			cds.AddStringField('Nombre', 20);
			cds.AddIntegerField('DiffCount');		
      cds.AddIntegerField('Index');
      cds.CreateTempDataset;
			cds.Open;			

			
			for i:=0 to selectList.Count-1 do begin
				cds.Insert;
				cds.FieldByName('Nombre').AsString:=selectList[i].TableName;
				cds.FieldByName('DiffCount').AsInteger:=selectList[i].DiffCount;				
        cds.FieldByName('Index').AsInteger:=i;
				cds.Post;
			end; 
			result:=cds;
		end;
end.
