unit FDetalleComparacion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  Vcl.Menus, cxShellBrowserDialog, Vcl.StdCtrls, cxButtons, cxTextEdit, cxMemo, Vcl.ExtCtrls, dxSkinsCore, TressMorado2013,
  Vcl.ImgList, cxClasses, cxLabel, cxPC, cxNavigator, dxBarDBNav;

type
  TDetalleComparacion = class(TForm)
    memComparacion: TcxMemo;
    panelBotones: TPanel;
    ImageButtons: TcxImageList;
    btOk: TcxButton;
    Panel1: TPanel;
    lblDestino: TcxLabel;
    cxLabel1: TcxLabel;
    txtBDFuente: TcxTextEdit;
    txtBDDestino: TcxTextEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FMensaje: String;
    FTabla: String;
    FBDDestino: String;
    FBDFuente: String;
  public
    { Public declarations }
    {property Mensaje: String read FMensaje write FMensaje;
    property BDDestino: String read FBDDestino write FBDDestino;
    property BDFuente: String read FBDFuente write FBDFuente;}
  end;


function MostrarComparacion (const sTabla:String; const sBDDestino: String; const sBDFuente: String; const sMensaje: String): String;

var
  DetalleComparacion: TDetalleComparacion;

implementation

{$R *.dfm}

function MostrarComparacion (const sTabla:String; const sBDDestino: String; const sBDFuente: String; const sMensaje: String): String;
begin
     with TDetalleComparacion.Create( Application ) do
     begin
          try
             FTabla := sTabla;
             FBDDestino := sBDDestino;
             FBDFuente := sBDFuente;
             FMensaje := sMensaje;
             ShowModal;
             // Result := txtArchivo3dt.Text;
          finally
            Free;
          end;
     end;
end;

procedure TDetalleComparacion.FormShow(Sender: TObject);
begin
    Caption := Caption + ': ' + FTabla;
    memComparacion.Lines.Add(FMensaje);
    txtBDDestino.Text := FBDDestino;
    txtBDFuente.Text := FBDFuente;
    btOK.SetFocus;
end;

end.
