unit Row;

interface
  uses
    SysUtils, Classes, System.TypInfo;

  type
    TRow = class

    private
      _Code, _Description:string;
			_IsSelected:Boolean;

    public
			Constructor Create; overload;
			Constructor Create(Code, Description:String); overload; 
			
			property Code:String read _Code write _Code;
			property Description:String read _Description write _Description;
			property IsSelected:Boolean read _IsSelected write _IsSelected;			
  end;

implementation

	Constructor TRow.Create;
		begin
		end;
	
	Constructor TRow.Create(Code, Description:String);
		begin
			_Code := Code;
			_Description := Description;
			_IsSelected:=False;
		end;

end.
