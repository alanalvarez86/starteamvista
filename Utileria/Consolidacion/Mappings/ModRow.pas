unit ModRow;

interface
  uses
    SysUtils, Classes, System.TypInfo, Row;

  type
    TModRow = class

    private
      _SlaveRow, _BaseRow:TRow;
			_IsSelected:Boolean;

    public
			Constructor Create; overload;
			Constructor Create(Slave, Base:TRow); overload; 
			
			property Slave:TRow read _SlaveRow write _SlaveRow;
			property Base:TRow read _BaseRow write _BaseRow;
			property IsSelected:Boolean read _IsSelected write _IsSelected; 			
  end;

implementation

	Constructor TModRow.Create;
		begin
		end;
	
	Constructor TModRow.Create(Slave, Base:TRow);
		begin
			_SlaveRow := Slave;
			_BaseRow := Base;
			_IsSelected:=False;
		end;

end.
