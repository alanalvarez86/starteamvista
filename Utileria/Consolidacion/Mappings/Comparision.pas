unit Comparision;

interface
  uses
    System.Generics.Collections, SysUtils, Classes, ModRow, Row;

  type
    TComparision = class

    private
			_TableName, _CodeColumn, _DescriptionColumn:String;
      _SRows,_BRows:TObjectList<TRow>;
			
			_ModRows:TObjectList<TModRow>;
			_NewRows:TObjectList<TRow>;
			_IsSelected:Boolean;

    public
			Constructor Create(Table:String);
			property TableName:String read _TableName write _TableName;      
			property CodeColumn:String read _CodeColumn write _CodeColumn;
			property DescriptionColumn:String read _DescriptionColumn write _DescriptionColumn;
			property SlaveRows:TObjectList<TRow> read _SRows write _SRows;
			property BaseRows:TObjectList<TRow> read _BRows write _BRows;
			
			property ModRows:TObjectList<TModRow> read _ModRows write _ModRows;
			property NewRows:TObjectList<TRow> read _NewRows write _NewRows;			
			property IsSelected:Boolean read _IsSelected write _IsSelected;
			
			function DiffCount:Integer;
			function HaveSelected:Boolean;

  end;
  

implementation
	
	Constructor TComparision.Create(Table:String);
		begin			
			self.TableName:=Table;
			self.SlaveRows:=TObjectList<TRow>.Create;
			self.BaseRows:=TObjectList<TRow>.Create;
			self.ModRows:=TObjectList<TModRow>.Create;
			self.NewRows:=TObjectList<TRow>.Create;			
			self.IsSelected:=False;
		end;

	function TComparision.DiffCount: Integer;
		begin
			result:=self.NewRows.Count + self.ModRows.Count;
		end;

	function TComparision.HaveSelected: Boolean;
		var
			i:Integer;
		begin
			if self.IsSelected then begin
				result:=True; EXIT;
			end;
			
			for i:=0 to self.NewRows.Count-1 do begin
				if self.NewRows[i].IsSelected then begin
					result:=True; EXIT;
				end;
			end;
			
			for i:=0 to self.ModRows.Count-1 do begin
				if self.ModRows[i].IsSelected then begin
					result:=True; EXIT;
				end;
			end;
			result:=false;
		end;
end.
