unit DCompara;

interface

uses
  System.SysUtils, System.Classes;

type
  TdmCompara = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FTablas: TList;
    // function GetTablas(Index: Integer): TTablaData;
    function GetTablasCount: Integer;
  public
    { Public declarations }
  end;

var
  dmCompara: TdmCompara;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmCompara.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FTablas := TList.Create;
end;

procedure TdmCompara.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FTablas );
end;


{function TdmCompara.GetTablas( Index: Integer ): TTablaData;
begin
     Result := TTablaData( FTablas.Items[ Index ] );
end;}

function TdmCompara.GetTablasCount: Integer;
begin
     Result := FTablas.Count;
end;


end.
