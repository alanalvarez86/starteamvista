object dmSistema: TdmSistema
  OldCreateOrder = False
  Height = 213
  Width = 339
  object cdsSistBaseDatos: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsSistBaseDatosAlAdquirirDatos
    AlCrearCampos = cdsSistBaseDatosAlCrearCampos
    Left = 56
    Top = 32
  end
  object cdsSistBaseDatosLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsSistBaseDatosLookUpAlAdquirirDatos
    LookupName = 'Base de Datos'
    LookupDescriptionField = 'DB_DESCRIP'
    LookupKeyField = 'DB_CODIGO'
    OnLookupSearch = cdsSistBaseDatosLookUpLookupSearch
    Left = 56
    Top = 104
  end
end
