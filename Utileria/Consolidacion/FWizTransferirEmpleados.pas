unit FWizTransferirEmpleados;

interface

uses
 ZetaCommonClasses, Forms, dxWizardControlForm, TressMorado2013, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
 dxSkinsCore, cxContainer, cxEdit, cxProgressBar, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel,
 dxCustomWizardControl, dxWizardControl, System.Classes, Vcl.Controls, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Data.DB, cxCheckListBox, Vcl.ImgList, ZetaClientDataSet, Dialogs,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxShellBrowserDialog, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxSkinscxPCPainter, cxMemo, cxCurrencyEdit;

const
    K_DEFAULT_BIT = 'TransfiereEmpleados.txt';

type
  TWizTransferirEmpleados = class(TdxWizardControlForm)
    dxWizardControl: TdxWizardControl;
    pgFinProceso: TdxWizardControlPage;
    pgBasesDatos: TdxWizardControlPage;
    lblFuente: TcxLabel;
    lblDestino: TcxLabel;
    DataSource: TDataSource;
    ImageButtons: TcxImageList;
    cxOpenDialog: TcxShellBrowserDialog;
    DataSourceGrid: TDataSource;
    luBDDestino: TZetaKeyLookup_DevEx;
    luBDFuente: TZetaKeyLookup_DevEx;
    pgConfigurar: TdxWizardControlPage;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    chbNominaEdit: TcxCheckBox;
    cxLabel3: TcxLabel;
    memSQL: TcxMemo;
    cbConfidencialidad: TcxComboBox;
    chbIMSSEdit: TcxCheckBox;
    pgEjecutar: TdxWizardControlPage;
    cxLabel4: TcxLabel;
    cxLabel8: TcxLabel;
    lblBitacora: TLabel;
    txtBitacora: TcxTextEdit;
    btnBitacora: TcxButton;
    lblEmpleadosBase: TcxLabel;
    lblConfidencialidad: TcxLabel;
    txtEmpleadosBase: TcxCurrencyEdit;
    lblNominas: TcxLabel;
    lblIMSS: TcxLabel;
    lblEstatus: TcxLabel;
    pbProgreso: TcxProgressBar;
    pnlResumen: TPanel;
    lblResumen: TcxLabel;
    btnVerBitacora: TcxButton;
    cxLabel5: TcxLabel;
    lblEmpleadosTransferibles: TcxLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure btnBitacoraClick(Sender: TObject);
    procedure btnVerBitacoraClick(Sender: TObject);
  private
    { Private declarations }
    Mutex      : THandle;
    fAvances   : TList;
    oParametros: TZetaParams;
    FEmpleadoMaximo: Integer;
    FEmpleadoCount: Integer;
    function Processing: Boolean;
    procedure HabilitaBotones;
    procedure agregaConfidencialidad;
    function TransferirEmpleados (out mensaje: string): Boolean;
    function CallMeBack( const sMensaje: String; iValor: Integer): Boolean;
    function GetEmpleadoBase: Integer;
    procedure SetEmpleadoMaximo;
    function GetNivel0: String;
    function Warning( const sError: String; oControl: TWinControl ): Boolean;
  public
    { Public declarations }
  end;

var
  WizTransferirEmpleados: TWizTransferirEmpleados;

implementation

uses
  SysUtils, Windows, DMotorPatch, MotorPatchUtils, MotorPatchThreads, DBClient, ZetaCommonLists, ShellAPI,
  ZetaRegistryServer, DSistema, DConsolida, ZetaDialogo, ZetaCommonTools;

{$R *.dfm}

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[0..79] of Char;
begin
     Result := ShellExecute( Application.MainForm.Handle,
                             nil,
                             StrPCopy( zFileName, FileName ),
                             StrPCopy( zParams, Params ),
                             StrPCopy( zDir, DefaultDir ),
                             ShowCmd );
end;

{ TWizTransferirEmpleados }

procedure TWizTransferirEmpleados.FormCreate(Sender: TObject);
begin
    HelpContext := 4;
    Mutex := CreateMutex(nil, False, PChar(ClassName + '.Execute'));

    if Mutex = 0 then
        RaiseLastOSError;

    fAvances := TList.Create;
end;

procedure TWizTransferirEmpleados.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := not Processing;
end;

procedure TWizTransferirEmpleados.FormDestroy(Sender: TObject);
begin
    FreeAndNil(oParametros);
    FreeAndNil(fAvances);
    CloseHandle(Mutex);
end;

procedure TWizTransferirEmpleados.FormShow(Sender: TObject);
begin
     with dmSistema do
     begin
          cdsSistBaseDatos.Conectar;
          cdsSistBaseDatosLookup.Conectar;
          DataSource.DataSet := cdsSistBaseDatos;
          luBDFuente.LookupDataset := cdsSistBaseDatosLookUp;
          luBDDestino.LookupDataset := cdsSistBaseDatosLookUp;
     end;

     txtBitacora.Text := Format( '%s' + K_DEFAULT_BIT, [ ExtractFilePath( Application.ExeName ) ] );
end;

function TWizTransferirEmpleados.Processing: Boolean;
begin
    // Result := (lblEstatus.Caption <> 'Estatus') and (lblEstatus.Caption <> 'Transferencia terminada');
    Result := (lblEstatus.Caption <> 'Estatus') and
        (Pos ('terminada', lblEstatus.Caption) = 0);
end;

procedure TWizTransferirEmpleados.HabilitaBotones;
begin
  with dxWizardControl.Buttons do
  begin
       Next.Enabled := not Processing;
       Back.Enabled := not Processing;
  end;
end;

procedure TWizTransferirEmpleados.btnBitacoraClick(Sender: TObject);
var sFileName: String;
begin
      sFileName := StrDef (ExtractFileName(txtBitacora.Text), K_DEFAULT_BIT);
      cxOpenDialog.Path := ExtractFilePath( Application.ExeName );
      if cxOpenDialog.Execute then
        txtBitacora.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
end;

procedure TWizTransferirEmpleados.btnVerBitacoraClick(Sender: TObject);
begin
    ShellExecute(0, nil, 'notepad', PChar(txtBitacora.Text), nil, SW_SHOWNORMAL);
end;

procedure TWizTransferirEmpleados.dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind;
  var AHandled: Boolean);
var
  mensaje, sMensajeError : string;
  oCursor: TCursor;

begin
  case AKind of
    wcbkNext:
      begin

        if (dxWizardControl.ActivePage = pgBasesDatos) then
        begin
            if luBDFuente.Llave = VACIO then
            begin
                AHandled := Warning('El campo Fuente no puede quedar vac�o.', luBDFuente);
            end
            else if luBDDestino.Llave = VACIO then
            begin
                AHandled := Warning('El campo Destino no puede quedar vac�o.', luBDDestino);
            end
            else if luBDFuente.Llave = luBDDestino.Llave then
            begin
                AHandled := Warning('La base de datos Destino no puede ser igual a la base de datos Fuente.', luBDDestino);
            end
            else
            begin
                oCursor := Screen.Cursor;
                try
                    dmSistema.cdsSistBaseDatos.Conectar;
                    dmSistema.cdsSistBaseDatos.Locate('DB_CODIGO', luBDDestino.Llave, []);
                    if (Trim (dmSistema.cdsSistBaseDatos.FieldByName('DB_CTRL_RL').AsString) <> '3DATOS')  then
                    begin
                      AHandled := Warning('La base de datos Destino debe ser de tipo Tress.', luBDDestino)
                    end
                    else
                    begin
                      // Proceso de inicar empresas y tablas en DConsolida
                      dxWizardControl.Enabled := FALSE;
                      // oCursor := Screen.Cursor;
                      Screen.Cursor := crHourglass;
                      // Inicializar bases de datos seleccionadas
                      dmConsolida.InitEmpresas (luBDFuente.Llave, luBDDestino.Llave);

                      FEmpleadoMaximo := dmConsolida.GetEmpleadoMaximo;
                      agregaConfidencialidad;
                      SetEmpleadoMaximo;
                    end;
                finally
                    dxWizardControl.Enabled := TRUE;
                    Screen.Cursor := oCursor;
                end;
            end;

        end;

        if (dxWizardControl.ActivePage = pgConfigurar) then
        begin
            if ( GetEmpleadoBase < FEmpleadoMaximo ) then
            begin
                AHandled := Warning(Format( 'El n�mero de empleado base debe ser mayor a %d', [ FEmpleadoMaximo ] ), txtEmpleadosBase);
                SetEmpleadoMaximo;
            end
            else
            begin
                FEmpleadoCount := dmConsolida.GetEmpleadoCount( ZetaCommonTools.BorraCReturn( Trim( memSQL.Lines.Text ) ), sMensajeError );
                if ( FEmpleadoCount = 0 ) then
                begin
                     AHandled := Warning ('No hay empleados por transferir.', memSQL);
                end
                else
                begin
                    if (FEmpleadoCount = -1) then
                    begin
                        AHandled := Warning (
                            Format ('Error al determinar cantidad de empleados por transferir.' + CR_LF + '%s', [sMensajeError]), memSQL);
                    end
                    else
                        lblEmpleadosTransferibles.Caption := IntToStr(FEmpleadoCount);
                end;
            end;
            // Par�metros
            lblEmpleadosBase.Caption := txtEmpleadosBase.Text;
            lblConfidencialidad.Caption := cbConfidencialidad.Text;
            lblNominas.Caption := 'Transferir n�minas:   ' + BoolAsSiNo (chbNominaEdit.Checked);
            lblIMSS.Caption := 'Transferir liquidaciones de IMSS:   ' + BoolAsSiNo (chbIMSSEdit.Checked);
        end;

        if (dxWizardControl.ActivePage = pgEjecutar) then
        begin
            if StrVacio (ExtractFileName (txtBitacora.Text)) then
                AHandled := Warning('Debe proporcionar un nombre de archivo de bit�cora',  txtBitacora)
            else
            begin
                pnlResumen.Visible := FALSE;
                pbProgreso.Position := 0;
                Application.ProcessMessages;
                dxWizardControl.ActivePage := pgFinProceso;
                oCursor := Screen.Cursor;
                try
                    dxWizardControl.Enabled := FALSE;
                    // oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;

                    // Ejecutar proceso de transferencia de empleados
                    if TransferirEmpleados (mensaje) then
                        pnlResumen.Visible := TRUE;
                    // Fin de Proceso
                finally
                    dxWizardControl.Enabled := TRUE;
                    Screen.Cursor := oCursor;
                end;
            end;
        end;

      end;
    wcbkCancel:
      begin
        if not Processing then
          ModalResult := mrCancel;
      end;


    wcbkFinish:
      ModalResult := mrOk;

  end;
  HabilitaBotones();
end;

procedure TWizTransferirEmpleados.agregaConfidencialidad;
var i: Integer;
begin
    with dmConsolida do
    begin
        with cbConfidencialidad do
        begin
            with Properties.Items do
            begin
                 try
                    Clear;
                    BeginUpdate;
                    Add( 'Confidencialidad ya asignada' );
                    for i := 0 to ( Nivel0.Count - 1 ) do
                    begin
                         Add( Nivel0.Values[ Nivel0.Names[ i ] ] );
                    end;
                 finally
                        EndUpdate;
                 end;
            end;
            ItemIndex := 0;
        end;
    end;
end;

procedure TWizTransferirEmpleados.dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage;
  var AAllow: Boolean);
begin
  if ANewPage = pgEjecutar then
    dxWizardControl.Buttons.Next.Caption := 'Aplicar'
  else
    dxWizardControl.Buttons.Next.Caption := 'Siguiente';
end;

function TWizTransferirEmpleados.TransferirEmpleados (out mensaje: string): Boolean;
var
   lOk, bTransferirEmpleados : Boolean;
   FLog: TTransferLog;
begin
     try
         FLog := TTransferLog.Create;
         try
              if StrVacio(ExtractFileExt(txtBitacora.Text)) then
                  FLog.Start( txtBitacora.Text + '.txt' )
              else
                FLog.Start( txtBitacora.Text );

              lblEstatus.Caption := 'Empezando transferencia de empleados';

              // Ajustar m�ximo de la barra de progreso a la cantidad de empleados a transferir
              pbProgreso.Properties.Max := FEmpleadoCount;

              // Ejecutar proceso de dmConsolida.TransferirEmpleados
              try
                bTransferirEmpleados := dmConsolida.TransferirEmpleados(
                          CallMeBack,
                          FLog,
                          GetEmpleadoBase,
                          chbNominaEdit.Checked,
                          chbIMSSEdit.Checked,
                          ZetaCommonTools.BorraCReturn( Trim( memSQL.Lines.Text )),
                          GetNivel0 );
              except
                  on Error: Exception do
                  begin
                    bTransferirEmpleados := FALSE;
                    lOk := FALSE;
                  end;
              end;

              if bTransferirEmpleados then
                lblEstatus.Caption := 'Transferencia terminada'
              else
                lblEstatus.Caption := 'Transferencia terminada con errores';

              lOk := True;
         finally
            with FLog do
            begin
                 Close;
                 Free;
            end;
         end;
     Except
        on e:exception do
        begin
            ZetaDialogo.ZError('Error', mensaje, 0);
            lOk := FALSE;
        end;
     end;

     Result := lOk;
end;

function TWizTransferirEmpleados.CallMeBack( const sMensaje: String; iValor: Integer): Boolean;
begin
     lblEstatus.Caption := sMensaje;
     pbProgreso.Position := iValor;
     Application.ProcessMessages;
     Result := TRUE;
end;

function TWizTransferirEmpleados.GetEmpleadoBase: Integer;
begin
     Result := StrToInt(txtEmpleadosBase.Text);
end;

procedure TWizTransferirEmpleados.SetEmpleadoMaximo;
begin
     txtEmpleadosBase.Text := IntToStr(FEmpleadoMaximo + 1);
end;

function TWizTransferirEmpleados.GetNivel0: String;
begin
     with cbConfidencialidad do
     begin
        if ( ItemIndex < 1 ) then
           Result := ''
        else
            Result := dmConsolida.Nivel0.Names[ ( ItemIndex - 1 ) ];
     end;
end;

function TWizTransferirEmpleados.Warning( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZWarning( Caption, sError, 0, mbOK );
     oControl.SetFocus;
     Result := TRUE;
end;

end.
