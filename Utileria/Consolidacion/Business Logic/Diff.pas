unit Diff;

interface
	uses 	  
		SysUtils, Generics.Collections, Classes, Comparision, Row, Variants, ModRow;

	function Get(CompareList:TObjectList<TComparision>):TObjectList<TComparision>;
	function Difference(c:TComparision):TComparision;
	function FindRow(SlaveRow:TRow; BaseList:TObjectList<TRow>):TRow;

implementation
	 
	function Get(CompareList:TObjectList<TComparision>):TObjectList<TComparision>;
		var
			r:TObjectList<TComparision>;
			c:TComparision;
			temp:TComparision;
		begin
			r:=TObjectList<TComparision>.Create;
			for c in CompareList do begin
				temp:=Difference(c);
				if (temp.DiffCount > 0) then
					r.Add(temp);
			end;			 									
			result:=r;
		end;

	function Difference(c:TComparision):TComparision;
		var	
			row, BaseRow:TRow;			
		begin		
			for row in c.SlaveRows do begin
				BaseRow:=FindRow(row, c.BaseRows);
				if(BaseRow <> nil) then begin
					if(BaseRow.Code <> row.Code) then									 
						c.ModRows.Add(TModRow.Create(row, BaseRow))				
				end
				else	
					c.NewRows.add(TRow.Create(row.code, row.Description));				
			end;
			result:=c;									
		end;		

	function FindRow(SlaveRow:TRow; BaseList:TObjectList<TRow>):TRow;
		var	
			temp, row:TRow;			
		begin	
			temp:=nil;		
			for row in BaseList do begin
				if(SlaveRow.Description = row.Description) then begin
					temp:=row;
					Break;
				end;
			end;
			result:=temp;
		end;
end.
