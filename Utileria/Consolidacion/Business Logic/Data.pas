unit Data;

interface
	uses 	  
		SysUtils, Generics.Collections, Classes, Comparision, Datasnap.DBClient, Data.DB, Data.Win.ADODB, Row, Variants;

	function Get(CompareList:TObjectList<TComparision>; BaseConnStr, SlaveConnStr:String):TObjectList<TComparision>;
	function IsValid(Comparision:TComparision):Boolean;
	function GetRows(Query, ConnStr:String):TObjectList<TRow>;
	function IsDBConnected(ConnStr:String):Boolean;  	

implementation

	var ADOConn : TADOConnection;
	
	 
	function Get(CompareList:TObjectList<TComparision>; BaseConnStr, SlaveConnStr:String):TObjectList<TComparision>;
		var
			c:TComparision;
			r:TObjectList<TComparision>;			
			qry:String;
		begin
			r:=TObjectList<TComparision>.Create;
			
			for c in CompareList do 
			begin							
				qry:=format('SELECT %s, %s FROM %s WHERE %s<>'''' AND %s<>'''' ', [c.CodeColumn ,c.DescriptionColumn, c.TableName, c.CodeColumn ,c.DescriptionColumn]);
				c.BaseRows := GetRows(qry, BaseConnStr);
				c.SlaveRows := GetRows(qry, SlaveConnStr);								
				
				if IsValid(c) then
					r.Add(c);
			end;			
			result:=r;
		end;
		

	function IsValid(Comparision:TComparision):Boolean;
		begin
			if (Comparision.SlaveRows.Count < 1) then
				result:=false
			else
				result:=true; 
		end;
		
	function GetRows(Query, ConnStr:String):TObjectList<TRow>;
		var
			ADOQuery:TADOQuery;
			RowList:TObjectList<TRow>;
		begin		
			ADOQuery := TADOQuery.Create(nil);			
			RowList:=TObjectList<TRow>.Create;				

			if(IsDBConnected(ConnStr)) then
      	begin
					ADOQuery.Connection := ADOConn;
					ADOQuery.SQL.Text := Query;					
					ADOQuery.Prepared := true;		

					try
						ADOQuery.Open;
						while(not ADOQuery.Eof) do
						begin							
							if(
									(ADOQuery.Fields[0].Value <> Null)
									AND  
									(ADOQuery.Fields[1].Value <> Null) 
								) then
								RowList.Add(TRow.Create(ADOQuery.Fields[0].Value, ADOQuery.Fields[1].Value)); 			
							ADOQuery.Next;													
						end;
						
						ADOQuery.Close;
					except
						on e: Exception do							
					end;
					ADOQuery.Free;
				end;
			
				result := RowList;

		end;

	

	function IsDBConnected(ConnStr:String):Boolean;
    begin
      ADOConn := TADOConnection.Create(nil);
      ADOConn.ConnectionString := ConnStr;
      ADOConn.LoginPrompt := False;
      try
        begin
            ADOConn.Connected := True;
            result := True;
        end;
      except
        on e: EADOError do
          result := false;
      end;
    end;
		

end.
