unit Writer;

interface
	uses 	  
		Comparision, SysUtils, Generics.Collections, Classes, Row, ModRow;

  type
    TWriter = class

    private
			_CompareList:TObjectList<TComparision>;
			_BaseQry, _SlaveQry:TStringList;			
			function GetMax(RowList:TObjectList<TRow>):Integer;
      procedure SaveToFile(BaseQryPath, SlaveQryPath:String);
			procedure UpdateSlaveRowsToDelta(Comparision:TComparision);
			procedure ResolveInserts(Comparision:TComparision);
			procedure ResolveModified(Comparision:TComparision);
			
    public
			property CompareList:TObjectList<TComparision> read _CompareList write _CompareList;
			Constructor Create(CompareList:TObjectList<TComparision>);								
			procedure ToSQLFile(BaseQryPath, SlaveQryPath:String);
			
		end;

implementation

uses
    ZetaCommonTools, ZetaCommonClasses;

	Constructor TWriter.Create(CompareList:TObjectList<TComparision>);
	begin
		self.CompareList:=CompareList;
	end;

	procedure TWriter.ToSQLFile(BaseQryPath, SlaveQryPath:String);
		var
			c:TComparision;
		begin						
			_BaseQry:=TStringList.Create;
			_SlaveQry:=TStringList.Create;
			for c in CompareList do begin
				if (c.HaveSelected) then begin
					UpdateSlaveRowsToDelta(c);							
					ResolveInserts(c);
					ResolveModified(c)
				end;
			end; 							
			SaveToFile(BaseQryPath, SlaveQryPath);
			_BaseQry.Free;
			_SlaveQry.Free		
		end;

	function TWriter.GetMax(RowList:TObjectList<TRow>):Integer;
		var
			row:TRow;
			code, high:Integer;
      noSpaces:String;
		begin
			high:=0;
			for row in RowList do begin
        noSpaces:=StrTransAll( row.Code, ' ', VACIO );
				if Integer.TryParse(noSpaces, code) then begin
					if  code > high then
						high:=code;
				end;
			end;
			result:=high;
		end;
		      
	procedure TWriter.SaveToFile(BaseQryPath, SlaveQryPath:String);
		begin
			_BaseQry.SaveToFile(BaseQryPath);
			_SlaveQry.SaveToFile(SlaveQryPath);
		end;
		
	procedure TWriter.UpdateSlaveRowsToDelta(Comparision:TComparision);	
		var
			delta:Integer;
			row:TRow;
			mRow:TModRow;
		begin
			delta:=GetMax(Comparision.SlaveRows);
			_SlaveQRY.Append('------'+Comparision.TableName+'------');
			_SlaveQRY.Append('--ApplyDeltaUpdate');
			
			for row in Comparision.NewRows do begin
				delta:= delta + 1;
				_SlaveQry.Append(format('UPDATE %s SET %s = ''%d'' WHERE %s = ''%s'' ' +CR_LF + 'GO', 
					[Comparision.TableName, Comparision.CodeColumn, delta, Comparision.CodeColumn, row.Code]));									
			end;
			
			for mRow in Comparision.ModRows do begin
				delta:= delta + 1;
				_SlaveQry.Append(format('UPDATE %s SET %s = ''%d'' WHERE %s = ''%s'' ' +CR_LF + 'GO', 
					[Comparision.TableName, Comparision.CodeColumn, delta, Comparision.CodeColumn, mRow.Slave.Code]));									
			end;
		end;

	procedure TWriter.ResolveInserts(Comparision:TComparision);
		var
			delta:Integer;
			row:TRow;
		begin					 
			delta:=GetMax(Comparision.BaseRows);
			_BaseQry.Append('------'+Comparision.TableName+'------');
			_BaseQry.Append('--Insert to Destino');
						
			_SlaveQry.Append('--Update to Destino Insert');
			
			for row in Comparision.NewRows do begin										
				if (row.IsSelected OR Comparision.IsSelected) then begin
					delta:= delta + 1;
					row.Code:=IntToStr(delta);
					_BaseQry.Append(format('INSERT INTO %s (%s, %s) VALUES (''%s'', ''%s'') ' + CR_LF + 'GO', 
						[Comparision.TableName, Comparision.CodeColumn, Comparision.DescriptionColumn, row.code, StrTransAll( row.Description, UnaCOMILLA, UnaCOMILLA+UnaCOMILLA+UnaCOMILLA+UnaCOMILLA ) ]));
					
					_SlaveQry.Append(format('UPDATE %s SET %s = ''%d'' WHERE %s = ''%s'' ' + CR_LF + 'GO', 
						[Comparision.TableName, Comparision.CodeColumn, delta, Comparision.DescriptionColumn, StrTransAll( row.Description, UnaCOMILLA,  UnaCOMILLA+UnaCOMILLA+UnaCOMILLA+UnaCOMILLA )]));
				end;
			end;
		end;	
			
	procedure TWriter.ResolveModified(Comparision:TComparision);
		var
			row:TModRow;
		begin					 			
			_SlaveQry.Append('--Update Modified Fields');
			
			for row in Comparision.ModRows do begin										
				if (row.IsSelected OR Comparision.IsSelected) then begin		
					_SlaveQry.Append(format('UPDATE %s SET %s = ''%s'' WHERE %s = ''%s'' ' +CR_LF + 'GO', 
						[Comparision.TableName, Comparision.CodeColumn, row.Slave.Code, Comparision.CodeColumn, row.Base.Code]));
				end;
			end;
		end;				

end.