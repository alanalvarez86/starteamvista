unit MetaData;

interface
	uses 	  
		SysUtils, Generics.Collections, Classes, Comparision, Datasnap.DBClient, Data.DB, Data.Win.ADODB;

	function Get(CompareList:TObjectList<TComparision>; ConnStr:String):TObjectList<TComparision>;
	function IsValid(Comparision:TComparision):Boolean;
	function GetCodeColumn(TableName, ConnStr:String):String;
	function GetDescriptionColumn(TableName, ConnStr:String):String;
	function IsDBConnected(ConnStr:String):Boolean;  	

implementation

	var ADOConn : TADOConnection;
	
	 
	function Get(CompareList:TObjectList<TComparision>; ConnStr:String):TObjectList<TComparision>;
		var
			c:TComparision;
			r:TObjectList<TComparision>;
		begin
			r:=TObjectList<TComparision>.Create;
			for c in CompareList do 
			begin				
				c.CodeColumn := GetCodeColumn(c.TableName, ConnStr);
				c.DescriptionColumn := GetDescriptionColumn(c.TableName, ConnStr);
				
				if IsValid(c) then
					r.Add(c);
			end;
			result:=r;
		end;
		

	function IsValid(Comparision:TComparision):Boolean;
		begin
			if (Comparision.CodeColumn <> '')  AND (Comparision.DescriptionColumn <> '') then
				result:=true
			else
				result:=false; 
		end;
		
	function GetCodeColumn(TableName, ConnStr:String):String;
		var
			ADOQuery :  TADOQuery;
			qry : string;
		begin		
			qry := 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), ''IsPrimaryKey'') = 1 AND table_name = :TableName';

			ADOQuery := TADOQuery.Create(nil);	

			if(IsDBConnected(ConnStr)) then
      	begin

					ADOQuery.Connection := ADOConn;
					ADOQuery.SQL.Text := qry;
					ADOQuery.Parameters.ParamByName('TableName').Value := Tablename;
					ADOQuery.Prepared := true;		

					try
						ADOQuery.Open;
						result := ADOQuery.Recordset.Fields[0].Value;
						ADOQuery.Close;
					except
						on e: Exception do
							result := '';
					end;
					ADOQuery.Free;
				end
			else
				result := '';
		
		end;
		
	function GetDescriptionColumn(TableName, ConnStr:String):String;
		var
			ADOQuery :  TADOQuery;
			qry : string;
		begin		
			qry := 'SELECT EN_ATDESC FROM R_ENTIDAD WHERE EN_TABLA = :TableName';

			ADOQuery := TADOQuery.Create(nil);	

			if(IsDBConnected(ConnStr)) then
      	begin

					ADOQuery.Connection := ADOConn;
					ADOQuery.SQL.Text := qry;
					ADOQuery.Parameters.ParamByName('TableName').Value := Tablename;
					ADOQuery.Prepared := true;		

					try
						ADOQuery.Open;
						result := ADOQuery.Recordset.Fields[0].Value;
						ADOQuery.Close;
					except
						on e: Exception do
							result := '';
					end;
					ADOQuery.Free;
				end
			else
				result := '';

		end;

	

	function IsDBConnected(ConnStr:String):Boolean;
    begin
      ADOConn := TADOConnection.Create(nil);
      ADOConn.ConnectionString := ConnStr;
      ADOConn.LoginPrompt := False;
      try
        begin
            ADOConn.Connected := True;
            result := True;
        end;
      except
        on e: EADOError do
          result := false;
      end;
    end;
		

end.
