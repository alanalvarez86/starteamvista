﻿program ConsolidacionTRESS;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Vcl.Forms,
  Sistema_TLB in '..\..\MTS\Sistema_TLB.pas',
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  Login_TLB in '..\..\MTS\Login_TLB.pas',
  DSistema in 'DSistema.pas' {dmSistema: TDataModule},
  FWizCompararTablas in 'FWizCompararTablas.pas' {WizCompararTablas},
  FWizTransferirTablas in 'FWizTransferirTablas.pas' {WizTransferirTablas},
  FDetalleComparacion in 'FDetalleComparacion.pas' {DetalleComparacion},
  FWizTransferirEmpleados in 'FWizTransferirEmpleados.pas' {WizTransferirEmpleados},
  FWizConsolidar in 'FWizConsolidar.pas' {WizConsolidar},
  ZDlgSiNoSiTodos in 'ZDlgSiNoSiTodos.pas' {ZDlgSiNoSiATodos},
  FConsolidaShell in 'FConsolidaShell.pas' {FConsolidacionTRESS},
  DConsolida in 'DConsolida.pas' {dmConsolida: TDataModule};

//  Differences in 'Business Logic\Differences.pas',
//  MetaData in 'Business Logic\MetaData.pas',
//  Comparision in 'Mappings\Comparision.pas',
//  Row in 'Mappings\Row.pas',
//  Data in 'Business Logic\Data.pas',
//  Diff in 'Business Logic\Diff.pas',
//  ModRow in 'Mappings\ModRow.pas',
//  Writer in 'Business Logic\Writer.pas',
//  FDiffDetail in 'Components\FDiffDetail.pas' {Form1};

{$R *.res}

{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recurso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\..\Traducciones\Spanish.RES}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.HelpFile := 'ConsolidaciónTRESS.chm';
  Application.Title := 'ConsolidacionTRESS';
  Application.CreateForm(TdmSistema, dmSistema);
  Application.CreateForm(TdmConsolida, dmConsolida);
  Application.CreateForm(TFConsolidacionTRESS, FConsolidacionTRESS);
  Application.CreateForm(TDetalleComparacion, DetalleComparacion);
  Application.CreateForm(TWizTransferirEmpleados, WizTransferirEmpleados);
  Application.CreateForm(TWizConsolidar, WizConsolidar);
  Application.CreateForm(TZDlgSiNoSiATodos, ZDlgSiNoSiATodos);
  Application.CreateForm(TFConsolidacionTRESS, FConsolidacionTRESS);
  Application.CreateForm(TdmConsolida, dmConsolida);
  Application.Run;
end.
