unit FAutoServerLoader;

interface
{$ifdef MSSQL}
{$define DEBUGSENTINEL}
{$define SENTINELVIRTUAL}
{$endif}

uses SysUtils, Classes,
{$ifdef DEBUGSENTINEL}
     ZetaLicenseMgr,
     DZetaServerProvider,
{$ifdef SENTINELVIRTUAL}
     FSentinelRegistry,
{$endif}
{$endif}
     FAutoClasses,
     FAutoServer;


{$ifdef SENTINELVIRTUAL}
   {ESTO VA PARA OTRA UNIDAD}
type TCalculoEmpleadosTRESSCFGThread = class( TThread )
  private
    { Private declarations }
     FZetaProvider: TdmZetaServerProvider;
     oLicenseMgr : TLicenseMgr;
     oAutoServerCalculo: TAutoServer;
  public
    termino : Boolean;
    property Provider : TdmZetaServerProvider  read FZetaProvider write FZetaProvider;

  protected
    procedure InitValues;
    procedure FreeValues;
    procedure Execute; override;
  end;
{$endif}

type TAutoServerLoader = class(TObject)
  private
  public
    { Public declarations }
    class procedure LoadSentinelInfo( FAutoServer: TAutoServer );
{$ifdef SENTINELVIRTUAL}
    class function CargarVirtualSentinelInfo( FAutoServer: TAutoServer; lEscribirAutorizacion : boolean = FALSE ) : Boolean;
    procedure EventLog(const sMensaje: String);
    procedure ErrorLog(const sMensaje: String);
{$endif}


end;

var
   FCalculandoEmpleadosLoader : boolean;


implementation

{ TAutoServerLoader }

class procedure TAutoServerLoader.LoadSentinelInfo( FAutoServer: TAutoServer );
{$ifdef DEBUGSENTINEL}
var
   oZetaProvider: TdmZetaServerProvider;
   oLicenseMgr: TLicenseMgr;
{$ifdef SENTINELVIRTUAL}
   oAutoServerLoader : TAutoServerLoader;
   oCalculoThread  : TCalculoEmpleadosTRESSCFGThread;
{$endif}
{$endif}
begin
{$ifdef DEBUGSENTINEL}
  with FAutoServer do
  begin
      oZetaProvider := TdmZetaServerProvider.Create( nil );
      {$ifdef SENTINELVIRTUAL}
      oAutoServerLoader := TAutoServerLoader.Create;
      {$endif}
      try
        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
        try
           OnSetData := oLicenseMgr.AutoSetData;
           OnGetData := oLicenseMgr.AutoGetData;
           try
              Load;
              if EsDemo then
              begin
                 {$ifdef SENTINELVIRTUAL}
                 try
                     Autorizar; // Obtenemos autorización para cargar lso datos de Conteo
                     FSentinelRegistry.FEmpleadosConteoVirtual := FAutoServer.EmpleadosConteo;
                     if ( not SentinelOK ) and (FSentinelRegistry.ExisteSentinelVirtual) and ( EsDemo ) then
                     begin
                        if FSentinelRegistry.ValidarSentinelVirtual(oAutoServerLoader.EventLog, oAutoServerLoader.ErrorLog) then
                           Autorizar;
                     end;

                     if ( FAutoServer.EmpleadosConteo = 0 ) and ( not FCalculandoEmpleadosLoader )  then
                     begin
                           FCalculandoEmpleadosLoader := TRUE;
                           oCalculoThread  := TCalculoEmpleadosTRESSCFGThread.Create(False);
                           oCalculoThread.Resume;
                     end;
                 except
                   on Error: Exception do
                   begin
                        
                   end;
                 end;


                 {$endif}
              end;
           finally
                  OnSetData := nil;
                  OnGetData := nil;
           end;
        finally
               FreeAndNil( oLicenseMgr );
        end;
      finally
            FreeAndNil( oZetaProvider );
            FreeAndNil( oAutoServerLoader );
      end;
  end;
{$else}
  with FAutoServer do
  begin
       Load;
  end;
{$endif}
end;


{$ifdef SENTINELVIRTUAL}
class function TAutoServerLoader.CargarVirtualSentinelInfo( FAutoServer: TAutoServer; lEscribirAutorizacion : boolean  ) : Boolean;
var
   oZetaProvider: TdmZetaServerProvider;
   oLicenseMgr: TLicenseMgr;
   oAutoServerLoader : TAutoServerLoader;          
begin
  Result := False;
  with FAutoServer do
  begin
      oZetaProvider := TdmZetaServerProvider.Create( nil );
      oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;

      oAutoServerLoader := TAutoServerLoader.Create;
      try
        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
        try
           OnSetData := oLicenseMgr.AutoSetData;
           OnGetData := oLicenseMgr.AutoGetData;
           try
             if ( not SentinelOK ) and (FSentinelRegistry.ExisteSentinelVirtual) then
             begin
                if FSentinelRegistry.ValidarSentinelVirtual(oAutoServerLoader.EventLog, oAutoServerLoader.ErrorLog) then
                begin
                    Autorizar;
                    Result := True;
                end;
             end;
           finally
                  OnSetData := nil;
                  OnGetData := nil;
                  OnGetDataAdditional := nil;
           end;
        finally
               FreeAndNil( oLicenseMgr );
        end;
      finally
            FreeAndNil( oZetaProvider );
            FreeAndNil( oAutoServerLoader );
      end;
  end;
end;


procedure TAutoServerLoader.ErrorLog(const sMensaje: String);
begin
//
end;

procedure TAutoServerLoader.EventLog(const sMensaje: String);
begin
//
end;
{$endif}


{$ifdef SENTINELVIRTUAL}
{ TCalculoEmpleadosThread }

procedure TCalculoEmpleadosTRESSCFGThread.Execute;
var
   oLoader : TAutoServerLoader;
begin
  inherited;
  FSentinelRegistry.FConteoVirtualPorThread := TRUE;
  InitValues;
  oLoader := TAutoServerLoader.Create;
  try
     oLoader.CargarVirtualSentinelInfo( oAutoServerCalculo, True );
  finally
  end;
  FreeValues;
  FCalculandoEmpleadosLoader := FALSE;
  FSentinelRegistry.FConteoVirtualPorThread := FALSE
end;

procedure TCalculoEmpleadosTRESSCFGThread.InitValues;
begin
   FZetaProvider := TdmZetaServerProvider.Create( nil );
   FZetaProvider.EmpresaActiva := FZetaProvider.Comparte;
   oLicenseMgr := TLicenseMgr.Create( FZetaProvider );

   oAutoServerCalculo := TAutoServer.Create;
   {$ifdef INTERBASE}
   with oAutoServerCalculo do
   begin
        SQLType := engInterbase;
        AppType := atAmbas;
   end;
   {$endif}
   {$ifdef MSSQL}
   with oAutoServerCalculo do
   begin
        SQLType := engMSSQL;
        AppType := atCorporativa;
   end;
   {$endif}

end;

procedure TCalculoEmpleadosTRESSCFGThread.FreeValues;
begin
   FreeAndNil( FZetaProvider );
   FreeAndNil (oLicenseMgr);
end;
{$endif}


end.
