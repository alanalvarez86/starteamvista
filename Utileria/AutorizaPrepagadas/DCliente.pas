unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBasicoCliente,
     Login_TLB,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TdmCliente = class(TBasicoCliente)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    procedure CargaActivosSistema(Parametros: TZetaParams);
    { Private declarations }
  public
    { Public declarations }
    function BuscaCompany( const sCodigo: String ): Boolean;
    function Empresa: OleVariant;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    function GetAuto: OleVariant;
    procedure CargaActivosTodos( Parametros: TZetaParams ); override;
  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses ZetaRegistryCliente,
     ZetaCommonTools;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     InitClientRegistry;                                               
     SetUsuario( 1 );
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     ClearClientRegistry;
     inherited;
end;

function TdmCliente.BuscaCompany( const sCodigo: String ): Boolean;
begin
     with cdsCompany do
     begin
          Data := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
          Result := Locate( 'CM_CODIGO', sCodigo, [] );
     end;
end;

function TdmCliente.Empresa: OleVariant;
begin
     Result := BuildEmpresa( cdsCompany );
end;

function TdmCliente.GetAuto: OleVariant;
begin
     Result := Servidor.GetAuto;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', Date );
          AddDate( 'FechaDefault', Date );
          AddInteger( 'YearDefault', TheYear( Date ) );
          AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', VACIO );
          AddString( 'CodigoEmpresa', VACIO );
     end;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosSistema(Parametros);
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
end;

end.
