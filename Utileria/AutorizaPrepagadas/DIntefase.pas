unit DIntefase;

interface

uses
  SysUtils, DateUtils, Classes,Graphics, Controls, Forms, Dialogs, Db, DBClient, FAutoClasses, Variants,
  ZetaCommonClasses,
  ZetaCommonLists,
  ZetaCommonTools,
  ZetaASCIIFile,
  ZetaClientDataSet,
  Consultas_TLB,
  Asistencia_TLB;

const
     P_EMPRESA        = 'EMPRESA';
     P_FECHA_INI      = 'FECHA';
     P_FECHA_FIN      = 'FECHAFINAL';
     P_USUARIO        = 'USUARIO';
     P_CLAVE          = 'CLAVE';


type
  TdmInterfase = class(TDataModule)
    cdsEmpleadosVaca: TZetaClientDataSet;
    cdsHorario: TZetaClientDataSet;
    cdsAutoExtPrep: TZetaClientDataSet;
    cdsTarjetas: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsAutoExtPrepAlAdquirirDatos(Sender: TObject);
    procedure cdsAutoExtPrepNewRecord(DataSet: TDataSet);
    procedure cdsAutoExtPrepAlEnviarDatos(Sender: TObject);
  private
    { Private declarations }
    FServerConsultas: IdmServerConsultasDisp;
    FServerAsistencia: IdmServerAsistenciaDisp;
    FBitacora: TAsciiLog;
    FListaParametros: TStrings;
    FUsuario: string;
    FClave: string;
    FEmpresa: String;
    FRecFechaInicial: TDate;
    FRecFechaFin: TDate;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
    procedure GetListaParametros;
    procedure AgregaAutorizaciones;
    procedure ParametrosPrepara;
    procedure BitacoraAutorizarPrep;
    procedure LogInit;
    procedure Log( const sTexto: String );
    procedure LogError( const sTexto: String );
    procedure LogEnd;
    procedure GetTiemposCompensacion;
    function GetEmpleadosVacacion: Boolean;
    function LoginBatch: Boolean;
  public
    { Public declarations }
    procedure Procesar;
    procedure DespliegaAyuda( sComando: string );
    property ListaParametros: TStrings read FListaParametros;
  end;

var
  dmInterfase: TdmInterfase;

implementation

uses
     FAutoServer,
     ZBaseThreads,
     ZAccesosMgr,
     ZAccesosTress,
     DCliente,
     ZASCIITools;

{$R *.dfm}

const
     K_MALA_CONFI = 'La L�nea de Comandos Est� Mal Configurada.';

     K_QRY_EMPLEADOS_VACA = 'select VACACION.CB_CODIGO, VACACION.VA_FEC_INI, VACACION.VA_FEC_FIN  from VACACION ' +
                            'left outer join COLABORA ON ( VACACION.CB_CODIGO = COLABORA.CB_CODIGO ) '+
                            'where ( VA_FEC_INI <= %1:s and VA_FEC_FIN > %0:s ) and '+
                            '( ( select TU_TEXTO from TURNO where ( TU_CODIGO = ( dbo.SP_KARDEX_CB_TURNO ( %0:s, COLABORA.CB_CODIGO )  ) ) )  = %2:s  )';


     K_QRY_HORARIOS = 'select HO_CODIGO, HO_NUMERO from HORARIO';

     K_RELLENO     = 10;
     K_HORA_NULA = '----';



function TdmInterfase.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( dmCliente.CreaServidor( CLASS_dmServerAsistencia, FServerAsistencia ) );
end;

function TdmInterfase.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;



procedure TdmInterfase.BitacoraAutorizarPrep;
begin
     Log( '** Par�metros para autorizar tiempo de compensaci�n**' );

     Log( Format( 'Fecha inicial: %s', [FechaCorta( FRecFechaInicial ) ] ) );

     Log( Format( 'Fecha final: %s', [FechaCorta( FRecFechaFin ) ] ) );

     Log( StringOfChar( '-', 40 ) );

end;

procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     FBitacora := TAsciiLog.Create;
     dmCliente := TdmCliente.Create( Self );
     FListaParametros := TStringList.Create;
     GetListaParametros;
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FListaPArametros );
     FreeAndNil( dmCliente );
     FreeAndNil( FBitacora );
end;


procedure TdmInterfase.DespliegaAyuda(sComando: string);
begin
     WriteLn( '' );
     WriteLn( 'Formato para autorizar tiempo de compensaci�n' );
     WriteLn( '===============================' );
     WriteLn( Format( '%s %s=<Y> %s=<Z> %s=<I> %s=<F>', [ sComando, P_USUARIO, P_CLAVE, P_FECHA_INI, P_FECHA_FIN ] ) );
     WriteLn( '' );
     WriteLn( '*** Par�metros ***' );
     WriteLn( '' );
     WriteLn( '<X>: C�digo de empresa (COMPANY.CM_CODIGO)' );
     WriteLn( '<Y>: Usuario del Sistema TRESS (US_CODIGO)' );
     WriteLn( '<Z>: Clave usuario Sistema TRESS (US_PASSWRD)' );
     WriteLn( '<I>: Fecha Inicial: Hoy = 0, Ayer = -1, Anteayer = -2, etc. ( Default = 0  )');
     WriteLn( '<F>: Fecha Final: Hoy = 0, Ayer = -1, Anteayer = -2, etc. ( Default = 0  )');

end;

procedure TdmInterfase.GetTiemposCompensacion;
begin
     cdsHorario.Data:= ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, K_QRY_HORARIOS );
end;

function TdmInterfase.GetEmpleadosVacacion: Boolean;
begin
     with cdsEmpleadosVaca do
     begin
          Data:= ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( K_QRY_EMPLEADOS_VACA, [ EntreComillas( FechaAsStr( FRecFechaInicial ) ), EntreComillas( FechaAsStr( FRecFechaFin ) ), EntreComillas( K_GLOBAL_SI ) ] ) );
          Result := ( not IsEmpty );
          if not Result then
          begin
               LogError( Format( 'No hay empleados a registrar tiempo de compensaci�n del %s al %s : ', [ FechaCorta(FRecFechaInicial),
                                                                                                          FechaCorta(FRecFechaFin) ]));
          end;
     end;
end;

procedure TdmInterfase.LogError(const sTexto: String);
begin
     Log( 'ERROR: ' + sTexto );
     Log( '' );
end;


procedure TdmInterfase.GetListaParametros;
var
   i: integer;
begin
     with FListaParametros do
     begin
          Clear;
          for i := 1 to ParamCount do
          begin
               // se modifico uppercase los parametros respetan minusculas
               Add( ParamStr( i ) );
          end;
     end;
end;

procedure TdmInterfase.Log(const sTexto: String);
begin
     FBitacora.WriteTexto( sTexto );
     WriteLn( sTexto );
end;

procedure TdmInterfase.LogEnd;
begin
     with FBitacora do
     begin
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Final %s ' + StringOfChar( '*', K_RELLENO ) );
          Close;
     end;
end;

procedure TdmInterfase.LogInit;
begin
     with FBitacora do
     begin
          if not Used then
          begin
               with Application do
               begin
                    Init( ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + Title + '.log' );
               end;
          end;
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ) );
     end;
end;

procedure TdmInterfase.ParametrosPrepara;
begin
     with FListaParametros do
     begin
          FEmpresa   := Values[ P_EMPRESA ];
          FUsuario   := Values[ P_USUARIO ];
          FClave     := Values[ P_CLAVE ];
          FRecFechaInicial    := Date + StrToIntDef( Values[ P_FECHA_INI ], 0 );
          FRecFechaFin        := Date + StrToIntDef( Values[ P_FECHA_FIN ], 0 );
     end;
end;


procedure TdmInterfase.Procesar;
var
   oParametros: TZetaParams;
   lLoop, lSalirTress: Boolean;
   sMensajeErrorHTTP: String;
begin
     LogInit;
     try
        ParametrosPrepara;
        if ( FRecFechaInicial <= FRecFechaFin ) then
        begin
             with FBitacora do
             begin
                  WriteTexto( Format( 'Empresa:             %s', [ FEmpresa ] ) );
                  WriteTexto( StringOfChar( '-', 40 ) );
             end;
             if dmCliente.BuscaCompany( FEmpresa ) then
             begin
                  oParametros := TZetaParams.Create;
                  try
                     if LoginBatch and dmCliente.ValidarConexionHTTP( lLoop, lSalirTress, sMensajeErrorHTTP, True ) then
                     begin
                          BitacoraAutorizarPrep;
                          dmCliente.CargaActivosTodos(oParametros);
                          with oParametros do
                          begin
                               AddDate( 'FechaInicial',FRecFechaInicial );
                               AddDate( 'FechaFinal', FRecFechaFin );
                          end;
                          if GetEmpleadosVacacion then
                             AgregaAutorizaciones;
                     end
                     else
                     begin
                          if not StrVacio( sMensajeErrorHTTP ) then
                             LogError ( sMensajeErrorHTTP );
                     end;
                  finally
                         oParametros.Free;
                  end;
             end
             else
             begin
                   LogError( K_MALA_CONFI + ' Empresa ' + FEmpresa + ' No Existe' );
             end;
        end
        else
            LogError( K_MALA_CONFI + 'La Fecha Final'  + FechaAsStr(FRecFechaFin) + ' es menor a la fecha inicial ' + FechaAsStr( FRecFechaInicial ) );
     finally
            LogEnd;
     end;
end;

procedure TdmInterfase.AgregaAutorizaciones;
var
   dInicioVaca, dFinVaca, dReferencia, dFin: TDate;
   i, iDias, iEmpleado: Integer;

   procedure RegistraAutoExtPrep;
   begin
        with cdsAutoExtPrep do
        begin
             Append;
             FieldByName('CB_CODIGO').AsInteger:= iEmpleado;
             FieldByName('HORAS' ).AsFloat := cdsHorario.FieldByName('HO_NUMERO').AsFloat;
             FieldByName('AU_FECHA').AsDateTime:= cdsTarjetas.FieldByName('AU_FECHA').AsDateTime;
             Post;
        end;
   end;

   function GetDias: Integer;
   begin
        //Si la fecha de inicio de vacaciones es mayor a la fecha de parametro toma esa fecha
        if ( dInicioVaca > FRecFechaInicial ) then
           dReferencia:= dInicioVaca
        else
            dReferencia:= FRecFechaInicial;

        // Si la fecha de termino de vacaciones es menor a fecha parametro toma esa fecha
        if ( dFinVaca < FRecFechaFin ) then
           dFin:=  dFinVaca
        else
            dFin:= FRecFechaFin;

        Result:= Trunc( dFin - dReferencia + 1 );
   end;

   procedure GetTarjetas;
   begin
        cdsTarjetas.Data:= ServerAsistencia.GetCalendarioEmpleado( dmCliente.Empresa, iEmpleado, dReferencia, dFin );
   end;

begin
     with cdsEmpleadosVaca do
     begin
          Log( 'Inicio del registro de tiempos de compensaci�n' );
          First;
          GetTiemposCompensacion;
          cdsAutoExtPrep.Conectar;
          while not EOF do
          begin
               iEmpleado:= FieldByName('CB_CODIGO').AsInteger;
               dInicioVaca:= FieldByName('VA_FEC_INI').AsDateTime;
               //En el dia final del registro de vacaciones el empleado ya esta activo y no se registra la autorizacion por esta v�a
               dFinVaca:= FieldByName('VA_FEC_FIN').AsDateTime - 1;
               iDias:= GetDias;
               //Trae las tarjetas del empleado en el rango de dReferencia a dFinal
               GetTarjetas;
               for i:= 0 to iDias - 1 do
               begin
                    { Se agrega la tarjeta si es dia h�bil y si en ese horario tiene registrado tiempo de compensaci�n }
                    if ( cdsTarjetas.FieldByName('AU_STATUS').AsInteger = Ord( auHabil ) )
                        and( cdsHorario.Locate('HO_CODIGO', cdsTarjetas.FieldByName( 'HO_CODIGO').AsString, [] )
                        and ( cdsHorario.FieldByName('HO_NUMERO').AsFloat > 0 ) ) then
                        RegistraAutoExtPrep;
                    cdsTarjetas.Next;
               end;
               Next;
          end;
          cdsAutoExtPrep.Enviar;
          Log( 'Fin del registro de tiempos de compensaci�n' );
     end;
end;


procedure TdmInterfase.cdsAutoExtPrepAlAdquirirDatos(Sender: TObject);
begin
     //Es Solo para traernos el metadata
     cdsAutoExtPrep.Data := ServerAsistencia.GetAutorizaciones( dmCliente.Empresa, NullDateTime, TRUE );
end;

procedure TdmInterfase.cdsAutoExtPrepNewRecord(DataSet: TDataSet);
begin
     with cdsAutoExtPrep do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'CH_SISTEMA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CH_GLOBAL' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CH_IGNORAR' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CH_H_REAL' ).AsString := K_HORA_NULA;
          FieldByName( 'CH_H_AJUS' ).AsString := K_HORA_NULA;
          //El tipo siempre es de tipo tipo prepagadas dentro de jornada y descripci�n igual a la de extras
          FieldByName('CH_DESCRIP').AsInteger:= Ord(dcExtras);
          FieldByName('CH_TIPO').AsInteger:= Ord(chPrepagFueraJornada);
          //Cada vez que se agregue un registro se inicializa el campo con Cero
          FieldByName( 'US_COD_OK' ).AsInteger := 0;
     end;
end;

procedure TdmInterfase.cdsAutoExtPrepAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   ErrorData: OleVariant;
begin
     ErrorCount := 0;
     with cdsAutoExtPrep do
     begin
          if State in [dsEdit, dsInsert] then
             PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile ( ServerAsistencia.GrabaAutorizaciones( dmCliente.Empresa, Delta, ErrorCount, ErrorData, Ord(ocSustituir) ) );
          end;
     end;
end;

function TdmInterfase.LoginBatch: Boolean;
var   eReply: eLogReply;
begin
     Result := FALSE;
     with dmCliente do
     begin
          eReply := GetUsuario( FUsuario, FClave, TRUE );
          case eReply of
               lrOK: Result := TRUE;
               lrLoggedIn: LogError( '�Usuario ya entr� al sistema!' );
               lrLockedOut: LogError( '� Usuario bloqueado !' );
               lrChangePassword: LogError( '� Debe cambiar clave de usuario !' );
               lrExpiredPassword: LogError( '� Clave de usuario vencida !' );
               lrInactiveBlock: LogError( '� Usuario bloqueado !' );
               lrNotFound: LogError( '� No se encontr� el usuario !' );
               lrAccessDenied: LogError( '� Password incorrecto de usuario !' );
               lrSystemBlock: LogError( '� Sistema Bloqueado !' );
          else
              LogError( '�Error general al verificar usuario!' );
          end;
     end;
end;

end.
