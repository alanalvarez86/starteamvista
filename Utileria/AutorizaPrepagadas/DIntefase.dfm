object dmInterfase: TdmInterfase
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 221
  Top = 246
  Height = 212
  Width = 331
  object cdsEmpleadosVaca: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 48
    Top = 8
  end
  object cdsHorario: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 148
    Top = 8
  end
  object cdsAutoExtPrep: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnNewRecord = cdsAutoExtPrepNewRecord
    AlAdquirirDatos = cdsAutoExtPrepAlAdquirirDatos
    AlEnviarDatos = cdsAutoExtPrepAlEnviarDatos
    Left = 232
    Top = 8
  end
  object cdsTarjetas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 48
    Top = 64
  end
end
