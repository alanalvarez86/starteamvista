inherited TressShell: TTressShell
  Left = 366
  Top = 267
  Width = 600
  Height = 439
  Caption = 'Exportaci'#243'n M'#250'ltiple de Registros Patronales'
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 374
    Width = 592
  end
  inherited PageControl: TPageControl
    Top = 145
    Width = 592
    Height = 195
    inherited TabBitacora: TTabSheet
      inherited MemBitacora: TMemo
        Width = 584
        Height = 126
        ScrollBars = ssVertical
      end
      inherited PanelBottom: TPanel
        Top = 126
        Width = 584
        inherited BitacoraSeek: TSpeedButton
          Left = 555
        end
        inherited ArchBitacora: TEdit
          Width = 488
        end
      end
    end
    inherited TabErrores: TTabSheet
      inherited Panel1: TPanel
        Top = 118
        Width = 584
        inherited ErroresSeek: TSpeedButton
          Left = 555
        end
        inherited ArchErrores: TEdit
          Width = 488
        end
      end
      inherited MemErrores: TMemo
        Width = 584
        Height = 118
        ScrollBars = ssVertical
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 340
    Width = 592
    Align = alBottom
    inherited BtnProcesar: TBitBtn
      Visible = False
    end
    inherited BtnDetener: TBitBtn
      Visible = False
    end
    inherited BtnSalir: TBitBtn
      Left = 450
    end
  end
  inherited PanelEncabezado: TPanel
    Width = 592
    Height = 145
    Visible = False
    inherited LblArchivo: TLabel
      Left = 12
    end
    object LblFecha: TLabel [3]
      Left = 15
      Top = 119
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = '&Fecha Default:'
      FocusControl = SistemaFechaZF
    end
    object LblFormatoFechas: TLabel [4]
      Left = 6
      Top = 94
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'Formato F&echas:'
      FocusControl = cbFFecha
    end
    object LblTipoImportacion: TLabel [5]
      Left = 7
      Top = 42
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = '&Tipo de Archivo:'
      FocusControl = rgTipoImp
    end
    object Label2: TLabel [6]
      Left = 14
      Top = 69
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = '&Formato ASCII:'
      Enabled = False
      FocusControl = cbFormato
    end
    object LblStep: TLabel [7]
      Left = 214
      Top = 119
      Width = 100
      Height = 13
      Caption = 'Brinco Periodo Vaca:'
    end
    object SistemaFechaZF: TZetaFecha
      Left = 88
      Top = 115
      Width = 115
      Height = 22
      Cursor = crArrow
      Hint = 'Fecha Default del Sistema'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Text = '13/Aug/99'
      Valor = 36385.000000000000000000
    end
    object cbFFecha: TZetaKeyCombo
      Left = 88
      Top = 90
      Width = 185
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      ListaFija = lfFormatoImpFecha
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object rgTipoImp: TRadioGroup
      Left = 88
      Top = 29
      Width = 185
      Height = 33
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Altas'
        'Kardex')
      TabOrder = 1
    end
    object cbFormato: TZetaKeyCombo
      Left = 88
      Top = 65
      Width = 185
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      Enabled = False
      ItemHeight = 13
      TabOrder = 2
      ListaFija = lfFormatoASCII
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object StepPeriodo: TZetaNumero
      Left = 318
      Top = 115
      Width = 28
      Height = 21
      Hint = 'Brinco de Periodo de Vacaciones'
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
    end
    object UpDownPer: TUpDown
      Left = 346
      Top = 115
      Width = 15
      Height = 21
      Associate = StepPeriodo
      Max = 5
      TabOrder = 6
      Thousands = False
    end
  end
  inherited LogoffTimer: TTimer
    Left = 296
    Top = 64
  end
  inherited ActionList: TActionList
    Left = 346
    Top = 68
  end
  inherited ShellMenu: TMainMenu
    inherited Archivo1: TMenuItem
      inherited CancelarProceso1: TMenuItem
        Visible = False
      end
      inherited CancelarProceso2: TMenuItem
        Visible = False
      end
    end
  end
  inherited OpenDialog: TOpenDialog
    Left = 366
    Top = 74
  end
end
