unit FWizIMSSExportarSuaMultiples;

{$define SUA_14}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs,  FileCtrl, Buttons, ExtCtrls, Mask, StdCtrls, ComCtrls,
     ZBaseWizardFiltro, ZetaNumero, ZetaKeyCombo, ZetaKeyLookup,
     ZetaEdit, ZetaWizard, CheckLst;

type
  TWizIMSSExportarSUAMultiples = class(TBaseWizardFiltro)
    EmpleadosGB: TGroupBox;
    ArchivoEmpleadosLBL: TLabel;
    BuscarArchivoEmpleado: TSpeedButton;
    lEmpleados: TCheckBox;
    ArchivoEmpleados: TEdit;
    lBorrarEmpleados: TCheckBox;
    lHomoclave: TCheckBox;
    MovimientosGB: TGroupBox;
    ArchivoMovimientosLBL: TLabel;
    BuscarArchivoMovimientos: TSpeedButton;
    lMovimientos: TCheckBox;
    ArchivoMovimientos: TEdit;
    OpenDialog: TOpenDialog;
    PeriodoGB: TGroupBox;
    Month: TZetaKeyCombo;
    Label6: TLabel;
    Label5: TLabel;
    Year: TZetaNumero;
    lEnviarFaltas: TCheckBox;
    PatronLbl: TLabel;
    lCuotaFija: TCheckBox;
    FormatoSUAGB: TGroupBox;
    Label2: TLabel;
    VersionSUA: TZetaKeyCombo;
    ZKBorrarMovimientos: TZetaKeyCombo;
    BorrarMovimientosLBL: TLabel;
    ArchivoDatosAfiliatoriosLBL: TLabel;
    ArchivoInfonavitLBL: TLabel;
    ArchivoInfonavit: TEdit;
    ArchivoDatosAfiliatorios: TEdit;
    BuscarArchivoDatosAfiliatorios: TSpeedButton;
    BuscarArchivoInfonavit: TSpeedButton;
    ArchivoIncapacidadLBL: TLabel;
    ArchivoIncapacidad: TEdit;
    BuscarArchivoIncapacidad: TSpeedButton;
    chklstPatrones: TCheckListBox;
    chkRespaldarArchivosExistentes: TCheckBox;
    gbConcentrados: TGroupBox;
    edtRutaDirectorioConcentrados: TEdit;
    Label1: TLabel;
    btnDirectorioConcentrados: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BuscarArchivoEmpleadoClick(Sender: TObject);
    procedure BuscarArchivoMovimientosClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure lEmpleadosClick(Sender: TObject);
    procedure BuscarArchivoDatosAfiliatoriosClick(Sender: TObject);
    procedure BuscarArchivoInfonavitClick(Sender: TObject);
    procedure BuscarArchivoIncapacidadClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnDirectorioConcentradosClick(Sender: TObject);
  private
    { Private declarations }
    FPatron: String;
    FRegistro: String;
    FPatronDesc: String;
    FCuantosPatrones : Integer;
    G_lstArchivosASEG : TStringList;
    G_lstArchivosAFIL : TStringList;
    G_lstArchivosINFO : TStringList;
    G_lstArchivosMOVTOS : TStringList;
    G_lstArchivosINCA : TStringList;
    procedure LlenaVersionesSUA;
    procedure CargaPatron;
    procedure SetControls;
    function ParametrosValidos : Boolean;
    function DirectorioValido(const strArchivo,strMensajeArchivo : string) : Boolean;
    function ReemplazarSimbolos( strOriginal : string ) : string;
    procedure RespaldarArchivo( strArchivo : string );
    function AgregaArchivoParametro(strArchivo,strNombreParametro,strMensajeArchivo: string): Boolean;
    function GetNombreRespaldo( strArchivoCompleto : string; iNumeroRespaldo : integer ) : string;
    function CrearSubDirectorios(const strDirectorio: string): Boolean;
    function AcumularArchivos(const lstArchivos : TStringList; const strRutaArchivoDestino : string ) : Boolean;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
    property CuantosPatrones : Integer read FCuantosPatrones write FCuantosPatrones;
    function GenerarConcentrados : Boolean;
  end;

var
  WizIMSSExportarSUAMultiples: TWizIMSSExportarSUAMultiples;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaClientTools,
     ZetaDialogo,
     ZGlobalTress,
     DCatalogos,
     DProcesos,
     DGlobal,
     FTressShell,
     ZetaSystemWorking,
     DCliente;

const
     K_SIMBOLO_RP = '#RP';
     K_SIMBOLO_RC = '#RC';
     K_SIMBOLO_MM = '#MM';
     K_SIMBOLO_AA = '#AA';

{$R *.DFM}

procedure TWizIMSSExportarSUAMultiples.FormCreate(Sender: TObject);
begin
     ArchivoEmpleados.Tag := K_GLOBAL_FILE_EMP_SUA;
     ArchivoMovimientos.Tag := K_GLOBAL_FILE_MOVS_SUA;
     ArchivoDatosAfiliatorios.Tag := K_GLOBAL_FILE_AFIL_SUA;
     ArchivoInfonavit.Tag := K_GLOBAL_FILE_INFO_SUA;
     ArchivoIncapacidad.Tag := K_GLOBAL_FILE_INCA_SUA;

{$ifdef SUA_14}
     lEnviarFaltas.Checked := TRUE;
     lCuotaFija.Visible := FALSE;
{$endif}

     inherited;
     ParametrosControl := Year;
     SetControls;
     HelpContext := H40444_Exportar_SUA;
     LlenaVersionesSUA;

     //Inicializar listas de concentrados
     G_lstArchivosASEG := TStringList.Create;
     G_lstArchivosAFIL := TStringList.Create;
     G_lstArchivosINFO := TStringList.Create;
     G_lstArchivosMOVTOS := TStringList.Create;
     G_lstArchivosINCA := TStringList.Create;

     FCuantosPatrones := 0;
end;

procedure TWizIMSSExportarSUAMultiples.FormShow(Sender: TObject);
begin
     inherited;
     with dmCatalogos.cdsRPatron do
     begin
          Conectar;
          //Limpiar Lista
          chklstPatrones.Items.Clear;
          //Recorrer dataset
          First;
          while not EOF do
          begin
               chklstPatrones.Items.Add( FieldByName('TB_CODIGO').AsString + '=' + FieldByName('TB_ELEMENT').AsString );
               chklstPatrones.Checked[ chklstPatrones.Items.Count - 1 ] := TRUE;
               Next;
          end;//while not EOF do

     end;//with dmCatalogos.cdsRPatron do

     with dmCliente do
     begin
          Year.Valor := IMSSYear;
          Month.Valor := IMSSMes;
     end;
     VersionSUA.ItemIndex := High( dProcesos.aVersionSUA );    // Sugiere la versi�n mas reciente

     //Sugerir Directorio de Concentrados
     edtRutaDirectorioConcentrados.Text := ExtractFilePath( Application.ExeName );
end;

procedure TWizIMSSExportarSUAMultiples.LlenaVersionesSUA;
var
   i: Integer;
begin
     with VersionSUA.Items do
     begin
          Clear;
          for i := Low( dProcesos.aVersionSUA ) to High( dProcesos.aVersionSUA ) do
          begin
	       Add( dProcesos.aVersionSUA[i] );
          end;
      end;
end;

procedure TWizIMSSExportarSUAMultiples.CargaPatron;
begin
     with dmCatalogos.cdsRPatron do
     begin
          FPatron := FieldByName( 'TB_CODIGO' ).AsString;
          FRegistro := FieldByName( 'TB_NUMREG' ).AsString;
          FPatronDesc := FieldByName( 'TB_ELEMENT' ).AsString;
     end;
end;

procedure TWizIMSSExportarSUAMultiples.SetControls;
var
   lEnabled: Boolean;
begin
     // Exportar Empleados
     lEnabled := lEmpleados.Checked;
     ArchivoEmpleadosLBL.Enabled := lEnabled;
     ArchivoEmpleados.Enabled := lEnabled;
     ArchivoDatosAfiliatoriosLBL.Enabled := lEnabled;
     ArchivoDatosAfiliatorios.Enabled := lEnabled;
     ArchivoInfonavitLBL.Enabled := lEnabled;
     ArchivoInfonavit.Enabled := lEnabled;
     BuscarArchivoEmpleado.Enabled := lEnabled;
     lBorrarEmpleados.Enabled := lEnabled;
     lBorrarEmpleados.Checked := lEnabled and Global.GetGlobalBooleano( K_GLOBAL_DELETE_FILE_SUA );
     lHomoclave.Enabled := lEnabled;
     lCuotaFija.Enabled := lEnabled;
     // Exportar Movimientos
     lEnabled := lMovimientos.Checked;
     ArchivoMovimientos.Enabled := lEnabled;
     ArchivoMovimientosLBL.Enabled := lEnabled;
     ArchivoIncapacidad.Enabled := lEnabled;
     ArchivoIncapacidadLBL.Enabled := lEnabled;
     BuscarArchivoMovimientos.Enabled := lEnabled;
     BorrarMovimientosLBL.Enabled := lEnabled;
     ZKBorrarMovimientos.Enabled := lEnabled;
     ZKBorrarMovimientos.ItemIndex := BoolToInt( lEnabled and Global.GetGlobalBooleano( K_GLOBAL_DELETE_FILE_SUA ) );
     lEnviarFaltas.Enabled := lEnabled;
end;

procedure TWizIMSSExportarSUAMultiples.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var i : integer;

function ValidaContenidoEdit( Control: TEdit; const sNombre: String ): Boolean;
var
   sContenido : String;
begin
     sContenido := Control.Text;
     Result := True;

     if ( ZetaCommonTools.StrVacio( sContenido ) ) then
     begin
          Result := False;
          ZetaDialogo.ZError( Caption, 'El Archivo De ' + sNombre + ' No Puede Quedar Vac�o', 0 );
          ActiveControl := Control;
     end;//if
end;

begin
     inherited;
     with Wizard do
     begin
          if CanMove and EsPaginaActual( Parametros ) and Adelante then
          begin
               CanMove := False;

               for i:=0 to chklstPatrones.Items.Count - 1 do
               begin
                    if chklstPatrones.Checked[ i ] then
                    begin
                         CanMove := True;
                         Break;
                    end;
               end;//for

               if not CanMove then
               begin
                    Error( 'Debe seleccionar por lo menos un registro patronal.', chklstPatrones );
               end
               else
               begin
                    if not lEmpleados.Checked and not lMovimientos.Checked then
                    begin
                         CanMove := Error( 'No Est� Seleccionado Para Enviar Nada', lEmpleados );
                    end
                    else
                    begin
                         if CanMove and ( not lBorrarEmpleados.Checked ) and
                            ( eBorrarMovimSUA( ZKBorrarMovimientos.ItemIndex ) = bsTodo ) then
                            CanMove := Error( 'Si no se borran empleados, no se pueden borrar todos los movimientos.' + CR_LF +
                                   'Quedar�a incompleto el historial de movimientos en SUA', ZKBorrarMovimientos );


                         if CanMove and lEmpleados.Checked then
                            CanMove := ValidaContenidoEdit( ArchivoEmpleados, 'Empleados' );
                         if CanMove and lEmpleados.Checked then
                            CanMove := ValidaContenidoEdit( ArchivoDatosAfiliatorios, 'Datos Afiliatorios' );
                         if CanMove and lEmpleados.Checked then
                            CanMove := ValidaContenidoEdit( ArchivoInfonavit, 'Infonavit' );
                         if CanMove and lMovimientos.Checked then
                            CanMove := ValidaContenidoEdit( ArchivoMovimientos, 'Movimientos' );
                         if CanMove and lMovimientos.Checked then
                            CanMove := ValidaContenidoEdit( ArchivoIncapacidad, 'Incapacidades' );
                    end;//else
               end;//else
          end;//if CanMove and EsPaginaActual( Parametros ) and Adelante then
     end;//with Wizard do
end;

procedure TWizIMSSExportarSUAMultiples.BuscarArchivoEmpleadoClick(Sender: TObject);
begin
     inherited;
     ArchivoEmpleados.Text := AbreDialogo( OpenDialog, ArchivoEmpleados.Text, 'sua' );
end;

procedure TWizIMSSExportarSUAMultiples.BuscarArchivoMovimientosClick(Sender: TObject);
begin
     inherited;
     ArchivoMovimientos.Text := AbreDialogo( OpenDialog, ArchivoMovimientos.Text, 'sua' );
end;

procedure TWizIMSSExportarSUAMultiples.lEmpleadosClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

function TWizIMSSExportarSUAMultiples.EjecutarWizard: Boolean;
var i : integer;
begin
     TressShell.EscribeBitacora( '********  PROCESO DE EXPORTACION INICIADO  ********');

     Result := False;
     ZetaSystemWorking.InitAnimation( 'Iniciando Exportaci�n...');

     try
        //Limpiar listas de concentrados
        G_lstArchivosASEG.Clear;
        G_lstArchivosAFIL.Clear;
        G_lstArchivosINFO.Clear;
        G_lstArchivosMOVTOS.Clear;
        G_lstArchivosINCA.Clear;

        //Contar Patrones Seleccionados
        for i := 0 to chklstPatrones.Items.Count - 1 do
        begin
             if chklstPatrones.Checked[ i ] then
             begin
                  inc( FCuantosPatrones );
             end;//if
        end;//for

        //Procesar Patrones
        for i := 0 to chklstPatrones.Items.Count - 1 do
        begin
             if chklstPatrones.Checked[ i ] then
             begin
                  if dmCatalogos.cdsRPatron.Locate( 'TB_CODIGO', chklstPatrones.Items.Names[ i ], [] ) then
                  begin
                       CargaPatron; //Carga FPatron, FRegistro y FPatronDesc

                       ZSystemWorking.WriteMensaje( 'Procesando Patr�n : ' + FPatron );
                       TressShell.EscribeBitacora( 'Procesando Patr�n : ' + FPatron + ' = ' + FPatronDesc );

                       //Cargar y Verificar Parametros Nuevos
                       if ParametrosValidos then
                       begin
                            Result := dmProcesos.ExportarSUA( ParameterList );
                       end//if ParametrosValidos then
                       else
                       begin
                            TressShell.EscribeError('Error en par�metros');
                            Close;
                       end;//else

                  end;//if dmCatalogos.cdsRPatron.Locate( 'TB_CODIGO', chklstPatrones.Items.Names[ i ], [] ) then

             end;//if chklstPatrones.Checked[ i ] then

        end;//for

        //Generar concentrados en caso de exito
        {if Result then
        begin
             ZSystemWorking.WriteMensaje( 'Procesando Concentrados...' );
             Result := GenerarConcentrados;
        end;//if Result then //}

     finally
            ZetaSystemWorking.EndAnimation;
     end;//try

     TressShell.EscribeBitacora( '********  PROCESO DE EXPORTACION FINALIZADO  ********');

     //Almacenar Bitacoras en Archivos
     TressShell.GenerarArchivosBitacora;

end;

procedure TWizIMSSExportarSUAMultiples.BuscarArchivoDatosAfiliatoriosClick(
  Sender: TObject);
begin
    inherited;
    ArchivoDatosAfiliatorios.Text := AbreDialogo( OpenDialog, ArchivoDatosAfiliatorios.Text, 'sua' );
end;

procedure TWizIMSSExportarSUAMultiples.BuscarArchivoInfonavitClick(Sender: TObject);
begin
     inherited;
     ArchivoInfonavit.Text := AbreDialogo( OpenDialog, ArchivoInfonavit.Text, 'sua' );
end;

procedure TWizIMSSExportarSUAMultiples.BuscarArchivoIncapacidadClick(Sender: TObject);
begin
     inherited;
     ArchivoIncapacidad.Text := AbreDialogo( OpenDialog, ArchivoIncapacidad.Text, 'sua' );
end;

function TWizIMSSExportarSUAMultiples.ParametrosValidos: Boolean;
begin
     Result := True;

     with ParameterList do
     begin
          if ZetaCommonTools.StrLleno( FPatron ) then
          begin
               AddString( 'Patron', FPatron );
          end
          else
          begin
               Result := False;
               TressShell.EscribeError('Error : El c�digo de registro patronal no puede estar vacio');
          end;

          if ZetaCommonTools.StrLleno( FRegistro ) then
          begin
               AddString( 'NumeroRegistro', FRegistro );
          end
          else
          begin
               Result := False;
               TressShell.EscribeError('Error : El n�mero de registro patronal no puede estar vacio');
          end;

          if Year.ValorEntero > 0 then
          begin
               AddInteger( 'Year', Year.ValorEntero );
          end
          else
          begin
               Result := False;
               TressShell.EscribeError('Error : El a�o especificado no es valido');
          end;

          if Month.Valor > 0 then
          begin
               AddInteger( 'Mes', Month.Valor );
          end
          else
          begin
               Result := False;
               TressShell.EscribeError('Error : El mes especificado no es valido');
          end;

          if not AgregaArchivoParametro( ArchivoEmpleados.Text , 'ArchivoEmpleados' , 'Empleados' ) then
          begin
               Result := False;
          end;

          if not AgregaArchivoParametro( ArchivoDatosAfiliatorios.Text , 'ArchivoDatosAfiliatorios' , 'Datos Afiliatorios' ) then
          begin
               Result := False;
          end;

          if not AgregaArchivoParametro( ArchivoInfonavit.Text , 'ArchivoInfonavit' , 'Infonavit' ) then
          begin
               Result := False;
          end;

          if not AgregaArchivoParametro( ArchivoMovimientos.Text , 'ArchivoMovimientos' , 'Movimientos' ) then
          begin
               Result := False;
          end;

          if not AgregaArchivoParametro( ArchivoIncapacidad.Text , 'ArchivoIncapacidad' , 'Incapacidad' ) then
          begin
               Result := False;
          end;

          if ZKBorrarMovimientos.ItemIndex >= 0 then
          begin
               AddInteger( 'BorrarMovimientos', ZKBorrarMovimientos.ItemIndex );
          end
          else
          begin
               Result := False;
               TressShell.EscribeError('Error : El Par�metro "Borrar en SUA" es Incorrecto');
          end;

          if VersionSUA.ItemIndex >= 0 then
          begin
               AddInteger( 'VersionSUA', VersionSUA.ItemIndex );
          end
          else
          begin
               Result := False;
               TressShell.EscribeError('Error : El Par�metro "Versi�n de SUA" es Incorrecto');
          end;

          AddBoolean( 'EnviarEmpleados', lEmpleados.Checked );
          AddBoolean( 'BorrarEmpleados', lBorrarEmpleados.Checked );
          AddBoolean( 'IncluirHomoclave', lHomoclave.Checked );
          AddBoolean( 'EnviarCuotaFija', lCuotaFija.Checked );
          AddBoolean( 'EnviarMovimientos', lMovimientos.Checked );
          AddBoolean( 'EnviarFaltaBaja', lEnviarFaltas.Checked );
          AddBoolean( 'ValidarCURPvsRFC', ( VersionSUA.ItemIndex = K_VERSION_SUA_2000 ) );
          AddBoolean( 'SuspenFinBim', FALSE );

     end;//with ParameterList do
end;

function TWizIMSSExportarSUAMultiples.AgregaArchivoParametro( strArchivo , strNombreParametro ,
strMensajeArchivo : string): Boolean;
begin
     Result := False;
     strArchivo := ReemplazarSimbolos( strArchivo );

     if DirectorioValido( strArchivo , strMensajeArchivo ) then
     begin
          //Respaldar en caso de ser necesario
          RespaldarArchivo( strArchivo );
          ParameterList.AddString( strNombreParametro , strArchivo );
          Result := True;

          //Agregar ruta de archivo a lista global de archivos
          if  pos(strNombreParametro , 'ArchivoEmpleados'+'ArchivoDatosAfiliatorios'+'ArchivoInfonavit'+
                                       'ArchivoMovimientos'+'ArchivoIncapacidad' ) > 0  then
          begin
               if  strNombreParametro = 'ArchivoEmpleados' then G_lstArchivosASEG.Add( strArchivo );
               if  strNombreParametro = 'ArchivoDatosAfiliatorios' then G_lstArchivosAFIL.Add( strArchivo );
               if  strNombreParametro = 'ArchivoInfonavit' then G_lstArchivosINFO.Add( strArchivo );
               if  strNombreParametro = 'ArchivoMovimientos' then G_lstArchivosMOVTOS.Add( strArchivo );
               if  strNombreParametro = 'ArchivoIncapacidad' then G_lstArchivosINCA.Add( strArchivo );
          end//if
          else
          begin
               TressShell.EscribeError('Error : El nombre de par�metro de archivo ' + strNombreParametro + ' es incorrecto');
               Result := False;
          end;//else

     end;//if DirectorioValido( strArchivo ) then
end;

function TWizIMSSExportarSUAMultiples.DirectorioValido(
 const strArchivo , strMensajeArchivo : string): Boolean;
var strRutaDirectorio : string;
begin
     strRutaDirectorio := ExtractFilePath( strArchivo );

     //Verificar si se puede crear directorio
     Result := CrearSubDirectorios( strRutaDirectorio );
     if not Result then
     begin
          TressShell.EscribeError( 'Error : No se puede crear el directorio "' + strRutaDirectorio +
                                   '" para el archivo de "' + strMensajeArchivo + '"' );
     end;//if
end;

function TWizIMSSExportarSUAMultiples.CrearSubDirectorios(
 const strDirectorio: string): Boolean;
var strDirActual,strTemp : string;
begin
     Result := True;
     strTemp := strDirectorio;
     strDirActual := copy( strTemp, 1, pos( '\' , strTemp ) );
     delete( strTemp, 1, pos( '\' , strTemp ) );

     while pos( '\' , strTemp ) > 0 do
     begin
          strDirActual := strDirActual + copy( strTemp, 1, pos( '\' , strTemp ) );
          delete( strTemp, 1, pos( '\' , strTemp ) );

          if not DirectoryExists( strDirActual ) then
          begin
               if not CreateDir( strDirActual ) then
               begin
                    Result := False;
                    Break;
               end;//if
          end;//if
     end;//while
end;

function TWizIMSSExportarSUAMultiples.ReemplazarSimbolos(
  strOriginal: string ): string;
var strResultado : string;
begin
     strOriginal := AnsiUpperCase( strOriginal );

     //Reemplazar simbolos comunes
     strResultado := ZetaCommonTools.StrTransALL( strOriginal, K_SIMBOLO_RP, FPatronDesc );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, K_SIMBOLO_RC, FPatron );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, K_SIMBOLO_MM,
                                                  ZetaCommonTools.StrZero( IntToStr(Month.Valor), 2 ) );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, K_SIMBOLO_AA, Year.ValorAsText );

     //Quitar simbolos invalidos = \/:*�?<>|"
     //strResultado := ZetaCommonTools.StrTransALL( strResultado, '\', '_' );
     //strResultado := ZetaCommonTools.StrTransALL( strResultado, ':', '_' );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, '/', '_' );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, '*', '_' );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, '�', '_' );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, '?', '_' );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, '<', '_' );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, '>', '_' );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, '|', '_' );
     strResultado := ZetaCommonTools.StrTransALL( strResultado, '"', '_' );

     Result := strResultado;
end;

procedure TWizIMSSExportarSUAMultiples.RespaldarArchivo(strArchivo: string);
var strNombreRespaldo : string;
    iNumeroRespaldo : integer;
begin

     if FileExists( strArchivo ) then
     begin
          if chkRespaldarArchivosExistentes.Checked then
          begin
               iNumeroRespaldo := 1;
               strNombreRespaldo := GetNombreRespaldo( strArchivo , iNumeroRespaldo );

               while FileExists( strNombreRespaldo ) do
               begin
                    inc( iNumeroRespaldo );
                    strNombreRespaldo := GetNombreRespaldo( strArchivo , iNumeroRespaldo );
               end;//while

               //Renombrar el archivo
               if not RenameFile( strArchivo , strNombreRespaldo ) then
               begin
                    TressShell.EscribeError('Error al renombrar el archivo "' + strArchivo + '"');
               end;

               TressShell.EscribeBitacora( 'El archivo "' + strArchivo + '" fue respaldado como "' + strNombreRespaldo + '"');
          end
          else
          begin
               TressShell.EscribeBitacora( 'El archivo "' + strArchivo + '" fue reemplazado por el nuevo del mismo nombre');
          end;
     end;//if FileExist( strArchivo ) then

end;

function TWizIMSSExportarSUAMultiples.GetNombreRespaldo(strArchivoCompleto : string;
  iNumeroRespaldo: integer): string;
begin
     Result := ExtractFilePath( strArchivoCompleto ) +
               'BACKUP_' + IntToStr(iNumeroRespaldo) +
               '_' + ExtractFileName( strArchivoCompleto );
end;

function TWizIMSSExportarSUAMultiples.GenerarConcentrados: Boolean;
var strRutaDirectorioConcentrados : string;
begin
     Result := True;

     //Obtener Directorio
     strRutaDirectorioConcentrados := ReemplazarSimbolos( edtRutaDirectorioConcentrados.Text );

     if CrearSubDirectorios( strRutaDirectorioConcentrados ) then
     begin

          //Concentrado de Archivos ASEG
          Result := AcumularArchivos( G_lstArchivosASEG , strRutaDirectorioConcentrados + 'Concentrado_ASEG.txt' );
          if Result then
          begin
               //Concentrado de Archivos AFIL
               Result := AcumularArchivos( G_lstArchivosAFIL , strRutaDirectorioConcentrados + 'Concentrado_AFIL.txt' );
               if Result then
               begin
                    //Concentrado de Archivos INFO
                    Result := AcumularArchivos( G_lstArchivosINFO , strRutaDirectorioConcentrados + 'Concentrado_INFO.txt' );
                    if Result then
                    begin
                         //Concentrado de Archivos MOVTOS
                         Result := AcumularArchivos( G_lstArchivosMOVTOS , strRutaDirectorioConcentrados + 'Concentrado_MOVTOS.txt' );
                         if Result then
                         begin
                              //Concentrado de Archivos INCA
                              Result := AcumularArchivos( G_lstArchivosINCA , strRutaDirectorioConcentrados + 'Concentrado_INCA.txt' );
                         end;//if
                    end;//if
               end;//if
          end;//if

     end;//if CrearSubDirectorios( strRutaDirectorioConcentrados ) then
end;

function TWizIMSSExportarSUAMultiples.AcumularArchivos(
  const lstArchivos: TStringList; const strRutaArchivoDestino: string) : Boolean;
var lstConcentrados, lstArchivoActual : TStringList;
    i : integer;
begin
     Result := True;

     lstConcentrados := TStringList.Create;
     lstArchivoActual := TStringList.Create;
     lstConcentrados.Clear;
     lstArchivoActual.Clear;

     try
        try
           for i := 0 to lstArchivos.Count - 1 do
           begin
                if FileExists( lstArchivos.Strings[ i ] ) then
                begin
                     lstArchivoActual.LoadFromFile( lstArchivos.Strings[ i ] );
                     lstConcentrados.AddStrings( lstArchivoActual );
                end//if
                else
                begin
                     TressShell.EscribeError('Error : No existe el archivo ' + lstArchivos.Strings[ i ] );
                     Result := False;
                end;//else
           end;//for

           //Respaldar en caso de ser necesario
           RespaldarArchivo( strRutaArchivoDestino );

           //Almacenar en archivo
           lstConcentrados.SaveToFile( strRutaArchivoDestino );

        except
              TressShell.EscribeError('Error al generar el concentrado ' + strRutaArchivoDestino );
              Result := False;
        end;//try
     finally
            lstConcentrados.Free;
            lstArchivoActual.Free;
     end;//try
end;


procedure TWizIMSSExportarSUAMultiples.FormDestroy(Sender: TObject);
begin
     inherited;

     //Liberar listas de concentrados
     G_lstArchivosASEG.Free;
     G_lstArchivosAFIL.Free;
     G_lstArchivosINFO.Free;
     G_lstArchivosMOVTOS.Free;
     G_lstArchivosINCA.Free;
end;

procedure TWizIMSSExportarSUAMultiples.btnDirectorioConcentradosClick(
  Sender: TObject);
var strDir : string;
begin
     strDir := ExtractFilePath( Application.ExeName );
     if SelectDirectory( strDir , [sdAllowCreate, sdPerformCreate, sdPrompt], 0 ) then
     begin
          edtRutaDirectorioConcentrados.Text := strDir + '\';
     end;
end;

end.



