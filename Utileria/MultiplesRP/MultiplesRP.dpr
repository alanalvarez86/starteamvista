program MultiplesRP;

uses
  Forms,
  ComObj,
  ActiveX,
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseImportaShell in '..\..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  ZWizardBasico in '..\..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZBaseWizard in '..\..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  DCatalogos in '..\..\DataModules\dCatalogos.pas' {dmCatalogos: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

{procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     //MuestraSplash;
     Application.Title := 'Exportaci�n M�ltiple de Registros Patronales';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if Login( False ) then
          begin
               //CierraSplash;
               Show;
               Update;
               BeforeRun;
               MostrarWizard;
               Application.Run;
          end
          else
          begin
               //CierraSplash;
               Free;
          end;
     end;
end.
