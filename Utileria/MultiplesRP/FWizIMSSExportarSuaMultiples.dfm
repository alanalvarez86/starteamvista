inherited WizIMSSExportarSUAMultiples: TWizIMSSExportarSUAMultiples
  Left = 443
  Top = 264
  Caption = 'Exportaci'#243'n M'#250'ltiple a SUA'
  ClientHeight = 633
  ClientWidth = 460
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 597
    Width = 460
    BeforeMove = WizardBeforeMove
    inherited Salir: TZetaWizardButton
      Left = 364
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 281
    end
  end
  inherited PageControl: TPageControl
    Width = 460
    Height = 597
    inherited Parametros: TTabSheet
      object EmpleadosGB: TGroupBox
        Left = 0
        Top = 225
        Width = 452
        Height = 145
        Align = alTop
        Caption = ' Empleados '
        TabOrder = 1
        object ArchivoEmpleadosLBL: TLabel
          Left = 121
          Top = 32
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Arc&hivo:'
          FocusControl = ArchivoEmpleados
        end
        object BuscarArchivoEmpleado: TSpeedButton
          Left = 414
          Top = 26
          Width = 25
          Height = 25
          Hint = 'Buscar Archivo de Empleados'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BuscarArchivoEmpleadoClick
        end
        object ArchivoDatosAfiliatoriosLBL: TLabel
          Left = 80
          Top = 57
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Datos afi&liatorios:'
          FocusControl = ArchivoDatosAfiliatorios
        end
        object ArchivoInfonavitLBL: TLabel
          Left = 69
          Top = 83
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Datos INFON&AVIT:'
          FocusControl = ArchivoInfonavit
        end
        object BuscarArchivoDatosAfiliatorios: TSpeedButton
          Left = 414
          Top = 52
          Width = 25
          Height = 25
          Hint = 'Buscar Archivo de Datos afiliatorios'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BuscarArchivoDatosAfiliatoriosClick
        end
        object BuscarArchivoInfonavit: TSpeedButton
          Left = 414
          Top = 78
          Width = 25
          Height = 25
          Hint = 'Buscar Archivo de INFONAVIT'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BuscarArchivoInfonavitClick
        end
        object lEmpleados: TCheckBox
          Left = 126
          Top = 10
          Width = 51
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Enviar:'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = lEmpleadosClick
        end
        object ArchivoEmpleados: TEdit
          Left = 163
          Top = 28
          Width = 247
          Height = 21
          TabOrder = 1
        end
        object lBorrarEmpleados: TCheckBox
          Left = 86
          Top = 102
          Width = 90
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Borrar en SUA:'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
        object lHomoclave: TCheckBox
          Left = 70
          Top = 122
          Width = 106
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Incl&uir Homoclave:'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
        object lCuotaFija: TCheckBox
          Left = 221
          Top = 122
          Width = 131
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Enviar Como Cuota &Fija:'
          TabOrder = 6
        end
        object ArchivoInfonavit: TEdit
          Left = 163
          Top = 79
          Width = 247
          Height = 21
          TabOrder = 3
        end
        object ArchivoDatosAfiliatorios: TEdit
          Left = 163
          Top = 53
          Width = 247
          Height = 21
          TabOrder = 2
        end
      end
      object MovimientosGB: TGroupBox
        Left = 0
        Top = 370
        Width = 452
        Height = 124
        Align = alTop
        Caption = ' Movimientos '
        TabOrder = 2
        object ArchivoMovimientosLBL: TLabel
          Left = 121
          Top = 30
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Arch&ivo:'
          FocusControl = ArchivoMovimientos
        end
        object BuscarArchivoMovimientos: TSpeedButton
          Left = 414
          Top = 24
          Width = 25
          Height = 25
          Hint = 'Buscar Archivo de Movimientos'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BuscarArchivoMovimientosClick
        end
        object BorrarMovimientosLBL: TLabel
          Left = 89
          Top = 81
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = 'Bo&rrar en SUA:'
          FocusControl = ZKBorrarMovimientos
        end
        object ArchivoIncapacidadLBL: TLabel
          Left = 87
          Top = 56
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'Incapaci&dades:'
          FocusControl = ArchivoIncapacidad
        end
        object BuscarArchivoIncapacidad: TSpeedButton
          Left = 414
          Top = 50
          Width = 25
          Height = 25
          Hint = 'Buscar Archivo de datos de Incapacidad'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BuscarArchivoIncapacidadClick
        end
        object lMovimientos: TCheckBox
          Left = 125
          Top = 9
          Width = 51
          Height = 17
          Alignment = taLeftJustify
          Caption = 'E&nviar:'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = lEmpleadosClick
        end
        object ArchivoMovimientos: TEdit
          Left = 163
          Top = 26
          Width = 247
          Height = 21
          TabOrder = 1
        end
        object lEnviarFaltas: TCheckBox
          Left = 3
          Top = 99
          Width = 173
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Movimien&tos con Fecha de Baja:'
          TabOrder = 4
        end
        object ZKBorrarMovimientos: TZetaKeyCombo
          Left = 163
          Top = 77
          Width = 125
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          DropDownCount = 3
          ItemHeight = 13
          TabOrder = 3
          ListaFija = lfBorrarMovimSUA
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
        object ArchivoIncapacidad: TEdit
          Left = 163
          Top = 52
          Width = 247
          Height = 21
          TabOrder = 2
        end
      end
      object PeriodoGB: TGroupBox
        Left = 0
        Top = 0
        Width = 452
        Height = 225
        Align = alTop
        Caption = ' Per'#237'odo '
        TabOrder = 0
        object Label6: TLabel
          Left = 77
          Top = 36
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'&o:'
          FocusControl = Year
        end
        object Label5: TLabel
          Left = 76
          Top = 14
          Width = 23
          Height = 13
          Alignment = taRightJustify
          Caption = '&Mes:'
          FocusControl = Month
        end
        object PatronLbl: TLabel
          Left = 15
          Top = 58
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Re&gistro Patronal:'
        end
        object Month: TZetaKeyCombo
          Left = 106
          Top = 10
          Width = 158
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          ListaFija = lfMeses
          ListaVariable = lvPuesto
          Offset = 1
          Opcional = False
        end
        object Year: TZetaNumero
          Left = 106
          Top = 32
          Width = 71
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
        object chklstPatrones: TCheckListBox
          Left = 104
          Top = 58
          Width = 339
          Height = 135
          ItemHeight = 13
          TabOrder = 2
        end
        object chkRespaldarArchivosExistentes: TCheckBox
          Left = 104
          Top = 200
          Width = 223
          Height = 17
          Caption = 'Crear respaldo de archivos que ya existen'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 3
        end
      end
      object FormatoSUAGB: TGroupBox
        Left = 0
        Top = 494
        Width = 452
        Height = 43
        Align = alTop
        Caption = ' Aplicaci'#243'n de SUA '
        TabOrder = 3
        object Label2: TLabel
          Left = 121
          Top = 14
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = '&Versi'#243'n:'
          FocusControl = VersionSUA
        end
        object VersionSUA: TZetaKeyCombo
          Left = 162
          Top = 10
          Width = 158
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
      end
      object gbConcentrados: TGroupBox
        Left = 0
        Top = 537
        Width = 452
        Height = 50
        Align = alClient
        Caption = ' Concentrados '
        TabOrder = 4
        object Label1: TLabel
          Left = 27
          Top = 22
          Width = 132
          Height = 13
          Alignment = taRightJustify
          Caption = 'Directorio de Concentrados:'
          FocusControl = VersionSUA
        end
        object btnDirectorioConcentrados: TSpeedButton
          Left = 414
          Top = 16
          Width = 25
          Height = 25
          Hint = 'Seleccionar Directorio para Concentrados'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnDirectorioConcentradosClick
        end
        object edtRutaDirectorioConcentrados: TEdit
          Left = 163
          Top = 18
          Width = 247
          Height = 21
          ReadOnly = True
          TabOrder = 0
          Text = 'C:\'
        end
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      inherited FiltrosGB: TGroupBox
        Left = 20
        Top = 128
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Top = 212
        Width = 452
        Height = 113
        Align = alNone
        Lines.Strings = (
          ''
          'Se generar'#225'n los archivos con informaci'#243'n del pago'
          'de IMSS correspondiente al mes del Registro Patronal activo.'
          ''
          'Presione el bot'#243'n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Top = 520
        Width = 452
        inherited ProgressBar: TProgressBar
          Left = 44
        end
      end
    end
  end
  object OpenDialog: TOpenDialog
    Filter = 'Archivos SUA (*.sua)|*.sua|Todos (*.*)|*.*'
    Left = 424
    Top = 296
  end
end
