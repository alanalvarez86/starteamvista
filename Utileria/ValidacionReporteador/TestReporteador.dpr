program TestReporteador;

uses
  MidasLib,
  Vcl.Forms,
  ZetaClientTools,
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  DSistema in 'DSistema.pas';

//dBaseTressDiccionario in '..\..\DataModules\dBaseTressDiccionario.pas';

{$R *.res}
//{$R WindowsXP.res}
begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Validacion de Componentes';
  //Application.MainFormOnTaskbar := True; //Que hace?
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
    if Login( False ) then
    begin
      Show;
      Update;
      BeforeRun;
      Application.Run;
    end
    else
      free;
  end;
end.
