inherited dmSistema: TdmSistema
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Height = 700
  Width = 708
  inherited cdsEmpresas: TZetaLookupDataSet
    AlCrearCampos = cdsEmpresasAlCrearCampos
  end
  inherited cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  inherited cdsUsuarios: TZetaLookupDataSet
    AlBorrar = cdsUsuariosAlBorrar
    LookupActivoField = 'US_ACTIVO'
  end
  inherited cdsEmpresasAccesos: TZetaLookupDataSet
    Left = 376
  end
  inherited cdsDerechos: TServerDataSet
    Left = 32
    Top = 256
  end
  inherited cdsClasifiRepEmp: TZetaLookupDataSet
    AlAdquirirDatos = cdsClasifiRepEmpAlAdquirirDatos
  end
  inherited cdsRoles: TZetaLookupDataSet
    BeforePost = cdsRolesBeforePost
    AfterCancel = cdsRolesAfterCancel
    AfterDelete = cdsRolesAfterDelete
    AfterScroll = cdsRolesAfterScroll
    AlAdquirirDatos = cdsRolesAlAdquirirDatos
    AlEnviarDatos = cdsRolesAlEnviarDatos
    AlModificar = cdsRolesAlModificar
    OnGetRights = cdsRolesGetRights
    Left = 416
  end
  inherited cdsUserRoles: TZetaLookupDataSet
    AlAdquirirDatos = cdsUserRolesAlAdquirirDatos
    AlEnviarDatos = cdsUserRolesAlEnviarDatos
    Left = 280
    Top = 128
  end
  inherited cdsGruposTodos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  object cdsPoll: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsPollBeforePost
    AfterDelete = cdsPollAfterDelete
    OnNewRecord = cdsPollNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsPollAlAdquirirDatos
    AlEnviarDatos = cdsPollAlEnviarDatos
    AlCrearCampos = cdsPollAlCrearCampos
    AlModificar = cdsPollAlModificar
    Left = 352
    Top = 136
  end
  object cdsUserSuper: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsUserSuperAlEnviarDatos
    Left = 24
    Top = 128
  end
  object cdsArbol: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsArbolAlEnviarDatos
    Left = 200
    Top = 128
  end
  object cdsNivel0: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsNivel0AfterDelete
    AlAdquirirDatos = cdsNivel0AlAdquirirDatos
    AlEnviarDatos = cdsNivel0AlEnviarDatos
    AlModificar = cdsNivel0AlModificar
    UsaCache = True
    LookupName = 'Confidencialidad'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 288
    Top = 184
  end
  object cdsEmpresasPortal: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpresasPortalAlAdquirirDatos
    LookupName = 'Empresas'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    Left = 112
    Top = 312
  end
  object cdsEmpleadosPortal: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Empleados'
    LookupDescriptionField = 'PRETTYNAME'
    LookupKeyField = 'CB_CODIGO'
    OnLookupKey = cdsEmpleadosPortalLookupKey
    OnLookupSearch = cdsEmpleadosPortalLookupSearch
    Left = 216
    Top = 312
  end
  object cdsEmpresasSeleccion: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsEmpresasSeleccionAlAdquirirDatos
    LookupName = 'Empresas'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    Left = 496
    Top = 64
  end
  object cdsGruposAdic: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsGruposAdicAlAdquirirDatos
    AlEnviarDatos = cdsGruposAdicAlEnviarDatos
    LookupName = 'Agrupaciones Adicionales'
    LookupDescriptionField = 'GX_TITULO'
    LookupKeyField = 'GX_CODIGO'
    Left = 112
    Top = 256
  end
  object cdsCamposAdic: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    Left = 200
    Top = 256
  end
  object cdsBitacora: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsBitacoraAfterOpen
    AlCrearCampos = cdsBitacoraAlCrearCampos
    AlModificar = cdsBitacoraAlModificar
    Left = 216
    Top = 192
  end
  object cdsEntidadesAccesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsEntidadesAccesosAlEnviarDatos
    Left = 352
    Top = 192
  end
  object cdsClasificacionesAccesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsClasificacionesAccesosAlEnviarDatos
    Left = 352
    Top = 240
  end
  object cdsRolUsuarios: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    Left = 24
    Top = 320
  end
  object cdsAdicionalesAccesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsAdicionalesAccesosAlEnviarDatos
    Left = 352
    Top = 288
  end
  object cdsSistLstDispositivos: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'DI_NOMBRE'
    Params = <>
    BeforePost = cdsSistLstDispositivosBeforePost
    AfterDelete = cdsSistLstDispositivosAfterDelete
    OnNewRecord = cdsSistLstDispositivosNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsSistLstDispositivosAlAdquirirDatos
    AlEnviarDatos = cdsSistLstDispositivosAlEnviarDatos
    AlCrearCampos = cdsSistLstDispositivosAlCrearCampos
    AlModificar = cdsSistLstDispositivosAlModificar
    Left = 40
    Top = 384
  end
  object cdsLstDispDisponibles: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 152
    Top = 384
  end
  object cdsLstDispAsignados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsLstDispAsignadosReconcileError
    Left = 264
    Top = 384
  end
  object cdsDisxCom: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 360
    Top = 384
  end
  object cdsACCArbol: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsACCArbolAlAdquirirDatos
    LookupName = 'Derechos de Acceso'
    LookupDescriptionField = 'FILTRO_DES'
    LookupKeyField = 'AX_NUMERO'
    Left = 496
    Top = 128
  end
  object cdsCarruseles: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'KW_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCarruselesAlAdquirirDatos
    AlEnviarDatos = cdsCarruselesAlEnviarDatos
    UsaCache = True
    LookupName = 'Carruseles'
    LookupDescriptionField = 'KW_NOMBRE'
    LookupKeyField = 'KW_CODIGO'
    Left = 490
    Top = 191
  end
  object cdsCarruselesInfo: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'KI_ORDEN'
    Params = <>
    BeforeInsert = cdsCarruselesInfoBeforeInsert
    BeforePost = cdsCarruselesInfoBeforePost
    AlAdquirirDatos = cdsCarruselesInfoAlAdquirirDatos
    Left = 490
    Top = 240
  end
  object cdsBitacoraBio: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlCrearCampos = cdsBitacoraBioAlCrearCampos
    Left = 464
    Top = 392
  end
  object cdsTermPorGrupo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterDelete = cdsTermPorGrupoAfterDelete
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsTermPorGrupoAlAdquirirDatos
    AlEnviarDatos = cdsTermPorGrupoAlEnviarDatos
    AlCrearCampos = cdsTermPorGrupoAlCrearCampos
    Left = 544
    Top = 352
  end
  object cdsListaGrupos: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsListaGruposBeforePost
    BeforeDelete = cdsListaGruposBeforeDelete
    AfterDelete = cdsListaGruposAfterDelete
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsListaGruposAlAdquirirDatos
    AlEnviarDatos = cdsListaGruposAlEnviarDatos
    AlModificar = cdsListaGruposAlModificar
    LookupName = 'Lista de Grupos'
    LookupDescriptionField = 'GP_DESCRIP'
    LookupKeyField = 'GP_CODIGO'
    Left = 456
    Top = 328
  end
  object cdsMensajes: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsMensajesBeforePost
    AfterDelete = cdsMensajesAfterDelete
    OnNewRecord = cdsMensajesNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsMensajesAlAdquirirDatos
    AlEnviarDatos = cdsMensajesAlEnviarDatos
    AlModificar = cdsMensajesAlModificar
    LookupName = 'Mensajes'
    LookupDescriptionField = 'DM_MENSAJE'
    LookupKeyField = 'DM_CODIGO'
    Left = 248
    Top = 448
  end
  object cdsTerminales: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AfterDelete = cdsTerminalesAfterDelete
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsTerminalesAlAdquirirDatos
    AlEnviarDatos = cdsTerminalesAlEnviarDatos
    AlCrearCampos = cdsTerminalesAlCrearCampos
    AlAgregar = cdsTerminalesAlAgregar
    AlModificar = cdsTerminalesAlModificar
    LookupName = 'Terminales'
    LookupDescriptionField = 'TE_NOMBRE'
    LookupKeyField = 'TE_CODIGO'
    Left = 320
    Top = 448
  end
  object cdsMensajesPorTerminal: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsMensajesPorTerminalBeforePost
    AfterDelete = cdsMensajesPorTerminalAfterDelete
    OnNewRecord = cdsMensajesPorTerminalNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsMensajesPorTerminalAlAdquirirDatos
    AlEnviarDatos = cdsMensajesPorTerminalAlEnviarDatos
    AlCrearCampos = cdsMensajesPorTerminalAlCrearCampos
    AlModificar = cdsMensajesPorTerminalAlModificar
    Left = 424
    Top = 448
  end
  object cdsHuellaGti: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsHuellaGtiAlAdquirirDatos
    Left = 40
    Top = 448
  end
  object cdsTablasBD: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 208
    Top = 552
  end
  object cdsSistActualizarBDs: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'DB_CODIGO'
    Params = <>
    OnNewRecord = cdsSistActualizarBDsNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsSistActualizarBDsAlAdquirirDatos
    AlCrearCampos = cdsSistActualizarBDsAlCrearCampos
    LookupName = 'Bases de Datos'
    LookupDescriptionField = 'DB_DESCRIP'
    LookupKeyField = 'DB_CODIGO'
    Left = 416
    Top = 504
  end
  object cdsCamposTabla: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 552
  end
  object cdsCamposLlaveTabla: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 416
    Top = 552
  end
end
