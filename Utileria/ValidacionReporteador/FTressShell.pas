{***DevEx(@am): TressShell piloto para validacion de componentes en XE5***}

unit FTressShell;


interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseShell, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  Vcl.ExtCtrls, dxStatusBar, dxRibbonStatusBar, Vcl.ComCtrls, cxClasses,
  dxSkinsForm, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxContainer, cxEdit, cxGroupBox,
  dxSkinsdxNavBarPainter, cxTreeView, dxNavBarCollns, dxNavBarBase, dxNavBar,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, Vcl.ImgList,
  dxBar, dxRibbon, dxGDIPlusClasses,
  ZetaTipoEntidad, cxImage, ZetaStateComboBox,ZetaCommonLists,
  {$IFDEF TRESS_DELPHIXE5_UP} HtmlHelpViewer, cxStyles {$ELSE} FHelpManager {$ENDIF};

type
  TTressShell = class(TBaseShell)
    PanelConsulta: TPanel;
    ClientAreaPG_DevEx: TcxPageControl;
    cxTabCatAulas: TcxTabSheet;
    DevEx_NavBar: TdxNavBar;
    DevEx_NavBarGroup1: TdxNavBarGroup;
    DevEx_NavBarGroup1Control: TdxNavBarGroupControl;
    arbolito1: TcxTreeView;
    DevEx_BarManager: TdxBarManager;
    AEmpresas: TdxBar;
    btnAbrirCatalogo: TdxBarLargeButton;
    NavBarLarge: TcxImageList;
    NavBarSmall: TcxImageList;
    btnFEmpFoto: TdxBarButton;
    btnExploradorReportes: TdxBarButton;
    btnPrueba3: TdxBarButton;
    btnMenuPrueba: TdxBarSubItem;
    btnMenu1: TdxBarButton;
    btnMenu2: TdxBarButton;
    btnMenu3: TdxBarButton;
    cxTabEmpFoto: TcxTabSheet;
    DevEx_NavBarGroup2: TdxNavBarGroup;
    DevEx_NavBarGroup3: TdxNavBarGroup;
    DevEx_NavBarGroup4: TdxNavBarGroup;
    DevEx_NavBarGroup5: TdxNavBarGroup;
    DevEx_NavBarGroup6: TdxNavBarGroup;
    DevEx_NavBarGroup7: TdxNavBarGroup;
    DevEx_NavBarGroup8: TdxNavBarGroup;
    DevEx_NavBarGroup2Control: TdxNavBarGroupControl;
    DevEx_NavBarGroup3Control: TdxNavBarGroupControl;
    DevEx_NavBarGroup4Control: TdxNavBarGroupControl;
    DevEx_NavBarGroup5Control: TdxNavBarGroupControl;
    DevEx_NavBarGroup6Control: TdxNavBarGroupControl;
    DevEx_NavBarGroup7Control: TdxNavBarGroupControl;
    DevEx_NavBarGroup8Control: TdxNavBarGroupControl;
    cxTabReportes: TcxTabSheet;
    PanelValorAEmpleado: TPanel;
    btnEmpAnterior_DevEx: TcxButton;
    btnEmplPrimero_DevEx: TcxButton;
    btnEmpSiguiente_DevEx: TcxButton;
    btnEmpUltimo_DevEx: TcxButton;
    EmpleadoLBL_DevEx: TLabel;
    EmpleadoPrettyName_DevEx: TLabel;
    ImagenStatus_DevEx: TcxImage;
    EmpleadoNumeroCB: TStateComboBox;
    Image24_StatusEmpleado: TcxImageList;
    btnBuscarEmp_DevEx: TcxButton;
    AEditar: TdxBar;
    Agregar: TdxBarLargeButton;
    Borrar: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    Modificar: TdxBarLargeButton;
    Refrescar: TdxBarLargeButton;
    TabArchivo: TdxRibbonTab;
    TabEditarFoto: TdxRibbonTab;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAbrirCatalogoClick(Sender: TObject);
    procedure btnFEmpFotoClick(Sender: TObject);
    procedure btnExploradorReportesClick(Sender: TObject);
    procedure btnPrueba3Click(Sender: TObject);
    procedure btnMenu1Click(Sender: TObject);
    procedure btnMenu2Click(Sender: TObject);
    procedure btnMenu3Click(Sender: TObject);
    procedure btnEmplPrimero_DevExClick(Sender: TObject);
    procedure btnEmpAnterior_DevExClick(Sender: TObject);
    procedure btnEmpSiguiente_DevExClick(Sender: TObject);
    procedure btnEmpUltimo_DevExClick(Sender: TObject);
    procedure EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure btnBuscarEmp_DevExClick(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
  private
    { Private declarations }
    procedure CambioEmpleadoActivo;
    procedure RefrescaEmpleadoActivos;
    procedure CambioValoresActivos(const Valor: TipoEstado);
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); virtual;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); virtual;
    {$endif}
  protected
    procedure DoOpenAll; override;
  public
    { Public declarations }
    {$ifdef MULTIPLES_ENTIDADES}
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    {$else}
    procedure SetDataChange(const Entidades: ListaEntidades);
    {$endif}
    procedure RefrescaEmpleado;
    procedure CargaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure CambiaEmpleadoActivos;
    procedure ReconectaFormaActiva; //(@am):Originalmente en zbasenavbarshell
  end;

var
  TressShell: TTressShell;
  //DevEx(by am): Arreglo para guardar los status del Empleado
  StatusEmpleado: array [0..3] of string = ('Activo', 'Reingreso','Baja','Antes ingreso');
implementation

uses
  ZetaCommonClasses,
  ZAccesosTress,
  ZetaDialogo,
  ZetaBuscaEmpleado_DevEx,
  {***DataModules***}
  DGlobal,
  DCliente,
  DReportes,
  DCatalogos,
  DTablas,
  DDiccionario,
  DSistema,
  DRecursos,

  {***Formas de Consulta***}
  FCatAulas_DevEx,
  FReportes_DevEx,
  FEmpFoto_DevEx; //Validacion Imagenes

{$R *.dfm}

procedure TTressShell.btnAbrirCatalogoClick(Sender: TObject);
begin
  inherited;
  ClientAreaPG_DevEx.ActivePage := cxTabCatAulas;
  if CatAulas_DevEx = nil then
  begin
    CatAulas_DevEx := TCatAulas_DevEx.Create(Self);
    CatAulas_DevEx.Parent := cxTabCatAulas;
    cxTabCatAulas.Caption:= CatAulas_DevEx.Caption;
    CatAulas_DevEx.DoConnect;
    CatAulas_DevEx.IndexDerechos := D_CAT_CAPA_AULAS;
  end;
  CatAulas_DevEx.Show;
end;

procedure TTressShell.btnMenu1Click(Sender: TObject);
begin
  inherited;
    ZetaDialogo.ZInformation( 'Mensaje','Click Prueba', 0 );
end;

procedure TTressShell.btnMenu2Click(Sender: TObject);
begin
  inherited;
  ZetaDialogo.ZInformation( 'Mensaje','Click Prueba', 0 );
end;

procedure TTressShell.btnMenu3Click(Sender: TObject);
begin
  inherited;
  ZetaDialogo.ZInformation( 'Mensaje','Click Prueba', 0 );
end;

procedure TTressShell.btnFEmpFotoClick(Sender: TObject);
begin
     inherited;
     ClientAreaPG_DevEx.ActivePage := cxTabEmpFoto;
     if EmpFoto_DevEx = nil then
     begin
          EmpFoto_DevEx := TEmpFoto_DevEx.Create(Self);
          EmpFoto_DevEx.Parent := cxTabEmpFoto;
          cxTabEmpFoto.Caption:= EmpFoto_DevEx.Caption;
          EmpFoto_DevEx.DoConnect;
          EmpFoto_DevEx.IndexDerechos := D_EMP_CURR_FOTO;
     end;
     EmpFoto_DevEx.Show;
     //ZetaDialogo.ZInformation( 'Mensaje','Click Prueba', 0 );
end;

procedure TTressShell.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TTressShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     ReconectaFormaActiva;
end;

procedure TTressShell.EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        lOk := dmCliente.SetEmpleadoNumero( EmpleadoNumeroCB.ValorEntero );
        if lOk then
            CambioEmpleadoActivo
        else
            ZetaDialogo.zInformation( 'Error', '! Empleado no encontrado !', 0 );
     finally
        Screen.Cursor := oCursor;
     end;
end;


procedure TTressShell.CambioValoresActivos(const Valor: TipoEstado);
begin
     NotifyDataChange( [], Valor );
end;

//DevEx (by am): Se modifica este metodo para desplegar de distinta forma el nombre y status del empleado segun la vista en la que se encuentre.
procedure TTressShell.RefrescaEmpleadoActivos;
var
   oColor : TColor;
   i:Integer;
   sCaption, sHint : String;

   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
     with dmCliente do
     begin
          with GetDatosEmpleadoActivo do
          begin
               EmpleadoPrettyName_DevEx.Caption := Nombre;
               GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);

               //DevEx (by am): Inicializando Bitmat y PNG
               ABitmap := TcxAlphaBitmap.CreateSize(24, 24);
               APngImage := nil;
               for i := Low(StatusEmpleado) to High(StatusEmpleado) do
               begin
                  if (sCaption = StatusEmpleado[i]) then
                  begin
                      try
                          //DevEx (by am): Creando el Bitmap
                          ABitmap.Clear;
                          Image24_StatusEmpleado.GetBitmap(i, ABitmap);
                          //DevEx (by am): Creando el PNG
                          APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                          ImagenStatus_DevEx.Picture.Graphic := APngImage;
                          ImagenStatus_DevEx.Hint := sHint;
                      finally
                          APngImage.Free;
                          ABitmap.Free;
                      end;
                      Break;
                  end;
               end;

          end;
     end;
end;

procedure TTressShell.btnBuscarEmp_DevExClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
  inherited;
   if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
              EmpleadoNumeroCB.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;

procedure TTressShell.btnEmpAnterior_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoAnterior then
             begin
                  EmpleadoNumeroCB.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.btnEmplPrimero_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoPrimero then
             begin
                EmpleadoNumeroCB.ValorEntero := Empleado;
                CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.btnEmpSiguiente_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoSiguiente then
             begin
                  EmpleadoNumeroCB.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.btnEmpUltimo_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoUltimo then
             begin
                  EmpleadoNumeroCB.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.btnExploradorReportesClick(Sender: TObject);
begin
  inherited;
  ClientAreaPG_DevEx.ActivePage := cxTabReportes;
  if Reportes_DevEx = nil then
  begin
    Reportes_DevEx := TReportes_DevEx.Create(Self);
    Reportes_DevEx.Parent := cxTabReportes;
    cxTabReportes.Caption:= Reportes_DevEx.Caption;
    Reportes_DevEx.DoConnect;
    Reportes_DevEx.IndexDerechos := D_CONS_REPORTES;
  end;
  Reportes_DevEx.Show;
    //ZetaDialogo.ZInformation( 'Mensaje','Click Prueba', 0 );
end;

procedure TTressShell.btnPrueba3Click(Sender: TObject);
begin
  inherited;
    ZetaDialogo.ZInformation( 'Mensaje','Click Prueba', 0 );
end;

procedure TTressShell.FormCreate(Sender: TObject);
begin
  Global := TdmGlobal.Create;
  dmCliente := TdmCliente.Create( Self );
  dmSistema := TdmSistema.Create( Self );
  dmCatalogos := TdmCatalogos.Create( Self );
  dmTablas := TdmTablas.Create( Self );
  dmRecursos := TdmRecursos.Create( Self );
  dmReportes := TdmReportes.Create( Self );
  dmDiccionario := TdmDiccionario.Create( Self );
  inherited;

  //DevEx (by am): Necesitarmos indicarlo porque al selecc. el metodo para llenar cdsReportes valida el ModoInterfaz
  dmCliente.ModoTress_DevEx:=TRUE;
  DevEx_ShellRibbon.ShowTabHeaders := TRUE;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
  inherited;
  {***Destruccion de formas utilizadas***}
  CatAulas_DevEx.Free;
  Reportes_DevEx.Free;
  {***Destruccion de Datamodules***}
  dmDiccionario.Free;
  dmReportes.Free;
  dmRecursos.Free;
  dmTablas.Free;
  dmCatalogos.Free;
  dmSistema.Free;
  dmCliente.Free;
  Global.Free;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        dmSistema.LeeAdicionales;   // Carga Campos Adicionales - Se ocupa para cuando se conecta Global

        Global.Conectar;
        with dmCliente do
        begin

             InitActivosSistema;
             InitArrayTPeriodo;//acl
             InitActivosAsistencia;
             dmCatalogos.InitActivosNavegacion;
             InitActivosEmpleado;
             InitActivosPeriodo;
             InitActivosIMSS;
        end;
        CargaEmpleadoActivos;
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
end;

{***METODOS AGREGADOS PARA PODER COMPILAR CON DATAMODULES GENERALES***}
{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
{$else}
procedure TTressShell.SetDataChange(const Entidades: ListaEntidades);
{$endif}
begin
  //(@am): NO NECESARIO PARA VALIDACION DE COMPONENTES
end;

procedure TTressShell.RefrescaEmpleado;
begin
  //(@am): NO NECESARIO PARA VALIDACION DE COMPONENTES
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
  with dmCliente do
  begin
        EmpleadoNumeroCB.ValorEntero := Empleado
  end;
  RefrescaEmpleadoActivos;
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
  //(@am): NO NECESARIO PARA VALIDACION DE COMPONENTES
end;

procedure TTressShell.CambiaSistemaActivos;
begin
  //(@am): NO NECESARIO PARA VALIDACION DE COMPONENTES
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
  //(@am): NO NECESARIO PARA VALIDACION DE COMPONENTES
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     if Assigned( EmpFoto_DevEx ) then
        EmpFoto_DevEx.DoRefresh;
end;

procedure TTressShell.AgregarClick(Sender: TObject);
begin
     if Assigned( EmpFoto_DevEx ) then
        EmpFoto_DevEx.DoInsert;
end;

procedure TTressShell.BorrarClick(Sender: TObject);
begin
     if Assigned( EmpFoto_DevEx ) then
        EmpFoto_DevEx.DoDelete;
end;

procedure TTressShell.ModificarClick(Sender: TObject);
begin
     if Assigned( EmpFoto_DevEx ) then
        EmpFoto_DevEx.DoEdit;
end;

procedure TTressShell.RefrescarClick(Sender: TObject);
begin
     if Assigned( EmpFoto_DevEx ) then
        EmpFoto_DevEx.DoRefresh;
end;

end.
