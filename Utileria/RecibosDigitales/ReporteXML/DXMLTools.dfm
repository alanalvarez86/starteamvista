object dmXMLTools: TdmXMLTools
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 155
  Top = 136
  Height = 150
  Width = 215
  object XMLDocument: TXMLDocument
    Options = [doNodeAutoCreate, doAutoPrefix, doNamespaceDecl]
    XML.Strings = (
      '')
    Left = 32
    Top = 16
    DOMVendorDesc = 'MSXML'
  end
  object XSLPageProducer: TXSLPageProducer
    XMLData = XMLDocument
    Left = 136
    Top = 16
    DOMVendorDesc = 'MSXML'
  end
end
