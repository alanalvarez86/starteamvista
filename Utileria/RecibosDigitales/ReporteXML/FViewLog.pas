unit FViewLog;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ShellAPI;

type
  TViewLog = class(TForm)
    PanelInferior: TPanel;
    Salir: TBitBtn;
    Bitacora: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FViewing: Boolean;
    FHTMLOnly: Boolean;
    procedure ShowMessage( const sText: String );
  public
    { Public declarations }
    property HTMLOnly: Boolean read FHTMLOnly write FHTMLOnly;
    procedure Execute;
  end;

var
  ViewLog: TViewLog;

implementation

uses FEmailWorkingFile,
     ZetaDialogo,
     DReportes;

{$R *.DFM}

function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;

{ ********* TViewLog ********** }

procedure TViewLog.FormCreate(Sender: TObject);
begin
     FViewing := False;
     FHTMLOnly := False;
end;

procedure TViewLog.FormActivate(Sender: TObject);
begin
     //Execute;
end;

procedure TViewLog.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     CanClose := not FViewing;
end;

procedure TViewLog.Execute;
var
   oCursor: TCursor;

begin

     Salir.Enabled := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     FViewing := True;
     try
        InitAnimation( 'Enviando Correos' );

        with dmReportes do
        begin
             OnLogCallBack := ShowMessage;
             ExecuteReportsViaEMail( FHTMLOnly );
        end;

     finally
            EndAnimation;
            FViewing := False;
            Screen.Cursor := oCursor;
            Salir.Enabled := not FViewing;
            dmReportes.OnLogCallBack := NIL;

     end;
     if FHTMLOnly and ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Ver El Archivo HTML Generado ?', 0, mbYes ) then
     begin
          with dmReportes do
          begin
               ExecuteFile( ArchivoHTML, '', ExtractFilePath( ArchivoHTML ) );
          end;
     end;
end;

procedure TViewLog.ShowMessage( const sText: String );
begin
     with Bitacora do
     begin
          with Lines do
          begin
               BeginUpdate;
               try
                  Add( sText );
               finally
                      EndUpdate;
               end;
               //ItemIndex := ( Count - 1 );
          end;
     end;
     Application.ProcessMessages;
end;

procedure TViewLog.FormShow(Sender: TObject);
begin
     Execute;
end;

procedure TViewLog.FormDestroy(Sender: TObject);
begin
     //
end;

end.
