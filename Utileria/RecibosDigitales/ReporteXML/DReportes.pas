unit DReportes;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet, FileCtrl, Types, 
  {$ifndef VER130}
  Variants,
  {$endif}
  DReportesGenerador,
  DTotaliza,
  {$IFDEF TRESS_DELPHIXE5_UP}
  DEmailService,
  {$ELSE}
  DEmailServiceD7,
  {$ENDIF}
  ZetaCommonLists,
  {$ifdef DOS_CAPAS}
       DServerSistema,
       DZetaServerProvider,
  {$else}
       Sistema_TLB,
  {$endif}
  ZReportTools,
  DXMLTools, xmldom, Xmlxform,
  TressMovilService, ZetaTipoEntidad;

type
  TdmReportes = class(TdmReportGenerator)
    cdsUsuariosLookup: TZetaLookupDataSet;
    cdsSuscripReportes: TZetaLookupDataSet;
    cdsEscogeReporte: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsSuscripReportesLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsUsuariosLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripReportesAlCrearCampos(Sender: TObject);
    procedure cdsUsuariosLookupGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
  private
    { Private declarations }
    FResultado: TZetaClientDataSet;
    FArchivoHTML: String;
    //FHTMLDefault: String;
    FEmpresa: String;
    FBorraArchivo: Boolean;
    FFrecuencia: integer;
    FUsuarios: String;
    FReportes: String;

    FHost: String;
    FUserID: String;
    FFromAddress: String;
    FFromName: String;
    FErrorMail: String;
    FSoloArchivos : String;
    FURLImagenes: String;
    FDIRImagenes: string;

    FURLServicio: String;
    FUsuarioServicio: String;
    FPassServicio: String;
    FLoginServicio: String;
    FLoginPassServicio: String;
    FAuthServicio: integer;
    FIdentificador : integer;
    FArchivoDestino : String;
    FTestingXML : Boolean;
    FUsarEncriptadoServicio : Boolean;

    FTipoReporte: eTipoReporte;
    FEntidadReporte: TipoEntidad;



    //FExportaPDF : Boolean;
    FUsuariosPlain: TStrings;
    FUsuariosHTML: TStrings;
    FErroresMail: TStrings;
    FListaReportes: TStrings;
    FClasifActivo: eClasifiReporte;
    FClasificaciones: TStrings;
    dmEmailService : TdmEmailService;

    {$ifdef DOS_CAPAS}
    FServerSistema: TdmServerSistema;
    {$else}
    FServerSistema: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    {$endif}

    function ConectaGlobal: Boolean;
    function ConectaHost: Boolean;
    function GetSuscripciones: Boolean;
    procedure AddColumn(Fuente: TField);
    procedure AddData(Fuente: TField );
    procedure AppParamsPrepare;
    procedure BorraArchivos;
    procedure ModificaListaCampos(Lista: TStrings);
    procedure ModificaListas;
    function GetSobreTotales: Boolean;
    procedure ValidaUsuarios(const sUsuarios: string);
    procedure EscribeMensaje(const sMensaje: string);
    procedure MueveTemporales;
    function GetClasificaciones(oClasificaciones: TStrings): string;
    procedure ObtieneEntidadGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    //procedure GetReportesLookup(const eClasifi: eClasifiReporte; oClasificaciones: TStrings);
    function SistemaBloqueado: Boolean;
  protected
    function GeneraReporte( const lHTMLOnly: Boolean ): Boolean;
    function GeneraUnReporte(const iReporte: Integer; const lHTMLOnly : Boolean ): Boolean;
    function GetExtensionDef : string;override;
    function GetSoloTotales: Boolean;
    function GeneraArchivoAscii: Boolean;
    function GeneraDataSetParaXML : Boolean;
    function GeneraArchivoXML: Boolean;
    function GetDirectorioImagenes: string;

    procedure DoOnGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoAfterGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoOnGetReportes( const lResultado: Boolean );override;
    procedure DoOnGetDatosImpresion;override;
    procedure ProcesaReportesViaEmail( const lHTMLOnly: Boolean );
    procedure GeneraOutPut;
    procedure GeneraQRReporte;
    procedure GeneraMailError( const Error : string );
    procedure SendEmail(const Tipo: eEmailType; const lMsgNormal : Boolean; oUsuarios: TStrings);
    procedure SendEmailError;

    procedure SendXML( FXMLTools : TdmXMLTools);

    procedure LogError(const sTexto: String;const lEnviar: Boolean = FALSE);override;
    procedure ModificaImagenHtml;
    procedure ModificaArchivoImagen(const sArchivo: string);
    procedure AgregaAttachments( oLista : TStrings );
    procedure AgregaEmailUsuario( const iVacio: Integer );
    procedure EliminaReporte( const iReporte : integer );
    procedure GetNombresArchivos;

    function PreparaPlantilla( var sError : WideString ) : Boolean;override;
    procedure DesPreparaPlantilla;override;

    {$ifdef DOS_CAPAS}
    property ServerSistema: TdmServerSistema read FServerSistema;
    {$else}
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    {$endif}
  public
    { Public declarations }
    property Empresa: String read FEmpresa write FEmpresa;
    property Frecuencia: Integer read FFrecuencia write FFrecuencia;
    property ArchivoHTML: String read FArchivoHTML;
    property DIRImagenes: string read FDIRImagenes write FDIRImagenes;

    property URLServicio: string read FURLServicio write FURLServicio;
    property UsuarioServicio: string read FUsuarioServicio write FUsuarioServicio;
    property PassServicio: string read FPassServicio write FPassServicio;
    property LoginServicio: string read FLoginServicio write FLoginServicio;
    property LoginPassServicio: string read FLoginPassServicio write FLoginPassServicio;
    property AuthServicio: integer read FAuthServicio write FAuthServicio;
    property Identificador: integer read FIdentificador write FIdentificador;
    property TestingXML : boolean read FTestingXML write FTestingXML;
    property ArchivoDestino : string read FArchivoDestino;
    property UsarEncriptadoServicio: boolean read FUsarEncriptadoServicio write FUsarEncriptadoServicio;
    property TipoReporte: eTipoReporte read FTipoReporte write FTipoReporte;
    property EntidadReporte: TipoEntidad read FEntidadReporte write FEntidadReporte;

    property ClasifActivo: eClasifiReporte read FClasifActivo write FClasifActivo;
    property Clasificaciones: TStrings read FClasificaciones write FClasificaciones;
    property Resultado: TZetaClientDataSet read FResultado write FResultado;
    function ExecuteReportsViaEMail( const lHTMLOnly: Boolean ): Boolean;
    procedure AppParamClear;
    procedure AppParamAdd( const sValue: String );
    procedure SetReportesXMLLogFileName(const sValue: String);
    procedure ImprimeUnaForma(const sFiltro: string);      overload;
        procedure ImprimeUnaForma( const sFiltro : string; const iReporte: integer; const eSalida : TipoPantalla = tgPreview );overload;
  end;

var
  dmReportes: TdmReportes;

implementation
uses
    DCliente,
    DGlobal,
    DDiccionario,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaDialogo,
    ZGlobalTress,
    ZQrReporteListado,
    ZFuncsCliente,
    ZReportConst,
    ZReporteAscii,
    ZetaTipoEntidadTools,
    FLookupReporte,
    FAutoClasses,
    FEmailWorkingFile,
    ZetaServerTools;

{$R *.DFM}

const
     K_CUADROVACIO = '&nbsp';
     K_MENSAJE_ERROR_HTML = '<HTML> '+
                            '<FONT face=Arial color=#800080 size=4> '+
                            '<HEAD> '+
                            {'<P><IMG height=60 alt="Logo de la Empresa" '+
                            'src= %s  width=100 align=right border=0> <!--Logotipo-->'+
                            '</P> '+}
                            '<STRONG> '+
                            '        <H1> %s <!--Titulo del Reporte--> </H1> '+
                            '        <P> Fecha: %s <!--Fecha del Reporte--></P> '+
                            '</STRONG></HEAD></FONT> '+
                            '<BODY> '+
                            '<DIV align=center> '+
                            '<FONT color=#FF0000 size=5>Notificaci�n Del M�dulo De Reportes V�a Email. '+
                            '<P>El Reporte </P> '+
                            '<P><STRONG>  %s <!--Nombre del Reporte--> </STRONG></P> '+
                            '<P>%s <!--Error Reportado--></P> '+
                            '</FONT></DIV></BODY></HTML>';
     K_MENSAJE_ERROR_PLAINTEXT = 'Notificaci�n Del M�dulo De Reportes V�a Email. ' +
                                 'El Reporte ' +
                                 '%s ' +
                                 '%s ';



procedure TdmReportes.DataModuleCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FServerSistema := TdmServerSistema.Create( self );
     {$endif}
     dmEmailService := TdmEmailService.Create( self );
     dmDiccionario := TdmDiccionario.Create( self );

     FUsuariosPlain := TStringList.Create;
     FUsuariosHTML := TStringList.Create;
     FErroresMail := TStringList.Create;
     FListaReportes := TStringList.Create;
     inherited;
end;

procedure TdmReportes.DataModuleDestroy(Sender: TObject);
begin
    {$ifdef DOS_CAPAS}
     FreeAndNil( FServerSistema );
    {$endif}
     FreeAndNil( FormaLookupReporte );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmEmailService );
     FreeAndNil( FListaReportes );
     FreeAndNil( FErroresMail );
     FreeAndNil( FUsuariosHTML );
     FreeAndNil( FUsuariosPlain );
     inherited;

end;

{$ifndef DOS_CAPAS}
function TdmReportES.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;
{$endif}


procedure TdmReportes.AppParamsPrepare;
const
     aTipo: array[ eTipoPeriodo ] of ZetaPChar = ( 'DI','S','C','Q','M','D','SA','SB','QA','QB','CA','CB');
     K_DIARIO = 'D';
     K_SEMANAL = 'S';
     K_MENSUAL = 'M';
     K_CADA_HORA = 'C';
     K_ESPECIAL = 'E';
var
   sFrecuencia, sTipoNomina: String;
   iTipoNomina: integer;
   i: eTipoPeriodo;
begin
     with FAppParams do
     begin
          FEmpresa := Values[ 'EMPRESA' ];
          sFrecuencia := Values[ 'FREC' ];
          if ( sFrecuencia = K_DIARIO ) then
             FFrecuencia := 0
          else if ( sFrecuencia = K_SEMANAL ) then
               FFrecuencia := 1
          else if ( sFrecuencia = K_MENSUAL ) then
               FFrecuencia := 2
          else if ( sFrecuencia = K_CADA_HORA ) then
               FFrecuencia := 3
          else if ( sFrecuencia = K_ESPECIAL ) then
               FFrecuencia := 4
          else FFrecuencia := -1;

          {CV: Se requiere que el a�o default se mueva?}
          if ZetaCommonTools.StrLleno( Values[ 'FECHA' ] ) then
             dmCliente.FechaDefault := StrToDate( QuitaComillas( Values[ 'FECHA' ] ) );

          FUsuarios := Values[ 'USUARIOS' ];
          ValidaUsuarios(FUsuarios);

          FReportes := Values[ 'REPORTE' ];

          FIdentificador := StrToIntDef( Values[ 'ID' ], 0);;

          if StrLleno( FReportes ) then
             FListaReportes.CommaText := FReportes;

          FSoloArchivos := Values[ 'SOLOARCHIVOS' ];

          FArchivoDestino := Values[ 'ARCHIVO' ];

          sTipoNomina := UpperCase( Values['TIPO']);
          iTipoNomina := -1;

          if StrLLeno( sTipoNomina ) then
          begin
               for i:= Low( aTipo ) to High( aTipo ) do
                   if ( sTipoNomina = aTipo[i] ) then
                   begin
                        iTipoNomina := Ord( i );
                        Break;
                   end;
          end;


          with Params do
          begin
               AddInteger( 'Year', StrToIntDef( Values['YEAR'], -1 ) );
               AddInteger( 'Tipo', iTipoNomina );
          end;


          if ( FFrecuencia = -1 ) and ZetaCommonTools.StrVacio( FUsuarios ) and ZetaCommonTools.StrVacio( FReportes ) then
             raise Exception.Create( 'No Se Ejecut� El Proceso Debido A Que No Se Especific� Frecuencia, Lista De Usuarios Ni Lista De Reportes ' );
     end;
end;

procedure TdmReportes.ValidaUsuarios( const sUsuarios : string );
 var oLista : TStrings;
     i : integer;
begin
     oLista := TStringList.Create;
     try
        oLista.CommaText := sUsuarios;
        for i:=0 to oLista.Count - 1 do
        begin
             if (StrToIntDef( oLista[i], -1 ) = -1) then
                raise Exception.Create( Format('"%s" No es un N�mero de Usuario V�lido',[ oLista[i] ]) );
        end;
     finally
            FreeAndNil(oLista);
     end;
end;

procedure TdmReportes.AppParamClear;
begin
     FAppParams.Clear;
end;

procedure TdmReportes.AppParamAdd( const sValue: String );
begin
     FAppParams.Add( UpperCase( sValue ) );
end;

function TdmReportes.ExecuteReportsViaEMail( const lHTMLOnly: Boolean ): Boolean;
begin
     Result := False;
     if ( FAppParams.Count > 0 ) then
     begin
          ProcesaReportesViaEmail( lHTMLOnly );
          Result := True;
     end
     else
     begin
          LogError( 'No Se Especific� Ning�n Par�metro' );
          Log( 'TRESSREPORTEXML.EXE Nombre_Empresa' );
          Log( 'Par�metros:' );
          Log( 'Nombre_Empresa: C�digo de Empresa( CM_CODIGO )' );
     end;
end;

procedure TdmReportes.ProcesaReportesViaEmail( const lHTMLOnly: Boolean );
begin
          try
                  AppParamsPrepare;
                 GeneraReporte( lHTMLOnly )
          except
                on Error:Exception do
                begin
                     LogError( 'Error Al Ejecutar Reportes XML: ' + Error.Message, TRUE );
                end;
          end;
          Log( 'Final: '+ FormatDateTime('dd/mmm/yyyy hh:mm:ss', Now ) );
          Log( '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*' );
end;

procedure TdmReportes.LogError(const sTexto: String;const lEnviar: Boolean );
begin
     inherited LogError( sTexto, lEnviar );
     if lEnviar then
     begin
          FErroresMail.Add( '~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*' );
          FErroresMail.Add( 'ERROR en el Reporte '+
                            ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, FFrecuencia ) +
                            ' - ' +
                            NombreReporte );
          FErroresMail.Add( sTexto );
     end;
end;

procedure TdmReportes.AgregaEmailUsuario( const iVacio: Integer );
var
   sUsuario: String;
begin
     with cdsUsuarios do
     begin
          if Locate( 'US_CODIGO', VarArrayOF( [ cdsSuscripcion.FieldByName( 'US_CODIGO' ).AsInteger ] ), [] ) then
          begin
               sUsuario := FieldByName( 'US_EMAIL' ).AsString;
               if ( eEmailFormato( FieldByName( 'US_FORMATO' ).AsInteger ) = efHTML ) then
                  FUsuariosHTML.AddObject( sUsuario, TObject( iVacio ) )
               else
                   FUsuariosPlain.AddObject( sUsuario, TObject( iVacio ) );
          end;
     end;
end;

procedure TdmReportes.GetNombresArchivos;
begin
     FArchivoHTML := ZReportTools.DirPlantilla;
     {FHTMLDefault := Global.GetGlobalString( K_GLOBAL_EMAIL_PLANTILLA );
     if ZetaCommonTools.StrLleno( FHTMLDefault ) and ZetaCommonTools.StrVacio( ExtractFileDir( FHTMLDefault ) ) then
       FHTMLDefault := FArchivoHTML + FHTMLDefault;}
     FArchivoHTML := FArchivoHTML + 'Def' + FormatDateTime( 'hhmmss', Time ) + '.htm';
end;


procedure TdmReportes.EliminaReporte( const iReporte : integer );
 var sReporte : string;
     i : integer;
begin
     if FBorraArchivo then
        DeleteFile( FArchivoHTML );

     sReporte := IntToStr( iReporte );

     for i:= 0 to FListaReportes.Count -1 do
     begin
          if sReporte = FListaReportes[ i ] then
          begin
               FListaReportes.Delete( i );
               Break;
          end;
     end;
end;


function TdmReportes.GeneraReporte( const lHTMLOnly: Boolean ): Boolean;
 var i, iReporte, iOldReporte : integer;
     oCursor : TCursor;
     lEsSupervisor: Boolean;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Result := ConectaGlobal;
        if Result then
        begin
             //GetNombresArchivos;
             Result := GetSuscripciones;

             if Result then
             begin
                  iOldReporte := -1;
                  lEsSupervisor := FALSE;
                  with cdsSuscripcion do
                  begin
                       while not Eof do
                       begin
                            iReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
                            FUsuariosHTML.Clear;
                            FUsuariosPlain.Clear;
                            AgregaEmailUsuario( FieldByName( 'SU_VACIO' ).AsInteger );

                            if ( ( FUsuariosHTML.Count + FUsuariosPlain.Count ) > 0 ) then
                            begin
                                 if ( iReporte <> iOldReporte ) or lEsSupervisor then
                                 begin
                                      FArchivoHTML := VACIO;

                                      if ( iOldReporte >=0 ) then
                                         EliminaReporte( iReporte );
                                      Result := GeneraUnReporte( iReporte, lHTMLOnly );

                                      {$ifdef TRESS}
                                      if cdsReporte.Active then
                                         lEsSupervisor := eClasifiReporte( cdsReporte.FieldByName( 'RE_CLASIFI' ).AsInteger ) = crSupervisor;
                                      {$else}
                                      lEsSupervisor := FALSE;
                                      {$endif}
                                      iOldReporte := iReporte;
                                 end;

                                 {
                                 if NOT Result then
                                 begin
                                      ActualizaListaUsuarios( FUsuariosHTML );
                                      ActualizaListaUsuarios( FUsuariosPlain );
                                 end;


                                 if ( DatosImpresion.Tipo in [tfHTML,tfHTM] ) AND UnSoloHTML then
                                    SendEmail( emtHtml, Result, FUsuariosHTML )
                                 else
                                     SendEmail( emtTexto, Result, FUsuariosHTML );
                                 SendEmail( emtTexto, Result, FUsuariosPlain );   }

                            end;

                            Next;

                       end;
                  end;
             end;


             for i:= 0 to FListaReportes.Count - 1 do
             begin
                  FArchivoHTML := '';
                  iReporte := StrToIntDef( FListaReportes[ i ], 0 );
                  if iReporte > 0 then
                  begin
                       if GeneraUnReporte( iReporte, lHTMLOnly ) then
                           Log( Format( 'El Reporte #%d Ha sido Generado y Enviado',[iReporte]));
                  end;
             end;

             if NOT Result AND ( FListaReportes.Count = 0 ) then
                SendEmailError;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmReportes.ConectaGlobal: Boolean;
begin
     with Global do
     begin
          Conectar;
          FHost := Global.GetGlobalString( K_GLOBAL_EMAIL_HOST );
          Result := ZetaCommonTools.StrLleno( FHost );
          if not Result then
             LogError( 'Datos Globales No Configurados' + CR_LF + 'Favor De Indicar El Servidor De Correos En El Cat�logo de Globales' );
          FUserID := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_USERID ), 'ReportesViaEmail' );
          FFromAddress := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMADRESS ), 'ReportesViaEmail@dominio.com' );
          FFromName := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMNAME ), 'Sistema TRESS-M�dulo Reportes V�a Email' );
          FErrorMail := Global.GetGlobalString( K_GLOBAL_EMAIL_MSGERROR );
     end;
end;

function TdmReportes.GetDirectorioImagenes: string;
begin
     Result := 'http://WWW.TRESS.COM.MX/media/';
end;

procedure TdmReportes.ModificaArchivoImagen( const sArchivo : string );
 const
      K_IMAGE_DIR = 'src="';
 var
    //i : integer;
    oLista : TStrings;
    //sImageName : string;
begin
     if FileExists( sArchivo ) then
     begin
          oLista := TStringList.Create;
          try
             oLista.LoadFromFile( sArchivo );

             {for i:= 0 to FNombresImagenes.Count - 1 do
             begin
                  sImageName := FNombresImagenes.Names[ i ];
                  oLista.Text := StrTransAll( oLista.Text,
                                              ZReportTools.DirPlantilla + sImageName,
                                              GetDirectorioImagenes + FNombresImagenes.Values[ sImageName ] );
             end;}
             oLista.Text := StrTransAll( oLista.Text,
                                         K_IMAGE_DIR,
                                         K_IMAGE_DIR + FURLImagenes );


             oLista.SaveToFile( sArchivo );
          finally
                 FreeAndNil( oLista );
          end;
     end;
end;

procedure TdmReportes.ModificaImagenHtml;
 var
    iPageCount : integer;
    sArchivo, sDir, sExt : string;
    oFile: TSearchRec;
begin
     if ( DatosImpresion.Tipo in [tfHtml,tfHtm] ) then
     begin
          sArchivo := FArchivoHTML;
          iPageCount := 0;
          if UnSoloHTML then
          begin
               ModificaArchivoImagen( sArchivo );
          end
          else
          begin
               sExt := ExtractFileExt( sArchivo );
               sDir := ExtractFilePath( sArchivo );
               sArchivo := Copy( sArchivo, 1, Pos( sExt, sArchivo )-1 ) + '*' + sExt;

               if FindFirst( sArchivo, faArchive, oFile) = 0 then
               begin
                    ModificaArchivoImagen( sDir + oFile.Name );
                    Inc( iPageCount );
                    while ( iPageCount <= FPageCount) AND ( FindNext( oFile ) = 0 ) do
                    begin
                         ModificaArchivoImagen( sDir + oFile.Name );
                         Inc( iPageCount );
                    end;
               end;
          end;
     end;
end;


procedure TdmReportes.GeneraMailError( const Error : string );
 const K_ARCHIVO = 'Error$Temp';
 var oArchivo : TStrings;
     sMensaje, sArchivo : string;
begin
     oArchivo := TStringList.Create;
     try
        if DatosImpresion.Tipo = tfHTML then
        begin
             sMensaje := Format ( K_MENSAJE_ERROR_HTML, [ {GetDirectorioImagenes,}
                                                          cdsReporte.FieldByName('RE_TITULO').AsString,
                                                          FormatDateTime('dd/mmm/yyyy', DATE),
                                                          NombreReporte,
                                                          Error] );

             sArchivo := ZReportTools.DirPlantilla + K_ARCHIVO + '.html';
        end
        else
        begin
             sMensaje := Format( K_MENSAJE_ERROR_PLAINTEXT, [ NombreReporte,
                                                              Error] );
             sArchivo := ZReportTools.DirPlantilla + K_ARCHIVO + '.txt';
        end;

        oArchivo.Text := sMensaje;
        oArchivo.SaveToFile( sArchivo );
     finally
            FArchivoHTML := sArchivo;
            FreeAndNil( oArchivo );
     end;

end;

procedure TdmReportes.DoOnGetResultado( const lResultado : Boolean; const Error : string );
begin
     if lResultado then
        GeneraOutPut
     else
     begin
          GeneraMailError( Error );
     end;
end;

procedure TdmReportes.DoAfterGetResultado( const lResultado : Boolean; const Error : string );
begin
     if lResultado then
     begin
          FArchivoHTML := DatosImpresion.Exportacion;
          ModificaImagenHtml;
     end;
end;


procedure TdmReportes.AddColumn( Fuente: TField );
begin
     with Resultado do
     begin
          case Fuente.DataType of
               ftString: AddStringField( Fuente.FieldName, Fuente.Size );
               ftSmallint: AddIntegerField( Fuente.FieldName );
               ftInteger: AddIntegerField( Fuente.FieldName );
               ftWord: AddIntegerField( Fuente.FieldName );
               ftBoolean: AddBooleanField( Fuente.FieldName );
               ftFloat: AddFloatField( Fuente.FieldName );
               ftCurrency: AddFloatField( Fuente.FieldName );
               ftDate: AddDateField( Fuente.FieldName );
               ftTime: AddDateField( Fuente.FieldName );
               ftDateTime: AddDateField( Fuente.FieldName );
          end;
     end;
end;

procedure TdmReportes.AddData( Fuente: TField );
var
   sCampo: String;
   Destino: TField;
begin
     with Resultado do
     begin
          sCampo := Fuente.FieldName;
          Destino := FieldByName( sCampo );
          case Fuente.DataType of
               ftString: Destino.AsString := Fuente.AsString;
               ftSmallint: Destino.AsInteger := Fuente.AsInteger;
               ftInteger: Destino.AsInteger := Fuente.AsInteger;
               ftWord: Destino.AsInteger := Fuente.AsInteger;
               ftBoolean: Destino.AsBoolean := Fuente.AsBoolean;
               ftFloat: Destino.AsFloat := Fuente.AsFloat;
               ftCurrency: Destino.AsFloat := Fuente.AsFloat;
               ftDate: Destino.AsDateTime := Fuente.AsDateTime;
               ftTime: Destino.AsDateTime := Fuente.AsDateTime;
               ftDateTime: Destino.AsDateTime := Fuente.AsDateTime;
          end;
     end;
end;

procedure TdmReportes.DoOnGetReportes( const lResultado: Boolean );
begin
     EscribeMensaje( Format( 'Procesando Reporte%s', [CR_LF + NombreReporte] ) );
end;

function TdmReportes.GeneraUnReporte(const iReporte: Integer; const lHTMLOnly: Boolean ): Boolean;
begin
     Result := GetResultado( iReporte, TRUE );
     if Result AND lHTMLOnly then
        Log( Format( 'La Fuente de Datos (#%d) correspondiente al Reporte #%d (%s) fu� Enviada al Servicio de TRESS', [ Identificador, iReporte, cdsReporte.FieldByName('RE_NOMBRE').AsString ] ) );
end;

procedure TdmReportes.MueveTemporales;
 var
    sExt, sDir : string;
    oFile: TSearchRec;

begin
     if DirectoryExists( FDirImagenes ) then
     begin
          if DatosImpresion.Tipo IN [ tfHTML,tfHTM ] then
          begin
               sExt := '.JPG';
               sDir := ExtractFilePath( FArchivoHTML );

               if FindFirst( sDir + ChangeFileExt(ExtractFileName( FArchivoHTML ), VACIO) + '*_I' + sExt, faArchive, oFile) = 0 then
               begin
                    CopyFile( PChar( sDir + oFile.Name ),
                              PChar( FDirImagenes +oFile.Name ),
                              FALSE );

                    while ( FindNext( oFile ) = 0 ) do
                    begin
                         CopyFile( PChar( sDir + oFile.Name ),
                                   PChar( FDirImagenes +oFile.Name ),
                                   FALSE );
                    end;
               end;
          end;
     end;
end;

procedure TdmReportes.AgregaAttachments( oLista : TStrings );
 var
    oFile: TSearchRec;
    sExt, sDir : string;
    iPageCount : integer;
begin
     iPageCount := 0;

     if DatosImpresion.Tipo IN [ tfHTML,tfHTM,tfTXT,tfWMF,tfEMF,tfBMP,tfPNG,tfJPEG,tfSVG ] then
     begin
          sExt := ExtractFileExt( FArchivoHTML );
          sDir := ExtractFilePath( FArchivoHTML );
          if FindFirst( Copy( FArchivoHTML, 1, Pos( sExt, FArchivoHTML )-1 ) + '*' + sExt, faArchive, oFile) = 0 then
          begin
               oLista.Add( sDir + oFile.Name );
               Inc( iPageCount );
               while ( iPageCount <= FPageCount) AND ( FindNext( oFile ) = 0 ) do
               begin
                    oLista.Add( sDir + oFile.Name );
                    Inc( iPageCount );
               end;
          end;
     end
     else
         oLista.Add( FArchivoHTML );
end;

procedure TdmReportes.EscribeMensaje( const sMensaje: string );
begin
     if EmailWorkingFile <> NIL then
        EmailWorkingFile.WriteMensaje( sMensaje );
end;

procedure TdmReportes.SendEmail( const Tipo: eEmailType;
                                 const lMsgNormal : Boolean;
                                 oUsuarios: TStrings );
var
   sTipo: String;
   sErrorMsg : string;
begin
     if ( oUsuarios.Count > 0 ) then
     begin
          with dmEmailService do
          begin
               ConectaHost;

               SubType := Tipo;
               Subject := 'Reporte ' + ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, FFrecuencia ) +
                          ' - '+ NombreReporte;

               ToAddress.AddStrings( oUsuarios );

               if ( SubType = emtHtml ) AND FileExists( FArchivoHTML )  then
               begin
                    MessageText.LoadFromFile( FArchivoHTML );
               end
               else
               begin
                    MessageText.Add( 'Reporte Enviado: '+ Subject );
                    AgregaAttachments( Attachments );
               end;

               MueveTemporales;

               //PENDIENTE
               //PostMessage.Date := DateToStr( Date );

               EscribeMensaje( Format( 'Enviando Reporte%s', [CR_LF + NombreReporte] ) );
               if Validate( sErrorMsg ) then
               begin
                    if SendEmail( sErrorMsg ) then
                    begin
                         if ( Tipo = emtHtml ) then
                            sTipo := 'en Formato HTML '
                         else
                             sTipo := 'con Archivo Adjunto ';

                         if lMsgNormal then
                            Log( Format( 'Reporte # %d Enviado %s A los Usuarios: %s', [ CodigoReporte, sTipo, CR_LF + oUsuarios.Text ] ) )
                    end
                    else
                        LogError( 'Error al Tratar de Enviar los Correos ' + CR_LF + sErrorMsg );
               end
               else
                   LogError( 'Se encontr� un Error ' + CR_LF + sErrorMsg );
          end;
     end;
end;

procedure TdmReportes.SendEmailError;
 var sErrorMsg : string;
begin
     {$ifdef ANTES}
     if ZetaCommonTools.StrVacio( FErrorMail ) then
     begin
          LogError( 'No Se Ha Especificado La Direccion de Email Para Errores' + CR_LF +
                    'Revisar Dentro Del Sistema Tress En Globales Empresa\Reportes V�a Email La Configuraci�n' );
     end;
     if ( FErroresMail.Count > 0 ) and ZetaCommonTools.StrLleno( FErrorMail ) and ConectaHost then
     begin
          with Email do
          begin
               SubType := mtPlain;
               with PostMessage do
               begin
                    ToAddress.Clear;
                    Body.Clear;
                    Attachments.Clear;
                    Subject := 'ERROR al Enviar Mensaje -  M�dulo De Reportes V�a Email';

                    ToAddress.Add( FErrorMail );
                    Body.Add( 'Notificaci�n Del M�dulo De Reportes V�a Email' );
                    Body.AddStrings( FErroresMail );
               end;
               PostMessage.Date := DateToStr( Date );
               SendMail;
          end;
     end;
     {$else}
     if ZetaCommonTools.StrVacio( FErrorMail ) then
     begin
          LogError( 'No Se Ha Especificado La Direccion de Email Para Errores' + CR_LF +
                    'Revisar Dentro Del Sistema Tress En Globales Empresa\Reportes V�a Email La Configuraci�n' );
     end;
     if ( FErroresMail.Count > 0 ) and ZetaCommonTools.StrLleno( FErrorMail ) then
     begin
          with dmEmailService do
          begin
               ConectaHost;
               SubType := emtTexto;

               Subject := 'ERROR al Enviar Mensaje -  M�dulo De Reportes V�a Email';
               ToAddress.Add( FErrorMail );
               MessageText.Add( 'Notificaci�n Del M�dulo De Reportes V�a Email' );
               MessageText.AddStrings( FErroresMail );

               if Validate( sErrorMsg ) then
               begin
                    if NOT SendEmail( sErrorMsg ) then
                       LogError( 'Error al Tratar de Enviar los Correos ' + CR_LF + sErrorMsg );
               end
               else
                   LogError( 'Se encontr� un Error ' + CR_LF + sErrorMsg );
          end;
     end;
     {$endif}
end;

procedure TdmReportes.DoOnGetDatosImpresion;
 {var sDir : string;}
begin
     with DatosImpresion do
     begin
          FBorraArchivo := Tipo in [tfImpresora];
          if FBorraArchivo then
          begin
               {.$ifdef TRESSEMAIL}
               {if FExportaPDF then
                  Tipo := tfPDF
               else}
               {.$endif}
               Tipo := tfHtml;

               {sDir := ExtractFilePath( Exportacion );
               if StrVacio( sDir ) then
                  Exportacion := VerificaDir( ExtractFilePath( Application.ExeName ) ) + Exportacion;
               if ( sDir = zReportTools.DirPlantilla ) then
                  Exportacion := StrTransAll( Exportacion, ExtractFilePath( Exportacion ), ExtractFilePath( Application.ExeName ) ) ;}
               Exportacion := VerificaDir( ExtractFilePath( Application.ExeName ) ) + 'Reporte.' + ObtieneElemento( lfExtFormato, Ord( Tipo ) ) ;

          end;
     end;
end;

function TdmReportes.GetExtensionDef : string;
 var eTipo : eExtFormato;
begin
     {.$ifdef TRESSEMAIL}
     {if FExportaPDF then
        eTipo := tfPDF
     else}
     {.$endif}
     eTipo := tfHtml;
     Result := ObtieneElemento( lfExtFormato, Ord( eTipo ) );
end;

procedure TdmReportes.BorraArchivos;
 var
    iPage : integer;
    sExt, sNombre, sArchivo : string;
begin
     if FileExists( DatosImpresion.Exportacion ) then
     begin
          DeleteFile( DatosImpresion.Exportacion );
     end
     else
     begin
          iPage := 1;
          with DatosImpresion do
          begin
               sExt := ExtractFileExt( Exportacion );
               sNombre := Copy( Exportacion, 1, Pos( sExt, Exportacion )-1 );
          end;

          sArchivo := sNombre + IntToStr( iPage ) + sExt;

          while FileExists( sArchivo ) do
          begin
               DeleteFile( sArchivo );
               Inc( iPage );
               sArchivo := sNombre + IntToStr( iPage ) + sExt;
          end;
     end;
end;

function TdmReportes.GetSobreTotales: Boolean;
 var i : integer;
begin
     Result := FALSE;
     for i := 0 to Campos.Count - 1 do
     begin
          with TCampoListado( Campos.Objects[ i ] ) do
          begin
               Result := Result OR (Operacion = ocSobreTotales);
          end;
     end;
end;

procedure TdmReportes.GeneraQRReporte;
 var eTipo : eTipoReporte;
begin
     with cdsReporte do
     begin
          eTipo := eTipoReporte( FieldByName('RE_TIPO').AsInteger );
          if ( eTipo = trForma ) then
          begin
               QRReporteListado.GeneraForma( FieldByName('RE_NOMBRE').AsString,
                                             FALSE,
                                             cdsResultados )
          end
          else if ( eTipo = trListado ) then
          begin
               QRReporteListado.GeneraListado( FieldByName('RE_NOMBRE').AsString,
                                               FALSE,
                                               zStrToBool( FieldByName('RE_VERTICA').AsString ),
                                               zStrToBool( FieldByName('RE_SOLOT').AsString ),
                                               GetSobreTotales,
                                               cdsResultados,
                                               Campos,
                                               Grupos );
          end;
     end;
end;

procedure TdmReportes.ModificaListaCampos( Lista : TStrings );
 var i : integer;
begin
     for i:=0 to Lista.Count -1 do
        with TCampoOpciones(Lista.Objects[i]) do
             if PosAgente >= 0 then
             begin
                  SQLColumna := FSQLAgente.GetColumna(PosAgente);
                  with FSQLAgente.GetColumna(PosAgente) do
                  begin
                       TipoImp := TipoFormula;
                       OpImp := Totalizacion;
                  end;
             end;
end;

procedure TdmReportes.ModificaListas;
 var i : integer;
begin
     ModificaListaCampos( Campos );
     for i:= 0 to Grupos.Count - 1 do
     begin
          with TGrupoOpciones( Grupos.Objects[i]) do
          begin
               ModificaListaCampos( ListaEncabezado );
               ModificaListaCampos( ListaPie );
          end;
     end;
end;

function TdmReportes.GeneraDataSetParaXML : boolean;
var
   i, iCampos, iGrupo: Integer;
begin
     Result := False;
     with cdsResultados do
     begin
          if  ( RecordCount > 0 ) then
          begin
               AsignaListas;
               iCampos := 0;
               iGrupo := -1;
               { Crear Campos }
               if SoloTotales then
               begin
                    { Encontrar el Grupo Encabezado M�s Interno }
                    iGrupo := ( Grupos.Count - 1 );
                    if ( iGrupo > 0 ) then
                    begin
                         { Asignar Los Campos Del Grupo Encabezado M�s Interno }
                         with TGrupoOpciones( Grupos.Objects[ iGrupo ] ).ListaEncabezado do
                         begin
                              for i := 0 to ( Count - 1 ) do
                              begin
                                   with TCampoListado( Objects[ i ] ) do
                                   begin
                                        if ( Calculado <> K_RENGLON_NUEVO ) then
                                        begin
                                             AddColumn( Fields[ SQLColumna.PosCampo ] );
                                             Inc( iCampos );
                                        end;
                                   end;
                              end;
                         end;
                    end;
               end;
               with Campos do
               begin
                    for i := 0 to ( Count - 1 ) do
                    begin
                         with TCampoListado( Objects[ i ] ) do
                         begin
                              if ( Calculado <> K_RENGLON_NUEVO ) then
                              begin
                                   if SoloTotales then
                                   begin
                                        if ( SQLColumna.Totalizacion <> ocNinguno ) then
                                        begin
                                             AddColumn( FieldByName( NombreTotal ) );
                                             Inc( iCampos );
                                        end;
                                   end
                                   else
                                   begin
                                        AddColumn( Fields[ SQLColumna.PosCampo ] );
                                        Inc( iCampos );

                                        {if ( SQLColumna.Totalizacion <> ocNinguno ) then
                                        begin
                                             AddColumn( FieldByName( NombreTotal ) );
                                             Inc( iCampos );
                                        end;}

                                   end;
                              end;
                         end;
                    end;
               end;
               if ( iCampos > 0 ) then
               begin
                    Result := True;
                    Resultado.CreateTempDataset;
                    { Agregar Titulos }
                    if SoloTotales and ( iGrupo > 0 ) then
                    begin
                         { Asignar Los T�tulos De Los Campos Del Grupo Encabezado M�s Interno }
                         with TGrupoOpciones( Grupos.Objects[ iGrupo ] ).ListaEncabezado do
                         begin
                              for i := 0 to ( Count - 1 ) do
                              begin
                                   with TCampoListado( Objects[ i ] ) do
                                   begin
                                        if ( Calculado <> K_RENGLON_NUEVO ) then
                                        begin
                                             Resultado.FieldByName( Fields[ SQLColumna.PosCampo ].FieldName ).DisplayLabel := Titulo;
                                        end;
                                   end;
                              end;
                         end;
                    end;
                    with Campos do
                    begin
                         for i := 0 to ( Count - 1 ) do
                         begin
                              with TCampoListado( Objects[ i ] ) do
                              begin
                                   if ( Calculado <> K_RENGLON_NUEVO ) then
                                   begin
                                        if SoloTotales then
                                        begin
                                             if ( SQLColumna.Totalizacion <> ocNinguno ) then
                                             begin
                                                  Resultado.FieldByName( NombreTotal ).DisplayLabel := Titulo;
                                             end;
                                        end
                                        else
                                        begin
                                             Resultado.FieldByName( Fields[ SQLColumna.PosCampo ].FieldName ).DisplayLabel := Titulo;
                                        end;
                                   end;
                              end;
                         end;
                    end;
                    { Copiar Datos }
                    First;
                    while not Eof do
                    begin
                         Resultado.Insert;
                         try
                            for i := 0 to ( Resultado.FieldCount - 1 ) do
                            begin
                                 AddData( FieldByName( Resultado.Fields[ i ].FieldName ) );
                            end;
                            Resultado.Post;
                         except
                               on Error: Exception do
                               begin
                                    Resultado.Cancel;
                                    raise;
                               end;
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

procedure TdmReportes.GeneraOutPut;
begin
     ModificaListas;
     BorraArchivos;

     with cdsResultados do
     begin
          if Active AND Not IsEmpty then
          begin
               First;
               {$ifdef CAROLINA}
               SaveToFile('d:\temp\ResEmail.cds');
               {$endif}
               ZFuncsCliente.RegistraFuncionesCliente;
               //Asignacion de Parametros del Reporte,
               //para poder evaluar la funcion PARAM() en el Cliente;
               ZFuncsCliente.ParametrosReporte := FSQLAgente.Parametros;
               GeneraArchivoXML;
          end;
     end;
end;

function TdmReportes.GetSoloTotales: Boolean;
begin
     Result := zStrToBool( cdsReporte.FieldByName('RE_SOLOT').AsString );
end;

function TdmReportes.GeneraArchivoAscii: Boolean;
 var oReporteAscii : TReporteAscii;
begin
     oReporteAscii := TReporteAscii.Create;
     try


        Result := oReporteAscii.GeneraAscii( cdsResultados,
                                             DatosImpresion,
                                             Campos,
                                             Grupos,
                                             GetSoloTotales,
                                             FALSE );
     finally
            FreeAndNil( oReporteAscii );
     end;
end;

function TdmReportes.ConectaHost: Boolean;
begin
     with dmEmailService do
     begin
          NewEMail;
          MailServer := FHost;
          User := FUserId;
          FromAddress := FFromAddress;
          FromName := FFromName;
          Result := TRUE;
     end;
end;

function TdmReportes.GetSuscripciones: Boolean;
var
   oUsuarios: Olevariant;
begin
     cdsSuscripcion.Data := ServerReportes.GetSuscripcion( dmCliente.Empresa, FFrecuencia, FUsuarios, FReportes, oUsuarios );
     cdsUsuarios.Data := oUsuarios;

     if StrVacio( FReportes ) then
     begin
          if cdsSuscripcion.IsEmpty then
             LogError( 'No Existe Ninguna Suscripci�n', TRUE );

          if cdsUsuarios.IsEmpty then
             LogError( 'No Existe Ning�n Usuario con Direcci�n de Correos', TRUE );
     end;
     
     Result := not ( cdsSuscripcion.IsEmpty OR cdsUsuarios.IsEmpty );
end;

function TdmReportes.PreparaPlantilla( var sError : WideString ) : Boolean;
 var
    lHayImagenes : Boolean;
    sDirectorio : string;
begin
     ZQRReporteListado.PreparaReporte;
     //DatosImpresion := GetDatosImpresion;

     sDirectorio := ExtractFilePath( DatosImpresion.Archivo );
     Result := DirectoryExists( sDirectorio );
     if NOT Result then
        sError := Format( 'El Directorio de Plantillas " %s " No Existe', [ sDirectorio ] );

     sDirectorio := ExtractFilePath( DatosImpresion.Exportacion );
     Result := DirectoryExists( sDirectorio );
     if Not Result then
        sError := Format( 'El Directorio del Archivo " %s " No Existe', [ sDirectorio ] );

     if Result then
     begin
          DoOnGetDatosImpresion;

          with QRReporteListado do
          begin
               Init( FSQLAgente,
                     DatosImpresion,
                     Parametros.Count,
                     lHayImagenes );

               AddNombreImagenes( FNombresImagenes );
          end;
     end;
end;

procedure TdmReportes.DesPreparaPlantilla;
begin
     with QRReporteListado do
     begin
          FPageCount := PageCount;
          FUnSoloHTML := UnSoloHTML;
     end;

     ZQrReporteListado.DesPreparaReporte;
end;



procedure TdmReportes.cdsSuscripReportesLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;var sKey, sDescription: String);
begin
     inherited;
     with Sender do
     begin
          if FormaLookupReporte = NIL then
             FormaLookupReporte := TFormaLookupReporte.Create( self );

          with FormaLookupReporte do
          begin
               lOK := ShowModal = mrOk;
               if lOk then
               begin
                    sKey := IntToStr( NumReporte );
                    sDescription := NombreReporte;
               end;
          end;
     end;
end;



procedure TdmReportes.cdsUsuariosLookupAlAdquirirDatos(Sender: TObject);
var
   iLongitud: Integer;
   lBloqueoSistema: WordBool;
begin
     inherited;
     cdsUsuariosLookUp.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBloqueoSistema );
end;

function TdmReportes.GetClasificaciones(oClasificaciones: TStrings): string;
var
   i: integer;
begin
     Result := VACIO;
     if not dmCliente.ModoTress then
     begin
          with oClasificaciones do                 
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Result := ConcatString( Result,
                                            IntToStr( Ord( eClasifiReporte( oclasificaciones.objects[ i ] ) ) ), ',' );
               end;
          end
     end;
end;

procedure TdmReportes.cdsSuscripReportesAlAdquirirDatos(Sender: TObject);
begin
     with cdsSuscripReportes do
     begin
          IndexFieldNames:='';
          IndexName:='';
          Data := ServerReportes.GetReportes( dmCliente.Empresa,
                                              Ord( FClasifActivo ),
                                              Ord( crFavoritos ),
                                              Ord( crSuscripciones ), 
                                              GetClasificaciones(FClasificaciones),
                                              dmDiccionario.VerConfidencial);
          IndexFieldNames:='RE_CODIGO';
     end;
end;

procedure TdmReportes.ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
{ var
    Pendientecaro: integer;}
begin

     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
              {$ifdef RDD}
              Text := VACIO;
              {$else}
              Text := ObtieneEntidad( TipoEntidad( Sender.AsInteger ) );
              {$endif}

     end
     else
         Text:= Sender.AsString;
end;

procedure TdmReportes.cdsSuscripReportesAlCrearCampos(Sender: TObject);
var
   oCampo: TField;
begin
     inherited;
     with cdsSuscripReportes do
     begin
          oCampo := FindField( 'RE_ENTIDAD' );
          if ( oCampo <> nil )  then
          begin
               with oCampo do
               begin
                    OnGetText := ObtieneEntidadGetText;
                    Alignment := taLeftJustify;
               end;
          end;
          ListaFija('RE_TIPO', lfTipoReporte);
     end;
end;

procedure TdmReportes.cdsUsuariosLookupGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     inherited;
     lHasRights := FALSE;
end;

function TdmReportes.SistemaBloqueado: Boolean;
begin
     Result:= zstrToBool( ServerLogin.GetClave( ZetaServerTools.CLAVE_SISTEMA_BLOQUEADO ) );
end;

function TdmReportes.GeneraArchivoXML: Boolean;
const
K_DETALLE_TAG = 'Detalle' ;
K_REPORTE_TAG = 'REPORTE' ;
var
   FXMLTools: DXMLTools.TdmXMLTools;
begin
     FXMLTools := TdmXMLTools.Create( nil );
     REsult := False;

     FResultado := TZetaClientDataSet.Create( Self );

     try
       if GeneraDataSetParaXML then
       begin
          with FXMLTools do
          begin
               NetXMLBuildDataSet( Resultado, K_REPORTE_TAG , K_DETALLE_TAG, Resultado.RecordCount );
               XMLEnd;

               if FTestingXML then
                     FArchivoDestino := Format('Datos_%d.xml', [Self.Identificador]);

               if StrLleno( FArchivoDestino )   then
               begin

                  XML.SaveToFile( FArchivoDestino );
               end;

               if not FTestingXML then
                  SendXML( FXMLTools );

               Result := True;
          end;
          Resultado.EmptyDataSet;

       end;
     finally
          FreeAndNil( FResultado );
          FreeAndNil( FXMLTools );
     end;

end;

procedure TdmReportes.SendXML( FXMLTools : TdmXMLTools);
const
     K_DURACION_TIMEOUT = 10; //10 minutos
var
  ws : TressMovilServiceSoap;
  sRespuesta, sTimeStamp : AnsiString;
  sXML : AnsiString;
  timestamp : TDateTime;
  UTC: TSystemTime;
  byteArray : TByteDynArray;

  function LoadFile(const FileName: TFileName): AnsiString;
begin
  with TFileStream.Create(FileName,
      fmOpenRead or fmShareDenyWrite) do begin
    try
      SetLength(Result, Size);
      Read(Pointer(Result)^, Size);
    except
      Result := '';  // Deallocates memory
      Free;
      raise;
    end;
    Free;
  end;
end;


function StringToByteArray(const ansiStr: string): TByteDynArray;
var
  MSTream: TMemoryStream;
  pTemp: pointer;
//  str : string;
  str : UTF8String;
begin
  //str := ansiStr;
  str := UTF8Encode( ansiStr );
  MStream := TMemoryStream.Create;
  MStream.SetSize( length(str)  );
  MStream.Write( Pointer( str)^,length(str));
  MStream.Position := 0;

  SetLength(Result, MStream.Size);
  pTemp := @Result[0];
  MStream.Position := 0;
  MStream.Read(pTemp^, MStream.Size);

  MStream.Free;
end;



begin
     GetSystemTime(UTC);
     timestamp := SystemTimeToDateTime( UTC );
     timestamp := timestamp + EncodeTime(0,K_DURACION_TIMEOUT,0,0);
     sTimeStamp := FormatDateTime('YYYY-MM-DD hh:mm', timestamp );
//     sTimeStamp := sTime;

     try
        try
           ws := GetTressMovilServiceSoap( False, Self.URLServicio,  nil, Self.UsuarioServicio, Self.PassServicio );
           sXML :=  FXMLTools.XMLAsText;
           //sXML := LoadFile( 'BIGXML.xml');
           sTimeStamp := Format('%s%d%s%s%d',[sTimeStamp, Length( sXML ), Self.LoginServicio, Self.LoginPassServicio, Self.Identificador]);
           //

          // swXML :=  WideString( sXML ) ;
           byteArray := StringToByteArray( sXML ) ;
           if UsarEncriptadoServicio then
               sRespuesta := ws.ProcesarReporteXML(  Encrypt( sXML )  , Encrypt( Format('%d',[Self.Identificador])), Encrypt( Self.LoginServicio ), Encrypt(Self.LoginPassServicio), Encrypt( sTimeStamp ) )
           else
               sRespuesta := ws.ProcesarReporteXMLUCLong( byteArray , Encrypt( Format('%d',[Self.Identificador])), Encrypt( Self.LoginServicio ), Encrypt(Self.LoginPassServicio), Encrypt( sTimeStamp ));
//               sRespuesta := ws.ProcesarReporteXMLUC( sXML , Encrypt( Format('%d',[Self.Identificador])), Encrypt( Self.LoginServicio ), Encrypt(Self.LoginPassServicio), Encrypt( sTimeStamp ));

           if ( Pos( 'ERROR',  UpperCase( sRespuesta)  ) > 0 )  or ( StrVacio( sRespuesta ))  then
               raise Exception.Create( 'No se pudo guardar la Fuente de Datos XML en el Servicio de TRESS:' +sRespuesta);


        except
              on Error: Exception do
              begin
                  raise Exception.Create( 'No se pudo enviar la Fuente de Datos XML al Servicio de TRESS :' + Error.Message );
              end;
        end;
     finally
            //FreeAndNil (ws);
            //FreeAndNil(  byteArray ) ;
     end;
end;

procedure TdmReportes.SetReportesXMLLogFileName(const sValue: String);
begin
     SetLogFileName( sValue);
end;

procedure TdmReportes.ImprimeUnaForma(const sFiltro: string);
begin
//
end;

procedure TdmReportes.ImprimeUnaForma(const sFiltro: string;
  const iReporte: integer; const eSalida: TipoPantalla);
begin
//
end;

end.
