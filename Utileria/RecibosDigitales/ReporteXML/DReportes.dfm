inherited dmReportes: TdmReportes
  OldCreateOrder = True
  Left = 379
  Top = 345
  Height = 479
  Width = 741
  object cdsUsuariosLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'US_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsUsuariosLookupAlAdquirirDatos
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    OnGetRights = cdsUsuariosLookupGetRights
    Left = 168
    Top = 16
  end
  object cdsSuscripReportes: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsSuscripReportesAlAdquirirDatos
    AlCrearCampos = cdsSuscripReportesAlCrearCampos
    LookupName = 'Reportes'
    LookupDescriptionField = 'RE_NOMBRE'
    OnLookupSearch = cdsSuscripReportesLookupSearch
    Left = 168
    Top = 88
  end
  object cdsEscogeReporte: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    Left = 144
    Top = 168
  end
end
