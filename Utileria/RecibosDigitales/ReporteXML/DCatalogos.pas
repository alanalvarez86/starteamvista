unit DCatalogos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet, ZetaCommonLists,
{$ifdef DOS_CAPAS}
     {$ifdef TRESS}
     DServerCatalogos
     {$endif}
     {$ifdef SELECCION}
     DServerSeleccion
     {$endif}
{$else}
  {$ifdef TRESS}
  Catalogos_TLB
  {$endif}
  {$ifdef SELECCION}
  Seleccion_TLB
  {$endif}
{$endif};

type
  TdmCatalogos = class(TDataModule)
    cdsCondiciones: TZetaLookupDataSet;
    cdsNomParamLookUp: TZetaLookupDataSet;
    cdsPuestos: TZetaLookupDataSet;
    cdsTurnos: TZetaLookupDataSet;
    cdsCursos: TZetaLookupDataSet;
    cdsNomParam: TZetaLookupDataSet;
    cdsClasifi: TZetaLookupDataSet;
    cdsConceptos: TZetaLookupDataSet;
    ZetaLookupDataSet1: TZetaLookupDataSet;
    cdsContratos: TZetaLookupDataSet;
    cdsEventos: TZetaLookupDataSet;
    cdsCalendario: TZetaClientDataSet;
    cdsFestTurno: TZetaLookupDataSet;
    cdsFolios: TZetaLookupDataSet;
    cdsHorarios: TZetaLookupDataSet;
    cdsInvitadores: TZetaLookupDataSet;
    cdsReglas: TZetaLookupDataSet;
    cdsOtrasPer: TZetaLookupDataSet;
    cdsMaestros: TZetaLookupDataSet;
    cdsMatrizCurso: TZetaClientDataSet;
    cdsOrdFolios: TZetaLookupDataSet;
    cdsEntNivel: TZetaClientDataSet;
    cdsRPatron: TZetaLookupDataSet;
    cdsSSocial: TZetaLookupDataSet;
    cdsPrestaci: TZetaLookupDataSet;
    cdsPRiesgo: TZetaLookupDataSet;
    cdsPeriodo: TZetaLookupDataSet;
    cdsConceptosLookUp: TZetaLookupDataSet;
    ZetaLookupDataSet2: TZetaLookupDataSet;
    cdsTools: TZetaLookupDataSet;
    cdsPercepFijas: TZetaClientDataSet;
    cdsAreas: TZetaLookupDataSet;
    cdsPuestosLookup: TZetaLookupDataSet;
    cdsPtoTools: TZetaClientDataSet;
    cdsSesiones: TZetaLookupDataSet;
    cdsHisCursos: TZetaClientDataSet;
    cdsAccReglas: TZetaLookupDataSet;
    cdsAulas: TZetaLookupDataSet;
    cdsPrerequisitosCurso: TZetaClientDataSet;
    cdsCertificaciones: TZetaLookupDataSet;
    cdsTiposPoliza: TZetaLookupDataSet;
    cdsTPeriodos: TZetaLookupDataSet;
    cdsCamposPerfil: TZetaClientDataSet;
    cdsSeccionesPerfil: TZetaLookupDataSet;
    cdsPerfilesPuesto: TZetaLookupDataSet;
    cdsPerfiles: TZetaClientDataSet;
    cdsDescPerfil: TZetaClientDataSet;
    cdsProvCap: TZetaLookupDataSet;
    cdsValPlantilla: TZetaLookupDataSet;
    cdsValFactores: TZetaLookupDataSet;
    cdsSubfactores: TZetaLookupDataSet;
    cdsValNiveles: TZetaLookupDataSet;
    cdsValuaciones: TZetaClientDataSet;
    cdsValPuntos: TZetaClientDataSet;
    cdsPuntosNivel: TZetaLookupDataSet;
    cdsPeriodoOtro: TZetaLookupDataSet;
    cdsTemp: TZetaClientDataSet;
    cdsRSocial: TZetaLookupDataSet;
    cdsMatrizCertificPuesto: TZetaClientDataSet;
    cdsCerNivel: TZetaClientDataSet;
    cdsReglaPrestamo: TZetaLookupDataSet;
    cdsPrestaXRegla: TZetaClientDataSet;
    cdsHistRev: TZetaLookupDataSet;
    cdsPeriodoAd: TZetaLookupDataSet;
    cdsEstablecimientos: TZetaLookupDataSet;
    cdsTiposPension: TZetaLookupDataSet;
    cdsSegGastosMed: TZetaLookupDataSet;
    cdsVigenciasSGM: TZetaLookupDataSet;
    cdsCosteoGrupos: TZetaLookupDataSet;
    cdsCosteoCriterios: TZetaLookupDataSet;
    cdsCosteoCriteriosPorConcepto: TZetaClientDataSet;
    cdsConceptosDisponibles: TZetaLookupDataSet;
    cdsTablasAmortizacion: TZetaLookupDataSet;
    cdsCostosAmortizacion: TZetaLookupDataSet;
    cdsCompetencias: TZetaLookupDataSet;
    cdsCompRevisiones: TZetaClientDataSet;
    cdsCompCursos: TZetaClientDataSet;
    cdsCatPerfiles: TZetaLookupDataSet;
    cdsRevPerfiles: TZetaClientDataSet;
    cdsMatPerfilComps: TZetaClientDataSet;
    cdsCompNiveles: TZetaLookupDataSet;
    cdsGpoCompPuesto: TZetaClientDataSet;
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerCatalogos:{$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif};

    property ServerCatalogo:{$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif} read GetServerCatalogos;
{$else}
    {$ifdef TRESS}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp ;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
    {$ENDIF}
    {$ifdef SELECCION}
    FServidor: IdmServerSeleccionDisp;
    function GetServerCatalogos: IdmServerSeleccionDisp ;
    property ServerCatalogo: IdmServerSeleccionDisp read GetServerCatalogos;
    {$ENDIF}
{$endif}

  public
    { Public declarations }
    procedure ShowCalendarioRitmo( const sTurno: string );
    procedure GetDatosPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo );
    function GetJornadaHorario(const sHorario: String): Currency;

  end;

var
  dmCatalogos: TdmCatalogos;

implementation
uses DCliente;
{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: {$ifdef TRESS}TdmServerCatalogos{$endif}{$ifdef SELECCION}TdmServerSeleccion{$endif};
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
 {$ifdef TRESS}
 function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
 begin
      Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
 end;
 {$endif}
 {$ifdef SELECCION}
 function TdmCatalogos.GetServerCatalogos: IdmServerSeleccionDisp;
 begin
      Result := IdmServerSeleccionDisp( dmCliente.CreaServidor( CLASS_dmServerSeleccion, FServidor ) );
 end;
 {$endif}
{$endif}

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

procedure TdmCatalogos.ShowCalendarioRitmo( const sTurno: string );
begin
end;


function TdmCatalogos.GetJornadaHorario(const sHorario: String): Currency;
begin
     Result := 0;
     with cdsHorarios do
     begin
          Conectar;
          if Locate( 'HO_CODIGO', sHorario, [] ) then
          begin
               Result := FieldByName( 'HO_JORNADA' ).AsFloat;
          end;
     end;
end;


procedure TdmCatalogos.GetDatosPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo );
begin
end;


end.
