unit ZetaDialogoWebTimbrado;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, CommDlg,ZetaCommonLists,MensajesTimbradoWebServices;

function ZetaMessageWebTimbrado( const sCaption, sLinkTemporal: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; InfoBotonOK: TInfoBotonOKDialogo; InfoBotonCLOSE: TInfoBotonCLOSEDialogo; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult; forward;
procedure ZDialogWebTimbrado( const sCaption, sLinkTemporal: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; InfoBotonOK: TInfoBotonOKDialogo; InfoBotonCLOSE: TInfoBotonCLOSEDialogo; const iHelpCtx: Longint );

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaMsgDlgWebTimbrado;
var
   ZDialogoWebTimbrado: TZDialogoWebTimbrado;


function GetDialogo: TZDialogoWebTimbrado;  // Siempre debe ser Privada a esta unidad //
begin
     if ( ZDialogoWebTimbrado = nil ) then
     begin
          ZDialogoWebTimbrado := TZDialogoWebTimbrado.Create( Application ); // Application Debe Destruirlo Al Finalizar //
     end;
     Result := ZDialogoWebTimbrado;
end;

function ZetaMessageWebTimbrado( const sCaption, sLinkTemporal: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; InfoBotonOK: TInfoBotonOKDialogo; InfoBotonCLOSE: TInfoBotonCLOSEDialogo; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult;
begin
     with GetDialogo do
     begin
          Caption := sCaption;
          LinkTemporal := sLinkTemporal;
          Tipo := DlgType;
          Botones := Buttons;
          BotonOK := InfoBotonOK;
          BotonCLOSE := InfoBotonCLOSE;
          DefaultBoton := oDefaultBoton;
          HelpCtx := iHelpCtx;
          Result := Execute;
     end;
end;

procedure ZDialogWebTimbrado( const sCaption, sLinkTemporal: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; InfoBotonOK: TInfoBotonOKDialogo; InfoBotonCLOSE: TInfoBotonCLOSEDialogo; const iHelpCtx: Longint );
begin
     ZetaMessageWebTimbrado( sCaption, sLinkTemporal, DlgType, Buttons, InfoBotonOK, InfoBotonCLOSE, iHelpCtx, mbOk );
end;
end.

