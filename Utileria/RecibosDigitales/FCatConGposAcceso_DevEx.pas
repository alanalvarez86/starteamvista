unit FCatConGposAcceso_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZBaseDlgModal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxContainer, cxEdit, cxCheckListBox, ImgList, cxButtons;

type
  TCatConGposAcceso_DevEx = class(TZetaDlgModal_DevEx)
    Prender: TcxButton;
    Apagar: TcxButton;
    PanelBusqueda: TPanel;
    Label1: TLabel;
    PanelLista: TPanel;
    Niveles: TcxCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure NivelesClickCheck(Sender: TObject; AIndex: Integer;
      APrevState, ANewState: TcxCheckBoxState);
    procedure CheckCLick;
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
  public
    { Public declarations }
  end;

var
  CatConGposAcceso_DevEx: TCatConGposAcceso_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema, dCatalogos;

{$R *.DFM}

procedure TCatConGposAcceso_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TCatConGposAcceso_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     OK_DevEx.Enabled := False;
     ActiveControl := Niveles;

     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;

end;

procedure TCatConGposAcceso_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TCatConGposAcceso_DevEx.Cargar;
var
   i: Integer;
begin
     dmCatalogos.CargaListaGrupos( FLista );
     with Niveles do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       with Items.Add do
                       begin
                        text :=( FLista[i] );
                        Checked := ( Integer( Objects[ i ] ) > 0 );
                       end
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TCatConGposAcceso_DevEx.Descargar;
var
   i: Integer;
begin
     with Niveles do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
            with items[i] do
              begin
                    if Checked then
                        FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmCatalogos.DescargaListaGrupos( FLista );
end;

procedure TCatConGposAcceso_DevEx.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Niveles do
     begin
     items.BeginUpdate;
        try
            for i := 0 to (items.Count - 1 ) do
            begin
                with Items[i] do
                begin
                    Checked := lState;
                end;
            end;
        finally
            items.EndUpdate;
        end;
     end;
     CheckCLick;
end;

procedure TCatConGposAcceso_DevEx.CheckCLick;
begin
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;
procedure TCatConGposAcceso_DevEx.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TCatConGposAcceso_DevEx.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TCatConGposAcceso_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;




procedure TCatConGposAcceso_DevEx.NivelesClickCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

end.
