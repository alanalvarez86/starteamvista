object WizardConciliacionTimbradoForm: TWizardConciliacionTimbradoForm
  Left = 319
  Top = 5
  Caption = 'Conciliaci'#243'n Timbrado'
  ClientHeight = 561
  ClientWidth = 1250
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxWizardControl1: TdxWizardControl
    Left = 0
    Top = 0
    Width = 1250
    Height = 561
    AutoSize = True
    Buttons.Back.Caption = '&Atras'
    Buttons.Back.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000206348524D00007A26000080
      840000FA00000080E8000075300000EA6000003A98000017709CBA513C000000
      097048597300000EAF00000EAF01711143EE0000008349444154384F63201674
      D77733F634A2602610DDDDD8C5DC55DF25015546186031088C810631020D6284
      2A230C8006850035FE06E2FFC81868D075525D346A100130F80CEAADEF55C166
      1008030D32802A231E00351560338C6497C100D50DECAEEFCC001A4079D8C100
      D522030646A681F8CA33AA18C800C2400341490AA8828101008A004FA9E01121
      AC0000000049454E44AE426082}
    Buttons.Cancel.Caption = '&Cancelar'
    Buttons.Cancel.Glyph.Data = {
      89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
      610000000467414D410000B18F0BFC6105000000206348524D00007A26000080
      840000FA00000080E8000075300000EA6000003A98000017709CBA513C000000
      097048597300000EAF00000EAF01711143EE000000BA49444154384F8D913112
      C220104529BC574AB07746BC85F7B0F018293C84BD92135968FE861F11D8357F
      E617C0BE97998D639E311CA6E82FF9A8063398CDC725B84831BCE6BEA7D3FE9A
      AF9BE00D33985D2525CCF62405CC2E9214FD583D484B4907CEF5A3BB0FC32E1D
      FDAD370050856706AC7CC192745BC2CC66490F66FE4A2C98D11766FF628905B3
      AA640BCC361215C63E949DAC120BC6C2ACC58AE411C3B979ACB6AD49C0CAC08F
      A482995AB2C28C481498A1E40B3BF701FFE789DE4F9390EE0000000049454E44
      AE426082}
    Buttons.CustomButtons.Buttons = <>
    Buttons.Finish.Caption = '&Aplicar'
    Buttons.Finish.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000206348524D00007A26000080
      840000FA00000080E8000075300000EA6000003A98000017709CBA513C000000
      097048597300000EAF00000EAF01711143EE000001FB49444154384FCD93CF2B
      055114C767DEF3E4577EC702C90E4549598884B9C34A6225B2B050786FE6D988
      CDDB4A562C10FF00A5B0B03233AFC4C242922CD850F2A388A5ADCF7926F5123D
      4A39F5E9CE9C7BCEF79E7BCFBDDABF32CB515AC4513AF89E5F9AED29DD76CD0C
      CB35F320CD77FFCCACB8D22C4F056DCF2C44AC0DA121C64A7F3A75B31C43B696
      2EC9880DC226DFA730A685F7BA3EF8CE6CAA89789D019273486C8069BE0FE109
      6EA96E960015E0232B8163EA7E6E92D9F82DCF0C59AE2A21B11B9611BB607C84
      13DB55F36CBB51F65E8AA31D5A09C8A5822441CB33E490B359AC1A4688DB863B
      B8855D9A301EF58CAA49AF33281DE9C3B9096B2421A832C6E36D1CB2C19CC121
      AB22E65A200607700FE7B08A782F39A510A4FA447BC34CDCC035CC410DD5A611
      C8B6CC0AFEFB6105CE402A12C1188B36939BCF8281280B270C074E73075EE018
      C28848A76A610264EECA670B4611AA864C7293CF98C95CCA1C603C826770F89F
      61942AC5F700B2AD25E88132844272453E195504E85439415370092228A35422
      42FB20E24D6CBFE0FDB27EF174E89674349D807A1216410EF815E41C37406E77
      15F06C78835F098949D710D311CB21D1807538A6E205C60EAA2E8ED0ADC938DD
      4AC5A2AE9246C8ED966B60C23062755493250BF961A91B1508727ED988E53386
      1277E7B7E60B4A95BA9DEAB6FEC634ED0DA90748A38751FD620000000049454E
      44AE426082}
    Buttons.Help.Visible = False
    Buttons.Next.Caption = '&Siguiente'
    Buttons.Next.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000206348524D00007A26000080
      840000FA00000080E8000075300000EA6000003A98000017709CBA513C000000
      097048597300000EAF00000EAF01711143EE000000A449444154384F630081AE
      FA2E89EEC62E869EC66E0CDC5DDFCD0856440A001AC88664202332061AC80C55
      463C80BAF03AD080FF68F837D0C010A832E2C1A8812806F6D477A64095110FF0
      18F81F28570055463CA0BA811DF51D3240CDDFD10D036160183A40951106F5F5
      F52C3D8D5DABB119D4D3D4DD0F5546188C1A44180C5A8398801A61E51890DFCD
      0EC41C4083882FCFA0068134A2148A60DCD40D554508303000009CD04F09670C
      D6D20000000049454E44AE426082}
    Buttons.Next.GlyphAlignment = blGlyphRight
    Buttons.Next.Width = 78
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyph, wchvTitleFont]
    Header.DescriptionFont.Charset = DEFAULT_CHARSET
    Header.DescriptionFont.Color = clDefault
    Header.DescriptionFont.Height = -11
    Header.DescriptionFont.Name = 'Tahoma'
    Header.DescriptionFont.Style = []
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBF00000EBF013805
      532400000317494441545847CD563168135118AEDA8A62101141DD141C441C1C
      1C3A04ECE8E0E0A068EE0215EAD04550BA74903BA1833825B9C4C92163731721
      831407850E1D3A380431F4EEE8902EA2E090C1A18370CFEF7FF7EECCDDBDE4EE
      426CFBC19F0B79DFFFFDDF7BFFFFAE9D618C1D6AF81F19C19A8B335EAD7CC133
      94A2575397F05CC373D533D412FDC60CE53C71B22097011429F0423565C00C95
      8D0BF07E1297D5D573225D8A4C0658E5D12C76569115CA12DC74B57C4AC84590
      6A8019A5CB28BE2713CE13FC44AAEA55D2F41AE5395655E7B9FE38035E55B988
      BEEECB0429A815586F79756559B4E6B38C371CE03FE36670A25463A40156514E
      13512A4205471D69E3E131EA3B0ABD95E50601ED0DCE9719C0E209ECE66B2209
      AEBDFAE33382960A3A72998EAFA5BAC4491860FA1D32B0914C5056682D0FF875
      C5D58C6B05C139090386722D4E44F1B5DCC58DD249E475E25AC341271D31E0EF
      3E3A485C047D15BA99C1B5FC21D610F241C69C450D544B67E32488DC109A1383
      EF14D70E46B622FA18D6A8819ABA304CC0EEF7B2BE52B342BCC257FC5341BB87
      0DC0E99B9881FB226FEA608DC5E3DF1B2FE6F8F7D080A1DE8389A520A8259CFD
      9F111A90C54180D739C882711C2D037A6B675E6FDB0B626DEA206DCD749A61B4
      DD775103A6FD40B71C86C52EFFBEB9392B72A702CDB43F927E109AE56C470DC0
      6184603ABB7ACB79AE37FBD2BF7E79A0BF77AE0F6BF330ED57A1014E5AB7AF24
      4808CDB27F1159B77A973871026033758976316AC0EA1524A43020F21B737293
      937300B9456C623FA6D5A5B58801429C182658CE37DD726F095A66D0D173E371
      CD96C3DFB412034E3F414640A49DB70598A9DBC8FB91D0B2DC8EA0480C98F656
      3C2108880D300BCB69B78366095CBA6A7F121AB4C14E3FFC775D72026E87DA00
      E28778721010DED54CF7B56E3A77A92D3417FCDAB6EC975ADB591FD3C63EB544
      94E24818E0D74E0C1A9E4F65BB9824A0D395B53061200E2417FD64B9705AF8A7
      B1B33AAA6DA90602D0D4E208B765456401D303BC6A2B69839BD9400012D42DFB
      09BDC7694E50E80BF5968617CF4FFF66A357102963111A38BC60337F01309D0B
      923E6473940000000049454E44AE426082}
    Header.TitleFont.Charset = DEFAULT_CHARSET
    Header.TitleFont.Color = clDefault
    Header.TitleFont.Height = -11
    Header.TitleFont.Name = 'Tahoma'
    Header.TitleFont.Style = []
    OptionsAnimate.TransitionEffect = wcteSlide
    ParentFont = False
    OnButtonClick = dxWizardControl1ButtonClick
    OnPageChanged = dxWizardControl1PageChanged
    OnPageChanging = dxWizardControl1PageChanging
    object WizardConciliacionPaginaContribuyente: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'En este proceso se comparar'#225' la informaci'#243'n de timbrado de n'#243'min' +
        'a de Sistema TRESS con la que se tiene reportada en PAC/SAT.'
      Header.Title = 'Conciliaci'#243'n de timbrado de n'#243'mina'
      ParentFont = False
      object cxGroupBox1: TcxGroupBox
        Left = 295
        Top = 176
        TabOrder = 0
        Height = 105
        Width = 650
        object Label10: TLabel
          Left = 40
          Top = 43
          Width = 68
          Height = 13
          Caption = 'Contribuyente:'
          Color = clWhite
          ParentColor = False
        end
        object RS_CODIGO: TZetaKeyLookup_DevEx
          Left = 110
          Top = 39
          Width = 500
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          OnValidLookup = RS_CODIGOValidLookup
        end
      end
    end
    object WizardConciliacionPagina1: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Seleccione el mes con el que realizar'#225' la conciliaci'#243'n, posterio' +
        'rmente indique los periodos de n'#243'mina que se comparar'#225'n con la i' +
        'nformaci'#243'n de PAC/SAT, haga clic en conciliar para iniciar el pr' +
        'oceso.'
      Header.Title = 'Conciliaci'#243'n de timbrado de n'#243'mina.'
      ParentFont = False
      object PanelGrid: TPanel
        Left = 0
        Top = 41
        Width = 1228
        Height = 380
        Align = alClient
        BevelOuter = bvNone
        Caption = 'PanelGrid'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object GridPeriodosAConciliar: TZetaCXGrid
          Left = 0
          Top = 0
          Width = 1228
          Height = 380
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGridDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            FilterBox.CustomizeDialog = False
            DataController.DataSource = DataSourceNominasXYear
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = 'No hay Periodos'
            OptionsView.GroupByBox = False
            object cxGridDBTableView1Column1: TcxGridDBColumn
              Caption = 'A'#241'o'
              DataBinding.FieldName = 'PE_YEAR'
              MinWidth = 50
              Options.Editing = False
            end
            object cxGridDBTableView1Column2: TcxGridDBColumn
              Caption = 'Tipo'
              DataBinding.FieldName = 'PE_TIPO'
              MinWidth = 50
              Options.Editing = False
            end
            object PERIODO: TcxGridDBColumn
              Caption = 'Periodo'
              DataBinding.FieldName = 'PE_NUMERO'
              MinWidth = 50
              Options.Editing = False
              Width = 100
            end
            object NOMINA: TcxGridDBColumn
              Caption = 'N'#243'mina'
              DataBinding.FieldName = 'NOMBRE_PERIODO'
              MinWidth = 50
              Options.Editing = False
              Width = 180
            end
            object STATUS_TIMBRADO: TcxGridDBColumn
              Caption = 'Estatus timbrado'
              DataBinding.FieldName = 'PE_TIMBRO'
              MinWidth = 50
              Options.Editing = False
              Width = 95
            end
            object TOTAL: TcxGridDBColumn
              Caption = 'Total'
              DataBinding.FieldName = 'TOTAL'
              MinWidth = 50
              Options.Editing = False
            end
            object TIMBRADO_EN_TRESS: TcxGridDBColumn
              Caption = 'Timbrado en TRESS'
              DataBinding.FieldName = 'TIMBRADOS_TRESS'
              MinWidth = 50
              Options.Editing = False
              Width = 119
            end
            object TIMBRADO_EN_SAT: TcxGridDBColumn
              Caption = 'Timbrado en SAT'
              DataBinding.FieldName = 'TIMBRADOS_SAT'
              Visible = False
              MinWidth = 50
              Options.Editing = False
              Width = 108
            end
            object cxGridDBTableView1Column3: TcxGridDBColumn
              Caption = 'Porcentaje de diferencias'
              DataBinding.FieldName = 'PORC_CONCILIADOS'
              Visible = False
              MinWidth = 50
              Options.Editing = False
              Width = 143
            end
            object ESTATUS_CONCILIACION: TcxGridDBColumn
              Caption = 'Diferencias'
              DataBinding.FieldName = 'CONCILIADOS'
              Visible = False
              MinWidth = 50
              Options.Editing = False
              Width = 100
            end
            object CONCILIAR: TcxGridDBColumn
              Caption = 'Conciliar'
              DataBinding.FieldName = 'CONCILIAR'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.NullStyle = nssUnchecked
              MinWidth = 60
              VisibleForEditForm = bTrue
              Width = 60
            end
            object STATUS_CONCILIACION: TcxGridDBColumn
              Caption = 'Status Conciliaci'#243'n'
              Visible = False
            end
          end
          object cxGridLevel2: TcxGridLevel
            GridView = cxGridDBTableView1
          end
        end
        object DBEdit1: TDBEdit
          Left = 214
          Top = 267
          Width = 274
          Height = 21
          DataField = 'RS_NOMBRE'
          DataSource = dataSourceContribuyente
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          Visible = False
        end
        object cxGroupBox3: TcxGroupBox
          Left = 214
          Top = 304
          Align = alCustom
          Caption = ' Periodo: '
          TabOrder = 2
          Visible = False
          Height = 145
          Width = 405
          object PeriodoTipoLbl: TLabel
            Left = 29
            Top = 22
            Width = 24
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tipo:'
            Color = clWhite
            ParentColor = False
          end
          object Label6: TLabel
            Left = 41
            Top = 93
            Width = 12
            Height = 13
            Alignment = taRightJustify
            Caption = 'Al:'
            Color = clWhite
            ParentColor = False
          end
          object PeriodoNumeroLBL: TLabel
            Left = 13
            Top = 46
            Width = 40
            Height = 13
            Alignment = taRightJustify
            Caption = 'N'#250'mero:'
            Color = clWhite
            ParentColor = False
          end
          object Label8: TLabel
            Left = 34
            Top = 69
            Width = 19
            Height = 13
            Alignment = taRightJustify
            Caption = 'Del:'
            Color = clWhite
            ParentColor = False
          end
          object FechaInicial: TZetaTextBox
            Left = 56
            Top = 67
            Width = 200
            Height = 17
            AutoSize = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object FechaFinal: TZetaTextBox
            Left = 56
            Top = 91
            Width = 200
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object iNumeroNomina: TZetaTextBox
            Left = 56
            Top = 44
            Width = 65
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object sDescripcion: TZetaTextBox
            Left = 133
            Top = 43
            Width = 255
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object iTipoNomina: TZetaTextBox
            Left = 56
            Top = 20
            Width = 200
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object Label1: TLabel
            Left = 20
            Top = 116
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Caption = 'Status:'
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object PE_TIMBRO: TZetaDBTextBox
            Left = 56
            Top = 114
            Width = 201
            Height = 17
            AutoSize = False
            Caption = 'PE_TIMBRO'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'PE_TIMBRO'
            DataSource = DatasetPeriodo
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
      end
      object PanelFiltro: TPanel
        Left = 0
        Top = 0
        Width = 1228
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Ctl3D = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        object Label11: TcxLabel
          Left = 134
          Top = 8
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object txtAnio: TcxSpinEdit
          Left = 171
          Top = 6
          Hint = 'Seleccionar el a'#241'o'
          Enabled = False
          ParentShowHint = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taBottomJustify
          Properties.AssignedValues.DisplayFormat = True
          Properties.MaxValue = 2050.000000000000000000
          Properties.MinValue = 1899.000000000000000000
          Properties.SpinButtons.Position = sbpHorzLeftRight
          ShowHint = True
          TabOrder = 1
          Value = 1899
          Width = 90
        end
        object Label12: TcxLabel
          Left = 0
          Top = 8
          Caption = 'Mes:'
          Transparent = True
        end
        object zcboMes: TcxStateComboBox
          Left = 33
          Top = 6
          Hint = 'Seleccionar el mes'
          AutoSize = False
          ParentFont = False
          ParentShowHint = False
          Properties.DropDownListStyle = lsFixedList
          Properties.ImmediateDropDownWhenKeyPressed = False
          Properties.IncrementalSearch = False
          ShowHint = True
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 3
          ListaFija = lfMes13
          ListaVariable = lvPuesto
          EsconderVacios = False
          LlaveNumerica = True
          MaxItems = 10
          Offset = 1
          OnLookUp = zcboMesLookUp
          Height = 21
          Width = 95
        end
        object Panel3: TPanel
          Left = 1043
          Top = 0
          Width = 185
          Height = 41
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 4
          object cxButton1: TcxButton
            Left = 92
            Top = 5
            Width = 26
            Height = 26
            Hint = 'Marcar todos los periodos'
            Caption = 'Marcar Todos'
            OptionsImage.ImageIndex = 1
            OptionsImage.Images = ImageButtons
            OptionsImage.Margin = 1
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = btnSeleccionarTodosClick
          end
          object cxButton2: TcxButton
            Left = 124
            Top = 5
            Width = 26
            Height = 26
            Hint = 'Desmarcar todos los periodos'
            Caption = 'Desmarcar Todos'
            OptionsImage.ImageIndex = 0
            OptionsImage.Images = ImageButtons
            OptionsImage.Margin = 1
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = btnDesmarcarTodasClick
          end
          object cxButton3: TcxButton
            Left = 156
            Top = 5
            Width = 26
            Height = 26
            Hint = 'Refrescar'
            OptionsImage.Glyph.Data = {
              36090000424D3609000000000000360000002800000018000000180000000100
              2000000000000009000000000000000000000000000000000000D8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE4C3A5FFEEDB
              C9FFF7EEE5FFFEFCFBFFF9F2EBFFF3E4D7FFE7CAAFFFD9AB81FFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFDAAE85FFF0DFCFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F3EDFFE4C3A5FFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFF9F2EBFFFBF7F3FFFFFFFFFFFEFEFDFFEFDC
              CBFFE4C3A5FFDDB58FFFE0BB99FFEAD1B9FFFBF7F3FFFFFFFFFFFEFCFBFFE1BE
              9DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFFCF8F5FFFFFFFFFFFFFFFFFFECD6C1FFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF3E6D9FFFFFFFFFFFBF7
              F3FFDBB189FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFEFCFBFFF0DFCFFFDAAE
              85FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFF9F3EDFFFFFF
              FFFFE9CFB7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFDDB58FFFFDFAF7FFEDD8C5FFDCB28BFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE9CEB5FFFFFF
              FFFFF7EEE5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD9AD83FFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFE2BF
              9FFFE2BF9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFF0DFCFFFF5EADFFFE2BF9FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB
              81FFDAAE85FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFEDD8C5FFFFFFFFFFF3E6D9FFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFEDD7C3FFFBF7
              F3FFE2BF9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFDFBA97FFFFFFFFFFFFFFFFFFE4C5A7FFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF2E2D3FFFFFFFFFFFFFFFFFFFFFF
              FFFFDEB691FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFF2E3D5FFFFFFFFFFFDFBF9FFE4C5A7FFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE9CEB5FFFFFFFFFFFFFFFFFFFFFF
              FFFFDAAE85FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF6EBE1FFFFFFFFFFFFFFFFFFF6EB
              E1FFEAD2BBFFE7CAAFFFEAD2BBFFF5EADFFFFFFFFFFFFFFFFFFFFBF6F1FFFDFA
              F7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFEFDCCBFFFEFEFDFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEDBC9FFDAAE85FFDDB5
              8FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE0BB99FFEDD8
              C5FFF3E6D9FFF9F2EBFFF3E4D7FFEBD3BDFFE1BD9BFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
            OptionsImage.Margin = 1
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btREfrescaPeriodosClick
          end
        end
      end
    end
    object WizardConciliacionPagina2: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Se muestran los periodos seleccionados para comparar la informac' +
        'i'#243'n.'
      Header.Title = 'Conciliaci'#243'n de timbrado de n'#243'mina.'
      ParentFont = False
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 1228
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object txtMesPagina2: TZetaTextBox
          Left = 32
          Top = 8
          Width = 93
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object txtAnioPagina2: TZetaTextBox
          Left = 171
          Top = 8
          Width = 88
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object cxLabel1: TcxLabel
          Left = 134
          Top = 8
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object cxLabel6: TcxLabel
          Left = 0
          Top = 8
          Caption = 'Mes:'
          Transparent = True
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 41
        Width = 1228
        Height = 380
        Align = alClient
        BevelOuter = bvNone
        Caption = 'PanelGrid'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object GridPeriodosAProcesar: TZetaCXGrid
          Left = 0
          Top = 0
          Width = 1228
          Height = 380
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object GridPeriodosProcesarDBTableView: TcxGridDBTableView
            OnMouseMove = GridPeriodosProcesarDBTableViewMouseMove
            Navigator.Buttons.CustomButtons = <>
            FilterBox.CustomizeDialog = False
            OnCellClick = GridPeriodosProcesarDBTableViewCellClick
            DataController.DataSource = DSNominasXYearAProcesar
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = 'No hay Periodos'
            OptionsView.GroupByBox = False
            object ANIO: TcxGridDBColumn
              Caption = 'A'#241'o'
              DataBinding.FieldName = 'PE_YEAR'
              Options.Editing = False
            end
            object PE_TIPO: TcxGridDBColumn
              Caption = 'Tipo'
              DataBinding.FieldName = 'PE_TIPO'
              Options.Editing = False
            end
            object PERIODO_APLICA: TcxGridDBColumn
              Caption = 'Periodo'
              DataBinding.FieldName = 'PE_NUMERO'
              MinWidth = 60
              Options.Editing = False
              Width = 60
            end
            object cxGridDBColumn9: TcxGridDBColumn
              Caption = 'N'#243'mina'
              DataBinding.FieldName = 'NOMBRE_PERIODO'
              MinWidth = 100
              Options.Editing = False
              Width = 130
            end
            object cxGridDBColumn10: TcxGridDBColumn
              Caption = 'Estatus timbrado'
              DataBinding.FieldName = 'PE_TIMBRO'
              MinWidth = 100
              Options.Editing = False
              Width = 100
            end
            object cxGridDBColumn11: TcxGridDBColumn
              Caption = 'Total'
              DataBinding.FieldName = 'TOTAL'
              MinWidth = 60
              Options.Editing = False
            end
            object cxGridDBColumn12: TcxGridDBColumn
              Caption = 'Timbrado en TRESS'
              DataBinding.FieldName = 'TIMBRADOS_TRESS'
              MinWidth = 95
              Options.Editing = False
              Width = 119
            end
            object cxGridDBColumn14: TcxGridDBColumn
              Caption = 'Timbrado en SAT'
              DataBinding.FieldName = 'TIMBRADOS_SAT'
              MinWidth = 100
              Options.Editing = False
              Width = 108
            end
            object PORCENTAJE_CONCILIACION: TcxGridDBColumn
              Caption = 'Porcentaje de diferencias'
              DataBinding.FieldName = 'PORC_CONCILIADOS'
              MinWidth = 100
              Options.Editing = False
              Width = 143
            end
            object CONCILIADOS: TcxGridDBColumn
              Caption = 'Diferencias'
              DataBinding.FieldName = 'CONCILIADOS'
              MinWidth = 100
              Options.Editing = False
              Width = 100
            end
            object GridPeriodosProcesarDBTableViewColumn1: TcxGridDBColumn
              Caption = 'Proceso'
              DataBinding.FieldName = 'CONCILIA_PROCESO'
              PropertiesClassName = 'TcxProgressBarProperties'
              MinWidth = 100
              Options.Editing = False
            end
            object GridPeriodosProcesarDBTableViewColumn2: TcxGridDBColumn
              Caption = 'Resultado'
              DataBinding.FieldName = 'CONCILIA_STATUS'
              MinWidth = 100
              Options.Editing = False
            end
            object DETALLE_2: TcxGridDBColumn
              Caption = 'Detalle'
              DataBinding.FieldName = 'DETALLE'
              PropertiesClassName = 'TcxHyperLinkEditProperties'
              RepositoryItem = LinksDetalleConciliacion
              Options.Editing = False
            end
            object cxGridDBColumn15: TcxGridDBColumn
              Caption = 'Conciliar'
              DataBinding.FieldName = 'CONCILIAR'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.NullStyle = nssUnchecked
              Visible = False
              MinWidth = 60
              Options.Editing = False
              VisibleForEditForm = bTrue
              Width = 60
            end
          end
          object cxGridLevel3: TcxGridLevel
            GridView = GridPeriodosProcesarDBTableView
          end
        end
      end
    end
    object WizardConciliacionPagina3: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Tahoma'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Tahoma'
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'En caso de haber detectado diferencias, seleccion'#233' los periodos ' +
        'que ser'#225'n actualizados con la informaci'#243'n de PAC/SAT.'
      Header.Title = 'Conciliaci'#243'n de timbrado de n'#243'mina.'
      ParentFont = False
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1228
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object txtMesPagina3: TZetaTextBox
          Left = 32
          Top = 8
          Width = 93
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object txtAnioPagina3: TZetaTextBox
          Left = 171
          Top = 8
          Width = 88
          Height = 17
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object cxLabel2: TcxLabel
          Left = 134
          Top = 8
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object cxLabel3: TcxLabel
          Left = 0
          Top = 8
          Caption = 'Mes:'
          Transparent = True
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 41
        Width = 1228
        Height = 380
        Align = alClient
        BevelOuter = bvNone
        Caption = 'PanelGrid'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object GridPeriodosConciliados: TZetaCXGrid
          Left = 0
          Top = 0
          Width = 1228
          Height = 380
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object GridDBTablePeriodosConciliados: TcxGridDBTableView
            OnMouseMove = GridDBTablePeriodosConciliadosMouseMove
            Navigator.Buttons.CustomButtons = <>
            FilterBox.CustomizeDialog = False
            OnCellClick = GridDBTablePeriodosConciliadosCellClick
            DataController.DataSource = DSPeriodosConciliadosTimbrado
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = 'No hay Periodos'
            OptionsView.GroupByBox = False
            object PE_YEAR: TcxGridDBColumn
              Caption = 'A'#241'o'
              DataBinding.FieldName = 'PE_YEAR'
              MinWidth = 100
              Options.Editing = False
            end
            object PE_TIPO_APLICA: TcxGridDBColumn
              Caption = 'Tipo'
              DataBinding.FieldName = 'PE_TIPO'
              MinWidth = 100
              Options.Editing = False
            end
            object PE_PERIODO: TcxGridDBColumn
              Caption = 'Periodo'
              DataBinding.FieldName = 'PE_NUMERO'
              MinWidth = 80
              Options.Editing = False
              Width = 100
            end
            object NOMBRE_PERIODO: TcxGridDBColumn
              Caption = 'N'#243'mina'
              DataBinding.FieldName = 'NOMBRE_PERIODO'
              MinWidth = 100
              Options.Editing = False
              Width = 130
            end
            object cxGridDBColumn5: TcxGridDBColumn
              Caption = 'Estatus timbrado'
              DataBinding.FieldName = 'PE_TIMBRO'
              MinWidth = 80
              Options.Editing = False
              Width = 95
            end
            object cxGridDBColumn6: TcxGridDBColumn
              Caption = 'Total'
              DataBinding.FieldName = 'TOTAL'
              MinWidth = 80
              Options.Editing = False
            end
            object cxGridDBColumn7: TcxGridDBColumn
              Caption = 'Timbrado en TRESS'
              DataBinding.FieldName = 'TIMBRADOS_TRESS'
              MinWidth = 100
              Options.Editing = False
              Width = 119
            end
            object cxGridDBColumn18: TcxGridDBColumn
              Caption = 'Timbrado en SAT'
              DataBinding.FieldName = 'TIMBRADOS_SAT'
              MinWidth = 80
              Options.Editing = False
              Width = 108
            end
            object cxGridDBColumn17: TcxGridDBColumn
              Caption = 'Porcentaje de diferencias'
              DataBinding.FieldName = 'PORC_CONCILIADOS'
              MinWidth = 100
              Options.Editing = False
              Width = 143
            end
            object cxGridDBColumn16: TcxGridDBColumn
              Caption = 'Diferencias'
              DataBinding.FieldName = 'CONCILIADOS'
              MinWidth = 100
              Options.Editing = False
              Width = 100
            end
            object CONCILIA_PROCESO: TcxGridDBColumn
              Caption = 'Proceso'
              DataBinding.FieldName = 'CONCILIA_PROCESO'
              PropertiesClassName = 'TcxProgressBarProperties'
              Options.Editing = False
            end
            object CONCILIA_STATUS: TcxGridDBColumn
              Caption = 'Resultado'
              DataBinding.FieldName = 'CONCILIA_STATUS'
              Options.Editing = False
            end
            object DETALLE: TcxGridDBColumn
              Caption = 'Detalle'
              DataBinding.FieldName = 'DETALLE'
              PropertiesClassName = 'TcxHyperLinkEditProperties'
              RepositoryItem = LinksDetalleConciliacion
              MinWidth = 80
              Options.Editing = False
            end
            object APLICAR: TcxGridDBColumn
              Caption = 'Aplicar'
              DataBinding.FieldName = 'CONCILIAR'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.NullStyle = nssUnchecked
              MinWidth = 60
              VisibleForEditForm = bTrue
              Width = 60
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = GridDBTablePeriodosConciliados
          end
        end
      end
    end
  end
  object dataSourceTimbrar: TDataSource
    Left = 832
    Top = 80
  end
  object dataSourceContribuyente: TDataSource
    Left = 1144
    Top = 200
  end
  object DataSourceCuentas: TDataSource
    Left = 1040
    Top = 16
  end
  object DataSourceRecibos: TDataSource
    Left = 1120
    Top = 8
  end
  object DataSourceErrores: TDataSource
    OnDataChange = DataSourceErroresDataChange
    OnUpdateData = DataSourceErroresUpdateData
    Left = 960
    Top = 80
  end
  object DatasetPeriodo: TDataSource
    Left = 64
    Top = 240
  end
  object ActionListGrid: TActionList
    Left = 811
    Top = 219
    object Grid_AbrirArchivo: TAction
      Caption = 'Abrir archivo...'
    end
    object Grid_AbrirArchivoExplorer: TAction
      Caption = 'Grid_AbrirArchivoExplorer'
    end
  end
  object cxEditRepositoryBotonesGrid: TcxEditRepository
    Left = 792
    Top = 160
    object ButtonsImpreso: TcxEditRepositoryButtonItem
      Properties.Buttons = <
        item
          Action = Grid_AbrirArchivo
          Default = True
          Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A4050000000000000000000000000000000000004858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFC6CBEFFFEEEF
            FAFF8D97DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFFACB3E8FF8D97DFFFC6CBEFFF969FE2FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4E5DCEFFAFB6E9FF7C87DAFFF6F7
            FDFF5968D1FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF5160CEFF707DD7FF5160CEFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF8D97DFFFDFE2F6FFD4D8F3FFA1A9E5FF818CDCFF5F6DD2FF4858
            CCFF4858CCFF7682D9FFCED2F1FFD4D8F3FFBAC0ECFFCED2F1FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF6875D5FFFCFCFEFFB7BE
            EBFFD1D5F2FFF1F2FBFFF4F5FCFFE2E5F7FFFFFFFFFFC8CDF0FF98A1E2FFBDC3
            EDFF848FDDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFFC3C8EEFF969FE2FF4858CCFF4858CCFFBAC0ECFFD4D8F3FF7682
            D9FF7682D9FF5F6DD2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF7F8ADBFFCED2F1FF4858CCFF9EA7
            E4FFC8CDF0FF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5B
            CDFFEBEDF9FF818CDCFFE5E7F8FF5160CEFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFFB7BEEBFFF6F7FDFF7985DAFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF7F8ADBFFDADD
            F5FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF939CE1FFEEEFFAFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFFCED2F1FFCBD0F1FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFE8EA
            F9FFA6AEE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFFE8EAF9FFA1A9E5FF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FF7C87DAFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
          Kind = bkGlyph
          Width = 10
        end
        item
          Action = Grid_AbrirArchivoExplorer
          Caption = 'Ubicar documento en carpeta...'
          Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFDDF3
            FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3
            FAFFDDF3FAFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF3FC4EEFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF3FC4EEFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF6AD1
            F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6AD1F0FFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFF92DDF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF96DFF5FFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFE8F6FAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF53CA
            EFFF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCE
            F0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDDF3FAFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDDF3FAFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF7ED7F2FF7ED7
            F2FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC1EBF8FFDDF3FAFFDDF3FAFFDDF3
            FAFF6ED2F1FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          Kind = bkGlyph
          Width = 10
        end>
      Properties.ViewStyle = vsButtonsAutoWidth
    end
    object ButtonsXML: TcxEditRepositoryButtonItem
      Properties.Buttons = <
        item
          Action = Grid_AbrirArchivo
          Default = True
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000BF9670E1CBA0
            78F0CBA078F0CA9E74F0C99C72F0C99C72F0C99C72F0C99C72F0C99C72F0C99C
            72F0C99C72F0C99C72F0CA9E74F0CBA078F0CBA078F0BF9670E1D5A77DFBD8AA
            7FFFD8A97EFFD9AD84FFDAAF87FFDBB088FFDDB58FFFDCB38CFFDDB48EFFDBB1
            89FFDAAF87FFDAAF87FFD9AD84FFD8A97EFFD8AA7FFFD5A77DFBCBA078F0D8AA
            7FFFD3A170FFEEDAC7FFFDFAF7FFFCF8F4FFF5EADFFFF9F1EAFFF8EFE7FFFDFB
            F8FFFDFAF7FFFDFAF7FFEEDAC7FFD3A170FFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD3A06EFFF0DFD0FFFFFFFFFFFFFFFFFFEFDDCBFFF0DDCDFFEBD2BCFFEBD3
            BDFFFFFFFFFFFFFFFFFFF0DFD0FFD3A06EFFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD3A06FFFEFDDCBFFFFFFFFFFFFFFFFFFF9F2EBFFF7EEE5FFF9F0E9FFF2E3
            D4FFFFFFFFFFFFFFFFFFEFDDCCFFD3A06EFFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A171FFF0DECFFFF3E5D8FFE5C6A9FFEFDCCBFFEDD7C1FFE9CFB6FFEBD3
            BDFFE6C9ADFFF1E1D2FFF0DECDFFD4A172FFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A272FFF1E0D1FFE7CAAEFFD9AB80FFEEDBC9FFE7CAAFFFD4A273FFD39F
            6EFFD09A67FFE5C7ABFFF0DECDFFD4A274FFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A272FFF1E0D0FFE8CDB3FFDFB895FFFAF4EEFFF0DDCDFFD8A97FFFD7A7
            7BFFD5A375FFE8CCB2FFF0DECDFFD4A273FFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A272FFF1E0D0FFE9CFB7FFD6A577FFFFFFFFFFEAD1BAFFD39F6EFFD8A9
            7EFFD5A476FFE8CCB3FFF0DECDFFD4A273FFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A272FFF1E0D0FFEBD2BBFFD5A274FFE6C7AAFFDDB691FFD6A77AFFD5A4
            76FFCF9864FFE5C5A9FFF0DECDFFD4A273FFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A272FFF1E0D0FFEAD0B8FFD8AA7EFFEFDCC9FFE3C2A5FFD5A375FFDEB7
            92FFE2C0A1FFF0DECFFFF2E3D4FFD3A171FFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A272FFF1E0D0FFE9CEB4FFD6A577FFEDD8C5FFE1BE9EFFCF9661FFEFDD
            CCFFFFFFFFFFFFFFFFFFE5C7ABFFD4A373FFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A171FFF2E2D4FFEED9C6FFDAAE85FFE8CDB3FFE2BF9DFFD6A577FFF4E6
            DAFFFFFFFFFFE7CCB2FFD4A273FFD7AA7EFFD8AA7FFFCBA078F0CBA078F0D8AA
            7FFFD4A170FFEEDAC8FFFAF5F0FFF6EBE1FFF4E7DBFFF6EAE0FFF6ECE2FFFCF7
            F3FFE9CFB6FFD39F6FFFD7A87BFFD8AA80FFD8AA7FFFCBA078F0D5A77DFBD8AA
            7FFFD8A97EFFD9AD83FFDCB28CFFDEB793FFDEB692FFDEB692FFDEB793FFDBB1
            89FFD5A476FFD7A97DFFD8AB80FFD8AA7FFFD8AA7FFFD5A77DFBBF9670E1CBA0
            78F0CBA078F0CA9E74F0C99C72F0C99C72F0C99C72F0C99C72F0C99C72F0C99C
            72F0CB9F76F0CBA078F0CBA078F0CBA078F0CBA078F0BF9670E1}
          Kind = bkGlyph
          Width = 20
        end
        item
          Action = Grid_AbrirArchivoExplorer
          Caption = 'Ubicar archivo .zip en carpeta'
          Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFDDF3
            FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3
            FAFFDDF3FAFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF3FC4EEFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF3FC4EEFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF6AD1
            F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6AD1F0FFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFF92DDF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF96DFF5FFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFE8F6FAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF53CA
            EFFF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCE
            F0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDDF3FAFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDDF3FAFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF7ED7F2FF7ED7
            F2FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC1EBF8FFDDF3FAFFDDF3FAFFDDF3
            FAFF6ED2F1FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          Kind = bkGlyph
          Width = 20
        end>
      Properties.ViewStyle = vsButtonsAutoWidth
    end
    object progressbarConciliacion: TcxEditRepositoryProgressBar
    end
    object LinksDetalleConciliacion: TcxEditRepositoryHyperLinkItem
      Properties.SingleClick = True
    end
  end
  object DataSourceNominasXYear: TDataSource
    OnUpdateData = DataSourceNominasXYearUpdateData
    Left = 904
    Top = 80
  end
  object DSNominasXYearAProcesar: TDataSource
    OnUpdateData = DSNominasXYearAProcesarUpdateData
    Left = 1040
    Top = 80
  end
  object ImageButtons: TcxImageList
    Height = 24
    Width = 24
    FormatVersion = 1
    DesignInfo = 18809632
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4959CCFF4959CCFF4A5ACDFF4B5BCDFF4B5BCDFF4B5B
          CDFF4C5CCDFF4A59CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE8EAF9FFE7E9F9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EA
          F9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EA
          F9FFE8EAF9FFE6E8F8FF4D5DCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE6E8F8FFE9EBF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EA
          F9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EA
          F9FFE9EAF9FFE8EAF9FF4A5ACCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE6E9F9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE6E8F8FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE6E9F9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE8EAF9FF4959CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE8EAF9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE7E9F9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE8EAF9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE6E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE8EAF9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE7E9F9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE7E9F9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE7E9F9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE8EAF9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE7E9F9FFE8EAF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFE8EAF9FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE7E9F9FFEAECF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EA
          F9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EAF9FFE8EA
          F9FFE9EBF9FFE5E8F8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE5E8F8FFE7E9F9FFE7E9F9FFE7E9F9FFE7E9F9FFE7E9
          F9FFE6E9F9FFE6E9F9FFE6E8F8FFE5E7F8FFE5E7F8FFE6E8F8FFE7E9F9FFE7E9
          F9FFE7E9F8FFE7E9F9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9
          F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFAFEBCEFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDEED6FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF8CE2B8FF7CDDAEFF7CDDAEFF7CDDAEFF7CDD
          AEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFFBDEED6FFFFFFFFFFBDEED6FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
          93FF5BD59AFF50D293FF50D293FF50D293FFA8E9C9FFFFFFFFFFBDEED6FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF55D3
          96FFE4F8EEFFC8F1DDFF63D79FFF50D293FFA8E9C9FFFFFFFFFFBDEED6FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FFB2EB
          D0FFFFFFFFFFFFFFFFFFF4FCF8FF95E4BDFF7CDDAEFFEFFBF5FFBDEED6FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF76DCABFFFCFE
          FDFFF1FBF7FFB2EBD0FFFCFEFDFFFFFFFFFFD3F4E4FF71DAA7FF87E0B5FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF66D8A1FF50D293FF53D395FFDEF7EBFFFFFF
          FFFF8AE1B7FF50D293FF76DCABFFDEF7EBFFFFFFFFFFFAFEFCFFA0E6C4FF53D3
          95FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF66D8A1FF50D293FF97E4BFFFFFFFFFFFCBF2
          DFFF50D293FF50D293FF50D293FF53D395FFA5E8C8FFFAFEFCFFFFFFFFFFD9F5
          E7FF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF66D8A1FF50D293FF53D395FFA5E8C8FF63D7
          9FFF50D293FF50D293FF50D293FF50D293FF71DAA7FF71DAA7FFD6F4E6FFFFFF
          FFFFFCFEFDFFA8E9C9FF55D396FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFA8E9C9FFE9F9F1FF79DDACFF9AE5
          C1FFF4FCF8FFFFFFFFFFDBF6E9FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFF8CE2B8FF7CDDAEFF7CDDAEFF7CDDAEFF7CDD
          AEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFFBDEED6FFFFFFFFFFBDEED6FF50D2
          93FF63D79FFFCBF2DFFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBDEED6FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4
          E4FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4E4FFA2E7C6FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D292FF50D2
          92FF50D292FF50D292FF50D292FF50D292FF50D292FF50D292FF50D292FF4FCF
          90FB1A4530540000000000000000000000000000000000000000000000000000
          00000000000000000000040A070C399769B746B880DF46B880DF000000000000
          0000000000000000000000000000000000000D21172848BD83E74FCF90FB1A45
          3054000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000008140E1843B07AD750D292FF000000000000
          00000000000000000000000000000D21172848BD83E74FCF90FB1A4530540000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000008140E1843B07AD7000000000000
          0000000000000000000000000000173B29481E4F37601331223C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000008140E18000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000030705080A1A12200A1A12200A1A12200A1A12200A1A12200A1A12200A1A
          12200A1A12200A1A12200A1A12200A1A12200A1A12200A1A1220000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001435254050D292FF50D292FF50D292FF50D292FF50D292FF50D292FF50D2
          92FF50D292FF50D292FF50D292FF50D292FF50D292FF50D292FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001435254050D292FF50D292FF50D292FF50D292FF50D292FF50D292FF50D2
          92FF50D292FF50D292FF50D292FF50D292FF50D292FF50D292FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001435254050D292FF50D292FF50D292FF50D292FF50D292FF50D292FF50D2
          92FF50D292FF50D292FF50D292FF50D292FF50D292FF50D292FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000A1A12202869498028694980286949802869498028694980286949802869
          4980286949802869498028694980286949802869498028694980000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002D76528F000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000002D76528F50D292FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001C483258286949801C483258000000000000000000000000000000000000
          00000000000000000000000000002D76528F50D292FF50D292FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000008140E1843B07AD750D292FF235C40700000000000000000000000000000
          0000000000000000000008140E181E4F37601E4F37601E4F3760000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000008140E1843B07AD750D292FF235C407000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000046B880DF50D292FF50D292FF50D292FF50D2
          92FF50D292FF50D292FF50D292FF50D292FF50D292FF21553B68000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000348A60A73C9D6DBF3C9D6DBF3C9D6DBF3C9D
          6DBF3C9D6DBF3C9D6DBF4AC287EB50D292FF48BD83E70D211728000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001A4530544FCF90FB48BD83E70D21172800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001A4530544FCF90FB48BD83E70D2117280000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000917101C0A1A1220050D0910000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFBFECF9FFBFECF9FFBFECF9FFBFEC
          F9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFB3E8
          F8FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FFD7F3FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF0CB6EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF34C2EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF6CD3F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF68D1F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8FDDF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF93DEF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB7E9F9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FFC3EDFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE7F8FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF70D4F3FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
          F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF64D0F2FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
          F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF8BDC
          F5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF5
          FCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF74D5F3FF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF1CBA
          EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF64D0F2FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
          F7FF3CC4EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      end>
  end
  object ActionList1: TActionList
    Left = 800
    Top = 360
    object actTodos: TAction
      Caption = 'Seleccionar todos'
      Hint = 'Seleccionar todas las bases de datos'
    end
    object actNinguno: TAction
      Caption = 'Desmarcar todos'
      Hint = 'Desmarcar todas las bases de datos seleccionadas'
    end
    object actIniciar: TAction
      Caption = ' Iniciar'
      Hint = 'Iniciar la actualizacion de las bases de datos seleccionadas'
    end
    object actSalir: TAction
      Caption = 'Salir'
      Hint = 'Cerrar Pantalla y Salir'
    end
    object actActualizarTabla: TAction
      Caption = '      Refrescar'
      Hint = 'Refrescar la lista de bases de datos'
      OnExecute = actActualizarTablaExecute
    end
  end
  object DSPeriodosConciliadosTimbrado: TDataSource
    OnUpdateData = DSPeriodosConciliadosTimbradoUpdateData
    Left = 1133
    Top = 145
  end
end
