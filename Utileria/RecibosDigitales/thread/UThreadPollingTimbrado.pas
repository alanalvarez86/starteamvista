unit UThreadPollingTimbrado;

interface

uses      
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    ComObj,ActiveX, Db, DBClient,
    ZetaCommonClasses, UThreadStack, UInterfaces, UPeticionWS, Timbres, FTimbramexClasses, PDFMerge, WininetUtils;


type
    TThreadPollingTimbrado = class(TThread)
                      FProcesando, HayErrores, HuboCambios : Boolean;
private
       oStack : TThreadStack;
       oLogBitacora : ILogBitacora;
protected

         procedure Execute; override;

public
      constructor Create( Stack : TThreadStack; logBitacora : ILogBitacora );
      Destructor  Destroy; override;
      property Procesando: Boolean read FProcesando write FProcesando;
end;
implementation

uses ZBaseThreads,
     ZetaCommonTools,
     ZetaClientTools;

{ TThreadSync }
constructor TThreadPollingTimbrado.Create( Stack : TThreadStack ; logBitacora : ILogBitacora );
begin
     inherited Create(True);
     Self.oStack := Stack;
     oLogBitacora := logBitacora;
     FProcesando := False;
     HayErrores := False;
     HuboCambios := False;

end;

destructor TThreadPollingTimbrado.Destroy;
begin
  inherited;
end;


procedure TThreadPollingTimbrado.Execute;
var
   oPeticionWS :  TPeticionWS;
   crypt := TWSTimbradoCrypt.Create;   
begin
 try
  // OleCheck will raise an error if the call fails
  OleCheck(CoInitializeEx(NIL, COINIT_MULTITHREADED or COINIT_SPEED_OVER_MEMORY));
  crypt := TWSTimbradoCrypt.Create;   
  try    
    while FProcesando do
    begin         
         if ( oStack.Count > 0 ) then
         begin
              oPeticionWS := TPeticionWS(oStack.Peek);
              if ( oPeticionWS <> nil ) then
              begin
                   oPeticionWS := TPeticionWS(oStack.Pop ) ;
                   oPeticionWS.sRespuesta := '';
                   oPeticionWS.sError := '';
                   if ( oPeticionWS.tipoPeticion <> peticionMergePDF ) and ( oPeticionWS.tipoPeticion <> peticionDownloadFile )   then
                      oPeticionWS.cnxSOAP := GetTimbresFiscalesSoap(False,  oPeticionWS.sURL  );

                   try
                      if ( oPeticionWS.tipoPeticion = peticionTimbrado ) then
                      begin
						   oPeticionWS.sRespuesta := crypt.Desencriptar(  oPeticionWs.cnxSoap.Timbrar( crypt.Encriptar( opeticionWS.sPeticion  ) );			
                         //oPeticionWS.sRespuesta := 					  oPeticionWs.cnxSoap.Timbrar( 					opeticionWS.sPeticion 	 );
                      end;
                   except on Error: Exception do
                   begin
                           oPeticionWS.sError := TIMBRADO_SIN_CONECTIVIDAD + ' ' + Error.Message;
                   end;
                   end;
                   oPeticionWS.lProceso := FALSE ;
              end;
       end
       else
           DelayNPM(200);
    end;
      finally
	  FreeAndNil( crypt );    
  end;
except
end;

end;

end.
