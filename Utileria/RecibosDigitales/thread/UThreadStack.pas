                                                                                                                                                  unit UThreadStack;

interface
uses
  Windows, Contnrs;
type
  TStack = Contnrs.TObjectList;
  TThreadStack = class(TObject)
  private
    FStack: TObjectList;
    FLock :TRTLCriticalSection;
  public
    constructor Create();
    destructor Destroy;  override;
    function LockStack : TObjectList;
    procedure UnlockStack;
    function Count: Integer;
    function Push(AItem: Tobject): integer;
    function Pop: Tobject;
    function Peek: Tobject;
  end;

implementation

{ TThreadStack }

function TThreadStack.LockStack: TObjectList;
begin
  EnterCriticalSection(FLock);
  Result := FStack;
end;

function TThreadStack.Count: Integer;
begin
  EnterCriticalSection(FLock);
  try
    Result := FStack.Count;
  finally
    LeaveCriticalSection(FLock);
  end;
end;

constructor TThreadStack.Create();
begin
  inherited Create();
  InitializeCriticalSection(FLock);
  FStack := Contnrs.TobjectList.Create;
end;

destructor TThreadStack.Destroy;
begin
  DeleteCriticalSection(FLock);
  FStack.Free;
  inherited;
end;

function TThreadStack.Peek: Tobject;
begin
  EnterCriticalSection(FLock);
  try
    Result := FStack.First;
  finally
    LeaveCriticalSection(FLock);
  end;
end;

function TThreadStack.Pop: Tobject;
begin
  EnterCriticalSection(FLock);
  try

    if FStack.Count > 0 then
    begin
        Result := FStack.First;
        FStack.Extract( Result );
    end
    else
        Result := nil; 
  finally
    LeaveCriticalSection(FLock);
  end;
end;

function TThreadStack.Push(AItem: Tobject): integer;
begin
  EnterCriticalSection(FLock);
  try
    Result := FStack.Add( AItem );
  finally
    LeaveCriticalSection(FLock);
  end;
end;

procedure TThreadStack.UnlockStack;
begin
  LeaveCriticalSection(FLock);
end;

end.


