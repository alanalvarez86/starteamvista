unit UManejadorPeticiones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UThreadStack, Contnrs, UThreadPolling, UPeticionWS,UInterfaces;





type
  TManejadorPeticiones = class(TObject)
  private
        oStack : TThreadStack;
        oPolling : TThreadPolling;
        oLogBitacora : ILogBitacora;
        procedure EscribeArchivo;
        procedure LeeArchivo;

  public
        constructor Create(logBitacora : ILogBitacora) ;
        destructor Destroy; override;
        procedure AgregarPeticionWS( oPeticionWS : TPeticionWS );
        function  PopPeticionWS( var oPeticionWS : TPeticionWS ) : integer;
        procedure  Stop(); 

  end;

const
     K_ARCHIVO_TEMP = 'TEMP.DAT';


implementation

{ TManejadorPeticiones }

procedure TManejadorPeticiones.AgregarPeticionWS(oPeticionWS : TPeticionWS);
begin
     oStack.Push( oPeticionWS );
     //EscribeArchivo;
end;

constructor TManejadorPeticiones.Create(logBitacora : ILogBitacora);
begin
  oStack := TThreadStack.Create;
  //LeeArchivo;
  oLogBitacora := logBitacora;
  oPolling := TThreadPolling.Create(oStack, logBitacora);
  oPolling.Procesando := True;
  oPolling.Resume;

end;

destructor TManejadorPeticiones.Destroy;
begin
  oPolling.Procesando := False;
  oPolling.WaitFor;
  //EscribeArchivo;
  freeAndNil(oPolling);
   freeAndNil(oStack);
  inherited;
end;

procedure TManejadorPeticiones.LeeArchivo;
var
  F: TextFile;
  S: string;
  oSL : TStringList;
  index : integer;
  oChecada : TPeticionWS;
begin

end;

procedure TManejadorPeticiones.EscribeArchivo;
var
  F: TextFile;
  S: string;
  i: Integer;
  sList : TStringList;
  lockedStack : TObjectList;
  Checada : TPeticionWS;
begin

     try
      sList := TStringList.Create;

      //Primero lo escibimos en memoria
      lockedStack := oStack.LockStack;


      for i:=0 to lockedStack.Count-1 do
      begin
           Checada := TPeticionWS( lockedStack.Items[i] );
           sList.Add( Checada.ToString  );
      end;

      oStack.UnlockStack;

     //Despues al archivo

      AssignFile(F, K_ARCHIVO_TEMP);
      ReWrite(F);

      for i:=0 to sList.Count-1 do
      begin
           WriteLN(F, sList[i] ) ;
      end;
      CloseFile(F);

      FreeAndNil( sList );
     except
       on E: Exception do
       begin
           oLogBitacora.EscribirBitacoraError('ERROR AL SALVAR ARCHIVO TEMPORAL');
       end;
     end

end;

function  TManejadorPeticiones.PopPeticionWS( var oPeticionWS : TPeticionWS ) : integer;
begin
     oPeticionWS := TPeticionWS( oStack.Pop ) ;

end;


procedure TManejadorPeticiones.Stop;
begin
     oPolling.FProcesando := FALSE;
     oPolling.Stop;
end;

end.
