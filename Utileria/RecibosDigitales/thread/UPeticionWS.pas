unit UPeticionWS;


interface

uses  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Contnrs, Timbres, WininetUtils, FTimbramexClasses, ZetaCommonClasses;


type

   eTipoPeticionWS = ( peticionTimbrado, peticionCancelarTimbrado, peticionTomarRecibosPDF, peticionTomarRecibosXML, peticionMergePDF, peticionDownloadFile, peticionCancelarTimbradoAsincrono, peticionCancelarTimbradoAsincronoStatus, peticionConciliacionNominaTimbrado, peticionAplicacionConciliacionNominaTimbrado, peticionAuditoriaConceptosTimbrado );


  TPeticionWS = class(TObject)
  public
  {SourceFile, LocalFile, func, sFileName}

        tipoPeticion : eTipoPeticionWS;
        cnxSoap : TimbresFiscalesSoap;
        dFechaHora : TDateTime;
        iEmpleado  : integer;
        sPeticion, sRespuesta , sError, sURL: string;
        sArchivoDestino, sArchivoOrigen, sLocalFile : string;
        slGrupoPDF : TStringList;
        lResultadoMerge,lResultadoDownload : boolean;
        lProceso : boolean;
        lCancelar : boolean;
        ACallback: TProgressCallback;
        ACallbackMensaje: TProgressPaqueteCallback;
        ACallbackActualizacion: TProgressConciliacionTimbradoCallback;
        wsConfig : TWebServiceConfig;
        oParamsConciliacion: TZetaParams;
        function ToString : string;
  end;

implementation


{ TPeticionWS }
function TPeticionWS.ToString: string;
var
   sFecha : string;
begin
     sFecha := FormatDateTime('c', Self.dFechaHora );
     Result := Format( 'ID=%s', [sFecha] );
end;


end.


