unit UManejadorPeticionesTimbrado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UThreadStack, Contnrs, UThreadPollingTimbrado, UPeticionWS,UInterfaces;





type
  TManejadorPeticionesTimbrado = class(TObject)
  private
        oStack : TThreadStack;
        oPolling : TThreadPollingTimbrado;
        oLogBitacora : ILogBitacora;
        procedure EscribeArchivo;
        procedure LeeArchivo;

  public
        constructor Create(logBitacora : ILogBitacora) ;
        destructor Destroy; override;
        procedure AgregarPeticionWS( oPeticionWS : TPeticionWS );
        function  PopPeticionWS( var oPeticionWS : TPeticionWS ) : integer;

  end;

const
     K_ARCHIVO_TEMP = 'TEMP.DAT';


implementation

{ TManejadorPeticiones }

procedure TManejadorPeticionesTimbrado.AgregarPeticionWS(oPeticionWS : TPeticionWS);
begin
     oStack.Push( oPeticionWS );
     //EscribeArchivo;
end;

constructor TManejadorPeticionesTimbrado.Create(logBitacora : ILogBitacora);
begin
  oStack := TThreadStack.Create;
  //LeeArchivo;
  oLogBitacora := logBitacora;
  oPolling := TThreadPollingTimbrado.Create(oStack, logBitacora);
  oPolling.Procesando := True;
  oPolling.Resume;

end;

destructor TManejadorPeticionesTimbrado.Destroy;
begin
  oPolling.Procesando := False;
  oPolling.WaitFor;
  //EscribeArchivo;
  freeAndNil(oPolling);
   freeAndNil(oStack);
  inherited;
end;

procedure TManejadorPeticionesTimbrado.LeeArchivo;
var
  F: TextFile;
  S: string;
  oSL : TStringList;
  index : integer;
  oChecada : TPeticionWS;
begin

 { try
      // Carga checadas pendientes
      if FileExists(K_ARCHIVO_TEMP) then begin
        AssignFile(F, K_ARCHIVO_TEMP);
        Reset(F);

        while not Eof(F) do
        begin
             ReadLn(F, S);
             oSL := TStringList.Create;
             oSL.CommaText := S;
             if osl.Values['ID'] <> '' then
             begin
                 //ID=%d,TIMESTAMP=%g,LINXID=%s,DIGITO=%s,LETRA=%s,FECHAHORA=%s
                  oChecada := TPeticionWS.Create;
                  oChecada.iEmpleado := StrToIntDef( osl.Values['ID'], 0 );
                  oChecada.dFechaHora := StrToFloatDef( osl.Values['TIMESTAMP'], 0 );
                  oChecada.sLinxID := osl.Values['LINXID'];
                  oChecada.sDigitoEmpresa := osl.Values['DIGITO'];
                  oChecada.sLetraCred := osl.Values['LETRA'];

                  if ( oChecada.iEmpleado > 0 ) and ( oChecada.dFechaHora > 0 ) then
                  begin
                       oStack.Push( oChecada );
                  end
                  else
                  begin
                       FreeAndNil( oChecada );
                  end;

             end;

             FreeAndNil(oSL);
         end;

        CloseFile(F);
      end
  except
       on E: Exception do
       begin
           oLogBitacora.EscribirBitacoraError('ERROR AL CARGAR ARCHIVO TEMPORAL');
       end;
  end   }
end;

procedure TManejadorPeticionesTimbrado.EscribeArchivo;
var
  F: TextFile;
  S: string;
  i: Integer;
  sList : TStringList;
  lockedStack : TObjectList;
  Checada : TPeticionWS;
begin

     try
      sList := TStringList.Create;

      //Primero lo escibimos en memoria
      lockedStack := oStack.LockStack;


      for i:=0 to lockedStack.Count-1 do
      begin
           Checada := TPeticionWS( lockedStack.Items[i] );
           sList.Add( Checada.ToString  );
      end;

      oStack.UnlockStack;

     //Despues al archivo

      AssignFile(F, K_ARCHIVO_TEMP);
      ReWrite(F);

      for i:=0 to sList.Count-1 do
      begin
           WriteLN(F, sList[i] ) ;
      end;
      CloseFile(F);

      FreeAndNil( sList );
     except
       on E: Exception do
       begin
           oLogBitacora.EscribirBitacoraError('ERROR AL SALVAR ARCHIVO TEMPORAL');
       end;
     end

end;

function  TManejadorPeticionesTimbrado.PopPeticionWS( var oPeticionWS : TPeticionWS ) : integer;
begin
     oPeticionWS := TPeticionWS( oStack.Pop ) ;

end;





{
   oChecada : TPeticionWS;
   i : integer;
begin
 for i:=1 to 10 do
     begin
     oManejadorBio.PopChecada( oChecada );

     if ( oChecada <> nil ) then
        freeAndNil( oChecada );
     end;

end;}


end.
