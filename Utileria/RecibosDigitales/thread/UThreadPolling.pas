unit UThreadPolling;

interface

uses      
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    ComObj,ActiveX, Db, DBClient,
    ZetaCommonClasses, UThreadStack, UInterfaces, UPeticionWS, Timbres, FTimbramexHelper, FTimbramexClasses, PDFMerge, WininetUtils,ZetaClientDataSet, DInterfase;


type
    TThreadPolling = class(TThread)
                      FProcesando, HayErrores, HuboCambios : Boolean;
private
       oStack : TThreadStack;
       oLogBitacora : ILogBitacora;
       oPeticionWSActual: TPeticionWS;
       FProgressConciliacionTimbradoCallback: TProgressConciliacionTimbradoCallback;

       //Conciliacion Timbrado
       FMensaje : string;
       FAvance: Integer;
       FDataSetUpdate: TZetaClientDataSet;
       FParams: TZetaParams;
       FHayErrorConciliacion: Boolean;
       //Fin
       procedure ProgressConciliacionTimbradoCallback;
protected

         procedure Execute; override;

public
      constructor Create( Stack : TThreadStack; logBitacora : ILogBitacora );
      Destructor  Destroy; override;
      property Procesando: Boolean read FProcesando write FProcesando;
      procedure Stop;
end;
implementation

uses ZBaseThreads,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaRegistryCliente;

{ TThreadSync }
constructor TThreadPolling.Create( Stack : TThreadStack ; logBitacora : ILogBitacora );
begin
     inherited Create(True);
     Self.oStack := Stack;
     oLogBitacora := logBitacora;
     FProcesando := False;
     HayErrores := False;
     HuboCambios := False;
     oPeticionWSActual := NIL;

end;

destructor TThreadPolling.Destroy;
begin
  inherited;
end;


procedure TThreadPolling.Execute;
var
   oPeticionWS :  TPeticionWS;
   FPDFMerge : TPDFMerge;
   oCryptExecute : TWSTimbradoCrypt;

   //Conciliacion de timbrado de nomina
   respuestaConciacionEmpleadosNube : TRespuestaConciliacionTimbradoNomina;
   XMl, Error: WideString;
   sPeticion, sRespuesta: String;
   sMensajePeriodoConciliado: String;

   function MergePDF(  slGrupoPDF : TStringList;  sArchivoDestino : string )  : Boolean;
   begin
      FPDFMerge := TPDFMerge.Create;

      try
         FPDFMerge.SetPathOrigen('');
         FPDFMerge.SetPathDestino(  ExtractFilePath( slGrupoPDF[0] ) );
         Result := FPDFMerge.TestMerge(slGrupoPDF, ExtractFileName( sArchivoDestino ) );
      finally
          FreeAndNil( FPDFMerge );
      end;
   end;

   function GetTimbradoPaqueteIntentos: integer;
   var
      registro : TZetaClientRegistry;
   begin
        registro :=  TZetaClientRegistry.Create;
        Result :=  registro.TimbradoPaqueteIntentos;
        FreeAndNil(registro);
   end;

   function GetTimbradoPaqueteEspera: integer;
   var
      registro : TZetaClientRegistry;
   begin
        registro :=  TZetaClientRegistry.Create;
        Result :=  registro.TimbradoPaqueteEspera;
        FreeAndNil(registro);
   end;

   function PeticionTimbradoWS( var oPeticionWS : TPeticionWS; const ACallbackMensaje: TProgressPaqueteCallback = nil ): string;
   const
        K_MENSAJE_TIMBRADO = 'Reintento de timbrado %d en %s segundos...';
        K_MENSAJE_ENVIANDO = 'Enviando Recibos...';
   var
      iTimbradoIntentos: integer;
      iTimbradoPaqueteIntentos: integer;
      iTimbradoPaqueteEspera: integer;
      oPeticionMensaje : TPeticionWS;
      dSegundos : Real;
      sMensaje: string;
      oCrypt : TWSTimbradoCrypt;
	  
   begin
        oCrypt := TWSTimbradoCrypt.Create;
        try
           iTimbradoIntentos := 0;
           iTimbradoPaqueteIntentos := GetTimbradoPaqueteIntentos;
           iTimbradoPaqueteEspera := GetTimbradoPaqueteEspera;
           try
              dSegundos := iTimbradoPaqueteEspera/1000;
           except
                 on Error : Exception do
                    dSegundos := 0;
           end;
           if not( oPeticionWS.lCancelar ) then
           begin			
			    Result := oCrypt.Desencriptar(  oPeticionWs.cnxSoap.Timbrar( oCrypt.Encriptar( opeticionWS.sPeticion ) ) ) ;
             // Result := 					   oPeticionWs.cnxSoap.Timbrar(                  opeticionWS.sPeticion   );
           end
           else
           begin
                Result := '';
           end;
        except on Error: Exception do
        begin
             Result := '';
             for iTimbradoIntentos := 0 to iTimbradoPaqueteIntentos -1 do
             begin
                  try
                     FTimbramexClasses.FAplicarTimeoutTimbrado := true;
                     if Assigned(ACallbackMensaje) then
                     begin
                          sMensaje := Format ( K_MENSAJE_TIMBRADO, [ iTimbradoIntentos + 1 , FormatFloat('0', dSegundos ) ] );
                          ACallbackMensaje( sMensaje  );
                     end;
                     if (oPeticionWS.lCancelar) then break;
                     DelayNPM( iTimbradoPaqueteEspera );
                     if Assigned(ACallbackMensaje) then
                     begin
                       ACallbackMensaje(  K_MENSAJE_ENVIANDO  );
                     end;
					 
                      oPeticionWS.sRespuesta := oCrypt.Desencriptar(  oPeticionWs.cnxSoap.Timbrar( oCrypt.Encriptar( opeticionWS.sPeticion ) ) );
                   //oPeticionWS.sRespuesta :=                      oPeticionWs.cnxSoap.Timbrar( 		          opeticionWS.sPeticion   );
                     if ( oPeticionWS.sRespuesta <> '' ) then
                     begin
                          Result := oPeticionWS.sRespuesta;
                          break;
                     end
                   except on Error: Exception do
                   begin
                        oPeticionWS.sError := TIMBRADO_SIN_CONECTIVIDAD + ' ' + Error.Message;
                   end;
                   end;
             end;
             if Assigned(ACallbackMensaje) then
             begin
               ACallbackMensaje(  K_MENSAJE_ENVIANDO  );
             end;
        end;
        end;
		FreeAndNil( oCrypt );
   end;

   //Conciliacion de Timbrado de nomina
   function ConsultarDiferenciasPeriodosTimbradoConciliacion( oParams: TZetaParams; var sMensajeError: String ): Boolean;
   begin
        Result := True;
        FDataSetUpdate := TZetaClientDataSet.Create(nil);
        try
           try
              FDataSetUpdate.Data := dmInterfase.GetDatosUpdateConciliacionTimbrado(oParams);   //ServerNominaTimbrado.ObtenerPeriodosAConciliarTimbrado(dmCliente.Empresa,dmCliente.AnioConciliacionTimbrado, dmCliente.MesConciliacionTimbrado, dmCliente.RazonSocialConciliacionTimbrado ); //ServerConsultas.GetConsultaDashletsTablero( oParams.ParamByName( 'EMPRESA' ).Value, oParametros.VarValues, ErrroCount );
           except
                 on Error: Exception do
                 begin
                      Result := False;
                      sMensajeError := Error.Message;
                 end;
           end;
        finally
               begin
               end;
        end;
   end;

   //FIN
   
begin
 try
  // OleCheck will raise an error if the call fails
  OleCheck(CoInitializeEx(NIL, COINIT_MULTITHREADED or COINIT_SPEED_OVER_MEMORY));
  oCryptExecute := TWSTimbradoCrypt.Create;
  try
    // Do work here

    while FProcesando do
    begin
         //oLogBitacora.EscribirBitacoraLog('Procesando checadas..');
         if ( oStack.Count > 0 ) then
         begin

              oPeticionWS := TPeticionWS(oStack.Peek);
              if ( oPeticionWS <> nil ) then
              begin
                   oPeticionWS := TPeticionWS(oStack.Pop ) ;
                   oPeticionWS.sRespuesta := '';
                   oPeticionWS.sError := '';
                   oPeticionWS.wsConfig := nil;
                   oPeticionWSActual := oPeticionWS;
                   if ( oPeticionWS.tipoPeticion <> peticionMergePDF ) and ( oPeticionWS.tipoPeticion <> peticionDownloadFile )   then
                      oPeticionWS.cnxSOAP := GetTimbresFiscalesSoap2(False,  oPeticionWS.sURL  , oPeticionWS.wsConfig );

                   try
                      if ( oPeticionWS.tipoPeticion = peticionTimbrado ) then
                      begin
                           with oPeticionWS do
                           sRespuesta := PeticionTimbradoWS( oPeticionWS, ACallbackMensaje );
                      end;

                      if ( oPeticionWS.tipoPeticion = peticionCancelarTimbrado ) then
                      begin
						   oPeticionWS.sRespuesta := oCryptExecute.Desencriptar(  oPeticionWs.cnxSoap.CancelarTimbre( oCryptExecute.Encriptar( opeticionWS.sPeticion  ) ) );
                         //oPeticionWS.sRespuesta := 					  oPeticionWs.cnxSoap.CancelarTimbre( 				   opeticionWS.sPeticion    );
                      end;

                      //Cancelar Timbrado Asincrono
                      if ( oPeticionWS.tipoPeticion = peticionCancelarTimbradoAsincrono ) then
                      begin
						   oPeticionWS.sRespuesta := oCryptExecute.Desencriptar(  oPeticionWs.cnxSoap.CancelarTimbreInit( oCryptExecute.Encriptar( opeticionWS.sPeticion  ) ));
                         //oPeticionWS.sRespuesta := 					  oPeticionWs.cnxSoap.CancelarTimbreInit( 				   opeticionWS.sPeticion 	);
                      end;
                      //Status peticionCancelarTimbradoAsincronoStatus
                      if ( oPeticionWS.tipoPeticion = peticionCancelarTimbradoAsincronoStatus ) then
                      begin
						   oPeticionWS.sRespuesta := oCryptExecute.Desencriptar(  oPeticionWs.cnxSoap.CancelarTimbreStatus( oCryptExecute.Encriptar( opeticionWS.sPeticion  ) ));
                         //oPeticionWS.sRespuesta := 					  oPeticionWs.cnxSoap.CancelarTimbreStatus( 				 opeticionWS.sPeticion 	  );
                      end;

                      if ( oPeticionWS.tipoPeticion = peticionTomarRecibosPDF ) then
                      begin
						   oPeticionWS.sRespuesta := oCryptExecute.Desencriptar(  oPeticionWs.cnxSoap.PeriodoGetRecibosImpresos( oCryptExecute.Encriptar( opeticionWS.sPeticion  ) ));
                      //   oPeticionWS.sRespuesta := 					  oPeticionWs.cnxSoap.PeriodoGetRecibosImpresos( 				  opeticionWS.sPeticion    );
                      end;

                      if ( oPeticionWS.tipoPeticion = peticionTomarRecibosXML ) then
                      begin
						  oPeticionWS.sRespuesta := oCryptExecute.Desencriptar(  oPeticionWs.cnxSoap.PeriodoGetRecibosXML( oCryptExecute.Encriptar( opeticionWS.sPeticion  ) ));
                        //oPeticionWS.sRespuesta := 					 oPeticionWs.cnxSoap.PeriodoGetRecibosXML( 					opeticionWS.sPeticion 	 );
                      end;

                      if ( oPeticionWS.tipoPeticion = peticionMergePDF ) then
                      begin
                           oPeticionWS.lResultadoMerge := MergePDF( oPeticionWS.slGrupoPDF,  oPeticionWS.sArchivoDestino );
                      end;

                      if ( oPeticionWS.tipoPeticion = peticionDownloadFile  ) then
                      begin
                           with oPeticionWS do
                                lResultadoDownload := DownloadFileWininet( sArchivoOrigen, sLocalFile, ACallback, sArchivoDestino );
                      end;

                      if ( oPeticionWS.tipoPeticion = peticionAuditoriaConceptosTimbrado ) then
                      begin
                           oPeticionWS.sRespuesta := oCryptExecute.Desencriptar(  oPeticionWs.cnxSoap.AuditarConceptosTRESS( oCryptExecute.Encriptar( opeticionWS.sPeticion  ) ));
                      end;

                      if ( oPeticionWS.tipoPeticion = peticionConciliacionNominaTimbrado ) then //Conciliacion de Timbrado de Nomina
                      begin
                           FProgressConciliacionTimbradoCallback := oPeticionWS.ACallbackActualizacion;
                           FParams := TZetaParams.Create;
                           FParams := oPeticionWS.oParamsConciliacion;
                           FParams.AddBoolean('ERROR_CONCILIACION', False);

                           if not ( oPeticionWS.lCancelar ) then
                           begin
                                dmInterfase.ObtenerXMLEmpleadosAConciliarTimbrado( FParams.ParamByName( 'EMPRESA' ).Value, FParams.VarValues, XMl, Error);
                                FAvance := 30;
                                FMensaje   := 'Generando petción XMl...';

                                Synchronize(ProgressConciliacionTimbradoCallback);

                                sPeticion := dmInterfase.GetPeticionTimbradoConciliacionNube(FParams);
                                FAvance := 60;
                                FMensaje   := 'Envio de petición sistema de timbrado...';
                                Synchronize(ProgressConciliacionTimbradoCallback);

                                respuestaConciacionEmpleadosNube := FTimbramexHelper.ConsultarEmpleadosAConciliarTimbradoNomina(sPeticion,FParams);
                                if respuestaConciacionEmpleadosNube.lHayRespuesta then
                                begin
                                     sRespuesta := respuestaConciacionEmpleadosNube.respuestaXML;
                                     if ( respuestaConciacionEmpleadosNube.respuesta.Resultado.Errores ) then //Mostrar Errores de Bitacora
                                     begin
                                          FAvance := 100;
                                          FMensaje := respuestaConciacionEmpleadosNube.respuesta.Bitacora.Eventos.Text;
                                          FParams.FindParam('ERROR_CONCILIACION').AsBoolean := True;
                                          Synchronize(ProgressConciliacionTimbradoCallback);
                                     end
                                     else if not ( respuestaConciacionEmpleadosNube.respuesta.Resultado.ErroresDS.Eof ) then //Posibles errores mostrar
                                     begin
                                          FAvance := 100;
                                          FMensaje := 'Error respuesta sistema de timbrado...';
                                          with  respuestaConciacionEmpleadosNube.respuesta.Resultado.ErroresDS do
                                          begin
                                               First;
                                               while not EOF do
                                               begin
                                                    FMensaje := FieldByName('DETALLE').AsString;
                                                    if StrVacio( FMensaje ) then
                                                       FMensaje := FieldByName('DESCRIPCION').AsString;
                                                    FParams.FindParam('ERROR_CONCILIACION').AsBoolean := True;
                                                    Break;
                                               end;
                                          end;
                                          Synchronize(ProgressConciliacionTimbradoCallback);
                                     end
                                     else
                                     begin
                                          //En caso de exito la peticion a la nube
                                          sRespuesta  := stringReplace( sRespuesta, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '', []);
                                          sRespuesta := stringReplace( sRespuesta, '<?xml version="1.0" standalone="yes"?>', '', []);
                                          sRespuesta := stringReplace( sRespuesta, '<?xml version="1.0" encoding="iso-8859-1"?>', '', []);
                                          FParams.AddString('XML_CONCILIACION_NUBE', sRespuesta);
                                          FAvance := 80;
                                          FMensaje := 'Conciliando periodo de nómina...';
                                          Synchronize(ProgressConciliacionTimbradoCallback);

                                          FAvance := 100;
                                          if ( dmInterfase.IniciarConciliacionTimbradoPeriodos(FParams) ) then
                                          begin
                                               FMensaje   := 'Conciliación terminada.';
                                               //Consultar las diferencias.
                                               if not ( ConsultarDiferenciasPeriodosTimbradoConciliacion(FParams, sMensajePeriodoConciliado) ) then
                                               begin
                                                    FMensaje := sMensajePeriodoConciliado;
                                                    FParams.FindParam('ERROR_CONCILIACION').AsBoolean := True;
                                               end;
                                          end
                                          else
                                          begin
                                               FMensaje := FParams.ParamByName( 'ERRORES_MENSAJE' ).AsString;
                                               FParams.FindParam('ERROR_CONCILIACION').AsBoolean := True;
                                          end;
                                          Synchronize(ProgressConciliacionTimbradoCallback);
                                     end;
                                end
                                else
                                begin
                                     FAvance := 100;
                                     FParams.FindParam('ERROR_CONCILIACION').AsBoolean := True;
                                     FMensaje := Trim( respuestaConciacionEmpleadosNube.sMensaje );
                                     Synchronize(ProgressConciliacionTimbradoCallback);
                                end;
                           end
                           else
                           begin
                                FMensaje   := 'Cancelando conciliación...';
                                Synchronize(ProgressConciliacionTimbradoCallback);
                           end;
                       //FIN
                      end;
                      //Aplicacion de conciliacion de timbrado de Nomina
                      if ( oPeticionWS.tipoPeticion = peticionAplicacionConciliacionNominaTimbrado ) then //Conciliacion de Timbrado de Nomina
                      begin
                           //Aplicar conciliacion de Timbrado de Nomina
                           FProgressConciliacionTimbradoCallback := oPeticionWS.ACallbackActualizacion;
                           FParams := TZetaParams.Create;
                           FParams := oPeticionWS.oParamsConciliacion;
                           FParams.AddBoolean('ERROR_CONCILIACION', False);

                           if not ( oPeticionWS.lCancelar ) then
                           begin
                                FAvance := 40;
                                FMensaje := 'Aplicando conciliación...';
                                Synchronize(ProgressConciliacionTimbradoCallback);

                                FAvance := 100;
                                if ( dmInterfase.AplicarConciliacionTimbradoPeriodos(FParams) ) then
                                begin
                                     FMensaje := 'Aplicación de conciliación terminada.';

                                     //Consultar las diferencias.
                                     if not ( ConsultarDiferenciasPeriodosTimbradoConciliacion(FParams, sMensajePeriodoConciliado) ) then
                                     begin
                                          FMensaje := sMensajePeriodoConciliado;
                                          FParams.FindParam('ERROR_CONCILIACION').AsBoolean := True;
                                     end;
                                end
                                else
                                begin
                                     FMensaje := FParams.ParamByName( 'ERRORES_MENSAJE' ).AsString;
                                     FParams.FindParam('ERROR_CONCILIACION').AsBoolean := True;
                                end;
                                Synchronize(ProgressConciliacionTimbradoCallback);
                           end
                           else
                           begin
                                FMensaje   := 'Cancelando aplicación de conciliación...';
                                Synchronize(ProgressConciliacionTimbradoCallback);
                           end;
                      end;
                   except on Error: Exception do
                   begin
                           oPeticionWS.sError := TIMBRADO_SIN_CONECTIVIDAD + ' ' + Error.Message;
                   end;
                   end;

                   oPeticionWS.lProceso := FALSE ;
                   oPeticionWSActual := NIL;

              end;

       end
       else
           DelayNPM(200);
    end;

      finally
		FreeAndNil ( oCryptExecute  );
   // CoUninitialize;
  end;
except
  // We failed, do something
end;

end;

procedure TThreadPolling.ProgressConciliacionTimbradoCallback;
begin
     if Assigned( FProgressConciliacionTimbradoCallback ) then FProgressConciliacionTimbradoCallback( FMensaje, FAvance, FDataSetUpdate, FParams );
end;

procedure TThreadPolling.Stop;
var
   oPeticionWS : TPeticionWS;
begin
     oPeticionWS :=oPeticionWSActual;

     if ( oPeticionWS <> nil ) and ( oPeticionWS.wsConfig <> nil ) then
     begin
          if Assigned( oPeticionWS.wsConfig.FHTTPRIO ) then FreeAndNil( oPeticionWS.wsConfig.FHTTPRIO ) ;
     end;

     FProcesando := FALSE;
end;

end.

