unit FTimbramexClasses;

interface

uses
  XMLDom, XMLIntf, MSXMLDom, XMLDoc, XSLProd, SysUtils, Classes,
  ZetaCommonClasses,IdCoder, IdCoder3to4, IdCoderMIME, IdBaseComponent,
  ZetaCommonTools,  ZetaServerTools, Db, DBClient, ZetaClientDataSet, DXMLTools,
   SOAPHTTPClient, SOAPHTTPTrans, Types,
   IdHTTP, IdIOHandlerSocket, IdSSLOpenSSL,InvokeRegistry,  wininet, ZetaRegistryCliente;

type

  TipoTransaccion = ( tNoDefinida, tTomarManifiesto, tRegistrar, tTomarRegimen, tCancelarRegistro, tTimbrar, tCancelarTimbrado, tTomarRecibos, tEnviarRecibos, tConsultarSaldo, tcConsultarEmpleadosConciliar );

  TRecolectorLog = record
    Registrar: boolean;
    UltimoRegistro: TDateTime;
  end;

  TBitacora = record
    Cuantos: string;
    Contenido: string;
    Eventos : TStringList;
  end;

  TResultado = record
    TX_ID: string;
    TX_IDi : integer;
    TX_STATUS: string;
    Errores: boolean;
    ErroresDS : TZetaClientDataSet;
    FacturasDS : TZetaClientDataSet;

    ArchivoUrl  : string;
    ArchivoNombre1 : string;
    ArchivoUrl2 : string;
    ArchivoNombre2 : string;
    Contenido: string;
    Saldo : string;
    CancelarStatus : integer;
    CancelarConteo : integer;
    CancelarFacturasConteo : integer;
    EmpresaRFC : string;
    EmpresaRazon : string;
    EmpresaRegimen : string;
    EmpresaRegimenes : string;

    SinEmailDS : TZetaClientDataSet;
    Recibos : integer;
    SinEmail : integer;
  end;

  TRespuestaStruct = record
    Bitacora: TBitacora;
    Resultado: TResultado;
    transaccion : TipoTransaccion;
    recolector : TRecolectorLog; 
  end;

  TWebServiceConfig = class( TObject )
  public
      FHTTPRIO: THTTPRIO;

      function GetWSUrl : string;
      procedure HTTPRIOBeforeExecute(const MethodName: string;  var SOAPRequest: TStream); {var SOAPRequest: InvString); }
      procedure HTTPRIOHTTPWebNodeBeforePost( const HTTPReqResp: THTTPReqResp; Data: Pointer);
  end;

  TRespuestaSaldo = class ( TObject )
  public
       respuesta : TRespuestaStruct;
       SaldoDS : TZetaClientDataSet;
       DetalleDS  : TZetaClientDataSet;
       lHayRespuesta : boolean;
       sMensaje : string;
  end;

  TRespuestaConciliacionTimbradoNomina = class ( TObject )
  public
       respuesta : TRespuestaStruct;
       respuestaXML : String;
       SaldoDS : TZetaClientDataSet;
       DetalleDS  : TZetaClientDataSet;
       lHayRespuesta : boolean;
       sMensaje : string;
  end;

  INotificarStatusReporte = Interface( IInterface)
      function NotificarStatusReporte( sTitulo, sDescripcion, sDetalle : string; lStatusOk : integer; lEnable: Boolean = TRUE ) : boolean;
  End;

  INotificarStatusAuditoriaConceptosTimbrado = Interface( IInterface)
      function NotificarStatusAuditoriaConceptosTimbrado( sTitulo, sDescripcion, sDetalle : string; lStatusOk : integer; lEnable: Boolean = TRUE ) : boolean;
  End;

  var
     FAplicarTimeoutTimbrado : Boolean;

const
K_SISTEMA_TIPO = 1;

{$ifdef PILOTO}
{$ifdef WSPROFILE}
K_VERSION_TIMBRADO = '4.80';   //Version Piloto Debug
{$else}
K_VERSION_TIMBRADO = '4.81';   //Version  Piloto
{$endif}
{$else}
K_VERSION_TIMBRADO = '4.82';   //Version Produccion
{$endif}

K_PAIS_DEFAULT = '1';
K_PAIS_DEFAULT_NAME = 'Mexico';


TIMBRADO_TESTING_MESSAGE = 'PRUEBA DE CONEXION';
TIMBRADO_SIN_CONECTIVIDAD = 'No hay conectividad con el Sistema de Timbrado. ';
TIMBRADO_ERR_TIMBRAMEX = 'Notificación del Sistema de Timbrado ';
TIMBRADO_NOPOSIBLE_TIMBRAMEX = 'No fue posible Realizar la Transacción en el Sistema de Timbrado, Intente de nuevo el proceso.  '+ char(10)+ char(13) + 'Para mayor detalle revise la Bitácora de Sistema TRESS.';


STATUS_TIMBRADO_DESCONOCIDO = '0';
STATUS_TIMBRADO_EN_PROCESO = '1';
STATUS_TIMBRADO_ERROR_GRAVE = '2';
STATUS_TIMBRADO_HAY_ERRORES = '3';
STATUS_TIMBRADO_TERMINO_SIN_ERROR = '4';
STATUS_TIMBRADO_CERRADA_POR_TASK = '5';

function GetTimbradoPaqueteTimeoutConnect : integer; //miliseconds
function GetTimbradoPaqueteTimeoutSend : integer;    //miliseconds
function GetTimbradoPaqueteTimeoutReceive : integer; //miliseconds

implementation


{ TWebServiceConfig }

function TWebServiceConfig.GetWSUrl: string;
begin
//
end;

procedure TWebServiceConfig.HTTPRIOBeforeExecute(const MethodName: string;
  //var SOAPRequest: InvString);
  var SOAPRequest: TStream);
var
 iTimeOUt : integer;

begin
if (FAplicarTimeoutTimbrado) then
begin
    iTimeOut := GetTimbradoPaqueteTimeoutConnect;
    InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@iTimeOut),
    SizeOf(iTimeOut));

    iTimeOut := GetTimbradoPaqueteTimeoutSend;
    InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@iTimeOut),
    SizeOf(iTimeOut));

    iTimeOut := GetTimbradoPaqueteTimeoutReceive;
    InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@iTimeOut),
    SizeOf(iTimeOut));
end
else
begin
     iTimeOut := 3600000;

     InternetSetOption(nil, INTERNET_OPTION_CONNECT_TIMEOUT, Pointer(@iTimeOut),
     SizeOf(iTimeOut));

     InternetSetOption(nil, INTERNET_OPTION_SEND_TIMEOUT, Pointer(@iTimeOut),
     SizeOf(iTimeOut));

     InternetSetOption(nil, INTERNET_OPTION_RECEIVE_TIMEOUT, Pointer(@iTimeOut),
     SizeOf(iTimeOut));

end;
FAplicarTimeoutTimbrado := FALSE;

end;

procedure TWebServiceConfig.HTTPRIOHTTPWebNodeBeforePost(
  const HTTPReqResp: THTTPReqResp; Data: Pointer);

begin

end;

//Intento Paquete de timbrado
function GetTimbradoPaqueteTimeoutConnect : integer; //miliseconds
var
   registro : TZetaClientRegistry;
begin
    registro :=  TZetaClientRegistry.Create;
    Result :=  registro.TimbradoPaqueteTimeoutConnect;
    FreeAndNil(registro);
end;

function GetTimbradoPaqueteTimeoutSend : integer;    //miliseconds
var
   registro : TZetaClientRegistry;
begin
    registro :=  TZetaClientRegistry.Create;
    Result :=  registro.TimbradoPaqueteTimeoutSend;
    FreeAndNil(registro);
end;

function GetTimbradoPaqueteTimeoutReceive : integer; //miliseconds
var
   registro : TZetaClientRegistry;
begin
    registro :=  TZetaClientRegistry.Create;
    Result :=  registro.TimbradoPaqueteTimeoutReceive;
    FreeAndNil(registro);
end;

end.
