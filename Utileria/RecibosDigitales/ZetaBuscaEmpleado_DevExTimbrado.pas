unit ZetaBuscaEmpleado_DevExTimbrado;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Grids, DBGrids, StrUtils,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxButtons, ExtCtrls, Mask,
     {$ifndef VER130}MaskUtils,{$endif}
     ZetaMessages,
     ZetaDBGrid,
     ZetaClientDataset, TressMorado2013;

type
  TBuscaEmpleado_DevExTimbrado = class(TForm)
    PanelControles: TPanel;
    ApePatLabel: TLabel;
    BuscarEdit: TEdit;
    DataSource: TDataSource;
    MostrarBajas: TCheckBox;
    BuscarBtn_DevEx: TcxButton;
    AceptarBtn_DevEx: TcxButton;
    GridEmpleados_DevEx: TZetaCXGrid;
    GridEmpleados_DevExDBTableView: TcxGridDBTableView;
    GridEmpleados_DevExLevel: TcxGridLevel;
    CB_CODIGO: TcxGridDBColumn;
    CB_APE_PAT: TcxGridDBColumn;
    CB_NOMBRES: TcxGridDBColumn;
    CB_RFC: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    CB_APE_MAT: TcxGridDBColumn;
    CB_BAN_ELE: TcxGridDBColumn;
    CB_ACTIVO: TcxGridDBColumn;
    STATUS: TcxGridDBColumn;
    TABLA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarEditChange(Sender: TObject);
    procedure MostrarBajasClick(Sender: TObject);
    procedure FiltraTextoEditChange(Sender: TObject);
    procedure BuscarBtn_DevExClick(Sender: TObject);
    procedure AceptarBtn_DevExClick(Sender: TObject);
    procedure GridEmpleados_DevExDBTableViewDblClick(Sender: TObject);
    procedure GridEmpleados_DevExDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure GridEmpleados_DevExDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure CB_CODIGOCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_APE_PATCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_APE_MATCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_NOMBRESCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_RFCCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_SEGSOCCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    {procedure CB_BAN_ELECustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);}
  private
    { Private declarations }
    FFiltro: String;
    FNumeroEmpleado: Integer;
    FNombreEmpleado: String;
    FCapturando: Boolean;
    FLookupDataSet: TZetaClientDataset;
    FEmpresaActual: OleVariant;
    procedure Buscar;
    procedure SetNumero;
    procedure SetBotones( const Vacio : Boolean );
    procedure ShowGrid( const lVisible: Boolean );
    procedure Connect;
    procedure Disconnect;
    procedure RemoveFilter;
    procedure SetFilter;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property NumeroEmpleado: Integer read FNumeroEmpleado;
    property NombreEmpleado: String read FNombreEmpleado;
    property Filtro: String read FFiltro write FFiltro;
    property LookupDataSet: TZetaClientDataset read FLookupDataSet write FLookupDataSet;
    property EmpresaActual: OleVariant read FEmpresaActual write FEmpresaActual;
  end;

procedure SetOnlyActivos( const lActivos: Boolean );

var
  BuscaEmpleado_DevExTimbrado: TBuscaEmpleado_DevExTimbrado;

function BuscaEmpleadoDialogoDataSet( const sFilter: String; var sKey, sDescription: String; const oDataSet: TZetaClientDataset; oEmpresa: OleVariant ): Boolean;
function BuscaEmpleadoDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientTools,
     DGlobal,
     ZGlobalTress,
     ZFiltroSQLTools,
     DInterfase,
     Dcliente;

var
   FShowOnlyActivos: Boolean;

{$R *.DFM}

procedure SetOnlyActivos( const lActivos: Boolean );
begin
     FShowOnlyActivos := lActivos;
end;



function BuscaEmpleadoDialogoDataSet( const sFilter: String; var sKey, sDescription: String; const oDataSet: TZetaClientDataset; oEmpresa: OleVariant ): Boolean;
begin
     Result := False;
     if ( BuscaEmpleado_DevExTimbrado = nil ) then
        BuscaEmpleado_DevExTimbrado := TBuscaEmpleado_DevExTimbrado.Create( Application );
     if ( BuscaEmpleado_DevExTimbrado <> nil ) then
     begin
          with BuscaEmpleado_DevExTimbrado do
          begin
               LookupDataset := oDataSet;
               EmpresaActual := oEmpresa;
               Filtro := sFilter;
               ShowModal;
               if ( ModalResult = mrOk ) and ( NumeroEmpleado <> 0 ) then
               begin
                    sKey := IntToStr( NumeroEmpleado );
                    sDescription := NombreEmpleado;
                    Result := True;
               end;
          end;
     end;
end;

function BuscaEmpleadoDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := BuscaEmpleadoDialogoDataSet( sFilter, sKey, sDescription, Dminterfase.cdsEmpleadoLookupTimbrado, dmCliente.Empresa );
end;

{************** TBuscaEmpleado ************** }

procedure TBuscaEmpleado_DevExTimbrado.FormCreate(Sender: TObject);
begin
     HelpContext := H00012_Busqueda_empleados;
end;

procedure TBuscaEmpleado_DevExTimbrado.FormShow(Sender: TObject);
begin
     WindowState := wsNormal;
     ActiveControl := BuscarEdit;
     Connect;
end;

procedure TBuscaEmpleado_DevExTimbrado.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

procedure TBuscaEmpleado_DevExTimbrado.SetFilter;
var
   sStatusFiltro :string;
begin
     sStatusFiltro := VACIO;

     if LookupDataset.FindField('STATUS') <> NIL then
     begin
          if MostrarBajas.Checked then
          begin
               sStatusFiltro := Format( 'or ( STATUS <> %d )',[ Ord( steBaja ) ] );
          end
          else
          begin
              sStatusFiltro := Format( 'or ( STATUS <> %d and TABLA = %s)',[ Ord( steBaja ),EntreComillas('COLABORA') ] );
          end;
     end;
     with LookupDataset do
     begin
          DisableControls;
          try
             Filtered := False;
             if MostrarBajas.Checked then
             begin
                  Filter := Filtro;
             end
             else
                 Filter := ZetaCommonTools.ConcatFiltros( Filtro, Format('( CB_ACTIVO = %s and TABLA = %s) %s',[EntreComillas(K_GLOBAL_SI),EntreComillas('COLABORA'),sStatusFiltro ] ) );
             Filtered := True;
          finally
                 EnableControls;
          end;
     end;
end;



procedure TBuscaEmpleado_DevExTimbrado.RemoveFilter;
begin
     with LookupDataset do
     begin
          DisableControls;
          try
             if Filtered then
             begin
                  Filtered := False;
                  Filter := VACIO;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TBuscaEmpleado_DevExTimbrado.Connect;
begin
     SetFilter;
     ShowGrid( False );
     SetBotones( True );
     ActiveControl := BuscarEdit;
     with MostrarBajas do
     begin
          if FShowOnlyActivos then
          begin
               Checked := False;
               Enabled := False;
               Visible := False;
          end
          else
          begin
               Enabled := True;
               Visible := True;
          end;
     end;
     BuscarEdit.Clear;
     //Para borrar los filtros si es que se hizo alguno
     GridEmpleados_DevExDBTableView.DataController.Filter.Root.Clear;
     Datasource.Dataset := LookupDataset;
end;

procedure TBuscaEmpleado_DevExTimbrado.Disconnect;
begin
     Datasource.Dataset := nil;
     with LookupDataset do
     begin
          if Filtered then
          begin
               RemoveFilter;
          end;
     end;
end;

procedure TBuscaEmpleado_DevExTimbrado.Buscar;
var
   oCursor: TCursor;
begin
     AceptarBtn_DevEx.Enabled := False;
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourglass;
        with LookupDataset do
        begin
             DisableControls;
             try
                Active := False;
                dminterfase.GetEmpleadosBuscados_DevExTimbrado(BuscarEdit.Text,EmpresaActual,LookUpDataSet);
                Active := True;
             finally
                    EnableControls;
             end;
             if IsEmpty then
             begin
                  zWarning( 'B�squeda de Empleados', '� No Hay Empleados Con Estos Datos !', 0, mbOK );
                  SetBotones( True );
                  ActiveControl := BuscarEdit;
             end
             else
             begin
                  SetFilter;
                  ShowGrid( True );
                  SetBotones( False );
                  ActiveControl := GridEmpleados_DevEx;
             end;
             GridEmpleados_DevExDBTableView.ApplyBestFit();
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBuscaEmpleado_DevExTimbrado.SetNumero;
begin
     with LookupDataset do
     begin
          FNumeroEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          FNombreEmpleado := FieldByName( 'PrettyName' ).AsString;
     end;
     ModalResult := mrOK;
end;

procedure TBuscaEmpleado_DevExTimbrado.SetBotones( const Vacio: Boolean );
begin
     FCapturando := Vacio;
     BuscarBtn_DevEx.Default := Vacio;
     with AceptarBtn_DevEx do
     begin
          Default := not Vacio;
          //Enabled := not Vacio;
          Enabled := (not Vacio) or (GridEmpleados_DevExDBTableView.DataController.RowCount>0);
     end;
end;

procedure TBuscaEmpleado_DevExTimbrado.ShowGrid( const lVisible: Boolean );
const
     GRID_HEIGHT = 200;
begin
      with GridEmpleados_DevEx do
     begin
          if lVisible then
          begin
               Visible:= True;
               Height := ZetaClientTools.GetScaledHeight( GRID_HEIGHT );
               Self.ClientHeight := ZetaClientTools.GetScaledHeight( PanelControles.Height + GRID_HEIGHT );
          end
          else
          begin
               Visible:= False;
               Height := 0;
               Self.ClientHeight := ZetaClientTools.GetScaledHeight( PanelControles.Height );
          end;
     end;
     //Repaint;
end;

procedure TBuscaEmpleado_DevExTimbrado.WMExaminar(var Message: TMessage);
begin
     //AceptarBtn.Click;
     AceptarBtn_DevEx.Click;     //Enter al Grid
end;

{procedure TBuscaEmpleado_DevEx.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;}

{procedure TBuscaEmpleado_DevEx.AceptarBtnClick(Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero;
end;}

procedure TBuscaEmpleado_DevExTimbrado.BuscarEditChange(Sender: TObject);
begin
     if not FCapturando then
        SetBotones( True );

     FiltraTextoEditChange( Sender );
end;

{procedure TBuscaEmpleado_DevEx.GridEmpleadosDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     with GridEmpleados do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    if(  zStrToBool( LookupDataset.FieldByName( 'CB_ACTIVO' ).AsString ) or  GetStatusAct )then
                       Font.Color := GridEmpleados.Font.Color
                    else
                        Font.Color := clRed;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end; }

procedure TBuscaEmpleado_DevExTimbrado.MostrarBajasClick(Sender: TObject);
begin
     SetFilter;
     GridEmpleados_DevExDBTableView.ApplyBestFit();
end;

procedure TBuscaEmpleado_DevExTimbrado.FiltraTextoEditChange(Sender: TObject);
var
   OldChange: TNotifyEvent;
   OldStart : Integer;
begin
    with (Sender as TEdit ) do
    begin
         OldChange := OnChange;
         OnChange  := nil;
         OldStart  := SelStart;
         Text := FiltrarTextoSQL( Text );
         OnChange := OldChange;
         SelStart := OldStart;
    end;
end;

procedure TBuscaEmpleado_DevExTimbrado.BuscarBtn_DevExClick(Sender: TObject);
begin
      Buscar;
end;

procedure TBuscaEmpleado_DevExTimbrado.AceptarBtn_DevExClick(Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero;
end;

procedure TBuscaEmpleado_DevExTimbrado.GridEmpleados_DevExDBTableViewDblClick(
  Sender: TObject);
begin
     AceptarBtn_DevEx.Click;
end;

procedure TBuscaEmpleado_DevExTimbrado.GridEmpleados_DevExDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

//DevEx: Se pone en rojo el color de los registros que son Baja
procedure TBuscaEmpleado_DevExTimbrado.GridEmpleados_DevExDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
const
     K_COL_STATUS_DB=7;
     K_COL_STATUS=8;
var
     sStatusDB:String;
     lStatus: boolean;
     Origen: string;
begin

   sStatusDB := AViewInfo.GridRecord.DisplayTexts [CB_ACTIVO.Index];
   Origen := AviewInfo.GridRecord.DisplayTexts[TABLA.Index] ;
   lStatus := FALSE;
   if AViewInfo.GridRecord.DisplayTexts[STATUS.Index] <> VACIO then
      lStatus := StrToInt(AViewInfo.GridRecord.DisplayTexts [STATUS.Index]) <> Ord(steBaja);

   with ACanvas do
   begin
       // if (  zStrToBool( LookupDataset.FieldByName( 'CB_ACTIVO' ).AsString ) or  GetStatusAct )then
       if (  zStrToBool( sStatusDB )  or  lStatus )then
             if (Origen = 'COLABORA' ) then
             begin
                  Font.Color := GridEmpleados_DevEx.Font.Color;
             end
             else
             begin
                  Font.Color := clGray;
             end
        else
            Font.Color := clRed;
   end;
end;

//DevEx: Pone el bold el font de las celdas donde se encontro la palabra
procedure TBuscaEmpleado_DevExTimbrado.CB_CODIGOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbrado.CB_APE_PATCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbrado.CB_APE_MATCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbrado.CB_NOMBRESCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbrado.CB_RFCCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbrado.CB_SEGSOCCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

{
procedure TBuscaEmpleado_DevExTimbrado.CB_BAN_ELECustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;
}end.
