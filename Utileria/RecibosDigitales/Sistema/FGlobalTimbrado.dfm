inherited GlobalTimbrado: TGlobalTimbrado
  Left = 523
  Top = 277
  Caption = 'Timbrado de N'#243'mina'
  ClientHeight = 172
  ClientWidth = 357
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 136
    Width = 357
    inherited OK_DevEx: TcxButton
      Left = 192
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 272
    end
  end
  object EnviarDetalleTimbrado: TCheckBox [1]
    Tag = 266
    Left = 117
    Top = 15
    Width = 210
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Enviar detalle de conceptos a timbrado:'
    TabOrder = 1
  end
  object EnviarIncapacidadesTimbrado: TCheckBox [2]
    Tag = 266
    Left = 98
    Top = 35
    Width = 229
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Enviar detalle de incapacidades a timbrado:'
    TabOrder = 2
  end
  object EnviarTiempoExtraTimbrado: TCheckBox [3]
    Tag = 266
    Left = 110
    Top = 55
    Width = 217
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Enviar detalle de tiempo extra a timbrado:'
    TabOrder = 3
  end
  object UsarDiasPeriodoComoPeriodicidad: TCheckBox [4]
    Tag = 266
    Left = 36
    Top = 75
    Width = 291
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Usar d'#237'as de tipo de periodo como periodicidad de pago:'
    TabOrder = 4
  end
  object ConsiderarTransferidos: TCheckBox [5]
    Tag = 266
    Left = 140
    Top = 95
    Width = 187
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Considerar empleados transferidos:'
    TabOrder = 5
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
