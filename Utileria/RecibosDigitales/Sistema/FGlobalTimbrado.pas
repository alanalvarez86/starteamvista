unit FGlobalTimbrado;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal, ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,  TressMorado2013, dxSkinsDefaultPainters,
  ImgList, cxButtons;

type
  TGlobalTimbrado = class(TBaseGlobal_DevEx)
    EnviarDetalleTimbrado: TCheckBox;
    EnviarIncapacidadesTimbrado: TCheckBox;
    EnviarTiempoExtraTimbrado: TCheckBox;
    UsarDiasPeriodoComoPeriodicidad: TCheckBox;
    ConsiderarTransferidos: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalTimbrado: TGlobalTimbrado;

implementation

{$R *.DFM}

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

procedure TGlobalTimbrado.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos       := ZAccesosTress.D_CAT_CONFI_NOMINA;
     EnviarDetalleTimbrado.Tag :=  K_GLOBAL_ESPECIAL_LOG_GLOBAL_5;
     EnviarIncapacidadesTimbrado.Tag :=  K_GLOBAL_ESPECIAL_LOG_GLOBAL_4;
     EnviarTiempoExtraTimbrado.Tag :=  K_GLOBAL_ESPECIAL_LOG_GLOBAL_3;
     UsarDiasPeriodoComoPeriodicidad.tag := K_GLOBAL_ESPECIAL_LOG_GLOBAL_2;
     ConsiderarTransferidos.tag := K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_1; //se maneja como global de logica (boleano)
     HelpContext := 0;
end;

procedure TGlobalTimbrado.FormShow(Sender: TObject);
begin
  inherited;
  EnviarDetalleTimbrado.SetFocus;
end;

end.
