unit ZetaDialogoLink;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, CommDlg,ZetaCommonLists;

function ZetaMessageLink( const sCaption, sMsgUrl, sMsgCompleto: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult; forward;
procedure ZErrorLink( const sCaption,sMsgUrl, sMsgCompleto: String; const iHelpCtx: Longint );

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaMsgDlgLink;
var
   ZDialogoLink: TZDialogoLink;


function GetDialogo: TZDialogoLink;  // Siempre debe ser Privada a esta unidad //
begin
     if ( ZDialogoLink = nil ) then
     begin
          ZDialogoLink := TZDialogoLink.Create( Application ); // Application Debe Destruirlo Al Finalizar //
     end;
     Result := ZDialogoLink;
end;

function ZetaMessageLink( const sCaption, sMsgUrl, sMsgCompleto: String; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; const iHelpCtx: Longint; oDefaultBoton: TMsgDlgBtn ): TModalResult;
begin
     with GetDialogo do
     begin
          Caption := sCaption;
          MensajeLink := sMsgUrl;
          MensajeCompleto := sMsgCompleto;
          Tipo := DlgType;
          Botones := Buttons;
          DefaultBoton := oDefaultBoton;
          HelpCtx := iHelpCtx;
          Result := Execute;
     end;
end;

procedure ZErrorLink( const sCaption, sMsgUrl, sMsgCompleto: String; const iHelpCtx: Longint );
begin
     ZetaMessageLink( sCaption,sMsgUrl, sMsgCompleto, mtError, [ mbOk ], iHelpCtx, mbOk );
end;
end.

