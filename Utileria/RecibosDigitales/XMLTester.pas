unit XMLTester;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FTimbramexHelper, FTimbramexClasses, Grids, DBGrids, DB,
  ComCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Memo2: TMemo;
    DBGrid2: TDBGrid;
    Button2: TButton;
    DataSourceFacturas: TDataSource;
    Button3: TButton;
    Memo3: TMemo;
    Edit1: TEdit;
    Memo4: TMemo;
    Memo5: TMemo;
    Button5: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    TabSheet3: TTabSheet;
    Memo6: TMemo;
    Edit2: TEdit;
    Button4: TButton;
    Edit3: TEdit;
    Edit4: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    TabSheet4: TTabSheet;
    Memo7: TMemo;
    Button6: TButton;
    Label6: TLabel;
    Edit5: TEdit;
    Edit6: TEdit;
    Label7: TLabel;
    DBGrid3: TDBGrid;
    Label8: TLabel;
    Label9: TLabel;
    DataSourceSinEmail: TDataSource;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    respuesta : TRespuestaStruct;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
     respuesta := FTimbraMexHelper.GetTimbradoRespuesta(Memo1.Text);
     DataSource1.DataSet := respuesta.Resultado.ErroresDS;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
 respuesta := FTimbraMexHelper.GetTimbradoRespuesta(Memo2.Text);
 DataSource1.DataSet := respuesta.Resultado.ErroresDS;
  DataSourceFacturas.DataSet := respuesta.Resultado.FacturasDS;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
    respuesta := FTimbraMexHelper.GetRecibosRespuesta(Memo3.Text);
 DataSource1.DataSet := respuesta.Resultado.ErroresDS;
  DataSourceFacturas.DataSet := respuesta.Resultado.FacturasDS;
  Edit1.Text := respuesta.Resultado.ArchivoUrl;
end;

procedure TForm1.Button5Click(Sender: TObject);

   function LimpiaCuentaPass( sXML : string )  : string;
   var
      iBegin , iEnd : integer;
   begin
   //       <CUENTA_PASSWORD>%s</CUENTA_PASSWORD>
        iBegin :=  Pos( '<CUENTA_PASSWORD>', sXML ) ;
        iEnd :=  Pos( '</CUENTA_PASSWORD>', sXML ) ;
        Result := sXML ;
        Delete ( Result, iBegin, iEnd ) ;

        Result := StringReplace( Result , 'Timbramex' , '' , [rfIgnoreCase, rfReplaceAll] )
   end;

begin
     Memo5.Text := LimpiaCuentaPass( Memo4.Text );
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  respuesta := FTimbraMexHelper.GetRegimenFiscal(Memo6.Text);
  DataSource1.DataSet := respuesta.Resultado.ErroresDS;
  DataSourceFacturas.DataSet := respuesta.Resultado.FacturasDS;
  Edit2.Text := respuesta.Resultado.EmpresaRFC;
  Edit3.Text := respuesta.Resultado.EmpresaRazon;
  Edit4.Text := respuesta.Resultado.EmpresaRegimenes;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  respuesta := FTimbraMexHelper.GetRecibosRespuesta(Memo7.Text, TRUE);
  DataSource1.DataSet := respuesta.Resultado.ErroresDS;
  DataSourceSinEmail.DataSet := respuesta.Resultado.SinEmailDS;
  Edit5.Text := Format( '%d' , [respuesta.Resultado.Recibos] );
  Edit6.Text := Format( '%d' , [respuesta.Resultado.SinEmail] );



end;

end.
