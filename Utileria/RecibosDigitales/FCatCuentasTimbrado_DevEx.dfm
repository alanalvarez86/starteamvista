inherited CatCuentasTimbrado_DevEx: TCatCuentasTimbrado_DevEx
  Left = 226
  Top = 239
  Caption = 'Cuentas de Timbrado'
  ClientHeight = 426
  ClientWidth = 813
  ExplicitWidth = 813
  ExplicitHeight = 426
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 813
    ExplicitWidth = 813
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 554
      ExplicitWidth = 554
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 548
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 813
    Height = 407
    ExplicitWidth = 813
    ExplicitHeight = 407
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      Navigator.Buttons.ConfirmDelete = True
      Navigator.Buttons.PriorPage.Enabled = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.NextPage.Enabled = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Cancel.Enabled = False
      Navigator.Buttons.Cancel.Visible = False
      Navigator.Buttons.SaveBookmark.Enabled = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Enabled = False
      Navigator.Buttons.Filter.Enabled = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.InfoPanel.DisplayMask = '[RecordIndex] de [RecordCount]'
      OptionsCustomize.ColumnGrouping = False
      OptionsView.NoDataToDisplayInfoText = 'No hay Cuentas de Timbrado'
      OptionsView.ShowEditButtons = gsebAlways
      OptionsView.GroupByBox = False
      object cxGridDBTableView2Column1: TcxGridDBColumn
        Caption = 'C'#243'digo '
        DataBinding.FieldName = 'CT_CODIGO'
        MinWidth = 80
      end
      object cxGridDBColumn7: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CT_DESCRIP'
        MinWidth = 100
        Options.Editing = False
        Width = 150
      end
      object cxGridDBColumn6: TcxGridDBColumn
        Caption = 'Cuenta Timbrado ID'
        DataBinding.FieldName = 'CT_ID'
        MinWidth = 150
        Options.Editing = False
        Width = 150
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 352
    Top = 344
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 14680560
  end
  inherited ActionList: TActionList
    Left = 354
    Top = 272
  end
  inherited PopupMenu1: TPopupMenu
    Left = 32
    Top = 288
  end
  object DataSourceCuentas: TDataSource
    Left = 492
    Top = 66
  end
end
