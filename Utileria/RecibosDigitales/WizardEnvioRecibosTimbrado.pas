unit WizardEnvioRecibosTimbrado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo, ZGridModeTools,
  Timbres,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaDBTextBox, ExtCtrls, ZetaKeyLookup, Mask,
  DBCtrls,FTimbramexClasses, FTimbramexHelper, Grids, DBGrids, Buttons,
  ZetaSmartLists, FBaseReportes_DevEx, ZetaCommonLists, cxHyperLinkEdit,
  cxRadioGroup, URLMon, cxShellBrowserDialog, WinInet, WininetUtils,
  ZetaEdit, ZetaCommonClasses, DBClient,DCatalogos, ZetaKeyCombo,
  TressMorado2013, ZetaCXGrid,ZetaKeyLookup_DevEx,cxMemo, dxGDIPlusClasses,
  cxImage;



type
  TWizardEnvioRecibosTimbradoForm = class(TdxWizardControlForm)
    dxWizardControl1: TdxWizardControl;
    dxWizardControlPageEnvio: TdxWizardControlPage;
    cxGroupBox1: TcxGroupBox;
    cxLabelAvance: TcxLabel;
    dxWizardControlPage2: TdxWizardControlPage;
    dataSourceTimbrar: TDataSource;
    dataSourceContribuyente: TDataSource;
    DataSourceCuentas: TDataSource;
    DataSourceRecibos: TDataSource;
    cxGroupBoxResultados: TcxGroupBox;
    cxLabel3: TcxLabel;
    DataSourceErrores: TDataSource;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    gridPeriodos: TZetaCXGrid;
    gridPeriodosDBTableView1: TcxGridDBTableView;
    gridPeriodosDBTableView1Column10: TcxGridDBColumn;
    gridPeriodosDBTableView1Column8: TcxGridDBColumn;
    gridPeriodosDBTableView1Column2: TcxGridDBColumn;
    gridPeriodosDBTableView1Column9: TcxGridDBColumn;
    gridPeriodosDBTableView1Column5: TcxGridDBColumn;
    gridPeriodosDBTableView1Column7: TcxGridDBColumn;
    gridPeriodosDBTableView1Column11: TcxGridDBColumn;
    gridPeriodosDBTableView1Column4: TcxGridDBColumn;
    gridPeriodosLevel1: TcxGridLevel;
    cxGroupBox3: TcxGroupBox;
    PeriodoTipoLbl: TLabel;
    Label6: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label8: TLabel;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    iTipoNomina: TZetaTextBox;
    Label1: TLabel;
    PE_TIMBRO: TZetaDBTextBox;
    DatasetPeriodo: TDataSource;
    CT_CODIGO: TZetaKeyLookup;
    dxWizardControlPageEmpleados: TdxWizardControlPage;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    cbOrden: TZetaKeyCombo;
    cxProgressBarEnvio: TcxProgressBar;
    cxLabel6: TcxLabel;
    cxRecibosTxt: TcxMaskEdit;
    cxLabel5: TcxLabel;
    cxSinEmailTxt: TcxMaskEdit;
    cxStatus: TcxLabel;
    cxGroupBoxSinEmail: TcxGroupBox;
    cxLabel1: TcxLabel;
    DataSourceSinEmail: TDataSource;
    FiltrosGB: TGroupBox;
    sCondicionLBl: TLabel;
    sFiltroLBL: TLabel;
    sFiltro: TcxMemo;
    Seleccionar: TcxButton;
    BAgregaCampo: TcxButton;
    ECondicion: TZetaKeyLookup_DevEx;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    RBTodos: TcxRadioButton;
    RBRango: TcxRadioButton;
    RBLista: TcxRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    BInicial: TcxButton;
    BFinal: TcxButton;
    BLista: TcxButton;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    ZetaCXGrid1DBTableView1NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2: TcxGridDBTableView;
    ZetaCXGrid1DBTableView2NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2DETALLE: TcxGridDBColumn;
    ZetaCXGrid1DBTableView3: TcxGridDBTableView;
    ZetaCXGrid1DBTableView3NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView3ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView3DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1Level1: TcxGridLevel;
    ZetaCXGrid1DBTableView4: TcxGridDBTableView;
    ZetaCXGrid1DBTableView4NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView4ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView4DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid2: TZetaCXGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    ZetaCXGrid2Level1: TcxGridLevel;
    ZetaCXGrid2DBTableView1: TcxGridDBTableView;
    ZetaCXGrid2DBTableView1NUMERO: TcxGridDBColumn;
    ZetaCXGrid2DBTableView1NOMBRE: TcxGridDBColumn;
    Panel1: TPanel;
    ZetaSpeedButton2: TcxButton;
    Panel3: TPanel;
    Advertencia: TcxLabel;
    cxImage1: TcxImage;
    Panel2: TPanel;
    ZetaSpeedButton1: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure CT_CODIGOValidLookup(Sender: TObject);
    procedure ZetaSpeedButton1Click(Sender: TObject);

    procedure IniciarProgress;
    procedure TerminarProgress;
    procedure btCancelarDescargaClick(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure ZetaSpeedButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gridPeriodosDBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure DataSourceErroresUpdateData(Sender: TObject);
    procedure DataSourceSinEmailUpdateData(Sender: TObject);
    procedure DataSourceErroresDataChange(Sender: TObject; Field: TField);
    procedure DataSourceSinEmailDataChange(Sender: TObject; Field: TField);

  private
   FContinuaDownload : boolean;
   FEjecuto : boolean;
   FVerificacion: Boolean;

   FTipoRango: eTipoRangoActivo;
   FEmpleadoCodigo: String;
   FEmpleadoFiltro: String;



   FParameterList: TZetaParams;
   FDescripciones: TZetaParams;
       { Private declarations }
    function RecibosServer : boolean;
    function BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;

    function GetRazonSocial : string;

    procedure MostrarGroupBoxesRespuesta( lSinEmail, lErrores : boolean );

  protected
    { Protected declarations }
    property ParameterList: TZetaParams read FParameterList;
    property Descripciones: TZetaParams read FDescripciones;
    property Verificacion: Boolean read FVerificacion write FVerificacion;

    function Verificar: Boolean; virtual;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure CargaListaVerificacion;
    procedure CargaParametros;
    procedure CargaListaNominas;

    procedure SetVerificacion( const Value: Boolean );
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );

  public

      property TipoRango: eTipoRangoActivo read FTipoRango;
      Procedure AgregaFiltroOrden;
      Function EntidadActiva:integer;

end;

var
  WizardEnvioRecibosTimbradoForm: TWizardEnvioRecibosTimbradoForm;


function ShowWizardTimbrado : boolean;

implementation

uses
 ZetaClientTools,
     ZetaTipoEntidad,
 DTablas, Dcliente,
 DProcesos,
 DGlobal,
 ZGlobalTress,
 DBasicoCliente,
     ZConstruyeFormula,
     ZConfirmaVerificacion,
     ZetaCommonTools,
     ZetaBuscaEmpleado_DevEx,
     ZetaBuscaEmpleado_DevExTimbrado,
     ZBaseSelectGrid_DevEx,
     ZBasicoSelectGrid_DevEx,
     FEmpleadoTimbradoGridSelect;


{$R *.dfm}

function ShowWizardTimbrado : boolean;
begin
    Result := FALSE;
end;

procedure TWizardEnvioRecibosTimbradoForm.cxButton1Click(Sender: TObject);
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
var
   sEchoResponse : string;
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;
begin

     try

        sServer := '';
        sServer := GetTimbradoServer;
        timbresSoap := nil;

        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
           if ( timbresSoap <> nil ) then
           begin
				crypt := TWSTimbradoCrypt.Create;
                sEchoResponse := crypt.Desencriptar(  timbresSoap.Echo( crypt.Encriptar(TESTING_MESSAGE) ) );
			  //sEchoResponse :=  					  timbresSoap.Echo(                 TESTING_MESSAGE    );
				FreeAndNil( crypt );
                ZInformation( Self.Caption, 'Hay Conectividad con el Servidor de Timbrado' , 0);
           end;
        end
        else
        begin
           zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
        end;
     except on Error: Exception do
               zError( Self.Caption, K_SIN_CONECTIVIDAD, 0);
     end;


end;

procedure TWizardEnvioRecibosTimbradoForm.DataSourceErroresDataChange(
  Sender: TObject; Field: TField);
begin
ZetaCXGrid1DBTableView4.ApplyBestFit();
end;

procedure TWizardEnvioRecibosTimbradoForm.DataSourceErroresUpdateData(
  Sender: TObject);
begin
     ZetaCXGrid1DBTableView4.ApplyBestFit();
end;

procedure TWizardEnvioRecibosTimbradoForm.DataSourceSinEmailDataChange(
  Sender: TObject; Field: TField);
begin
     ZetaCXGrid2DBTableView1.ApplyBestFit();
end;

procedure TWizardEnvioRecibosTimbradoForm.DataSourceSinEmailUpdateData(
  Sender: TObject);
begin
     ZetaCXGrid2DBTableView1.ApplyBestFit();
end;

procedure TWizardEnvioRecibosTimbradoForm.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin

  if AKind = wcbkCancel then
  begin
         if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
         begin
               if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
               begin
                    ModalResult := mrCancel;
               end;
         end
         else
         begin
              ModalResult := mrCancel;
         end;
  end
  else if AKind = wcbkBack then
  begin
       dxWizardControl1.Buttons.Finish.Enabled := True;
       dxWizardControl1.Buttons.Cancel.Caption := 'Cancelar';
  end
  else if AKind = wcbkFinish then
  begin
    Ahandled := True;

    if (not FEjecuto) and RecibosServer then
    begin

         with dmInterfase.RespuestaTimbramex.Resultado do
         begin
              if Recibos > 0 then
              begin

                   if ( SinEmail = 0 ) then
                   begin
                       if ( Recibos  > 0 ) then
                       ZInformation( Self.Caption, Format( 'Se enviar�n %d recibos del Periodo por correo electr�nico a trav�s del Sistema de Timbrado',
                           [Recibos] ) , 0);
                   end
                   else
                   begin
                        if ( Recibos - SinEmail > 0 ) then
                       ZInformation( Self.Caption, Format( 'Se enviar�n %d recibos del Periodo por correo electr�nico a trav�s del Sistema de Timbrado' + CR_LF + '%d recibo(s) se timbraron sin especificar correo electr�nico.',
                           [Recibos - SinEmail, SinEmail] ) , 0)
                        else
                        if ( SinEmail = Recibos ) then
                         ZInformation( Self.Caption, Format( 'Los %d recibos del Periodo  se timbraron sin especificar correo electr�nico.'+ CR_LF + 'No se enviar�n los recibos.',
                           [Recibos] ) , 0);


                   end;

              end;

         end;
    end;

  end;

end;

procedure TWizardEnvioRecibosTimbradoForm.MostrarGroupBoxesRespuesta( lSinEmail, lErrores : boolean );
const
  K_BASE_TOP = 85;
  K_BASE_DIF = 105;

begin
//     cxGroupBoxSinEmail.Visible := lSinEmail;
//     cxGroupBoxResultados.Visible := lErrores;
//
//     if lSinEmail and lErrores then
//     begin
//        cxGroupBoxSinEmail.Top := K_BASE_TOP;
//        cxGroupBoxSinEmail.Height := K_BASE_DIF;
//        cxGroupBoxResultados.Top := K_BASE_TOP + K_BASE_DIF;
//        cxGroupBoxResultados.Height := K_BASE_DIF;
//     end;

end;

function TWizardEnvioRecibosTimbradoForm.RecibosServer: boolean;


  function GetHeadFilename : string;
  begin
     with dmCliente do
     begin
          with GetDatosPeriodoActivo do
          begin
               Result := Format('Recibos_%s_%d_%s_%d', [dmCliente.Compania, dmCliente.YearDefault, ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) ), Numero] );
          end;
     end;


  end;

//Metodo que se iria a una Helper Class
  function   GetRecibosServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No fue posible Consultar el Timbrado de la Nomina en el Sistema de Timbrado: ';
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;
	 crypt : TWSTimbradoCrypt; 

     sReciboHeadName : string;
  begin

     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     try

          sServer := '';
          sServer := GetTimbradoServer;
          timbresSoap := nil;
          IniciarProgress;

          if ( sServer <> '' ) then
          begin
             cxStatus.Caption := 'Conectando a Servidor...';
             timbresSoap := Timbres.GetTimbresFiscalesSoap(False, GetTimbradoURL );

             if ( timbresSoap <> nil ) then
             begin
                          REsult := TRUE;
                  cxStatus.Caption := 'Formando Petici�n...';
                  cxProgressBarEnvio.Position := 20.00;  cxProgressBarEnvio.Update;
				  				  
                  sPeticion := dmInterfase.GetPeticionEnvioRecibos;

                  cxProgressBarEnvio.Position := 40.00;  cxProgressBarEnvio.Update;
                  cxStatus.Caption := 'Generando Recibos en Sistema de Timbrado...';
                  Application.ProcessMessages;

                  
                  crypt := TWSTimbradoCrypt.Create;
		  sRespuesta := crypt.Desencriptar(  timbresSoap.PeriodoEnviarCorreos( crypt.Encriptar( sPeticion ) ));
		 //sRespuesta :=					     timbresSoap.PeriodoEnviarCorreos( 				    sPeticion   );
		  FreeAndNil(crypt);
				  
                  cxProgressBarEnvio.Position := 80.00;  cxProgressBarEnvio.Update;
                  respuestaTimbramex := FTimbramexHelper.GetRecibosRespuesta(sRespuesta, TRUE );


                  dmInterfase.RespuestaTimbramex := respuestaTimbramex;
                  DatasourceErrores.DataSet := respuestaTimbramex.Resultado.ErroresDS;
                  //cxGroupBoxResultados.Visible := not respuestaTimbramex.Resultado.ErroresDS.IsEmpty;

                  with respuestaTimbramex.Resultado do
                  begin
                       cxRecibosTxt.Text := Format( '%d' , [Recibos] );
                       cxSinEmailTxt.Text := Format( '%d' , [SinEmail] );
                       DataSourceSinEmail.DataSet := SinEmailDS;
                       //MostrarGroupBoxesRespuesta( not SinEmailDS.IsEmpty, not ErroresDS.IsEmpty );
                  end;

                  cxStatus.Caption := VACIO;

                  if respuestaTimbramex.Resultado.Errores then
                  begin
                    zError( TIMBRADO_ERR_TIMBRAMEX , respuestaTimbramex.Bitacora.Eventos.Text  , 0);
                    cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                    Result := FALSE;
                  end
                  else
                  if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                  begin
                     cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                     zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX , 0);
                  end
                  else
                  begin
                       cxStatus.Caption := VACIO;
                       dxWizardControl1.Buttons.Finish.Enabled := False;
                       dxWizardControl1.Buttons.Cancel.Caption := 'Salir';
                       cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;
                       REsult := TRUE;
                       TerminarProgress;
                  end;
             end;
          end
          else
          begin
              cxStatus.Caption := VACIO;
              zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
          end;
       except on Error: Exception do
                 zError( Self.Caption, K_SIN_CONECTIVIDAD , 0);
       end;


       Screen.Cursor := oCursor;

  end;

var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     Result := GetRecibosServer;

     Screen.Cursor := oCursor;
end;
Procedure TWizardEnvioRecibosTimbradoForm.AgregaFiltroOrden;
begin
    with dmCatalogos.cdsFolios do
     begin
          Conectar;
          first;
          while (not eof) do
          begin
               if( fieldByName('FL_DESCRIP').AsString <> VACIO) then
               begin
                    cbOrden.Lista.Add( Format( 'NOMINA.NO_FOLIO_%d=%s' , [fieldByName('FL_CODIGO').asInteger, fieldByName('FL_DESCRIP').AsString] ));
               end;
               next;
          end;
     end;
end;
procedure TWizardEnvioRecibosTimbradoForm.FormCreate(Sender: TObject);



    procedure LLenarComboOrden;
    var
       iNivel : integer;
       sNivelName : string;
    begin

      with cbOrden.Lista do
      begin
          BeginUpdate;
          try
             Clear;
             Add( 'NOMINA.CB_CODIGO=N�mero de Empleado' );
             for iNivel := 1 to 9 do
             begin
                sNivelName := Global.GetGlobalString( K_GLOBAL_NIVEL1 + iNivel -1 );
                if strLleno( sNivelName ) then
                begin
                     Add( Format( 'NOMINA.CB_NIVEL%d=%s' , [iNivel, sNivelName] ));
                end;
             end;
          finally
                 EndUpdate;
          end;
      end;
      AgregaFiltroOrden;
      cbOrden.ItemIndex := 0;
      cbOrden.Llave := 'NOMINA.CB_CODIGO';
    end;

    procedure InicializarFiltros;
    begin

     FEmpleadoCodigo := 'NOMINA' + '.CB_CODIGO';
     FParameterList := TZetaParams.Create;
     FDescripciones := TZetaParams.Create;
     ZBasicoSelectGrid_DevEx.ParametrosGrid := FDescripciones;

     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := VACIO; //IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     dmCatalogos.cdsCondiciones.Conectar;
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;

     LLenarComboOrden;

    end;

begin

     HelpContext:= H32136_EnvioRecibos;
     cxStatus.Caption := VACIO;


     InicializarFiltros;

     //MostrarGroupBoxesRespuesta( False, False );
     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
              // sStatusNomina.Caption := //;;ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
               FechaInicial.Caption := FormatDateTime( FormatSettings.LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( FormatSettings.LongDateFormat, Fin );
          end;
     end;


     DatasetPeriodo.DataSet := dmInterfase.cdsPeriodosAfectadosTotal;

     if  dmInterfase.cdsPeriodosRecibos.IsEmpty  then
    begin
         dmCliente.RazonSocial := VACIO;
    end
    else
    begin
         dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosRecibos.FieldByName('RS_CODIGO').AsString, []) ;
         dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
    end;

     dataSourceTimbrar.DataSet :=  dmInterfase.cdsPeriodosRecibos;
     dmTablas.cdsEstado.Conectar;
     CT_CODIGO.LookupDataset := dmInterfase.cdsCuentasTimbramex;
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
      //cxGroupBoxResultados.Visible := FALSE;

end;

procedure TWizardEnvioRecibosTimbradoForm.Button1Click(Sender: TObject);
begin
     //memoPreview.Text :=  dmInterfase.GetRecibos;
end;

procedure TWizardEnvioRecibosTimbradoForm.dxWizardControl1PageChanging(Sender: TObject;
  ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
begin

  if ( AnewPage = dxWizardControlPageEnvio ) then
  begin
       with dmCliente do
       begin
            with GetDatosPeriodoActivo do
            begin
                 Advertencia.Caption := 'Al aplicar el proceso se gestionar� el env�o por correo electr�nico de los recibos de los empleados correspondientes al periodo'
                 +' '+iTipoNomina.Caption+' '+  iNumeroNomina.Caption +' del a�o '+IntToStr(Year)+', contribuyente '+dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString +'.';
            end;
       end;

       if(dmCliente.EsTRESSPruebas) then
       begin
            DxWizardControl1.buttons.Finish.Enabled := False;
       end;
          if  dmInterfase.cdsPeriodosRecibos.IsEmpty  then
          begin
               dmCliente.RazonSocial := VACIO;
          end
          else
          begin
               dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosRecibos.FieldByName('RS_CODIGO').AsString, []) ;
               dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
          end;

          CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
          AAllow := TRUE;

          if StrVacio( CT_CODIGO.Llave ) then
          begin
               zError( Self.Caption, 'No se ha especificado la Cuenta de Timbrado', 0  );
               AAllow := FALSE;
          end;

          FVerificacion := FALSE;

  end;

end;

procedure TWizardEnvioRecibosTimbradoForm.CT_CODIGOValidLookup(Sender: TObject);
begin
      DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;




procedure TWizardEnvioRecibosTimbradoForm.IniciarProgress;
begin

     cxProgressBarEnvio.Properties.Min := 0.00;
          cxProgressBarEnvio.Properties.Max := 100.00;  // puede ser por la cantidad de empleados en nomina
          cxProgressBarEnvio.Position := 0.00;
          cxProgressBarEnvio.Update;

     cxProgressBarEnvio.Visible := TRUE;
     cxLabelAvance.Visible := TRUE;
     cxStatus.Visible := TRUE;

end;

procedure TWizardEnvioRecibosTimbradoForm.TerminarProgress;
begin
     {cxProgressBarEnvio.Visible := FALSE;
     cxLabelAvance.Visible := FALSE;
     cxStatus.Visible := FALSE;}
end;


procedure TWizardEnvioRecibosTimbradoForm.btCancelarDescargaClick(
  Sender: TObject);
begin
     FContinuaDownload := FALSE;
end;

procedure TWizardEnvioRecibosTimbradoForm.SeleccionarClick(Sender: TObject);
var
   lCarga: Boolean;
   lOk: Boolean;
begin
     inherited;
     if Verificacion then
     begin
          case ZConfirmaVerificacion.ConfirmVerification of
               mrYes:
               begin
                    lCarga := True;
                    lOk := True;
               end;
               mrNo:
               begin
                    lCarga := False;
                    lOk := True;
               end;
          else
              begin
                   lCarga := False;
                   lOk := False;
              end;
          end;
     end
     else
     begin
          lCarga := True;
          lOk := True;
     end;
     if lOk then
     begin
          if lCarga then
          begin
               CargaListaNominas;
          end;
          SetVerificacion( Verificar );
     end;
end;

procedure TWizardEnvioRecibosTimbradoForm.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardEnvioRecibosTimbradoForm.BFinalClick(Sender: TObject);
begin
    with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardEnvioRecibosTimbradoForm.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TWizardEnvioRecibosTimbradoForm.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( EntidadActiva, Text, SelStart, evBase );
     end;
end;
function TWizardEnvioRecibosTimbradoForm.EntidadActiva;
begin
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
          result := enEmpleado;
     end
     else
     begin
          result := enEmpTimb;
     end;

end ;

function TWizardEnvioRecibosTimbradoForm.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
           if ZetaBuscaEmpleado_DevExTimbrado.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
           begin
                if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                   Result := sLlave + ',' + sKey
                else
                    Result := sKey;
           end;
     end
     else
     begin
          if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
           begin
                if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                   Result := sLlave + ',' + sKey
                else
                    Result := sKey;
           end;
     end;
end;

procedure TWizardEnvioRecibosTimbradoForm.FormDestroy(Sender: TObject);
begin
     FDescripciones.Free;
     FParameterList.Free;


end;

procedure TWizardEnvioRecibosTimbradoForm.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;


procedure TWizardEnvioRecibosTimbradoForm.EnabledBotones(
  const eTipo: eTipoRangoActivo);
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;

procedure TWizardEnvioRecibosTimbradoForm.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );

          AddString( 'RazonSocial',  GetRazonSocial );
          AddInteger( 'StatusAnterior', ord(estiTimbrado) );
          AddInteger( 'StatusNuevo',  ord(estiTimbrado) ) ;

          AddInteger( 'Year',dmCliente.YearDefault );
          AddInteger( 'Tipo',ord( dmCliente.PeriodoTipo )  );
          AddInteger( 'Numero', dmCliente.PeriodoNumero );

          AddString( 'Orden', cbOrden.Llave );
     end;

     with Descripciones do
     begin
          AddString( 'Lista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;

     with dmCliente do
     begin
          CargaActivosIMSS( ParameterList );
          CargaActivosPeriodo( ParameterList );
          CargaActivosSistema( ParameterList );
     end;
end;

function TWizardEnvioRecibosTimbradoForm.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          if ( global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) )then
             Result := stringReplace(Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString ), 'COLABORA', 'V_EMP_TIMB' ,[rfReplaceAll, rfIgnoreCase])
          else
              Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
     end
     else
         Result := '';
end;

function TWizardEnvioRecibosTimbradoForm.GetFiltro: String;
begin
  Result := Trim( sFiltro.Text );
end;

function TWizardEnvioRecibosTimbradoForm.GetRango: String;
var
   sl : TStringList;
   i : integer;
   sLista : string;
begin

     sl := TStringList.Create;
     sl.CommaText :=  ELista.Text;
     sLista := VACIO;
     for i:=0 to sl.Count -1  do
     begin
          if StrLleno( sl[i] ) then
          begin
               if StrVacio( sLista)  then
                  sLista := sl[i]
               else
                  sLista :=  sLista + ','+ sl[i];
          end;
     end;
     sl.Free;

     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FEmpleadoCodigo, Trim( sLista  ) );
     else
         Result := '';
     end;
end;

procedure TWizardEnvioRecibosTimbradoForm.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TWizardEnvioRecibosTimbradoForm.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TWizardEnvioRecibosTimbradoForm.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;


procedure TWizardEnvioRecibosTimbradoForm.CargaListaVerificacion;
begin
     dmProcesos.TimbrarNominaGetLista( ParameterList );
end;

function TWizardEnvioRecibosTimbradoForm.Verificar: Boolean;
begin
     if ( cbOrden.ItemIndex = 0 ) then
        FEmpleadoTimbradoGridSelect.FTituloOrden := VACIO
     else
         FEmpleadoTimbradoGridSelect.FTituloOrden := cbOrden.Descripcion;

     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataSetTimbrados, TEmpleadoTimbradoGridSelect );
end;

function TWizardEnvioRecibosTimbradoForm.GetRazonSocial: string;
begin
    Result := dmCliente.RazonSocial;
end;

procedure TWizardEnvioRecibosTimbradoForm.gridPeriodosDBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
 		ZGridModeTools.BorrarItemGenericoAll( AValueList );
     		if gridPeriodosDBTableView1.DataController.IsGridMode then
        		ZGridModeTools.FiltroSetValueLista( gridPeriodosDBTableView1, AItemIndex, AValueList );
end;

procedure TWizardEnvioRecibosTimbradoForm.CargaListaNominas;
var
 oCursor : TCursor;
begin
    oCursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
       CargaParametros;
       CargaListaVerificacion;
    finally
           Screen.Cursor := oCursor;
    end;
end;

procedure TWizardEnvioRecibosTimbradoForm.ZetaSpeedButton2Click(
  Sender: TObject);
 var
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Empleados Timbrados sin Correo Electr�nico',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin
      if ZetaCXGrid2DBTableView1.DataController.DataSetRecordCount > 0 then
      begin
           if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
           begin
                Valor := ValoresGrid;
                FBaseReportes_DevEx.ExportarGrid( ZetaCXGrid2DBTableView1,
                                            ZetaCXGrid2DBTableView1.DataController.DataSource.DataSet, Caption, 'IM',
                                            Valor[0],Valor[1],Valor[2],Valor[3]);
           end;
      end
      else
      begin
           //ZError('Error en '+Self.Caption, 'El Grid de Errores est� Vac�o', 0);
           ZetaDialogo.ZError('Error en '+Self.Caption, '�Se encontr� un Error! '+ CR_LF + 'El grid de empleados sin correo electr�nico est� vac�o',0)
      end;
end;


procedure TWizardEnvioRecibosTimbradoForm.ZetaSpeedButton1Click(Sender: TObject);
 var
    lExporta: Boolean;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Empleados Timbrados sin Correo Electr�nico',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin
      if ZetaCXGrid1DBTableView4.DataController.DataSetRecordCount > 0 then
      begin
           if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
           begin
                Valor := ValoresGrid;
                FBaseReportes_DevEx.ExportarGrid( ZetaCXGrid1DBTableView4,
                                            ZetaCXGrid1DBTableView4.DataController.DataSource.DataSet, Caption, 'IM',
                                            Valor[0],Valor[1],Valor[2],Valor[3]);
           end;
      end
      else
      begin
           //ZError('Error en '+Self.Caption, 'El Grid de Errores est� Vac�o', 0);
           ZetaDialogo.ZError('Error en '+Self.Caption, '�Se encontr� un Error! '+ CR_LF + 'El grid de respuesta del sistema de timbrado est� vac�o',0)
      end;


end;


procedure TWizardEnvioRecibosTimbradoForm.FormShow(Sender: TObject);
begin
     ZetaCXGrid1DBTableView1.ApplyBestFit();
if(dmCliente.EsTRESSPruebas) then
     begin
          DxWizardControl1.buttons.Finish.Enabled := False;
     end;
end;

end.


