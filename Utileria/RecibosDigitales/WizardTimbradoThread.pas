unit WizardTimbradoThread;
{$define MULTIPAQUETE}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,                                                          
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,ZGridModeTools,
  Timbres, Recolector,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaDBTextBox, ExtCtrls, ZetaKeyLookup, Mask,
  DBCtrls,FTimbramexClasses, FTimbramexHelper, Grids, DBGrids, Buttons,
  ZetaSmartLists, FBaseReportes_DevEx, UManejadorPeticiones, UPeticionWS, UThreadStack, UThreadPolling, UInterfaces,Registry,
  {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     ZetaCommonLists,ZetaCommonClasses, ZetaDBGrid, TressMorado2013, ZetaCXGrid,
  ZetaKeyLookup_DevEx, ZetaKeyCombo, ZetaEdit,
  cxRadioGroup, cxMemo, ZetaFecha, dxCheckGroupBox, dxGDIPlusClasses, cxImage,
  cxCheckGroup, Vcl.ImgList, cxCheckListBox,
  FProcesoTimbradoPendiente;

type
  TWizardTimbradoThreadForm = class(TdxWizardControlForm, ILogBitacora, INotificarStatusReporte)
    dxWizardControl1: TdxWizardControl;
    dxWizardControlPageTimbrado: TdxWizardControlPage;
    cxGroupBox1: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxProgressBarEnvio: TcxProgressBar;
    cxStatus: TcxLabel;
    dxWizardControlPageInicio: TdxWizardControlPage;
    gridPeriodos: TZetaCXGrid;
    gridPeriodosDBTableView1: TcxGridDBTableView;
    gridPeriodosDBTableView1Column10: TcxGridDBColumn;
    gridPeriodosDBTableView1Column2: TcxGridDBColumn;
    gridPeriodosDBTableView1Column9: TcxGridDBColumn;
    gridPeriodosDBTableView1Column4: TcxGridDBColumn;
    gridPeriodosDBTableView1Column5: TcxGridDBColumn;
    gridPeriodosDBTableView1Column8: TcxGridDBColumn;
    gridPeriodosLevel1: TcxGridLevel;
    dataSourceTimbrar: TDataSource;
    dxWizardControlPageCuentaTimbrado: TdxWizardControlPage;
    dataSourceContribuyente: TDataSource;
    DataSourceCuentas: TDataSource;
    dxWizardControlPageRecibos: TdxWizardControlPage;
    cxGridRecibos: TZetaCXGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    DataSourceRecibos: TDataSource;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    cxCheckBox1: TcxCheckBox;
    cxGroupBoxResultados: TcxGroupBox;
    cxLabel3: TcxLabel;
    DataSourceErrores: TDataSource;
    cxGroupBox2: TcxGroupBox;
    PeriodoTipoLbl: TLabel;
    Label6: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label8: TLabel;
    DatasetPeriodo: TDataSource;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    iTipoNomina: TZetaTextBox;
    ZetaDBTextBox2: TZetaDBTextBox;
    gridPeriodosDBTableView1Column7: TcxGridDBColumn;
    gridPeriodosDBTableView1Column11: TcxGridDBColumn;
    cxLabel1: TcxLabel;
    cxSaldo: TcxMaskEdit;
    cxLabel5: TcxLabel;
    cxTimbrados: TcxMaskEdit;
    cxLabel6: TcxLabel;
    cxTimbrar: TcxMaskEdit;
    DataSourceTimbrados: TDataSource;
    DataSourceDetalle: TDataSource;
    Label1: TLabel;
    Label4: TLabel;
    cxCbDescripcion: TcxComboBox;
    CxGroupOpcionesEnvio: TcxGroupBox;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    ZetaCXGrid1DBTableView1NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1Level1: TcxGridLevel;
    ZetaCXGrid1DBTableView2: TcxGridDBTableView;
    ZetaCXGrid1DBTableView2NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2DETALLE: TcxGridDBColumn;
    dxWizardControlFiltroEmpleados: TdxWizardControlPage;
    FiltrosGB: TGroupBox;
    sCondicionLBl: TLabel;
    sFiltroLBL: TLabel;
    sFiltro: TcxMemo;
    Seleccionar: TcxButton;
    BAgregaCampo: TcxButton;
    ECondicion: TZetaKeyLookup_DevEx;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    RBTodos: TcxRadioButton;
    RBRango: TcxRadioButton;
    RBLista: TcxRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    BInicial: TcxButton;
    BFinal: TcxButton;
    BLista: TcxButton;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    cbOrden: TZetaKeyCombo;
    cxButton1: TcxButton;
    CxAjustaFechaPago: TdxCheckGroupBox;
    PE_FECH_PAG: TZetaFecha;
    cxEnviarDetalle: TcxCheckBox;
    cxEnviarIncapacidades: TcxCheckBox;
    cxEnviarTiempoExtra: TcxCheckBox;
    cxButton2: TcxButton;
    ZetaDBTextBox3: TZetaDBTextBox;
    lblDI_IP: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    cxErrores: TcxMaskEdit;
    cxLabel2: TcxLabel;
    ZetaSpeedButton1: TcxButton;
    Panel3: TPanel;
    Advertencia: TcxLabel;
    cxImage1: TcxImage;
    ZetaDBTextBox1: TZetaDBTextBox;
    Label9: TLabel;
    Label2: TLabel;
    CT_CODIGO: TZetaKeyLookup_DevEx;
    cxEnviarSubContratos: TcxCheckBox;
    Label7: TLabel;
	cxPeriodicidadPago: TZetaKeyCombo;
    dxWizardControlPageReportes: TdxWizardControlPage;
    cxCheckListReportes: TcxCheckListBox;
    cxImageListReportes: TcxImageList;
    lblUso: TLabel;
    UsoPeriodo: TZetaTextBox;
    dxWizardControlPageOpciones: TdxWizardControlPage;
    LblFechaPago: TLabel;
    cxEsUnaNominaAjuste: TdxCheckGroupBox;
    lblNominaOriginal: TLabel;
    lookupNominaOriginal: TZetaKeyLookup_DevEx;
    procedure cxButton1Click(Sender: TObject);
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure CT_CODIGOValidLookup(Sender: TObject);
        procedure Exportar;
    procedure ZetaSpeedButton1Click(Sender: TObject);
    procedure ZetaSpeedButton2Click(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure ExportarExcelEmpleadosClick(Sender: TObject);
    procedure CxAjustaFechaPagoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gridPeriodosDBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure DataSourceErroresUpdateData(Sender: TObject);
    procedure DataSourceErroresDataChange(Sender: TObject; Field: TField);
    procedure dxWizardControl1PageChanged(Sender: TObject);
    procedure cxCheckListReportesDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cxEsUnaNominaAjusteClick(Sender: TObject);
  private
   FContinuaDownload : boolean;
   FEjecuto : boolean;
   FVerificacion: Boolean;
   FParametrosFecha : TZetaParams;
   FParametrosPeriodicidad : TZetaParams;
   FTipoRango: eTipoRangoActivo;
   FEmpleadoCodigo: String;
   FEmpleadoFiltro: String;
   FTimbrandoNominas : boolean;
   FTimbrandoNominasCenacelar : boolean;

   FAllowTimbrarHayDatos : boolean;
   FRecargarReportes : boolean;



   FParameterList: TZetaParams;
   FDescripciones: TZetaParams;
   //Folios Repetidos
   FParamsTimbradoNomina: TZetaParams;
    { Private declarations }
        {$ifdef DOS_CAPAS}
    oZetaProvider: TdmZetaServerProvider;
     {$endif}
    function TimbrarNomina : boolean;
    function GetRecibosDataSet : boolean;
    function BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
    function MostrarAdvertenciaTimbradoNomina: Boolean;
    procedure InitStatusReporte;
    procedure TomarCuentaTimbradoSeleccionadoGrid;//US 16927
    function LeerURLRegistro(var sValorRegistro, sRuta: string) : boolean;
    procedure AddValorRegistro(sValor, sRuta, sLlave: string);
    procedure AddLlaveTimbradoTerminalesRegistro(sValor, sLlave : string);
    function ConsultaRegistroFechaTerminales(var sAccion : string): boolean;

  protected
    function GetRango: String;
    function GetCondicion: String;
    function GetFiltro: String;
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );
  public
    { Public declarations }
    property TipoRango: eTipoRangoActivo read FTipoRango;
    property ParamsTimbradoNomina: TZetaParams read FParamsTimbradoNomina write FParamsTimbradoNomina;
    procedure EscribirBitacoraLog( sMensaje : string );
    procedure EscribirBitacoraError( sMensaje : string );
    Function EntidadActiva:integer;

    function GetStatusReporteItemNo(sTitulo : string) : integer;
    function GetStatusReporteItemNo2(sTitulo : string) : integer;
    function NotificarStatusReporte(sTitulo, sDescripcion, sDetalle : string; lStatusOk : integer; lEnable: Boolean = TRUE  ) : boolean;
  end;

  procedure MensajePaqueteTimbrado( sMensaje : string );

var
  WizardTimbradoThreadForm: TWizardTimbradoThreadForm;


function ShowWizardTimbrado : boolean;

implementation

uses ZetaClientTools,DTablas, DGlobal, ZGlobalTress,  Dcliente, ZetaCommonTools,
      ZetaBuscaEmpleado_DevExTimbrado,ZConstruyeFormula,ZetaTipoEntidad,DCatalogos,
     ZDiccionTools,ZetaBuscaEmpleado_DevEx, RegularExpressions, ZetaDialogoLink,
     ZReportConst, ZetaBuscaEmpleado_DevExAvanzada, ZetaBuscaEmpleado_DevExTimbradoAvanzada,MensajesTimbradoWebServices,
     ZetaDialogoWebTimbrado;


{$R *.dfm}
function ShowWizardTimbrado : boolean;
begin
    Result := FALSE;
end;

function TWizardTimbradoThreadForm.EntidadActiva;
begin
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
          result := enEmpleado;
     end
    else
    begin
         result := enEmpTimb;
     end;
end;

procedure TWizardTimbradoThreadForm.cxButton1Click(Sender: TObject);
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
var
   sEchoResponse : string;
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;
begin

     try


        timbresSoap := nil;
        sServer := GetTimbradoServer;
        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
           if ( timbresSoap <> nil ) then
           begin
                crypt := TWSTimbradoCrypt.Create;
                sEchoResponse := crypt.Desencriptar(  timbresSoap.Echo( crypt.Encriptar(TESTING_MESSAGE) ) ) ;
				FreeAndNil( crypt );				
                //{$ifdef WSPROFILE}WSProfile( 'ECHO', sEchoResponse ) ;{$endif}

                ZInformation( Self.Caption, 'Hay Conectividad con el Servidor de Timbrado ', 0);
           end;
        end
        else
        begin
           zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
        end;
     except on Error: Exception do
               zError( Self.Caption, K_SIN_CONECTIVIDAD, 0);
     end;


end;

procedure TWizardTimbradoThreadForm.cxCheckListReportesDrawItem(
  Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  ACanvas: TcxCanvas;
  AText: string;
  ATextRect: TRect;
  AGlyphWidth: Integer;
  AListBox: TcxCheckListBox;
  ACheckBmp: TBitmap;
  ACanvasFont: TFont;
  AItemState: TcxCheckBoxState;
  AItemEnabled: Boolean;
begin
  AListBox := (Control as TcxCheckListBox);
  ACanvas := AListBox.InnerCheckListBox.Canvas;
  ACanvasFont := ACanvas.Font;
  AItemState := AListBox.Items[Index].State;
  AItemEnabled := AListBox.Items[Index].Enabled;

  ACanvasFont.Color := clBlack;

  case  AListBox.Items[Index].ImageIndex  of
     K_STATUS_REPORTE_IDLE :
      begin
        ACanvasFont.Color := clBlack;
      end;
    K_STATUS_REPORTE_OK :
      begin
        //ACanvasFont.Style := [fsBold];
        ACanvasFont.Color := clBlack;
      end;
    K_STATUS_REPORTE_ERROR :
      begin
        ACanvasFont.Color := clRed;
      end;
    K_STATUS_REPORTE_HOLD :
      begin
        ACanvasFont.Color := clBlack;
      end;
    K_STATUS_REPORTE_EXECUTING :
      begin
        ACanvasFont.Color := clBlack;
      end;
  end;

  if not AItemEnabled then
    ACanvasFont.Color := clGray;

  ACanvas.Brush.Color := clWhite;
  ACanvas.FillRect(Rect);

  AGlyphWidth := 0;
  AText := AListBox.Items[Index].Text;
  ATextRect := Rect;
  ATextRect.Left := ATextRect.Left + 3 + AGlyphWidth;
   ACanvas.Brush.Color :=  ACanvasFont.Color ;
  ACanvas.DrawTexT(AText, ATextRect, 0);
end;


procedure TWizardTimbradoThreadForm.DataSourceErroresDataChange(Sender: TObject;
  Field: TField);
begin
       ZetaCXGrid1DBTableView2.ApplyBestFit();
end;

procedure TWizardTimbradoThreadForm.DataSourceErroresUpdateData(
  Sender: TObject);
begin
     ZetaCXGrid1DBTableView2.ApplyBestFit();
end;

procedure TWizardTimbradoThreadForm.cxEsUnaNominaAjusteClick(Sender: TObject);
begin
    lookupNominaOriginal.Enabled :=  cxEsUnaNominaAjuste.CheckBox.Checked;
    lblFechaPago.Enabled         :=  cxEsUnaNominaAjuste.CheckBox.Checked;
end;

procedure TWizardTimbradoThreadForm.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin

  if AKind = wcbkCancel then
  begin
//    ModalResult := mrCancel;
    if ( FTimbrandoNominas ) then
    begin
         ModalResult := mrCancel;
         if ZetaDialogo.ZConfirm( self.Caption, '�Desea cancelar el proceso de timbrado de recibos de n�mina?', 0, mbNo ) then
         begin
              MensajePaqueteTimbrado( 'Cancelando proceso de timbrado de recibos de n�mina...' );
              FTimbrandoNominas := FALSE;
         end;
    end
    else
    begin
         if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
         begin
               if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
               begin
                    ModalResult := mrCancel;
               end;
         end
         else
         begin
              ModalResult := mrCancel;
         end;
    end;
  end
  else if AKind = wcbkFinish then
  begin

    dxWizardControl1.Buttons.Finish.Enabled := False;
    if TimbrarNomina then
    begin
         ModalResult := mrOk;
    end
    else
    begin
         dxWizardControl1.Buttons.Finish.Enabled := FALSE;
         dxWizardControl1.Buttons.Back.Enabled := FALSE;
         dxWizardControl1.Buttons.Cancel.Caption := 'Salir';

    end;

  end;

end;

function TWizardTimbradoThreadForm.TimbrarNomina: boolean;

  procedure AgregaParametros;
  begin
       if(CxAjustaFechaPago.CheckBox.Checked) then
            FparametrosFecha.AddDate('FechaAjuste',PE_FECH_PAG.Valor);

       if (cxEsUnaNominaAjuste.CheckBox.Checked) and (  strLleno( lookupNominaOriginal.Llave) ) and ( not dmCatalogos.cdsPeriodo.IsEmpty) then
       begin
             FparametrosFecha.AddInteger('OriginalYear',  dmCatalogos.cdsPeriodo.FieldByName('PE_YEAR').AsInteger) ;
             FparametrosFecha.AddInteger('OriginalTipo', dmCatalogos.cdsPeriodo.FieldByName('PE_TIPO').AsInteger) ;
             FparametrosFecha.AddInteger('OriginalNumero', dmCatalogos.cdsPeriodo.FieldByName('PE_NUMERO').AsInteger) ;
       end;


       FParametrosPeriodicidad.AddInteger('PeriodicidadNomina', cxPeriodicidadPago.ItemIndex );
  end;

  procedure LimpiaParametros;
  begin
       FparametrosFecha.clear;
       FParametrosPeriodicidad.Clear;
  end;

//Metodo que se iria a una Helper Class
  function   TimbrarNominaServer : boolean;
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta, sLinkObtenido:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;
     iCount : integer;
     iShowBuzz: integer;
     broker : TManejadorPeticiones ;

     oPeticionWS : TPeticionWS;

     slPaquetes : TStringList;
     iPaquete : integer;
     oTimbradoPendiente: TProcesoTimbradoPendiente;

     procedure EsperarPeticion;
     var
        iCount : integer;
     begin
         iCount := 0;
         while oPeticionWS.lProceso do
         begin
           Inc( iCount );
           if not FTimbrandoNominas then
           begin
                oPeticionWS.lCancelar := TRUE;
                if Assigned( broker )
                then
                    broker.Destroy;
                break;
           end;
           if ( iCount mod 4  = 0 ) then
              Application.ProcessMessages
           else
               DelayNPM( 500  );
         end;
         Application.ProcessMessages;
     end;
  begin

     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
      broker := TManejadorPeticiones.Create( ILogBitacora( Self ) );
      sPeticion := VACIO;
      sRespuesta := VACIO;

     try


          timbresSoap := nil;
          cxProgressBarEnvio.Properties.Min := 0.00;
          cxProgressBarEnvio.Properties.Max := 100.00;  // puede ser por la cantidad de empleados en nomina
          cxProgressBarEnvio.Position := 0.00;
          cxProgressBarEnvio.Update;
          cxErrores.Text := VACIO;
          cxSaldo.Text := VACIO;
          cxTimbrados.Text := VACIO;
          DatasourceErrores.DataSet := nil;
          DatasourceTimbrados.DataSet := nil;

          if ( GetTimbradoServer <> '' ) then
          begin
             cxStatus.Caption := 'Conectando a Servidor...';
             dmInterfase.MarcarNomina;
             Application.ProcessMessages;

             timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
             if ( timbresSoap <> nil ) then
             begin

                 if ( not dmInterfase.cdsRecibosTimbrar.Active) or ( dmInterfase.cdsRecibosTimbrar.IsEmpty ) then
                 begin
                      ZWarning( Self.Caption, ' No hay Recibos por Timbrar, Se recalcul� Status de Timbrado', 0, mbOK);
                 end
                 else
                 begin
                     {$ifdef MULTIPAQUETE}
                     oPeticionWS := TPeticionWS.Create;

                     AgregaParametros;
                     oPeticionWS.tipoPeticion := peticionTimbrado;

                     slPaquetes := dmInterfase.GetRecibosList(FparametrosFecha, FParametrosPeriodicidad, cxEnviarDetalle.Checked, cxCbDescripcion.Text , cxEnviarIncapacidades.Checked,  cxEnviarTiempoExtra.Checked, cxEnviarSubContratos.Checked, CxAjustaFechaPago.CheckBox.Checked ) ;

                     //Timbrado pendiente de Nomina
                     oTimbradoPendiente := TProcesoTimbradoPendiente.Create;
                     oTimbradoPendiente.ParamsTimbradoNomina := FParamsTimbradoNomina;
                     oTimbradoPendiente.SetEstatusPendienteEmpleados;
                     oTimbradoPendiente.Destroy;

                     {$ifdef PRUEBAS_TIMBRADO_PENDIENTE}
                     if ZetaDialogo.ZConfirm( self.Caption, '�Desea cancelar proceso timbrado?', 0, mbNo ) then
                     begin
                          raise Exception.CreateFmt('Invalid name : ''%s''', ['prueba']);
                     end;
                     {$endif}
                     LimpiaParametros;

                     cxTimbrar.Text := Format('%d', [dmInterfase.cdsRecibosTimbrar.RecordCount] );
                     cxStatus.Caption := 'Enviando Recibos...';
                     cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                     cxSaldo.Text := '';
                     Application.ProcessMessages;
                     FTimbrandoNominas := TRUE;
                     for iPaquete := 0 to  slPaquetes.Count -1 do
                     begin
                          sPeticion := slPaquetes[iPaquete];
                        //  {$ifdef WSPROFILE}WSProfile( 'PETICION', sPeticion ) ;{$endif}
                          FTimbramexClasses.FAplicarTimeoutTimbrado := true;
                          oPeticionWS := TPeticionWS.Create;
                          oPeticionWS.tipoPeticion := peticionTimbrado;
                          oPeticionWs.cnxSoap := timbresSoap;
                          oPeticionWs.ACallbackMensaje := MensajePaqueteTimbrado;
                          oPeticionWS.sPeticion :=  slPaquetes[iPaquete];
                          oPeticionWS.sURL := GetTimbradoURL;
                          if not FTimbrandoNominas then
                             oPeticionWS.lCancelar := TRUE;
                          oPeticionWS.lProceso := TRUE;
                          broker.AgregarPeticionWS( oPeticionWS );
                          EsperarPeticion; //Esperar la confirmacion de la finalizacion del Thread
                          if not FTimbrandoNominas then break;
                          if FTimbrandoNominas then
                          begin
                               sRespuesta := oPeticionWS.sRespuesta;
                               respuestaTimbramex := FTimbramexHelper.GetTimbradoRespuesta( sRespuesta );

                               if ( not dmInterfase.HuboRespuestaTimbramex ) then
                               begin
                                    dmInterfase.RespuestaTimbramex := respuestaTimbramex;
                                    dmInterfase.HuboRespuestaTimbramex := TRUE;
                                    DatasourceErrores.DataSet := dmInterfase.RespuestaTimbramex.Resultado.ErroresDS;
                                    DataSourceTimbrados.DataSet :=dmInterfase.RespuestaTimbramex.Resultado.FacturasDS;
                                    //cxGroupBoxResultados.Visible := not dmInterfase.RespuestaTimbramex.Resultado.ErroresDS.IsEmpty;
                               end
                               else
                               begin
                                    AcumulaRespuestaTimbrado( dmInterfase.RespuestaTimbramex,  respuestaTimbramex );
                               end;

                               cxSaldo.Text :=respuestaTimbramex.Resultado.Saldo;;
                               cxProgressBarEnvio.Position :=  trunc( ( (iPaquete+ 1.0) / (slPaquetes.Count*1.0) ) * 100.00) ;  cxProgressBarEnvio.Update;

                               if DatasourceErrores.DataSet <> nil then
                                 cxErrores.Text := Format('%d', [DatasourceErrores.DataSet.RecordCount]);
                               if DataSourceTimbrados.DataSet <> nil then
                                 cxTimbrados.Text := Format('%d', [DataSourceTimbrados.DataSet.RecordCount]);

                               Application.ProcessMessages;

                               DelayNPM( 100 );

                               if respuestaTimbramex.Resultado.TX_STATUS = STATUS_TIMBRADO_ERROR_GRAVE then
                               begin
                                  dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta ) ;
                                  break;
                               end;
                          end;
                     end;

                     //FTimbrandoNominas := FAlSE;
                     FreeAndNil( slPaquetes );

                     //cxGroupBoxResultados.Visible := not dmInterfase.RespuestaTimbramex.Resultado.ErroresDS.IsEmpty;


                     if FTimbrandoNominas then
                     begin
                          respuestaTimbramex := dmInterfase.RespuestaTimbramex;

                          if dmInterfase.HuboRespuestaTimbramex   then
                          begin
                              if respuestaTimbramex.Resultado.Errores then
                              begin
                                zError( TIMBRADO_ERR_TIMBRAMEX , TIMBRADO_NOPOSIBLE_TIMBRAMEX + char(10)+char(13)+  respuestaTimbramex.Bitacora.Eventos.Text  , 0);
                                dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + char(10)+char(13)+ respuestaTimbramex.Bitacora.Eventos.Text + char(10)+ char(13) + 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta ) ;
                                cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                                Result := FALSE;
                              end
                              else
                              begin
                                   cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;

                                   //if (  cxGroupBoxResultados.Visible  ) then
                                   if (  not dmInterfase.respuestaTimbramex.Resultado.ErroresDS.IsEmpty ) then
                                   begin
                                      Result := FALSE;
                                     /// ModalResult := mr;
                                   end
                                   else
                                   begin
                                       if StrLleno( cxSaldo.Text ) then
                                          ZInformation(Self.Caption  + ', �xito al Timbrar N�mina','�xito al Timbrar N�mina, Su Saldo del Sistema de Timbrado es de '+ cxSaldo.Text, 0 );
                                       REsult := TRUE;
                                   end;

                              end;
                          end;
                     end;

                 {$else}
                          cxStatus.Caption := 'Formando Recibos...';
                          cxProgressBarEnvio.Position := 20.00;  cxProgressBarEnvio.Update;
                         Application.ProcessMessages;

                         {$ifdef WSPROFILE}WSProfile( 'PETICION', sPeticion ) ;{$endif}
                          sPeticion := dmInterfase.GetRecibos;
                          cxProgressBarEnvio.Position := 40.00;  cxProgressBarEnvio.Update;
                         cxStatus.Caption := 'Enviando Recibos al servidor...';

                         oPeticionWS := TPeticionWS.Create;
                         oPeticionWS.tipoPeticion := peticionTimbrado;
                         oPeticionWs.cnxSoap := timbresSoap;
                         oPeticionWS.sPeticion :=  sPeticion;
                         oPeticionWS.sURL := GetTimbradoURL;

                         oPeticionWS.lProceso := TRUE;
                         broker.AgregarPeticionWS( oPeticionWS );

                         iCount := 0;
                         while oPeticionWS.lProceso do
                         begin
                           Inc( iCount );
                           iShowBuzz := iCount mod 4;
                           cxProgressBarEnvio.Position := iShowBuzz * 20;  cxProgressBarEnvio.Update;
                           DelayNPM( 500  );
                           if ( iCount mod 4  = 0 ) then
                              Application.ProcessMessages;
                         end;
                         Application.ProcessMessages;
						 sRespuesta := oPeticionWS.sRespuesta;                         
                        {$ifdef WSPROFILE}  WSProfile( 'RESPUESTA', sRespuesta  ) ;  {$endif}

                         if( strLLeno( oPeticionWS.sError ) ) then
                         begin
                              ZWarning( Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + ' Reintente el Proceso', 0, mbOK);
                              REsult := FALSE;
                         end
                         else
                         begin

                             cxProgressBarEnvio.Position := 80.00;  cxProgressBarEnvio.Update;
                             respuestaTimbramex := FTimbramexHelper.GetTimbradoRespuesta( sRespuesta );

                             dmInterfase.HuboRespuestaTimbramex := TRUE;
                             dmInterfase.RespuestaTimbramex := respuestaTimbramex;

                             DatasourceErrores.DataSet := respuestaTimbramex.Resultado.ErroresDS;

                             //cxGroupBoxResultados.Visible := not respuestaTimbramex.Resultado.ErroresDS.IsEmpty;

                             if respuestaTimbramex.Resultado.Errores then
                             begin

                               zError( TIMBRADO_ERR_TIMBRAMEX , TIMBRADO_NOPOSIBLE_TIMBRAMEX + char(10)+char(13)+  respuestaTimbramex.Bitacora.Eventos.Text  , 0);
                               cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                               Result := FALSE;
                             end
                             else
                             begin
                                  cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;

                                   //if (  cxGroupBoxResultados.Visible  ) then
                                   if (  dmInterfase.respuestaTimbramex.Resultado.ErroresDS.IsEmpty ) then
                                  begin
                                     Result := FALSE;
                                    /// ModalResult := mr;
                                  end
                                  else
                                      REsult := TRUE;

                             end;
                         end;
                    {$endif}

                    if dmInterfase.HuboRespuestaTimbramex   then
                    begin
                         if DatasourceErrores.DataSet <> nil then
                            cxErrores.Text := Format('%d', [DatasourceErrores.DataSet.RecordCount]);

                    end;


                 end;
             end;
          end
          else
          begin
             zError( Self.Caption, TIMBRADO_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
             dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_SIN_CONECTIVIDAD + ' No hay servidor especificado ' );
          end;

       except on Error: Exception do
       begin
                 zError( Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX , 0);

                 dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + ' '+Error.Message  +   char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta  );


                // {$ifdef WSPROFILE}  WSProfile( 'RESPUESTA', Error.Message  ) ;  {$endif}
                 Result := FALSE;
       end;
       end;
       if ( FTimbrandoNominas ) then
          FreeAndNil( broker );
       freeAndNil(oPeticionWS);
       Screen.Cursor := oCursor;
       FTimbrandoNominas := FAlSE;

  end;


  procedure  EnviarCambiosConceptos;
  var
     sMensaje, sRespuesta  : string;
     recolectorSoap : EntradaSoap;
     respuestaRecolector : TRespuestaStruct;
     lEnviar : boolean;


     dFechaUltimoRegistro, dFecharegistroterminales : TDateTime;
     
     sXMLInfoTressFiles : string;
     sXMLInfoTerminales : string;
     sXMLInfoAnalitica : string;
     sError, sErrorTerminales, sErrorAnalitica, sAccion : string;
     lEnviarInfoTressFiles, lEnviarInfoTerminales, lEnviarInfoAnalitica : Boolean;

     sXMLLastDateBakcUp : string;
     sErrorLastDateBakcUp : string;
     lEnviarLastDateBakcUp : Boolean;

  begin
       try
          FTimbramexClasses.FAplicarTimeoutTimbrado := TRUE;
          recolectorSoap := Recolector.GetEntradaSoap(False,  FTimbramexHelper.GetRecolectorURL );
          if ( recolectorSoap <> nil ) then
          begin
               sMensaje := dmInterfase.GetMensajeAutorizacion;
               sRespuesta := recolectorSoap.Autorizar( sMensaje );
               respuestaRecolector := FTimbramexHelper.GetTimbradoRespuesta( sRespuesta );

               if ( not  respuestaRecolector.Resultado.Errores) and ( respuestaRecolector.recolector.Registrar) then
               begin
                  sMensaje := dmInterfase.GetCambiosConcepto(respuestaRecolector.recolector.UltimoRegistro ,   lEnviar)  ;
                  dFechaUltimoRegistro := respuestaRecolector.recolector.UltimoRegistro;

                  if ( lEnviar ) and  StrLleno( sMensaje )  then
                  begin
                     FTimbramexClasses.FAplicarTimeoutTimbrado := TRUE;
                     sRespuesta := recolectorSoap.Registrar( sMensaje );
                     respuestaRecolector := FTimbramexHelper.GetTimbradoRespuesta( sRespuesta );
                  end;

                  sErrorTerminales := VACIO;
                  lEnviarInfoTerminales := false;
                  dFecharegistroterminales := now;
                  sXMLInfoTerminales := VACIO;
                  sAccion := VACIO;

                  ConsultaRegistroFechaTerminales(sAccion);
                  if (sAccion <> 'No Enviar') then
                  begin
                        sXMLInfoTerminales := dmInterfase.GetRegistrosTerminales(true, dFecharegistroterminales, sErrorTerminales, lEnviarInfoTerminales);
                        if lEnviarInfoTerminales then
                        begin
                             sRespuesta := recolectorSoap.Registrar( sXMLInfoTerminales );
                        end;
                  end;


                  sErrorAnalitica := VACIO;
                  lEnviarInfoAnalitica := false;
                  sXMLInfoAnalitica := dmInterfase.GetRegistrosAnalitica(true, dFechaUltimoRegistro, sErrorAnalitica, lEnviarInfoAnalitica);
                  if lEnviarInfoAnalitica then
                  begin
                       sRespuesta := recolectorSoap.Registrar( sXMLInfoAnalitica );
                  end;

                  sError := VACIO;
                  lEnviarInfoTressFiles := false;
                  sXMLInfoTressFiles := dmInterfase.GetRegistrosTressFiles(true, dFechaUltimoRegistro, sError, lEnviarInfoTressFiles);
                  if lEnviarInfoTressFiles then
                  begin
                       sRespuesta := recolectorSoap.Registrar( sXMLInfoTressFiles );
                  end;

                  sErrorLastDateBakcUp := VACIO;
                  lEnviarLastDateBakcUp := false;
                  sXMLLastDateBakcUp := dmInterfase.GetLastDateBackup(true, dFechaUltimoRegistro, sErrorLastDateBakcUp, lEnviarLastDateBakcUp);
                  if lEnviarLastDateBakcUp then
                  begin
                       sRespuesta := recolectorSoap.Registrar( sXMLLastDateBakcUp );
                  end;


               end;
          end;
       except on Error: Exception do
       begin
            //No se reporta ningun error al usuario.
       end;
      end;
  end;

var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     Result := TimbrarNominaServer;

     if ( Result ) then
     begin
           EnviarCambiosConceptos;
     end;

     Screen.Cursor := oCursor;
end;

procedure MensajePaqueteTimbrado(
  sMensaje: string);
begin
     with WizardTimbradoThreadForm do
     begin
          cxStatus.Caption := sMensaje;
     end;
end;

function TWizardTimbradoThreadForm.ConsultaRegistroFechaTerminales(var sAccion : string): boolean;
var
    sValorRegistro, sTimbradoSistemaLabel, sLlave, sValor, sFechaRegistro, sFechaRegistroObtenida : string;
    dFecharegistroterminales : TDateTime;
    iDiasEntreFechas : integer;

begin
     try
            sAccion := 'Enviar';
            sValorRegistro := VACIO;
            sFechaRegistro := VACIO;
            sLlave := VACIO;
            sLlave := 'TimbradoRegistroTerminales';
            dFecharegistroterminales := now;
            sFechaRegistro :=  formatdatetime('dd/mm/yy', now);

            sValorRegistro := VACIO;
            sLlave := VACIO;
            sLlave := 'TimbradoRegistroTerminales';
            if LeerURLRegistro(sValorRegistro, sLlave) then
            begin
                 sFechaRegistroObtenida := sValorRegistro;
            end
            else
            begin
                 AddLlaveTimbradoTerminalesRegistro(sFechaRegistro, sLlave);
            end;

            dFecharegistroterminales :=  strtodatetime(sFechaRegistroObtenida);
            iDiasEntreFechas := DaysBetween(dFecharegistroterminales, now);

            if (iDiasEntreFechas < 8)then
            begin
                 sAccion := 'No Enviar';
            end;
            AddLlaveTimbradoTerminalesRegistro(sFechaRegistro, sLlave);
     Except
     end;
end;

function TWizardTimbradoThreadForm.LeerURLRegistro(var sValorRegistro, sRuta: string): boolean;
const
     RutaURL = 'Software\Grupo Tress\TressWin\Client\';
     RutaURLWOW6432 = 'Software\Wow6432Node\Grupo Tress\TressWin\Client\';
var
     oRegistro : TRegistry;
     sRutaRegistro: string;
     I: Integer;
begin
      Result := False;
      sValorRegistro := VACIO;
      try
              for I := 1 to 2 do
              begin
                   sRutaRegistro := VACIO;
                   case I of
                       1: sRutaRegistro := RutaURL;
                       2: sRutaRegistro := RutaURLWOW6432;
                   end;
                  oRegistro:= TRegistry.Create(KEY_READ);
                  oRegistro.RootKey:= HKEY_LOCAL_MACHINE;
                  if (oRegistro.KeyExists(sRutaRegistro)) then
                  begin
                        oRegistro.OpenKeyReadOnly(RutaURL);
                        if (oRegistro.ValueExists(sRuta)) then
                        begin
                             sValorRegistro := oRegistro.ReadString(sRuta);
                             if sValorRegistro <> VACIO then
                             begin
                                  Result := True;
                             end;
                        end;
                  end;
                  oRegistro.CloseKey();
                  oRegistro.Free;

                  if Result then
                  begin
                       break;
                  end;
              end;
      Except
      end;
end;


procedure TWizardTimbradoThreadForm.AddLlaveTimbradoTerminalesRegistro(sValor, sLlave : string);
const
     RutaURL = 'Software\Grupo Tress\TressWin\Client\';
     RutaURLWOW6432 = 'Software\Wow6432Node\Grupo Tress\TressWin\Client\';

var
     oRegistro : TRegistry;
     sRutaRegistro: string;
     I: Integer;

begin
      for I := 1 to 2 do
      begin
           sRutaRegistro := VACIO;
           case I of
               1: sRutaRegistro := RutaURL;
               2: sRutaRegistro := RutaURLWOW6432;
           end;
      AddValorRegistro(sValor, sRutaRegistro, sLlave);
      end;
end;

procedure TWizardTimbradoThreadForm.AddValorRegistro(sValor, sRuta, sLlave: string);
var
 oRegistro : TRegistry;
begin
    try
        oRegistro:= TRegistry.Create(KEY_READ);
        oRegistro.RootKey:= HKEY_LOCAL_MACHINE;
        if (not oRegistro.KeyExists(sRuta)) then
        begin
              oRegistro.Access:= KEY_WRITE or KEY_WOW64_64KEY;
              oRegistro.OpenKey(sRuta,True);
              if (oRegistro.ValueExists(sLlave)) then
              begin
                   oRegistro.WriteString(sLlave, sValor);
              end
              else
              begin

                   oRegistro.WriteString(sLlave, sValor);
              end;
        end
        else
        begin
              oRegistro.Access:= KEY_WRITE or KEY_WOW64_64KEY;
              oRegistro.OpenKey(sRuta,True);
              if (oRegistro.ValueExists(sLlave)) then
              begin
                   oRegistro.WriteString(sLlave, sValor);
              end
              else
              begin
                   oRegistro.WriteString(sLlave, sValor);
              end;
        end;
        oRegistro.CloseKey();
        oRegistro.Free;
    EXCEPT

    end;
end;


procedure TWizardTimbradoThreadForm.FormCreate(Sender: TObject);
const
   K_TIMBRAMEX_OTRA = 99;
   K_TIMBRAMEX_OTRA_POSICION = 11;
   procedure LLenarComboOrden;
    var
       iNivel : integer;
       sNivelName : string;
    begin

      with cbOrden.Lista do
      begin
          BeginUpdate;
          try
             Clear;
             Add( 'NOMINA.CB_CODIGO=N�mero de Empleado' );
             for iNivel := 1 to 9 do
             begin
                sNivelName := Global.GetGlobalString( K_GLOBAL_NIVEL1 + iNivel -1 );
                if strLleno( sNivelName ) then
                begin
                     Add( Format( 'NOMINA.CB_NIVEL%d=%s' , [iNivel, sNivelName] ));
                end;
             end;
          finally
                 EndUpdate;
          end;
      end;
      cbOrden.ItemIndex := 0;
      cbOrden.Llave := 'NOMINA.CB_CODIGO';
    end;

    procedure LLenaPeriodicidadCombo;
    begin
         with cxPeriodicidadPago.Lista do
         begin
              BeginUpdate;
              try
                 Clear;
                 Add( '1=Semana' );
                 Add( '2=Decena' );
                 Add( '3=Catorcena' );
                 Add( '4=Quincena' );
                 Add( '5=Mes' );
                 Add( '6=Eventual' );
                 Add( '7=D�a' );
                 Add( '8=Bimestre' );
                 Add( '9=Unidad de Obra' );
                 Add( '10=Comisi�n' );
                 Add( '11=Precio Alzado' );
                 Add( '12=Otra' );
              finally
                     EndUpdate;
              end;
         end;
         cxPeriodicidadPago.ItemIndex := 0;
    end;

  procedure InicializarFiltros;
    begin
     LLenarComboOrden;
     LLenaPeriodicidadCombo;
     FEmpleadoCodigo := 'NOMINA' + '.CB_CODIGO';
     FParameterList := TZetaParams.Create;
     FDescripciones := TZetaParams.Create;
     FparametrosFecha := TZetaParams.Create;
     FParametrosPeriodicidad := TZetaParams.Create;
     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := VACIO; //IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     dmCatalogos.cdsCondiciones.Conectar;
     dmCatalogos.cdsPeriodo.Conectar;
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;
     lookupNominaOriginal.LookupDataset := dmCatalogos.cdsPeriodo;

     PE_FECH_PAG.Enabled := False;
    end;
begin

     HelpContext:= H32132_TimbrarNomina;
     FAllowTimbrarHayDatos := FALSE;
     FRecargarReportes := FALSE;

     InicializarFiltros ;
    // dmInterfase.cdsPeriodosTimbrar.Refrescar;
{$ifdef DOS_CAPAS}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     {$endif}

     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;

          with GetDatosPeriodoActivo do
          begin
               PE_FECH_PAG.Valor := Pago;
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );

               FechaInicial.Caption := FormatDateTime( FormatSettings.LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( FormatSettings.LongDateFormat, Fin );
               {*** US 14377: Es necesario ajustar la periodicidad a Otros cuando el uso del Periodo sea Especial, asi como tambien es necesario mostrar el uso del periodo al Timbrar La Nomina***}
               UsoPeriodo.Caption := ObtieneElemento( lfUsoPeriodo, Ord( Uso ) );
               lookupNominaOriginal.Filtro := Format( '(PE_STATUS > %d) and (PE_NUMERO <> %d) ', [ ord(spCalculadaTotal),  Numero] );
               dmCatalogos.cdsPeriodo.Filter :=   lookupNominaOriginal.Filtro;
               dmCatalogos.cdsPeriodo.Filtered := TRUE;

         end;
     end;

    DatasetPeriodo.DataSet := dmInterfase.cdsPeriodosAfectadosTotal;
    dmCliente.RazonSocial := VACIO;

    TomarCuentaTimbradoSeleccionadoGrid;


     dataSourceTimbrar.DataSet :=  dmInterfase.cdsPeriodosTimbrar;
     dmTablas.cdsEstado.Conectar;


     CT_CODIGO.LookupDataset := dmInterfase.cdsCuentasTimbramex;
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
     //cxGroupBoxResultados.Visible := FALSE;

     cxEnviarDetalle.Checked :=  Global.GetGlobalBooleano( K_GLOBAL_TIMBRADO_ENVIAR_DETALLE_5 ) ;
     cxEnviarIncapacidades.Checked :=  Global.GetGlobalBooleano( K_GLOBAL_TIMBRADO_ENVIAR_INCAPACIDADES_4 ) ;
     cxEnviarTiempoExtra.Checked :=  Global.GetGlobalBooleano( K_GLOBAL_TIMBRADO_ENVIAR_TIEMPO_EXTRA_3 ) ;
     cxEnviarSubContratos.Checked :=  Global.GetGlobalBooleano( K_GLOBAL_TIMBRADO_DETALLE_SUBCONTRATO_6 ) ;

     {*** US 14237: El usuario requiere especificar la periodicidad al momento de timbrar para as� poder incluir los nuevos tipos que el SAT propone para el CFDI 1.2 ***}
     if dmInterfase.cdsPeriodosAfectados.FieldByName('Uso').AsInteger = Ord( eUsoPeriodo( upEspecial ) ) + 1 then
        cxPeriodicidadPago.ItemIndex := K_TIMBRAMEX_OTRA_POSICION
     else
         cxPeriodicidadPago.ItemIndex := dmInterfase.cdsPeriodosAfectados.FieldByName('Frecuencia').AsInteger - 1;
     {*** FIN ***}
     FParamsTimbradoNomina := TZetaParams.Create;
end;

procedure TWizardTimbradoThreadForm.Button1Click(Sender: TObject);
begin
     //memoPreview.Text :=  dmInterfase.GetRecibos;
end;

procedure TWizardTimbradoThreadForm.dxWizardControl1PageChanged(
  Sender: TObject);
begin
  if (dxWizardControl1.ActivePage = dxWizardControlPageReportes ) and ( FRecargarReportes )  then
  begin
       InitStatusReporte;
       if GetRecibosDataSet then
       begin
           FAllowTimbrarHayDatos := TRUE;
           cxTimbrar.Text := Format('%d', [dmInterfase.cdsRecibosTimbrar.RecordCount] );
       end
       else
       begin
           ZError(Self.Caption, 'No hay Recibos por Timbrar ', 0);
           FAllowTimbrarHayDatos := FALSE;
       end;
  end;
end;

procedure TWizardTimbradoThreadForm.dxWizardControl1PageChanging(Sender: TObject;
  ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
var
   iRS_CONTID : integer;
   sNombreRazon : string;
begin

  if ( AnewPage = dxWizardControlFiltroEmpleados )and (dxWizardControl1.ActivePage = dxWizardControlPageOpciones )  then
  begin
      if (cxEsUnaNominaAjuste.CheckBox.Checked and strVacio( lookupNominaOriginal.Llave)) then
      begin
           ZWarning(Self.Caption, 'Es necesario elegir la n�mina que contiene los timbres cancelados y que ser�n sustituidos por �ste periodo ' , 0, mbOK);
           if lookupNominaOriginal.CanFocus then
              lookupNominaOriginal.SetFocus;
           AAllow := FALSE;
      end
      else
      begin
          AAllow := TRUE;
      end;
  end;

  //dxWizardControlPageReportes
  if ( AnewPage = dxWizardControlPageReportes) then
  begin
      if(dxWizardControl1.ActivePage = dxWizardControlFiltroEmpleados )  then
      begin
           FRecargarReportes := TRUE;
           InitStatusReporte;

           TomarCuentaTimbradoSeleccionadoGrid;

           CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;

           iRS_CONTID := dmInterfase.cdsRSocial.FieldByName('RS_CONTID').AsInteger;
           sNombreRazon :=  dmInterfase.cdsRSocial.FieldByName('RS_NOMBRE').AsString;

           if (iRS_CONTID <=0 ) then
           begin
                ZWarning(Self.Caption, Format( 'La raz�n social %s no est� registrada en el Sistema de Timbrado ' , [sNombreRazon]) , 0, mbOK);
                AAllow := FALSE;
           end
           else
           begin
               AAllow := TRUE;
           end;
      end
      else
      begin
           FRecargarReportes := FALSE;
      end;
  end;

  if ( AnewPage = dxWizardControlPageRecibos) and (dxWizardControl1.ActivePage = dxWizardControlPageReportes )  then
  begin
       AAllow := FAllowTimbrarHayDatos;
  end;

  if ( AnewPage = dxWizardControlPageCuentaTimbrado) then
  begin
       if(dmCliente.EsTRESSPruebas) then
       begin
            DxWizardControl1.buttons.Finish.Enabled := False;
       end;
       TomarCuentaTimbradoSeleccionadoGrid;
       CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
  end;

  if ( AnewPage = dxWizardControlPageTimbrado ) then
     begin
           with dmCliente do
           begin
                with GetDatosPeriodoActivo do
                begin
                     Advertencia.Caption := 'Al aplicar el proceso se proceder� a timbrar los recibos de los empleados seleccionados correspondientes al periodo'
                     +' '+iTipoNomina.Caption+' '+  iNumeroNomina.Caption +' del a�o '+IntToStr(Year)+', contribuyente '+dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString +'.';
                end;
           end;


          AAllow := TRUE;
          TomarCuentaTimbradoSeleccionadoGrid;
          CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;

          if StrVacio( CT_CODIGO.Llave ) then
          begin
               zWarning( Self.Caption, 'No se ha especificado la Cuenta de Timbrado', 0 , mbOK );
               CT_CODIGO.SetFocus;
               AAllow := FALSE;
          end
          else
          begin
               //AAllow := CargarManifiesto;
          end;
     end;
  if ( AnewPage = dxWizardControlFiltroEmpleados ) and (dxWizardControl1.ActivePage = dxWizardControlPageInicio )  then
  begin
       AAllow := MostrarAdvertenciaTimbradoNomina;
  end;
end;



procedure TWizardTimbradoThreadForm.CT_CODIGOValidLookup(Sender: TObject);
begin
      DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;

function TWizardTimbradoThreadForm.GetRecibosDataSet: boolean;
var
 oCursor: TCursor;
 nFields, iField : integer;
 sDisplayLab : string;
begin
  oCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Result := FALSE;
  try
        if  dmInterfase.cdsPeriodosTimbrar.IsEmpty  then
       begin
            dmCliente.RazonSocial := VACIO;
       end
       else
       begin
            dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosTimbrar.FieldByName('RS_CODIGO').AsString, []) ;
            dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
       end;

         with FParameterList do
         begin
             AddString( 'RangoLista', GetRango ) ;
             AddString( 'Condicion', GetCondicion );
             AddString( 'Filtro', GetFiltro );
             AddString( 'Orden', cbOrden.Llave );
         end;
        Result := dmInterfase.GetRecibosDataSet(FParameterList, INotificarStatusReporte(Self) , cxEnviarDetalle.Checked , cxEnviarIncapacidades.Checked,  cxEnviarTiempoExtra.Checked, cxEnviarSubContratos.Checked ) ;
        FparameterList.Clear();
        nFields := dmInterfase.cdsRecibosTimbrar.FieldCount;
        for iField := 0 to nFields -1 do
        begin
             sDisplayLab := UpperCase( dmInterfase.cdsRecibosTimbrar.Fields[iField].DisplayLabel ) ;


             if ( sDisplayLab  = 'NUMERO' ) then
                cxGridDBColumn1.DataBinding.FieldName :=  dmInterfase.cdsRecibosTimbrar.Fields[iField].FieldName;

             if ( sDisplayLab  = 'NOMBRE' ) then
                cxGridDBColumn2.DataBinding.FieldName :=  dmInterfase.cdsRecibosTimbrar.Fields[iField].FieldName;

             if ( sDisplayLab  = 'TOTPER' ) then
                cxGridDBColumn3.DataBinding.FieldName :=  dmInterfase.cdsRecibosTimbrar.Fields[iField].FieldName;

             if ( sDisplayLab  = 'RFC' ) then
                cxGridDBColumn4.DataBinding.FieldName :=  dmInterfase.cdsRecibosTimbrar.Fields[iField].FieldName;

        end;
        DataSourceRecibos.DataSet := dmInterfase.cdsRecibosTimbrar;

        if cxEnviarDetalle.Checked then
        begin
             DataSourceDetalle.DataSet := dmInterfase.cdsRecibosDetalleTimbrar;
        end;
   finally
     Screen.Cursor := oCursor;
   end;


end;

function TWizardTimbradoThreadForm.MostrarAdvertenciaTimbradoNomina: Boolean;
var
   oMensajesTimbrado: TMensajesTimbradoWebServices;
   sErrores: String;
   oParametros: TParametrosPeticion;
begin
     try
        try
           Result := True;
           TomarCuentaTimbradoSeleccionadoGrid;

           CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
           oParametros.CT_CODIGO := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
           oParametros.RS_CONTID := dmInterfase.cdsRSocial.FieldByName('RS_CONTID').AsInteger;
           oMensajesTimbrado := TMensajesTimbradoWebServices.Create(oParametros);
           oMensajesTimbrado.FormarPeticionCartaTimbrado;
           if oMensajesTimbrado.SolicitarPeticion(sErrores) then
           begin
                if oMensajesTimbrado.FormarHTMLMensajeDialogo(50004) then
                begin
                     ZDialogWebTimbrado( oMensajesTimbrado.TituloDialogo, oMensajesTimbrado.LinkTemp, oMensajesTimbrado.TipoDialogoUI, oMensajesTimbrado.BotonesDialogoUI, oMensajesTimbrado.InfoBotonOKDialogo, oMensajesTimbrado.InfoBotonCLOSEDialogo , 0);
                     if ( eTipoImagenMensaje( oMensajesTimbrado.TipoError ) in [rError,rErrorGrave] ) then
                     begin
                          Result := False;
                     end;
                end;
           end
           else
           begin
                zError( Self.Caption,sErrores, 0);
                Result := False;
           end;
        except on Error: Exception do
               begin
                    zError( Self.Caption, Error.Message, 0);
                    Result := False;
               end;
        end;
     finally
            FreeAndNil(oMensajesTimbrado);
     end;
end;


function TWizardTimbradoThreadForm.GetStatusReporteItemNo(sTitulo : string) : integer;
begin
     Result := -1;

     if ( sTitulo = K_REPORTE_TIMBRADO) then  Result := 0
     else
     if ( sTitulo = K_REPORTE_TIMBRADO_DETALLE) then  Result :=  1
     else
     if ( sTitulo = K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA) then  Result :=  2
     else
     if ( sTitulo = K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES) then  Result :=  3
     else
     if ( sTitulo = K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS) then  Result :=  4;
end;

function TWizardTimbradoThreadForm.GetStatusReporteItemNo2(sTitulo : string) : integer;
begin
     Result := -1;

     if ( sTitulo = K_REPORTE_TIMBRADO) then  Result := 0
     else
     if ( sTitulo = K_REPORTE_TIMBRADO_DETALLE) then  Result :=  2
     else
     if ( sTitulo = K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA) then  Result :=  4
     else
     if ( sTitulo = K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES) then  Result :=  6
     else
     if ( sTitulo = K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS) then  Result :=  8;
end;


function TWizardTimbradoThreadForm.NotificarStatusReporte(sTitulo, sDescripcion, sDetalle : string; lStatusOk : integer; lEnable: Boolean  ) : boolean;
var
     itemNo2 : integer;
begin
     itemNo2 := GetStatusReporteItemNo2(sTitulo);
     if ( itemNo2 >=0 ) and ( itemNo2+1 < cxCheckListReportes.Items.Count ) then
     begin
           cxCheckListReportes.Items[itemNo2].Text := 'Reporte "'+sTitulo+'".';
           cxCheckListReportes.Items[itemNo2+1].Text := 'Estatus: '+sDescripcion+'.'+sDetalle;
           cxCheckListReportes.Items[itemNo2].Enabled := lEnable;
           cxCheckListReportes.Items[itemNo2+1].Enabled := lEnable;
           if (lEnable) then
           begin
               cxCheckListReportes.Items[itemNo2].ImageIndex := lStatusOk;
              { if ( lStatusOk = 1) then
                  cxCheckListReportes.Items[itemNo2].State := TcxCheckBoxState.cbsChecked
               else
                   cxCheckListReportes.Items[itemNo2].State := TcxCheckBoxState.cbsUnChecked;}
//               cxCheckListReportes.Items[itemNo2].
           end
           else
           begin
                cxCheckListReportes.Items[itemNo2].Text :='Reporte "'+sTitulo+'".';
                cxCheckListReportes.Items[itemNo2+1].Text := VACIO;
                cxCheckListReportes.Items[itemNo2].ImageIndex := 0;
                cxCheckListReportes.Items[itemNo2].State := TcxCheckBoxState.cbsGrayed;
           end;
     end;


     Application.ProcessMessages;
end;

procedure TWizardTimbradoThreadForm.gridPeriodosDBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
 		ZGridModeTools.BorrarItemGenericoAll( AValueList );
     		if gridPeriodosDBTableView1.DataController.IsGridMode then
        		ZGridModeTools.FiltroSetValueLista( gridPeriodosDBTableView1, AItemIndex, AValueList );
end;

procedure TWizardTimbradoThreadForm.InitStatusReporte;
begin
     //cxEnviarDetalle.Checked, cxCbDescripcion.Text ,
     // cxEnviarIncapacidades.Checked,
     // cxEnviarTiempoExtra.Checked, cxEnviarSubContratos.Checked,

     NotificarStatusReporte( K_REPORTE_TIMBRADO, '', '', 0, True);
     NotificarStatusReporte( K_REPORTE_TIMBRADO_DETALLE, '', '', 0, cxEnviarDetalle.Checked);
     NotificarStatusReporte( K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, '', '', 0, cxEnviarIncapacidades.Checked);
     NotificarStatusReporte( K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, '', '', 0, cxEnviarTiempoExtra.Checked);
     NotificarStatusReporte( K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, '', '', 0, cxEnviarSubContratos.Checked);
end;

procedure TWizardTimbradoThreadForm.Exportar;
 var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Errores de Timbrado',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin
       if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
      Valor := ValoresGrid;
      FBaseReportes_DevEx.ExportarGrid( ZetaCXGrid1DBTableView2,
      ZetaCXGrid1DBTableView2.DataController.DataSource.DataSet, Caption, 'IM',
      Valor[0],Valor[1],Valor[2],Valor[3]);
      end;
end;

procedure TWizardTimbradoThreadForm.ZetaSpeedButton1Click(Sender: TObject);
begin
      if ZetaCXGrid1DBTableView2.DataController.DataSetRecordCount > 0 then
      begin
           Exportar;
      end
      else
      begin
           //ZError('Error en '+Self.Caption, 'El Grid de Errores est� Vac�o', 0);
           ZetaDialogo.ZError('Error en '+Self.Caption, '�Se encontr� un Error! '+ CR_LF + 'El grid de respuesta del sistema de timbrado est� vac�o',0)
      end;
end;

procedure TWizardTimbradoThreadForm.EscribirBitacoraError(
  sMensaje: string);
begin
//
end;

procedure TWizardTimbradoThreadForm.EscribirBitacoraLog(sMensaje: string);
begin
//
end;

procedure TWizardTimbradoThreadForm.ZetaSpeedButton2Click(Sender: TObject);
var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Empleados a Timbrar',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin
    
end;

procedure TWizardTimbradoThreadForm.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardTimbradoThreadForm.BFinalClick(Sender: TObject);
begin
   with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardTimbradoThreadForm.BListaClick(Sender: TObject);
begin
 with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TWizardTimbradoThreadForm.BAgregaCampoClick(Sender: TObject);
begin
  with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( EntidadActiva, Text, SelStart, evBase );
     end;
end;

function TWizardTimbradoThreadForm.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
       if ZetaBuscaEmpleado_DevExTimbradoAvanzada.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
         begin
              if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                 Result := sLlave + ',' + sKey
              else
                  Result := sKey;
         end;
     end
     else
     begin
          if ZetaBuscaEmpleado_DevExAvanzada.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
         begin
              if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                 Result := sLlave + ',' + sKey
              else
                  Result := sKey;
         end;
     end;
end;
function TWizardTimbradoThreadForm.GetRango: String;
var
   sl : TStringList;
   i : integer;
   sLista : string;
begin

     sl := TStringList.Create;
     sl.CommaText :=  ELista.Text;
     sLista := VACIO;
     for i:=0 to sl.Count -1  do
     begin
          if StrLleno( sl[i] ) then
          begin
               if StrVacio( sLista)  then
                  sLista := sl[i]
               else
                  sLista :=  sLista + ','+ sl[i];
          end;
     end;
     sl.Free;

     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FEmpleadoCodigo, Trim( sLista  ) );
     else
         Result := '';
     end;
end;
procedure TWizardTimbradoThreadForm.RBTodosClick(Sender: TObject);
begin
    EnabledBotones( raTodos );
end;

procedure TWizardTimbradoThreadForm.RBRangoClick(Sender: TObject);
begin
    EnabledBotones( raRango );
end;

procedure TWizardTimbradoThreadForm.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;
procedure TWizardTimbradoThreadForm.EnabledBotones(
  const eTipo: eTipoRangoActivo);
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;
function TWizardTimbradoThreadForm.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          if ( global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) )then
             Result := stringReplace(Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString ), 'COLABORA', 'V_EMP_TIMB' ,[rfReplaceAll, rfIgnoreCase])
          else
              Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
     end
     else
         Result := '';
end;
function TWizardTimbradoThreadForm.GetFiltro: String;
begin
  Result := Trim( sFiltro.Text );
end;
procedure TWizardTimbradoThreadForm.ExportarExcelEmpleadosClick(
  Sender: TObject);
 var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Empleados a Timbrar',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin
        if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
        begin
              Valor := ValoresGrid;
              ExportarGrid( cxGridDBTableView1,
              DataSourceRecibos.DataSet, Caption, 'IM',
              Valor[0],Valor[1],Valor[2],Valor[3]);
        end;
end;

procedure TWizardTimbradoThreadForm.CxAjustaFechaPagoClick(
  Sender: TObject);
begin
    PE_FECH_PAG.Enabled :=  CxAjustaFechaPago.CheckBox.Checked;
    lblFechaPago.Enabled :=  CxAjustaFechaPago.CheckBox.Checked;
end;

procedure TWizardTimbradoThreadForm.FormShow(Sender: TObject);
begin
     ZetaCXGrid1DBTableView2.ApplyBestFit();
if(dmCliente.EsTRESSPruebas) then
     begin
          DxWizardControl1.buttons.Finish.Enabled := False;
     end;
end;

procedure TWizardTimbradoThreadForm.TomarCuentaTimbradoSeleccionadoGrid;
begin
     if  dmInterfase.cdsPeriodosTimbrar.IsEmpty  then
     begin
          dmCliente.RazonSocial := VACIO;
     end
     else
     begin
          dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosTimbrar.FieldByName('RS_CODIGO').AsString, []) ;
          dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
     end;
end;

end.

