unit FWizImportarConceptosConfiguracion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.Menus,
  Vcl.ExtCtrls, Vcl.StdCtrls, cxTextEdit, cxButtons,
  ZetaKeyLookup_DevEx, cxProgressBar, cxDropDownEdit,
  ZetaCXStateComboBox, cxMaskEdit, cxSpinEdit, Vcl.Mask,
  ZetaFecha, cxCheckBox, FWizConfiguracionConceptoGridSelect,ZetaClientDataSet, DBClient;

type
  TWizImportarConceptosConfiguracion = class(TcxBaseWizard)
    gbArchivoConfiguracionConcepto: TcxGroupBox;
    cxLabel1: TcxLabel;
    btnConfiguracionConcepto: TcxButton;
    ArchivoConfiguracionConcepto: TcxTextEdit;
    DialogoConfiguracionConcepto: TOpenDialog;
    PanelProgressBar: TPanel;
    ProgressBar: TcxProgressBar;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    txtActulizaConceptosPath: TcxTextEdit;
    BuscaBitacoraTimbrado: TcxButton;
    DialogoBitacora: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure btnConfiguracionConceptoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure BuscaBitacoraTimbradoClick(Sender: TObject);
  private
    { Private declarations }
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizImportarConceptosConfiguracion: TWizImportarConceptosConfiguracion;

implementation

uses
    DCliente, ZetaClientTools, ZetaCommonClasses,
    ZetaDialogo, ZetaCommonTools, ZetaCommonLists,DInterfase,
    ZBasicoSelectGrid_DevEx;

{$R *.dfm}

procedure TWizImportarConceptosConfiguracion.btnConfiguracionConceptoClick(Sender: TObject);
begin
    inherited;
    DialogoConfiguracionConcepto.InitialDir := ExtractFilePath( Application.Name );
    ArchivoConfiguracionConcepto.Text := AbreDialogo( DialogoConfiguracionConcepto, ArchivoConfiguracionConcepto.Text,'csv' );
end;

procedure TWizImportarConceptosConfiguracion.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H60621_Catalogo_conceptos;
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
     ProgressBar.Visible := False;
end;

procedure TWizImportarConceptosConfiguracion.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Al aplicar este proceso se importara la configuraci�n de conceptos de Timbrado.';
     ArchivoConfiguracionConcepto.Text := ExtractFilePath( Application.ExeName );
     txtActulizaConceptosPath.Text:= ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) ) + 'ImportarConfiguracionConceptoTimbrado.log';
end;

procedure TWizImportarConceptosConfiguracion.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                 if ( StrVacio(ArchivoConfiguracionConcepto.Text) ) then
                 begin
                      ZetaDialogo.ZError( Self.Caption, 'El archivo de configuraci�n de conceptos de Timbrado est� vac�o.', 0 );
                      CanMove := False;
                 end
                 else if dmInterfase.IsOpen( ArchivoConfiguracionConcepto.Text ) then
                 begin
                      ZetaDialogo.ZError( Self.Caption, 'Archivo ' + ArchivoConfiguracionConcepto.Text + ' en uso',  0 );
                      CanMove := False;
                 end
                 else if ( StrVacio( txtActulizaConceptosPath.Text) ) then
                 begin
                      ZetaDialogo.ZError( Self.Caption, 'El directorio de la bit�cora no puede quedar vac�o', 0 );
                      CanMove := False;
                 end
                 else if dmInterfase.IsOpen( txtActulizaConceptosPath.Text ) then
                 begin
                      ZetaDialogo.ZError( Self.Caption, 'Archivo ' + ArchivoConfiguracionConcepto.Text + ' en uso',  0 );
                      CanMove := False;
                 end
                 else if dmInterfase.ArchivoInvalido( ArchivoConfiguracionConcepto.Text ) then
                 begin
                      ZetaDialogo.ZError(Self.Caption,'El archivo de configuraci�n de conceptos de Timbrado es inv�lido � est� vac�o',0);
                      CanMove := False;
                 end
                 else
                 begin
                      //si hay registros a procesar, abrir el grid para filtrar
                      CanMove := ZBasicoSelectGrid_DevEx.GridSelectBasico(
                                 TZetaClientDataSet( dmInterfase.cdsImpotConfConcepto ), TWizConfiguracionConceptoGridSelect, FALSE );
                       //si no hay registros a procesar, despues de filtrarlos por el grid de conceptos, no avanzar
                      if ( dmInterfase.cdsImpotConfConcepto.IsEmpty ) then
                      begin
                           ZetaDialogo.ZError(Self.Caption,'El archivo de configuraci�n de conceptos de Timbrado es inv�lido � est� vac�o',0);
                           CanMove := False;
                           //cancelar los cambios realizados por el grid de conceptos, cuando se eliminaron TODOS los registros a procesar
                           TClientDataSet( dmInterfase.cdsImpotConfConcepto ).CancelUpdates;
                      end;
                 end;
            end;
     end;
end;

procedure TWizImportarConceptosConfiguracion.BuscaBitacoraTimbradoClick( Sender: TObject);
begin
     inherited;
     txtActulizaConceptosPath.Text := ZetaClientTools.AbreDialogo( DialogoBitacora , txtActulizaConceptosPath.Text, 'log' );
end;

procedure TWizImportarConceptosConfiguracion.CargaParametros;
begin
     Descripciones.Clear;
     with Descripciones do
     begin
          AddString( 'Archivo a importar', ArchivoConfiguracionConcepto.Text );
          AddString( 'Guardar bit�cora en', txtActulizaConceptosPath.Text );
     end;
     inherited;
end;

function TWizImportarConceptosConfiguracion.EjecutarWizard: Boolean;
var
   sMensaje : String;
begin
     with dmInterfase do
     begin
          LogInit( txtActulizaConceptosPath.Text ,'Inicio Actualizaci�n de configuraci�n de conceptos de Timbrado.' );
          if ImportarConfiguracionConceptos( sMensaje ) then
             ZInformation( Self.Caption, sMensaje, 0 )
          else
              ZetaDialogo.ZError( Self.Caption, sMensaje, 0 );
          LogEnd;
     end;
end;

end.
