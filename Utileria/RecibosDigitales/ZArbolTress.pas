unit ZArbolTress;

interface

uses SysUtils, ComCtrls, Dialogs, DB, Controls,
     ZArbolTools, ZNavBarTools, ZetaCommonLists,cxTreeView;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
procedure MuestraArbolOriginal( oArbol: TcxTreeView ); 
//DevEx: Este metodo creara los arbolitos que se asignaran a la NavBar

//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
procedure CreaNavBarUsuario ( oNavBarMgr: TNavBarMgr; const sFileName: String ; Arbolitos: array of TcxTreeView ; ArbolitoUsuario : TcxTreeView);
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
//procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String; iAccesoGlobal: Integer );
procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses DGlobal,
     DSistema,
     DCliente,
     ZetaDespConsulta,
     ZGlobalTress,
     ZetaCommonTools,
     ZAccesosMgr,
     ZetaTipoEntidad,
     ZBaseNavBarShell;

const
     K_TIMBRADO                         = 734;
     K_TIM_DATOS_PERIODOS               = 735;
     K_TIM_DATOS_SALDO                  = 736;
     K_TIM_DATOS_CUENTAS                = 737;
     K_TIM_DATOS_CONTRIBUYENTES         = 738;
     K_TAB_OFI_SAT_TIPOS_CONCEPTOS      = 717;
     K_CAT_NOMINA_CONCEPTOS             = 144;

     //MA( Ver 2.7 ) NOTA: Cuando se agreguen constantes que corresponden a nodos tipo FOLDER dentro de el
     //�rbol de formas en Tress, es necesario que el valor de la constante sea igual al valor del derecho
     //de acceso en ZAccesosTress.pas.
     //                   Ej.- K_CAPACITACION = D_CAPACITACION        ( Sugerencia para versiones futuras )

function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma := TRUE;
     Result.Caption := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma := FALSE;
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;
//DevEx
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     case iNodo of
          // Timbrado
          K_TIMBRADO          : Result := Folder( 'Timbrado', iNodo );
          K_TIM_DATOS_PERIODOS          : Result := Forma( 'Periodos de N�mina', efcTimDatosPeriodos );
          K_TIM_DATOS_SALDO             : Result := Forma( 'Estado de Cuenta de Timbrado', efcTimDatosSaldo );
          K_TIM_DATOS_CUENTAS           : Result := Forma( 'Cuentas de Timbrado', efcTimDatosCuentas );
          K_TIM_DATOS_CONTRIBUYENTES    : Result := Forma( 'Contribuyentes', efcTimDatosContribuyentes );
          K_TAB_OFI_SAT_TIPOS_CONCEPTOS : Result := Forma( 'Tipos de Conceptos', efcTTiposConceptosSAT );
          K_CAT_NOMINA_CONCEPTOS        : Result := Forma( 'Conceptos', efcCatConceptos );

       else
           Result := Folder( '', 0 );
     end;
end;
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
     case iNodo of
         // Timbrado
            K_TIMBRADO          : Result := Grupo( 'Timbrado', iNodo );
     else
          Result := Grupo( '', 0 );
     end;
end;

procedure CreaArbolDefault( oArbolMgr : TArbolMgr );

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

begin  // CreaArbolDefault

     // ************ Presupuestos *****************
     NodoNivel( 0, K_TIMBRADO );
      NodoNivel( 1, K_TIM_DATOS_PERIODOS );
      NodoNivel( 1, K_TIM_DATOS_SALDO );
      NodoNivel( 1, K_TIM_DATOS_CUENTAS );
      NodoNivel( 1, K_TIM_DATOS_CONTRIBUYENTES );
      NodoNivel( 1, K_TAB_OFI_SAT_TIPOS_CONCEPTOS );
      NodoNivel( 1, K_CAT_NOMINA_CONCEPTOS );

end;
//DevEx(by am)
procedure CreaArbolitoDefault( oArbolMgr : TArbolMgr; iAccesoGlobal: Integer );
function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo); //Siempre mandar 0 en iNodo
end;
begin
     case iAccesoGlobal of
          // Timbrado
          K_TIMBRADO          :
          begin
               NodoNivel( 0, K_TIM_DATOS_PERIODOS);
               NodoNivel( 0, K_TIM_DATOS_SALDO );
               NodoNivel( 0, K_TIM_DATOS_CUENTAS );
               NodoNivel( 0, K_TIM_DATOS_CONTRIBUYENTES );
               NodoNivel( 0, K_TAB_OFI_SAT_TIPOS_CONCEPTOS );
               NodoNivel( 0, K_CAT_NOMINA_CONCEPTOS );
          end;
     end;
end;
//DevEx
//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
{***DevEx(by am)***}
//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     //CreaArbolitoUsuario( oArbolMgr, '',K_GLOBAL );
                     //Antes metodo: CreaArbolUsuario
                     with oArbolMgr do
                     begin
                           //Configuracion := TRUE;
                           //oArbolMgr.NumDefault := K_GLOBAL ;
                           CreaArbolitoDefault( oArbolMgr, K_GLOBAL  );
                           Arbol_DevEx.FullCollapse;//edit by mp: el arbol dentro del navbar es el arbol_DevEx
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario}
     // ************ Timbrado *****************
     if( NodoNivel( K_TIMBRADO, 1 )) then
     begin
          CreaArbolito ( K_TIMBRADO, Arbolitos[0]);
     end
     else
     begin
      CreaNavBarUsuario(oNavBarMgr, '',Arbolitos, nil);
     end;
end;
//DevEx(by am)
procedure GetDatosNavBarUsuario( oNavBarMgr: TNavBarMgr; DataSet: TDataSet; const sFileName: String );
var
   iUsuario: Integer;
   lImportando: Boolean;
   oDatosUsuario: TDatosArbolUsuario;
begin
     // SUPONE que 'cdsUsuarios' est� posicionado en el usuario correcto
     with Dataset, oDatosUsuario do
     begin
          iUsuario          := FieldByName( 'US_CODIGO' ).AsInteger;
          NombreNodoInicial := FieldByName( 'US_NOMBRE' ).AsString;
          IncluirSistema    := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
          NodoDefault       := FieldByName( 'US_FORMA' ).AsInteger;
          TieneArbol        := FALSE;
     end;
     lImportando := StrLleno( sFileName );
     // Se trae los registros de Arbol del Servidor
     with dmSistema, cdsArbol, oDatosUsuario do
     begin
            if ( lImportando ) then
              cdsArbol.LoadFromFile( sFileName )
            else
               GetArbolUsuario( iUsuario );

        if not IsEmpty then
        begin
             // Busca el Primer registro para obtener nombre del Nodo
             if ( FieldByName( 'AB_ORDEN' ).AsInteger <= 0 ) then
             begin
               // En caso de IMPORTACION
               // En el primer registro va 'IncluirSistema' y 'Forma Default'
               if ( lImportando ) then
               begin
                    IncluirSistema := FieldByName( 'AB_NIVEL' ).AsInteger > 0;
                    NodoDefault    := FieldByName( 'AB_NODO' ).AsInteger;
               end
               else // Si viene de Base de Datos, permite renombrar RAIZ
               begin
                    if StrLleno( FieldByName( 'AB_DESCRIP' ).AsString ) then
                       NombreNodoInicial:= FieldByName('AB_DESCRIP').AsString;
               end;
               Next;
             end;
             TieneArbol := Not Eof;
        end;
        if ( NodoDefault = 0 ) then
           NodoDefault     := K_TIMBRADO; //K_EMP_REGISTRO;
     end;
     oNavBarMgr.DatosUsuario := oDatosUsuario;
end;
{***DevEx(by am)***}
//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
procedure CreaNavBarUsuario (  oNavBarMgr: TNavBarMgr; const sFileName: String; Arbolitos: array of TcxTreeView ; ArbolitoUsuario : TcxTreeView);
var
   DataSet: TDataSet;
   NodoRaiz: TGrupoInfo;
procedure CreaArbolito( Arbolito: TcxTreeView);//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     CreaArbolitoUsuario( oArbolMgr, '' );
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
     if (  oNavBarMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
     else
         DataSet := dmCliente.cdsUsuario;

     GetDatosNavBarUsuario(oNavBarMgr, DataSet, sFileName );
     //CreaNavBarUsuario
     with oNavBarMgr.DatosUsuario do
     begin
          oNavBarMgr.NumDefault := NodoDefault;

          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oNavBarMgr.Configuracion ) then
          begin
           { if ( IncluirSistema or ( not TieneArbol )) then
             begin
                  if ( oNavBarMgr.NumDefault = 0 ) then
                         oNavBarMgr.NumDefault :=  K_TIMBRADO; //Por si solo se agregara el arbol del sistema
                  CreaNavBarDefault(oNavBarMgr, Arbolitos );
                  //PENDIENTE: Implementar FullColapse de la NavBar
             end;
                  }
             // Si por derechos no tiene nada, agregar grupo
             if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
             begin
                  with NodoRaiz do
                  begin
                     Caption := 'No tiene derechos';
                     //EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)
                  end;
             end;
          end;
     end;
end;
procedure GetDatosArbolUsuario( oArbolMgr: TArbolMgr; DataSet: TDataSet; const sFileName: String );
var
   iUsuario: Integer;
   lImportando: Boolean;
begin
     // SUPONE que 'cdsUsuarios' est� posicionado en el usuario correcto
     with Dataset, oArbolMgr.DatosUsuario do
     begin
          iUsuario          := FieldByName( 'US_CODIGO' ).AsInteger;
          NombreNodoInicial := FieldByName( 'US_NOMBRE' ).AsString;
          IncluirSistema    := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
          NodoDefault       := FieldByName( 'US_FORMA' ).AsInteger;
          TieneArbol        := FALSE;
     end;
     lImportando := StrLleno( sFileName );
     // Se trae los registros de Arbol del Servidor
     with dmSistema, cdsArbol, oArbolMgr.DatosUsuario do
     begin
            if ( lImportando ) then
              cdsArbol.LoadFromFile( sFileName )
            else
               GetArbolUsuario( iUsuario );

        if not IsEmpty then
        begin
             // Busca el Primer registro para obtener nombre del Nodo
             if ( FieldByName( 'AB_ORDEN' ).AsInteger <= 0 ) then
             begin
               // En caso de IMPORTACION
               // En el primer registro va 'IncluirSistema' y 'Forma Default'
               if ( lImportando ) then
               begin
                    IncluirSistema := FieldByName( 'AB_NIVEL' ).AsInteger > 0;
                    NodoDefault    := FieldByName( 'AB_NODO' ).AsInteger;
               end
               else // Si viene de Base de Datos, permite renombrar RAIZ
               begin
                    if StrLleno( FieldByName( 'AB_DESCRIP' ).AsString ) then
                       NombreNodoInicial:= FieldByName('AB_DESCRIP').AsString;
               end;
               Next;
             end;
             TieneArbol := Not Eof;
        end;
        if ( NodoDefault = 0 ) then
           NodoDefault     := K_TIMBRADO; //K_EMP_REGISTRO;
     end;
end;

procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
var
   NodoRaiz: TNodoInfo;
   DataSet: TDataSet;

  procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
  var
     oNodoInfo : TNodoInfo;
  begin
       oNodoInfo := GetNodoInfo( iNodo );
       oNodoInfo.Caption := sCaption;
       oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
  end;
begin
      // CreaArbolUsuario
       if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

       GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );

       with oArbolMgr.DatosUsuario do
       begin
          oArbolMgr.NumDefault := NodoDefault;

          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oArbolMgr.Configuracion ) or ( TieneArbol ) then
              with NodoRaiz do
              begin
                   Caption := NombreNodoInicial;
                   EsForma := FALSE;
                   IndexDerechos := K_SIN_RESTRICCION;
                   oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
              end;

          if TieneArbol then
            with dmSistema.cdsArbol do
              while not Eof do
              begin
                  begin
                       NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger,
                                  FieldByName( 'AB_NODO' ).AsInteger,
                                  FieldByName( 'AB_DESCRIP' ).AsString );

                  end; //End if 1
              Next;
              end;

          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oArbolMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
                ZArbolTress.CreaArbolDefault( oArbolMgr );

             // Si por derechos no tiene nada, agregar nodo
             if ( oArbolMgr.Arbol.Items.Count = 0 ) then
             begin
                with NodoRaiz do
                begin
                     Caption := 'No tiene derechos';
                     EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                end;
             end;
          end;
        end;

end;

{***DevEx(by am)***}
procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
var
   //NodoRaiz: TNodoInfo;
   DataSet: TDataSet;
procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
var
   oNodoInfo : TNodoInfo;
begin
     oNodoInfo := GetNodoInfo( iNodo );
     oNodoInfo.Caption := sCaption;
     oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
end;
begin
     if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

     GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );
     with oArbolMgr.DatosUsuario do
     begin
           if TieneArbol then
            with dmSistema.cdsArbol do
              while not Eof do
              begin
                   begin
                        //A AB_NIVEL LE RESTAMOS UNO POR QUE EL GRUPO SERA E NODO PADRE
                        NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger -1,
                              FieldByName( 'AB_NODO' ).AsInteger,
                              FieldByName( 'AB_DESCRIP' ).AsString );
                   end;
                   Next;
              end;
     end;
end;


//DevEx(@am): Agregado para ver el nombre original de los nodos del arbol de usuario.
procedure MuestraArbolOriginal( oArbol: TcxTreeView );
var
   oNodo: TTreeNode;
   iNodo: Integer;
begin
     oArbol.Items.BeginUpdate;
     oNodo := oArbol.Items.GetFirstNode;
     while ( oNodo <> NIL ) do
     begin
          iNodo := Integer( oNodo.Data );
          if ( iNodo > 0 ) then
             oNodo.Text := GetNodoInfo( iNodo ).Caption;
          oNodo := oNodo.GetNext;
     end;
     oArbol.Items.EndUpdate;
end;

end.
