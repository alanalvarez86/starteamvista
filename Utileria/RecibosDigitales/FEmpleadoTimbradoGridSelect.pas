unit FEmpleadoTimbradoGridSelect;

interface


uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, ExtCtrls, Db,
     ZBaseTimbradoSelectGrid,
     ZetaDBGrid, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
  TressMorado2013;

type
  TEmpleadoTimbradoGridSelect = class(TBaseTimbradoGridSelect)
    procedure ExportarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpleadoTimbradoGridSelect: TEmpleadoTimbradoGridSelect;
  FTituloOrden : string;

implementation


{$R *.DFM}

procedure TEmpleadoTimbradoGridSelect.ExportarClick(Sender: TObject);
begin
  inherited;
//
end;

procedure TEmpleadoTimbradoGridSelect.FormCreate(Sender: TObject);
begin
  inherited;
  if FTituloOrden <> '' then
  begin
    ZetaDBGridDBTableView.Columns[0].Caption := FTituloOrden;
    ZetaDBGridDBTableView.Columns[0].Visible := True;
  end
  else
  begin
    ZetaDBGridDBTableView.Columns[0].Visible := False;
  end;
end;

end.
