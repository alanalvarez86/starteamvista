unit FWizConfiguracionConceptoGridSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, DBClient,ZetaCommonTools,ZetaCommonLists;

type
  TValidacion = set of 1..7;
  TWizConfiguracionConceptoGridSelect = class(TBasicoGridSelect_DevEx)

    procedure Cancelar_DevExClick(Sender: TObject);
    procedure CO_SAT_CLPGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
    procedure CO_SAT_CLNGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
    procedure CO_SAT_EXEGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure DisplayTextCampoGrid( var AText: String; const sMensajeError: String; const aValidacion: TValidacion; const lfValidacion: ListasFijas );
  public
    { Public declarations }
  end;

var
  WizConfiguracionConceptoGridSelect: TWizConfiguracionConceptoGridSelect;

implementation

{$R *.dfm}

procedure TWizConfiguracionConceptoGridSelect.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     TClientDataSet(DataSource.DataSet ).CancelUpdates;
end;

procedure TWizConfiguracionConceptoGridSelect.CO_SAT_CLNGetDisplayText( Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
const
     K_ERROR_CLASE = 'La clase no es correcta';
var
   aValidacion : TValidacion;
   oLista : ListasFijas;
begin
     inherited;
     aValidacion := [ 0, 1, 2, 3, 4 ];
     oLista := ListasFijas( lfClaseConceptosSAT );
     DisplayTextCampoGrid( AText, K_ERROR_CLASE, aValidacion, oLista  );
end;

procedure TWizConfiguracionConceptoGridSelect.CO_SAT_CLPGetDisplayText( Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
const
     K_ERROR_CLASE = 'La clase no es correcta';
var
   aValidacion : TValidacion;
   oLista : ListasFijas;
begin
     inherited;
     aValidacion := [ 0, 1, 2, 3, 4 ];
     oLista := ListasFijas( lfClaseConceptosSAT );
     DisplayTextCampoGrid( AText, K_ERROR_CLASE, aValidacion, oLista );
end;

procedure TWizConfiguracionConceptoGridSelect.CO_SAT_EXEGetDisplayText( Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string );
const
     K_ERROR_CLASE = 'El tratamiento no es correcto';
var
   aValidacion : TValidacion;
   oLista : ListasFijas;
begin
     inherited;
     aValidacion := [ 0, 1, 2, 3, 4, 5, 6, 7 ];
     oLista := ListasFijas( lfTipoExentoSAT );
     DisplayTextCampoGrid( AText, K_ERROR_CLASE, aValidacion, oLista );
end;

procedure TWizConfiguracionConceptoGridSelect.DisplayTextCampoGrid( var AText: String; const sMensajeError: String; const aValidacion: TValidacion; const lfValidacion: ListasFijas );
var
   iValor : Integer;
begin
     try
        iValor := 0;
        if StrLleno( AText ) then
        begin
             iValor := StrToInt ( AText );
             if iValor in aValidacion  then
                AText := ObtieneElemento( lfValidacion, StrToInt( AText ) )
             else
                 AText := sMensajeError;
        end
        else
            AText := sMensajeError;
     except
           on Error : Exception do
           begin
                AText := sMensajeError;
           end;
     end;
end;

procedure TWizConfiguracionConceptoGridSelect.FormShow(Sender: TObject);
const
     K_WIDTH_FIX = 19;
begin
     inherited;
     if ( Width < 550 ) then
        Width := 550
     else
         if ( Width < 1261 ) then
            Width := Width + K_WIDTH_FIX
         else
             Width := 1280;
     resize;
end;

end.
