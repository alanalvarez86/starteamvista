object WizardTimbradoThreadForm: TWizardTimbradoThreadForm
  Left = 156
  Top = 140
  Caption = 'Timbrado de Recibos de N'#243'mina'
  ClientHeight = 645
  ClientWidth = 673
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxWizardControl1: TdxWizardControl
    Left = 0
    Top = 0
    Width = 673
    Height = 645
    AutoSize = True
    Buttons.Back.Caption = '&Atras'
    Buttons.Back.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F342370000008349444154384F63201674D77733F634A2
      602610DDDDD8C5DC55DF25015546186031088C810631020D62842A230C800685
      0035FE06E2FFC81868D075525D346A100130F80CEAADEF55C1661008030D3280
      2A231E00351560338C6497C100D50DECAEEFCC001A4079D8C100D522030646A6
      81F8CA33AA18C800C2400341490AA8828101008A004FA9E01121AC0000000049
      454E44AE426082}
    Buttons.Cancel.Caption = '&Cancelar'
    Buttons.Cancel.Glyph.Data = {
      89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
      610000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC7249000000BA49444154384F8D913112C220104529BC574AB07746BC
      85F7B0F018293C84BD92135968FE861F11D8357FE617C0BE97998D639E311CA6
      E82FF9A8063398CDC725B84831BCE6BEA7D3FE9AAF9BE00D33985D2525CCF624
      05CC2E9214FD583D484B4907CEF5A3BB0FC32E1DFDAD370050856706AC7CC192
      745BC2CC66490F66FE4A2C98D11766FF628905B3AA640BCC361215C63E949DAC
      120BC6C2ACC58AE411C3B979ACB6AD49C0CAC08FA482995AB2C28C481498A1E4
      0B3BF701FFE789DE4F9390EE0000000049454E44AE426082}
    Buttons.CustomButtons.Buttons = <>
    Buttons.Finish.Caption = '&Aplicar'
    Buttons.Finish.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F34237000001FB49444154384FCD93CF2B055114C767DE
      F3E4577EC702C90E4549598884B9C34A6225B2B050786FE6D988CDDB4A562C10
      FF00A5B0B03233AFC4C242922CD850F2A388A5ADCF7926F5123D4A39F5E9CE9C
      7BCEF79E7BCFBDDABF32CB515AC4513AF89E5F9AED29DD76CD0CCB35F320CD77
      FFCCACB8D22C4F056DCF2C44AC0DA121C64A7F3A75B31C43B6962EC9880DC226
      DFA730A685F7BA3EF8CE6CAA89789D019273486C8069BE0FE1096EA96E960015
      E0232B8163EA7E6E92D9F82DCF0C59AE2A21B11B9611BB607C8413DB55F36CBB
      51F65E8AA31D5A09C8A5822441CB33E490B359AC1A4688DB863BB8855D9A301E
      F58CAA49AF33281DE9C3B9096B2421A832C6E36D1CB2C19CC121AB22E65A2006
      07700FE7B08A782F39A510A4FA447BC34CDCC035CC410DD5A611C8B6CC0AFEFB
      6105CE402A12C1188B36939BCF8281280B270C074E73075EE018C28848A76A61
      0264EECA670B4611AA864C7293CF98C95CCA1C603C826770F89F61942AC5F700
      B2AD25E88132844272453E195504E85439415370092228A3542242FB20E24D6C
      BFE0FDB27EF174E89674349D807A1216410EF815E41C37406E7715F06C78835F
      098949D710D311CB21D1807538A6E205C60EAA2E8ED0ADC938DD4AC5A2AE9246
      C8ED966B60C23062755493250BF961A91B1508727ED988E533861277E7B7E60B
      4A95BA9DEAB6FEC634ED0DA90748A38751FD620000000049454E44AE426082}
    Buttons.Help.Visible = False
    Buttons.Next.Caption = '&Siguiente'
    Buttons.Next.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F34237000000A449444154384F630081AEFA2E89EEC62E
      869EC66E0CDC5DDFCD0856440A001AC88664202332061AC80C55463C80BAF03A
      D080FF68F837D0C010A832E2C1A8812806F6D477A64095110FF018F81F285700
      55463CA0BA811DF51D3240CDDFD10D036160183A40951106F5F5F52C3D8D5DAB
      B119D4D3D4DD0F5546188C1A44180C5A8398801A61E51890DFCD0EC41C408388
      2FCFA0068134A2148A60DCD40D554508303000009CD04F09670CD6D200000000
      49454E44AE426082}
    Buttons.Next.GlyphAlignment = blGlyphRight
    Buttons.Next.Width = 78
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F40000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC7249000004BF494441545847AD972D6C5C4714850D020A0A020A0A02
      0A0202835A7BD7D28675FDDE735D104B0101060505050B0A0A23050414140418
      2C080828300828280C30080C3428083008282808080870CF77E6DE79B39B75B4
      953AD2D19BB973E7CEB93F33B3BB334CFB379F42BFD7DFDF8996B2C369BF987F
      3DBF358EE7BBCCA37B38E9CEADACC63875AEC38E8C5D1D4EFA275A78F211A6DD
      05DFB057740B1E1D7DF3ED57391E26FD9F32F64CBAE7361A2DEC9CC777CD76BF
      64AD8DF6D3FE5E37E9EEB60AC7B3E3CFF57DD9F44FCA26FD5BAD7934CC862F3C
      0E129A67A38F08A093EB2B760F6FFBDB12C0683526D8C32090DEF295B7AFD08D
      3DC6144E0E7E1CA6DDCF9B08B4D1329288FAFF8940834A201B1BC5DC25FAC8FE
      3702DE215A6CB45203AC672E8D821C6F4D0025BE89D96CF699945608A8287FD7
      F8E1D1ECE826F3E8E5FA30B8C40EB07E1048DD4437EBBE0CFD4220AB781D2EB8
      86403966FD638C40426B1D3513A02FC229F7D704BABF36DB56D106018C5C0B4E
      47EB8164AF91E72930F6863B45BFBB90CE4F10C14B9FACC6D626D83394392A6C
      C40563A19A37119A1C5E0E93EE5D5DA8C69CD3A5CB298EA88B106013E4187DDB
      2B29BC913272E55CE3A13CAAC7283D4411027C19B70490B38E7E446863B10174
      727DDAF43916F3F752FCFBBB49F7342340C1E16D41FF8A05B0C6032D5C6ACDAF
      E885A117D40791609C90FC14BBFAD6A399046A043410535DC532C26493DBE70E
      A98AA57D0F6831FF2C863582D442CE153BC33EF278138A7CDAFD426DA4AE8B10
      260EFFDEC1F795407C35EFA25B03B22A979EA2580828726792395D51840B934C
      3D391A7D6C5F39840936CDD038DCA1B411256D785EB0DFCD4981BDD63C3690A9
      FF226BC47AE5817B19577AB907126CE88561A010E8CEE8B7E0CE974EADF684EF
      89B0C5984DF932AE0414293F4E7AC28908F295960B024B3C6A8DE8D1F941467C
      89AC63DD81D61EFD2400EAE62C22771EA8350416DC0FAD8C7E898AE6A96E8CB9
      EFDF133CC5F5DA95FC122F33A56C1869639DD07F300929BA08314ECBCD6A510A
      F99E334F1593471DCF7F589BEBED081118AFE885747E2382AC33D982F8DDE034
      5EF9F248F86844F52B9F0FFC4608F9938BAFF5E2DD07752DBF0744361E1A5FC5
      7CD9B43D0DEB4411C40F0A3CEACE365D4430B6AC84D9BF0919D3F0C2501A924C
      42F2D7253DFD13D6A14F04894CEAD8080D36184A459AFA661BC3DAAC2BEFE813
      FE92B672FEAF436B372EA6F2A3341B21C1D0A6C728C319626F4A58E923F7CD16
      B75EE65658D46890CE76B3B6E1E1A7B17A0CD9840D9B62AB8F112D7591716401
      32D95A2190B5963954AE5C302B705DE0552AAB95B9926FF76333179A6EBE6D08
      1051A2829E09A09C2948707E83C8499E651B9AF47F48A617AEDC90B95EDFF28B
      2865DAA0AE5114934071A6A6692460032104361404D2A8A1B1D1E86E22B08E4A
      40916CE55B116823A0B995371EDD9640AB5BE6391DE3F12602C85DB45ABB1581
      B606D283F4883EEBF3B2517DF8874A36F4C7B5BA03622CBDC7D8AE061A1286AB
      3C5390155F42CF3C3F5E2A010CFA64D8E3D1868F675CE7D6C59EF40DF591D980
      309ED90692733AC6FF054108B404C2A0896675C77BBF80147AD80BCFF52BCB3F
      5E4F9111A272155F036E2D0CD0B89E19036DE8EBB9D5CB371E9073D20189D463
      DE3281F961DABFF917E869BD82C68855EE0000000049454E44AE426082}
    OptionsAnimate.TransitionEffect = wcteSlide
    ParentFont = False
    OnButtonClick = dxWizardControl1ButtonClick
    OnPageChanged = dxWizardControl1PageChanged
    OnPageChanging = dxWizardControl1PageChanging
    object dxWizardControlPageInicio: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.AssignedValues = [wchvGlyph]
      Header.Glyph.Data = {
        89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
        F40000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
        BC0195BC7249000004BF494441545847AD972D6C5C4714850D020A0A020A0A02
        0A0202835A7BD7D28675FDDE735D104B0101060505050B0A0A23050414140418
        2C080828300828280C30080C3428083008282808080870CF77E6DE79B39B75B4
        953AD2D19BB973E7CEB93F33B3BB334CFB379F42BFD7DFDF8996B2C369BF987F
        3DBF358EE7BBCCA37B38E9CEADACC63875AEC38E8C5D1D4EFA275A78F211A6DD
        05DFB057740B1E1D7DF3ED57391E26FD9F32F64CBAE7361A2DEC9CC777CD76BF
        64AD8DF6D3FE5E37E9EEB60AC7B3E3CFF57DD9F44FCA26FD5BAD7934CC862F3C
        0E129A67A38F08A093EB2B760F6FFBDB12C0683526D8C32090DEF295B7AFD08D
        3DC6144E0E7E1CA6DDCF9B08B4D1329288FAFF8940834A201B1BC5DC25FAC8FE
        3702DE215A6CB45203AC672E8D821C6F4D0025BE89D96CF699945608A8287FD7
        F8E1D1ECE826F3E8E5FA30B8C40EB07E1048DD4437EBBE0CFD4220AB781D2EB8
        86403966FD638C40426B1D3513A02FC229F7D704BABF36DB56D106018C5C0B4E
        47EB8164AF91E72930F6863B45BFBB90CE4F10C14B9FACC6D626D83394392A6C
        C40563A19A37119A1C5E0E93EE5D5DA8C69CD3A5CB298EA88B106013E4187DDB
        2B29BC913272E55CE3A13CAAC7283D4411027C19B70490B38E7E446863B10174
        727DDAF43916F3F752FCFBBB49F7342340C1E16D41FF8A05B0C6032D5C6ACDAF
        E885A117D40791609C90FC14BBFAD6A399046A043410535DC532C26493DBE70E
        A98AA57D0F6831FF2C863582D442CE153BC33EF278138A7CDAFD426DA4AE8B10
        260EFFDEC1F795407C35EFA25B03B22A979EA2580828726792395D51840B934C
        3D391A7D6C5F39840936CDD038DCA1B411256D785EB0DFCD4981BDD63C3690A9
        FF226BC47AE5817B19577AB907126CE88561A010E8CEE8B7E0CE974EADF684EF
        89B0C5984DF932AE0414293F4E7AC28908F295960B024B3C6A8DE8D1F941467C
        89AC63DD81D61EFD2400EAE62C22771EA8350416DC0FAD8C7E898AE6A96E8CB9
        EFDF133CC5F5DA95FC122F33A56C1869639DD07F300929BA08314ECBCD6A510A
        F99E334F1593471DCF7F589BEBED081118AFE885747E2382AC33D982F8DDE034
        5EF9F248F86844F52B9F0FFC4608F9938BAFF5E2DD07752DBF0744361E1A5FC5
        7CD9B43D0DEB4411C40F0A3CEACE365D4430B6AC84D9BF0919D3F0C2501A924C
        42F2D7253DFD13D6A14F04894CEAD8080D36184A459AFA661BC3DAAC2BEFE813
        FE92B672FEAF436B372EA6F2A3341B21C1D0A6C728C319626F4A58E923F7CD16
        B75EE65658D46890CE76B3B6E1E1A7B17A0CD9840D9B62AB8F112D7591716401
        32D95A2190B5963954AE5C302B705DE0552AAB95B9926FF76333179A6EBE6D08
        1051A2829E09A09C2948707E83C8499E651B9AF47F48A617AEDC90B95EDFF28B
        2865DAA0AE5114934071A6A6692460032104361404D2A8A1B1D1E86E22B08E4A
        40916CE55B116823A0B995371EDD9640AB5BE6391DE3F12602C85DB45ABB1581
        B606D283F4883EEBF3B2517DF8874A36F4C7B5BA03622CBDC7D8AE061A1286AB
        3C5390155F42CF3C3F5E2A010CFA64D8E3D1868F675CE7D6C59EF40DF591D980
        309ED90692733AC6FF054108B404C2A0896675C77BBF80147AD80BCFF52BCB3F
        5E4F9111A272155F036E2D0CD0B89E19036DE8EBB9D5CB371E9073D20189D463
        DE3281F961DABFF917E869BD82C68855EE0000000049454E44AE426082}
      Header.Description = 
        'Mecanismo que genera el timbrado usando el complemento de n'#243'mina' +
        ' 1.2 para cada uno de los recibos incluidos en el proceso.'
      Header.Title = 'Timbrado de N'#243'mina'
      ParentFont = False
      object Label3: TLabel
        Left = 15
        Top = 437
        Width = 66
        Height = 13
        Caption = 'Razon Social:'
        Visible = False
      end
      object gridPeriodos: TZetaCXGrid
        Left = 0
        Top = 223
        Width = 651
        Height = 282
        Align = alCustom
        TabOrder = 3
        object gridPeriodosDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          DataController.DataSource = dataSourceTimbrar
          DataController.Filter.OnGetValueList = gridPeriodosDBTableView1DataControllerFilterGetValueList
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsView.NoDataToDisplayInfoText = 'No hay Periodos'
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object gridPeriodosDBTableView1Column10: TcxGridDBColumn
            Caption = 'Status Env'#237'o'
            DataBinding.FieldName = 'PE_T_STATUS'
            Visible = False
            Width = 85
          end
          object gridPeriodosDBTableView1Column8: TcxGridDBColumn
            Caption = 'Contribuyente'
            DataBinding.FieldName = 'RS_NOMBRE'
            Options.Editing = False
            Options.Filtering = False
            Width = 190
          end
          object gridPeriodosDBTableView1Column2: TcxGridDBColumn
            Caption = 'N'#243'mina'
            DataBinding.FieldName = 'TP_NOMBRE'
            Visible = False
            Options.Editing = False
            Width = 85
          end
          object gridPeriodosDBTableView1Column9: TcxGridDBColumn
            Caption = 'Periodo'
            DataBinding.FieldName = 'PE_NUMERO'
            Visible = False
            Width = 53
          end
          object gridPeriodosDBTableView1Column5: TcxGridDBColumn
            Caption = 'Empleados'
            DataBinding.FieldName = 'PE_NUM_EMP'
            Options.Editing = False
            Width = 80
          end
          object gridPeriodosDBTableView1Column7: TcxGridDBColumn
            Caption = 'Timbrados'
            DataBinding.FieldName = 'TIMBRADOS'
            Width = 80
          end
          object gridPeriodosDBTableView1Column11: TcxGridDBColumn
            Caption = 'Por Timbrar'
            DataBinding.FieldName = 'PORTIMBRAR'
            Width = 80
          end
          object gridPeriodosDBTableView1Column4: TcxGridDBColumn
            Caption = 'Monto Neto'
            DataBinding.FieldName = 'PE_TOT_NET'
            Visible = False
            Options.Editing = False
            Width = 79
          end
        end
        object gridPeriodosLevel1: TcxGridLevel
          GridView = gridPeriodosDBTableView1
        end
      end
      object DBEdit1: TDBEdit
        Left = 35
        Top = 393
        Width = 79
        Height = 21
        DataField = 'RS_NOMBRE'
        DataSource = dataSourceContribuyente
        ReadOnly = True
        TabOrder = 1
        Visible = False
      end
      object cxCheckBox1: TcxCheckBox
        Left = 150
        Top = 393
        Caption = 'Ver Recibos a Timbrar'
        ParentBackground = False
        ParentColor = False
        Style.Color = clWhite
        TabOrder = 2
        Visible = False
        Width = 145
      end
      object cxGroupBox2: TcxGroupBox
        Left = 0
        Top = 0
        Align = alTop
        Caption = ' Periodo: '
        TabOrder = 0
        Height = 216
        Width = 651
        object PeriodoTipoLbl: TLabel
          Left = 59
          Top = 13
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
          Color = clWhite
          ParentColor = False
        end
        object Label6: TLabel
          Left = 71
          Top = 85
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al:'
          Color = clWhite
          ParentColor = False
        end
        object PeriodoNumeroLBL: TLabel
          Left = 43
          Top = 37
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
          Color = clWhite
          ParentColor = False
        end
        object Label8: TLabel
          Left = 64
          Top = 63
          Width = 19
          Height = 13
          Alignment = taRightJustify
          Caption = 'Del:'
          Color = clWhite
          ParentColor = False
        end
        object FechaInicial: TZetaTextBox
          Left = 86
          Top = 59
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object FechaFinal: TZetaTextBox
          Left = 86
          Top = 83
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object iNumeroNomina: TZetaTextBox
          Left = 86
          Top = 34
          Width = 65
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object sDescripcion: TZetaTextBox
          Left = 160
          Top = 35
          Width = 255
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object iTipoNomina: TZetaTextBox
          Left = 86
          Top = 11
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ZetaDBTextBox2: TZetaDBTextBox
          Left = 86
          Top = 107
          Width = 200
          Height = 17
          AutoSize = False
          Caption = 'ZetaDBTextBox2'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_TIMBRO'
          DataSource = DatasetPeriodo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label1: TLabel
          Left = 50
          Top = 106
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
          Color = clWhite
          ParentColor = False
        end
        object Label4: TLabel
          Left = 24
          Top = 156
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descripci'#243'n:'
          Color = clWhite
          ParentColor = False
        end
        object Label7: TLabel
          Left = 22
          Top = 184
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = 'Periodicidad:'
          Color = clWhite
          ParentColor = False
        end
        object lblUso: TLabel
          Left = 61
          Top = 133
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'Uso:'
          Color = clWhite
          ParentColor = False
        end
        object UsoPeriodo: TZetaTextBox
          Left = 86
          Top = 131
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object cxCbDescripcion: TcxComboBox
          Left = 85
          Top = 154
          Hint = 'Seleccione o captura le descripci'#243'n de n'#243'mina a timbrar'
          ParentFont = False
          ParentShowHint = False
          Properties.Items.Strings = (
            'Pago de n'#243'mina'
            'Aguinaldo'
            'Prima vacacional'
            'Fondo de ahorro'
            'Liquidaci'#243'n '
            'Finiquito')
          ShowHint = True
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 0
          Text = 'Pago de n'#243'mina'
          Width = 202
        end
        object cxPeriodicidadPago: TZetaKeyCombo
          Left = 86
          Top = 181
          Width = 200
          Height = 21
          Hint = 'Seleccione la periodicidad de n'#243'mina a timbrar'
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          TextHint = 'Seleccione la periodicidad de n'#243'mina a timbrar'
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
    end
    object dxWizardControlPageOpciones: TdxWizardControlPage
      Header.Description = 
        'Active y configure las opciones que necesite para timbrar correc' +
        'tamente su n'#243'mina.'
      Header.Title = 'Opciones de Timbrado de N'#243'mina'
      object CxGroupOpcionesEnvio: TcxGroupBox
        Left = 0
        Top = 0
        Align = alTop
        Caption = ' Enviar detalle de: '
        TabOrder = 0
        Height = 145
        Width = 651
        object cxEnviarDetalle: TcxCheckBox
          Left = 10
          Top = 25
          Caption = 'Montos por concepto.'
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = []
          Style.TransparentBorder = True
          Style.IsFontAssigned = True
          TabOrder = 0
          Width = 209
        end
        object cxEnviarIncapacidades: TcxCheckBox
          Left = 10
          Top = 50
          Caption = 'Incapacidades.'
          ParentBackground = False
          ParentColor = False
          Style.Color = clWhite
          Style.TransparentBorder = True
          TabOrder = 1
          Width = 191
        end
        object cxEnviarSubContratos: TcxCheckBox
          Left = 10
          Top = 75
          Caption = 'Subcontratos.'
          ParentBackground = False
          ParentColor = False
          Style.Color = clWhite
          Style.TransparentBorder = True
          TabOrder = 2
          Width = 191
        end
        object cxEnviarTiempoExtra: TcxCheckBox
          Left = 10
          Top = 100
          Caption = 'Tiempo extra.'
          ParentBackground = False
          ParentColor = False
          Style.Color = clWhite
          Style.TransparentBorder = True
          TabOrder = 3
          Width = 209
        end
      end
      object CxAjustaFechaPago: TdxCheckGroupBox
        Left = 0
        Top = 145
        Align = alTop
        Caption = 'Ajustar fecha de pago:'
        CheckBox.Checked = False
        TabOrder = 1
        OnClick = CxAjustaFechaPagoClick
        Height = 65
        Width = 651
        object LblFechaPago: TLabel
          Left = 32
          Top = 28
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha de pago:'
          Color = clWhite
          ParentColor = False
        end
        object PE_FECH_PAG: TZetaFecha
          Left = 113
          Top = 25
          Width = 115
          Height = 22
          Cursor = crArrow
          Enabled = False
          TabOrder = 0
          Text = '29/Apr/14'
          Valor = 41758.000000000000000000
        end
      end
      object cxEsUnaNominaAjuste: TdxCheckGroupBox
        Left = 0
        Top = 210
        Align = alTop
        Caption = 'Es una n'#243'mina de ajuste'
        CheckBox.Checked = False
        TabOrder = 2
        OnClick = cxEsUnaNominaAjusteClick
        Height = 65
        Width = 651
        object lblNominaOriginal: TLabel
          Left = 32
          Top = 28
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#243'mina original:'
          Color = clWhite
          ParentColor = False
        end
        object lookupNominaOriginal: TZetaKeyLookup_DevEx
          Left = 113
          Top = 24
          Width = 300
          Height = 21
          Hint = 
            'N'#243'mina que contiene los timbres cancelados y que ser'#225'n sustituid' +
            'os por '#233'ste periodo'
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
      end
    end
    object dxWizardControlFiltroEmpleados: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Genera la lista de empleados a los cuales se les aplicar'#225' en el ' +
        'proceso.'
      Header.Title = 'Filtro de Empleados '
      ParentFont = False
      object GroupBox1: TGroupBox
        Left = 89
        Top = 157
        Width = 473
        Height = 49
        Caption = ' Orden: '
        Color = clWhite
        ParentColor = False
        TabOrder = 1
        Visible = False
        object Label5: TLabel
          Left = 52
          Top = 20
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = '&Ordenar por:'
        end
        object cbOrden: TZetaKeyCombo
          Left = 116
          Top = 17
          Width = 176
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object FiltrosGB: TGroupBox
        Left = 89
        Top = 70
        Width = 473
        Height = 312
        Caption = ' Para Generar Lista de Empleados: '
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object sCondicionLBl: TLabel
          Left = 33
          Top = 157
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Con&dici'#243'n:'
          FocusControl = ECondicion
        end
        object sFiltroLBL: TLabel
          Left = 58
          Top = 180
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = '&Filtro:'
        end
        object sFiltro: TcxMemo
          Left = 86
          Top = 178
          ParentFont = False
          Properties.ScrollBars = ssVertical
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Courier New'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 0
          Height = 83
          Width = 314
        end
        object Seleccionar: TcxButton
          Left = 152
          Top = 267
          Width = 173
          Height = 26
          Hint = 'Invocar Pantalla de Selecci'#243'n de Empleados'
          Caption = ' Verificar Lista de Empleados'
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            200000000000000900000000000000000000000000000000000050D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF5ED69BFFE6F9F0FFBAEDD5FF5BD59AFFF4FCF8FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFCFEFDFFC5F0DBFF55D396FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFBAEDD5FFFFFFFFFFFFFFFFFF6EDAA6FF60D69DFF66D8
            A1FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF66D8A1FF66D8
            A1FF79DDACFFE6F9F0FFAAE9CAFF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF76DCABFFFFFFFFFFFFFFFFFFFFFFFFFFB5ECD1FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFAAE9CAFFD0F3E2FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFA5E8C8FFFFFFFFFFAAE9CAFFFAFEFCFFF7FDFAFF58D498FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF60D69DFF8AE1B7FF50D293FFBAEDD5FFFFFFFFFF9AE5C1FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF74DBA9FFFFFFFFFFE4F8EEFF50D2
            93FF53D395FF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDD
            AEFF58D498FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFD9F5E7FFFFFFFFFF7FDE
            B0FF53D395FF8FE2BAFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
            BCFF58D498FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF95E4BDFF69D8A2FF8FE2BAFFFFFFFFFFB8ED
            D3FF55D396FFBAEDD5FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEE
            D6FF60D69DFFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF92E3BCFF53D395FF9DE6C2FF79DD
            ACFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF92E3BCFF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF92E3BCFF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFA8E9C9FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF92E3BCFF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF71DAA7FFCEF2E1FFE9F9F1FFE9F9
            F1FFE9F9F1FFF4FCF8FFD0F3E2FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF92E3BCFF60D69DFF92E3BCFF79DD
            ACFF53D395FF8FE2BAFF92E3BCFF92E3BCFFECFAF3FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF7FDFAFF79DDACFF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF92E3BCFF92E3BCFF9DE6C2FFC3EF
            DAFF50D293FF79DDACFF7CDDAEFF9DE6C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFF7FDFAFF79DDACFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF92E3BCFF74DBA9FFC3EFDAFFA8E9
            C9FF53D395FFB8EDD3FFBDEED6FFCEF2E1FFFFFFFFFFFFFFFFFFFFFFFFFFF7FD
            FAFF79DDACFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFC8F1DDFFA2E7C6FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFF7FDFAFF79DD
            ACFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF97E4BFFFF1FBF7FF87E0B5FF66D8A1FF66D8
            A1FF66D8A1FF66D8A1FF66D8A1FF8CE2B8FFFFFFFFFFF7FDFAFF79DDACFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FFAAE9CAFFECFAF3FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECFAF3FF79DDACFF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
        end
        object BAgregaCampo: TcxButton
          Left = 401
          Top = 179
          Width = 21
          Height = 21
          Hint = 'Abrir constructor de f'#243'rmulas'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFECD5BFFFF0DE
            CDFFEFDCCBFFDCB28BFFD8AA7FFFE4C5A7FFF2E3D5FFDAAE85FFD8AA7FFFEEDA
            C7FFEAD2BBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFECD5BFFFE1BD9BFFDCB38DFFF8EFE7FFD9AB81FFDAAE85FFDAAE
            85FFE7CBB1FFDAAE85FFFDFBF9FFDDB58FFFDEB793FFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF6EB
            E1FFE6C9ADFFD8AA7FFFD8AA7FFFDAAF87FFEEDAC7FFF6EBE1FFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFECD5BFFFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFEDD7
            C3FFEDD7C3FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE4C3A5FFFEFCFBFFD9AD
            83FFD8AA7FFFD8AA7FFFF5EADFFFF0DFCFFFD9AB81FFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFDAAF87FFFFFFFFFFE2BF9FFFDAAE85FFE1BE9DFFFDFBF9FFDCB38DFFECD5
            BFFFEDD7C3FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF9F3EDFFECD5BFFFD8AA7FFFDEB6
            91FFDFBA97FFD8AA7FFFDAAE85FFDFBA97FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF1E0
            D1FFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFE9CFB7FFFDFBF9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE0BB99FFFFFFFFFFDFBA
            97FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAF
            87FFDEB793FFFFFFFFFFECD5BFFFDDB58FFFDAAF87FFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFE2BF9FFFECD5BFFFF9F2EBFFF9F2EBFFECD5BFFFE2BF
            9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE9CF
            B7FFF9F2EBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFDCB28BFFFEFCFBFFDCB38DFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEAD2BBFFECD6
            C1FFD8AA7FFFF5E8DDFFE2BF9FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFEBD3BDFFE9CFB7FFF3E4D7FFDBB189FFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
            7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BAgregaCampoClick
        end
        object ECondicion: TZetaKeyLookup_DevEx
          Left = 86
          Top = 153
          Width = 338
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 3
          TabStop = True
          WidthLlave = 82
        end
        object GBRango: TGroupBox
          Left = 86
          Top = 29
          Width = 313
          Height = 118
          TabOrder = 4
          object lbInicial: TLabel
            Left = 24
            Top = 52
            Width = 30
            Height = 13
            Alignment = taRightJustify
            Caption = 'Inicial:'
            Enabled = False
          end
          object lbFinal: TLabel
            Left = 171
            Top = 52
            Width = 25
            Height = 13
            Alignment = taRightJustify
            Caption = 'Final:'
            Enabled = False
          end
          object RBTodos: TcxRadioButton
            Left = 18
            Top = 11
            Width = 113
            Height = 17
            Caption = '&Todos'
            Checked = True
            TabOrder = 0
            TabStop = True
            OnClick = RBTodosClick
            Transparent = True
          end
          object RBRango: TcxRadioButton
            Left = 18
            Top = 30
            Width = 57
            Height = 17
            Caption = '&Rango'
            TabOrder = 1
            OnClick = RBRangoClick
            Transparent = True
          end
          object RBLista: TcxRadioButton
            Left = 18
            Top = 69
            Width = 73
            Height = 17
            Caption = '&Lista'
            TabOrder = 6
            OnClick = RBListaClick
            Transparent = True
          end
          object EInicial: TZetaEdit
            Left = 56
            Top = 47
            Width = 80
            Height = 21
            Enabled = False
            MaxLength = 9
            TabOrder = 2
          end
          object EFinal: TZetaEdit
            Left = 198
            Top = 48
            Width = 80
            Height = 21
            Enabled = False
            MaxLength = 9
            TabOrder = 4
          end
          object ELista: TZetaEdit
            Left = 37
            Top = 87
            Width = 239
            Height = 21
            Enabled = False
            TabOrder = 7
          end
          object BInicial: TcxButton
            Left = 141
            Top = 47
            Width = 21
            Height = 21
            Hint = 'Invocar b'#250'squeda de empleados'
            Enabled = False
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A4050000000000000000000000000000000000008B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCAC6
              CBFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFFB5AEB5FFFFFFFFFFFDFDFDFF9F97A0FF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFBAB4BBFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF988F99FFC0BAC0FFD2CED2FFBEB8BFFFEFEDEFFFFFFFFFFFD3D0
              D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFE9E7E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFF4F3F4FFF0EF
              F1FFAFA9B0FF908791FFAFA9B0FFFBFBFBFFE9E7E9FF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFFA8A1A9FFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFF
              FFFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFFFFFFFFB7B0B7FF8B818CFF8B81
              8CFF8B818CFFC5C0C6FFFFFFFFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFB
              FBFFE9E7E9FF8B818CFF8B818CFF968D97FFEDEBEDFFF8F7F8FF908791FF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFF8F7F8FFDCD9DDFFFAF9FAFFFDFD
              FDFFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAEA7AEFFE7E5
              E8FFF0EFF1FFE0DDE0FFAAA3ABFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BInicialClick
          end
          object BFinal: TcxButton
            Left = 283
            Top = 47
            Width = 21
            Height = 21
            Hint = 'Invocar B'#250'squeda de Empleados'
            Enabled = False
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A4050000000000000000000000000000000000008B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCAC6
              CBFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFFB5AEB5FFFFFFFFFFFDFDFDFF9F97A0FF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFBAB4BBFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF988F99FFC0BAC0FFD2CED2FFBEB8BFFFEFEDEFFFFFFFFFFFD3D0
              D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFE9E7E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFF4F3F4FFF0EF
              F1FFAFA9B0FF908791FFAFA9B0FFFBFBFBFFE9E7E9FF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFFA8A1A9FFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFF
              FFFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFFFFFFFFB7B0B7FF8B818CFF8B81
              8CFF8B818CFFC5C0C6FFFFFFFFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFB
              FBFFE9E7E9FF8B818CFF8B818CFF968D97FFEDEBEDFFF8F7F8FF908791FF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFF8F7F8FFDCD9DDFFFAF9FAFFFDFD
              FDFFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAEA7AEFFE7E5
              E8FFF0EFF1FFE0DDE0FFAAA3ABFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = BFinalClick
          end
          object BLista: TcxButton
            Left = 283
            Top = 87
            Width = 21
            Height = 21
            Hint = 'Invocar B'#250'squeda de Empleados'
            Enabled = False
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A4050000000000000000000000000000000000008B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCAC6
              CBFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFFB5AEB5FFFFFFFFFFFDFDFDFF9F97A0FF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFBAB4BBFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF988F99FFC0BAC0FFD2CED2FFBEB8BFFFEFEDEFFFFFFFFFFFD3D0
              D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFE9E7E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFF4F3F4FFF0EF
              F1FFAFA9B0FF908791FFAFA9B0FFFBFBFBFFE9E7E9FF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFFA8A1A9FFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFF
              FFFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFFFFFFFFB7B0B7FF8B818CFF8B81
              8CFF8B818CFFC5C0C6FFFFFFFFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFB
              FBFFE9E7E9FF8B818CFF8B818CFF968D97FFEDEBEDFFF8F7F8FF908791FF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFF8F7F8FFDCD9DDFFFAF9FAFFFDFD
              FDFFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAEA7AEFFE7E5
              E8FFF0EFF1FFE0DDE0FFAAA3ABFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
              8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            OnClick = BListaClick
          end
        end
      end
    end
    object dxWizardControlPageReportes: TdxWizardControlPage
      Header.Description = 'Consulta y estatus de reportes de Timbrado de N'#243'mina. '
      Header.Title = 'Reportes de Timbrado'
      object cxCheckListReportes: TcxCheckListBox
        Left = 0
        Top = 0
        Width = 651
        Height = 205
        TabStop = False
        Align = alTop
        AllowDblClickToggle = False
        AllowGrayed = True
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000002000000070000000C0000001000000012000000110000
          000E000000080000000200000000000000000000000000000000000000000000
          000100000004000101120D2A1D79184E36C6216B4BFF216B4BFF216C4BFF1A53
          3AD20F2F21840001011500000005000000010000000000000000000000000000
          0005050F0A351C5B40DC24805CFF29AC7EFF2CC592FF2DC894FF2DC693FF2AAE
          80FF258560FF1A563DD405110C3D00000007000000010000000000000003040E
          0A31206548ED299D74FF2FC896FF2EC996FF56D4ACFF68DAB5FF3BCD9DFF30C9
          96FF32CA99FF2BA479FF227050F805110C3D00000005000000000000000A1A57
          3DD02EA57CFF33CA99FF2EC896FF4CD2A8FF20835CFF00673BFF45BE96FF31CB
          99FF31CB98FF34CC9CFF31AD83FF1B5C41D300010113000000020B23185E2E8A
          66FF3BCD9EFF30CA97FF4BD3A9FF349571FF87AF9DFFB1CFC1FF238A60FF45D3
          A8FF36CF9FFF33CD9BFF3ED0A3FF319470FF0F32237F00000007184D37B63DB3
          8CFF39CD9FFF4BD5A9FF43A382FF699782FFF8F1EEFFF9F3EEFF357F5DFF56C4
          A1FF43D5A8FF3ED3A4FF3CD1A4FF41BC95FF1B5C43CD0000000B1C6446DF4BCA
          A4FF44D2A8FF4FB392FF4E826AFFF0E9E6FFC0C3B5FFEFE3DDFFCEDDD4FF1B75
          4FFF60DCB8FF48D8ACFF47D6AAFF51D4ACFF247A58F80000000E217050F266D9
          B8FF46D3A8FF0B6741FFD2D2CBFF6A8F77FF116B43FF73967EFFF1E8E3FF72A2
          8BFF46A685FF5EDFBAFF4CD9AFFF6BE2C2FF278460FF020604191E684ADC78D9
          BEFF52DAB1FF3DBA92FF096941FF2F9C76FF57DEB8FF2D9973FF73967EFFF0EA
          E7FF4F886CFF5ABB9AFF5BDEB9FF7FE2C7FF27835FF80000000C19523BAB77C8
          B0FF62E0BCFF56DDB7FF59DFBAFF5CE1BDFF5EE2BEFF5FE4C1FF288C67FF698E
          76FFE6E1DCFF176B47FF5FD8B4FF83D5BDFF1E674CC60000000909201747439C
          7BFF95ECD6FF5ADFBAFF5EE2BDFF61E4BFFF64E6C1FF67E6C5FF67E8C7FF39A1
          7EFF1F6D4AFF288B64FF98EFD9FF4DAC8CFF1036286D00000004000000041C5F
          46B578C6ADFF9AEED9FF65E5C0FF64E7C3FF69E7C6FF6BE8C8FF6CE9C9FF6BEA
          C9FF5ED6B6FF97EDD7FF86D3BBFF237759D20102010C0000000100000001030A
          0718247B5BDA70C1A8FFB5F2E3FF98F0DAFF85EDD4FF75EBCEFF88EFD6FF9CF2
          DDFFBAF4E7FF78CDB3FF2A906DEA0615102E0000000200000000000000000000
          0001030A07171E694FB844AB87FF85D2BBFFA8E6D6FFC5F4EBFFABE9D8FF89D8
          C1FF4BB692FF237F60CB05130E27000000030000000000000000000000000000
          000000000001000000030A241B411B60489D258464CF2C9D77EE258867CF1F71
          56B00E3226560000000600000002000000000000000000000000}
        GlyphCount = 0
        Images = cxImageListReportes
        Items = <
          item
            ImageIndex = 0
            Text = 'Reporte "1.2 Timbrado de N'#243'mina".'
          end
          item
          end
          item
            ImageIndex = 0
            Text = 'Reporte "1.2 Timbrado detalle concepto".'
          end
          item
          end
          item
            ImageIndex = 0
            Text = 'Reporte "1.2 Timbrado tiempo extra".'
          end
          item
          end
          item
            ImageIndex = 0
            Text = 'Reporte "1.2 Timbrado incapacidades".'
          end
          item
          end
          item
            ImageIndex = 0
            Text = 'Reporte "1.2 Timbrado subcontratos".'
          end
          item
          end>
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowChecks = False
        ShowHint = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.Shadow = False
        Style.TextStyle = []
        Style.TransparentBorder = True
        Style.IsFontAssigned = True
        TabOrder = 0
        OnDrawItem = cxCheckListReportesDrawItem
      end
    end
    object dxWizardControlPageRecibos: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Verifique que sea correcta la lista de empleados a los cuales se' +
        ' les timbrar'#225' la n'#243'mina.'
      Header.Title = 'Lista de recibos a timbrar '
      ParentFont = False
      object cxGridRecibos: TZetaCXGrid
        Left = 0
        Top = 0
        Width = 651
        Height = 420
        Align = alTop
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          DataController.DataSource = DataSourceRecibos
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnAddValueItems = False
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnMoving = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = 'No hay Recibos'
          OptionsView.GroupByBox = False
          object cxGridDBColumn1: TcxGridDBColumn
            Caption = 'Empleado'
            DataBinding.FieldName = 'COLUMNA2'
            Options.Editing = False
            Width = 85
          end
          object cxGridDBColumn2: TcxGridDBColumn
            Caption = 'Nombre'
            DataBinding.FieldName = 'COLUMNA3'
            Width = 197
          end
          object cxGridDBColumn4: TcxGridDBColumn
            Caption = 'RFC'
            DataBinding.FieldName = 'COLUMNA5'
            Options.Editing = False
            Width = 109
          end
          object cxGridDBColumn3: TcxGridDBColumn
            Caption = 'Percepciones'
            DataBinding.FieldName = 'COLUMNA4'
            Options.Editing = False
            Width = 114
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object cxButton1: TcxButton
        Left = 569
        Top = 426
        Width = 80
        Height = 26
        Hint = 'Exportar a Excel'
        Caption = 'Exportar'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFA2E7C6FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4
          E4FF50D293FF5ED69BFFE1F7ECFFE9F9F1FFE1F7ECFF5ED69BFF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF63D79FFFF4FCF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF50D293FFC5F0DBFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFE4F8EEFF7CDD
          AEFF8AE1B7FFFFFFFFFFE9F9F1FFC8F1DDFF50D293FF50D293FFB5ECD1FFBDEE
          D6FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFC5F0DBFFFFFFFFFFFFFFFFFFA0E6
          C4FFEFFBF5FF74DBA9FF92E3BCFF92E3BCFF92E3BCFF92E3BCFFD6F4E6FFFFFF
          FFFFDBF6E9FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF5ED69BFFF1FBF7FFFFFFFFFFFFFF
          FFFFD6F4E6FFA5E8C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFECFAF3FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF8AE1B7FFFFFFFFFFFFFF
          FFFFE9F9F1FF84DFB3FFFCFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF5BD59AFFF7FDFAFFFFFF
          FFFFFFFFFFFF95E4BDFF50D293FF50D293FF50D293FF50D293FFBDEED6FFFAFE
          FCFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FFBDEED6FFFFFFFFFFFFFF
          FFFFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D293FF7FDEB0FF76DC
          ABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF84DFB3FFFFFFFFFFFFFFFFFFEFFB
          F5FFFFFFFFFFFFFFFFFFCBF2DFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF5BD59AFFECFAF3FFFFFFFFFFFCFEFDFF71DA
          A7FFC5F0DBFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFBDEED6FFFFFFFFFFFFFFFFFFA8E9C9FF50D2
          93FF5ED69BFFECFAF3FFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF6BD9A4FFD3F4E4FFD3F4E4FFC5F0DBFF53D395FF50D2
          93FF50D293FF81DFB1FFD3F4E4FFD3F4E4FFA2E7C6FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = ExportarExcelEmpleadosClick
      end
    end
    object dxWizardControlPageCuentaTimbrado: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Cuenta de sistema de timbrado con la cual se realizar'#225' el timbra' +
        'do de los recibos indicados.'
      Header.Title = 'Cuenta de Sistema de Timbrado '
      PageVisible = False
      ParentFont = False
      object ZetaDBTextBox3: TZetaDBTextBox
        Left = 645
        Top = 395
        Width = 273
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox3'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CT_PASSWRD'
        DataSource = DataSourceCuentas
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblDI_IP: TLabel
        Left = 654
        Top = 418
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contrase'#241'a:'
        Color = clWhite
        ParentColor = False
        Visible = False
      end
      object ZetaDBTextBox1: TZetaDBTextBox
        Left = 222
        Top = 205
        Width = 121
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox1'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CT_ID'
        DataSource = DataSourceCuentas
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 167
        Top = 207
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cuenta ID:'
        Color = clWhite
        ParentColor = False
      end
      object Label2: TLabel
        Left = 134
        Top = 181
        Width = 80
        Height = 13
        Caption = 'Cuenta timbrado:'
        Color = clWhite
        ParentColor = False
      end
      object cxButton2: TcxButton
        Left = 414
        Top = 227
        Width = 108
        Height = 26
        Hint = 'Verificar conectividad'
        Caption = 'Probar conexi'#243'n'
        LookAndFeel.Kind = lfUltraFlat
        LookAndFeel.NativeStyle = False
        LookAndFeel.SkinName = 'TressMorado2013'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF77A5FFFFBFD4FFFFFFFFFFFFD1E0
          FFFF82ACFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFB4CDFFFFD6E4FFFFD6E4FFFFE0EAFFFFFFFFFFFFE8F0
          FFFFD6E4FFFFD6E4FFFFC6D9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFFD3E2FFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6
          FFFFC1D6FFFFCCDDFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFCCDDFFFF6095FFFFBFD4
          FFFF9BBDFFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6599FFFF5B92FFFF6D9E
          FFFF6397FFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC4D8FFFF6397FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF84ADFFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFF87AFFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF72A1FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF8EB4FFFF5E94FFFF5B92
          FFFF5B92FFFF5B92FFFFE3ECFFFFF5F8FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1
          FFFFEAF1FFFFF2F6FFFFFAFCFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFD8E5FFFF6599FFFF5B92
          FFFF5B92FFFF5B92FFFF6397FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0
          FFFF70A0FFFF70A0FFFF6599FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF75A3FFFF84ADFFFF84ADFFFF84ADFFFF7AA7
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94FFFFF2F6FFFF75A3
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF689BFFFFABC7FFFFFFFFFFFFBAD1FFFF6A9C
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF689BFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFD8E5FFFFEAF1FFFFEAF1FFFFF2F6FFFFFFFFFFFFF2F6FFFFEAF1
          FFFFEAF1FFFFDEE9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC4D8FFFF6A9C
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFB4CDFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BB
          FFFFB4CDFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF9BBDFFFF6095
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF6A9CFFFF8CB2FFFF5B92FFFF9BBDFFFF6095
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF7AA7FFFFB7CFFFFF5B92FFFFCCDDFFFF6D9E
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF96B9FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0
          FFFF96B9FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFDEE9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE5EEFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = cxButton1Click
      end
      object CT_CODIGO: TZetaKeyLookup_DevEx
        Left = 222
        Top = 178
        Width = 300
        Height = 21
        Enabled = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CT_CODIGOValidLookup
      end
    end
    object dxWizardControlPageTimbrado: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Al iniciar el proceso se cerrar'#225' el asistente, al concluir recib' +
        'ir'#225' una notificaci'#243'n presentando la bit'#225'cora con los resultados ' +
        'del proceso'
      Header.Title = 'Presione Aplicar para iniciar el proceso'
      ParentFont = False
      object cxGroupBox1: TcxGroupBox
        Left = 0
        Top = 89
        Align = alTop
        Caption = ' Env'#237'o y Procesamiento de Recibos '
        TabOrder = 0
        Height = 85
        Width = 651
        object cxLabel4: TcxLabel
          Left = 24
          Top = 19
          Caption = 'Avance:'
          Transparent = True
        end
        object cxProgressBarEnvio: TcxProgressBar
          Left = 72
          Top = 19
          TabStop = False
          ParentShowHint = False
          Properties.AnimationSpeed = 15
          ShowHint = False
          TabOrder = 1
          Width = 569
        end
        object cxStatus: TcxLabel
          Left = 71
          Top = 41
          Transparent = True
        end
        object cxLabel1: TcxLabel
          Left = 530
          Top = 46
          Caption = 'Saldo:'
          Transparent = True
        end
        object cxSaldo: TcxMaskEdit
          Left = 568
          Top = 44
          ParentFont = False
          Properties.Alignment.Horz = taCenter
          Properties.ReadOnly = True
          Style.Color = clWindow
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
          TabOrder = 4
          Width = 73
        end
        object cxLabel5: TcxLabel
          Left = 381
          Top = 46
          Caption = 'Timbrados:'
          Transparent = True
        end
        object cxTimbrados: TcxMaskEdit
          Left = 440
          Top = 44
          Properties.Alignment.Horz = taCenter
          Properties.ReadOnly = True
          Style.Color = clWindow
          TabOrder = 6
          Width = 73
        end
        object cxLabel6: TcxLabel
          Left = 241
          Top = 46
          Caption = 'A Timbrar:'
          Transparent = True
        end
        object cxTimbrar: TcxMaskEdit
          Left = 296
          Top = 44
          Properties.Alignment.Horz = taCenter
          Properties.ReadOnly = True
          Style.Color = clWindow
          TabOrder = 8
          Width = 73
        end
      end
      object cxGroupBoxResultados: TcxGroupBox
        Left = 0
        Top = 174
        Align = alClient
        Caption = ' Respuesta del Sistema de Timbrado'
        TabOrder = 1
        Height = 331
        Width = 651
        object cxLabel3: TcxLabel
          Left = 32
          Top = 72
        end
        object ZetaCXGrid1: TZetaCXGrid
          Left = 3
          Top = 42
          Width = 645
          Height = 247
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object ZetaCXGrid1DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            FilterBox.CustomizeDialog = False
            DataController.DataSource = DataSourceErrores
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.FocusCellOnTab = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnGrouping = False
            OptionsCustomize.ColumnHidingOnGrouping = False
            OptionsCustomize.ColumnHorzSizing = False
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.GroupBySorting = True
            OptionsCustomize.GroupRowSizing = True
            OptionsSelection.HideSelection = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object ZetaCXGrid1DBTableView1NUMERO: TcxGridDBColumn
              Caption = 'Empleado'
              DataBinding.FieldName = 'NUMERO'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
            end
            object ZetaCXGrid1DBTableView1ERRORID: TcxGridDBColumn
              Caption = 'Error ID'
              DataBinding.FieldName = 'ERRORID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
            end
            object ZetaCXGrid1DBTableView1DESCRIPCION: TcxGridDBColumn
              Caption = 'Descripci'#243'n'
              DataBinding.FieldName = 'DESCRIPCION'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Width = 469
            end
          end
          object ZetaCXGrid1DBTableView2: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DataSourceErrores
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.FocusCellOnTab = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnGrouping = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.HideSelection = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            object ZetaCXGrid1DBTableView2NUMERO: TcxGridDBColumn
              Caption = 'Empleado'
              DataBinding.FieldName = 'NUMERO'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.ReadOnly = True
              MinWidth = 55
              Width = 55
            end
            object ZetaCXGrid1DBTableView2ERRORID: TcxGridDBColumn
              Caption = 'Error ID'
              DataBinding.FieldName = 'ERRORID'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.ReadOnly = True
              MinWidth = 45
              Width = 45
            end
            object ZetaCXGrid1DBTableView2DESCRIPCION: TcxGridDBColumn
              Caption = 'Descripci'#243'n'
              DataBinding.FieldName = 'DESCRIPCION'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.ReadOnly = True
              MinWidth = 70
              Width = 70
            end
            object ZetaCXGrid1DBTableView2DETALLE: TcxGridDBColumn
              Caption = 'Detalle'
              DataBinding.FieldName = 'DETALLE'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.ReadOnly = True
              MinWidth = 70
              Width = 70
            end
          end
          object ZetaCXGrid1Level1: TcxGridLevel
            GridView = ZetaCXGrid1DBTableView2
          end
        end
        object Panel1: TPanel
          Left = 3
          Top = 289
          Width = 645
          Height = 32
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object ZetaSpeedButton1: TcxButton
            Left = 565
            Top = 3
            Width = 80
            Height = 26
            Hint = 'Exportar a Excel'
            Caption = 'Exportar'
            OptionsImage.Glyph.Data = {
              36090000424D3609000000000000360000002800000018000000180000000100
              200000000000000900000000000000000000000000000000000050D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FFA2E7C6FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4E4FFD3F4
              E4FF50D293FF5ED69BFFE1F7ECFFE9F9F1FFE1F7ECFF5ED69BFF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF63D79FFFF4FCF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF50D293FFC5F0DBFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFE4F8EEFF7CDD
              AEFF8AE1B7FFFFFFFFFFE9F9F1FFC8F1DDFF50D293FF50D293FFB5ECD1FFBDEE
              D6FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FFC5F0DBFFFFFFFFFFFFFFFFFFA0E6
              C4FFEFFBF5FF74DBA9FF92E3BCFF92E3BCFF92E3BCFF92E3BCFFD6F4E6FFFFFF
              FFFFDBF6E9FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF5ED69BFFF1FBF7FFFFFFFFFFFFFF
              FFFFD6F4E6FFA5E8C8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFECFAF3FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF8AE1B7FFFFFFFFFFFFFF
              FFFFE9F9F1FF84DFB3FFFCFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF5BD59AFFF7FDFAFFFFFF
              FFFFFFFFFFFF95E4BDFF50D293FF50D293FF50D293FF50D293FFBDEED6FFFAFE
              FCFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FFBDEED6FFFFFFFFFFFFFF
              FFFFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D293FF7FDEB0FF76DC
              ABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF84DFB3FFFFFFFFFFFFFFFFFFEFFB
              F5FFFFFFFFFFFFFFFFFFCBF2DFFF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF5BD59AFFECFAF3FFFFFFFFFFFCFEFDFF71DA
              A7FFC5F0DBFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FFBDEED6FFFFFFFFFFFFFFFFFFA8E9C9FF50D2
              93FF5ED69BFFECFAF3FFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF6BD9A4FFD3F4E4FFD3F4E4FFC5F0DBFF53D395FF50D2
              93FF50D293FF81DFB1FFD3F4E4FFD3F4E4FFA2E7C6FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
            OptionsImage.Margin = 1
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = ZetaSpeedButton1Click
          end
        end
        object Panel2: TPanel
          Left = 3
          Top = 15
          Width = 645
          Height = 27
          Align = alTop
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          object cxErrores: TcxMaskEdit
            Left = 565
            Top = 0
            Properties.Alignment.Horz = taCenter
            Properties.ReadOnly = True
            Style.Color = clWindow
            Style.TextColor = clMaroon
            TabOrder = 0
            Width = 73
          end
          object cxLabel2: TcxLabel
            Left = 521
            Top = 1
            Caption = 'Errores:'
            Transparent = True
          end
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 651
        Height = 89
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object Advertencia: TcxLabel
          Left = 66
          Top = 0
          Align = alClient
          AutoSize = False
          ParentColor = False
          ParentFont = False
          Style.BorderColor = clScrollBar
          Style.BorderStyle = ebsFlat
          Style.Color = clInfoBk
          Style.Edges = [bTop, bRight, bBottom]
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Meiryo UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Properties.ShadowedColor = clInfoBk
          Properties.WordWrap = True
          Height = 89
          Width = 585
          AnchorY = 45
        end
        object cxImage1: TcxImage
          Left = 0
          Top = 0
          Align = alLeft
          AutoSize = True
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000040
            000000400806000000AA6971DE0000000467414D410000B18F0BFC6105000000
            097048597300000EBC00000EBC0195BC7249000004A149444154785EDD5B2D70
            1341148E40202A110804A202814020101515150804025181405454542011CC5D
            672A2A100844052282617A97834154545454202A2A2A1A72172A221011888A0A
            44057CDFE63549C94B73C9EDDE6DFBCD7CC94C76F7DECFFEBDDD7BA9B946B8BF
            7F2B8C7E2C85516B258CD3F5A0916D058DF6377C1F801DC3383BC4F74E90B43F
            B20EB81A6EB79E85F5CE6D79CCF542D868CEC1E0E5A0917E8271676123FB3B0B
            D1FE0F9D1536D257E1E7EC8E3CDE4F989E8EB397419CEE5271CDA0228423CF31
            4AF6C3B8B5E6DDC8E0708582479AE22E0859271C1522BE3A8449FAD8F48AA264
            19344EC7FA22EA9407CE47084F34A5AAA0E984EDF4BEA8E716E197EC416F08EA
            CA5445E8D4C5F782A8E90658E49E42D0CCABBA6B42B773676B431865AF7B0274
            E13E3188DB9BDC9544F5E280E11F34413E133A27569CC09ED70414659064DB88
            F85E9050B6AED5294A4496EFC58CD92073DEFEB047C02422FAE06F6ADDA24408
            2E22A683ACF6D6173C448ADF45C40858A6B52942D38149BA2822F241F679275B
            1DF6EC772266042CD3DA142542F3DF61DC9C17319301E39D05393C20899811B0
            4C6B6383B0E950C45C0D86B7DA036C118A9C865F7FDE13717DF03753A6B4B146
            2CB8226E3CCA88EDD91B5C63442416C0E6BCF94DA96B93907172E5D6682E2194
            86AE08671F935A9933E2382DE65E063D030F9576A4AD8AB0116786E69C983D80
            B37DD84746E95B317B00DEE4A8956F20616B2A66F7C021E1E21ACB6B0EC705BC
            C0542BDD68B6DE88F96E0390496494869DA0C36FADDC15FB21B9ACFEA55F72C0
            E80333F2645F3657DF4A3DB76CDEC5F0FFB1A417DA27B72033DAA2D613E3FD21
            54E2009E14F9A1165AA2B99DC1B17AD2A565150E08926C03FB7FBAAE155A63CE
            A368250E88B33A63FF2DADD01ABD7640BA0B07F0DD9B5EC11931EAC4EE3E2A1A
            01471C01075AA153FAE3802E1DD0D10A9DD21307903C03FCD20A9CD22F07B8BF
            8818A12F5300D127A7C08E56E894DE38203BAE99B414A5D029FD71C09EFB4048
            A32F0E6020046556B542A7F4C601EDCDD22F420D3D7180B92065C21156C3726F
            837C71C0C501ADF470D803079830F802A57BDF871130AC83BC0C2D2FFBC38B29
            D07E24A27BC09E585EBA5BC50EE0F947C40E60323095CA4E58BD03465FD19BDD
            A0ACF4B70A1D001B4FC7E61D97D60B558E0045F625707B501BDA64450E806DDD
            8989D6A55C936B0E88B2E76A5D9BCC9B30E57C47885ACB22AA0F6688A8752DD1
            AC6F79F30619229AE1A23CA828F1DCB1098CCCEBD3DA1425649E31F547C4E403
            1A2EA061F1E088899649BA6818B51ECAE3C7827506F52D25692A232E17AC2C4C
            39DF0B68304ED09E390D27ADFA93605E6D690FCE4BBE00C5949A89055FDB331D
            57CC981DE6ED71152F4F0A92AFBFD55CA059609CE06881724193886DCBF86160
            48AE5859185DB2E89C9F042E4C656773E4A1D9EA665DEDA74559999D79095D4E
            D031D3EDF33680E1C63F3B54F6072AC8EE725AE68EF05C80C279976094519474
            41C83AE55CF7EA1FA45C759981C924444D691BE44D0E2F33FCFF1F31D608E6E1
            F5F662DD98BC446F1F99DEFEFF0EEFBA80A96866FB4CB20D1853675A0A8D1A9E
            32DC55D0B3CC1CDFEBD56162556B8DD1A03CC6116AB57F058711FAC14F69E200
            00000049454E44AE426082}
          Style.BorderColor = clScrollBar
          Style.Edges = [bLeft, bTop, bBottom]
          TabOrder = 1
        end
      end
    end
  end
  object dataSourceTimbrar: TDataSource
    Left = 492
    Top = 84
  end
  object dataSourceContribuyente: TDataSource
    Left = 170
    Top = 528
  end
  object DataSourceCuentas: TDataSource
    Left = 58
    Top = 514
  end
  object DataSourceRecibos: TDataSource
    Left = 24
    Top = 532
  end
  object DataSourceErrores: TDataSource
    OnDataChange = DataSourceErroresDataChange
    OnUpdateData = DataSourceErroresUpdateData
    Left = 586
    Top = 142
  end
  object DatasetPeriodo: TDataSource
    Left = 544
    Top = 120
  end
  object DataSourceTimbrados: TDataSource
    Left = 328
    Top = 536
  end
  object DataSourceDetalle: TDataSource
    Left = 243
    Top = 532
  end
  object cxImageListReportes: TcxImageList
    FormatVersion = 1
    DesignInfo = 28508739
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000003333
          33FF333333FF333333FF333333FF333333FF333333FF333333FF333333FF3333
          33FF333333FF333333FF333333FF333333FF333333FF00000000000000003333
          33FF333333FF333333FF333333FF333333FF333333FF333333FF333333FF3333
          33FF333333FF333333FF333333FF333333FF333333FF00000000000000003333
          33FF333333FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000333333FF333333FF00000000000000003333
          33FF333333FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000333333FF333333FF00000000000000003333
          33FF333333FF000000001F1F1F991F1F1F99000000001F1F1F991F1F1F991F1F
          1F991F1F1F991F1F1F9900000000333333FF333333FF00000000000000003333
          33FF333333FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000333333FF333333FF00000000000000003333
          33FF333333FF000000001F1F1F991F1F1F99000000001F1F1F991F1F1F991F1F
          1F991F1F1F991F1F1F9900000000333333FF333333FF00000000000000003333
          33FF333333FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000333333FF333333FF00000000000000003333
          33FF333333FF000000001F1F1F991F1F1F99000000001F1F1F991F1F1F991F1F
          1F991F1F1F991F1F1F9900000000333333FF333333FF00000000000000003333
          33FF333333FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000333333FF333333FF00000000000000003333
          33FF333333FF000000001F1F1F991F1F1F99000000001F1F1F991F1F1F991F1F
          1F991F1F1F991F1F1F9900000000333333FF333333FF00000000000000003333
          33FF333333FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000333333FF333333FF00000000000000003333
          33FF333333FF000000001F1F1F991F1F1F99000000001F1F1F991F1F1F991F1F
          1F991F1F1F991F1F1F9900000000333333FF333333FF00000000000000003333
          33FF333333FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000333333FF333333FF00000000000000003333
          33FF333333FF333333FF333333FF333333FF333333FF333333FF333333FF3333
          33FF333333FF333333FF333333FF333333FF333333FF00000000000000003333
          33FF333333FF333333FF333333FF333333FF333333FF333333FF333333FF3333
          33FF333333FF333333FF333333FF333333FF333333FF00000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000002000000070000000C0000001000000012000000110000
          000E000000080000000200000000000000000000000000000000000000000000
          000100000004000101120D2A1D79184E36C6216B4BFF216B4BFF216C4BFF1A53
          3AD20F2F21840001011500000005000000010000000000000000000000000000
          0005050F0A351C5B40DC24805CFF29AC7EFF2CC592FF2DC894FF2DC693FF2AAE
          80FF258560FF1A563DD405110C3D00000007000000010000000000000003040E
          0A31206548ED299D74FF2FC896FF2EC996FF56D4ACFF68DAB5FF3BCD9DFF30C9
          96FF32CA99FF2BA479FF227050F805110C3D00000005000000000000000A1A57
          3DD02EA57CFF33CA99FF2EC896FF4CD2A8FF20835CFF00673BFF45BE96FF31CB
          99FF31CB98FF34CC9CFF31AD83FF1B5C41D300010113000000020B23185E2E8A
          66FF3BCD9EFF30CA97FF4BD3A9FF349571FF87AF9DFFB1CFC1FF238A60FF45D3
          A8FF36CF9FFF33CD9BFF3ED0A3FF319470FF0F32237F00000007184D37B63DB3
          8CFF39CD9FFF4BD5A9FF43A382FF699782FFF8F1EEFFF9F3EEFF357F5DFF56C4
          A1FF43D5A8FF3ED3A4FF3CD1A4FF41BC95FF1B5C43CD0000000B1C6446DF4BCA
          A4FF44D2A8FF4FB392FF4E826AFFF0E9E6FFC0C3B5FFEFE3DDFFCEDDD4FF1B75
          4FFF60DCB8FF48D8ACFF47D6AAFF51D4ACFF247A58F80000000E217050F266D9
          B8FF46D3A8FF0B6741FFD2D2CBFF6A8F77FF116B43FF73967EFFF1E8E3FF72A2
          8BFF46A685FF5EDFBAFF4CD9AFFF6BE2C2FF278460FF020604191E684ADC78D9
          BEFF52DAB1FF3DBA92FF096941FF2F9C76FF57DEB8FF2D9973FF73967EFFF0EA
          E7FF4F886CFF5ABB9AFF5BDEB9FF7FE2C7FF27835FF80000000C19523BAB77C8
          B0FF62E0BCFF56DDB7FF59DFBAFF5CE1BDFF5EE2BEFF5FE4C1FF288C67FF698E
          76FFE6E1DCFF176B47FF5FD8B4FF83D5BDFF1E674CC60000000909201747439C
          7BFF95ECD6FF5ADFBAFF5EE2BDFF61E4BFFF64E6C1FF67E6C5FF67E8C7FF39A1
          7EFF1F6D4AFF288B64FF98EFD9FF4DAC8CFF1036286D00000004000000041C5F
          46B578C6ADFF9AEED9FF65E5C0FF64E7C3FF69E7C6FF6BE8C8FF6CE9C9FF6BEA
          C9FF5ED6B6FF97EDD7FF86D3BBFF237759D20102010C0000000100000001030A
          0718247B5BDA70C1A8FFB5F2E3FF98F0DAFF85EDD4FF75EBCEFF88EFD6FF9CF2
          DDFFBAF4E7FF78CDB3FF2A906DEA0615102E0000000200000000000000000000
          0001030A07171E694FB844AB87FF85D2BBFFA8E6D6FFC5F4EBFFABE9D8FF89D8
          C1FF4BB692FF237F60CB05130E27000000030000000000000000000000000000
          000000000001000000030A241B411B60489D258464CF2C9D77EE258867CF1F71
          56B00E3226560000000600000002000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000020000000C05031A46110852AB190C76E31D0E89FF1C0E89FF190C
          76E4120852AD06031B4D0000000E000000030000000000000000000000000000
          000301010519130A55A9211593FF2225AEFF2430C2FF2535CBFF2535CCFF2430
          C3FF2225AFFF211594FF140B58B20101051E0000000400000000000000020101
          03151C1270CD2522A6FF2D3DCCFF394BD3FF3445D1FF2939CDFF2839CDFF3344
          D0FF394AD4FF2D3CCDFF2523A8FF1C1270D20101051D00000003000000091912
          5BA72A27AAFF2F41D0FF3541C7FF2726ABFF3137BCFF384AD3FF384BD3FF3137
          BCFF2726ABFF3540C7FF2E40D0FF2927ACFF1A115EB10000000D08061C3D3129
          A2FD2C3CCCFF3842C6FF5F5DBDFFEDEDF8FF8B89CEFF3337B9FF3437B9FF8B89
          CEFFEDEDF8FF5F5DBDFF3741C6FF2B3ACDFF3028A4FF0907204A1E185F9F373B
          BCFF3042D0FF2621A5FFECE7ECFFF5EBE4FFF8F2EEFF9491D1FF9491D1FFF8F1
          EDFFF3E9E2FFECE6EBFF2621A5FF2E3FCFFF343ABEFF201A66B0312A92E03542
          CBFF3446D1FF2C2FB5FF8070ADFFEBDBD3FFF4EAE4FFF7F2EDFFF8F1EDFFF4E9
          E2FFEADAD1FF7F6FACFF2B2EB5FF3144D0FF3040CBFF312A95E53E37AEFA3648
          D0FF374AD3FF3A4ED5FF3234B4FF8A7FB9FFF6ECE7FFF5ECE6FFF4EBE5FFF6EB
          E5FF897DB8FF3233B4FF384BD3FF3547D2FF3446D1FF3E37AEFA453FB4FA4557
          D7FF3B50D5FF4C5FDAFF4343B7FF9189C7FFF7EFE9FFF6EEE9FFF6EFE8FFF7ED
          E8FF9087C5FF4242B7FF495DD8FF394CD4FF3F52D4FF443FB3FA403DA1DC5967
          DAFF5B6EDDFF4F4DBAFF8F89CAFFFBF6F4FFF7F1ECFFEDE1D9FFEDE0D9FFF7F0
          EAFFFAF5F2FF8F89CAFF4E4DB9FF576ADCFF5765D9FF403EA4E12E2D70987C85
          DDFF8798E8FF291D9BFFE5DADEFFF6EEEBFFEDDFDAFF816EA9FF816EA9FFEDDF
          D8FFF4ECE7FFE5D9DCFF291D9BFF8494E7FF7A81DDFF33317BAC111125356768
          D0FC9EACEDFF686FCEFF5646A1FFCCB6BCFF7A68A8FF4C4AB6FF4D4BB7FF7A68
          A8FFCBB5BCFF5646A1FF666DCCFF9BAAEEFF696CD0FD1212273F000000043B3B
          79977D84DFFFA5B6F1FF6D74D0FF2D219BFF5151B9FF8EA2ECFF8EA1ECFF5252
          BBFF2D219BFF6B72D0FFA2B3F0FF8086E0FF404183A700000008000000010303
          050C4E509DBC8087E2FFAEBDF3FFA3B6F1FF9DAFF0FF95A9EEFF95A8EEFF9BAD
          EFFFA2B3F0FFACBCF3FF838AE3FF4F52A0C10303051100000002000000000000
          000100000005323464797378D9F8929CEAFFA1AEEFFFB0BFF3FFB0BFF4FFA2AE
          EFFF939DE9FF7479DAF83234647D000000080000000200000000000000000000
          000000000000000000031213232D40437D935D61B5D07378DFFC7378DFFC5D61
          B5D040437D951212223000000004000000010000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000101010462121218D2F2F2FC7383838F0383838F02F2F
          2FC72121218D1010104600000000000000000000000000000000000000000000
          0000080808202121218C393939F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF393939F32121218C080808200000000000000000000000000808
          0820262626A33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF262626A30808082000000000000000002121
          218C3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2121218C00000000101010463939
          39F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF393939F3101010462121218D3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2121218D2F2F2FC73C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2F2F2FC7383838F03C3C
          3CFF3C3C3CFF3C3C3CFF00000000000000000000000000000000000000000000
          000000000000000000003C3C3CFF3C3C3CFF3C3C3CFF383838F0383838F03C3C
          3CFF3C3C3CFF3C3C3CFF00000000000000000000000000000000000000000000
          000000000000000000003C3C3CFF3C3C3CFF3C3C3CFF383838F02F2F2FC73C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2F2F2FC72121218D3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2121218D101010463939
          39F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF393939F310101046000000002121
          218C3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2121218C00000000000000000808
          0820262626A33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF262626A30808082000000000000000000000
          0000080808202121218C393939F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF393939F32121218C080808200000000000000000000000000000
          00000000000000000000101010462121218D2F2F2FC7383838F0383838F02F2F
          2FC72121218D1010104600000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000B0B0B382D2D2DE00B0B0B38000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000333333FF1616
          167018181877333333FF1818187716161670333333FF00000000000000000000
          0000333333FF333333FF333333FF333333FF333333FF00000000161616703333
          33FF323232F9333333FD323232F9333333FF1616167000000000000000000000
          0000333333FF333333FF333333FF333333FF333333FF0B0B0B38181818773232
          32F9262626C00606061F262626C0323232F8181818770B0B0B38000000000000
          0000333333FF333333FF0000000000000000000000002D2D2DE0333333FF3333
          33FD0606061F000000000606061F333333FD333333FF2D2D2DE0000000000000
          0000333333FF333333FF00000000262626BF000000000B0B0B38181818773232
          32F9262626C00606061F262626C0323232F9181818770B0B0B38000000000000
          0000333333FF333333FF00000000000000000000000000000000161616703333
          33FF323232F9333333FD323232F9333333FF1616167000000000000000000000
          0000333333FF333333FF00000000262626BF262626BF00000000333333FF1616
          167018181877333333FF1818187716161670333333FF00000000000000000000
          0000333333FF333333FF00000000000000000000000000000000000000000000
          00000B0B0B382D2D2DE00B0B0B38000000000000000000000000000000000000
          0000333333FF333333FF00000000262626BF262626BF262626BF262626BF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000333333FF333333FF00000000000000000000000000000000000000000000
          000000000000333333FF333333FF000000000000000000000000000000000000
          0000333333FF333333FF00000000262626BF262626BF262626BF262626BF2626
          26BF00000000333333FF333333FF000000000000000000000000000000000000
          0000333333FF333333FF00000000000000000000000000000000000000000000
          000000000000333333FF333333FF000000000000000000000000000000000000
          0000333333FF333333FF333333FF333333FF333333FF333333FF333333FF3333
          33FF333333FF333333FF333333FF000000000000000000000000000000000000
          0000333333FF333333FF333333FF333333FF333333FF333333FF333333FF3333
          33FF333333FF333333FF333333FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000101010462121218D2F2F2FC7383838F0383838F02F2F
          2FC72121218D1010104600000000000000000000000000000000000000000000
          0000080808202121218C393939F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF393939F32121218C080808200000000000000000000000000808
          0820262626A33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF262626A30808082000000000000000002121
          218C3C3C3CFF3C3C3CFF3C3C3CFF2D2D2DBE3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF2D2D2DBE3C3C3CFF3C3C3CFF3C3C3CFF2121218C00000000101010463939
          39F33C3C3CFF3C3C3CFF1E1E1E7E000000001E1E1E7E3C3C3CFF3C3C3CFF1E1E
          1E7E000000001E1E1E7E3C3C3CFF3C3C3CFF393939F3101010462121218D3C3C
          3CFF3C3C3CFF2D2D2DBF0000000000000000000000001E1E1E7E1E1E1E7E0000
          000000000000000000002D2D2DBF3C3C3CFF3C3C3CFF2121218D2F2F2FC73C3C
          3CFF3C3C3CFF3C3C3CFF1E1E1E81000000000000000000000000000000000000
          0000000000001E1E1E813C3C3CFF3C3C3CFF3C3C3CFF2F2F2FC7383838F03C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF1E1E1E810000000000000000000000000000
          00001E1E1E813C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF383838F0383838F03C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF1E1E1E7E0000000000000000000000000000
          00001E1E1E7E3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF383838F02F2F2FC73C3C
          3CFF3C3C3CFF3C3C3CFF1E1E1E7E000000000000000000000000000000000000
          0000000000001E1E1E7E3C3C3CFF3C3C3CFF3C3C3CFF2F2F2FC72121218D3C3C
          3CFF3C3C3CFF2D2D2DBF0000000000000000000000001E1E1E811E1E1E810000
          000000000000000000002D2D2DBF3C3C3CFF3C3C3CFF2121218D101010463939
          39F33C3C3CFF3C3C3CFF1E1E1E81000000001E1E1E813C3C3CFF3C3C3CFF1E1E
          1E81000000001E1E1E813C3C3CFF3C3C3CFF393939F310101046000000002121
          218C3C3C3CFF3C3C3CFF3C3C3CFF2D2D2DC13C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF2D2D2DC13C3C3CFF3C3C3CFF3C3C3CFF2121218C00000000000000000808
          0820262626A33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF262626A30808082000000000000000000000
          0000080808202121218C393939F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF393939F32121218C080808200000000000000000000000000000
          00000000000000000000101010462121218D2F2F2FC7383838F0383838F02F2F
          2FC72121218D1010104600000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000101010462121218D2F2F2FC7383838F0383838F02F2F
          2FC72121218D1010104600000000000000000000000000000000000000000000
          0000080808202121218C393939F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF393939F32121218C080808200000000000000000000000000808
          0820262626A33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF262626A30808082000000000000000002121
          218C3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF1E1E1E7E3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2121218C00000000101010463939
          39F33C3C3CFF3C3C3CFF3C3C3CFF1E1E1E7E000000001E1E1E7E3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF393939F3101010462121218D3C3C
          3CFF3C3C3CFF3C3C3CFF1E1E1E7E0000000000000000000000001E1E1E7E3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2121218D2F2F2FC73C3C
          3CFF3C3C3CFF3C3C3CFF00000000000000001E1E1E8100000000000000001E1E
          1E7E3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2F2F2FC7383838F03C3C
          3CFF3C3C3CFF3C3C3CFF000000001E1E1E813C3C3CFF1E1E1E81000000000000
          00001E1E1E7E3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF383838F0383838F03C3C
          3CFF3C3C3CFF3C3C3CFF1E1E1E813C3C3CFF3C3C3CFF3C3C3CFF1E1E1E810000
          0000000000001E1E1E7E3C3C3CFF3C3C3CFF3C3C3CFF383838F02F2F2FC73C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF1E1E
          1E8100000000000000003C3C3CFF3C3C3CFF3C3C3CFF2F2F2FC72121218D3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF1E1E1E81000000003C3C3CFF3C3C3CFF3C3C3CFF2121218D101010463939
          39F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF1E1E1E813C3C3CFF3C3C3CFF393939F310101046000000002121
          218C3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF2121218C00000000000000000808
          0820262626A33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF3C3C3CFF3C3C3CFF262626A30808082000000000000000000000
          0000080808202121218C393939F33C3C3CFF3C3C3CFF3C3C3CFF3C3C3CFF3C3C
          3CFF3C3C3CFF393939F32121218C080808200000000000000000000000000000
          00000000000000000000101010462121218D2F2F2FC7383838F0383838F02F2F
          2FC72121218D1010104600000000000000000000000000000000}
      end>
  end
end
