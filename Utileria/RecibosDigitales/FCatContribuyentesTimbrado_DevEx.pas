unit FCatContribuyentesTimbrado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, System.Actions, ZetaDialogo;

type
  TCatContribuyentesTimbrado_DevEx = class(TBaseGridLectura_DevEx)
    DataSourceRSocial: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
    procedure Refrescar;
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
  end;

var
  CatContribuyentesTimbrado_DevEx: TCatContribuyentesTimbrado_DevEx;

implementation

uses dInterfase,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.dfm}

procedure TCatContribuyentesTimbrado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Refrescar;
     CanLookup := false;
     HelpContext:= H32141_Contribuyente;
end;

procedure TCatContribuyentesTimbrado_DevEx.Refrescar;
begin
     dmInterfase.cdsRSocial.Refrescar;
     DataSource.DataSet :=  dmInterfase.cdsRSocial;
end;

procedure TCatContribuyentesTimbrado_DevEx.Connect;
begin
     inherited;
     with dmInterfase do
     begin
          cdsRSocial.Conectar;
          DataSource.DataSet:= cdsRSocial;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatContribuyentesTimbrado_DevEx.Refresh;
begin
     inherited;
     dmInterfase.cdsRSocial.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatContribuyentesTimbrado_DevEx.Agregar;
begin
     inherited;
     ZInformation('Contribuyentes','No se permite agregar en Contribuyentes.',0);
end;

procedure TCatContribuyentesTimbrado_DevEx.Borrar;
begin
     inherited;
     ZInformation('Contribuyentes','No se permite borrar en Contribuyentes.',0);
end;

procedure TCatContribuyentesTimbrado_DevEx.Modificar;
begin
     inherited;
     ZInformation('Contribuyentes','No se permite modificar en Contribuyentes.',0);
end;

procedure TCatContribuyentesTimbrado_DevEx.FormShow(Sender: TObject);
begin
 ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('RS_NOMBRE'), K_SIN_TIPO , '', skCount);
  ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TCatContribuyentesTimbrado_DevEx.ZetaDBGridDBTableViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
    //dmInterfase.cdsRSocial.Modificar;
end;




end.
