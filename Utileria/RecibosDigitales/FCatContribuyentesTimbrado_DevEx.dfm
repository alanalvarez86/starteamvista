inherited CatContribuyentesTimbrado_DevEx: TCatContribuyentesTimbrado_DevEx
  Left = 226
  Top = 239
  Caption = 'Contribuyentes'
  ClientHeight = 258
  ClientWidth = 520
  ExplicitWidth = 520
  ExplicitHeight = 258
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 520
    ExplicitWidth = 520
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 261
      ExplicitWidth = 261
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 255
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 520
    Height = 239
    ExplicitWidth = 520
    ExplicitHeight = 239
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsCustomize.ColumnGrouping = False
      OptionsView.NoDataToDisplayInfoText = 'No hay Razones Sociales'
      OptionsView.ShowEditButtons = gsebAlways
      OptionsView.GroupByBox = False
      object cxGridDBColumn1: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'RS_CODIGO'
        Visible = False
        Width = 85
      end
      object cxGridDBColumn2: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'RS_NOMBRE'
        MinWidth = 100
        Options.Editing = False
        Width = 100
      end
      object cxGridDBColumn3: TcxGridDBColumn
        Caption = 'R.F.C.'
        DataBinding.FieldName = 'RS_RFC'
        MinWidth = 100
        Options.Editing = False
        Width = 100
      end
      object cxGridDBColumn4: TcxGridDBColumn
        Caption = 'Representante Legal'
        DataBinding.FieldName = 'RS_RLEGAL'
        MinWidth = 150
        Width = 150
      end
      object cxGridDBTableView1Column1: TcxGridDBColumn
        Caption = 'Contribuyente Registrado #'
        DataBinding.FieldName = 'RS_CONTID'
        MinWidth = 160
        Width = 160
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 112
    Top = 184
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 12058664
  end
  inherited ActionList: TActionList
    Left = 34
    Top = 136
  end
  inherited PopupMenu1: TPopupMenu
    Left = 32
    Top = 88
  end
  object DataSourceRSocial: TDataSource
    Left = 324
    Top = 44
  end
end
