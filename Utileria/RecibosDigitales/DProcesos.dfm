object dmProcesos: TdmProcesos
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 479
  Width = 752
  object cdsDataSet: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 16
  end
  object cdsASCII: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 128
    Top = 16
    object cdsASCIIRenglon: TStringField
      DisplayWidth = 4096
      FieldName = 'Renglon'
      Size = 4096
    end
  end
  object cdsDeclaraGlobales: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 88
  end
  object cdsDataSetTimbrados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 48
    Top = 152
  end
end
