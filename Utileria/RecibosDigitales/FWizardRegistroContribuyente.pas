unit FWizardRegistroContribuyente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,
  Timbres,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ExtCtrls, ZetaKeyLookup, ZetaDBTextBox, Buttons,
  IdCoder, IdCoder3to4, IdCoderMIME, IdBaseComponent, ZetaCommonClasses, ZetaCommonTools, ZetaClientTools,
  Mask, DBCtrls, cxMemo, cxRichEdit, DTablas, FTimbramexClasses,
  ZetaEdit, ZetaFilesTools, OleCtrls, SHDocVw, cxScrollBox, TressMorado2013,
  ZetaKeyLookup_DevEx, cxDBEdit, dxGDIPlusClasses, cxImage, ShellAPI,
  ZetaKeyCombo;

type
  TWizardRegistroContribuyente = class(TdxWizardControlForm)
    dxWizardControl1: TdxWizardControl;
    dxWizardControlPageFiel: TdxWizardControlPage;
    dxWizardControlPageRegistrar: TdxWizardControlPage;
    dxWizardControlPageSeleccionRazonSocial: TdxWizardControlPage;
    dxWizardControlPageRazonSocial: TdxWizardControlPage;
    dataSourceContribuyente: TDataSource;
    Label1: TLabel;
    OpenDialog: TOpenDialog;
    Label3: TLabel;
    ZetaDBTextBox2: TZetaDBTextBox;
    Label18: TLabel;
    RS_RFC: TDBEdit;
    RS_LEGAL: TDBEdit;
    Label6: TLabel;
    cxGroupBox3: TcxGroupBox;
    Label13: TLabel;
    Label5: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label7: TLabel;
    RS_CALLE: TDBEdit;
    RS_NUMEXT: TDBEdit;
    RS_NUMINT: TDBEdit;
    RS_CIUDAD: TDBEdit;
    RS_CODPOST: TDBEdit;
    RS_ENTIDAD: TZetaDBKeyLookup_DevEx;
    dxWizardControlPageFirma: TdxWizardControlPage;
    cxCheckBoxAceptar: TcxCheckBox;
    dxWizardControlPageCuenta: TdxWizardControlPage;
    txtPassword: TZetaDBTextBox;
    DataSourceCuentas: TDataSource;
    lblDI_IP: TLabel;
    Label12: TLabel;
    RS_COLONIA: TDBEdit;
    Label14: TLabel;
    cxScrollBox1: TcxScrollBox;
    WebBrowser: TWebBrowser;
    RS_NOMBRE: TDBEdit;
    ActualizarCSD: TcxCheckBox;
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    cxImage1: TcxImage;
    RS_CODIGO: TZetaKeyLookup_DevEx;
    Label10: TLabel;
    Advertencia: TcxLabel;
    personaFirma: TEdit;
    Label8: TLabel;
    Label4: TLabel;
    llavePrivada: TEdit;
    ArchivoCertificado: TEdit;
    lbCert: TLabel;
    lbKey: TLabel;
    ArchivoLlavePrivada: TEdit;
    BuscarLlavePrivada: TcxButton;
    BuscarCertificado: TcxButton;
    CT_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    Label9: TLabel;
    txtCuentaID: TZetaDBTextBox;
    cxButton1: TcxButton;
    cxProgressBarEnvio: TcxProgressBar;
    cxLabel4: TcxLabel;
    cxStatus: TcxLabel;
    cxGroupBoxHTML: TcxGroupBox;
    WebBrowserMensajeHTML: TWebBrowser;
    Label11: TLabel;
    RS_CURP: TDBEdit;
    Label19: TLabel;
    RS_RL_RFC: TDBEdit;
    ListaTimbradoCB_DevEx: TZetaKeyCombo;
    procedure cxButton1Click(Sender: TObject);
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure BuscarLlavePrivadaClick(Sender: TObject);
    procedure BuscarCertificadoClick(Sender: TObject);
    procedure CT_CODIGOValidLookup(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure ActualizarCSDClick(Sender: TObject);
    procedure RS_CODIGOValidLookup(Sender: TObject);
    procedure WebBrowserMensajeHTMLBeforeNavigate2(ASender: TObject; const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
    procedure WebBrowserMensajeHTMLProgressChange(ASender: TObject; Progress,ProgressMax: Integer);
    procedure WebBrowserMensajeHTMLDocumentComplete(ASender: TObject;const pDisp: IDispatch; const URL: OleVariant);
  private
     FCargoDatosContribuyente : boolean;
    { Private declarations }
   // procedure EnviarRecibos;
     FInicioPaginaWEB : Boolean;
     function CargarManifiesto  : boolean;
     function CargarRegimenFiscal : boolean;
     function AgregarContribuyente : boolean;
     function ExisteEnTimbrado : boolean;
     procedure CargaValoresContribuyente;
     procedure ValidarContribuyenteEnTimbrado;
     procedure RestaurarTamanioForma(lRestaurar: Boolean);
     procedure MostrarAdvertenciaTimbradoNomina;
     procedure EsconderWebBrowserTimbrado(lOcultar: Boolean);
     procedure LlenaListaCampos( DataSet: TDataSet );
     function VerificarConexion: boolean;

  public
    { Public declarations }

  end;


var
  WizardRegistroContribuyentec: TWizardRegistroContribuyente;


implementation

uses
FTimbramexHelper,DCliente,MensajesTimbradoWebServices,ZetaDialogoWebTimbrado;

{$R *.dfm}

procedure TWizardRegistroContribuyente.cxButton1Click(Sender: TObject);
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
var
   sEchoResponse : string;
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;
begin
     try
        timbresSoap := nil;
        sServer := GetTimbradoServer;
        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL);
           if ( timbresSoap <> nil ) then
           begin                
				crypt := TWSTimbradoCrypt.Create;
                sEchoResponse := crypt.Desencriptar(  timbresSoap.Echo( crypt.Encriptar(TESTING_MESSAGE) ) );
			  //sEchoResponse :=  					  timbresSoap.Echo(                 TESTING_MESSAGE    );
				FreeAndNil( crypt );
                ZInformation( Self.Caption, 'Hay Conectividad con el Servidor de Timbrado' , 0);
           end;
        end
        else
        begin
           zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
        end;
     except on Error: Exception do
               zError( Self.Caption, K_SIN_CONECTIVIDAD, 0);
     end;


end;

   function TWizardRegistroContribuyente.VerificarConexion: Boolean;
   var
     xVersionServer : Extended;
     sMensajeError, sMensajeValidacion : string ;
   begin
        Result := true;
        sMensajeError := VACIO;
        sMensajeValidacion := VACIO;
        if not VerificaConexionServer(xVersionServer, sMensajeError, sMensajeValidacion) then
        begin
             if strLleno( sMensajeError ) then
             begin
                ZetaDialogo.ZError('Timbrado de N�mina', sMensajeError, 0);
                Result := false;
             end;
        end;
   end;


procedure TWizardRegistroContribuyente.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
const
     K_CONTRIBUYENTE_REGISTRADO = 'El contribuyente %s fue Registrado Exitosamente en el Sistema de Timbrado con el ID #%d' ;
     K_CONTRIBUYENTE_ACTUALIZADO = 'El contribuyente %s con el ID %d fue Actualizado Exitosamente en el Sistema de Timbrado' ;
var
   sMensajeFmt : string;
begin

  if AKind = wcbkCancel then
  begin
         if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
         begin
               if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
               begin
                    ModalResult := mrCancel;
               end;
         end
         else
         begin
              ModalResult := mrCancel;
         end;
  end
  else if AKind = wcbkFinish then
  begin
    if AgregarContribuyente then
    begin
         if ExisteEnTimbrado then
         begin
            sMensajeFmt :=K_CONTRIBUYENTE_ACTUALIZADO;
            ZInformation( Self.Caption, Format( sMensajeFmt, [dmInterfase.cdsRSocial.FieldByName('RS_NOMBRE').AsString, dmInterfase.cdsRSocial.FieldByName('RS_CONTID').AsInteger] ) , 0);
         end
         else
         begin
              sMensajeFmt := K_CONTRIBUYENTE_REGISTRADO;
              MostrarAdvertenciaTimbradoNomina;
         end;
         ModalResult := mrOk;
    end;
  end;

end;

procedure TWizardRegistroContribuyente.MostrarAdvertenciaTimbradoNomina;
var
   oMensajesTimbrado: TMensajesTimbradoWebServices;
   sErrores: String;
   oParametros: TParametrosPeticion;
begin
     try
        try
           oParametros.CT_CODIGO := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
           oParametros.RS_CONTID := dmInterfase.cdsRSocial.FieldByName('RS_CONTID').AsInteger;
           oMensajesTimbrado := TMensajesTimbradoWebServices.Create(oParametros);
           oMensajesTimbrado.FormarPeticionCartaTimbrado;
           if oMensajesTimbrado.SolicitarPeticion(sErrores) then
           begin
                if oMensajesTimbrado.FormarHTMLMensajeDialogo(50007) then
                begin
                     ZDialogWebTimbrado( oMensajesTimbrado.TituloDialogo, oMensajesTimbrado.LinkTemp, oMensajesTimbrado.TipoDialogoUI, oMensajesTimbrado.BotonesDialogoUI, oMensajesTimbrado.InfoBotonOKDialogo, oMensajesTimbrado.InfoBotonCLOSEDialogo , 0);
                end;
           end
           else
           begin
                zError( Self.Caption,sErrores, 0);
           end;
        except on Error: Exception do
               begin
                    zError( Self.Caption, Error.Message, 0);
               end;
        end;
     finally
            FreeAndNil(oMensajesTimbrado);
     end;
end;

procedure TWizardRegistroContribuyente.CargaValoresContribuyente;
var
   sRegimenFiscal, sCT_CODIGO : string;
begin
     FCargoDatosContribuyente := FALSE;
     txtCuentaID.Caption := VACIO;
     txtPassword.Caption := VACIO;

     sRegimenFiscal := dmInterfase.GetRegimenFiscal;

     if StrVacio( sRegimenFiscal ) then
     begin
         FCargoDatosContribuyente := CargarRegimenFiscal;
     end
     else
         ListaTimbradoCB_DevEx.Llave := sRegimenFiscal;
     dmInterfase.ParametrosRazonSocial.Clear;
     if FCargoDatosContribuyente then
        dmInterfase.ParametrosRazonSocial.AddString('REGIMEN_FISCAL_ANTERIOR', ListaTimbradoCB_DevEx.Text)
     else
         dmInterfase.ParametrosRazonSocial.AddString('REGIMEN_FISCAL_ANTERIOR', VACIO);


     sCT_CODIGO := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString ;

     if  StrLLeno(sCT_CODIGO) then
     begin
          dmInterfase.cdsCuentasTimbramex.Locate('CT_CODIGO', sCT_CODIGO, []);
     end;
end;


procedure TWizardRegistroContribuyente.FormCreate(Sender: TObject);
var
    iConteoListas : integer;
begin
     HelpContext:= H32130_ActualizarContribuyente;

     dmInterfase.cdsRSocial.Conectar;
     RS_CODIGO.LookupDataset := dmInterfase.cdsRSocial;
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;

     dmInterfase.cdsListasTimbrado.Conectar;
     iConteoListas := dmInterfase.cdsListasTimbrado.RecordCount;
     if iConteoListas > 0 then
     begin
          LlenaListaCampos( dmInterfase.cdsListasTimbrado );
     end
     else
     begin
           zError( Self.Caption, 'No hay registros en la tabla TimListas de datos', 0);
     end;


     dmTablas.cdsEstado.Conectar;
     RS_ENTIDAD.LookupDataSet := dmTablas.cdsEstado;
     dmInterfase.cdsCuentasTimbramex.Conectar;
     CT_CODIGO.LookupDataset := dmInterfase.cdsCuentasTimbramex;

     dxWizardControl1.ViewStyle := wcvsWizard97;
     dxWizardControl1.OptionsAnimate.TransitionEffect := wcteSlide;
     RestaurarTamanioForma(True);
     dmInterfase.ParametrosRazonSocial.Clear;
end;

procedure TWizardRegistroContribuyente.LlenaListaCampos( DataSet: TDataSet );
var
   Pos : TBookMark;
begin
     with ListaTimbradoCB_DevEx do
     begin
          Lista.BeginUpdate;
          Clear;
     end;
     with DataSet do
     begin
          DisableControls;
          try
             Pos:= GetBookMark;
             First;
             while ( not EOF ) do
             begin
                 with ListaTimbradoCB_DevEx.Lista do
                 begin
                      Add( FieldByName( 'Valor'  ).AsString +'='+ FieldByName( 'Descripcion' ).AsString  )
                 end;
                 Next;
             end;
             if ( Pos <> nil ) then
             begin
                  GotoBookMark( Pos );
                  FreeBookMark( Pos );
             end;
          finally
                EnableControls;
                ListaTimbradoCB_DevEx.Lista.EndUpdate;
          end;
     end;
end;



procedure TWizardRegistroContribuyente.RS_CODIGOValidLookup(Sender: TObject);
begin
      try
         dmInterfase.cdsRSocial.Locate('RS_CODIGO', RS_CODIGO.Llave, []);
         CargaValoresContribuyente;
         ValidarContribuyenteEnTimbrado;
      Except

      end;

end;

procedure TWizardRegistroContribuyente.ValidarContribuyenteEnTimbrado;
begin
     if ExisteEnTimbrado then
     begin
         cxLabel1.Caption := 'Al aplicar el proceso se actualizar� al contribuyente en el Sistema de Timbrado para as� poder realizar las operaciones de Timbrado.';
         dxWizardControlPageFiel.PageVisible := FALSE;
         dxWizardControlPageFirma.PageVisible := FALSE;
         dxWizardControlPageCuenta.PageVisible := FALSE;
         ActualizarCSD.Visible := True;
     end
     else
     begin
         cxLabel1.Caption := 'Al aplicar el proceso se registrar� al contribuyente en el Sistema de Timbrado para as� poder realizar las operaciones de Timbrado.';
         dxWizardControlPageFiel.PageVisible := TRUE;
         dxWizardControlPageFirma.PageVisible := FALSE;
         dxWizardControlPageCuenta.PageVisible := TRUE;
         ActualizarCSD.Visible := False;
     end;
end;

procedure TWizardRegistroContribuyente.WebBrowserMensajeHTMLBeforeNavigate2(ASender: TObject; const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
var
   sURL : String;
begin
     sURL := URL;
     if not FInicioPaginaWEB then
     begin
          Cancel := True;
          ShellExecute(Application.Handle, 'open', PChar(sURL), nil, nil, SW_NORMAL);
     end;
     FInicioPaginaWEB := False;
end;

procedure TWizardRegistroContribuyente.WebBrowserMensajeHTMLDocumentComplete(ASender: TObject; const pDisp: IDispatch; const URL: OleVariant);
var
   CurWebrowser : IWebBrowser;
   TopWebBrowser: IWebBrowser;
   Document     : OLEvariant;
   WindowName   : string;
begin
     CurWebrowser := pDisp as IWebBrowser;
     TopWebBrowser := (ASender as TWebBrowser).DefaultInterface;
     EsconderWebBrowserTimbrado(False);
end;

procedure TWizardRegistroContribuyente.WebBrowserMensajeHTMLProgressChange( ASender: TObject; Progress, ProgressMax: Integer);
begin
     Application.ProcessMessages;
end;

procedure TWizardRegistroContribuyente.BuscarLlavePrivadaClick(Sender: TObject);
begin
     inherited;
     with ArchivoLlavePrivada do
     begin
          OpenDialog.Filter := VACIO;
          Text := AbreDialogo( OpenDialog, Text, 'Llave Privada|*.key' )
     end;
end;

procedure TWizardRegistroContribuyente.BuscarCertificadoClick(Sender: TObject);
begin
     inherited;
     with ArchivoCertificado do
     begin
          OpenDialog.Filter := VACIO;
          Text := AbreDialogo( OpenDialog, Text,  'Certificado|*.cer' )
     end;

end;


procedure TWizardRegistroContribuyente.CT_CODIGOValidLookup(
  Sender: TObject);
begin
     DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;

procedure TWizardRegistroContribuyente.EsconderWebBrowserTimbrado(lOcultar: Boolean);
begin
     cxGroupBoxHTML.Align := alNone;
     if ( lOcultar ) then
     begin
          cxGroupBoxHTML.Left := -cxGroupBoxHTML.Width  - 1;
          cxGroupBoxHTML.Top  := -cxGroupBoxHTML.Height - 1;
     end
     else
     begin
          cxGroupBoxHTML.Left := 42;
          cxGroupBoxHTML.Top  := 330;
     end;
end;

procedure TWizardRegistroContribuyente.dxWizardControl1PageChanging(
  Sender: TObject; ANewPage: TdxWizardControlCustomPage;
  var AAllow: Boolean);

  function CargarMensajesTimbrado: Boolean;
  var
     oMensajesTimbrado: TMensajesTimbradoWebServices;
     sErrores: String;
     oParametros: TParametrosPeticion;
  begin
       try
          try
             Result := True;
             EsconderWebBrowserTimbrado(True);
             RestaurarTamanioForma(True);
             oParametros.CT_CODIGO := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
             oParametros.RS_CONTID := dmInterfase.cdsRSocial.FieldByName('RS_CONTID').AsInteger;
             oMensajesTimbrado := TMensajesTimbradoWebServices.Create(oParametros);
             oMensajesTimbrado.FormarPeticionCartaTimbrado;
             if oMensajesTimbrado.SolicitarPeticion(sErrores) then
             begin
                  if oMensajesTimbrado.FormarHTMLMensaje(50004) then
                  begin
                       RestaurarTamanioForma(False);
                       FInicioPaginaWEB := TRUE;
                       WebBrowserMensajeHTML.Navigate('file:///'+  oMensajesTimbrado.LinkTemp );
                       Result := True;
                  end;
             end
             else
             begin
                  RestaurarTamanioForma(True);
                  zError( Self.Caption,sErrores, 0);
                  Result:= False;
             end;
          except on Error: Exception do
                 begin
                      RestaurarTamanioForma(True);
                      zError( Self.Caption, Error.Message, 0);
                      Result := False;
                 end;
          end;
       finally
              FreeAndNil(oMensajesTimbrado);
       end;
  end;
begin
     RestaurarTamanioForma(True);
     if ( AnewPage = dxWizardControlPageSeleccionRazonSocial ) { and  not ExisteEnTimbrado} then
     begin
          AAllow := TRUE;
     end;

     if ( AnewPage = dxWizardControlPageRazonSocial ) {( AnewPage = dxWizardControlPageFiel ) or ( AnewPage = dxWizardControlPageCuenta )} then
     begin
          AAllow := TRUE;
          if StrVacio( RS_CODIGO.Llave ) then
          begin
               zError( Self.Caption, 'El contribuyente no puede quedar vac�o.', 0  );
               AAllow := FALSE;
          end;

          if ExisteEnTimbrado then
          begin
                AAllow := CargarMensajesTimbrado;
          end;
          if (VerificarConexion <> true) then
          begin
               AAllow := FALSE;
          end;
     end;

     if ( AnewPage = dxWizardControlPageRegistrar ) then
     begin
          if(dmCliente.EsTRESSPruebas) then
          begin
          		DxWizardControl1.buttons.Finish.Enabled := False;
          end;
          AAllow := TRUE;
     end;

     if ( AnewPage = dxWizardControlPageFiel ) then
     begin
          AAllow := TRUE;
          if StrVacio( RS_RFC.Text ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado el R.F.C. en Sistema TRESS', 0  );
               AAllow := FALSE;
          end
          else
          if StrVacio( RS_NOMBRE.Text ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado el Nombre en Sistema TRESS', 0  );
               AAllow := FALSE;
          end
          else
          if StrVacio( RS_LEGAL.Text ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado el Representante Legal en Sistema TRESS', 0  );
               AAllow := FALSE;
          end
          else
          if StrVacio( RS_RL_RFC.Text ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado el R.F.C del Representante Legal en Sistema TRESS', 0  );
               AAllow := FALSE;
          end
          else
          if StrVacio( RS_CALLE.Text ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado la Calle en Sistema TRESS', 0  );
               AAllow := FALSE;
          end
          else
          if StrVacio( RS_NUMEXT.Text ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado el N�mero Exterior en Sistema TRESS', 0  );
               AAllow := FALSE;
          end
          else
          if StrVacio( RS_CIUDAD.Text ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado la Ciudad en Sistema TRESS', 0  );
               AAllow := FALSE;
          end
          else
          if StrVacio( RS_COLONIA.Text ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado la Colonia en Sistema TRESS', 0  );
               AAllow := FALSE;
          end
          else
          if StrVacio( RS_ENTIDAD.Llave ) then
          begin
               zError( Self.Caption, 'La Raz�n Social no tiene capturado el Estado en Sistema TRESS', 0  );
               AAllow := FALSE;
          end;

     end;

     if ( AnewPage = dxWizardControlPageRegistrar ) and  not ExisteEnTimbrado then
     begin
          AAllow := TRUE;
          if StrVacio( CT_CODIGO.Llave ) then
          begin
               zError( Self.Caption, 'No se ha especificado la Cuenta de Timbrado', 0  );
               CT_CODIGO.SetFocus;
               AAllow := FALSE;
          end;
          if (VerificarConexion <> true) then
          begin
               AAllow := FALSE;
          end;
     end;


     if (( AnewPage = dxWizardControlPageCuenta )  and  (not ExisteEnTimbrado)) or ((AnewPage = dxWizardControlPageRegistrar) and (ActualizarCSD.checked))   then
     begin
          AAllow := TRUE;
          if StrVacio( ArchivoLlavePrivada.Text ) then
          begin
               zError( Self.Caption, 'No se ha especificado el Archivo de Llave Privada', 0  );
               ArchivoLlavePrivada.SetFocus;
               AAllow := FALSE;
          end
          else
          if not FileExists(ArchivoLlavePrivada.Text) then
          begin
               zError( Self.Caption, 'El Archivo de Llave Privada no existe', 0  );
               ArchivoLlavePrivada.SetFocus;
               AAllow := FALSE;
          end
          else
          if StrVacio( ArchivoCertificado.Text ) then
          begin
               zError( Self.Caption, 'No se ha especificado el Archivo de Certificado', 0  );
               ArchivoCertificado.SetFocus;
               AAllow := FALSE;
          end
          else
          if not FileExists(ArchivoCertificado.Text) then
          begin
               zError( Self.Caption, 'El Archivo de Certificado no existe', 0  );
               ArchivoLlavePrivada.SetFocus;
               AAllow := FALSE;
          end
          else
          if StrVacio( llavePrivada.Text ) then
          begin
               zError( Self.Caption, 'No se ha especificado la Contrase�a de Llave Privada', 0  );
               llavePrivada.SetFocus;
               AAllow := FALSE;
          end
          else
          if StrVacio( personaFirma.Text ) then
          begin
               zError( Self.Caption, 'No se ha especificado el Nombre de Persona que Firma', 0  );
               personaFirma.SetFocus;
               AAllow := FALSE;
          end;

     end;
end;

function TWizardRegistroContribuyente.CargarManifiesto : boolean;

var
  sTempHTMLPath, sTempHTML : string;

  function GetTempFileDownload( const sPrefijo, sExt : string ): string;
    {$ifndef DOTNET}
    var
       aDir,aArchivo : array[0..1024] of char;
    {$endif}
    begin
         {$ifdef DOTNET}
         Result := '';
         {$else}
         GetTempPath(1024, aDir);
         GetTempFileName( aDir,PChar(sPrefijo), 0,aArchivo );
         Result := PChar(ChangeFileExt( aArchivo, '.'+ sExt ));
         DeleteFile( aArchivo );
         {$endif}
    end;

  function GrabarTexto( const sArchivo,  sTexto : string ) : boolean;
  var
     sl : TStringList;
  begin
       sl := TStringList.Create;

       sl.Text := sTexto;
       sl.SaveToFile( sArchivo );

       FreeAndNil(sl);

  end;


//Metodo que se iria a una Helper Class
  function   GetManifiestoFromServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No fue posible traer el manifiesto del Sistema de Timbrado: ';
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;
	 crypt : TWSTimbradoCrypt; 
  begin

     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     try


          timbresSoap := nil;

          sServer := GetTimbradoServer; 

          if ( sServer <> '' ) then
          begin

             timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
             if ( timbresSoap <> nil ) then
             begin
				  crypt := TWSTimbradoCrypt.Create;
                  sPeticion :=  dmInterfase.GetManifiesto(personaFirma.Text, CT_CODIGO.Llave, 0 ) ;
				  
				  sRespuesta := crypt.Desencriptar(  timbresSoap.ContribuyenteGetManifiesto( crypt.Encriptar( sPeticion ) ) );
                  //sRespuesta :=                    timbresSoap.ContribuyenteGetManifiesto( sPeticion );
                  respuestaTimbramex :=  FTimbramexHelper.GetManifiesto( sRespuesta );
				  FreeAndNil ( crypt ); 
				  
                  if respuestaTimbramex.Resultado.Errores then
                  begin
                     zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX  + char(10)+char(13)+ respuestaTimbramex.Bitacora.Eventos.Text, 0)
                  end
                  else
                  begin
                       sTempHTMLPath := GetTempFileDownload( 'MANIFIESTO', 'html');
                       GrabarTexto( sTempHTMLPath ,  respuestaTimbramex.Resultado.Contenido );
                       WebBrowser.Navigate('file:///'+ sTempHTMLPath );


                       Result := TRUE;
                  end;
             end;
          end
          else
          begin
             zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor ' , 0);
          end;
       except on Error: Exception do
                 zError( Self.Caption, K_SIN_CONECTIVIDAD + Error.Message, 0);
       end;


       Screen.Cursor := oCursor;

  end;

var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     Result := GetManifiestoFromServer;

     Screen.Cursor := oCursor;
end;



function TWizardRegistroContribuyente.AgregarContribuyente : boolean;


//Metodo que se iria a una Helper Class
  function   AgregarContribuyenteFromServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No fue posible registrar contribuyente: ';
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;
     crypt : TWSTimbradoCrypt;
  begin

     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     sPeticion := VACIO;
     sRespuesta := VACIO;

     try


          timbresSoap := nil;

          cxProgressBarEnvio.Properties.Min := 0.00;
          cxProgressBarEnvio.Properties.Max := 100.00;
          cxProgressBarEnvio.Position := 0.00;
          cxProgressBarEnvio.Update;

          if ( GetTimbradoURL <> '' ) then
          begin
             cxStatus.Caption := 'Conectando a Servidor...';
             timbresSoap := Timbres.GetTimbresFiscalesSoap(False, GetTimbradoURL);
             if ( timbresSoap <> nil ) then
             begin
                  cxStatus.Caption := 'Registrando Contribuyente...';
                  cxProgressBarEnvio.Position := 20.00;  cxProgressBarEnvio.Update;
                  sPeticion := dmInterfase.GetContribuyente( ArchivoLlavePrivada.Text, ArchivoCertificado.Text, llavePrivada.Text, personaFirma.Text , ListaTimbradoCB_DevEx.Text, ListaTimbradoCB_DevEx.Llave  );
				  crypt := TWSTimbradoCrypt.Create;                				 
				  sRespuesta := crypt.Desencriptar(  timbresSoap.ContribuyenteAgregar( crypt.Encriptar( sPeticion ) ) );
                  //sRespuesta := 					 timbresSoap.ContribuyenteAgregar( sPeticion );
				  FreeAndNil( crypt ); 
                  cxProgressBarEnvio.Position := 80.00;  cxProgressBarEnvio.Update;
                  respuestaTimbramex := FTimbramexHelper.GetAgregar( sRespuesta );

                  if respuestaTimbramex.Resultado.Errores then
                  begin
                     cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                     zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX  + char(10)+char(13)+ respuestaTimbramex.Bitacora.Eventos.Text, 0);

                     dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + char(10)+char(13)+ respuestaTimbramex.Bitacora.Eventos.Text +  char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta ) ;

                  end
                  else
                  begin
                       with dmInterfase.cdsRSocial do
                       begin
                            dmInterfase.ParametrosRazonSocial.AddString('REGIMEN_FISCAL_NUEVO', ListaTimbradoCB_DevEx.Text);
                            dmInterfase.GrabaContribuyente( FieldByName('RS_CODIGO').AsString, CT_CODIGO.Llave,  respuestaTimbramex.Resultado.TX_IDi );
                       end;


                       Result := TRUE;
                  end;
             end;
          end
          else
          begin
             zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
              dmInterfase.EscribeBitacoraTRESS(Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado ' );
          end;
       except on Error: Exception do
              begin
                 zError( Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX , 0);
                 dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + ' '+Error.Message  +   char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta  );
              end;
       end;


       Screen.Cursor := oCursor;

  end;


  function   ActualizaContribuyenteFromServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No es posible actualizar contribuyente: ';
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;
	 crypt : TWSTimbradoCrypt;
     {$ifdef debug}
     SaveString : TStringList;
     {$endif}
  begin

     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     sPeticion := VACIO;
     sRespuesta := VACIO;

     try


          timbresSoap := nil;

          cxProgressBarEnvio.Properties.Min := 0.00;
          cxProgressBarEnvio.Properties.Max := 100.00;
          cxProgressBarEnvio.Position := 0.00;
          cxProgressBarEnvio.Update;

          if ( GetTimbradoURL <> '' ) then
          begin
             cxStatus.Caption := 'Conectando a Servidor...';
             timbresSoap := Timbres.GetTimbresFiscalesSoap(False, GetTimbradoURL);
             if ( timbresSoap <> nil ) then
             begin
                  if( ActualizarCSD.checked) then
                  begin
                       cxStatus.Caption := 'Actualizando Contribuyente y CSD...';
                  end
                  else
                  begin
                       cxStatus.Caption := 'Actualizando Contribuyente...';
                  end;
                  cxProgressBarEnvio.Position := 20.00;  cxProgressBarEnvio.Update;
                  if( not ActualizarCSD.Checked) then
                  begin
                       sPeticion := dmInterfase.GetContribuyenteActualiza( ListaTimbradoCB_DevEx.Text, ListaTimbradoCB_DevEx.Llave  );
                  end
                  else
                  begin
                       sPeticion := dmInterfase.GetContribuyente( ArchivoLlavePrivada.Text, ArchivoCertificado.Text, llavePrivada.Text, personaFirma.Text , ListaTimbradoCB_DevEx.Text, ListaTimbradoCB_DevEx.Llave  );
                  end;
          {$ifdef debug}
           SaveString := Tstringlist.Create();
           saveString.Text := sPeticion;
           SaveString.SaveToFile('c:\\XMLUpdateFIle.Txt');
          {$endif}
                  
				crypt := TWSTimbradoCrypt.Create;                				 
				sRespuesta := crypt.Desencriptar(  timbresSoap.ContribuyenteActualizar( crypt.Encriptar( sPeticion ) ) );
				//sRespuesta := timbresSoap.ContribuyenteActualizar( sPeticion );
				FreeAndNil( crypt ); 
          {$ifdef debug}
           saveString.Text := sRespuesta;
           SaveString.SaveToFile('c:\\XMLRespuestaUpdate.Txt');
          {$endif}
                  cxProgressBarEnvio.Position := 80.00;  cxProgressBarEnvio.Update;
                  respuestaTimbramex := FTimbramexHelper.GetAgregar( sRespuesta );

                  if respuestaTimbramex.Resultado.Errores then
                  begin
                     cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                     zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX  + char(10)+char(13)+ respuestaTimbramex.Bitacora.Eventos.Text, 0);

                     dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + char(10)+char(13)+ respuestaTimbramex.Bitacora.Eventos.Text +  char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta ) ;

                  end
                  else
                  begin
                       dmInterfase.ParametrosRazonSocial.AddString('REGIMEN_FISCAL_NUEVO', ListaTimbradoCB_DevEx.Text);
                       dmInterfase.ParametrosRazonSocial.AddBoolean('ACTUALIZAR_CSD', ActualizarCSD.Checked);
                       dmInterfase.ParametrosRazonSocial.AddString('PERSONA_FIRMA', personaFirma.Text);
                       dmInterfase.ParametrosRazonSocial.AddString('LLAVE_PRIVADA', ArchivoLlavePrivada.Text);
                       dmInterfase.ParametrosRazonSocial.AddString('CSD', ArchivoCertificado.Text);
                       dmInterfase.EscribeBitacoraTRESSRazonSocial;
                       Result := TRUE;
                  end;
             end;
          end
          else
          begin
             zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
              dmInterfase.EscribeBitacoraTRESS(Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado ' );
          end;
       except on Error: Exception do
              begin
                 zError( Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX , 0);
                 dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + ' '+Error.Message  +   char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta  );
              end;
       end;


       Screen.Cursor := oCursor;

  end;


var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     if  not  ExisteEnTimbrado then
          Result := AgregarContribuyenteFromServer
     else
         Result := ActualizaContribuyenteFromServer;


     Screen.Cursor := oCursor;
end;



function TWizardRegistroContribuyente.CargarRegimenFiscal : boolean;


//Metodo que se iria a una Helper Class
  function   GetRegimenFiscalFromServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No fue posible traer el Regimen Fiscal del Sistema de Timbrado: ';
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;
	 crypt : TWSTimbradoCrypt; 
  begin
     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
          timbresSoap := nil;
          sServer := GetTimbradoServer;
          if ( sServer <> '' ) then
          begin
             timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
             if ( timbresSoap <> nil ) then
             begin
                  sPeticion :=  dmInterfase.GetPeticionRegimenFiscal;
				  crypt := TWSTimbradoCrypt.Create;                				 
				  sRespuesta := crypt.Desencriptar(  timbresSoap.ContribuyenteConsultar( crypt.Encriptar( sPeticion ) ) );
                  //sRespuesta := timbresSoap.ContribuyenteConsultar( sPeticion );
				  FreeAndNil( crypt ); 
				  
                  respuestaTimbramex :=  FTimbramexHelper.GetRegimenFiscal( sRespuesta );
                  if respuestaTimbramex.Resultado.Errores then
                  begin
                     zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX  + char(10)+char(13)+ respuestaTimbramex.Bitacora.Eventos.Text, 0)
                  end
                  else
                  begin
                       ListaTimbradoCB_DevEx.Llave := respuestaTimbramex.Resultado.EmpresaRegimen;
                       Result := TRUE;
                  end;
             end;
          end
          else
          begin
             zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor ' , 0);
          end;
       except on Error: Exception do
                 zError( Self.Caption, K_SIN_CONECTIVIDAD + Error.Message, 0);
       end;
       Screen.Cursor := oCursor;
   end;
var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     Result := GetRegimenFiscalFromServer;

     Screen.Cursor := oCursor;
end;


function TWizardRegistroContribuyente.ExisteEnTimbrado: boolean;
begin
   Result := (dmInterfase.cdsRSocial.FieldByName('RS_CONTID').AsInteger > 0 ) and FCargoDatosContribuyente;
end;

procedure TWizardRegistroContribuyente.ActualizarCSDClick(Sender: TObject);
begin
if( ActualizarCSD.Checked )then
     begin
          dxWizardControlPageFiel.PageVisible := TRUE;
     end
     else
     begin
          dxWizardControlPageFiel.PageVisible := False;
     end;
end;

procedure TWizardRegistroContribuyente.RestaurarTamanioForma(lRestaurar: Boolean);
begin
     if lRestaurar then
     begin
          Self.ClientHeight := 472;
          cxGroupBoxHTML.Visible := False;
     end
     else
     begin
          Self.ClientHeight := 660;
          cxGroupBoxHTML.Visible := True;
     end;
end;
end.
