object dmInterfase: TdmInterfase
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 655
  Width = 842
  object cdsIncapacidades: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 176
    Top = 8
  end
  object cdsResultados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 56
    Top = 8
  end
  object cdsPeriodosAfectados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsPeriodosAfectadosCalcFields
    AlAdquirirDatos = cdsPeriodosAfectadosAlAdquirirDatos
    AlCrearCampos = cdsPeriodosAfectadosAlCrearCampos
    Left = 62
    Top = 80
  end
  object cdsPeriodosTimbrar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsPeriodosTimbrarAlAdquirirDatos
    AlCrearCampos = cdsPeriodosAfectadosAlCrearCampos
    Left = 184
    Top = 80
  end
  object cdsRSocial: TZetaLookupDataSet
    Tag = 49
    Aggregates = <>
    Params = <>
    BeforePost = cdsRSocialBeforePost
    AfterPost = cdsRSocialAfterPost
    AlAdquirirDatos = cdsRSocialAlAdquirirDatos
    AlEnviarDatos = cdsRSocialAlEnviarDatos
    AlModificar = cdsRSocialAlModificar
    LookupName = 'Cat'#225'logo de razones sociales'
    LookupDescriptionField = 'RS_NOMBRE'
    LookupKeyField = 'RS_CODIGO'
    OnGetRights = cdsRSocialGetRights
    Left = 592
    Top = 408
  end
  object cdsCuentasTimbramex: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsCuentasTimbramexBeforePost
    BeforeDelete = cdsCuentasTimbramexBeforeDelete
    AfterDelete = cdsCuentasTimbramexAfterDelete
    AlAdquirirDatos = cdsCuentasTimbramexAlAdquirirDatos
    AlEnviarDatos = cdsCuentasTimbramexAlEnviarDatos
    AlBorrar = cdsCuentasTimbramexAlBorrar
    AlModificar = cdsCuentasTimbramexAlModificar
    LookupName = 'Cuentas de Timbrado'
    LookupDescriptionField = 'CT_DESCRIP'
    LookupKeyField = 'CT_CODIGO'
    OnGetRights = cdsCuentasTimbramexGetRights
    Left = 480
    Top = 408
  end
  object cdsRecibosTimbrar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsRecibosTimbrarAlCrearCampos
    Left = 296
    Top = 8
  end
  object cdsNominaFiltrar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 168
    Top = 224
  end
  object cdsPeriodosAfectadosTotal: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsPeriodosAfectadosCalcFields
    AlAdquirirDatos = cdsPeriodosAfectadosTotalAlAdquirirDatos
    AlCrearCampos = cdsPeriodosAfectadosAlCrearCampos
    Left = 72
    Top = 392
  end
  object cdsPeriodosCancelar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsPeriodosAfectadosAlCrearCampos
    Left = 160
    Top = 288
  end
  object cdsPeriodosRecibos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsPeriodosAfectadosAlCrearCampos
    Left = 416
    Top = 80
  end
  object cdsTiposSAT: TZetaLookupDataSet
    Tag = 73
    Aggregates = <>
    Params = <>
    BeforePost = cdsTiposSATBeforePost
    LookupName = 'Tipo de Percepci'#243'n/Deducci'#243'n'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 416
    Top = 160
  end
  object cdsRecibosDetalleTimbrar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsRecibosTimbrarAlCrearCampos
    Left = 296
    Top = 72
  end
  object cdsSaldoTimbrado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsSaldoTimbradoAlCrearCampos
    Left = 536
    Top = 88
  end
  object cdsSaldoTimbradoDetalle: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsSaldoTimbradoDetalleAlCrearCampos
    Left = 536
    Top = 160
  end
  object cdsRecibosIncapacidades: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsRecibosTimbrarAlCrearCampos
    Left = 312
    Top = 136
  end
  object cdsRecibosTiempoExtra: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsRecibosTimbrarAlCrearCampos
    Left = 304
    Top = 192
  end
  object cdsEmpleadosTimbrar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 298
    Top = 248
  end
  object cdsFoliosFiscalesPendientes: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 418
    Top = 220
  end
  object cdsArchivosDescargados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 304
    Top = 320
    object cdsArchivosDescargadosTIPO: TStringField
      FieldName = 'TIPO'
      Size = 30
    end
    object cdsArchivosDescargadosGRUPO: TStringField
      FieldName = 'GRUPO'
      Size = 30
    end
    object cdsArchivosDescargadosARCHIVO: TStringField
      FieldName = 'ARCHIVO'
      Size = 1024
    end
    object cdsArchivosDescargadosPERIODO: TIntegerField
      FieldName = 'PERIODO_NUMERO'
    end
  end
  object cdsEmpleadoLookUpTimbrado: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Empleados'
    LookupDescriptionField = 'PRETTYNAME'
    LookupKeyField = 'CB_CODIGO'
    OnLookupKey = cdsEmpleadoLookUpTimbradoLookupKey
    OnLookupSearch = cdsEmpleadoLookUpTimbradoLookupSearch
    Left = 418
    Top = 216
  end
  object cdsCambiosConceptos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 552
    Top = 240
  end
  object cdsNominasXYear: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsNominasXYearAlCrearCampos
    Left = 472
    Top = 328
  end
  object cdsNominasXYearAProcesar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsNominasXYearAlCrearCampos
    Left = 608
    Top = 328
  end
  object cdsErroresGeneral: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsNominasXYearAlCrearCampos
    Left = 168
    Top = 344
  end
  object cdsRecibosSubContratos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsRecibosTimbrarAlCrearCampos
    Left = 176
    Top = 144
  end
  object cdsListasTimbrado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsListasTimbradoAlAdquirirDatos
    Left = 248
    Top = 408
  end
  object cdsAscii: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 632
    Top = 24
    object cdsAsciiRenglon: TStringField
      FieldName = 'Renglon'
      Size = 2048
    end
  end
  object cdsImpotConfConcepto: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 536
    Top = 16
  end
  object cdsPeriodosXMesYear: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsPeriodosXMesYearAlAdquirirDatos
    AlCrearCampos = cdsPeriodosXMesYearAlCrearCampos
    Left = 48
    Top = 240
  end
  object cdsPeriodosXMesYearAProcesar: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsPeriodosXMesYearAProcesarAlCrearCampos
    Left = 48
    Top = 320
  end
  object cdsEmpleadosConciliarTimbrado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 68
    Top = 184
  end
  object cdsTimbradoConciliaGetPeriodos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsTimbradoConciliaGetPeriodosAlAdquirirDatos
    AlCrearCampos = cdsTimbradoConciliaGetPeriodosAlCrearCampos
    Left = 80
    Top = 496
  end
  object cdsDetalleConciliacionTimbrado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 272
    Top = 520
  end
  object cdsAuditoriaConceptosTimbradoXML: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 728
    Top = 48
  end
end
