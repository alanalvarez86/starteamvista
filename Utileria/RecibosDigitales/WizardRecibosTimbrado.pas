unit WizardRecibosTimbrado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,ZGridModeTools,
  Timbres,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaDBTextBox, ExtCtrls, ZetaKeyLookup, Mask,
  DBCtrls,FTimbramexClasses, FTimbramexHelper, Grids, DBGrids, Buttons,
  ZetaSmartLists, FBaseReportes_DevEx, ZetaCommonLists, cxHyperLinkEdit,
  cxRadioGroup, URLMon, cxShellBrowserDialog, WinInet, WininetUtils,
  ZetaEdit, ZetaCommonClasses, DBClient,     DCatalogos, ZetaKeyCombo,
  ZetaCXGrid, TressMorado2013,ZetaKeyLookup_DevEx,cxMemo, ZetaFilesTools,
  cxButtonEdit, ActnList, UManejadorPeticiones, UPeticionWS, UThreadStack, UThreadPolling, UInterfaces,
  cxEditRepositoryItems, ZetaNumero, System.Actions, dxGDIPlusClasses, cxImage;



type
  TWizardRecibosTimbradoForm = class(TdxWizardControlForm, ILogBitacora)
    dxWizardControl1: TdxWizardControl;
    dxWizardControlPageTimbrado: TdxWizardControlPage;
    cxGroupBoxAvance: TcxGroupBox;
    dxWizardControlPage2: TdxWizardControlPage;
    dataSourceTimbrar: TDataSource;
    dataSourceContribuyente: TDataSource;
    DataSourceCuentas: TDataSource;
    DataSourceRecibos: TDataSource;
    cxGroupBoxResultados: TcxGroupBox;
    cxLabel3: TcxLabel;
    DataSourceErrores: TDataSource;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    cxGroupBox2: TcxGroupBox;
    cxRecibosImpresos: TcxCheckBox;
    cxRecibosXML: TcxCheckBox;
    gridPeriodos: TZetaCXGrid;
    gridPeriodosDBTableView1: TcxGridDBTableView;
    gridPeriodosDBTableView1Column10: TcxGridDBColumn;
    gridPeriodosDBTableView1Column8: TcxGridDBColumn;
    gridPeriodosDBTableView1Column2: TcxGridDBColumn;
    gridPeriodosDBTableView1Column9: TcxGridDBColumn;
    gridPeriodosDBTableView1Column5: TcxGridDBColumn;
    gridPeriodosDBTableView1Column7: TcxGridDBColumn;
    gridPeriodosDBTableView1Column11: TcxGridDBColumn;
    gridPeriodosDBTableView1Column4: TcxGridDBColumn;
    gridPeriodosLevel1: TcxGridLevel;
    cxGroupBox3: TcxGroupBox;
    PeriodoTipoLbl: TLabel;
    Label6: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label8: TLabel;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    iTipoNomina: TZetaTextBox;
    Label1: TLabel;
    PE_TIMBRO: TZetaDBTextBox;
    DatasetPeriodo: TDataSource;
    CT_CODIGO: TZetaKeyLookup;
    cxShellBrowserDialogRecibos: TcxShellBrowserDialog;
    dxWizardControlPageEmpleados: TdxWizardControlPage;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    cbOrden: TZetaKeyCombo;
    FiltrosGB: TGroupBox;
    sCondicionLBl: TLabel;
    sFiltroLBL: TLabel;
    sFiltro: TcxMemo;
    Seleccionar: TcxButton;
    BAgregaCampo: TcxButton;
    ECondicion: TZetaKeyLookup_DevEx;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    RBTodos: TcxRadioButton;
    RBRango: TcxRadioButton;
    RBLista: TcxRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    BInicial: TcxButton;
    BFinal: TcxButton;
    BLista: TcxButton;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    ZetaCXGrid1DBTableView1NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2: TcxGridDBTableView;
    ZetaCXGrid1DBTableView2NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2DETALLE: TcxGridDBColumn;
    ZetaCXGrid1Level1: TcxGridLevel;
    ZetaCXGrid1DBTableView3: TcxGridDBTableView;
    ZetaCXGrid1DBTableView3NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView3ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView3DESCRIPCION: TcxGridDBColumn;
    Label4: TLabel;
    cbGrupo: TZetaKeyCombo;
    dsArchivosDescargados: TDataSource;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaCXGrid2Level1: TcxGridLevel;
    ZetaCXGrid2: TZetaCXGrid;
    ZetaDBGridDBTableViewColumn1: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumnaGrupo: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn3: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn4: TcxGridDBColumn;
    ActionListGrid: TActionList;
    Grid_AbrirArchivo: TAction;
    Grid_AbrirArchivoExplorer: TAction;
    cxEditRepositoryBotonesGrid: TcxEditRepository;
    ButtonsImpreso: TcxEditRepositoryButtonItem;
    ButtonsXML: TcxEditRepositoryButtonItem;
    Panel4: TPanel;
    cxButtonExportExcelLista: TcxButton;
    Panel1: TPanel;
    ZetaSpeedButton1: TcxButton;
    Panel2: TPanel;
    cxLabel4: TcxLabel;
    Panel3: TPanel;
    cxLabelRecibos: TcxLabel;
    cxTextEditStatus: TcxTextEdit;
    cxProgressBarEnvio: TcxProgressBar;
    cxLabel5: TcxLabel;
    cxLabelAvance: TcxLabel;
    cxLabelCarpeta: TcxLabel;
    cxLabel2: TcxLabel;
    cxFolderRecibosTxt: TcxTextEdit;
    btBuscarFolder: TcxButton;
    btCancelarDescarga: TcxButton;
    Panel5: TPanel;
    Advertencia: TcxLabel;
    cxImage1: TcxImage;
    cxTextEditCarpetaRecibos: TcxTextEdit;
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure CT_CODIGOValidLookup(Sender: TObject);
    procedure Exportar;
    procedure ExportarListaArchivos;
    procedure ZetaSpeedButton1Click(Sender: TObject);
    procedure IniciarProgress;
    procedure btBuscarFolderClick(Sender: TObject);
    procedure btCancelarDescargaClick(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    Procedure AgregaFiltroFolio;
    procedure Grid_AbrirArchivoExecute(Sender: TObject);
    procedure Grid_AbrirArchivoExplorerExecute(Sender: TObject);
    procedure cxButtonExportExcelListaClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewColumn4GetProperties(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure gridPeriodosDBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure DataSourceErroresUpdateData(Sender: TObject);
    procedure DataSourceErroresDataChange(Sender: TObject; Field: TField);
  private
   FContinuaDownload : boolean;
   FEjecuto : boolean;
   FVerificacion: Boolean;

   FTipoRango: eTipoRangoActivo;
   FEmpleadoCodigo: String;
   FEmpleadoFiltro: String;
   FNombreCarpeta  :String; 




   FParameterList: TZetaParams;
   FDescripciones: TZetaParams;
       { Private declarations }
    function RecibosServer : boolean;
    function GetRecibosDataSet : boolean;

    function GetArchivoFromURL( sURL,sDestinoPath, sFileName : string ) : string;
    function BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;

    function GetRazonSocial : string;

    function GetGrupoName( sCodigoGrupo : string; var sGrupoDescripcion : string ) : string;

    function GetNombreArchivoElegido : string; 

  protected
    { Protected declarations }
    property ParameterList: TZetaParams read FParameterList;
    property Descripciones: TZetaParams read FDescripciones;
    property Verificacion: Boolean read FVerificacion write FVerificacion;

    function Verificar: Boolean; virtual;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure CargaListaVerificacion;
    procedure CargaParametros;
    procedure CargaListaNominas; 

    procedure SetVerificacion( const Value: Boolean );
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );
    procedure ApplyMinWidth;
    procedure AjustaGruposResultados;

    procedure SetStatus( sStatus : string );

    procedure SetCarpetaRecibos(sCarpetaRecibos: string);

  public


      FTotalRead , FTotalSize : TPesos;
      FFileNameDescarga : String;

      property TipoRango: eTipoRangoActivo read FTipoRango;
      procedure EscribirBitacoraLog( sMensaje : string );
      procedure EscribirBitacoraError( sMensaje : string );

end;


  function DescargaEnProgreso(FileName : string; ATotalSize,  ATotalRead, AStartTime: DWORD): Boolean;

var
  WizardRecibosTimbradoForm: TWizardRecibosTimbradoForm;


function ShowWizardTimbrado : boolean;

implementation

uses
 ZetaClientTools,
 ZetaTipoEntidad,
 DTablas, Dcliente,
 DProcesos,
 DGlobal,
 ZGlobalTress,
 DBasicoCliente,
 ZConstruyeFormula,
 ZConfirmaVerificacion,
 ZetaCommonTools,
 ZetaBuscaEmpleado_DevEx,
 ZBaseSelectGrid_DevEx,
 ZBasicoSelectGrid_DevEx,
 FEmpleadoTimbradoGridSelect, ZetaClientDataSet,
 PDFMerge,
 ZetaBuscaEmpleado_DevExAvanzada, ZetaBuscaEmpleado_DevExTimbradoAvanzada;


{$R *.dfm}

function ShowWizardTimbrado : boolean;
begin
    Result := FALSE;
end;

procedure TWizardRecibosTimbradoForm.AjustaGruposResultados;
  begin
  
       if cxGroupBoxResultados.Visible then
       begin
//             cxGroupBoxAvance.Height := 321;
//             cxGroupBoxAvance.Align := alTop;
//             cxGroupBoxResultados.Align := alClient;
       end
       else
       begin
//             cxGroupBoxAvance.Align := alClient;
       end;
  end;


procedure TWizardRecibosTimbradoForm.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin

  if AKind = wcbkCancel then
  begin
         if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
         begin
               if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
               begin
                    ModalResult := mrCancel;
               end;
         end
         else
         begin
              ModalResult := mrCancel;
         end;
  end
  else if AKind = wcbkBack then
  begin
       dxWizardControl1.Buttons.Finish.Enabled := True;
       dxWizardControl1.Buttons.Cancel.Caption := 'Cancelar'; 
  end
  else if AKind = wcbkFinish then
  begin
    Ahandled := True;

    if ( not FEjecuto )then
    begin
         dxWizardControl1.Buttons.Finish.Enabled := False; 
    end;

    if (not FEjecuto) and RecibosServer then
    begin
         {ZInformation( Self.Caption, Format( 'El Timbrado de Nomina del contribuyente %s fue Registrado Exitosamente en el Sistema de Timbrado',
                       [dmInterfase.cdsRSocial.FieldByName('RS_NOMBRE').AsString] ) , 0);}
         //ModalResult := mrOk;
    end;

  end;

end;

function TWizardRecibosTimbradoForm.RecibosServer: boolean;
  function GetHeadFilename : string;
  begin
     with dmCliente do
     begin
          with GetDatosPeriodoActivo do
          begin
               Result := Format('Recibos_%s_%d_%s_%d', [dmCliente.Compania, dmCliente.YearDefault, ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) ), Numero] );
          end;
     end;


  end;


  procedure AgregaArchivoListo( sTipo, sGrupo, sNombre : string );
  begin
       with dmInterfase.cdsArchivosDescargados do
       begin
            Append;
            FieldByName('TIPO').AsString := sTipo;
            FieldByName('GRUPO').AsString := sGrupo;
            FieldByName('ARCHIVO').AsString := sNombre;
            Post;
       end;
  end;



//Metodo que se iria a una Helper Class
  function   GetRecibosServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No fue posible Consultar el Timbrado de la Nomina en el Sistema de Timbrado: ';
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;

     slPeticiones, slPeticionesXML, slGruposPDF, slGrupoXML : TStringList;
     sReciboHeadName, sGrupoHeadName, sArchivoListo : string;

     broker : TManejadorPeticiones ;
     oPeticionWS : TPeticionWS;


     iContadorNominasTimbras : integer;
     sRazonSocial : string;

     function GetListaRecibosGrupo( sGrupo : string )  : TStringList;
     var
        iPeticion : integer;
     begin
          Result := TStringList.Create;

          for iPeticion := 0 to slGruposPDF.Count -1 do
          begin
               if ( slGruposPDF[iPeticion] = sGrupo ) then
               begin
                    Result.Add( slPeticiones[iPeticion] ) ;
               end;
          end
     end;

     procedure EsperarPeticion;
     var
        iCount : integer;
     begin
         iCount := 0;
         while oPeticionWS.lProceso do
         begin
           Inc( iCount );
           //iShowBuzz := iCount mod 4;
           //cxProgressBarEnvio.Position := iShowBuzz * 20;  cxProgressBarEnvio.Update;

           if ( iCount mod 4  = 0 ) then
              Application.ProcessMessages
           else
               DelayNPM( 200  );
         end;
         Application.ProcessMessages;
     end;

     
     function MergeGrupoPDF( sGrupo, sNombreArchivoGeneral : string ) : boolean;
     var
        slGrupoPDF : TStringList;
        sGrupoNameArchivo, sArchivo, sGrupoDescripcion : string;
        FPDFMerge : TPDFMerge;

     begin
          if strLleno( sGrupo ) then
          begin
               sGrupoNameArchivo := GetGrupoName(sGrupo, sGrupoDescripcion);
               sArchivo := Format( '%s_%s' , [sGrupoNameArchivo, sNombreArchivoGeneral] );
          end
          else
              sArchivo :=  sNombreArchivoGeneral;

          slGrupoPDF := GetListaRecibosGrupo( sGrupo );

          SetStatus( 'Generando archivo: ' + sArchivo  + '...'  );
          try
              oPeticionWS := TPeticionWS.Create;
              oPeticionWS.tipoPeticion := peticionMergePDF;
              oPeticionWS.sArchivoDestino := sArchivo;
              oPeticionWS.slGrupoPDF := slGrupoPDF;
              oPeticionWS.lProceso := TRUE;
              broker.AgregarPeticionWS( oPeticionWS );

              EsperarPeticion;

              Result := oPeticionWS.lResultadoMerge;

              if Result then
                 AgregaArchivoListo('Impreso', sGrupoDescripcion,  ExtractFilePath( slGrupoPDF[0] ) +  sArchivo);
          finally
               SetStatus( VACIO );
          end;

     end;




     procedure TomarRecibosImpresos;
     var
         iPeticion, iTipoPaquete : integer;
         sGrupoActual, sGrupoDescripcion : string;
         sArchivoTmp, sArchivoFinal : string;

         slArchivo_HTML : TStringList;

         procedure InitArchivoHTML;
         begin
          slArchivo_HTML := TStringList.Create;
          slArchivo_HTML.Add( '<html><body> ');
          slArchivo_HTML.Add( '<style type="text/css">');
          slArchivo_HTML.Add( 'html, body { margin: 0px; padding: 0px; border: 0px; color: #000; background: #fff; } ');
          slArchivo_HTML.Add( 'html, body, p, th, td, li, dd, dt { font: 0.9em Arial, Helvetica, sans-serif; }  ');
          slArchivo_HTML.Add( ' h1, h2, h3, h4, h5, h6 { font-family: Arial, Helvetica, sans-serif; }       ');
          slArchivo_HTML.Add( ' h1 { font-size: 1.5em; } h2 { font-size: 1.0em; } h3 { font-size: 0.9em ; }   ');
          slArchivo_HTML.Add( ' h4 { font-size: 1.0em; } h5 { font-size: 0.9em; } h6 { font-size: 0.8em; }    ');
          slArchivo_HTML.Add( '  a:link { color: #00f; } a:visited { color: #009; } a:hover { color: #06f; } a:active { color: #0cf; }    ');
          slArchivo_HTML.Add( '</style>   ');
          slArchivo_HTML.Add( '<h1>Ligas para descargar Recibos<h1> ');


             with dmCliente do
             begin
                 with GetDatosPeriodoActivo do
                 begin
                       slArchivo_HTML.Add(  Format('<h2>Empresa:%s</h2><h3>A�o:%d N�mina:%s Periodo:%d</h3>', [dmCliente.Compania, dmCliente.YearDefault, ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) ), Numero] ) ) ;
                  end;
             end;


         end;

         procedure AppendArchivo( sURL, sNombre : string);
         begin
              slArchivo_HTML.Add( Format( '<p><a href="%s">%s</a></p>', [sURL, sNombre] ) );
         end;

         procedure SaveArchivoHTML( sNombre : string ) ;
         begin
              slArchivo_HTML.Add( '</body></html> ');
              slArchivo_HTML.SaveToFile( Format('%s\%s_Recibos.html', [ cxFolderRecibosTxt.Text, sNombre] ) );
         end;

     begin

          InitArchivoHTML;
          if (cbGrupo.Llave = 'NOAPLICA') then
             iTipoPaquete := 0
          else
          if (cbGrupo.Llave = 'NOMINA.CB_CODIGO') then
             iTipoPaquete := 1
          else
              iTipoPaquete := 2;



          slPeticiones := dmInterfase.GetPeriodoRecibos(TRUE, slGruposPDF, iTipoPaquete);

          cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
//          cxStatus.Caption := 'Generando Recibos en Sistema de Timbrado...';
          SetStatus( 'Generando recibos en Sistema de Timbrado...' ) ;
          Application.ProcessMessages;

          if ( slPeticiones.Count > 0 ) then
              sGrupoActual := slGruposPDF[0]
          else
              sGrupoActual := VACIO;

          for iPeticion := 0 to slPeticiones.Count -1  do
          begin
              oPeticionWS := TPeticionWS.Create;
              oPeticionWS.tipoPeticion := peticionTomarRecibosPDF;
              oPeticionWs.cnxSoap := timbresSoap;
              oPeticionWS.sPeticion :=   slPeticiones[iPeticion];
              oPeticionWS.sURL := GetTimbradoURL;
              oPeticionWS.lProceso := TRUE;
              broker.AgregarPeticionWS( oPeticionWS );
              EsperarPeticion;
              sRespuesta := oPeticionWS.sRespuesta;

              if ( iPeticion  < slGruposPDF.Count ) then
              begin
                 sGrupoHeadName := GetGrupoName( slGruposPDF[iPeticion], sGrupoDescripcion  );
              end;

              respuestaTimbramex := FTimbramexHelper.GetRecibosRespuesta(sRespuesta );
              Application.ProcessMessages;
              dmInterfase.RespuestaTimbramex := respuestaTimbramex;
              DatasourceErrores.DataSet := respuestaTimbramex.Resultado.ErroresDS;

              with respuestaTimbramex.Resultado do
              begin

                  if StrLleno( ArchivoUrl ) then
                  begin
                      sArchivoFinal := ArchivoNombre1;
                      FNombreCarpeta := sArchivoFinal;
                      sArchivoTmp :=  Format( '%d_%s_%s' , [iPeticion,  slGruposPDF[iPeticion], ArchivoNombre1] );
                      sArchivoListo := GetArchivoFromURL(ArchivoUrl,  cxFolderRecibosTxt.Text,  sArchivoTmp );

                      AppendArchivo( ArchivoUrl, sArchivoTmp );

                      slPeticiones[iPeticion] := sArchivoListo;
                  end;
              end;

              if respuestaTimbramex.Resultado.Errores then
                 Break;

              if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                 Break;

               if ( iPeticion  <  slPeticiones.Count - 1 ) then
               begin
                  if ( slGruposPDF[iPeticion] <> slGruposPDF[iPeticion+1] )  then
                  begin
                       MergeGrupoPDF( slGruposPDF[iPeticion], sArchivoFinal);

                  end;
               end
               else
               begin
                 //En caso de que sea el ultimo paquete y sea huerfano
                  if ( iPeticion  = slPeticiones.Count -1 ) then
                      MergeGrupoPDF(  slGruposPDF[iPeticion], sArchivoFinal );
               end;
               cxProgressBarEnvio.Position := 10.00 + ( (iPeticion+1) / slPeticiones.Count ) * 90.00    ;  cxProgressBarEnvio.Update;
               Application.ProcessMessages;
          end;

          SaveArchivoHTML( FNombreCarpeta );
//          cxStatus.Caption := VACIO;
          SetStatus( VACIO );
      end;

     procedure TomarRecibosXML;
     var
         iPeticion, iTipoPaquete: integer;
         sGrupoDescripcion : string; 
     begin

          if (cbGrupo.Llave = 'NOAPLICA') then
             iTipoPaquete := 0
          else
          if (cbGrupo.Llave = 'NOMINA.CB_CODIGO') then
             iTipoPaquete := 1
          else
              iTipoPaquete := 2;



         cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
//         cxStatus.Caption := 'Generando Recibos XML en Sistema de Timbrado...';
         SetStatus( 'Generando Recibos XML en Sistema de Timbrado...' );
         Application.ProcessMessages;

         //En el Caso de XML debe generarse un archivo por Grupo
         slPeticionesXML := dmInterfase.GetPeriodoRecibos(FALSE, slGrupoXML, iTipoPaquete);

         for iPeticion := 0 to slPeticionesXML.Count -1  do
         begin             
             oPeticionWS := TPeticionWS.Create;
             oPeticionWS.tipoPeticion := peticionTomarRecibosXML;
             oPeticionWs.cnxSoap := timbresSoap;
             oPeticionWS.sPeticion :=   slPeticionesXML[iPeticion];
             oPeticionWS.sURL := GetTimbradoURL;

             oPeticionWS.lProceso := TRUE;
             broker.AgregarPeticionWS( oPeticionWS );

             EsperarPeticion;

             sRespuesta := oPeticionWS.sRespuesta;

             if ( iPeticion  < slGrupoXML.Count ) then
                sGrupoHeadName := GetGrupoName( slGrupoXML[iPeticion], sGrupoDescripcion  );

             respuestaTimbramex := FTimbramexHelper.GetRecibosRespuesta(sRespuesta );

             cxProgressBarEnvio.Position := 10.00 + ( (iPeticion+1) / slPeticionesXML.Count ) * 100.00    ;  cxProgressBarEnvio.Update;
             Application.ProcessMessages;

             dmInterfase.RespuestaTimbramex := respuestaTimbramex;
             DatasourceErrores.DataSet := respuestaTimbramex.Resultado.ErroresDS;
             //cxGroupBoxResultados.Visible := not respuestaTimbramex.Resultado.ErroresDS.IsEmpty;

             with respuestaTimbramex.Resultado do
             begin
                 if StrLleno( ArchivoUrl ) then
                 begin
                     FNombreCarpeta := ArchivoNombre1;
                     SetCarpetaRecibos(FNombreCarpeta);
                     if strLleno(sGrupoHeadName) then
                        sArchivoListo := GetArchivoFromURL(ArchivoUrl,  cxFolderRecibosTxt.Text,  sGrupoHeadName+ '_' + ArchivoNombre1 )
                     else
                        sArchivoListo := GetArchivoFromURL(ArchivoUrl,  cxFolderRecibosTxt.Text,  ArchivoNombre1 );

                     AgregaArchivoListo('XML', sGrupoDescripcion, sArchivoListo);
                 end;
             end;

             if respuestaTimbramex.Resultado.Errores then
                Break;

             if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                Break;

         end;
     end;

  begin

     Result := False;
     oCursor := Screen.Cursor;

     Screen.Cursor := crHourglass;
     broker := TManejadorPeticiones.Create( ILogBitacora( Self ) );



          with dmProcesos.cdsDataSetTimbrados do
          begin
               iContadorNominasTimbras := RecordCount;
               {if ( dmProcesos.cdsDataSetTimbrados = nil ) or ( dmProcesos.cdsDataSetTimbrados.IsEmpty ) then
               begin
                    sRazonSocial := 'No Hay Nominas para ese empleado';
               end
               else
               begin
                    sRazonSocial := FieldByName('PRETTYNAME').AsString;
               end;  }

          end;

     AjustaGruposResultados;
     try

          sServer := '';
          sGrupoHeadName:= '';
          sServer := GetTimbradoServer;
          timbresSoap := nil;
          IniciarProgress;

          with dmInterfase.cdsArchivosDescargados do
          begin
               if ( Active ) then
                   EmptyDataSet
               else
                   CreateTempDataset;
          end;
          ZetaDBGridDBTableView.ApplyBestFit();



          if ( sServer <> '' ) then
          begin
             SetStatus( 'Conectandose a servidor de Sistema de Timbrado...' ) ;
             timbresSoap := Timbres.GetTimbresFiscalesSoap(False, GetTimbradoURL );

             if ( timbresSoap <> nil ) then
             begin
                  Result := TRUE;
                  SetStatus ( 'Formando petici�n...' );
                  cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;

                  if ( cxRecibosImpresos.Checked ) then
                  begin

                       TomarRecibosImpresos;

                       if respuestaTimbramex.Resultado.Errores then
                       begin
                         zError( TIMBRADO_ERR_TIMBRAMEX , respuestaTimbramex.Bitacora.Eventos.Text  , 0);
                         cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                         Result := FALSE;
                       end
                       else
                       if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                       begin
                          cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                          zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX , 0);
                       end
                       else
                       begin
                            SetStatus( VACIO );
                            dxWizardControl1.Buttons.Finish.Enabled := False;
                            dxWizardControl1.Buttons.Cancel.Caption := 'Salir';
                            cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;
                            REsult := TRUE;
                       end;


                  end
                  else
                      Result:=TRUE;


                  if ( cxRecibosXML.Checked ) and Result then
                  begin

                       TomarRecibosXML; 

                       if respuestaTimbramex.Resultado.Errores then
                       begin
                         zError( TIMBRADO_ERR_TIMBRAMEX , respuestaTimbramex.Bitacora.Eventos.Text  , 0);
                         cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                         Result := FALSE;
                       end
                       else
                       if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                       begin
                          cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                          zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX , 0);
                          Result := FALSE;
                       end
                       else
                       begin
                            SetStatus( VACIO );
                            dxWizardControl1.Buttons.Finish.Enabled := False;
                            dxWizardControl1.Buttons.Cancel.Caption := 'Salir';
                            cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;
                            REsult := TRUE;
                       end;

                  end;

                  if  Result  then
                     zInformation( 'Obtener Recibos Timbrados',Format ('El proceso de descarga de recibos para n�mina %s #%d ha terminado exitosamente',[ObtieneElemento( lfTipoPeriodo, ord( dmCliente.PeriodoTipo ) ) , dmCliente.PeriodoNumero]), 0)


             end;
          end
          else
          begin
              SetStatus( VACIO );
              zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
          end;
       except on Error: Exception do
              begin
                 //zError( Self.Caption, K_SIN_CONECTIVIDAD  + CR_LF + 'Detalle: '+ Error.Message , 0);
                 ZWarning( 'Obtener Recibos Timbrados',Format ('El proceso de descarga de recibos para n�mina %s #%d ha terminado con errores' + CR_LF + 'Detalle: %s',[ObtieneElemento( lfTipoPeriodo, ord( dmCliente.PeriodoTipo ) ) , dmCliente.PeriodoNumero, Error.Message]), 0, mbOK);
              end;
       end;


       AjustaGruposResultados;

       Screen.Cursor := oCursor;

  end;

var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     Result := GetRecibosServer;

     Screen.Cursor := oCursor;
end;

Procedure TWizardRecibosTimbradoForm.AgregaFiltroFolio;
begin
     with dmCatalogos.cdsFolios do
     begin
          Conectar;
          first;
          while (not eof) do
          begin
               if( fieldByName('FL_DESCRIP').AsString <> VACIO) then      //cambiar a strVacio()
               begin
                    cbOrden.Lista.Add( Format( 'NOMINA.NO_FOLIO_%d=Folio %d - %s ' , [fieldByName('FL_CODIGO').asInteger,fieldByName('FL_CODIGO').asInteger, fieldByName('FL_DESCRIP').AsString] ));
               end;
               next;
          end;
     end;
end;

procedure TWizardRecibosTimbradoForm.FormCreate(Sender: TObject);

       
    procedure LLenarComboGrupo;
    var
       iNivel : integer;
       sNivelName : string;
       sLlaveDefault : string;
    begin
      sLlaveDefault := VACIO;

      with cbGrupo.Lista do
      begin
          BeginUpdate;
          try
             Clear;
             Add( Format( 'NOAPLICA=%s' , ['No Aplica'] ));
             Add( Format( 'NOMINA.CB_CODIGO=%s' , ['Grupos de empleados (100 p/archivo)'] ));
             for iNivel := 1 to 9 do
             begin
                sNivelName := Global.GetGlobalString( K_GLOBAL_NIVEL1 + iNivel -1 );
                if strLleno( sNivelName ) then
                begin
                     Add( Format( 'NOMINA.CB_NIVEL%d=%s' , [iNivel, sNivelName] ));
                     if StrVacio( sLlaveDefault ) then
                     begin
                        sLlaveDefault := Format( 'NOMINA.CB_NIVEL%d' , [iNivel] );
                     end;
                end;
             end;
             sLlaveDefault := Format( 'NOMINA.CB_NIVEL%d' , [0] );
          finally
                 EndUpdate;
          end;
      end;
      cbGrupo.ItemIndex := 0;
      cbGrupo.Llave := sLlaveDefault;
    end;


    procedure LLenarComboOrden;
    var
       iNivel : integer;
       sNivelName : string;
    begin

      with cbOrden.Lista do
      begin
          BeginUpdate;
          try
             Clear;
             Add( 'NOMINA.CB_CODIGO=N�mero de Empleado' );
             for iNivel := 1 to 9 do
             begin
                sNivelName := Global.GetGlobalString( K_GLOBAL_NIVEL1 + iNivel -1 );
                if strLleno( sNivelName ) then
                begin
                     Add( Format( 'NOMINA.CB_NIVEL%d=%s' , [iNivel, sNivelName] ));
                end;
             end;
          finally
                 EndUpdate;
          end;
      end;
      AgregaFiltroFolio;
      cbOrden.ItemIndex := 0;
      cbOrden.Llave := 'NOMINA.CB_CODIGO';
    end;

    procedure InicializarFiltros;
    begin

     FEmpleadoCodigo := 'NOMINA' + '.CB_CODIGO';
     FParameterList := TZetaParams.Create;
     FDescripciones := TZetaParams.Create;
     ZBasicoSelectGrid_DevEx.ParametrosGrid := FDescripciones;

     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := VACIO; //IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     dmCatalogos.cdsCondiciones.Conectar;
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;  

     LLenarComboOrden;
     LLenarComboGrupo; 

    end;

begin


     HelpContext:= H32133_TomarRecibos;

     btCancelarDescarga.Visible := FALSE;
     //cxLabelCarpeta.Visible := FALSE;
     //cxTextEditCarpetaRecibos.Visible := FALSE;


     SetStatus(VACIO);

     InicializarFiltros;

     with dmInterfase.cdsArchivosDescargados do
     begin
          if Active then
          begin
              EmptyDataSet;
             // Close;
          end
          else
            CreateTempDataset;
     end;

     dsArchivosDescargados.DataSet := dmInterfase.cdsArchivosDescargados;
      ZetaDBGridDBTableView.ApplyBestFit();


     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
              // sStatusNomina.Caption := //;;ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
               FechaInicial.Caption := FormatDateTime( FormatSettings.LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( FormatSettings.LongDateFormat, Fin );
          end;
     end;

     DatasetPeriodo.DataSet := dmInterfase.cdsPeriodosAfectadosTotal;

     if  dmInterfase.cdsPeriodosRecibos.IsEmpty  then
    begin
         dmCliente.RazonSocial := VACIO;
    end
    else
    begin
         dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosRecibos.FieldByName('RS_CODIGO').AsString, []) ;
         dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
    end;

     dataSourceTimbrar.DataSet :=  dmInterfase.cdsPeriodosRecibos;
     dmTablas.cdsEstado.Conectar;

     CT_CODIGO.LookupDataset := dmInterfase.cdsCuentasTimbramex;
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
     //cxGroupBoxResultados.Visible := FALSE;
     AjustaGruposResultados;

end;

procedure TWizardRecibosTimbradoForm.Button1Click(Sender: TObject);
begin
     //memoPreview.Text :=  dmInterfase.GetRecibos;
end;

procedure TWizardRecibosTimbradoForm.dxWizardControl1PageChanging(Sender: TObject;
  ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
  var
  sPruebda : string;
begin

  if ( AnewPage = dxWizardControlPageEmpleados ) then
  begin
          if  dmInterfase.cdsPeriodosRecibos.IsEmpty  then
          begin
               dmCliente.RazonSocial := VACIO;
          end
          else
          begin
               dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosRecibos.FieldByName('RS_CODIGO').AsString, []) ;
               dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
          end;

          CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
          AAllow := TRUE;

          FVerificacion := FALSE;

          if ( not cxRecibosImpresos.Checked )  and ( not cxRecibosXML.Checked )  then
          begin
               zError( Self.Caption,'Seleccione al menos un tipo de Entrega de Recibos',0);
               cxRecibosImpresos.SetFocus;
               AAllow := FALSE;
          end

  end;

  if ( AnewPage = dxWizardControlPageTimbrado ) then
     begin
          if(dmCliente.EsTRESSPruebas) then
          begin
               DxWizardControl1.buttons.Finish.Enabled := False;
          end;
          cxFolderRecibosTxt.Text := GetRecibosPath;
          FEjecuto := FALSE;


          CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
          AAllow := TRUE;
          if StrVacio( CT_CODIGO.Llave ) then
          begin
               zError( Self.Caption, 'No se ha especificado la Cuenta de Timbrado', 0  );
               AAllow := FALSE;
          end
          else
          begin
               if not FVerificacion then
               begin
                    CargaListaNominas;
               end;

               if ( dmProcesos.cdsDataSetTimbrados = nil ) or ( dmProcesos.cdsDataSetTimbrados.IsEmpty ) then
               begin
                   ZWarning(  Self.Caption, 'No hay empleados timbrados en esa n�mina', 0, mbOK);
                   AAllow := FALSE;
               end;
               //sPruebda := dmProcesos.cdsDataSetTimbrados.FieldByName('PRETTYNAME').AsString;
          end;


          ZetaDBGridDBTableViewColumnaGrupo.Caption := cbGrupo.Descripcion;


     end;
end;

procedure TWizardRecibosTimbradoForm.CT_CODIGOValidLookup(Sender: TObject);
begin
      DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;

function TWizardRecibosTimbradoForm.GetRecibosDataSet: boolean;
begin

end;



procedure TWizardRecibosTimbradoForm.gridPeriodosDBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
 		ZGridModeTools.BorrarItemGenericoAll( AValueList );
     		if gridPeriodosDBTableView1.DataController.IsGridMode then
        		ZGridModeTools.FiltroSetValueLista( gridPeriodosDBTableView1, AItemIndex, AValueList );
end;

procedure TWizardRecibosTimbradoForm.Exportar;
 var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Errores de Timbrado',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin

               if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes_DevEx.ExportarGrid( ZetaCXGrid1DBTableView3 ,
                                                ZetaCXGrid1DBTableView3.DataController.DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
               end;

end;

procedure TWizardRecibosTimbradoForm.ZetaSpeedButton1Click(Sender: TObject);
begin
      if ZetaCXGrid1DBTableView3.DataController.DataSetRecordCount > 0 then
      begin
           Exportar;
      end
      else
      begin
           //ZError('Error en '+Self.Caption, 'El Grid de Errores est� Vac�o', 0);
           ZetaDialogo.ZError('Error en '+Self.Caption, '�Se encontr� un Error! '+ CR_LF + 'El grid de respuesta del sistema de timbrado est� vac�o',0)
      end;
end;

procedure TWizardRecibosTimbradoForm.IniciarProgress;
begin

     cxProgressBarEnvio.Properties.Min := 0.00;
          cxProgressBarEnvio.Properties.Max := 100.00;  // puede ser por la cantidad de empleados en nomina
          cxProgressBarEnvio.Position := 0.00;
          cxProgressBarEnvio.Update;

     cxProgressBarEnvio.Visible := TRUE;
     cxLabelAvance.Visible := TRUE;

end;

function TWizardRecibosTimbradoForm.GetArchivoFromURL(
  sURL, sDestinoPath,  sFileName: string): string;
var
  SourceFile, LocalFile, sFileNamePath: string;
  func: TProgressCallback;
  brokerDownload : TManejadorPeticiones;
  oPeticionDownload : TPeticionWS;


    function GetTempFileDownload( const sPrefijo, sExt : string ): string;
    {$ifndef DOTNET}
    var
       aDir,aArchivo : array[0..1024] of char;
    {$endif}
    begin
         {$ifdef DOTNET}
         Result := '';
         {$else}
         GetTempPath(1024, aDir);
         GetTempFileName( aDir,PChar(sPrefijo), 0,aArchivo );
         Result := PChar(ChangeFileExt( aArchivo, '.'+ sExt ));
         DeleteFile( aArchivo );
         {$endif}
    end;


    function UntilNotExistsFileName : string;
    var
       iCount : integer;
       sDirPath, sExtension  : string;
    begin
         iCount := 0;

         /// { Pasarla a una funcion Set , para que NombreCarpeta sea una propiedad
         sExtension := ExtractFileExt( FNombreCarpeta );
         if strLLeno( sExtension ) then
            FNombreCarpeta :=  Copy( FNombreCarpeta, 1, ( Pos( sExtension, FNombreCarpeta ) - 1 ) )
         else
             FNombreCarpeta := FNombreCarpeta;
         ///

         sDirPath := sDestinoPath + '\' + FNombreCarpeta;

         if not DirectoryExists( sDirPath ) then
            CreateDir( sDirPath );
         SetCarpetaRecibos(sDirPath);
         repeat
               sFileNamePath := ExtractFilePath( sDirPath + '\' + sFileName );
               if (icount = 0 ) then
                sFileNamePath := sFileNamePath  +  sFileName
               else
                sFileNamePath := sFileNamePath  + Format('(%d)%s', [iCount, sFileName]);

                Inc(icount);
         until not FileExists( sFileNamePath );

         Result := sFileNamePath;
    end;

    procedure EsperarDescarga;
    var
        iCount : integer;
    begin
        iCount := 0;
        FTotalRead := 0;
        FTotalSize := 0;
        FFileNameDescarga := VACIO;

        while oPeticionDownload.lProceso do
        begin
          if StrLleno( FFileNameDescarga ) then
             SetStatus( Format('Descargando %s (%.2f Mb de %.2f Mb )', [FFileNameDescarga, fTotalRead, fTotalSize ] ) );

          Inc( iCount );
          //iShowBuzz := iCount mod 4;
          //cxProgressBarEnvio.Position := iShowBuzz * 20;  cxProgressBarEnvio.Update;

          if ( iCount mod 4  = 0 ) then
             Application.ProcessMessages
          else
             DelayNPM( 200 );
         end;
        Application.ProcessMessages;
    end;



    function  DescargarArchivo : boolean;
    begin
         Result := FALSE;

         try
              oPeticionDownload := TPeticionWS.Create;
              oPeticionDownload.tipoPeticion := peticionDownloadFile;
              oPeticionDownload.sArchivoDestino := sFileName;
              oPeticionDownload.sArchivoOrigen :=  SourceFile;
              oPeticionDownload.sLocalFile := LocalFile;
              oPeticionDownload.ACallback :=  DescargaEnProgreso;
              oPeticionDownload.lProceso := TRUE;
              brokerDownload.AgregarPeticionWS( oPeticionDownload );
              EsperarDescarga;
              Result := oPeticionDownload.lResultadoDownload;
          finally
               SetStatus( VACIO );
          end;

    end;


begin
  SourceFile := sURL;

  LocalFile := GetTempFileDownload('REC', 'zip');

  Result := VACIO;

  try
     brokerDownload := TManejadorPeticiones.Create( ILogBitacora( Self ) );
     FContinuaDownload := TRUE;
     btCancelarDescarga.Visible := FALSE;
     SetStatus( VACIO );

     func := DescargaEnProgreso;

     sFileNamePath := UntilNotExistsFileName;


     if DescargarArchivo then
     begin
       RenameFile(LocalFile, sFileNamePath);
       Result := sFileNamePath;
     end
     else
        zInformation( 'Exportar...',Format( 'Error al Descargar Archivo del Sistema de Timbrado: %s', [sFileName] ), 0);

     except on Error: Exception do
     begin
        zInformation( 'Exportar...',Format( 'Error al Descargar Archivo del Sistema de Timbrado: %s', [sFileName] ), 0);
     end;

  end;
  btCancelarDescarga.Visible := FALSE;
  SetStatus( VACIO );
  FreeAndNil(brokerDownload); 

end;



procedure TWizardRecibosTimbradoForm.btBuscarFolderClick(Sender: TObject);
begin

     cxShellBrowserDialogRecibos.Path :=  cxFolderRecibosTxt.Text;
     if cxShellBrowserDialogRecibos.Execute then
     begin
         cxFolderRecibosTxt.Text := cxShellBrowserDialogRecibos.Path ;
     end;

     if  DirectoryExists(cxFolderRecibosTxt.Text ) then
         FTimbramexHelper.SetRecibosPath( cxFolderRecibosTxt.Text );


end;


function DescargaEnProgreso(FileName : string; ATotalSize,  ATotalRead, AStartTime: DWORD): Boolean;
begin
   with WizardRecibosTimbradoForm
    do
   begin
     FFileNameDescarga := FileName;
     FTotalRead :=  ATotalRead / (1048576.0);
     FTotalSize :=  ATotalSize / (1048576.0);
     Result := FContinuaDownload;
   end;
end;

procedure TWizardRecibosTimbradoForm.btCancelarDescargaClick(
  Sender: TObject);
begin
     FContinuaDownload := FALSE;
end;

procedure TWizardRecibosTimbradoForm.SeleccionarClick(Sender: TObject);
var
   lCarga: Boolean;
   lOk: Boolean;
begin
     inherited;
     if Verificacion then
     begin
          case ZConfirmaVerificacion.ConfirmVerification of
               mrYes:
               begin
                    lCarga := True;
                    lOk := True;
               end;
               mrNo:
               begin
                    lCarga := False;
                    lOk := True;
               end;
          else
              begin
                   lCarga := False;
                   lOk := False;
              end;
          end;
     end
     else
     begin
          lCarga := True;
          lOk := True;
     end;
     if lOk then
     begin
          if lCarga then
          begin
               CargaListaNominas;
          end;
          SetVerificacion( Verificar );
     end;
end;

procedure TWizardRecibosTimbradoForm.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardRecibosTimbradoForm.BFinalClick(Sender: TObject);
begin
    with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardRecibosTimbradoForm.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TWizardRecibosTimbradoForm.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, Text, SelStart, evBase );
     end;
end;


function TWizardRecibosTimbradoForm.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
           if ZetaBuscaEmpleado_DevExTimbradoAvanzada.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
           begin
                if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                   Result := sLlave + ',' + sKey
                else
                    Result := sKey;
           end;
     end
     else
     begin
          if ZetaBuscaEmpleado_DevExAvanzada.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
           begin
                if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                   Result := sLlave + ',' + sKey
                else
                    Result := sKey;
           end;

     end;

end;

procedure TWizardRecibosTimbradoForm.FormDestroy(Sender: TObject);
begin
     FDescripciones.Free;
     FParameterList.Free;


end;

procedure TWizardRecibosTimbradoForm.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;


procedure TWizardRecibosTimbradoForm.EnabledBotones(
  const eTipo: eTipoRangoActivo);
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;

procedure TWizardRecibosTimbradoForm.CargaParametros;
var
    sRazonSocial : string;
begin
     with ParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          sRazonSocial :=  GetRango;
          AddString( 'Condicion', GetCondicion );
          sRazonSocial :=  GetCondicion;
          AddString( 'Filtro', GetFiltro );
          sRazonSocial := GetFiltro;

          AddString( 'RazonSocial',  GetRazonSocial );
          AddInteger( 'StatusAnterior', ord(estiTimbrado) );
          AddInteger( 'StatusNuevo',  ord(estiTimbrado) ) ;

          AddInteger( 'Year',dmCliente.YearDefault );
          AddInteger( 'Tipo',ord( dmCliente.PeriodoTipo )  );
          AddInteger( 'Numero', dmCliente.PeriodoNumero );

          if ( cbGrupo.Llave = 'NOAPLICA' ) then
              AddString( 'Orden', cbOrden.Llave + ',' + cbOrden.Llave  )
          else
              AddString( 'Orden', cbGrupo.Llave + ',' + cbOrden.Llave  );
     end;

     with Descripciones do
     begin
          AddString( 'Lista', GetRango );
          sRazonSocial :=  GetRango;

          AddString( 'Condicion', GetCondicion );
          sRazonSocial :=  GetCondicion;

          AddString( 'Filtro', GetFiltro );
          sRazonSocial := GetFiltro;

     end;

          sRazonSocial :=  GetRango;
          sRazonSocial :=  GetCondicion;
          sRazonSocial := GetFiltro;
          sRazonSocial := GetRazonSocial;

{     statusAnterior := eStatusTimbrado( ParamList.ParamByName( 'StatusAnterior').Asinteger ) ;
     statusNuevo := eStatusTimbrado( ParamList.ParamByName( 'StatusNuevo').Asinteger ) ;
     sRazonSocial := ParamList.ParamByName( 'RazonSocial').AsString;}


     with dmCliente do
     begin
          CargaActivosIMSS( ParameterList );
          CargaActivosPeriodo( ParameterList );
          CargaActivosSistema( ParameterList );
     end;


end;

function TWizardRecibosTimbradoForm.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
          { Causa problemas porque le quita la '@'; los par�ntesis no son necesarios
          if EsFormula( Result ) then
             Result := Parentesis( Result );
          }
     end
     else
         Result := '';
end;

function TWizardRecibosTimbradoForm.GetFiltro: String;
begin
  Result := Trim( sFiltro.Text );
     { Los Par�ntesis no son necesarios
     if StrLleno( Result ) then
        Result := Parentesis( Result );
     }
end;

function TWizardRecibosTimbradoForm.GetRango: String;
var
   sl : TStringList;
   i : integer;
   sLista : string;
begin

     sl := TStringList.Create;
     sl.CommaText :=  ELista.Text;
     sLista := VACIO;
     for i:=0 to sl.Count -1  do                       
     begin
          if StrLleno( sl[i] ) then
          begin
               if StrVacio( sLista)  then
                  sLista := sl[i]
               else
                  sLista :=  sLista + ','+ sl[i];
          end;
     end;
     sl.Free;

     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FEmpleadoCodigo, Trim( sLista  ) );
     else
         Result := '';
     end;
end;

procedure TWizardRecibosTimbradoForm.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TWizardRecibosTimbradoForm.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TWizardRecibosTimbradoForm.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;


procedure TWizardRecibosTimbradoForm.CargaListaVerificacion;
var
  sValor1 : string;
  sValor2 : string;
  sValor3 : string;
  sValor4 : string;
  sValor5 : string;
  sValor6 : string;
  sValor7 : string;
  sValor8 : string;
  sValor9 : string;
  sValor10 : string;
  sValor11 : string;
  sValor12 : string;
  sValor13 : string;
  sValor14 : string;
  sValor15 : string;
  sValor16 : string;
  sValor17 : string;
  sValor18 : string;
  sValor19 : string;
  sValor20 : string;
  sValor21 : string;
  sValor22 : string;
  sValor23 : string;
  sValor24 : string;
  sValor25 : string;
  sValor26 : string;
  sValor27 : string;
begin
     sValor1 := ParameterList.Items[0].AsString;
     sValor2 := ParameterList.Items[1].AsString;
     sValor3 := ParameterList.Items[2].AsString;
     sValor4 := ParameterList.Items[3].AsString;
     sValor5 := ParameterList.Items[4].AsString;
     sValor6 := ParameterList.Items[5].AsString;
     sValor7 := ParameterList.Items[6].AsString;
     sValor8 := ParameterList.Items[7].AsString;
     sValor9 := ParameterList.Items[8].AsString;
     sValor10 := ParameterList.Items[9].AsString;
     sValor11 := ParameterList.Items[10].AsString;
     sValor12 := ParameterList.Items[11].AsString;
     sValor13 := ParameterList.Items[12].AsString;
     sValor14 := ParameterList.Items[13].AsString;
     sValor15 := ParameterList.Items[14].AsString;
     sValor16 := ParameterList.Items[15].AsString;
     sValor17 := ParameterList.Items[16].AsString;
     sValor18 := ParameterList.Items[17].AsString;
     sValor19 := ParameterList.Items[18].AsString;
     sValor20 := ParameterList.Items[19].AsString;

     dmProcesos.TimbrarNominaGetLista( ParameterList );
//     dmProcesos.cdsDataset.SaveToFile('ListaVerificar.XML', dfXML);
end;

function TWizardRecibosTimbradoForm.Verificar: Boolean;
begin
     if ( cbOrden.ItemIndex = 0 ) then
        FEmpleadoTimbradoGridSelect.FTituloOrden := VACIO
     else
         FEmpleadoTimbradoGridSelect.FTituloOrden := cbOrden.Descripcion;
//FTituloOrden
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataSetTimbrados, TEmpleadoTimbradoGridSelect );
end;

function TWizardRecibosTimbradoForm.GetRazonSocial: string;
begin
    Result := dmCliente.RazonSocial;
end;

procedure TWizardRecibosTimbradoForm.CargaListaNominas;
var
 oCursor : TCursor;
begin
    oCursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
       CargaParametros;
       CargaListaVerificacion;
    finally
           Screen.Cursor := oCursor;
    end;
end;

procedure TWizardRecibosTimbradoForm.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
          ZetaCXGrid1DBTableView3.ApplyBestFit();
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;

if(dmCliente.EsTRESSPruebas) then
     begin
          DxWizardControl1.buttons.Finish.Enabled := False;
     end;



end;

function TWizardRecibosTimbradoForm.GetGrupoName(
  sCodigoGrupo: string; var sGrupoDescripcion : string): string;
var
   iNivel : integer;
begin
     //Aqui debe buscar la descripcion en los niveles de organigrama
     //Esto segun el Grupo elegido en la UIX
     //Por el momento se pone el puro Codigo

     iNivel :=  StrToIntDef( StrRight( cbGrupo.Llave, 1 ), 0);

     case iNivel of
          1 :
          begin
               dmTablas.cdsNivel1.Conectar;
               dmTablas.cdsNivel1.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          2 :
          begin
               dmTablas.cdsNivel2.Conectar;
               dmTablas.cdsNivel2.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          3 :
          begin
               dmTablas.cdsNivel3.Conectar;
               dmTablas.cdsNivel3.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          4 :
          begin
               dmTablas.cdsNivel4.Conectar;
               dmTablas.cdsNivel4.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          5 :
          begin
               dmTablas.cdsNivel5.Conectar;
               dmTablas.cdsNivel5.LookupKey( sCodigoGrupo, VACIO, Result);
          end ;
          6 :
          begin
               dmTablas.cdsNivel6.Conectar;
               dmTablas.cdsNivel6.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          7 :
          begin
               dmTablas.cdsNivel7.Conectar;
               dmTablas.cdsNivel7.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          8 :
          begin
               dmTablas.cdsNivel8.Conectar;
               dmTablas.cdsNivel8.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          9 :
          begin
               dmTablas.cdsNivel9.Conectar;
               dmTablas.cdsNivel9.LookupKey( sCodigoGrupo, VACIO, Result);
          end;

     else
         Result := VACIO;
     end;


     if  strVacio( Result ) then
     begin
          Result := sCodigoGrupo; 
          sGrupoDescripcion := Result;
     end
     else
     begin
         sGrupoDescripcion := sCodigoGrupo + ' - ' + Result;
         Result := sCodigoGrupo + '_' + Result; 
     end;

     Result :=  QuitaComillas (  QuitaAcentos(  Result ) ) ;
     Result := StrTransAll( Result, ' ', '_' );
     Result := StrTransAll( Result, '/', '' );
     Result := StrTransAll( Result, '\', '' );
     Result := StrTransAll( Result, '.', '' );
end;

procedure TWizardRecibosTimbradoForm.ApplyMinWidth;
var
   i: Integer;
begin
    { with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption);
          end;
     end;}

end;


procedure TWizardRecibosTimbradoForm.Grid_AbrirArchivoExecute(
  Sender: TObject);
var
   sArchivo :string;
begin

  sArchivo := GetNombreArchivoElegido;

  if strLleno( sArchivo ) then
    ZetaFilesTools.AbreArchivoPrograma( sArchivo );

end;

procedure TWizardRecibosTimbradoForm.Grid_AbrirArchivoExplorerExecute(
  Sender: TObject);
var
   sArchivo :string;
begin

  sArchivo := GetNombreArchivoElegido;

  if strLleno( sArchivo ) then
    ZetaFilesTools.AbreArchivoEnExplorer( sArchivo );
end;

function TWizardRecibosTimbradoForm.GetNombreArchivoElegido: string;
begin
 Result := VACIO;
 with dmInterfase.cdsArchivosDescargados do
 begin
    if not isEmpty then
     Result :=  FieldByName('ARCHIVO').AsString;
 end;
end;

procedure TWizardRecibosTimbradoForm.EscribirBitacoraError(
  sMensaje: string);
begin
//
end;

procedure TWizardRecibosTimbradoForm.EscribirBitacoraLog(sMensaje: string);
begin
  //
end;

procedure TWizardRecibosTimbradoForm.SetStatus(sStatus: string);
begin

     cxTextEditStatus.Text := sStatus;
end;

procedure TWizardRecibosTimbradoForm.SetCarpetaRecibos(sCarpetaRecibos: string);
begin
     if strLleno( sCarpetaRecibos ) then
     begin
        //cxLabelCarpeta.Visible := True;
        //cxTextEditCarpetaRecibos.Visible := True;
        cxTextEditCarpetaRecibos.Text := sCarpetaRecibos;
     end;
end;

procedure TWizardRecibosTimbradoForm.ExportarListaArchivos;
 var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Recibos de Timbrado',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin

               if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes_DevEx.ExportarGrid( ZetaDBGridDBTableView ,
                                                ZetaDBGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
               end;

end;

procedure TWizardRecibosTimbradoForm.cxButtonExportExcelListaClick(
  Sender: TObject);
begin
    ExportarListaArchivos;
end;

procedure TWizardRecibosTimbradoForm.DataSourceErroresDataChange(
  Sender: TObject; Field: TField);
begin
     ZetaCXGrid1DBTableView3.ApplyBestFit();
end;

procedure TWizardRecibosTimbradoForm.DataSourceErroresUpdateData(
  Sender: TObject);
begin
     ZetaCXGrid1DBTableView3.ApplyBestFit();
end;

procedure TWizardRecibosTimbradoForm.ZetaDBGridDBTableViewColumn4GetProperties(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  Value: Variant;
  Buttons: TcxEditButtons;
  ButtonEnabled : Boolean;
begin

  if VarIsNull(ARecord.Values[0]) then
    AProperties := ButtonsImpreso.Properties
    // or AProperties := ButtonsVisible.Properties depending on what you want/need
  else
  begin
      Value := ARecord.Values[0];
      if (Value = 'Impreso') then
         AProperties := ButtonsImpreso.Properties
      else
        AProperties := ButtonsXML.Properties;
  end;

end;

end.


