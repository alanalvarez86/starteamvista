object BuscaEmpleado_DevExTimbrado: TBuscaEmpleado_DevExTimbrado
  Left = 529
  Top = 275
  ActiveControl = BuscarEdit
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'B'#250'squeda de Empleados'
  ClientHeight = 211
  ClientWidth = 649
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelControles: TPanel
    Left = 0
    Top = 0
    Width = 649
    Height = 57
    Align = alTop
    TabOrder = 0
    DesignSize = (
      649
      57)
    object ApePatLabel: TLabel
      Left = 23
      Top = 20
      Width = 36
      Height = 13
      Caption = '&Buscar:'
      FocusControl = BuscarEdit
    end
    object BuscarEdit: TEdit
      Left = 64
      Top = 16
      Width = 257
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 20
      TabOrder = 0
      OnChange = BuscarEditChange
    end
    object MostrarBajas: TCheckBox
      Left = 380
      Top = 20
      Width = 157
      Height = 17
      Alignment = taLeftJustify
      Anchors = [akRight, akBottom]
      Caption = 'Mostrar Ba&jas y Transferidos:'
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 1
      OnClick = MostrarBajasClick
    end
    object BuscarBtn_DevEx: TcxButton
      Left = 326
      Top = 16
      Width = 21
      Height = 21
      Hint = 'Buscar Un Empleado'
      Default = True
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCAC6
        CBFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFB5AEB5FFFFFFFFFFFDFDFDFF9F97A0FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFBAB4BBFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF988F99FFC0BAC0FFD2CED2FFBEB8BFFFEFEDEFFFFFFFFFFFD3D0
        D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE9E7E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFF4F3F4FFF0EF
        F1FFAFA9B0FF908791FFAFA9B0FFFBFBFBFFE9E7E9FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFA8A1A9FFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFF
        FFFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFFFFFFFFB7B0B7FF8B818CFF8B81
        8CFF8B818CFFC5C0C6FFFFFFFFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFB
        FBFFE9E7E9FF8B818CFF8B818CFF968D97FFEDEBEDFFF8F7F8FF908791FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFF8F7F8FFDCD9DDFFFAF9FAFFFDFD
        FDFFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAEA7AEFFE7E5
        E8FFF0EFF1FFE0DDE0FFAAA3ABFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BuscarBtn_DevExClick
    end
    object AceptarBtn_DevEx: TcxButton
      Left = 549
      Top = 16
      Width = 75
      Height = 26
      Hint = 'Aceptar y Escribir Datos'
      Anchors = [akRight, akBottom]
      Caption = '&Aceptar'
      Enabled = False
      ModalResult = 1
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8
        A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF8CE2B8FFE9F9
        F1FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFF4FCF8FFFFFF
        FFFFADEACCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF53D395FFD6F4E6FFFFFFFFFFFFFF
        FFFFFCFEFDFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFD9F5E7FF53D395FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF79DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF5BD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
        D6FFFFFFFFFFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF50D293FF55D3
        96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF8AE1B7FFE6F9F0FF6BD9A4FF50D293FF50D293FF50D2
        93FF60D69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF53D395FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF66D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF69D8A2FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF6BD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF53D395FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF79DDACFFF7FDFAFFFFFFFFFFB8EDD3FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF71DAA7FFEFFBF5FFFFFFFFFFA0E6
        C4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFE1F7ECFFFFFF
        FFFF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF58D498FFBDEE
        D6FFFCFEFDFF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF81DFB1FF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = AceptarBtn_DevExClick
    end
  end
  object GridEmpleados_DevEx: TZetaCXGrid
    Left = 0
    Top = 57
    Width = 649
    Height = 154
    Align = alClient
    TabOrder = 1
    ExplicitHeight = 155
    object GridEmpleados_DevExDBTableView: TcxGridDBTableView
      OnDblClick = GridEmpleados_DevExDBTableViewDblClick
      Navigator.Buttons.CustomButtons = <>
      FilterBox.CustomizeDialog = False
      FilterBox.Visible = fvNever
      OnCustomDrawCell = GridEmpleados_DevExDBTableViewCustomDrawCell
      DataController.DataSource = DataSource
      DataController.Filter.OnGetValueList = GridEmpleados_DevExDBTableViewDataControllerFilterGetValueList
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        OnCustomDrawCell = CB_CODIGOCustomDrawCell
        MinWidth = 75
        Width = 75
      end
      object CB_APE_PAT: TcxGridDBColumn
        Caption = 'Apellido Paterno'
        DataBinding.FieldName = 'CB_APE_PAT'
        OnCustomDrawCell = CB_APE_PATCustomDrawCell
        MinWidth = 122
        Width = 122
      end
      object CB_APE_MAT: TcxGridDBColumn
        Caption = 'Apellido Materno'
        DataBinding.FieldName = 'CB_APE_MAT'
        OnCustomDrawCell = CB_APE_MATCustomDrawCell
        MinWidth = 114
        Width = 114
      end
      object CB_NOMBRES: TcxGridDBColumn
        Caption = 'Nombre(s)'
        DataBinding.FieldName = 'CB_NOMBRES'
        OnCustomDrawCell = CB_NOMBRESCustomDrawCell
        MinWidth = 86
        Width = 86
      end
      object CB_RFC: TcxGridDBColumn
        Caption = 'R.F.C.'
        DataBinding.FieldName = 'CB_RFC'
        OnCustomDrawCell = CB_RFCCustomDrawCell
        MinWidth = 65
        Width = 65
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = 'N.S.S.'
        DataBinding.FieldName = 'CB_SEGSOC'
        OnCustomDrawCell = CB_SEGSOCCustomDrawCell
        MinWidth = 76
        Width = 76
      end
      object CB_BAN_ELE: TcxGridDBColumn
        Caption = 'Banca Electr'#243'nica'
        DataBinding.FieldName = 'CB_BAN_ELE'
        Visible = False
        MinWidth = 115
        Width = 115
      end
      object CB_ACTIVO: TcxGridDBColumn
        DataBinding.FieldName = 'CB_ACTIVO'
        Visible = False
      end
      object STATUS: TcxGridDBColumn
        DataBinding.FieldName = 'STATUS'
        Visible = False
      end
      object TABLA: TcxGridDBColumn
        DataBinding.FieldName = 'TABLA'
        Visible = False
      end
    end
    object GridEmpleados_DevExLevel: TcxGridLevel
      GridView = GridEmpleados_DevExDBTableView
    end
  end
  object DataSource: TDataSource
    Left = 149
    Top = 143
  end
end
