unit DCatalogos;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, cxCheckListBox, DConsultas,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerLabor,
{$else}
     Catalogos_TLB,
     Labor_TLB,
{$endif}
{$ifndef VER130}
     Variants,
{$endif}
     ZBaseEdicion_DevEx, //DevEx
     FCamposMgr,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaKeyLookup, ZetaKeyLookup_DevEx,
     ZetaCommonClasses,
     ZetaServerTools;

{$define QUINCENALES}
{.$undefine QUINCENALES}

type
  TdmCatalogos = class(TDataModule)
    cdsPuestos: TZetaLookupDataSet;
    cdsTurnos: TZetaLookupDataSet;
    cdsCursos: TZetaLookupDataSet;
    cdsNomParam: TZetaLookupDataSet;
    cdsClasifi: TZetaLookupDataSet;
    cdsConceptos: TZetaLookupDataSet;
    cdsCondiciones: TZetaLookupDataSet;
    cdsContratos: TZetaLookupDataSet;
    cdsEventos: TZetaLookupDataSet;
    cdsCalendario: TZetaClientDataSet;
    cdsFestTurno: TZetaLookupDataSet;
    cdsFolios: TZetaLookupDataSet;
    cdsHorarios: TZetaLookupDataSet;
    cdsInvitadores: TZetaLookupDataSet;
    cdsReglas: TZetaLookupDataSet;
    cdsOtrasPer: TZetaLookupDataSet;
    cdsMaestros: TZetaLookupDataSet;
    cdsMatrizCurso: TZetaClientDataSet;
    cdsOrdFolios: TZetaLookupDataSet;
    cdsEntNivel: TZetaClientDataSet;
    cdsRPatron: TZetaLookupDataSet;
    cdsSSocial: TZetaLookupDataSet;
    cdsPrestaci: TZetaLookupDataSet;
    cdsPRiesgo: TZetaLookupDataSet;
    cdsPeriodo: TZetaLookupDataSet;
    cdsConceptosLookUp: TZetaLookupDataSet;
    cdsNomParamLookUp: TZetaLookupDataSet;
    cdsTools: TZetaLookupDataSet;
    cdsPercepFijas: TZetaClientDataSet;
    cdsAreas: TZetaLookupDataSet;
    cdsPuestosLookup: TZetaLookupDataSet;
    cdsPtoTools: TZetaClientDataSet;
    cdsSesiones: TZetaLookupDataSet;
    cdsHisCursos: TZetaClientDataSet;
    cdsAccReglas: TZetaLookupDataSet;
    cdsAulas: TZetaLookupDataSet;
    cdsPrerequisitosCurso: TZetaClientDataSet;
    cdsCertificaciones: TZetaLookupDataSet;
    cdsTiposPoliza: TZetaLookupDataSet;
    cdsTPeriodos: TZetaLookupDataSet;
    cdsCamposPerfil: TZetaClientDataSet;
    cdsSeccionesPerfil: TZetaLookupDataSet;
    cdsPerfilesPuesto: TZetaLookupDataSet;
    cdsPerfiles: TZetaClientDataSet;
    cdsDescPerfil: TZetaClientDataSet;
    cdsProvCap: TZetaLookupDataSet;
    cdsValPlantilla: TZetaLookupDataSet;
    cdsValFactores: TZetaLookupDataSet;
    cdsSubfactores: TZetaLookupDataSet;
    cdsValNiveles: TZetaLookupDataSet;
    cdsValuaciones: TZetaClientDataSet;
    cdsValPuntos: TZetaClientDataSet;
    cdsPuntosNivel: TZetaLookupDataSet;
    cdsPeriodoOtro: TZetaLookupDataSet;
    cdsTemp: TZetaClientDataSet;
    cdsRSocial: TZetaLookupDataSet;
    cdsMatrizCertificPuesto: TZetaClientDataSet;
    cdsCerNivel: TZetaClientDataSet;
    cdsReglaPrestamo: TZetaLookupDataSet;
    cdsPrestaXRegla: TZetaClientDataSet;
    cdsPeriodoAd: TZetaLookupDataSet;
    cdsHistRev: TZetaLookupDataSet;
    cdsEstablecimientos: TZetaLookupDataSet;
    cdsCosteoGrupos: TZetaLookupDataSet;
    cdsCosteoCriterios: TZetaLookupDataSet;
    cdsCosteoCriteriosPorConcepto: TZetaClientDataSet;
    cdsConceptosDisponibles: TZetaLookupDataSet;
    cdsTiposPension: TZetaLookupDataSet;
    cdsSegGastosMed: TZetaLookupDataSet;
    cdsVigenciasSGM: TZetaLookupDataSet;
    cdsCompetencias: TZetaLookupDataSet;
    cdsCompRevisiones: TZetaClientDataSet;
    cdsCompCursos: TZetaClientDataSet;
    cdsCatPerfiles: TZetaLookupDataSet;
    cdsRevPerfiles: TZetaClientDataSet;
    cdsMatPerfilComps: TZetaClientDataSet;
    cdsCompNiveles: TZetaLookupDataSet;
    cdsGpoCompPuesto: TZetaClientDataSet;
    cdsTablasAmortizacion: TZetaLookupDataSet;
    cdsCostosAmortizacion: TZetaLookupDataSet;
    cdsTiposSAT: TZetaLookupDataSet;
    procedure cdsPuestosAlAdquirirDatos(Sender: TObject);
    procedure cdsTurnosAlAdquirirDatos(Sender: TObject);
    procedure cdsCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsCalendarioAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiAlAdquirirDatos(Sender: TObject);
    procedure cdsConceptosAlAdquirirDatos(Sender: TObject);
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
    procedure cdsContratosAlAdquirirDatos(Sender: TObject);
    procedure cdsEventosAlAdquirirDatos(Sender: TObject);
    procedure cdsFestTurnoAlAdquirirDatos(Sender: TObject);
    procedure cdsFoliosAlAdquirirDatos(Sender: TObject);
    procedure cdsHorariosAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamAlAdquirirDatos(Sender: TObject);
    procedure cdsRPatronAlAdquirirDatos(Sender: TObject);
    procedure cdsInvitadoresAlAdquirirDatos(Sender: TObject);
    procedure cdsReglasAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodoAlAdquirirDatos(Sender: TObject);
    procedure cdsMatrizCursoAlAdquirirDatos(Sender: TObject);
    procedure cdsOtrasPerAlAdquirirDatos(Sender: TObject);
    procedure cdsMaestrosAlAdquirirDatos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisCursosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisCursosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    function SubeBajaOrden(eAccion: eSubeBaja; DataSet: TZetaClientDataset; const sCodigo,sPerfil:string):Integer;
    function  PuedeCrearPerfil(sCodigo:string):Boolean ;
    procedure cdsClasifiAlModificar(Sender: TObject);
    procedure CopiarPerfil(const sFuente,sDestino: string);
    procedure NP_FORMULAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FE_MESGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FE_CAMBIOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FE_YEARGetText(Sender: TField; var Text: String; DisplayText: Boolean);    
    procedure MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure OrderPerfilesGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FE_TIPOChange(Sender: TField);
    procedure TU_RIT_PATChange(Sender: TField);
    procedure VT_CUALChange(Sender: TField);
    procedure cdsConceptosCO_NUMEROValidate(Sender: TField);
    procedure RevisaDiasVacaciones(Sender: TField);
    procedure cdsCalendarioAlModificar(Sender: TObject);
    procedure cdsAfterPost(DataSet: TDataSet);
    procedure cdsConceptosAlModificar(Sender: TObject);
    procedure cdsCalendarioNewRecord(DataSet: TDataSet);
    procedure cdsCondicionesAlModificar(Sender: TObject);
    procedure cdsCondicionesBeforePost(DataSet: TDataSet);
    procedure cdsContratosAlModificar(Sender: TObject);
    procedure cdsInvitadoresAlModificar(Sender: TObject);
    procedure cdsInvitadoresBeforePost(DataSet: TDataSet);
    procedure cdsInvitadoresNewRecord(DataSet: TDataSet);
    procedure cdsMaestrosBeforePost(DataSet: TDataSet);
    procedure cdsMatrizCursoAlModificar(Sender: TObject);
    procedure cdsMatrizCursoAfterCancel(DataSet: TDataSet);
    procedure cdsMatrizCursoAfterOpen(DataSet: TDataSet);
    procedure cdsMatrizCursoAlCrearCampos(Sender: TObject);
    procedure cdsMatrizCursoBeforePost(DataSet: TDataSet);
    procedure cdsMatrizCursoNewRecord(DataSet: TDataSet);
    procedure cdsOtrasPerAlModificar(Sender: TObject);
    procedure cdsOtrasPerBeforePost(DataSet: TDataSet);
    procedure cdsPeriodoAlModificar(Sender: TObject);
    procedure cdsPuestosAlModificar(Sender: TObject);
    procedure cdsPuestosBeforePost(DataSet: TDataSet);
    procedure cdsReglasAlModificar(Sender: TObject);
    procedure cdsReglasBeforePost(DataSet: TDataSet);
    procedure cdsRPatronAlModificar(Sender: TObject);
    procedure cdsRPatronBeforePost(DataSet: TDataSet);
    procedure cdsTurnosAlModificar(Sender: TObject);
    procedure cdsTurnosBeforePost(DataSet: TDataSet);
    procedure cdsPeriodoBeforePost(DataSet: TDataSet);
    procedure cdsReglasNewRecord(DataSet: TDataSet);
    procedure cdsCursosAlModificar(Sender: TObject);
    procedure cdsCursosBeforePost(DataSet: TDataSet);
    procedure cdsCursosNewRecord(DataSet: TDataSet);
    procedure cdsEventosNewRecord(DataSet: TDataSet);
    procedure cdsEventosAlModificar(Sender: TObject);
    procedure cdsEventosBeforePost(DataSet: TDataSet);
    procedure cdsFestTurnoAlModificar(Sender: TObject);
    procedure cdsFestTurnoNewRecord(DataSet: TDataSet);
    procedure cdsFestTurnoBeforePost(DataSet: TDataSet);
    procedure cdsFoliosAlModificar(Sender: TObject);
    procedure cdsFoliosNewRecord(DataSet: TDataSet);
    procedure cdsFoliosBeforePost(DataSet: TDataSet);
    procedure cdsFoliosAfterOpen(DataSet: TDataSet);
    procedure cdsFoliosAfterCancel(DataSet: TDataSet);
    procedure cdsHorariosAlModificar(Sender: TObject);
    procedure cdsHorariosNewRecord(DataSet: TDataSet);
    procedure cdsHorariosBeforePost(DataSet: TDataSet);
    procedure cdsConceptosNewRecord(DataSet: TDataSet);
    procedure cdsCondicionesNewRecord(DataSet: TDataSet);
    procedure cdsContratosNewRecord(DataSet: TDataSet);
    procedure cdsContratosBeforePost(DataSet: TDataSet);
    procedure cdsCondicionesBeforeEdit(DataSet: TDataSet);
    procedure cdsFestTurnoAfterPost(DataSet: TDataSet);
    procedure cdsFestTurnoAfterDelete(DataSet: TDataSet);
    procedure cdsTurnosNewRecord(DataSet: TDataSet);
    procedure cdsOtrasPerNewRecord(DataSet: TDataSet);
    procedure cdsPeriodoBeforeInsert(DataSet: TDataSet);
    procedure cdsPeriodoNewRecord(DataSet: TDataSet);
    procedure cdsEntNivelAfterDelete(DataSet: TDataSet);
    procedure cdsSSocialAfterCancel(DataSet: TDataSet);
    procedure cdsSSocialAfterDelete(DataSet: TDataSet);
    procedure cdsSSocialAfterOpen(DataSet: TDataSet);
    procedure cdsSSocialAlAdquirirDatos(Sender: TObject);
    procedure cdsSSocialAlModificar(Sender: TObject);
    procedure cdsSSocialNewRecord(DataSet: TDataSet);
    procedure cdsPrestaciNewRecord(DataSet: TDataSet);
    procedure cdsPrestaciBeforeInsert(DataSet: TDataSet);
    procedure cdsPrestaciAfterDelete(DataSet: TDataSet);
    procedure cdsRPatronAfterCancel(DataSet: TDataSet);
    procedure cdsRPatronNewRecord(DataSet: TDataSet);
    procedure cdsPRiesgoAfterDelete(DataSet: TDataSet);
    procedure cdsPRiesgoNewRecord(DataSet: TDataSet);
    procedure cdsNomParamAlModificar(Sender: TObject);
    procedure cdsNomParamBeforeInsert(DataSet: TDataSet);
    procedure cdsNomParamNewRecord(DataSet: TDataSet);
    procedure cdsConceptosBeforeInsert(DataSet: TDataSet);
    procedure cdsInvitadoresBeforeInsert(DataSet: TDataSet);
    procedure cdsPeriodoLookupDescription(Sender: TZetaLookupDataSet; var sDescription: String);
    procedure cdsSSocialAlEnviarDatos(Sender: TObject);
    procedure cdsFoliosAlEnviarDatos(Sender: TObject);
    procedure cdsPeriodoAfterEdit(DataSet: TDataSet);
    procedure cdsPeriodoAlCrearCampos(Sender: TObject);
    procedure cdsCalendarioAlCrearCampos(Sender: TObject);
    procedure cdsCalendarioAfterPost(DataSet: TDataSet);
    procedure cdsClasifiAlCrearCampos(Sender: TObject);
    procedure cdsConceptosAlCrearCampos(Sender: TObject);
    procedure cdsInvitadoresAlCrearCampos(Sender: TObject);
    procedure cdsRPatronAlCrearCampos(Sender: TObject);
    procedure cdsOtrasPerAlCrearCampos(Sender: TObject);
    procedure cdsFestTurnoAlCrearCampos(Sender: TObject);
    procedure cdsHorariosAlCrearCampos(Sender: TObject);
    procedure cdsCursosAlCrearCampos(Sender: TObject);
    procedure cdsNomParamAlCrearCampos(Sender: TObject);
    procedure cdsSSocialAlCrearCampos(Sender: TObject);
    procedure cdsPrestaciAlCrearCampos(Sender: TObject);
    procedure cdsConceptosLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsConceptosAfterPost(DataSet: TDataSet);
    procedure cdsPeriodoLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsPeriodoLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamAfterPost(DataSet: TDataSet);
    procedure cdsFoliosAfterDelete(DataSet: TDataSet);
    procedure cdsEntNivelNewRecord(DataSet: TDataSet);
    procedure cdsPuestosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsTurnosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsCursosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsClasifiGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsConceptosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsCondicionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEventosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsFestTurnoGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsFoliosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsHorariosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsReglasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsMaestrosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsNomParamGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsContratosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsInvitadoresGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsPeriodoGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsSSocialGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsRPatronGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsOtrasPerGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsTiposPolizaGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsCertificacionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsPeriodoBeforeDelete(DataSet: TDataSet);
    procedure cdsPuestosAlEnviarDatos(Sender: TObject);
    procedure cdsCursosAlEnviarDatos(Sender: TObject);
    procedure cdsTurnosAlEnviarDatos(Sender: TObject);
    procedure cdsInvitadoresAlEnviarDatos(Sender: TObject);
    procedure cdsPeriodoAlBorrar(Sender: TObject);
    procedure cdsPeriodoAlEnviarDatos(Sender: TObject);
    procedure cdsNomParamAfterCancel(DataSet: TDataSet);
    procedure cdsNomParamBeforeEdit(DataSet: TDataSet);
    procedure cdsNomParamBeforePost(DataSet: TDataSet);
    procedure cdsToolsGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsToolsAlAdquirirDatos(Sender: TObject);
    procedure cdsToolsAlCrearCampos(Sender: TObject);
    procedure cdsToolsAlModificar(Sender: TObject);
    procedure cdsToolsBeforePost(DataSet: TDataSet);
    procedure cdsMatrizCursoAlEnviarDatos(Sender: TObject);
    procedure cdsMatrizCursoAfterDelete(DataSet: TDataSet);
    procedure cdsPRiesgoAfterOpen(DataSet: TDataSet);
    procedure cdsTurnosAfterOpen(DataSet: TDataSet);
    procedure cdsPrestaciBeforePost(DataSet: TDataSet);
    procedure cdsCatalogosOnPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure cdsCursosAfterDelete(DataSet: TDataSet);
    procedure cdsFoliosBeforeInsert(DataSet: TDataSet);
    procedure cdsPercepFijasAlEnviarDatos(Sender: TObject);
    procedure cdsPercepFijasAlAdquirirDatos(Sender: TObject);
    procedure cdsAreasAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsPtoToolsAlEnviarDatos(Sender: TObject);
    procedure cdsPuestosNewRecord(DataSet: TDataSet);
    procedure cdsPuestosBeforeInsert(DataSet: TDataSet);
    procedure cdsPtoToolsAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosAfterDelete(DataSet: TDataSet);
    procedure cdsSesionesAlAdquirirDatos(Sender: TObject);
    procedure cdsSesionesAlModificar(Sender: TObject);
    procedure cdsSesionesNewRecord(DataSet: TDataSet);
    procedure cdsSesionesAlEnviarDatos(Sender: TObject);
    procedure cdsSesionesAlBorrar(Sender: TObject);
    procedure cdsSesionesAfterDelete(DataSet: TDataSet);
    procedure cdsSesionesAlCrearCampos(Sender: TObject);
    procedure cdsSesionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsHisCursosAfterDelete(DataSet: TDataSet);
    procedure cdsHisCursosAfterEdit(DataSet: TDataSet);
    procedure cdsHisCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCursosBeforePost(DataSet: TDataSet);
    procedure cdsSesionesBeforePost(DataSet: TDataSet);
    procedure cdsGridCB_CODIGOValidate(Sender: TField);
    procedure CursosCB_CODIGOChange(Sender: TField);
    procedure cdsHisCursosAlCrearCampos(Sender: TObject);
    procedure cdsHisCursosNewRecord(DataSet: TDataSet);
    //procedure cdsSesionesAfterOpen(DataSet: TDataSet);
    //procedure SESIONHORASChange(Sender: TField);
    procedure cdsAccReglasAlAdquirirDatos(Sender: TObject);
    procedure cdsAccReglasAlModificar(Sender: TObject);
    procedure cdsAccReglasBeforePost(DataSet: TDataSet);
    procedure cdsAccReglasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsAccReglasNewRecord(DataSet: TDataSet);
    procedure cdsAccReglasAlCrearCampos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsSesionesCalcFields(DataSet: TDataSet);
    procedure cdsConceptosBeforeDelete(DataSet: TDataSet);
    procedure cdsConceptosBeforePost(DataSet: TDataSet);
    procedure cdsAulasAlAdquirirDatos(Sender: TObject);
    procedure cdsAulasAlModificar(Sender: TObject);
    procedure cdsAulasBeforePost(DataSet: TDataSet);
    procedure cdsAulasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsAulasNewRecord(DataSet: TDataSet);
    procedure cdsPrerequisitosCursoAlAdquirirDatos(Sender: TObject);
    procedure cdsPrerequisitosCursoAlCrearCampos(Sender: TObject);
    procedure cdsPrerequisitosCursoNewRecord(DataSet: TDataSet);
    procedure cdsPrerequisitosCursoAlModificar(Sender: TObject);
    procedure cdsPrerequisitosCursoBeforePost(DataSet: TDataSet);
    procedure cdsCertificacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsCertificacionesAlModificar(Sender: TObject);
    procedure cdsCertificacionesBeforePost(DataSet: TDataSet);
    procedure cdsCertificacionesAlEnviarDatos(Sender: TObject);
    procedure cdsTiposPolizaAlAdquirirDatos(Sender: TObject);
    procedure cdsTiposPolizaAlModificar(Sender: TObject);
    procedure cdsTiposPolizaAlEnviarDatos(Sender: TObject);
    procedure cdsTiposPolizaBeforePost(DataSet: TDataSet);
    procedure cdsTPeriodosAlAdquirirDatos(Sender: TObject);
    procedure cdsTPeriodosAlModificar(Sender: TObject);
    procedure cdsPerfilesPuestoAlAdquirirDatos(Sender: TObject);
    procedure cdsSeccionesPerfilAlAdquirirDatos(Sender: TObject);
    procedure cdsCamposPerfilAlCrearCampos(Sender: TObject);
    procedure cdsSeccionesPerfilBeforePost(DataSet: TDataSet);
    procedure cdsSeccionesPerfilAlEnviarDatos(Sender: TObject);
    procedure cdsSeccionesPerfilAlModificar(Sender: TObject);
    procedure cdsCamposPerfilNewRecord(DataSet: TDataSet);
    procedure cdsCamposPerfilAlEnviarDatos(Sender: TObject);
    procedure cdsCamposPerfilBeforePost(DataSet: TDataSet);
    procedure cdsCamposPerfilAlModificar(Sender: TObject);
    procedure cdsPerfilesPuestoAlModificar(Sender: TObject);
    procedure cdsPerfilesPuestoAlAgregar(Sender: TObject);
    procedure cdsCamposPerfilAfterDelete(DataSet: TDataSet);
    procedure cdsSeccionesPerfilAfterDelete(DataSet: TDataSet);
    procedure cdsCamposPerfilAlAdquirirDatos(Sender: TObject);
    procedure cdsPerfilesNewRecord(DataSet: TDataSet);
    procedure cdsPerfilesAlEnviarDatos(Sender: TObject);
    procedure cdsPerfilesAlCrearCampos(Sender: TObject);
    procedure cdsPerfilesAlAdquirirDatos(Sender: TObject);
    procedure cdsPerfilesAlAgregar(Sender: TObject);
    procedure cdsDescPerfilAlEnviarDatos(Sender: TObject);
    procedure cdsDescPerfilAfterDelete(DataSet: TDataSet);
    procedure cdsPerfilesPuestoAfterDelete(DataSet: TDataSet);
    procedure cdsPerfilesAlModificar(Sender: TObject);
    procedure PF_SEXOSetText(Sender: TField; const Text: String);
    procedure PF_SEXOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PF_EDADValidate(Sender: TField);
    procedure PF_EXP_PTOValidate(Sender: TField);
    procedure cdsDescPerfilAlCrearCampos(Sender: TObject);
    procedure cdsDescPerfilAlAdquirirDatos(Sender: TObject);
    procedure cdsDescPerfilAlModificar(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsDescPerfilNewRecord(DataSet: TDataSet);
    procedure cdsPerfilesPuestoAlEnviarDatos(Sender: TObject);
    procedure cdsSeccionesPerfilAlCrearCampos(Sender: TObject);
    procedure cdsDescPerfilAfterCancel(DataSet: TDataSet);
    procedure cdsDescPerfilBeforeInsert(DataSet: TDataSet);
    procedure cdsDescPerfilBeforeEdit(DataSet: TDataSet);
    procedure cdsCertificacionesAfterDelete(DataSet: TDataSet);
    procedure cdsTiposPolizaAfterDelete(DataSet: TDataSet);
    procedure cdsSeccionesPerfilAlBorrar(Sender: TObject);
    procedure cdsCamposPerfilBeforeEdit(DataSet: TDataSet);
    procedure cdsCamposPerfilBeforeInsert(DataSet: TDataSet);
    procedure cdsDescPerfilBeforePost(DataSet: TDataSet);
    procedure cdsPerfilesBeforePost(DataSet: TDataSet);
    procedure cdsClasifiNewRecord(DataSet: TDataSet);
    procedure cdsMaestrosNewRecord(DataSet: TDataSet);
    procedure cdsCertificacionesNewRecord(DataSet: TDataSet);
    procedure cdsSSocialBeforeInsert(DataSet: TDataSet);
    procedure cdsProvCapAlAdquirirDatos(Sender: TObject);
    procedure cdsProvCapBeforePost(DataSet: TDataSet);
    procedure cdsProvCapAlModificar(Sender: TObject);
    procedure cdsProvCapGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsProvCapNewRecord(DataSet: TDataSet);
    procedure cdsValPlantillaAlAdquirirDatos(Sender: TObject);
    procedure cdsValPlantillaAlModificar(Sender: TObject);
    procedure cdsValPlantillaAlCrearCampos(Sender: TObject);
    procedure cdsValPlantillaNewRecord(DataSet: TDataSet);
    procedure cdsValPlantillaBeforePost(DataSet: TDataSet);
    procedure cdsValPlantillaGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsValPlantillaAlEnviarDatos(Sender: TObject);
    procedure cdsValFactoresAlAdquirirDatos(Sender: TObject);
    procedure cdsValFactoresAlEnviarDatos(Sender: TObject);
    procedure cdsValFactoresAlModificar(Sender: TObject);
    procedure cdsValFactoresGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsValFactoresNewRecord(DataSet: TDataSet);
    procedure cdsSubfactoresAlAdquirirDatos(Sender: TObject);
    procedure cdsSubfactoresAlModificar(Sender: TObject);
    procedure cdsSubfactoresAlEnviarDatos(Sender: TObject);
    procedure cdsSubfactoresGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsValNivelesAlAdquirirDatos(Sender: TObject);
    procedure cdsValNivelesAlModificar(Sender: TObject);
    procedure cdsValNivelesAlEnviarDatos(Sender: TObject);
    procedure cdsValPlantillaAlBorrar(Sender: TObject);
    procedure cdsValFactoresBeforePost(DataSet: TDataSet);
    procedure cdsSubfactoresBeforePost(DataSet: TDataSet);
    procedure cdsValNivelesBeforePost(DataSet: TDataSet);
    procedure cdsSubfactoresNewRecord(DataSet: TDataSet);
    procedure cdsValNivelesNewRecord(DataSet: TDataSet);
    procedure cdsValFactoresAlBorrar(Sender: TObject);
    procedure cdsValuacionesAlCrearCampos(Sender: TObject);
    procedure cdsValuacionesNewRecord(DataSet: TDataSet);
    procedure cdsSubfactoresAlBorrar(Sender: TObject);
    procedure cdsValNivelesAlBorrar(Sender: TObject);
    procedure cdsValuacionesReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsValuacionesAlModificar(Sender: TObject);
    procedure cdsValuacionesAlEnviarDatos(Sender: TObject);
    procedure cdsValPuntosAfterDelete(DataSet: TDataSet);
    procedure cdsValPuntosAfterEdit(DataSet: TDataSet);
    procedure cdsValPuntosAlAdquirirDatos(Sender: TObject);
    procedure cdsValPuntosAlCrearCampos(Sender: TObject);
    procedure cdsValNivelesLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsValuacionesAfterDelete(DataSet: TDataSet);
    procedure cdsValuacionesBeforePost(DataSet: TDataSet);
    procedure cdsValPuntosBeforePost(DataSet: TDataSet);
    procedure cdsPuntosNivelAlAdquirirDatos(Sender: TObject);
    procedure cdsPuntosNivelLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsSubfactoresAlCrearCampos(Sender: TObject);
    procedure cdsValFactoresAlCrearCampos(Sender: TObject);
    procedure cdsValNivelesAlCrearCampos(Sender: TObject);
    procedure cdsMaestrosAlEnviarDatos(Sender: TObject);
    procedure cdsValuacionesAfterCancel(DataSet: TDataSet);
    procedure cdsRSocialAlAdquirirDatos(Sender: TObject);
    procedure cdsRSocialGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsRSocialBeforePost(DataSet: TDataSet);
    procedure cdsRSocialAlModificar(Sender: TObject);
    procedure cdsRSocialAlEnviarDatos(Sender: TObject);
    procedure cdsMatrizCertificPuestoAlAdquirirDatos(Sender: TObject);
    procedure cdsMatrizCertificPuestoAlCrearCampos(Sender: TObject);
    procedure cdsCerNivelAfterDelete(DataSet: TDataSet);
    procedure cdsCerNivelNewRecord(DataSet: TDataSet);
    procedure cdsMatrizCertificPuestoAfterCancel(DataSet: TDataSet);
    procedure cdsMatrizCertificPuestoAfterDelete(DataSet: TDataSet);
    procedure cdsMatrizCertificPuestoAfterOpen(DataSet: TDataSet);
    procedure cdsMatrizCertificPuestoAlEnviarDatos(Sender: TObject);
    procedure cdsMatrizCertificPuestoAlModificar(Sender: TObject);
    procedure cdsMatrizCertificPuestoBeforePost(DataSet: TDataSet);
    procedure cdsMatrizCertificPuestoNewRecord(DataSet: TDataSet);
    procedure cdsPeriodoOtroLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsPeriodoOtroLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsReglaPrestamoAlAdquirirDatos(Sender: TObject);
    procedure cdsReglaPrestamoAlEnviarDatos(Sender: TObject);
    procedure cdsReglaPrestamoAlModificar(Sender: TObject);
    procedure cdsReglaPrestamoAlCrearCampos(Sender: TObject);
    procedure RP_LISTAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsPrestaXReglaAlAdquirirDatos(Sender: TObject);
    procedure cdsPrestaXReglaAlEnviarDatos(Sender: TObject);
    procedure cdsPrestaXReglaNewRecord(DataSet: TDataSet);
    procedure cdsReglaPrestamoBeforePost(DataSet: TDataSet);
    procedure cdsReglaPrestamoNewRecord(DataSet: TDataSet);
    procedure cdsReglaPrestamoAfterDelete(DataSet: TDataSet);
    procedure cdsTPeriodosLookupDescription(Sender: TZetaLookupDataSet;var sDescription: String);
    procedure cdsCondicionesAfterPost(DataSet: TDataSet);
    procedure cdsReglaPrestamoAfterCancel(DataSet: TDataSet);
    procedure cdsPeriodoAdAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodoAdLookupDescription(Sender: TZetaLookupDataSet; var sDescription: String);
    procedure cdsPeriodoAdLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsPeriodoAdLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsPeriodoAdReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHistRevReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsHistRevNewRecord(DataSet: TDataSet);
    procedure cdsHistRevAfterEdit(DataSet: TDataSet);
    procedure cdsHistRevPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure cdsHistRevAfterPost(DataSet: TDataSet);
    procedure cdsHistRevBeforePost(DataSet: TDataSet);
    procedure cdsHistRevAlCrearCampos(Sender: TObject);
    procedure cdsHistRevAfterDelete(DataSet: TDataSet);
    procedure cdsFestTurnoAlEnviarDatos(Sender: TObject);
    procedure cdsAreasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEstablecimientosAlAdquirirDatos(Sender: TObject);
    procedure cdsEstablecimientosAlEnviarDatos(Sender: TObject);
    procedure cdsEstablecimientosAlModificar(Sender: TObject);
    procedure cdsEstablecimientosBeforePost(DataSet: TDataSet);
    procedure cdsEstablecimientosNewRecord(DataSet: TDataSet);
    procedure cdsTiposPensionAlAdquirirDatos(Sender: TObject);
    procedure cdsTiposPensionAlEnviarDatos(Sender: TObject);
    procedure cdsTiposPensionBeforePost(DataSet: TDataSet);
    procedure cdsTiposPensionNewRecord(DataSet: TDataSet);
    procedure cdsTiposPensionAlModificar(Sender: TObject);
    procedure cdsTiposPensionAlCrearCampos(Sender: TObject);
    procedure TP_NOMINAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure TP_CO_PENGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsTiposPensionAfterDelete(DataSet: TDataSet);
    procedure cdsSegGastosMedAlAdquirirDatos(Sender: TObject);
    procedure cdsSegGastosMedAlModificar(Sender: TObject);
    procedure cdsSegGastosMedBeforePost(DataSet: TDataSet);
    procedure cdsSegGastosMedNewRecord(DataSet: TDataSet);
    procedure cdsVigenciasSGMNewRecord(DataSet: TDataSet);
    procedure cdsSegGastosMedAlCrearCampos(Sender: TObject);
    procedure cdsVigenciasSGMAfterDelete(DataSet: TDataSet);
    procedure cdsSegGastosMedAfterCancel(DataSet: TDataSet);
    procedure cdsSegGastosMedAlEnviarDatos(Sender: TObject);
    procedure cdsSegGastosMedAfterOpen(DataSet: TDataSet);
    procedure cdsSegGastosMedAfterDelete(DataSet: TDataSet);
    procedure cdsVigenciasSGMAlEnviarDatos(Sender: TObject);
    procedure cdsVigenciasSGMBeforeDelete(DataSet: TDataSet);
    procedure cdsVigenciasSGMBeforePost(DataSet: TDataSet);
    procedure cdsVigenciasSGMAfterPost(DataSet: TDataSet);
    procedure cdsSegGastosMedReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsCosteoGruposAlAdquirirDatos(Sender: TObject);
    procedure cdsCosteoGruposAlModificar(Sender: TObject);
    procedure cdsCosteoGruposBeforePost(DataSet: TDataSet);
    procedure cdsCosteoGruposAlEnviarDatos(Sender: TObject);
    procedure cdsCosteoGruposNewRecord(DataSet: TDataSet);
    procedure cdsCosteoGruposGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsCosteoCriteriosAlAdquirirDatos(Sender: TObject);
    procedure cdsCosteoCriteriosNewRecord(DataSet: TDataSet);
    procedure cdsCosteoCriteriosAlModificar(Sender: TObject);
    procedure cdsCosteoCriteriosBeforePost(DataSet: TDataSet);
    procedure cdsCosteoCriteriosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsCosteoCriteriosAlEnviarDatos(Sender: TObject);
    procedure cdsCosteoCriteriosPorConceptoAlAdquirirDatos(Sender: TObject);
    procedure cdsCosteoCriteriosPorConceptoAlCrearCampos(Sender: TObject);
    procedure cdsCosteoCriteriosPorConceptoAlEnviarDatos(Sender: TObject);
    procedure cdsCosteoCriteriosPorConceptoAlModificar(Sender: TObject);
    procedure cdsCosteoCriteriosPorConceptoNewRecord(DataSet: TDataSet);
    procedure cdsConceptosDisponiblesAlAdquirirDatos(Sender: TObject);
    procedure cdsCosteoGruposAfterDelete(DataSet: TDataSet);
    procedure cdsCosteoCriteriosAfterDelete(DataSet: TDataSet);
    procedure cdsCosteoCriteriosPorConceptoAfterDelete(DataSet: TDataSet);
    procedure cdsCosteoCriteriosPorConceptoBeforePost(DataSet: TDataSet);
    procedure cdsCosteoCriteriosPorConceptoReconcileError(DataSet: TCustomClientDataSet;E: EReconcileError; UpdateKind: TUpdateKind;var Action: TReconcileAction);
    procedure cdsCosteoGruposReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsCompetenciasAlAdquirirDatos(Sender: TObject);
    procedure cdsCatPerfilesAlAdquirirDatos(Sender: TObject);
    procedure cdsCompetenciasAlModificar(Sender: TObject);
    procedure cdsCompetenciasBeforePost(DataSet: TDataSet);
    procedure cdsCompetenciasAlEnviarDatos(Sender: TObject);
    procedure cdsCompNivelesBeforePost(DataSet: TDataSet);
    procedure cdsCompNivelesNewRecord(DataSet: TDataSet);
    procedure cdsCompRevisionesNewRecord(DataSet: TDataSet);
    procedure cdsCompRevisionesBeforePost(DataSet: TDataSet);
    procedure cdsCompCursosNewRecord(DataSet: TDataSet);
    procedure cdsCompCursosBeforePost(DataSet: TDataSet);
    procedure cdsCompetenciasAfterDelete(DataSet: TDataSet);
    procedure cdsCompetenciasNewRecord(DataSet: TDataSet);
    procedure cdsCompetenciasAlCrearCampos(Sender: TObject);
    procedure cdsCompetenciasAfterCancel(DataSet: TDataSet);
    procedure cdsCompCursosAlCrearCampos(Sender: TObject);
    procedure cdsCatPerfilesAlModificar(Sender: TObject);
    procedure cdsCatPerfilesBeforePost(DataSet: TDataSet);
    procedure cdsCatPerfilesNewRecord(DataSet: TDataSet);
    procedure cdsCatPerfilesAfterCancel(DataSet: TDataSet);
    procedure cdsCatPerfilesAlEnviarDatos(Sender: TObject);
    procedure cdsRevPerfilesNewRecord(DataSet: TDataSet);
    procedure cdsMatPerfilCompsAlAdquirirDatos(Sender: TObject);
    procedure cdsMatPerfilCompsAlCrearCampos(Sender: TObject);
    procedure cdsMatPerfilCompsNewRecord(DataSet: TDataSet);
    procedure cdsMatPerfilCompsBeforePost(DataSet: TDataSet);
    procedure cdsMatPerfilCompsAlEnviarDatos(Sender: TObject);
    procedure cdsCompRevisionesAlCrearCampos(Sender: TObject);
    procedure cdsCatPerfilesAlCrearCampos(Sender: TObject);
    procedure cdsRevPerfilesAlCrearCampos(Sender: TObject);
    procedure cdsMatPerfilCompsAlModificar(Sender: TObject);
    procedure ActivoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsCompRevisionesAfterEdit(DataSet: TDataSet);
    procedure cdsRevPerfilesAfterEdit(DataSet: TDataSet);
    procedure cdsGpoCompPuestoBeforePost(DataSet: TDataSet);
    procedure cdsGpoCompPuestoNewRecord(DataSet: TDataSet);
    procedure cdsGpoCompPuestoAlCrearCampos(Sender: TObject);
    procedure cdsGpoCompPuestoAlEnviarDatos(Sender: TObject);
    procedure cdsGpoCompPuestoAfterEdit(DataSet: TDataSet);
    procedure cdsPuestosAfterCancel(DataSet: TDataSet);
    procedure cdsGpoCompPuesto_CP_CODIGOValidate(Sender: TField);
    procedure cdsCatPerfilesAfterDelete(DataSet: TDataSet);
    procedure cdsCompetenciasReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsMatPerfilCompsAfterDelete(DataSet: TDataSet);
    procedure cdsCatPerfilesReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsCompCursosCU_CODIGOValidate(Sender: TField);
    procedure cdsMatPerfilCompsReconcileError(
      DataSet: TCustomClientDataSet; E: EReconcileError;
      UpdateKind: TUpdateKind; var Action: TReconcileAction);


    procedure cdsTurnosAlCrearCampos(Sender: TObject);
    procedure cdsTurnosTU_CODIGOValidate(Sender: TField);
    procedure cdsPuestosAlCrearCampos(Sender: TObject);
    procedure cdsPuestosPU_CODIGOValidate(Sender: TField);
    procedure cdsCursosCU_CODIGOValidate(Sender: TField);
    procedure cdsInvitadoresAfterPost(DataSet: TDataSet); // BIOMETRICO
    procedure cdsEventosEV_TIPNOMChange(Sender: TField);
    procedure cdsEventosAlCrearCampos(Sender: TObject);
    procedure cdsTablasAmortizacionAlAdquirirDatos(Sender: TObject);
    procedure cdsTablasAmortizacionAlModificar(Sender: TObject);
    procedure cdsTablasAmortizacionBeforePost(DataSet: TDataSet);
    procedure cdsTablasAmortizacionNewRecord(DataSet: TDataSet);
    procedure cdsCostosAmortizacionNewRecord(DataSet: TDataSet);
    procedure cdsTablasAmortizacionAfterCancel(DataSet: TDataSet);
    procedure cdsTablasAmortizacionAlEnviarDatos(Sender: TObject);
    procedure cdsTablasAmortizacionAfterDelete(DataSet: TDataSet);
    procedure cdsCostosAmortizacionAlEnviarDatos(Sender: TObject);
    procedure cdsCostosAmortizacionBeforePost(DataSet: TDataSet);
    procedure cdsCostosAmortizacionAfterPost(DataSet: TDataSet);
    procedure cdsCostosAmortizacionAfterDelete(DataSet: TDataSet);
    procedure cdsCostosAmortizacionAlCrearCampos(Sender: TObject);
    procedure cdsVigenciasSGMAlCrearCampos(Sender: TObject);
    procedure cdsCostosAmortizacionAfterEdit(DataSet: TDataSet);
    procedure CO_USO_NOMGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsAulasAlCrearCampos(Sender: TObject);
    procedure cdsTiposSATAfterDelete(DataSet: TDataSet);
    procedure cdsTiposSATAlAdquirirDatos(Sender: TObject);
    procedure cdsTiposSATAlCrearCampos(Sender: TObject);
    procedure cdsTiposSATAlEnviarDatos(Sender: TObject);
    procedure cdsTiposSATAlModificar(Sender: TObject);
    procedure cdsTiposSATLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsFoliosBeforeDelete(DataSet: TDataSet);
    procedure cdsRPatronBeforeDelete(DataSet: TDataSet);
    procedure cdsMatrizCertificPuestoBeforeDelete(DataSet: TDataSet);
    procedure cdsSSocialBeforeDelete(DataSet: TDataSet);
    procedure cdsMatrizCursoBeforeDelete(DataSet: TDataSet);
    procedure cdsTiposSATBeforePost(DataSet: TDataSet);
    procedure cdsTiposSATGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsConceptosLookUpLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: string; var sKey, sDescription: string);
  private
    { Private declarations }
    FEditando: Boolean;
    FSoloInsercion: Boolean;
    FTurnoFestivo: String;
    FMatrizPorCurso: Boolean;
    FMatrizPorCertific: Boolean;

    FFormaCatPeriodos: Boolean;
    FRefrescaPeriodo: Boolean;
    FUltimoPeriodo: Integer;
    FNuevoPeriodo: Integer;
    FUltimoConcepto: Integer;
    FUltimoInvitador: Integer;
    FUltimoParam: Integer;
    FUltimoYear: Integer;
    FUltimaVaca: TDiasHoras;
    FFormaTipoPeriodo: eTipoPeriodo;
    FMaxFolio: Integer;
    FFecIniSesion: TDateTime;
    FFecFinSesion: TDateTime;
    FFolioSesion: Integer;
    FCodigoCursoSesion: String;
    FMaxFolioSesion: Integer;
    FDuracion_Curso: TDiasHoras;
    FPrerequisitosCurso: String;
    FSeccionCampos:String;
    FAgregandoPerfil:Boolean;
    FCamposAdic: TListaCampos;
    FClasificaciones: TListaClasificaciones;
    FListaCampos: String;
    FCambioPrestaciones: Boolean;
    FPlantillaActiva: Integer;
    FAgregandoVal: Boolean;
    // FRefrescarSubfactor: Boolean;
    FCambioNiveles: Boolean;
    FTiposPrestamosVacios :Boolean;
    FFormaCatCondiciones: Boolean;
    FNavegacion : String;
    FHayErrorCount : Integer;
    FAgregaReglaPrestamo: Boolean;
    FRefrescaEntidades: Boolean;
    FGrupoDeCosteo : String;
    FCriterioDeCosteo : Integer;
    FFechaUltRevComp:TDateTime;
    FListaCostoSGM:TList;
{$ifdef DOS_CAPAS}
    function GetServerCatalogos: TdmServerCatalogos;
    property ServerCatalogo: TdmServerCatalogos read GetServerCatalogos;
    function GetServerLabor: TdmServerLabor;
    property ServerLabor: TdmServerLabor read GetServerLabor;
{$else}
    FServidor: IdmServerCatalogosDisp;
    FServidorLabor: IdmServerLaborDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
    function GetServerLabor: IdmServerLaborDisp;
    property ServerLabor: IdmServerLaborDisp read GetServerLabor;
{$endif}
    function CalculaFactorIntegracion: Currency;
    procedure cdsBeforePost(DataSet: TDataSet; const sCampo: String);

    procedure OnCO_SAT_CLPChange(Sender: TField);
    procedure OnCO_SAT_CLNChange(Sender: TField);
    procedure OnCO_SAT_EXEChange(Sender: TField);
    procedure OnCO_TIMBRAChange(Sender: TField);


    procedure cdsPrestaciCamposFactorChange(Sender: TField);
    procedure PE_NUMEROChange(Sender: TField);
    procedure PE_USOChange(Sender: TField);
{$ifdef QUINCENALES}
    procedure PE_FEC_INIChange(Sender: TField);
    procedure PE_FEC_FINChange(Sender: TField);
{$endif}
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CursoDescripChange( Sender: TField );
    procedure SetPlantillaActiva( const Value: Integer );
    procedure BorraVal( DataSet: TZetaClientDataSet; sCampo: String );
    procedure TotalesGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SetNavegacion(const Value: String);    
    procedure ActualizaRevision;
    function GetErrorMessage( E: EReconcileError ): String;
    procedure PE_STATUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  public
    { Public declarations }
    property MatrizPorCurso: Boolean read FMatrizPorCurso;
    property MatrizPorCertific: Boolean read FMatrizPorCertific write FMatrizPorCertific;
    property TurnoFestivo: String read FTurnoFestivo write FTurnoFestivo;
    property FormaCatPeriodos: Boolean read FFormaCatPeriodos write FFormaCatPeriodos;
    property FormaTipoPeriodo: eTipoPeriodo read FFormaTipoPeriodo write FFormaTipoPeriodo;
    property FecIniSesion: TDateTime read FFecIniSesion write FFecIniSesion;
    property FecFinSesion: TDateTime read FFecFinSesion write FFecFinSesion;
    property CodigoCursoSesion: String read FCodigoCursoSesion write FCodigoCursoSesion;
    property FolioSesion: Integer read FFolioSesion write FFolioSesion;
    property MaxFolioSesion: Integer read FMaxFolioSesion write FMaxFolioSesion;
    property Duracion_Curso: TDiasHoras read FDuracion_Curso write FDuracion_Curso;
    property PrerequisitosCurso: String read FPrerequisitosCurso write FPrerequisitosCurso;
    property SeccionCampos: String read FSeccionCampos write FSeccionCampos;
    property CamposAdic: TListaCampos read FCamposAdic;
    property Clasificaciones: TListaClasificaciones read FClasificaciones;
    property PlantillaActiva: Integer read FPlantillaActiva write SetPlantillaActiva;
    property AgregandoVal: Boolean read FAgregandoVal write FAgregandoVal;
    property TiposPrestamosVacios: Boolean read FTiposPrestamosVacios write FTiposPrestamosVacios;
    property FormaCatCondiciones: Boolean read FFormaCatCondiciones write FFormaCatCondiciones;
    property Navegacion: String read FNavegacion write setNavegacion;
    property HayErrorCount: Integer read FHayErrorCount write FHayErrorCount;
    property AgregaReglaPrestamo: Boolean read FAgregaReglaPrestamo write FAgregaReglaPrestamo;
    property RefrescaEntidades : Boolean read FRefrescaEntidades write FRefrescaEntidades;
    property GrupoDeCosteo : String read FGrupoDeCosteo write FGrupoDeCosteo;
    property CriterioDeCosteo : Integer read FCriterioDeCosteo write FCriterioDeCosteo;
    property FechaUltRevComp: TDateTime read FFechaUltRevComp write FFechaUltRevComp;
    property ListaCostoSGM : TList read FListaCostoSGM write FListaCostoSGM;
    function BuscaPeriodo(const iYear, iNumero: Integer; const eTipo: eTipoPeriodo; var eStatus: eStatusPeriodo): Boolean; overload;
    function BuscaPeriodo(const iYear, iNumero: Integer; const eTipo: eTipoPeriodo; var eStatus: eStatusPeriodo; var dFechaInicial, dFechaFinal: TDate ): Boolean; overload;
    function DescrVencimiento( const sLlave: String; const dFecha: TDateTime ): String;
    function GetJornadaHorario( const sHorario: String ): Currency;
    function PlantillaEnDiseno( const iPlantilla: Integer; var sMensaje: String; const sAccion: String): Boolean;
    function PlantillaLiberada( const iPlantilla: Integer; var sMensaje: String; const sAccion: String): Boolean;
    //function GetTipoNomina( const sTurno: String ): eTipoPeriodo;
    procedure ShowCalendarioRitmo( const sTurno: TCodigo );
    procedure CargaListaHerramientas( Lista: TStrings );
    procedure DescargaHerramientas( Lista: TStrings );
    procedure SetMatriz( const lCurso: Boolean; const sLlave: String );
    procedure AgregaOrdenFolio(const iFolio: Integer; const oLista: TListaObjetos);
    procedure AgregaPeriodo;
    procedure AplicaSSocial;
    procedure CargaOrdenFolio( oLista: TListaObjetos );
    procedure GetDatosPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo );
    procedure GetDatosPeriodoAd( const iYear: Integer; const TipoPeriodo: eTipoPeriodo );    
    procedure GetDatosPeriodoOtro( const iYear: Integer );
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure CargaListaPuestos( oLista: TStrings ); overload;
    procedure CargaListaPuestos( oLista: TcxCheckListBoxItems ); overload;
    procedure CargaListaEventos( oLista: TStrings );
    procedure GuardaEntrenaPuestosProg( const sPuesto, sCurso : string ;const MatrizCursos:Boolean );
    procedure SetPuestoPercepcionesFijas;
    procedure SetPuestoHerramientas;
    procedure ObtieneSesiones;
    procedure ConectacdsHisCursos(const iFolio: Integer);
    procedure FiltraDatosSeccion( const sSeccion, sSeccionDescrip: String );
    procedure LlenaListaCampos(DataSet:TDataSet;const DF_CAMPO:string = '');
    function CargaDocumento( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
    procedure AbreDocumento;
    procedure BorraDocumento;
    procedure RefrescarValuacion( Parametros: TZetaParams );
    procedure AgregaCopiaValuacion( Parametros: TZetaParams );
    procedure ConectacdsValPuntos( const iFolio: Integer );
    procedure ConectacdsValNivel( const iPlantilla: Integer; const iSubfactor: Integer );
    procedure ObtieneTotalesValuacion( var rPuntos: Double; var iRespuestas: Integer );
    procedure GetPeriodoPorMes( const eTipoPer: eTipoPeriodo; iMes: Integer; var iPeriodoIni, iPeriodoFinal: Integer);
    procedure ModificaEdicionTotalesNomina ( const lNomTotales:Boolean );
    procedure InitActivosNavegacion;
    function LlenaNavegacion(Lista: TStrings; sCodigo: string ): Integer;
    function GetNavegacionDescripcion: String;
    procedure AbreFormaEdicionCondicion;
    procedure CargaListaGrupos( Lista: TStrings );
    procedure DescargaListaGrupos( Lista: TStrings );
    procedure GetCursosRevision( const sCurso: String);
    procedure CosteoAgregaConceptos;
    procedure ValidaMemoField(DataSet: TDataSet; const sField: string);
    procedure AplicaFiltroStatusPeriodo(DBLookup: TZetaKeyLookup;Aplicar:Boolean ); overload;
    procedure AplicaFiltroStatusPeriodo(DBLookup: TZetaKeyLookup_DevEx;Aplicar:Boolean ); overload;
    function GetUltimaRevisionPerfil:TDateTime;
    function GetUltimaRevisionCompetencia:TDateTime;
    procedure LlenarListaCostosSGM;
    function GetNivelRequeridoCompetencia(Competencia: string;var Descripcion:string): Integer;
  end;
type
    TCostoSGM = class(TObject)
    public
     Indice:Integer;
     EdadInicial:Integer;
     EdadFin:Integer;
end;

const
  DaysInMonth: array[ 1..12 ] of Integer = (31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                                       // 29 en Febrero por los a�os bisiestos
var
  dmCatalogos: TdmCatalogos;

implementation

uses DCliente,
     DSistema,
     DRecursos,
     DGlobal,
     DNomina,
     ZAccesosMgr,
     zAccesosTress,
     ZGlobalTress,
     ZReconcile,
     ZReportTools,
     ZetaDataSetTools,
     ZetaBuscaPeriodo_DevEx,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaMsgDlg,
     ZetaClientTools,
     ZBaseDlgModal, ZBaseDlgModal_DevEx,
     FTressShell,
     FEditCatConceptos_DevEx,
     FBusquedaConceptos_Version_DevEx,
     FEditTipoSAT,
     FEditCatCondiciones_DevEx,
     ZcxWizardBasico,
     DProcesos,
     ZetaFilesTools,
     FBusquedaTipoSAT_Version_DevEx,
     DBasicoCliente,
     dTablas;

{$R *.DFM}

{ TdmCatalogos }

const
     K_MAX_FOLIO = 5;
     K_ESPACIO = ' ';

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
function TdmCatalogos.GetServerLabor: TdmServerLabor;
begin
     Result := DCliente.dmCliente.ServerLabor;
end;
{$else}
function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
function TdmCatalogos.GetServerLabor: IdmServerLaborDisp;
begin
     Result := IdmServerLaborDisp( dmCliente.CreaServidor( CLASS_dmServerLabor, FServidorLabor ) );
end;
{$endif}

procedure TdmCatalogos.DataModuleCreate(Sender: TObject);
begin
     FDuracion_Curso := 0;
     FAgregandoPerfil := FALSE;
     FCamposAdic := TListaCampos.Create;
     FClasificaciones := TListaClasificaciones.Create;
     FSoloInsercion := FALSE;
     FAgregandoVal := TRUE;
     FCambioNiveles:= FALSE;
     FTiposPrestamosVacios := FALSE;
     FRefrescaEntidades := TRUE;
end;

procedure TdmCatalogos.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FCamposAdic );
     FreeAndNil( FClasificaciones );
end;

{$ifdef VER130}
procedure TdmCatalogos.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmCatalogos.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

procedure TdmCatalogos.cdsBeforePost( DataSet: TDataSet; const sCampo: String );
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( sCampo ).AsString ) then
             DataBaseError( 'El C�digo No Puede Quedar Vac�o' );
     end;
end;

procedure TdmCatalogos.cdsAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmCatalogos.cdsCatalogosOnPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
     ZetaDialogo.ZError('Error', GetDBErrorDescription( E ), 0 );
     Action := daAbort;
end;

// Debe Regresar TPesos por que es a 4 Decimales
function TdmCatalogos.CalculaFactorIntegracion: Currency;
var
   rVacaciones, rDiasDominical: Real;
   lPrimaSeptimo, lPagoSeptimo: Boolean;
begin
     with cdsPrestaci do
     begin
          lPrimaSeptimo := zStrToBool( FieldByName( 'PT_PRIMA_7' ).AsString );
          lPagoSeptimo := zStrToBool( FieldByName( 'PT_PAGO_7' ).AsString );
          rDiasDominical := 365 / 7 * FieldByName( 'PT_PRIMADO' ).AsFloat / 100;
          rVacaciones := FieldByName( 'PT_DIAS_VA' ).AsFloat * FieldByName( 'PT_PRIMAVA' ).AsFloat / 100;
          if lPrimaSeptimo and lPagoSeptimo then
             rVacaciones := rVacaciones * 7 / 6;
          if not lPagoSeptimo then
             rVacaciones := rVacaciones - FieldByName( 'PT_DIAS_VA' ).AsFloat / 6;
          Result := ( 365 +
                      FieldByName('PT_DIAS_AG').AsFloat +
                      FieldByName('PT_DIAS_AD').AsFloat +
                      rVacaciones +
                      rDiasDominical ) / 365;
     end;
end;

procedure TdmCatalogos.AplicaSSocial;
begin
     with cdsPrestaci do
     begin
          DisableControls;
          First;
          while not EOF do
          begin
               Edit;
               FieldByName( 'PT_PRIMAVA' ).AsFloat := cdsSSocial.FieldByName( 'TB_PRIMAVA' ).AsFloat;
               FieldByName( 'PT_DIAS_AG' ).AsFloat := cdsSSocial.FieldByName( 'TB_DIAS_AG' ).AsFloat;
               FieldByName( 'PT_PAGO_7' ).AsString := cdsSSocial.FieldByName( 'TB_PAGO_7' ).AsString;
               FieldByName( 'PT_PRIMA_7' ).AsString := cdsSSocial.FieldByName( 'TB_PRIMA_7' ).AsString;
               FieldByName( 'PT_PRIMADO' ).AsFloat := cdsSSocial.FieldByName( 'TB_PRIMADO' ).AsFloat;
               FieldByName( 'PT_DIAS_AD' ).AsFloat := cdsSSocial.FieldByName( 'TB_DIAS_AD' ).AsFloat;
               Post;
               Next;
          end;
          EnableControls;
     end;
end;

function TdmCatalogos.DescrVencimiento(const sLlave: String; const dFecha: TDateTime): String;
begin
     with cdsContratos do
     begin
          if Locate( 'TB_CODIGO', sLlave, [] ) then
          begin
               if ( FieldByName( 'TB_DIAS' ).AsInteger > 0 ) then
                  Result := FechaCorta( dFecha + FieldByName( 'TB_DIAS' ).AsInteger - 1 )
               else
                   Result := 'Sin Vencimiento';
          end;
     end;
end;

procedure TdmCatalogos.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;
{$ifdef MULTIPLES_ENTIDADES}
procedure TdmCatalogos.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmCatalogos.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enFolio , Entidades ) then
     {$else}
     if ( enFolio in Entidades ) then
     {$endif}
        cdsFolios.SetDataChange;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCurso , Entidades ) then
     {$else}
     if ( enCurso in Entidades ) then
     {$endif}
     begin
          cdsCalendario.SetDataChange;
          cdsMatrizCurso.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPuesto , Entidades ) then
     {$else}
     if ( enPuesto in Entidades ) then
     {$endif}
     begin
          cdsMatrizCurso.SetDataChange;
          cdsPuestosLookup.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enTurno , Entidades ) then
     {$else}
     if ( enTurno in Entidades ) then
     {$endif}
        cdsFestTurno.SetDataChange;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enRPatron , Entidades ) then
     {$else}
     if ( enRPatron in Entidades ) then
     {$endif}
        cdsRPatron.SetDataChange;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPeriodo , Entidades ) or ( Estado = stSistema ) then
     {$else}
     if ( enPeriodo in Entidades ) or ( Estado = stSistema ) then
     {$endif}
        cdsPeriodo.SetDataChange;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enSesion , Entidades ) then
     {$else}
     if ( enSesion in Entidades ) then
     {$endif}
        cdsSesiones.SetDataChange;
     if Dentro( enCamposPerfil , Entidades ) then
        cdsCamposPerfil.SetDataChange;

     if Dentro( enPerfilPuesto , Entidades ) then
        cdsPerfilesPuesto.SetDataChange;

     if Dentro( enValFact, Entidades ) then
     begin
          cdsSubfactores.SetDataChange;
          cdsValPlantilla.SetDataChange;
     end;

     if Dentro( enValSubfact, Entidades ) then
     begin
          cdsValFactores.SetDataChange;
          cdsValPlantilla.SetDataChange;
     end;

     if Dentro( enValNiveles, Entidades ) then
     begin
          cdsSubfactores.SetDataChange;
          cdsValFactores.SetDataChange;
          cdsValuaciones.SetDataChange;
          cdsValPlantilla.SetDataChange;
          cdsPuntosNivel.SetDataChange;
     end;

     if Dentro( enValuacion, Entidades ) then
        cdsValPlantilla.SetDataChange;

     if Dentro( enValPlant, Entidades ) then
     begin
          cdsSubfactores.SetDataChange;
          cdsValfactores.SetDataChange;
     end;

     if Dentro ( enRSocial, Entidades ) then
        cdsRPatron.SetDataChange;
     if ( Estado = stSistema ) then
        cdsFestTurno.SetDataChange;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCatSegGasMed , Entidades ) then
     {$else}
     if ( enCatSegGasMed in Entidades ) then
     {$endif}
     begin
          cdsSegGastosMed.SetDataChange;
          cdsVigenciasSGM.SetDataChange;
     end;

{$ifndef DOS_CAPAS}
     if Dentro( enCriteriosCosteo, Entidades ) or Dentro( enGruposCosteo, Entidades ) then
          cdsCosteoCriteriosPorConcepto.SetDataChange;
{$endif}
end;

{ cdsClasifi }

procedure TdmCatalogos.cdsClasifiAlAdquirirDatos(Sender: TObject);
begin
     cdsClasifi.Data := ServerCatalogo.GetClasifi( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsClasifiAlCrearCampos(Sender: TObject);
begin
     with cdsClasifi do
     begin
          MaskPesos( 'TB_SALARIO' );
     end;
end;

procedure TdmCatalogos.cdsClasifiAlModificar(Sender: TObject);
begin
//     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatClasifi_DevEx, TEditCatClasifi_DevEx );
end;

procedure TdmCatalogos.cdsClasifiGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CONT_CLASIFI, iRight );
end;

procedure TdmCatalogos.cdsClasifiNewRecord(DataSet: TDataSet);
begin
     DataSet.FieldByName( 'TB_ACTIVO').AsString:= K_GLOBAL_SI;
end;

{ cdsCalendario }

procedure TdmCatalogos.cdsCalendarioAlAdquirirDatos(Sender: TObject);
begin
     cdsCursos.Conectar;
     cdsCalendario.Data := ServerCatalogo.GetCalendario( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCalendarioNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'CU_CODIGO' ).AsString := VACIO;
          FieldByName( 'CC_FECHA' ).AsDateTime := dmCliente.FechaDefault;
     end;
end;

//DevEx (by am): Se agrega la validacion de vista para desplegar la forma correspondiente
procedure TdmCatalogos.cdsCalendarioAlModificar(Sender: TObject);
begin
//     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatCalendario_DevEx, TEditCatCalendario_DevEx );
end;

{ cdsConceptos }

procedure TdmCatalogos.cdsConceptosAlAdquirirDatos(Sender: TObject);
const
     K_CONSULTA = 'select CO_NUMERO, CO_DESCRIP, case CO_TIPO when 0 then ''Acumulado D�as/Horas'' when 1    then ''Percepci�n'' when 2    '
     +'then ''Deducci�n'' when 3    then ''Obligaci�n Empresa'' when 4    then ''Prestaci�n'' when 5    then ''C�lculo Temporal'' when 6    then ''De Captura'' when 7 '
     +'   then ''Resultados'' end , CO_SUB_CTA , CO_ACTIVO , CO_CALCULA , ''Todas'' , case when CO_SAT_CLP = 0 then '''' '
     +' when CO_SAT_CLP IN (1,2) then  ( SELECT ''(''+CAST(TB_SAT_NUM as varchar(20)) +'') '' + TB_ELEMENT  from TIPO_SAT where TB_CODIGO = C.CO_SAT_TPP)'
     +'when CO_SAT_CLP = 3 then ''Exento'' when CO_SAT_CLP = 4 then ''Otros Pagos'' else ''No Aplica'' '
     +'end  [Tipo_SAT], case when CO_SAT_CLN = 0 then '''' when CO_SAT_CLN IN (1,2) then  ( SELECT ''(''+CAST(TB_SAT_NUM as varchar(20)) +'') '' + TB_ELEMENT'
     +'  from TIPO_SAT where TB_CODIGO = C.CO_SAT_TPN) when CO_SAT_CLN = 3 then ''Exento'' when CO_SAT_CLN = 4 then ''Otros Pagos'' else ''No Aplica'''
     +' end  [Tipo_SAT_2], case CO_SAT_EXE when 0 then '''' when 1 then ''Exento 100%''  when 2 then ''Exento por Monto'' when 3 then ''Exento por Concepto'' '
     +'when 4 then ''Gravado 100%'' when 5 then ''Gravado por Monto'' when 6 then ''Gravado por Concepto'' when 7 then ''Exento + Cancelaci�n'' end [Tratamiento], '
     +'case CO_SAT_CON when 0 then '''' else ''('' +  CAST (PUENTE_NOM AS varchar(20))  + '') '' +PUENTE_DESC end AS [CO_Exento_Gravado], '
     +'CO_A_PTU,CO_ACTIVO,CO_CALCULA,CO_FORMULA,CO_G_IMSS, '
     +'CO_G_ISPT, CO_IMP_CAL, CO_IMPRIME, CO_LISTADO, CO_MENSUAL, CO_QUERY, CO_RECIBO, CO_TIPO, CO_X_ISPT, CO_SUB_CTA, CO_ISN, CO_FRM_ALT, '
     +'CO_D_EXT, CO_D_NOM, CO_D_BLOB, CO_NOTA, CO_VER_INF, CO_VER_SUP, CO_LIM_INF, CO_LIM_SUP,CO_VER_ACC, CO_GPO_ACC,CO_SUMRECI,CO_CAMBIA,'
     +'CO_USO_NOM , CO_SAT_CLP, CO_SAT_CLN, CO_SAT_CON, CO_SAT_EXE, CO_SAT_TPP, CO_SAT_TPN, CO_PS_TIPO, CO_TIMBRA from CONCEPTO '
     +'C LEFT JOIN (SELECT CO_NUMERO AS PUENTE_NOM, CO_DESCRIP AS PUENTE_DESC FROM CONCEPTO WHERE CO_NUMERO IN (SELECT DISTINCT CO_SAT_CON FROM CONCEPTO)) '
     +'AS PUENTES ON C.CO_SAT_CON = PUENTES.PUENTE_NOM ;';
begin
     dmCatalogos.cdsTiposSAT.Conectar;
     cdsConceptos.Data := dmConsultas.ServerConsultas.GetQueryGralTodos(dmCliente.Empresa, K_CONSULTA);
end;

procedure TdmCatalogos.cdsConceptosAlCrearCampos(Sender: TObject);
begin
     with cdsConceptos do
     begin
          FieldByName( 'CO_NUMERO' ).OnValidate := cdsConceptosCO_NUMEROValidate;
          ListaFija( 'CO_TIPO', lfTipoConcepto );
          FieldByName( 'CO_USO_NOM' ).OnGetText := CO_USO_NOMGetText;

          ListaFija( 'CO_SAT_CLP', lfClaseConceptosSAT) ;
          ListaFija( 'CO_SAT_CLN', lfClaseConceptosSAT) ;
          ListaFija( 'CO_SAT_EXE', lfTipoExentoSAT ) ;

          CreateSimpleLookup( dmCatalogos.cdsTiposSAT, 'POS_NAME', 'CO_SAT_TPP' );
          CreateSimpleLookup( dmCatalogos.cdsTiposSAT, 'NEG_NAME', 'CO_SAT_TPN' );

          FieldByName( 'CO_SAT_CLP' ).OnChange := OnCO_SAT_CLPChange;
          FieldByName( 'CO_SAT_CLN' ).OnChange := OnCO_SAT_CLNChange;
          FieldByName( 'CO_SAT_EXE' ).OnChange := OnCO_SAT_EXEChange;
          FieldByName( 'CO_TIMBRA' ).OnChange := OnCO_TIMBRAChange;
     end;
end;

procedure TdmCatalogos.cdsConceptosCO_NUMEROValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 999 ) then
             DataBaseError( 'Los Conceptos Mayores a 1000 son para uso Exclusivo del Sistema.'+ CR_LF +
                            'Indicar un N�mero de Concepto Menor o Igual a 999');
     end;
end;

procedure TdmCatalogos.CO_USO_NOMGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
        Text := ObtieneElemento( lfUsoConceptoNomina, Sender.AsInteger );
end;

procedure TdmCatalogos.cdsConceptosAfterPost(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsConceptos do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile ( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enAcumula, enMovGral ] );
                    cdsConceptosLookUp.Refrescar;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsConceptosBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          {*** US 11116: Validacion para no Permitir guardar un concepto de nomina igual a 0 CO_NUMERO <= 0 ***}
          if ( State = dsInsert ) and ( FieldByName('CO_NUMERO').AsInteger <= 0 ) then
             DataBaseError( 'El n�mero de concepto debe ser mayor que cero' );
          if ( State in [dsEdit] ) then
          begin
               if StrVacio( FieldByName('CO_NOTA').AsString ) then
                  FieldByName('CO_NOTA').AsString := K_ESPACIO;
               FieldByName('CO_D_NOM').AsString := ExtractFileName(FieldByName('CO_D_NOM').AsString);
          end;

          if ( ( zStrToBool( FieldByName('CO_VER_INF').AsString) and zStrToBool(FieldByName('CO_VER_SUP').AsString )  ) and( FieldByName('CO_LIM_SUP').AsFloat < FieldByName('CO_LIM_INF').AsFloat) ) then
          begin
               DataBaseError('El L�mite Inferior no puede ser mayor al L�mite Superior');
          end;

          if (  (FieldByName('CO_SAT_CLP').AsInteger in [1,2] ) and StrVacio(FieldByName('CO_SAT_TPP').AsString) )  then
          begin
               DataBaseError( 'Tipo de Percepci�n/Deducci�n en Montos Positivos no puede quedar vac�o') ;
          end;

          if ( (FieldByName('CO_SAT_CLN').AsInteger in [1,2] ) and StrVacio(FieldByName('CO_SAT_TPN').AsString) )  then
          begin
               DataBaseError( 'Tipo de Percepci�n/Deducci�n en Montos Negativos no puede quedar vac�o') ;
          end;

          if ( (FieldByName('CO_SAT_EXE').AsInteger in [3,6] ) and ( FieldByName('CO_SAT_CON').AsInteger = 0  )  )  then
          begin
               DataBaseError( 'Concepto para Gravado/Exento debe estar capturado') ;
          end;

//          if ( (FieldByName('CO_TIPO').AsInteger in [1,4] ) and (( FieldByName('CO_SAT_CLN').AsInteger = 0  ) and (FieldByName('CO_SAT_CLP').AsInteger = 0 ) ) )  then
//          begin
//               FieldByName('CO_SAT_CLP').FocusControl;
//               ZWarning( 'Conceptos de N�mina','El concepto no tiene configuraci�n de timbrado, verifique la clave SAT y tratamiento de exento que le corresponde.' , 0, mbOK);
//               //DataBaseError( 'El concepto no tiene configuraci�n de timbrado, verifique la clave SAT y tratamiento de exento que le corresponde.') ;
//          end;
     end;
end;

procedure TdmCatalogos.cdsConceptosBeforeDelete(DataSet: TDataSet);
begin
     if cdsConceptos.FieldByName('CO_NUMERO').AsInteger > 999 then
        raise EDataBaseError.Create( 'Los Conceptos Mayores a 1000 son para uso Exclusivo del Sistema.'+ CR_LF +
                                     'Indicar un N�mero de Concepto Menor o Igual a 999');
end;

procedure TdmCatalogos.cdsConceptosBeforeInsert(DataSet: TDataSet);
begin
     with cdsConceptos do
     begin
          DisableControls;
          IndexFieldNames := 'CO_NUMERO';
          SetRange( [ 1 ], [ 999 ] );
          Last;
          FUltimoConcepto := FieldByName( 'CO_NUMERO' ).AsInteger;
          CancelRange;
          EnableControls;
     end;
     cdsConceptosLookUp.Data := cdsConceptos.Data; // si se hace despu�s, regresa el Dataset a dsBrowse
end;

procedure TdmCatalogos.cdsConceptosNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
{$ifdef ANTES}
          FieldByName( 'CO_NUMERO' ).AsInteger := FUltimoConcepto + 1;
{$else}
          if ( FUltimoConcepto < 999 ) then  { Si es mayor o igual a 999 no sugiere valor }
             FieldByName( 'CO_NUMERO' ).AsInteger := FUltimoConcepto + 1;
{$endif}
          FieldByName( 'CO_TIPO' ).AsInteger := Ord( coPercepcion );
          FieldByName( 'CO_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CO_CALCULA' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CO_A_PTU' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CO_MENSUAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CO_G_IMSS' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CO_G_ISPT' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CO_IMPRIME' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CO_LISTADO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CO_LIM_SUP' ).AsFloat := 0;
          FieldByName( 'CO_LIM_INF' ).AsFloat := 0;
          FieldByName( 'CO_VER_SUP' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CO_VER_INF' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CO_VER_ACC' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CO_GPO_ACC' ).AsString := VACIO;
          FieldByName( 'CO_ISN' ).AsString := K_GLOBAL_NO;
{$ifdef QUINCENALES}
          FieldByName( 'CO_CAMBIA' ).AsString := K_GLOBAL_NO;
{$endif}
          FieldByName( 'CO_SUMRECI' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CO_SAT_CLP' ).AsInteger := 0;
          FieldByName( 'CO_SAT_CLN' ).AsInteger := 0;
          FieldByName( 'CO_SAT_EXE' ).AsInteger := 0;
          FieldByName( 'CO_SAT_TPP' ).AsString := VACIO;
          FieldByName( 'CO_SAT_TPN' ).AsString := VACIO;
          FieldByName( 'CO_SAT_CON' ).AsString := VACIO;
          FieldByName( 'CO_TIMBRA' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmCatalogos.cdsConceptosAlModificar(Sender: TObject);
begin
    ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatConceptos_DevEx, TEditCatConceptos_DevEx );
end;

procedure TdmCatalogos.cdsConceptosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_NOMINA_CONCEPTOS, iRight );
end;


procedure TdmCatalogos.OnCO_SAT_CLNChange(Sender: TField);
begin
     with cdsConceptos do
     begin
          FieldByName('CO_SAT_TPN').AsString := VACIO;
          FieldByName('CO_SAT_EXE').AsInteger := 0;
          FieldByName('CO_SAT_CON').AsInteger := 0;
     end;
end;

procedure TdmCatalogos.OnCO_SAT_CLPChange(Sender: TField);
begin
     with cdsConceptos do
     begin
          FieldByName('CO_SAT_TPP').AsString := VACIO;
          FieldByName('CO_SAT_EXE').AsInteger := 0;
          FieldByName('CO_SAT_CON').AsInteger := 0;
     end;
end;

procedure TdmCatalogos.OnCO_SAT_EXEChange(Sender: TField);
begin
     with cdsConceptos do
     begin
          if  not(  FieldByName('CO_SAT_EXE').AsInteger  in [3,6 ] ) then
          begin
               FieldByName('CO_SAT_CON').AsInteger := 0;
          end;
     end;
end;

procedure TdmCatalogos.OnCO_TIMBRAChange(Sender: TField);
begin
    with cdsConceptos do
     begin
          if not zStrToBool( FieldByName('CO_TIMBRA').AsString )   then
          begin
                FieldByName('CO_SAT_TPN').AsString := VACIO;
                FieldByName('CO_SAT_TPP').AsString := VACIO;
                FieldByName('CO_SAT_CLP' ).AsInteger := 0;
                FieldByName('CO_SAT_CLN' ).AsInteger := 0;
                FieldByName('CO_SAT_EXE').AsInteger := 0;
                FieldByName('CO_SAT_CON').AsInteger := 0;
          end;
     end;
end;


procedure TdmCatalogos.AbreDocumento;
begin
     ZetaFilesTools.AbreDocumento( cdsConceptos, 'CO_D_BLOB', 'CO_D_EXT' );
end;

procedure TdmCatalogos.BorraDocumento;
begin
     with cdsConceptos do
     begin
          if State = dsBrowse then
             Edit;

          FieldByName('CO_D_NOM').AsString := VACIO;
          FieldByName('CO_D_EXT').AsString := VACIO;
          FieldByName('CO_D_BLOB').AsString := VACIO;
     end;
end;

function TdmCatalogos.CargaDocumento( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
begin
     Result := StrLleno( sObservaciones ) ;
     if Result then
     begin
        with cdsConceptos do
        begin
             if State = dsBrowse then
                Edit;

             if lEncimarArchivo then
                Result := ZetaFilesTools.CargaDocumento( cdsConceptos,
                                                         sPath,
                                                         'CO_D_BLOB',
                                                         'CO_D_EXT' );
             if Result then
                FieldByName('CO_D_NOM').AsString := sObservaciones;
        end
     end
     else
         ZetaDialogo.ZError( 'Error en el Documento...', 'El nombre no puede quedar vac�o', 0 );

end;

{ cdsConceptosLookUp }

procedure TdmCatalogos.cdsConceptosLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsConceptos do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque la transferencia
          begin                          // de Data regresa el cds a dsBrowse
               Conectar;
               cdsConceptosLookUp.Data := Data;
          end;
     end;
end;

procedure TdmCatalogos.cdsConceptosLookUpLookupSearch(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: string; var sKey,
  sDescription: string);
const
     K_FILTRO_CONCEPTO_PUENTE = '( CO_TIPO = 3 ) and ( CO_SAT_CLP = 3 )';
var
    sFiltro: string;
begin
         sFiltro := VACIO;
         if (FEditCatConceptos_DevEx.ComboValue = 3) then
         begin
             with dmCatalogos.cdsConceptosLookUp do
             begin
                  Filtered := False;
                  Filter := K_FILTRO_CONCEPTO_PUENTE;
                  Filtered := True;
             end;
             sFiltro := K_FILTRO_CONCEPTO_PUENTE;
         end
         else
         begin
             with dmCatalogos.cdsConceptosLookUp do
             begin
                  Filtered := False;
                  Filter := '';
                  Filtered := True;
             end;
             sFiltro := VACIO;

         end;

            lOk := FBusquedaConceptos_Version_DevEx.BusquedaConceptoDevex_ShowSearchForm( Sender, sFiltro, sKey, sDescription)
end;

{ cdsCondiciones }
procedure TdmCatalogos.InitActivosNavegacion;
begin
     cdsCondiciones.conectar;
     FNavegacion := VACIO;
end;

function TdmCatalogos.LlenaNavegacion( Lista: TStrings; sCodigo: String ): Integer;
var
   sOldIndex: string;
   oTexto : TStringObject;   
begin
     Result := 0;
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Insert( 0, 'Todos' );
          with cdsCondiciones do
          begin
               DisableControls;
               try
               sOldIndex := IndexFieldNames;
               Filtered := False;
               Filter := 'QU_NAVEGA = ''S'' ';
               Filtered := True;
               IndexFieldNames := 'QU_ORDEN;QU_DESCRIP';
               
               while not Eof do
               begin
                    oTexto := TStringObject.Create;
                    oTexto.Texto := FieldByName( 'QU_CODIGO' ).AsString;
                    AddObject( FieldByName( 'QU_DESCRIP' ).AsString,  TObject(oTexto) );
                    if oTexto.Texto = sCodigo then
                      Result:= Lista.Count-1;
                    Next;
               end;
               finally
                      Filtered := False;
                      Filter := VACIO;
                      IndexFieldNames:= sOldIndex;
                      EnableControls;
               end;
          end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmCatalogos.SetNavegacion(const Value: String);
begin
     if ( FNavegacion <> Value ) then
     begin
          cdsCondiciones.Locate( 'QU_CODIGO', Value, [] );
          FNavegacion := Value;
     end;
end;

function TdmCatalogos.GetNavegacionDescripcion: String;
begin
     Result := 'Navegaci�n: ' + cdsCondiciones.FieldByName( 'QU_DESCRIP' ).AsString;
end;

procedure TdmCatalogos.AbreFormaEdicionCondicion;
begin
     FFormaCatCondiciones := TRUE;
     try
        with dmCatalogos.cdsCondiciones do
        begin
             Append;
             Modificar;
        end;
     finally
            FFormaCatCondiciones := FALSE;     
     end;
end;

procedure TdmCatalogos.cdsCondicionesAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     FHayErrorCount := 0;
     with cdsCondiciones do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
          end;
          if ( Tag = 6 ) and ( ErrorCount = 0 ) then
          begin
               if FFormaCatCondiciones then
                  FNavegacion := cdsCondiciones.FieldByName('QU_CODIGO').AsString;
               TressShell.SetDataChange( [ enQuerys ] );
          end
          else
              FHayErrorCount := ErrorCount;
     end;
end;

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCondicionesNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'QU_NIVEL' ).AsInteger := Ord( nuPrioridadMinima );
          FieldByName( 'QU_NAVEGA').AsString := zBoolToStr( FFormaCatCondiciones );
          FieldByName( 'QU_ORDEN').AsInteger := 0;
          FieldByName( 'QU_CANDADO').AsString := K_GLOBAL_NO;          
     end;
end;

procedure TdmCatalogos.cdsCondicionesBeforeEdit(DataSet: TDataSet);
begin
     with DataSet, FieldByName( 'QU_NIVEL' ) do
     begin
          if ( NOT ( dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION ) ) and
             ( zStrToBool( FieldByName('QU_CANDADO').AsString )) and
             (FieldByName('US_CODIGO').AsInteger <> dmCliente.Usuario ) then
               DataBaseError( 'Usuario No Puede Modificar/Borrar Esta Condici�n' );

          if ( AsInteger > 0 ) and ( AsInteger < Ord( dmCliente.GetDatosUsuarioActivo.Nivel ) ) then
          begin
               DataBaseError( 'El Nivel Que Tiene Como Usuario' + CR_LF + 'No Le Permite Modificar/Borrar Esta Condici�n !!!' );
          end;
     end;
end;

procedure TdmCatalogos.cdsCondicionesBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'QU_CODIGO' );
     DataSet.FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmCatalogos.cdsCondicionesAlModificar(Sender: TObject);
begin
         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatCondiciones_DevEx, TEditCatCondiciones_DevEx );
end;

procedure TdmCatalogos.cdsCondicionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_GRALES_CONDICIONES, iRight );
end;

{ cdsContratos }

procedure TdmCatalogos.cdsContratosAlAdquirirDatos(Sender: TObject);
begin
     cdsContratos.Data := ServerCatalogo.GetContratos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsContratosNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'TB_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'TB_DIAS' ).AsInteger := 0;
     end;
end;

procedure TdmCatalogos.cdsContratosBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'TB_CODIGO' );
end;

//DevEx (by am): Se agrega validacion para mostrar la forma correspondiente al a vista
procedure TdmCatalogos.cdsContratosAlModificar(Sender: TObject);
begin
//        ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatContratos_DevEx, TEditCatContratos_DevEx );
end;

procedure TdmCatalogos.cdsContratosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CONT_CONTRATO, iRight );
end;

{ cdsTurnos }

procedure TdmCatalogos.cdsTurnosAlAdquirirDatos(Sender: TObject);
begin
     cdsTurnos.Data := ServerCatalogo.GetTurnos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsTurnosAfterOpen(DataSet: TDataSet);
begin
     with cdsTurnos do
     begin
          MaskPesos( 'TU_JORNADA' );
          FieldByName( 'TU_RIT_PAT' ).OnChange := TU_RIT_PATChange;
          FieldByName( 'TU_DIAS' ).OnChange := RevisaDiasVacaciones;
     end;
end;

procedure TdmCatalogos.RevisaDiasVacaciones(Sender: TField);
begin
     if Assigned( Sender ) then
     begin
          with Sender.Dataset do
          begin
               if ( FieldByName( 'TU_DIAS' ).AsFloat = K_DIAS_SEMANA_HABILES ) then
                   FieldByName( 'TU_VACA_HA' ).AsFloat := K_DIAS_VACACION_HABIL
               else
                   FieldByName( 'TU_VACA_HA' ).AsFloat := K_DIAS_VACACION_DEF;
          end;
     end;
end;

procedure TdmCatalogos.TU_RIT_PATChange(Sender: TField);
begin
     with Sender do
     begin
          if ZetaCommonTools.StrLleno( AsString ) then
          begin
               with Dataset.FieldByName( 'TU_RIT_INI' ) do
               begin
                    if ( AsDateTime = NullDateTime ) then
                       AsDateTime := Date;
               end;
          end
          else
          begin
               with Dataset.FieldByName( 'TU_RIT_INI' ) do
               begin
                    if ( AsDateTime <> NullDateTime ) then
                       AsDateTime := NullDateTime;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsTurnosNewRecord(DataSet: TDataSet);
begin
     with cdsTurnos do
     begin
          FieldByName( 'TU_DIAS' ).AsInteger := K_DIAS_SEMANA_HABILES;
          FieldByName( 'TU_JORNADA' ).AsInteger := 48;
          FieldByName( 'TU_DOBLES' ).AsInteger := 9;
          FieldByName( 'TU_HORARIO' ).AsInteger := Ord( ttNormal );
          FieldByName( 'TU_TIP_JOR' ).AsInteger := Ord( tjSemanaCompleta );
          //FieldByName( 'TU_NOMINA' ).AsInteger := Ord( tpSemanal );
          FieldByName( 'TU_TIP_1' ).AsInteger := Ord( auHabil );
          FieldByName( 'TU_TIP_2' ).AsInteger := FieldByName( 'TU_TIP_1' ).AsInteger;
          FieldByName( 'TU_TIP_3' ).AsInteger := FieldByName( 'TU_TIP_1' ).AsInteger;
          FieldByName( 'TU_TIP_4' ).AsInteger := FieldByName( 'TU_TIP_1' ).AsInteger;
          FieldByName( 'TU_TIP_5' ).AsInteger := FieldByName( 'TU_TIP_1' ).AsInteger;
          FieldByName( 'TU_TIP_6' ).AsInteger := Ord( auSabado );
          FieldByName( 'TU_TIP_7' ).AsInteger := Ord( auDescanso );
          FieldByName( 'TU_DOMINGO' ).AsFloat := 0;
          FieldByName( 'TU_FESTIVO' ).AsString := K_GLOBAL_NO;
          FieldByName( 'TU_VACA_HA' ).AsFloat := K_DIAS_VACACION_HABIL;
          FieldByName( 'TU_VACA_SA' ).AsFloat := 0;
          FieldByName( 'TU_VACA_DE' ).AsFloat := 0;
          FieldByName( 'TU_ACTIVO' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmCatalogos.cdsTurnosBeforePost(DataSet: TDataSet);
const
     K_MENSAJE_VACACIONES = 'Las Vacaciones Proporcionales A Los %s No Pueden Ser Negativas';
var
   sTipoDias: String;
begin
     cdsBeforePost(Dataset, 'TU_CODIGO' );
     sTipoDias := VACIO;
     with cdsTurnos do
     begin
          if ( FieldByName('TU_VACA_HA').AsFloat < 0 ) then
              sTipoDias := 'D�as H�biles';
          if ( FieldByName('TU_VACA_SA').AsFloat < 0 ) then
              sTipoDias := 'S�bados';
          if ( FieldByName('TU_VACA_DE').AsFloat < 0 ) then
              sTipoDias := 'Descansos/Festivos';
          if strLleno( sTipoDias ) then
             DatabaseError( Format( K_MENSAJE_VACACIONES, [sTipoDias] ) );
     end;
end;

procedure TdmCatalogos.cdsTurnosAlModificar(Sender: TObject);
begin
//        ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatTurnos_DevEx, TEditCatTurnos_DevEx );
end;

procedure TdmCatalogos.cdsTurnosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsTurnos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enFestivo ] );
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsTurnosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CONT_TURNOS, iRight );
end;

{ cdsPeriodo }

procedure TdmCatalogos.cdsPeriodoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          GetDatosPeriodo( YearDefault, PeriodoTipo );
     end;
end;

procedure TdmCatalogos.cdsPeriodoAlCrearCampos(Sender: TObject);
begin
     with cdsPeriodo do
     begin
          MaskFecha('PE_FEC_INI');
          MaskFecha('PE_FEC_FIN');
          MaskFecha('PE_FEC_PAG');
          FieldByName( 'PE_NUMERO' ).OnChange := PE_NUMEROChange;
          FieldByName( 'PE_USO' ).OnChange := PE_USOChange;
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
          FieldByName( 'PE_STATUS' ).OnGetText := PE_STATUSGetText;

          ListaFija( 'PE_TIPO', lfTipoPeriodo );
          ListaFija( 'PE_USO', lfUsoPeriodo );
{$ifdef QUINCENALES}
          FieldByName( 'PE_FEC_INI' ).OnChange := PE_FEC_INIChange;
          FieldByName( 'PE_FEC_FIN' ).OnChange := PE_FEC_FINChange;
{$endif}
     end;
end;

procedure TdmCatalogos.GetDatosPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo );
begin
     with cdsPeriodo do
     begin
          Data := ServerCatalogo.GetPeriodos( dmCliente.Empresa, iYear, Ord(TipoPeriodo) );
          ResetDataChange;
     end;
end;

procedure  TdmCatalogos.AplicaFiltroStatusPeriodo(DBLookup: TZetaKeyLookup;Aplicar:Boolean);
var
   Derecho:Boolean;
begin
     if DBLookup.Name = 'VP_NOMNUME' then
        Derecho := not ZAccesosMgr.CheckDerecho( D_CONS_PLANVACACION, K_DERECHO_NIVEL0 )
     else
         Derecho := not ZAccesosMgr.CheckDerecho( D_EMP_EXP_VACA, K_DERECHO_SIST_KARDEX );

     if ( Derecho )then
     begin
          if Aplicar then
          begin
               DBLookup.Filtro := Format('PE_STATUS < %d',[Ord(spAfectadaParcial)]);
               DBLookup.ResetMemory;
               with cdsPeriodo do
               begin
                    Filtered := False;
                    Filter := DBLookup.Filtro;
                    Filtered := True;
               end;
          end
          else
          begin
               DBLookup.Filtro := VACIO;
               with cdsPeriodo do
               begin
                    Filtered := False;
               end;
          end;
     end;
end;

procedure  TdmCatalogos.AplicaFiltroStatusPeriodo(DBLookup: TZetaKeyLookup_DevEx;Aplicar:Boolean);
var
   Derecho:Boolean;
begin
     if DBLookup.Name = 'VP_NOMNUME' then
        Derecho := not ZAccesosMgr.CheckDerecho( D_CONS_PLANVACACION, K_DERECHO_NIVEL0 )
     else
         Derecho := not ZAccesosMgr.CheckDerecho( D_EMP_EXP_VACA, K_DERECHO_SIST_KARDEX );

     if ( Derecho )then
     begin
          if Aplicar then
          begin
               DBLookup.Filtro := Format('PE_STATUS < %d',[Ord(spAfectadaParcial)]);
               DBLookup.ResetMemory;
               with cdsPeriodo do
               begin
                    Filtered := False;
                    Filter := DBLookup.Filtro;
                    Filtered := True;
               end;
          end
          else
          begin
               DBLookup.Filtro := VACIO;
               with cdsPeriodo do
               begin
                    Filtered := False;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsPeriodoNewRecord(DataSet: TDataSet);
begin
     with cdsPeriodo do
     begin
          FieldByName( 'PE_NUMERO' ).AsInteger :=  FUltimoPeriodo + 1;
          FieldByName( 'PE_FEC_MOD' ).AsDateTime := NOW();
          with dmCliente do
          begin
               FieldByName( 'PE_FEC_INI' ).AsDateTime := FechaDefault;
               FieldByName( 'PE_FEC_FIN' ).AsDateTime := FechaDefault;
               FieldByName( 'PE_FEC_PAG' ).AsDateTime := FechaDefault;
               {$ifdef QUINCENALES}
               FieldByName( 'PE_ASI_INI' ).AsDateTime := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               FieldByName( 'PE_ASI_FIN' ).AsDateTime := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               {$endif}
               FieldByName( 'PE_YEAR' ).AsInteger := YearDefault;
               if FFormaCatPeriodos then
                  FieldByName( 'PE_TIPO' ).AsInteger := Ord( FFormaTipoPeriodo )     // Cuando est� en la forma de consulta
               else
                  FieldByName( 'PE_TIPO' ).AsInteger := Ord( PeriodoTipo );
               FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
          end;
          FieldByName( 'PE_DIAS' ).AsInteger := 1;
          FieldByName( 'PE_DIAS_AC' ).AsInteger := 0;
          FieldByName( 'PE_DIA_MES' ).AsInteger := 0;
          FieldByName( 'PE_POS_MES' ).AsInteger := 0;
          FieldByName( 'PE_PER_MES' ).AsInteger := 0;
          FieldByName( 'PE_PER_TOT' ).AsInteger := 0;
          FieldByName( 'PE_STATUS' ).AsInteger := 0;
          case eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger ) of
               upOrdinaria: FieldByName( 'PE_MES' ).AsInteger := 1;
               upEspecial: FieldByName( 'PE_MES' ).Asinteger := 13;
          end;
          FieldByName( 'PE_CAL' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsPeriodoBeforePost(DataSet: TDataSet);
begin
     //cdsBeforePost( Dataset, 'PE_NUMERO' );
     with cdsPeriodo do
     begin
          if ( FieldByName( 'PE_NUMERO').AsInteger <= 0 ) then
             DataBaseError( 'N�mero de Per�odo debe ser Mayor a Cero');
          if ( FieldByName( 'PE_NUMERO').AsInteger > 999 ) then
             DataBaseError( 'El N�mero M�ximo Para Un Per�odo Es 999' );
          if ( FieldByName( 'PE_STATUS').AsInteger > Ord( spCalculadaTotal ) ) then
             DataBaseError( 'N�mina no puede ser modificada: STATUS =' +
                            ObtieneElemento( lfStatusPeriodo, FieldByName( 'PE_STATUS').AsInteger ) );
          FieldByName( 'PE_FEC_MOD' ).AsDateTime := Now();
          if State in [ dsInsert ] then
             FNuevoPeriodo := FieldByName( 'PE_NUMERO' ).AsInteger
          else
             FNuevoPeriodo := 0;
     end;
end;

procedure TdmCatalogos.cdsPeriodoBeforeDelete(DataSet: TDataSet);
begin
     FNuevoPeriodo := 0;
end;

procedure TdmCatalogos.cdsPeriodoBeforeInsert(DataSet: TDataSet);
begin
     // Para usar en el caso de la creaci�n anual de per�odos
     with cdsPeriodo do
     begin
          DisableControls;
          Last;
          FUltimoPeriodo := FieldByName( 'PE_NUMERO' ).AsInteger ;
          EnableControls;
     end;
end;

procedure TdmCatalogos.cdsPeriodoAlModificar(Sender: TObject);
begin
//     FRefrescaPeriodo := FALSE;
//     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatPeriodos_DevEx, TEditCatPeriodos_DevEx );
//     if FRefrescaPeriodo then
//        TressShell.SetDataChange( [ enPeriodo, enNomina, enMovGral, enFaltas, enMovimien, enPolHead ] );
end;

procedure TdmCatalogos.cdsPeriodoAlBorrar(Sender: TObject);
var
   oCursor: TCursor;
   iNumero: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        iNumero := dmCatalogos.cdsPeriodo.FieldByName( 'PE_NUMERO' ).AsInteger;
        dmNomina.BorrarNominaActiva( False );
        cdsPeriodo.Locate( 'PE_NUMERO', iNumero, [] );  //Si no se borro intenta reposicionarlo
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmCatalogos.cdsPeriodoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPeriodo do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if ( Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) ) then
               begin
                    if ( FNuevoPeriodo > 0 ) and ( eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger ) = dmCliente.PeriodoTipo ) then
                       dmCliente.SetPeriodoNumero( FNuevoPeriodo );
                    FRefrescaPeriodo := TRUE;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsPeriodoLookupDescription( Sender: TZetaLookupDataSet; var sDescription: String);
begin
     with Sender do
     begin
          sDescription := 'De ' + FechaCorta( FieldByName( 'PE_FEC_INI' ).AsDateTime ) +
                          ' a ' + FechaCorta( FieldByName( 'PE_FEC_FIN' ).AsDateTime );
     end;
end;

procedure TdmCatalogos.cdsPeriodoLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     with cdsPeriodo do
     begin
          Filtered := False;
          Filter := sFilter;
          Filtered := True;
          lOk := Locate( 'PE_NUMERO', StrToIntDef( sKey, 0 ), [] );
          if lOk then
          begin
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmCatalogos.cdsPeriodoLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
         lOk := ZetaBuscaPeriodo_DevEx.BuscaPeriodoDialogo( Sender, sFilter, sKey, sDescription );
end;

procedure TdmCatalogos.cdsPeriodoAfterEdit(DataSet: TDataSet);
begin
     with cdsPeriodo do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmCatalogos.AgregaPeriodo;
begin
     with cdsPeriodo do
     begin
          if not Active then
             Conectar
          else if ( ( FieldByName( 'PE_TIPO' ).AsInteger <> Ord( dmCliente.PeriodoTipo ) ) or
                    ( FieldByName( 'PE_YEAR' ).AsInteger <> dmCliente.YearDefault ) ) then
             Refrescar;

          Append;
          FieldByName( 'PE_TIPO' ).AsInteger := Ord( dmCliente.PeriodoTipo );

          Modificar;

          if FFormaCatPeriodos and ( dmCliente.PeriodoTipo <> FFormaTipoPeriodo ) then
             GetDatosPeriodo( dmCliente.YearDefault, FFormaTipoPeriodo );
     end;
end;

procedure TdmCatalogos.PE_USOChange(Sender: TField);
begin
     with cdsPeriodo do
     begin
         case eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger ) of
              upOrdinaria:
              begin
                   FieldByName( 'PE_AHORRO' ).AsString := K_GLOBAL_SI;
                   FieldByName( 'PE_PRESTAM' ).AsString := FieldByName( 'PE_AHORRO' ).AsSTring;
                   FieldByName( 'PE_MES' ).AsInteger := 1;
              end;
              upEspecial:
              begin
                   FieldByName( 'PE_AHORRO' ).AsString := K_GLOBAL_NO;
                   FieldByName( 'PE_PRESTAM' ).AsString := FieldByName( 'PE_AHORRO' ).AsSTring;
                   FieldByName( 'PE_MES' ).AsInteger := 13;
              end;
         end;
     end;
end;

procedure TdmCatalogos.PE_NUMEROChange(Sender: TField);
begin
     with cdsPeriodo do
     begin
          if ( FieldByName( 'PE_NUMERO').AsInteger < Global.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ) then
          begin
               FieldByName( 'PE_USO' ).AsInteger := Ord( upOrdinaria );
               FieldByName( 'PE_SOLO_EX' ).AsString := K_GLOBAL_NO;
               FieldByName( 'PE_INC_BAJ' ).AsString := K_GLOBAL_NO;
          end
          else
          begin
               FieldByName( 'PE_USO' ).AsInteger := Ord( upEspecial );
               FieldByName( 'PE_SOLO_EX' ).AsString := K_GLOBAL_SI;
               FieldByName( 'PE_INC_BAJ' ).AsString := K_GLOBAL_SI;
               FieldByName( 'PE_CAL' ).AsString := K_GLOBAL_NO;
          end;
     end;
end;

{$ifdef QUINCENALES}
procedure TdmCatalogos.PE_FEC_INIChange(Sender: TField);
begin
     cdsPeriodo.FieldByName( 'PE_ASI_INI' ).AsDateTime := Sender.AsDateTime;
end;

procedure TdmCatalogos.PE_FEC_FINChange(Sender: TField);
begin
     cdsPeriodo.FieldByName( 'PE_ASI_FIN' ).AsDateTime := Sender.AsDateTime;
end;
{$endif}

procedure TdmCatalogos.cdsPeriodoGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_NOMINA_PERIODOS, iRight );
end;

function TdmCatalogos.BuscaPeriodo( const iYear, iNumero: Integer; const eTipo: eTipoPeriodo; var eStatus: eStatusPeriodo ): Boolean;
var
   dFechaInicial, dFechaFinal: TDate;
begin
     Result:= BuscaPeriodo( iYear, iNumero, eTipo, eStatus, dFechaInicial, dFechaFinal );
end;

function TdmCatalogos.BuscaPeriodo(const iYear, iNumero: Integer; const eTipo: eTipoPeriodo; var eStatus: eStatusPeriodo; var dFechaInicial, dFechaFinal: TDate ): Boolean;

    function BuscaUnPeriodo( Dataset: TDataset; const iYear, iNumero: Integer; const eTipo: eTipoPeriodo; var eStatus: eStatusPeriodo ): Boolean;
    begin
         with Dataset do
         begin
              Result := Locate( 'PE_YEAR;PE_TIPO;PE_NUMERO', VarArrayOf( [ iYear, Ord( eTipo ), iNumero ] ), [] );
              if Result then
              begin
                   eStatus := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
                   dFechaInicial:= FieldByName('PE_FEC_INI').AsDateTime;
                   dFechaFinal:=  FieldByName('PE_FEC_FIN').AsDateTime;
              end;
         end;
    end;
begin
     with cdsPeriodo do
     begin
          //if not Active then
          Conectar;
          if ( FieldByName( 'PE_YEAR' ).AsInteger = iYear ) and ( eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger ) = eTipo ) then
             Result := BuscaUnPeriodo( cdsPeriodo, iYear, iNumero, eTipo, eStatus )
          else
          begin
               with cdsTemp do
               begin
                    if ( State in [dsInactive] ) or ( FieldByName( 'PE_YEAR' ).AsInteger <> iYear ) or ( eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger ) <> eTipo )  then
                       Data := ServerCatalogo.GetPeriodos( dmCliente.Empresa, iYear, Ord( eTipo ) );
               end;
               Result := BuscaUnPeriodo( cdsTemp, iYear, iNumero, eTipo, eStatus );
          end;
     end;
end;

{ cdsInvitadores }

procedure TdmCatalogos.cdsInvitadoresAlAdquirirDatos(Sender: TObject);
begin
     cdsInvitadores.Data := ServerCatalogo.GetInvitadores( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsInvitadoresAlCrearCampos(Sender: TObject);
begin
     with cdsInvitadores do
     begin
          MaskFecha( 'IV_FEC_INI' );
          MaskFecha( 'IV_FEC_FIN' );
     end;
end;

procedure TdmCatalogos.cdsInvitadoresBeforeInsert(DataSet: TDataSet);
begin
     with cdsInvitadores do
     begin
          DisableControls;
          IndexFieldNames := 'IV_CODIGO';
          Last;
          FUltimoInvitador := FieldByName( 'IV_CODIGO' ).AsInteger;
          EnableControls;
     end;
end;

procedure TdmCatalogos.cdsInvitadoresNewRecord(DataSet: TDataSet);
begin
     with cdsInvitadores, dmCliente do
     begin
          FieldByName( 'IV_CODIGO' ).AsInteger := FUltimoInvitador + 1;
          FieldByName( 'IV_ACTIVO' ).AsString := K_GLOBAL_SI;
//          FieldByName( 'IV_FEC_FIN' ).AsDateTime := FechaDefault;
//          FieldByName( 'IV_FEC_INI' ).AsDateTime := FechaDefault
     end;
end;

procedure TdmCatalogos.cdsInvitadoresBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'IV_CODIGO' );
     {$ifndef DOS_CAPAS}
     //BIOMETRICO
     with cdsInvitadores do
     begin
          if ( ( FieldByName( 'IV_ID_BIO' ).AsInteger <> 0 ) and StrVacio( FieldByName( 'IV_ID_GPO' ).AsString ) ) then
          begin
               FieldByName( 'IV_ID_GPO' ).FocusControl;
               DataBaseError( 'Grupo de Dispositivos es requerido' );
          end;
     end;
     {$endif}
end;

procedure TdmCatalogos.cdsInvitadoresAlModificar(Sender: TObject);
begin
     //ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatInvitadores_DevEx, TEditCatInvitadores_DevEx ); //DevEx (icm)
end;

procedure TdmCatalogos.cdsInvitadoresAlEnviarDatos(Sender: TObject);
var
   ErrorCount{$ifndef DOS_CAPAS}, idBio{$endif} : Integer;
   {$ifndef DOS_CAPAS}lCambioIDBio : Boolean; {$endif}
begin
     ErrorCount := 0;
     with cdsInvitadores do
     begin
{$ifndef DOS_CAPAS}
          // BIOMETRICO
          lCambioIDBio := ( State in [ dsInsert, dsEdit ] );

          if not lCambioIDBio then //No esta insertando... > Edit o Browse
          begin
               lCambioIDBio := ( ( ZetaServerTools.CambiaCampo( FieldByName( 'IV_CODIGO' ) ) or
                                   ZetaServerTools.CambiaCampo( FieldByName( 'IV_ID_BIO' ) ) or
                                   ZetaServerTools.CambiaCampo( FieldByName( 'IV_ID_GPO' ) ) or
                                   ZetaServerTools.CambiaCampo( FieldByName( 'CB_CODIGO' ) ) ) );
          end;

          if( lCambioIDBio )then //y no esta insertando...
          begin
               if ( FieldByName( 'IV_ID_BIO' ).AsInteger = 0 ) and ( FieldByName( 'IV_ID_GPO' ).AsString = VACIO ) then
               begin
                    dmSistema.ActualizaIdBio( 0, FieldByName( 'IV_ID_BIO' ).AsInteger, VACIO, FieldByName( 'IV_CODIGO' ).AsInteger );
                    dmSistema.EliminaHuella( 0, FieldByName( 'IV_CODIGO' ).AsInteger, K_TERMINAL_HUELLA );
               end
               else
               begin
                    lCambioIDBio := ZetaServerTools.CambiaCampo( FieldByName( 'IV_ID_BIO' ) );

                    if lCambioIDBio then
                       idBio := dmSistema.ObtenMaxIdBio
                    else
                        idBio := FieldByName( 'IV_ID_BIO' ).AsInteger;

                    dmSistema.ActualizaIdBio( FieldByName( 'CB_CODIGO' ).AsInteger, idBio, FieldByName( 'IV_ID_GPO' ).AsString, FieldByName( 'IV_CODIGO' ).AsInteger );
                    if lCambioIDBio then
                    begin
                         if not( State in [ dsEdit, dsInsert ] )then
                            Edit;

                         FieldByName( 'IV_ID_BIO' ).AsInteger := idBio;
                         PostData;
                    end;
               end;
          end;
{$endif}

          if State in [ dsEdit, dsInsert ] then //Si edita o inserta... postea el invitador.
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile ( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enInvitacion ] );
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsInvitadoresGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CAFE_INVITADORES, iRight );
end;

{ cdsRPatron }

procedure TdmCatalogos.cdsRPatronAlAdquirirDatos(Sender: TObject);
begin
     cdsRSocial.Conectar;
     cdsRPatron.Data := ServerCatalogo.GetRPatron( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsRPatronAlCrearCampos(Sender: TObject);
begin
     with cdsRPatron do
     begin
          ListaFija( 'TB_MODULO', lfRPatronModulo );
          cdsPRiesgo.DataSetField := TDataSetField( FieldByName( 'qryDetail' ) );
          CreateSimpleLookup( cdsRSocial, 'RS_NOMBRE', 'RS_CODIGO' );
     end;
end;

procedure TdmCatalogos.cdsRPatronBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'TB_CODIGO' );
     with DataSet do
     begin
          if (FieldByName('TB_CLASE').AsInteger = Ord(claseNingun)) then
               FieldByName('TB_FRACC').AsInteger:= 0;
          if StrVacio( FieldByName( 'RS_CODIGO' ).AsString ) then
             DataBaseError( 'La raz�n social no puede quedar vac�a' );
     end;
end;

procedure TdmCatalogos.cdsRPatronAlModificar(Sender: TObject);
begin
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatRPatron_DevEx, TEditCatRPatron_DevEx );
end;

procedure TdmCatalogos.cdsRPatronGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_GRALES_PATRONALES, iRight );
end;

{ cdsReglas }

procedure TdmCatalogos.cdsReglasAlAdquirirDatos(Sender: TObject);
begin
     cdsReglas.Data := ServerCatalogo.GetReglas( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsReglasNewRecord(DataSet: TDataSet);
begin
     with cdsReglas do
     begin
          FieldByName( 'CL_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CL_LIMITE' ).AsString := '1';
          FieldByName( 'CL_TOTAL' ).AsString := 'DIARIO()';
          FieldByName( 'CL_LETRERO' ).AsString := 'PASO LIMITE DIARIO';
     end;
end;

procedure TdmCatalogos.cdsReglasBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'CL_CODIGO' );
end;

procedure TdmCatalogos.cdsReglasAlModificar(Sender: TObject);
begin
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatReglas_DevEx, TEditCatReglas_DevEx );   //DevEx (icm)
end;

procedure TdmCatalogos.cdsReglasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CAFE_REGLAS, iRight );
end;

{ cdsPuestos }

procedure TdmCatalogos.cdsPuestosAlAdquirirDatos(Sender: TObject);
begin
     cdsPuestos.Data := ServerCatalogo.GetPuestos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsPuestosBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'PU_CODIGO' );
end;

//DevEx (by am)
procedure TdmCatalogos.cdsPuestosAlModificar(Sender: TObject);
begin
//        ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatPuestos_DevEx, TEditCatPuestos_DevEx );
end;

procedure TdmCatalogos.cdsPuestosAlEnviarDatos(Sender: TObject);
var
   ErrorCount{$ifndef DOS_CAPAS},ErrorCountComp{$endif}: Integer;
begin
     ErrorCount := 0;
     {$ifndef DOS_CAPAS}
     ErrorCountComp := 0;
     {$endif}
     with cdsPuestos do
     begin
          {$ifndef DOS_CAPAS}
          with cdsGpoCompPuesto do
          begin
               if State in [ dsEdit, dsInsert ] then
                  Post;
               if ( ChangeCount > 0 ) then
               begin
                    Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, cdsGpoCompPuesto.Tag, Delta, ErrorCountComp ));
               end;
          end;
          if ErrorCountComp = 0 then
          begin
          {$endif}
               if State in [ dsEdit, dsInsert ] then
                  Post;
               if ( ChangeCount > 0 ) then
               begin

                    if Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
                    begin
                         TressShell.SetDataChange( [ enEntrena, enEntNivel, enPuesto{$ifndef DOS_CAPAS}, enPlanCapacitacion {$endif}] );
                         {$ifndef DOS_CAPAS}
                         dmRecursos.cdsPlanCapacitacion.Refrescar;
                         {$endif}
                    end;
               end;
          {$ifndef DOS_CAPAS}
          end;
          {$endif}
     end;
end;

procedure TdmCatalogos.cdsPuestosAfterDelete(DataSet: TDataSet);
begin
     cdsPuestos.Enviar;
end;

procedure TdmCatalogos.cdsPuestosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CONT_PUESTOS, iRight );
end;

procedure TdmCatalogos.cdsPuestosBeforeInsert(DataSet: TDataSet);
begin
     cdsPuestosLookup.Data := cdsPuestos.Data;  // si se hace despu�s, regresa el Dataset a dsBrowse
end;

procedure TdmCatalogos.cdsPuestosNewRecord(DataSet: TDataSet);
begin
     with cdsPuestos do
     begin
          FieldByName('PU_TIPO').AsInteger := Ord( ptoEmpleado );
          FieldByName('PU_PLAZAS').AsInteger := 1;
          FieldByName('PU_ACTIVO').AsString := K_GLOBAL_SI;
          FieldByName('PU_AUTOSAL').AsString := K_USAR_GLOBAL;
          FieldByName('PU_CHECA').AsString := K_USAR_GLOBAL;
     end;
end;

{ cdsOtrasPer }

procedure TdmCatalogos.cdsOtrasPerAlAdquirirDatos(Sender: TObject);
begin
     cdsOtrasPer.Data := ServerCatalogo.GetOtrasPer( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsOtrasPerAlCrearCampos(Sender: TObject);
begin
     with cdsOtrasPer do
     begin
          MaskPesos( 'TB_MONTO' );
          MaskPesos( 'TB_TASA' );
          ListaFija( 'TB_TIPO', lfTipoOtrasPer );
          ListaFija( 'TB_IMSS', lfIMSSOtrasPer );
          ListaFija( 'TB_ISPT', lfISPTOtrasPer );
     end;
end;

procedure TdmCatalogos.cdsOtrasPerNewRecord(DataSet: TDataSet);
begin
     with cdsOtrasPer do
     begin
          FieldByName( 'TB_TIPO' ).AsString := '0';
          FieldByName( 'TB_IMSS' ).AsInteger := Ord( ioGravado );
          FieldByName( 'TB_ISPT' ).AsInteger := Ord( soGravado );
     end;
end;

procedure TdmCatalogos.cdsOtrasPerBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'TB_CODIGO' );
end;

procedure TdmCatalogos.cdsOtrasPerAlModificar(Sender: TObject);
begin
//          ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatOtrasPer_DevEx, TEditCatOtrasPer_DevEx );
end;

procedure TdmCatalogos.cdsOtrasPerGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CONT_PER_FIJAS, iRight );
end;

// ********** cdsMatrizCurso ************

procedure TdmCatalogos.SetMatriz(const lCurso: Boolean; const sLlave: String);
begin
     FMatrizPorCurso := lCurso;
     {
     FMatrizCodigo := sLlave;
     }
end;

procedure TdmCatalogos.cdsMatrizCursoAlAdquirirDatos(Sender: TObject);
var
   sCodigo: String;
begin
     cdsCursos.Conectar;
     if ( FMatrizPorCurso ) then
        sCodigo := cdsCursos.FieldByName( 'CU_CODIGO' ).AsString
     else
         sCodigo := cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString;
     cdsMatrizCurso.Data := ServerCatalogo.GetMatriz( dmCliente.Empresa, FMatrizPorCurso, sCodigo );
end;

//DevEx (by am): Se agrega la validacion de la vista para mostrar la forma correspondiente.
procedure TdmCatalogos.cdsMatrizCursoAlModificar(Sender: TObject);
begin
//          if FMatrizPorCurso then
//             ZBaseEdicion_DevEx.ShowFormaEdicion( EditMatriz_DevEx, TEditMatriz_DevEx )
//          else
//              ZBaseEdicion_DevEx.ShowFormaEdicion( EditMatrizPuesto_DevEx, TEditMatrizPuesto_DevEx );
end;

procedure TdmCatalogos.cdsMatrizCursoNewRecord(DataSet: TDataSet);
begin
     with Dataset do
     begin
          if FMatrizPorCurso then
             FieldByName( 'CU_CODIGO' ).AsString := cdsCursos.FieldByName( 'CU_CODIGO' ).AsString
          else
              FieldByName( 'PU_CODIGO' ).AsString := cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString;
          FieldByName( 'EN_OPCIONA' ).AsString := K_GLOBAL_SI;
          FieldByName( 'EN_LISTA' ).AsString := K_GLOBAL_NO;
          FieldByName('EN_REPROG').AsString:= K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsMatrizCursoBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'PU_CODIGO' );
     cdsBeforePost( DataSet, 'CU_CODIGO' );
end;

procedure TdmCatalogos.cdsMatrizCursoAfterOpen(DataSet: TDataSet);
begin
     cdsEntNivel.DataSetField := TDataSetField( cdsMatrizCurso.FieldByName( 'qryDetail' ) );
end;

procedure TdmCatalogos.cdsMatrizCursoAfterCancel(DataSet: TDataSet);
begin
     cdsEntNivel.CancelUpdates;
end;

procedure TdmCatalogos.cdsMatrizCursoAfterDelete(DataSet: TDataSet);
begin
     cdsMatrizCurso.Enviar;
end;

procedure TdmCatalogos.cdsMatrizCursoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsMatrizCurso do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaMatriz( dmCliente.Empresa, FMatrizPorCurso, Delta, ErrorCount ) );
          end;
     end;
     dmRecursos.cdsHisCursosProg.Active := False; // para que se refresque
end;

procedure TdmCatalogos.cdsMatrizCursoAlCrearCampos(Sender: TObject);
begin
     with cdsMatrizCurso do
     begin
          CreateSimpleLookup( cdsPuestos, 'PU_NOMBRE', 'PU_CODIGO' );
          CreateSimpleLookup( cdsCursos, 'CU_NOMBRE', 'CU_CODIGO' );
          CreateLookup( cdsCursos, 'CU_REVISIO', 'CU_CODIGO', 'CU_CODIGO', 'CU_REVISIO' );
          //ListaFija('CU_TIPO',lfTipoCapacitacion );
     end;
end;

procedure TdmCatalogos.cdsEntNivelAfterDelete(DataSet: TDataSet);
begin
     with cdsMatrizCurso do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmCatalogos.cdsEntNivelNewRecord(DataSet: TDataSet);
begin
     with cdsEntNivel do
     begin
          FieldByName( 'CU_CODIGO' ).AsString := cdsMatrizCurso.FieldByName( 'CU_CODIGO' ).AsString;
          FieldByName( 'PU_CODIGO' ).AsString := cdsMatrizCurso.FieldByName( 'PU_CODIGO' ).AsString;
     end;
end;

{ cdsMaestros }

procedure TdmCatalogos.cdsMaestrosAlAdquirirDatos(Sender: TObject);
begin
     cdsMaestros.Data := ServerCatalogo.GetMaestros( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsMaestrosBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'MA_CODIGO' );
     //ValidaMemoField( Dataset, 'MA_IMAGEN');
end;

//DevEx (by am): Se agrega validacion de la vista del usuario para mostrar la forma correspondiente
procedure TdmCatalogos.cdsMaestrosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CAPA_MAESTROS, iRight );
end;

procedure TdmCatalogos.cdsMaestrosNewRecord(DataSet: TDataSet);
begin
     DataSet.FieldByName( 'MA_ACTIVO').AsString := K_GLOBAL_SI;
end;

procedure TdmCatalogos.cdsMaestrosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsMaestros do
     begin
          if State in [ dsEdit, dsInsert ] then
             PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaMaestros(dmCliente.Empresa, Delta, ErrorCount) );
          end;
     end;
end;

{cdsProvCap}

procedure TdmCatalogos.cdsProvCapAlAdquirirDatos(Sender: TObject);
begin
     cdsProvCap.Data:= ServerCatalogo.GetProvCap( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsProvCapBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'PC_CODIGO' );
end;

//DevEx (by am): Se agrega la validacion de la vista para mostrar la forma correspondiente
procedure TdmCatalogos.cdsProvCapAlModificar(Sender: TObject);
begin
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatProvCap_DevEx, TEditCatProvCap_DevEx );
end;

procedure TdmCatalogos.cdsProvCapGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CAPA_PROVEEDORES, iRight );
end;

procedure TdmCatalogos.cdsProvCapNewRecord(DataSet: TDataSet);
begin
     DataSet.FieldByName( 'PC_ACTIVO').AsString := K_GLOBAL_SI;
end;
{ cdsFestTurno }

procedure TdmCatalogos.cdsFestTurnoAlAdquirirDatos(Sender: TObject);
begin
     cdsFestTurno.Data := ServerCatalogo.GetFestTurno( dmCliente.Empresa, FTurnoFestivo, dmCliente.YearDefault );
end;

procedure TdmCatalogos.cdsFestTurnoAlCrearCampos(Sender: TObject);
begin
     with cdsFestTurno do
     begin
          FieldByName( 'FE_MES' ).OnGetText := FE_MESGetText;
          FieldByName( 'FE_TIPO' ).OnChange := FE_TIPOChange;
          ListaFija( 'FE_TIPO', lfTipoFestivo );
          MaskFecha( 'FE_CAMBIO' );
          MaskFecha('FE_YEAR');
          FieldByName( 'FE_CAMBIO' ).OnGetText := FE_CAMBIOGetText;
          FieldByName( 'FE_YEAR' ).OnGetText := FE_YEARGetText;
     end;
end;

procedure TdmCatalogos.FE_TIPOChange( Sender: TField );
begin
     with cdsFestTurno do
     begin
          if ( eTipoFestivo( Sender.AsInteger ) = tfIntercambio ) and ( FieldByName( 'FE_CAMBIO' ).AsDateTime = NullDateTime ) then
          begin
               FieldByName( 'FE_CAMBIO' ).AsDateTime := dmCliente.FechaDefault;
               FieldByName( 'FE_CAMBIO' ).FocusControl;
          end;
     end;
end;

procedure TdmCatalogos.FE_CAMBIOGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
    if DisplayText then
    begin
      with cdsFestTurno do
        if eTipoFestivo( FieldByName( 'FE_TIPO' ).AsInteger ) = tfFestivo then
            Text := ''
        else
            Text := Sender.AsString;
    end
    else
        Text := Sender.AsString;
end;

procedure TdmCatalogos.FE_MESGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if ( Sender.AsInteger > 0 ) then
             Text:= ObtieneElemento( lfMeses, ( Sender.AsInteger - 1 ) )
          else
              Text := '';
     end
     else
         Text:= Sender.AsString;
end;

procedure TdmCatalogos.FE_YEARGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if (cdsFestTurno.FieldByName('FE_MES').AsInteger <> 0 ) and ( Sender.AsInteger = 0 ) then
             Text:= 'Todos'
          else
              Text := Sender.AsString;
     end
     else
         Text:= VACIO;
end;

procedure TdmCatalogos.cdsFestTurnoNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'TU_CODIGO' ).AsString := FTurnoFestivo;
          FieldByName( 'FE_MES' ).AsInteger := Ord( emEnero ) + 1;
          FieldByName( 'FE_DIA' ).AsInteger := 1;
          FieldByName( 'FE_TIPO' ).AsInteger := Ord( tfFestivo );
          FieldByName( 'FE_YEAR' ).AsInteger := 0;
          // EZM: Y1900. FieldByName( 'FE_CAMBIO' ).AsDateTime := NullDateTime;
      end;
end;

procedure TdmCatalogos.cdsFestTurnoBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if ( FieldByName( 'FE_DIA' ).AsInteger > DaysInMonth[ FieldByName( 'FE_MES' ).AsInteger ] ) then
             DataBaseError( 'El D�a ' + FieldByName( 'FE_DIA' ).AsString + ' No Existe En ' + ObtieneElemento( lfMeses, FieldByName( 'FE_MES' ).AsInteger - 1 ) );
          if NOT DayIsValid(FieldByName( 'FE_YEAR' ).AsInteger,FieldByName( 'FE_MES' ).AsInteger,FieldByName( 'FE_DIA' ).AsInteger ) then
             DataBaseError( 'El D�a ' + FieldByName( 'FE_DIA' ).AsString + ' No Existe En ' + ObtieneElemento( lfMeses, FieldByName( 'FE_MES' ).AsInteger - 1 ) );
          if ( eTipoFestivo( FieldByName( 'FE_TIPO' ).AsInteger ) = tfIntercambio ) and
             ( FieldByName( 'FE_CAMBIO' ).AsDateTime = NullDateTime ) then
             DataBaseError( 'La Fecha de Intercambio No Puede Quedar Vac�a' );
          if ( eTipoFestivo( FieldByName( 'FE_TIPO' ).AsInteger ) = tfFestivo ) and
             ( FieldByName( 'FE_CAMBIO' ).AsDateTime <> NullDateTime ) then
             FieldByName( 'FE_CAMBIO' ).AsDateTime:= NullDateTime;
          if ( FieldByName('FE_YEAR').AsInteger <> dmCliente.YearDefault )
          and ( FieldByName('FE_YEAR').AsInteger <> 0 )  and  ( not ZetaDialogo.ZConfirm( '� Atenci�n !', 'El a�o de vigencia es diferente al del sistema. � Desea Continuar ?', 0, mbNo ) ) then
             Abort
          else if ( FieldByName('FE_TIPO').AsInteger = 1 ) and
           ( TheYear( FieldByName('FE_CAMBIO').AsDateTime ) <> FieldByName('FE_YEAR').AsInteger ) and
           NOT (ZetaDialogo.ZConfirm('� Atenci�n !', 'El a�o de la fecha de intercambio es diferente al a�o de la fecha del festivo. �Desea continuar? ',0, mbNo ) ) then
             Abort;
     end;
end;

procedure TdmCatalogos.cdsFestTurnoAfterPost(DataSet: TDataSet);
var
   sTieneFestivos: String;
begin
     if ( FTurnoFestivo <> TURNO_VACIO ) then
     begin
          sTieneFestivos := zBoolToStr( not DataSet.IsEmpty );
          with cdsTurnos do
          begin
               if ( FieldByName( 'TU_FESTIVO' ).AsString <> sTieneFestivos ) then
               begin
                    Edit;
                    FieldByName( 'TU_FESTIVO' ).AsString := sTieneFestivos;
                    Post;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsFestTurnoAfterDelete(DataSet: TDataSet);
begin
     cdsFestTurno.Enviar;
end;

procedure TdmCatalogos.cdsFestTurnoAlModificar(Sender: TObject);
begin

//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditFestTurno_DevEx, TEditFestTurno_DevEx );
end;

procedure TdmCatalogos.cdsFestTurnoGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_GRALES_FEST_TURNO, iRight );
end;

{ cdsFolios }

procedure TdmCatalogos.cdsFoliosAlAdquirirDatos(Sender: TObject);
begin
     cdsFolios.Data := ServerCatalogo.GetFolios( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsFoliosAfterOpen(DataSet: TDataSet);
begin
     cdsOrdFolios.DataSetField := TDataSetField( cdsFolios.FieldByName( 'qryDetail' ) );
end;
procedure TdmCatalogos.cdsFoliosBeforeInsert(DataSet: TDataSet);
begin
     FMaxFolio := ZetaDataSetTools.GetNextEntero( DataSet, 'FL_CODIGO', 0 );
     if ( FMaxFolio > K_MAX_FOLIO ) then
        FMaxFolio := 0;         // Si el siguiente es 6 no sugiere valor, ya que solo se permiten valores de 1 a 5
end;

procedure TdmCatalogos.cdsFoliosNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'FL_CODIGO' ).AsInteger := FMaxFolio;
          FieldByName( 'FL_MONTO' ).AsString := 'NO_NETO';
          FieldByName( 'FL_REPITE' ).AsString := K_GLOBAL_NO;
          FieldByName( 'FL_CEROS' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsFoliosBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if zStrToBool( FieldByName( 'FL_REPITE' ).AsString ) and
             ( FieldByName( 'FL_MONEDA' ).AsFloat <= 0 ) then
             DatabaseError( 'Moneda a Repetir No Puede Ser Cero !!' );
          if ( FieldByName( 'FL_CODIGO' ).AsInteger <= 0 ) or
             ( FieldByName( 'FL_CODIGO' ).AsInteger > K_MAX_FOLIO ) then
             DatabaseError( Format( 'C�digo de Folio Debe Ser Un N�mero Del 1 al %d', [ K_MAX_FOLIO ] ) );
     end;
end;

procedure TdmCatalogos.cdsFoliosAlModificar(Sender: TObject);
begin

//      ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatFolios_DevEx, TEditCatFolios_DevEx );
end;

procedure TdmCatalogos.cdsFoliosAfterCancel(DataSet: TDataSet);
begin
     cdsOrdFolios.CancelUpdates;
end;

procedure TdmCatalogos.cdsFoliosAfterDelete(DataSet: TDataSet);
begin
     cdsFolios.Enviar;
end;

procedure TdmCatalogos.cdsFoliosAlEnviarDatos(Sender: TObject);
begin
     with cdsFolios do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     cdsAfterPost( cdsFolios );
end;

procedure TdmCatalogos.CargaOrdenFolio( oLista: TListaObjetos );

function GetDireccion( const sValue: String ): Integer;
begin
     if ( sValue = K_GLOBAL_NO ) then
        Result := 0
     else
         Result := 1;
end;

var
   oOrden: TOrdenOpciones;
begin
     oLista.Clear;
     with cdsOrdFolios do
     begin
          //First; .. OCASIONA PROBLEMAS y no es necesario
          while not EOF do
          begin
               oOrden := TOrdenOpciones.Create;
               with oOrden do
               begin
                    Formula := FieldByName( 'OF_CAMPO' ).AsString;
                    Titulo := FieldByName( 'OF_TITULO' ).AsString;
                    DireccionOrden := GetDireccion( FieldByName( 'OF_DESCEND' ).AsString );
               end;
               oLista.AddObject( oOrden.Titulo, oOrden );
               Next;
          end;
     end;
     cdsOrdFolios.First;
end;

procedure TdmCatalogos.AgregaOrdenFolio( const iFolio: Integer; const oLista: TListaObjetos );

function EsDescendente( const j: Integer ): Char;
begin
     if ( j = 0 ) then
        Result := K_GLOBAL_NO
     else
         Result := K_GLOBAL_SI;
end;

var
   i: Integer;
begin
     with cdsOrdFolios do
     begin
          First;
          while not Eof do
          begin
               Delete;
          end;
     end;
     with oLista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with cdsOrdFolios do
               begin
                    Append;
                    FieldByName( 'FL_CODIGO' ).AsInteger := iFolio;
                    FieldByName( 'OF_POSICIO' ).AsInteger := i;
                    FieldByName( 'OF_CAMPO' ).AsString := TOrdenOpciones( Objects[ i ] ).Formula;
                    FieldByName( 'OF_TITULO' ).AsString := TOrdenOpciones( Objects[ i ] ).Titulo;
                    FieldByName( 'OF_DESCEND' ).AsString := EsDescendente( TOrdenOpciones( Objects[ i ] ).DireccionOrden );
                    Post;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsFoliosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_NOMINA_FOLIOS, iRight );
end;

{ cdsHorarios }

procedure TdmCatalogos.cdsHorariosAlAdquirirDatos(Sender: TObject);
begin
     cdsHorarios.Data := ServerCatalogo.GetHorarios( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsHorariosAlCrearCampos(Sender: TObject);
begin
     with cdsHorarios do
     begin
          MaskHoras( 'HO_JORNADA' );
          MaskTime( 'HO_INTIME' );
          MaskTime( 'HO_OUTTIME' );
          ListaFija( 'HO_TIPO', lfTipoHorario );
     end;
end;

procedure TdmCatalogos.cdsHorariosNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'HO_TIPO' ).AsInteger := 1;
          FieldByName( 'HO_INTIME' ).AsString := '0800';
          FieldByName( 'HO_IN_TEMP' ).AsInteger := 90;
          FieldByName( 'HO_IN_TARD' ).AsInteger := 5;
          FieldByName( 'HO_OUTTIME' ).AsString := '1700';
          FieldByName( 'HO_OU_TEMP' ).AsInteger := 0;
          FieldByName( 'HO_OU_TARD' ).AsInteger := 30;
          FieldByName( 'HO_LASTOUT' ).AsString := '0300';
          FieldByName( 'HO_JORNADA' ).AsFloat := 8;
          FieldByName( 'HO_DOBLES' ).AsFloat := 3;
          FieldByName( 'HO_EXT_FIJ' ).AsFloat := 0;
          FieldByName( 'HO_COMER' ).AsInteger := 60;
          FieldByName( 'HO_MIN_EAT' ).AsInteger := 240;
          FieldByName( 'HO_CHK_EAT' ).AsString := K_GLOBAL_NO;
          FieldByName( 'HO_IGN_EAT' ).AsString := K_GLOBAL_NO;
          FieldByName( 'HO_ADD_EAT' ).AsString := K_GLOBAL_NO;
          FieldByName( 'HO_OUT_EAT' ).AsString := '1200';
          FieldByName( 'HO_IN_EAT' ).AsString :='1300';
          FieldByName( 'HO_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'HO_PARES' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsHorariosBeforePost(DataSet: TDataSet);
var
   iIn, iOut: Integer;
begin
     with DataSet do
     begin
          cdsBeforePost( Dataset, 'HO_CODIGO' );
          iIn := AMinutos( FieldByName( 'HO_INTIME' ).AsString );
          iOut := AMinutos( FieldByName( 'HO_OUTTIME' ).AsString );
          if ( iIn <= iOut ) then
             FieldByName( 'HO_JORNADA' ).AsFloat:= aHoras( iOut - iIn - FieldByName( 'HO_COMER' ).AsInteger )
          else
              FieldByName( 'HO_JORNADA' ).AsFloat := aHoras( iOut - ( iIn - 24 * 60 ) - FieldByName( 'HO_COMER' ).AsInteger );

          if ( not ( eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ) in [ thSinHorario,thNormal,thMadrugadaEntrada ] ) ) then
             FieldByName( 'HO_PARES' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsHorariosAlModificar(Sender: TObject);
begin

//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatHorarios_DevEx, TEditCatHorarios_DevEx );
end;

procedure TdmCatalogos.cdsHorariosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CONT_HORARIOS, iRight );
end;

{ cdsCursos }

procedure TdmCatalogos.cdsCursosAlAdquirirDatos(Sender: TObject);
begin
     cdsCursos.Data := ServerCatalogo.GetCursos( dmCliente.Empresa );
     { Se conecta solo el metadata de Historial de Revisiones por Curso }
     GetCursosRevision(VACIO);
end;


procedure TdmCatalogos.GetCursosRevision( const sCurso : String );
begin
     dmSistema.cdsUsuarios.Conectar;
     cdsHistRev.Data := ServerCatalogo.GetHistRev( dmCliente.Empresa, sCurso );
end;

procedure TdmCatalogos.cdsCursosAlCrearCampos(Sender: TObject);
begin
     with cdsCursos do
     begin
          maskFecha('KC_FEC_TOM');
          MaskHoras( 'CU_HORAS' );
          FieldByName( 'CU_FOLIO' ).OnChange := CursoDescripChange;
          FieldByName( 'CU_NOMBRE' ).OnChange := CursoDescripChange;
          ListaFija('CU_OBJETIV',lfObjetivoCursoSTPS);
          ListaFija('CU_MODALID',lfModalidadCursoSTPS);

          FieldByName( 'CU_CODIGO' ).OnValidate := cdsCursosCU_CODIGOValidate;
     end;
end;

procedure TdmCatalogos.cdsCursosNewRecord(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'CU_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'CU_FEC_REV' ).AsDateTime := Date;
          FieldByName( 'CU_STPS').AsString:= K_GLOBAL_NO;
     end;
     { Refresca las revisiones sin Revisi�n }
     GetCursosRevision( VACIO );
end;

procedure TdmCatalogos.CursoDescripChange( Sender: TField );
begin
     with cdsCursos do
     begin
          FieldByName( 'CU_DESCRIP' ).AsString := ConcatString( FieldByName( 'CU_FOLIO' ).AsString,
                                                                FieldByName( 'CU_NOMBRE' ).AsString, ' : ' );
     end;
end;

procedure TdmCatalogos.cdsCursosBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'CU_CODIGO' );
     ValidaMemoField( DataSet, 'CU_TEXTO1' );
     ValidaMemoField( DataSet, 'CU_TEXTO2' );

     FSoloInsercion:= ( cdsCursos.State = dsInsert );
     if ( FSoloInsercion ) then
     begin
          if ( StrLleno(cdsCursos.FieldByName('CU_REVISIO').AsString) ) then
          begin
               with cdsHistRev do
               begin
                    DisableControls;
                    try
                       if not( State  in [ dsEdit, dsInsert ] ) then
                          Edit;
                       FieldByName('CU_CODIGO').AsString := cdsCursos.FieldByName('CU_CODIGO').AsString;
                       FieldByName('CH_REVISIO').AsString := cdsCursos.FieldByName('CU_REVISIO').AsString;
                       FieldByName('CH_FECHA').AsDatetime := cdsCursos.FieldByName('CU_FEC_REV').AsDatetime;
                       Post;
                    finally
                           EnableControls;
                    end;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsCursosAlModificar(Sender: TObject);
begin

//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatCursos_DevEx, TEditCatCursos_DevEx );
end;

procedure TdmCatalogos.cdsCursosAfterDelete(DataSet: TDataSet);
begin
     cdsCursos.Enviar;
end;

procedure TdmCatalogos.cdsCursosAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorCountH: Integer;
   oCursos, oHResult: OleVariant;
   sCurso: String;
begin
     cdsHistRev.PostData;
     cdsCursos.PostData;

     with cdsCursos do
     begin
          sCurso:= FieldByName('CU_CODIGO').AsString;
          if ( Changecount > 0 ) or ( cdsHistRev.ChangeCount > 0 ) then
          begin
               oCursos := ServerCatalogo.GrabaCursos( dmCliente.Empresa, DeltaNull,
                                                      cdsHistRev.DeltaNull, ErrorCount,
                                                      ErrorCountH, oHResult );
               DisableControls;
               try
                  if ( ( ChangeCount = 0 ) or Reconcile( oCursos ) ) then
                  begin
                       if ( ErrorCountH = 0 ) or cdsHistRev.Reconcile( oHResult ) then
                       begin
                            cdsCursos.Refrescar;
                            cdsCursos.Locate( 'CU_CODIGO', sCurso, [] );
                       end
                       else if ( cdsHistRev.ChangeCount > 0 ) then
                       begin
                            cdsHistRev.Edit;
                       end;
                  end
                  else if ( cdsCursos.ChangeCount > 0 ) then
                  begin
                       cdsCursos.Edit;
                  end;
               finally
                      EnableControls;
               end;
               if FRefrescaEntidades then
                  TressShell.SetDataChange( [ enKarCurso, enEntrena, enCalCurso, enCurProg ] );
          end;
     end;
end;

procedure TdmCatalogos.cdsCursosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CAPA_CURSOS, iRight );
end;

{ cdsEventos }

procedure TdmCatalogos.cdsEventosAlAdquirirDatos(Sender: TObject);
begin
     cdsEventos.Data := ServerCatalogo.GetEventos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsEventosAlModificar(Sender: TObject);
begin

//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatEventos_DevEx, TEditCatEventos_DevEx );
end;

procedure TdmCatalogos.cdsEventosNewRecord(DataSet: TDataSet);
begin
     with cdsEventos do
     begin
          FieldByName( 'EV_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'EV_FEC_CON' ).AsString := K_GLOBAL_NO;
          FieldByName( 'EV_TABULA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'EV_AUTOSAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'EV_BAJA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'EV_ALTA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'EV_M_ANTIG' ).AsString := K_GLOBAL_SI;
          FieldByName( 'EV_PRIORID' ).AsInteger := Ord( nuPrioridadNormal );
          FieldByName( 'EV_INCLUYE' ).AsInteger := Ord( ieEmpleados );
          FieldByName( 'EV_ANT_INI' ).AsInteger := 0;
          FieldByName( 'EV_ANT_FIN' ).AsInteger := 0;
          FieldByName( 'EV_M_SVAC').AsString:= K_GLOBAL_NO;
          FieldByName( 'EV_M_TVAC').AsInteger:= 0;
          FieldByName( 'EV_CAMBNOM').AsString:= K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsEventosBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'EV_CODIGO' );
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'EV_CONTRAT' ).AsString ) then
             FieldByName( 'EV_FEC_CON' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsEventosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_GRALES_EVENTOS, iRight );
end;

{ cdsSSocial }

procedure TdmCatalogos.cdsSSocialAlAdquirirDatos(Sender: TObject);
begin
     cdsSSocial.Data := ServerCatalogo.GetPrestaciones( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsSSocialAlCrearCampos(Sender: TObject);
begin
     cdsSSocial.MaskTasa( 'TB_PRIMAVA' );
end;

procedure TdmCatalogos.cdsSSocialAfterOpen(DataSet: TDataSet);
begin
     cdsPrestaci.DataSetField := TDataSetField( cdsSSocial.FieldByName( 'qryDetail' ) );
end;

procedure TdmCatalogos.cdsSSocialAfterCancel(DataSet: TDataSet);
begin
     cdsPrestaci.CancelUpdates;
     FSoloInsercion := FALSE;
end;

procedure TdmCatalogos.cdsSSocialBeforeInsert(DataSet: TDataSet);
begin
     if ( not FCambioPrestaciones ) then
        FSoloInsercion := TRUE;
end;

procedure TdmCatalogos.cdsSSocialAfterDelete(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsSSocial do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
          end;
     end;
end;

//DevEx(by am): Se agrega la validacion del tipo de vista para mostrar la forma de edicion correspondiente
procedure TdmCatalogos.cdsSSocialAlModificar(Sender: TObject);
begin
//     FCambioPrestaciones:=FALSE;
//        ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatPrestaciones_DevEx, TEditCatPrestaciones_DevEx );
//     if ( ZAccesosMgr.Revisa(D_EMP_PROC_RECALC_INT) ) then
//     begin
//          if FCambioPrestaciones and ( ZetaDialogo.ZConfirm( 'Informaci�n','� Desea recalcular salarios integrados ?', 0 , mbYes ) )then
//          begin
//               DProcesos.ShowWizard( prRHSalarioIntegrado );
//          end;
//          FCambioPrestaciones := FALSE;
//     end;
end;

procedure TdmCatalogos.cdsSSocialNewRecord(DataSet: TDataSet);
begin
     with cdsSSocial do
     begin
          FieldByName( 'TB_PRIMAVA' ).AsFloat := 25.0;
          FieldByName( 'TB_DIAS_AG' ).AsFloat := 15.0;
          FieldByName( 'TB_PAGO_7' ).AsString := K_GLOBAL_SI;
          FieldByName( 'TB_PRIMA_7' ).AsString := K_GLOBAL_NO;
          FieldByName( 'TB_PRIMADO' ).AsFloat := 0.0;
          FieldByName( 'TB_DIAS_AD' ).AsFloat := 0.0;
          FieldByName( 'TB_ACTIVO' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmCatalogos.cdsSSocialAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   lEditPrestaciones: Boolean;
begin
     ErrorCount := 0;
     with cdsSsocial do
     begin
          lEditPrestaciones:= ( not FSoloInsercion ) and ( State = dsEdit );

          if State in [ dsEdit, dsInsert ] then
             Post;

          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
               FCambioPrestaciones:= lEditPrestaciones;
               FSoloInsercion := FALSE;
          end;
     end;
end;

procedure TdmCatalogos.cdsSSocialGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CONT_PRESTA, iRight );
end;

{ cdsPrestaci }

procedure TdmCatalogos.cdsPrestaciAlCrearCampos(Sender: TObject);
begin
     with cdsPrestaci do
     begin
          MaskNumerico( 'PT_FACTOR', '#,0.0000' );
          {
          MaskTasa( 'PT_FACTOR' );
          }
          MaskTasa( 'PT_PRIMAVA' );
          MaskTasa( 'PT_PRIMADO' );
          //MaskPesos( 'PT_DIAS_VA' );
          FieldByName( 'PT_DIAS_VA' ).OnChange := cdsPrestaciCamposFactorChange;
          FieldByName( 'PT_PRIMAVA' ).OnChange := cdsPrestaciCamposFactorChange;
          FieldByName( 'PT_DIAS_AG' ).OnChange := cdsPrestaciCamposFactorChange;
          FieldByName( 'PT_DIAS_AD' ).OnChange := cdsPrestaciCamposFactorChange;
          FieldByName( 'PT_PRIMADO' ).OnChange := cdsPrestaciCamposFactorChange;
          FieldByName( 'PT_PAGO_7' ).OnChange := cdsPrestaciCamposFactorChange;
          FieldByName( 'PT_PRIMA_7' ).OnChange := cdsPrestaciCamposFactorChange;
     end;
end;

procedure TdmCatalogos.cdsPrestaciCamposFactorChange(Sender: TField);
begin
     { OBJETIVO: Al cambiar:
     PT_DIAS_VA, PT_PRIMAVA, PT_DIAS_AG, PT_DIAS_AD, PT_PRIMADO, PT_PAGO_7, PT_PRIMA_7,
     se recalcula el factor de integracion }
     cdsPrestaci.FieldByName( 'PT_FACTOR' ).AsFloat := CalculaFactorIntegracion;
end;

procedure TdmCatalogos.cdsPrestaciNewRecord(DataSet: TDataSet);
begin
     with cdsPrestaci do
     begin
          FieldByName( 'PT_YEAR' ).AsInteger := FUltimoYear+1;
          FieldByName( 'PT_DIAS_VA' ).AsFLoat := FUltimaVaca;
          FieldByName( 'PT_PRIMAVA' ).AsFloat := cdsSSocial.FieldByName( 'TB_PRIMAVA' ).AsFloat;
          FieldByName( 'PT_DIAS_AG' ).AsFloat := cdsSSocial.FieldByName( 'TB_DIAS_AG' ).AsFloat;
          FieldByName( 'PT_DIAS_AD' ).AsFloat := cdsSSocial.FieldByName( 'TB_DIAS_AD' ).AsFloat;
          FieldByName( 'PT_PRIMADO' ).AsFloat := cdsSSocial.FieldByName( 'TB_PRIMADO' ).AsFloat;
          FieldByName( 'PT_PAGO_7' ).AsString := cdsSSocial.FieldByName( 'TB_PAGO_7' ).AsString;
          FieldByName( 'PT_PRIMA_7' ).AsString := cdsSSocial.FieldByName( 'TB_PRIMA_7' ).AsString;
     end;
end;

procedure TdmCatalogos.cdsPrestaciBeforeInsert(DataSet: TDataSet);
begin
     with cdsPrestaci do
     begin
          DisableControls;
          Last;
          FUltimoYear := FieldByName( 'PT_YEAR' ).AsInteger;
          FUltimaVaca := FieldByName( 'PT_DIAS_VA' ).AsFloat;
          EnableControls;
     end;
end;

procedure TdmCatalogos.cdsPrestaciAfterDelete(DataSet: TDataSet);
begin
     with cdsSSocial do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmCatalogos.cdsPrestaciBeforePost(DataSet: TDataSet);
begin
     if ( cdsPrestaci.FieldByName('PT_YEAR').AsInteger > 99 ) or
        ( cdsPrestaci.FieldByName('PT_YEAR').AsInteger < 1 ) then
        DatabaseError('El A�o tiene que estar dentro del rango (1-99)');
end;

{ cdsRPatron }

procedure TdmCatalogos.cdsRPatronAfterCancel(DataSet: TDataSet);
begin
     cdsPRiesgo.CancelUpdates;
end;

procedure TdmCatalogos.cdsRPatronNewRecord(DataSet: TDataSet);
begin
     with cdsRPatron do
     begin
          FieldByName( 'TB_MODULO' ).AsInteger := 1;
          FieldByName('RS_CODIGO').AsString:= cdsRSocial.FieldByName('RS_CODIGO').AsString;
          FieldByName('TB_PLANPER').AsString := K_GLOBAL_NO;
          FieldByName('TB_PLANPRE').AsString := K_GLOBAL_NO;
          FieldByName('TB_STYPS').AsString := K_GLOBAL_NO;
          FieldByName('TB_ACTIVO').AsString := K_GLOBAL_SI;
     end;

end;

{ cdsPRiesgo }

procedure TdmCatalogos.cdsPRiesgoAfterOpen(DataSet: TDataSet);
begin
     with cdsPRiesgo do
     begin
          MaskTasa( 'RT_PRIMA' );
          MaskTasa( 'RT_PTANT' );  //Prima de Riesgo anterior
          MaskTasa( 'RT_M' );    //Prima de Riesgo M�nima
          MaskPesos('RT_S');  //D�as subsideados p/temporales
          MaskPesos( 'RT_I');  //Incapacidades permanentes
          MaskPesos('RT_D');  //Numero de defunciones
          MaskPesos('RT_N');  //Trabajadores promedio
          MaskPesos('RT_F');  //Factor prima 
          MaskFecha( 'RT_FECHA' );
          with FieldByName( 'RT_FECHA' ) do
          begin
               EditMask := '90/90/0099;1; ';
          end;
     end;
end;

procedure TdmCatalogos.cdsPRiesgoAfterDelete(DataSet: TDataSet);
begin
     with cdsRPatron do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmCatalogos.cdsPRiesgoNewRecord(DataSet: TDataSet);
begin
     with cdsPRiesgo do
     begin
          FieldByName( 'RT_FECHA' ).AsDateTime := Date;
     end;
end;

{ cdsNomParamLookUp }

procedure TdmCatalogos.cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsNomParam do
     begin
          if ( State <> dsInsert ) then // se condiciona porque la transferencia
          begin                         // de Data regresa el cds a dsBrowse
               Conectar;
               cdsNomParamLookUp.Data := Data;
          end;
     end;
end;

{ cdsNomParam }

procedure TdmCatalogos.cdsNomParamAlAdquirirDatos(Sender: TObject);
begin
     cdsNomParam.Data := ServerCatalogo.GetNomParam( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsNomParamAlCrearCampos(Sender: TObject);
begin
     cdsNomParam.FieldByName( 'NP_FORMULA' ).OnGetText := NP_FORMULAGetText;
end;

procedure TdmCatalogos.NP_FORMULAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     { Se requiere condicionar para que al Hacer un }
     { cdsNomParam.Cancel no ocurra un Invalid BLOB Handle }
     if not FEditando then
        Text := Sender.AsString;
end;

procedure TdmCatalogos.cdsNomParamAfterCancel(DataSet: TDataSet);
begin
     FEditando := False;
end;

procedure TdmCatalogos.cdsNomParamAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsNomParam do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
               begin
                    cdsNomParamLookUp.Refrescar;
               end;
          end;
     end;
     FEditando := False;
end;

procedure TdmCatalogos.cdsNomParamAlModificar(Sender: TObject);
begin

//      ZBaseEdicion_DevEx.ShowFormaEdicion( EditNomParams_DevEx, TEditNomParams_DevEx );
end;

procedure TdmCatalogos.cdsNomParamBeforeInsert(DataSet: TDataSet);
begin
     with cdsNomParam do
     begin
          DisableControls;
          Last;
          FUltimoParam := FieldByName( 'NP_FOLIO' ).AsInteger;
          EnableControls;
     end;
     cdsNomParamLookUp.Data := cdsNomParam.Data; { Si se hace despu�s, regresa el Dataset a dsBrowse }
     FEditando := True;
end;

procedure TdmCatalogos.cdsNomParamBeforeEdit(DataSet: TDataSet);
begin
     FEditando := True;
end;

procedure TdmCatalogos.cdsNomParamNewRecord(DataSet: TDataSet);
begin
     with cdsNomParam do
     begin
          FieldByName( 'NP_FOLIO' ).AsInteger := FUltimoParam+1;
          FieldByName( 'NP_TIPO' ).AsInteger := Ord( tgFloat );
          FieldByName( 'NP_ACTIVO' ).AsString := K_GLOBAL_SI;
     end
end;

procedure TdmCatalogos.cdsNomParamGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_NOMINA_PARAMETROS, iRight );
end;

procedure TdmCatalogos.cdsNomParamBeforePost(DataSet: TDataSet);
begin
    with cdsNomParam do
        FieldByName( 'NP_FORMULA' ).AsString := ZetaCommonTools.BorraCReturn( FieldByName( 'NP_FORMULA' ).AsString );
end;

{ cdsCalendario }

procedure TdmCatalogos.cdsCalendarioAlCrearCampos(Sender: TObject);
begin
     with cdsCalendario do
     begin
          MaskFecha( 'CC_FECHA' );
          CreateSimpleLookup( cdsCursos, 'CU_NOMBRE', 'CU_CODIGO' );
          CreateLookup( cdsCursos, 'CU_REVISIO', 'CU_CODIGO', 'CU_CODIGO', 'CU_REVISIO' );
     end;
end;

procedure TdmCatalogos.cdsCalendarioAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCalendario do
     begin
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
                  TressShell.SetDataChange( [ enCurProg ] );
          end;
     end;
end;

{ cdsTools }

procedure TdmCatalogos.cdsToolsAlAdquirirDatos(Sender: TObject);
begin
     cdsTools.Data := ServerCatalogo.GetTools( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsToolsGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_HERR_TOOLS, iRight );
end;

procedure TdmCatalogos.cdsToolsAlCrearCampos(Sender: TObject);
begin
     with cdsTools do
     begin
          MaskPesos( 'TO_NUMERO' );
          MaskPesos( 'TO_COSTO' );
          MaskPesos( 'TO_VAL_REP' );
          MaskPesos( 'TO_VAL_BAJ' );
     end;
end;

procedure TdmCatalogos.cdsToolsAlModificar(Sender: TObject);
begin

//   ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatTools_DevEx, TEditCatTools_DevEx );
end;

procedure TdmCatalogos.cdsToolsBeforePost(DataSet: TDataSet);
begin
     with cdsTools do
     begin
          if ( eDescuentoTools( FieldByName( 'TO_DESCTO' ).AsInteger ) <> dtFijo ) and
             ( FieldByName( 'TO_VIDA' ).AsInteger <= 0 ) then
          begin
               FieldByName( 'TO_VIDA' ).FocusControl;
               DataBaseError( 'Eligi� Tipo De Descuento Que Requiere Indicar La Vida Util' );
          end;
     end;
end;

function TdmCatalogos.GetJornadaHorario(const sHorario: String): Currency;
begin
     Result := 0;
     with cdsHorarios do
     begin
          Conectar;
          if Locate( 'HO_CODIGO', sHorario, [] ) then
          begin
               Result := FieldByName( 'HO_JORNADA' ).AsFloat;
          end;
     end;
end;

procedure TdmCatalogos.CargaListaPuestos(oLista: TStrings);
begin
     with cdsPuestos do
     begin
          Conectar;
          First;
          while not EOF do
          begin
               oLista.Add( FieldByName( 'PU_CODIGO' ).AsString + '=' +
                           FieldByName( 'PU_DESCRIP' ).AsString );
               Next;
          end;
     end;
end;

//DevEx (by am): Agregado para cargar el nuevo componente CheckListBox
procedure TdmCatalogos.CargaListaPuestos( oLista: TcxCheckListBoxItems );
begin
     with cdsPuestos do
     begin
          Conectar;
          First;
          while not EOF do
          begin
               oLista.Add.Text := FieldByName( 'PU_CODIGO' ).AsString + '=' + FieldByName( 'PU_DESCRIP' ).AsString ;
               Next;
          end;
     end;
end;

procedure TdmCatalogos.GuardaEntrenaPuestosProg(const sPuesto, sCurso: string; const MatrizCursos:Boolean );
begin
     if MatrizCursos then
     begin
          with cdsMatrizCurso do
          begin
               Append;
               FieldByName( 'CU_CODIGO' ).AsString := sCurso;
               FieldByName( 'PU_CODIGO' ).AsString := sPuesto;
               FieldByName( 'EN_DIAS' ).AsInteger := 0;
               FieldByName( 'EN_OPCIONA' ).AsString := K_GLOBAL_NO;
               Post;
          end;
     end
     else
     begin
          with cdsMatrizCertificPuesto do
          begin
               Append;
               FieldByName( 'CI_CODIGO' ).AsString := sCurso;
               FieldByName( 'PU_CODIGO' ).AsString := sPuesto;
               FieldByName( 'PC_DIAS' ).AsInteger := 0;
               FieldByName( 'PC_OPCIONA' ).AsString := K_GLOBAL_NO;
               Post;
          end;
     end;
end;

{function TdmCatalogos.GetTipoNomina(const sTurno: String): eTipoPeriodo;
begin
     with cdsTurnos do
     begin
          Conectar;
          if Locate( 'TU_CODIGO', sTurno, [] ) then
             Result := eTipoPeriodo( FieldByName( 'TU_NOMINA' ).AsInteger )
          else
             Result := tpSemanal;    // Por default regresa Semanal
     end;
end;}

procedure TdmCatalogos.CargaListaEventos(oLista: TStrings);
begin
     with cdsEventos do
     begin
          Conectar;
          DisableControls;
          try
             First;
             while not EOF do
             begin
                  if ( zStrToBool( FieldByName( 'EV_ACTIVO' ).AsString ) ) then
                     oLista.Add( FieldByName( 'EV_CODIGO' ).AsString + '=' +
                                 FieldByName( 'EV_DESCRIP' ).AsString );
                  Next;
             end;
          finally
             EnableControls;
          end;
     end;
end;

{cdsPercepFijas}

procedure TdmCatalogos.cdsPercepFijasAlAdquirirDatos(Sender: TObject);
begin
     cdsPercepFijas.Data := ServerCatalogo.GetPercepFijas( dmCliente.Empresa, cdsPuestos.FieldByName('PU_CODIGO').AsString );
end;

procedure TdmCatalogos.cdsPercepFijasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPercepFijas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaPercepFijas( dmCliente.Empresa, Delta, cdsPuestos.FieldByName('PU_CODIGO').AsString, ErrorCount ) );
          end;
     end;
end;

procedure TdmCatalogos.SetPuestoPercepcionesFijas;
begin
//     cdsPercepFijas.Refrescar;
//        ZBaseDlgModal_DevEx.ShowDlgModal( PercepcionesFijas_DevEx, TPercepcionesFijas_DevEx );
end;

{ cdsAreas }
procedure TdmCatalogos.cdsAreasAlAdquirirDatos(Sender: TObject);
begin
     cdsAreas.Data := ServerLabor.GetAreas( dmCliente.Empresa );
end;

{ cdsPuestosLookup }

procedure TdmCatalogos.cdsPuestosLookupAlAdquirirDatos(Sender: TObject);
begin
     with cdsPuestos do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque el traspaso de Data regresa el
          begin                          // dataset a dsBrowse
               Conectar;
               cdsPuestosLookUp.Data := Data;
          end;
     end;
end;

{ cdsPtoTools }

procedure TdmCatalogos.cdsPtoToolsAlAdquirirDatos(Sender: TObject);
begin
     cdsPtoTools.Data := ServerCatalogo.GetHerramientas( dmCliente.Empresa, cdsPuestos.FieldByName('PU_CODIGO').AsString );
end;

procedure TdmCatalogos.cdsPtoToolsAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     inherited;
     ErrorCount := 0;
     with cdsPtoTools do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          Reconcile( ServerCatalogo.GrabaHerramientas( dmCliente.Empresa,  DeltaNull, cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString, ErrorCount ) );
    end;
end;

//DevEx (by am): Se muestra la forma correspondiente a la vista en la que se encuentre el usuario
procedure TdmCatalogos.SetPuestoHerramientas;
begin
//         ZBaseDlgModal_DevEx.ShowDlgModal( FHerramientas_DevEx, TFHerramientas_DevEx );
end;

procedure TdmCatalogos.CargaListaHerramientas( Lista: TStrings );
var
   sCodigo, sOldIndex: String;
   iPos: Integer;
begin
     with cdsTools do
     begin
          Conectar;
          sOldIndex := IndexFieldNames;
          try
             IndexFieldNames := 'TO_DESCRIP';
             First;
             with Lista do
             begin
                  BeginUpdate;
                  try
                     Clear;
                     while not Eof do
                     begin
                          AddObject( FieldByName( 'TO_CODIGO' ).AsString + '=' + FieldByName( 'TO_DESCRIP' ).AsString, TObject( 0 ) );
                          Next;
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
          finally
             IndexFieldNames := sOldIndex;
          end;
     end;
     with cdsPtoTools do
     begin
          Refrescar;
          First;
          with Lista do
          begin
               BeginUpdate;
               try
                  while not Eof do
                  begin
                       sCodigo := FieldByName( 'TO_CODIGO' ).AsString;
                       iPos := IndexOfName( sCodigo );
                       if ( iPos < 0 ) then
                          AddObject( sCodigo + '=' + ' ???', TObject( 1 ) )
                       else
                           Objects[ iPos ] := TObject( 1 );
                       Next;
                  end;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

procedure TdmCatalogos.DescargaHerramientas( Lista: TStrings );
var
   i: Integer;
   sCodigo, sPuesto: String;
begin
     sPuesto := cdsPuestos.FieldByName('PU_CODIGO').AsString;
     cdsPtoTools.EmptyDataset;
     with Lista do
     begin
          BeginUpdate;
          try
             for i := 0 to ( Count - 1 ) do
             begin
                  if ( Integer( Lista.Objects[ i ] ) > 0 ) then
                  begin
                       sCodigo := Names[ i ];
                       with cdsPtoTools do
                       begin
                            Append;
                            FieldByName( 'PU_CODIGO' ).AsString := sPuesto;
                            FieldByName( 'TO_CODIGO' ).AsString := sCodigo;
                            Post;
                       end;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
     cdsPtoTools.Enviar;
end;

{cdsSesiones}
procedure TdmCatalogos.cdsSesionesAlAdquirirDatos(Sender: TObject);
begin
     ObtieneSesiones;
end;

procedure TdmCatalogos.ObtieneSesiones;
begin
     with cdsSesiones do
     begin
          Data := ServerCatalogo.GetSesiones( dmCliente.Empresa, FCodigoCursoSesion, FFecIniSesion, FFecFinSesion, FFolioSesion );
     end;
end;

procedure TdmCatalogos.cdsSesionesAlModificar(Sender: TObject);
begin
     //ZBaseEdicion.ShowFormaEdicion( EditSesion, TEditSesion );
end;

procedure TdmCatalogos.cdsSesionesNewRecord(DataSet: TDataSet);
begin
     if strLleno( FCodigoCursoSesion ) and ( FCodigoCursoSesion <> cdsCursos.FieldByName('CU_CODIGO').AsString ) then   // Ya est� conectado cdsCursos desde el AlCrearCampos de cdsSesiones
        cdsCursos.Locate( 'CU_CODIGO', FCodigoCursoSesion, [] );   // Debe estar posicionado
     with cdsSesiones do
     begin
          FieldByName( 'SE_FEC_INI' ).AsDateTime := Date;
          FieldByName( 'SE_FEC_FIN' ).AsDateTime := Date;
          //FieldByName( 'SE_HOR_INI' ).AsString := FormatDateTime( 'hhnn', Now );
          //FieldByName( 'SE_HOR_FIN' ).AsString := FormatDateTime( 'hhnn', Now );
          FieldByName( 'SE_HORAS' ).AsFloat := cdsCursos.FieldByName( 'CU_HORAS' ).AsFloat;
          FDuracion_Curso := FieldByName( 'SE_HORAS' ).AsFloat;
          FieldByName( 'CU_CODIGO' ).AsString := cdsCursos.FieldByName('CU_CODIGO').AsString;
          FieldByName( 'MA_CODIGO' ).AsString := cdsCursos.FieldByName('MA_CODIGO').AsString;
          FieldByName( 'SE_REVISIO' ).AsString := cdsCursos.FieldByName( 'CU_REVISIO' ).AsString;
          FieldByName( 'SE_COSTO1' ).AsFloat := cdsCursos.FieldByName( 'CU_COSTO1' ).AsFloat;
          FieldByName( 'SE_COSTO2' ).AsFloat := cdsCursos.FieldByName( 'CU_COSTO2' ).AsFloat;
          FieldByName( 'SE_COSTO3' ).AsFloat := cdsCursos.FieldByName( 'CU_COSTO3' ).AsFloat;
     end;
end;

procedure TdmCatalogos.cdsSesionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorKCCount, iChangeKCCount: Integer;
   iFolio: Integer;
   oSesion, KCResults: OleVariant;
begin
     with cdsHisCursos do
     begin
          PostData;
{
          if State in [dsEdit, dsInsert] then
             Post;
}
          iChangeKCCount := ChangeCount;
     end;
     ErrorCount := 0;
     ErrorKCCount := 0;
     with cdsSesiones do
     begin
          PostData;
{
          if State in [ dsEdit, dsInsert ] then
             Post;
}
          if ( ( ChangeCount > 0 ) or ( iChangeKCCount > 0 ) ) then
          begin
               ErrorCount := ChangeCount;
               ErrorKCCount := iChangeKCCount;
               DisableControls;
               try
                  oSesion := ServerCatalogo.GrabaSesiones( dmCliente.Empresa, DeltaNull, cdsHisCursos.DeltaNull, iFolio, Ord( ocIgnorar), ErrorCount, ErrorKCCount, KCResults );
                  if (  ( ChangeCount = 0 ) or Reconcile( oSesion )  )then
                  begin
                       if( iFolio > 0)then
                       begin
                            Edit;
                            FieldByName('SE_FOLIO').AsInteger := iFolio;
                            Post;
                            MergeChangeLog;
                            FMaxFolioSesion := iFolio;
                       end;
                       if( ( iChangeKCCount = 0 ) or cdsHisCursos.Reconcile( KCResults ) )then
                       begin
                            TressShell.SetDataChange( [ enKarCurso, enSesion ] );
                       end
                       else
                       begin
                            if ( cdsHisCursos.ChangeCount > 0 ) then
                            begin
                                 Edit;
                            end;
                       end;
                  end
                  else
                  begin
                       if ( ChangeCount > 0 ) then
                       begin
                            Edit;
                       end;
                  end;
               finally
                 EnableControls;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsSesionesAlBorrar(Sender: TObject);
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ? ' ) then
     begin
          cdsSesiones.Delete;
     end;
end;

procedure TdmCatalogos.cdsSesionesAfterDelete(DataSet: TDataSet);
begin
     cdsSesiones.Enviar;
end;

procedure TdmCatalogos.cdsSesionesAlCrearCampos(Sender: TObject);
begin
     cdsCursos.Conectar;
     cdsMaestros.Conectar;
     with cdsSesiones do
     begin
          CreateSimpleLookup( cdsCursos, 'CU_NOMBRE', 'CU_CODIGO' );
          CreateSimpleLookup( cdsMaestros, 'MA_NOMBRE', 'MA_CODIGO' );
          CreateCalculated( 'SE_COSTOT', ftFloat, 0 );
          MaskPesos( 'SE_COSTOT' );
     end;
end;

procedure TdmCatalogos.cdsSesionesCalcFields(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'SE_COSTOT' ).AsFloat := FieldByName( 'SE_COSTO1' ).AsFloat +
                                                FieldByName( 'SE_COSTO2' ).AsFloat +
                                                FieldByName( 'SE_COSTO3' ).AsFloat;
     end;
end;

procedure TdmCatalogos.cdsSesionesGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CAPA_SESIONES, iRight );
end;

procedure TdmCatalogos.cdsSesionesBeforePost(DataSet: TDataSet);
begin
     with cdsSesiones do
     begin
          FieldByName('US_CODIGO').AsInteger:= dmCliente.Usuario;
          if( FieldByName('SE_FEC_FIN').AsDateTime < FieldByName('SE_FEC_INI').AsDateTime )then
          begin
               FieldByName('SE_FEC_FIN').FocusControl;
               DataBaseError( 'La Fecha De Inicio Debe Der Menor o Igual A La Fecha De Terminaci�n' );
          end;
          if( strVacio( FieldByName('CU_CODIGO').AsString ) )then
              DatabaseError( ' El C�digo Del Curso No Puede Quedar Vac�o ');
     end;
end;

{cdsHisCursos}
procedure TdmCatalogos.cdsHisCursosAfterDelete(DataSet: TDataSet);
begin
     with cdsSesiones do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmCatalogos.cdsHisCursosAfterEdit(DataSet: TDataSet);
begin
     with cdsSesiones do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmCatalogos.ConectacdsHisCursos( const iFolio: Integer );
begin
     with cdsHisCursos do
     begin
          Data := ServerCatalogo.GetHisCursos( dmCliente.Empresa, iFolio );
     end;
end;

procedure TdmCatalogos.cdsHisCursosAlAdquirirDatos(Sender: TObject);
begin
     ConectacdsHisCursos( cdsSesiones.FieldByname('SE_FOLIO').AsInteger );
end;

procedure TdmCatalogos.cdsHisCursosBeforePost(DataSet: TDataSet);
begin
     with cdsHisCursos do
     begin
          FieldByName('KC_FEC_TOM').AsDateTime := cdsSesiones.FieldByName('SE_FEC_INI').AsDateTime;
          FieldByName('KC_FEC_FIN').AsDateTime := cdsSesiones.FieldByName('SE_FEC_FIN').AsDateTime;
          if ( FieldByName( 'CB_CODIGO' ).AsInteger = 0 ) then
               DataBaseError( 'N�mero de Empleado No Puede Quedar Vac�o' );
          if( FieldByName('KC_HORAS').AsFloat <= 0 )then
              DataBaseError( 'Las Horas Deben Ser Mayores A Cero' );
          if( FieldByName('KC_EVALUA').AsFloat < 0 )then
              DataBaseError( 'La Evaluaci�n Debe Ser Mayor o Igual A Cero' );
          FieldByName('CU_CODIGO').AsString := cdsSesiones.FieldByName('CU_CODIGO').AsString;
          FieldByName('SE_FOLIO').AsInteger := cdsSesiones.FieldByName('SE_FOLIO').AsInteger;
          //FieldByName('KC_REVISIO').AsString := cdsSesiones.FieldByName('SE_REVISIO').AsString;
     end;
end;

{$ifdef VER130}
procedure TdmCatalogos.cdsHisCursosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError : String;
begin
    if PK_Violation( E ) then
    begin
      with DataSet do
        sError := Format( 'El curso ya fu� registrado.' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s',
                    [ FieldByName( 'CB_CODIGO' ).AsInteger, FechaCorta( FieldByName( 'KC_FEC_TOM' ).AsDateTime ) ] );
    end
    else
        sError := GetErrorDescription( E );
    cdsHisCursos.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO', sError );
    Action := raCancel;
end;
{$else}
procedure TdmCatalogos.cdsHisCursosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError : String;
begin
    if PK_Violation( E ) then
    begin
      with DataSet do
        sError := Format( 'El curso ya fu� registrado.' + CR_LF + 'Empleado: %d' + CR_LF + 'Fecha: %s',
                    [ FieldByName( 'CB_CODIGO' ).AsInteger, FechaCorta( FieldByName( 'KC_FEC_TOM' ).AsDateTime ) ] );
    end
    else
        sError := GetErrorDescription( E );
    cdsHisCursos.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO', sError );
    Action := raCancel;
end;
{$endif}

procedure TdmCatalogos.cdsGridCB_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No Existe el Empleado #' + IntToStr( AsInteger ) );
          end
          else
              DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' );
     end;
end;

procedure TdmCatalogos.CursosCB_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          with dmCliente do
          begin
               FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
          end;
          with dmRecursos.GetDatosClasificacion( cdsHisCursos.FieldByName('CB_CODIGO').AsInteger, dmCliente.FechaDefault ) do
          begin
               FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
               FieldByName( 'CB_TURNO' ).AsString := Turno;
               FieldByName( 'CB_PUESTO' ).AsString := Puesto;
               FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
               FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
               FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
               FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
               FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
               FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
               FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
               FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
               FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
{$ifdef ACS}
               FieldByName( 'CB_NIVEL10' ).AsString := Nivel10;
               FieldByName( 'CB_NIVEL11' ).AsString := Nivel11;
               FieldByName( 'CB_NIVEL12' ).AsString := Nivel12;
{$endif}
          end;
     end;
end;

procedure TdmCatalogos.cdsHisCursosAlCrearCampos(Sender: TObject);
begin
     with cdsHisCursos do
     begin
          MaskHoras( 'KC_HORAS' );
          MaskHoras( 'KC_EVALUA' );
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := CursosCB_CODIGOChange;
     end;
end;

procedure TdmCatalogos.cdsHisCursosNewRecord(DataSet: TDataSet);
begin
{
     with cdsSesiones do
     begin
          cdsHisCursos.FieldByName('KC_HORAS').AsFloat := FDuracion_Curso;//MiRound( FieldbyName( 'SE_HORAS' ).AsFloat, 2 );
     end;
}
     cdsHisCursos.FieldByName( 'KC_HORAS' ).AsFloat := FDuracion_Curso;//MiRound( FieldbyName( 'SE_HORAS' ).AsFloat, 2 );

end;

procedure TdmCatalogos.ShowCalendarioRitmo( const sTurno: TCodigo );
begin
//     cdsTurnos.Conectar;
//     FToolsCatalogos.MuestraCalendarioRitmo( sTurno, cdsTurnos );
end;

{
procedure TdmCatalogos.ShowCalendarioRitmo( const sTurno: TCodigo );
const
     K_UN_DIA = 1;
     K_FILLER = '0';
var
   i: Integer;
   sHorario, sPatron: String;
   lHabil, lHabilAnterior: Boolean;
   dInicio: TDate;
   oDatosRitmo: TDatosRitmo;
begin
     with oDatosRitmo do
     begin
          Inicio := NullDateTime;
          Patron := VACIO;
          Horario1 := VACIO;
          Horario2 := VACIO;
     end;
     cdsHorarios.Conectar;
     with cdsTurnos do
     begin
          if ( FieldByName( 'TU_CODIGO' ).AsString = sTurno ) or Locate( 'TU_CODIGO', sTurno, [] ) then
          begin
               with oDatosRitmo do
               begin
                    Horario1 := FieldByName( 'TU_HOR_1' ).AsString;
                    Horario2 := FieldByName( 'TU_HOR_2' ).AsString;
               end;
               if ZetaCommonTools.StrLleno( FieldByName( 'TU_RIT_PAT' ).AsString ) then
               begin
                    with oDatosRitmo do
                    begin
                         Inicio := FieldByName( 'TU_RIT_INI' ).AsDateTime;
                         Patron := FieldByName( 'TU_RIT_PAT' ).AsString;
                    end;
               end
               else
               begin
                    lHabilAnterior := False;
                    for i := 1 to ZetaCommonClasses.K_DIAS_SEMANA do
                    begin
                         sHorario := FieldByName( Format( 'TU_HOR_%d', [ i ] ) ).AsString;
                         lHabil := ( eStatusAusencia( FieldByName( Format( 'TU_TIP_%d', [ i ] ) ).AsInteger ) = auHabil );
                         if ( i > 1 ) and ( lHabil = lHabilAnterior ) then
                            sPatron := ZetaCommonTools.ConcatString( sPatron, K_FILLER, ',' );
                         sPatron := ZetaCommonTools.ConcatString( sPatron, Format( '%d:%s', [ K_UN_DIA, sHorario ] ), ',' );
                         lHabilAnterior := lHabil;
                    end;
                    dInicio := EncodeDate( 1990, 1, 1 );
                    with oDatosRitmo do
                    begin
                         Inicio := dInicio + ( Global.GetGlobalInteger( K_GLOBAL_PRIMER_DIA ) - DayOfWeek( dInicio ) );
                         Patron := sPatron;
                    end;
               end;
          end;
          FCalendarioRitmo.ShowCalendarioRitmo( sTurno, FieldByName( 'TU_DESCRIP' ).AsString, oDatosRitmo );
     end;
end;
}

{
ER ( 15/Sep/2004 ) - Version 2.6 - Ya no se calcula la duraci�n para que tenga el mismo comportamiento que
en Registro de Sesiones - "FGridCursos"

procedure TdmCatalogos.SESIONHORASChange(Sender: TField);

var
   iHoras: Integer;
   dInicial, dFinal: TDateTime;
   iHour, iMin, iSec, iMSec: Word;
begin
     if Assigned( Sender ) then
     begin
          with Sender.DataSet do
          begin
               iHoras := ( aMinutos( FieldByName('SE_HOR_INI').AsString ) );
               dInicial := FieldByName('SE_FEC_INI').AsDateTime + EncodeTime( iHoras Div 60, iHoras mod 60, 0, 0 );
               iHoras := ( aMinutos( FieldByName('SE_HOR_FIN').AsString ) );
               dFinal := FieldByName('SE_FEC_FIN').AsDateTime + EncodeTime( iHoras Div 60, iHoras mod 60, 0, 0 );
               dFinal := ( dFinal - dInicial );
               if( dFinal > 0 )then
               begin
                    DecodeTime(( dFinal - Trunc( dFinal ) ), iHour, iMin, iSec, iMSec);
                    FieldByName('SE_HORAS').AsFloat := rMax( 24 * Trunc(dFinal) + iHour + (iMin/60) , 0 );
               end
               else
               begin
                    FieldByName('SE_HORAS').AsFloat := 0;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsSesionesAfterOpen(DataSet: TDataSet);
begin
     with cdsSesiones do
     begin
          FieldByName('SE_FEC_INI').OnChange := SESIONHORASChange;
          FieldByName('SE_FEC_FIN').OnChange := SESIONHORASChange;
          FieldByName('SE_HOR_INI').OnChange := SESIONHORASChange;
          FieldByName('SE_HOR_FIN').OnChange := SESIONHORASChange;
     end;
end;
}

procedure TdmCatalogos.cdsAccReglasAlAdquirirDatos(Sender: TObject);
begin
     cdsAccReglas.Data := ServerCatalogo.GetAccReglas( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsAccReglasAlModificar(Sender: TObject);
begin

//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatAccReglas_DevEx, TEditCatAccReglas_DevEx );
end;

procedure TdmCatalogos.cdsAccReglasBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'AE_CODIGO' );
     DataSet.FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmCatalogos.cdsAccReglasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_ACCESOS_REGLAS, iRight );
end;

procedure TdmCatalogos.cdsAccReglasNewRecord(DataSet: TDataSet);
begin
     with cdsAccReglas do
     begin
          FieldByName( 'AE_ACTIVO' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmCatalogos.cdsAccReglasAlCrearCampos(Sender: TObject);
begin
     with cdsAccReglas do
     begin
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

{ cdsAulas }
procedure TdmCatalogos.cdsAulasAlAdquirirDatos(Sender: TObject);
begin
     cdsAulas.Data := ServerCatalogo.GetAulas( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsAulasAlModificar(Sender: TObject);
begin

//     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatAulas_DevEx, TEditCatAulas_DevEx );
end;

procedure TdmCatalogos.cdsAulasBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'AL_CODIGO' );
end;

procedure TdmCatalogos.cdsAulasGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CAPA_AULAS, iRight );
end;

procedure TdmCatalogos.cdsAulasNewRecord(DataSet: TDataSet);
begin
     cdsAulas.FieldByName('AL_ACTIVA').AsString := K_GLOBAL_SI;
end;

{ ***************** cdsPrerequisitos ************************** }
procedure TdmCatalogos.cdsPrerequisitosCursoAlAdquirirDatos( Sender: TObject);
begin
     cdsCursos.Conectar;
     cdsPrerequisitosCurso.Data := ServerCatalogo.GetPrerequisitosCurso( dmCliente.Empresa, FPrerequisitosCurso );
end;

procedure TdmCatalogos.cdsPrerequisitosCursoAlCrearCampos(Sender: TObject);
begin
     with cdsPrerequisitosCurso do
     begin
          CreateSimpleLookup( cdsCursos, 'CU_NOMBRE', 'CP_CURSO' );
     end;
end;

procedure TdmCatalogos.cdsPrerequisitosCursoNewRecord(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'CU_CODIGO' ).AsString := FPrerequisitosCurso;
          FieldByName( 'CP_OPCIONA' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsPrerequisitosCursoAlModificar(Sender: TObject);
begin

//      ZBaseEdicion_DevEx.ShowFormaEdicion( EditPrerequisitosCurso_DevEx, TEditPrerequisitosCurso_DevEx )
end;

procedure TdmCatalogos.cdsPrerequisitosCursoBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( cdsPrerequisitosCurso, 'CP_CURSO' );
     with cdsPrerequisitosCurso do
     begin
          if( FieldByName('CU_CODIGO').AsString = FieldByName('CP_CURSO').AsString )then
              DataBaseError( 'Un curso no puede ser prerequisito de el mismo' );
     end;
end;

{ cdsTPeriodos }

procedure TdmCatalogos.cdsTPeriodosAlAdquirirDatos(Sender: TObject);
begin
     cdsTPeriodos.Data := ServerCatalogo.GetTPeriodos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsTPeriodosAlModificar(Sender: TObject);
begin

//     	ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatTPeriodos_DevEx, TEditCatTPeriodos_DevEx );
 end;


{ ***************** cdsCertificaciones ************************** }

procedure TdmCatalogos.cdsCertificacionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCertificaciones.Data := ServerCatalogo.GetCertificaciones( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCertificacionesAlModificar(Sender: TObject);
begin

//        ZbaseEdicion_DevEx.ShowFormaEdicion( EditCatCertificaciones_DevEx, TEditCatCertificaciones_DevEx );
end;

procedure TdmCatalogos.cdsCertificacionesBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( cdsCertificaciones, 'CI_CODIGO' );
     with cdsCertificaciones do
     begin
          if FieldByName('CI_RENOVAR').AsInteger < 0 then
             DataBaseError('La renovaci�n de la certificaci�n debe ser Mayor o igual a Cero(0)');
          if State in [ dsEdit ] then
          begin
               if StrVacio( FieldByName( 'CI_DETALLE' ).AsString )then
                  FieldByName( 'CI_DETALLE' ).AsString := K_ESPACIO;
          end;
     end;
end;

procedure TdmCatalogos.cdsCertificacionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCertificaciones do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmCatalogos.cdsCertificacionesAfterDelete(DataSet: TDataSet);
begin
     cdsCertificaciones.Enviar;
end;

procedure TdmCatalogos.cdsCertificacionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_CERTIFICACIONES, iRight );
end;

procedure TdmCatalogos.cdsCertificacionesNewRecord(DataSet: TDataSet);
begin
     DataSet.FieldByName('CI_ACTIVO').AsString:= K_GLOBAL_SI;
end;


{ ***************** cdsTiposPoliza ************************** }

procedure TdmCatalogos.cdsTiposPolizaAlAdquirirDatos(Sender: TObject);
begin
     cdsTiposPoliza.Data := ServerCatalogo.GetTiposPoliza( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsTiposPolizaAlModificar(Sender: TObject);
begin

//      ZbaseEdicion_DevEx.ShowFormaEdicion( EditCatTiposPoliza_DevEx, TEditCatTiposPoliza_DevEx );
end;

procedure TdmCatalogos.cdsTiposPolizaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsTiposPoliza do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmCatalogos.cdsTiposPolizaBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( cdsTiposPoliza, 'PT_CODIGO' );
     with DataSet do
     begin
          if( FieldByName('PT_NOMBRE').AsString = '')then
          begin
               DataBaseError( 'El Nombre de la P�liza no puede quedar vac�o' );
          end;
          if(  FieldByName('PT_REPORTE').AsInteger = 0)then
          begin
               DataBaseError( 'El Formato de la p�liza no puede quedar vac�o o ser cero' );
          end;
     end;
end;

procedure TdmCatalogos.cdsTiposPolizaGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_NOMINA_POLIZAS, iRight );
end;

procedure TdmCatalogos.cdsTiposPolizaAfterDelete(DataSet: TDataSet);
begin
     cdsTiposPoliza.Enviar;
end;

{ ***************** cdsPerfilesPuesto ************************** }

procedure TdmCatalogos.cdsPerfilesPuestoAlAdquirirDatos(Sender: TObject);
begin
     cdsPerfilesPuesto.Data := ServerCatalogo.GetPerfilesPuesto( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsPerfilesPuestoAlModificar(Sender: TObject);
begin
//     FAgregandoPerfil := FALSE;
//     with cdsPerfiles do
//     begin
//          Refrescar;
//          Modificar;
//     end;
end;

procedure TdmCatalogos.cdsPerfilesPuestoAlAgregar(Sender: TObject);
begin
     FAgregandoPerfil := TRUE;
     with cdsPerfiles do
     begin
          Refrescar;
          Agregar;
     end;
end;

procedure TdmCatalogos.cdsPerfilesPuestoAfterDelete(DataSet: TDataSet);
begin
    cdsPerfilesPuesto.Enviar;
end;

procedure TdmCatalogos.cdsPerfilesPuestoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPerfilesPuesto do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, cdsPerfiles.Tag, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmCatalogos.ValidaMemoField( DataSet : TDataSet; const sField : string );
begin
     with DataSet do
     begin
          if( State = dsEdit ) then
          begin
               with FieldByName(sField) do
               begin
                    if StrVacio(AsString) then
                       AsString := K_ESPACIO;
               end;
          end;
     end;
end;

{cdsValPuntos}

procedure TdmCatalogos.cdsValPuntosAfterDelete(DataSet: TDataSet);
begin
     with cdsValuaciones do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmCatalogos.cdsValPuntosAfterEdit(DataSet: TDataSet);
begin
     with cdsValuaciones do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmCatalogos.ConectacdsValPuntos( const iFolio: Integer );
begin
     with cdsValPuntos do
     begin
          Data := ServerCatalogo.GetValPuntos( dmCliente.Empresa, iFolio );
     end;
end;

procedure TdmCatalogos.cdsValPuntosAlAdquirirDatos(Sender: TObject);
begin
     cdsSubfactores.Refrescar;
     ConectacdsValPuntos( cdsValuaciones.FieldByName('VP_FOLIO').AsInteger );
     cdsValFactores.Conectar;
end;

procedure TdmCatalogos.cdsValPuntosAlCrearCampos(Sender: TObject);
begin
     cdsValPuntos.CreateSimpleLookup( cdsSubfactores, 'VS_NOMBRE', 'VT_ORDEN' );
     cdsValPuntos.FieldByName( 'VT_CUAL' ).onChange := VT_CUALChange;
end;

procedure TdmCatalogos.cdsValPuntosBeforePost(DataSet: TDataSet);
begin
     ValidaMemoField( DataSet, 'VT_COMENTA');
end;

procedure TdmCatalogos.VT_CUALChange(Sender: TField);
begin
     if Sender.AsInteger > 0 then
        cdsValPuntos.FieldByName( 'VT_PUNTOS' ).AsFloat := cdsPuntosNivel.FieldByName('VN_PUNTOS').AsFloat
     else
         cdsValPuntos.FieldByName( 'VT_PUNTOS' ).AsFloat := 0;
end;

procedure TdmCatalogos.ObtieneTotalesValuacion( var rPuntos : Double; var iRespuestas: Integer );
begin
     rPuntos:= 0;
     iRespuestas:= 0;
     with cdsValPuntos do
     begin
          DisableControls;
          try
             First;
             while not Eof do
             begin
                  { AP: Se cambi� para que validara el campo correcto
                  if ( FieldByName('VT_PUNTOS').AsFloat <> 0 ) then, }
                  if ( FieldByName('VT_CUAL').AsFloat <> 0 ) then
                  begin
                       rPuntos:= rPuntos + FieldByName('VT_PUNTOS').AsFloat;
                       Inc( iRespuestas );
                  end;
                  Next;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

{cdsValuaciones}

procedure TdmCatalogos.RefrescarValuacion( Parametros: TZetaParams );
var
   Pos: TBookMark;
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsValuaciones do
     begin
          Pos:= GetBookMark;
          Data := ServerCatalogo.GetValuaciones( dmCliente.Empresa, Parametros.VarValues );
          if ( Pos <> nil ) then
          begin
               if BookMarkValid( Pos ) then
                  GotoBookMark( Pos );
               FreeBookMark( Pos );
          end;
     end;
end;

function TdmCatalogos.PlantillaEnDiseno( const iPlantilla: Integer; var sMensaje: String; const sAccion: String ): Boolean;
begin
     with cdsValPlantilla do
     begin
          Result:= ( Locate('VL_CODIGO', iPlantilla ,[] ) ) and
                   ( FieldByName('VL_STATUS').AsInteger = Ord(spDiseno) );
          if not Result then
             sMensaje:= Format( 'No se Puede %s a una Plantilla que No este en Dise�o', [ sAccion ] );
     end;
end;

function TdmCatalogos.PlantillaLiberada( const iPlantilla: Integer; var sMensaje: String; const sAccion: String ): Boolean;
begin
     with cdsValPlantilla do
     begin
          Result:= ( Locate('VL_CODIGO', iPlantilla ,[] ) ) and
                   ( FieldByName('VL_STATUS').AsInteger = Ord(spLiberada) );
          if not Result then
             sMensaje:= Format( 'No se Puede %s a una Plantilla que No este Liberada', [ sAccion ] );
     end;
end;

procedure TdmCatalogos.cdsValuacionesAlCrearCampos(Sender: TObject);
begin
     with cdsValuaciones do
     begin
          CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'PU_CODIGO' );
          CreateSimpleLookup( cdsValPlantilla, 'VL_NOMBRE', 'VL_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          MaskFecha( 'VP_FECHA' );
          MaskPesos( 'VP_PUNTOS' );
          ListaFija( 'VP_STATUS', lfStatusValuacion );
     end;
end;

procedure TdmCatalogos.cdsValuacionesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     if UK_Violation( E ) then
     begin
          with DataSet do
               sError := Format('� Valuaci�n ya Registrada con el Puesto: %s y Fecha: %s !',[  FieldByName('PU_CODIGO').AsString ,
                                                                                                                             FechaCorta( FieldByName('VP_FECHA').AsDateTime ) ] );
     end
     else
         sError := GetErrorDescription( E );
    cdsValuaciones.ReconciliaError(DataSet,UpdateKind,E,'VP_FOLIO', sError );
    Action := raCancel;
end;

procedure TdmCatalogos.AgregaCopiaValuacion( Parametros: TZetaParams );
var
   ErrorCount: Integer;
   iFolio:Integer;
begin
     ErrorCount := 0;
     iFolio:= Parametros.ParamByName('Folio').AsInteger;
     with cdsValuaciones do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if ( iFolio = 0 ) then
               begin
                    if ( Reconcile( ServerCatalogo.AgregaValuacion( dmCliente.Empresa, Delta, ErrorCount, iFolio, Parametros.ParamByName('Plantilla').AsInteger ) ) ) then
                    begin
                         TressShell.SetDataChange( [ enValuacion ] );
                    end;
               end
               else
               begin
                    iFolio := ServerCatalogo.CopiaValuacion( dmCliente.Empresa, Parametros.VarValues, ErrorCount ) ;
                    TressShell.SetDataChange( [ enValuacion ] );
               end;
          end;

          if( iFolio > 0 ) then
          begin
               {***DevEx(@am): Anteriormente se modificaba el valor para no tener que refrescar todo el cds, sin embargo
               como VP_FOLIO es un campo Identity y XE5 si detecta cuando estos campos son Identity, ya no nos permite modificarlos ***}
               {Edit;
               FieldByName('VP_FOLIO').AsInteger := iFolio;
               Post;
               MergeChangeLog;
               } //old
               cdsValuaciones.Refrescar;
               cdsValuaciones.Locate('VP_FOLIO',iFolio,[]);
               try
                  Modificar;
               finally
                      FAgregandoVal := TRUE;
               end;

          end;
     end;
end;

procedure TdmCatalogos.cdsValuacionesNewRecord(DataSet: TDataSet);
begin
     with cdsValuaciones do
     begin
          FieldByName('VL_CODIGO').AsInteger:= PlantillaActiva;
          FieldByName('US_CODIGO').AsInteger:= dmCliente.Usuario;
          FieldByName('VP_FECHA').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('VP_FEC_INI').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('VP_FEC_FIN').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('VP_STATUS').AsInteger:= Ord( svProceso );
     end;
end;

procedure TdmCatalogos.cdsValuacionesAlModificar(Sender: TObject);
begin
//     {$IFNDEF DOS_CAPAS}
//          ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatValuaciones_DevEx, TEditCatValuaciones_DevEx );
//     {$ENDIF}
end;

procedure TdmCatalogos.cdsValuacionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorKCCount, iChangeKCCount: Integer;
   iFolio,  iRespuestas: Integer;
   rPuntos: Double;
   oValuacion, KCResults: OleVariant;
   lEditando: Boolean;
   Pos: TBookMark;
begin
     with cdsValPuntos do
     begin
          PostData;
          iChangeKCCount := ChangeCount;
     end;
     ErrorCount := 0;
     ErrorKCCount := 0;
     with cdsValuaciones do
     begin
          lEditando := ( State in [ dsEdit, dsInsert ] );
          PostData;
          if ( ( ChangeCount > 0 ) or ( iChangeKCCount > 0 ) ) then
          begin
               ErrorCount := ChangeCount;
               ErrorKCCount := iChangeKCCount;
               DisableControls;
               try
                  oValuacion := ServerCatalogo.GrabaValuacion( dmCliente.Empresa, DeltaNull, cdsValPuntos.DeltaNull, iFolio, ErrorCount, ErrorKCCount, KCResults );
                  if (  ( ChangeCount = 0 ) or Reconcile( oValuacion )  )then
                  begin
                       if( ( iChangeKCCount = 0 ) or cdsValPuntos.Reconcile( KCResults ) )then
                       begin
                            if lEditando then
                            begin
                                 Pos:= cdsValPuntos.GetBookMark;
                                 try
                                    ObtieneTotalesValuacion( rPuntos, iRespuestas );
                                    cdsValPuntos.GotoBookMark( Pos );
                                 finally
                                     cdsValPuntos.FreeBookMark( Pos );
                                 end;
                                 Edit;
                                 FieldByName( 'VP_PUNTOS' ).AsFloat := rPuntos;
                                 FieldByName( 'VP_NUM_FIN' ).AsInteger := iRespuestas;
                                 Post;
                                 MergeChangeLog;
                            end;
                             { AP: Se cambi� porque daba problemas con el bookmark cuando estaba filtrado por puesto}
                            TressShell.SetDataChange( [ enValuacion ] );
                       end
                       else
                       begin
                            if ( cdsValPuntos.ChangeCount > 0 ) then
                            begin
                                 Edit;
                            end;
                       end;
                  end
                  else
                  begin
                       if ( ChangeCount > 0 ) then
                       begin
                            Edit;
                       end;
                  end;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsValuacionesAfterDelete(DataSet: TDataSet);
begin
     cdsValuaciones.Enviar;
end;

procedure TdmCatalogos.cdsValuacionesBeforePost(DataSet: TDataSet);
begin
     ValidaMemoField( DataSet, 'VP_COMENTA');
end;

procedure TdmCatalogos.cdsValuacionesAfterCancel(DataSet: TDataSet);
begin
     with cdsValPuntos do
     begin
          if ( State <> dsInactive ) then
             CancelUpdates;
     end;
end;

 {cdsValPlantilla}

procedure TdmCatalogos.SetPlantillaActiva( const Value: Integer );
begin
     if ( FPlantillaActiva <> Value ) then
     begin
          FPlantillaActiva := Value;
          TressShell.SetDataChange( [ enValNiveles ] ); //Al cambiar la plantilla activa avisar� a todas los
                                                        //datasets que cambi� ( enValNiveles tiene �stos)
     end;
end;

procedure TdmCatalogos.cdsValPlantillaAlAdquirirDatos(Sender: TObject);
begin
     with cdsValPlantilla do
     begin
          Data:= ServerCatalogo.GetValPlantilla( dmCliente.Empresa );
          if ( PlantillaActiva = 0 ) then
          begin
               Last;
               PlantillaActiva:= FieldByName('VL_CODIGO').AsInteger;
          end;
     end;
end;

procedure TdmCatalogos.cdsValPlantillaAlModificar(Sender: TObject);
begin
//     {$IFNDEF DOS_CAPAS}
//
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatValPlantillas_DevEx, TEditCatValPlantillas_DevEx );
//     {$ENDIF}
end;

procedure TdmCatalogos.cdsValPlantillaAlCrearCampos(Sender: TObject);
begin
      with cdsValPlantilla do
      begin
           MaskFecha('VL_FECHA');
           ListaFija( 'VL_STATUS', lfStatusPlantilla );
           MaskPesos( 'VL_MAX_PTS');
      end;
end;

procedure TdmCatalogos.cdsValPlantillaBeforePost(DataSet: TDataSet);
begin
     ValidaMemoField( DataSet, 'VL_COMENTA');
end;

procedure TdmCatalogos.cdsValPlantillaNewRecord(DataSet: TDataSet);
begin
     cdsValPlantilla.FieldByName('VL_FECHA').AsDateTime:= dmCliente.FechaDefault;
end;

procedure TdmCatalogos.cdsValPlantillaGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_VAL_PLANTILLAS, iRight );
end;

procedure TdmCatalogos.cdsValPlantillaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   lInsertando: Boolean;
begin
     ErrorCount := 0;
     with cdsValPlantilla do
     begin
          lInsertando:= ( State = dsInsert );
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if ( Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) ) then
               begin
                    TressShell.SetDataChange( [ enValPlant ] );
                    if ( lInsertando ) then
                    begin
                         Refrescar;
                         Last;
                         PlantillaActiva:= FieldByName('VL_CODIGO').AsInteger;
                    end;
               end;
          end;
     end;
end;


procedure TdmCatalogos.cdsValPlantillaAlBorrar(Sender: TObject);
begin
     if ZetaDialogo.ZWarningConfirm('� Advertencia !', 'Al Borrar la Plantilla se borrar�n todos los datos incluidos en sus Factores y Subfactores ' +
                                                       'Incluyendo las Valuaciones de Puestos ya realizadas � Desea Continuar ?',
                                         0, mbCancel) then
     begin
          with cdsValPlantilla do
          begin
               Delete;
               Enviar;
          end;

     end;
end;


{cdsValFactores}

procedure TdmCatalogos.cdsValFactoresAlAdquirirDatos(Sender: TObject);
begin
     cdsValPlantilla.Conectar;
     cdsValFactores.Data:= ServerCatalogo.GetValFactores( dmCliente.Empresa, PlantillaActiva );
end;

procedure TdmCatalogos.cdsValFactoresAlCrearCampos(Sender: TObject);
begin
     cdsValFactores.MaskPesos('VF_MAX_PTS');
end;

procedure TdmCatalogos.cdsValFactoresAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iOrden: Integer;
begin
     ErrorCount := 0;
     with cdsValFactores do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCamposVal( dmCliente.Empresa, Tag, Delta, ErrorCount ,iOrden ) ) then
               begin
                    TressShell.SetDataChange( [ enValFact ] );
                    if( iOrden > 0 ) and ( ChangeCount = 0 ) then
                    begin
                         Edit;
                         FieldByName('VF_ORDEN').AsInteger := iOrden;
                         Post;
                         MergeChangeLog;
                    end;
               end;
          end;
     end;
end;


procedure TdmCatalogos.cdsValFactoresAlModificar(Sender: TObject);
begin
//     {$IFNDEF DOS_CAPAS}
//
//          ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatValFactores_DevEx, TEditCatValFactores_DevEx );
//     {$ENDIF}
end;


procedure TdmCatalogos.cdsValFactoresGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_VAL_FACTORES, iRight );
end;

procedure TdmCatalogos.cdsValFactoresNewRecord(DataSet: TDataSet);
begin
     cdsValFactores.FieldByName('VL_CODIGO').AsInteger:= PlantillaActiva;
end;

procedure TdmCatalogos.cdsValFactoresBeforePost(DataSet: TDataSet);
begin
     ValidaMemoField(DataSet,'VF_DESCRIP');
     ValidaMemoField(DataSet,'VF_DES_ING');
end;

procedure TdmCatalogos.cdsValFactoresAlBorrar(Sender: TObject);
begin
     BorraVal( cdsValFactores, 'VF_ORDEN' );
end;

procedure TdmCatalogos.BorraVal( DataSet: TZetaClientDataSet; sCampo: String );
var
   Pos: Integer;
   lEsUltimo: Boolean;
begin
     with DataSet do
     begin
          lEsUltimo:= ( RecNo = RecordCount );
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ? ' ) then
          begin
               Delete;
               Enviar;
          end;
          if not ( lEsUltimo ) then
          begin
               Pos:= FieldByName(sCampo).AsInteger - 1;
               Refrescar;
               Locate(sCampo,IntToStr(Pos),[]);
          end;
     end;
end;



{cdsSubfactores}

procedure TdmCatalogos.cdsSubfactoresAlAdquirirDatos(Sender: TObject);
var
   Pos: TBookMark;
begin
     cdsValPlantilla.Conectar;
     with cdsSubfactores do
     begin
          Pos:= GetBookMark;
          Data:= ServerCatalogo.GetValSubfactores( dmCliente.Empresa, PlantillaActiva );
          if ( Pos <> nil ) then
          begin
               if BookMarkValid( Pos ) then
                  GotoBookMark( Pos );
               FreeBookMark( Pos );
          end;
     end;
end;

procedure TdmCatalogos.cdsSubfactoresAlCrearCampos(Sender: TObject);
begin
     cdsValFactores.Conectar;
     with cdsSubfactores do
     begin
          CreateSimpleLookup( cdsValFactores, 'VF_NOMBRE', 'VF_ORDEN' );
          MaskPesos( 'VS_MAX_PTS' );
     end;
end;

procedure TdmCatalogos.cdsSubfactoresAlModificar(Sender: TObject);
begin
//     {$IFNDEF DOS_CAPAS}
//          ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatSubfactor_DevEx, TEditCatSubfactor_DevEx );
//     if( FCambioNiveles ) then
//     begin
//         try
//            TressShell.SetDataChange( [ enValNiveles ] );
//         finally
//                FCambioNiveles:= FALSE;
//         end;
//     end;
//     {$ENDIF}
end;


procedure TdmCatalogos.cdsSubfactoresAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iOrden: Integer;
begin
     ErrorCount := 0;
     with cdsSubfactores do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCamposVal( dmCliente.Empresa, Tag, Delta, ErrorCount ,iOrden ) ) then
               begin
                    TressShell.SetDataChange( [ enValSubfact ] );
                    if( iOrden > 0 ) and ( ChangeCount = 0 ) then
                    begin
                         Edit;
                         FieldByName('VS_ORDEN').AsInteger := iOrden;
                         Post;
                         MergeChangeLog;
                    end;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsSubfactoresAlBorrar(Sender: TObject);
begin
     BorraVal( cdsSubfactores,'VS_ORDEN');
end;

procedure TdmCatalogos.cdsSubfactoresGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_VAL_SUBFACTORES, iRight );
end;

procedure TdmCatalogos.cdsSubfactoresBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          ValidaMemoField(DataSet,'VS_DESCRIP');
          ValidaMemoField(DataSet,'VS_DES_ING');
          if ( FieldByName( 'VF_ORDEN' ).AsInteger = 0 ) then
          begin
               FieldByName( 'VF_ORDEN' ).FocusControl;
               DataBaseError( 'El Factor No Puede Quedar Vac�o' );
          end;
     end;
end;

procedure TdmCatalogos.cdsSubfactoresNewRecord(DataSet: TDataSet);
begin
     cdsSubfactores.FieldByName('VL_CODIGO').AsInteger:= PlantillaActiva;
end;

{cdsPuntosNivel}

procedure TdmCatalogos.cdsPuntosNivelAlAdquirirDatos(Sender: TObject);
begin
     cdsPuntosNivel.Data:= ServerCatalogo.GetValNiveles( dmCliente.Empresa, cdsValuaciones.FieldByName('VL_CODIGO').AsInteger,
                                                        0 );
end;

procedure TdmCatalogos.cdsPuntosNivelLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
//     {$IFNDEF DOS_CAPAS}
////         lOk := FBusquedaValNiveles_DevEx.BuscaNivelDialogo( Sender, sFilter, sKey, sDescription );
//     {$ENDIF}
end;

{cdsValNiveles}

procedure TdmCatalogos.cdsValNivelesAlAdquirirDatos(Sender: TObject);
begin
     with cdsSubfactores do
     begin
          //if ( FieldByName('VS_ORDEN').AsInteger <> 0 ) then
          ConectacdsValNivel( PlantillaActiva, FieldByName('VS_ORDEN').AsInteger );
     end;
end;

procedure TdmCatalogos.ConectacdsValNivel( const iPlantilla: Integer; const iSubfactor: Integer );
begin
     cdsValNiveles.Data:= ServerCatalogo.GetValNiveles( dmCliente.Empresa, iPlantilla, iSubfactor );
end;

procedure TdmCatalogos.cdsValNivelesAlModificar(Sender: TObject);
begin
//     {$IFNDEF DOS_CAPAS}
//         ZbaseEdicion_DevEX.ShowFormaEdicion( EditCatValNivel_DevEx, TEditCatValNivel_DevEx );
//     {$ENDIF}
end;

procedure TdmCatalogos.cdsValNivelesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iOrden: Integer;
begin
     ErrorCount := 0;
     with cdsValNiveles do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCamposVal( dmCliente.Empresa, Tag, Delta, ErrorCount ,iOrden ) ) then
               begin
                    if( iOrden > 0 ) and ( ChangeCount = 0 ) then
                    begin
                         Edit;
                         FieldByName('VN_ORDEN').AsInteger := iOrden;
                         Post;
                         MergeChangeLog;
                    end;
                    FCambioNiveles:= TRUE;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsValNivelesBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          ValidaMemoField(DataSet,'VN_DESCRIP');
          ValidaMemoField(DataSet,'VN_DES_ING');
     end;
end;

procedure TdmCatalogos.cdsValNivelesNewRecord(DataSet: TDataSet);
begin
     with cdsValNiveles do
     begin
          FieldByName('VL_CODIGO').AsInteger:= PlantillaActiva;
          FieldByName('VS_ORDEN').AsInteger:= cdsSubfactores.FieldByName('VS_ORDEN').AsInteger;
          FieldByName('VN_PUNTOS').AsFloat:= 0.00;
     end;
end;

procedure TdmCatalogos.cdsValNivelesAlBorrar(Sender: TObject);
begin
     BorraVal( cdsValNiveles,'VN_ORDEN');
end;

procedure TdmCatalogos.cdsValNivelesLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
   //  lOk := FBusquedaValNiveles.BuscaNivelDialogo( Sender, sFilter, sKey, sDescription );
   // Se cambi� a otro dataset ( cdsPuntosNivel )
end;

procedure TdmCatalogos.cdsValNivelesAlCrearCampos(Sender: TObject);
begin
      cdsValNiveles.MaskPesos('VN_PUNTOS');
end;

{ ***************** cdsSeccionesPerfil ************************** }

procedure TdmCatalogos.cdsSeccionesPerfilAlAdquirirDatos(Sender: TObject);
begin
     cdsSeccionesPerfil.Data := ServerCatalogo.GetSeccionesPerfil(dmCliente.Empresa);
end;

procedure TdmCatalogos.cdsSeccionesPerfilBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( cdsSeccionesPerfil, 'DT_CODIGO' );
     with DataSet do
     begin
          if strVacio( FieldByName('DT_NOMBRE').AsString) then
          begin
               DataBaseError( 'El nombre de la secci�n no puede quedar vac�o' );
          end;
     end;
end;

procedure TdmCatalogos.cdsSeccionesPerfilAlEnviarDatos(Sender: TObject);
var
   ErrorCount,iOrden: Integer;
begin
     ErrorCount := 0;
     with cdsSeccionesPerfil do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaSeccion( dmCliente.Empresa, Delta, ErrorCount, iOrden ) ) then
               begin
                    if ( ChangeCount = 0 ) and ( iOrden > 0 ) then
                    begin
                         Edit;
                         FieldByName('DT_ORDEN').AsInteger := iOrden;
                         Post;
                         MergeChangeLog;
                    end;
                    TressShell.SetDataChange( [ enCamposPerfil ] );
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsSeccionesPerfilAlModificar(Sender: TObject);
begin

//         ZbaseEdicion_DevEx.ShowFormaEdicion( EditSeccionPerfil_DevEx, TEditSeccionPerfil_DevEx );
end;

procedure TdmCatalogos.cdsSeccionesPerfilAfterDelete(DataSet: TDataSet);
begin
     with cdsSeccionesPerfil do
     begin
          Enviar;
          Refrescar;
     end;
end;

procedure TdmCatalogos.cdsSeccionesPerfilAlCrearCampos(Sender: TObject);
begin
     cdsSeccionesPerfil.FieldByName( 'DT_ORDEN' ).OnGetText := OrderPerfilesGetText;
end;

procedure TdmCatalogos.OrderPerfilesGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString;
end;

procedure TdmCatalogos.cdsSeccionesPerfilAlBorrar(Sender: TObject);
begin
     with cdsSeccionesPerfil do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Si borra esta secci�n eliminar� informaci�n capturada referente a la secci�n ?' ) then
          begin
               Delete;
          end;
     end;
end;

{ ***************** cdsCamposPerfil ************************** }

procedure TdmCatalogos.cdsCamposPerfilAlAdquirirDatos(Sender: TObject);
begin
     with cdsCamposPerfil do
     begin
          Data := ServerCatalogo.GetCamposPerfil( dmCliente.Empresa, FSeccionCampos );
     end;
end;

procedure TdmCatalogos.cdsCamposPerfilAlCrearCampos(Sender: TObject);
begin
     with cdsCamposPerfil do
     begin
          ListaFija( 'DF_CONTROL', lfControles );
          MaskNumerico( 'DF_LIMITE', '#' );
          FieldByName('DF_ORDEN').OnGetText := OrderPerfilesGetText;
          CreateSimpleLookup( cdsSeccionesPerfil, 'DT_NOMBRE', 'DT_CODIGO' );
     end;
end;

procedure TdmCatalogos.cdsCamposPerfilNewRecord(DataSet: TDataSet);
begin
     with cdsCamposPerfil do
     begin
          FieldByName('DT_CODIGO').AsString   := FSeccionCampos;
          FieldByName('DF_CONTROL').AsInteger := Ord( coTextoCorto );
     end;
end;

procedure TdmCatalogos.cdsCamposPerfilAlEnviarDatos(Sender: TObject);
var
   ErrorCount,iOrden: Integer;
begin
     ErrorCount := 0;
     with cdsCamposPerfil do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCamposPerfil( dmCliente.Empresa, Delta, ErrorCount ,iOrden ) ) then
               begin
                    if( iOrden > 0 ) and ( ChangeCount = 0 ) then
                    begin
                         Edit;
                         FieldByName('DF_ORDEN').AsInteger := iOrden;
                         Post;
                         MergeChangeLog;
                    end;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsCamposPerfilBeforePost(DataSet: TDataSet);
const
     K_MENSAJE_ERROR = '';
var
   eControl:eControles;

   function Dentro( const Control: eControles; Controles : Array of eControles ): Boolean;
   var i: integer;
   begin
     Result := FALSE;
     for i:=  Low(Controles) to High(Controles) do
     begin
          Result := ( Control = Controles[i] );
          if Result then
             Break;
     end;
   end;

   procedure ValidaCampoControl( sCampo:string; Controles :array of eControles );
   begin
        if( Pos( sCampo,DataSet.FieldByName( 'DF_CAMPO' ).AsString ) <> 0 )then
         begin
              if ( not Dentro( eControl,Controles ) )then
              begin
                   DataBaseError( 'El tipo de campo es inv�lido' )
              end;
         end;
   end;

begin
     cdsBeforePost( cdsCamposPerfil, 'DT_CODIGO' );
     with DataSet do
     begin
          if StrVacio( FieldByName('DF_TITULO').AsString ) then
          begin
               DataBaseError( 'El T�tulo del campo no puede quedar vac�o' );
          end;
          if StrVacio( FieldByName('DF_CAMPO').AsString ) then
          begin
               DataBaseError( 'El campo no puede quedar vac�o' );
          end;

          eControl := eControles(FieldByName('DF_CONTROL').AsInteger);

          if ( not ( eControl  in [ coCombo , coLista ]  ) )  then
          begin
                FieldByName('DF_VALORES').AsString := VACIO;
          end;

          if ( not ( eControl in [ coTextoCorto , coTextoLargo ] ) ) then
          begin
                FieldByName('DF_LIMITE').AsInteger := 0;
          end;

          if( FieldByName('DF_LIMITE').AsInteger < 0 )then
          begin
               DataBaseError( 'El ancho l�mite debe ser mayor o igual a  0 (cero) ' );
          end;

          if ( not StrVacio( FListaCampos ) ) then
          begin
               if Pos(FieldByName( 'DF_CAMPO' ).AsString,FListaCampos) <> 0 then
               begin
                    DataBaseError( 'El nombre del campo ya existe' );
               end;
          end;

          if State in [ dsEdit ] then
          begin
               if StrVacio( FieldByName( 'DF_VALORES' ).AsString )then
                  FieldByName( 'DF_VALORES' ).AsString := K_ESPACIO;
          end;

          //validar tipo de campo como texto...
          ValidaCampoControl( 'DP_TEXT_',[ coTextoCorto, coCombo, coLista ] );

          //validar tipo de campo como n�mero..
          ValidaCampoControl( 'DP_NUME_',[ coNumero ] );

          //validar tipo de campo como memo..
          ValidaCampoControl( 'DP_MEMO_',[ coTextoLargo ] );

          //validar tipo de campo como fecha..
          ValidaCampoControl( 'DP_FECH_',[ coFecha ] );
     end;
end;

procedure TdmCatalogos.cdsCamposPerfilAlModificar(Sender: TObject);
begin
//         ZbaseEdicion_DevEx.ShowFormaEdicion( EditCamposPerfil_DevEx, TEditCamposPerfil_DevEx );
end;

/// sube baja orden de las Secciones y campos

function TdmCatalogos.SubeBajaOrden(eAccion: eSubeBaja; DataSet:TZetaClientDataSet; const sCodigo,sPerfil:string):Integer;
const
     K_PRIMER_ORD = 1;
var
   iPosActual, iNuevaPos: Integer;
   oCursor: Tcursor;
   sCampo,sCatalogo : string;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ( DataSet = cdsSeccionesPerfil  ) then
        begin
           sCampo := 'DT_ORDEN';
           sCatalogo := 'Secciones de Perfil';
        end
        else if ( DataSet = cdsCamposPerfil  ) then
        begin
             sCampo := 'DF_ORDEN' ;
             sCatalogo := 'Campos de Perfil';
        end
        else if ( DataSet = cdsDescPerfil  ) then
        begin
             sCampo := 'DP_ORDEN' ;
             sCatalogo := 'Descripci�n de Perfil';
        end
        else if ( DataSet = cdsValfactores ) then
        begin
             sCampo := 'VF_ORDEN' ;
             sCatalogo := 'Factores de Valuaci�n'
        end
        else if ( DataSet = cdsSubfactores ) then
        begin
             sCampo:= 'VS_ORDEN' ;
             sCatalogo := 'Subfactores de Valuaci�n'
        end
        else if ( DataSet = cdsValNiveles ) then
        begin
             sCampo:= 'VN_ORDEN' ;
             sCatalogo := 'Niveles de Valuaci�n';
        end
        else if ( DataSet = cdsReglaPrestamo ) then
        begin
             sCampo:= 'RP_ORDEN' ;
             sCatalogo := 'Reglas de Validaci�n de Pr�stamos';
        end;


        iPosActual := DataSet.FieldByName(sCampo).AsInteger;
        iNuevaPos  := iPosActual;
        case( eAccion ) of
              eSubir: begin
                           if( iPosActual = K_PRIMER_ORD )then
                               ZetaDialogo.ZInformation( sCatalogo, 'Es el inicio de la lista de '+ sCatalogo, 0 )
                           else
                           begin
                                iNuevaPos := iPosActual - 1;
                                ServerCatalogo.CambiaOrden(dmCliente.Empresa, DataSet.Tag ,iPosActual, iNuevaPos,sCodigo,sPerfil );
                                DataSet.Refrescar;
                                DataSet.Locate(sCampo,iNuevaPos,[]);
                           end;
                      end;
              eBajar: begin
                           if( iPosActual = DataSet.RecordCount  )then
                               ZetaDialogo.ZInformation( sCatalogo, 'Es el final de la lista de '+sCatalogo, 0 )
                           else
                           begin
                                iNuevaPos := iPosActual + 1;
                                ServerCatalogo.CambiaOrden(dmCliente.Empresa, DataSet.Tag , iPosActual, iNuevaPos ,sCodigo,sPerfil );
                                DataSet.Refrescar;
                                DataSet.Locate(sCampo,iNuevaPos,[]);
                           end;
                      end;
        else
            ZetaDialogo.ZError( sCatalogo, 'Operaci�n No V�lida', 0 )
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     Result := iNuevaPos;
end;

procedure TdmCatalogos.cdsCamposPerfilAfterDelete(DataSet: TDataSet);
begin
     with cdsCamposPerfil do
     begin
          Enviar;
          Refrescar;
     end;
end;

procedure TdmCatalogos.cdsCamposPerfilBeforeEdit(DataSet: TDataSet);
begin
     LlenaListaCampos(DataSet,DataSet.FieldByName( 'DF_CAMPO' ).AsString);
end;

procedure TdmCatalogos.LlenaListaCampos( DataSet: TDataSet;const DF_CAMPO:string = '' );
var
   Pos : TBookMark;
begin
     FListaCampos := VACIO;
     with DataSet do
     begin
          DisableControls;
          try
             Pos:= GetBookMark;
             First;
             while ( not EOF ) do
             begin
                  if ( FieldByName( 'DF_CAMPO' ).AsString <> DF_CAMPO ) then
                     FListaCampos := ConcatString( FListaCampos, FieldByName( 'DF_CAMPO' ).AsString, ',' );
                  Next;
             end;
             if ( Pos <> nil ) then
             begin
                  GotoBookMark( Pos );
                  FreeBookMark( Pos );
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmCatalogos.cdsCamposPerfilBeforeInsert(DataSet: TDataSet);
begin
      LlenaListaCampos( DataSet );
end;

{************************cds Perfiles********************************}

function TdmCatalogos.PuedeCrearPerfil(sCodigo:string):Boolean ;
begin
     Result := not cdsPerfilesPuesto.Locate( 'PU_CODIGO',sCodigo,[] );
end;

procedure TdmCatalogos.cdsPerfilesNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'PF_ESTUDIO' ).AsInteger := ord(eEstudios(esNinguno));//Ninguno
          FieldByName( 'PF_FECHA').AsDateTime := dmCliente.FechaDefault; //Hoy
          FieldByName( 'PF_SEXO' ).AsString := VACIO;// Indistinto
     end;
end;

procedure TdmCatalogos.cdsPerfilesAlCrearCampos(Sender: TObject);
begin
     cdsPuestos.Conectar;
     with cdsPerfiles do
     begin
          CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'PU_CODIGO' );
          with FieldByName( 'PF_SEXO' ) do
          begin
               OnGetText := PF_SEXOGetText;
               OnSetText := PF_SEXOSetText;
          end;
          FieldByName('PF_EDADMIN').OnValidate := PF_EDADValidate;
          FieldByName('PF_EDADMAX').OnValidate := PF_EDADValidate;
          FieldByName('PF_EXP_PTO').OnValidate := PF_EXP_PTOValidate;
     end;
end;

procedure TdmCatalogos.PF_SEXOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := Sender.AsString;
     if strVacio( Text ) then
        Text := 'I';
end;

procedure TdmCatalogos.PF_SEXOSetText(Sender: TField; const Text: String);
var
   sTexto: String;
begin
     sTexto := Text;
     if ( sTexto = 'I' ) then
        sTexto := VACIO;
     Sender.AsString := sTexto;
end;

procedure TdmCatalogos.PF_EDADValidate(Sender: TField);
begin
     if( ( Sender.AsInteger < 16 ) or ( Sender.AsInteger > 100 ) )then
         DataBaseError('La edad debe ser de 16 a 100 a�os');
end;

procedure TdmCatalogos.PF_EXP_PTOValidate(Sender: TField);
begin
     if( Sender.AsInteger > 100 )then
         DataBaseError('Los a�os de experiencia debe ser menor o igual a 100 a�os');
end;

procedure TdmCatalogos.cdsPerfilesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPerfiles do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if ( Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) ) then
               begin
                    TressShell.SetDataChange( [ enPerfilPuesto ] );
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsPerfilesAlAdquirirDatos(Sender: TObject);
begin
     if FAgregandoPerfil then
         cdsPerfiles.Data := ServerCatalogo.GetPerfiles( dmCliente.Empresa,VACIO )
     else
         cdsPerfiles.Data := ServerCatalogo.GetPerfiles( dmCliente.Empresa,cdsPerfilesPuesto.FieldByName( 'PU_CODIGO' ).AsString );
end;

procedure TdmCatalogos.CopiarPerfil(const sFuente,sDestino: string);
begin
     with cdsPerfiles do
     begin
          Data := ServerCatalogo.CopiarPerfil( dmCliente.Empresa, sFuente, sDestino );
          if Locate( 'PU_CODIGO', sDestino, [] ) then
          begin
               Modificar;
               TressShell.SetDataChange( [ enPerfilPuesto ] );
          end
          else
              ZetaDialogo.ZError( 'Copiar perfil', 'El perfil no pudo ser copiado', 0 );
     end;
end;

procedure TdmCatalogos.cdsPerfilesAlAgregar(Sender: TObject);
begin
//     cdsPerfiles.Append;
//         ZcxWizardBasico.ShowWizard( TWizPerfil_DevEx );
end;

procedure TdmCatalogos.cdsPerfilesAlModificar(Sender: TObject);
begin
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditPerfil_DevEx, TEditPerfil_DevEx );
end;

procedure TdmCatalogos.cdsPerfilesBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if State in [ dsEdit ] then
          begin
               if StrVacio( FieldByName( 'PF_DESCRIP' ).AsString )then
                  FieldByName( 'PF_DESCRIP' ).AsString := K_ESPACIO;

               if StrVacio( FieldByName( 'PF_OBJETIV' ).AsString )then
                  FieldByName( 'PF_OBJETIV' ).AsString := K_ESPACIO;

               if StrVacio( FieldByName( 'PF_POSTING' ).AsString )then
                  FieldByName( 'PF_POSTING' ).AsString := K_ESPACIO;
          end;
     end;
end;

// *************************cdsDescPerfiles*********************//

procedure TdmCatalogos.cdsDescPerfilAlAdquirirDatos(Sender: TObject);
begin
     cdsDescPerfil.Data := ServerCatalogo.GetDescPerfil( dmCliente.Empresa, cdsPerfiles.FieldByName( 'PU_CODIGO' ).AsString );
end;

procedure TdmCatalogos.cdsDescPerfilAlCrearCampos(Sender: TObject);
var
   i : Integer;
begin
     with cdsDescPerfil do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               if ( Fields[ i ] is TFloatField ) then
                  MaskPesos( Fields[i].FieldName )
               else if ( Fields[ i ] is TDateTimeField ) then
                  MaskFecha( Fields[i].FieldName )
               else if ( ( Fields[ i ] is TMemoField ) or ( Fields[ i ] is TBlobField )  )then
                  Fields[ i ].OnGetText := MemoGetText;
          end;
          CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'PU_CODIGO' );
     end;
end;

procedure TdmCatalogos.cdsDescPerfilBeforePost(DataSet: TDataSet);
   procedure ValidarMemos;
   var
      iIndice: Integer;
   begin
        with cdsDescPerfil do
        begin
             for iIndice := 0 to ( FieldCount - 1 ) do
             begin
                  if ( ( Fields[ iIndice ] is TMemoField ) or ( Fields[ iIndice ] is TBlobField )  )then
                     if StrVacio( Fields[ iIndice ].AsString )then
                        Fields[iIndice].AsString := K_ESPACIO;
             end;
        end;
   end;

begin
     with DataSet do
     begin
          if State in [ dsEdit ] then
          begin
               ValidarMemos;
          end;
     end;
end;

procedure TdmCatalogos.cdsDescPerfilAlEnviarDatos(Sender: TObject);
var
   ErrorCount,iOrden: Integer;
   NombresCampos:TStrings;
begin
     NombresCampos := TStringList.Create;
     ErrorCount := 0;
     with cdsDescPerfil do
     begin
          Fields.GetFieldNames(NombresCampos);
          PostData;
          if ( ChangeCount > 0 ) then
          begin
              if ( Reconcile( ServerCatalogo.GrabaDescPerfil( dmCliente.Empresa, Delta, NombresCampos.CommaText, ErrorCount ,iOrden ) ) )then
               begin
                    if( iOrden > 0 ) and ( ChangeCount = 0 ) then
                    begin
                         Edit;
                         FieldByName('DP_ORDEN').AsInteger := iOrden;
                         Post;
                         MergeChangeLog;
                    end;
               end;
          end;
     end;
     FEditando:= False;
end;

procedure TdmCatalogos.cdsDescPerfilAfterDelete(DataSet: TDataSet);
begin
     cdsDescPerfil.Enviar;
end;

procedure TdmCatalogos.cdsDescPerfilNewRecord(DataSet: TDataSet);
begin
     with cdsDescPerfil do
     begin
          FieldByName( 'PU_CODIGO' ).AsString := cdsPerfiles.FieldByName( 'PU_CODIGO' ).AsString;
          FieldByName( 'DT_CODIGO' ).AsString := FSeccionCampos;
     end;
end;

procedure TdmCatalogos.cdsDescPerfilAfterCancel(DataSet: TDataSet);
begin
     FEditando := False;
end;

procedure TdmCatalogos.cdsDescPerfilBeforeInsert(DataSet: TDataSet);
begin
     FEditando := True;
end;

procedure TdmCatalogos.cdsDescPerfilBeforeEdit(DataSet: TDataSet);
begin
     FEditando := True;
end;


procedure TdmCatalogos.MemoGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if not FEditando then
        Text := cdsDescPerfil.FieldByName(Sender.FieldName).AsString;
end;

procedure TdmCatalogos.cdsDescPerfilAlModificar(Sender: TObject);
begin
//     try
//           ZBaseEdicion_DevEx.ShowFormaEdicion( EditDescPerfil_DevEx, TEditDescPerfil_DevEx );
//     finally
//                FreeAndNil( EditDescPerfil_DevEx );
//     end;

end;

procedure TdmCatalogos.FiltraDatosSeccion( const sSeccion, sSeccionDescrip: String );

   function GetTipoCampo( const oControl: eControles ): eExCampoTipo;
   begin
        case oControl of
             coTextoCorto: Result := xctTexto;
             coTextoLargo: Result := xctMemo;
             coNumero: Result := xctNumero;
             coFecha: Result := xctFecha;
        else
            Result := eExCampoTipo( Ord( oControl ) + 3 )
        end;
   end;

begin
     FSeccionCampos := sSeccion;
     // Clasificaciones para edici�n
     with cdsDescPerfil do
     begin
          Filtered:= False;
          Filter := 'DT_CODIGO = ' + EntreComillas( FSeccionCampos );
          Filtered:= True;
     end;

     cdsCamposPerfil.Refrescar;

     // Llenar clases para forma de edici�n
     // Clasificaciones
     with Clasificaciones do
     begin
          Clear;
          Add( FSeccionCampos, sSeccionDescrip, 0 ,MaxInt);
     end;
     // CamposAdic
     FCamposAdic.Clear;
     with cdsCamposPerfil do
     begin
          First;
          while ( not EOF ) do
          begin
               if( cdsDescPerfil.FindField( FieldByName('DF_CAMPO').AsString ) <> Nil )then
               begin
                    FCamposAdic.Add( GetTipoCampo( eControles( FieldByName( 'DF_CONTROL' ).AsInteger ) ),
                                     FieldByName( 'DF_TITULO' ).AsString,
                                     FieldByName( 'DF_CAMPO' ).AsString,
                                     ZetaCommonClasses.VACIO,
                                     FieldByName( 'DT_CODIGO' ).AsString,
                                     FieldByName( 'DF_ORDEN' ).AsInteger,
                                     arSiempre,
                                     enNinguno,
                                     FieldByName( 'DF_LIMITE' ).AsInteger,
                                     FieldByName( 'DF_VALORES' ).AsString );
               end ;
               //else ZetaDialogo.ZError('Perfil de puestos','No se ha definido el campo: '+FieldByName('DF_CAMPO').AsString+' en la tabla de Descripci�n de Puestos',0);
               Next;
          end;
     end;
end;


{ cdsRSocial }

procedure TdmCatalogos.cdsRSocialAlAdquirirDatos(Sender: TObject);
begin
     cdsRSocial.Data:= ServerCatalogo.GetRSocial(dmCliente.Empresa);
end;

procedure TdmCatalogos.cdsRSocialGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_GRALES_SOCIALES, iRight );
end;

procedure TdmCatalogos.cdsRSocialBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'RS_CODIGO' );
     if( ( cdsRSocial.FieldByName('RS_SUBSID').AsFloat > 1 ) or ( cdsRSocial.FieldByName('RS_SUBSID').AsFloat < 0 ) )then
     begin
          DataBaseError( '� La Proporci�n Del Subsidio '+
                         'Acreditable Debe De Ser Un Valor Entre 0 - 1 !');

     end;
     if zStrToBool( cdsRSocial.FieldByName('RS_SELLO').AsString ) then
     begin
          if StrVacio( cdsRSocial.FieldByName('RS_KEY_PU').AsString ) or
             StrVacio( cdsRSocial.FieldByName('RS_KEY_PR').AsString ) then
          begin
               DatabaseError( 'Los archivos del SAT no han sido validados correctamente.' +CR_LF +
                              'Favor de volver a indicarlos y validarlos' );
          end;
     end
     else
     begin
          with cdsRSocial do
          begin
               FieldByName('RS_CERT').AsString := ' ';
               FieldByName('RS_SERIAL').AsString := VACIO;
               FieldByName('RS_ISSUETO').AsString := VACIO ;
               FieldByName('RS_ISSUEBY').AsString := VACIO ;
               FieldByName('RS_VAL_INI').AsDateTime := NullDateTime ;
               FieldByName('RS_VAL_FIN').AsDateTime := NullDateTime ;
               FieldByName('RS_KEY_PU').AsString := ' ';
               FieldByName('RS_KEY_PR').AsString:= ' ';
          end;
     end;
end;

procedure TdmCatalogos.cdsRSocialAlModificar(Sender: TObject);
begin

//    ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatRSocial_DevEx, TEditCatRSocial_DevEx );
end;

procedure TdmCatalogos.cdsRSocialAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsRSocial do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enRSocial ] );
               end;
          end;
     end;
end;

procedure TdmCatalogos.GetPeriodoPorMes( const eTipoPer: eTipoPeriodo; iMes: Integer; var iPeriodoIni, iPeriodoFinal: Integer);
var
   iNominaFinal: Integer;
   sFiltroAnterior: String;
   lFiltrado: Boolean;
begin
     iNominaFinal:= 0;
     with cdsPeriodoOtro do
     begin
          sFiltroAnterior:= Filter;
          lFiltrado:= Filtered;
          Filtered:= FALSE;
          try
             Filter:= Format('( PE_TIPO = %d )',[ Ord(eTipoPer) ]);
             Filtered:= TRUE;
             First;
             if Locate('PE_MES;PE_CAL',VarArrayOf( [iMes,K_GLOBAL_SI ] ),[]) then
             begin
                  iPeriodoIni:= FieldByName('PE_NUMERO').AsInteger;
                  while ( ( FieldByName('PE_MES').AsInteger = iMes ) and not EOF ) do
                  begin
                       iNominaFinal:= FieldByName('PE_NUMERO').AsInteger;
                       Next;
                  end;
                  iPeriodoFinal:= iNominaFinal;
             end;
          finally
                 cdsPeriodoOtro.Filter:= sFiltroAnterior;
                 cdsPeriodoOtro.Filtered:= lFiltrado;
          end;
     end;
end;

{ Matriz de Certificaciones }

procedure TdmCatalogos.cdsMatrizCertificPuestoAlAdquirirDatos(Sender: TObject);
var
   sCodigo: String;
begin
     cdsCertificaciones.Conectar;
     if ( FMatrizPorCertific ) then
        sCodigo := cdsCertificaciones.FieldByName( 'CI_CODIGO' ).AsString
     else
         sCodigo := cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString;
     cdsMatrizCertificPuesto.Data := ServerCatalogo.GetMatrizCertifica( dmCliente.Empresa,sCodigo,FMatrizPorCertific);
end;

procedure TdmCatalogos.cdsMatrizCertificPuestoAlCrearCampos(
  Sender: TObject);
begin
 with cdsMatrizCertificPuesto do
     begin
          CreateSimpleLookup( cdsPuestos, 'PU_NOMBRE', 'PU_CODIGO' );
          CreateSimpleLookup( cdsCertificaciones, 'CI_NOMBRE', 'CI_CODIGO' );
          //CreateLookup( cdsCertificaciones, 'CI_NOMBRE', 'CI_CODIGO', 'CI_CODIGO', 'CI_NOMBRE' );
     end;
end;

procedure TdmCatalogos.cdsCerNivelAfterDelete(DataSet: TDataSet);
begin
     with cdsMatrizCertificPuesto do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmCatalogos.cdsCerNivelNewRecord(DataSet: TDataSet);
begin
    with cdsCerNivel do
     begin
          FieldByName( 'CI_CODIGO' ).AsString := cdsMatrizCertificPuesto.FieldByName( 'CI_CODIGO' ).AsString;
          FieldByName( 'PU_CODIGO' ).AsString := cdsMatrizCertificPuesto.FieldByName( 'PU_CODIGO' ).AsString;
     end;
end;

procedure TdmCatalogos.cdsMatrizCertificPuestoAfterCancel(
  DataSet: TDataSet);
begin
     cdsCerNivel.CancelUpdates;
end;

procedure TdmCatalogos.cdsMatrizCertificPuestoAfterDelete(
  DataSet: TDataSet);
begin
     cdsMatrizCertificPuesto.Enviar;
end;

procedure TdmCatalogos.cdsMatrizCertificPuestoAfterOpen(DataSet: TDataSet);
begin
     cdsCerNivel.DataSetField := TDataSetField( cdsMatrizCertificPuesto.FieldByName( 'qryDetail' ) );
end;

procedure TdmCatalogos.cdsMatrizCertificPuestoAlEnviarDatos(
  Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsMatrizCertificPuesto do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaMatrizCetifica( dmCliente.Empresa, FMatrizPorCertific, Delta, ErrorCount ) );
          end;
     end;
     dmRecursos.cdsHisCerProg.Active := False; // para que se refresque

end;

//DevEx (by am):Se agrega la validacion del a vista para mostrar la forma correspondiente.
procedure TdmCatalogos.cdsMatrizCertificPuestoAlModificar(Sender: TObject);
begin
//     if FMatrizPorCertific then
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditMatrizPuestoCertificacion_DevEx, TEditMatrizPuestoCertificacion_DevEx )
//     else
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditMatrizCertificaPuesto_DevEx, TEditMatrizCertificaPuesto_DevEx );
end;

procedure TdmCatalogos.cdsMatrizCertificPuestoBeforePost(
  DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'PU_CODIGO' );
     cdsBeforePost( DataSet, 'CI_CODIGO' );
end;

procedure TdmCatalogos.cdsMatrizCertificPuestoNewRecord(DataSet: TDataSet);
begin
     with Dataset do
     begin
          if FMatrizPorCertific then
             FieldByName( 'CI_CODIGO' ).AsString := cdsCertificaciones.FieldByName( 'CI_CODIGO' ).AsString
          else
              FieldByName( 'PU_CODIGO' ).AsString := cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString;
          FieldByName( 'PC_OPCIONA' ).AsString := K_GLOBAL_SI;
          FieldByName( 'PC_LISTA' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCatalogos.cdsPeriodoOtroLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
         lOk := ZetaBuscaPeriodo_DevEx.BuscaPeriodoDialogo( Sender, sFilter, sKey, sDescription );
end;

procedure TdmCatalogos.GetDatosPeriodoOtro( const iYear: Integer );
begin
     with cdsPeriodoOtro do
     begin
          Data := ServerCatalogo.GetPeriodos( dmCliente.Empresa, iYear, -1 );
          ResetDataChange;
     end;
end;

procedure TdmCatalogos.cdsPeriodoOtroLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     with cdsPeriodoOtro do
     begin
          lOk := Locate( 'PE_NUMERO', StrToIntDef( sKey, 0 ), [] );
          if lOk then
          begin
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmCatalogos.ModificaEdicionTotalesNomina ( const lNomTotales:Boolean );
begin
     if lNomTotales then
     begin
          with cdsPeriodo do
          begin
               FieldByName( 'PE_TOT_NET' ).OnGetText := TotalesGetText;
               FieldByName( 'PE_TOT_DED' ).OnGetText := TotalesGetText;
               FieldByName( 'PE_TOT_PER' ).OnGetText := TotalesGetText;
               FieldByName( 'PE_NUM_EMP' ).OnGetText := TotalesGetText;
          end;
     end;
     cdsPeriodo.Modificar;
     with cdsPeriodo do
     begin
          FieldByName( 'PE_TOT_NET' ).OnGetText := Nil;
          FieldByName( 'PE_TOT_DED' ).OnGetText := Nil;
          FieldByName( 'PE_TOT_PER' ).OnGetText := Nil;
          FieldByName( 'PE_NUM_EMP' ).OnGetText := Nil;
     end;
end;

procedure TdmCatalogos.TotalesGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.FieldName = 'PE_NUM_EMP' then
     begin
          Text := IntToStr( dmNomina.cdsTotales.FieldByName(Sender.FieldName).AsInteger );
     end
     else
         Text := FormatFloat( '#,0.00', dmNomina.cdsTotales.FieldByName(Sender.FieldName).AsFloat);
end;

procedure TdmCatalogos.cdsReglaPrestamoAlAdquirirDatos(Sender: TObject);
begin
     cdsReglaPrestamo.Data := ServerCatalogo.GetReglasPrestamo( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsReglaPrestamoAlCrearCampos(Sender: TObject);
begin
     with cdsReglaPrestamo do
     begin
          ListaFija ('RP_EMP_STS',lfFiltroStatusEmpleado); 
          FieldByName( 'RP_LISTA' ).OnGetText := RP_LISTAGetText;
     end;

end;

procedure TdmCatalogos.cdsHistRevReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError : String;
begin
    if PK_Violation( E ) then
    begin
      with DataSet do
        sError := '� Revisi�n Ya Existe ! ';
    end
    else
        sError := GetErrorDescription( E );
    cdsHistRev.ReconciliaError(DataSet,UpdateKind,E, 'CH_REVISIO', sError );
end;

procedure TdmCatalogos.cdsHistRevNewRecord(DataSet: TDataSet);
begin
     with cdsHistRev do
     begin
          FieldByName('CU_CODIGO').AsString := cdsCursos.FieldByName('CU_CODIGO').AsString;
          FieldByName( 'CH_FECHA' ).AsDateTime := dmCliente.FechaDefault;
     end;
end;


procedure TdmCatalogos.cdsHistRevAfterEdit(DataSet: TDataSet);
begin
     with cdsCursos do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;


procedure TdmCatalogos.cdsHistRevPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
     ZetaDialogo.ZError('Error', GetDBErrorDescription( E ), 0 );
     Action := daAbort;
end;

procedure TdmCatalogos.cdsHistRevAfterPost(DataSet: TDataSet);
begin
     ActualizaRevision;
end;

procedure TdmCatalogos.cdsHistRevBeforePost(DataSet: TDataSet);
begin
     with cdsHistRev do
     begin
          FieldByName('US_CODIGO').AsInteger:= dmCliente.Usuario;
          if ZetaCommonTools.StrVacio( FieldByName( 'CH_REVISIO' ).AsString ) then
             DataBaseError( 'La Revisi�n No Puede Quedar Vac�a' );
     end;
end;

procedure TdmCatalogos.cdsHistRevAlCrearCampos(Sender: TObject);
begin
     with cdsHistRev do
     begin
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          MaskFecha( 'CH_FECHA' );
     end;
end;

procedure TdmCatalogos.cdsHistRevAfterDelete(DataSet: TDataSet);
begin
     with cdsCursos do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
     ActualizaRevision;
end;

procedure TdmCatalogos.ActualizaRevision;
begin
     with cdsHistRev do
     begin
          DisableControls;
          try
             Last;
             if (cdsCursos.FieldByName('CU_REVISIO').AsString <> FieldByName('CH_REVISIO').AsString) OR
                (cdsCursos.FieldByName('CU_FEC_REV').AsDateTime <> FieldByName('CH_FECHA').AsDateTime) then
             begin
                  if not ( cdsCursos.State in [ dsEdit, dsInsert ] ) then
                     cdsCursos.Edit;
                  cdsCursos.FieldByName('CU_FEC_REV').AsDateTime := FieldByName('CH_FECHA').AsDateTime;
                  cdsCursos.FieldByName('CU_REVISIO').AsString := FieldByName('CH_REVISIO').AsString;
             end;
          finally
                 EnableControls;
          end;
     end;
end;


procedure TdmCatalogos.RP_LISTAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if StrLleno ( Sender.AsString ) then
     begin
          if zStrToBool(Sender.AsString) then
          begin
               Text := 'Algunos';
          end
          else
              Text := 'Todos';
     end
     else
         Text := Sender.AsString;
end;

procedure TdmCatalogos.cdsReglaPrestamoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsReglaPrestamo do
     begin
          //iCodigo := FieldByName('RP_CODIGO').AsInteger;
          FAgregaReglaPrestamo := ( State = dsInsert ) or FAgregaReglaPrestamo;
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if ( Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) ) then
               begin
                    if FAgregaReglaPrestamo then
                    begin
                         FAgregaReglaPrestamo := FALSE;
                         Refrescar;
                         Last;
                    end{
                    else
                        Locate('RP_CODIGO',iCodigo,[]);}
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsReglaPrestamoAfterCancel(DataSet: TDataSet);
begin
     FAgregaReglaPrestamo := FALSE;
end;

procedure TdmCatalogos.cdsReglaPrestamoAlModificar(Sender: TObject);
begin

//      ZBaseEdicion_DevEx.ShowFormaEdicion( EditReglaPrestamo_DevEx, TEditReglaPrestamo_DevEx )
end;

procedure TdmCatalogos.cdsPrestaXReglaAlAdquirirDatos(Sender: TObject);
begin
     cdsPrestaXRegla.Data := ServerCatalogo.GetPrestamoPorRegla( dmCliente.Empresa ,cdsReglaPrestamo.FieldByName('RP_CODIGO').AsInteger );
end;

procedure TdmCatalogos.cdsPrestaXReglaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPrestaXRegla do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if( ( RecordCount = 0 ) and (  zStrToBool(cdsReglaPrestamo.FieldByName('RP_LISTA').AsString ) ) ) then
               begin
                    //DataBaseError('No se seleccion� ning�n Tipo de Pr�stamo')
               end
               else
                   Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, cdsPrestaXRegla.Tag , Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmCatalogos.cdsPrestaXReglaNewRecord(DataSet: TDataSet);
begin
     cdsPrestaXRegla.FieldByName('RP_CODIGO').AsInteger := cdsReglaPrestamo.FieldByName('RP_CODIGO').AsInteger;
end;

procedure TdmCatalogos.cdsReglaPrestamoBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'RP_CODIGO' );
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'RP_LETRERO' ).AsString ) then
             DataBaseError( 'El Mensaje de la Regla No Puede Quedar Vac�o' );
          if zStrToBool( FieldByName( 'RP_LISTA' ).AsString ) and ( FTiposPrestamosVacios ) then
             DataBaseError( 'Se debe de agregar al menos un Tipo de Pr�stamo a validar' );
          if FieldByName( 'RP_LIMITE' ).AsInteger < 0 then
             DataBaseError( 'El l�mite de Pr�stamos no puede ser menor a Cero' );

          if zStrToBool( FieldByName('RP_VAL_FEC').AsString ) then
          begin
               if zStrToBool( FieldByName('RP_LISTA').AsString ) then
               begin
                  if (cdsPrestaXRegla.RecordCount <> 1) then
                     DataBaseError( 'Al validar fechas se debe de seleccionar un Tipo de Pr�stamo' )
               end
               else
                  DataBaseError( 'Al validar fechas se debe de seleccionar un Tipo de Pr�stamo' );
          end;

          if ( FieldByName( 'RP_ANT_INI' ).AsInteger < 0 ) or
             ( FieldByName( 'RP_ANT_FIN' ).AsInteger < 0 ) then
             DataBaseError( 'La antig�edad del Empleado no puede ser menor a Cero' );

          if ( FieldByName( 'RP_ANT_INI' ).AsInteger > Trunc( High(Smallint) )	 ) or
             ( FieldByName( 'RP_ANT_FIN' ).AsInteger > Trunc( High(Smallint) ) ) then
             DataBaseError( Format( 'La antig�edad del Empleado no puede ser mayor a %d', [Trunc( High(Smallint) )] ) );

     end;
end;

procedure TdmCatalogos.cdsReglaPrestamoNewRecord(DataSet: TDataSet);
begin
     with cdsReglaPrestamo do
     begin
          FieldByName('RP_ACTIVO').AsString:= K_GLOBAL_SI ;
          FieldByName('RP_VAL_FEC').AsString:= K_GLOBAL_NO;

     end;
end;

procedure TdmCatalogos.cdsReglaPrestamoAfterDelete(DataSet: TDataSet);
begin
     cdsAfterPost(DataSet );
     cdsReglaPrestamo.Refrescar;
end;

//acl
procedure TdmCatalogos.cdsTPeriodosLookupDescription( Sender: TZetaLookupDataSet; var sDescription: String);
var
   iPos: Integer;
begin
     iPos:= Pos ( '/', cdsTPeriodos.FieldByName( 'TP_DESCRIP' ).AsString );
     if ( ( iPos > 0 ) and ( StrLleno( cdsTPeriodos.FieldByName( 'TP_DESCRIP' ).AsString[iPos+1] ) ) and ( StrLleno( cdsTPeriodos.FieldByName( 'TP_DESCRIP' ).AsString[iPos-1] ) ) ) then
     begin
          sDescription:=Copy( cdsTPeriodos.FieldByName( 'TP_DESCRIP' ).AsString, 1, iPos - 1 )
     end
     else
         sDescription:= cdsTPeriodos.FieldByName( 'TP_DESCRIP' ).AsString;
end;

procedure TdmCatalogos.CargaListaGrupos( Lista: TStrings);
var
   iPos,i: Integer;
   Codigo, sOldIndex: String;
   GruposAsignados :TStrings;
   DataSet : TZetaClientDataSet;
begin
     DataSet := TZetaClientDataSet.Create(Self);
     GruposAsignados := TStringList.Create;
     with DataSet do
     begin
          Data := ServerCatalogo.GetGruposConceptos(dmCliente.Empresa);
          Filter:= Format( '( GR_CODIGO <> %d )', [D_GRUPO_SIN_RESTRICCION] );
          Filtered:= TRUE;
          First;
          sOldIndex:= IndexFieldNames;
          IndexFieldNames:= 'GR_CODIGO';
          with Lista do
          begin
               try
                  BeginUpdate;
                  Clear;
                  while not Eof do
                  begin
                       AddObject( FieldByName( 'GR_CODIGO' ).AsString + '=' + FieldByName( 'GR_DESCRIP' ).AsString, TObject( 0 ) );
                       Next;
                  end;
               finally
                     IndexFieldNames:= sOldIndex;
                     EndUpdate;
               end;
          end;
     end;
     with cdsConceptos do
     begin
          GruposAsignados.DelimitedText := ',';
          GruposAsignados.CommaText := FieldByName('CO_GPO_ACC').AsString;
          with Lista do
          begin
               for i := 0 to GruposAsignados.Count - 1 do
               begin
                    Codigo := GruposAsignados[i];
                    iPos := IndexOfName( Codigo );
                    if ( iPos < 0 ) then
                       AddObject( Codigo + '=' + ' ???', TObject( 1 ) )
                    else
                        Objects[ iPos ] := TObject( 1 );
               end;
          end;
     end;
end;

procedure TdmCatalogos.DescargaListaGrupos( Lista: TStrings );
var
   sLista, sCodigoGrupo: String;
   i: Integer;
begin
     with cdsConceptos do
     begin
          if not ( State in [dsEdit] ) then
          begin
               Edit;
          end;
          FieldByName('CO_GPO_ACC').AsString := VACIO;
          with Lista do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if ( Integer( Lista.Objects[ i ] ) > 0 ) then
                    begin
                         sCodigoGrupo := Names[i];
                         sLista := ConcatString(sLista,sCodigoGrupo,',');
                    end;
               end;
          end;
          FieldByName('CO_GPO_ACC').AsString := sLista;
     end;
end;

{ACL 08.Abril.09}
procedure TdmCatalogos.GetDatosPeriodoAd( const iYear: Integer; const TipoPeriodo: eTipoPeriodo );
begin
     with cdsPeriodoAd do
     begin
          Data := ServerCatalogo.GetPeriodos( dmCliente.Empresa, iYear, Ord(TipoPeriodo) );
          ResetDataChange;
     end;
end;

procedure TdmCatalogos.cdsPeriodoAdAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          GetDatosPeriodo( YearDefault, PeriodoTipo );
     end;
end;

procedure TdmCatalogos.cdsPeriodoAdLookupDescription( Sender: TZetaLookupDataSet; var sDescription: String);
begin
     with Sender do
     begin
          sDescription := 'De ' + FechaCorta( FieldByName( 'PE_FEC_INI' ).AsDateTime ) +
                          ' a ' + FechaCorta( FieldByName( 'PE_FEC_FIN' ).AsDateTime );
     end;
end;

procedure TdmCatalogos.cdsPeriodoAdLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     with cdsPeriodoAd do
     begin
          lOk := Locate( 'PE_NUMERO', StrToIntDef( sKey, 0 ), [] );
          if lOk then
          begin
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmCatalogos.cdsPeriodoAdLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
         lOk := ZetaBuscaPeriodo_DevEx.BuscaPeriodoDialogo( Sender, sFilter, sKey, sDescription );
end;

procedure TdmCatalogos.cdsPeriodoAdReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

procedure TdmCatalogos.cdsFestTurnoAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iMes, iDia, iYear: Integer;
   sCodigo: string;
begin
     ErrorCount := 0;
     with cdsFestTurno do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;     
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
               begin
                    sCodigo := FieldByName('TU_CODIGO').AsString;
                    iMes := FieldByName('FE_MES').AsInteger;
                    iDia := FieldByName('FE_DIA').AsInteger;
                    iYear := FieldByName('FE_YEAR').AsInteger;
                    cdsFestTurno.Refrescar;
                    cdsFestTurno.Locate( 'TU_CODIGO; FE_MES; FE_DIA; FE_YEAR', Vararrayof([ sCodigo, iMes, iDia,iYear ]), [] );
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsAreasGetRights(Sender: TZetaClientDataSet; const iRight: Integer;
          var lHasRights: Boolean);
begin
     lHasRights := FALSE;
end;

procedure TdmCatalogos.cdsEstablecimientosAlAdquirirDatos(Sender: TObject);
begin
     cdsEstablecimientos.Data := ServerCatalogo.GetEstablecimientos(dmCliente.Empresa);
end;

procedure TdmCatalogos.cdsEstablecimientosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsEstablecimientos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaEstablecimientos( dmCliente.Empresa,Delta, ErrorCount ) ) then
               begin
                    //TressShell.SetDataChange( [ enEstablecimientos ] );
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsEstablecimientosAlModificar(Sender: TObject);
begin

//     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEstablecimientos_DevEx, TEditEstablecimientos_DevEx );
end;

procedure TdmCatalogos.cdsEstablecimientosBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost(cdsEstablecimientos,'ES_CODIGO');
     if ZetaCommonTools.StrVacio( cdsEstablecimientos.FieldByName('ES_ELEMENT').AsString ) then
        DataBaseError( 'El Nombre No Puede Quedar Vac�o' );
end;

procedure TdmCatalogos.cdsEstablecimientosNewRecord(DataSet: TDataSet);
begin
     with cdsEstablecimientos do
     begin
          FieldByName('ES_ACTIVO').AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmCatalogos.cdsTiposPensionAlAdquirirDatos(Sender: TObject);
begin
     cdsTiposPension.Data := ServerCatalogo.GetTiposPension(dmCliente.Empresa);
end;

procedure TdmCatalogos.cdsTiposPensionAlEnviarDatos(Sender: TObject);
var
   iErrorCount: Integer;
begin
     iErrorCount := 0;
     with cdsTiposPension do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCatalogo(dmCliente.Empresa,Tag,Delta,iErrorCount ) ) then
                  ZetaDialogo.ZWarning( 'Tipo de Pensi�n', 'Recuerde revisar configuraci�n de conceptos de descuento en la N�mina' + CR_LF +
                                                               'para incluir este cambio', 0, mbOK );
          end;
     end;
end;

procedure TdmCatalogos.cdsTiposPensionBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost(cdsTiposPension,'TP_CODIGO');
     if ZetaCommonTools.StrVacio( cdsTiposPension.FieldByName('TP_NOMBRE').AsString ) then
        DataBaseError( 'El Nombre No Puede Quedar Vac�o' );
     with cdsTiposPension do
     begin
          If not StrLleno(FieldByName('TP_CO_PENS').AsString) and ( not zStrToBool(FieldByName('TP_PERCEP').AsString)) and ( not zStrToBool(FieldByName('TP_PRESTA').AsString)) then
          begin
               FieldByName('TP_CO_PENS').FocusControl;
               DataBaseError( 'Seleccionar al menos un Concepto de Pago de Pensi�n');
          end;
          if ( not zStrToBool( FieldByName('TP_APL_NOR').AsString ) )and ( not zStrToBool( FieldByName('TP_APL_LIQ').AsString )  ) and (not zStrToBool( FieldByName('TP_APL_IND').AsString ) ) then
          begin
               FieldByName('TP_APL_NOR').FocusControl;
               DataBaseError( 'Seleccionar que Clasificaci�n de N�mina se Aplicar�' );
          end;
     end;
end;

procedure TdmCatalogos.cdsTiposPensionNewRecord(DataSet: TDataSet);
begin
     with cdsTiposPension do
     begin
          FieldByName('TP_APL_NOR').AsString := K_GLOBAL_NO;
          FieldByName('TP_APL_LIQ').AsString := K_GLOBAL_NO;
          FieldByName('TP_APL_IND').AsString := K_GLOBAL_NO;
          FieldByName('TP_PERCEP').AsString := K_GLOBAL_NO;
          FieldByName('TP_PRESTA').AsString := K_GLOBAL_NO;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmCatalogos.cdsTiposPensionAlModificar(Sender: TObject);
begin

//    	ZBaseEdicion_DevEx.ShowFormaEdicion(EditCatTiposPension_DevEx,TEditCatTiposPension_DevEx);
end;

procedure TdmCatalogos.cdsTiposPensionAlCrearCampos(Sender: TObject);
begin
      dmSistema.cdsUsuarios.Conectar;
     with cdsTiposPension do
     begin
          CreateCalculated('TP_APL_NOM',ftString,250 );
          CreateCalculated('TP_CO_PENS_TXT',ftString,250 );
          FieldByName('TP_APL_NOM').OnGetText := TP_NOMINAGetText;
          FieldByName('TP_CO_PENS_TXT').OnGetText := TP_CO_PENGetText;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmCatalogos.TP_NOMINAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
var
   sTexto:string;
begin
     if DisplayText then
     begin
          With cdsTiposPension do
          begin
               sTexto := VACIO;
               if zStrToBool(FieldByName('TP_APL_NOR').AsString)then
                  sTexto := ConcatString(sTexto,'Normal',' - ');

               if zStrToBool(FieldByName('TP_APL_LIQ').AsString)then
                  sTexto := ConcatString(sTexto,'Liquidaci�n',' - ');

               if zStrToBool(FieldByName('TP_APL_IND').AsString)then
                  sTexto := ConcatString(sTexto,'Indemnizaci�n',' - ');

               Text := sTexto;
          end;
     end;
end;

procedure TdmCatalogos.TP_CO_PENGetText( Sender: TField; var Text: String; DisplayText: Boolean);
var
   sTexto:string;
begin
     With cdsTiposPension do
     begin
          sTexto := VACIO;
          if zStrToBool(FieldByName('TP_PERCEP').AsString)then
             sTexto := ConcatString(sTexto,'TOT_PER()','+');

          if zStrToBool(FieldByName('TP_PRESTA').AsString)then
             sTexto := ConcatString(sTexto,'TOT_PRESTA()','+');

          Text := ConcatString(sTexto,FieldByName('TP_CO_PENS').AsString,'+');
     end;
end;


procedure TdmCatalogos.cdsTiposPensionAfterDelete(DataSet: TDataSet);
begin
     cdsTiposPension.Enviar;
end;

{Seguro de Gastos Medicos}
procedure TdmCatalogos.cdsSegGastosMedAlAdquirirDatos(Sender: TObject);
var
   Vigencias :OleVariant;
begin
     cdsSegGastosMed.Data := ServerCatalogo.GetSeguroGastosMedicos(dmCliente.Empresa,Vigencias );
     cdsVigenciasSGM.Data :=  Vigencias;
end;

procedure TdmCatalogos.cdsSegGastosMedAlModificar(Sender: TObject);
begin
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditSeguroGastosMedicos_DevEx, TEditSeguroGastosMedicos_DevEx );
end;

procedure TdmCatalogos.cdsSegGastosMedBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost(cdsSegGastosMed,'PM_CODIGO');
     ValidaMemoField(DataSet,'PM_OBSERVA');
     with cdsSegGastosMed do
     begin
          if StrVacio(FieldByName('PM_NUMERO').AsString)then
          begin
               FieldByName('PM_NUMERO').FocusControl;
               DataBaseError('No. P�liza No Puede Quedar Vac�o');
          end;
          if StrVacio(FieldByName('PM_DESCRIP').AsString)then
          begin
               FieldByName('PM_DESCRIP').FocusControl;
               DataBaseError('Descripci�n No Puede Quedar Vac�a');
          end;
     end;
end;

procedure TdmCatalogos.cdsSegGastosMedNewRecord(DataSet: TDataSet);
begin
     with cdsSegGastosMed do
     begin
          FieldByName('PM_TIPO').AsInteger := Ord(tsGastosMayores);

     end;
end;

procedure TdmCatalogos.cdsVigenciasSGMNewRecord(DataSet: TDataSet);
begin
     with cdsVigenciasSGM do
     begin
          FieldByName('PM_CODIGO').AsString := cdsSegGastosMed.FieldByName('PM_CODIGO').AsString;
          FieldByName('PV_FEC_INI').AsDateTime := dmCliente.FechaDefault ;
          FieldByName('PV_FEC_FIN').AsDateTime := dmCliente.FechaDefault + 365;
     end;
end;

procedure TdmCatalogos.cdsSegGastosMedAlCrearCampos(Sender: TObject);
begin
     with cdsSegGastosMed do
     begin
          ListaFija( 'PM_TIPO', lfTipoSGM  );
     end;
end;

procedure TdmCatalogos.cdsVigenciasSGMAfterDelete(DataSet: TDataSet);
begin
      with cdsSegGastosMed do
      begin
           if State = dsBrowse then
              Edit;
      end;
end;

procedure TdmCatalogos.cdsSegGastosMedAfterCancel(DataSet: TDataSet);
begin
     cdsVigenciasSGM.CancelUpdates;
end;

procedure TdmCatalogos.cdsSegGastosMedAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsSegGastosMed do
     begin
          PostData;
          cdsVigenciasSGM.PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
          end;

     end;
     cdsVigenciasSGM.Enviar;
end;

procedure TdmCatalogos.cdsSegGastosMedAfterOpen(DataSet: TDataSet);
begin
    // cdsVigenciasSGM.DataSetField := TDataSetField( cdsSegGastosMed.FieldByName( 'qryDetail' ) );
end;

procedure TdmCatalogos.cdsSegGastosMedAfterDelete(DataSet: TDataSet);
begin
     cdsSegGastosMed.Enviar;
end;

procedure TdmCatalogos.cdsVigenciasSGMAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsVigenciasSGM do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaVigencias ( dmCliente.Empresa, cdsVigenciasSGM.Delta, ErrorCount ) );
               //MergeChangeLog;
          end;
     end;
end;

procedure TdmCatalogos.cdsVigenciasSGMBeforeDelete(DataSet: TDataSet);
begin
     with cdsVigenciasSGM do
     begin
          if ( FieldByName('PV_STITULA').AsInteger > 0 ) or ( FieldByName('PV_SDEPEND').AsInteger > 0 )then
          begin
               DataBaseError('No se Puede Borrar Vigencia, Existen P�lizas Asignadas.');
          end;
     end;
end;

procedure TdmCatalogos.cdsVigenciasSGMBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          ValidaMemoField(DataSet,'PV_CONDIC'); 

          if StrVacio(FieldByName('PV_REFEREN').AsString)then
          begin
               DataBaseError('Referencia No Puede Quedar Vac�a');
          end;

          if (FieldByName('PV_FEC_INI').AsDateTime >= FieldByName('PV_FEC_FIN').AsDateTime )then
          begin
               DataBaseError('La Fecha Final de Vigencia debe ser Mayor a la Inicial');
          end;

          if (FieldByName('PV_FEC_INI').AsDateTime = NullDateTime)then
          begin
               DataBaseError('La Fecha Inicial es Obligatoria');
          end;

          if (FieldByName('PV_FEC_FIN').AsDateTime = NullDateTime)then
          begin
               DataBaseError('La Fecha Final es Obligatoria');
          end;
          
     end;
end;

procedure TdmCatalogos.cdsVigenciasSGMAfterPost(DataSet: TDataSet);
begin
     cdsSegGastosMed.Edit; 
end;

procedure TdmCatalogos.cdsSegGastosMedReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError:string;
begin
     if FK_Violation(E)then
        with DataSet do
        begin
             sError := Format('No se Puede Borrar P�liza: %s Tiene Asignadas P�lizas de Empleado',[ FieldByName('PM_CODIGO').AsString +' : '+ FieldByName('PM_DESCRIP').AsString]);
        end;
     ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
     case UpdateKind of
          ukDelete: Action := raCancel;
     else
         Action := raAbort;
     end;
end;

{ cdsCosteoGrupos - Grupos de Costeo JB }

procedure TdmCatalogos.cdsCosteoGruposAlAdquirirDatos(Sender: TObject);
begin
     cdsCosteoGrupos.Data := ServerCatalogo.GetCosteoGrupos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCosteoGruposAlModificar(Sender: TObject);
begin
//        ZBaseEdicion_DevEx.ShowFormaEdicion( EditCosteoGrupos_DevEx, TEditCosteoGrupos_DevEx );
end;

procedure TdmCatalogos.cdsCosteoGruposBeforePost(DataSet: TDataSet);
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'GpoCostoCodigo' ).AsString ) then
             DataBaseError( 'El C�digo No Puede Quedar Vac�o' );
     end;
end;


procedure TdmCatalogos.cdsCosteoGruposAlEnviarDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   ErrorCount: Integer;
   Pos: TBookMark;
{$endif}
begin
{$ifndef DOS_CAPAS}
     ErrorCount := 0;
     with cdsCosteoGrupos do
     begin
          try
             if State in [ dsEdit, dsInsert ] then
                Post;
             if ( ChangeCount > 0 ) then
                  if Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
                       TressShell.SetDataChange( [ enGruposCosteo ] );
             Pos:= GetBookMark;
             Refrescar;
             if (RecordCount > 0) and (BookMarkValid( Pos )) then
                  GotoBookMark( Pos );
          finally
                 FreeBookMark( Pos );
          end;
     end;
{$endif}
end;

procedure TdmCatalogos.cdsCosteoGruposNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'GpoCostoActivo' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmCatalogos.cdsCosteoGruposGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_COSTEO_GRUPOS, iRight );
end;


procedure TdmCatalogos.cdsCosteoGruposAfterDelete(DataSet: TDataSet);
begin
     cdsCosteoGrupos.Enviar;
end;

{ cdsCosteoCriterios - Criterios de Costeo JB }

procedure TdmCatalogos.cdsCosteoCriteriosAlAdquirirDatos(Sender: TObject);
begin
     cdsCosteoCriterios.Data := ServerCatalogo.GetCosteoCriterios( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCosteoCriteriosNewRecord(DataSet: TDataSet);
begin
     DataSet.FieldByName( 'CritCostoActivo' ).AsString := K_GLOBAL_SI;
end;

procedure TdmCatalogos.cdsCosteoCriteriosAlModificar(Sender: TObject);
begin
//      ZBaseEdicion_DevEx.ShowFormaEdicion( EditCosteoCriterios_DevEx, TEditCosteoCriterios_DevEx );
end;

procedure TdmCatalogos.cdsCosteoCriteriosBeforePost(DataSet: TDataSet);
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'CritCostoNombre' ).AsString ) then
             DataBaseError( 'El Nombre del Criterio No Puede Quedar Vac�o' );
          if FieldByName( 'CritCostoID' ).AsInteger < 0 then
             DataBaseError( 'Codigo del Criterio no puede ser Negativo.' );
     end;
end;

procedure TdmCatalogos.cdsCosteoCriteriosGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_COSTEO_CRITERIOS, iRight );
end;

procedure TdmCatalogos.cdsCosteoCriteriosAlEnviarDatos(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   ErrorCount: Integer;
{$endif}   
begin
{$ifndef DOS_CAPAS}
     ErrorCount := 0;
     with cdsCosteoCriterios do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enCriteriosCosteo ] );
               end;
          end;
     end;
{$endif}
end;

procedure TdmCatalogos.cdsCosteoCriteriosAfterDelete(DataSet: TDataSet);
begin
     cdsCosteoCriterios.Enviar;
end;

{ cdsCosteoCriteriosPorConcepto - Conceptos por Criterio y Grupo JB}

procedure TdmCatalogos.cdsCosteoCriteriosPorConceptoAlAdquirirDatos(Sender: TObject);
begin
     cdsCosteoCriteriosPorConcepto.Data := ServerCatalogo.GetCosteoCriteriosGrupos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCosteoCriteriosPorConceptoAlCrearCampos(Sender: TObject);
begin
     cdsConceptos.Conectar;
     with cdsCosteoCriteriosPorConcepto do
     begin
          CreateSimpleLookup( cdsConceptos, 'Descripcion', 'CO_NUMERO' );
          CreateSimpleLookup( cdsCosteoCriterios, 'DescripcionCriterios', 'CritCostoID' );
     end
end;


procedure TdmCatalogos.cdsCosteoCriteriosPorConceptoAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsCosteoCriteriosPorConcepto do
     begin
          if State in [ dsEdit ] then
             Post;
          if (ChangeCount > 0) then
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;

procedure TdmCatalogos.cdsCosteoCriteriosPorConceptoAlModificar(Sender: TObject);
begin
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditCosteoCriteriosPorConcepto_DevEx, TEditCosteoCriteriosPorConcepto_DevEx );
end;

procedure TdmCatalogos.cdsCosteoCriteriosPorConceptoNewRecord(DataSet: TDataSet);
begin
     if (cdsCosteoGrupos.Locate( 'GpoCostoCodigo', GrupoDeCosteo, [] )) then
     begin
          DataSet.FieldByName( 'GpoCostoID' ).AsInteger := cdsCosteoGrupos.FieldByName('GpoCostoID').AsInteger;
          DataSet.FieldByName( 'CritCostoID' ).AsInteger := CriterioDeCosteo;
     end;
end;

procedure TdmCatalogos.CosteoAgregaConceptos;
//const
//     K_ATENCION = 'Conceptos de Costeo';
begin
//     with cdsCosteoCriteriosPorConcepto do //BAJAR
//     begin
//		  cdsConceptosDisponibles.Refrescar;
//          if cdsConceptosDisponibles.RecordCount > 0 then
//          begin
//               if CheckDerecho( D_COSTEO_CRITERIOS_CONCEPTO, K_DERECHO_ALTA ) and
//                  CheckDerecho( D_COSTEO_CRITERIOS_CONCEPTO, K_DERECHO_BAJA ) and
//                  CheckDerecho( D_COSTEO_CRITERIOS_CONCEPTO, K_DERECHO_CAMBIO ) then
//               begin
//                    //DevEx
//                         if AddCosteoCriteriosPorConcepto_DevEx = nil then
//                           AddCosteoCriteriosPorConcepto_DevEx := TAddCosteoCriteriosPorConcepto_DevEx.Create( Self );
//                        AddCosteoCriteriosPorConcepto_DevEx.ShowModal;
//               end
//               else
//                    ZetaDialogo.zInformation( K_ATENCION, 'No tiene derecho a Agregar o Borrar o Modificar la Informaci�n.', 0 );
//          end
//          else
//               ZetaDialogo.zInformation( K_ATENCION, 'No hay Conceptos Disponibles para Agregar.', 0 );
//     end;
end;

procedure TdmCatalogos.cdsCosteoCriteriosPorConceptoAfterDelete(DataSet: TDataSet);
begin
     cdsCosteoCriteriosPorConcepto.Enviar;
end;

procedure TdmCatalogos.cdsCosteoCriteriosPorConceptoBeforePost(DataSet: TDataSet);
begin
     with Dataset do
          if FieldByName( 'CO_NUMERO' ).AsInteger <= 0 then
             DataBaseError( 'El C�digo del Concepto No Puede Quedar Vac�o' );
end;

{ cdsConceptosDisponibles - Conceptos Disponibles para Criterio y Grupo JB}

procedure TdmCatalogos.cdsConceptosDisponiblesAlAdquirirDatos(Sender: TObject);
begin
     cdsConceptosDisponibles.Data := ServerCatalogo.GetCosteoConceptosDisponibles(dmCliente.Empresa,GrupoDeCosteo,CriterioDeCosteo);
end;

function TdmCatalogos.GetErrorMessage( E: EReconcileError ): String;
begin
    if PK_Violation( E ) or ( Pos( 'UNIQUE KEY', UpperCase( E.Message )) > 0 ) then
       Result := '� C�digo Ya Existe !'
    else
        Result := GetErrorDescription( E );
end;

procedure TdmCatalogos.cdsCosteoCriteriosPorConceptoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
    Action := cdsCosteoCriteriosPorConcepto.ReconciliaError(DataSet,UpdateKind,E, 'CO_NUMERO;GpoCostoID', GetErrorMessage( E ) );
end;

procedure TdmCatalogos.cdsCosteoGruposReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := cdsCosteoGrupos.ReconciliaError(DataSet,UpdateKind,E, 'GpoCostoCodigo', GetErrorMessage( E ) );
end;

// BIOMETRICO
procedure TdmCatalogos.cdsInvitadoresAfterPost(DataSet: TDataSet);
{$ifndef DOS_CAPAS}
var
   lBiometrico : Boolean;
{$endif}
begin
{$ifndef DOS_CAPAS}
     //EditCatInvitadores
     {with cdsInvitadores do
     begin
          lBiometrico := ( ( FieldByName( 'IV_ID_BIO' ).AsInteger <> 0 ) and
                           ( FieldByName( 'IV_ID_GPO' ).AsString <> VACIO ) );
          EditCatInvitadores.RegistroBiometrico( lBiometrico );
     end;}
{$endif}
end;

procedure TdmCatalogos.cdsEventosEV_TIPNOMChange(Sender: TField);
begin 
     if Sender.NewValue <> Sender.OldValue  then
     begin
          with Sender do
          begin
               dmCliente.cdsTPeriodos.Locate('TP_TIPO', Sender.AsString, []);
               if not ListaIntersectaConfidencialidad(
                  dmCliente.cdsTPeriodos.FieldByName (dmCliente.cdsTPeriodos.LookupConfidenField).AsString,
                  dmCliente.Confidencialidad) then
                  begin
                      FocusControl;
                      DataBaseError( Format ('No es posible asignar el tipo de n�mina: ''%s'' ',
                             [ ObtieneElemento(lfTipoPeriodo, Sender.AsInteger) ]));
                  end;
          end;
     end;
end;

procedure TdmCatalogos.cdsEventosAlCrearCampos(Sender: TObject);
begin
     with cdsEventos do
     begin
          FieldByName( 'EV_TIPNOM' ).OnChange := cdsEventosEV_TIPNOMChange;
          FieldByName( 'EV_NOMTIPO' ).OnChange := cdsEventosEV_TIPNOMChange;
     end;
end;

procedure TdmCatalogos.cdsTablasAmortizacionAlAdquirirDatos(
  Sender: TObject);
var
   Costos :OleVariant;
begin
     cdsTablasAmortizacion.Data := ServerCatalogo.GetTablasAmortizacion(dmCliente.Empresa,Costos );
     cdsCostosAmortizacion.Data := Costos;
end;

procedure TdmCatalogos.cdsTablasAmortizacionAlModificar(Sender: TObject);
begin
//         ZBaseEdicion_DevEx.ShowFormaEdicion( EditTablaAmortizacion_DevEx, TEditTablaAmortizacion_DevEx );
end;

procedure TdmCatalogos.cdsTablasAmortizacionBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost(cdsTablasAmortizacion,'AT_CODIGO');
     with cdsTablasAmortizacion do
     begin
          if StrVacio(FieldByName('AT_DESCRIP').AsString)then
          begin
               FieldByName('AT_DESCRIP').FocusControl;
               DataBaseError('Descripci�n No Puede Quedar Vac�a');
          end;
     end;
     cdsCostosAmortizacion.PostData;  
end;

procedure TdmCatalogos.cdsTablasAmortizacionNewRecord(DataSet: TDataSet);
begin
     with cdsTablasAmortizacion do
     begin
          FieldByName('AT_ACTIVO').AsString := K_GLOBAL_SI;

     end;
end;

procedure TdmCatalogos.cdsCostosAmortizacionNewRecord(DataSet: TDataSet);
begin
     with cdsCostosAmortizacion do
     begin
          FieldByName('AT_CODIGO').AsString := cdsTablasAmortizacion.FieldByName('AT_CODIGO').AsString;
     end;
end;

procedure TdmCatalogos.cdsTablasAmortizacionAfterCancel(DataSet: TDataSet);
begin
     cdsCostosAmortizacion.CancelUpdates;
end;

procedure TdmCatalogos.cdsTablasAmortizacionAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsTablasAmortizacion do
     begin
          PostData;
          cdsCostosAmortizacion.PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, cdsTablasAmortizacion.Tag, Delta, ErrorCount ) );
          end;

     end;
     cdsCostosAmortizacion.Enviar;
end;

procedure TdmCatalogos.cdsTablasAmortizacionAfterDelete(DataSet: TDataSet);
begin
     cdsTablasAmortizacion.Enviar;
end;

procedure TdmCatalogos.cdsCostosAmortizacionAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsCostosAmortizacion do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCostosTablaAmort( dmCliente.Empresa, Delta, ErrorCount ) );
               //MergeChangeLog;
          end;
     end;
end;

procedure TdmCatalogos.cdsCostosAmortizacionBeforePost(DataSet: TDataSet);
var
   indiceEdit : Integer;

procedure RevisarDuplicadosEmpalmes(Inicio,Fin:Integer);
var
   i:Integer;
begin
     with ListaCostoSGM  do
     begin
          for i := 0 to Count - 1 do
          begin
               if TCostoSGM(ListaCostoSGM[i]).Indice <> indiceEdit then
               begin
                    if ( ( TCostoSGM(ListaCostoSGM[i]).EdadInicial <= Inicio) and (TCostoSGM(ListaCostoSGM[i]).EdadFin >= Inicio) )then
                    begin
                         DataBaseError('La Edad Inicial Esta dentro de Otro Rango de Edad');
                         //Exit;
                    end;
                    if ( ( TCostoSGM(ListaCostoSGM[i]).EdadInicial <= Fin ) and (TCostoSGM(ListaCostoSGM[i]).EdadFin >= Fin ) )then
                    begin
                         DataBaseError('La Edad Fin Esta dentro de Otro Rango de Edad');
                         //Exit;
                    end;
               end;
          end;
     end;
end;

procedure AddCostoSGM( EdadI,EdadF:Integer);
var
   CostoNuevo:TCostoSGM;
begin
     CostoNuevo := TCostoSGM.Create;
     with CostoNuevo do
     begin
          EdadInicial := EdadI;
          EdadFin := EdadF;
     end;
     FListaCostoSGM.Add(CostoNuevo);
end;

function GetIndice(EdadI,EdadF:Integer):Integer;
var
   i:Integer;
begin
     with ListaCostoSGM  do
     begin
          Result := -1;
          for i := 0 to Count -1 do
          begin
               if( ( TCostoSGM(ListaCostoSGM[i]).EdadInicial = EdadI ) and ( TCostoSGM(ListaCostoSGM[i]).EdadFin = EdadF ) )then
               begin
                    Result := TCostoSGM(ListaCostoSGM[i]).Indice;
                    Exit;
               end;
          end;
     end;
end;

begin
     with DataSet do
     begin
          if( ( FieldByName('AC_EDADINI').AsInteger < 0) or ( FieldByName('AC_EDADFIN').AsInteger < 0 ) ) then
          begin
               DataBaseError('El rango de Edad debe ser Mayor a 0 (Cero)');
          end;

          if( ( FieldByName('AC_CHOMBRE').AsInteger < 0) or ( FieldByName('AC_CMUJER').AsInteger < 0 ) ) then
          begin
               DataBaseError('El costo debe ser Mayor a 0 (Cero)');
          end;

          if(  FieldByName('AC_EDADINI').AsInteger > FieldByName('AC_EDADFIN').AsInteger )then
          begin
               DataBaseError('La Edad de Inicio debe ser menor � igual a la Edad Final del Rango');
          end;

          if state = dsEdit then
             indiceEdit := GetIndice(VarAsType( FieldByName('AC_EDADINI').OldValue,varInteger),VarAsType( FieldByName('AC_EDADFIN').OldValue,varInteger))
          else
              indiceEdit := -1;

          RevisarDuplicadosEmpalmes(FieldByName('AC_EDADINI').AsInteger,FieldByName('AC_EDADFIN').AsInteger);

     end;
end;

procedure TdmCatalogos.cdsCostosAmortizacionAfterPost(DataSet: TDataSet);
begin
     //cdsTablasAmortizacion.Edit;
     ListaCostoSGM.Clear;
     LlenarListaCostosSGM;
end;

procedure TdmCatalogos.LlenarListaCostosSGM;
var
   Costo:TCostoSGM;
   index:Integer;
begin
     with cdsCostosAmortizacion do
     begin
          First;
          index := 1;
          while not eof do
          begin
               Costo := TCostoSGM.Create;
               Costo.Indice := index;
               Costo.EdadInicial  := FieldByName('AC_EDADINI').AsInteger;
               Costo.EdadFin := FieldByName('AC_EDADFIN').AsInteger;
               dmCatalogos.ListaCostoSGM.Add(Costo);
               index := index + 1;
               Next;
          end;
     end;
end;

procedure TdmCatalogos.cdsCostosAmortizacionAfterDelete(DataSet: TDataSet);
begin
     //cdsTablasAmortizacion.Edit;
     ListaCostoSGM.Clear;
     LlenarListaCostosSGM
end;

procedure TdmCatalogos.cdsCostosAmortizacionAlCrearCampos(Sender: TObject);
begin
     with cdsCostosAmortizacion do
     begin
          MaskPesos('AC_CHOMBRE');
          MaskPesos('AC_CMUJER');
     end;
end;

procedure TdmCatalogos.cdsVigenciasSGMAlCrearCampos(Sender: TObject);
begin
     with cdsVigenciasSGM do
     begin
          MaskPesos('PV_CFIJO');
     end;
end;

procedure TdmCatalogos.cdsCostosAmortizacionAfterEdit(DataSet: TDataSet);
begin
     //cdsTablasAmortizacion.Edit;
end;

procedure TdmCatalogos.cdsTurnosAlCrearCampos(Sender: TObject);
begin
     with cdsTurnos do
     begin
          FieldByName( 'TU_CODIGO' ).OnValidate := cdsTurnosTU_CODIGOValidate;
     end;
end;

procedure TdmCatalogos.cdsTurnosTU_CODIGOValidate(Sender: TField);
begin
     if Pos (K_ESPACIO, Sender.AsString) > 0 then
        DataBaseError( 'C�digo De Turno No Puede Contener Espacios');
end;

procedure TdmCatalogos.cdsPuestosAlCrearCampos(Sender: TObject);
begin  
     with cdsPuestos do
     begin
          FieldByName( 'PU_CODIGO' ).OnValidate := cdsPuestosPU_CODIGOValidate;
     end;
end;

procedure TdmCatalogos.cdsPuestosPU_CODIGOValidate(Sender: TField);
begin
     if Pos (K_ESPACIO, Sender.AsString) > 0 then
        DataBaseError( 'C�digo De Puesto No Puede Contener Espacios');
end;

procedure TdmCatalogos.cdsCursosCU_CODIGOValidate(Sender: TField);
begin
     if Pos (K_ESPACIO, Sender.AsString) > 0 then
        DataBaseError( 'C�digo De Curso No Puede Contener Espacios');
end;

procedure TdmCatalogos.cdsCompetenciasAlAdquirirDatos(Sender: TObject);
var
   oRevisiones,oCursos,oNiveles :OleVariant;
begin
     cdsCompetencias.Data := ServerCatalogo.GetCompetencias(dmCliente.Empresa,oRevisiones,oNiveles,oCursos);
     cdsCompRevisiones.Data := oRevisiones;
     cdsCompNiveles.Data := oNiveles;
     cdsCompCursos.Data := oCursos;
end;

procedure TdmCatalogos.cdsCatPerfilesAlAdquirirDatos(Sender: TObject);
VAR
   oRevisiones,oPtoGpoCompeten:OleVariant;
begin
     cdsCatPerfiles.Data := ServerCatalogo.GetCatPerfiles(dmCliente.Empresa,VACIO,oRevisiones,oPtoGpoCompeten);
     cdsRevPerfiles.Data := oRevisiones;
     cdsGpoCompPuesto.Data  := oPtoGpoCompeten;
end;

procedure TdmCatalogos.cdsCompetenciasAlModificar(Sender: TObject);
begin
//      ZBaseEdicion_DevEx.ShowFormaEdicion( EditCompetencias_DevEx, TEditCompetencias_DevEx );
end;

procedure TdmCatalogos.cdsCompetenciasBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          cdsBeforePost(DataSet,'CC_CODIGO');
          ValidaMemoField(DataSet,'CC_DETALLE');
          with DataSet do
          begin
               if StrVacio(FieldByName('CC_ELEMENT').AsString)then
               begin
                    FieldByName('CC_ELEMENT').FocusControl;
                    DataBaseError('Nombre no puede quedar Vac�o');
               end;
               if StrVacio(FieldByName('TB_CODIGO').AsString)then
               begin
                    FieldByName('TB_CODIGO').FocusControl;
                    DataBaseError('Tipo no puede quedar Vac�o');
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsCompetenciasAlEnviarDatos(Sender: TObject);
var
   ErrorCount,ErrorCountNiv,ErrorCountCur,ErrorCountRev: Integer;
   RevisionesRes,CursosRes,NivelesRes :OleVariant;
   sComp:string;

   procedure ReconciliarDetalle(ErrorC :Integer;ClienteDatos:TZetaClientDataSet;Resultado:OleVariant );
   begin
        if ( ClienteDatos.ChangeCount > 0)then
        begin
             if ( ErrorC = 0 ) or ClienteDatos.Reconcile( Resultado ) then
             begin
                  cdsCompetencias.Refrescar;
                  cdsCompetencias.Locate( 'CC_CODIGO', sComp, [] );
             end
             else if ( ClienteDatos.ChangeCount > 0 ) then
             begin
                  ClienteDatos.Edit;
             end;
        end;
   end;

begin
     ErrorCount := 0;
     ErrorCountNiv := 0;
     ErrorCountCur := 0;
     ErrorCountRev := 0;
     with cdsCompetencias do
     begin
          PostData;
          sComp:= cdsCompetencias.FieldByName('CC_CODIGO').AsString;
          cdsCompNiveles.PostData;
          cdsCompRevisiones.PostData;
          cdsCompCursos.PostData;
          try
             DisableControls;
             if ( Changecount > 0 ) or ( cdsCompNiveles.ChangeCount > 0 )  or ( cdsCompCursos.ChangeCount > 0 ) or ( cdsCompRevisiones.ChangeCount > 0 ) then
             begin
                  if Reconcile( ServerCatalogo.GrabaCompetencias( dmCliente.Empresa,
                                                               DeltaNull,
                                                               cdsCompNiveles.DeltaNull,
                                                               cdsCompCursos.DeltaNull,
                                                               cdsCompRevisiones.DeltaNull,
                                                               ErrorCount,
                                                               NivelesRes,
                                                               CursosRes,
                                                               RevisionesRes,
                                                               ErrorCountNiv,
                                                               ErrorCountCur,
                                                               ErrorCountRev  ) )then
                  begin
                       Refrescar;
                       Locate( 'CC_CODIGO', sComp, [] );
                  end;
             end;
             if (cdsCompNiveles.ChangeCount > 0)then
                ReconciliarDetalle(ErrorCountNiv,cdsCompNiveles,NivelesRes);
             if (cdsCompCursos.ChangeCount > 0)then
                ReconciliarDetalle(ErrorCountCur,cdsCompCursos,CursosRes);
             if (cdsCompRevisiones.ChangeCount > 0)then
                ReconciliarDetalle(ErrorCountRev,cdsCompRevisiones,RevisionesRes);
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmCatalogos.cdsCompNivelesBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if StrVacio(FieldByName('NC_DESCRIP').AsString)then
          begin
               FieldByName('NC_DESCRIP').FocusControl;
               DataBaseError('Descripci�n de Nivel no puede quedar Vac�o');
          end;
          if FieldByName('NC_NIVEL').AsInteger = 0 then
          begin
               FieldByName('NC_NIVEL').FocusControl;
               DataBaseError('Nivel Debe ser Mayor a Cero');
          end;
     end;
end;

procedure TdmCatalogos.cdsCompNivelesNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          //FieldByName('NC_PORCENT').AsFloat := 0;
          //FieldByName('NC_COLOR').AsString := 'Color';
          FieldByName('CC_CODIGO').AsString := cdsCompetencias.FieldByName('CC_CODIGO').AsString;
     end;
end;

procedure TdmCatalogos.cdsCompRevisionesNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName('RC_FEC_INI').AsDateTime := Now;
          FieldByName('RC_FEC_FIN').AsDateTime := NullDateTime;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('CC_CODIGO').AsString := cdsCompetencias.FieldByName('CC_CODIGO').AsString;
     end;
end;

procedure TdmCatalogos.cdsCompRevisionesBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if FieldByName('RC_FEC_INI').AsDateTime <= FechaUltRevComp then
          begin
               FieldByName('RC_FEC_INI').FocusControl;
               DataBaseError(Format('La Fecha de Inicio Debe Ser Mayor a: %s',[DateToStr(FechaUltRevComp)]));
          end;

     end;
end;

procedure TdmCatalogos.cdsCompCursosNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName('CC_CODIGO').AsString := cdsCompetencias.FieldByName('CC_CODIGO').AsString;

     end;
end;

procedure TdmCatalogos.cdsCompCursosBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if StrVacio(FieldByName('CU_CODIGO').AsString)then
          begin
               FieldByName('CU_CODIGO').FocusControl;
               DataBaseError('Curso No Puede Quedar Vac�o');
          end;
     end;
end;

procedure TdmCatalogos.cdsCompetenciasAfterDelete(DataSet: TDataSet);
begin
     cdsCompetencias.Enviar;
end;

procedure TdmCatalogos.cdsCompetenciasNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
           FieldByName('CC_ACTIVO').AsString := K_GLOBAL_SI;
           FieldByName('TB_CODIGO').AsString := dmTablas.cdsTCompetencias.FieldByName('TB_CODIGO').AsString;
     end;
end;

procedure TdmCatalogos.cdsCompetenciasAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsTCompetencias.Conectar;
     cdsCompetencias.CreateSimpleLookup( dmTablas.cdsTCompetencias, 'TB_ELEMENT', 'TB_CODIGO' );
     cdsCompetencias.FieldByName('CC_ACTIVO').OnGetText := ActivoGetText;
end;

procedure TdmCatalogos.cdsCompetenciasAfterCancel(DataSet: TDataSet);
begin
     cdsCompRevisiones.CancelUpdates;
     cdsCompNiveles.CancelUpdates;
     cdsCompCursos.CancelUpdates;
end;

procedure TdmCatalogos.cdsCompCursosAlCrearCampos(Sender: TObject);
begin
     cdsCursos.Conectar;
     cdsCompCursos.CreateSimpleLookup( cdsCursos, 'CU_DESCRIP', 'CU_CODIGO' );
     cdsCompCursos.FieldByName('CU_CODIGO').OnValidate := cdsCompCursosCU_CODIGOValidate;
end;

procedure TdmCatalogos.cdsCompCursosCU_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if StrVacio(cdsCursos.GetDescripcion( Sender.AsString ) )then
             DataBaseError( Format('No Existe Curso: %s ; Favor de Buscar o Capturar uno diferente',[Sender.AsString] ) );   
     end;
end;

procedure TdmCatalogos.cdsCatPerfilesAlModificar(Sender: TObject);
begin
//      ZBaseEdicion_DevEx.ShowFormaEdicion( EditCPerfiles_DevEx , TEditCPerfiles_DevEx );
     end;

procedure TdmCatalogos.cdsCatPerfilesBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          cdsBeforePost(DataSet,'CP_CODIGO');
          ValidaMemoField(DataSet,'CP_DETALLE');
          with DataSet do
          begin
               if StrVacio(FieldByName('CP_ELEMENT').AsString)then
               begin
                    FieldByName('CP_ELEMENT').FocusControl;
                    DataBaseError('Nombre no puede quedar Vac�o');
               end;
               if StrVacio(FieldByName('TB_CODIGO').AsString)then
               begin
                    FieldByName('TB_CODIGO').FocusControl;
                    DataBaseError('Tipo no puede quedar Vac�o');
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsCatPerfilesNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName('TB_CODIGO').AsString := dmTablas.cdsTPerfiles.FieldByName('TB_CODIGO').AsString;
          FieldByName('CP_ACTIVO').AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmCatalogos.cdsCatPerfilesAfterCancel(DataSet: TDataSet);
begin
     cdsRevPerfiles.CancelUpdates;  
end;

procedure TdmCatalogos.cdsCatPerfilesAlEnviarDatos(Sender: TObject);
var
   ErrorCount,ErrorCountR: Integer;
   oResultRevisiones:OleVariant;
   sPerfil :string;
begin
     ErrorCount := 0;
     with cdsCatPerfiles do
     begin
          PostData;
          sPerfil := FieldByName('CP_CODIGO').AsString;
          cdsRevPerfiles.PostData;
          if ( Changecount > 0 ) or ( cdsRevPerfiles.ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaCPerfiles( dmCliente.Empresa,DeltaNull,cdsRevPerfiles.DeltaNull,oResultRevisiones , ErrorCount ,ErrorCountR ) )then
               begin
                    if ( ErrorCountR = 0 ) or cdsRevPerfiles.Reconcile( oResultRevisiones ) then
                    begin
                         Refrescar;
                         Locate( 'CP_CODIGO', sPerfil, [] );
                    end
                    else if ( cdsRevPerfiles.ChangeCount > 0 ) then
                    begin
                         cdsRevPerfiles.Edit;
                    end;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsRevPerfilesNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName('CP_CODIGO').AsString := cdsCatPerfiles.FieldByName('CP_CODIGO').AsString;
          FieldByName('RP_FEC_INI').AsDateTime := Now;
          FieldByName('RP_FEC_FIN').AsDateTime := NullDateTime;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmCatalogos.cdsMatPerfilCompsAlAdquirirDatos(Sender: TObject);
begin
      cdsMatPerfilComps.Data := ServerCatalogo.GetMatrizPerfilComps(dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsMatPerfilCompsAlCrearCampos(Sender: TObject);
begin
      cdsCompetencias.Conectar;
      with cdsMatPerfilComps do
      begin
           CreateSimpleLookup( cdsCompetencias, 'CC_ELEMENT', 'CC_CODIGO' );
           CreateSimpleLookup( cdsCatPerfiles, 'CP_ELEMENT', 'CP_CODIGO' );
           //CreateLookup( cdsCompNiveles, 'NC_DESCRIP', 'CC_CODIGO,NC_NIVEL','CC_CODIGO,NC_NIVEL','NC_DESCRIP');
           MaskTasa('PC_PESO');
           MaskFecha('RP_FEC_INI');
      end;
end;

procedure TdmCatalogos.cdsMatPerfilCompsNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName('CP_CODIGO').AsString := cdsCatPerfiles.FieldByName('CP_CODIGO').AsString;
     end;
end;

function TdmCatalogos.GetUltimaRevisionPerfil:TDateTime;
begin
     Result := NullDateTime;
     with cdsRevPerfiles do
     begin
          Filtered := False;
          Filter := Format(' CP_CODIGO = ''%s''',[cdsCatPerfiles.FieldByName('CP_CODIGO').AsString]);
          Filtered := True;
          //OrdenarPor(cdsRevPerfiles,'RP_FEC_INI');
          if RecordCount > 0 then
             Result := FieldByName('RP_FEC_INI').AsDateTime;
     end;
end;


function TdmCatalogos.GetUltimaRevisionCompetencia:TDateTime;
begin
     Result := NullDateTime;
     with cdsCompRevisiones  do
     begin
          Filtered := False;
          Filter := Format(' CC_CODIGO = ''%s''',[cdsCompetencias.FieldByName('CC_CODIGO').AsString]);
          Filtered := True;
          //OrdenarPor(cdsCompRevisiones,'RC_FEC_INI');
          if RecordCount > 0 then
             Result := FieldByName('RC_FEC_INI').AsDateTime;
     end;
end;
procedure TdmCatalogos.cdsMatPerfilCompsBeforePost(DataSet: TDataSet);
begin
     with cdsMatPerfilComps do
     begin
          if StrVacio(FieldByName('CC_CODIGO').AsString) then
             DatabaseError('Seleccionar Competencia');

          if FieldByName('NC_NIVEL').AsInteger <= 0 then
             DatabaseError('Seleccionar Nivel');

          if GetUltimaRevisionPerfil = NullDateTime then
             DatabaseError('Grupo de Competencias No Tiene Revisiones');

          FieldByName('RP_FEC_INI').AsDateTime := GetUltimaRevisionPerfil;
     end;
end;

procedure TdmCatalogos.cdsMatPerfilCompsAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;

     with cdsMatPerfilComps do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogo.GrabaMatrizPerfComp( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    Refrescar;
                    dmRecursos.cdsMatrizHabilidades.SetDataChange;
               end;
          end;
     end;
end;

procedure TdmCatalogos.cdsCompRevisionesAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsCompRevisiones do
     begin
          MaskFecha( 'RC_FEC_INI' );
          MaskFecha( 'RC_FEC_FIN' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmCatalogos.cdsCatPerfilesAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsTPerfiles.Conectar;
     cdsCatPerfiles.CreateSimpleLookup( dmTablas.cdsTPerfiles, 'TB_ELEMENT', 'TB_CODIGO' );
     cdsCatPerfiles.FieldByName('CP_ACTIVO').OnGetText := ActivoGetText;
end;

procedure TdmCatalogos.cdsRevPerfilesAlCrearCampos(Sender: TObject);
begin
      dmSistema.cdsUsuarios.Conectar;
     with cdsRevPerfiles do
     begin
          MaskFecha( 'RP_FEC_INI' );
          MaskFecha( 'RP_FEC_FIN' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmCatalogos.cdsMatPerfilCompsAlModificar(Sender: TObject);
begin
//     ZBaseEdicion_DevEx.ShowFormaEdicion( EditMatPerComps_DevEx, TEditMatPerComps_DevEx );
end;

procedure TdmCatalogos.ActivoGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
    if DisplayText then
    begin
         if StrLleno(Sender.AsString)then
         begin
              if zStrToBool(Sender.AsString) then
                 Text := K_PROPPERCASE_SI
              else
                 Text := K_PROPPERCASE_NO;
         end
         else
             Text := VACIO;
    end
    else
        Text := Sender.AsString;
end;


procedure TdmCatalogos.cdsCompRevisionesAfterEdit(DataSet: TDataSet);
begin
     with cdsCompetencias do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;

end;

procedure TdmCatalogos.cdsRevPerfilesAfterEdit(DataSet: TDataSet);
begin
     with cdsCatPerfiles do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmCatalogos.cdsGpoCompPuestoBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if StrVacio(FieldByName('CP_CODIGO').AsString)then
          begin
               DataBaseError('Grupo de Competencias es Obligatorio');
          end;
     end;
end;

procedure TdmCatalogos.cdsGpoCompPuestoNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName('PU_CODIGO').AsString  := cdsPuestos.FieldByName('PU_CODIGO').AsString;
          FieldByName('US_CODIGO').AsInteger  := dmCliente.Usuario;
          FieldByName('PP_ACTIVO').AsString  := K_GLOBAL_SI;
          FieldByName('PP_FEC_REG').AsDateTime  := Now;
          FieldByName('PP_PESO').AsFloat  := 0;
     end;
end;

procedure TdmCatalogos.cdsGpoCompPuestoAlCrearCampos(Sender: TObject);
begin
     with cdsGpoCompPuesto do
     begin
          MaskFecha( 'PP_FEC_REG' );
          MaskTasa('PP_PESO');
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( cdsCatPerfiles, 'CP_ELEMENT', 'CP_CODIGO' );
          FieldByName( 'CP_CODIGO' ).OnValidate := cdsGpoCompPuesto_CP_CODIGOValidate;
     end;
end;

procedure TdmCatalogos.cdsGpoCompPuesto_CP_CODIGOValidate(Sender: TField);
begin
     with Sender do
     begin
          if StrVacio(cdsCatPerfiles.GetDescripcion(Sender.AsString))then
             DataBaseError( Format('No Existe Grupo de Competencias: %s ; Favor de Buscar o Capturar uno diferente',[Sender.AsString] ) );
     end;
end;

procedure TdmCatalogos.cdsGpoCompPuestoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsGpoCompPuesto do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, cdsGpoCompPuesto.Tag, Delta, ErrorCount ));
          end;
     end;
end;

procedure TdmCatalogos.cdsGpoCompPuestoAfterEdit(DataSet: TDataSet);
begin
     cdsPuestos.Edit;
end;

procedure TdmCatalogos.cdsPuestosAfterCancel(DataSet: TDataSet);
begin
     {$ifndef DOS_CAPAS}
     cdsGpoCompPuesto.CancelUpdates;
     {$endif}
end;

procedure TdmCatalogos.cdsCatPerfilesAfterDelete(DataSet: TDataSet);
begin
     cdsCatPerfiles.Enviar;
end;

procedure TdmCatalogos.cdsCompetenciasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError:string;

   function MensajeMatriz:Boolean;
   begin
        Result :=  Pos( 'C_PER_COMP',E.Message) > 0;
   end;

   function MensajeEvaluacion:Boolean;
   begin
        Result :=  Pos( 'C_EVA_COMP',E.Message) > 0;
   end;

   function MensajeExpediente:Boolean;
   begin
        Result :=  Pos( 'C_EMP_COMP',E.Message) > 0;
   end;

   function GetRelacion:string;
   begin
        Result := VACIO;
        if MensajeMatriz then
           Result := 'la Matriz de Funciones'
        else if MensajeEvaluacion then
             Result := 'las Evaluaciones de Competencias del Empleado'
        else if MensajeExpediente then
                Result := 'las Competencias Individuales del Empleado';

   end;

begin
     if ( FK_Violation(E) ) and ( UpdateKind = ukDelete ) then
     begin
          with DataSet do
          begin
               sError := Format('No se Puede Borrar la Competencia: %s ;%s est� Relacionada en %s ',[ FieldByName('CC_CODIGO').AsString +' : '+ FieldByName('CC_ELEMENT').AsString,CR_LF,GetRelacion]);
          end;
          ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
          case UpdateKind of
               ukDelete: Action := raCancel;
          else
              Action := raAbort;
          end;
     end
     else
         Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;

procedure TdmCatalogos.cdsMatPerfilCompsAfterDelete(DataSet: TDataSet);
begin
     cdsMatPerfilComps.Enviar;
end;

procedure TdmCatalogos.cdsCatPerfilesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError:string;
   function MensajeMatriz:Boolean;
   begin
        Result :=  Pos( 'C_PER_COMP',E.Message) > 0;
   end;

   function MensajePuestos:Boolean;
   begin
        Result :=  Pos( 'C_PERF_PTO',E.Message) > 0;
   end;

    function GetRelacion:string;
   begin
        Result := VACIO;
        if MensajeMatriz then
           Result := ' en la Matriz de Funciones'
        else if MensajePuestos then
             Result := 'en las Competencias de alg�n Puesto';

   end;

begin
     if ( FK_Violation(E) ) and ( UpdateKind = ukDelete ) then
     begin
           with DataSet do
           begin
                sError := Format('No se Puede Borrar Grupo de Competencias: %s ; %s Est� Relacionado %s ',[ FieldByName('CP_CODIGO').AsString +' : '+ FieldByName('CP_ELEMENT').AsString,CR_LF,GetRelacion]);
           end;
           ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
           case UpdateKind of
                ukDelete: Action := raCancel;
           else
               Action := raAbort;
           end;
     end
     else
         Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;

procedure TdmCatalogos.cdsMatPerfilCompsReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError:string;
   function MensajeNiveles:Boolean;
   begin
        Result :=  Pos( 'C_NIV_COMP',E.Message) > 0;
   end;


   function GetRelacion:string;
   begin
        Result := VACIO;
        if MensajeNiveles then
           Result := ' '
   end;

begin
     if ( FK_Violation(E) ) and ( UpdateKind = ukInsert ) then
     begin
           with DataSet do
           begin
                sError := Format('No se Puede Agregar Competencia: %s ; a la Matriz  %s No tiene al Menos un Nivel',[ FieldByName('CC_CODIGO').AsString +' : '+ FieldByName('CC_ELEMENT').AsString,CR_LF,GetRelacion]);
           end;
           ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
           case UpdateKind of
                ukInsert: Action := raCancel;
           else
               Action := raAbort;
           end;
     end
     else
         Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );

end;

function TdmCatalogos.GetNivelRequeridoCompetencia(Competencia:string;var Descripcion:string):Integer;
var
   GpoComp:string;
   Nivel:Integer;
begin
     cdsMatPerfilComps.Conectar;
     GpoComp := cdsCatPerfiles.FieldByName('CP_CODIGO').AsString;
     With cdsMatPerfilComps do
     begin
          Locate('CC_CODIGO;CP_CODIGO',VarArrayOf([Competencia,GpoComp]),[]);
          Nivel := FieldByName('NC_NIVEL').AsInteger;
     end;

     with cdsCompNiveles do
     begin
          Locate('CC_CODIGO;NC_NIVEL',VarArrayOf([Competencia,Nivel]),[]);
          Descripcion := FieldByName('NC_DESCRIP').AsString;
     end;
     Result := Nivel;
end;
procedure TdmCatalogos.cdsAulasAlCrearCampos(Sender: TObject);
begin
      with  cdsAulas do
      begin
       MaskFecha('RV_FEC_INI');
      end;
end;


procedure TdmCatalogos.PE_STATUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := ZetaCommonTools.GetDescripcionStatusPeriodo( eStatusPeriodo( Sender.AsInteger) , eStatusTimbrado(Sender.DataSet.FieldByName('PE_TIMBRO').AsInteger  ) ) ;
end;



procedure TdmCatalogos.cdsTiposSATAfterDelete(DataSet: TDataSet);
begin
//
end;

procedure TdmCatalogos.cdsTiposSATAlAdquirirDatos(Sender: TObject);
begin
     cdsTiposSAT.Data := ServerCatalogo.GetTiposSAT( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsTiposSATAlCrearCampos(Sender: TObject);
begin
     with cdsTiposSAT do
     begin
          ListaFija( 'TB_SAT_CLA', lfClaseConceptosSAT) ;
     end;
end;

procedure TdmCatalogos.cdsTiposSATAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsTiposSAT do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogo.GrabaCatalogo( dmCliente.Empresa, cdsTiposSAT.Tag, Delta, ErrorCount ));
          end;
     end;
end;

procedure TdmCatalogos.cdsTiposSATAlModificar(Sender: TObject);
begin
    ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoSAT, TEditTipoSAT );
end;

procedure TdmCatalogos.cdsTiposSATBeforePost(DataSet: TDataSet);
begin
 with cdsTiposSat do
    begin
          if ( not StrLleno( FieldByName( 'TB_CODIGO' ).AsString ) ) then
          begin
               FieldByName( 'TB_CODIGO' ).FocusControl;
               DataBaseError( 'C�digo no puede quedar vac�o' );
          end;
    end;
end;

procedure TdmCatalogos.cdsTiposSATGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_TAB_OFI_SAT_TIPOS_CONCEPTO, iRight );
end;

procedure TdmCatalogos.cdsTiposSATLookupSearch(Sender: TZetaLookupDataSet;
  var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
     lOk := FBusquedaTipoSAT_Version_DevEx.BusquedaSATDevex_ShowSearchForm( Sender, sFilter, sKey, sDescription)
end;


procedure TdmCatalogos.cdsFoliosBeforeDelete(DataSet: TDataSet);
begin
     with cdsOrdFolios do
     begin
          First;
          while not Eof do
          begin
               Delete;
          end;
     end;
end;

procedure TdmCatalogos.cdsRPatronBeforeDelete(DataSet: TDataSet);
begin
    with cdsPRiesgo do
    begin
          First;
          while not Eof do
          begin
               Delete;
          end;
    end;
end;

procedure TdmCatalogos.cdsMatrizCertificPuestoBeforeDelete(
  DataSet: TDataSet);
begin
      with cdsCerNivel do
    begin
          First;
          while not Eof do
          begin
               Delete;
          end;
    end;
end;

procedure TdmCatalogos.cdsSSocialBeforeDelete(DataSet: TDataSet);
begin
     with cdsPrestaci  do
     begin
          First;
          while not Eof do
          begin
               Delete;
          end;
     end;
end;

procedure TdmCatalogos.cdsMatrizCursoBeforeDelete(DataSet: TDataSet);
begin
     with cdsEntNivel do
    begin
          First;
          while not Eof do
          begin
               Delete;
          end;
    end;
end;

end.
