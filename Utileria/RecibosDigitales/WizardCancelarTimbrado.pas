unit WizardCancelarTimbrado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,
  Timbres,
  FProcesoTimbradoPendiente,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaDBTextBox, ExtCtrls, ZetaKeyLookup, Mask,
  DBCtrls,FTimbramexClasses, FTimbramexHelper, Grids, DBGrids, Buttons,
  ZetaSmartLists, FBaseReportes_DevEx, ZetaCommonLists, TressMorado2013,
  ZetaCXGrid, UManejadorPeticiones, UPeticionWS, UThreadStack, UThreadPolling, UInterfaces,
  ZetaEdit, DCatalogos, ZetaCommonClasses, ZetaCommonTools, ZGridModeTools,
  ZetaKeyLookup_DevEx, cxMemo, cxRadioGroup,DGlobal, dxGDIPlusClasses, cxImage;

type
  TWizardCancelarTimbradoForm = class(TdxWizardControlForm, ILogBitacora)
    dxWizardControl1: TdxWizardControl;
    dxWizardControlPageTimbrado: TdxWizardControlPage;
    cxGroupBox1: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxProgressBarEnvio: TcxProgressBar;
    cxStatus: TcxLabel;
    dxWizardControlPage2: TdxWizardControlPage;
    dataSourceTimbrar: TDataSource;
    dxWizardControlPageCuentaTimbrado: TdxWizardControlPage;
    dataSourceContribuyente: TDataSource;
    DataSourceCuentas: TDataSource;
    DataSourceRecibos: TDataSource;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    cxGroupBoxResultados: TcxGroupBox;
    cxLabel3: TcxLabel;
    DataSourceErrores: TDataSource;
    cxGroupBox2: TcxGroupBox;
    PeriodoTipoLbl: TLabel;
    Label6: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label8: TLabel;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    iTipoNomina: TZetaTextBox;
    ZetaDBTextBox2: TZetaDBTextBox;
    DatasetPeriodo: TDataSource;
    Label1: TLabel;
    gridPeriodos: TZetaCXGrid;
    gridPeriodosDBTableView1: TcxGridDBTableView;
    gridPeriodosDBTableView1Column10: TcxGridDBColumn;
    gridPeriodosDBTableView1Column8: TcxGridDBColumn;
    gridPeriodosDBTableView1Column2: TcxGridDBColumn;
    gridPeriodosDBTableView1Column9: TcxGridDBColumn;
    gridPeriodosDBTableView1Column5: TcxGridDBColumn;
    gridPeriodosDBTableView1Column7: TcxGridDBColumn;
    gridPeriodosDBTableView1Column11: TcxGridDBColumn;
    gridPeriodosDBTableView1Column4: TcxGridDBColumn;
    gridPeriodosLevel1: TcxGridLevel;
    dxWizardControlPageEmpleados: TdxWizardControlPage;
    FiltrosGB: TGroupBox;
    sFiltroLBL: TLabel;
    sCondicionLBl: TLabel;
    ECondicion: TZetaKeyLookup_DevEx;
    cxCheckBoxFiltrar: TcxCheckBox;
    Seleccionar: TcxButton;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    RBTodos: TcxRadioButton;
    RBRango: TcxRadioButton;
    RBLista: TcxRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    BInicial: TcxButton;
    BFinal: TcxButton;
    BLista: TcxButton;
    BAgregaCampo: TcxButton;
    sFiltro: TcxMemo;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1Level1: TcxGridLevel;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    ZetaCXGrid1DBTableView1NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1DESCRIPCION: TcxGridDBColumn;
    ZetaDBTextBox3: TZetaDBTextBox;
    lblDI_IP: TLabel;
    cxButton2: TcxButton;
    Panel1: TPanel;
    Label5: TLabel;
    Panel2: TPanel;
    Advertencia: TcxLabel;
    cxImage1: TcxImage;
    Panel3: TPanel;
    ZetaSpeedButton1: TcxButton;
    ZetaDBTextBox1: TZetaDBTextBox;
    Label9: TLabel;
    Label2: TLabel;
    CT_CODIGO: TZetaKeyLookup_DevEx;
    procedure cxButton1Click(Sender: TObject);
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure CT_CODIGOValidLookup(Sender: TObject);
        procedure Exportar;
    procedure ZetaSpeedButton1Click(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    function BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
    procedure cxCheckBoxFiltrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gridPeriodosDBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure DataSourceErroresUpdateData(Sender: TObject);
    procedure DataSourceErroresDataChange(Sender: TObject; Field: TField);
  private

     FVerificacion: Boolean;
     FTipoRango: eTipoRangoActivo;
     FEmpleadoCodigo: String;
     FEmpleadoFiltro: String;
     FParameterList: TZetaParams;
     FDescripciones: TZetaParams;
     FCancelandoNominas: boolean; //Indica si se estan cancelando nominas en Sistema de Timbrado
     //Folios Repetidos
     FParamsTimbradoNomina: TZetaParams;
    { Private declarations }
    function CancelarNomina : boolean;
    function GetRecibosDataSet : boolean;
    function GetRazonSocial : string;

    procedure HabilitarDeshabilitarBotones;

  protected
    { Protected declarations }
    property ParameterList: TZetaParams read FParameterList;
    property Descripciones: TZetaParams read FDescripciones;
    property Verificacion: Boolean read FVerificacion write FVerificacion;

    function Verificar: Boolean; virtual;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure CargaListaVerificacion;
    procedure CargaParametros;
    procedure CargaListaNominas;

    procedure SetVerificacion( const Value: Boolean );
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );
  public
    { Public declarations }
    property TipoRango: eTipoRangoActivo read FTipoRango;
    property ParamsTimbradoNomina: TZetaParams read FParamsTimbradoNomina write FParamsTimbradoNomina;
    procedure EscribirBitacoraLog( sMensaje : string );
    procedure EscribirBitacoraError( sMensaje : string );
    Function EntidadActiva:integer;
  end;


var
  WizardCancelarTimbradoForm: TWizardCancelarTimbradoForm;


function ShowWizardTimbrado : boolean;

implementation

uses DTablas, Dcliente, DProcesos,
     ZetaTipoEntidad,
     DBasicoCliente,
     ZetaClientTools,
     ZConstruyeFormula,
     ZConfirmaVerificacion,
     ZetaBuscaEmpleado_DevExTimbrado,
     ZetaBuscaEmpleado_DevEx,
     ZBaseSelectGrid_DevEx,
     ZBasicoSelectGrid_DevEx,
     FEmpleadoTimbradoGridSelect,
     ZGlobalTress,
     ZetaBuscaEmpleado_DevExAvanzada,
     ZetaBuscaEmpleado_DevExTimbradoAvanzada;


{$R *.dfm}

function ShowWizardTimbrado : boolean;
begin
    Result := FALSE;
end;

procedure TWizardCancelarTimbradoForm.cxButton1Click(Sender: TObject);
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
var
   sEchoResponse : string;
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;
begin

     try

        sServer := '';
        sServer := GetTimbradoServer;
        timbresSoap := nil;

        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
           if ( timbresSoap <> nil ) then
           begin
				crypt := TWSTimbradoCrypt.Create;
                sEchoResponse := crypt.Desencriptar(  timbresSoap.Echo( crypt.Encriptar(TESTING_MESSAGE) ) );
			  //sEchoResponse :=  					  timbresSoap.Echo(                 TESTING_MESSAGE    );
				FreeAndNil( crypt );                				
                ZInformation( Self.Caption, 'Hay Conectividad con el Servidor de Timbrado' , 0);
           end;
        end
        else
        begin
           zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
        end;
     except on Error: Exception do
               zError( Self.Caption, StringReplace( K_SIN_CONECTIVIDAD, ':', '', [rfReplaceAll] ), 0);
     end;


end;

procedure TWizardCancelarTimbradoForm.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin

  if AKind = wcbkCancel then
  begin
//    ModalResult := mrCancel;
    if ( FCancelandoNominas ) then
    begin
         ModalResult := mrCancel;
         if ZetaDialogo.ZConfirm(Self.Caption,'Al salir de esta ventana el proceso de cancelaci�n continuara en el Sistema de Timbrado' + CR_LF + 'ejecute de nuevo la cancelaci�n m�s tarde' + CR_LF + '�Desea Continuar?' ,0, mbNo ) then //Advertencia
         begin
              FCancelandoNominas := FALSE;
         end
    end
    else
    begin
         if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
         begin
               if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
               begin
                    ModalResult := mrCancel;
               end;
         end
         else
         begin
              ModalResult := mrCancel;
         end;
    end;
  end
  else if AKind = wcbkFinish then
  begin
    cxProgressBarEnvio.Visible := True;
    dxWizardControl1.Buttons.Back.Enabled := FALSE;
    dxWizardControl1.Buttons.Finish.Enabled := FALSE;
    //Preguntar si desea cancelar timbrado de nomina
    if ZetaDialogo.ZConfirm( self.Caption, 'Al iniciar este proceso no podr� ser interrumpido' + CR_LF + '�Desea Continuar?', 0, mbNo ) then
    begin
         if CancelarNomina then
         begin
              {ZInformation( Self.Caption, Format( 'El Timbrado de Nomina del contribuyente %s fue Registrado Exitosamente en el Sistema de Timbrado',
                            [dmInterfase.cdsRSocial.FieldByName('RS_NOMBRE').AsString] ) , 0);}
              cxProgressBarEnvio.Visible := False;
              ModalResult := mrOk;
         end;
    end
    else
        ModalResult := mrCancel
  end;

end;

function TWizardCancelarTimbradoForm.CancelarNomina: boolean;

  procedure LanzarPeticionWS( var oPeticionWS: TPeticionWS; tipoPeticion: eTipoPeticionWS; timbresSoap: TimbresFiscalesSoap; sPeticion: String; sTimbradoURL: String; var broker: TManejadorPeticiones );
  begin
       oPeticionWS.tipoPeticion := tipoPeticion;
       oPeticionWS.cnxSoap := timbresSoap;
       oPeticionWS.sPeticion :=  sPeticion;
       oPeticionWS.sURL := sTimbradoURL;
       oPeticionWS.lProceso := TRUE;
       broker.AgregarPeticionWS( oPeticionWS );
  end;

  procedure EsperarPeticionWS( var oPeticionWS: TPeticionWS; iEspera: integer = 500 );
  var
     iShowBuzz, iCount: integer;
  begin
       while oPeticionWS.lProceso do
       begin
         Inc( iCount );
         iShowBuzz := iCount mod 4;
         DelayNPM( iEspera  );
         if ( iCount mod 4  = 0 ) then
            Application.ProcessMessages;
       end;
  end;

  procedure InicializarProgressBar;
  begin
       cxProgressBarEnvio.Properties.ShowTextStyle := cxtsPercent;
       cxProgressBarEnvio.Properties.Min := 0.00;
       cxProgressBarEnvio.Properties.Max := 100.00;
       cxProgressBarEnvio.Position := 0.00;
       cxProgressBarEnvio.Update;
  end;

  procedure MensajeStatusProgress( sMensaje: String; dProgress: Double  );
  begin
       if not StrVacio( sMensaje ) then
       begin
            cxStatus.Caption := sMensaje;
            Application.ProcessMessages;
       end;
       cxProgressBarEnvio.Position := dProgress;
       cxProgressBarEnvio.Update;
  end;

  procedure CancelacionTimbradoWS( var oPeticionInitWS, oPeticionCancelarWS: TPeticionWS; timbresSoap:TimbresFiscalesSoap; var broker: TManejadorPeticiones; var sPeticion: String; var iTXID, iFacturasCount: Integer; var respuestaTimbramex: TRespuestaStruct );
  var
     oTimbradoPendiente: TProcesoTimbradoPendiente;
  begin
       sPeticion := dmInterfase.GetPeriodoCancelar( cxCheckBoxFiltrar.Checked, 0, true ) ;
       MensajeStatusProgress( 'Formando Petici�n de Cancelado...', 20.00  );
       //Lanzar Peticion para Crear Transaccion ID - Timbrado de nomina Cancelar
       LanzarPeticionWS( oPeticionInitWS, peticionCancelarTimbradoAsincrono, timbresSoap, sPeticion, GetTimbradoURL, broker );
       MensajeStatusProgress( 'Enviando Cancelaci�n de Timbrado al servidor...', 40.00 );
       EsperarPeticionWS( oPeticionInitWS );
       MensajeStatusProgress( '', 100.00 );
       //Obtener la transaccion ( TX_ID ) y el total de timbres a cancelar FacturasCount
       respuestaTimbramex := FTimbramexHelper.GetTimbradoRespuesta( oPeticionInitWS.sRespuesta, TRUE );
       iTXID := StrToInt( respuestaTimbramex.Resultado.TX_ID);
       iFacturasCount := respuestaTimbramex.Resultado.CancelarFacturasConteo;
       //Lanzar Peticion para cancelar timbres
       sPeticion := dmInterfase.GetPeriodoCancelar( cxCheckBoxFiltrar.Checked, iTXID, false);
       //Cancelacion de Timbrado Pendiente
       CargaParametros;
       CargaListaVerificacion;
       oTimbradoPendiente := TProcesoTimbradoPendiente.Create;
       oTimbradoPendiente.ParamsTimbradoNomina := FParamsTimbradoNomina;
       oTimbradoPendiente.SetEstatusCancelacionPendienteEmpleados;
       oTimbradoPendiente.Destroy;
       {$ifdef PRUEBAS_TIMBRADO_PENDIENTE}
       if ZetaDialogo.ZConfirm( self.Caption, '�Desea cancelar proceso timbrado?', 0, mbNo ) then
       begin
            raise Exception.CreateFmt('Invalid name : ''%s''', ['prueba']);
       end;
       {$endif}
       LanzarPeticionWS( oPeticionCancelarWS, peticionCancelarTimbrado, timbresSoap, sPeticion, GetTimbradoURL, broker );
       FCancelandoNominas := True;
  end;

  procedure CancelacionStatusWS( var oPeticionStatusWS: TPeticionWS; var brokerStatus: TManejadorPeticiones; timbresSoap: TimbresFiscalesSoap; var sPeticion: String; var respuestaTimbramex: TRespuestaStruct; iFacturasCount: Integer );
  const
       K_SEGUNDO_ESPERA = 1000;
  var
     iFacturasCanceladas: Integer;
  begin
       LanzarPeticionWS( oPeticionStatusWS, peticionCancelarTimbradoAsincronoStatus, timbresSoap, sPeticion, GetTimbradoURL, brokerStatus );
       EsperarPeticionWS( oPeticionStatusWS, K_SEGUNDO_ESPERA );
       respuestaTimbramex := FTimbramexHelper.GetTimbradoRespuesta( oPeticionStatusWS.sRespuesta, TRUE );
       iFacturasCanceladas := respuestaTimbramex.Resultado.CancelarConteo;
       try
          MensajeStatusProgress( Format( 'Cancelados %d de %d', [ iFacturasCanceladas, iFacturasCount ] ), trunc( ( (iFacturasCanceladas ) / (iFacturasCount ) ) * 100.00 ) );
       except on Error: EDivByZero do
               MensajeStatusProgress('', 100.00 );
       end;
       if ( respuestaTimbramex.Resultado.CancelarStatus = 4 ) then MensajeStatusProgress( Format( 'Cancelados %d de %d', [ iFacturasCount, iFacturasCount ] ), 100.00 );
  end;

//Metodo que se iria a una Helper Class
  function   TimbrarNominaServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No fue posible cancelar el timbrado de la n�mina en el Sistema de Timbrado';
       K_NOPOSIBLE_TIMBRAMEX_ALGUNOS = 'Solo fue posible cancelar %d recibo(s) en el Sistema de Timbrado';
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;
     broker, brokerStatus : TManejadorPeticiones ;
     oPeticionInitWS, oPeticionCancelarWS, oPeticionStatusWS: TPeticionWS;
     iTXID, iFacturasCount: integer;
     wsConfig : TWebServiceConfig;

  begin
     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     oPeticionInitWS := TPeticionWS.Create;
     oPeticionCancelarWS := TPeticionWS.Create;
     oPeticionStatusWS := TPeticionWS.Create;
     broker := TManejadorPeticiones.Create( ILogBitacora( Self ) );
     brokerStatus := TManejadorPeticiones.Create( ILogBitacora( Self ) );
     try
          sServer := '';
          sServer := GetTimbradoServer;
          timbresSoap := nil;
          InicializarProgressBar;
          if ( sServer <> '' ) then
          begin
             cxStatus.Caption := 'Conectando a Servidor...';
             Application.ProcessMessages;
             timbresSoap := Timbres.GetTimbresFiscalesSoap2(False, GetTimbradoURL, wsConfig );
             if ( timbresSoap <> nil ) then
             begin
{*************** Cancelacion de TImbrado de Nomina *****************}
                  CancelacionTimbradoWS( oPeticionInitWS, oPeticionCancelarWS, timbresSoap, broker, sPeticion, iTXID, iFacturasCount, respuestaTimbramex );
                  MensajeStatusProgress( Format( 'Cancelados %d de %d', [ 0, iFacturasCount ]), 0.00 );
                  while True do
                  begin
                       if not ( FCancelandoNominas ) then
                       begin
                            broker.Stop;
                            wsConfig.FHTTPRIO := nil;
                            break;
                       end;
                       CancelacionStatusWS( oPeticionStatusWS, brokerStatus, timbresSoap, sPeticion, respuestaTimbramex, iFacturasCount );
                       if ( respuestaTimbramex.Resultado.CancelarStatus <> 1 ) then break;
                  end;
{********************** FIN *********************}
                  Application.ProcessMessages;
                  if ( FCancelandoNominas ) then
                  begin
                       //EsperarPeticionWS( oPeticionCancelarWS, K_SEGUNDO_ESPERA );
                       sRespuesta := oPeticionCancelarWS.sRespuesta;
                       respuestaTimbramex := FTimbramexHelper.GetTimbradoRespuesta( sRespuesta, TRUE );
                       dmInterfase.HuboRespuestaTimbramex := TRUE;
                       dmInterfase.RespuestaTimbramex := respuestaTimbramex;
                       DatasourceErrores.DataSet := respuestaTimbramex.Resultado.ErroresDS;
                       //cxGroupBoxResultados.Visible := not respuestaTimbramex.Resultado.ErroresDS.IsEmpty;

                       if respuestaTimbramex.Resultado.TX_STATUS = STATUS_TIMBRADO_ERROR_GRAVE then
                       begin
                            dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta ) ;
                            FCancelandoNominas := False;
                       end;

                       if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                       begin
                          cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;

                          if (respuestaTimbramex.Resultado.FacturasDS = nil ) or ( respuestaTimbramex.Resultado.FacturasDS.IsEmpty ) then
                             zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX , 0)
                          else
                              zError( K_ERR_TIMBRAMEX , Format( K_NOPOSIBLE_TIMBRAMEX_ALGUNOS, [respuestaTimbramex.Resultado.FacturasDS.RecordCount ] ) , 0);
                          FCancelandoNominas := False;
                       end
                       else
                       begin
                            cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;
                            REsult := TRUE;
                            FCancelandoNominas := False;
                       end;
                  end
                  else
                      REsult := False;
             end;
          end
          else
          begin
             zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
             dmInterfase.EscribeBitacoraTRESS(Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado Servidor:' + sServer ) ;
             FCancelandoNominas := False;
          end;
      except on Error: Exception do
             begin
                 zError( Self.Caption, StringReplace( K_SIN_CONECTIVIDAD, ':', '', [rfReplaceAll] ) , 0);
                 dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + ' '+Error.Message  +   char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta  );
                 FCancelandoNominas := False;
             end;
      end;
      FreeAndNil( oPeticionInitWS );
      FreeAndNil( oPeticionCancelarWS );
      FreeAndNil( oPeticionStatusWS );
      if ( FCancelandoNominas ) then
      begin
           FreeAndNil( broker );
           FreeAndNil( brokerStatus );
      end;
      Screen.Cursor := oCursor;
  end;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Result := TimbrarNominaServer;
     Screen.Cursor := oCursor;
end;


procedure TWizardCancelarTimbradoForm.FormCreate(Sender: TObject);


    procedure InicializarFiltros;
    begin

     FEmpleadoCodigo := 'NOMINA' + '.CB_CODIGO';
     FParameterList := TZetaParams.Create;
     FDescripciones := TZetaParams.Create;
     ZBasicoSelectGrid_DevEx.ParametrosGrid := FDescripciones;

     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := VACIO; //IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     cxCheckBoxFiltrar.Checked := False;
     HabilitarDeshabilitarBotones;
     dmCatalogos.cdsCondiciones.Conectar;
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;

    end;

begin


     HelpContext:= H32134_CancelarTimbrado;

     InicializarFiltros;

     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
              // sStatusNomina.Caption := //;;ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
               FechaInicial.Caption := FormatDateTime( FormatSettings.LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( FormatSettings.LongDateFormat, Fin );
          end;
     end;

     DatasetPeriodo.DataSet := dmInterfase.cdsPeriodosAfectadosTotal;

     
    if  dmInterfase.cdsPeriodosCancelar.IsEmpty  then
    begin
         dmCliente.RazonSocial := VACIO;
    end
    else
    begin
         dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosCancelar.FieldByName('RS_CODIGO').AsString, []) ;
         dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
    end;

     dataSourceTimbrar.DataSet :=  dmInterfase.cdsPeriodosCancelar;
     dmTablas.cdsEstado.Conectar;
     CT_CODIGO.LookupDataset := dmInterfase.cdsCuentasTimbramex;
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
      //cxGroupBoxResultados.Visible := FALSE;
     dxWizardControlPageTimbrado.Header.Description := 'Al aplicar el proceso se cancelar� el timbrado de acuerdo a los par�metros indicados.' + CR_LF + 'Al iniciar este proceso no podr� ser interrumpido.';
end;

procedure TWizardCancelarTimbradoForm.Button1Click(Sender: TObject);
begin
     //memoPreview.Text :=  dmInterfase.GetRecibos;
end;

procedure TWizardCancelarTimbradoForm.dxWizardControl1PageChanging(Sender: TObject;
  ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
begin
  if ( AnewPage = dxWizardControlPageEmpleados) then
  begin

      if  dmInterfase.cdsPeriodosCancelar.IsEmpty  then
      begin
           dmCliente.RazonSocial := VACIO;
      end
      else
      begin
           dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosCancelar.FieldByName('RS_CODIGO').AsString, []) ;
           dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
      end;

      CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
      AAllow := TRUE;
      FVerificacion := FALSE;
  end;

  if ( AnewPage = dxWizardControlPageCuentaTimbrado ) then
  begin
       if(dmCliente.EsTRESSPruebas) then
       begin
            DxWizardControl1.buttons.Finish.Enabled := False;
       end;
       if cxCheckBoxFiltrar.Checked then
       begin
           if not FVerificacion then
           begin
                CargaListaNominas;
           end;

           if ( dmProcesos.cdsDataSetTimbrados = nil ) or ( dmProcesos.cdsDataSetTimbrados.IsEmpty ) then
           begin
               ZWarning(  Self.Caption, 'No hay empleados timbrados en esa n�mina', 0, mbOK);
               AAllow := FALSE;
           end;
       end;
  end;

  if ( AnewPage = dxWizardControlPageTimbrado ) then
  begin

     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               Advertencia.Caption := 'Al aplicar el proceso se proceder� a cancelar los recibos de los empleados seleccionados correspondientes al periodo'
               +' '+iTipoNomina.Caption+' '+  iNumeroNomina.Caption +' del a�o '+IntToStr(Year)+', contribuyente '+dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString +'.';
          end;
     end;
          {
          with GetDatosPeriodoActivo do
          begin
               IntToStr( Numero );
               IntToStr( Year );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
          end; }

       AAllow := TRUE;
       if StrVacio( CT_CODIGO.Llave ) then
       begin
            zError( Self.Caption, 'No se ha especificado la Cuenta de Timbrado', 0  );
            CT_CODIGO.SetFocus;
            AAllow := FALSE;
       end; 
  end;
end;

procedure TWizardCancelarTimbradoForm.CT_CODIGOValidLookup(Sender: TObject);
begin
      DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;

function TWizardCancelarTimbradoForm.GetRecibosDataSet: boolean;
begin

end;



procedure TWizardCancelarTimbradoForm.gridPeriodosDBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
 		ZGridModeTools.BorrarItemGenericoAll( AValueList );
     		if gridPeriodosDBTableView1.DataController.IsGridMode then
        		ZGridModeTools.FiltroSetValueLista( gridPeriodosDBTableView1, AItemIndex, AValueList );
end;

procedure TWizardCancelarTimbradoForm.Exportar;
 var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Errores de Timbrado',
                             '',
                             stTodos,
                             stNinguno ] );
end;
begin
               if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes_DevEx.ExportarGrid( ZetaCXGrid1DBTableView1 ,
                                                DataSourceErrores.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
               end;
end;

procedure TWizardCancelarTimbradoForm.ZetaSpeedButton1Click(Sender: TObject);
begin
      if ZetaCXGrid1DBTableView1.DataController.DataSetRecordCount > 0 then
      begin
           Exportar;
      end
      else
      begin
           //ZError('Error en '+Self.Caption, 'El Grid de Errores est� Vac�o', 0);
           ZetaDialogo.ZError('Error en '+Self.Caption, '�Se encontr� un Error! '+ CR_LF + 'El grid de respuesta del sistema de timbrado est� vac�o',0)
      end;
end;

procedure TWizardCancelarTimbradoForm.EscribirBitacoraError(
  sMensaje: string);
begin
   //
end;

procedure TWizardCancelarTimbradoForm.EscribirBitacoraLog(
  sMensaje: string);
begin
   //
end;



procedure TWizardCancelarTimbradoForm.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;


procedure TWizardCancelarTimbradoForm.EnabledBotones(
  const eTipo: eTipoRangoActivo);
var
   lEnabled: Boolean;
begin
     if cxCheckBoxFiltrar.Checked then
     begin
         FTipoRango := eTipo;
         lEnabled := ( eTipo = raRango );
         lbInicial.Enabled := lEnabled;
         lbFinal.Enabled := lEnabled;
         EInicial.Enabled := lEnabled;
         EFinal.Enabled := lEnabled;
         bInicial.Enabled := lEnabled;
         bFinal.Enabled := lEnabled;
         lEnabled := ( eTipo = raLista );
         ELista.Enabled := lEnabled;
         bLista.Enabled := lEnabled;

         RBTodos.Enabled := TRUE;
         RBRango.Enabled := TRUE;
         RBLista.Enabled := TRUE;
         sCondicionLBL.Enabled := TRUE;
         ECondicion.Enabled := TRUE;
         sFiltroLBL.Enabled := TRUE;
         sFiltro.Enabled := TRUE;
         BAgregaCampo.Enabled := TRUE;
         Seleccionar.Enabled := TRUE;

     end
     else
     begin

         RBTodos.Enabled := FALSE;
         RBRango.Enabled := FALSE;
         RBLista.Enabled := FALSE;

         lbInicial.Enabled := FALSE;
         lbFinal.Enabled := FALSE;
         EInicial.Enabled := FALSE;
         EFinal.Enabled := FALSE;
         bInicial.Enabled := FALSE;
         bFinal.Enabled := FALSE;
         ELista.Enabled := FALSE;
         bLista.Enabled := FALSE;

         sCondicionLBL.Enabled := FALSE;
         ECondicion.Enabled := FALSE;
         sFiltroLBL.Enabled := FALSE;
         sFiltro.Enabled := FALSE;
         BAgregaCampo.Enabled := FALSE;
         Seleccionar.Enabled := False;
     end;
end;

procedure TWizardCancelarTimbradoForm.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );

          AddString( 'RazonSocial',  GetRazonSocial );
          AddInteger( 'StatusAnterior', ord(estiTimbrado) );
          AddInteger( 'StatusNuevo',  ord(estiTimbrado) ) ;

          AddInteger( 'Year',dmCliente.YearDefault );
          AddInteger( 'Tipo',ord( dmCliente.PeriodoTipo )  );
          AddInteger( 'Numero', dmCliente.PeriodoNumero );

          AddString( 'Orden', 'NOMINA.CB_CODIGO' );
     end;

     with Descripciones do
     begin
          AddString( 'Lista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;


{     statusAnterior := eStatusTimbrado( ParamList.ParamByName( 'StatusAnterior').Asinteger ) ;
     statusNuevo := eStatusTimbrado( ParamList.ParamByName( 'StatusNuevo').Asinteger ) ;
     sRazonSocial := ParamList.ParamByName( 'RazonSocial').AsString;}


     with dmCliente do
     begin
          CargaActivosIMSS( ParameterList );
          CargaActivosPeriodo( ParameterList );
          CargaActivosSistema( ParameterList );
     end;
end;

function TWizardCancelarTimbradoForm.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
           if ( global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) )then
             Result := stringReplace(Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString ), 'COLABORA', 'V_EMP_TIMB' ,[rfReplaceAll, rfIgnoreCase])
           else
               Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
          { Causa problemas porque le quita la '@'; los par�ntesis no son necesarios
          if EsFormula( Result ) then
             Result := Parentesis( Result );
          }
     end
     else
         Result := '';
end;

function TWizardCancelarTimbradoForm.GetFiltro: String;
begin
  Result := Trim( sFiltro.Text );
     { Los Par�ntesis no son necesarios
     if StrLleno( Result ) then
        Result := Parentesis( Result );
     }
end;

function TWizardCancelarTimbradoForm.GetRango: String;
var
   sl : TStringList;
   i : integer;
   sLista : string;
begin

     sl := TStringList.Create;
     sl.CommaText :=  ELista.Text;
     sLista := VACIO;
     for i:=0 to sl.Count -1  do                       
     begin
          if StrLleno( sl[i] ) then
          begin
               if StrVacio( sLista)  then
                  sLista := sl[i]
               else
                  sLista :=  sLista + ','+ sl[i];
          end;
     end;
     sl.Free;

     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FEmpleadoCodigo, Trim( sLista  ) );
     else
         Result := '';
     end;
end;

procedure TWizardCancelarTimbradoForm.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TWizardCancelarTimbradoForm.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TWizardCancelarTimbradoForm.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;


procedure TWizardCancelarTimbradoForm.CargaListaVerificacion;
begin
     dmProcesos.TimbrarNominaGetLista( ParameterList );
//     dmProcesos.cdsDataset.SaveToFile('ListaVerificar.XML', dfXML);
end;

function TWizardCancelarTimbradoForm.Verificar: Boolean;
begin
    FEmpleadoTimbradoGridSelect.FTituloOrden := 'N�mero de Empleado';
    Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataSetTimbrados, TEmpleadoTimbradoGridSelect );
end;

procedure TWizardCancelarTimbradoForm.CargaListaNominas;
var
 oCursor : TCursor;
begin
    oCursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
       CargaParametros;
       CargaListaVerificacion;
    finally
           Screen.Cursor := oCursor;
    end;
end;


procedure TWizardCancelarTimbradoForm.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardCancelarTimbradoForm.BFinalClick(Sender: TObject);
begin
    with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardCancelarTimbradoForm.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TWizardCancelarTimbradoForm.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( EntidadActiva, Text, SelStart, evBase );
     end;
end;

function TWizardCancelarTimbradoForm.EntidadActiva;
begin
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
          result := enEmpleado;
     end
     else
     begin
          result := enEmpTimb;
     end;

end ;

procedure TWizardCancelarTimbradoForm.SeleccionarClick(Sender: TObject);
var
   lCarga: Boolean;
   lOk: Boolean;
begin
     inherited;
     if Verificacion then
     begin
          case ZConfirmaVerificacion.ConfirmVerification of
               mrYes:
               begin
                    lCarga := True;
                    lOk := True;
               end;
               mrNo:
               begin
                    lCarga := False;
                    lOk := True;
               end;
          else
              begin
                   lCarga := False;
                   lOk := False;
              end;
          end;
     end
     else
     begin
          lCarga := True;
          lOk := True;
     end;
     if lOk then
     begin
          if lCarga then
          begin
               CargaListaNominas;
          end;
          SetVerificacion( Verificar );
     end;
end;


function TWizardCancelarTimbradoForm.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
           if ZetaBuscaEmpleado_DevExTimbradoAvanzada.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
           begin
                if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                   Result := sLlave + ',' + sKey
                else
                    Result := sKey;
           end;
     end
     else
     begin
          if ZetaBuscaEmpleado_DevExAvanzada.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
           begin
                if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                   Result := sLlave + ',' + sKey
                else
                    Result := sKey;
           end;

     end;
end;


function TWizardCancelarTimbradoForm.GetRazonSocial: string;
begin
    Result := dmCliente.RazonSocial;
end;

procedure TWizardCancelarTimbradoForm.cxCheckBoxFiltrarClick(
  Sender: TObject);
begin
     HabilitarDeshabilitarBotones;
end;


procedure TWizardCancelarTimbradoForm.DataSourceErroresDataChange(
  Sender: TObject; Field: TField);
begin
     ZetaCXGrid1DBTableView1.ApplyBestFit();
end;

procedure TWizardCancelarTimbradoForm.DataSourceErroresUpdateData(
  Sender: TObject);
begin
     ZetaCXGrid1DBTableView1.ApplyBestFit();
end;

procedure TWizardCancelarTimbradoForm.HabilitarDeshabilitarBotones;
begin
   if RBTodos.Checked then
     EnabledBotones( raTodos );

   if RBRango.Checked then
     EnabledBotones( raRango );

   if RBLista.Checked then
     EnabledBotones( raLista );
end;

procedure TWizardCancelarTimbradoForm.FormShow(Sender: TObject);
begin
     ZetaCXGrid1DBTableView1.ApplyBestFit();
if(dmCliente.EsTRESSPruebas) then
     begin
          DxWizardControl1.buttons.Finish.Enabled := False;
     end;
end;

end.
