unit DInterfase;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    Db, DBClient, ZetaClientDataSet, Variants, DZetaServerProvider, ZetaTipoEntidad,
    ZetaTipoEntidadTools, ZetaCommonLists, ZetaCommonClasses, Recursos_TLB, ZBaseEdicion_DevEx,
    IdCoder, IdCoder3to4, IdCoderMIME, IdBaseComponent,
    {$ifdef DOS_CAPAS}
    DServerConsultas,
    DServerCatalogosTimbrado,                                   
    DServerSistemaTimbrado,
    DServerNominaTimbrado,
    {$else}
    Consultas_TLB,
    TimbradoCatalogos_TLB,
    TimbradoSistema_TLB,
    TimbradoNomina_TLB,
    {$endif}
    OleServer,
    FTimbramexClasses,
    ZetaMsgDlg,
    HTTPApp,
    ZAsciiTools,
    ZetaAsciiFile;

type
  TdmInterfase = class(TDataModule)
    cdsIncapacidades: TZetaClientDataSet;
    cdsResultados: TZetaClientDataSet;
    cdsPeriodosAfectados: TZetaClientDataSet;
    cdsPeriodosTimbrar: TZetaClientDataSet;
    cdsRSocial: TZetaLookupDataSet;
    cdsCuentasTimbramex: TZetaLookupDataSet;
    cdsRecibosTimbrar: TZetaClientDataSet;
    cdsNominaFiltrar: TZetaClientDataSet;
    cdsPeriodosAfectadosTotal: TZetaClientDataSet;
    cdsPeriodosCancelar: TZetaClientDataSet;
    cdsPeriodosRecibos: TZetaClientDataSet;
    cdsTiposSAT: TZetaLookupDataSet;
    cdsRecibosDetalleTimbrar: TZetaClientDataSet;
    cdsSaldoTimbrado: TZetaClientDataSet;
    cdsSaldoTimbradoDetalle: TZetaClientDataSet;
    cdsRecibosIncapacidades: TZetaClientDataSet;
    cdsRecibosTiempoExtra: TZetaClientDataSet;
    cdsEmpleadosTimbrar: TZetaClientDataSet;
    cdsFoliosFiscalesPendientes: TZetaClientDataSet;
	cdsEmpleadoLookUpTimbrado: TZetaLookupDataSet;
    cdsArchivosDescargados: TZetaClientDataSet;
    cdsArchivosDescargadosTIPO: TStringField;
    cdsArchivosDescargadosGRUPO: TStringField;
    cdsArchivosDescargadosARCHIVO: TStringField;
    cdsArchivosDescargadosPERIODO: TIntegerField;
    cdsCambiosConceptos: TZetaClientDataSet;
    cdsNominasXYear: TZetaClientDataSet;
    cdsNominasXYearAProcesar: TZetaClientDataSet;
    cdsErroresGeneral: TZetaClientDataSet;
    cdsRecibosSubContratos: TZetaClientDataSet;
    cdsListasTimbrado: TZetaClientDataSet;
    cdsAscii: TZetaClientDataSet;
    cdsAsciiRenglon: TStringField;
    cdsImpotConfConcepto: TZetaClientDataSet;
    cdsPeriodosXMesYear: TZetaClientDataSet;
    cdsPeriodosXMesYearAProcesar: TZetaClientDataSet;
    cdsEmpleadosConciliarTimbrado: TZetaClientDataSet;
    cdsTimbradoConciliaGetPeriodos: TZetaClientDataSet;
    cdsDetalleConciliacionTimbrado: TZetaClientDataSet;
    cdsAuditoriaConceptosTimbradoXML: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsPeriodosAfectadosAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodosAfectadosAlCrearCampos(Sender: TObject);
    procedure cdsRSocialAfterPost(DataSet: TDataSet);
    procedure cdsRSocialAlAdquirirDatos(Sender: TObject);
    procedure cdsRSocialGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsRSocialBeforePost(DataSet: TDataSet);
    procedure cdsRSocialAlModificar(Sender: TObject);
    procedure cdsRSocialAlEnviarDatos(Sender: TObject);
    procedure cdsCuentasTimbramexAlAdquirirDatos(Sender: TObject);
    procedure cdsCuentasTimbramexAlEnviarDatos(Sender: TObject);
    procedure cdsCuentasTimbramexBeforePost(DataSet: TDataSet);
    procedure cdsCuentasTimbramexAlModificar(Sender: TObject);
    procedure cdsCuentasTimbramexAfterDelete(DataSet: TDataSet);
    procedure cdsPeriodosAfectadosTotalAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodosTimbrarAlAdquirirDatos(Sender: TObject);
    procedure cdsRecibosTimbrarAlCrearCampos(Sender: TObject);

    procedure cdsSaldoTimbradoAlCrearCampos(Sender: TObject);
    procedure cdsSaldoTimbradoDetalleAlCrearCampos(Sender: TObject);
    procedure cdsCuentasTimbramexBeforeDelete(DataSet: TDataSet);
    procedure cdsCuentasTimbramexAlBorrar(Sender: TObject);
    procedure TimbrarNominaGetLista(Parametros: TZetaParams);
    procedure cdsTiposSATBeforePost(DataSet: TDataSet);
    procedure cdsEmpleadoLookUpTimbradoLookupKey(
      Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter,
      sKey: String; var sDescription: String);
    procedure cdsEmpleadoLookUpTimbradoLookupSearch(
      Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
      var sKey, sDescription: String);
    procedure cdsNominasXYearAlCrearCampos(Sender: TObject);
    procedure cdsCuentasTimbramexGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsListasTimbradoAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodosAfectadosCalcFields(DataSet: TDataSet);
  //  Procedure SetParametrosEmpleadosTimbrar;
    procedure cdsPeriodosXMesYearAProcesarAlCrearCampos(Sender: TObject);
    procedure cdsPeriodosXMesYearAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodosXMesYearAlCrearCampos(Sender: TObject);
    procedure cdsTimbradoConciliaGetPeriodosAlAdquirirDatos(Sender: TObject);
    procedure cdsTimbradoConciliaGetPeriodosAlCrearCampos(Sender: TObject);
    procedure PERIODO_NOMBREGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    function GetDatosUpdateConciliacionTimbrado( oParams: TZetaParams ): OleVariant;
  private
    { Private declarations }
    FUltimoConcepto: Integer;
    FHuboCambios, FProcesandoThread, FHuboErrores, FHuboErrorEnviar: Boolean;
    FEmpleado, FLinea: Integer;
    FArchivo: string;
    FDatos, FRenglon: TStringList;
    FTodosEmpleados : boolean;
    //esto tendria que cambiarse a un array de peticiones ( como los de Process List )
    FHuboRespuestaTimbramex : boolean;
    FRespuestaTimbramex : TRespuestaStruct;
    FoliosParametros : TzetaParams;
    oZetaProvider: TdmZetaServerProvider;
    FFormatoFecha: eFormatoImpFecha;
    FAnio, FTipoNomina, FPeriodoNomina: integer;
    FManual: boolean;


    FParametros : TZetaParams;
    FParametrosRazonSocial : TZetaParams;
    {$ifndef DOS_CAPAS}
    FServerConsultas: IdmServerConsultasDisp;
    FServerCatalogosTimbrado: IdmServerCatalogosTimbradoDisp;
    FServerSistemaTimbrado: IdmServerSistemaTimbradoDisp;
    FServerNominaTimbrado: IdmServerNominaTimbradoDisp;
    {$else}
    FServerCatalogosTimbrado: TdmServerCatalogosTimbrado;
    FServerSistemaTimbrado: TdmServerSistemaTimbrado;
    FServerNominaTimbrado: TdmServerNominaTimbrado;
    {$endif}
    FBitacora: TAsciiLog;
    function GetHeaderTexto(const iEmpleado: Integer; const iLinea: Integer): string;
    function GetSQLScript( iConsulta: Integer ): String;

    procedure ReportaProcessDetail(const iFolio: Integer);
    procedure SetNuevosEventos;
    procedure ProcesaArchivo;
    procedure ValidaEnviar( DataSet: TZetaClientDataSet );
    procedure RenombraArchivo( sArchivo: string );
    procedure cdsEmpleadoBeforePost(DataSet: TDataSet);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsHisPermisoBeforePost(DataSet: TDataSet);

    procedure CB_SEGSOCValidate(Sender: TField);
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure ProcesaResultados;
    procedure cdsBeforePost( DataSet: TDataSet; const sCampo: String );


    {$ifndef DOS_CAPAS}
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerCatalogosTimbrado: IdmServerCatalogosTimbradoDisp;
    function GetServerSistemaTimbrado: IdmServerSistemaTimbradoDisp;
    function GetServerNominaTimbrado: IdmServerNominaTimbradoDisp;
    {$else}
    function GetServerConsultas: TdmServerConsultas;
    {$endif}
    procedure ExportarConfiguracionConcepto( Sender: TAsciiExport; DataSet: TDataset );
    function AgregaMensaje( const sMensaje, sValor: String ): String;
    function CargaImportacionConcepto( const sRuta: String ):Olevariant;
    function GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
    function ImportarConfiguracionConceptosGetASCII( ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant;
    procedure ObtenerInfoConceptoConfiguracion;
    function ValidarConfiguracionConcepto: Boolean;
  protected
    { Protected declarations }

  public
    { Public declarations }
    Function GetFoliosFiscalesPendientes(oParametros:TzetaParams)  : boolean;
    Function GetFacturaUUID( const iNoFactura : integer ;  sCT_CODIGO : string ): String;
    function ConstruyeListaEmpleados( oDataset: TDataset; const sFiltro : string ): string;
    procedure Procesa;
    procedure GetEmpleadosBuscados_DevExTimbrado (const sPista: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
    procedure GetEmpleadosBuscados_DevExTimbradoAvanzada( const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet );
    function GetEmpleadoTransferidoParaActivo(const iEmpleado: TNumEmp; const oEmpresa: OleVariant):Boolean;
    function GetEmpleadoTransferidoSiguiente ( const oEmpresa: OleVariant; const iEmpleado: TNumEmp; CatalogosNavegacion : string; var Datos : OleVariant): Boolean;
    function GetEmpleadoTransferidoAnterior ( const oEmpresa: OleVariant; const iEmpleado: TNumEmp; CatalogosNavegacion : string; var Datos : OleVariant): Boolean;
    function GetValidacionEmpleadoTransferido ( const oEmpresa: OleVariant; const iEmpleado: TNumEmp): Boolean;
    function  ValidadorNavegacionEmpTransferidos( const oDatos: OleVariant ): Boolean;
    function GetPeriodoEspecialTimbra(const oEmpresa: OleVariant; const iYear, iTipo, iNumero: Integer; var Datos: OleVariant): Boolean;
    function GetPeriodoAnteriorEspecialTimbra(const oEmpresa: OleVariant; const iYear, iTipo, iNumero: Integer; var Datos: OleVariant): Boolean;
    function GetPeriodoSiguienteEspecialTimbra(const oEmpresa: OleVariant; const iYear, iTipo, iNumero: Integer; var Datos: OleVariant): Boolean;

    function SetCuentaTimbradoFromRS : boolean;

    function GetRecibos : string;
    function GetMensajeAutorizacion: string;
    function GetCambiosConcepto( dTimeStamp : TDateTime;  var lEnviar : boolean): string;
    function GetNominasPorYear( sYear, sEmpleado : string): string;
    function GetPeriodosConciliacionPorMesYear( iMes, iYear: integer): string;//US Conciliacion
    function GetRegistrosTerminales (bActualizar : Boolean; dFechaUltimoRegistro : TDateTime; var sError: string;  var lEnviar : boolean) : string;
    function GetRegistrosAnalitica (bActualizar : Boolean; dFechaUltimoRegistro : TDateTime; var sError: string;  var lEnviar : boolean) : string;
    function GetRegistrosTressFiles (bActualizar : Boolean; dFechaUltimoRegistro : TDateTime; var sError: string;  var lEnviar : boolean) : string;
    function GetLastDateBackup (bActualizar : Boolean; dFechaUltimoRegistro : TDateTime; var sError: string;  var lEnviar : boolean) : string;

    function GetRecibosList(Parametros :TZetaParams; ParametrosPeriodicidad : TZetaParams; lIncluirDetalle : boolean = FALSE; sDescripcion : string  = ''; lIncluirIncapacidades : boolean= FALSE; lIncluirTiempoExtra : boolean = FALSE; lIncluirSubContratos : boolean = FALSE; FAjustaPago : boolean = False   ): TStringList;
    function GetPeriodoCancelar( lFiltrarEmpleados : boolean = FALSE; iTXID: integer = 0; lTXINIT: boolean = FALSE )  : string;
    Function UpdateFoliosPendientes(oParametros:TzetaParams):boolean;

    function GetPeriodoRecibos( lUsoPaquetes: boolean; var slPaquetesGroup : TStringList; iTipoPaquetes : integer  ) : TStringList;
    function GetPeriodoRecibosXEmpleado( lUsoPaquetes: boolean; var slPaquetesGroup : TStringList; iTipoPaquetes: integer; cdsPeriodoNominaAProcesar : TZetaClientDataset  ) : TStringList;
    function GetRecibosDataSet( ParametrosEmpleados: TZetaParams; oNotificarStatus : INotificarStatusReporte; lIncluirDetalle : boolean = FALSE; lIncluirIncapacidades: boolean = FALSE; lIncluirTiempoExtra : boolean = FALSE; lIncluirSubContratos : boolean = FALSE)  : boolean;
    function GetRegimenFiscal : String;
    function GetManifiesto( const sEmpresaFirma : string;  sCT_CODIGO : string ; iContribuyenteID : integer ): String;
    function GetContribuyente( const sArchivoKey, sArchivoCer, sClavePrivada , sEmpresaFirma: string;  sRegimenFiscalDescrip   : string = '' ; sRegimenFiscalCodigo   : string = '' ; lActualizar  : boolean = FALSE  ) : String;
    function GetContribuyenteActualiza( sRegimenFiscalDescrip : string; sRegimenFiscalCodigo : string)  : string;
    procedure GrabaContribuyente( sRS_CODIGO,  sCT_CODIGO : string; iContribuyenteID : integer );
    function GetContribuyenteCancelar : string;
    function GetPeticionRegimenFiscal : String;
    function GetPeticionEnvioRecibos: string;
    function ConsultarSaldo( sRS_CODIGO : string; dFecIni, dFecFin : TDatetime): String;

    function TimbrarNomina( oParamsTimbradoNomina: TZetaParams ) : boolean;
    function MarcarNomina : boolean;
    procedure EscribeBitacoraTRESS( sError, sDetalle  : string );
    Procedure EscribeBitacoraTRESSExito ( sMensaje, sDetalle  : string );
    function AplicarPatchTimbradoComparte:Boolean;
    function AplicarPatchTimbradoDatos:Boolean;
    function CancelarTimbradoNomina( oParamsTimbradoNomina: TZetaParams ) : boolean;
    //Timbrado de nomina pendiente
    function GetRecibosTodosEmpleadosList( Parametros:TZetaParams; ParametrosPeriodicidad : TZetaParams ): WideString;
    function SetEstatusEmpleadoNomina( estatusTimbradoNuevo: eStatusTimbrado; oParamsTimbradoNomina: TZetaParams ): boolean;


    procedure HandleProcessEnd(const iIndice, iValor: Integer);
     {$ifndef DOS_CAPAS}
    property ServerCatalogosTimbrado: IdmServerCatalogosTimbradoDisp read GetServerCatalogosTimbrado;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerSistemaTimbrado: IdmServerSistemaTimbradoDisp read GetServerSistemaTimbrado;
    property ServerNominaTimbrado: IdmServerNominaTimbradoDisp read GetServerNominaTimbrado;
    {$else}
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
    property ServerSistemaTimbrado: TdmServerSistemaTimbrado read FServerSistemaTimbrado;
    property ServerCatalogosTimbrado: TdmServerCatalogosTimbrado read FServerCatalogosTimbrado;
    property ServerNominaTimbrado: TdmServerNominaTimbrado read FServerNominaTimbrado;
    {$EndIf}
    Property TodosEmpleados: boolean read FTodosEmpleados write FTodosEmpleados ;
    property FormatoFecha : eFormatoImpFecha read FFormatoFecha write FFormatoFecha;
    property Archivo : string read FArchivo write FArchivo;


    property Anio: integer read FAnio write FAnio;
    property TipoNomina: integer read FTipoNomina write FTipoNomina;
    property PeriodoNomina: integer read FPeriodoNomina write FPeriodoNomina;
    property Manual: boolean read FManual write FManual;
    property RespuestaTimbramex : TRespuestaStruct read FRespuestaTimbramex   write  FRespuestaTimbramex;
    property HuboRespuestaTimbramex : Boolean read FHuboRespuestaTimbramex write FHuboRespuestaTimbramex;
    property Bitacora :TAsciiLog read FBitacora write FBitacora;
    property ParametrosRazonSocial : TZetaParams read FParametrosRazonSocial write FParametrosRazonSocial;

    procedure RefrescaPeriodoActivosShell(lCambioPeriodoDesdeGrid:Boolean);
    procedure CambioNumeroPeriodoDesdeGridShell(iNumeroPeriodo : integer);

    procedure _P_TimbrarNominaShell;
    procedure _P_CancelarTimbradoNominaShell;
    procedure _P_GetTimbradosShell;
    procedure _P_GetTimbradosXEmpleadoShell;
    procedure _P_EnviarTimbradosShell;

    function GetCountContribuyentes : integer;
    function GetCountTimbradasPorContribuyente : integer;
    {*** Exportacion de Conceptos Configuracion ***}
    function CrearAsciiConcepto (cdsDataSet: TZetaClientDataSet;const sArchivoConcepto:string):String;
    function ArchivoInvalido( const sRuta: String ):Boolean;
    function IsOpen(const fName: string):Boolean;
    function ImportarConfiguracionConceptos( var sMensaje: String ): Boolean;
    procedure AgregarEditarConcepto;
    procedure LogInit( sPath,sProceso:string );
    procedure Log( sTexto:string );
    procedure LogEnd;
    //US Conciliacion TImbrado
    function GetMotorPatchAvance(Parametros: OleVariant): OleVariant;
    function GetCredencialesConciliacionTimbrado: String;
    function GetPeticionTimbradoConciliacionNube(fParams: TZetaParams): String;
    procedure IniciarConciliacionNominaTIMBRES(Parametros: OleVariant);
    procedure ObtenerXMLEmpleadosAConciliarTimbrado( oEmpresa: OleVariant; Parametros: OleVariant; var XML: WideString; var Error: WideString );
    function IniciarConciliacionTimbradoPeriodos(Parametros: TZetaParams): Boolean;
    function AplicarConciliacionTimbradoPeriodos(Parametros: TZetaParams): Boolean;
    procedure GetDetalleConciliacionPeriodo( Parametros: TZetaParams ); //const iPE_YEAR: Integer; const iPE_TIPO: Integer; const iPE_NUMERO: Integer );
    function GetPeticionAuditoriaConceptoNube( sXMLAuditoria: String; var sError: String ): String;
    //Bitacora de Nomina
    function GetContribuyenteParametros: string;
    function MensajeRegistroContribuyenteExito: String;
    procedure EscribeBitacoraTRESSRazonSocial;
    function ObtieneInformacionConceptosTimbrado( var sXML: string; var sError: String  ): Boolean;
  end;

var
  dmInterfase: TdmInterfase;

const
     // Consultas SQL
     K_CONSULTA_PERIODOS        = 1;
     K_CONSULTA_FECHA_PAGO      = 2;
     K_GET_FOLIO_PARIENTES      = 3;
     K_GET_MAX_ID               = 4;
     K_GET_RESULTADOS           = 5;
     K_GET_MAX_ID_MANUAL        = 6;

     K_MAX_CAMPOS_PERIODO = 9;

     { Constantes para Catalogos }
     K_DEFAULT_SEIS = '999999';
     K_DEFAULT_UNO  = '9';
     K_EMPTY        = '"EMPTY"';
     K_VALIDA_INCAPACIADES = ' PE_VALIDAFECHASINC %d,''%s'',''%s'' ';
     K_VALIDA_NUMERO_INCAPACIDAD = 'select count(IN_NUMERO) NUM_INC from INCAPACI where IN_NUMERO = ''%s'' ';
     K_PREFIJO_CFDI = '1.2 ';
     K_REPORTE_TIMBRADO = K_PREFIJO_CFDI + 'Timbrado de n�mina';
     K_REPORTE_TIMBRADO_DETALLE = K_PREFIJO_CFDI + 'Timbrado detalle concepto';
     K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES = K_PREFIJO_CFDI + 'Timbrado incapacidades';
     K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA = K_PREFIJO_CFDI + 'Timbrado tiempo extra';
     K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS = K_PREFIJO_CFDI + 'Timbrado subcontratos';

     K_RECOLECCION_INFORMACION = '1. Recolecci�n de informaci�n en Sistema TRESS.';
     K_ENVIO_CONFIGURACION = '2. Env�o de configuraci�n de conceptos al administrador de timbrado.';
     K_DESCARGA_AUDITORIA = '3. Descarga de auditoria.';

     K_STATUS_REPORTE_IDLE                    = 0;
     K_STATUS_REPORTE_OK                      = 1;
     K_STATUS_REPORTE_ERROR                   = 2;
     K_STATUS_REPORTE_HOLD                    = 3;
     K_STATUS_REPORTE_EXECUTING               = 4;
     K_RELLENO                                = 10;


implementation

uses dProcesos,
     dCliente,
     dTablas,
     dCatalogos,
     dConsultas,
     dGlobal,
     dSistema,
     DXMLTools,
     DReportesXML,
     ZGlobalTress,
     FTressShell,
     ZetaDialogo,
     ZBaseThreads,
     ZetaCommonTools,
     dSuperAscii,
     StrUtils,
     ZReconcile,
     FAutoClasses,
     ZAccesosMgr,
     ZAccesosTress,
     FEditCuentaTimbramex, FTimbramexHelper, DBasicoCliente,
     ZetaFilesTools,
     FWizardFoliosFiscalesPendientes,
     ZetaBuscaEmpleado_DevExTimbrado;

{$R *.DFM}

{ TdmInterfase }
{$ifndef DOS_CAPAS}
function TdmInterfase.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result:= IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;


function TdmInterfase.GetServerCatalogosTimbrado: IdmServerCatalogosTimbradoDisp;
begin
     Result:= IdmServerCatalogosTimbradoDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogosTimbrado, FServerCatalogosTimbrado ) );
end;

function TdmInterfase.GetServerSistemaTimbrado: IdmServerSistemaTimbradoDisp;
begin
     Result:= IdmServerSistemaTimbradoDisp( dmCliente.CreaServidor( CLASS_dmServerSistemaTimbrado, FServerSistemaTimbrado ) );
end;

function TdmInterfase.GetServerNominaTimbrado: IdmServerNominaTimbradoDisp;
begin
     Result:= IdmServerNominaTimbradoDisp( dmCliente.CreaServidor( CLASS_dmServerNominaTimbrado, FServerNominaTimbrado ) );
end;
{$else}

function TdmInterfase.GetServerConsultas: TdmServerConsultas;
begin
     Result := DCliente.dmCliente.ServerConsultas; 
end;

{$endif}

procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     FArchivo      := VACIO;
     Fparametros := TZetaParams.Create;
     FParametrosRazonSocial := TZetaParams.Create;
     SetNuevosEventos;

     dmReportesXML := TdmReportesXML.Create(Self);

     {$ifdef DOS_CAPAS}
     FServerSistemaTimbrado := TdmServerSistemaTimbrado.Create( Self );
     FServerNominaTimbrado := TdmServerNominaTimbrado.Create( Self );
     FServerCatalogosTimbrado := TdmServerCatalogosTimbrado.Create( Self );
     {$endif}
     FBitacora := TAsciiLog.Create;
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil ( FParametros );
     FreeAndNil ( FParametrosRazonSocial );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerSistemaTimbrado );
     FreeAndNil( FServerNominaTimbrado );
     FreeAndNil( FServerCatalogosTimbrado );
     {$endif}

     {$ifndef DOS_CAPAS}
          FreeAndNil( dmReportesXML );
     {$endif}
     FreeAndNil( FBitacora );
end;

{ Manejo de Fin de Proceso - Bit�cora }
procedure TdmInterfase.HandleProcessEnd( const iIndice, iValor: Integer );
begin
end;

procedure TdmInterfase.ReportaProcessDetail( const iFolio: Integer );
begin
end;

{ Utilerias de Bit�cora }
function TdmInterfase.GetHeaderTexto( const iEmpleado: Integer; const iLinea: Integer ): string;
const
     K_HEADER_EMPLEADO = '%sEmpleado #%d: ';
var
   sFechaHora: String;
begin
     Result := VACIO;
     sFechaHora := '(' + FechaCorta( date ) + '-' + FormatDateTime( 'hh:mm', Now ) + '): ';
     if( iLinea > 0 )then
         sFechaHora := sFechaHora + Format( 'L�nea #%d, ', [ iLinea ] );

     if ( iEmpleado > 0 ) then
        Result := Format( K_HEADER_EMPLEADO, [ sFechaHora, iEmpleado ] )
     else
         Result := sFechaHora;
end;


procedure TdmInterfase.SetNuevosEventos;
begin
end;

procedure TdmInterfase.cdsHisPermisoBeforePost(DataSet: TDataSet);
begin
end;

procedure TdmInterfase.cdsListasTimbradoAlAdquirirDatos(Sender: TObject);
const
     K_QUERY_LISTAS = 'select ' +
     ' TimListas.TL_IDLISTA as ID, TimListas.TL_VALOR as Valor, TimListas.TL_DESCRIP AS Descripcion'+
     ' from TimListas where TL_IDLISTA = 600  order by TimListas.TL_VALOR';

begin
        with dmConsultas do
        begin
             SQLText := K_QUERY_LISTAS;

             try
                cdsQueryGeneral.Refrescar;
                cdsListasTimbrado.Data := cdsQueryGeneral.Data;
             except
                   on Error : Exception do
                   begin

                        DatabaseError( Format( 'Error al consultar listas de timbrado: %s', [ Error.Message ] ) ) ;
                   end;
             end;
        end;
end;


procedure TdmInterfase.cdsNominasXYearAlCrearCampos(Sender: TObject);
begin
    with TZetaClientDataSet( Sender )  do
    begin
         MaskFecha( 'Pago');
         MaskFecha( 'Inicial');
         MaskFecha( 'Final');
         ListaFija( 'PE_TIPO', lfTipoPeriodo );
         //MaskBool( 'OBTENER' );
    end;
end;

procedure TdmInterfase.cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
end;

procedure TdmInterfase.cdsEmpleadoBeforePost(DataSet: TDataSet);
begin
     dmCliente.cdsEmpleadoBeforePost( DataSet );
end;

procedure TdmInterfase.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     dmCliente.cdsEmpleadoAlCrearCampos( Sender );
     dmCliente.cdsEmpleado.FieldByName( 'CB_SEGSOC' ).onValidate := self.CB_SEGSOCValidate;
end;

procedure TdmInterfase.CB_SEGSOCValidate(Sender: TField);
var
   sMensaje: String;
begin

end;

procedure TdmInterfase.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
end;

{ Renombra el archivo indicado con un formato de [nombre]_[fecha]_[Hora].bak }
procedure TdmInterfase.RefrescaPeriodoActivosShell(lCambioPeriodoDesdeGrid:Boolean);
begin
     TressShell.RefrescaPeriodoActivos(lCambioPeriodoDesdeGrid);
end;

procedure TdmInterfase.RenombraArchivo( sArchivo: string );
begin
end;

function TdmInterfase.ValidadorNavegacionEmpTransferidos(const oDatos: OleVariant ): Boolean;
var
   cdsValidador: TZetaClientDataSet;
   iEmpleados : integer;
begin
     try
          Result := False;
          cdsValidador := TZetaClientDataSet.Create(nil);
          cdsValidador.Data := oDatos;
          iEmpleados := cdsValidador.RecordCount;
          if iEmpleados > 0 then
          begin
                if cdsValidador.FieldByName('CB_CODIGO').AsInteger <> 0 then
                begin
                     Result := True;
                end
                else
                begin
                     Result := False;
                end;
          end;
     Finally
         FreeAndNil(cdsValidador);
     end;
end;

procedure TdmInterfase.ValidaEnviar(DataSet: TZetaClientDataSet);
begin
end;




procedure TdmInterfase._P_CancelarTimbradoNominaShell;
begin
     TressShell._P_CancelarTimbradoNominaE;
end;

procedure TdmInterfase._P_EnviarTimbradosShell;
begin
     TressShell._P_EnviarTimbradosE;
end;

procedure TdmInterfase._P_GetTimbradosShell;
begin
     TressShell._P_GetTimbradosE;
end;

procedure TdmInterfase._P_GetTimbradosXEmpleadoShell;
begin
     TressShell._P_GetTimbradosXEmpleadoE;
end;

procedure TdmInterfase._P_TimbrarNominaShell;
begin
     TressShell._P_TimbrarNominaE;
end;

// Regresa el texto de la consulta indicada
function TdmInterfase.GetSQLScript(iConsulta: Integer): String;
begin
     case iConsulta of
          K_CONSULTA_PERIODOS: Result := 'select PE_YEAR, PE_NUMERO from PERIODO where '+
                                         '''%0:s'' between PE_FEC_INI and PE_FEC_FIN and PE_TIPO = %1:d and PE_NUMERO < %2:d order by PE_NUMERO';
          K_CONSULTA_FECHA_PAGO: Result := 'select PE_YEAR, PE_NUMERO, PE_STATUS from PERIODO where ( PE_FEC_PAG = ''%s'' ) and ( PE_TIPO = %d ) and ( PE_NUMERO < %d )';
          K_GET_FOLIO_PARIENTES: Result := 'select coalesce( MAX(PA_FOLIO), 0 ) as FOLIO from PARIENTE where ( CB_CODIGO = %d ) and ( PA_RELACIO = %d )';
          K_GET_MAX_ID: Result := 'select MAX(ID) MAXIMO from ENCABEZADO';
          K_GET_RESULTADOS: Result := 'select * from DETALLE where ( ID = %d )';
          K_GET_MAX_ID_MANUAL: Result := 'select MAX(ID) MAXIMO from ENCABEZADO where ( PE_YEAR = %d ) and ( PE_TIPO = %d ) and ( PE_NUMERO = %d )';
     else
          Result := VACIO;
     end;
end;



{ Proceso principal para procesar el archivo }
procedure TdmInterfase.Procesa;
begin
end;

{ Procesa el archivo ingresado }
procedure TdmInterfase.ProcesaArchivo;
begin
end;


procedure TdmInterfase.ProcesaResultados;
begin
end;

function TdmInterfase.GetRecibos: string;
const
     K_TITULO_ERROR = 'Exportaci�n de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
Var
    iReporte, iReportePeriodos : integer;

     XMLTools: DXMLTools.TdmXMLTools;
     sNombre : string;
     sXML, sCONT_PASS, sUsuarioNombre : string;
     iCONT_ID  : integer;

begin
     { Variables locales del procesamiento }
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;

     dmCliente.cdsTPeriodos.Refrescar;
     cdsRecibosTimbrar.First;
     Result := '';
     XMLTools := TdmXMLTools.Create( nil );


       try
                 Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
                 Result := Result + '<PROCESS>' +char(10)+char(13);
                 Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                 Result := Result +         '<DATA>'  +char(10)+char(13);
                 {$ifdef PROFILE}WSProfile('GetRecibos', Result); {$endif}
                 Result := Result + XMLTools.RowToXMLNodeStr( cdsPeriodosTimbrar, 'PERIODOS', 'PERIODO' , K_MAX_CAMPOS_PERIODO );
                 {$ifdef PROFILE}WSProfile('GetRecibos', Result); {$endif}
                 sXML := GeneraXMLFromDataset( cdsRecibosTimbrar, 'NOMINAS', 'NOMINA');
                 {$ifdef PROFILE}WSProfile('GetRecibos', Result); {$endif}
                 sXML := stringReplace( sXML, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
                 sXML := stringReplace( sXML, '<?xml version="1.0" standalone="yes"?>', '', []);
                {$ifdef PROFILE}WSProfile('GetRecibos', Result); {$endif}
                 Result := Result + sXML;
                 Result := Result + '</PERIODO>'+char(10)+char(13);
                 Result := Result + '</DATA>'  +char(10)+char(13);
                 Result := Result + '</PROCESS>'  +char(10)+char(13);
                 {$ifdef PROFILE}WSProfile('GetRecibos', Result); {$endif}


        except on Error: Exception do

               zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
        end;

     FreeAndNil(XMLTools);
     Application.ProcessMessages;

end;

function TdmInterfase.GetMensajeAutorizacion: string;
begin
     try
        Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
        Result := Result + '<PROCESS>' +char(10)+char(13);
        Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
        Result := Result + '</PROCESS>'  +char(10)+char(13);
     except on Error: Exception do
     end;
end;


function TdmInterfase.GetCambiosConcepto( dTimeStamp : TDateTime;  var lEnviar : boolean): string;
const
K_QUERY_BITACORA_CONCEPTOS = 'select   CM_NOMBRE = %s, BI.US_CODIGO as UsuarioNumero, US.US_NOMBRE as UsuarioNombre,  BI_FECHA  as BitFecha, BI_HORA as BitHora,'+
  ' BI_CLASE  as BitClase, BI_TEXTO as BitDescripcion, REPLACE( REPLACE( left( cast ( BI_DATA as varchar(max) ), 1024), ''''+char(10), '''' ) , ''''+char(13), '' '')  as BitDetalle'+
  ' from BITACORA BI  with (INDEX(XIE2BITACORA))'+
  ' join V_USUARIO US on US.US_CODIGO = BI.US_CODIGO where BI_FECHA >= %s and BI_FECHA <= %s'+
  ' and BI_CLASE = 19 and BI_FEC_MOV > %s and BI_FEC_MOV <= %s';


Var
     dFechaIni, dFechaFin, dFechaFinTime, dFechaIniTime: TDateTime;
     sXML, sFechaIniTime, sFechaFinTime: string;
     iCuantos : integer;

     function GetDatosCambiosConceptos : boolean;
     begin
          with dmConsultas do
          begin
            dFechaIni := dTimeStamp;
            dFechaIniTime := dTimeStamp;

            dFechaFin := Now;
            dFechaFinTime := dFechaFin;
            sFechaIniTime := FormatDateTime( 'yyyy-mm-dd hh:nn:ss', dFechaIniTime );
            sFechaFinTime := FormatDateTime( 'yyyy-mm-dd hh:nn:ss', dFechaFinTime );

            SQLText := Format( K_QUERY_BITACORA_CONCEPTOS, [ EntreComillas( dmCliente.Compania), EntreComillas( FechaAsStr(dFechaIni) ), EntreComillas( FechaAsStr( dFechaFin)),
                    EntreComillas(sFechaIniTime ), EntreComillas( sFechaFinTime )   ] );

             try
                cdsQueryGeneral.Refrescar;
                cdsCambiosConceptos.Data := cdsQueryGeneral.Data;
                result := TRUE;
              except
                   on Error : Exception do
                   begin
                       // DatabaseError( Format( 'Error al consultar conceptos modificados: %s', [ Error.Message ] ) );
                        Result := False;
                   end;
             end;

          end;
     end;

begin
     { Variables locales del procesamiento }
     lEnviar := FALSE;
     Result :=  VACIO;

     if  GetDatosCambiosConceptos then
     begin
       iCuantos := cdsCambiosConceptos.RecordCount;
       If iCuantos > 0 then
       begin
          try
              Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
              Result := Result + '<PROCESS>' +char(10)+char(13);
              Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
              Result := Result +         '<DATA>'  +char(10)+char(13);
              sXML := GeneraXMLFromDataset( cdsCambiosConceptos, 'CONCEPTOS', 'CONCEPTO');
              sXML := stringReplace( sXML, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
              sXML := stringReplace( sXML, '<?xml version="1.0" standalone="yes"?>', '', []);
              Result := Result + sXML;
              Result := Result + '</DATA>'  +char(10)+char(13);
              Result := Result + '</PROCESS>'  +char(10)+char(13);
              lEnviar := TRUE;

              except on Error: Exception do
              begin
                     lEnviar := FALSE;
              end;
           end
       end
       else
       begin
            lEnviar := FALSE;
       end;
     end;
     Application.ProcessMessages;
end;


function TdmInterfase.GetRegistrosTerminales (bActualizar : Boolean; dFechaUltimoRegistro : TDateTime; var sError: string;  var lEnviar : boolean) : string;
const
  QUERY = 'select TE_CODIGO, TE_NOMBRE, TE_TEXTO, TE_MAC, TE_APP, TE_ACTIVO, TE_HUELLAS, TE_VERSION, TE_ID  from V_TERMINAL';

var
    sQuery : String;
    sFechaACC, sFechaMod, sFechaUPD : string;
    FQuery : TZetaCursor;
    oZetaProvider: TdmZetaServerProvider;
    tHoy : TDateTime;
    sFechaHoy, sFechaUltimoRegistro : string;
    tablaInfoTerminales : TZetaClientDataSet;
    iCuantos : integer;
    sXmlTerminales : string;
    dFechaIniTime : TDateTime;


     function GetDatosTerminales : boolean;
     begin
          with dmConsultas do
          begin
          tHoy := Now;
          sFechaHoy := FechaToStr(tHoy);
          dFechaIniTime := dFechaUltimoRegistro;
          sFechaUltimoRegistro := FormatDateTime( 'yyyy-mm-dd hh:nn:ss', dFechaIniTime );

            SQLText := QUERY;

             try
                cdsQueryGeneral.Refrescar;
                tablaInfoTerminales.Data := cdsQueryGeneral.Data;
                result := TRUE;
              except
                   on Error : Exception do
                   begin
                        Result := False;
                   end;
             end;

          end;
     end;


begin
  try
     try
        tablaInfoTerminales := TZetaClientDataSet.Create(nil);
        tablaInfoTerminales.FieldDefs.Add('TE_CODIGO', ftInteger, 0, true);
        tablaInfoTerminales.FieldDefs.Add('TE_NOMBRE', ftString, 30, false);
        tablaInfoTerminales.FieldDefs.Add('TE_NUMERO', ftString, 30, false); //FLOAT - numeric 15,2
        tablaInfoTerminales.FieldDefs.Add('TE_TEXTO', ftString, 40, false);
        tablaInfoTerminales.FieldDefs.Add('TE_MAC', ftString, 50, false);
        tablaInfoTerminales.FieldDefs.Add('TE_APP', ftInteger, 0, false);
        tablaInfoTerminales.FieldDefs.Add('TE_BEAT', ftDate, 0, false);
        tablaInfoTerminales.FieldDefs.Add('TE_ZONA', ftString, 40, false);
        tablaInfoTerminales.FieldDefs.Add('TE_ACTIVO', ftString, 2, false);
        tablaInfoTerminales.FieldDefs.Add('TE_XML', ftString, 255, false); //XML - text
        tablaInfoTerminales.FieldDefs.Add('TE_LOGO', ftString, 255, false);  //text
        tablaInfoTerminales.FieldDefs.Add('TE_FONDO', ftString, 255, false); //text
        tablaInfoTerminales.FieldDefs.Add('TE_ZONA_ES', ftString, 40, false);
        tablaInfoTerminales.FieldDefs.Add('TE_VERSION', ftString, 30, false);
        tablaInfoTerminales.FieldDefs.Add('TE_IP', ftString, 30, false);
        tablaInfoTerminales.FieldDefs.Add('TE_HUELLAS', ftInteger, 0, false);
        tablaInfoTerminales.FieldDefs.Add('TE_ID', ftInteger, 0, false);
        tablaInfoTerminales.CreateDataSet;
        tablaInfoTerminales.Open;


         lEnviar := FALSE;
         Result :=  VACIO;
         if  GetDatosTerminales then
         begin
            iCuantos := tablaInfoTerminales.RecordCount;
            If iCuantos > 0 then
            begin
                 try
                     Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
                     Result := Result + '<PROCESS>' +char(10)+char(13);
                     Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                     Result := Result +         '<DATA>'  +char(10)+char(13);
                     sXmlTerminales := GeneraXMLFromDataset(tablaInfoTerminales , 'TERMINALES', 'TERMINAL');
                     sXmlTerminales := stringReplace( sXmlTerminales, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
                     sXmlTerminales := stringReplace( sXmlTerminales, '<?xml version="1.0" standalone="yes"?>', '', []);
                     Result := Result + sXmlTerminales;
                     Result := Result + '</DATA>'  +char(10)+char(13);
                     Result := Result + '</PROCESS>'  +char(10)+char(13);
                     lEnviar := TRUE;

                 except
                       on Error: Exception do
                       begin
                            lEnviar := FALSE;
                       end;
                 end
            end
            else
            begin
                 lEnviar := false;
            end;
       end;
    Except
          on E : Exception do
          begin
              sError := E.Message;
              lEnviar := false;
          end
    end
  finally
    FreeAndNil(tablaInfoTerminales);
  end;
end;


function TdmInterfase.GetRegistrosAnalitica (bActualizar : Boolean; dFechaUltimoRegistro : TDateTime; var sError: string;  var lEnviar : boolean) : string;
const
  QUERY = 'select AnaliticaID, AnaliticaModulo, AnaliticaHelpContext, AnaliticaCodigo, AnaliticaDescripcion, AnaliticaBool, AnaliticaNumero, AnaliticaFecha, AnaliticaFechaUpd  from T_Analitica';

var
    sQuery : String;
    sFechaACC, sFechaMod, sFechaUPD : string;
    FQuery : TZetaCursor;
    oZetaProvider: TdmZetaServerProvider;
    tHoy : TDateTime;
    sFechaHoy, sFechaUltimoRegistro : string;
    tablaInfoAnalitica : TZetaClientDataSet;
    iCuantos : integer;
    sXmlAnalitica : string;
    dFechaIniTime : TDateTime;


     function GetDatosAnalitica : boolean;
     begin
          with dmConsultas do
          begin
          tHoy := Now;
          sFechaHoy := FechaToStr(tHoy);
          dFechaIniTime := dFechaUltimoRegistro;
          sFechaUltimoRegistro := FormatDateTime( 'yyyy-mm-dd hh:nn:ss', dFechaIniTime );

            SQLText := QUERY;

             try
                cdsQueryGeneral.Refrescar;
                tablaInfoAnalitica.Data := cdsQueryGeneral.Data;
                result := TRUE;
              except
                   on Error : Exception do
                   begin
                        Result := False;
                   end;
             end;

          end;
     end;


begin
  try
     try
        tablaInfoAnalitica := TZetaClientDataSet.Create(nil);
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaID', ftInteger, 0, true);
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaModulo', ftString, 30, false);
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaHelpContext', ftInteger, 0, true);
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaCodigo', ftString, 30, false);
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaDescripcion', ftString, 255, false);
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaBool', ftString, 2, false); //-----------
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaNumero', ftfloat, 0, true);
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaFecha', ftDate, 0, false);
        tablaInfoAnalitica.FieldDefs.Add('AnaliticaFechaUpd', ftDate, 0, false);
        tablaInfoAnalitica.CreateDataSet;
        tablaInfoAnalitica.Open;


         lEnviar := FALSE;
         Result :=  VACIO;
         if  GetDatosAnalitica then
         begin
            iCuantos := tablaInfoAnalitica.RecordCount;
            If iCuantos > 0 then
            begin
                 try
                     Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
                     Result := Result + '<PROCESS>' +char(10)+char(13);
                     Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                     Result := Result +         '<DATA>'  +char(10)+char(13);
                     sXmlAnalitica := GeneraXMLFromDataset(tablaInfoAnalitica , 'AnaliticaTress', 'Item');
                     sXmlAnalitica := stringReplace( sXmlAnalitica, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
                     sXmlAnalitica := stringReplace( sXmlAnalitica, '<?xml version="1.0" standalone="yes"?>', '', []);
                     Result := Result + sXmlAnalitica;
                     Result := Result + '</DATA>'  +char(10)+char(13);
                     Result := Result + '</PROCESS>'  +char(10)+char(13);
                     lEnviar := TRUE;

                 except
                       on Error: Exception do
                       begin
                            lEnviar := FALSE;
                       end;
                 end
            end
            else
            begin
                 lEnviar := false;
            end;
       end;
    Except
          on E : Exception do
          begin
              sError := E.Message;
              lEnviar := false;
          end
    end
  finally
    FreeAndNil(tablaInfoAnalitica);
  end;
end;


function TdmInterfase.GetRegistrosTressFiles (bActualizar : Boolean; dFechaUltimoRegistro : TDateTime; var sError: string;  var lEnviar : boolean) : string;
const
    QUERY =  'Select CM_NOMBRE = %s, TF_PATH as ArchivoPath,  TF_EXT as ArchivoExt, TF_FILE ArchivoNombre, TF_DESCR as ArchivoDescripcion, '+
  'TF_VERSION as ArchivoVersion, TF_BUILD as ArchivoBuild, TF_FEC_MOD as ArchivoModFecha, TF_FEC_ACC ArchivoFecha, '+
  'TF_HOST as ArchivoServidor, TF_FEC_UPD as FechaRegistro from VTRESSFILE where TF_FEC_UPD > %s and TF_FEC_UPD <= %s';
var
    sQuery : String;
    sFechaACC, sFechaMod, sFechaUPD : string;
    FQuery : TZetaCursor;
    oZetaProvider: TdmZetaServerProvider;
    tHoy : TDateTime;
    sFechaHoy, sFechaUltimoRegistro : string;
    tablaInfoTressFiles : TZetaClientDataSet;
    iCuantos : integer;
    sXmlTressFiles : string;
    dFechaIniTime : TDateTime;
     function GetDatosCambiosTressFiles : boolean;
     begin


          with dmConsultas do
          begin
          tHoy := Now;
          sFechaHoy := FechaToStr(tHoy);
          dFechaIniTime := dFechaUltimoRegistro;
          sFechaUltimoRegistro := FormatDateTime( 'yyyy-mm-dd hh:nn:ss', dFechaIniTime );

            SQLText := Format(QUERY , [EntreComillas( dmCliente.Compania), EntreComillas(sFechaUltimoRegistro), EntreComillas(sFechaHoy)]);

             try
                cdsQueryGeneral.Refrescar;
                tablaInfoTressFiles.Data := cdsQueryGeneral.Data;
                result := TRUE;
              except
                   on Error : Exception do
                   begin
                        Result := False;
                   end;
             end;

          end;
     end;


begin
  try
     try
        tablaInfoTressFiles := TZetaClientDataSet.Create(nil);
        tablaInfoTressFiles.FieldDefs.Add('CM_NOMBRE', ftString, 50, false);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoPath', ftString, 600, true);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoExt', ftString, 30, true);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoNombre', ftString, 255, true);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoDescripcion', ftString, 255, false);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoVersion', ftString, 30, false);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoBuild', ftString, 30, false);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoModFecha', ftDate, 0, false);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoLlamadas', ftInteger, 0, false);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoFecha', ftDate, 0, false);
        tablaInfoTressFiles.FieldDefs.Add('ArchivoServidor', ftString, 255, false);
        tablaInfoTressFiles.FieldDefs.Add('FechaRegistro', ftDate, 0, false);
        tablaInfoTressFiles.CreateDataSet;
        tablaInfoTressFiles.Open;


         lEnviar := FALSE;
         Result :=  VACIO;
         if  GetDatosCambiosTressFiles then
         begin
            iCuantos := tablaInfoTressFiles.RecordCount;
            If iCuantos > 0 then
            begin
                 try
                     Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
                     Result := Result + '<PROCESS>' +char(10)+char(13);
                     Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                     Result := Result +         '<DATA>'  +char(10)+char(13);
                     sXmlTressFiles := GeneraXMLFromDataset(tablaInfoTressFiles , 'TRESSFILES', 'TRESSFILE');
                     sXmlTressFiles := stringReplace( sXmlTressFiles, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
                     sXmlTressFiles := stringReplace( sXmlTressFiles, '<?xml version="1.0" standalone="yes"?>', '', []);
                     Result := Result + sXmlTressFiles;
                     Result := Result + '</DATA>'  +char(10)+char(13);
                     Result := Result + '</PROCESS>'  +char(10)+char(13);
                     lEnviar := TRUE;

                 except
                       on Error: Exception do
                       begin
                            lEnviar := FALSE;
                       end;
                 end
            end
            else
            begin
                 lEnviar := false;
            end;
       end;
    Except
          on E : Exception do
          begin
              sError := E.Message;
              lEnviar := false;
          end
    end
  finally
    FreeAndNil(tablaInfoTressFiles);
  end;
end;

function TdmInterfase.GetLastDateBackup (bActualizar : Boolean; dFechaUltimoRegistro : TDateTime; var sError: string;  var lEnviar : boolean) : string;
const
  QUERY =  'Select CM_NOMBRE = %s, sdb.Name AS DatabaseName, MAX(bus.backup_finish_date) LastBackUpTime FROM sys.databases sdb '+
  'LEFT OUTER JOIN msdb.dbo.backupset bus ON bus.database_name = sdb.name where sdb.Name = db_name() '+
  'and bus.backup_finish_date > %s and bus.backup_finish_date <= %s GROUP BY sdb.Name';

var
    sQuery : String;
    sFechaLastBackUp : string;
    FQuery : TZetaCursor;
    oZetaProvider: TdmZetaServerProvider;
    tHoy : TDateTime;
    sFechaHoy, sFechaUltimoRegistro : string;
    cdsLastDataBackupBDEmpresa : TZetaClientDataSet;
    iCuantos : integer;
    sXmlLastBackup : string;
    dFechaIniTime : TDateTime;
    dFechaLastBackUp : TDateTime;


     function GetDatosLastBackup : boolean;
     begin
          with dmConsultas do
          begin
            tHoy := Now;
            sFechaHoy := FormatDateTime( 'yyyy-mm-dd hh:nn:ss', tHoy );
            dFechaIniTime := dFechaUltimoRegistro;
            sFechaUltimoRegistro := FormatDateTime( 'yyyy-mm-dd hh:nn:ss', dFechaIniTime );

            SQLText := Format(QUERY , [ EntreComillas( dmCliente.Compania), EntreComillas(sFechaUltimoRegistro), EntreComillas(sFechaHoy) ]);

             try
                cdsQueryGeneral.Refrescar;
                cdsLastDataBackupBDEmpresa.Data := cdsQueryGeneral.Data;
                result := TRUE;
              except
                   on Error : Exception do
                   begin
                        Result := False;
                   end;
             end;

          end;
     end;

begin

 lEnviar := FALSE;
 Result :=  VACIO;
  try
     try
        cdsLastDataBackupBDEmpresa := TZetaClientDataSet.Create(nil);
        cdsLastDataBackupBDEmpresa.FieldDefs.Add('CM_NOMBRE', ftString, 50, false);
        cdsLastDataBackupBDEmpresa.FieldDefs.Add('DatabaseName', ftString, 255, false);
        cdsLastDataBackupBDEmpresa.FieldDefs.Add('LastBackUpTime', ftString, 50, false);
        cdsLastDataBackupBDEmpresa.CreateDataSet;
        cdsLastDataBackupBDEmpresa.Open;

         if  GetDatosLastBackup then
         begin
              iCuantos := cdsLastDataBackupBDEmpresa.RecordCount;
              If iCuantos > 0 then
              begin
                   try
                       Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
                       Result := Result + '<PROCESS>' +char(10)+char(13);
                       Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                       Result := Result +         '<DATA>'  +char(10)+char(13);
                       sXmlLastBackup := GeneraXMLFromDataset(cdsLastDataBackupBDEmpresa , 'RESPALDOS', 'RESPALDO');
                       sXmlLastBackup := stringReplace( sXmlLastBackup, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
                       sXmlLastBackup := stringReplace( sXmlLastBackup, '<?xml version="1.0" standalone="yes"?>', '', []);
                       Result := Result + sXmlLastBackup;
                       Result := Result + '</DATA>'  +char(10)+char(13);
                       Result := Result + '</PROCESS>'  +char(10)+char(13);
                       lEnviar := TRUE;

                       except on Error: Exception do
                       begin
                              lEnviar := FALSE;
                       end;
                    end
              end
              else
              begin
                   lEnviar := false;
              end
        end;
    Except
          on E : Exception do
          begin
              sError := E.Message;
              lEnviar := false;
          end
    end
  finally
    FreeAndNil(cdsLastDataBackupBDEmpresa);
  end;
end;


procedure TdmInterfase.cdsPeriodosAfectadosAlAdquirirDatos(
  Sender: TObject);
const
     K_QUERY_PERIODOS_AFECTADOS = 'select ' +
     {$ifdef DOS_CAPAS}
     ' PERIODO.PE_YEAR AS Anio, '+
     {$ELSE}
     ' PERIODO.PE_YEAR AS Year, '+
     {$ENDIF}
     ' PERIODO.PE_TIPO AS Tipo, PERIODO.PE_NUMERO AS Numero, PERIODO.PE_DESCRIP AS Nombre, PE_FEC_INI as Inicial, PE_FEC_FIN as Final, ' +
     ' PE_FEC_PAG as Pago, 1 as Frecuencia, PE_USO + 1 as Uso, ' + 
     ' 0 as INCLUIR,RPATRON.RS_CODIGO, RS_NOMBRE, PERIODO.PE_YEAR, PERIODO.PE_TIPO,TP_NOMBRE, TP_DESCRIP, '+
     ' PERIODO.PE_NUMERO, PE_DESCRIP, StatusPeriodo.PE_STATUS, StatusPeriodo.PE_TIMBRO,  StatusPeriodo.PendientesTotal as PORTIMBRAR, ' +
     ' PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, count(*) as PE_NUM_EMP,  sum(NO_PERCEPC) as PE_TOT_PER, sum(NO_DEDUCCI) as PE_TOT_DED,  sum(NO_NETO) as PE_TOT_NET,' +
     ' StatusPeriodo.TimbradasTotal as TIMBRADOS from PERIODO' +
     ' join NOMINA on PERIODO.PE_YEAR = NOMINA.PE_YEAR and PERIODO.PE_TIPO = NOMINA.PE_TIPO and PERIODO.PE_NUMERO = NOMINA.PE_NUMERO' +
     ' join TPERIODO on TPERIODO.TP_TIPO = PERIODO.PE_TIPO' +
     ' join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON' +
     ' join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO' +
     ' join ( select PE_YEAR, PE_TIPO, PE_NUMERO, PE_STATUS, PE_TIMBRO, PendientesTotal, TimbradasTotal from ' +
                       'dbo.Periodo_GetStatusTimbrado( %1:d,%2:d,%3:d,%4:s ) ) StatusPeriodo  ' +
                       'on PERIODO.PE_YEAR = StatusPeriodo.PE_YEAR and PERIODO.PE_TIPO = StatusPeriodo.PE_TIPO and PERIODO.PE_NUMERO = StatusPeriodo.PE_NUMERO  ' +
     ' where PERIODO.PE_STATUS >= %0:d and PERIODO.PE_YEAR = %1:d  and PERIODO.PE_TIPO = %2:d  and PERIODO.PE_NUMERO = %3:d ' +
     ' group by  RPATRON.RS_CODIGO, RS_NOMBRE, PERIODO.PE_YEAR, PERIODO.PE_TIPO, TP_NOMBRE, TP_DESCRIP,'+
     ' PERIODO.PE_NUMERO, PERIODO.PE_DESCRIP, StatusPeriodo.PE_STATUS, StatusPeriodo.PE_TIMBRO, PERIODO.PE_FEC_INI, PERIODO.PE_FEC_FIN, PERIODO.PE_FEC_PAG, PERIODO.PE_USO, ' +
     ' StatusPeriodo.PendientesTotal , StatusPeriodo.TimbradasTotal ' +
     ' order by PE_FEC_PAG desc ';

K_TIMBRAMEX_SEMANA = 1;
K_TIMBRAMEX_DECENA = 2;
K_TIMBRAMEX_CATORCENA = 3;
K_TIMBRAMEX_QUINCENA = 4;
K_TIMBRAMEX_MES = 5;
K_TIMBRAMEX_EVENTUAL = 6;
K_TIMBRAMEX_DIARIO = 7;

var
   oParametrosNominasTotales: TZetaParams;


     function GetFrecuenciaTimbramex : integer;
     var
     Dias:Integer;
     begin
     if (Global.GetGlobalBooleano( K_GLOBAL_TIMBRADO_USAR_PERIODICIDAD_2 ))then
     begin
          DmCatalogos.cdsTPeriodos.Refrescar;
          if(DmCatalogos.CdsTperiodos.Locate('TP_TIPO',dmCliente.PeriodoTipo,[]))then
          begin
			Dias := DmCatalogos.cdsTPeriodos.FieldByName('TP_DIAS').AsInteger;
			if(dias >16)                   then result := K_TIMBRAMEX_MES;
			if(dias <= 16) and (dias >12)  then result := K_TIMBRAMEX_QUINCENA;
			if(dias <= 12) and (dias > 10) then result := K_TIMBRAMEX_CATORCENA ;
			if(dias <=10)  and (dias > 7)  then result := K_TIMBRAMEX_DECENA;
			if(dias <=7)   and ( dias > 1 )       then result := K_TIMBRAMEX_SEMANA;
			if(dias <=1)  then result := K_TIMBRAMEX_DIARIO; 
          end
          else
          begin
               REsult := K_TIMBRAMEX_SEMANA ;
          end
     end
     else
     begin
       //eTipoPeriodo   = (tpDiario,tpSemanal,tpCatorcenal,tpQuincenal,tpMensual,tpDecenal,tpSemanalA,tpSemanalB,tpQuincenalA,tpQuincenalB,tpCatorcenalA,tpCatorcenalB);
          case   dmCliente.PeriodoClasificacion of  //@(am):Tipos de periodo
                 tpDiario: Result := K_TIMBRAMEX_DIARIO ;
                 tpSemanal:REsult := K_TIMBRAMEX_SEMANA;
                 tpCatorcenal:REsult := K_TIMBRAMEX_CATORCENA ;
                 tpQuincenal: REsult := K_TIMBRAMEX_QUINCENA;
                 tpMensual : REsult :=K_TIMBRAMEX_MES;
                 tpDecenal : REsult :=K_TIMBRAMEX_DECENA;
          else
              Result := K_TIMBRAMEX_SEMANA
          end;
     end;

     end;

     function GetTotalNominasPendientes( oParametros: TZetaClientDataSet ): TZetaParams;
     const
          K_CONSULTA = 'select pendientes,timbradas,PendientesTotal, TimbradoPendientes, CancelacionPendiente ,TimbradasTotal from dbo.Periodo_GetStatusTimbrado_RazonSocial( %0:d, %1:d, %2:d, %3:s, %4:s )';
     begin
          Result := TZetaParams.Create(nil);
          with dmConsultas do
          begin
               SQLText := Format( K_CONSULTA, [  oParametros.FieldByName('Year').AsInteger, oParametros.FieldByName('Tipo').AsInteger,
                                                 oParametros.FieldByName('Numero').AsInteger, EntreComillas( dmCliente.Confidencialidad ),
                                                 EntreComillas( oParametros.FieldByName('RS_CODIGO').AsString )] );
               try
                  cdsQueryGeneral.Refrescar;
                  Result.AddInteger('TIMBRADOS', 0 );
                  Result.AddInteger('PORTIMBRAR', 0 );
                  if ( cdsQueryGeneral.RecordCount > 0 ) then
                  begin
                       Result.AddInteger('TIMBRADOS', cdsQueryGeneral.FieldByName( 'TimbradasTotal' ).AsInteger );
                       Result.AddInteger('PORTIMBRAR', cdsQueryGeneral.FieldByName( 'PendientesTotal' ).AsInteger );
                  end;
               except
                     on Error : Exception do
                     begin
                     end;
               end;
          end;
     end;

begin
//  eStatusPeriodo = (spNueva,spPreNomina,spSinCalcular,spCalculadaParcial,spCalculadaTotal,spAfectadaParcial,spAfectadaTotal);

       //dmCliente.PeriodoTipo := eTipoPeriodo( cdsPeriodosAfectadosTotal.FieldByName('PE_TIPO').AsInteger  );
       //dmCliente.SetPeriodoNumero( cdsPeriodosAfectadosTotal.FieldByName('PE_NUMERO').AsInteger );


        with dmConsultas do
        begin
             SQLText := Format( K_QUERY_PERIODOS_AFECTADOS, [  Ord(spAfectadaParcial), dmCliente.YearDefault, Ord( dmCliente.PeriodoTipo ), dmCliente.PeriodoNumero, EntreComillas( dmCliente.Confidencialidad )] );

             {$ifdef WSPROFILE_SQL}
             SQLProfile( 'Tomar Periodos Afectados',  SQLText );
             {$endif}

             try
                cdsQueryGeneral.Refrescar;
                cdsPeriodosAfectados.Data := cdsQueryGeneral.Data;
                cdsPeriodosAfectados.Edit;
                cdsPeriodosAfectados.DisableControls;

               {$ifdef WSPROFILE_SQL}
              cdsPeriodosAfectados.SaveToFile(Format('SQLPROFILE_PeriodosAfectados_%d_%d_%d.xml', [dmCliente.YearDefault, Ord( dmCliente.PeriodoTipo ), dmCliente.PeriodoNumero]), dfXML);
               {$endif}


                with cdsPeriodosAfectados do
                begin
                     first;
                     while not Eof do
                     begin
                          Edit;
                          if StrVacio( FieldByName('Nombre').AsString ) then
                          begin

                              FieldByName('Nombre').AsString :=Format('%s #%d del %d', [  ObtieneElemento( lfTipoPeriodo,  FieldByName('Tipo').AsInteger ) , FieldByName('Numero').AsInteger,  {$IFDEF DOS_CAPAS}FieldByName('Anio').AsInteger{$ELSE}FieldByName('Year').AsInteger{$ENDIF} ] );

                          end;
                          oParametrosNominasTotales := GetTotalNominasPendientes( cdsPeriodosAfectados );
                          FieldByName( 'TIMBRADOS' ).AsInteger :=  oParametrosNominasTotales.ParamByName( 'TIMBRADOS' ).AsInteger;
                          FieldByName( 'PORTIMBRAR' ).AsInteger := oParametrosNominasTotales.ParamByName( 'PORTIMBRAR' ).AsInteger;
                          FieldByName( 'Frecuencia' ).AsInteger := GetFrecuenciaTimbramex;

                          Next;
                     end;
                end;
                cdsPeriodosAfectados.EnableControls;

             except
                   on Error : Exception do
                   begin
                        DatabaseError( Format( 'Error al consultar periodos afectados: %s', [ Error.Message ] ) );
                   end;
             end;
        end;
end;

procedure TdmInterfase.cdsPeriodosAfectadosAlCrearCampos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender )  do
     begin
          MaskPesos( 'PE_TOT_PER' );
          MaskPesos( 'PE_TOT_DED' );
          MaskPesos( 'PE_TOT_NET' );
          MaskFecha( 'PE_FEC_INI');
          MaskFecha( 'PE_FEC_FIN');
          MaskFecha( 'PE_FEC_PAG');
          ListaFija( 'PE_TIPO', lfTipoPeriodo );
          ListaFija( 'PE_STATUS', lfStatusPeriodo );
          ListaFija( 'PE_TIMBRO', lfStatusTimbrado );
     end;
end;

procedure TdmInterfase.cdsPeriodosAfectadosCalcFields(DataSet: TDataSet);
//var
//   Parametros, RecibeParametros: TZetaParams;
begin
//     with Dataset do
//     begin
//          try
//             Parametros := TZetaParams.Create;
//             with Parametros do
//             begin
//                  AddInteger('Anio', dmCliente.GetDatosPeriodoActivo.Year );
//                  AddInteger('Tipo', dmCliente.GetDatosPeriodoActivo.Tipo );
//                  AddInteger('Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
//                  AddString('Nivel0', dmCliente.Confidencialidad );
//             end;
//
//             RecibeParametros := TZetaParams.Create;
//
//             RecibeParametros.VarValues := dmProcesos.GetStatusTimbrado( Parametros );
//             FieldByName( 'PE_TIMBRADAS' ).AsInteger := RecibeParametros.ParamValues['Timbradas'];
//
//          finally
//                 Parametros.Free;
//                 RecibeParametros.Free;
//          end;
//     end;
end;

procedure TdmInterfase.cdsBeforePost( DataSet: TDataSet; const sCampo: String );
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( sCampo ).AsString ) then
             DataBaseError( 'El C�digo No Puede Quedar Vac�o' );
     end;
end;


procedure TdmInterfase.cdsRSocialAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with TClientDataset( Dataset ) do
     begin
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerCatalogosTimbrado.GrabaCatalogoRazonesSociales( dmCliente.Empresa, Tag, Delta, ErrorCount, FParametrosRazonSocial.VarValues ) );
          end;
     end;
end;

procedure TdmInterfase.cdsRSocialAlAdquirirDatos(Sender: TObject);
begin
     cdsRSocial.Data:= ServerCatalogosTimbrado.GetRSocial(dmCliente.Empresa);
end;


procedure TdmInterfase.cdsRSocialGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := false;
end;

procedure TdmInterfase.cdsRSocialBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'RS_CODIGO' );
     if( ( cdsRSocial.FieldByName('RS_SUBSID').AsFloat > 1 ) or ( cdsRSocial.FieldByName('RS_SUBSID').AsFloat < 0 ) )then
     begin
          DataBaseError( '� La Proporci�n Del Subsidio '+
                         'Acreditable Debe De Ser Un Valor Entre 0 - 1 !');

     end;
     if zStrToBool( cdsRSocial.FieldByName('RS_SELLO').AsString ) then
     begin
          if StrVacio( cdsRSocial.FieldByName('RS_KEY_PU').AsString ) or
             StrVacio( cdsRSocial.FieldByName('RS_KEY_PR').AsString ) then
          begin
               DatabaseError( 'Los archivos del SAT no han sido validados correctamente.' +CR_LF +
                              'Favor de volver a indicarlos y validarlos' );
          end;
     end
     else
     begin
          with cdsRSocial do
          begin
               FieldByName('RS_CERT').AsString := ' ';
               FieldByName('RS_SERIAL').AsString := VACIO;
               FieldByName('RS_ISSUETO').AsString := VACIO ;
               FieldByName('RS_ISSUEBY').AsString := VACIO ;
               FieldByName('RS_VAL_INI').AsDateTime := NullDateTime ;
               FieldByName('RS_VAL_FIN').AsDateTime := NullDateTime ;
               FieldByName('RS_KEY_PU').AsString := ' ';
               FieldByName('RS_KEY_PR').AsString:= ' ';
          end;
     end;
end;

procedure TdmInterfase.cdsRSocialAlModificar(Sender: TObject);
begin
    // ZBaseEdicion.ShowFormaEdicion( EditCatRSocial, TEditCatRSocial );
end;

procedure TdmInterfase.cdsRSocialAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsRSocial do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCatalogosTimbrado.GrabaCatalogoRazonesSociales( dmCliente.Empresa, Tag, Delta, ErrorCount, FParametrosRazonSocial.VarValues ) ) then
               begin
                    TressShell.SetDataChange( [ enRSocial ] );
               end;
          end;
     end;

end;

procedure TdmInterfase.cdsCuentasTimbramexAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsCuentasTimbramex.Data := ServerSistemaTimbrado.GetCuentasTimbrado;//
end;


procedure TdmInterfase.cdsCuentasTimbramexAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     inherited;
     ErrorCount := 0;
     with cdsCuentasTimbramex do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconciliar( ServerSistemaTimbrado.GrabaCuentasTimbrado( Delta, ErrorCount ) );
     end;
end;

procedure TdmInterfase.cdsCuentasTimbramexBeforePost(DataSet: TDataSet);
begin
  inherited;
     with cdsCuentasTimbramex do
     begin
          if ( not StrLleno( FieldByName( 'CT_CODIGO' ).AsString ) ) then
          begin
               FieldByName( 'CT_CODIGO' ).FocusControl;
               DataBaseError( 'C�digo no puede quedar vac�o' );
          end;

          if ( not StrLleno( FieldByName( 'CT_PASSWRD' ).AsString ) ) then
          begin
               FieldByName( 'CT_PASSWRD' ).FocusControl;
               DataBaseError( 'Contrase�a no puede quedar vac�a' );
          end
          else
          begin
               if StrVacio( DecryptPassword( FieldByName( 'CT_PASSWRD' ).AsString ) ) then
                  DataBaseError( 'Contrase�a es inv�lida' );
          end;

          if ( FieldByName( 'CT_ID' ).AsInteger <= 0   ) then
          begin
               FieldByName( 'CT_ID' ).FocusControl;
               DataBaseError( 'Cuenta ID debe ser un n�mero mayor a 0' );
          end;




     end;
end;

procedure TdmInterfase.cdsCuentasTimbramexGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
      lHasRights := ZAccesosMgr.CheckDerecho( D_TIM_DATOS_CUENTAS, iRight );
end;

procedure TdmInterfase.cdsCuentasTimbramexAlModificar(Sender: TObject);
begin
 ZBaseEdicion_DevEx.ShowFormaEdicion( EditCuentaTimbramex, TEditCuentaTimbramex );
end;

procedure TdmInterfase.cdsCuentasTimbramexAfterDelete(DataSet: TDataSet);
begin
  inherited;
     cdsCuentasTimbramex.Enviar;
end;

function TdmInterfase.GetContribuyente( const sArchivoKey, sArchivoCer, sClavePrivada, sEmpresaFirma : string; sRegimenFiscalDescrip : string; sRegimenFiscalCodigo : string; lActualizar : boolean) : string;
const
     K_TITULO_ERROR = 'Exportaci�n de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
Var
    iReporte, iReportePeriodos : integer;

     XMLTools: DXMLTools.TdmXMLTools;
     sNombre : string;
     sXML, sCONT_PASS, sUsuarioNombre : string;
     iCONT_ID  : integer;

     function FileToBase46( sFilePath : string ) : string;
     var
        SourceStr: TFileStream;
        Encoder: TIdEncoderMIME;
     begin
         SourceStr := TFileStream.Create(sFilePath, fmOpenRead);
         try
           Encoder := TIdEncoderMIME.Create(nil);
           try
             Result := Encoder.Encode(SourceStr);
           finally
             Encoder.Free;
           end;
         finally
           SourceStr.Free;
         end;
     end;


begin
     { Variables locales del procesamiento }
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;

     dmCliente.cdsTPeriodos.Refrescar;

     Result := '';
     iReporte := 640;
     iReportePeriodos := 641;
     iCONT_ID := 1;

     XMLTools := TdmXMLTools.Create( nil );

       try
            if ( iReporte > 0 ) then
            begin
                 Result := '<PROCESS>' +char(10)+char(13);
                 Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                 Result := Result +         '<DATA>'  +char(10)+char(13);
                 Result := Result +
                '<EmpresaRFC> ' +HTMLEncode( cdsRSocial.FieldByName('RS_RFC').AsString) +  '</EmpresaRFC> '+
                '<EmpresaCURP> ' +HTMLEncode( cdsRSocial.FieldByName('RS_CURP').AsString) +  '</EmpresaCURP> '+
		'<EmpresaRazon>' + HTMLEncode(cdsRSocial.FieldByName('RS_NOMBRE').AsString )+  '</EmpresaRazon>  '+
		'<EmpresaRepLegal>' + HTMLEncode(cdsRSocial.FieldByName('RS_RLEGAL').AsString) +  '</EmpresaRepLegal>     '+
    '<EmpresaRepLegalRFC>' + HTMLEncode(cdsRSocial.FieldByName('RS_RL_RFC').AsString) +  '</EmpresaRepLegalRFC>     '+
                '<EmpresaRegimenes>' + HTMLEncode( sRegimenFiscalDescrip ) +  '</EmpresaRegimenes>     '+
                '<EmpresaRegimen>' + HTMLEncode( sRegimenFiscalCodigo ) +  '</EmpresaRegimen>     '+
		'<DomicilioCalle>' + HTMLEncode(cdsRSocial.FieldByName('RS_CALLE').AsString) +  '</DomicilioCalle> '+
		'<DomicilioNumExt>' +HTMLEncode( cdsRSocial.FieldByName('RS_NUMEXT').AsString) +  '</DomicilioNumExt> '+
		'<DomicilioNumInt>' + HTMLEncode(cdsRSocial.FieldByName('RS_NUMINT').AsString) +  '</DomicilioNumInt> '+
		'<DomicilioColonia>' +HTMLEncode( cdsRSocial.FieldByName('RS_COLONIA').AsString) +  '</DomicilioColonia> '+
		'<DomicilioCodPost>' + HTMLEncode(cdsRSocial.FieldByName('RS_CODPOST').AsString) +  '</DomicilioCodPost> '+
		'<DomicilioCiudad>' + HTMLEncode(cdsRSocial.FieldByName('RS_CIUDAD').AsString )+  '</DomicilioCiudad> '+
		'<EstadoID>' + HTMLEncode(dmTablas.cdsEstado.FieldByName('TB_STPS').AsString )+  '</EstadoID> '+
		'<EstadoNombre>' + dmTablas.cdsEstado.FieldByName('TB_ELEMENT').AsString +  '</EstadoNombre> '+
		'<PaisID>' + K_PAIS_DEFAULT +  '</PaisID> '+
		'<PaisNombre>' + K_PAIS_DEFAULT_NAME +  '</PaisNombre> ';

                if not lActualizar then
                begin
                    Result := Result + '       <NumCert>01234567890123456789</NumCert> ';
                    Result := Result + '       <Certificado>' + FileToBase46(sArchivoCer) + '</Certificado>';
                    Result := Result + '       <Llave>' + FileToBase46(sArchivoKey) + '</Llave>';
                    Result := Result + '       <Privada>' + HTMLEncode( sClavePrivada ) + '</Privada>';
                    Result := Result + '       <EmpresaFirma>' + HTMLEncode( sEmpresaFirma ) +  '</EmpresaFirma> ';
                end;
                 Result := Result +         '</DATA>'  +char(10)+char(13);
                 Result := Result + '</PROCESS>'  +char(10)+char(13);

            end;
        except on Error: Exception do

               zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
        end;

     FreeAndNil(XMLTools);
     Application.ProcessMessages;

end;

function TdmInterfase.GetManifiesto(const sEmpresaFirma: string;  sCT_CODIGO : string ; iContribuyenteID : integer): String;
const
     K_SISTEMA_TIPO = 1; // Tress


  function GetDatosManifiestoContribuyente : string;
  begin
       Result := VACIO;
       if ( not  cdsRSocial.IsEmpty) then
       begin
           Result := Result + '<EmpresaRFC> ' + HTMLEncode( cdsRSocial.FieldByName('RS_RFC').AsString) +  '</EmpresaRFC> '+
                              '<EmpresaRazon>' + HTMLEncode( cdsRSocial.FieldByName('RS_NOMBRE').AsString) +  '</EmpresaRazon>  '+
                              '<EmpresaRepLegal>' + HTMLEncode( cdsRSocial.FieldByName('RS_RLEGAL').AsString )+  '</EmpresaRepLegal> '+
                              '<EmpresaFirma>' + HTMLEncode( sEmpresaFirma ) +  '</EmpresaFirma> ';
       end;
  end;

begin
  Result := '<?xml version="1.0" encoding="iso-8859-1" ?>';
  Result := Result + '<PROCESS> ';
  Result := Result + GetCredenciales(  sCT_CODIGO, 0  ) ;
  Result := Result + '<DATA>' + GetDatosManifiestoContribuyente + '</DATA>' ;
  Result := Result + '</PROCESS> ';
end;

function TdmInterfase.GetFacturaUUID( const iNoFactura : integer ;  sCT_CODIGO : string ): String;
const
     K_SISTEMA_TIPO = 1; // Tress

     function GetDatosFacturaUUID : string;
     begin

        Result := Result  + HTMLEncode( intToStr(iNoFactura)) ;

     end;

begin
  Result := '<?xml version="1.0" encoding="iso-8859-1" ?>';
  Result := Result + '<PROCESS> ';
  Result := Result + GetCredenciales(  sCT_CODIGO, 0  ) ;
  Result := Result + '<DATA><FACTURA_ID>' + GetDatosFacturaUUID + '</FACTURA_ID></DATA>' ;
  Result := Result + '</PROCESS> ';
end;


procedure TdmInterfase.GrabaContribuyente( sRS_CODIGO,  sCT_CODIGO : string; iContribuyenteID : integer );
begin
{
CT_CODIGO
RS_CONTID}

     with cdsRSocial do
     begin
          Edit;
          FieldByName('CT_CODIGO').AsString  := sCT_CODIGO;
          FieldByName('RS_CONTID').AsInteger := iContribuyenteID;
          Post;
     end;

end;



function TdmInterfase.GetContribuyenteCancelar : string;
const
     K_TITULO_ERROR = 'Exportaci�n de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
Var
    iReporte, iReportePeriodos : integer;

     XMLTools: DXMLTools.TdmXMLTools;
     sNombre : string;
     sXML, sCONT_PASS, sUsuarioNombre : string;
     iCONT_ID  : integer;



begin
     { Variables locales del procesamiento }
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;

     dmCliente.cdsTPeriodos.Refrescar;

     Result := '';
     iReporte := 640;
     iReportePeriodos := 641;
     iCONT_ID := 1;

     XMLTools := TdmXMLTools.Create( nil );

       try
            if ( iReporte > 0 ) then
            begin
                 Result := '<PROCESS>' +char(10)+char(13);
                 Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                 Result := Result + '</PROCESS>'  +char(10)+char(13);

            end;
        except on Error: Exception do

               zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
        end;

     FreeAndNil(XMLTools);
     Application.ProcessMessages;

end;


function TdmInterfase.GetRecibosDataSet(ParametrosEmpleados : TZetaParams; oNotificarStatus : INotificarStatusReporte; lIncluirDetalle, lIncluirIncapacidades, lIncluirTiempoExtra, lIncluirSubContratos  : boolean): boolean;
     function SetParametrosEmpleadosTimbrar : boolean;
     begin

        dmCliente.RazonSocial := cdsRSocial.FieldByName('RS_CODIGO').AsString ;
        if(ParametrosEmpleados[0].AsString='') and (ParametrosEmpleados[1].AsString = '') and (ParametrosEmpleados[2].AsString = '')
        then
        begin
              FTodosEmpleados:= true;
        end
        else
        begin
             FTodosEmpleados:= false;
        end;
        with FParametros do
        begin
                    AddInteger( 'Year',dmCliente.YearDefault );
                    AddInteger( 'Tipo',ord( dmCliente.PeriodoTipo )  );
                    AddInteger( 'Numero', dmCliente.PeriodoNumero );
                    AddInteger( 'StatusAnterior', ord( estiPendiente ) );
                    AddInteger( 'StatusNuevo', ord( estiTimbrado ) );
                    AddInteger( 'ContribuyenteID',  cdsRSocial.FieldByName('RS_CONTID').AsInteger );
                    AddString('RazonSocial' , cdsRSocial.FieldByName('RS_CODIGO').AsString  );
                    AddString( 'RangoLista', ParametrosEmpleados[0].AsString );
                    AddString( 'Condicion', ParametrosEmpleados[1].AsString  );
                    AddString( 'Filtro', ParametrosEmpleados[2].AsString  );
                    AddString( 'Orden', ParametrosEmpleados[3].AsString );
        end;
     end;



    Function GetEmpleadosAtimbrar:boolean;
    begin
    SetParametrosEmpleadosTimbrar;
          if(not FTodosEmpleados) then
          begin

          TimbrarNominaGetLista( FParametros );
          end;
    end;
  { function SetParametrosNominasTimbrar : boolean;
   begin

        dmCliente.RazonSocial := cdsRSocial.FieldByName('RS_CODIGO').AsString ;
        with FParametros do
        begin
             AddInteger( 'Year',dmCliente.YearDefault );
             AddInteger( 'Tipo',ord( dmCliente.PeriodoTipo )  );
             AddInteger( 'Numero', dmCliente.PeriodoNumero );
             AddInteger( 'StatusAnterior', ord( estiPendiente ) );
             AddInteger( 'StatusNuevo', ord( estiTimbrado ) );
             AddInteger( 'ContribuyenteID',  cdsRSocial.FieldByName('RS_CONTID').AsInteger );
             AddString('RazonSocial' , cdsRSocial.FieldByName('RS_CODIGO').AsString  );
             AddString( 'RangoLista', Vacio )
             AddString( 'Condicion', Vacio );
             AddString( 'Filtro', Vacio );
        end ;
   end;}

   {function GetNominasFiltradas : boolean;
   begin
        SetParametrosNominasTimbrar;
        dmProcesos.TimbrarNominaGetLista( FParametros );
        cdsNominaFiltrar.Data := dmProcesos.cdsDataSet.Data;

   end; }


   function GetReporteCodigo( sReporteName : string)  : Integer;
   const
        K_GET_REPORTE_CODIGO = 'select RE_CODIGO from REPORTE where UPPER( RE_NOMBRE ) = UPPER( ''%s'')';
   begin
        Result := 0;
        with dmConsultas do
        begin

             SQLText := Format( K_GET_REPORTE_CODIGO , [sReporteName ] ) ;

             try
                cdsQueryGeneral.Refrescar;
                if ( cdsQueryGeneral.RecordCount > 0 ) then
                   result := cdsQueryGeneral.FieldByName( 'RE_CODIGO' ).AsInteger;
             except
                   on Error : Exception do
                   begin
                        zError( 'No existe el reporte',  'No existe un Reporte Configurado como '+ sReporteName, 0);
                   end;
             end;
        end;
   end;

   function GetReporteTimbradoNominas : integer;
   begin
        Result := GetReporteCodigo( K_REPORTE_TIMBRADO );
   end;

   function GetReporteTimbradoDetalle : integer;
   begin
        Result := GetReporteCodigo( K_REPORTE_TIMBRADO_DETALLE);
   end;

   function GetReporteTimbradoIncapacidades : integer;
   begin
        Result := GetReporteCodigo( K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES);
   end;

   function GetReporteTimbradoTiempoExtra : integer;
   begin
        Result := GetReporteCodigo( K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA);
   end;

   {*** Detalle de Recibo de SubContrato ***}
   function GetReporteTimbradoSubContratos : integer;
   begin
        Result := GetReporteCodigo( K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS);
   end;
   {*** FIN ***}

   procedure ReplicaDisplayLables(Source, Dest: TDataSet);
var
  Field : TField;
  FieldDef: TFieldDef;
  iField, nFields : integer;
begin
  nFields := Source.Fields.Count;
  iField := 0;
  for iField := 0 to nFields-1  do
  begin
    Field := Source.Fields[iField];
    Dest.FieldByName( Field.FieldName ).DisplayLabel := Field.DisplayLabel;
  end;
end;


procedure AjustarValoresActivos;
begin
      dmCliente.PeriodoTipo := eTipoPeriodo( cdsPeriodosTimbrar.FieldByName('PE_TIPO').AsInteger  );
      dmCliente.SetPeriodoNumero( cdsPeriodosTimbrar.FieldByName('PE_NUMERO').AsInteger ) ;
end;

function GenerarRecibosDetalleTimbrar : boolean;
var
   iReporteDetalle : integer;
   sNombreDetalle : string;
   Parametros :TZetaParams;
begin
    iReporteDetalle := GetReporteTimbradoDetalle;
    sNombreDetalle := VACIO;

    if ( cdsRecibosDetalleTimbrar.Active ) then
     cdsRecibosDetalleTimbrar.EmptyDataSet;

    if iReporteDetalle  > 0 then
    begin
       {$ifdef PROFILE}WSProfile('GenerarRecibosDetalleTimbrar', 'Procesando'); {$endif}
       oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE, 'Procesando...', '' , K_STATUS_REPORTE_EXECUTING  );
       AjustarValoresActivos;

       {$ifdef PROFILE}WSProfile('GenerarRecibosDetalleTimbrar', sNombreDetalle); {$endif}
       if ( dmReportesXML.Procesar('ReporteNominas', iReporteDetalle, sNombreDetalle) > 0 ) then
       begin
            {$ifdef PROFILE}WSProfile('GenerarRecibosDetalleTimbrar', 'Con datos'); {$endif}
            cdsRecibosDetalleTimbrar.Data := dmReportesXML.Resultado.Data;
            ReplicaDisplayLables( dmReportesXML.Resultado, cdsRecibosDetalleTimbrar ) ;
              {$ifdef PROFILE}WSProfile('GenerarRecibosDetalleTimbrar', 'DEspues de  ReplicaDisplayLables'); {$endif}
            Result := ( cdsRecibosDetalleTimbrar.Active ) and (  not  cdsRecibosDetalleTimbrar.IsEmpty );
            oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE, 'Termin�', Format('(%d movimentos ) ', [cdsRecibosDetalleTimbrar.RecordCount])  , K_STATUS_REPORTE_OK  );
            dmReportesXML.Resultado := nil;
       end
       else
       begin
                {$ifdef PROFILE}WSProfile('GenerarRecibosDetalleTimbrar', 'Sin datos'); {$endif}
             oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE, 'Termin�', '(sin movimientos)', K_STATUS_REPORTE_ERROR  );
            try
               Parametros := TZetaParams.Create(Self);
               Parametros.AddString('Titulo','Reporte de Timbrado Detalle Sin Registros');
               Parametros.AddString('Descripcion',dmReportesXML.FSQLAgente.SQL.Text);
               ServerSistemaTimbrado.CrearBitacora(dmCliente.Empresa,0,Parametros.VarValues);
            finally
                   Parametros.Free;
            end;
       end
  end
  else
  begin
    oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE, 'No existe el reporte en la empresa', '' , K_STATUS_REPORTE_ERROR  );
    zError( K_REPORTE_TIMBRADO_DETALLE, Format( 'No se encontr� el reporte de Detalle: %s', [ K_REPORTE_TIMBRADO_DETALLE ] ), 0 );
  end;

end;

function GenerarRecibosDetalleIncapacidades : boolean;
var
   iReporteDetalle, iCount : integer;
   sNombreDetalle : string;
   Parametros :TZetaParams;
begin
    iReporteDetalle := GetReporteTimbradoIncapacidades;
    sNombreDetalle := VACIO;

    if ( cdsRecibosIncapacidades.Active ) then
       cdsRecibosIncapacidades.EmptyDataSet;



    Result := FALSE;

    if iReporteDetalle  > 0 then
    begin
       oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, 'Procesando...', '' , K_STATUS_REPORTE_EXECUTING  );
       AjustarValoresActivos;

       try
           if ( dmReportesXML.ProcesarToleraVacio('ReporteNominas', iReporteDetalle, sNombreDetalle, iCount) ) then
           begin
                cdsRecibosIncapacidades.Data := dmReportesXML.Resultado.Data;
                ReplicaDisplayLables( dmReportesXML.Resultado, cdsRecibosIncapacidades ) ;
                if ( iCount > 0 )  then
                   oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, 'Termin�', Format('(%d registros ) ', [cdsRecibosIncapacidades.RecordCount])  , K_STATUS_REPORTE_OK  )
                else
                    oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, 'Termin�', '(Sin registros ) ' , K_STATUS_REPORTE_OK  );
           end
           else
               oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, 'Termin�', '(Sin registros ) ' , K_STATUS_REPORTE_OK  );

           dmReportesXML.Resultado := nil;
           Result := TRUE;
       except on Error: Exception do
          begin
              oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, 'Error',  Error.Message , K_STATUS_REPORTE_ERROR  );
              zError( K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, Format( 'Error al consultar el reporte de Incapacidades: %s', [ Error.Message ] ),0 );
          end;
       end;
  end
  else
  begin
    oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, 'No existe el reporte en la empresa', '' , K_STATUS_REPORTE_ERROR  );
    zError( K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, Format( 'No se encontr� el reporte de Incapacidades: %s', [ K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES ] ),0 );
  end;

end;

function GenerarRecibosDetalleTiempoExtra : boolean;
var
   iReporteDetalle, iCount: integer;
   sNombreDetalle : string;
   Parametros :TZetaParams;
begin
    Result := FALSE;
    iReporteDetalle := GetReporteTimbradoTiempoExtra;
    sNombreDetalle := VACIO;

    if ( cdsRecibosTiempoExtra.Active ) then
        cdsRecibosTiempoExtra.EmptyDataSet;

    if iReporteDetalle  > 0 then
    begin
       oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, 'Procesando...', '' , K_STATUS_REPORTE_EXECUTING  );
       AjustarValoresActivos;

       try
           if ( dmReportesXML.ProcesarToleraVacio('ReporteNominas', iReporteDetalle, sNombreDetalle, iCount) ) then
           begin
                cdsRecibosTiempoExtra.Data := dmReportesXML.Resultado.Data;
                ReplicaDisplayLables( dmReportesXML.Resultado, cdsRecibosTiempoExtra ) ;
                if iCount > 0  then
                   oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, 'Termin�', Format('(%d registros ) ', [cdsRecibosTiempoExtra.RecordCount])  , K_STATUS_REPORTE_OK  )
                else
                    oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, 'Termin�', '(Sin registros ) ' , K_STATUS_REPORTE_OK  );

           end
           else
               oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, 'Termin�', '(Sin registros ) ' , K_STATUS_REPORTE_OK  );

           Result :=  TRUE;
           dmReportesXML.Resultado := nil;
       except on Error: Exception do
              begin
                   oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, 'Error',  Error.Message , K_STATUS_REPORTE_ERROR );
                   zError( K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, Format( 'Error al consultar el reporte de Tiempo Extra: %s', [ Error.Message ] ), 0 );
              end;
       end;
    end
    else
    begin
         oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, 'No existe el reporte en la empresa', '' , K_STATUS_REPORTE_ERROR  );
         zError( K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, Format( 'No se encontr� el reporte de Tiempo Extra: %s', [ K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA ] ),0 );
    end;

end;

{*** Detalle de Recibo de SubContrato ***}
function GenerarRecibosDetalleSubContratos : boolean;
var
   iReporteDetalle, iCount: integer;
   sNombreDetalle : string;
   Parametros :TZetaParams;
begin
    Result := FALSE;
    iReporteDetalle := GetReporteTimbradoSubContratos;
    sNombreDetalle := VACIO;

    if ( cdsRecibosSubContratos.Active ) then
        cdsRecibosSubContratos.EmptyDataSet;

    if iReporteDetalle  > 0 then
    begin
       AjustarValoresActivos;
       oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, 'Procesando...', '' , K_STATUS_REPORTE_EXECUTING  );
       try
           if ( dmReportesXML.ProcesarToleraVacio('ReporteNominas', iReporteDetalle, sNombreDetalle, iCount)) then
           begin
                cdsRecibosSubContratos.Data := dmReportesXML.Resultado.Data;
                ReplicaDisplayLables( dmReportesXML.Resultado, cdsRecibosSubContratos ) ;
                if iCount > 0  then
                   oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, 'Termin�', Format('(%d registros ) ', [cdsRecibosSubContratos.RecordCount])  , K_STATUS_REPORTE_OK  )
                else
                    oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, 'Termin�', '(Sin registros ) ' , K_STATUS_REPORTE_OK  );
           end
           else
               oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, 'Termin�', '(Sin registros ) ' , K_STATUS_REPORTE_OK  );


           Result :=  TRUE;
           dmReportesXML.Resultado := nil;
       except on Error: Exception do
              begin
                   oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, 'Error',  Error.Message , K_STATUS_REPORTE_ERROR );
                   zError( K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, Format( 'Error al consultar el reporte de SubContratos: %s', [ Error.Message ] ), 0 );
              end;
       end;
    end
    else
    begin
         oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, 'No existe', '' , K_STATUS_REPORTE_ERROR  );
         zError( K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS,  Format( 'No se encontr� el reporte de SubContratos: %s', [ K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS ] ) , 0);
    end;

end;
{*** FIN ***}

var
   iReporte : integer;
   sNombre : string;
   Parametros :TZetaParams;
begin
{  oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO, '', '' , 0 );
  oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE, '', '' , 0 );
  oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES, '', '' , 0 );
  oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA, '', '' , 0 );
  oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS, '', '' , 0 );}

  Result := FALSE;
  GetEmpleadosAtimbrar;
  iReporte := GetReporteTimbradoNominas;
  sNombre := VACIO;

  if ( cdsRecibosTimbrar.Active ) then
     cdsRecibosTimbrar.EmptyDataSet;



  if iReporte  > 0 then
  begin
       AjustarValoresActivos;
       oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO, 'Procesando...', '' , K_STATUS_REPORTE_EXECUTING  );
       if ( dmReportesXML.Procesar('ReporteNominas', iReporte, sNombre) > 0 ) then
       begin
            cdsRecibosTimbrar.Data := dmReportesXML.Resultado.Data;
            ReplicaDisplayLables( dmReportesXML.Resultado, cdsRecibosTimbrar ) ;
            Result :=  not  cdsRecibosTimbrar.IsEmpty;
            oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO, 'Termin�', Format('(%d Recibos ) ', [cdsRecibosTimbrar.RecordCount])  , K_STATUS_REPORTE_OK  );
            dmReportesXML.Resultado := nil;
            if Result and  lIncluirDetalle  then
            begin
                 Result := GenerarRecibosDetalleTimbrar;
            end;
            if Result then
            begin
                 //Como reportar los errores de los reportes de INcapacidades y Tiempo Extra

                 if lIncluirTiempoExtra then
                    Result := GenerarRecibosDetalleTiempoExtra;

                 if Result and lIncluirIncapacidades then
                    Result := GenerarRecibosDetalleIncapacidades;

                 if Result and lIncluirSubContratos then
                    Result := GenerarRecibosDetalleSubContratos;
            end;
       end
       else
       begin
            oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO, 'Sin Datos',  'El reporte no arroj� informaci�n.' , K_STATUS_REPORTE_ERROR  );
            try
               Parametros := TZetaParams.Create(Self);
               Parametros.AddString('Titulo','Reporte de Timbrado Sin Registros');
               Parametros.AddString('Descripcion',dmReportesXML.FSQLAgente.SQL.Text);
               ServerSistemaTimbrado.CrearBitacora(dmCliente.Empresa,0,Parametros.VarValues);
            finally
                   Parametros.Free;
            end;
       end


  end
  else
  begin
         oNotificarStatus.NotificarStatusReporte(K_REPORTE_TIMBRADO, 'Error',  'No se encontr� el reporte.' , K_STATUS_REPORTE_ERROR  );
         zError( K_REPORTE_TIMBRADO, Format( 'No se encontr� el reporte de Timbrado de N�mina: %s', [ K_REPORTE_TIMBRADO ] ), 0 );
  end;
   if ( cdsEmpleadosTimbrar.Active ) then
        cdsEmpleadosTimbrar.EmptyDataset;
end;

function TdmInterfase.TimbrarNomina( oParamsTimbradoNomina: TZetaParams ) : boolean;

   function SetParametrosNominasTimbrar : boolean;
   begin

         dmcliente.SetPeriodoNumero( dmCliente.PeriodoNumero ) ;
        dmCliente.RazonSocial := cdsRSocial.FieldByName('RS_CODIGO').AsString ;
        with FParametros do
        begin
             AddInteger( 'Year', oParamsTimbradoNomina.ParamByName('PE_YEAR').AsInteger );
             AddInteger( 'Tipo', oParamsTimbradoNomina.ParamByName('PE_TIPO').AsInteger  );
             AddInteger( 'Numero', oParamsTimbradoNomina.ParamByName('PE_NUMERO').AsInteger );
             AddInteger( 'StatusAnterior', ord( estiPendiente ) );
             AddInteger( 'StatusNuevo', ord( estiTimbrado ) );
             AddString('RazonSocial' , cdsRSocial.FieldByName('RS_CODIGO').AsString  );
             AddInteger( 'ContribuyenteID',  cdsRSocial.FieldByName('RS_CONTID').AsInteger );
             AddString( 'RangoLista', VACIO );
             AddString( 'Condicion', VACIO );
             AddString( 'Filtro', VACIO );
        end ;
   end;

begin

     Result := FALSE;
     
     if RespuestaTimbramex.transaccion = tTimbrar then
     begin
        SetParametrosNominasTimbrar;
        dmProcesos.cdsDataSet := RespuestaTimbramex.Resultado.FacturasDS;
        dmProcesos.TimbrarNomina( FParametros );
        Result := TRUE;
     end;

end;

function TdmInterfase.SetEstatusEmpleadoNomina( estatusTimbradoNuevo: eStatusTimbrado; oParamsTimbradoNomina: TZetaParams ): boolean;
   function SetParametrosNominasTimbrar : boolean;
   begin
        dmcliente.SetPeriodoNumero( dmCliente.PeriodoNumero );
        dmCliente.RazonSocial := cdsRSocial.FieldByName('RS_CODIGO').AsString;
        with FParametros do
        begin
             AddInteger( 'Year', oParamsTimbradoNomina.ParamByName('PE_YEAR').AsInteger );
             AddInteger( 'Tipo', oParamsTimbradoNomina.ParamByName('PE_TIPO').AsInteger  );
             AddInteger( 'Numero', oParamsTimbradoNomina.ParamByName('PE_NUMERO').AsInteger );
             AddInteger( 'StatusAnterior', ord( estatusTimbradoNuevo ) );
             AddInteger( 'StatusNuevo', ord( estatusTimbradoNuevo ) );
             AddString('RazonSocial' , cdsRSocial.FieldByName('RS_CODIGO').AsString );
             AddInteger( 'ContribuyenteID',  cdsRSocial.FieldByName('RS_CONTID').AsInteger );
             AddString( 'RangoLista', VACIO );
             AddString( 'Condicion', VACIO );
             AddString( 'Filtro', VACIO );
             AddBoolean('RESPUESTA_TIMBRADO', dmInterfase.HuboRespuestaTimbramex);
        end;
   end;
begin
     SetParametrosNominasTimbrar;
     if ( ( estatusTimbradoNuevo = estiTimbradoPendiente ) or ( estatusTimbradoNuevo = estiTimbradoPendienteLimpiar ) ) then
     begin
          if ( cdsRecibosTimbrar.Active ) then
             ServerNominaTimbrado.SetEmpleadosPendienteTimbrado(dmCliente.Empresa, GetLista(cdsRecibosTimbrar), FParametros.VarValues );
     end
     else if ( ( estatusTimbradoNuevo = estiCancelacionPendiente ) or ( estatusTimbradoNuevo = estiCancelacionPendienteLimpiar ) )then
     begin
          if ( dmProcesos.cdsDatasetTimbrados.Active ) then
             ServerNominaTimbrado.SetEmpleadosPendienteTimbrado(dmCliente.Empresa, GetLista(dmProcesos.cdsDatasetTimbrados), FParametros.VarValues );
     end;
     Result := TRUE;
end;


function TdmInterfase.MarcarNomina : boolean;
var
   rsContribuyenteID : integer;
   rsNombre : string;

   function SetParametrosNominasTimbrar : boolean;
   begin

        dmcliente.SetPeriodoNumero( dmCliente.PeriodoNumero ) ;
        dmCliente.RazonSocial := cdsRSocial.FieldByName('RS_CODIGO').AsString ;
        with FParametros do
        begin
             AddInteger( 'Year',dmCliente.YearDefault );
             AddInteger( 'Tipo',ord( dmCliente.PeriodoTipo )  );
             AddInteger( 'Numero', dmCliente.PeriodoNumero );
             AddInteger( 'StatusAnterior', ord( estiPendiente ) );
             AddInteger( 'StatusNuevo', ord( estiTimbrado ) );
             AddString('RazonSocial' , cdsRSocial.FieldByName('RS_CODIGO').AsString  );
             AddInteger( 'ContribuyenteID',  cdsRSocial.FieldByName('RS_CONTID').AsInteger );
             AddString( 'RangoLista', VACIO );
             AddString( 'Condicion', VACIO );
             AddString( 'Filtro', VACIO );
        end ;
   end;

begin
    rsContribuyenteID := cdsRSocial.FieldByName('RS_CONTID').AsInteger ;
    rsNombre := cdsRSocial.FieldByName('RS_NOMBRE').AsString ;

    if rsContribuyenteID <=0 then
    begin
         DatabaseError( Format( 'La raz�n social %s no est� registrada en el Sistema de Timbrado ' , [rsNombre]) ) ;
    end;

     SetParametrosNominasTimbrar;
     Result := ServerNominaTimbrado.MarcarTimbrado( dmCliente.Empresa,  FParametros.VarValues );
end;


procedure TdmInterfase.cdsPeriodosAfectadosTotalAlAdquirirDatos(
  Sender: TObject);
const
     K_QUERY_PERIODOS_AFECTADOS = 'select ' +
     {$ifdef DOS_CAPAS}
     ' PERIODO.PE_YEAR AS Anio, ' +
     {$ELSE}
     ' PERIODO.PE_YEAR AS Year, ' +
     {$ENDIF}
     ' PERIODO.PE_TIPO AS Tipo, PERIODO.PE_NUMERO AS Numero, PERIODO.PE_DESCRIP AS Nombre, PE_FEC_INI as Inicial, PE_FEC_FIN as Final, ' +
     ' PE_FEC_PAG as Pago, 1 as Frecuencia, ' +
     ' 0 as INCLUIR,PERIODO.PE_YEAR, PERIODO.PE_TIPO,TP_NOMBRE, TP_DESCRIP, '+
     ' PERIODO.PE_NUMERO, PE_DESCRIP, PE_STATUS, PE_TIMBRO,' +
     ' PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_NUM_EMP,PE_TOT_PER,PE_TOT_DED, PE_TOT_NET,  ' +
     ' (SELECT timbradas FROM DBO.Periodo_GetStatusTimbrado ( PERIODO.PE_YEAR, PERIODO.PE_TIPO, PERIODO.PE_NUMERO, '''' )) AS PE_TIMBRADAS from PERIODO  ' +
     ' join NOMINA on PERIODO.PE_YEAR = NOMINA.PE_YEAR and PERIODO.PE_TIPO = NOMINA.PE_TIPO and PERIODO.PE_NUMERO = NOMINA.PE_NUMERO' +
     ' join TPERIODO on TPERIODO.TP_TIPO = PERIODO.PE_TIPO' +
     ' where PERIODO.PE_STATUS >= %d and PERIODO.PE_YEAR = %d  and PERIODO.PE_TIPO = %d ' +
     ' group by  PERIODO.PE_YEAR, PERIODO.PE_TIPO, TP_NOMBRE, TP_DESCRIP,'+
     ' PERIODO.PE_NUMERO, PERIODO.PE_DESCRIP, PERIODO.PE_STATUS, PERIODO.PE_TIMBRO, PERIODO.PE_FEC_INI, PERIODO.PE_FEC_FIN, PERIODO.PE_FEC_PAG,  PE_NUM_EMP,PE_TOT_PER,PE_TOT_DED, PE_TOT_NET ' +
     ' order by PERIODO.PE_NUMERO  ';


begin
//  eStatusPeriodo = (spNueva,spPreNomina,spSinCalcular,spCalculadaParcial,spCalculadaTotal,spAfectadaParcial,spAfectadaTotal);


        with dmConsultas do
        begin
             SQLText := Format( K_QUERY_PERIODOS_AFECTADOS, [  Ord(spAfectadaParcial), FAnio,  ord( dmCliente.PeriodoTipo )  ] );

             try
                cdsQueryGeneral.Refrescar;
                cdsPeriodosAfectadosTotal.Data := cdsQueryGeneral.Data;
                cdsPeriodosAfectadosTotal.Edit;
                cdsPeriodosAfectadosTotal.DisableControls;
                with cdsPeriodosAfectadosTotal do
                begin
                     first;
                     while not Eof do
                     begin
                          Edit;
                          if StrVacio( FieldByName('Nombre').AsString ) then
                          begin

                              FieldByName('Nombre').AsString :=Format('%s #%d del %d', [  ObtieneElemento( lfTipoPeriodo,  FieldByName('Tipo').AsInteger ) , FieldByName('Numero').AsInteger, {$IFDEF DOS_CAPAS}FieldByName('Anio').AsInteger{$ELSE}FieldByName('Year').AsInteger{$ENDIF} ] );

                          end;
                          Next;
                     end;
                end;
                cdsPeriodosAfectadosTotal.EnableControls;


             except
                   on Error : Exception do
                   begin

                        DatabaseError( Format( 'Error al consultar periodos afectados: %s', [ Error.Message ] ) ) ;
                   end;
             end;
        end;
end;

procedure TdmInterfase.CambioNumeroPeriodoDesdeGridShell(
  iNumeroPeriodo: integer);
begin
      try
         TressShell.CambioNumeroPeriodoDesdeGrid(iNumeroPeriodo);
      Except
      end;
end;

function TdmInterfase.CancelarTimbradoNomina( oParamsTimbradoNomina: TZetaParams ): boolean;

 function SetParametrosNominasTimbrar : boolean;
   begin

        dmcliente.SetPeriodoNumero( dmCliente.PeriodoNumero ) ;
        dmCliente.RazonSocial := cdsRSocial.FieldByName('RS_CODIGO').AsString ;
        with FParametros do
        begin
             AddInteger( 'Year', oParamsTimbradoNomina.ParamByName('PE_YEAR').AsInteger );
             AddInteger( 'Tipo', oParamsTimbradoNomina.ParamByName('PE_TIPO').AsInteger );
             AddInteger( 'Numero', oParamsTimbradoNomina.ParamByName('PE_NUMERO').AsInteger );
             AddInteger( 'StatusAnterior', ord( estiTimbrado ) );
             AddInteger( 'StatusNuevo', ord( estiPendiente ) );
             AddString('RazonSocial' , cdsRSocial.FieldByName('RS_CODIGO').AsString  );
             AddInteger( 'ContribuyenteID',  cdsRSocial.FieldByName('RS_CONTID').AsInteger );
             AddString( 'RangoLista', VACIO );
             AddString( 'Condicion', VACIO );
             AddString( 'Filtro', VACIO );
        end ;
   end;

begin
     Result := FALSE;
     
     if RespuestaTimbramex.transaccion = tCancelarTimbrado then
     begin
          SetParametrosNominasTimbrar;

          dmProcesos.cdsDataSet := RespuestaTimbramex.Resultado.FacturasDS;
          dmProcesos.TimbrarNomina( FParametros );
          Result := TRUE;
     end;

end;

function TdmInterfase.GetPeriodoCancelar( lFiltrarEmpleados : boolean = FALSE; iTXID: integer = 0; lTXINIT: boolean = FALSE)  : string;
const
     K_TITULO_ERROR = 'Exportaci�n de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
Var
    iReporte, iReportePeriodos : integer;

     XMLTools: DXMLTools.TdmXMLTools;
     sNombre : string;
     sXML, sCONT_PASS, sUsuarioNombre : string;
     iCONT_ID  : integer;


     function GetNominasXML : string;
     var
        sl : TStringList;
     begin
          Result := VACIO;
          if ( lFiltrarEmpleados ) then
          begin
              if ( dmProcesos.cdsDatasetTimbrados = nil ) or ( dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
              begin
                   Result := VACIO;
              end
              else
              with dmProcesos.cdsDatasetTimbrados do
              begin
                   Result := VACIO;
                   sl := TStringList.Create;
                   sl.Add('<NOMINAS>');
                   First;
                   while not Eof do
                   begin
                        sl.Add( Format( '<NOMINA Numero="%d" CURP="%s" />', [ FieldByName('Empleado').AsInteger, FieldByName('CURP').AsString ] ) )  ;
                        Next;
                   end;
                   sl.Add('</NOMINAS>');
                   Result := sl.Text;
              end;
          end;
     end;

begin
     { Variables locales del procesamiento }
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;

     dmCliente.cdsTPeriodos.Refrescar;

     Result := '';
     XMLTools := TdmXMLTools.Create( nil );

       try

                 Result := '<PROCESS>' +char(10)+char(13);
                 Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger, iTXID, lTXINIT );
                 Result := Result +         '<DATA>'  +char(10)+char(13);
                 Result := Result + XMLTools.RowToXMLNodeStr( cdsPeriodosCancelar, 'PERIODOS', 'PERIODO' , 8 );
                 Result := Result + GetNominasXML;
                 Result := Result + '</PERIODO>'+char(10)+char(13);
                 Result := Result + '</DATA>'  +char(10)+char(13);
                 Result := Result + '</PROCESS>'  +char(10)+char(13);


        except on Error: Exception do

               zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
        end;

     FreeAndNil(XMLTools);
     Application.ProcessMessages;

end;

//iTipoPaquetes  = 0 No Aplica 1 Por Empleado   2 por Nivel de organigrama
function TdmInterfase.GetPeriodoRecibos( lUsoPaquetes : boolean;  var slPaquetesGroup : TStringList; iTipoPaquetes : integer  ) : TStringList;
const
     K_TITULO_ERROR = 'Exportaci�n de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
     K_MAX_PAQUETE = 30000;
Var
    iReporte, iReportePeriodos : integer;

     XMLTools: DXMLTools.TdmXMLTools;
     sNombre : string;
     sXML, sCONT_PASS, sUsuarioNombre : string;
     iCONT_ID, iPaq, iPaquetes, iPaqueteSize  : integer;



     sEncabezado, sPie, sGrupo : string;

     function GetNominasXML: string;
     var
        sl : TStringList;
     begin
          if ( dmProcesos.cdsDatasetTimbrados = nil ) or ( dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
               Result := VACIO;
          end
          else
          with dmProcesos.cdsDatasetTimbrados do
          begin
               Result := VACIO;
               sl := TStringList.Create;
               sl.Add('<NOMINAS>');
               First;
               while not Eof do
               begin
                    sl.Add( Format( '<NOMINA Numero="%d" CURP="%s" />', [ FieldByName('Empleado').AsInteger, FieldByName('CURP').AsString ] ) )  ;
                    Next;
               end;
               sl.Add('</NOMINAS>');
               Result := sl.Text;
          end;
     end;

     procedure GetNominasXML_IniciarPaquete;
     begin
          if ( dmProcesos.cdsDatasetTimbrados <> nil ) and  ( not dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
              dmProcesos.cdsDatasetTimbrados.First;
          end
     end;

     function GetNominasXML_PaquetesCount : integer;
     begin
          Result := 0;

          if ( dmProcesos.cdsDatasetTimbrados <> nil ) and  ( not dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
              Result  := ( dmProcesos.cdsDatasetTimbrados.RecordCount div GetTimbradoPaqueteSize ) + 1 ;
          end

     end;

     function GetNominasXML_Paquete( var sGrupo : string; iPaqueteSize: integer ) : string;
     var
        sl : TStringList;
        iNomina : integer;
        sGrupoRow : string;
     begin
          if ( dmProcesos.cdsDatasetTimbrados = nil ) or ( dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
               Result := VACIO;
          end
          else
          with dmProcesos.cdsDatasetTimbrados do
          begin
               Result := VACIO;
               sl := TStringList.Create;
               sl.Add('<NOMINAS>');

               iNomina := 0;
               while not Eof do
               begin
                    inc( iNomina );
                    sGrupoRow := FieldByName('Grupo').AsString ;
                    if ( iNomina >= iPaqueteSize ) or ( sGrupo <> sGrupoRow )  then
                    begin
                       sGrupo := sGrupoRow;
                       break;
                    end;
                    sl.Add( Format( '<NOMINA Numero="%d" CURP="%s" />', [ FieldByName('Empleado').AsInteger, FieldByName('CURP').AsString ] ) )  ;
                    Next;

               end;
               sl.Add('</NOMINAS>');
               Result := sl.Text;

               FreeAndNil ( sl );
          end;
     end;


     function GetNominasXML_PaqueteGrupoEmpleado( var sGrupo : string; iPaqueteSize: integer ) : string;
     var
        sl : TStringList;
        iNomina, iEmpleadoIni, iEmpleadoFin : integer;
        sGrupoRow : string;
     begin
          iEmpleadoIni := 0;
          iEmpleadoFin := 0;

          if ( dmProcesos.cdsDatasetTimbrados = nil ) or ( dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
               Result := VACIO;
          end
          else
          with dmProcesos.cdsDatasetTimbrados do
          begin
               Result := VACIO;
               sl := TStringList.Create;
               sl.Add('<NOMINAS>');

               iNomina := 0;

               if not Eof then
               begin
                  iEmpleadoIni :=  FieldByName('Empleado').AsInteger;
               end;

               while not Eof do
               begin
                    inc( iNomina );

                    if ( iNomina > iPaqueteSize )  then
                    begin
                       break;
                    end;
                    sl.Add( Format( '<NOMINA Numero="%d" CURP="%s" />', [ FieldByName('Empleado').AsInteger, FieldByName('CURP').AsString ] ) )  ;
                    iEmpleadoFin :=  FieldByName('Empleado').AsInteger;
                    Next;

               end;
               sl.Add('</NOMINAS>');
               Result := sl.Text;

               FreeAndNil ( sl );
          end;

          sGrupo := Format('%d-%d', [iEmpleadoIni, iEmpleadoFin] );

     end;



begin
     { Variables locales del procesamiento }
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;

     Result := TStringList.Create;

     slPaquetesGroup := TStringList.Create;

     dmCliente.cdsTPeriodos.Refrescar;

     XMLTools := TdmXMLTools.Create( nil );

     try

               sEncabezado := '<PROCESS>' +char(10)+char(13);
               sEncabezado := sEncabezado + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
               sEncabezado := sEncabezado +         '<DATA>'  +char(10)+char(13);
               sEncabezado := sEncabezado + XMLTools.RowToXMLNodeStr( cdsPeriodosRecibos, 'PERIODOS', 'PERIODO' , 8 );

               sPie := sPie + '</PERIODO>'+char(10)+char(13);
               sPie := sPie + '</DATA>'  +char(10)+char(13);
               sPie := sPie + '</PROCESS>'  +char(10)+char(13);

               GetNominasXML_IniciarPaquete;
               if ( dmProcesos.cdsDatasetTimbrados <>nil ) and  ( not dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
               begin
                    sGrupo := dmProcesos.cdsDatasetTimbrados.FieldByName('Grupo').AsString ;
               end
               else
                   sGrupo := VACIO;


               if ( iTipoPaquetes < 2 ) then
                  sGrupo := VACIO;

               if lUsoPaquetes then
               begin
                   iPaqueteSize := GetTimbradoPaqueteSize;
                   while ( not dmProcesos.cdsDatasetTimbrados.Eof ) do
                   begin
                        if ( iTipoPaquetes = 0 ) then
                        begin
                           slPaquetesGroup.Add( VACIO );
                           Result.Add( sEncabezado + GetNominasXML_PaqueteGrupoEmpleado(sGrupo, iPaqueteSize)  + sPie );
                        end
                        else
                        if ( iTipoPaquetes = 1 ) then
                        begin
                           Result.Add( sEncabezado + GetNominasXML_PaqueteGrupoEmpleado(sGrupo, iPaqueteSize)  + sPie );
                           slPaquetesGroup.Add( sGrupo );
                        end
                        else
                        begin
                           slPaquetesGroup.Add( sGrupo );
                           Result.Add( sEncabezado + GetNominasXML_Paquete(sGrupo, iPaqueteSize)  + sPie );
                        end;
                   end
               end
               else
               begin
                   while ( not dmProcesos.cdsDatasetTimbrados.Eof ) do
                   begin
                        if ( iTipoPaquetes = 0 ) then
                        begin
                           slPaquetesGroup.Add( VACIO );
                           Result.Add( sEncabezado + GetNominasXML_PaqueteGrupoEmpleado(sGrupo, K_MAX_PAQUETE)  + sPie );
                        end
                        else
                        if ( iTipoPaquetes = 1 ) then
                        begin
                           Result.Add( sEncabezado + GetNominasXML_PaqueteGrupoEmpleado(sGrupo, K_MAX_PAQUETE)  + sPie );
                           slPaquetesGroup.Add( sGrupo );
                        end
                        else
                        begin
                           slPaquetesGroup.Add( sGrupo );
                           Result.Add( sEncabezado + GetNominasXML_Paquete(sGrupo, K_MAX_PAQUETE)  + sPie );
                        end;


                   end
               end;

      except on Error: Exception do

             zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
      end;

     FreeAndNil(XMLTools);
     Application.ProcessMessages;


end;



function TdmInterfase.GetPeriodoRecibosXEmpleado( lUsoPaquetes : boolean;  var slPaquetesGroup : TStringList; iTipoPaquetes : integer; cdsPeriodoNominaAProcesar : TZetaClientDataset  ) : TStringList;
const
     K_TITULO_ERROR = 'Exportaci�n de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
     K_MAX_PAQUETE = 30000;
Var
    iReporte, iReportePeriodos : integer;

     XMLTools: DXMLTools.TdmXMLTools;
     sNombre : string;
     sXML, sCONT_PASS, sUsuarioNombre : string;
     iCONT_ID, iPaq, iPaquetes, iPaqueteSize  : integer;
     iNumeroNominasAProcesar : integer;



     sEncabezado, sPie, sGrupo : string;

     function GetNominasXML: string;
     var
        sl : TStringList;
     begin
          if ( dmProcesos.cdsDatasetTimbrados = nil ) or ( dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
               Result := VACIO;
          end
          else
          with dmProcesos.cdsDatasetTimbrados do
          begin
               Result := VACIO;
               sl := TStringList.Create;
               sl.Add('<NOMINAS>');
               First;
               while not Eof do
               begin
                    sl.Add( Format( '<NOMINA Numero="%d" CURP="%s" />', [ FieldByName('Empleado').AsInteger, FieldByName('CURP').AsString ] ) )  ;
                    Next;
               end;
               sl.Add('</NOMINAS>');
               Result := sl.Text;
          end;
     end;

     procedure GetNominasXML_IniciarPaquete;
     begin
          if ( dmProcesos.cdsDatasetTimbrados <> nil ) and  ( not dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
              dmProcesos.cdsDatasetTimbrados.First;
          end
     end;

     function GetNominasXML_PaquetesCount : integer;
     begin
          Result := 0;

          if ( dmProcesos.cdsDatasetTimbrados <> nil ) and  ( not dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
              Result  := ( dmProcesos.cdsDatasetTimbrados.RecordCount div GetTimbradoPaqueteSize ) + 1 ;
          end

     end;

     function GetNominasXML_Paquete( var sGrupo : string; iPaqueteSize: integer ) : string;
     var
        sl : TStringList;
        iNomina : integer;
        sGrupoRow : string;
     begin
          if ( dmProcesos.cdsDatasetTimbrados = nil ) or ( dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
               Result := VACIO;
          end
          else
          with dmProcesos.cdsDatasetTimbrados do
          begin
               Result := VACIO;
               sl := TStringList.Create;
               sl.Add('<NOMINAS>');

               iNomina := 0;
               while not Eof do
               begin
                    inc( iNomina );
                    sGrupoRow := FieldByName('Grupo').AsString ;
                    if ( iNomina >= iPaqueteSize ) or ( sGrupo <> sGrupoRow )  then
                    begin
                       sGrupo := sGrupoRow;
                       break;
                    end;
                    sl.Add( Format( '<NOMINA Numero="%d" CURP="%s" />', [ FieldByName('Empleado').AsInteger, FieldByName('CURP').AsString ] ) )  ;

                    Next;

               end;
               sl.Add('</NOMINAS>');
               Result := sl.Text;

               FreeAndNil ( sl );
          end;
     end;


     function GetNominasXML_PaqueteGrupoEmpleado( var sGrupo : string; iPaqueteSize: integer ) : string;
     var
        sl : TStringList;
        iNomina, iEmpleadoIni, iEmpleadoFin : integer;
        sGrupoRow : string;
     begin
          iEmpleadoIni := 0;
          iEmpleadoFin := 0;

          if ( dmProcesos.cdsDatasetTimbrados = nil ) or ( dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
          begin
               Result := VACIO;
          end
          else
          with dmProcesos.cdsDatasetTimbrados do
          begin
               Result := VACIO;
               sl := TStringList.Create;
               sl.Add('<NOMINAS>');

               iNomina := 0;

               if not Eof then
               begin
                  iEmpleadoIni :=  FieldByName('Empleado').AsInteger;
               end;

               while not Eof do
               begin
                    inc( iNomina );

                    if ( iNomina > iPaqueteSize )  then
                    begin
                       break;
                    end;
                    sl.Add( Format( '<NOMINA Numero="%d" CURP="%s" />', [ FieldByName('Empleado').AsInteger, FieldByName('CURP').AsString ] ) )  ;
                    iEmpleadoFin :=  FieldByName('Empleado').AsInteger;
                    Next;

               end;
               sl.Add('</NOMINAS>');
               Result := sl.Text;

               FreeAndNil ( sl );
          end;

          sGrupo := Format('%d-%d', [iEmpleadoIni, iEmpleadoFin] );

     end;



begin
     { Variables locales del procesamiento }
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;

     Result := TStringList.Create;

     slPaquetesGroup := TStringList.Create;

     dmCliente.cdsTPeriodos.Refrescar;

     XMLTools := TdmXMLTools.Create( nil );

     try
               with cdsPeriodosRecibos do
               begin
                    //FieldByName('Numero').AsInteger := iNumeroPeriodo;
               end;
               sEncabezado := '<PROCESS>' +char(10)+char(13);
               sEncabezado := sEncabezado + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
               sEncabezado := sEncabezado +         '<DATA>'  +char(10)+char(13);
               sEncabezado := sEncabezado + XMLTools.RowToXMLNodeStr( cdsPeriodoNominaAProcesar, 'PERIODOS', 'PERIODO' , 8 );

               sPie := sPie + '</PERIODO>'+char(10)+char(13);
               sPie := sPie + '</DATA>'  +char(10)+char(13);
               sPie := sPie + '</PROCESS>'  +char(10)+char(13);

               GetNominasXML_IniciarPaquete;
               if ( dmProcesos.cdsDatasetTimbrados <>nil ) and  ( not dmProcesos.cdsDatasetTimbrados.IsEmpty ) then
               begin
                    sGrupo := dmProcesos.cdsDatasetTimbrados.FieldByName('Grupo').AsString ;
               end
               else
                   sGrupo := VACIO;

               iNumeroNominasAProcesar := dmProcesos.cdsDatasetTimbrados.RecordCount;

               if ( iTipoPaquetes < 2 ) then
                  sGrupo := VACIO;

               if lUsoPaquetes then
               begin
                   iPaqueteSize := GetTimbradoPaqueteSize;
                   while ( not dmProcesos.cdsDatasetTimbrados.Eof ) do
                   begin
                        if ( iTipoPaquetes = 0 ) then
                        begin
                           slPaquetesGroup.Add( VACIO );
                           Result.Add( sEncabezado + GetNominasXML_PaqueteGrupoEmpleado(sGrupo, iPaqueteSize)  + sPie );
                        end
                        else
                        if ( iTipoPaquetes = 1 ) then
                        begin
                           Result.Add( sEncabezado + GetNominasXML_PaqueteGrupoEmpleado(sGrupo, iPaqueteSize)  + sPie );
                           slPaquetesGroup.Add( sGrupo );
                        end
                        else
                        begin
                           slPaquetesGroup.Add( sGrupo );
                           Result.Add( sEncabezado + GetNominasXML_Paquete(sGrupo, iPaqueteSize)  + sPie );
                        end;
                   end
               end
               else
               begin
                   while ( not dmProcesos.cdsDatasetTimbrados.Eof ) do
                   begin
                        if ( iTipoPaquetes = 0 ) then
                        begin
                           slPaquetesGroup.Add( VACIO );
                           Result.Add( sEncabezado + GetNominasXML_PaqueteGrupoEmpleado(sGrupo, K_MAX_PAQUETE)  + sPie );
                        end
                        else
                        if ( iTipoPaquetes = 1 ) then
                        begin
                           Result.Add( sEncabezado + GetNominasXML_PaqueteGrupoEmpleado(sGrupo, K_MAX_PAQUETE)  + sPie );
                           slPaquetesGroup.Add( sGrupo );
                        end
                        else
                        begin
                           slPaquetesGroup.Add( sGrupo );
                           Result.Add( sEncabezado + GetNominasXML_Paquete(sGrupo, K_MAX_PAQUETE)  + sPie );
                        end;


                   end
               end;

      except on Error: Exception do

             zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
      end;

     FreeAndNil(XMLTools);
     Application.ProcessMessages;


end;


function TdmInterfase.AplicarPatchTimbradoComparte: Boolean;
const
     K_ERROR_PATCH_TIMBRADO_COMPARTE = 'Autopatch Comparte';
begin
     try
        Result:= ServerSistemaTimbrado.CrearEstructurasTimbradoComparte;
     except on Error: Exception do
       begin
            Result := False;
            zError( K_ERROR_PATCH_TIMBRADO_COMPARTE, Error.Message, 0);
       end;
     end;

end;

function TdmInterfase.AplicarPatchTimbradoDatos: Boolean;
const
     K_ERROR_PATCH_TIMBRADO_DATOS = 'Autopatch Datos';
begin
     try
        Result := ServerSistemaTimbrado.CrearEstructurasTimbradoEmpresa(dmCliente.Empresa)
     except on Error: Exception do
        begin
            Result := False;
            zError( K_ERROR_PATCH_TIMBRADO_DATOS, Error.Message, 0);
        end; 
     end;
end;


procedure TdmInterfase.cdsPeriodosTimbrarAlAdquirirDatos(Sender: TObject);
begin
     //cdsPeriodosTimbrar
end;

procedure TdmInterfase.cdsPeriodosXMesYearAlAdquirirDatos(Sender: TObject);
begin
     cdsPeriodosXMesYear.Data:= ServerNominaTimbrado.ObtenerPeriodosAConciliarTimbrado(dmCliente.Empresa,dmCliente.AnioConciliacionTimbrado, dmCliente.MesConciliacionTimbrado, dmCliente.RazonSocialConciliacionTimbrado);
end;

procedure TdmInterfase.cdsPeriodosXMesYearAlCrearCampos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender )  do
     begin
          ListaFija( 'PE_TIPO', lfTipoPeriodo );
          ListaFija( 'PE_TIMBRO', lfStatusTimbrado );
          with FieldByName( 'NOMBRE_PERIODO' ) do
          begin
               OnGetText :=  PERIODO_NOMBREGetText;
               Alignment:= taLeftJustify;
          end;
          MaskTasa('PORC_CONCILIADOS');
     end;
end;

procedure TdmInterfase.PERIODO_NOMBREGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.DataSet.IsEmpty then
         Text := VACIO
     else
     begin
          if DisplayText then
          begin
               Sender.Alignment := taLeftJustify;
               with cdsPeriodosXMesYear do
               begin
                    if IsEmpty then
                       Text := VACIO
                    else
                    begin
                         Text := Format('%s #%d del %d', [  ObtieneElemento( lfTipoPeriodo,  Sender.DataSet.FieldByName('PE_TIPO').AsInteger ) , Sender.DataSet.FieldByName('PE_NUMERO').AsInteger, Sender.DataSet.FieldByName('PE_YEAR').AsInteger ] );
                    end;
               end;
          end
          else
              Text:= Sender.AsString;
     end;
end;

procedure TdmInterfase.cdsPeriodosXMesYearAProcesarAlCrearCampos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender )  do
     begin
          ListaFija( 'PE_TIPO', lfTipoPeriodo );
          ListaFija( 'PE_TIMBRO', lfStatusTimbrado );
          with FieldByName( 'NOMBRE_PERIODO' ) do
          begin
               OnGetText :=  PERIODO_NOMBREGetText;
               Alignment:= taLeftJustify;
          end;
          MaskTasa('PORC_CONCILIADOS');
     end;
end;

function TdmInterfase.GetRecibosList(Parametros:TZetaParams; ParametrosPeriodicidad : TZetaParams; lIncluirDetalle : boolean; sDescripcion : string; lIncluirIncapacidades, lIncluirTiempoExtra, lIncluirSubContratos, FAjustaPago : boolean) : TStringList;
const
     K_TITULO_ERROR = 'Exportaci�n de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
     K_NO_GENERO_REPORTE_VALIDACION = 'No fue posible generar la informaci�n hacia el Sistema de Timbrado debido a que el campo ';
     K_TIMBRAMEX_OTRA = 99;
     K_TIMBRAMEX_OTRA_POSICION = 11;
Var
    iReporte, iReportePeriodos : integer;

     sCampoValidacion:string;

     XMLTools: DXMLTools.TdmXMLTools;
     sNombre : string;
     sXML, sCONT_PASS, sUsuarioNombre, sPaquete : string;
     iCONT_ID  : integer;
     iCuantos, iPaquetes, iPaq : integer;
     K_PAQUETES : integer;
     iEmpleadoIni , iEmpleadoFin : integer;
     sAtributosExtra : string;


     function dwGetFieldName( ds : TZetaClientDataSet; sDisplayLab  : string ) : string ;
     var
          iField, nFields : integer;
          sDsDisplayLab : string;
     begin
           Result := VACIO;
           nFields := ds.FieldCount;

           for iField := 0 to nFields -1 do
           begin
                sDsDisplayLab := UpperCase(ds.Fields[iField].DisplayLabel ) ;

                if ( sDisplayLab  = sDsDisplayLab ) then
                begin
                    Result  :=  ds.Fields[iField].FieldName;
                    break;
                end;
           end;


     end;

     function GetXMLMontos( iEmpleadoIni , iEmpleadoFin : integer )  : string ;
     var
        sFieldEmpleado : string;
        sFieldTipoSAT : string;
     begin
           sCampoValidacion := 'Numero no est� definido en el reporte ' +K_REPORTE_TIMBRADO_DETALLE;
           sFieldEmpleado :=  dwGetFieldName( cdsRecibosDetalleTimbrar, 'NUMERO' ) ;
           sCampoValidacion := 'Clase no est� definido en el reporte ' +K_REPORTE_TIMBRADO_DETALLE;
           sFieldTipoSAT :=  dwGetFieldName( cdsRecibosDetalleTimbrar, 'CLASE' ) ;

           cdsRecibosDetalleTimbrar.First;
           if ( iEmpleadoFin > 0 ) then
              cdsRecibosDetalleTimbrar.Filter := Format( '(%s > 0) and (%s >=%d ) and (%s < %d)', [sFieldTipoSAT, sFieldEmpleado, iEmpleadoIni, sFieldEmpleado, iEmpleadoFin] )
           else
              cdsRecibosDetalleTimbrar.Filter := Format( '(%s > 0) and (%s >=%d )', [sFieldTipoSAT, sFieldEmpleado, iEmpleadoIni] ) ;

           cdsRecibosDetalleTimbrar.Filtered := TRUE;

           Result  := GeneraXMLFromDataset( cdsRecibosDetalleTimbrar, 'MONTOS', 'MONTO' );
           Result  := stringReplace( Result, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '', []);
           Result := stringReplace( Result, '<?xml version="1.0" standalone="yes"?>', '', []);

           cdsRecibosDetalleTimbrar.Filtered := FALSE;
           cdsRecibosDetalleTimbrar.Filter := vacio;


     end;


     function GetXMLIncapacidades( iEmpleadoIni , iEmpleadoFin : integer )  : string ;
     var
        sFieldEmpleado : string;
        slForceIntAttributes : TStringList ;
     begin
          if ( cdsRecibosIncapacidades.Active ) and ( not  cdsRecibosIncapacidades.IsEmpty ) then
          begin
              sCampoValidacion := 'Numero no est� definido en el reporte ' +K_REPORTE_TIMBRADO_DETALLE_INCAPACIDADES;
              sFieldEmpleado :=  dwGetFieldName( cdsRecibosIncapacidades, 'NUMERO' ) ;
              cdsRecibosIncapacidades.First;
              if ( iEmpleadoFin > 0 ) then
                 cdsRecibosIncapacidades.Filter := Format( '(%s >=%d ) and (%s < %d)', [sFieldEmpleado, iEmpleadoIni, sFieldEmpleado, iEmpleadoFin] )
              else
                 cdsRecibosIncapacidades.Filter := Format( '(%s >=%d )', [sFieldEmpleado, iEmpleadoIni] ) ;

              cdsRecibosIncapacidades.Filtered := TRUE;
              slForceIntAttributes := TStringList.Create;
              slForceIntAttributes.Add( 'Tipo' );
              slForceIntAttributes.Add( 'Dias' );
              slForceIntAttributes.Add( 'Numero' );
              Result  := GeneraXMLFromDatasetForceInt( cdsRecibosIncapacidades, 'INCAPACIDADES', 'INCAPACIDAD' , slForceIntAttributes);
              Result  := stringReplace( Result, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
              Result := stringReplace( Result, '<?xml version="1.0" standalone="yes"?>', '', []);
              cdsRecibosIncapacidades.Filtered := FALSE;
              cdsRecibosIncapacidades.Filter := vacio;
              FreeAndNil( slForceIntAttributes );
          end;
     end;

     function GetXMLTiempoExtra( iEmpleadoIni , iEmpleadoFin : integer )  : string ;
     var
        sFieldEmpleado : string;
        slForceIntAttributes : TStringList ;
     begin
          if ( cdsRecibosTiempoExtra.Active ) and ( not  cdsRecibosTiempoExtra.IsEmpty ) then
          begin
              sCampoValidacion := 'Numero no est� definido en el reporte ' +K_REPORTE_TIMBRADO_DETALLE_TIEMPO_EXTRA;
              sFieldEmpleado :=  dwGetFieldName( cdsRecibosTiempoExtra, 'NUMERO' ) ;

              cdsRecibosTiempoExtra.First;
              if ( iEmpleadoFin > 0 ) then
                 cdsRecibosTiempoExtra.Filter := Format( '(%s >=%d ) and (%s < %d)', [sFieldEmpleado, iEmpleadoIni, sFieldEmpleado, iEmpleadoFin] )
              else
                 cdsRecibosTiempoExtra.Filter := Format( '(%s >=%d )', [sFieldEmpleado, iEmpleadoIni] ) ;

              cdsRecibosTiempoExtra.Filtered := TRUE;

              slForceIntAttributes := TStringList.Create;
              slForceIntAttributes.Add( 'DiasDoble' );
              slForceIntAttributes.Add( 'HorasDoble' );
              slForceIntAttributes.Add( 'DiasTriple' );
              slForceIntAttributes.Add( 'HorasTriple' );
              slForceIntAttributes.Add( 'Numero' );


              Result  := GeneraXMLFromDatasetForceInt( cdsRecibosTiempoExtra, 'EXTRAS', 'EXTRA' , slForceIntAttributes);
              Result  := stringReplace( Result, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
              Result := stringReplace( Result, '<?xml version="1.0" standalone="yes"?>', '', []);

              cdsRecibosTiempoExtra.Filtered := FALSE;
              cdsRecibosTiempoExtra.Filter := vacio;
              FreeAndNil( slForceIntAttributes );
          end;
     end;

     {*** Detalle de SubContratos ***}
     function GetXMLSubContratos( iEmpleadoIni , iEmpleadoFin : integer )  : string ;
     var
        sFieldEmpleado : string;
        slForceIntAttributes : TStringList ;
     begin
          if ( cdsRecibosSubContratos.Active ) and ( not  cdsRecibosSubContratos.IsEmpty ) then
          begin
              sCampoValidacion := 'Numero no est� definido en el reporte ' +K_REPORTE_TIMBRADO_DETALLE_SUB_CONTRATOS;
              sFieldEmpleado :=  dwGetFieldName( cdsRecibosSubContratos, 'NUMERO' ) ;

              cdsRecibosSubContratos.First;
              if ( iEmpleadoFin > 0 ) then
                 cdsRecibosSubContratos.Filter := Format( '(%s >=%d ) and (%s < %d)', [sFieldEmpleado, iEmpleadoIni, sFieldEmpleado, iEmpleadoFin] )
              else
                 cdsRecibosSubContratos.Filter := Format( '(%s >=%d )', [sFieldEmpleado, iEmpleadoIni] ) ;

              cdsRecibosSubContratos.Filtered := TRUE;

              slForceIntAttributes := TStringList.Create;
              slForceIntAttributes.Add( 'Numero' );
              slForceIntAttributes.Add( 'RFC' );
              slForceIntAttributes.Add( 'Tiempo' );


              Result  := GeneraXMLFromDatasetForceInt( cdsRecibosSubContratos, 'SUBCONTRATOS', 'SUBCONTRATO' , slForceIntAttributes);
              Result  := stringReplace( Result, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', []);
              Result := stringReplace( Result, '<?xml version="1.0" standalone="yes"?>', '', []);

              cdsRecibosSubContratos.Filtered := FALSE;
              cdsRecibosSubContratos.Filter := vacio;
              FreeAndNil( slForceIntAttributes );
          end;
     end;
     {*** FIN ***}

     function GetAtributosExtra : string;
     begin
          Result := VACIO;

          if ( lIncluirDetalle ) then
             Result := ' Desglosar="1"' ;

          if StrLleno( sDescripcion ) then
             Result := Result + Format( ' Descrip="%s" ', [ HTMLEncode(sDescripcion)])  ;

          if  (Parametros.IndexOf('OriginalNumero') >= 0) and
              (Parametros.IndexOf('OriginalTipo') >= 0) and
              (Parametros.IndexOf('OriginalYear') >= 0)  then
          begin
               Result := Result + Format( ' OriginalYear="%d" OriginalTipo="%d"  OriginalNumero="%d" ',
                      [Parametros.ParamByName('OriginalYear').AsInteger,
                      Parametros.ParamByName('OriginalTipo').AsInteger,
                       Parametros.ParamByName('OriginalNumero').AsInteger
                        ] );
          end;

     end;


begin
     { Variables locales del procesamiento }
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;
     iCuantos := cdsRecibosTimbrar.RecordCount;

     K_PAQUETES :=  GetTimbradoPaqueteSize;

     iPaquetes := iCuantos div K_PAQUETES;

     Result := TStringList.Create;

     dmCliente.cdsTPeriodos.Refrescar;

     cdsPeriodosTimbrar.First;
     cdsRecibosTimbrar.First;

     sAtributosExtra :=  GetAtributosExtra;

     for iPaq := 0 to iPaquetes  do
     begin
          XMLTools := TdmXMLTools.Create( nil );
        try
           if ( not cdsPeriodosTimbrar.EOF ) then
           begin

                  sCampoValidacion := 'Numero no est� definido en el reporte ' +K_REPORTE_TIMBRADO ;
                  iEmpleadoIni := cdsRecibosTimbrar.FieldByName( dwGetFieldName(cdsRecibosTimbrar, 'NUMERO' ) ).AsInteger;

                  { Pruebas de Particion }
                  {$ifdef PRUEBAS_PARTICION}
                  cdsPeriodosTimbrar.Edit;

                  cdsPeriodosTimbrar.FieldByName( 'Pago' ).AsDateTime :=  cdsPeriodosTimbrar.FieldByName( 'Pago' ).AsDateTime  + iPaq;
                  cdsPeriodosTimbrar.Post;
                  {$endif}
                  if(FAjustaPago)
                  then
                  begin
                        cdsPeriodosTimbrar.Edit;
                        cdsPeriodosTimbrar.FieldByName( 'Pago' ).AsDateTime :=  Parametros.ParamByName('FechaAjuste').AsDateTime;
                        cdsPeriodosTimbrar.Post;
                  end;
                  { Terminaci�n de Pruebas }

                  {*** US 14237: El usuario requiere especificar la periodicidad al momento de timbrar para as� poder incluir los nuevos tipos que el SAT propone para el CFDI 1.2 ***}
                  cdsPeriodosTimbrar.Edit;
                  if ParametrosPeriodicidad.ParamByName('PeriodicidadNomina').AsInteger = K_TIMBRAMEX_OTRA_POSICION then
                     cdsPeriodosTimbrar.FieldByName( 'Frecuencia' ).AsInteger := K_TIMBRAMEX_OTRA
                  else
                      cdsPeriodosTimbrar.FieldByName( 'Frecuencia' ).AsInteger := ParametrosPeriodicidad.ParamByName('PeriodicidadNomina').AsInteger + 1;
                  cdsPeriodosTimbrar.Post;
                   {*** FIN ***}

                  sPaquete := '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+char(10)+char(13);
                  sPaquete := sPaquete + '<PROCESS>' +char(10)+char(13);
                  sPaquete := sPaquete + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                  sPaquete := sPaquete +         '<DATA>'  +char(10)+char(13);
                  sPaquete := sPaquete + XMLTools.RowToXMLNodeStr( cdsPeriodosTimbrar, 'PERIODOS', 'PERIODO' , K_MAX_CAMPOS_PERIODO  ,sAtributosExtra);
                  {$ifdef PROFILE}WSProfile('GetRecibosPaquete', sPaquete); {$endif}
                  sXML := GeneraXMLFromDatasetLimit( cdsRecibosTimbrar, 'NOMINAS', 'NOMINA', K_PAQUETES, iPaq*K_PAQUETES);
                  {$ifdef PROFILE}WSProfile('GetRecibosPaquete', sXML); {$endif}
                  sXML := stringReplace( sXML, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '', []);
                  sXML := stringReplace( sXML, '<?xml version="1.0" standalone="yes"?>', '', []);
                  sPaquete := sPaquete + sXML;

                  {$ifdef PROFILE}WSProfile('GetRecibosPaquete', sXML); {$endif}

                  if  (cdsRecibosTimbrar.EOF) then
                      iEmpleadoFin := 0
                  else
                  begin
                       sCampoValidacion := 'Numero no est� definido en el reporte ' +K_REPORTE_TIMBRADO;
                       iEmpleadoFin := cdsRecibosTimbrar.FieldByName( dwGetFieldName(cdsRecibosTimbrar, 'NUMERO' ) ).AsInteger;
                  end;

                  if ( lIncluirDetalle ) then
                     sPaquete := sPaquete + GetXMLMontos( iEmpleadoIni, iEmpleadoFin ) ;

                  {$ifdef PROFILE}WSProfile('GetRecibosPaquete', sPaquete); {$endif}
                  if ( lIncluirIncapacidades ) then
                     sPaquete := sPaquete + GetXMLIncapacidades( iEmpleadoIni, iEmpleadoFin ) ;

                 {$ifdef PROFILE}WSProfile('GetRecibosPaquete', sPaquete); {$endif}
                  if ( lIncluirTiempoExtra ) then
                     sPaquete := sPaquete + GetXMLTiempoExtra( iEmpleadoIni, iEmpleadoFin ) ;

                  {$ifdef PROFILE}WSProfile('GetRecibosPaquete', sPaquete); {$endif}
                  if ( lIncluirSubContratos ) then
                     sPaquete := sPaquete + GetXMLSubContratos( iEmpleadoIni, iEmpleadoFin ) ;

                 {$ifdef PROFILE}WSProfile('GetRecibosPaquete', sPaquete); {$endif}
                  sPaquete := sPaquete + '</PERIODO>'+char(10)+char(13);
                  sPaquete := sPaquete + '</DATA>'  +char(10)+char(13);
                  sPaquete := sPaquete + '</PROCESS>'  +char(10)+char(13);

                  {$ifdef PROFILE}WSProfile('GetRecibosPaquete', sPaquete); {$endif}

                  Result.Add( sPaquete );
           end;
         except on Error: Exception do
         begin
                if AnsiContainsText(Error.Message, 'Field '''' not found') then
                begin
                     zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE_VALIDACION+sCampoValidacion, 0);
                     break;
                end
                else
                begin
                      zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
                end;
         end;
         end;
         FreeAndNil(XMLTools);
         Application.ProcessMessages;
     end;

     {$ifdef PROFILE}
     Result.SaveToFile( 'Peticiones.txt' );
     {$endif}

end;

//INI - ListadoEmpleadosXML
function TdmInterfase.GetRecibosTodosEmpleadosList( Parametros:TZetaParams; ParametrosPeriodicidad : TZetaParams ): WideString;
const
     K_TITULO_ERROR = 'Exportaci�n de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
     K_NO_GENERO_REPORTE_VALIDACION = 'No fue posible generar la informaci�n hacia el Sistema de Timbrado debido a que el campo ';
     K_TIMBRAMEX_OTRA = 99;
     K_TIMBRAMEX_OTRA_POSICION = 11;
Var
    iReporte, iReportePeriodos : integer;

     sCampoValidacion:string;

     XMLTools: DXMLTools.TdmXMLTools;
     sNombre : string;
     sXML, sCONT_PASS, sUsuarioNombre, sPaquete : string;
     sAtributosExtra : string;

     function GetAtributosExtra : string;
     begin
          Result := VACIO;
          if  (Parametros.IndexOf('OriginalNumero') >= 0) and
              (Parametros.IndexOf('OriginalTipo') >= 0) and
              (Parametros.IndexOf('OriginalYear') >= 0)  then
          begin
               Result := Result + Format( ' OriginalYear="%d" OriginalTipo="%d"  OriginalNumero="%d" ',
                      [Parametros.ParamByName('OriginalYear').AsInteger,
                      Parametros.ParamByName('OriginalTipo').AsInteger,
                       Parametros.ParamByName('OriginalNumero').AsInteger
                        ] );
          end;

     end;

     function dwGetFieldName( ds : TZetaClientDataSet; sDisplayLab  : string ) : string ;
     var
          iField, nFields : integer;
          sDsDisplayLab : string;
     begin
           Result := VACIO;
           nFields := ds.FieldCount;

           for iField := 0 to nFields -1 do
           begin
                sDsDisplayLab := UpperCase(ds.Fields[iField].DisplayLabel ) ;

                if ( sDisplayLab  = sDsDisplayLab ) then
                begin
                    Result  :=  ds.Fields[iField].FieldName;
                    break;
                end;
           end;


     end;


begin
     { Variables locales del procesamiento }
     Result := VACIO;

     dmCliente.cdsTPeriodos.Refrescar;
     cdsPeriodosTimbrar.First;
     cdsRecibosTimbrar.First;

     sAtributosExtra :=  GetAtributosExtra;

     XMLTools := TdmXMLTools.Create( nil );
     try
        if ( not cdsPeriodosTimbrar.EOF ) then
        begin
             sCampoValidacion := 'Numero no est� definido en el reporte ' +K_REPORTE_TIMBRADO ;
             {*** US 14237: El usuario requiere especificar la periodicidad al momento de timbrar para as� poder incluir los nuevos tipos que el SAT propone para el CFDI 1.2 ***}
             cdsPeriodosTimbrar.Edit;
             if ParametrosPeriodicidad.ParamByName('PeriodicidadNomina').AsInteger = K_TIMBRAMEX_OTRA_POSICION then
                cdsPeriodosTimbrar.FieldByName( 'Frecuencia' ).AsInteger := K_TIMBRAMEX_OTRA
             else
                 cdsPeriodosTimbrar.FieldByName( 'Frecuencia' ).AsInteger := ParametrosPeriodicidad.ParamByName('PeriodicidadNomina').AsInteger + 1;
             cdsPeriodosTimbrar.Post;
             {*** FIN ***}

             sPaquete := '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+char(10)+char(13);
             sPaquete := sPaquete + '<PROCESS>' +char(10)+char(13);
             sPaquete := sPaquete + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
             sPaquete := sPaquete +         '<DATA>'  +char(10)+char(13);
             sPaquete := sPaquete + XMLTools.RowToXMLNodeStr( cdsPeriodosTimbrar, 'PERIODOS', 'PERIODO' , K_MAX_CAMPOS_PERIODO  ,sAtributosExtra);
             sXML := GeneraXMLFromDatasetLimit( cdsRecibosTimbrar, 'NOMINAS', 'NOMINA', cdsRecibosTimbrar.RecordCount, -1);
             sXML := stringReplace( sXML, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '', []);
             sXML := stringReplace( sXML, '<?xml version="1.0" standalone="yes"?>', '', []);
             sPaquete := sPaquete + sXML;

             if  not (cdsRecibosTimbrar.EOF) then
             begin
                  sCampoValidacion := 'Numero no est� definido en el reporte ' +K_REPORTE_TIMBRADO;
             end;
             sPaquete := sPaquete + '</PERIODO>'+char(10)+char(13);
             sPaquete := sPaquete + '</DATA>'  +char(10)+char(13);
             sPaquete := sPaquete + '</PROCESS>'  +char(10)+char(13);
             Result := sPaquete;
        end;
     except on Error: Exception do
            begin
                 if AnsiContainsText(Error.Message, 'Field '''' not found') then
                 begin
                      zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE_VALIDACION+sCampoValidacion, 0);
                 end
                 else
                 begin
                       zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
                 end;
            end;
     end;
     FreeAndNil(XMLTools);
     Application.ProcessMessages;
     //end;
end;


//FIN


procedure TdmInterfase.cdsRecibosTimbrarAlCrearCampos(Sender: TObject);
begin
     with cdsRecibosTimbrar do
     begin
          MaskPesos( 'COLUMNA2' );
     end;
end;

procedure TdmInterfase.EscribeBitacoraTRESS(sError, sDetalle: string);
const
     K_ENCABEZADO_ERROR  = 'Timbrado - ';
var
   Parametros : TZetaParams;
begin
     try
        Parametros := TZetaParams.Create(Self);
        Parametros.AddString('Titulo', K_ENCABEZADO_ERROR + sError );
        Parametros.AddString('Descripcion', LimpiaCuentaPass( sDetalle)  );
        ServerSistemaTimbrado.CrearBitacora(dmCliente.Empresa,2,Parametros.VarValues);
     finally
            Parametros.Free;
     end;
end;
procedure TdmInterfase.EscribeBitacoraTRESSExito(sMensaje, sDetalle: string);
const
     K_ENCABEZADO_ERROR  = 'Timbrado - ';
var
   Parametros : TZetaParams;
begin
     try
        Parametros := TZetaParams.Create(Self);
        Parametros.AddString('Titulo', K_ENCABEZADO_ERROR + sMensaje );
        Parametros.AddString('Descripcion', LimpiaCuentaPass( sDetalle)  );
        ServerSistemaTimbrado.CrearBitacora(dmCliente.Empresa,0,Parametros.VarValues);
     finally
            Parametros.Free;
     end;
end;

function TdmInterfase.GetRegimenFiscal : String;
const
 K_REGIMEN_GENERAL_CFDI = '1';   // 600, 1, 'General de Ley Personas Morales'
var
   iContribuyenteID : integer;
begin
     if ( cdsRSocial.isEmpty ) then
        Result := K_REGIMEN_GENERAL_CFDI  //Codigo  "defensivo"
     else
     begin
        iContribuyenteID := cdsRSocial.FieldByName('RS_CONTID').AsInteger;

        if ( iContribuyenteID = 0  ) then
        begin
             Result := K_REGIMEN_GENERAL_CFDI;
        end
        else
        begin
             SetCuentaTimbradoFromRS;
             //Se pone vac�o para que el Wizard Consulta el valor
             Result := VACIO;
        end;
     end;
end;

function TdmInterfase.SetCuentaTimbradoFromRS: boolean;
var
   sCT_CODIGO : string;
begin
     Result := FALSE;

     if  not ( cdsRSocial.isEmpty ) then
     begin
        sCT_CODIGO := cdsRSocial.FieldByName('CT_CODIGO').AsString;

        if strLleno( sCT_CODIGO ) then
        begin
             if ( cdsCuentasTimbramex.Locate('CT_CODIGO', sCT_CODIGO, [])  ) then
             begin
                  Result := TRUE;
             end;
        end;
     end;
end;

function TdmInterfase.GetPeticionRegimenFiscal : String;


{  function GetDatosRegimenFiscalContribuyente : string;
  begin
       Result := VACIO;
       if ( not  cdsRSocial.IsEmpty) then
       begin
           Result := Result + '<EmpresaRFC> ' + HTMLEncode( cdsRSocial.FieldByName('RS_RFC').AsString) +  '</EmpresaRFC> '+
                              '<EmpresaRazon>' + HTMLEncode( cdsRSocial.FieldByName('RS_NOMBRE').AsString) +  '</EmpresaRazon>  '+
                              '<EmpresaRepLegal>' + HTMLEncode( cdsRSocial.FieldByName('RS_RLEGAL').AsString )+  '</EmpresaRepLegal> '+
                              '<EmpresaFirma>' + HTMLEncode( sEmpresaFirma ) +  '</EmpresaFirma> ';
       end;
  end;}

begin
  Result := '<?xml version="1.0" encoding="iso-8859-1" ?>';
  Result := Result + '<PROCESS> ';
  Result := Result + GetCredenciales( cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger   ) ;
  //Result := Result + '<DATA>' + GetDatosManifiestoContribuyente + '</DATA>' ;
  Result := Result + '</PROCESS> ';


end;


function TdmInterfase.GetContribuyenteActualiza(
  sRegimenFiscalDescrip: string; sRegimenFiscalCodigo : string): string;
begin
     Result := GetContribuyente('','', '', '', sRegimenFiscalDescrip, sRegimenFiscalCodigo, TRUE);
end;

function TdmInterfase.GetPeticionEnvioRecibos: string;
const
     K_TITULO_ERROR = 'Env�o de Recibos';
     K_NO_GENERO_REPORTE = 'No hay informaci�n respecto a las n�minas seleccionadas ';
var
     XMLTools: DXMLTools.TdmXMLTools;
begin
     Result := '';
     XMLTools := TdmXMLTools.Create( nil );

       try
                 Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
                 Result := Result + '<PROCESS>' +char(10)+char(13);
                 Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
                 Result := Result +         '<DATA>'  +char(10)+char(13);
                 Result := Result + XMLTools.RowToXMLNodeStr( cdsPeriodosRecibos, 'PERIODOS', 'PERIODO' , 8 );
                 Result := Result + '</PERIODO>'+char(10)+char(13);
                 Result := Result + '</DATA>'  +char(10)+char(13);
                 Result := Result + '</PROCESS>'  +char(10)+char(13);


        except on Error: Exception do

               zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
        end;

     FreeAndNil(XMLTools);
     Application.ProcessMessages;
end;                         //



function TdmInterfase.ConsultarSaldo( sRS_CODIGO : string; dFecIni, dFecFin : TDatetime): String;
var
  respuestaSaldo : TRespuestaSaldo;


  function SetRazonSocial( sRS_CODIGO : string ) : boolean;
  var
     sCT_CODIGO : string;
  begin
     Result := FALSE;

     if  not ( cdsRSocial.isEmpty ) then
     begin
        if ( cdsRSocial.Locate('RS_CODIGO', sRS_CODIGO, [])  ) then
        begin
           sCT_CODIGO := cdsRSocial.FieldByName('CT_CODIGO').AsString;
           if strLleno( sCT_CODIGO ) then
           begin
                if ( cdsCuentasTimbramex.Locate('CT_CODIGO', sCT_CODIGO, [])  ) then
                begin
                     Result := TRUE;
                end;
           end;
        end;
     end;
  end;

begin

   if cdsSaldoTimbrado.Active then
      cdsSaldoTimbrado.EmptyDataSet;

   if cdsSaldoTimbradoDetalle.Active then
      cdsSaldoTimbradoDetalle.EmptyDataSet;


   if SetRazonSocial(sRS_CODIGO) then
   begin
        respuestaSaldo :=  ConsultarSaldoServer(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger, dFecIni, dFecFin );
        if respuestaSaldo.lHayRespuesta then
        begin
            cdsSaldoTimbrado.Data := respuestaSaldo.SaldoDS.Data;
            cdsSaldoTimbradoDetalle.Data := respuestaSaldo.DetalleDS.Data;
        end
        else
        begin
             zError( 'Estado de Cuenta de Timbrado', respuestaSaldo.sMensaje, 0);
        end;
   end;

end;

procedure TdmInterfase.cdsSaldoTimbradoAlCrearCampos(Sender: TObject);
begin
     with cdsSaldoTimbrado do
     begin
          MaskNumerico('SALDO_INI', '#,0;-#,0');
          MaskNumerico('CARGOS', '#,0;-#,0');
          MaskNumerico('ABONOS', '#,0;-#,0');
          MaskNumerico('SALDO_FIN', '#,0;-#,0');
     end;
end;

procedure TdmInterfase.cdsSaldoTimbradoDetalleAlCrearCampos(
  Sender: TObject);
begin
     with cdsSaldoTimbradoDetalle do
     begin
          MaskNumerico('Cargo', '#,0;-#,0');
          MaskNumerico('Abono', '#,0;-#,0');
          MaskNumerico('Saldo', '#,0;-#,0');
          MaskFecha('FechaHora');
     end;
end;

procedure TdmInterfase.cdsCuentasTimbramexBeforeDelete(DataSet: TDataSet);
begin
//
end;

procedure TdmInterfase.cdsCuentasTimbramexAlBorrar(Sender: TObject);
begin
     if ( GetCountContribuyentes > 0 ) then
        ZetaDialogo.ZWarning('Cuenta de Timbrado', 'No es posible borrar, existen contribuyentes registrados a esta cuenta en el Sistema de Timbrado.'  , 0  , mbOK )
     else
         if ZetaMsgDlg.ConfirmaCambio( '�Desea borrar este registro de Cuenta de Timbrado? ' ) then
         begin
              cdsCuentasTimbramex.Delete;
         end;
end;

function TdmInterfase.GetCountContribuyentes: integer;
const
     K_GET_COUNT_CONTRIBUYENTES = 'select count(*) as CUANTOS from RSOCIAL where CT_CODIGO = ''%s'' and RS_CONTID > 0  ';
var
   sCT_CODIGO : string;
begin
     Result := 0;
     with dmConsultas do
     begin
          sCT_CODIGO := VACIO;
          if  not ( cdsCuentasTimbramex.isEmpty ) then
               sCT_CODIGO := cdsCuentasTimbramex.FieldByName('CT_CODIGO').AsString;

          SQLText := Format( K_GET_COUNT_CONTRIBUYENTES , [sCT_CODIGO ] ) ;
          try
             cdsQueryGeneral.Refrescar;
             if ( cdsQueryGeneral.RecordCount > 0 ) then
                result := cdsQueryGeneral.FieldByName( 'CUANTOS' ).AsInteger;
          except
                on Error : Exception do
                begin
                     Result := 9999;
                end;
          end;
     end;
end;

function TdmInterfase.GetCountTimbradasPorContribuyente: integer;
const
     K_GET_COUNT_TIMBRADAS  = 'select count(*) as CUANTOS  from NOMINA ' +
                              ' join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON ' +
                              ' join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO ' +
                              ' where NO_TIMBRO = %d and RSOCIAl.RS_CONTID = %d ';
var
   iRS_CONTID : integer;
begin
     Result := 0;
     with dmConsultas do
     begin
          iRS_CONTID := -1;
          if  not ( cdsRSocial.isEmpty ) then
               iRS_CONTID := cdsRSocial.FieldByName('RS_CONTID').AsInteger;

          SQLText := Format( K_GET_COUNT_TIMBRADAS , [ ord( estiTimbrado ),  iRS_CONTID ] ) ;
          try
             cdsQueryGeneral.Refrescar;
             if ( cdsQueryGeneral.RecordCount > 0 ) then
                result := cdsQueryGeneral.FieldByName( 'CUANTOS' ).AsInteger;
          except
                on Error : Exception do
                begin
                     Result := 9999;
                end;
          end;
     end;
end;

procedure TdmInterfase.TimbrarNominaGetLista(Parametros: TZetaParams);
begin
      with cdsEmpleadosTimbrar do
     begin
          Data := ServerNominaTimbrado.TimbrarNominasgetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;
function TdmInterfase.ConstruyeListaEmpleados( oDataset: TDataset; const sFiltro : string ): string;

 const K_LIMITE_OBJETOS_FB = 1000; {Corrige defecto 113}
 var
    iCodigo : integer;
    sFiltroTemporal: string;
begin
     Result := '';
     with oDataSet do
     begin
          DisableControls;
          try
             if IsEmpty then
                Raise Exception.Create('No hay recibos por timbrar');
             iCodigo := FieldByName('Empleado').AsInteger;
             First;
             while NOT EOF do
             begin
                  if ( RecNo MOD K_LIMITE_OBJETOS_FB ) = 0 then
                  begin
                       sFiltroTemporal := ConcatString( sFiltroTemporal, sFiltro + ' IN ('+Result+')', ' OR ' );
                       Result := VACIO;
                  end;

                  Result := ConcatString( Result, FieldByName('Empleado').AsString, ',' );
                  Next;
             end;

             if StrLleno(Result) or StrLLeno(sFiltroTemporal) then
             begin
                  Result := sFiltro + ' IN ('+Result+')';
                  Result := ConcatString( sFiltroTemporal, Result, ' OR ')
             end;
             Locate('Empleado', VarArrayOf([iCodigo]), [] );
          finally
                 EnableControls;
          end;
     end;
end;
procedure TdmInterfase.cdsTimbradoConciliaGetPeriodosAlAdquirirDatos( Sender: TObject );
begin
     cdsTimbradoConciliaGetPeriodos.Data:= ServerNominaTimbrado.ObtenerPeriodosAConciliarTimbrado(dmCliente.Empresa,dmCliente.AnioConciliacionTimbrado, dmCliente.MesConciliacionTimbrado, dmCliente.RazonSocialConciliacionTimbrado );
end;

function TdmInterfase.GetDatosUpdateConciliacionTimbrado( oParams: TZetaParams ): OleVariant;
begin
     Result := ServerNominaTimbrado.ObtenerPeriodosAConciliarTimbrado(dmCliente.Empresa,dmCliente.AnioConciliacionTimbrado, dmCliente.MesConciliacionTimbrado, dmCliente.RazonSocialConciliacionTimbrado );
end;

procedure TdmInterfase.cdsTimbradoConciliaGetPeriodosAlCrearCampos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender )  do
     begin
          ListaFija( 'PE_TIPO', lfTipoPeriodo );
          ListaFija( 'PE_TIMBRO', lfStatusTimbrado );
          with FieldByName( 'NOMBRE_PERIODO' ) do
          begin
               OnGetText :=  PERIODO_NOMBREGetText;
               Alignment:= taLeftJustify;
          end;
          MaskTasa('PORC_CONCILIADOS');
     end;
end;

procedure TdmInterfase.cdsTiposSATBeforePost(DataSet: TDataSet);
begin
with cdsTiposSat do
    begin
          if ( not StrLleno( FieldByName( 'TB_CODIGO' ).AsString ) ) then
          begin
               FieldByName( 'TB_CODIGO' ).FocusControl;
               DataBaseError( 'C�digo no puede quedar vac�o' );
          end;
    end;
end;
function tdmInterfase.GetFoliosFiscalesPendientes(oParametros:TzetaParams) : boolean;
begin

   
     cdsFoliosFiscalesPendientes.Data :=  ServerNominaTimbrado.GetFoliosPendientes( dmcliente.empresa,oParametros.varvalues);
     cdsFoliosFiscalesPendientes.first;
     if (cdsFoliosFiscalesPendientes.IsEmpty) then
     begin
          result := false;
     end
     else
     begin
          result := true;
     end;
end;
Function tdmInterfase.UpdateFoliosPendientes(oParametros:TzetaParams):boolean;
begin
     Result := ServerNominaTimbrado.ActualizaFoliosFiscales( dmcliente.empresa,oParametros.varvalues);
end;


procedure TdmInterfase.GetEmpleadosBuscados_DevExTimbrado (const sPista: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet);
begin
     with oDataSet do
     begin
          Init;
          if ( FServerNominaTimbrado = nil ) then
             Data := ServerNominaTimbrado.GetEmpleadosBuscados_DevExTimbrado( oEmpresa, sPista )
          else
             Data := FServerNominaTimbrado.GetEmpleadosBuscados_DevExTimbrado( oEmpresa, sPista );
     end;
end;

//Busqueda Avanzada
procedure TdmInterfase.GetEmpleadosBuscados_DevExTimbradoAvanzada( const sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca: String; const oEmpresa: OleVariant; const oDataSet: TZetaClientDataSet );
begin
     with oDataSet do
     begin
          Init;
          if ( FServerNominaTimbrado = nil ) then
             Data := ServerNominaTimbrado.GetEmpleadosBuscados_DevExTimbradoAvanzada( oEmpresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca )
          else
             Data := FServerNominaTimbrado.GetEmpleadosBuscados_DevExTimbradoAvanzada( oEmpresa, sPaterno, sMaterno, sNombre, sRFC, sNSS, sBanca );
     end;
end;



function TdmInterfase.GetEmpleadoTransferidoParaActivo(const iEmpleado: TNumEmp; const oEmpresa: OleVariant):Boolean;
var
   Datos: OleVariant;
   iCodigo : integer;
begin
          if ( FServerNominaTimbrado = nil ) then
              Result := ServerNominaTimbrado.GetEmpleadoTransferidoParaActivo( oEmpresa, iEmpleado, Datos )
          else
              Result := FServerNominaTimbrado.GetEmpleadoTransferidoParaActivo( oEmpresa, iEmpleado, Datos );
         if Result then
         begin
              if ValidadorNavegacionEmpTransferidos(Datos) then
              begin
                    with dmcliente.cdsEmpleado do
                    begin
                         Data := Datos;
                    end;
              end
              else
              begin
                   Result := False;
              end;
         end;
end;

function TdmInterfase.GetEmpleadoTransferidoSiguiente(
  const oEmpresa: OleVariant; const iEmpleado: TNumEmp;
  CatalogosNavegacion: string; var Datos: OleVariant): Boolean;
begin
          if ( FServerNominaTimbrado = nil ) then
                   Result := ServerNominaTimbrado.GetEmpleadoTransferidoSiguiente( oEmpresa, iEmpleado, CatalogosNavegacion, Datos )

          else
                    Result := FServerNominaTimbrado.GetEmpleadoTransferidoSiguiente( oEmpresa, iEmpleado, CatalogosNavegacion, Datos );

end;

function TdmInterfase.GetEmpleadoTransferidoAnterior(const oEmpresa: OleVariant;
  const iEmpleado: TNumEmp; CatalogosNavegacion: string;
  var Datos: OleVariant): Boolean;
begin


          if ( FServerNominaTimbrado = nil ) then
                    Result := ServerNominaTimbrado.GetEmpleadoTransferidoAnterior( oEmpresa, iEmpleado, CatalogosNavegacion,  Datos )
          else
                    Result := FServerNominaTimbrado.GetEmpleadoTransferidoAnterior( oEmpresa, iEmpleado, CatalogosNavegacion,  Datos );
end;

 function TdmInterfase.GetValidacionEmpleadoTransferido ( const oEmpresa: OleVariant; const iEmpleado: TNumEmp): Boolean;
 var
     Datos: OleVariant;
     cdsValidador: TZetaClientDataSet;
 begin

          if ( FServerNominaTimbrado = nil ) then
                    Result := ServerNominaTimbrado.GetValidacionEmpleadoTransferido( oEmpresa, iEmpleado, Datos)
          else
                    Result := FServerNominaTimbrado.GetValidacionEmpleadoTransferido( oEmpresa, iEmpleado, Datos );

          cdsValidador := TZetaClientDataSet.Create(nil);
          cdsValidador.Data := Datos;

          if cdsValidador.FieldByName('TABLA').asstring = 'EMP_TRANSF' then
          begin
            Result := True;
          end
          else
          begin
              Result := false;
          end;
          FreeAndNil(cdsValidador);

end;


procedure TdmInterfase.cdsEmpleadoLookUpTimbradoLookupKey(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter,
  sKey: String; var sDescription: String);
var
   Datos: OleVariant;
   resultado : boolean;
begin
     if ( FServerNominaTimbrado = nil ) then
        lOk := ServerNominaTimbrado.GetLookupEmpleado( DmCliente.Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( eLookEmpGeneral ) )
     else
        lOk := FServerNominaTimbrado.GetLookupEmpleado( DmCliente.Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( eLookEmpGeneral ) );
     if lOk then
     begin
          with cdsEmpleadoLookUpTimbrado do
          begin
               Init;
               Data := Datos;
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmInterfase.cdsEmpleadoLookUpTimbradoLookupSearch(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
begin
     //if ( dmCliente.GetDatosUsuarioActivo.Vista = tvClasica )then
     //lOk := ZetaBuscaEmpleado.BuscaEmpleadoDialogo( sFilter, sKey, sDescription )
     // else
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
        lOk := ZetaBuscaEmpleado_DevExTimbrado.BuscaEmpleadoDialogo( '', sKey, sDescription )  //DevEx: Se agrego el if de la vista para llamar ala forma de busqueda indicada
     else
     ZetaBuscaEmpleado_DevExTimbrado.BuscaEmpleadoDialogo( '', sKey, sDescription );  //DevEx: Se agrego el if de la vista para llamar ala forma de busqueda indicada.


end;



function TdmInterfase.GetNominasPorYear( sYear, sEmpleado : string): string;
const
K_TRUE = 'True';
K_FALSE = 'False';
K_QUERY_NOMINAS_X_YEAR = 'select PE.PE_YEAR as Year, PE.PE_TIPO as Tipo, PE.PE_NUMERO as Numero, PE.PE_DESCRIP AS Nombre, '+
                              'PE.PE_FEC_INI as Inicial, PE.PE_FEC_FIN as Final, PE.PE_FEC_PAG as Pago, 1 as Frecuencia, NO_STATUS, NO_TIMBRO, '+
                              'RS.CT_CODIGO, PE.PE_TIPO, RP.RS_CODIGO, RS.RS_CONTID,  OBTENER = %s '+
                              'from NOMINA  NO join PERIODO PE on PE.PE_YEAR = NO.PE_YEAR and PE.PE_TIPO = NO.PE_TIPO and PE.PE_NUMERO = NO.PE_NUMERO  and NO_TIMBRO = 2 '+ {El NO_TIMBRO = 2 es Filtro para solo nominas timbradas}
                              'join RPATRON RP on RP.TB_CODIGO  = NO.CB_PATRON join RSOCIAL RS on RS.RS_CODIGO = RP.RS_CODIGO '+
                              'where NO_STATUS > 5  and PE.PE_YEAR = %s and NO.CB_CODIGO = %s order by PE.PE_FEC_PAG  ';
Var
     iYear, iCuantos : integer;
     bX : Boolean;
begin
     Result :=  VACIO;
          with dmConsultas do
          begin
             SQLText := Format( K_QUERY_NOMINAS_X_YEAR, [ EntreComillas( K_TRUE ), EntreComillas( sYear ), EntreComillas( sEmpleado ) ] );
             try
                cdsQueryGeneral.Refrescar;
                cdsNominasXYear.Data := cdsQueryGeneral.Data;
                cdsNominasXYear.Edit;
                //cdsNominasXYear.DisableControls;
              except
                   on Error : Exception do
                   begin
                        //DatabaseError( Format( 'Error al consultar conceptos modificados: %s', [ Error.Message ] ) );
                        //Result := 'Error';
                   end;
             end;
          end;

     iCuantos := cdsNominasXYear.RecordCount;
       If iCuantos > 0 then
       begin
                with cdsNominasXYear do
                begin
                     first;
                     while not Eof do
                     begin
                          Edit;
                          iYear := StrAsInteger(sYear);
                          FieldByName('Nombre').AsString :=Format('%s #%d del %d', [  ObtieneElemento( lfTipoPeriodo,  FieldByName('Tipo').AsInteger ) , FieldByName('Numero').AsInteger,  iYear ] );
                          //bX := FieldByName('OBTENER').AsBoolean;
                          //FieldByName('OBTENER').AsBoolean := true;
                          Next;
                     end;
                end;

            //cdsNominasXYear.EnableControls;
            Result :=  VACIO;
//          try
//              except on Error: Exception do
//              begin
//              end;
//           end
       end
       else
       begin
             Result := 'No hay periodos con n�minas timbradas en ese a�o.';
       end;
end;


//US: Conciliacion Timbrado de Nomina
function TdmInterfase.GetPeriodosConciliacionPorMesYear( iMes, iYear: integer): string;
const
K_TRUE = 'True';
K_FALSE = 'False';
K_QUERY_PERIODOS_X_MES_YEAR = 'select PE.PE_YEAR as Year, PE.PE_TIPO as Tipo, PE.PE_NUMERO as Numero, PE.PE_DESCRIP AS Nombre,' +
                              'PE.PE_FEC_INI as Inicial, PE.PE_FEC_FIN as Final, PE.PE_FEC_PAG as Pago, 1 as Frecuencia, PE_STATUS,' +
                              'PE.PE_TIPO, CONCILIAR = %s, 0 as CONCILIA_PROCESO,' + 'cast ('''' as VARCHAR(250) ) as CONCILIA_STATUS, ' +
                              '85 as CONCILIA_PROCESO, 0 as CONCILIA_AVANCE, 0 as DB_PROCESO ' +
                              'from PERIODO PE WHERE PE.PE_YEAR = %d AND PE.PE_MES= %d';
Var
     iCuantos : integer;
     bX : Boolean;
begin
     Result :=  VACIO;
     with dmConsultas do
     begin
        SQLText := Format( K_QUERY_PERIODOS_X_MES_YEAR, [ EntreComillas( K_TRUE ), iYear, iMes ] );
        try
           cdsQueryGeneral.Refrescar;
           cdsPeriodosXMesYear.Data := cdsQueryGeneral.Data;
           cdsPeriodosXMesYear.Edit;
         except
              on Error : Exception do
              begin
              end;
        end;
     end;

     iCuantos := cdsPeriodosXMesYear.RecordCount;
     if iCuantos > 0 then
     begin
          with cdsPeriodosXMesYear do
          begin
               first;
               while not Eof do
               begin
                    Edit;
                    FieldByName('Nombre').AsString :=Format('%s #%d del %d', [  ObtieneElemento( lfTipoPeriodo,  FieldByName('Tipo').AsInteger ) , FieldByName('Numero').AsInteger,  iYear ] );
                    Next;
               end;
          end;
          Result :=  VACIO;
     end
     else
     begin
          Result := 'No hay periodos en ese a�o y mes.';
     end;
end;

//procedure TdmInterfase.cdsSistActualizarBDsAlAdquirirDatos(Sender: TObject);
//begin
//  inherited;
//  Screen.Cursor := crHourGlass;
//  try
//    TZetaLookupDataSet(Sender).Data := ServerSistema.ConsultaActualizarBDs(0);
//  finally
//    Screen.Cursor := crDefault;
//  end;
//end;

function TdmInterfase.GetPeriodoEspecialTimbra(const oEmpresa: OleVariant; const iYear, iTipo, iNumero: Integer; var Datos: OleVariant): Boolean;
begin
	try
          if ( FServerNominaTimbrado = nil ) then
                   Result := ServerNominaTimbrado.GetPeriodoTimbrado( oEmpresa, iYear, Ord( iTipo ), iNumero, Datos )

          else
                    Result := FServerNominaTimbrado.GetPeriodoTimbrado( oEmpresa, iYear, Ord( iTipo ), iNumero, Datos );

	Except
	End;
end;


function TdmInterfase.GetPeriodoAnteriorEspecialTimbra(const oEmpresa: OleVariant; const iYear, iTipo, iNumero: Integer; var Datos: OleVariant): Boolean;
begin
	try
          if ( FServerNominaTimbrado = nil ) then
                   Result := ServerNominaTimbrado.GetPeriodoAnteriorTimbrado( oEmpresa, iYear, Ord( iTipo ), iNumero, Datos )

          else
                    Result := FServerNominaTimbrado.GetPeriodoAnteriorTimbrado( oEmpresa, iYear, Ord( iTipo ), iNumero, Datos );

	Except
	End;
end;


function TdmInterfase.GetPeriodoSiguienteEspecialTimbra(const oEmpresa: OleVariant; const iYear, iTipo, iNumero: Integer; var Datos: OleVariant): Boolean;
begin
	try
          if ( FServerNominaTimbrado = nil ) then
                   Result := ServerNominaTimbrado.GetPeriodoSiguienteTimbrado( oEmpresa, iYear, Ord( iTipo ), iNumero, Datos )

          else
                    Result := FServerNominaTimbrado.GetPeriodoSiguienteTimbrado( oEmpresa, iYear, Ord( iTipo ), iNumero, Datos );
	Except
	End;
end;

{*** Exportacion de Conceptos ***}
procedure TdmInterfase.ExportarConfiguracionConcepto( Sender: TAsciiExport; DataSet: TDataset );
const
     K_DOBLE_COMILLA = '"';
     K_COMA = ',';
     K_EPACIO = ' ';
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'CO_NUMERO' ).Datos := IntToStr( FieldByName( 'CO_NUMERO' ).AsInteger );
               ColumnByName( 'CO_DESCRIP' ).Datos := AnsiQuotedStr( StrTransAll( FieldByName( 'CO_DESCRIP' ).AsString, K_COMA , K_EPACIO), K_DOBLE_COMILLA );
               ColumnByName( 'CO_SAT_CLP' ).Datos := IntToStr( FieldByName( 'CO_SAT_CLP' ).AsInteger );
               ColumnByName( 'CO_SAT_TPP' ).Datos := FieldByName( 'CO_SAT_TPP' ).AsString;
               ColumnByName( 'CO_SAT_CLN' ).Datos := IntToStr( FieldByName( 'CO_SAT_CLN' ).AsInteger );
               ColumnByName( 'CO_SAT_TPN' ).Datos := FieldByName( 'CO_SAT_TPN' ).AsString;
               ColumnByName( 'CO_SAT_EXE' ).Datos := IntToStr( FieldByName( 'CO_SAT_EXE' ).AsInteger );
               ColumnByName( 'CO_SAT_CON' ).Datos := IntToStr( FieldByName( 'CO_SAT_CON' ).AsInteger );
          end;
     end;
end;

function TdmInterfase.CrearAsciiConcepto (cdsDataSet: TZetaClientDataSet;const sArchivoConcepto:string):String;
const
     K_FORMATO = '#,###,###,###,##0';
var
   iRegistros : Integer;
begin
     iRegistros := 0;
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoConcepto, False ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  Delimitador := VACIO;
                  AgregaRenglon( 'CO_NUMERO,CO_DESCRIP,CO_SAT_CLP,CO_SAT_TPP,CO_SAT_CLN,CO_SAT_TPN,CO_SAT_EXE,CO_SAT_CON' );
                  AgregaColumna( 'CO_NUMERO', tgTexto, 5 );
                  AgregaColumna( 'CO_DESCRIP', tgTexto, 30 );
                  AgregaColumna( 'CO_SAT_CLP', tgTexto, 6 );
                  AgregaColumna( 'CO_SAT_TPP',tgTexto,6 );
                  AgregaColumna( 'CO_SAT_CLN',tgTexto,6 );
                  AgregaColumna( 'CO_SAT_TPN',tgTexto,6 );
                  AgregaColumna( 'CO_SAT_EXE',tgTexto,6 );
                  AgregaColumna( 'CO_SAT_CON',tgTexto,5 );
                  AlExportarColumnas := ExportarConfiguracionConcepto;
                  iRegistros := Exporta( faASCIIDel, cdsDataset );
                  if not HayError then
                     Result :='Se exportaron: ' + FormatFloat( K_FORMATO, iRegistros ) + ' registros.'
                  else
                      Result := AgregaMensaje( Result, Status )
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

function TdmInterfase.AgregaMensaje( const sMensaje, sValor: String ): String;
begin
     Result := sMensaje + CR_LF + sValor;
end;

function TdmInterfase.CargaImportacionConcepto( const sRuta: String ):Olevariant;
var
   iErrorCount :Integer;
begin
     FillcdsASCII( cdsASCII, sRuta );
     Result := cdsAscii.Data;
     with cdsASCII do
     begin
          if ( RecordCount > 0 )then
          begin
               Active := True;
               //remover el renglon de los encabezados
               First;
               Delete;
               MergeChangeLog;
               Result := ImportarConfiguracionConceptosGetASCII( GetLista( cdsASCII ), iErrorCount );
               Active := False;
          end;
     end;
end;

function TdmInterfase.GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
begin
     with ZetaDataset do
     begin
          MergeChangeLog;  // Elimina Registros Borrados //
          Result := Data;
     end;
end;

function TdmInterfase.ImportarConfiguracionConceptosGetASCII( ListaASCII: OleVariant; var ErrorCount: Integer): OleVariant;
var
   oSuperASCII:TdmSuperASCII;
begin
     oSuperASCII := TdmSuperASCII.Create( Self );
     try
        with oSuperASCII do
        begin
             Formato := faASCIIDel;
             FormatoImpFecha := ifDDMMYYs;
             UsaCommaText := False;
             AgregaColumna( 'CO_NUMERO',  tgNumero, 5, TRUE );
             AgregaColumna( 'CO_DESCRIP', tgTexto, 30 );
             AgregaColumna( 'CO_SAT_CLP', tgNumero, 6 );
             AgregaColumna( 'CO_SAT_TPP', tgTexto, 6 );
             AgregaColumna( 'CO_SAT_CLN', tgNumero, 6 );
             AgregaColumna( 'CO_SAT_TPN', tgTexto, 6 );
             AgregaColumna( 'CO_SAT_EXE', tgNumero, 6 );
             AgregaColumna( 'CO_SAT_CON', tgNumero, 5 );
             Result := Procesa( ListaASCII );
             ErrorCount := Errores;
        end;
     finally
            oSuperASCII.Free;
     end;
end;


function TdmInterfase.ArchivoInvalido( const sRuta: String ):Boolean;
begin
     cdsImpotConfConcepto.Data := CargaImportacionConcepto( sRuta );
     Result := cdsImpotConfConcepto.IsEmpty;
end;

function TdmInterfase.ImportarConfiguracionConceptos( var sMensaje: String ): Boolean;
const
     K_FORMATO_ERROR = 'El concepto no se encuentra en el cat�logo: %d,%s,%d,%s,%d,%s,%d,%d';
begin
     try
        Result := False;
        sMensaje := VACIO;
        dmCatalogos.cdsConceptos.DisableControls;
        cdsImpotConfConcepto.First;
        while not cdsImpotConfConcepto.Eof do
        begin
           if dmCatalogos.cdsConceptos.Locate('CO_NUMERO', cdsImpotConfConcepto.FieldByName ('CO_NUMERO').AsString, []) then
           begin
               if ValidarConfiguracionConcepto then
               begin
                    dmCatalogos.cdsConceptos.Edit;
                    AgregarEditarConcepto;
               end;
           end
           else
           begin
                Log( Format( K_FORMATO_ERROR ,[cdsImpotConfConcepto.FieldByName('CO_NUMERO').AsInteger,
                                               cdsImpotConfConcepto.FieldByName('CO_DESCRIP').AsString,
                                               cdsImpotConfConcepto.FieldByName('CO_SAT_CLP').AsInteger,
                                               cdsImpotConfConcepto.FieldByName('CO_SAT_TPP').AsString,
                                               cdsImpotConfConcepto.FieldByName('CO_SAT_CLN').AsInteger,
                                               cdsImpotConfConcepto.FieldByName('CO_SAT_TPN').AsString,
                                               cdsImpotConfConcepto.FieldByName('CO_SAT_EXE').AsInteger,
                                               cdsImpotConfConcepto.FieldByName('CO_SAT_CON').AsInteger] ) );
           end;
           cdsImpotConfConcepto.Next;
        end;
        sMensaje := 'Termino el proceso de importaci�n.';
        dmCatalogos.cdsConceptos.EnableControls;
        Result := True;
     except
           on Error : Exception do
           begin
                sMensaje := Error.Message;
                Result := False;
           end;
     end;
end;

procedure TdmInterfase.ObtenerInfoConceptoConfiguracion;
const
     K_CONCEPTO = 'Error en: %d,%s,%d,%s,%d,%s,%d,%d';
begin
     Log( Format( K_CONCEPTO , [ cdsImpotConfConcepto.FieldByName('CO_NUMERO').AsInteger,
                                 cdsImpotConfConcepto.FieldByName('CO_DESCRIP').AsString,
                                 cdsImpotConfConcepto.FieldByName('CO_SAT_CLP').AsInteger,
                                 cdsImpotConfConcepto.FieldByName('CO_SAT_TPP').AsString,
                                 cdsImpotConfConcepto.FieldByName('CO_SAT_CLN').AsInteger,
                                 cdsImpotConfConcepto.FieldByName('CO_SAT_TPN').AsString,
                                 cdsImpotConfConcepto.FieldByName('CO_SAT_EXE').AsInteger,
                                 cdsImpotConfConcepto.FieldByName('CO_SAT_CON').AsInteger] ) );
end;

function TdmInterfase.ValidarConfiguracionConcepto: Boolean;
const
     K_CONCEPTO_SISTEMA = 1000;
     K_CONCEPTO_CERO = 0;
     K_CO_SAT_CON = 0;

     function ValidoTipoPercepcion( const sTipoSAT: String ): Boolean;
     const
         Q_TIPO_SAT = 'select COUNT(*) AS RESULTADO from TIPO_SAT WHERE TB_CODIGO = %s';
     var
        iCuantos : Integer;
     begin
          Result := False;
          iCuantos := 0;
          dmConsultas.SQLText:= Format( Q_TIPO_SAT, [ EntreComillas( sTipoSAT ) ] );
          with dmConsultas.cdsQueryGeneral do
          begin
               Refrescar;
               if not IsEmpty then
                  iCuantos := FieldByName('RESULTADO').AsInteger;
               Result := iCuantos > 0;
          end;
     end;

     function ValidoConcepto( iNumeroConcepto: Integer ): Boolean;
     const
         Q_CONCEPTOS_SAT = 'select COUNT(*) AS RESULTADO from CONCEPTO WHERE CO_NUMERO = %d';
     var
        iCuantos : Integer;
     begin
          Result := False;
          iCuantos := 0;
          dmConsultas.SQLText:= Format( Q_CONCEPTOS_SAT, [ iNumeroConcepto ] );
          with dmConsultas.cdsQueryGeneral do
          begin
               Refrescar;
               if not IsEmpty then
                  iCuantos := FieldByName('RESULTADO').AsInteger;
               Result := iCuantos > 0;
          end;
     end;
begin
     Result := True;
     with cdsImpotConfConcepto do
     begin
          if ( FieldByName('CO_NUMERO').AsInteger <= K_CONCEPTO_CERO ) then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'El n�mero de concepto debe ser mayor que cero: ' + IntToStr( FieldByName('CO_NUMERO').AsInteger ) );
               Result := False;
          end;

          if ( FieldByName('CO_NUMERO').AsInteger >= K_CONCEPTO_SISTEMA ) then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'El n�mero de concepto debe ser menor que: ' + IntToStr( K_CONCEPTO_SISTEMA ) );
               Result := False;
          end;

          if not ( FieldByName('CO_SAT_CLP').AsInteger in [0,1,2,3,4] ) then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Clase de concepto en Montos Positivos no es correcto: ' + IntToStr( FieldByName('CO_SAT_CLP').AsInteger ) ) ;
               Result := False;
          end;

          if (  (FieldByName('CO_SAT_CLP').AsInteger in [1,2] ) and StrVacio(FieldByName('CO_SAT_TPP').AsString) )  then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Tipo de Percepci�n/Deducci�n en Montos Positivos no puede quedar vac�o') ;
               Result := False;
          end;

          if ( StrLleno(FieldByName('CO_SAT_TPP').AsString) ) and ( not ValidoTipoPercepcion( FieldByName('CO_SAT_TPP').AsString ) ) then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Tipo de Percepci�n/Deducci�n en Montos Positivos no es correcto: ' + FieldByName('CO_SAT_TPP').AsString ) ;
               Result := False;
          end;

          if not ( FieldByName('CO_SAT_CLN').AsInteger in [0,1,2,3,4] ) then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Clase de concepto en Montos Negativos no es correcto: ' + IntToStr( FieldByName('CO_SAT_CLN').AsInteger ) ) ;
               Result := False;
          end;

          if ( (FieldByName('CO_SAT_CLN').AsInteger in [1,2] ) and StrVacio(FieldByName('CO_SAT_TPN').AsString) )  then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Tipo de Percepci�n/Deducci�n en Montos Negativos no puede quedar vac�o' ) ;
               Result := False;
          end;

          if ( StrLleno(FieldByName('CO_SAT_TPN').AsString) ) and ( not ValidoTipoPercepcion( FieldByName('CO_SAT_TPN').AsString ) ) then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Tipo de Percepci�n/Deducci�n en Montos Negativos no es correcto: ' + FieldByName('CO_SAT_TPN').AsString) ;
               Result := False;
          end;

          if not ( FieldByName('CO_SAT_EXE').AsInteger in [0,1,2,3,4,5,6,7] ) then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Tratamiento de Percepci�n no es correcto: ' + IntToStr( FieldByName('CO_SAT_EXE').AsInteger ) ) ;
               Result := False;
          end;

          if ( (FieldByName('CO_SAT_EXE').AsInteger in [3,6] ) and ( FieldByName('CO_SAT_CON').AsInteger = K_CO_SAT_CON  )  )  then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Concepto para Gravado/Exento debe estar capturado') ;
               Result := False;
          end;

          if ( FieldByName('CO_SAT_CON').AsInteger > K_CO_SAT_CON  ) and ( not ValidoConcepto( FieldByName('CO_SAT_CON').AsInteger ) ) then
          begin
               ObtenerInfoConceptoConfiguracion;
               Log( 'Concepto para Gravado/Exento no es correcto: ' + IntToStr ( FieldByName('CO_SAT_CON').AsInteger ) ) ;
               Result := False;
          end;
     end;
end;

procedure TdmInterfase.AgregarEditarConcepto;
var iCampos: Integer;
begin
    with dmCatalogos.CdsConceptos do
    begin
         FieldByName('CO_SAT_CLP').AsInteger :=  cdsImpotConfConcepto.FieldByName('CO_SAT_CLP').AsInteger;
         FieldByName('CO_SAT_TPP').AsString  :=  cdsImpotConfConcepto.FieldByName('CO_SAT_TPP').AsString;
         FieldByName('CO_SAT_CLN').AsInteger :=  cdsImpotConfConcepto.FieldByName('CO_SAT_CLN').AsInteger;
         FieldByName('CO_SAT_TPN').AsString  :=  cdsImpotConfConcepto.FieldByName('CO_SAT_TPN').AsString;
         FieldByName('CO_SAT_EXE').AsInteger :=  cdsImpotConfConcepto.FieldByName('CO_SAT_EXE').AsInteger;
         FieldByName('CO_SAT_CON').AsInteger  :=  cdsImpotConfConcepto.FieldByName('CO_SAT_CON').AsInteger;
    end;
    dmCatalogos.CdsConceptos.Enviar;
end;

function TdmInterfase.IsOpen(const fName: string):Boolean;
var
   HFileRes: HFILE;
begin
     Result := False;
     if not FileExists(fName) then begin
        Exit;
     end;

     HFileRes := CreateFile(PChar(fName)
      ,GENERIC_READ or GENERIC_WRITE
      ,0
      ,nil
      ,OPEN_EXISTING
      ,FILE_ATTRIBUTE_NORMAL
      ,0);

    Result := (HFileRes = INVALID_HANDLE_VALUE);

    if not(Result) then begin
      CloseHandle(HFileRes);
    end;
end;

{ Eventos de la bit�cora }
procedure TdmInterfase.LogInit( sPath,sProceso:string );
begin
     with FBitacora do
     begin
          try
             if not Used then
             begin
                  Init( sPath );
             end;
             WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ) );
             WriteTexto( Format( 'Empresa: %s', [ dmCliente.GetDatosEmpresaActiva.Nombre ] ) );
             WriteTexto( StringOfChar( '-', 40 ) );
             WriteTexto( sProceso );

             except on Error: Exception do
             begin
                  Raise Exception.Create( 'Error al grabar la bit�cora' );
             end;

          end;
     end;
end;

procedure TdmInterfase.Log( sTexto:string );
begin
     with FBitacora do
     begin
          WriteTexto( sTexto );
     end;
end;

procedure TdmInterfase.LogEnd;
begin
     with FBitacora do
     begin
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Final %s ' + StringOfChar( '*', K_RELLENO ) );
          WriteTexto( '' );
          Close;
     end;
end;

//US Conciliacion Timbrado
{* Regresar una tabla con datos de un proceso de actualizaci�n. @autor Ricardo Carrillo}
function TdmInterfase.GetMotorPatchAvance(Parametros: OleVariant): OleVariant;
var
   oParametros: TZetaParams;
begin
     oParametros := TZetaParams.Create(Self);
     oParametros.VarValues := Parametros;
     oParametros.AddVariant('Empresa', dmCliente.Empresa);
     Parametros := oParametros.VarValues;
     Result := ServerNominaTimbrado.GetMotorAvanceProceso(Parametros);
end;

procedure TdmInterfase.IniciarConciliacionNominaTIMBRES(Parametros: OleVariant);
var
   oParametros: TZetaParams;
begin
     oParametros := TZetaParams.Create(Self);
     oParametros.VarValues := Parametros;
     oParametros.AddVariant('Empresa', dmCliente.Empresa);
     Parametros := oParametros.VarValues;
     //ServerNominaTimbrado.GetMotorAvanceProceso(Parametros);
end;

procedure TdmInterfase.ObtenerXMLEmpleadosAConciliarTimbrado( oEmpresa: OleVariant; Parametros: OleVariant; var XML: WideString; var Error: WideString );
begin
     cdsEmpleadosConciliarTimbrado.Data := ServerNominaTimbrado.ObtenerXMLEmpleadosAConciliarTimbrado(oEmpresa, Parametros, XML, Error);
end;

function TdmInterfase.GetCredencialesConciliacionTimbrado: String;
begin
     Result := VACIO;
     Result := GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
end;

function TdmInterfase.GetPeticionTimbradoConciliacionNube(fParams: TZetaParams): String;
const
     K_TITULO_ERROR = 'Conciliac�n de Timbrado';
     K_NO_GENERO_REPORTE = 'No se genero la peticion de conciliaci�n';
var
     XMLTools: DXMLTools.TdmXMLTools;
     sXML: String;
     sXMLPeriodo: String;
begin
     Result := VACIO;
     sXML := VACIO;
     XMLTools := TdmXMLTools.Create( nil );
     try
        Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
        Result := Result + '<PROCESS>' +char(10)+char(13);
        Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
        Result := Result +         '<DATA>'  +char(10)+char(13);
        Result := Result + Format( '<PERIODO Year="%d" Tipo="%d" Numero="%d" >', [fParams.ParamByName( 'PE_YEAR' ).AsInteger,  fParams.ParamByName( 'PE_TIPO' ).AsInteger, fParams.ParamByName( 'PE_NUMERO' ).AsInteger] ) +char(10)+char(13);
        //Generacion de la peticion xml
        sXML := GeneraXMLFromDataset( cdsEmpleadosConciliarTimbrado, 'NOMINAS', 'NOMINA' );
        sXML  := stringReplace( sXML, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '', []);
        sXML := stringReplace( sXML, '<?xml version="1.0" standalone="yes"?>', '', []);
        //FIN
        Result := Result + sXML;
        Result := Result + '</PERIODO>'  +char(10)+char(13);
        Result := Result + '</DATA>'  +char(10)+char(13);
        Result := Result + '</PROCESS>'  +char(10)+char(13);
     except on Error: Exception do

            zError( K_TITULO_ERROR, K_NO_GENERO_REPORTE + Error.Message, 0);
     end;
     FreeAndNil(XMLTools);
     Application.ProcessMessages;
end;                         //

function TdmInterfase.GetPeticionAuditoriaConceptoNube( sXMLAuditoria: String; var sError: String ): String;
const
     K_TITULO_ERROR = 'Conciliac�n de Timbrado';
     K_NO_GENERO_REPORTE = 'No se genero la peticion de conciliaci�n';
var
     XMLTools: DXMLTools.TdmXMLTools;
     sXML: String;
     sXMLPeriodo: String;
begin
     Result := VACIO;
     sXML := VACIO;
     sError := VACIO;
     XMLTools := TdmXMLTools.Create( nil );
     try
        Result := '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>'+char(10)+char(13);
        Result := Result + '<PROCESS>' +char(10)+char(13);
        Result := Result + GetCredenciales(  cdsRSocial.FieldByName('CT_CODIGO').AsString,  cdsRSocial.FieldByName('RS_CONTID').AsInteger);
        Result := Result +         '<DATA>'  +char(10)+char(13);
        //Generacion de la peticion xml
        sXML := sXMLAuditoria;
        sXML  := stringReplace( sXML, '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>', '', []);
        sXML := stringReplace( sXML, '<?xml version="1.0" standalone="yes"?>', '', []);
        //FIN
        Result := Result + sXML;
        Result := Result + '</DATA>'  +char(10)+char(13);
        Result := Result + '</PROCESS>'  +char(10)+char(13);
     except on Error: Exception do
            sError := Error.Message;
     end;
     FreeAndNil(XMLTools);
     Application.ProcessMessages;
end;

function TdmInterfase.IniciarConciliacionTimbradoPeriodos(Parametros: TZetaParams): Boolean;
var
   VarValues : OleVariant;
begin
     Parametros.AddVariant('Empresa', dmCliente.Empresa);
     VarValues := Parametros.VarValues;
     Result := ServerNominaTimbrado.IniciarConciliacionTimbradoPeriodos(VarValues);
     Parametros.VarValues := VarValues;
end;

function TdmInterfase.AplicarConciliacionTimbradoPeriodos(Parametros: TZetaParams): Boolean;
var
   VarValues : OleVariant;
begin
     Parametros.AddVariant('Empresa', dmCliente.Empresa);
     VarValues := Parametros.VarValues;
     Result := ServerNominaTimbrado.AplicarConciliacionTimbradoPeriodos(VarValues);
     Parametros.VarValues := VarValues;
end;


procedure TdmInterfase.GetDetalleConciliacionPeriodo( Parametros: TZetaParams ); //const iPE_YEAR: Integer; const iPE_TIPO: Integer; const iPE_NUMERO: Integer );
var
   oCursor: TCursor;
   VarValues : OleVariant;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        VarValues := Parametros.VarValues;
        cdsDetalleConciliacionTimbrado.Data := ServerNominaTimbrado.GetEmpleadosConciliados( dmCliente.Empresa, VarValues );
     finally
            Screen.Cursor := oCursor;
     end;
end;

//Bitacora de Timbrado
function TdmInterfase.GetContribuyenteParametros: string;
   function FileToBase46( sFilePath : string ) : string;
   var
      SourceStr: TFileStream;
      Encoder: TIdEncoderMIME;
   begin
       SourceStr := TFileStream.Create(sFilePath, fmOpenRead);
       try
         Encoder := TIdEncoderMIME.Create(nil);
         try
           Result := Encoder.Encode(SourceStr);
         finally
           Encoder.Free;
         end;
       finally
         SourceStr.Free;
       end;
   end;
begin
     Result := VACIO;
     Result := 'Raz�n social: ' + cdsRSocial.FieldByName('RS_CODIGO').AsString + ' - ' + cdsRSocial.FieldByName('RS_NOMBRE').AsString + CR_LF;
     Result := Result + 'N�mero de contribuyente en recibo digital: ' + cdsRSocial.FieldByName('RS_CONTID').AsString + CR_LF;
     Result := Result + 'R.F.C.: ' + cdsRSocial.FieldByName('RS_RFC').AsString + CR_LF;
     Result := Result + 'CURP: ' + cdsRSocial.FieldByName('RS_CURP').AsString + CR_LF;
     Result := Result + 'Contribuyente: ' + cdsRSocial.FieldByName('RS_NOMBRE').AsString + CR_LF;
     Result := Result + 'Representante legal: ' + cdsRSocial.FieldByName('RS_RLEGAL').AsString + CR_LF;
     Result := Result + 'R.F.C. Representante legal: ' + cdsRSocial.FieldByName('RS_RL_RFC').AsString + CR_LF;
     if ( FParametrosRazonSocial.ParamByName('REGIMEN_FISCAL_ANTERIOR').AsString <> FParametrosRazonSocial.ParamByName('REGIMEN_FISCAL_NUEVO').AsString ) then
     begin
          Result := Result + 'R�gimen fiscal anterior: ' + FParametrosRazonSocial.ParamByName('REGIMEN_FISCAL_ANTERIOR').AsString + CR_LF;
          Result := Result + 'R�gimen fiscal nuevo: ' + FParametrosRazonSocial.ParamByName('REGIMEN_FISCAL_NUEVO').AsString + CR_LF;
     end
     else
         Result := Result + 'R�gimen fiscal: ' + FParametrosRazonSocial.ParamByName('REGIMEN_FISCAL_NUEVO').AsString + CR_LF;
     Result := Result + 'Domicilio fiscal:' + CR_LF;
     Result := Result + 'Calle: ' + cdsRSocial.FieldByName('RS_CALLE').AsString + CR_LF;
     Result := Result + '# Exterior:' + cdsRSocial.FieldByName('RS_NUMEXT').AsString + CR_LF;
     Result := Result + '# Interior: ' + cdsRSocial.FieldByName('RS_NUMINT').AsString + CR_LF;
     Result := Result + 'Ciudad: ' + cdsRSocial.FieldByName('RS_CIUDAD').AsString + CR_LF;
     Result := Result + 'Colonia: ' + cdsRSocial.FieldByName('RS_COLONIA').AsString + CR_LF;
     Result := Result + 'Estado: ' + dmTablas.cdsEstado.FieldByName('TB_CODIGO').AsString + ' - ' + dmTablas.cdsEstado.FieldByName('TB_ELEMENT').AsString + CR_LF;
     Result := Result + 'C�digo postal: ' + cdsRSocial.FieldByName('RS_CODPOST').AsString;
     if ( FParametrosRazonSocial.ParamByName('ACTUALIZAR_CSD').AsBoolean) then
     begin
          Result := Result + CR_LF;
          Result := Result + 'Llave privada (*.key): ' + FileToBase46(FParametrosRazonSocial.ParamByName('LLAVE_PRIVADA').AsString) + CR_LF;
          Result := Result + 'Certificado (*.cer): ' + FileToBase46(FParametrosRazonSocial.ParamByName('CSD').AsString) + CR_LF;
     end;
end;

function TdmInterfase.MensajeRegistroContribuyenteExito: String;
begin
     Result := GetContribuyenteParametros;
end;

procedure TdmInterfase.EscribeBitacoraTRESSRazonSocial;
const
     K_ENCABEZADO_MSG  = 'Actualizaci�n de contribuyente raz�n social: %s';
var
   Parametros : TZetaParams;
begin
     try
        with dmInterfase.cdsRSocial do
        begin
             Parametros := TZetaParams.Create(Self);
             Parametros.AddString('Titulo', Format( K_ENCABEZADO_MSG, [ cdsRSocial.FieldByName('RS_CODIGO').AsString ] ) );
             Parametros.AddString('Descripcion', MensajeRegistroContribuyenteExito);
             ServerSistemaTimbrado.CrearBitacora(dmCliente.Empresa,0,Parametros.VarValues);
        end;
     finally
            Parametros.Free;
     end;
end;

function TdmInterfase.ObtieneInformacionConceptosTimbrado( var sXML: string; var sError: String ): Boolean;
var
   Parametros : TZetaParams;
   VarValues : OleVariant;
begin
     sXML := VACIO;
     Result := FALSE;
     try
        try
           Parametros := TZetaParams.Create(Self);
           Parametros.AddVariant('Parametro1', 1);
           VarValues := Parametros.VarValues;
           cdsAuditoriaConceptosTimbradoXML.Data := ServerNominaTimbrado.GetXMLAutidoriaConceptoTimbrado( dmCliente.Empresa, VarValues );
           with cdsAuditoriaConceptosTimbradoXML do
           begin
                if not Eof then
                begin
                     sXML :=  FieldByName('ConceptosXML').AsString;
                     Result := TRUE;
                end;
           end;
        except on Error: Exception do
               begin
                    sError := Error.Message;
                    Result := FALSE;
               end;
        end;
     finally
            Parametros.Free;
     end;
     Application.ProcessMessages;
end;

end.
