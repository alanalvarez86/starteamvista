unit DDiccionario;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
  {$ifndef VER130}Variants,{$endif}  
  DBaseDiccionario,
  DBaseTressDiccionario,
  ZetaTipoEntidad,
  ZetaCommonLists,
  ZetaClientDataSet  ;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
    procedure cdsDiccionAlModificar(Sender: TObject);
    procedure cdsDiccionAfterPost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    {$ifdef RDD}
     {$else}
      {$ifdef TRESS}
      procedure AgregandoRelacion(Sender: TObject);
      {$endif}
     {$endif}

  protected
    procedure SetAgregaRelacion;override;
    {$ifdef RDD}

    {$else}
    function GetNOClasificacion(const lFavoritos, lMigracion, lSuscripciones: Boolean): ListaClasificaciones;override;

    {$endif}

  public
  {$ifdef RDD}
  {$else}
    procedure CamposPorEntidad( const oEntidad: TipoEntidad;
                                const lTodos: Boolean; oLista: TStrings);
  {$endif}
    procedure SetLookupNames; override;
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;
    function GetValorActivo({$ifdef RDD}const eValor :eRDDValorActivo{$else}const sValor :string{$endif} ): string;override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation

uses
    //**ZBaseEdicion,
    ZBaseEdicion_DevEx,
{$ifdef RDD}
{$else}
     ZetaEntidad,
{$endif}
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZReportTools,
{$ifndef TRESSCFG}
     //**FEditDiccion,
//     FEditDiccion_DevEx,
{$endif}
{$ifdef RDD}

{$else}
     DEntidadesTress,
{$endif}
     DGlobal,
{$ifndef TRESSCFG}
     DConteo,
{$endif}
     DCliente{$ifndef TRESSCFG},
     DTablas{$endif};

{$R *.DFM}


procedure TdmDiccionario.DataModuleCreate(Sender: TObject);
begin
     inherited;
{$ifdef RDD}

{$else}
       FClasificacion :=   VarArrayOf( [crFavoritos,
                          crSuscripciones,
                          crEmpleados ,
                          crCursos ,
                          crAsistencia ,
                          crNominas ,
                          crPagosIMSS ,
                          crConsultas ,
                          crCatalogos ,
                          crTablas ,
                          crSupervisor ,
                          crCafeteria ,
                          crLabor ,
                          crMedico ,
                          crCarrera ,
                          crKiosco ,
                          crAccesos ,
                          crEvaluacion ,
                          crCajaAhorro ,
                          crMigracion ,
                          crEmailEmpleados]
                         );
{$endif}

end;

{$ifdef RDD}

{$else}

procedure TdmDiccionario.CamposPorEntidad( const oEntidad : TipoEntidad;
                                           const lTodos: Boolean;
                                                 oLista : TStrings );
 var oCampo : TObjetoString;
begin
     oLista.Clear;
     GetListaDatosTablas( oEntidad, lTodos );
     with cdsBuscaPorTabla do
          while NOT EOF do
          begin
               oCampo := TObjetoString.Create;
               oCampo.Campo := FieldByName('DI_NOMBRE').AsString;
               oLista.AddObject( FieldByName('DI_TITULO').AsString,
                                 oCampo );
               Next;
          end;
end;
{$endif}
procedure TdmDiccionario.cdsDiccionAlModificar(Sender: TObject);
begin
     inherited;
     {$ifndef TRESSCFG}
     //**Usar forma nueva
     //ZBaseEdicion.ShowFormaEdicion( EditDiccion, TEditDiccion );
//     ZBaseEdicion_DevEx.ShowFormaEdicion( EditDiccion_DevEx, TEditDiccion_DevEx );
     {$endif}
end;

procedure TdmDiccionario.cdsDiccionAfterPost(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     inherited;
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
             Reconcile ( ServerDiccionario.GrabaDiccion( dmCliente.Empresa,Delta, ErrorCount ) );
     end;
end;


{$ifdef RDD}
{$else}
  {$ifdef TRESS}
procedure TdmDiccionario.AgregandoRelacion(Sender : TObject);
   var oEntidad : TipoEntidad;
       i:integer;
begin
     {CV: Ahorita el Sender siempre es la Entidad enCONTEO}
      with TEntidad(Sender), dmEntidadesShell do
      begin
           for i:=0 to K_MAX_CONTEO_NIVELES do
           begin
                oEntidad := dmConteo.GetConteoEntidad(i);
                if oEntidad = enNinguno then
                   Exit
                else
                begin
                     if oEntidad <> enNivel0 then
                        AgregaRelacion(GetEntidad(oEntidad),'CT_NIVEL_'+ IntToStr(i));
                end;
           end;
      end;
end;
   {$endif}
{$endif}

procedure TdmDiccionario.SetAgregaRelacion;
begin
     {CV: Ahorita la unica Entidad que tiene un evento AgregaRelacion es
     enConteo, en un futuro, si alguna otra cae en ese caso, se haria una
     lista de entidades, y el siguiente codigo estaria dentro de un FOR}
     {$ifdef RDD}
     {$else}
      {$ifdef TRESS}
      with dmEntidadesShell.GetEntidad(enConteo) do
          AlAgregarRelacion := AgregandoRelacion;
      {$endif}
     {$endif}

end;
procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
{$ifndef TRESSCFG}
     dmTablas.SetLookupNames;
{$endif}
end;


procedure TdmDiccionario.GetListaClasifiModulo( oLista : TStrings; const lMigracion : Boolean );
{$ifdef RDD}
{$else}
 var
    i: integer;
    Clasificaciones: ListaClasificaciones;
{$endif}
begin
       inherited GetListaClasifiModulo( oLista, lMigracion );

       {$ifdef RDD}
       {$else}
       with oLista do
       begin

            Clasificaciones := GetNOClasificacion( FALSE, lMigracion, FALSE );

            for i:= VarArrayLowBound( FClasificacion, 1 ) to VarArrayHighBound( FClasificacion, 1 ) do
            begin
                 if NOT ( eClasifiReporte( FClasificacion[i] ) in Clasificaciones ) then
                    AgregaClasifi( oLista, eClasifiReporte( FClasificacion[i] ), TRUE );
            end;
       end;
       {$endif}
end;

{$ifdef RDD}

{$else}
function TdmDiccionario.GetNOClasificacion( const lFavoritos, lMigracion, lSuscripciones : Boolean ): ListaClasificaciones;
begin
     Result := inherited GetNOClasificacion( lFavoritos, lMigracion, lSuscripciones );

     if NOT lMigracion then
        Result := Result + [crMigracion];

     Result := Result + [crEvaluacion] + [crCajaAhorro];
end;
{$endif}


function TdmDiccionario.GetValorActivo(  {$ifdef RDD}const eValor :eRDDValorActivo{$else}const sValor :string{$endif} ): string;
begin
     {$ifdef RDD}
     with dmCliente do
     begin
          case eValor of
               vaImssPatron: Result := Comillas( IMSSPatron );
               vaImssMes: Result := IntToStr( IMSSMes );
               vaImssTipo: Result := IntToStr( Ord( IMSSTipo ) );
               vaImssYear: Result := IntToStr( IMSSYear );
          else
              Result := inherited GetValorActivo( eValor )
          end;
     end;
     {$else}
     with dmCliente do
     begin
          if ( sValor = K_IMSS_PATRON ) then
             Result := Comillas( IMSSPatron )
          else
              if ( sValor = K_IMSS_YEAR ) then
                 Result := IntToStr( IMSSYear )
              else
                  if ( sValor = K_IMSS_MES ) then
                     Result := IntToStr( IMSSMes )
                  else
                      if ( sValor = K_IMSS_TIPO ) then
                         Result := IntToStr( Ord( IMSSTipo ) )
                      else
                          Result := inherited GetValorActivo(  sValor );
     end;

     {$endif}

end;


end.
