inherited dmCliente: TdmCliente
  Height = 364
  Width = 523
  inherited cdsUsuario: TZetaClientDataSet
    Left = 112
    Top = 40
  end
  inherited cdsCompany: TZetaClientDataSet
    Left = 88
    Top = 112
  end
  inherited cdsEmpleadoLookUp: TZetaLookupDataSet
    Left = 220
    Top = 128
  end
  inherited cdsTPeriodos: TZetaLookupDataSet
    Left = 201
    Top = 18
  end
  object cdsPatron: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 78
    Top = 184
  end
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsEmpleadoBeforePost
    AfterDelete = cdsEmpleadoAfterDelete
    OnNewRecord = cdsEmpleadoNewRecord
    OnReconcileError = cdsEmpleadoReconcileError
    AlAdquirirDatos = cdsEmpleadoAlAdquirirDatos
    AlEnviarDatos = cdsEmpleadoAlEnviarDatos
    AlCrearCampos = cdsEmpleadoAlCrearCampos
    AlAgregar = cdsEmpleadoAlAgregar
    AlBorrar = cdsEmpleadoAlBorrar
    Left = 272
    Top = 104
  end
  object cdsPeriodo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 192
    Top = 192
  end
end
