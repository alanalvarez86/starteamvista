unit DReportesXML;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, Registry, ComCtrls,
     DReportesGenerador,
     DXMLTools,
     ZetaCommonLists,
     ZetaClientDataSet;

type
  TdmReportesXML = class(TdmReportGenerator)
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FResultado: TZetaClientDataSet;
    FReportName: String;
    FXMLTools: DXMLTools.TdmXMLTools;
    function GetDerechosClasificacion( const eClasificacion: eClasifiReporte): Integer;
    procedure AddColumn(Fuente: TField);
    procedure AddData(Fuente: TField );
  protected
    { Protected declarations }
    procedure DoOnGetReportes( const lResultado: Boolean ); override;
    procedure DoOnGetResultado( const lResultado: Boolean; const Error : string );override;
  public
    { Public declarations }
    property Resultado: TZetaClientDataSet read FResultado write FResultado;
    property XMLTools: DXMLTools.TdmXMLTools read FXMLTools write FXMLTools;
    function Procesar(const sEmpresa: String; const iReporte: Integer; var sNombre: String): Integer;
    function ProcesarToleraVacio(const sEmpresa: String; const iReporte: Integer; var sNombre: String;  var iCount  : integer): boolean;
    function GeneraXML( const sRaiz,  sTag : string) : string;
  end;

var
  dmReportesXML: TdmReportesXML;


function GeneraXMLFromDataset( cdsSource : TZetaClientDataSet; const sRaiz,  sTag : string) : string;
function GeneraXMLFromDatasetLimit( cdsSource : TZetaClientDataSet; const sRaiz,  sTag : string; nCuantos, iRecInicial : integer) : string;
function GeneraXMLFromDatasetForceInt( cdsSource : TZetaClientDataSet; const sRaiz,  sTag : string;  slForceIntAttributes : TStringList) : string;

Const
K_RENGLON_NUEVO = -2;
implementation

{$R *.DFM}

uses DCliente,
     DDiccionario,
     FAutoClasses,
     ZAccesosMgr,
     //ZAccesosTress,
     ZetaDialogo,
     ZReportTools,
     ZReportConst,
     ZetaCommonClasses,
     ZetaCommonTools;


function GeneraXMLFromDataset( cdsSource : TZetaClientDataSet; const sRaiz,  sTag : string) : string;
var
     FXMLTools: TdmXMLTools;
begin
     FXMLTools := TdmXMLTools.Create( nil );
     Result := '';

     try
       if cdsSource <> nil  then
       begin
          with FXMLTools do
          begin
               RevXMLBuildDataSet( cdsSource, sRaiz , sTag, cdsSource.RecordCount );
               XMLEnd;
               Result := XML.Text;
          end;
          StringReplace(Result, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', [rfReplaceAll, rfIgnoreCase] )

       end;
     finally
          FreeAndNil( FXMLTools );
     end;

end;


function GeneraXMLFromDatasetLimit( cdsSource : TZetaClientDataSet; const sRaiz,  sTag : string;  nCuantos, iRecInicial : integer) : string;
var
     FXMLTools: TdmXMLTools;
     ArchivoDestino : string;
begin
     FXMLTools := TdmXMLTools.Create( nil );
     Result := '';

     try
       if cdsSource <> nil  then
       begin
          with FXMLTools do
          begin
          // Cursor: TDataset; const sTag, sName: String; const iRecCuantos: Integer = 0; const iRecInicial: Integer = 1; false  ;false;   true );
               RevXMLBuildDataSet( cdsSource, sRaiz , sTag, nCuantos, -1 );
               XMLEnd;
               Result := XML.Text;
          end;
          StringReplace(Result, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', [rfReplaceAll, rfIgnoreCase] )

       end;
     finally
          FreeAndNil( FXMLTools );
     end;

end;


function GeneraXMLFromDatasetForceInt( cdsSource : TZetaClientDataSet; const sRaiz,  sTag : string;  slForceIntAttributes : TStringList) : string;
var
     FXMLTools: TdmXMLTools;
     ArchivoDestino : string;
begin
     FXMLTools := TdmXMLTools.Create( nil );
     Result := '';

     try
       if cdsSource <> nil  then
       begin
          with FXMLTools do
          begin
               AtributosForceInt := slForceIntAttributes;
               AtributosForceInt.Sort;
               RevXMLBuildDataSet( cdsSource, sRaiz , sTag, cdsSource.RecordCount );
               XMLEnd;
               Result := XML.Text;
          end;
          StringReplace(Result, '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>', '', [rfReplaceAll, rfIgnoreCase] )

       end;
     finally
          FreeAndNil( FXMLTools );
     end;

end;



procedure TdmReportesXML.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     {$ifdef FALSE}
     {$endif}
   {  if Assigned( FResultado ) then
        FResultado := nil;}
end;

{ *********** TdmReportGenerator ( Reportes para Excel ) ************* }

function TdmReportesXML.GetDerechosClasificacion( const eClasificacion: eClasifiReporte): Integer;
begin
     Result := dmDiccionario.GetDerechosClasifi(eClasificacion);

end;

procedure TdmReportesXML.AddColumn( Fuente: TField );
begin
     with Resultado do
     begin
          case Fuente.DataType of
               ftString: AddStringField( Fuente.FieldName, Fuente.Size );
               ftSmallint: AddIntegerField( Fuente.FieldName );
               ftInteger: AddIntegerField( Fuente.FieldName );
               ftWord: AddIntegerField( Fuente.FieldName );
               ftBoolean: AddBooleanField( Fuente.FieldName );
               ftFloat: AddFloatField( Fuente.FieldName );
               ftCurrency: AddFloatField( Fuente.FieldName );
               ftDate: AddDateField( Fuente.FieldName );
               ftTime: AddDateField( Fuente.FieldName );
               ftDateTime: AddDateField( Fuente.FieldName );
          end;
     end;
end;

procedure TdmReportesXML.AddData( Fuente: TField );
var
   sCampo: String;
   Destino: TField;
begin
     with Resultado do
     begin
          sCampo := Fuente.FieldName;
          Destino := FieldByName( sCampo );
          case Fuente.DataType of
               ftString: Destino.AsString := Fuente.AsString;
               ftSmallint: Destino.AsInteger := Fuente.AsInteger;
               ftInteger: Destino.AsInteger := Fuente.AsInteger;
               ftWord: Destino.AsInteger := Fuente.AsInteger;
               ftBoolean: Destino.AsBoolean := Fuente.AsBoolean;
               ftFloat: Destino.AsFloat := Fuente.AsFloat;
               ftCurrency: Destino.AsFloat := Fuente.AsFloat;
               ftDate: Destino.AsDateTime := Fuente.AsDateTime;
               ftTime: Destino.AsDateTime := Fuente.AsDateTime;
               ftDateTime: Destino.AsDateTime := Fuente.AsDateTime;
          end;
     end;
end;

procedure TdmReportesXML.DoOnGetReportes( const lResultado: Boolean );
var
   iDerecho: Integer;
begin
     if lResultado then
     begin
          FReportName := cdsReporte.FieldByName( 'RE_NOMBRE' ).AsString;



          //cdsReporte.Edit;
          //cdsReporte.FieldByName('RE_FILTRO').AsString := '@NOMINA.NO_TIMBRO = 0 and NOMINA.CB_PATRON in (''1'') and NOMINA.PE_TIPO = 1 and NOMINA.PE_YEAR = 2011 and NOMINA.NO_STATUS = 6';
          //cdsReporte.Post;
         { if ( iDerecho > 0 ) then
          begin
               if not ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_CONSULTA ) then
               begin
                    raise Exception.Create( Format( 'No Tiene Derecho De Acceso Al Reporte %d', [ cdsReporte.FieldByName( 'RE_CODIGO' ).AsInteger ] ) );
               end;
          end; }
          if ( eTipoReporte( cdsReporte.FieldByName( 'RE_TIPO' ).AsInteger ) <> trListado ) then
          begin
               raise Exception.Create( Format( 'El Reporte %d No Es Un %s', [ cdsReporte.FieldByName( 'RE_CODIGO' ).AsInteger, ZetaCommonLists.ObtieneElemento( lfTipoReporte, Ord( trListado ) ) ] ) );
          end;
     end;
end;

procedure TdmReportesXML.DoOnGetResultado( const lResultado: Boolean; const Error: String );
var
   i, iCampos, iGrupo: Integer;
begin
     with cdsResultados do
     begin
          if lResultado and ( RecordCount > 0 ) then
          begin
               AsignaListas;
               iCampos := 0;
               iGrupo := -1;
               { Crear Campos }
               if SoloTotales then
               begin
                    { Encontrar el Grupo Encabezado M�s Interno }
                    iGrupo := ( Grupos.Count - 1 );
                    if ( iGrupo > 0 ) then
                    begin
                         { Asignar Los Campos Del Grupo Encabezado M�s Interno }
                         with TGrupoOpciones( Grupos.Objects[ iGrupo ] ).ListaEncabezado do
                         begin
                              for i := 0 to ( Count - 1 ) do
                              begin
                                   with TCampoListado( Objects[ i ] ) do
                                   begin
                                        if ( Calculado <> K_RENGLON_NUEVO ) then
                                        begin
                                             if ( SQLColumna.PosCampo  >= 0 ) then
                                             begin
                                                  AddColumn( Fields[ SQLColumna.PosCampo ] );
                                                  Inc( iCampos );
                                             end;
                                        end;
                                   end;
                              end;
                         end;
                    end;
               end;
               with Campos do
               begin
                    for i := 0 to ( Count - 1 ) do
                    begin
                         with TCampoListado( Objects[ i ] ) do
                         begin
                              if ( Calculado <> K_RENGLON_NUEVO ) then
                              begin
                                   if SoloTotales then
                                   begin
                                        if ( SQLColumna.Totalizacion <> ocNinguno ) then
                                        begin
                                             AddColumn( FieldByName( NombreTotal ) );
                                             Inc( iCampos );
                                        end;
                                   end
                                   else
                                   begin
                                        if ( SQLColumna.PosCampo >= 0 ) then
                                        begin
                                               AddColumn( Fields[ SQLColumna.PosCampo ] );
                                               Inc( iCampos );
                                        end;
                                   end;
                              end;
                         end;
                    end;
               end;
               if ( iCampos > 0 ) then
               begin
                    Resultado.CreateTempDataset;
                    { Agregar Titulos }
                    if SoloTotales and ( iGrupo > 0 ) then
                    begin
                         { Asignar Los T�tulos De Los Campos Del Grupo Encabezado M�s Interno }
                         with TGrupoOpciones( Grupos.Objects[ iGrupo ] ).ListaEncabezado do
                         begin
                              for i := 0 to ( Count - 1 ) do
                              begin
                                   with TCampoListado( Objects[ i ] ) do
                                   begin
                                        if ( Calculado <> K_RENGLON_NUEVO ) then
                                        begin
                                             if ( SQLColumna.PosCampo >= 0 ) then
                                             begin
                                                  Resultado.FieldByName( Fields[ SQLColumna.PosCampo ].FieldName ).DisplayLabel := Titulo;
                                             end;
                                        end;
                                   end;
                              end;
                         end;
                    end;
                    with Campos do
                    begin
                         for i := 0 to ( Count - 1 ) do
                         begin
                              with TCampoListado( Objects[ i ] ) do
                              begin
                                   if ( Calculado <> K_RENGLON_NUEVO ) then
                                   begin
                                        if SoloTotales then
                                        begin
                                             if ( SQLColumna.Totalizacion <> ocNinguno ) then
                                             begin
                                                  Resultado.FieldByName( NombreTotal ).DisplayLabel := Titulo;
                                             end;
                                        end
                                        else
                                        begin
                                             if ( SQLColumna.PosCampo >= 0 ) then
                                             begin
                                                  Resultado.FieldByName( Fields[ SQLColumna.PosCampo ].FieldName ).DisplayLabel := Titulo;
                                             end;
                                        end;
                                   end;
                              end;
                         end;
                    end;
                    { Copiar Datos }
                    First;
                    while not Eof do
                    begin
                         Resultado.Insert;
                         try
                            for i := 0 to ( Resultado.FieldCount - 1 ) do
                            begin
                                 AddData( FieldByName( Resultado.Fields[ i ].FieldName ) );
                            end;
                            Resultado.Post;
                         except
                               on Error: Exception do
                               begin
                                    Cancel;
                                    raise;
                               end;
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

function TdmReportesXML.Procesar( const sEmpresa: String; const iReporte: Integer; var sNombre: String): Integer;
begin
     Empresa := sEmpresa;
       Resultado := TZetaClientDataSet.Create(Self);
     FReportName := VACIO;
     if GetResultado( iReporte, False ) then
     begin
          Result := Resultado.RecordCount;
     end
     else
     begin
          Result := 0;
     end;
     sNombre := FReportName;
end;

function TdmReportesXML.GeneraXML( const sRaiz,  sTag : string) : string;
begin
     Result := GeneraXMLFromDataset( Resultado, sRaiz, sTag );
end;


procedure TdmReportesXML.DataModuleCreate(Sender: TObject);
begin
  inherited;
  SetLogFileName( 'LOG_GENERACION_NOMINA.TXT' );
  Resultado := TZetaClientDataSet.Create(Self);
end;

function TdmReportesXML.ProcesarToleraVacio(const sEmpresa: String;
  const iReporte: Integer; var sNombre: String;
  var iCount: integer): boolean;
begin
     Empresa := sEmpresa;
     Resultado := TZetaClientDataSet.Create(Self);
     FReportName := VACIO;

     Result :=  GetResultado( iReporte, False );

     if Result then
     begin
          iCount := Resultado.RecordCount;
     end
     else
     begin
          iCount := 0;
     end;
     
     sNombre := FReportName;
end;

end.
