unit WizardRecibosXEmpleado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,
  Timbres,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaDBTextBox, ExtCtrls, ZetaKeyLookup, Mask,
  DBCtrls,FTimbramexClasses, FTimbramexHelper, Grids, DBGrids, Buttons,
  ZetaSmartLists, FBaseReportes_DevEx, ZetaCommonLists, cxHyperLinkEdit,
  cxRadioGroup, URLMon, cxShellBrowserDialog, WinInet, WininetUtils,
  ZetaEdit, ZetaCommonClasses, DBClient,     DCatalogos, ZetaKeyCombo,
  ZetaCXGrid, TressMorado2013,ZetaKeyLookup_DevEx,cxMemo, ZetaFilesTools,
  cxButtonEdit, ActnList, UManejadorPeticiones, UPeticionWS, UThreadStack, UThreadPolling, UInterfaces,
  cxEditRepositoryItems, ZetaNumero, System.Actions, ZetaCXStateComboBox,
  cxImage, dxGDIPlusClasses, ZBaseShell, Vcl.ImgList, ZetaDespConsulta;



type
  TWizardRecibosXEmpleadoForm = class(TdxWizardControlForm, ILogBitacora)
    dxWizardControl1: TdxWizardControl;
    dxWizardControlPageTimbrado: TdxWizardControlPage;
    dxWizardControlPage2: TdxWizardControlPage;
    dataSourceTimbrar: TDataSource;
    dataSourceContribuyente: TDataSource;
    DataSourceCuentas: TDataSource;
    DataSourceRecibos: TDataSource;
    DataSourceErrores: TDataSource;
    DBEdit1: TDBEdit;
    cxGroupBox3: TcxGroupBox;
    PeriodoTipoLbl: TLabel;
    Label6: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label8: TLabel;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    iTipoNomina: TZetaTextBox;
    Label1: TLabel;
    PE_TIMBRO: TZetaDBTextBox;
    DatasetPeriodo: TDataSource;
    CT_CODIGO: TZetaKeyLookup;
    cxShellBrowserDialogRecibos: TcxShellBrowserDialog;
    dxWizardControlPageEmpleados: TdxWizardControlPage;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    cbOrden: TZetaKeyCombo;
    FiltrosGB: TGroupBox;
    sCondicionLBl: TLabel;
    sFiltroLBL: TLabel;
    sFiltro: TcxMemo;
    Seleccionar: TcxButton;
    BAgregaCampo: TcxButton;
    ECondicion: TZetaKeyLookup_DevEx;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    RBTodos: TcxRadioButton;
    RBRango: TcxRadioButton;
    RBLista: TcxRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    BInicial: TcxButton;
    BFinal: TcxButton;
    BLista: TcxButton;
    Label4: TLabel;
    cbGrupo: TZetaKeyCombo;
    dsArchivosDescargados: TDataSource;
    ActionListGrid: TActionList;
    Grid_AbrirArchivo: TAction;
    Grid_AbrirArchivoExplorer: TAction;
    cxEditRepositoryBotonesGrid: TcxEditRepository;
    ButtonsImpreso: TcxEditRepositoryButtonItem;
    ButtonsXML: TcxEditRepositoryButtonItem;
    cxGroupBox2: TcxGroupBox;
    cxRecibosImpresos: TcxCheckBox;
    cxRecibosXML: TcxCheckBox;
    GridPeriodosAnio: TZetaCXGrid;
    GridDBTableViewPeriodosAnio: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    DataSourceNominasXYear: TDataSource;
    cxGridDBColumn7: TcxGridDBColumn;
    PanelValorAEmpleado: TPanel;
    PanelValorAEmpleado_2: TPanel;
    EmpleadoPrettyName_DevEx: TLabel;
    ImagenStatus_DevEx: TcxImage;
    PanelValorEmpleado_1: TPanel;
    EmpleadoLBL_DevEx: TLabel;
    btnBuscarEmp_DevEx: TcxButton;
    btnEmplPrimero_DevEx: TcxButton;
    btnEmpAnterior_DevEx: TcxButton;
    btnEmpSiguiente_DevEx: TcxButton;
    btnEmpUltimo_DevEx: TcxButton;
    EmpleadoNumeroCB_DevEx: TcxStateComboBox;
    NavegacionCB_DevEx: TcxStateComboBox;
    Image24_StatusEmpleado: TcxImageList;
    DSNominasXYearAProcesar: TDataSource;
    ImageButtons: TcxImageList;
    btnSeleccionarTodos: TcxButton;
    btnDesmarcarTodas: TcxButton;
    btREfrescaPeriodos: TcxButton;
    ZetaSpeedButton2: TcxButton;
    Panel5: TPanel;
    cxGroupBoxResultados: TcxGroupBox;
    cxLabel3: TcxLabel;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    ZetaCXGrid1DBTableView1NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView1DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2: TcxGridDBTableView;
    ZetaCXGrid1DBTableView2NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2DETALLE: TcxGridDBColumn;
    ZetaCXGrid1DBTableView3: TcxGridDBTableView;
    ZetaCXGrid1DBTableView3NUMERO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView3ERRORID: TcxGridDBColumn;
    ZetaCXGrid1DBTableView3DESCRIPCION: TcxGridDBColumn;
    ZetaCXGrid1Level1: TcxGridLevel;
    Panel2: TPanel;
    ZetaSpeedButton1: TcxButton;
    Panel6: TPanel;
    cxGroupBoxAvance: TcxGroupBox;
    btBuscarFolder: TcxButton;
    btCancelarDescarga: TcxButton;
    ZetaCXGrid2: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridDBTableViewColumn2: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn1: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumnaGrupo: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn3: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn4: TcxGridDBColumn;
    ZetaCXGrid2Level1: TcxGridLevel;
    Panel3: TPanel;
    cxLabelRecibos: TcxLabel;
    cxTextEditStatus: TcxTextEdit;
    cxLabel5: TcxLabel;
    cxLabelAvance: TcxLabel;
    cxProgressBarEnvio: TcxProgressBar;
    cxLabelCarpeta: TcxLabel;
    cxFolderRecibosTxt: TcxTextEdit;
    cxLabel2: TcxLabel;
    Panel1: TPanel;
    cxLabel4: TcxLabel;
    Panel4: TPanel;
    cxButtonExportExcelLista: TcxButton;
    Panel7: TPanel;
    Advertencia: TcxLabel;
    cxImage1: TcxImage;
    cxTextEditCarpetaRecibos: TcxTextEdit;
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure CT_CODIGOValidLookup(Sender: TObject);
    procedure Exportar;
    procedure ExportarListaArchivos;
    procedure ZetaSpeedButton1Click(Sender: TObject);
    procedure IniciarProgress;
    procedure btBuscarFolderClick(Sender: TObject);
    procedure btCancelarDescargaClick(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure cambiarSeleccion (elegir: string);
    procedure RBListaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NavegacionCBSelect(Sender: TObject);
    procedure _E_BuscarEmpleadoExecute(Sender: TObject);
    procedure _Emp_UltimoExecute(Sender: TObject);
    //procedure EmpleadoNumeroCB_DevExLookUp(Sender: TObject; var lOk: Boolean);
    procedure _Emp_PrimeroExecute(Sender: TObject);
    Procedure AgregaFiltroFolio;
    procedure EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    //procedure _E_BuscarEmpleadoExecute(Sender: TObject);
    procedure Grid_AbrirArchivoExecute(Sender: TObject);
    procedure Grid_AbrirArchivoExplorerExecute(Sender: TObject);
    procedure cxButtonExportExcelListaClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewColumn4GetProperties(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);

    procedure _Emp_AnteriorExecute(Sender: TObject);
    procedure _Emp_SiguienteExecute(Sender: TObject);
    procedure btREfrescaPeriodosClick(Sender: TObject);
    procedure ZetaSpeedButton2Click(Sender: TObject);
    procedure cxGridDBColumn7PropertiesChange(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodasClick(Sender: TObject);
    procedure DataSourceNominasXYearUpdateData(Sender: TObject);
    procedure DataSourceErroresUpdateData(Sender: TObject);
    procedure DataSourceErroresDataChange(Sender: TObject; Field: TField);
  private
   FContinuaDownload : boolean;
   FEjecuto : boolean;
   FVerificacion: Boolean;
   FEsEmpleadoTransferido : Boolean;

   FTipoRango: eTipoRangoActivo;
   FEmpleadoCodigo: String;
   FEmpleadoFiltro: String;
   FNombreCarpeta  :String;


   FParameterList: TZetaParams;
   FDescripciones: TZetaParams;
       { Private declarations }
    function RecibosServer : boolean;
    function GetRecibosDataSet : boolean;

    function GetArchivoFromURL( sURL,sDestinoPath, sFileName : string ) : string;
    function BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
    function RevisaDerechoForma( FormaConsulta: eFormaConsulta; const TipoDerecho: Integer ): Boolean;

    function GetRazonSocial : string;

    function GetGrupoName( sCodigoGrupo : string; var sGrupoDescripcion : string ) : string;

    function GetNombreArchivoElegido : string;
    procedure CambioEmpleadoActivo;
    procedure RefrescaEmpleadoActivos;
    procedure CargaNavegacion;
    procedure RefrescarNominasPorYear;
    procedure BuscarPrimerEmpleado;


  protected
    { Protected declarations }
    property ParameterList: TZetaParams read FParameterList;
    property Descripciones: TZetaParams read FDescripciones;
    property Verificacion: Boolean read FVerificacion write FVerificacion;

    function Verificar: Boolean; virtual;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure CargaListaVerificacion;
    procedure CargaParametros;
    //procedure CargaListaNominas(cdsNominasXYearAProcesar : );
    procedure CargaListaNominas;

    procedure SetVerificacion( const Value: Boolean );
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );
    procedure ApplyMinWidth;
    procedure AjustaGruposResultados;

    procedure SetStatus( sStatus : string );

    procedure SetCarpetaRecibos(sCarpetaRecibos: string);

  public


      FTotalRead , FTotalSize : TPesos;
      FFileNameDescarga : String;

      property TipoRango: eTipoRangoActivo read FTipoRango;
      procedure EscribirBitacoraLog( sMensaje : string );
      procedure EscribirBitacoraError( sMensaje : string );

      procedure CargaEmpleadoActivos;

const
     K_PANEL_EMPLEADO = 1;
     K_PANEL_NAVEGA = 4;

end;


  function DescargaEnProgreso(FileName : string; ATotalSize,  ATotalRead, AStartTime: DWORD): Boolean;

var
  WizardRecibosXEmpleadoForm: TWizardRecibosXEmpleadoForm;
  StatusEmpleado: array [0..3] of string = ('Activo', 'Reingreso','Baja','Antes ingreso');


function ShowWizardTimbrado : boolean;

implementation

uses
 ZetaClientTools,
 ZetaTipoEntidad,
 DTablas, Dcliente,
 DProcesos,
 DGlobal,
 ZGlobalTress,
 ZAccesosMgr,
 DBasicoCliente,
 ZetaBuscador_DevEx,
 ZConstruyeFormula,
 ZConfirmaVerificacion,
 ZetaCommonTools,
 ZetaBuscaEmpleado_DevEx,
 ZBaseSelectGrid_DevEx,
 ZBasicoSelectGrid_DevEx,
 FEmpleadoTimbradoGridSelect, ZetaClientDataSet,
 PDFMerge,
 ZetaBuscaEmpleado_DevExAvanzada, ZetaBuscaEmpleado_DevExTimbradoAvanzada;


{$R *.dfm}

function ShowWizardTimbrado : boolean;
begin
    Result := FALSE;
end;

procedure TWizardRecibosXEmpleadoForm.AjustaGruposResultados;
  begin
  
       {if cxGroupBoxResultados.Visible then
       begin
             cxGroupBoxAvance.Height := 321;
             cxGroupBoxAvance.Align := alTop;
             cxGroupBoxResultados.Align := alClient;
       end
       else
       begin
             cxGroupBoxAvance.Align := alClient;
       end;  }
  end;


procedure TWizardRecibosXEmpleadoForm.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin

  if AKind = wcbkCancel then
  begin
         if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
         begin
               if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
               begin
                    ModalResult := mrCancel;
               end;
         end
         else
         begin
              ModalResult := mrCancel;
         end;
  end
  else if AKind = wcbkBack then
  begin
       dxWizardControl1.Buttons.Finish.Enabled := True;
       dxWizardControl1.Buttons.Cancel.Caption := 'Cancelar'; 
  end
  else if AKind = wcbkFinish then
  begin
    Ahandled := True;

    if ( not FEjecuto )then
    begin
         dxWizardControl1.Buttons.Finish.Enabled := False; 
    end;

    if (not FEjecuto) and RecibosServer then
    begin
         {ZInformation( Self.Caption, Format( 'El Timbrado de Nomina del contribuyente %s fue Registrado Exitosamente en el Sistema de Timbrado',
                       [dmInterfase.cdsRSocial.FieldByName('RS_NOMBRE').AsString] ) , 0);}
         //ModalResult := mrOk;
    end;

  end;

end;

function TWizardRecibosXEmpleadoForm.RecibosServer: boolean;
  function GetHeadFilename : string;
  begin
     with dmCliente do
     begin
          with GetDatosPeriodoActivo do
          begin
               Result := Format('Recibos_%s_%d_%s_%d', [dmCliente.Compania, dmCliente.YearDefault, ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) ), Numero] );
          end;
     end;


  end;


  procedure AgregaArchivoListo(iNumeroPeriodo: INTEGER; sTipo, sGrupo, sNombre : string );
  begin
       with dmInterfase.cdsArchivosDescargados do
       begin
            Append;
            FieldByName('PERIODO_NUMERO').AsInteger := iNumeroPeriodo;
            FieldByName('TIPO').AsString := sTipo;
            FieldByName('GRUPO').AsString := sGrupo;
            FieldByName('ARCHIVO').AsString := sNombre;
            Post;
       end;
  end;



//Metodo que se iria a una Helper Class
  function   GetRecibosServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No fue posible Consultar el Timbrado de la Nomina en el Sistema de Timbrado: ';
       K_TRUE = 'True';
       K_QUERY_FILTRO_NOMINASAPROCESAR = 'OBTENER = %s';
       K_QUERY_FILTRO_NOMINA_EMPLEADO = '(NOMINA.CB_CODIGO in ( %d ))';
       K_QUERY_FILTRO_RAZON_SOCIAL = 'RS_CODIGO = %s';
  var



     slErrores : TStringList;

     iContadorRazonesSociales, iContadorNominas, iContadorNominasAProcesar : integer;

     //cdsErroresGeneral : TZetaClientDataSet;

     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;

     slPeticiones, slPeticionesXML, slGruposPDF, slGrupoXML : TStringList;
     sReciboHeadName, sGrupoHeadName, sArchivoListo : string;

     broker : TManejadorPeticiones ;
     oPeticionWS : TPeticionWS;
     iContador: integer;
     I: Integer;

     sRazonSocial : string;
     iTipoPeriodo, iNumeroPeriodo: integer;
     iContadorNominasTimbras : integer;
     sEmpleadoGetRango: string;

     bObtener : Boolean;
     sObtener : string;


     function GetListaRecibosGrupo( sGrupo : string )  : TStringList;
     var
        iPeticion : integer;
     begin
          Result := TStringList.Create;

          for iPeticion := 0 to slGruposPDF.Count -1 do
          begin
               if ( slGruposPDF[iPeticion] = sGrupo ) then
               begin
                    Result.Add( slPeticiones[iPeticion] ) ;
               end;
          end
     end;

     procedure EsperarPeticion;
     var
        iCount : integer;
     begin
         iCount := 0;
         while oPeticionWS.lProceso do
         begin
           Inc( iCount );
           //iShowBuzz := iCount mod 4;
           //cxProgressBarEnvio.Position := iShowBuzz * 20;  cxProgressBarEnvio.Update;

           if ( iCount mod 4  = 0 ) then
              Application.ProcessMessages
           else
               DelayNPM( 200  );
         end;
         Application.ProcessMessages;
     end;

     procedure crearDatasetParaErrores;
     begin
          dmInterfase.cdsErroresGeneral := TZetaClientDataSet.Create( nil );
          with dmInterfase.cdsErroresGeneral do
          begin
                if ( Active ) then
                begin
                    EmptyDataSet
                end
                else
                begin
                     AddIntegerField( 'ORDEN' );
                     AddIntegerField( 'ERRORID' );
                     AddIntegerField( 'NUMERO' );
                     AddStringField('DESCRIPCION' , 255 );
                     AddStringField('DETALLE', 1024 ) ;
                     AddIntegerField( 'PERIODO_NUMERO' );
                     CreateTempDataset;
                end;
          end;
     end;


     procedure llenarDatasetDeErrores (iNumeroPeriodo : integer; sDescripcionError, sDetalle :string);
     begin
           with dmInterfase.cdsErroresGeneral do
           begin
                Append;
                FieldByName('PERIODO_NUMERO').AsInteger := iNumeroPeriodo;
                FieldByName('DESCRIPCION').AsString := sDescripcionError;
                FieldByName('DETALLE').AsString := sDetalle;
                Post;
           end;
     end;

     procedure crearDatasetParaArchivosDescargados;
     begin
          with dmInterfase.cdsArchivosDescargados do
          begin
                if ( Active ) then
                begin
                      EmptyDataSet
                end
                else
                begin
                     CreateTempDataset;
                end;
          end;
          //cxGroupBoxResultados.Visible := False;
     end;

     
     function MergeGrupoPDF(iNumeroPeriodo:integer; sGrupo, sNombreArchivoGeneral : string ) : boolean;
     var
        slGrupoPDF : TStringList;
        sGrupoNameArchivo, sArchivo, sGrupoDescripcion : string;
        FPDFMerge : TPDFMerge;

     begin
          if strLleno( sGrupo ) then
          begin
               sGrupoNameArchivo := GetGrupoName(sGrupo, sGrupoDescripcion);
               sArchivo := Format( '%s_%s' , [sGrupoNameArchivo, sNombreArchivoGeneral] );
          end
          else
              sArchivo :=  sNombreArchivoGeneral;

          slGrupoPDF := GetListaRecibosGrupo( sGrupo );

          SetStatus( 'Generando archivo: ' + sArchivo  + '...'  );
          try
              oPeticionWS := TPeticionWS.Create;
              oPeticionWS.tipoPeticion := peticionMergePDF;
              oPeticionWS.sArchivoDestino := sArchivo;
              oPeticionWS.slGrupoPDF := slGrupoPDF;
              oPeticionWS.lProceso := TRUE;
              broker.AgregarPeticionWS( oPeticionWS );

              EsperarPeticion;

              Result := oPeticionWS.lResultadoMerge;

              if Result then
                 AgregaArchivoListo(iNumeroPeriodo, 'Impreso', sGrupoDescripcion,  ExtractFilePath( slGrupoPDF[0] ) +  sArchivo);
          finally
               SetStatus( VACIO );
          end;

     end;

     procedure TomarRecibosImpresos(iNumeroPeriodo: integer; cdsPeriodoNominaAProcesar : TZetaClientDataset);
     var
         iPeticion, iTipoPaquete : integer;
         sGrupoActual, sGrupoDescripcion : string;
         sArchivoTmp, sArchivoFinal : string;

         slArchivo_HTML : TStringList;

         procedure InitArchivoHTML;
         begin
          slArchivo_HTML := TStringList.Create;
          slArchivo_HTML.Add( '<html><body> ');
          slArchivo_HTML.Add( '<style type="text/css">');
          slArchivo_HTML.Add( 'html, body { margin: 0px; padding: 0px; border: 0px; color: #000; background: #fff; } ');
          slArchivo_HTML.Add( 'html, body, p, th, td, li, dd, dt { font: 0.9em Arial, Helvetica, sans-serif; }  ');
          slArchivo_HTML.Add( ' h1, h2, h3, h4, h5, h6 { font-family: Arial, Helvetica, sans-serif; }       ');
          slArchivo_HTML.Add( ' h1 { font-size: 1.5em; } h2 { font-size: 1.0em; } h3 { font-size: 0.9em ; }   ');
          slArchivo_HTML.Add( ' h4 { font-size: 1.0em; } h5 { font-size: 0.9em; } h6 { font-size: 0.8em; }    ');
          slArchivo_HTML.Add( '  a:link { color: #00f; } a:visited { color: #009; } a:hover { color: #06f; } a:active { color: #0cf; }    ');
          slArchivo_HTML.Add( '</style>   ');
          slArchivo_HTML.Add( '<h1>Ligas para descargar Recibos<h1> ');


             with dmCliente do
             begin
                 with GetDatosPeriodoActivo do
                 begin
                       slArchivo_HTML.Add(  Format('<h2>Empresa:%s</h2><h3>A�o:%d N�mina:%s Periodo:%d</h3>', [dmCliente.Compania, dmCliente.YearDefault, ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) ), Numero] ) ) ;
                  end;
             end;


         end;

         procedure AppendArchivo( sURL, sNombre : string);
         begin
              slArchivo_HTML.Add( Format( '<p><a href="%s">%s</a></p>', [sURL, sNombre] ) );
         end;

         procedure SaveArchivoHTML( sNombre : string ) ;
         var
            sPathEmpleado,sNombreEmpleado , sCodigoEmpleado,sYear : string;
         begin
             with dmCliente do
             begin
              sNombreEmpleado :=  dmCliente.GetDatosEmpleadoActivo.Nombre;
              sCodigoEmpleado := IntToStr(dmCliente.GetDatosEmpleadoActivo.Numero);
              sYear := IntToStr(dmCliente.YearDefault);
             end;


              slArchivo_HTML.Add( '</body></html> ');

              {sPathEmpleado := cxFolderRecibosTxt.Text+'\'+sNombreEmpleado+'_'+sCodigoEmpleado+'_'+sYear;
              if DirectoryExists( sPathEmpleado ) then
              begin
                   slArchivo_HTML.SaveToFile( Format('%s\%s_Recibos.html', [ sPathEmpleado, sNombre] ) );
              end; }

         end;

     begin

          InitArchivoHTML;
          if (cbGrupo.Llave = 'NOAPLICA') then
             iTipoPaquete := 0
          else
          if (cbGrupo.Llave = 'NOMINA.CB_CODIGO') then
             iTipoPaquete := 1
          else
              iTipoPaquete := 2;



          slPeticiones := dmInterfase.GetPeriodoRecibosXEmpleado(TRUE, slGruposPDF, iTipoPaquete,cdsPeriodoNominaAProcesar);

          cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
//          cxStatus.Caption := 'Generando Recibos en Sistema de Timbrado...';
          SetStatus( 'Generando recibos en Sistema de Timbrado...' ) ;
          Application.ProcessMessages;

          if ( slPeticiones.Count > 0 ) then
              sGrupoActual := slGruposPDF[0]
          else
              sGrupoActual := VACIO;

          for iPeticion := 0 to slPeticiones.Count -1  do
          begin
              oPeticionWS := TPeticionWS.Create;
              oPeticionWS.tipoPeticion := peticionTomarRecibosPDF;
              oPeticionWs.cnxSoap := timbresSoap;
              oPeticionWS.sPeticion :=   slPeticiones[iPeticion];
              oPeticionWS.sURL := GetTimbradoURL;

              oPeticionWS.lProceso := TRUE;
              broker.AgregarPeticionWS( oPeticionWS );

              EsperarPeticion;

              sRespuesta := oPeticionWS.sRespuesta;              

              if ( iPeticion  < slGruposPDF.Count ) then
              begin
                 sGrupoHeadName := GetGrupoName( slGruposPDF[iPeticion], sGrupoDescripcion  );
              end;

              respuestaTimbramex := FTimbramexHelper.GetRecibosRespuesta(sRespuesta );

              //cxProgressBarEnvio.Position := 10.00 + ( (iPeticion+1) / slPeticiones.Count ) * 90.00    ;  cxProgressBarEnvio.Update;
              Application.ProcessMessages;

              dmInterfase.RespuestaTimbramex := respuestaTimbramex;
              //DatasourceErrores.DataSet := respuestaTimbramex.Resultado.ErroresDS;
              //cxGroupBoxResultados.Visible := not respuestaTimbramex.Resultado.ErroresDS.IsEmpty;

              with respuestaTimbramex.Resultado do
              begin

                  if StrLleno( ArchivoUrl ) then
                  begin
                      ArchivoNombre1 := IntToStr(dmCliente.GetDatosEmpleadoActivo.Numero)+'_'+ ArchivoNombre1;
                      sArchivoFinal := ArchivoNombre1;
                      FNombreCarpeta := sArchivoFinal;
                      sArchivoTmp :=  Format( '%d_%s_%s' , [iPeticion,  slGruposPDF[iPeticion], ArchivoNombre1] );
                      sArchivoListo := GetArchivoFromURL(ArchivoUrl,  cxFolderRecibosTxt.Text,  sArchivoTmp );

                      AppendArchivo( ArchivoUrl, sArchivoTmp );

                      slPeticiones[iPeticion] := sArchivoListo;
                  end;
              end;

              if respuestaTimbramex.Resultado.Errores then
                 Break;

              if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                 Break;

               if ( iPeticion  <  slPeticiones.Count - 1 ) then
               begin
                  if ( slGruposPDF[iPeticion] <> slGruposPDF[iPeticion+1] )  then
                  begin
                       MergeGrupoPDF(iNumeroPeriodo, slGruposPDF[iPeticion], sArchivoFinal);

                  end;
               end
               else
               begin
                 //En caso de que sea el ultimo paquete y sea huerfano
                  if ( iPeticion  = slPeticiones.Count -1 ) then
                      MergeGrupoPDF(iNumeroPeriodo,  slGruposPDF[iPeticion], sArchivoFinal );
               end;
               cxProgressBarEnvio.Position := 10.00 + ( (iPeticion+1) / slPeticiones.Count ) * 90.00    ;  cxProgressBarEnvio.Update;
               Application.ProcessMessages;
          end;

          SaveArchivoHTML( FNombreCarpeta );
//          cxStatus.Caption := VACIO;
          SetStatus( VACIO );
      end;

     procedure TomarRecibosXML(iNumeroPeriodo : integer; cdsPeriodoNominaAProcesar : TZetaClientDataset);
     var
         iPeticion, iTipoPaquete: integer;
         sGrupoDescripcion : string; 
     begin

          if (cbGrupo.Llave = 'NOAPLICA') then
             iTipoPaquete := 0
          else
          if (cbGrupo.Llave = 'NOMINA.CB_CODIGO') then
             iTipoPaquete := 1
          else
              iTipoPaquete := 2;



         cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
//         cxStatus.Caption := 'Generando Recibos XML en Sistema de Timbrado...';
         SetStatus( 'Generando Recibos XML en Sistema de Timbrado...' );
         Application.ProcessMessages;

         //En el Caso de XML debe generarse un archivo por Grupo
         slPeticionesXML := dmInterfase.GetPeriodoRecibosXEmpleado(FALSE, slGrupoXML, iTipoPaquete, cdsPeriodoNominaAProcesar);

         for iPeticion := 0 to slPeticionesXML.Count -1  do
         begin             
             oPeticionWS := TPeticionWS.Create;
             oPeticionWS.tipoPeticion := peticionTomarRecibosXML;
             oPeticionWs.cnxSoap := timbresSoap;
             oPeticionWS.sPeticion :=   slPeticionesXML[iPeticion];
             oPeticionWS.sURL := GetTimbradoURL;

             oPeticionWS.lProceso := TRUE;
             broker.AgregarPeticionWS( oPeticionWS );

             EsperarPeticion;

             sRespuesta := oPeticionWS.sRespuesta;

             if ( iPeticion  < slGrupoXML.Count ) then
                sGrupoHeadName := GetGrupoName( slGrupoXML[iPeticion], sGrupoDescripcion  );

             respuestaTimbramex := FTimbramexHelper.GetRecibosRespuesta(sRespuesta );

             cxProgressBarEnvio.Position := 10.00 + ( (iPeticion+1) / slPeticionesXML.Count ) * 100.00    ;  cxProgressBarEnvio.Update;
             Application.ProcessMessages;

             dmInterfase.RespuestaTimbramex := respuestaTimbramex;

             //DatasourceErrores.DataSet := respuestaTimbramex.Resultado.ErroresDS;


             //cxGroupBoxResultados.Visible := not respuestaTimbramex.Resultado.ErroresDS.IsEmpty;

             with respuestaTimbramex.Resultado do
             begin
                 if StrLleno( ArchivoUrl ) then
                 begin
                     ArchivoNombre1 := IntToStr(dmCliente.GetDatosEmpleadoActivo.Numero)+'_'+ ArchivoNombre1;
                     FNombreCarpeta := ArchivoNombre1;
                     SetCarpetaRecibos(FNombreCarpeta);
                     if strLleno(sGrupoHeadName) then
                        sArchivoListo := GetArchivoFromURL(ArchivoUrl,  cxFolderRecibosTxt.Text,  sGrupoHeadName+ '_' + ArchivoNombre1 )
                     else
                        sArchivoListo := GetArchivoFromURL(ArchivoUrl,  cxFolderRecibosTxt.Text,  ArchivoNombre1 );

                     AgregaArchivoListo(iNumeroPeriodo, 'XML', sGrupoDescripcion, sArchivoListo);
                 end;
             end;

             if respuestaTimbramex.Resultado.Errores then
                Break;

             if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                Break;

         end;
     end;

      procedure CargaParametrosNominasporEmpleado (sRazon, sEmpleadoGetRango: string;  iTipoPeriodo, iNumeroPeriodo: integer);
      var
        PeriodoNumero : integer;
      begin
           with ParameterList do
           begin
                AddString( 'RangoLista', sEmpleadoGetRango );
                AddString( 'Condicion', GetCondicion );
                AddString( 'Filtro', GetFiltro );

                AddString( 'RazonSocial',  sRazon );
                AddInteger( 'StatusAnterior', ord(estiTimbrado) );
                AddInteger( 'StatusNuevo',  ord(estiTimbrado) ) ;

                AddInteger( 'Year',dmCliente.YearDefault );
                AddInteger( 'Tipo',ord( eTipoPeriodo(iTipoPeriodo) ) );
                AddInteger( 'Numero', iNumeroPeriodo{dmCliente.PeriodoNumero} );


                PeriodoNumero := ParameterList.ParamByName('Numero').AsInteger;


                if ( cbGrupo.Llave = 'NOAPLICA' ) then
                    AddString( 'Orden', cbOrden.Llave + ',' + cbOrden.Llave  )
                else
                    AddString( 'Orden', cbGrupo.Llave + ',' + cbOrden.Llave  );
           end;

           with Descripciones do
           begin
                AddString( 'Lista', GetRango );
                AddString( 'Condicion', GetCondicion );
                AddString( 'Filtro', GetFiltro );
           end;

           with dmCliente do
           begin
                CargaActivosIMSS( ParameterList );
                CargaActivosSistema( ParameterList );
           end;
      end;

  begin

     Result := False;
     oCursor := Screen.Cursor;

     Screen.Cursor := crHourglass;
     broker := TManejadorPeticiones.Create( ILogBitacora( Self ) );

     AjustaGruposResultados;
     try

          sServer := '';
          sGrupoHeadName:= '';
          sServer := GetTimbradoServer;
          timbresSoap := nil;
          IniciarProgress;
               {bObtener := dmInterfase.cdsNominasXYearAProcesar.FieldByName('OBTENER').AsBoolean;
               sObtener := dmInterfase.cdsNominasXYearAProcesar.FieldByName('OBTENER').AsString; }
          //----------------------------------INICIA FOR-----------------------------------------------
          slErrores := TStringList.Create;
          crearDatasetParaErrores;
          crearDatasetParaArchivosDescargados;

          with dmInterfase.cdsNominasXYearAProcesar do
          begin
               Filter := Format( K_QUERY_FILTRO_NOMINASAPROCESAR, [ EntreComillas( K_TRUE )] );
               Filtered := true;
               iContadorNominasAProcesar := RecordCount;
          end;

          with dmInterfase.cdsNominasXYearAProcesar do
          begin
          First;
             while ( not EOF ) do
             begin


          if  dmInterfase.cdsNominasXYearAProcesar.IsEmpty  then
          begin
               dmCliente.RazonSocial := VACIO;
          end
          else
          begin
               dmInterfase.cdsRSocial.Filter := Format( K_QUERY_FILTRO_RAZON_SOCIAL, [ EntreComillas( dmInterfase.cdsNominasXYearAProcesar.FieldByName('RS_CODIGO').AsString )] );
               dmInterfase.cdsRSocial.Filtered := true;
               dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
               CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
          end;

          iTipoPeriodo := dmInterfase.cdsNominasXYearAProcesar.FieldByName('PE_TIPO').AsInteger;
          iNumeroPeriodo := dmInterfase.cdsNominasXYearAProcesar.FieldByName('Numero').AsInteger;
          sRazonSocial := dmInterfase.cdsNominasXYearAProcesar.FieldByName('RS_CODIGO').AsString;
          sEmpleadoGetRango := Format( K_QUERY_FILTRO_NOMINA_EMPLEADO, [ dmCliente.Empleado ] );

          CargaParametrosNominasporEmpleado(sRazonSocial,sEmpleadoGetRango,iTipoPeriodo,iNumeroPeriodo);
          CargaListaVerificacion;

          with dmProcesos.cdsDataSetTimbrados do
          begin
               iContadorNominasTimbras := RecordCount;
               if ( dmProcesos.cdsDataSetTimbrados = nil ) or ( dmProcesos.cdsDataSetTimbrados.IsEmpty ) then
               begin
                    sRazonSocial := 'No Hay Nominas para ese empleado';
               end
               else
               begin

                    {with dmInterfase.cdsArchivosDescargados do
                    begin
                         if ( Active ) then
                             EmptyDataSet
                         else
                             CreateTempDataset;
                    end;}
                    ZetaDBGridDBTableView.ApplyBestFit();

                    if ( sServer <> '' ) then
                    begin
                       SetStatus( 'Conectandose a servidor de Sistema de Timbrado...' ) ;
                       timbresSoap := Timbres.GetTimbresFiscalesSoap(False, GetTimbradoURL );

                       if ( timbresSoap <> nil ) then
                       begin
                            Result := TRUE;
                            SetStatus ( 'Formando petici�n...' );
                            cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;

                            if ( cxRecibosImpresos.Checked ) then
                            begin

                                 TomarRecibosImpresos(iNumeroPeriodo, dmInterfase.cdsNominasXYearAProcesar);

                                 if respuestaTimbramex.Resultado.Errores then
                                 begin
                                   //zError( TIMBRADO_ERR_TIMBRAMEX , respuestaTimbramex.Bitacora.Eventos.Text  , 0);

                                    llenarDatasetDeErrores(iNumeroPeriodo, TIMBRADO_ERR_TIMBRAMEX, respuestaTimbramex.Bitacora.Eventos.Text);

                                   slErrores.Add(TIMBRADO_ERR_TIMBRAMEX+': '+respuestaTimbramex.Bitacora.Eventos.Text);
                                   cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                                   Result := FALSE;
                                 end
                                 else
                                 if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                                 begin
                                    cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                                    //zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX , 0);

                                    llenarDatasetDeErrores(iNumeroPeriodo, K_ERR_TIMBRAMEX, K_NOPOSIBLE_TIMBRAMEX);


                                    slErrores.Add(K_ERR_TIMBRAMEX+': '+K_NOPOSIBLE_TIMBRAMEX);
                                 end
                                 else
                                 begin
                                      SetStatus( VACIO );
                                      dxWizardControl1.Buttons.Finish.Enabled := False;
                                      dxWizardControl1.Buttons.Cancel.Caption := 'Salir';
                                      cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;
                                      REsult := TRUE;
                                 end;


                            end
                            else
                                Result:=TRUE;


                            if ( cxRecibosXML.Checked ) and Result then
                            begin

                                 TomarRecibosXML(iNumeroPeriodo, dmInterfase.cdsNominasXYearAProcesar);

                                 if respuestaTimbramex.Resultado.Errores then
                                 begin
                                   //zError( TIMBRADO_ERR_TIMBRAMEX , respuestaTimbramex.Bitacora.Eventos.Text  , 0);

                                   llenarDatasetDeErrores(iNumeroPeriodo, TIMBRADO_ERR_TIMBRAMEX, respuestaTimbramex.Bitacora.Eventos.Text);

                                   slErrores.Add(TIMBRADO_ERR_TIMBRAMEX+': '+respuestaTimbramex.Bitacora.Eventos.Text);
                                   cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                                   Result := FALSE;
                                 end
                                 else
                                 if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                                 begin
                                    cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;

                                    //zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX , 0);

                                    llenarDatasetDeErrores(iNumeroPeriodo, K_ERR_TIMBRAMEX, K_NOPOSIBLE_TIMBRAMEX);

                                    slErrores.Add(K_ERR_TIMBRAMEX+': '+K_NOPOSIBLE_TIMBRAMEX);
                                    Result := FALSE;
                                 end
                                 else
                                 begin
                                      SetStatus( VACIO );
                                      dxWizardControl1.Buttons.Finish.Enabled := False;
                                      dxWizardControl1.Buttons.Cancel.Caption := 'Salir';
                                      cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;
                                      REsult := TRUE;
                                 end;

                            end;

                            //if  Result  then
                               //zInformation( 'Obtener Recibos Timbrados',Format ('El proceso de descarga de recibos para n�mina %s #%d ha terminado exitosamente',[ObtieneElemento( lfTipoPeriodo, ord( dmCliente.PeriodoTipo ) ) , iNumeroPeriodo]), 0)
                       end;
                    end
                    else
                    begin
                        SetStatus( VACIO );
                        zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
                    end;
               end;

          end;

              dmInterfase.cdsRSocial.Filtered := false;

          Next;
          DatasourceErrores.DataSet := dmInterfase.cdsErroresGeneral;
          end;
          end;
          //----------------------------------TERMINA FOR-----------------------------------------------

          if slErrores.Count > 0 then
          begin
               //slErrores.SaveToFile('Errores en Recibos por Empleado.txt');
               //ZWarning( 'Obtener Recibos Timbrados',Format ('El proceso de descarga de recibos para n�mina por empleado ha terminado con errores' + CR_LF + 'Detalle: %s',['Revisar bit�cora del proceso en la ra�z']), 0, mbOK);
               ZWarning( 'Obtener Recibos Timbrados por Empleado','El proceso de descarga de recibos para n�mina por empleado ha terminado con errores' , 0, mbOK);
               //cxGroupBoxResultados.Visible := True;
          end
          else
          begin
               zInformation( 'Obtener Recibos Timbrados por Empleado','El proceso de descarga de recibos para n�mina por empleado ha terminado exitosamente', 0);
               //cxGroupBoxResultados.Visible := False;
          end;

          //DatasourceErrores.DataSet := cdsErroresGeneral;

          slErrores.Free;
          //FreeAndNil(cdsErroresGeneral);

       except on Error: Exception do
              begin
                 //zError( Self.Caption, K_SIN_CONECTIVIDAD  + CR_LF + 'Detalle: '+ Error.Message , 0);
                 ZWarning( 'Obtener Recibos Timbrados',Format ('El proceso de descarga de recibos para n�mina %s #%d ha terminado con errores' + CR_LF + 'Detalle: %s',[ObtieneElemento( lfTipoPeriodo, ord( eTipoPeriodo(iTipoPeriodo)  {dmCliente.PeriodoTipo} ) ) , iNumeroPeriodo, Error.Message]), 0, mbOK);
              end;
       end;


       AjustaGruposResultados;

       Screen.Cursor := oCursor;

  end;

var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     Result := GetRecibosServer;

     Screen.Cursor := oCursor;
end;

Procedure TWizardRecibosXEmpleadoForm.AgregaFiltroFolio;
begin
     with dmCatalogos.cdsFolios do
     begin
          Conectar;
          first;
          while (not eof) do
          begin
               if( fieldByName('FL_DESCRIP').AsString <> VACIO) then      //cambiar a strVacio()
               begin
                    cbOrden.Lista.Add( Format( 'NOMINA.NO_FOLIO_%d=Folio %d - %s ' , [fieldByName('FL_CODIGO').asInteger,fieldByName('FL_CODIGO').asInteger, fieldByName('FL_DESCRIP').AsString] ));
               end;
               next;
          end;
     end;
end;


procedure TWizardRecibosXEmpleadoForm.EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;

var
  iEmpleado : integer;

begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;

     try
        if FEsEmpleadoTransferido then
        begin
               {if ( dmCliente.Empleado <> EmpleadoNumeroCB_DevEx.ValorEntero ) then
               begin
                    if dminterfase.GetEmpleadoTransferidoParaActivo( EmpleadoNumeroCB_DevEx.ValorEntero, dmCliente.Empresa ) then
                    begin
                         with dmcliente.cdsEmpleado do
                         begin
                              //FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                              iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                              iEmpleado := dmCliente.Empleado;
                         end;
                         //FEmpleadoBOF := False;
                         //FEmpleadoEOF := False;
                    end;
                    CambioEmpleadoActivo
               end;}
               dmcliente.SetEmpleadoNumeroDeTransferido(EmpleadoNumeroCB_DevEx.ValorEntero);
               CambioEmpleadoActivo;
               iEmpleado := dmCliente.Empleado;
        end
        else
        begin
              lOk := dmCliente.SetEmpleadoNumero( EmpleadoNumeroCB_DevEx.ValorEntero );
              if lOk then
                 CambioEmpleadoActivo
              else
                  ZetaDialogo.zInformation( 'Error', '! Empleado no encontrado !', 0 );
        end;
     finally
            Screen.Cursor := oCursor;

     end;
end;

procedure TWizardRecibosXEmpleadoForm.FormCreate(Sender: TObject);

       
    procedure LLenarComboGrupo;
    var
       iNivel : integer;
       sNivelName : string;
       sLlaveDefault : string;
    begin
      sLlaveDefault := VACIO;

      with cbGrupo.Lista do
      begin
          BeginUpdate;
          try
             Clear;
             Add( Format( 'NOAPLICA=%s' , ['No Aplica'] ));
             Add( Format( 'NOMINA.CB_CODIGO=%s' , ['Grupos de empleados (100 p/archivo)'] ));
             for iNivel := 1 to 9 do
             begin
                sNivelName := Global.GetGlobalString( K_GLOBAL_NIVEL1 + iNivel -1 );
                if strLleno( sNivelName ) then
                begin
                     Add( Format( 'NOMINA.CB_NIVEL%d=%s' , [iNivel, sNivelName] ));
                     if StrVacio( sLlaveDefault ) then
                     begin
                        sLlaveDefault := Format( 'NOMINA.CB_NIVEL%d' , [iNivel] );
                     end;
                end;
             end;
             sLlaveDefault := Format( 'NOMINA.CB_NIVEL%d' , [0] );
          finally
                 EndUpdate;
          end;
      end;
      cbGrupo.ItemIndex := 0;
      cbGrupo.Llave := sLlaveDefault;
    end;


    procedure LLenarComboOrden;
    var
       iNivel : integer;
       sNivelName : string;
    begin

      with cbOrden.Lista do
      begin
          BeginUpdate;
          try
             Clear;
             Add( 'NOMINA.CB_CODIGO=N�mero de Empleado' );
             for iNivel := 1 to 9 do
             begin
                sNivelName := Global.GetGlobalString( K_GLOBAL_NIVEL1 + iNivel -1 );
                if strLleno( sNivelName ) then
                begin
                     Add( Format( 'NOMINA.CB_NIVEL%d=%s' , [iNivel, sNivelName] ));
                end;
             end;
          finally
                 EndUpdate;
          end;
      end;
      AgregaFiltroFolio;
      cbOrden.ItemIndex := 0;
      cbOrden.Llave := 'NOMINA.CB_CODIGO';
    end;

    procedure InicializarFiltros;
    begin

     FEmpleadoCodigo := 'NOMINA' + '.CB_CODIGO';
     FParameterList := TZetaParams.Create;
     FDescripciones := TZetaParams.Create;
     ZBasicoSelectGrid_DevEx.ParametrosGrid := FDescripciones;

     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := VACIO; //IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     dmCatalogos.cdsCondiciones.Conectar;
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;

     LLenarComboOrden;
     LLenarComboGrupo; 

    end;

begin


     HelpContext:= H32135_TomarRecibosXEmpleado;

     btCancelarDescarga.Visible := FALSE;
     //cxLabelCarpeta.Visible := FALSE;
     //cxTextEditCarpetaRecibos.Visible := FALSE;


     SetStatus(VACIO);

     InicializarFiltros;

     with dmInterfase.cdsArchivosDescargados do
     begin
          if Active then
          begin
              EmptyDataSet;
             // Close;
          end
          else
            CreateTempDataset;
     end;

     dsArchivosDescargados.DataSet := dmInterfase.cdsArchivosDescargados;
      ZetaDBGridDBTableView.ApplyBestFit();


     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
              // sStatusNomina.Caption := //;;ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
               FechaInicial.Caption := FormatDateTime( FormatSettings.LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( FormatSettings.LongDateFormat, Fin );
          end;
     end;

     DatasetPeriodo.DataSet := dmInterfase.cdsPeriodosAfectadosTotal;


//    //if  dmInterfase.cdsPeriodosRecibos.IsEmpty  then
//    if  dmInterfase.cdsNominasXYear.IsEmpty  then
//    begin
//         dmCliente.RazonSocial := VACIO;
//    end
//    else
//    begin
//         //dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsPeriodosRecibos.FieldByName('RS_CODIGO').AsString, []) ;
//         dmInterfase.cdsRSocial.Locate('RS_CODIGO', dmInterfase.cdsNominasXYear.FieldByName('RS_CODIGO').AsString, []) ;
//         dmCliente.RazonSocial := dmInterfase.cdsRSocial.FieldByName('RS_CODIGO').AsString ;
//    end;



    DataSourceNominasXYear.DataSet := dmInterfase.cdsNominasXYear;
    //dataSourceTimbrar.DataSet :=  dmInterfase.cdsPeriodosRecibos;
    dataSourceTimbrar.DataSet := dmInterfase.cdsNominasXYear;
    dmTablas.cdsEstado.Conectar;

    CT_CODIGO.LookupDataset := dmInterfase.cdsCuentasTimbramex;
    dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
    //cxGroupBoxResultados.Visible := FALSE;
    AjustaGruposResultados;

    dmCatalogos.InitActivosNavegacion;
    CargaNavegacion;
    CargaEmpleadoActivos;

    RefrescarNominasPorYear;

end;

procedure TWizardRecibosXEmpleadoForm.Button1Click(Sender: TObject);
begin
     //memoPreview.Text :=  dmInterfase.GetRecibos;
end;

procedure TWizardRecibosXEmpleadoForm.CargaNavegacion;
begin
     with NavegacionCB_DevEx do
     begin
          ItemIndex := dmCatalogos.LlenaNavegacion( Properties.Items, dmCatalogos.Navegacion );
          {if RevisaDerechoForma( efcCatCondiciones, K_DERECHO_ALTA) then
             Properties.Items.Add('Nuevo...');}
          Properties.DropDownRows := iMin( Properties.Items.Count, 12 );   // M�ximo muestra 12 sin que aparezca el Scroll
     end;
     //StatusBarMsg( dmCatalogos.GetNavegacionDescripcion, K_PANEL_NAVEGA );
end;

procedure TWizardRecibosXEmpleadoForm.NavegacionCBSelect(Sender: TObject);
var
   oTexto : TStringObject;
begin
     inherited;
          with NavegacionCB_DevEx do
          begin
               {***DevEx (by am): Se agrega la validacion de ItemIndex <> -1 porque los nuevos Combos no tiene evento OnSelect.
                                  Por lo tanto se ligo este metodo al evento OnChange***}
               if ItemIndex <> -1 then
               begin
                    if ItemIndex = 0 then
                    begin
                         dmCatalogos.Navegacion := VACIO;
                    end
                    {else if ( RevisaDerechoForma( efcCatCondiciones, K_DERECHO_ALTA) ) and ( ItemIndex = ( Properties.Items.Count - 1 ) ) then
                         dmCatalogos.AbreFormaEdicionCondicion }
                    else
                    begin
                         oTexto:= TStringObject( Properties.Items.Objects[ItemIndex] );
                         dmCatalogos.Navegacion := oTexto.Texto;
                    end;
               end;
          end;
     dmCliente.InitNavegadorEmpleado;
end;


function TWizardRecibosXEmpleadoForm.RevisaDerechoForma( FormaConsulta: eFormaConsulta; const TipoDerecho: Integer ): Boolean;
Begin
     Result := False;
     if ZAccesosMgr.CheckDerecho( GetPropConsulta( FormaConsulta ).IndexDerechos, TipoDerecho ) then
        Result := True;
end;

procedure TWizardRecibosXEmpleadoForm.CargaEmpleadoActivos;
begin
     with dmCliente do
     begin
          EmpleadoNumeroCB_DevEx.ValorEntero := Empleado; //DevEx
     end;
     RefrescaEmpleadoActivos;
     RefrescarNominasPorYear;
end;

procedure TWizardRecibosXEmpleadoForm.dxWizardControl1PageChanging(Sender: TObject;
  ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
const
    K_TRUE = 'True';
    K_QUERY_FILTRO_NOMINASAPROCESAR = 'OBTENER = %s';
  var
    iContadorRazonesSociales, iContadorNominas, iContadorNominasAProcesar : integer;
    i : integer;
    sCheckParaProcesar : string;
begin

//  if ( AnewPage = dxWizardControlPage2 ) then
//  begin
//          if dmInterfase.cdsNominasXYearAProcesar <> NIL then
//          begin
//
//              with dmInterfase.cdsNominasXYearAProcesar do
//              begin
//                    {Filtered := false;}
//                    dmInterfase.cdsNominasXYearAProcesar := nil;
//              end;
//          end;
//  end;


  if ( AnewPage = dxWizardControlPageEmpleados ) then
  begin
          AAllow := TRUE;

          dmInterfase.cdsNominasXYearAProcesar.Data :=  dmInterfase.cdsNominasXYear.Data;
          //DSNominasXYearAProcesar.DataSet :=  DataSourceNominasXYear.DataSet;   //SE HACE UNA COPIA DEL DATASOURCE DEL GRID PARA APLICAR FILTROS Y NO GENERE ERRORES EN CASO DE REGRESAR

          with dmInterfase.cdsNominasXYearAProcesar do
          begin
               Filter := Format( K_QUERY_FILTRO_NOMINASAPROCESAR, [ EntreComillas( K_TRUE )] );
               Filtered := true;
               iContadorNominasAProcesar := RecordCount;

               iContadorNominas := dmInterfase.cdsNominasXYear.RecordCount;

//               Filtered := false;
//               iContadorNominas := RecordCount;
//
//               Filtered := true;
//               iContadorNominas := RecordCount;


               if iContadorNominasAProcesar < 1  then
                begin
                     zError( Self.Caption,'Seleccione al menos una n�mina del empleado para procesar',0);
                     AAllow := FALSE;
                end;
                Filtered := false;
          end;



          FVerificacion := FALSE;
  end;

  if ( AnewPage = dxWizardControlPageTimbrado ) then
     begin

          AAllow := TRUE;
//          if(dmCliente.EsTRESSPruebas) then
//          begin
//               DxWizardControl1.buttons.Finish.Enabled := False;
//          end;
          cxFolderRecibosTxt.Text := GetRecibosPath;
          FEjecuto := FALSE;

          if ( not cxRecibosImpresos.Checked )  and ( not cxRecibosXML.Checked )  then
          begin
               zError( Self.Caption,'Seleccione al menos un tipo de Entrega de Recibos',0);
               cxRecibosImpresos.SetFocus;
               AAllow := FALSE;
          end;

         { CT_CODIGO.Llave := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString;
          if StrVacio( CT_CODIGO.Llave ) then
          begin
               zError( Self.Caption, 'No se ha especificado la Cuenta de Timbrado', 0  );
               AAllow := FALSE; }
//          end
//          else
//          begin
//               if not FVerificacion then
//               begin
//                    CargaListaNominas;
//               end;
//
//               if ( dmProcesos.cdsDataSetTimbrados = nil ) or ( dmProcesos.cdsDataSetTimbrados.IsEmpty ) then
//               begin
//                   ZWarning(  Self.Caption, 'No hay empleados timbrados en esa n�mina', 0, mbOK);
//                   AAllow := FALSE;
//               end;
          {end;}


          ZetaDBGridDBTableViewColumnaGrupo.Caption := cbGrupo.Descripcion;
     end;
end;

procedure TWizardRecibosXEmpleadoForm.CT_CODIGOValidLookup(Sender: TObject);
begin
      DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;

function TWizardRecibosXEmpleadoForm.GetRecibosDataSet: boolean;
begin

end;



procedure TWizardRecibosXEmpleadoForm.Exportar;
 var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Errores de Timbrado',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin

               if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes_DevEx.ExportarGrid( ZetaCXGrid1DBTableView3 ,
                                                ZetaCXGrid1DBTableView3.DataController.DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
               end;

end;

procedure TWizardRecibosXEmpleadoForm.ZetaSpeedButton1Click(Sender: TObject);
begin
      if ZetaCXGrid1DBTableView3.DataController.DataSetRecordCount > 0 then
      begin
           Exportar;
      end
      else
      begin
           //ZError('Error en '+Self.Caption, 'El Grid de Errores est� Vac�o', 0);
           ZetaDialogo.ZError('Error en '+Self.Caption, '�Se encontr� un Error! '+ CR_LF + 'El grid de respuesta del sistema de timbrado est� vac�o',0)
      end;
end;

procedure TWizardRecibosXEmpleadoForm.ZetaSpeedButton2Click(Sender: TObject);
begin
   ZetaBuscador_DevEx.BuscarCodigo( 'Numero', 'Periodos Afectados', 'Numero', dmInterfase.cdsNominasXYear {dmInterfase.cdsPeriodosAfectadosTotal} );
end;

procedure TWizardRecibosXEmpleadoForm.IniciarProgress;
begin

     cxProgressBarEnvio.Properties.Min := 0.00;
          cxProgressBarEnvio.Properties.Max := 100.00;  // puede ser por la cantidad de empleados en nomina
          cxProgressBarEnvio.Position := 0.00;
          cxProgressBarEnvio.Update;

     cxProgressBarEnvio.Visible := TRUE;
     cxLabelAvance.Visible := TRUE;

end;

function TWizardRecibosXEmpleadoForm.GetArchivoFromURL(
  sURL, sDestinoPath,  sFileName: string): string;
var
  SourceFile, LocalFile, sFileNamePath: string;
  func: TProgressCallback;
  brokerDownload : TManejadorPeticiones;
  oPeticionDownload : TPeticionWS;


    function GetTempFileDownload( const sPrefijo, sExt : string ): string;
    {$ifndef DOTNET}
    var
       aDir,aArchivo : array[0..1024] of char;
    {$endif}
    begin
         {$ifdef DOTNET}
         Result := '';
         {$else}
         GetTempPath(1024, aDir);
         GetTempFileName( aDir,PChar(sPrefijo), 0,aArchivo );
         Result := PChar(ChangeFileExt( aArchivo, '.'+ sExt ));
         DeleteFile( aArchivo );
         {$endif}
    end;


    function UntilNotExistsFileName : string;
    var
       iCount : integer;
       sDirPath,sDirPathEmpleadoNombre, sExtension  : string;
       sYear, sCodigoEmpleado, sNombreEmpleado : string;
       sNombre, sApellido : string;

    procedure crearDirectorio (sPath : string);
    begin
         if not DirectoryExists( sPath ) then
            CreateDir( sPath );
    end;
    begin
         iCount := 0;

         /// { Pasarla a una funcion Set , para que NombreCarpeta sea una propiedad
         sExtension := ExtractFileExt( FNombreCarpeta );
         if strLLeno( sExtension ) then
            FNombreCarpeta :=  Copy( FNombreCarpeta, 1, ( Pos( sExtension, FNombreCarpeta ) - 1 ) )
         else
             FNombreCarpeta := FNombreCarpeta;
         ///

         with dmCliente do
         begin

          //sNombreEmpleado :=  dmCliente.GetDatosEmpleadoActivo.Nombre;
          sNombre := dmCliente.cdsEmpleado.FieldByName('CB_NOMBRES').AsString;
          sApellido := dmCliente.cdsEmpleado.FieldByName('CB_APE_PAT').AsString;
          sCodigoEmpleado := IntToStr(dmCliente.GetDatosEmpleadoActivo.Numero);
          sYear := IntToStr(dmCliente.YearDefault);
         end;


          sDirPathEmpleadoNombre :=  sDestinoPath + '\'+sCodigoEmpleado+'_'+sApellido+'_' +sNombre+'_'+sYear;
          crearDirectorio(sDirPathEmpleadoNombre);

          //------sDirPath := sDestinoPath + '\' +sNombreEmpleado+sCodigoEmpleado+sYear+ '\' +FNombreCarpeta;
          //sDirPath := sDirPathEmpleadoNombre+ '\' +FNombreCarpeta;
          //crearDirectorio(sDirPath);

          sDirPath := sDirPathEmpleadoNombre;

         SetCarpetaRecibos(sDirPath);
         repeat
               sFileNamePath := ExtractFilePath( sDirPath + '\' + sFileName );
               if (icount = 0 ) then
                sFileNamePath := sFileNamePath  +  sFileName
               else
                sFileNamePath := sFileNamePath  + Format('(%d)%s', [iCount, sFileName]);

                Inc(icount);
         until not FileExists( sFileNamePath );

         Result := sFileNamePath;
    end;

    procedure EsperarDescarga;
    var
        iCount : integer;
    begin
        iCount := 0;
        FTotalRead := 0;
        FTotalSize := 0;
        FFileNameDescarga := VACIO;

        while oPeticionDownload.lProceso do
        begin
          if StrLleno( FFileNameDescarga ) then
             SetStatus( Format('Descargando %s (%.2f Mb de %.2f Mb )', [FFileNameDescarga, fTotalRead, fTotalSize ] ) );

          Inc( iCount );
          //iShowBuzz := iCount mod 4;
          //cxProgressBarEnvio.Position := iShowBuzz * 20;  cxProgressBarEnvio.Update;

          if ( iCount mod 4  = 0 ) then
             Application.ProcessMessages
          else
             DelayNPM( 200 );
         end;
        Application.ProcessMessages;
    end;



    function  DescargarArchivo : boolean;
    begin
         Result := FALSE;

         try
              oPeticionDownload := TPeticionWS.Create;
              oPeticionDownload.tipoPeticion := peticionDownloadFile;
              oPeticionDownload.sArchivoDestino := sFileName;
              oPeticionDownload.sArchivoOrigen :=  SourceFile;
              oPeticionDownload.sLocalFile := LocalFile;
              oPeticionDownload.ACallback :=  DescargaEnProgreso;
              oPeticionDownload.lProceso := TRUE;
              brokerDownload.AgregarPeticionWS( oPeticionDownload );
              EsperarDescarga;
              Result := oPeticionDownload.lResultadoDownload;
          finally
               SetStatus( VACIO );
          end;

    end;


begin
  SourceFile := sURL;

  LocalFile := GetTempFileDownload('REC', 'zip');

  Result := VACIO;

  try
     brokerDownload := TManejadorPeticiones.Create( ILogBitacora( Self ) );
     FContinuaDownload := TRUE;
     btCancelarDescarga.Visible := FALSE;
     SetStatus( VACIO );

     func := DescargaEnProgreso;

     sFileNamePath := UntilNotExistsFileName;


     if DescargarArchivo then
     begin
       RenameFile(LocalFile, sFileNamePath);
       Result := sFileNamePath;
     end
     else
        zInformation( 'Exportar...',Format( 'Error al Descargar Archivo del Sistema de Timbrado: %s', [sFileName] ), 0);

     except on Error: Exception do
     begin
        zInformation( 'Exportar...',Format( 'Error al Descargar Archivo del Sistema de Timbrado: %s', [sFileName] ), 0);
     end;

  end;
  btCancelarDescarga.Visible := FALSE;
  SetStatus( VACIO );
  FreeAndNil(brokerDownload); 

end;



procedure TWizardRecibosXEmpleadoForm.btBuscarFolderClick(Sender: TObject);
begin

     cxShellBrowserDialogRecibos.Path :=  cxFolderRecibosTxt.Text;
     if cxShellBrowserDialogRecibos.Execute then
     begin
         cxFolderRecibosTxt.Text := cxShellBrowserDialogRecibos.Path ;
     end;

     if  DirectoryExists(cxFolderRecibosTxt.Text ) then
         FTimbramexHelper.SetRecibosPath( cxFolderRecibosTxt.Text );


end;


function DescargaEnProgreso(FileName : string; ATotalSize,  ATotalRead, AStartTime: DWORD): Boolean;
begin
   with WizardRecibosXEmpleadoForm
    do
   begin
     FFileNameDescarga := FileName;
     FTotalRead :=  ATotalRead / (1048576.0);
     FTotalSize :=  ATotalSize / (1048576.0);
     Result := FContinuaDownload;
   end;
end;

procedure TWizardRecibosXEmpleadoForm.btCancelarDescargaClick(
  Sender: TObject);
begin
     FContinuaDownload := FALSE;
end;

procedure TWizardRecibosXEmpleadoForm.btnDesmarcarTodasClick(Sender: TObject);
begin
     cambiarSeleccion('False');
end;

procedure TWizardRecibosXEmpleadoForm.btnSeleccionarTodosClick(Sender: TObject);
begin
     cambiarSeleccion('True');
end;

procedure TWizardRecibosXEmpleadoForm.cambiarSeleccion (elegir: string);
var sRegistroActual: String;
begin
      try
          with DataSourceNominasXYear.DataSet do
          begin
              sRegistroActual := FieldByName('Numero').AsString;
              GridDBTableViewPeriodosAnio.OptionsView.ScrollBars := ssNone;
              First;
              while not Eof do
              begin
                  Edit;
                  FieldByName ('OBTENER').AsString := elegir;
                  Post;
                  Next;
              end;
              First;
              GridDBTableViewPeriodosAnio.OptionsView.ScrollBars := ssBoth;
              Locate('Numero', sRegistroActual, []);
          end;
       except on Error: Exception do
              begin
                 //ZWarning( 'Obtener Recibos Timbrados por Empleado','', 0, mbOK);
              end;
       end;

end;

procedure TWizardRecibosXEmpleadoForm.btREfrescaPeriodosClick(Sender: TObject);
begin
     RefrescarNominasPorYear;
     //dmInterfase.cdsPeriodosAfectadosTotal.Refrescar;
     //gridPeriodosDBTableView1.applybestfit();
end;

procedure TWizardRecibosXEmpleadoForm.RefrescaEmpleadoActivos;
var
   oColor : TColor;
   i:Integer;
   sCaption, sHint : String;

   bEsTransferido : boolean;

   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
     with dmCliente do
     begin

          //StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
          //MuestraDatosUsuarioActivo; //DevEx(by am): Se refresca el usuario y el grupo en el Panel de valores activos
          with GetDatosEmpleadoActivo do
          begin
               bEsTransferido := false;
               EmpleadoPrettyName_DevEx.Caption := Nombre;

               GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);

               if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
               begin
                    bEsTransferido := dmInterfase.GetValidacionEmpleadoTransferido(dmCliente.Empresa, dmCliente.Empleado);
               end;
               //DevEx (by am): Inicializando Bitmat y PNG
               ABitmap := TcxAlphaBitmap.CreateSize(24, 24);
               APngImage := nil;
               if bEsTransferido then
               begin
                     try
                           //DevEx (by am): Creando el Bitmap
                           ABitmap.Clear;
                           Image24_StatusEmpleado.GetBitmap(4, ABitmap);
                           //DevEx (by am): Creando el PNG
                           APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                           ImagenStatus_DevEx.Picture.Graphic := APngImage;
                           ImagenStatus_DevEx.Hint := sHint;
                     finally
                            APngImage.Free;
                            ABitmap.Free;
                     end;
               end
               else
               begin
                     for i := Low(StatusEmpleado) to High(StatusEmpleado) do
                     begin
                          if (sCaption = StatusEmpleado[i]) then
                          begin
                               try
                                     //DevEx (by am): Creando el Bitmap
                                     ABitmap.Clear;
                                     Image24_StatusEmpleado.GetBitmap(i, ABitmap);
                                     //DevEx (by am): Creando el PNG
                                     APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                                     ImagenStatus_DevEx.Picture.Graphic := APngImage;
                                     ImagenStatus_DevEx.Hint := sHint;
                               finally
                                      APngImage.Free;
                                      ABitmap.Free;
                               end;
                               Break;
                          end;
                     end;
               end;

          end;
     end;
end;

procedure TWizardRecibosXEmpleadoForm.RefrescarNominasPorYear;
var
  sError : String;
begin
  sError := VACIO;
  with dmInterfase do
  begin
       sError := dmInterfase.GetNominasPorYear(IntToStr(Anio),IntToStr( dmCliente.Empleado ));
       if ( cdsNominasXYear.IsEmpty ) or (sError <> VACIO) then
   	   begin
            if GridDBTableViewPeriodosAnio.DataController.RecordCount > 0 then
            begin
                 GridDBTableViewPeriodosAnio.DataController.RecordCount := 0;
            end;
            Refresh;
    	 end
    	 else
    	 begin
          DataSourceNominasXYear.DataSet := dmInterfase.cdsNominasXYear;
          Refresh;
          GridDBTableViewPeriodosAnio.DataController.FocusedRowIndex := 0;
    	 end;
  end;
end;

procedure TWizardRecibosXEmpleadoForm.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
     RefrescarNominasPorYear;
     //CambioValoresActivos( stEmpleado );
end;

procedure TWizardRecibosXEmpleadoForm._Emp_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
          with dmCliente do
          begin
               if GetEmpleadoPrimeroTransferido then
               begin
                    FEsEmpleadoTransferido := true;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;

     end
     else
     begin
          with dmCliente do
          begin
               if GetEmpleadoPrimero then
               begin
                    FEsEmpleadoTransferido := False;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;
     end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizardRecibosXEmpleadoForm.BuscarPrimerEmpleado;
begin

     if NOT global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
          with dmCliente do
          begin
               if GetEmpleadoPrimero then
               begin
                    FEsEmpleadoTransferido := False;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;
     end;
end;


procedure TWizardRecibosXEmpleadoForm._Emp_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try

     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
          with dmCliente do
          begin
               if GetEmpleadoAnteriorTransferido then
               begin
                    FEsEmpleadoTransferido := true;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;

     end
     else
     begin
          with dmCliente do
          begin
               if GetEmpleadoAnterior then
               begin
                    FEsEmpleadoTransferido := False;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;
     end;

//        with dmCliente do
//        begin
//             if GetEmpleadoAnterior then
//             begin
//                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
//                  CambioEmpleadoActivo;
//             end;
//        end;


     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizardRecibosXEmpleadoForm._E_BuscarEmpleadoExecute(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
           if ZetaBuscaEmpleado_DevExTimbradoAvanzada.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
           begin
                FEsEmpleadoTransferido := true;
                EmpleadoNumeroCB_DevEx.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
           end;
     end
     else
     begin
          if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
          begin
               EmpleadoNumeroCB_DevEx.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
          end;
          FEsEmpleadoTransferido := false;
     end;
end;

procedure TWizardRecibosXEmpleadoForm._Emp_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try

     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
          with dmCliente do
          begin
               if GetEmpleadoSiguienteTransferido then
               begin
                    FEsEmpleadoTransferido := true;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;

     end
     else
     begin
          with dmCliente do
          begin
               if GetEmpleadoSiguiente then
               begin
                    FEsEmpleadoTransferido := False;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;
     end;


//        with dmCliente do
//        begin
//             if GetEmpleadoSiguiente then
//             begin
//                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
//                  CambioEmpleadoActivo;
//             end;
//        end;



     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizardRecibosXEmpleadoForm._Emp_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try



     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
          with dmCliente do
          begin
               if GetEmpleadoUltimoTransferido then
               begin
                    FEsEmpleadoTransferido := true;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;

     end
     else
     begin
          with dmCliente do
          begin
               if GetEmpleadoUltimo then
               begin
                    FEsEmpleadoTransferido := False;
                    EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                    CambioEmpleadoActivo;
               end;
          end;
     end;



//        with dmCliente do
//        begin
//             if GetEmpleadoUltimo then
//             begin
//                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
//                  CambioEmpleadoActivo;
//             end;
//        end;





     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizardRecibosXEmpleadoForm.SeleccionarClick(Sender: TObject);
var
   lCarga: Boolean;
   lOk: Boolean;
begin
     inherited;
//     if Verificacion then
//     begin
//          case ZConfirmaVerificacion.ConfirmVerification of
//               mrYes:
//               begin
//                    lCarga := True;
//                    lOk := True;
//               end;
//               mrNo:
//               begin
//                    lCarga := False;
//                    lOk := True;
//               end;
//          else
//              begin
//                   lCarga := False;
//                   lOk := False;
//              end;
//          end;
//     end
//     else
//     begin
//          lCarga := True;
//          lOk := True;
//     end;
//     if lOk then
//     begin
//          if lCarga then
//          begin
//               CargaListaNominas;
//          end;
//          SetVerificacion( Verificar );
//     end;
end;

procedure TWizardRecibosXEmpleadoForm.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardRecibosXEmpleadoForm.BFinalClick(Sender: TObject);
begin
    with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TWizardRecibosXEmpleadoForm.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TWizardRecibosXEmpleadoForm.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, Text, SelStart, evBase );
     end;
end;


function TWizardRecibosXEmpleadoForm.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     if global.getglobalBooleano(K_GLOBAL_TIMBRADO_EMP_TRANSFERIDOS_1) then
     begin
           if ZetaBuscaEmpleado_DevExTimbradoAvanzada.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
           begin
                if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                   Result := sLlave + ',' + sKey
                else
                    Result := sKey;
           end;
     end
     else
     begin
          if ZetaBuscaEmpleado_DevExAvanzada.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
           begin
                if lConcatena and ZetaCommonTools.StrLleno( Text ) then
                   Result := sLlave + ',' + sKey
                else
                    Result := sKey;
           end;

     end;

end;

procedure TWizardRecibosXEmpleadoForm.FormDestroy(Sender: TObject);
begin
     FDescripciones.Free;
     FParameterList.Free;


end;

procedure TWizardRecibosXEmpleadoForm.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;

procedure TWizardRecibosXEmpleadoForm.EnabledBotones(
  const eTipo: eTipoRangoActivo);
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;



function TWizardRecibosXEmpleadoForm.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
          { Causa problemas porque le quita la '@'; los par�ntesis no son necesarios
          if EsFormula( Result ) then
             Result := Parentesis( Result );
          }
     end
     else
         Result := '';
end;

function TWizardRecibosXEmpleadoForm.GetFiltro: String;
begin
  Result := Trim( sFiltro.Text );
     { Los Par�ntesis no son necesarios
     if StrLleno( Result ) then
        Result := Parentesis( Result );
     }
end;

function TWizardRecibosXEmpleadoForm.GetRango: String;
var
   sl : TStringList;
   i : integer;
   sLista : string;
begin

     sl := TStringList.Create;
     sl.CommaText :=  ELista.Text;
     sLista := VACIO;
     for i:=0 to sl.Count -1  do                       
     begin
          if StrLleno( sl[i] ) then
          begin
               if StrVacio( sLista)  then
                  sLista := sl[i]
               else
                  sLista :=  sLista + ','+ sl[i];
          end;
     end;
     sl.Free;

     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FEmpleadoCodigo, Trim( sLista  ) );
     else
         Result := '';
     end;
end;

procedure TWizardRecibosXEmpleadoForm.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TWizardRecibosXEmpleadoForm.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TWizardRecibosXEmpleadoForm.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;

function TWizardRecibosXEmpleadoForm.Verificar: Boolean;
begin
     if ( cbOrden.ItemIndex = 0 ) then
        FEmpleadoTimbradoGridSelect.FTituloOrden := VACIO
     else
         FEmpleadoTimbradoGridSelect.FTituloOrden := cbOrden.Descripcion;
//FTituloOrden
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataSetTimbrados, TEmpleadoTimbradoGridSelect );
end;

function TWizardRecibosXEmpleadoForm.GetRazonSocial: string;
begin
    Result := dmCliente.RazonSocial;
end;

procedure TWizardRecibosXEmpleadoForm.CargaListaNominas;
var
 oCursor : TCursor;
begin
    oCursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
       CargaParametros;
       CargaListaVerificacion;
    finally
           Screen.Cursor := oCursor;
    end;
end;

procedure TWizardRecibosXEmpleadoForm.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );

          AddString( 'RazonSocial',  GetRazonSocial );
          AddInteger( 'StatusAnterior', ord(estiTimbrado) );
          AddInteger( 'StatusNuevo',  ord(estiTimbrado) ) ;

          AddInteger( 'Year',dmCliente.YearDefault );
          AddInteger( 'Tipo',ord( dmCliente.PeriodoTipo )  );
          AddInteger( 'Numero', dmCliente.PeriodoNumero );

          if ( cbGrupo.Llave = 'NOAPLICA' ) then
              AddString( 'Orden', cbOrden.Llave + ',' + cbOrden.Llave  )
          else
              AddString( 'Orden', cbGrupo.Llave + ',' + cbOrden.Llave  );
     end;

     with Descripciones do
     begin
          AddString( 'Lista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;

     with dmCliente do
     begin
          CargaActivosIMSS( ParameterList );
          CargaActivosPeriodo( ParameterList );
          CargaActivosSistema( ParameterList );
     end;
end;

procedure TWizardRecibosXEmpleadoForm.CargaListaVerificacion;
var
  sValor1x : string;
  sValor2x : string;
  sValor3x : string;
  sValor4x : string;
  sValor5x : string;
  sValor6x : string;
  sValor7x : string;
  sValor8x : string;
  sValor9x : string;
  sValor10x : string;
  sValor11x : string;
  sValor12x : string;
  sValor13x : string;
  sValor14x : string;
  sValor15x : string;
  sValor16x : string;
  sValor17x : string;
  sValor18x : string;
  sValor19x : string;
  sValor20x : string;
  sValor21x : string;
  sValor22x : string;
  sValor23x : string;
  sValor24x : string;
  sValor25x : string;
  sValor26x : string;
  sValor27x : string;
begin
     sValor1x := ParameterList.Items[0].AsString;
     sValor2x := ParameterList.Items[1].AsString;
     sValor3x := ParameterList.Items[2].AsString;
     sValor4x := ParameterList.Items[3].AsString;
     sValor5x := ParameterList.Items[4].AsString;
     sValor6x := ParameterList.Items[5].AsString;
     sValor7x := ParameterList.Items[6].AsString;
     sValor8x := ParameterList.Items[7].AsString;
     sValor9x := ParameterList.Items[8].AsString;
     sValor10x := ParameterList.Items[9].AsString;
     sValor11x := ParameterList.Items[10].AsString;
     sValor12x := ParameterList.Items[11].AsString;
     sValor13x := ParameterList.Items[12].AsString;
     sValor14x := ParameterList.Items[13].AsString;
     sValor15x := ParameterList.Items[14].AsString;
     sValor16x := ParameterList.Items[15].AsString;
     sValor17x := ParameterList.Items[16].AsString;
     sValor18x := ParameterList.Items[17].AsString;
     sValor19x := ParameterList.Items[18].AsString;
     sValor20x := ParameterList.Items[19].AsString;
     sValor27x := ParameterList.ParamByName('Numero').AsString;
   {  sValor21 := ParameterList.Items[20].AsString;
     sValor22 := ParameterList.Items[21].AsString;
     sValor23 := ParameterList.Items[22].AsString;
     sValor24 := ParameterList.Items[23].AsString;
     sValor25 := ParameterList.Items[24].AsString;
     sValor26 := ParameterList.Items[25].AsString;
     sValor27 := ParameterList.Items[26].AsString;  }
     dmProcesos.TimbrarNominaGetLista( ParameterList );
end;


procedure TWizardRecibosXEmpleadoForm.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
          ZetaCXGrid1DBTableView3.ApplyBestFit();
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
     FEsEmpleadoTransferido := false;

     if(dmCliente.EsTRESSPruebas) then
     begin
          DxWizardControl1.buttons.Finish.Enabled := False;
     end;

     GridDBTableViewPeriodosAnio.applybestfit();

      {***Configuracion nuevo Grid***}
     //Desactiva la posibilidad de agrupar
     GridDBTableViewPeriodosAnio.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     GridDBTableViewPeriodosAnio.OptionsView.GroupByBox := False;
     //Para remover las opcioens de filtrado
     GridDBTableViewPeriodosAnio.OptionsCustomize.ColumnFiltering := False;
     //Para que nunca muestre el filterbox inferior
     GridDBTableViewPeriodosAnio.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     GridDBTableViewPeriodosAnio.FilterBox.CustomizeDialog := False;


     BuscarPrimerEmpleado;

end;

function TWizardRecibosXEmpleadoForm.GetGrupoName(
  sCodigoGrupo: string; var sGrupoDescripcion : string): string;
var
   iNivel : integer;
begin
     //Aqui debe buscar la descripcion en los niveles de organigrama
     //Esto segun el Grupo elegido en la UIX
     //Por el momento se pone el puro Codigo

     iNivel :=  StrToIntDef( StrRight( cbGrupo.Llave, 1 ), 0);

     case iNivel of
          1 :
          begin
               dmTablas.cdsNivel1.Conectar;
               dmTablas.cdsNivel1.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          2 :
          begin
               dmTablas.cdsNivel2.Conectar;
               dmTablas.cdsNivel2.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          3 :
          begin
               dmTablas.cdsNivel3.Conectar;
               dmTablas.cdsNivel3.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          4 :
          begin
               dmTablas.cdsNivel4.Conectar;
               dmTablas.cdsNivel4.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          5 :
          begin
               dmTablas.cdsNivel5.Conectar;
               dmTablas.cdsNivel5.LookupKey( sCodigoGrupo, VACIO, Result);
          end ;
          6 :
          begin
               dmTablas.cdsNivel6.Conectar;
               dmTablas.cdsNivel6.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          7 :
          begin
               dmTablas.cdsNivel7.Conectar;
               dmTablas.cdsNivel7.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          8 :
          begin
               dmTablas.cdsNivel8.Conectar;
               dmTablas.cdsNivel8.LookupKey( sCodigoGrupo, VACIO, Result);
          end;
          9 :
          begin
               dmTablas.cdsNivel9.Conectar;
               dmTablas.cdsNivel9.LookupKey( sCodigoGrupo, VACIO, Result);
          end;

     else
         Result := VACIO;
     end;


     if  strVacio( Result ) then
     begin
          Result := sCodigoGrupo; 
          sGrupoDescripcion := Result;
     end
     else
     begin
         sGrupoDescripcion := sCodigoGrupo + ' - ' + Result;
         Result := sCodigoGrupo + '_' + Result; 
     end;

     Result :=  QuitaComillas (  QuitaAcentos(  Result ) ) ;
     Result := StrTransAll( Result, ' ', '_' );
     Result := StrTransAll( Result, '/', '' );
     Result := StrTransAll( Result, '\', '' );
     Result := StrTransAll( Result, '.', '' );
end;

procedure TWizardRecibosXEmpleadoForm.ApplyMinWidth;
var
   i: Integer;
begin
     {with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption);
          end;
     end;   }

end;


procedure TWizardRecibosXEmpleadoForm.Grid_AbrirArchivoExecute(
  Sender: TObject);
var
   sArchivo :string;
begin

  sArchivo := GetNombreArchivoElegido;

  if strLleno( sArchivo ) then
    ZetaFilesTools.AbreArchivoPrograma( sArchivo );

end;

procedure TWizardRecibosXEmpleadoForm.Grid_AbrirArchivoExplorerExecute(
  Sender: TObject);
var
   sArchivo :string;
begin

  sArchivo := GetNombreArchivoElegido;

  if strLleno( sArchivo ) then
    ZetaFilesTools.AbreArchivoEnExplorer( sArchivo );
end;

function TWizardRecibosXEmpleadoForm.GetNombreArchivoElegido: string;
begin
 Result := VACIO;
 with dmInterfase.cdsArchivosDescargados do
 begin
    if not isEmpty then
     Result :=  FieldByName('ARCHIVO').AsString;
 end;
end;

procedure TWizardRecibosXEmpleadoForm.EscribirBitacoraError(
  sMensaje: string);
begin
//
end;

procedure TWizardRecibosXEmpleadoForm.EscribirBitacoraLog(sMensaje: string);
begin
  //
end;

procedure TWizardRecibosXEmpleadoForm.SetStatus(sStatus: string);
begin

     cxTextEditStatus.Text := sStatus;
end;

procedure TWizardRecibosXEmpleadoForm.SetCarpetaRecibos(sCarpetaRecibos: string);
begin
     if strLleno( sCarpetaRecibos ) then
     begin
//        cxLabelCarpeta.Visible := True;
//        cxTextEditCarpetaRecibos.Visible := True;
        cxTextEditCarpetaRecibos.Text := sCarpetaRecibos;
     end;
end;

procedure TWizardRecibosXEmpleadoForm.ExportarListaArchivos;
 var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Recibos de Timbrado',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin

               if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes_DevEx.ExportarGrid( ZetaDBGridDBTableView ,
                                                ZetaDBGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
               end;

end;

procedure TWizardRecibosXEmpleadoForm.cxButtonExportExcelListaClick(
  Sender: TObject);
begin
    ExportarListaArchivos; 
end;

procedure TWizardRecibosXEmpleadoForm.cxGridDBColumn7PropertiesChange(
  Sender: TObject);
begin
              //DataSourceNominasXYear.DataSet.Edit;
              //DataSourceNominasXYear.DataSet.FieldByName ('OBTENER').AsBoolean := TRUE;
              //DataSourceNominasXYear.DataSet.Post;
end;

procedure TWizardRecibosXEmpleadoForm.DataSourceErroresDataChange(
  Sender: TObject; Field: TField);
begin
ZetaCXGrid1DBTableView3.ApplyBestFit();
end;

procedure TWizardRecibosXEmpleadoForm.DataSourceErroresUpdateData(
  Sender: TObject);
begin
ZetaCXGrid1DBTableView3.ApplyBestFit();
end;

procedure TWizardRecibosXEmpleadoForm.DataSourceNominasXYearUpdateData(
  Sender: TObject);
begin
     GridDBTableViewPeriodosAnio.applybestfit();
end;

procedure TWizardRecibosXEmpleadoForm.ZetaDBGridDBTableViewColumn4GetProperties(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  Value: Variant;
  Buttons: TcxEditButtons;
  ButtonEnabled : Boolean;
begin

  if VarIsNull(ARecord.Values[1]) then
    AProperties := ButtonsImpreso.Properties
    // or AProperties := ButtonsVisible.Properties depending on what you want/need
  else
  begin
      Value := ARecord.Values[1];
      if (Value = 'Impreso') then
         AProperties := ButtonsImpreso.Properties
      else
        AProperties := ButtonsXML.Properties;
  end;

end;

end.


