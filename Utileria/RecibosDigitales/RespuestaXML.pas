
{***********************************************************************}
{                                                                       }
{                           XML Data Binding                            }
{                                                                       }
{***********************************************************************}

unit RespuestaXML;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRESPUESTA = interface;
  IXMLRESPUESTA_BITACORA = interface;
  IXMLRESPUESTA_BITACORA_EVENTO = interface;
  IXMLRESPUESTA_BITACORA_EVENTOList = interface;
  IXMLRESPUESTA_RESULTADO = interface;
  IXMLRESPUESTA_SALIDA = interface;
  IXMLRESPUESTA_SALIDA_MENSAJE = interface;
  IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK = interface;
  IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE = interface;
  IXMLRESPUESTA_SALIDA_MENSAJEList = interface;
  IXMLRESPUESTA_SALIDA_URL = interface;

{ IXMLRESPUESTA }

  IXMLRESPUESTA = interface(IXMLNode)
    ['{F775AB7E-C98A-491A-8FB4-95C0AD64B237}']
    { Property Accessors }
    function Get_BITACORA: IXMLRESPUESTA_BITACORA;
    function Get_RESULTADO: IXMLRESPUESTA_RESULTADO;
    function Get_SALIDA: IXMLRESPUESTA_SALIDA;
    { Methods & Properties }
    property BITACORA: IXMLRESPUESTA_BITACORA read Get_BITACORA;
    property RESULTADO: IXMLRESPUESTA_RESULTADO read Get_RESULTADO;
    property SALIDA: IXMLRESPUESTA_SALIDA read Get_SALIDA;
  end;

{ IXMLRESPUESTA_BITACORA }

  IXMLRESPUESTA_BITACORA = interface(IXMLNode)
    ['{659DB36F-845C-4473-A217-394E8B9F35AE}']
    { Property Accessors }
    function Get_Cuantos: Integer;
    function Get_EVENTO: IXMLRESPUESTA_BITACORA_EVENTOList;
    { Methods & Properties }
    property EVENTO: IXMLRESPUESTA_BITACORA_EVENTOList read Get_EVENTO;
    property CUANTOS: Integer read Get_Cuantos;
  end;

  { IXMLRESPUESTA_SALIDA_MENSAJE }

  IXMLRESPUESTA_BITACORA_EVENTO = interface(IXMLNode)
    ['{D406DAD2-2E94-49E7-822A-36055EDEFCA3}']
    { Property Accessors }
    function Get_Tipo: UnicodeString;
    function Get_ID: Integer;
    function Get_Detalle: UnicodeString;
    { Methods & Properties }
    property Tipo: UnicodeString read Get_Tipo;
    property ID: Integer read Get_ID;
    property Detalle: UnicodeString read Get_Detalle;
  end;

  { IXMLRESPUESTA_BITACORA_EVENTOList }

  IXMLRESPUESTA_BITACORA_EVENTOList = interface(IXMLNodeCollection)
    ['{86A938AC-F6D2-4CFE-A8F2-4F2A2ADBB807}']
    { Methods & Properties }
    function Add: IXMLRESPUESTA_BITACORA_EVENTO;
    function Insert(const Index: Integer): IXMLRESPUESTA_BITACORA_EVENTO;

    function Get_Item(Index: Integer): IXMLRESPUESTA_BITACORA_EVENTO;
    property Items[Index: Integer]: IXMLRESPUESTA_BITACORA_EVENTO read Get_Item; default;
  end;


{ IXMLRESPUESTA_RESULTADO }

  IXMLRESPUESTA_RESULTADO = interface(IXMLNode)
    ['{109C0587-2853-4F5A-8430-A77536779D22}']
    { Property Accessors }
    function Get_TX_ID: Integer;
    function Get_TX_STATUS: Integer;
    function Get_Errores: Boolean;
    { Methods & Properties }
    property TX_ID: Integer read Get_TX_ID;
    property TX_STATUS: Integer read Get_TX_STATUS;
    property Errores: Boolean read Get_Errores;
  end;

{ IXMLRESPUESTA_SALIDA }

  IXMLRESPUESTA_SALIDA = interface(IXMLNode)
    ['{6137205F-C51C-4724-AAA1-4E35A84C5CBE}']
    { Property Accessors }
    function Get_MENSAJE: IXMLRESPUESTA_SALIDA_MENSAJEList;
    function Get_URL: IXMLRESPUESTA_SALIDA_URL;
    { Methods & Properties }
    property MENSAJE: IXMLRESPUESTA_SALIDA_MENSAJEList read Get_MENSAJE;
    property URL: IXMLRESPUESTA_SALIDA_URL read Get_URL;
  end;

{ IXMLRESPUESTA_SALIDA_MENSAJE }

  IXMLRESPUESTA_SALIDA_MENSAJE = interface(IXMLNode)
    ['{003C0071-D1A0-4AB8-8BF2-5318118E1E41}']
    { Property Accessors }
    function Get_VerboID: Integer;
    function Get_Tipo: Integer;
    function Get_Titulo: UnicodeString;
    function Get_ErrorID: Integer;
    function Get_HTML: UnicodeString;
    function Get_BotonDialogoOK: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK;
    function Get_BotonDialogoCLOSE: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE;
    { Methods & Properties }
    property VerboID: Integer read Get_VerboID;
    property Tipo: Integer read Get_Tipo;
    property Titulo: UnicodeString read Get_Titulo;
    property ErrorID: Integer read Get_ErrorID;
    property HTML: UnicodeString read Get_HTML;
    property BotonDialogoOK: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK read Get_BotonDialogoOK;
    property BotonDialogoCLOSE: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE read Get_BotonDialogoCLOSE;
  end;


{ IXMLRESPUESTA_SALIDA_MENSAJEList }

  IXMLRESPUESTA_SALIDA_MENSAJEList = interface(IXMLNodeCollection)
    ['{6DBEE670-D35A-47D4-B63E-3E9084CEFF15}']
    { Methods & Properties }
    function Add: IXMLRESPUESTA_SALIDA_MENSAJE;
    function Insert(const Index: Integer): IXMLRESPUESTA_SALIDA_MENSAJE;

    function Get_Item(Index: Integer): IXMLRESPUESTA_SALIDA_MENSAJE;
    property Items[Index: Integer]: IXMLRESPUESTA_SALIDA_MENSAJE read Get_Item; default;
  end;

  { IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK }

  IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK = interface(IXMLNode)
    ['{9537988C-7E79-4810-80BF-B5B82D991165}']
    { Property Accessors }
    function Get_Label_: UnicodeString;
    function Get_Caption: UnicodeString;
    function Get_Tipo: ShortInt;
    function Get_URL: UnicodeString;
    { Methods & Properties }
    property Label_: UnicodeString read Get_Label_;
    property Caption: UnicodeString read Get_Caption;
    property Tipo: ShortInt read Get_Tipo;
    property URL: UnicodeString read Get_URL;
  end;

  { IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE }

  IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE = interface(IXMLNode)
    ['{6BD39DAF-9E92-48A8-9CAC-8FE903E2D619}']
    { Property Accessors }
    function Get_Label_: UnicodeString;
    function Get_Caption: UnicodeString;
    function Get_Tipo: ShortInt;
    { Methods & Properties }
    property Label_: UnicodeString read Get_Label_;
    property Caption: UnicodeString read Get_Caption;
    property Tipo: ShortInt read Get_Tipo;
  end;

{ IXMLRESPUESTA_SALIDA_URL }

  IXMLRESPUESTA_SALIDA_URL = interface(IXMLNode)
    ['{555F62BF-51B2-479F-8278-3BAD434A1428}']
    { Property Accessors }
    function Get_DESPACHO: UnicodeString;
    function Get_CLIENTE: UnicodeString;
    { Methods & Properties }
    property DESPACHO: UnicodeString read Get_DESPACHO;
    property CLIENTE: UnicodeString read Get_CLIENTE;
  end;

{ Forward Decls }

  TXMLRESPUESTA = class;
  TXMLRESPUESTA_BITACORA = class;
  TXMLRESPUESTA_BITACORA_EVENTO = class;
  TXMLRESPUESTA_BITACORA_EVENTOList = class;
  TXMLRESPUESTA_RESULTADO = class;
  TXMLRESPUESTA_SALIDA = class;
  TXMLRESPUESTA_SALIDA_MENSAJE = class;
  TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK = class;
  TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE = class;
  TXMLRESPUESTA_SALIDA_MENSAJEList = class;
  TXMLRESPUESTA_SALIDA_URL = class;

{ TXMLRESPUESTA }

  TXMLRESPUESTA = class(TXMLNode, IXMLRESPUESTA)
  protected
    { IXMLRESPUESTA }
    function Get_BITACORA: IXMLRESPUESTA_BITACORA;
    function Get_RESULTADO: IXMLRESPUESTA_RESULTADO;
    function Get_SALIDA: IXMLRESPUESTA_SALIDA;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRESPUESTA_BITACORA }

  TXMLRESPUESTA_BITACORA = class(TXMLNode, IXMLRESPUESTA_BITACORA)
  private
    FEVENTO: IXMLRESPUESTA_BITACORA_EVENTOList;
  protected
    { IXMLRESPUESTA_BITACORA }
    function Get_Cuantos: Integer;
    function Get_Evento: IXMLRESPUESTA_BITACORA_EVENTOList;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRESPUESTA_BITACORA_EVENTO }

  TXMLRESPUESTA_BITACORA_EVENTO = class(TXMLNode, IXMLRESPUESTA_BITACORA_EVENTO)
  protected
    { XMLRESPUESTA_BITACORA_EVENTO }
    function Get_Tipo: UnicodeString;
    function Get_ID: Integer;
    function Get_Detalle: UnicodeString;
  end;

{ TXMLRESPUESTA_BITACORA_EVENTOList }

  TXMLRESPUESTA_BITACORA_EVENTOList = class(TXMLNodeCollection, IXMLRESPUESTA_BITACORA_EVENTOList)
  protected
    { IXMLRESPUESTA_BITACORA_EVENTOList }
    function Add: IXMLRESPUESTA_BITACORA_EVENTO;
    function Insert(const Index: Integer): IXMLRESPUESTA_BITACORA_EVENTO;
    function Get_Item(Index: Integer): IXMLRESPUESTA_BITACORA_EVENTO;
  end;

{ TXMLRESPUESTA_RESULTADO }

  TXMLRESPUESTA_RESULTADO = class(TXMLNode, IXMLRESPUESTA_RESULTADO)
  protected
    { IXMLRESPUESTA_RESULTADO }
    function Get_TX_ID: Integer;
    function Get_TX_STATUS: Integer;
    function Get_Errores: Boolean;
  end;

{ TXMLRESPUESTA_SALIDA }

  TXMLRESPUESTA_SALIDA = class(TXMLNode, IXMLRESPUESTA_SALIDA)
  private
    FMENSAJE: IXMLRESPUESTA_SALIDA_MENSAJEList;
  protected
    { IXMLRESPUESTA_SALIDA }
    function Get_MENSAJE: IXMLRESPUESTA_SALIDA_MENSAJEList;
    function Get_URL: IXMLRESPUESTA_SALIDA_URL;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRESPUESTA_SALIDA_MENSAJE }

  TXMLRESPUESTA_SALIDA_MENSAJE = class(TXMLNode, IXMLRESPUESTA_SALIDA_MENSAJE)
  protected
    { IXMLRESPUESTA_SALIDA_MENSAJE }
    function Get_VerboID: Integer;
    function Get_Tipo: Integer;
    function Get_Titulo: UnicodeString;
    function Get_ErrorID: Integer;
    function Get_HTML: UnicodeString;
    function Get_BotonDialogoOK: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK;
    function Get_BotonDialogoCLOSE: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRESPUESTA_SALIDA_MENSAJEList }

  TXMLRESPUESTA_SALIDA_MENSAJEList = class(TXMLNodeCollection, IXMLRESPUESTA_SALIDA_MENSAJEList)
  protected
    { IXMLRESPUESTA_SALIDA_MENSAJEList }
    function Add: IXMLRESPUESTA_SALIDA_MENSAJE;
    function Insert(const Index: Integer): IXMLRESPUESTA_SALIDA_MENSAJE;
    function Get_Item(Index: Integer): IXMLRESPUESTA_SALIDA_MENSAJE;
  end;

{ TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK }

  TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK = class(TXMLNode, IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK)
  protected
    { IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK }
    function Get_Label_: UnicodeString;
    function Get_Caption: UnicodeString;
    function Get_Tipo: ShortInt;
    function Get_URL: UnicodeString;
  end;

{ TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE }

  TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE = class(TXMLNode, IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE)
  protected
    { IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE }
    function Get_Label_: UnicodeString;
    function Get_Caption: UnicodeString;
    function Get_Tipo: ShortInt;
  end;

{ TXMLRESPUESTA_SALIDA_URL }

  TXMLRESPUESTA_SALIDA_URL = class(TXMLNode, IXMLRESPUESTA_SALIDA_URL)
  protected
    { IXMLRESPUESTA_SALIDA_URL }
    function Get_DESPACHO: UnicodeString;
    function Get_CLIENTE: UnicodeString;
  end;

{ Global Functions }

function GetRESPUESTA(Doc: IXMLDocument): IXMLRESPUESTA;
function LoadRESPUESTA(const sXML: String): IXMLRESPUESTA;
function GetMENSAJEVERBOID(const xRespuesta: IXMLRESPUESTA; const iVerboID: Integer): IXMLRESPUESTA_SALIDA_MENSAJE;
function HayErrorBitacora(const xRespuesta: IXMLRESPUESTA): Boolean;
function GetErrorBitacora(const xRespuesta: IXMLRESPUESTA; const sTipoError: String): IXMLRESPUESTA_BITACORA_EVENTO;
function GetErroresBitacora(const xRespuesta: IXMLRESPUESTA): String;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetRESPUESTA(Doc: IXMLDocument): IXMLRESPUESTA;
begin
  Result := Doc.GetDocBinding('RESPUESTA', TXMLRESPUESTA, TargetNamespace) as IXMLRESPUESTA;
end;

function LoadRESPUESTA(const sXML: string): IXMLRESPUESTA;
begin
  Result := LoadXMLData(sXML).GetDocBinding('RESPUESTA', TXMLRESPUESTA, TargetNamespace) as IXMLRESPUESTA;
end;

function GetMENSAJEVERBOID(const xRespuesta: IXMLRESPUESTA; const iVerboID: Integer): IXMLRESPUESTA_SALIDA_MENSAJE;
var
   iMensajes: Integer;
begin
     Result := nil;
     for iMensajes := 0 to xRespuesta.SALIDA.MENSAJE.Count-1 do
     begin
          if xRespuesta.SALIDA.MENSAJE.Items[iMensajes].VerboID = iVerboID then
          begin
               Result := xRespuesta.SALIDA.MENSAJE.Items[iMensajes];
               break;
          end;
     end;
end;

function HayErrorBitacora(const xRespuesta: IXMLRESPUESTA): Boolean;
var
   iEventos: Integer;
begin
     Result := False;
     if ( xRespuesta.BITACORA.CUANTOS > 0 ) then
        Result := True;
end;

function GetErrorBitacora(const xRespuesta: IXMLRESPUESTA; const sTipoError: String): IXMLRESPUESTA_BITACORA_EVENTO;
var
   iEventos: Integer;
begin
     Result := nil;
     for iEventos := 0 to xRespuesta.BITACORA.CUANTOS-1 do
     begin
          if xRespuesta.BITACORA.EVENTO.Items[iEventos].Tipo = sTipoError  then
          begin
               Result := xRespuesta.BITACORA.EVENTO.Items[iEventos];
               break;
          end;
     end;
end;

function GetErroresBitacora(const xRespuesta: IXMLRESPUESTA): String;
var
   iEventos: Integer;
begin
     Result := '';
     for iEventos := 0 to xRespuesta.BITACORA.CUANTOS-1 do
     begin
          Result := Result + xRespuesta.BITACORA.EVENTO.Items[iEventos].Text;
     end;
end;



{ TXMLRESPUESTA }

procedure TXMLRESPUESTA.AfterConstruction;
begin
  RegisterChildNode('BITACORA', TXMLRESPUESTA_BITACORA);
  RegisterChildNode('RESULTADO', TXMLRESPUESTA_RESULTADO);
  RegisterChildNode('SALIDA', TXMLRESPUESTA_SALIDA);
  inherited;
end;

function TXMLRESPUESTA.Get_BITACORA: IXMLRESPUESTA_BITACORA;
begin
  Result := ChildNodes['BITACORA'] as IXMLRESPUESTA_BITACORA;
end;

function TXMLRESPUESTA.Get_RESULTADO: IXMLRESPUESTA_RESULTADO;
begin
  Result := ChildNodes['RESULTADO'] as IXMLRESPUESTA_RESULTADO;
end;

function TXMLRESPUESTA.Get_SALIDA: IXMLRESPUESTA_SALIDA;
begin
  Result := ChildNodes['SALIDA'] as IXMLRESPUESTA_SALIDA;
end;

{ TXMLRESPUESTA_BITACORA }

function TXMLRESPUESTA_BITACORA.Get_Cuantos: Integer;
begin
  Result := AttributeNodes['Cuantos'].NodeValue;
end;

function TXMLRESPUESTA_BITACORA.Get_EVENTO: IXMLRESPUESTA_BITACORA_EVENTOList;
begin
  Result := FEVENTO;
end;

{ TXMLRESPUESTA_BITACORA_EVENTO }

function TXMLRESPUESTA_BITACORA_EVENTO.Get_Tipo: UnicodeString;
begin
  Result := AttributeNodes['Tipo'].NodeValue;
end;

function TXMLRESPUESTA_BITACORA_EVENTO.Get_ID: Integer;
begin
  Result := AttributeNodes['ID'].NodeValue;
end;

function TXMLRESPUESTA_BITACORA_EVENTO.Get_Detalle: UnicodeString;
begin
  Result := AttributeNodes['Detalle'].Text;
end;

{ TXMLRESPUESTA_SALIDA_MENSAJEList }

function TXMLRESPUESTA_BITACORA_EVENTOList.Add: IXMLRESPUESTA_BITACORA_EVENTO;
begin
  Result := AddItem(-1) as IXMLRESPUESTA_BITACORA_EVENTO;
end;

function TXMLRESPUESTA_BITACORA_EVENTOList.Insert(const Index: Integer): IXMLRESPUESTA_BITACORA_EVENTO;
begin
  Result := AddItem(Index) as IXMLRESPUESTA_BITACORA_EVENTO;
end;

function TXMLRESPUESTA_BITACORA_EVENTOList.Get_Item(Index: Integer): IXMLRESPUESTA_BITACORA_EVENTO;
begin
  Result := List[Index] as IXMLRESPUESTA_BITACORA_EVENTO;
end;


procedure TXMLRESPUESTA_BITACORA.AfterConstruction;
begin
  RegisterChildNode('EVENTO', TXMLRESPUESTA_BITACORA_EVENTO);
  FEVENTO := CreateCollection(TXMLRESPUESTA_BITACORA_EVENTOList, IXMLRESPUESTA_BITACORA_EVENTO, 'EVENTO') as IXMLRESPUESTA_BITACORA_EVENTOList;
  inherited;
end;


{ TXMLRESPUESTA_RESULTADO }

function TXMLRESPUESTA_RESULTADO.Get_TX_ID: Integer;
begin
  Result := AttributeNodes['TX_ID'].NodeValue;
end;

function TXMLRESPUESTA_RESULTADO.Get_TX_STATUS: Integer;
begin
  Result := AttributeNodes['TX_STATUS'].NodeValue;
end;

function TXMLRESPUESTA_RESULTADO.Get_Errores: Boolean;
begin
  Result := AttributeNodes['Errores'].NodeValue;
end;

{ TXMLRESPUESTA_SALIDA }

procedure TXMLRESPUESTA_SALIDA.AfterConstruction;
begin
  RegisterChildNode('MENSAJE', TXMLRESPUESTA_SALIDA_MENSAJE);
  RegisterChildNode('URL', TXMLRESPUESTA_SALIDA_URL);
  FMENSAJE := CreateCollection(TXMLRESPUESTA_SALIDA_MENSAJEList, IXMLRESPUESTA_SALIDA_MENSAJE, 'MENSAJE') as IXMLRESPUESTA_SALIDA_MENSAJEList;
  inherited;
end;

function TXMLRESPUESTA_SALIDA.Get_MENSAJE: IXMLRESPUESTA_SALIDA_MENSAJEList;
begin
  Result := FMENSAJE;
end;

function TXMLRESPUESTA_SALIDA.Get_URL: IXMLRESPUESTA_SALIDA_URL;
begin
  Result := ChildNodes['URL'] as IXMLRESPUESTA_SALIDA_URL;
end;

{ TXMLRESPUESTA_SALIDA_MENSAJE }

function TXMLRESPUESTA_SALIDA_MENSAJE.Get_VerboID: Integer;
begin
  Result := AttributeNodes['VerboID'].NodeValue;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE.Get_Tipo: Integer;
begin
  Result := AttributeNodes['Tipo'].NodeValue;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE.Get_Titulo: UnicodeString;
begin
  Result := AttributeNodes['Titulo'].Text;
end;

procedure TXMLRESPUESTA_SALIDA_MENSAJE.AfterConstruction;
begin
  RegisterChildNode('BotonDialogoOK', TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK);
  RegisterChildNode('BotonDialogoCLOSE', TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE);
  inherited;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE.Get_BotonDialogoCLOSE: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE;
begin
   Result := ChildNodes['BotonDialogoCLOSE'] as IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE.Get_BotonDialogoOK: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK;
begin
   Result := ChildNodes['BotonDialogoOK'] as IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE.Get_ErrorID: Integer;
begin
  Result := AttributeNodes['ErrorID'].NodeValue;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE.Get_HTML: UnicodeString;
begin
  Result := ChildNodes['HTML'].Text;
end;

{ TXMLRESPUESTA_SALIDA_MENSAJEList }

function TXMLRESPUESTA_SALIDA_MENSAJEList.Add: IXMLRESPUESTA_SALIDA_MENSAJE;
begin
  Result := AddItem(-1) as IXMLRESPUESTA_SALIDA_MENSAJE;
end;

function TXMLRESPUESTA_SALIDA_MENSAJEList.Insert(const Index: Integer): IXMLRESPUESTA_SALIDA_MENSAJE;
begin
  Result := AddItem(Index) as IXMLRESPUESTA_SALIDA_MENSAJE;
end;

function TXMLRESPUESTA_SALIDA_MENSAJEList.Get_Item(Index: Integer): IXMLRESPUESTA_SALIDA_MENSAJE;
begin
  Result := List[Index] as IXMLRESPUESTA_SALIDA_MENSAJE;
end;

{ TXMLRESPUESTA_SALIDA_URL }

function TXMLRESPUESTA_SALIDA_URL.Get_DESPACHO: UnicodeString;
begin
  Result := ChildNodes['DESPACHO'].Text;
end;

function TXMLRESPUESTA_SALIDA_URL.Get_CLIENTE: UnicodeString;
begin
  Result := ChildNodes['CLIENTE'].Text;
end;

{ TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK }

function TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK.Get_Caption: UnicodeString;
begin
  Result := AttributeNodes['Caption'].Text;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK.Get_Label_: UnicodeString;
begin
   Result := AttributeNodes['Label'].Text;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK.Get_Tipo: ShortInt;
begin
   Result := AttributeNodes['Tipo'].NodeValue;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK.Get_URL: UnicodeString;
begin
   Result := AttributeNodes['URL'].Text;
end;

{ TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE }

function TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE.Get_Caption: UnicodeString;
begin
   Result := AttributeNodes['Caption'].Text;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE.Get_Label_: UnicodeString;
begin
   Result := AttributeNodes['Label'].Text;
end;

function TXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE.Get_Tipo: ShortInt;
begin
   Result := AttributeNodes['Tipo'].NodeValue;
end;

end.
