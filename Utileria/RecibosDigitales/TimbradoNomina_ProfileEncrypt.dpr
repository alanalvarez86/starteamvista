program TimbradoNomina_ProfileEncrypt;
{$define DEVEX}

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

{$R 'TimbradoNominaResource.res' 'TimbradoNominaResource.rc'}
{$R *.dres}

uses
  FTressShell in 'FTressShell.pas' {TressShell},
  MidasLib,
  Vcl.Forms,
  ZetaClientTools,
  System.SysUtils,
  ZBaseDlgModal_DevEx in '..\..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  DBasicoCliente in '..\..\Datamodules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZBasicoNavBarShell in '..\..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  ZBaseConsulta in '..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseGridLectura_DevEx in '..\..\Tools\ZBaseGridLectura_DevEx.pas',
  ZcxWizardBasico in '..\..\Tools\ZcxWizardBasico.pas' {CXWizardBasico},
  ZcxBaseWizard in '..\..\Tools\ZcxBaseWizard.pas' {cxBaseWizard},
  ZetaCxWizard in '..\..\Componentes\ZetaCxWizard.pas' {$R *.RES},
  ZBasicoSelectGrid_DevEx in '..\..\Tools\ZBasicoSelectGrid_DevEx.pas' {BasicoGridSelect_DevEx};

{$R *.RES}
{$R WindowsXP.res}
{$R ..\Traducciones\Spanish.RES}

begin
//  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Timbrado de N�mina';
  Application.HelpFile := 'TimbradoNomina.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
        if Login( False ) then
        begin
             Show;
             Update;
             BeforeRun;
             Application.Run;
        end
        else
        begin
             Free;
        end;

     end;
end.
