{$HINTS OFF}
unit FDetalleConciliacionTimbrado_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons,
     Grids, DBGrids, DBCtrls, Db, ComCtrls, Variants,
     ZetaCommonLists,
     ZetaDBTextBox,
     ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     Menus, cxButtons, cxNavigator, cxDBNavigator, cxStyles,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
     ZetaCXGrid, cxContainer, cxTextEdit, cxMemo, cxDBEdit, FBaseReportes_DevEx,ZetaCommonClasses;

type
  TDetalleConciliacionTimbrado_DevEx = class(TForm)
    Datasource: TDataSource;
    SaveDialog: TSaveDialog;
    PanelInferior_DevEx: TPanel;
    DevEx_cxDBNavigatorEdicion: TcxDBNavigator;
    Imprimir_DevEx: TcxButton;
    ExportarBtn_DevEx: TcxButton;
    Salir_DevEx: TcxButton;
    ZetaCXGridDBTableView: TcxGridDBTableView;
    ZetaCXGridLevel: TcxGridLevel;
    ZetaCXGrid: TZetaCXGrid;
    CB_CODIGO: TcxGridDBColumn;
    NC_CURP: TcxGridDBColumn;
    NO_TIM_DESC: TcxGridDBColumn;
    NC_RES_DESC: TcxGridDBColumn;
    EMPLEADO: TcxGridDBColumn;
    NO_FACUUID: TcxGridDBColumn;
    NC_UUID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    //procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    //procedure DBGridDblClick(Sender: TObject);
    procedure Imprimir_DevExClick(Sender: TObject);
    procedure ExportarBtn_DevExClick(Sender: TObject);
    procedure Salir_DevExClick(Sender: TObject);
    procedure ZetaCXGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaCXGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaCXGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure ZetaCXGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    function GetBitacoraFile( var sArchivo: String ): Boolean;
    procedure Connect;
    procedure Disconnect;
    procedure ExportarExcelConciliados;
    procedure ReconnectGrid;
    procedure RefreshGrid;
    procedure ApplyMinWidth;
    procedure Exportar;
  public
    { Public declarations }
  end;

var
  DetalleConciliacionTimbrado_DevEx: TDetalleConciliacionTimbrado_DevEx;

  procedure ShowDetalleConciliacionPeriodo( Parametros: TZetaParams );
  procedure GetDetalleConciliacionPeriodo( const iPE_YEAR: Integer; const iPE_TIPO: Integer; const iPE_NUMERO: Integer; const sMes: String; const sRazonSocial: String  );

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaClientTools,
     ZGridModeTools,
     ZetaMessages,
     ZetaDialogo,
     ZetaClientDataSet,
     DInterfase,
     ZImprimeGrid; //(@am): Se cambia FBaseReportes por ZImprimeGrid pues ahora toda la logica de exportacion esta ahi.


{$R *.DFM}

procedure ShowDetalleConciliacionPeriodo( Parametros: TZetaParams );
const
     K_TITULO_DETALLE_CONCILIACION = 'Detalle de conciliaci�n - %s %d - Periodo %d';
begin
     with TDetalleConciliacionTimbrado_DevEx.Create( Application ) do
     begin
          try
             Caption := Format(K_TITULO_DETALLE_CONCILIACION, [ Parametros.ParamByName('MES').AsString,  Parametros.ParamByName('PE_YEAR').AsInteger,  Parametros.ParamByName('PE_NUMERO').AsInteger ]);
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure GetDetalleConciliacionPeriodo( const iPE_YEAR: Integer; const iPE_TIPO: Integer; const iPE_NUMERO: Integer; const sMes: String; const sRazonSocial: String );
var
   Params: TZetaParams;
begin
     with dmInterfase do
     begin
          try
             Params := TZetaParams.Create;
             Params.AddInteger('PE_YEAR', iPE_YEAR );
             Params.AddInteger('PE_TIPO', iPE_TIPO);
             Params.AddInteger('PE_NUMERO', iPE_NUMERO);
             Params.AddString('MES', sMes);
             Params.AddString('RAZON_SOCIAL', sRazonSocial);
             GetDetalleConciliacionPeriodo(Params);
             ShowDetalleConciliacionPeriodo(Params);
          finally
                 FreeAndNil(Params);
          end;
     end;
end;

{ ************ TShowLog ************ }

procedure TDetalleConciliacionTimbrado_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext := H00005_Bitacora_procesos;
     ZetaCXGridDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaCXGridDBTableView.DataController.Filter.AutoDataSetFilter := True;
end;

procedure TDetalleConciliacionTimbrado_DevEx.FormShow(Sender: TObject);
begin
     Connect;
     //Desactiva la posibilidad de agrupar
     ZetaCXGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     ZetaCXGridDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     ZetaCXGridDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaCXGridDBTableView.FilterBox.CustomizeDialog := False;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ApplyMinWidth;
     ZetaCXGridDBTableView.ApplyBestFit();  
end;

procedure TDetalleConciliacionTimbrado_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     ZGridModeTools.LimpiaCacheGrid(ZetaCXGridDBTableView);
     ZetaCXGridDBTableView.DataController.Filter.Root.Clear;
     Disconnect;
end;

procedure TDetalleConciliacionTimbrado_DevEx.Connect;
begin
     with dmInterfase do
     begin
          Datasource.Dataset := cdsDetalleConciliacionTimbrado;
          //dsLog.Dataset := cdsLogDetail;
     end;
     //DevEx:
     with Imprimir_DevEx do
     begin
          Enabled := ZAccesosMgr.CheckDerecho( D_CONS_BITACORA, K_DERECHO_IMPRESION );
          ExportarBtn_DevEx.Enabled := Enabled;
     end;
end;

procedure TDetalleConciliacionTimbrado_DevEx.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TDetalleConciliacionTimbrado_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if Key = Chr( VK_ESCAPE ) then
     begin
          Key := #0;
          Close;
     end;
end;

function TDetalleConciliacionTimbrado_DevEx.GetBitacoraFile( var sArchivo: String ): Boolean;
begin
     with SaveDialog do
     begin
          FileName := ExtractFileName( sArchivo );
          InitialDir := ExtractFilePath( sArchivo );
          if Execute then
          begin
               Result := True;
               sArchivo := FileName;
          end
          else
              Result := False;
     end;
end;

procedure TDetalleConciliacionTimbrado_DevEx.Exportar;
 var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;

    function ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ 'Conciliaci�n',
                             '',
                             stTodos,
                             stNinguno ] );
end;

begin

               if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes_DevEx.ExportarGrid( ZetaCXGridDBTableView ,
                                                ZetaCXGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
               end;

end;

procedure TDetalleConciliacionTimbrado_DevEx.ExportarExcelConciliados;
begin
     if ZetaCXGridDBTableView.DataController.DataSetRecordCount > 0 then
     begin
          Exportar;
     end
     else
     begin
          ZetaDialogo.ZError('Error en '+Self.Caption, '�Se encontr� un Error! '+ CR_LF + 'El grid de empleados conciliados est� vac�o',0)
     end;
end;

procedure TDetalleConciliacionTimbrado_DevEx.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     //dmConsultas.UsuarioGetText( Sender, Text, DisplayText );
end;

procedure TDetalleConciliacionTimbrado_DevEx.Imprimir_DevExClick(Sender: TObject);
begin
     if ZetaDialogo.zConfirm( 'Imprimir...', '� Desea Imprimir El Grid De ' + Caption + '?', 0, mbYes ) then
     begin
//          if oDataSet.IsEmpty then
//             raise Exception.Create('El Grid ' + sTitulo + ' est� Vac�o' )
//          else
              ZImprimeGrid.ImprimirGrid( ZetaCXGridDBTableView, ZetaCXGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM', '','',stNinguno,stNinguno);
      end;
end;

procedure TDetalleConciliacionTimbrado_DevEx.ExportarBtn_DevExClick(Sender: TObject);
begin
     ExportarExcelConciliados;
end;

procedure TDetalleConciliacionTimbrado_DevEx.Salir_DevExClick(Sender: TObject);
begin
     Close;
end;

procedure TDetalleConciliacionTimbrado_DevEx.ZetaCXGridDBTableViewDblClick(Sender: TObject);
begin
     //TZetaClientDataSet(dsLog.Dataset).Modificar;
end;

procedure TDetalleConciliacionTimbrado_DevEx.ZetaCXGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaCXGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaCXGridDBTableView, AItemIndex, AValueList );
end;

procedure TDetalleConciliacionTimbrado_DevEx.ZetaCXGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   //oColor: TColor;
   tipoBitacora:eTipoBitacora;
begin
  try
      //oColor:= ACanvas.Font.Color;
     for tipoBitacora:= tbError to tbErrorGrave do
     begin
          if ( ObtieneElemento( lfTipoBitacora, Ord( tipoBitacora))=  AViewInfo.GridRecord.DisplayTexts [2] ) then
          begin
               ACanvas.Font.Color := clRed;
          end;
     end;
  finally
         //ACanvas.Font.Color:= oColor;
  end;
end;

procedure TDetalleConciliacionTimbrado_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =35;
begin
     with  ZetaCXGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TDetalleConciliacionTimbrado_DevEx.ZetaCXGridDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if Key = VK_RETURN then
     begin
           //TZetaClientDataSet(dsLog.Dataset).Modificar;
     end;
end;

procedure TDetalleConciliacionTimbrado_DevEx.ReconnectGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaCXGridDBTableView );
end;

procedure TDetalleConciliacionTimbrado_DevEx.RefreshGrid;
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaCXGridDBTableView );
end;

end.
