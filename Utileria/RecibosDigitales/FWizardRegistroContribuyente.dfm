object WizardRegistroContribuyente: TWizardRegistroContribuyente
  Left = 296
  Top = 99
  Caption = 'Registro de Contribuyente'
  ClientHeight = 676
  ClientWidth = 572
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 122
    Top = 14
    Width = 64
    Height = 13
    Caption = 'Razon social:'
  end
  object ZetaDBTextBox2: TZetaDBTextBox
    Left = 193
    Top = 14
    Width = 276
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox2'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object dxWizardControl1: TdxWizardControl
    Left = 0
    Top = 0
    Width = 572
    Height = 676
    AutoSize = True
    Buttons.Back.Caption = '&Atras'
    Buttons.Back.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F342370000008349444154384F63201674D77733F634A2
      602610DDDDD8C5DC55DF25015546186031088C810631020D62842A230C800685
      0035FE06E2FFC81868D075525D346A100130F80CEAADEF55C1661008030D3280
      2A231E00351560338C6497C100D50DECAEEFCC001A4079D8C100D522030646A6
      81F8CA33AA18C800C2400341490AA8828101008A004FA9E01121AC0000000049
      454E44AE426082}
    Buttons.Cancel.Caption = '&Cancelar'
    Buttons.Cancel.Glyph.Data = {
      89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
      610000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC7249000000BA49444154384F8D913112C220104529BC574AB07746BC
      85F7B0F018293C84BD92135968FE861F11D8357FE617C0BE97998D639E311CA6
      E82FF9A8063398CDC725B84831BCE6BEA7D3FE9AAF9BE00D33985D2525CCF624
      05CC2E9214FD583D484B4907CEF5A3BB0FC32E1DFDAD370050856706AC7CC192
      745BC2CC66490F66FE4A2C98D11766FF628905B3AA640BCC361215C63E949DAC
      120BC6C2ACC58AE411C3B979ACB6AD49C0CAC08FA482995AB2C28C481498A1E4
      0B3BF701FFE789DE4F9390EE0000000049454E44AE426082}
    Buttons.CustomButtons.Buttons = <>
    Buttons.Finish.Caption = '&Aplicar'
    Buttons.Finish.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F34237000001FB49444154384FCD93CF2B055114C767DE
      F3E4577EC702C90E4549598884B9C34A6225B2B050786FE6D988CDDB4A562C10
      FF00A5B0B03233AFC4C242922CD850F2A388A5ADCF7926F5123D4A39F5E9CE9C
      7BCEF79E7BCFBDDABF32CB515AC4513AF89E5F9AED29DD76CD0CCB35F320CD77
      FFCCACB8D22C4F056DCF2C44AC0DA121C64A7F3A75B31C43B6962EC9880DC226
      DFA730A685F7BA3EF8CE6CAA89789D019273486C8069BE0FE1096EA96E960015
      E0232B8163EA7E6E92D9F82DCF0C59AE2A21B11B9611BB607C8413DB55F36CBB
      51F65E8AA31D5A09C8A5822441CB33E490B359AC1A4688DB863BB8855D9A301E
      F58CAA49AF33281DE9C3B9096B2421A832C6E36D1CB2C19CC121AB22E65A2006
      07700FE7B08A782F39A510A4FA447BC34CDCC035CC410DD5A611C8B6CC0AFEFB
      6105CE402A12C1188B36939BCF8281280B270C074E73075EE018C28848A76A61
      0264EECA670B4611AA864C7293CF98C95CCA1C603C826770F89F61942AC5F700
      B2AD25E88132844272453E195504E85439415370092228A3542242FB20E24D6C
      BFE0FDB27EF174E89674349D807A1216410EF815E41C37406E7715F06C78835F
      098949D710D311CB21D1807538A6E205C60EAA2E8ED0ADC938DD4AC5A2AE9246
      C8ED966B60C23062755493250BF961A91B1508727ED988E533861277E7B7E60B
      4A95BA9DEAB6FEC634ED0DA90748A38751FD620000000049454E44AE426082}
    Buttons.Help.Visible = False
    Buttons.Next.Caption = '&Siguiente'
    Buttons.Next.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F34237000000A449444154384F630081AEFA2E89EEC62E
      869EC66E0CDC5DDFCD0856440A001AC88664202332061AC80C55463C80BAF03A
      D080FF68F837D0C010A832E2C1A8812806F6D477A64095110FF018F81F285700
      55463CA0BA811DF51D3240CDDFD10D036160183A40951106F5F5F52C3D8D5DAB
      B119D4D3D4DD0F5546188C1A44180C5A8398801A61E51890DFCD0EC41C408388
      2FCFA0068134A2148A60DCD40D554508303000009CD04F09670CD6D200000000
      49454E44AE426082}
    Buttons.Next.GlyphAlignment = blGlyphRight
    Buttons.Next.Width = 78
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyph]
    Header.DescriptionFont.Charset = DEFAULT_CHARSET
    Header.DescriptionFont.Color = clDefault
    Header.DescriptionFont.Height = -11
    Header.DescriptionFont.Name = 'Tahoma'
    Header.DescriptionFont.Style = []
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC300000EC301C76F
      A86400000391494441545847BD573F681361144F55FC830E821D14C43F504144
      8A88828AA08343072197A2A20E22D4D1C1A14307C9D7BB2849EE2E26B9441C3A
      542C62AA8B20A58342878A05B1B4B95A9B455AE8D0808383940A1DEAFB7DF79A
      5E2E97B4A5177FF0C87DEF7BF7DEFBDEBFFB12DA28D222BDDF14FAFD946618A6
      A6E774A177A744AA8DB79B07183654A3D750F5DFA66AACF8D0405EE4F7B178B0
      308579920C2C780CFAD1477E255840B1C7505DA29474F06BC1804EDF4A8A97A5
      0127E7D76084EAA00729410D30EF912363C6F8D5609016F163CEE9F42921C40E
      66FB82EAE34F4A35FA79190CC8E86E282627D6CD2FC9CC9A9A91E66570A0507F
      4088795917E4C0305A9497C10035202320CCCBCCAA0B391BC8095E0603431857
      117ECC0166D5058A1169E065307066803EC2CB864094285AA3BC0C0E540363A6
      485EE7A52FE4A4D48C38B5E10B66050732FE0023B8511B4286C2BF406938C3AC
      608134503DDCE0650D78FF362F8307293F8D61941089C3CCAA00ADB79141B565
      90130F29CCF388042624C810C9C7E0E199C59A8BA4489E9345A91A4BC839D130
      E5FD206FFF3FE8B16C7BD3430EF80D20ADFFF381E860A92CDE4CF78891DA11B1
      E58B094E86C2C250C118F6169E3AF0EDBC785B5A890ECE0C89D7A56DCC96401B
      223DF87E60766C3A4AF09E14545D40A08CB72548690B45A0200667AA1C233EBE
      9C336BEFE9A3E0652723ADD654E48E652B859C1D7E9FB395AEDC64E408BFB606
      79E1A476721BAF2873F5389DBCDB8940A99C48F49D6076081716D73BB3BA9638
      248DD9CA8A1F5976389AFF7E732D821E055544A7F9852F23E4A285690B0E80E2
      4F3247C1A3D077B8E4973037C8489571ABA814734565A88A672BCEFDC1F9E8F0
      D5AB0EADDE76B4975FCE3A11F821E73E1CA3FDF955394A591FC25E65888C67C7
      EEEEC289BD4EE426955321F9217119AB4738A9A1E5F652F817D557E317A403AA
      FECE2B839CBB8D5B76E74EC8023E4EC4E4ADC7ADA401CDA25045613A167F6AD1
      9F946497570653D129389C2EF2934FDE929F08EF910549CEC00972EC133B3087
      EB542584EB52CCCCE85AA6FD59AF7E1C6DEADD47DBBA4EF8D72A86DBA8FA9FE3
      D7218A88F35C860C7EE14095927568D9B97CC8915CBB4F7BA4B8A6FA571DA8E1
      A3107D153520BF935788FE33A0CF6B0CD57360227271D30E34220C234C403226
      DC86F2B6720FE4E6A15630D8B83E8345B618D94EF9CEB80DBA09C6DDDDD134A0
      CFADAFB72C323A47512923E7D678E7253172852542A17F2E93173ABAE0498800
      00000049454E44AE426082}
    ParentFont = False
    OnButtonClick = dxWizardControl1ButtonClick
    OnPageChanging = dxWizardControl1PageChanging
    object dxWizardControlPageSeleccionRazonSocial: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Seleccione el contribuyente para el cual se desea realizar el re' +
        'gistro en el Sistema de Timbrado.'
      Header.Title = 'Registro / Actualizaci'#243'n de Contribuyente'
      ParentFont = False
      object Label10: TLabel
        Left = 85
        Top = 156
        Width = 68
        Height = 13
        Caption = 'Contribuyente:'
        Color = clWhite
        ParentColor = False
      end
      object RS_CODIGO: TZetaKeyLookup_DevEx
        Left = 159
        Top = 153
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        OnValidLookup = RS_CODIGOValidLookup
      end
    end
    object dxWizardControlPageRazonSocial: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Verifique que los datos del contribuyente seleccionado sean corr' +
        'ectos.'
      Header.Title = 'Datos del Contribuyente'
      ParentFont = False
      DesignSize = (
        550
        536)
      object Label1: TLabel
        Left = 101
        Top = 49
        Width = 68
        Height = 13
        Caption = 'Contribuyente:'
        Color = clWhite
        ParentColor = False
      end
      object Label18: TLabel
        Left = 136
        Top = 3
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'R.F.C.:'
        Color = clWhite
        ParentColor = False
      end
      object Label6: TLabel
        Left = 71
        Top = 73
        Width = 98
        Height = 13
        Caption = 'Representante legal:'
        Color = clWhite
        ParentColor = False
      end
      object Label14: TLabel
        Left = 34
        Top = 93
        Width = 135
        Height = 13
        Caption = 'R.F.C. Representante Legal:'
        Color = clWhite
        ParentColor = False
      end
      object Label11: TLabel
        Left = 135
        Top = 27
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'CURP:'
        Color = clWhite
        ParentColor = False
      end
      object Label19: TLabel
        Left = 97
        Top = 118
        Width = 72
        Height = 13
        Caption = 'R'#233'gimen fiscal:'
        Color = clWhite
        ParentColor = False
      end
      object RS_RFC: TDBEdit
        Left = 177
        Top = 0
        Width = 188
        Height = 21
        DataField = 'RS_RFC'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 0
      end
      object RS_LEGAL: TDBEdit
        Left = 177
        Top = 69
        Width = 274
        Height = 21
        DataField = 'RS_RLEGAL'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 1
      end
      object cxGroupBox3: TcxGroupBox
        Left = 42
        Top = 134
        Caption = ' Domicilio fiscal: '
        TabOrder = 2
        Height = 166
        Width = 457
        object Label13: TLabel
          Left = 104
          Top = 18
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Calle:'
          Color = clWhite
          ParentColor = False
        end
        object Label5: TLabel
          Left = 82
          Top = 42
          Width = 48
          Height = 13
          Caption = '# Exterior:'
          Color = clWhite
          ParentColor = False
        end
        object Label16: TLabel
          Left = 94
          Top = 64
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ciudad:'
          Color = clWhite
          ParentColor = False
        end
        object Label15: TLabel
          Left = 94
          Top = 112
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Estado:'
          Color = clWhite
          ParentColor = False
        end
        object Label17: TLabel
          Left = 63
          Top = 134
          Width = 67
          Height = 13
          Alignment = taRightJustify
          Caption = 'C'#243'digo postal:'
          Color = clWhite
          ParentColor = False
        end
        object Label7: TLabel
          Left = 212
          Top = 43
          Width = 45
          Height = 13
          Caption = '# Interior:'
        end
        object Label12: TLabel
          Left = 92
          Top = 88
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = 'Colonia:'
          Color = clWhite
          ParentColor = False
        end
        object RS_CALLE: TDBEdit
          Left = 138
          Top = 14
          Width = 274
          Height = 21
          DataField = 'RS_CALLE'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 0
        end
        object RS_NUMEXT: TDBEdit
          Left = 138
          Top = 38
          Width = 70
          Height = 21
          DataField = 'RS_NUMEXT'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 1
        end
        object RS_NUMINT: TDBEdit
          Left = 263
          Top = 39
          Width = 70
          Height = 21
          DataField = 'RS_NUMINT'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 2
        end
        object RS_CIUDAD: TDBEdit
          Left = 138
          Top = 61
          Width = 274
          Height = 21
          DataField = 'RS_CIUDAD'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 3
        end
        object RS_CODPOST: TDBEdit
          Left = 138
          Top = 131
          Width = 188
          Height = 21
          DataField = 'RS_CODPOST'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 6
        end
        object RS_ENTIDAD: TZetaDBKeyLookup_DevEx
          Left = 138
          Top = 108
          Width = 300
          Height = 21
          Enabled = False
          ReadOnly = True
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 5
          TabStop = True
          WidthLlave = 60
          DataField = 'RS_ENTIDAD'
          DataSource = dataSourceContribuyente
        end
        object RS_COLONIA: TDBEdit
          Left = 138
          Top = 85
          Width = 274
          Height = 21
          DataField = 'RS_COLONIA'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 4
        end
      end
      object RS_NOMBRE: TDBEdit
        Left = 177
        Top = 46
        Width = 274
        Height = 21
        DataField = 'RS_NOMBRE'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 3
      end
      object ActualizarCSD: TcxCheckBox
        Left = 276
        Top = 306
        Caption = 'Actualizar certificado de sello digital (CSD)'
        TabOrder = 4
        Transparent = True
        OnClick = ActualizarCSDClick
        Width = 230
      end
      object cxGroupBoxHTML: TcxGroupBox
        Left = 42
        Top = 258
        Anchors = []
        ParentFont = False
        Style.BorderStyle = ebsFlat
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        Height = 185
        Width = 457
        object WebBrowserMensajeHTML: TWebBrowser
          Left = 2
          Top = 3
          Width = 451
          Height = 160
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
          OnProgressChange = WebBrowserMensajeHTMLProgressChange
          OnBeforeNavigate2 = WebBrowserMensajeHTMLBeforeNavigate2
          OnDocumentComplete = WebBrowserMensajeHTMLDocumentComplete
          ControlData = {
            4C0000009D2E0000891000000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E12620A000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
      object RS_CURP: TDBEdit
        Left = 177
        Top = 23
        Width = 188
        Height = 21
        DataField = 'RS_CURP'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 6
      end
      object RS_RL_RFC: TDBEdit
        Left = 177
        Top = 92
        Width = 274
        Height = 21
        DataField = 'RS_RL_RFC'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 7
      end
      object ListaTimbradoCB_DevEx: TZetaKeyCombo
        Left = 177
        Top = 115
        Width = 273
        Height = 21
        BevelKind = bkSoft
        Style = csDropDownList
        Color = clHighlightText
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 8
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
    object dxWizardControlPageFiel: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Indique los archivos de llave privada y certificado, as'#237' como lo' +
        's datos requeridos'
      Header.Title = 'Llave privada y Certificado de Sello Digital '
      ParentFont = False
      object Label8: TLabel
        Left = 51
        Top = 229
        Width = 142
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre de persona que firma:'
        Color = clWhite
        ParentColor = False
      end
      object Label4: TLabel
        Left = 58
        Top = 205
        Width = 135
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contrase'#241'a de llave privada:'
        Color = clWhite
        ParentColor = False
      end
      object lbCert: TLabel
        Left = 109
        Top = 181
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Certificado (*.cer):'
        Color = clWhite
        ParentColor = False
      end
      object lbKey: TLabel
        Left = 93
        Top = 157
        Width = 100
        Height = 13
        Alignment = taRightJustify
        Caption = 'Llave privada (*.key):'
        Color = clWhite
        ParentColor = False
      end
      object Advertencia: TcxLabel
        Left = 2
        Top = 95
        Align = alCustom
        AutoSize = False
        Caption = 
          'Aseg'#250'rese que el archivo .CER sea del certificado de sello digit' +
          'al, ya que de utilizar la FIEL el timbrado ser'#225' invalido y por e' +
          'nde la n'#243'mina no ser'#225' deducible.'
        ParentColor = False
        ParentFont = False
        Style.BorderColor = clScrollBar
        Style.BorderStyle = ebsFlat
        Style.Color = clInfoBk
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -12
        Style.Font.Name = 'Meiryo UI'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfFlat
        Style.LookAndFeel.NativeStyle = False
        Style.TextStyle = []
        Style.TransparentBorder = True
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfFlat
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.Kind = lfFlat
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.Kind = lfFlat
        StyleHot.LookAndFeel.NativeStyle = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.Alignment.Vert = taVCenter
        Properties.ShadowedColor = clInfoBk
        Properties.WordWrap = True
        Height = 51
        Width = 542
        AnchorY = 121
      end
      object personaFirma: TEdit
        Left = 195
        Top = 225
        Width = 270
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object llavePrivada: TEdit
        Left = 195
        Top = 201
        Width = 166
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        PasswordChar = '*'
        TabOrder = 4
      end
      object ArchivoCertificado: TEdit
        Left = 195
        Top = 177
        Width = 270
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object ArchivoLlavePrivada: TEdit
        Left = 195
        Top = 153
        Width = 270
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object BuscarLlavePrivada: TcxButton
        Left = 471
        Top = 153
        Width = 21
        Height = 21
        Hint = 'Buscar llave privada'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
          FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
          F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
          F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
          F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
          FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        OptionsImage.Spacing = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = BuscarLlavePrivadaClick
      end
      object BuscarCertificado: TcxButton
        Left = 471
        Top = 177
        Width = 21
        Height = 21
        Hint = 'Buscar certificado'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
          FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
          F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
          F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
          F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
          FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BuscarCertificadoClick
      end
    end
    object dxWizardControlPageCuenta: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Seleccione la cuenta de timbrado correspondiente al contribuyent' +
        'e.'
      Header.Title = 'Selecci'#243'n de cuenta en Sistema de Timbrado'
      ParentFont = False
      object txtPassword: TZetaDBTextBox
        Left = 177
        Top = 308
        Width = 273
        Height = 21
        AutoSize = False
        Caption = 'txtPassword'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CT_PASSWRD'
        DataSource = DataSourceCuentas
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblDI_IP: TLabel
        Left = 113
        Top = 311
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contrase'#241'a:'
        Color = clWhite
        ParentColor = False
        Visible = False
      end
      object Label2: TLabel
        Left = 85
        Top = 131
        Width = 80
        Height = 13
        Caption = 'Cuenta timbrado:'
        Color = clWhite
        ParentColor = False
      end
      object Label9: TLabel
        Left = 114
        Top = 157
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cuenta ID:'
        Color = clWhite
        ParentColor = False
      end
      object txtCuentaID: TZetaDBTextBox
        Left = 173
        Top = 155
        Width = 121
        Height = 17
        AutoSize = False
        Caption = 'txtCuentaID'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CT_ID'
        DataSource = DataSourceCuentas
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CT_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 171
        Top = 128
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CT_CODIGOValidLookup
        DataField = 'CT_CODIGO'
        DataSource = dataSourceContribuyente
      end
      object cxButton1: TcxButton
        Left = 362
        Top = 178
        Width = 108
        Height = 26
        Hint = 'Verificar conectividad'
        Caption = 'Probar conexi'#243'n'
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        LookAndFeel.SkinName = 'TressMorado2013'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF77A5FFFFBFD4FFFFFFFFFFFFD1E0
          FFFF82ACFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFB4CDFFFFD6E4FFFFD6E4FFFFE0EAFFFFFFFFFFFFE8F0
          FFFFD6E4FFFFD6E4FFFFC6D9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFFD3E2FFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6
          FFFFC1D6FFFFCCDDFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFCCDDFFFF6095FFFFBFD4
          FFFF9BBDFFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6599FFFF5B92FFFF6D9E
          FFFF6397FFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC4D8FFFF6397FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF84ADFFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFF87AFFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF72A1FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF8EB4FFFF5E94FFFF5B92
          FFFF5B92FFFF5B92FFFFE3ECFFFFF5F8FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1
          FFFFEAF1FFFFF2F6FFFFFAFCFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFD8E5FFFF6599FFFF5B92
          FFFF5B92FFFF5B92FFFF6397FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0
          FFFF70A0FFFF70A0FFFF6599FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF75A3FFFF84ADFFFF84ADFFFF84ADFFFF7AA7
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94FFFFF2F6FFFF75A3
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF689BFFFFABC7FFFFFFFFFFFFBAD1FFFF6A9C
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF689BFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFD8E5FFFFEAF1FFFFEAF1FFFFF2F6FFFFFFFFFFFFF2F6FFFFEAF1
          FFFFEAF1FFFFDEE9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC4D8FFFF6A9C
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFB4CDFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BB
          FFFFB4CDFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF9BBDFFFF6095
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF6A9CFFFF8CB2FFFF5B92FFFF9BBDFFFF6095
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF7AA7FFFFB7CFFFFF5B92FFFFCCDDFFFF6D9E
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF96B9FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0
          FFFF96B9FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFDEE9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE5EEFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Visible = False
        OnClick = cxButton1Click
      end
    end
    object dxWizardControlPageFirma: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Al leer y firmar la Carta de Manifiesto'#160#160'provista,'#160'se autoriza q' +
        'ue el Sistema de Timbrado pueda timbrar sus n'#243'minas dentro del m' +
        'arco legal establecido.'
      Header.Title = 'Carta de Manifiesto '
      ParentFont = False
      DesignSize = (
        550
        536)
      object cxCheckBoxAceptar: TcxCheckBox
        Left = 0
        Top = 312
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Aceptar y firmar'
        ParentBackground = False
        ParentColor = False
        Style.Color = clWhite
        TabOrder = 0
        Width = 169
      end
      object cxScrollBox1: TcxScrollBox
        Left = 0
        Top = 0
        Width = 550
        Height = 306
        Align = alTop
        TabOrder = 1
        object WebBrowser: TWebBrowser
          Left = 0
          Top = 0
          Width = 548
          Height = 304
          Align = alClient
          ParentShowHint = False
          ShowHint = False
          TabOrder = 0
          ExplicitHeight = 261
          ControlData = {
            4C000000A33800006B1F00000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E12620A000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object dxWizardControlPageRegistrar: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Al iniciar el proceso se cerrar'#225' el asistente, al concluir recib' +
        'ir'#225' una notificaci'#243'n presentando la bit'#225'cora con los resultados ' +
        'del proceso'
      Header.Title = 'Presione Aplicar para iniciar el proceso'
      ParentFont = False
      object Panel1: TPanel
        Left = 0
        Top = 2
        Width = 547
        Height = 89
        Align = alCustom
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxLabel1: TcxLabel
          Left = 66
          Top = 0
          Align = alClient
          AutoSize = False
          Caption = 
            'Al aplicar el proceso se registrar'#225' al  contribuyente en el Sist' +
            'ema de Timbrado  para as'#237' poder realizar las operaciones de Timb' +
            'rado.'
          ParentColor = False
          ParentFont = False
          Style.BorderColor = clScrollBar
          Style.BorderStyle = ebsFlat
          Style.Color = clInfoBk
          Style.Edges = [bTop, bRight, bBottom]
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Meiryo UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Properties.ShadowedColor = clInfoBk
          Properties.WordWrap = True
          Height = 89
          Width = 481
          AnchorY = 45
        end
        object cxImage1: TcxImage
          Left = 0
          Top = 0
          Align = alLeft
          AutoSize = True
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000040
            000000400806000000AA6971DE0000000467414D410000B18F0BFC6105000000
            097048597300000EBC00000EBC0195BC7249000004A149444154785EDD5B2D70
            1341148E40202A110804A202814020101515150804025181405454542011CC5D
            672A2A100844052282617A97834154545454202A2A2A1A72172A221011888A0A
            44057CDFE63549C94B73C9EDDE6DFBCD7CC94C76F7DECFFEBDDD7BA9B946B8BF
            7F2B8C7E2C85516B258CD3F5A0916D058DF6377C1F801DC3383BC4F74E90B43F
            B20EB81A6EB79E85F5CE6D79CCF542D868CEC1E0E5A0917E8271676123FB3B0B
            D1FE0F9D1536D257E1E7EC8E3CDE4F989E8EB397419CEE5271CDA0228423CF31
            4AF6C3B8B5E6DDC8E0708582479AE22E0859271C1522BE3A8449FAD8F48AA264
            19344EC7FA22EA9407CE47084F34A5AAA0E984EDF4BEA8E716E197EC416F08EA
            CA5445E8D4C5F782A8E90658E49E42D0CCABBA6B42B773676B431865AF7B0274
            E13E3188DB9BDC9544F5E280E11F34413E133A27569CC09ED70414659064DB88
            F85E9050B6AED5294A4496EFC58CD92073DEFEB047C02422FAE06F6ADDA24408
            2E22A683ACF6D6173C448ADF45C40858A6B52942D38149BA2822F241F679275B
            1DF6EC772266042CD3DA142542F3DF61DC9C17319301E39D05393C20899811B0
            4C6B6383B0E950C45C0D86B7DA036C118A9C865F7FDE13717DF03753A6B4B146
            2CB8226E3CCA88EDD91B5C63442416C0E6BCF94DA96B93907172E5D6682E2194
            86AE08671F935A9933E2382DE65E063D030F9576A4AD8AB0116786E69C983D80
            B37DD84746E95B317B00DEE4A8956F20616B2A66F7C021E1E21ACB6B0EC705BC
            C0542BDD68B6DE88F96E0390496494869DA0C36FADDC15FB21B9ACFEA55F72C0
            E80333F2645F3657DF4A3DB76CDEC5F0FFB1A417DA27B72033DAA2D613E3FD21
            54E2009E14F9A1165AA2B99DC1B17AD2A565150E08926C03FB7FBAAE155A63CE
            A368250E88B33A63FF2DADD01ABD7640BA0B07F0DD9B5EC11931EAC4EE3E2A1A
            01471C01075AA153FAE3802E1DD0D10A9DD21307903C03FCD20A9CD22F07B8BF
            8818A12F5300D127A7C08E56E894DE38203BAE99B414A5D029FD71C09EFB4048
            A32F0E6020046556B542A7F4C601EDCDD22F420D3D7180B92065C21156C3726F
            837C71C0C501ADF470D803079830F802A57BDF871130AC83BC0C2D2FFBC38B29
            D07E24A27BC09E585EBA5BC50EE0F947C40E60323095CA4E58BD03465FD19BDD
            A0ACF4B70A1D001B4FC7E61D97D60B558E0045F625707B501BDA64450E806DDD
            8989D6A55C936B0E88B2E76A5D9BCC9B30E57C47885ACB22AA0F6688A8752DD1
            AC6F79F30619229AE1A23CA828F1DCB1098CCCEBD3DA1425649E31F547C4E403
            1A2EA061F1E088899649BA6818B51ECAE3C7827506F52D25692A232E17AC2C4C
            39DF0B68304ED09E390D27ADFA93605E6D690FCE4BBE00C5949A89055FDB331D
            57CC981DE6ED71152F4F0A92AFBFD55CA059609CE06881724193886DCBF86160
            48AE5859185DB2E89C9F042E4C656773E4A1D9EA665DEDA74559999D79095D4E
            D031D3EDF33680E1C63F3B54F6072AC8EE725AE68EF05C80C279976094519474
            41C83AE55CF7EA1FA45C759981C924444D691BE44D0E2F33FCFF1F31D608E6E1
            F5F662DD98BC446F1F99DEFEFF0EEFBA80A96866FB4CB20D1853675A0A8D1A9E
            32DC55D0B3CC1CDFEBD56162556B8DD1A03CC6116AB57F058711FAC14F69E200
            00000049454E44AE426082}
          Style.BorderColor = clScrollBar
          Style.Edges = [bLeft, bTop, bBottom]
          TabOrder = 1
        end
      end
      object cxProgressBarEnvio: TcxProgressBar
        Left = 38
        Top = 195
        TabOrder = 1
        Width = 473
      end
      object cxLabel4: TcxLabel
        Left = 38
        Top = 172
        Caption = 'Avance:'
        Transparent = True
      end
      object cxStatus: TcxLabel
        Left = 42
        Top = 220
        Transparent = True
      end
    end
  end
  object dataSourceContribuyente: TDataSource
    Left = 688
    Top = 48
  end
  object OpenDialog: TOpenDialog
    FileName = 'D:\3win_20_2010_Build_1\Sistema\Copy of New Text Document.cer'
    Filter = 'Llave Privada|*.key'
    Left = 480
    Top = 8
  end
  object DataSourceCuentas: TDataSource
    Left = 480
    Top = 8
  end
end
