unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,

                     efcTimDatosPeriodos,
                     efcTimDatosSaldo,
                     efcTimDatosCuentas,
                     efcTimDatosContribuyentes,
                     efcTTiposConceptosSAT,
                     efcCatConceptos,

                     efcReportes,
                     efcQueryGral,
                     efcTOTAhorro,
                     efcTOTPresta,
                     efcCtasBancarias,
                     efcTiposDeposito,
                     efcCatCondiciones,
                     efcSistGlobales,
                     efcSistBitacora,
                     efcDiccion,
                     efcSistProcesos
                      );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonLists,
     FReportes_DevEx,
     FQueryGral_DevEx,
     FSistBitacora_DevEx,
     FSistProcesos_DevEx,
     FTablas_DevEx,
     FCatPeriodosNomina_DevEx,
     //FCatEstadoCuentaTimbrado_DevEx,
     FCatCuentasTimbrado_DevEx,
     FCatContribuyentesTimbrado_DevEx,
     FCatTipoSAT_DevEx,
     FCatConceptos_DevEx,
     DCliente;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin

          case Forma of

          efcTimDatosPeriodos              : Result := Consulta( D_TIM_DATOS_PERIODOS, TCatPeriodosNomina_DevEx );
          //efcTimDatosSaldo                 : Result := Consulta( D_TIM_DATOS_SALDO, TCatEstadoCuentaTimbrado_DevEx );
          efcTimDatosCuentas               : Result := Consulta( D_TIM_DATOS_CUENTAS, TCatCuentasTimbrado_DevEx );
          efcTimDatosContribuyentes        : Result := Consulta( D_TIM_DATOS_CONTRIBUYENTES, TCatContribuyentesTimbrado_DevEx );
          efcTTiposConceptosSAT            :Result := Consulta(D_TAB_OFI_SAT_TIPOS_CONCEPTO,TCatTipoSAT_DevEx);
          efcCatConceptos                  : Result := Consulta( D_CAT_NOMINA_CONCEPTOS, TCatConceptos_DevEx );


          efcReportes               : Result := Consulta( D_REPORTES_CAJAAHORRO, TReportes_DevEx );
          efcQueryGral              : Result := Consulta( D_AHORRO_SQL, TQueryGral_DevEx );
          efcSistBitacora           : Result := Consulta( D_AHORRO_BITACORA, TSistBitacora_DevEx );
          efcSistProcesos           : Result := Consulta( D_AHORRO_BITACORA, TSistProcesos_DevEx );
          else
              Result := Consulta( 0, nil );
          end;

end;

end.
