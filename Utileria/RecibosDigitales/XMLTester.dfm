object Form1: TForm1
  Left = 148
  Top = 39
  Width = 1035
  Height = 689
  Caption = 'Recibo Digital XML Parser Tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 515
    Top = 446
    Width = 44
    Height = 13
    Caption = 'Facturas:'
  end
  object Label2: TLabel
    Left = 15
    Top = 446
    Width = 36
    Height = 13
    Caption = 'Errores:'
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1002
    Height = 433
    ActivePage = TabSheet4
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Timbrados'
      object Button1: TButton
        Left = 770
        Top = 8
        Width = 231
        Height = 25
        Caption = 'Get Timbrado Respuesta (Solo Errores)'
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 770
        Top = 184
        Width = 231
        Height = 25
        Caption = 'Get Timbrado Respuesta (Con Facturas)'
        TabOrder = 1
        OnClick = Button2Click
      end
      object Memo2: TMemo
        Left = 24
        Top = 184
        Width = 745
        Height = 145
        Lines.Strings = (
          '<?xml version="1.0" encoding="iso-8859-1"?>'#10
          '<RESPUESTA>'#10
          '  <BITACORA Cuantos="0" />'#10
          
            '  <RESULTADO TX_ID="217" TX_STATUS="4" Errores="false" /><SALIDA' +
            '>'#10
          '  <ERRORES />'#10
          
            '  <FACTURAS><FACTURA Numero="6" Nombre="Ram??rez Gonz??lez, Maur' +
            'icio" FacturaID="1828" /><FACTURA Numero="7" Nombre="Gomez Lopez' +
            ', '
          
            'Juan Pedro" FacturaID="1829" /><FACTURA Numero="8" Nombre="Lazcu' +
            'rain De los Montero, Rosa" FacturaID="1830" /><FACTURA Numero="9' +
            '" '
          
            'Nombre="Gonzalez Hernandez, Juliana" FacturaID="1831" /><FACTURA' +
            ' Numero="10" Nombre="Uribe Casta??on, Sara" FacturaID="1832" '
          '/></FACTURAS>'#10
          '</SALIDA></RESPUESTA>')
        TabOrder = 2
      end
      object Memo1: TMemo
        Left = 24
        Top = 8
        Width = 745
        Height = 169
        Lines.Strings = (
          '<?xml version="1.0" encoding="iso-8859-1"?>'#10
          '<RESPUESTA>'#10
          '  <BITACORA Cuantos="0" />'#10
          
            '  <RESULTADO TX_ID="212" TX_STATUS="3" Errores="false" /><SALIDA' +
            '>'#10
          '  <ERRORES>'#10
          
            '    <ERROR Orden="1" ErrorID="0" NominaNumero="4" Descripcion="Y' +
            'a hay ese HASH con otra serie/folio RFC=DEMO851010AB7  serie cfd' +
            'i=NOMINA '
          '29  serie base datos=NOMINA 23" Detalle="" />'#10
          
            '    <ERROR Orden="2" ErrorID="0" NominaNumero="5" Descripcion="Y' +
            'a hay ese HASH con otra serie/folio RFC=DEMO851010AB7  serie cfd' +
            'i=NOMINA '
          '30  serie base datos=NOMINA 24" Detalle="" />'#10
          '  </ERRORES>'#10
          '  <FACTURAS />'#10
          '</SALIDA></RESPUESTA>')
        TabOrder = 3
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Recibos'
      ImageIndex = 1
      object Memo4: TMemo
        Left = 24
        Top = 16
        Width = 737
        Height = 89
        Lines.Strings = (
          
            '<?xml version="1.0" encoding="iso-8859-1"?><RESPUESTA>  <BITACOR' +
            'A Cuantos="0" />  <RESULTADO TX_ID="0" TX_STATUS="4" '
          
            'Errores="false" /><SALIDA>  <ARCHIVO Url="http://ws.recibodigita' +
            'l.mx/Timbrado/Archivos/datos.xml" /></SALIDA></RESPUESTA>')
        TabOrder = 0
      end
      object Memo5: TMemo
        Left = 24
        Top = 112
        Width = 737
        Height = 89
        Lines.Strings = (
          
            '<?xml version="1.0" encoding="iso-8859-1"?><RESPUESTA>  <BITACOR' +
            'A Cuantos="0" />  <RESULTADO TX_ID="0" TX_STATUS="4" '
          
            'Errores="false" /><SALIDA>  <ARCHIVO Url="http://ws.recibodigita' +
            'l.mx/Timbrado/Archivos/datos.xml" /></SALIDA></RESPUESTA>')
        TabOrder = 1
      end
      object Button5: TButton
        Left = 760
        Top = 16
        Width = 231
        Height = 25
        Caption = 'Limpia Datos Confidenciales'
        TabOrder = 2
        OnClick = Button5Click
      end
      object Button3: TButton
        Left = 770
        Top = 216
        Width = 231
        Height = 25
        Caption = 'Get Recibo Respuesta ('#201'xito)'
        TabOrder = 3
        OnClick = Button3Click
      end
      object Memo3: TMemo
        Left = 24
        Top = 216
        Width = 745
        Height = 97
        Lines.Strings = (
          
            '<?xml version="1.0" encoding="iso-8859-1"?><RESPUESTA>  <BITACOR' +
            'A Cuantos="0" />  <RESULTADO TX_ID="0" TX_STATUS="4" '
          
            'Errores="false" /><SALIDA>  <ARCHIVO Url="http://ws.recibodigita' +
            'l.mx/Timbrado/Archivos/datos.xml" /></SALIDA></RESPUESTA>')
        TabOrder = 4
      end
      object Edit1: TEdit
        Left = 24
        Top = 312
        Width = 745
        Height = 21
        TabOrder = 5
        Text = 'Edit1'
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Regimen Fiscal'
      ImageIndex = 2
      object Label3: TLabel
        Left = 23
        Top = 160
        Width = 24
        Height = 13
        Caption = 'RFC:'
        OnClick = Button1Click
      end
      object Label4: TLabel
        Left = 23
        Top = 184
        Width = 34
        Height = 13
        Caption = 'Razon:'
        OnClick = Button1Click
      end
      object Label5: TLabel
        Left = 23
        Top = 208
        Width = 56
        Height = 13
        Caption = 'Regimenes:'
        OnClick = Button1Click
      end
      object Memo6: TMemo
        Left = 16
        Top = 16
        Width = 745
        Height = 143
        Lines.Strings = (
          
            '<?xml version="1.0" encoding="iso-8859-1"?><RESPUESTA>  <BITACOR' +
            'A Cuantos="0" />  <RESULTADO TX_ID="0" TX_STATUS="4" '
          'Errores="false" /><SALIDA><CONTRIBUYENTE>'
          '<EmpresaRFC>BBCC901015T88</EmpresaRFC>'
          '<EmpresaRazon>Industrias TAO, S.A. de C.V.</EmpresaRazon>'
          '<EmpresaRegimenes>Regimen General</EmpresaRegimenes>'
          '</CONTRIBUYENTE>'
          '</SALIDA></RESPUESTA>')
        TabOrder = 0
      end
      object Edit2: TEdit
        Left = 83
        Top = 160
        Width = 679
        Height = 21
        TabOrder = 1
        Text = 'Edit1'
      end
      object Button4: TButton
        Left = 762
        Top = 16
        Width = 231
        Height = 25
        Caption = 'Get R'#233'gimen Fiscal'
        TabOrder = 2
        OnClick = Button4Click
      end
      object Edit3: TEdit
        Left = 83
        Top = 184
        Width = 679
        Height = 21
        TabOrder = 3
        Text = 'Edit1'
      end
      object Edit4: TEdit
        Left = 83
        Top = 208
        Width = 679
        Height = 21
        TabOrder = 4
        Text = 'Edit1'
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      object Label6: TLabel
        Left = 487
        Top = 88
        Width = 42
        Height = 13
        Caption = 'Recibos:'
        OnClick = Button1Click
      end
      object Label7: TLabel
        Left = 487
        Top = 112
        Width = 46
        Height = 13
        Caption = 'Sin Email:'
        OnClick = Button1Click
      end
      object Label8: TLabel
        Left = 35
        Top = 544
        Width = 44
        Height = 13
        Caption = 'Facturas:'
      end
      object Label9: TLabel
        Left = 491
        Top = 160
        Width = 46
        Height = 13
        Caption = 'Sin Email:'
      end
      object Memo7: TMemo
        Left = 8
        Top = 8
        Width = 441
        Height = 289
        Lines.Strings = (
          '<?xml version="1.0" encoding="iso-8859-1"?>'
          '  <RESPUESTA>'
          '    <BITACORA Cuantos="0" />'
          '    <RESULTADO TX_ID="2073" TX_STATUS="4" Errores="false" />'
          '    <SALIDA>'
          '      <ERRORES />'
          '      <EMAILS Recibos="2" SinEmail="2">'
          '        <SINEMAIL>'
          
            '          <NOMINA Numero="211269" Nombre="Ramirez Perez, Silvest' +
            're" />'
          
            '          <NOMINA Numero="211269" Nombre="Ramirez Perez, Silvest' +
            're" />'
          '        </SINEMAIL>'
          '      </EMAILS>'
          '    </SALIDA>'
          '  </RESPUESTA>')
        TabOrder = 0
      end
      object Button6: TButton
        Left = 490
        Top = 24
        Width = 231
        Height = 25
        Caption = 'Get Respuesta Envio Recibos'
        TabOrder = 1
        OnClick = Button6Click
      end
      object Edit5: TEdit
        Left = 547
        Top = 88
        Width = 150
        Height = 21
        TabOrder = 2
        Text = 'Edit1'
      end
      object Edit6: TEdit
        Left = 547
        Top = 112
        Width = 150
        Height = 21
        TabOrder = 3
        Text = 'Edit1'
      end
      object DBGrid3: TDBGrid
        Left = 491
        Top = 176
        Width = 475
        Height = 125
        DataSource = DataSourceSinEmail
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 15
    Top = 462
    Width = 475
    Height = 221
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBGrid2: TDBGrid
    Left = 515
    Top = 462
    Width = 475
    Height = 221
    DataSource = DataSourceFacturas
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DataSource1: TDataSource
    Left = 1064
    Top = 128
  end
  object DataSourceFacturas: TDataSource
    Left = 832
    Top = 296
  end
  object DataSourceSinEmail: TDataSource
    Left = 820
    Top = 96
  end
end
