unit FWizExportarConceptosConfiguracion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, cxContainer, cxEdit, cxProgressBar, Vcl.ExtCtrls, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, Vcl.Menus, Vcl.StdCtrls, cxTextEdit, cxButtons,
  cxClasses, cxShellBrowserDialog, ZetaAsciiFile, ZetaClientDataSet;

type
  TWizExportarConceptosConfiguracion = class(TcxBaseWizard)
    PanelProgressBar: TPanel;
    ProgressBar: TcxProgressBar;
    gbArchivoConceptosConfiguracion: TcxGroupBox;
    cxLabel1: TcxLabel;
    btnExportarConfiguracionConcepto: TcxButton;
    txtPathExportarConfiguracionConcepto: TcxTextEdit;
    SaveDialog: TSaveDialog;
    cxOpenDialogExportacionConcepto: TcxShellBrowserDialog;
    procedure btnExportarConfiguracionConceptoClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtPathExportaConfigutacionConceptoPropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizExportarConceptosConfiguracion: TWizExportarConceptosConfiguracion;

const
     K_CONCEPTOS_CONFIGURACION = 'SELECT CO_NUMERO,CO_DESCRIP, CO_SAT_CLP, CO_SAT_TPP,CO_SAT_CLN, CO_SAT_TPN, CO_SAT_EXE, CO_SAT_CON FROM CONCEPTO WHERE CO_NUMERO < 1000';

implementation

{$R *.dfm}


uses
    ZetaCommonTools, ZetaDialogo, ZetaCommonClasses,
    ZetaClientTools, ZetaCommonLists, DConsultas,DCliente,DInterfase;


procedure TWizExportarConceptosConfiguracion.btnExportarConfiguracionConceptoClick(Sender: TObject);
begin
     inherited;
     cxOpenDialogExportacionConcepto.Path := ExtractFilePath( Application.ExeName );
     if cxOpenDialogExportacionConcepto.Execute then
        txtPathExportarConfiguracionConcepto.Text := VerificaDir (cxOpenDialogExportacionConcepto.Path);
end;

function TWizExportarConceptosConfiguracion.EjecutarWizard: Boolean;
var
   oConsultaConceptosExportar: TZetaClientDataSet;
   sFileName : String;
   FileConcepto : TextFile;
   lArchivoAbierto : Boolean;
   h: THandle;
begin
     inherited;
     Result := True;
     oConsultaConceptosExportar := TZetaClientDataSet.Create( self );
     try
        try
           oConsultaConceptosExportar.Data := dmConsultas.ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, K_CONCEPTOS_CONFIGURACION );
           with SaveDialog do
           begin
                Filter := 'Archivos CSV (*.csv)|*.csv';
                DefaultExt := 'csv';
                sFileName := StrDef (ExtractFileName(txtPathExportarConfiguracionConcepto.Text), 'ConfiguracionConcepto.csv');
                FileName := ChangeFileExt( sFileName, '.csv' );
                InitialDir := txtPathExportarConfiguracionConcepto.Text;
                if Execute then
                begin
                     sFileName := FileName;
                     with dmInterfase do
                     begin
                          if FileExists( sFileName ) and ZetaDialogo.zConfirm(Self.Caption, 'El Archivo ' + sFileName + ' Ya Existe' + CR_LF + '� Desea Sobreescribirlo ?', 0, mbYes ) then
                          begin
                               if dmInterFase.IsOpen( sFileName ) then
                               begin
                                    ZetaDialogo.ZError( Self.Caption, 'Archivo ' + sFileName + ' en uso',  0 );
                                    Result := False;
                               end
                               else
                               begin
                                    try
                                       Result := DeleteFile( sFileName );
                                    except
                                          on Error: Exception do
                                          begin
                                              ZetaDialogo.zExcepcion( Self.Caption, 'Archivo ' + sFileName + ' en uso', Error, 0 );
                                              Result := False;
                                          end;
                                    end;
                               end;
                          end
                          else
                              Result := False;
                          if ( not FileExists( sFileName ) ) then
                          begin
                               ZInformation( Self.Caption, CrearAsciiConcepto( oConsultaConceptosExportar, FileName ), 0 );
                          end;
                     end;
                end
                else
                    Result := False;
           end;
         except
               on Error: Exception do
               begin
                    ZInformation( Self.Caption, 'Error al exportar configuraci�n de conceptos de Timbrado: ' + Error.Message, 0 );
                    Result := False;
               end;
         end;
     finally
            FreeAndNil( oConsultaConceptosExportar );
     end;
end;
procedure TWizExportarConceptosConfiguracion.FormCreate(Sender: TObject);
begin
     inherited;
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
     HelpContext := H60621_Catalogo_conceptos;
     ProgressBar.Visible := False;
end;

procedure TWizExportarConceptosConfiguracion.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Al aplicar este proceso se generar� la exportaci�n de la configuraci�n de conceptos de Timbrado con los par�metros indicados.';
     txtPathExportarConfiguracionConcepto.Text := ExtractFilePath( Application.ExeName );
end;

procedure TWizExportarConceptosConfiguracion.txtPathExportaConfigutacionConceptoPropertiesEditValueChanged(
  Sender: TObject);
begin
     inherited;
     if ( Length( txtPathExportarConfiguracionConcepto.Text) < 68 )  then
        txtPathExportarConfiguracionConcepto.Properties.Alignment.Horz := taLeftJustify
     else
         txtPathExportarConfiguracionConcepto.Properties.Alignment.Horz := taRightJustify;
end;

procedure TWizExportarConceptosConfiguracion.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( StrVacio( txtPathExportarConfiguracionConcepto.Text ) ) then
               begin
                     ZetaDialogo.ZError( Self.Caption, 'El archivo de configuraci�n de concepto de Timbrado est� vac�o', 0 );
                     CanMove := False;
               end
               else if FileExists(txtPathExportarConfiguracionConcepto.Text) then
               begin
                    CanMove := ZConfirm(Self.Caption ,'El archivo de configuraci�n de conceptos de Timbrado ya existe �Desea reemplazarlo?',0,mbYes);
               end;
          end;
     end;
end;

procedure TWizExportarConceptosConfiguracion.CargaParametros;
begin
     Descripciones.Clear;
     with Descripciones do
     begin
          AddString( 'Exportar archivo en', txtPathExportarConfiguracionConcepto.Text );
     end;
     inherited;
end;


end.
