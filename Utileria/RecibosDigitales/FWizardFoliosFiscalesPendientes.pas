unit FWizardFoliosFiscalesPendientes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,
  Timbres,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ExtCtrls, ZetaKeyLookup, ZetaDBTextBox, Buttons,
  IdCoder, IdCoder3to4, IdCoderMIME, IdBaseComponent, ZetaCommonClasses, ZetaCommonTools, ZetaClientTools,
  Mask, DBCtrls, cxMemo, cxRichEdit, DTablas, FTimbramexClasses,
  ZetaEdit, ZetaFilesTools, OleCtrls, SHDocVw, cxScrollBox, TressMorado2013,
  ZetaKeyLookup_DevEx, ZetaFecha, dxGDIPlusClasses, cxImage,ZetaClientDataSet,dateutils;

type
  TWizardFoliosfiscalespendientes = class(TdxWizardControlForm)
    dxWizardControl1: TdxWizardControl;
    dataSourceContribuyente: TDataSource;
    OpenDialog: TOpenDialog;
    Label3: TcxLabel;
    ZetaDBTextBox2: TZetaDBTextBox;
    DataSourceCuentas: TDataSource;
    cxGroupBox2: TcxGroupBox;
    cxLabel1: TcxLabel;
    FInicial: TZetaFecha;
    cxLabel2: TcxLabel;
    FFinal: TZetaFecha;
    WizardPageEjecuta: TdxWizardControlPage;
    cxLabel3: TcxLabel;
    lblFaltante: TcxLabel;
    BarraProgreso: TcxProgressBar;
    WizardFecha: TdxWizardControlPage;
    WIzardPageCuenta: TdxWizardControlPage;
    LblStatus: TcxLabel;
    cxButton2: TcxButton;
    ZetaDBTextBox1: TZetaDBTextBox;
    Label9: TcxLabel;
    Label2: TcxLabel;
    CT_CODIGO: TZetaKeyLookup_DevEx;
    Panel3: TPanel;
    cxLabel4: TcxLabel;
    cxImage2: TcxImage;
    procedure cxButton1Click(Sender: TObject);
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure Transferir;
    procedure CT_CODIGOValidLookup(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
      FParametros : TZetaParams;
    { Private declarations }
   // procedure EnviarRecibos;
      Registros : Integer;
      Salir : boolean;
      errores: Integer;
    function HayFoliosFiscalesPendientes : boolean;

  public
    { Public declarations }
     Property parametros : TzetaParams Read FParametros ;
     property IRegistros : Integer  read Registros;

  end;

       type
       TCalcThread = class(TThread)
       private
                procedure RefreshCount;
       protected
                procedure Execute; override;
       public
             ConnStr : widestring;
             SQLString : widestring;
             ListBox : TListBox;
             Priority: TThreadPriority;
             ticksLabel : TLabel;
             Ticks : Cardinal;
             Parar:boolean;
       end;

var
  WizardFoliosfiscalespendientes: TWizardFoliosfiscalespendientes;
  Year : integer;

implementation

uses
FTimbramexHelper,DCliente;

{$R *.dfm}



procedure TCalcThread.RefreshCount;
begin
end;

procedure TCalcThread.Execute;
begin
end;

procedure TWizardFoliosfiscalespendientes.Transferir;
const
K_INACTIVO = 0;
K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
var
   modulo : double;
   pivote,iprom : integer;
   timbresSoap : TimbresFiscalesSoap;
   sPeticion, sRespuesta:  string;
   oCursor: TCursor;
   sServer : string;
   respuestaFacturaUUID : TzetaParams;
   UpdateFolioParametros: TzetaParams;
   startime : Tdate;
   endTime : Tdate;
   elepsedTime: tdate;
   calculadoActual,calculadoRestante,sprom,errorMsg : string;

   calculado,restante : double;
   crypt : TWSTimbradoCrypt;

function SecToTime(Sec: Integer): string;
var
   H, M, S: string;
   ZH, ZM, ZS: Integer;
begin
   ZH := Sec div 3600;
   ZM := Sec div 60 - ZH * 60;
   ZS := Sec - (ZH * 3600 + ZM * 60) ;

   if(ZH < 10)then
   begin
       H := '0' + IntToStr(ZH);
   end
   else
   begin
       H := IntToStr(ZH);
   end;

   if( ZM < 10) then
   begin
         M := '0'+ IntToStr(ZM);
   end
   else
   begin
         M := IntToStr(ZM) ;
   end;

   if( ZS < 10) then
   begin
       S := '0' + IntToStr(ZS);
   end
   else
   begin
       S := IntToStr(ZS) ;
   end;
   Result := H + ':' + M + ':' + S;
end;
begin
    // try
        UpdateFolioParametros:= tzetaparams.create;
        sServer := GetTimbradoServer;
        timbresSoap := nil;
        if ( sServer <> '' ) then
        BEGIN
             timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
        END;
        LblStatus.Caption := 'Iniciando descarga';
        salir := False;
        dxwizardcontrol1.Buttons.Cancel.Caption := 'Detener';
        BarraProgreso.properties.max := Registros;
        pivote := 0;
        modulo := (registros/100) + 1;
        respuestaFacturaUUID := TzetaParams.create;
        with dminterfase.cdsFoliosFiscalesPendientes do
        begin
             first;
             startime := now;
             while ( not eof) and (Salir = false) and (errores = 0) do
             begin
                  if ( timbresSoap <> nil ) then
                  begin
                       try
                          //invoco proceso de transferencia
                          sPeticion :=  dmInterfase.GetFacturaUUID( fieldbyname('NO_FACTURA').asInteger,  '');
                          
						  crypt := TWSTimbradoCrypt.Create;                				 
						  sRespuesta := crypt.Desencriptar(  timbresSoap.FacturaGetUUID( crypt.Encriptar( sPeticion ) ) );
						//sRespuesta := 				     timbresSoap.FacturaGetUUID( 				  sPeticion   );
						  FreeAndNil( crypt ); 
						  
                          respuestaFacturaUUID.clear();
                          respuestaFacturaUUID :=  FTimbramexHelper.GetFacturasUUID(sRespuesta ,respuestaFacturaUUID );
                          if( respuestaFacturaUUID.ParamByName('Errores').asBoolean ) then
                          begin
                               zError( K_ERR_TIMBRAMEX , 'No fue posible realizar la descarga, '  + respuestaFacturaUUID.ParamByName('ErrorDesc').asString,0);
                               errorMsg :='No fue posible realizar la descarga '  + respuestaFacturaUUID.ParamByName('ErrorDesc').asString;
                               errores :=1;
                          end
                          else
                          begin
                               if( respuestaFacturaUUID.ParamByName('FacturaId').AsInteger <> K_INACTIVO) then
                               begin
                                    dmInterfase.UpdateFoliosPendientes( respuestaFacturaUUID);
                               end;
                          end;
                       except
                          on Error:Exception do
                          begin
                          Errores := errores + 1;
                          errorMsg := Error.Message;
                          end;
                       end;
                       if(errores = 0) then
                       begin
                            Pivote := Pivote +1;
                            BarraProgreso.Position := pivote;
                            if(trunc(barraprogreso.Position) mod trunc(modulo)) = 0 then
                            begin
                                 LblStatus.Caption := 'Obteniendo UUID ('+ inttoStr(Pivote) + '/' + intToStr(registros)+')';
                                 calculado := SecondsBetween(startime, Now) / pivote ;
                                 calculadoActual := SecToTime(SecondsBetween(startime, Now));
                                 restante := calculado * (registros-pivote);
                                 calculadoRestante := SecToTime(trunc(restante));
                                 sprom := FloatToStr(calculado);
                                 //LblStatus.Caption :=  LblStatus.Caption  + 'Transcurrido : '+calculadoActual+' | restante : '+calculadoRestante + ' promedio : '+sprom;
                                  lblFaltante.Caption := 'Tiempo estimado para finalizar el proceso : '+calculadoRestante ;
                                 Application.ProcessMessages;
                            end;
                       end;
                  end;
                  next;
             end;
        end;
        if(errores = 0) then
        begin
             if(salir = true) then
             begin
                  dminterfase.EscribeBitacoraTRESS(caption,errorMsg);
                  zetadialogo.ZInformation(caption,'Se detubo la descarga, si desea continuar volver a iniciar el proceso',0);
                  LblStatus.Caption := 'Descarga detenida';
             end
             else
             begin
                  dminterfase.EscribeBitacoraTRESSExito(caption,'Se obtuvieron todos los folios satisfactoriamente');
                  zetadialogo.ZInformation(caption,'Se obtuvieron todos los folios satisfactoriamente',0);
                  LblStatus.Caption := 'Descarga finalizada';

             end;
        end
        else
        begin
             if(salir = true) then
             begin
                  dminterfase.EscribeBitacoraTRESS(caption,'Se detubo la descarga por el usuario');
                  zetadialogo.ZInformation(caption,'Se detubo la descarga, si desea continuar volver a iniciar el proceso',0);
                  LblStatus.Caption := 'Descarga detenida';
             end
             else
             begin
                 dminterfase.EscribeBitacoraTRESS(caption,errorMsg);
                  zetadialogo.ZInformation(caption,'Existieron errores en la descarga de los folios, favor de intentarlo de nuevo mas tarde',0);
                  LblStatus.Caption := 'Fallo en la descarga';
             end;
        end;
        dxwizardcontrol1.Buttons.Cancel.Caption := '&Salir';
end;
procedure TWizardFoliosfiscalespendientes.cxButton1Click(Sender: TObject);
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
begin
end;

procedure TWizardFoliosfiscalespendientes.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
const
     K_CONTRIBUYENTE_REGISTRADO = 'El contribuyente %s fue Registrado Exitosamente en el Sistema de Timbrado con el ID #%d' ;
     K_CONTRIBUYENTE_ACTUALIZADO = 'El contribuyente %s con el ID %d fue Actualizado Exitosamente en el Sistema de Timbrado' ;
var
   sMensajeFmt : string;
   contador ,Pivote :integer;
begin
     if(AKind = wcbkCancel) then
     begin
          Salir := true;
          if (dxwizardcontrol1.Buttons.Cancel.Caption = '&Cancelar') or (dxwizardcontrol1.Buttons.Cancel.Caption = '&Salir') then
          begin
               if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
               begin
                     if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
                     begin
                          ModalResult := mrCancel;
                     end;
               end
               else
               begin
                    ModalResult := mrCancel;
               end;
          end
          else
          begin
               dxwizardcontrol1.Buttons.Cancel.Caption := '&Salir';
               LblStatus.Caption := 'Detenido';
          end;
     end;
     if AKind = wcbkFinish then
     begin
         if ( ZetaDialogo.ZConfirm(Caption, 'Este proceso tardar� varios minutos'+CR_LF+'�Est� seguro de continuar?', 0, mbOK ) ) then
         begin
              dxwizardcontrol1.Buttons.Finish.Enabled := False;
              dxwizardcontrol1.Buttons.Back.Enabled := False;
              transferir;
         end;
     end;

end;




function TWizardFoliosfiscalespendientes.HayFoliosFiscalesPendientes:boolean;
begin
     result := dmInterfase.GetFoliosFiscalesPendientes(parametros);
end;


procedure TWizardFoliosfiscalespendientes.FormCreate(Sender: TObject);
var
sCT_CODIGO:string;
begin
     HelpContext:= H32137_AdquirirFoliosFiscales;
     FParametros := TzetaParams.create;
     Year := ZetaCommonTools.TheYear(Date);
     FInicial.Valor :=   ZetaCommonTools.FirsTDayOfYear(Year);
     FFinal.valor := Date;
     dmInterfase.cdsCuentasTimbramex.Conectar;
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
     sCT_CODIGO := dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString ;
     CT_CODIGO.LookupDataset := dmInterfase.cdsCuentasTimbramex;
     if  StrLLeno(sCT_CODIGO) then
     begin
          CT_CODIGO.LLave :=sCT_CODIGO;
          dmInterfase.cdsCuentasTimbramex.Locate('CT_CODIGO', sCT_CODIGO, []);
     end;
end;

procedure TWizardFoliosfiscalespendientes.dxWizardControl1PageChanging(
  Sender: TObject; ANewPage: TdxWizardControlCustomPage;
  var AAllow: Boolean);
begin
     if( ANewPage = WIzardPageCuenta) then
     begin
          fparametros.Clear;
          Fparametros.AddDate('FechaIni',Finicial.Valor);
          Fparametros.AddDate('FechaFin',FFinal.Valor);
          if(HayFoliosFiscalesPendientes) then
          begin
              Registros := dmInterfase.cdsFoliosFiscalesPendientes.RecordCount;
          end
          else
          begin
               AAllow := false;
               zetadialogo.ZWarning(Caption,'No hay folios fiscales pendientes en el periodo seleccionado',0, mbok);
          end;
     end;
     if( ANewPage = WizardPageEjecuta) then
     begin
          if (CT_CODIGO.llave = VACIO) then
          begin
               AAllow := false;
               zetadialogo.ZWarning(Caption,'Tienes que seleccionar una cuenta de Timbrado',0, mbok);
          end;
     end
end;

procedure TWizardFoliosfiscalespendientes.CT_CODIGOValidLookup(
  Sender: TObject);
begin
      DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;

procedure TWizardFoliosfiscalespendientes.cxButton2Click(Sender: TObject);
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
var
   sEchoResponse : string;
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;
begin
  try
        timbresSoap := nil;
        sServer := GetTimbradoServer;
        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
           if ( timbresSoap <> nil ) then
           begin                
				crypt := TWSTimbradoCrypt.Create;
                sEchoResponse := crypt.Desencriptar(  timbresSoap.Echo( crypt.Encriptar(TESTING_MESSAGE) ) );
			  //sEchoResponse :=  					  timbresSoap.Echo(                 TESTING_MESSAGE    );
				FreeAndNil( crypt );				
                ZInformation( Self.Caption, 'Hay Conectividad con el Servidor de Timbrado', 0);
           end;
        end
        else
        begin
           zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
        end;
     except on Error: Exception do
               zError( Self.Caption, K_SIN_CONECTIVIDAD, 0);
     end;
end;

procedure TWizardFoliosfiscalespendientes.FormShow(Sender: TObject);
begin
          FInicial.setfocus;
end;

end.
