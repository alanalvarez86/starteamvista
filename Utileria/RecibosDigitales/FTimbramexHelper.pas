unit FTimbramexHelper;

interface
{$INCLUDE JEDI.INC}

uses
  XMLDom, XMLIntf, MSXMLDom, XMLDoc, XSLProd, SysUtils, Classes,
  ZetaCommonClasses,  DInterfase,IdCoder, IdCoder3to4, IdCoderMIME, IdBaseComponent,
  ZetaCommonTools,  ZetaServerTools, Db, DBClient, ZetaClientDataSet, DXMLTools,
  FTimbramexClasses, ZetaRegistryCliente, Timbres, XSBuiltIns
  {$IFDEF DELPHIXE3_UP},Chilkat_v9_5_0_TLB
  {$else},CHILKATCRYPT2Lib_TLB
  {$endif}
  {$ifdef PROFILE},ZetaProfiler{$endif};


type TWSTimbradoCrypt = class( TObject )
private
   ChilkatCrypt_Send: TChilkatCrypt2;
   ChilkatCrypt_Recv: TChilkatCrypt2;

        procedure InitEncriptacion;
        procedure EndEncriptacion;
public
        Constructor Create;
        function  Encriptar( sValue : string ) : string;
        function  Desencriptar( sValue : string ) : string;
        Destructor  Destroy; override;
end;

{$ifdef PROFILE}
procedure WSProfile( sTitulo, sTexto : string );
{$endif}
procedure SQLProfile( sTitulo, sTexto : string );
procedure EncryptProfile( sTitulo, sTexto : string );

function DecryptPassword ( sPassword : string ) : string;

function VerificaVersionServer( var xVersionServer : Extended; var sMensajeError, sMensajeValidacion: string  )  : boolean;
function VerificaConexionServer( var xVersionServer : Extended; var sMensajeError, sMensajeValidacion: string  )  : boolean;

function GetTimbradoServer : string;
function GetTimbradoURL : string;
function GetRecolectorServer : string;
function GetRecolectorURL : string;
function GetRecibosPath : string;
function GetTimbradoPaqueteSize : integer;
function SetRecibosPath( sRecibosPath :string) : boolean;
function GetManifiesto( const sXML: String ): TRespuestaStruct;
function GetAgregar( const sXML: String ): TRespuestaStruct;
function GetCancelar( const sXML: String ): TRespuestaStruct;
function GetRegimenFiscal( const sXML: String ): TRespuestaStruct;
function GetCredenciales( sCT_CODIGO : string ; iContribuyenteID : integer; txID: integer = 0; lTXINIT: boolean = false) : string;
function GetTimbradoRespuesta( const sXML : string ; lPorCancelacion : boolean  = FALSE) : TRespuestaStruct;
function GetRecibosRespuesta( const sXML : string;  lEnvioEmail : boolean = FALSE ) : TRespuestaStruct;
function GetFacturasEmpty : TZetaClientDataSet;
function GetFacturasUUID( const sXML : string; Parametros: tzetaparams  ) : TZetaParams;
function ConsultarSaldoServer(  sCT_CODIGO : string ; iContribuyenteID : integer; dFecIni, dFecFin : TDateTime )  : TRespuestaSaldo;
procedure ObtenerCuentaTimbramexRSSeleccionado;
function ConsultarEmpleadosAConciliarTimbradoNomina(  sPeticion:String; fParams: TZetaParams  )  : TRespuestaConciliacionTimbradoNomina;

procedure AcumulaRespuestaTimbrado( resDestino, resOrigen : TRespuestaStruct ) ;

function LimpiaCuentaPass( sXML : string )  : string;


implementation

uses
     dCliente,
     dRecursos,
     dTablas,
     dCatalogos,
     dConsultas,
     dGlobal,
     dSistema,
     dNomina,
     DReportesXML,
     ZGlobalTress,
     FTressShell,
     ZAsciiTools,
     ZetaAsciiFile,
     ZetaDialogo,
     ZBaseThreads,
     ZBaseSelectGrid,
     dSuperAscii,
     StrUtils,
     ZReconcile,
     FAutoClasses,
     ZAccesosMgr,
     ZAccesosTress,
     DBasicoCliente, InvokeRegistry;

const
   K_RESPONSE_RESPUESTA = 'RESPUESTA';
   K_RESPONSE_RESPUESTA_ROOT = 'ROOT' ;
   K_RESPONSE_BITACORA = 'BITACORA';
   K_RESPONSE_EVENTO = 'EVENTO';
   K_RESPONSE_CUANTOS = 'Cuantos';
   K_RESPONSE_RESULTADO = 'RESULTADO';
   K_RESPONSE_TX_ID = 'TX_ID';
   K_RESPONSE_TX_STATUS = 'TX_STATUS';
   K_RESPONSE_ERRORES = 'Errores';
   K_RESPONSE_CDATA = '#cdata-section' ;


   K_REQUEST_PROCESS = 'PROCESS';
   K_REQUEST_CUENTA_ID = 'CUENTA_ID';
   K_REQUEST_CUENTA_PASSWORD = 'CUENTA_PASSWORD';
   K_REQUEST_SISTEMA = 'SISTEMA';
   K_REQUEST_SISTEMA_ID = 'SISTEMA_ID';
   K_REQUEST_CONT_ID = 'CONT_ID';
   K_REQUEST_USUARIO_ID = 'USUARIO_ID';
   K_REQUEST_USUARIO_NOMBRE = 'USUARIO_NOMBRE';
   K_REQUEST_DATA = 'DATA';
   K_REQUEST_EMPRESA_RFC = 'EmpresaRFC';
   K_REQUEST_EMPRESA_RAZON = 'EmpresaRazon';
   K_REQUEST_EMPRESA_REP_LEGAL = 'EmpresaRepLegal';
   K_REQUEST_EMRPESA_FIRMA = 'EmpresaFirma';

   K_SERVER_TEST_REVSERV = 'http://revolutionsrv';
   K_SERVER_TEST_NUBEDEV = 'http://nubedev';
   K_SERVER_TEST_NUBEDEV_RECOLECTOR = 'http://nubedev';
   K_SERVER_TEST_NUBEDEV_V33 = 'http://devwebsrv01';


   K_REQUEST_URL_TEST = '/timbres/Timbres.asmx';
   K_REQUEST_URL_TEST_NUBEDEV = '/WSReciboDigital/Timbres.asmx';
{$ifdef PILOTO}
   K_REQUEST_URL = '/ReciboDigitalWSDev/timbres.asmx';
{$else}
   K_REQUEST_URL = '/ReciboDigitalWS/timbres.asmx';
{$endif}

   K_REQUEST_URL_TEST_NUBEDEV_V33 = '/WsReciboDigitalv33/timbres.asmx';

   K_REQUEST_URL_RECOLECTOR_TEST = '/recolector/entrada.asmx';
   K_REQUEST_URL_RECOLECTOR_TEST_NUBEDEV = '/recolector/entrada.asmx';
   K_REQUEST_URL_RECOLECTOR = '/Recolector/Entrada.asmx';

   K_USO_HORARIO_TRESS = 1;

{$ifdef CFDI_VERSION_3}
   K_CFDI_VERSION = '3';
{$else}
   K_CFDI_VERSION = '2';
{$endif}

{$ifdef PROFILE}
  var
     iArchivoProfile : integer = 0;


  procedure WSProfile( sTitulo, sTexto : string );
  var
     ls : TStringList;
     sFecha, sDir : string;
  begin
       ls := TStringList.Create;
       ls.Text := sTexto;
       DateTimeToString(sFecha, 'ddmmyy_hhnn', Now);

       sDir :=  GetCurrentDir + '\ProfileLog';
       if SysUtils.ForceDirectories(sDir) then
       begin
           iArchivoProfile := iArchivoProfile + 1;
           ls.SaveToFile( Format( sDir +'\WSPROFILE_%s_%s_%d.txt ', [sTitulo,sFecha, iArchivoProfile] ) );
           FreeAndNil(ls);
       end;
  end;

{$endif}

  procedure SQLProfile( sTitulo, sTexto : string );
  var
     ls : TStringList;
     sFecha : string;
  begin
       ls := TStringList.Create;
       ls.Text := sTexto;
       DateTimeToString(sFecha, 'ddmmyy_hhnn', Now);
       ls.SaveToFile( Format( 'SQLPROFILE_%s_%s.txt ', [sTitulo,sFecha] ) );
       FreeAndNil(ls);
  end;


  procedure EncryptProfile( sTitulo, sTexto : string );
  var
     ls : TStringList;
     sFecha : string;
  begin
       ls := TStringList.Create;
       ls.Text := sTexto;
       DateTimeToString(sFecha, 'ddmmyy_hhnn', Now);
       ls.SaveToFile( Format( 'ENCRYPT_%s_%s.txt ', [sTitulo,sFecha] ) );
       FreeAndNil(ls);
  end;



   function GetTimbradoServer : string;
   var
      registro : TZetaClientRegistry;
   begin
       registro :=  TZetaClientRegistry.Create;
       Result :=  registro.TimbradoURL ;
       FreeAndNil(registro);
   end;

   function GetTimbradoURL : string;
   begin
       if ( GetTimbradoServer = K_SERVER_TEST_NUBEDEV_V33 ) then
          Result :=  GetTimbradoServer +  K_REQUEST_URL_TEST_NUBEDEV_V33
       else
       if ( GetTimbradoServer = K_SERVER_TEST_NUBEDEV ) then
          Result :=  GetTimbradoServer +  K_REQUEST_URL_TEST_NUBEDEV
       else
       if ( GetTimbradoServer = K_SERVER_TEST_REVSERV ) then
           Result :=  GetTimbradoServer +  K_REQUEST_URL_TEST
       else
           Result :=  GetTimbradoServer +  K_REQUEST_URL;
   end;

   function GetRecolectorServer : string;
   var
      registro : TZetaClientRegistry;
   begin
       registro :=  TZetaClientRegistry.Create;
       Result :=  registro.TimbradoURL ;
       FreeAndNil(registro);
   end;

   function GetRecolectorURL : string;
   begin
       if ( GetTimbradoServer = K_SERVER_TEST_NUBEDEV ) then
          Result :=  K_SERVER_TEST_NUBEDEV_RECOLECTOR +  K_REQUEST_URL_RECOLECTOR_TEST_NUBEDEV
       else
       if ( GetTimbradoServer = K_SERVER_TEST_REVSERV ) then
           Result :=  GetTimbradoServer +  K_REQUEST_URL_RECOLECTOR_TEST
       else
           Result :=  GetTimbradoServer +  K_REQUEST_URL_RECOLECTOR;
   end;






   function GetRecibosPath : string;
   var
      registro : TZetaClientRegistry;
   begin
       registro :=  TZetaClientRegistry.Create;
       Result :=  registro.RecibosPath ;
       FreeAndNil(registro);
   end;


   function GetTimbradoPaqueteSize : integer;
    var
      registro : TZetaClientRegistry;
   begin
       registro :=  TZetaClientRegistry.Create;
       Result :=  registro.TimbradoPaqueteSize ;
       FreeAndNil(registro);
   end;

    function SetRecibosPath( sRecibosPath :string) : boolean;
   var
      registro : TZetaClientRegistry;
   begin
       registro :=  TZetaClientRegistry.Create;
       try
          registro.RecibosPath := sRecibosPath ;
          Result := TRUE;
       except on Error: Exception do

              Result := FALSE;
        end;
       FreeAndNil(registro);
   end;

      function FileToBase46( sFilePath : string ) : string;
     var
        SourceStr: TFileStream;
        Encoder: TIdEncoderMIME;
     begin
         SourceStr := TFileStream.Create(sFilePath, fmOpenRead);
         try
           Encoder := TIdEncoderMIME.Create(nil);
           try
             Result := Encoder.Encode(SourceStr);
           finally
             Encoder.Free;
           end;
         finally
           SourceStr.Free;
         end;
     end;

      function DecodeBase46( base64Str : string ) : string;
     var
        Decoder: TIdDecoderMIME;

     begin
         REsult := '';
         try
           Decoder := TIdDecoderMIME.Create(nil);
           try
              Result := Decoder.DecodeString( base64Str)
{              Decoder.
             Result := Encoder.Encode(SourceStr);}
           finally
             Decoder.Free
             ;
           end;
         finally

         end;
     end;


     function LimpiaCuentaPass( sXML : string )  : string;
   var
      iBegin , iEnd : integer;
   begin
   //       <CUENTA_PASSWORD>%s</CUENTA_PASSWORD>
        iBegin :=  Pos( '<CUENTA_PASSWORD>', sXML ) ;
        iEnd :=  Pos( '</CUENTA_PASSWORD>', sXML ) ;
        Result := sXML ;
        Delete ( Result, iBegin, iEnd ) ;

        Result := StringReplace( Result , 'Timbramex' , '' , [rfIgnoreCase, rfReplaceAll] )
   end;



function HelperSelectNodes(xnRoot: IXmlNode; const nodePath: WideString): IXMLNodeList;
var
  intfSelect : IDomNodeSelect;
  intfAccess : IXmlNodeAccess;
  dnlResult  : IDomNodeList;
  intfDocAccess : IXmlDocumentAccess;
  doc: TXmlDocument;
  i : Integer;
  dn : IDomNode;
begin
  Result := nil;
  if not Assigned(xnRoot)
    or not Supports(xnRoot, IXmlNodeAccess, intfAccess)
    or not Supports(xnRoot.DOMNode, IDomNodeSelect, intfSelect) then
    Exit;

  dnlResult := intfSelect.selectNodes(nodePath);
  if Assigned(dnlResult) then
  begin
    Result := TXmlNodeList.Create(intfAccess.GetNodeObject, '', nil);
    if Supports(xnRoot.OwnerDocument, IXmlDocumentAccess, intfDocAccess) then
      doc := intfDocAccess.DocumentObject
    else
      doc := nil;

    for i := 0 to dnlResult.length - 1 do
    begin
      dn := dnlResult.item[i];
      Result.Add(TXmlNode.Create(dn, nil, doc));
    end;
  end;
end;

function GetAllNodesText( xNodo : IXMLNode;  sRuta : string ) : string;
var
    iNode : IXMLNode;
    idNodes : IXMLNodeList;
    XMLTools: DXMLTools.TdmXMLTools;
    i : integer;
begin
     XMLTools :=  TdmXMLTools.Create(nil);
     Result := VACIO;
     idNodes := HelperSelectNodes(xNodo, sRuta);

     for i := 0 to idNodes.Count -1  do
     begin
          iNode := idNodes.Get(i);
          Result := Result + XMLTools.TagAsString(iNode, '' );
     end;

     FreeAndNil( XMLTools );
end;


function GetManifiesto( const sXML: String ): TRespuestaStruct;
var
   Respuesta : TRespuestaStruct;
   xDocumento : IXMLDocument;
   xNodoActual : IXMLNode;
      xEventos : IXMLNodeList;
   iEvento : integer;
   sManifiesto : string;

begin
     xDocumento := LoadXMLData(sXML);

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA); 
     Respuesta.Bitacora.Cuantos := xNodoActual.Attributes[K_RESPONSE_CUANTOS];
     Respuesta.Bitacora.Eventos := TStringList.Create;
     xEventos :=    xNodoActual.ChildNodes;

     //<EVENTO Tipo="ErrorGrave" ID="0" Detalle="Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).">Error al leer parametros</EVENTO>
     for iEvento := 0 to xEventos.Count-1 do
     begin
          if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
          begin
              Respuesta.Bitacora.Eventos.Add(xEventos.Get(iEvento).Text );
          end;
     end;


     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.GetChildNodes().FindNode(K_RESPONSE_CDATA);

     if ( xNodoActual <> nil ) then
     begin
          sManifiesto := xNodoActual.Nodevalue ;
          sManifiesto := StringReplace( sManifiesto, 'UTF-16' , 'iso-8859-1' , [rfIgnoreCase,rfReplaceAll]);
          Respuesta.Resultado.Contenido := sManifiesto;

     end;

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESULTADO);
     if ( xNodoActual <> nil ) then
     begin
          Respuesta.Resultado.TX_ID := xNodoActual.Attributes[K_RESPONSE_TX_ID];
          Respuesta.Resultado.TX_IDi := StrToInt(Respuesta.Resultado.TX_ID);
          Respuesta.Resultado.TX_STATUS := xNodoActual.Attributes[K_RESPONSE_TX_STATUS];
          Respuesta.Resultado.Errores :=  StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE );
     end;

     Respuesta.transaccion := tTomarManifiesto;
     result := Respuesta;
end;

function GetAgregar( const sXML: String ): TRespuestaStruct;
var
   Respuesta : TRespuestaStruct;
   xDocumento : IXMLDocument;
   xDocumentoManifiesto :  IXMLDocument;
   xNodoActual : IXMLNode;
      xEventos : IXMLNodeList;
   iEvento : integer;

begin
     xDocumento := LoadXMLData(sXML);

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA); 
     Respuesta.Bitacora.Cuantos := xNodoActual.Attributes[K_RESPONSE_CUANTOS];
     Respuesta.Bitacora.Eventos := TStringList.Create;
     xEventos :=    xNodoActual.ChildNodes;

     //<EVENTO Tipo="ErrorGrave" ID="0" Detalle="Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).">Error al leer parametros</EVENTO>
     for iEvento := 0 to xEventos.Count-1 do
     begin
          if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
          begin
              Respuesta.Bitacora.Eventos.Add(xEventos.Get(iEvento).Text );
          end;
     end;



     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESULTADO);
     if ( xNodoActual <> nil ) then
     begin
          Respuesta.Resultado.TX_ID := xNodoActual.Attributes[K_RESPONSE_TX_ID];
          Respuesta.Resultado.TX_IDi := StrToInt(Respuesta.Resultado.TX_ID); 
          Respuesta.Resultado.TX_STATUS := xNodoActual.Attributes[K_RESPONSE_TX_STATUS];
          Respuesta.Resultado.Errores :=  StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE );
     end;

     Respuesta.transaccion := tRegistrar;
     result := Respuesta;
end;

function GetCancelar( const sXML: String ): TRespuestaStruct;
var
   Respuesta : TRespuestaStruct;
   xDocumento : IXMLDocument;
   xDocumentoManifiesto :  IXMLDocument;
   xNodoActual : IXMLNode;
      xEventos : IXMLNodeList;
   iEvento : integer;

begin
     xDocumento := LoadXMLData(sXML);

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA);
     Respuesta.Bitacora.Cuantos := xNodoActual.Attributes[K_RESPONSE_CUANTOS];
     Respuesta.Bitacora.Eventos := TStringList.Create;
     xEventos :=    xNodoActual.ChildNodes;

     //<EVENTO Tipo="ErrorGrave" ID="0" Detalle="Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).">Error al leer parametros</EVENTO>
     for iEvento := 0 to xEventos.Count-1 do
     begin
          if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
          begin
              Respuesta.Bitacora.Eventos.Add(xEventos.Get(iEvento).Text );
          end;
     end;



     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESULTADO);
     if ( xNodoActual <> nil ) then
     begin
          Respuesta.Resultado.TX_ID := xNodoActual.Attributes[K_RESPONSE_TX_ID];
          Respuesta.Resultado.TX_IDi := StrToInt(Respuesta.Resultado.TX_ID); 
          Respuesta.Resultado.TX_STATUS := xNodoActual.Attributes[K_RESPONSE_TX_STATUS];
          Respuesta.Resultado.Errores :=  StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE );
     end;


     Respuesta.transaccion := tCancelarRegistro;
     result := Respuesta;
end;


function DecryptPassword( sPassword  : string ) : string;
const
      CUENTA_PRUEBA = 'D6D53813-E47F-4E0F-A966-9F914DA1C4F3';
begin
     try
        if ( sPassword = CUENTA_PRUEBA ) then
           Result := CUENTA_PRUEBA
        else
            Result := ZetaServerTools.Decrypt( sPassword );
      except on Error: Exception do
            Result := VACIO;
       end;

end;

function GetCredenciales( sCT_CODIGO : string ; iContribuyenteID : integer; txID: integer = 0; lTXINIT: boolean = false) : string;
 var
    iCuentaID, iSistemaID, iUsuario : integer ;
    sCuentaPass, sUsuarioNombre: string;
 const
      CUENTA_PRUEBA = 'D6D53813-E47F-4E0F-A966-9F914DA1C4F3';

 begin
      Result := VACIO;
      ObtenerCuentaTimbramexRSSeleccionado;
      with dmInterfase do
      begin
          iCuentaID := cdsCuentasTimbramex.FieldByName('CT_ID').AsInteger;
          sCuentaPass := cdsCuentasTimbramex.FieldByName('CT_PASSWRD').AsString;

          if ( sCuentaPass <> CUENTA_PRUEBA ) then
          begin
             // sCuentaPass := DecodeBase46( sCuentaPass );
              sCuentaPass := ZetaServerTools.Decrypt( sCuentaPass );
          end;

          iUsuario := dmCliente.Usuario;
          sUsuarioNombre := dmCliente.GetDatosUsuarioActivo.Nombre;
          iSistemaID := Autorizacion.Empresa;
          //iSistemaID := dmCliente.

          Result := VACIO;
          {
             VERSION_ARCHIVO = '2.19.120.1';
     VERSION_PRODUCTO = '2013';}

          Result := Result + Format( '<CUENTA_ID>%d</CUENTA_ID>', [iCuentaID] ) +char(10)+char(13);
          Result := Result + Format( '<CUENTA_PASSWORD>%s</CUENTA_PASSWORD>', [sCuentaPass] ) +char(10)+char(13);
          Result := Result + Format( '<TX_ID>%d</TX_ID>', [txID] ) +char(10)+char(13);
          if ( lTXINIT ) then
          Result := Result + Format( '<TX_INIT>%s</TX_INIT>', ['true'] ) +char(10)+char(13)
          else
              Result := Result + Format( '<TX_INIT>%s</TX_INIT>', ['false'] ) +char(10)+char(13);
          Result := Result + Format( '<SISTEMA>%d</SISTEMA>', [K_SISTEMA_TIPO] ) +char(10)+char(13);
          Result := Result + Format( '<SISTEMA_ID Version="%s" VersionSistema="%s" >%d</SISTEMA_ID>', [K_VERSION_TIMBRADO,  VERSION_PRODUCTO, iSistemaID] ) +char(10)+char(13);
          if ( iContribuyenteID > 0 ) then
             Result := Result + Format( '<CONT_ID>%d</CONT_ID>', [iContribuyenteID] ) +char(10)+char(13);
          Result := Result + Format( '<USUARIO_ID>%d</USUARIO_ID>', [iUsuario] ) +char(10)+char(13);
          Result := Result + Format( '<CLIENTE_ID>%d</CLIENTE_ID>', [iContribuyenteID] ) +char(10)+char(13);
          Result := Result + Format( '<USUARIO_NOMBRE>%s</USUARIO_NOMBRE>', [sUsuarioNombre] ) +char(10)+char(13);
          Result := Result + Format( '<CFDI_VERSION>%s</CFDI_VERSION>', [K_CFDI_VERSION] ) +char(10)+char(13);


      end;
  end;

function GetCredencialesTXID( sCT_CODIGO : string ; iContribuyenteID : integer; iTXID: integer; lTXINIT: boolean = true) : string;
 var
    iCuentaID, txID, iSistemaID, iUsuario : integer ;
    sCuentaPass, sUsuarioNombre: string;
 const
      CUENTA_PRUEBA = 'D6D53813-E47F-4E0F-A966-9F914DA1C4F3';

 begin
      Result := VACIO;
      ObtenerCuentaTimbramexRSSeleccionado;
      with dmInterfase do
      begin
          iCuentaID := cdsCuentasTimbramex.FieldByName('CT_ID').AsInteger;
          sCuentaPass := cdsCuentasTimbramex.FieldByName('CT_PASSWRD').AsString;

          if ( sCuentaPass <> CUENTA_PRUEBA ) then
          begin
             // sCuentaPass := DecodeBase46( sCuentaPass );
              sCuentaPass := ZetaServerTools.Decrypt( sCuentaPass );
          end;

          iUsuario := dmCliente.Usuario;
          sUsuarioNombre := dmCliente.GetDatosUsuarioActivo.Nombre;
          iSistemaID := Autorizacion.Empresa;
          //iSistemaID := dmCliente.

          Result := VACIO;
          {
             VERSION_ARCHIVO = '2.19.120.1';
     VERSION_PRODUCTO = '2013';}

          Result := Result + Format( '<CUENTA_ID>%d</CUENTA_ID>', [iCuentaID] ) +char(10)+char(13);
          Result := Result + Format( '<CUENTA_PASSWORD>%s</CUENTA_PASSWORD>', [sCuentaPass] ) +char(10)+char(13);
          Result := Result + Format( '<TX_ID>%d</TX_ID>', [iTXID] ) +char(10)+char(13);
          if ( lTXINIT ) then
          Result := Result + Format( '<TX_INIT>%s</TX_INIT>', ['true'] ) +char(10)+char(13)
          else
              Result := Result + Format( '<TX_INIT>%s</TX_INIT>', ['false'] ) +char(10)+char(13);
          Result := Result + Format( '<SISTEMA>%d</SISTEMA>', [K_SISTEMA_TIPO] ) +char(10)+char(13);
          Result := Result + Format( '<SISTEMA_ID Version="%s" VersionSistema="%s" >%d</SISTEMA_ID>', [K_VERSION_TIMBRADO,  VERSION_PRODUCTO, iSistemaID] ) +char(10)+char(13);
          if ( iContribuyenteID > 0 ) then
             Result := Result + Format( '<CONT_ID>%d</CONT_ID>', [iContribuyenteID] ) +char(10)+char(13);
          Result := Result + Format( '<USUARIO_ID>%d</USUARIO_ID>', [iUsuario] ) +char(10)+char(13);
          Result := Result + Format( '<USUARIO_NOMBRE>%s</USUARIO_NOMBRE>', [sUsuarioNombre] ) +char(10)+char(13);

      end;
  end;

function GetFacturasUUID( const sXML : string; Parametros: tzetaparams ) : TZetaParams;
   var
   xDocumento : IXMLDocument;
   xNodoActual : IXMLNode;
   xEventos : IXMLNodeList;
   error:string;
   iEvento: integer;
   procedure GetFacturasUUID ;
   var
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
      XMLTools: DXMLTools.TdmXMLTools;
      i: integer;

   begin
        XMLTools :=  TdmXMLTools.Create(nil);
        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/FACTURA' );
        try
        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             Parametros.Addstring('FacturaID', intToStr(XMLTools.AttributeasInteger( iNode, 'FacturaID', 0)));
             Parametros.Addstring('FACTURAUUID', XMLTools.AttributeAsString( iNode, 'FacturaUUID', ''));
             Parametros.addInteger('FacturaActivo',XMLTools.AttributeAsInteger( iNode, 'FacturaActivo', 0));
        end;
        except
        begin

        end;
        end;

   end;
begin
     xDocumento := LoadXMLData(sXML);
     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     GetFacturasUUID;

     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESULTADO);
     if ( xNodoActual <> nil ) then
     begin
        Parametros.addboolean('Errores', StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE ));
     end;
     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA);
     xEventos :=    xNodoActual.ChildNodes;
     error :='';
     for iEvento := 0 to xEventos.Count-1 do
     begin
          if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
          begin
              error := error + xEventos.Get(iEvento).Text ;
          end;
          Parametros.Addstring('ErrorDesc',error);
     end;
     Result := Parametros;
end;




function GetTimbradoRespuesta( const sXML : string ; lPorCancelacion : boolean) : TRespuestaStruct;
   var
   Respuesta : TRespuestaStruct;
   xDocumento : IXMLDocument;
   xNodoActual : IXMLNode;
      xEventos : IXMLNodeList;



   {
     <ERROR Orden="1" ErrorID="0" NominaNumero="4" Descripcion="Ya hay ese HASH con otra serie/folio RFC=DEMO851010AB7  serie cfdi=NOMINA 27  serie base datos=NOMINA 25" Detalle="" />\
     }
   function GetErrores : TZetaClientDataSet;
   var
      cdsErrores : TZetaClientDataSet;
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);
        cdsErrores := TZetaClientDataSet.Create( nil );
        with cdsErrores do
        begin
           AddIntegerField( 'ORDEN' );
           AddIntegerField( 'ERRORID' );
           AddIntegerField( 'NUMERO' );
           AddStringField('DESCRIPCION' , 255 );
           AddStringField('DETALLE', 1024 ) ;

           CreateTempDataset;
        end;


        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/ERRORES/ERROR' );

        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             with cdsErrores do
             begin
                  Append;
                  FieldByName('ORDEN').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Orden', 0);
                  FieldByName('ERRORID').AsInteger := XMLTools.AttributeAsInteger( iNode, 'ErrorID', 0);
                  FieldByName('NUMERO').AsInteger := XMLTools.AttributeAsInteger( iNode, 'NominaNumero', 0);
                  FieldByName('DESCRIPCION').AsString := XMLTools.AttributeAsString( iNode, 'Descripcion', '');
                  FieldByName('DETALLE').AsString := XMLTools.AttributeAsString( iNode, 'Detalle', '');
                  Post;
             end;

        end;

        Result := cdsErrores;
   end;


   function GetFacturas : TZetaClientDataSet;
   var
      cdsFacturas : TZetaClientDataSet;
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);
        cdsFacturas := TZetaClientDataSet.Create( nil );
        with cdsFacturas do
        begin
           AddIntegerField( 'NUMERO' );
           AddStringField('NOMBRE' , 255 );
           AddStringField('SELLO' , 255 );
           AddStringField('FACTURAUUID' , 255 );
           AddIntegerField( 'FACTURAID' );
           CreateTempDataset;
        end;

{
	<FACTURAS>
			<FACTURA Numero="4" Nombre="Gutierrez Barbosa, Jos?? Gerard" FacturaID="1654" />
			<FACTURA Numero="5" Nombre="Juarez Mu??oz, Fernanda Maria" FacturaID="1655" />}
        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/FACTURAS/FACTURA' );

        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             with cdsFacturas do
             begin
                  Append;
                  FieldByName('NUMERO').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Numero', 0);
                  FieldByName('NOMBRE').AsString := XMLTools.AttributeAsString( iNode, 'Nombre', '');
                  FieldByName('FACTURAID').AsInteger := XMLTools.AttributeAsInteger( iNode, 'FacturaID', 0);
                  FieldByName('SELLO').AsString := XMLTools.AttributeAsString( iNode, 'FacturaSello', '');
				              FieldByName('FACTURAUUID').AsString := XMLTools.AttributeAsString( iNode, 'FacturaUUID', '');
                  Post;
             end;

        end;

        Result := cdsFacturas;
   end;


   function GetSaldo : string;
   var
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);

        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/SALDO_ACTUAL' );

        Result := VACIO;
        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             Result := XMLTools.TagAsString(iNode, '' );
        end;
        FreeAndNil( XMLTools );

   end;

   function GetCancelarStatus : integer;
   var
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);

        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/STATUS' );

        Result := 0;
        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             Result := XMLTools.TagAsInteger(iNode, 0 );
        end;
        FreeAndNil( XMLTools );

   end;

   function GetCancelarConteo : integer;
   var
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);

        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/CONTEO' );

        Result := 0;
        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             Result := XMLTools.TagAsInteger(iNode, 0 );
        end;
        FreeAndNil( XMLTools );

   end;

   function GetCancelarFacturasConteo : integer;
   var
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);

        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/FACTURAS' );

        Result := 0;
        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             Result := XMLTools.AttributeAsInteger( iNode, 'Conteo', 0);
        end;
        FreeAndNil( XMLTools );
   end;


   function  GetRecolectorLog : TRecolectorLog;
   var
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);

        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/LOG' );

        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             if ( XMLTools.AttributeAsInteger( iNode, 'Registrar', 0) = 1 ) then
                Result.Registrar := TRUE
             else
                 Result.Registrar := FALSE;

             Result.UltimoRegistro := XMLTools.AttributeISOAsDateTime( iNode, 'UltimoRegistro', 0);
        end;
        FreeAndNil( XMLTools );
   end;


   procedure GetBitacora;
   var
      iEvento : integer;
       XMLTools: DXMLTools.TdmXMLTools;
   begin
        Respuesta.Bitacora.Cuantos := xNodoActual.Attributes[K_RESPONSE_CUANTOS];
         Respuesta.Bitacora.Eventos := TStringList.Create;
         xEventos :=    xNodoActual.ChildNodes;
         XMLTools :=  TdmXMLTools.Create(nil);
         //<EVENTO Tipo="ErrorGrave" ID="0" Detalle="Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).">Error al leer parametros</EVENTO>
         for iEvento := 0 to xEventos.Count-1 do
         begin
              if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
              begin
                  Respuesta.Bitacora.Eventos.Add(xEventos.Get(iEvento).Text );


                  with  Respuesta.Resultado.ErroresDS do
                  begin
                       Append;
                       FieldByName('ORDEN').AsInteger := XMLTools.AttributeAsInteger( xEventos.Get(iEvento), 'Orden', 0);
                       FieldByName('ERRORID').AsInteger := XMLTools.AttributeAsInteger( xEventos.Get(iEvento), 'ID', 0);
                       FieldByName('NUMERO').AsInteger := 0;
                       FieldByName('DESCRIPCION').AsString := xEventos.Get(iEvento).Text;
                       FieldByName('DETALLE').AsString := XMLTools.AttributeAsString( xEventos.Get(iEvento), 'Detalle', '');
                       Post;
                  end;



              end;
         end;
           FreeAndNil( XMLTools );

   end;
begin
     xDocumento := LoadXMLData(sXML);

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);

     Respuesta.Resultado.ErroresDS :=   GetErrores;
     Respuesta.Resultado.FacturasDS := GetFacturas;
     Respuesta.Resultado.Saldo := GetSaldo;
     Respuesta.recolector := GetRecolectorLog;
     Respuesta.Resultado.CancelarStatus := GetCancelarStatus;
     Respuesta.Resultado.CancelarConteo := GetCancelarConteo;
     Respuesta.Resultado.CancelarFacturasConteo := GetCancelarFacturasConteo;

     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA);
     GetBitacora;

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESULTADO);
     if ( xNodoActual <> nil ) then
     begin
          Respuesta.Resultado.TX_ID := xNodoActual.Attributes[K_RESPONSE_TX_ID];
          Respuesta.Resultado.TX_IDi := StrToInt(Respuesta.Resultado.TX_ID);
          Respuesta.Resultado.TX_STATUS := xNodoActual.Attributes[K_RESPONSE_TX_STATUS];
          Respuesta.Resultado.Errores :=  StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE );
     end;


     if lPorCancelacion then
        Respuesta.transaccion := tCancelarTimbrado
     else
          Respuesta.transaccion := tTimbrar;
     result := Respuesta;
end;



function GetSinEmailEmpty : TZetaClientDataSet;
var
   cdsSinEmail : TZetaClientDataSet;
begin
     cdsSinEmail := TZetaClientDataSet.Create( nil );
     with cdsSinEmail do
     begin
        AddIntegerField( 'NUMERO' );
        AddStringField('NOMBRE' , 255 );
        CreateTempDataset;
     end;
     Result := cdsSinEmail;
end;


function GetRecibosRespuesta( const sXML : string;  lEnvioEmail : boolean ) : TRespuestaStruct;
  var
   Respuesta : TRespuestaStruct;
   xDocumento : IXMLDocument;

   xNodoActual : IXMLNode;
      xEventos : IXMLNodeList;
   iEvento : integer;



   {
     <ERROR Orden="1" ErrorID="0" NominaNumero="4" Descripcion="Ya hay ese HASH con otra serie/folio RFC=DEMO851010AB7  serie cfdi=NOMINA 27  serie base datos=NOMINA 25" Detalle="" />\
     }
   function GetErrores : TZetaClientDataSet;
   var
      cdsErrores : TZetaClientDataSet;
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);
        cdsErrores := TZetaClientDataSet.Create( nil );
        with cdsErrores do
        begin
           AddIntegerField( 'ORDEN' );
           AddIntegerField( 'ERRORID' );
           AddIntegerField( 'NUMERO' );
           AddStringField('DESCRIPCION' , 255 );
           AddStringField('DETALLE', 512 ) ;

           CreateTempDataset;
        end;


        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/ERRORES/ERROR' );

        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             with cdsErrores do
             begin
                  Append;
                  FieldByName('ORDEN').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Orden', 0);
                  FieldByName('ERRORID').AsInteger := XMLTools.AttributeAsInteger( iNode, 'ErrorID', 0);
                  FieldByName('NUMERO').AsInteger := XMLTools.AttributeAsInteger( iNode, 'NominaNumero', 0);
                  FieldByName('DESCRIPCION').AsString := XMLTools.AttributeAsString( iNode, 'Descripcion', '');
                  FieldByName('DETALLE').AsString := XMLTools.AttributeAsString( iNode, 'Detalle', '');
                  Post;
             end;

        end;

        Result := cdsErrores;
   end;

   procedure  GetArchivo;
    var
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/ARCHIVO' );

        Respuesta.Resultado.ArchivoUrl := VACIO;
        Respuesta.Resultado.ArchivoUrl2 := VACIO;

        Respuesta.Resultado.ArchivoNombre1 := VACIO;
        Respuesta.Resultado.ArchivoNombre2 := VACIO;

        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);

             if ( i = 0 ) then
             begin
                Respuesta.Resultado.ArchivoUrl := XMLTools.AttributeAsString( iNode, 'Url', '');
                Respuesta.Resultado.ArchivoNombre1 := XMLTools.AttributeAsString( iNode, 'Nombre', '');
             end
             else
             begin
                 Respuesta.Resultado.ArchivoUrl2 := XMLTools.AttributeAsString( iNode, 'Url', '');
                 Respuesta.Resultado.ArchivoNombre2 := XMLTools.AttributeAsString( iNode, 'Nombre', '');
             end;
        end;
   end;

   function  GetSinEmail : TZetaClientDataSet;
    var
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/EMAILS' );
        Result := GetSinEmailEmpty;
        Respuesta.Resultado.Recibos := 0;
        Respuesta.Resultado.SinEmail := 0;

        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             Respuesta.Resultado.Recibos := XMLTools.AttributeAsInteger( iNode, 'Recibos', 0);
             Respuesta.Resultado.SinEmail := XMLTools.AttributeAsInteger( iNode, 'SinEmail', 0);
        end;

        if Respuesta.Resultado.SinEmail > 0 then
        begin
              idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/EMAILS/SINEMAIL/NOMINA' );

               for i := 0 to idNodes.Count -1  do
               begin
                    iNode := idNodes.Get(i);

                    with Result do
                    begin
                         Append;
                         FieldByName('NUMERO').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Numero', 0);
                         FieldByName('NOMBRE').AsString := XMLTools.AttributeAsString( iNode, 'Nombre', '');
                         Post;
                    end;
               end;

        end;
   end;

begin
     xDocumento := LoadXMLData(sXML);

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);

     Respuesta.Resultado.ErroresDS :=   GetErrores;
     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     GetArchivo;
     if lEnvioEmail then
        Respuesta.Resultado.SinEmailDS:= GetSinEmail;
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA);
     Respuesta.Bitacora.Cuantos := xNodoActual.Attributes[K_RESPONSE_CUANTOS];
     Respuesta.Bitacora.Eventos := TStringList.Create;
     xEventos :=    xNodoActual.ChildNodes;

     //<EVENTO Tipo="ErrorGrave" ID="0" Detalle="Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).">Error al leer parametros</EVENTO>
     for iEvento := 0 to xEventos.Count-1 do
     begin
          if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
          begin
              Respuesta.Bitacora.Eventos.Add(xEventos.Get(iEvento).Text );
          end;
     end;

     Respuesta.Resultado.Errores :=  xEventos.Count > 0;


     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESPUESTA);
     if ( xNodoActual <> nil ) then
     begin
          Respuesta.Resultado.TX_ID := xNodoActual.Attributes[K_RESPONSE_TX_ID];
          Respuesta.Resultado.TX_IDi := StrToInt(Respuesta.Resultado.TX_ID);
          Respuesta.Resultado.TX_STATUS := xNodoActual.Attributes[K_RESPONSE_TX_STATUS];
          Respuesta.Resultado.Errores :=  StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE );
     end;

     Respuesta.transaccion := tTomarRecibos;
     result := Respuesta;
end;



function GetFacturasEmpty : TZetaClientDataSet;
   var
      cdsFacturas : TZetaClientDataSet;
   begin

        cdsFacturas := TZetaClientDataSet.Create( nil );
        with cdsFacturas do
        begin
           AddIntegerField( 'NUMERO' );
           AddStringField('NOMBRE' , 255 );
           AddStringField('SELLO' , 255 );
           AddIntegerField( 'FACTURAID' );
           AddStringField('FACTURAUUID' , 255 );
           CreateTempDataset;
        end;
        Result := cdsFacturas;
   end;

   procedure AcumulaRespuestaTimbrado( resDestino, resOrigen : TRespuestaStruct ) ;
begin
     resDestino.Bitacora.Eventos.AddStrings( resOrigen.Bitacora.Eventos );

    if not resDestino.Resultado.Errores  and resOrigen.Resultado.Errores then
       resDestino.Resultado.Errores := TRUE;

    resDestino.Resultado.Saldo := resOrigen.Resultado.Saldo;

    with  resOrigen.Resultado.ErroresDS do
    begin
         First;
         while not EOF do
         begin
              resDestino.Resultado.ErroresDS.Append;
              resDestino.Resultado.ErroresDS.FieldByName('ORDEN').AsInteger :=FieldByName('ORDEN').AsInteger;
              resDestino.Resultado.ErroresDS.FieldByName('ERRORID').AsInteger :=FieldByName('ERRORID').AsInteger;
              resDestino.Resultado.ErroresDS.FieldByName('NUMERO').AsInteger := FieldByName('NUMERO').AsInteger;
              resDestino.Resultado.ErroresDS.FieldByName('DESCRIPCION').AsString := FieldByName('DESCRIPCION').AsString;
              resDestino.Resultado.ErroresDS.FieldByName('DETALLE').AsString := FieldByName('DETALLE').AsString ;
              resDestino.Resultado.ErroresDS.Post;
              Next;
         end;
    end;

    {AddIntegerField( 'NUMERO' );
           AddStringField('NOMBRE' , 255 );
           AddStringField('SELLO' , 255 );
           AddIntegerField( 'FACTURAID' );}

    with  resOrigen.Resultado.FacturasDS do
    begin
         First;
         while not EOF do
         begin
              resDestino.Resultado.FacturasDS.Append;
              resDestino.Resultado.FacturasDS.FieldByName('NUMERO').AsInteger := FieldByName('NUMERO').AsInteger;
              resDestino.Resultado.FacturasDS.FieldByName('NOMBRE').AsString := FieldByName('NOMBRE').AsString;
              resDestino.Resultado.FacturasDS.FieldByName('SELLO').AsString := FieldByName('SELLO').AsString ;
              resDestino.Resultado.FacturasDS.FieldByName('FACTURAID').AsInteger :=FieldByName('FACTURAID').AsInteger;
              resDestino.Resultado.FacturasDS.FieldByName('FACTURAUUID').AsString :=FieldByName('FACTURAUUID').AsString;
              resDestino.Resultado.FacturasDS.Post;
              Next;
         end;
    end;


end;




function GetRegimenFiscal( const sXML: String ): TRespuestaStruct;
var
   Respuesta : TRespuestaStruct;
   xDocumento : IXMLDocument;
   xNodoActual : IXMLNode;
      xEventos : IXMLNodeList;
   iEvento : integer;



   function GetEmpresaRegimenes : string;
   begin
        Respuesta.Resultado.EmpresaRFC := GetAllNodesText(xNodoActual, 'SALIDA/CONTRIBUYENTE/EmpresaRFC' );
        Respuesta.Resultado.EmpresaRegimen := GetAllNodesText(xNodoActual, 'SALIDA/CONTRIBUYENTE/EmpresaRegimen' );
        Respuesta.Resultado.EmpresaRegimenes := GetAllNodesText(xNodoActual, 'SALIDA/CONTRIBUYENTE/EmpresaRegimenes' );
        Respuesta.Resultado.EmpresaRazon := GetAllNodesText(xNodoActual, 'SALIDA/CONTRIBUYENTE/EmpresaRazon' );
   end;


begin
     xDocumento := LoadXMLData(sXML);

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA); 
     Respuesta.Bitacora.Cuantos := xNodoActual.Attributes[K_RESPONSE_CUANTOS];
     Respuesta.Bitacora.Eventos := TStringList.Create;
     xEventos :=    xNodoActual.ChildNodes;

     //<EVENTO Tipo="ErrorGrave" ID="0" Detalle="Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).">Error al leer parametros</EVENTO>
     for iEvento := 0 to xEventos.Count-1 do
     begin
          if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
          begin
              Respuesta.Bitacora.Eventos.Add(xEventos.Get(iEvento).Text );
          end;
     end;


     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     if ( xNodoActual <> nil ) then
     begin
        GetEmpresaRegimenes;
     end;

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESULTADO);
     if ( xNodoActual <> nil ) then
     begin
          Respuesta.Resultado.TX_ID := xNodoActual.Attributes[K_RESPONSE_TX_ID];
          Respuesta.Resultado.TX_IDi := StrToInt(Respuesta.Resultado.TX_ID);
          Respuesta.Resultado.TX_STATUS := xNodoActual.Attributes[K_RESPONSE_TX_STATUS];
          Respuesta.Resultado.Errores :=  StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE );
     end;

     Respuesta.transaccion := tTomarRegimen;
     result := Respuesta;
end;




function GetRespuestaGenerica( const sXML: String ): TRespuestaStruct;
var
   Respuesta : TRespuestaStruct;
   xDocumento : IXMLDocument;
   xNodoActual : IXMLNode;
      xEventos : IXMLNodeList;


   function GetErrores : TZetaClientDataSet;
   var
      cdsErrores : TZetaClientDataSet;
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);
        cdsErrores := TZetaClientDataSet.Create( nil );
        with cdsErrores do
        begin
           AddIntegerField( 'ORDEN' );
           AddIntegerField( 'ERRORID' );
           AddIntegerField( 'NUMERO' );
           AddStringField('DESCRIPCION' , 255 );
           AddStringField('DETALLE', 1024 ) ;

           CreateTempDataset;
        end;


        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/ERRORES/ERROR' );

        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             with cdsErrores do
             begin
                  Append;
                  FieldByName('ORDEN').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Orden', 0);
                  FieldByName('ERRORID').AsInteger := XMLTools.AttributeAsInteger( iNode, 'ErrorID', 0);
                  FieldByName('NUMERO').AsInteger := XMLTools.AttributeAsInteger( iNode, 'NominaNumero', 0);
                  FieldByName('DESCRIPCION').AsString := XMLTools.AttributeAsString( iNode, 'Descripcion', '');
                  FieldByName('DETALLE').AsString := XMLTools.AttributeAsString( iNode, 'Detalle', '');
                  Post;
             end;

        end;

        Result := cdsErrores;
   end;



     procedure GetBitacora;
   var
      iEvento : integer;
       XMLTools: DXMLTools.TdmXMLTools;
   begin
        Respuesta.Bitacora.Cuantos := xNodoActual.Attributes[K_RESPONSE_CUANTOS];
         Respuesta.Bitacora.Eventos := TStringList.Create;
         xEventos :=    xNodoActual.ChildNodes;
         XMLTools :=  TdmXMLTools.Create(nil);
         //<EVENTO Tipo="ErrorGrave" ID="0" Detalle="Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).">Error al leer parametros</EVENTO>
         for iEvento := 0 to xEventos.Count-1 do
         begin
              if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
              begin
                  Respuesta.Bitacora.Eventos.Add(xEventos.Get(iEvento).Text );


                  with  Respuesta.Resultado.ErroresDS do
                  begin
                       Append;
                       FieldByName('ORDEN').AsInteger := XMLTools.AttributeAsInteger( xEventos.Get(iEvento), 'Orden', 0);
                       FieldByName('ERRORID').AsInteger := XMLTools.AttributeAsInteger( xEventos.Get(iEvento), 'ID', 0);
                       FieldByName('NUMERO').AsInteger := 0;
                       FieldByName('DESCRIPCION').AsString := xEventos.Get(iEvento).Text;
                       FieldByName('DETALLE').AsString := XMLTools.AttributeAsString( xEventos.Get(iEvento), 'Detalle', '');
                       Post;
                  end;



              end;
         end;
           FreeAndNil( XMLTools );

   end;

begin
     xDocumento := LoadXMLData(sXML);

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     Respuesta.Resultado.ErroresDS :=   GetErrores;

     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA);
     GetBitacora;

     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESULTADO);
     if ( xNodoActual <> nil ) then
     begin
          Respuesta.Resultado.TX_ID := xNodoActual.Attributes[K_RESPONSE_TX_ID];
          Respuesta.Resultado.TX_IDi := StrToInt(Respuesta.Resultado.TX_ID);
          Respuesta.Resultado.TX_STATUS := xNodoActual.Attributes[K_RESPONSE_TX_STATUS];
          Respuesta.Resultado.Errores :=  StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE );
     end;

     Respuesta.transaccion := tNoDefinida;
     result := Respuesta;

end;

//Conciliador timbrado Nomina
function GetRespuestaGenericaConciliador( const sXML: String ): TRespuestaStruct;
var
   Respuesta : TRespuestaStruct;
   xDocumento : IXMLDocument;
   xNodoActual : IXMLNode;
      xEventos : IXMLNodeList;


   function GetErrores : TZetaClientDataSet;
   var
      cdsErrores : TZetaClientDataSet;
      iNode : IXMLNode;
      idNodes : IXMLNodeList;
         XMLTools: DXMLTools.TdmXMLTools;
         i : integer;
   begin
        XMLTools :=  TdmXMLTools.Create(nil);
        cdsErrores := TZetaClientDataSet.Create( nil );
        with cdsErrores do
        begin
           AddIntegerField( 'ORDEN' );
           AddIntegerField( 'ERRORID' );
           AddIntegerField( 'NUMERO' );
           AddStringField('DESCRIPCION' , 255 );
           AddStringField('DETALLE', 1024 ) ;

           CreateTempDataset;
        end;


        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/ERRORES/ERROR' );

        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             with cdsErrores do
             begin
                  Append;
                  FieldByName('ORDEN').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Orden', 0);
                  FieldByName('ERRORID').AsInteger := XMLTools.AttributeAsInteger( iNode, 'ErrorID', 0);
                  FieldByName('NUMERO').AsInteger := XMLTools.AttributeAsInteger( iNode, 'NominaNumero', 0);
                  FieldByName('DESCRIPCION').AsString := XMLTools.AttributeAsString( iNode, 'Descripcion', '');
                  FieldByName('DETALLE').AsString := XMLTools.AttributeAsString( iNode, 'Detalle', '');
                  Post;
             end;

        end;

        Result := cdsErrores;
   end;



     procedure GetBitacora;
   var
      iEvento : integer;
       XMLTools: DXMLTools.TdmXMLTools;
   begin
        Respuesta.Bitacora.Cuantos := xNodoActual.Attributes[K_RESPONSE_CUANTOS];
         Respuesta.Bitacora.Eventos := TStringList.Create;
         xEventos :=    xNodoActual.ChildNodes;
         XMLTools :=  TdmXMLTools.Create(nil);
         //<EVENTO Tipo="ErrorGrave" ID="0" Detalle="Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).">Error al leer parametros</EVENTO>
         for iEvento := 0 to xEventos.Count-1 do
         begin
              if ( xEventos.Get(iEvento).NodeName = K_RESPONSE_EVENTO ) then
              begin
                  Respuesta.Bitacora.Eventos.Add(xEventos.Get(iEvento).Text );


                  with  Respuesta.Resultado.ErroresDS do
                  begin
                       Append;
                       FieldByName('ORDEN').AsInteger := XMLTools.AttributeAsInteger( xEventos.Get(iEvento), 'Orden', 0);
                       FieldByName('ERRORID').AsInteger := XMLTools.AttributeAsInteger( xEventos.Get(iEvento), 'ID', 0);
                       FieldByName('NUMERO').AsInteger := 0;
                       FieldByName('DESCRIPCION').AsString := xEventos.Get(iEvento).Text;
                       FieldByName('DETALLE').AsString := XMLTools.AttributeAsString( xEventos.Get(iEvento), 'Detalle', '');
                       Post;
                  end;



              end;
         end;
           FreeAndNil( XMLTools );

   end;

begin
     xDocumento := LoadXMLData(sXML);

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     Respuesta.Resultado.ErroresDS :=   GetErrores;

     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_BITACORA);
     GetBitacora;

     xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);
     xNodoActual := xNodoActual.ChildNodes.FindNode(K_RESPONSE_RESULTADO);
     if ( xNodoActual <> nil ) then
     begin
          Respuesta.Resultado.TX_ID := xNodoActual.Attributes[K_RESPONSE_TX_ID];
          Respuesta.Resultado.TX_IDi := StrToInt(Respuesta.Resultado.TX_ID);
          Respuesta.Resultado.TX_STATUS := xNodoActual.Attributes[K_RESPONSE_TX_STATUS];
          Respuesta.Resultado.Errores :=  StrToBoolDef( xNodoActual.Attributes[K_RESPONSE_ERRORES], TRUE );
     end;

     Respuesta.transaccion := tNoDefinida;
     result := Respuesta;

end;

//FIN



function VerificaConexionServer( var xVersionServer : Extended;  var sMensajeError, sMensajeValidacion : string )  : boolean;
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad con el Servidor de Timbrado ' + CR_LF  + 'Verifique conectividad hacia el Internet, contacte a su administrador de sistemas e Intente de nuevo.';
var
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   versionIn: TXSDecimal;
   sXMLRespuesta: String;
   xDocumento : IXMLDocument;
   xNodoActual : IXMLNode;
   lOK: Boolean;
   sVersionCorrecta: String;
   sURL: String;
begin
     Result := FALSE;
     lOK := False;
     sVersionCorrecta := VACIO;
     sURL := VACIO;
     try
        timbresSoap := nil;
        sServer := GetTimbradoServer;
        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL);
           if ( timbresSoap <> nil ) then
           begin

                versionIn :=  TXSDecimal.Create;
                versionIn.XSToNative( K_VERSION_TIMBRADO );
                sXMLRespuesta := timbresSoap.VerificarVersionXml(versionIn); //ESTE MENSAJE TMB SIRVE PARA VALIDAR Y SI NO ENCUENTRA NADA, TRUENA Y SE VA AL EXCEPTION

								if ( sXMLRespuesta <> VACIO ) then
                begin
										lOK := TRUE;
                    Result := True;
                end;
                versionIn.Free;
           end;
        end
        else
        begin
           sMensajeError :=  K_SIN_CONECTIVIDAD;
        end;
     except on Error: Exception do
              sMensajeError :=  K_SIN_CONECTIVIDAD;
     end;
end;


function VerificaVersionServer( var xVersionServer : Extended;  var sMensajeError, sMensajeValidacion : string )  : boolean;
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad con el Servidor de Timbrado ' + CR_LF  + 'Verifique conectividad hacia el Internet, contacte a su administrador de sistemas.';
var
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   versionIn: TXSDecimal;
   sXMLRespuesta: String;
   xDocumento : IXMLDocument;
   xNodoActual : IXMLNode;
   lOK: Boolean;
   sVersionCorrecta: String;
   sURL: String;
begin
     Result := FALSE;
     lOK := False;
     sVersionCorrecta := VACIO;
     sURL := VACIO;
     try
        timbresSoap := nil;
        sServer := GetTimbradoServer;
        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL);
           if ( timbresSoap <> nil ) then
           begin

                versionIn :=  TXSDecimal.Create;
                versionIn.XSToNative( K_VERSION_TIMBRADO );
                sXMLRespuesta := timbresSoap.VerificarVersionXml(versionIn);

                if ( sXMLRespuesta <> VACIO ) then
                begin
                     xDocumento := LoadXMLData(sXMLRespuesta);
                     xNodoActual := xDocumento.GetChildNodes().FindNode('MENSAJE');
                     if ( xNodoActual <> nil ) then
                     begin
                          lOK := ( xNodoActual.Attributes['OK'] = 'True' );
                          sVersionCorrecta := xNodoActual.Attributes['VersionCorrecta'];
                          sURL := xNodoActual.Attributes['Url'];
                          sMensajeValidacion := Trim(xNodoActual.Text);
                     end;
                end
                else
                begin
                     lOK := False;
                     sMensajeError := 'No es posible verificar la version correcta del sistema de timbrado.';
                end;

                if ( lOK ) then
                begin
                     Result := True;
                end
                else
                begin
                     Result := False;
                end;
                versionIn.Free;
           end;
        end
        else
        begin
           sMensajeError :=  K_SIN_CONECTIVIDAD + ' No hay servidor especificado';
        end;
     except on Error: Exception do
              sMensajeError :=  K_SIN_CONECTIVIDAD;
     end;
end;




function ConsultarSaldoServer(  sCT_CODIGO : string ; iContribuyenteID : integer; dFecIni, dFecFin : TDateTime )  : TRespuestaSaldo;
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad con el Servidor de Timbrado ' + CR_LF  + 'Verifique conectividad hacia el Internet, contacte a su administrador de sistemas.';
var
   sServer, sPeticion, sRespuesta : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;


   function GetEmptySaldoData : TZetaClientDataSet;
   begin
        Result := TZetaClientDataSet.Create( nil );
        with  Result  do
        begin
           AddStringField('STATUS_DESC' , 255 );
           AddIntegerField( 'SALDO_INI' );
           AddIntegerField( 'CARGOS' );
           AddIntegerField( 'ABONOS' );
           AddIntegerField( 'SALDO_FIN' );
           CreateTempDataset;
        end;
   end;

   function GetEmptySaldoDetalleData : TZetaClientDataSet;
   begin
        Result := TZetaClientDataSet.Create( nil );
        with  Result  do
        begin
           AddDateTimeField('FechaHora');
           AddStringField('Descrip' , 255 );
           AddStringField('RFC' , 50);
           AddStringField('Referencia' , 255 );
           AddIntegerField( 'Cargo' );
           AddIntegerField( 'Abono'  );
           AddIntegerField( 'Saldo' );
           CreateTempDataset;
        end;
   end;

   procedure SetDataFromXML( sXML : string ; respuestaSaldo : TRespuestaSaldo );
   var
      xDocumento : IXMLDocument;
      xNodoActual, iNode : IXMLNode;
      idNodes : IXMLNodeList;
      sStatusDesc : string;
      i : integer;
      XMLTools :TdmXMLTools;
   begin

        XMLTools :=  TdmXMLTools.Create(nil);
        xDocumento := LoadXMLData(sXML);
        xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);

        with respuestaSaldo.SaldoDS do
        begin
             sStatusDesc := GetAllNodesText(xNodoActual, 'SALIDA/STATUS_DESC');
             if strLleno( sStatusDesc ) then
             begin
                 Append;
                 FieldByName('STATUS_DESC').AsString := sStatusDesc;
                 FieldByName('SALDO_INI').AsString  := GetAllNodesText(xNodoActual, 'SALIDA/SALDO_INI' );
                 FieldByName('CARGOS').AsString  := GetAllNodesText(xNodoActual, 'SALIDA/CARGOS' );
                 FieldByName('ABONOS').AsString  := GetAllNodesText(xNodoActual, 'SALIDA/ABONOS' );
                 FieldByName('SALDO_FIN').AsString  := GetAllNodesText(xNodoActual, 'SALIDA/SALDO_FIN' );
                 Post;
             end;
        end;

        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/MOVS/MOV' );
        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             with respuestaSaldo.DetalleDS do
             begin
                  Append;
                  FieldByName('FechaHora').AsDateTime := XMLTools.AttributeISOAsDateTime( iNode, 'FechaHora', 0);
                  FieldByName('Descrip').AsString := XMLTools.AttributeAsString( iNode, 'Descrip', '');
                  FieldByName('RFC').AsString := XMLTools.AttributeAsString( iNode, 'RFC', '');
                  FieldByName('Referencia').AsString := XMLTools.AttributeAsString( iNode, 'Referencia', '');
                  FieldByName('Cargo').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Cargo', 0);
                  FieldByName('Abono').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Abono', 0);
                  FieldByName('Saldo').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Saldo', 0);
                  Post;
             end;
        end;


        FreeAndnil(XMLTools);
        //FreeAndNil( xDocumento );

   end;

   function GetConsultarSaldoPeticion : string;
   begin

     Result := '<?xml version="1.0" encoding="iso-8859-1" ?>';
     Result := Result + '<PROCESS> ';
     Result := Result + GetCredenciales( sCT_CODIGO,  iContribuyenteID  ) ;
     Result := Result + '<DATA>'
                   + '<FECHA_INICIAL>' + DateTime2XMLValue(dFecIni) + '</FECHA_INICIAL>'
                   + '<FECHA_FINAL>' + DateTime2XMLValue(dFecFin)   + '</FECHA_FINAL>'
                   + '<HUSO_ID>' + Format('%d', [K_USO_HORARIO_TRESS])+  '</HUSO_ID>'
                   + '</DATA>' ;
     Result := Result + '</PROCESS> ';
   end;

begin

     Result  := TRespuestaSaldo.Create;
     Result.SaldoDS :=  GetEmptySaldoData;
     Result.DetalleDS := GetEmptySaldoDetalleData; 
     Result.sMensaje := VACIO;
     Result.lHayRespuesta := FALSE;

     try
        timbresSoap := nil;
        sServer := GetTimbradoServer;
        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL);
           if ( timbresSoap <> nil ) then
           begin
                sPeticion := GetConsultarSaldoPeticion;
				
				crypt := TWSTimbradoCrypt.Create;                				 
				sRespuesta := crypt.Desencriptar( timbresSoap.CuentaConsultaSaldo( crypt.Encriptar( sPeticion ) ) );
              //sRespuesta := 					  timbresSoap.CuentaConsultaSaldo( 				    sPeticion   );
				FreeAndNil( crypt ); 
                Result.respuesta := GetRespuestaGenerica( sRespuesta );
                SetDataFromXML( sRespuesta, Result );
                Result.respuesta.transaccion := tConsultarSaldo;
                Result.lHayRespuesta := TRUE; 
           end;
        end
        else
        begin
           Result.sMensaje :=  K_SIN_CONECTIVIDAD + ' No hay servidor especificado';
        end;
     except on Error: Exception do
              Result.sMensaje :=  K_SIN_CONECTIVIDAD;
     end;
end;

procedure ObtenerCuentaTimbramexRSSeleccionado;
begin
     dmInterfase.cdsCuentasTimbramex.Locate('CT_CODIGO', dmInterfase.cdsRSocial.FieldByName('CT_CODIGO').AsString, []) ;
end;

//Conciliacion Timbrado de Nomina
function ConsultarEmpleadosAConciliarTimbradoNomina(  sPeticion:String; fParams: TZetaParams  )  : TRespuestaConciliacionTimbradoNomina;
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad con el Servidor de Timbrado ' + CR_LF  + 'Verifique conectividad hacia el Internet, contacte a su administrador de sistemas.';
var
   sServer, sRespuesta : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;


   function GetEmptySaldoData : TZetaClientDataSet;
   begin
        Result := TZetaClientDataSet.Create( nil );
        with  Result  do
        begin
           AddStringField('STATUS_DESC' , 255 );
           AddIntegerField( 'SALDO_INI' );
           AddIntegerField( 'CARGOS' );
           AddIntegerField( 'ABONOS' );
           AddIntegerField( 'SALDO_FIN' );
           CreateTempDataset;
        end;
   end;

   function GetEmptySaldoDetalleData : TZetaClientDataSet;
   begin
        Result := TZetaClientDataSet.Create( nil );
        with  Result  do
        begin
           AddDateTimeField('FechaHora');
           AddStringField('Descrip' , 255 );
           AddStringField('RFC' , 50);
           AddStringField('Referencia' , 255 );
           AddIntegerField( 'Cargo' );
           AddIntegerField( 'Abono'  );
           AddIntegerField( 'Saldo' );
           CreateTempDataset;
        end;
   end;

   procedure SetDataFromXML( sXML : string ; respuestaSaldo : TRespuestaConciliacionTimbradoNomina );
   var
      xDocumento : IXMLDocument;
      xNodoActual, iNode : IXMLNode;
      idNodes : IXMLNodeList;
      sStatusDesc : string;
      i : integer;
      XMLTools :TdmXMLTools;
   begin

        XMLTools :=  TdmXMLTools.Create(nil);
        xDocumento := LoadXMLData(sXML);
        xNodoActual := xDocumento.GetChildNodes().FindNode(K_RESPONSE_RESPUESTA);

        with respuestaSaldo.SaldoDS do
        begin
             sStatusDesc := GetAllNodesText(xNodoActual, 'SALIDA/STATUS_DESC');
             if strLleno( sStatusDesc ) then
             begin
                 Append;
                 FieldByName('STATUS_DESC').AsString := sStatusDesc;
                 FieldByName('SALDO_INI').AsString  := GetAllNodesText(xNodoActual, 'SALIDA/SALDO_INI' );
                 FieldByName('CARGOS').AsString  := GetAllNodesText(xNodoActual, 'SALIDA/CARGOS' );
                 FieldByName('ABONOS').AsString  := GetAllNodesText(xNodoActual, 'SALIDA/ABONOS' );
                 FieldByName('SALDO_FIN').AsString  := GetAllNodesText(xNodoActual, 'SALIDA/SALDO_FIN' );
                 Post;
             end;
        end;

        idNodes := HelperSelectNodes(xNodoActual, 'SALIDA/MOVS/MOV' );
        for i := 0 to idNodes.Count -1  do
        begin
             iNode := idNodes.Get(i);
             with respuestaSaldo.DetalleDS do
             begin
                  Append;
                  FieldByName('FechaHora').AsDateTime := XMLTools.AttributeISOAsDateTime( iNode, 'FechaHora', 0);
                  FieldByName('Descrip').AsString := XMLTools.AttributeAsString( iNode, 'Descrip', '');
                  FieldByName('RFC').AsString := XMLTools.AttributeAsString( iNode, 'RFC', '');
                  FieldByName('Referencia').AsString := XMLTools.AttributeAsString( iNode, 'Referencia', '');
                  FieldByName('Cargo').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Cargo', 0);
                  FieldByName('Abono').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Abono', 0);
                  FieldByName('Saldo').AsInteger := XMLTools.AttributeAsInteger( iNode, 'Saldo', 0);
                  Post;
             end;
        end;


        FreeAndnil(XMLTools);
        //FreeAndNil( xDocumento );

   end;


begin
     Result  := TRespuestaConciliacionTimbradoNomina.Create;
     Result.SaldoDS :=  GetEmptySaldoData;
     Result.DetalleDS := GetEmptySaldoDetalleData;
     Result.sMensaje := VACIO;
     Result.lHayRespuesta := FALSE;
     try
        timbresSoap := nil;
        sServer := GetTimbradoServer;
        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL);
           if ( timbresSoap <> nil ) then
           begin
                crypt := TWSTimbradoCrypt.Create;
                sRespuesta := crypt.Desencriptar( timbresSoap.ConciliarNominas( crypt.Encriptar( sPeticion ) ) );
		FreeAndNil( crypt );
                Result.respuesta := GetRespuestaGenericaConciliador( sRespuesta );
                SetDataFromXML( sRespuesta, Result );
                Result.respuestaXMl := sRespuesta;
                Result.respuesta.transaccion := tcConsultarEmpleadosConciliar;
                Result.lHayRespuesta := TRUE;
           end;
        end
        else
        begin
           Result.sMensaje :=  K_SIN_CONECTIVIDAD + ' No hay servidor especificado';
        end;
     except on Error: Exception do
              Result.sMensaje :=  K_SIN_CONECTIVIDAD;
     end;
end;



{ TWSTimbradoCrypt }
procedure TWSTimbradoCrypt.InitEncriptacion;
{$ifdef WS_ENCRIPTAR}
const
    /// Valor de la llave usada para encriptar valores
    KEY_FOR_ENCRYPTION = 'Em7P7ZtR6ADcUNfDcSQIwin5v9vwtSAK';
    /// Valor de la llave usada para desencriptar valores
    KEY_FOR_DECRYPTION = 'Kn7VOKqky1tn13uGNKCEViYXFF4+UXf9';
    /// Valor del vector usado para encriptar valores
    VECTOR_FOR_ENCRYPTION = 'XVEr5ZbYt+Q=';
    /// Valor del vector usado para desencriptar valores
    VECTOR_FOR_DECRYPTION = 'fKlTDr5joE8=';
{$endif}
begin
{$ifdef WS_ENCRIPTAR}
     ChilkatCrypt_Send := TChilkatCrypt2.Create(nil);
     ChilkatCrypt_Send.UnlockComponent('TRESSCCrypt_pnFywPZSRHjw');
     ChilkatCrypt_Send.CryptAlgorithm := '3des';
     ChilkatCrypt_Send.CipherMode := 'cbc';
     ChilkatCrypt_Send.KeyLength := 192;
     ChilkatCrypt_Send.PaddingScheme := 1;
     ChilkatCrypt_Send.EncodingMode := 'base64';
     ChilkatCrypt_Send.SetEncodedIV(VECTOR_FOR_ENCRYPTION, ChilkatCrypt_Send.EncodingMode );
     ChilkatCrypt_Send.SetEncodedKey(KEY_FOR_ENCRYPTION, ChilkatCrypt_Send.EncodingMode );
     ChilkatCrypt_Send.charset := 'UTF-8';

     ChilkatCrypt_Recv := TChilkatCrypt2.Create(nil);
     ChilkatCrypt_Recv.UnlockComponent('TRESSCCrypt_pnFywPZSRHjw');
     ChilkatCrypt_Recv.CryptAlgorithm := '3des';
     ChilkatCrypt_Recv.CipherMode := 'cbc';
     ChilkatCrypt_Recv.KeyLength := 192;
     ChilkatCrypt_Recv.PaddingScheme := 1;
     ChilkatCrypt_Recv.EncodingMode := 'base64';
     ChilkatCrypt_Recv.SetEncodedIV(VECTOR_FOR_DECRYPTION, ChilkatCrypt_Recv.EncodingMode );
     ChilkatCrypt_Recv.SetEncodedKey(KEY_FOR_DECRYPTION, ChilkatCrypt_Recv.EncodingMode );
{$endif}
end;

function TWSTimbradoCrypt.Encriptar( sValue : string ) : string;
begin
{$ifdef WS_ENCRIPTAR}
{$ifdef WSPROFILE}
     EncryptProfile('Encriptar', sValue );
{$endif}
     sValue := ANSItoUTF8( sValue ); 
     Result := ChilkatCrypt_Send.EncryptStringENC( sValue ) ;
{$ifdef WSPROFILE}
     EncryptProfile('Encriptar_Resultado', Result );
{$endif}

{$else}
       Result := sValue;
{$endif}
end;

constructor TWSTimbradoCrypt.Create;
begin
 InitEncriptacion;
end;

function  TWSTimbradoCrypt.Desencriptar( sValue : string ) : string;
var
   ansiStr : AnsiString;
begin
{$ifdef WS_ENCRIPTAR}
{$ifdef WSPROFILE}
     EncryptProfile('Desencriptar', sValue );
{$endif}
     Result := ChilkatCrypt_Recv.DecryptStringENC( sValue ) ;
   {$IFDEF DELPHIXE3_UP}
     Result := UTF8ToANSI( Result);
   {$ENDIF}
{$ifdef WSPROFILE}
     EncryptProfile('Desencriptar_Resultado', Result );
{$endif}
{$else}
       Result := sValue;
{$endif}
end;

destructor TWSTimbradoCrypt.Destroy;
begin
  EndEncriptacion;
  inherited;
end;

procedure TWSTimbradoCrypt.EndEncriptacion;
begin
{$ifdef WS_ENCRIPTAR}
    ChilkatCrypt_Send.Free;
    ChilkatCrypt_Recv.Free;
{$endif}
end;


end.

