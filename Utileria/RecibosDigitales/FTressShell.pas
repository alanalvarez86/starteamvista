unit FTressShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}
{$define MULTIPAQUETE}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseImportaShell,  Psock, NMsmtp,} ImgList, Menus, ActnList, ExtCtrls,  {ZBaseImportaShell, }
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, FileCTRL, ZBaseConsulta, ZetaDBTextBox,FProcesoTimbradoPendiente,ZetaCommonClasses,
  ZetaStateComboBox, cxGraphics, cxControls, cxLookAndFeels,  dxBarExtItems,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, cxTextEdit, cxMemo, cxRichEdit, dxRibbonSkins,
  dxSkinsdxRibbonPainter, cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar,
  cxLabel, cxStyles, cxBarEditItem, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, DB, DBClient,  cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxGridCustomView, cxGrid, cxCheckBox, cxRadioGroup, cxButtonEdit,ZetaDespConsulta,
  cxPCdxBarPopupMenu, cxPC, ZetaSmartLists, {CHILKATCRYPT2Lib_TLB,} ShellAPI,
  ZetaCXGrid, cxGroupBox, ZetaKeyLookup_DevEx, cxButtons, TressMorado2013,
  dxSkinsForm, {ZBaseUtileriaShell, }cxMaskEdit, cxDropDownEdit,
  ZetaCXStateComboBox, ZetaClientDataSet,dxStatusBar, dxRibbonStatusBar, dxSkinsdxStatusBarPainter,
  {dxSkinBlack,} dxRibbonCustomizationForm, dxBarBuiltInMenu, System.Actions,ZGridModeTools,
  ZBasicoNavBarShell, ZBaseShell,  cxtreeview,
  cxLocalization, dxSkinsdxNavBarPainter, dxNavBar,Registry, FAutoClasses,cxImage, dxGDIPlusClasses;

type
  TTressShell = class(TBasicoNavBarShell)
    gbNomina: TGroupBox;
    PeriodoNumeroLBL: TLabel;
    PeriodoNumeroCB: TStateComboBox;
    PeriodoDelLBL: TLabel;
    PeriodoFechaInicial: TZetaTextBox;
    PeriodoFechaFinal: TZetaTextBox;
    DataSourcePeriodos: TDataSource;
    DataSourceRSocial: TDataSource;
    DataSourceCuentas: TDataSource;
    _R_RegistrarContribuyente: TAction;
    _R_CancelarContribuyente: TAction;
    _P_TimbrarNomina: TAction;
    _P_CancelarTimbradoNomina: TAction;
    _P_GetTimbrados: TAction;
    PopupMenu1: TPopupMenu;
    imbrarNmina1: TMenuItem;
    CancelarTimbrado1: TMenuItem;
    omarRecibos1: TMenuItem;
    _P_EnviarTimbrados: TAction;
    _C_ConfigurarConceptos: TAction;
    DataSourceEstadoDeCuenta: TDataSource;
    DataSourceEstadoCuentaDetalle: TDataSource;
    cxImageList16Edicion: TcxImageList;
    btnYearBefore_DevEx: TdxBarButton;
    btnYearNext_DevEx: TdxBarButton;
    SistemaYear_DevEx: TdxBarEdit;

    PanelTipoNomina: TPanel;
    PeriodoTipoCB_DevEx2: TcxStateComboBox;
    PopupMenu2: TPopupMenu;
    _A_FoliosFiscales: TAction;
    _P_GetTimbradosXEmpleado: TAction;
    TabProcesos: TdxRibbonTab;
    ToolBarContribuyente: TdxBar;
    btnActualizarContribuyente: TdxBarLargeButton;
    btnCancelarContribuyente: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ToolBarTimbrado: TdxBar;
    ToolBarRecibos: TdxBar;
    ToolBarFoliosFiscales: TdxBar;
    btnTimbrarNomina: TdxBarLargeButton;
    btnCancelarTimbrado: TdxBarLargeButton;
    btnRecibosPeriodo: TdxBarLargeButton;
    btnRecibosEmpleado: TdxBarLargeButton;
    btnEnvioRecibos: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    btnAdquirirFolios: TdxBarLargeButton;
    PanelValorNomina: TPanel;
    PanelControlesNomina: TPanel;
    Label1: TLabel;
    PeriodoPrimero_DevEx: TcxButton;
    PeriodoAnterior_DevEx: TcxButton;
    PeriodoSiguiente_DevEx: TcxButton;
    PeriodoUltimo_DevEx: TcxButton;
    PeriodoNumeroCB_DevEx2: TcxStateComboBox;
    PanelRangoFechas: TPanel;
    PanelPeriodoStatus: TPanel;
    ImageList1: TImageList;
    AConsultas: TdxBar;
    dxBarButton1: TdxBarButton;
    _A_ExploradorReportes: TAction;
    _A_SQL_DevEx: TAction;
    _A_Bitacora_DevEx: TAction;
    _A_ListaProcesos: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    TabArchivo_btnExplorardorReportes: TdxBarLargeButton;
    TabArchivo_btnSQL: TdxBarLargeButton;
    TabArchivo_btnBitacora: TdxBarLargeButton;
    TabArchivo_btnListaProcesos: TdxBarLargeButton;
    btnAbrirEnlaceTimbradoWeb: TcxButton;
    TimbradoStatus_DevEx: TcxImage;
    TimbradoImagenes: TcxImageList;
    dxBarLargeButton2: TdxBarLargeButton;
    _C_ConciliarTimbrado: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure zFechaValidDate(Sender: TObject);
    procedure _P_ProcesarExecute(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure PeriodoTipoCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure PeriodoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);

    procedure _Per_PrimeroExecute(Sender: TObject);
    procedure _Per_SiguienteExecute(Sender: TObject);
    procedure _Per_AnteriorExecute(Sender: TObject);
    procedure _Per_UltimoExecute(Sender: TObject);
    procedure _Per_AnteriorUpdate(Sender: TObject);
    procedure _Per_SiguienteUpdate(Sender: TObject);

    procedure btnActualizarContribuyenteClick(Sender: TObject);
    procedure _V_ArbolOriginalExecute(Sender: TObject);
    procedure dxYearChange(Sender: TObject);
    procedure btModificarRSClick(Sender: TObject);
    procedure btAgregarCuentaClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure RefrescarBtnClick(Sender: TObject);
    procedure cxGridDBTableView2CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure btRefrescarRSClick(Sender: TObject);
    function FormHelp(Command: Word; Data: Integer;
      var CallHelp: Boolean): Boolean;
    procedure _H_ContenidoExecute(Sender: TObject);
    procedure _R_RegistrarContribuyenteExecute(Sender: TObject);
    procedure _R_CancelarContribuyenteExecute(Sender: TObject);
    procedure cxGridDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);

    procedure btREfrescaPeriodosClick(Sender: TObject);
    procedure _P_TimbrarNominaExecute(Sender: TObject);
    procedure _P_CancelarTimbradoNominaExecute(Sender: TObject);
    procedure _P_GetTimbradosExecute(Sender: TObject);
    procedure _P_GetTimbradosXEmpleadoExecute(Sender: TObject);
    procedure _P_EnviarTimbradosExecute(Sender: TObject);

    procedure SistemaYearCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure btnYearBefore_DevExClick(Sender: TObject);
    procedure btnYearNext_DevExClick(Sender: TObject);
    procedure SistemaYear_DevExPropertiesChange(Sender: TObject);
    procedure CalculaAnio ( Operacion:Integer );

    procedure n1Click(Sender: TObject);
    procedure btBuscarCuentaClick(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure _A_FoliosFiscalesExecute(Sender: TObject);
    procedure gridPeriodosDBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _A_ListaProcesosExecute(Sender: TObject);
    procedure _A_ExploradorReportesExecute(Sender: TObject);
    procedure _A_Bitacora_DevExExecute(Sender: TObject);
    procedure _A_SQL_DevExExecute(Sender: TObject);
    procedure btnAbrirEnlaceTimbradoWebClick(Sender: TObject);
    procedure _C_ConciliarTimbradoExecute(Sender: TObject);
  private
    { Private declarations }
    FTempPanelRangoFechas:String;
    lModoBatch: Boolean;
    FTimbradoPos : TBookMark;
    //Folios Repetidos
    FParamsTimbradoNomina: TZetaParams;
    function fEsPruebas : Boolean;
    procedure SetNombreLogs;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure CargaSistemaActivos;

    procedure RefrescaPeriodoInfo;

    procedure RefrescaSistemaActivos;
    procedure AbreRuta;
    procedure SetNumeroPeriodoInterfase;
    procedure CambioPeriodoActivo;
    procedure CargaPeriodoActivos;

    procedure RevisaDerechos;
    procedure HabilitaBotonesAnio;

    procedure SetPosTimbradoBookMark;
    procedure GoPosTimbradoBookMark;

    function CheckDerechosTress: Boolean;
    procedure CargaTraducciones;
    procedure AsignacionTabOrder_DevEx;
    procedure DeshabilitarNavegacion;
    procedure HabilitarNavegacion;
    procedure PosicionarPeriodoNominaGrid(iPeriodo:integer);
    procedure SetPeriodoValActivo(iPeriodo: integer);
    procedure AbreFormaInicial;
    function LeerURLRegistro(var sValorRegistro, sRuta: string) : boolean;
    procedure AddValorRegistro(sValor, sRuta, sLlave: string);
    procedure AddLlaveTimbradoWebRegistro(sValor, sLlave : string);
    procedure CrearValoresInicialesTimbradoWeb;
    procedure LimpiaIconoPeriodo;
    //Timbrados pendientes
    procedure SetEstatusPendienteEmpleadosResultadoDSLimpiar;
    procedure RefrescarPosicionGridTimbrados;
    function RegresarParametrosNomina: TZetaParams;


  protected
    { Protected declarations }
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure AbreFormaConsulta( const TipoForma: eFormaConsulta );
    Procedure DoOpenSistema;override;
    procedure ApplyMinWidth;

  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    property EsPrueba:boolean read fEspruebas;
    function FormaActivaNomina: Boolean;

    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure LLenaOpcionesGrupos;
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    {procedure ReconectaFormaActiva;  }
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
    procedure CambiaSistemaActivos;
    procedure CambiaPeriodoActivos;
    procedure RefrescaCuentas;
    procedure RefrescaRazonesSociales;
    procedure AsistenciaFechavalid(Valor:TDate);
    procedure RefrescaIMSSActivoPublico;
    procedure RefrescaPeriodo;
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
     //DevEx (by am): Metodo para cargar la configuracion de las vistas
    procedure CargaVista; override;
    procedure RefrescaPeriodoActivos(lCambioPeriodoDesdeGrid:Boolean);
    procedure CambioNumeroPeriodoDesdeGrid(iNumeroPeriodo : integer);

    procedure _P_TimbrarNominaE;
    procedure _P_CancelarTimbradoNominaE;
    procedure _P_GetTimbradosE;
    procedure _P_GetTimbradosXEmpleadoE;
    procedure _P_EnviarTimbradosE;
    procedure RefrescarShellPeriodos;
    procedure _C_ConciliarTimbradoE;

    property TempPanelRangoFechas: String  read FTempPanelRangoFechas write FTempPanelRangoFechas;
  end;

var
  TressShell: TTressShell;
  //DevEx(by Angie): Arreglo para guardar los status de la n�mina
  StatusTimbradoArray: array [0..5] of string = ('', 'Pendiente', 'Timbrada Parcial', 'Timbrada', 'Timbrado pendiente', 'Cancelaci�n pendiente');
const

     K_MANUAL = 0;
     K_ANTERIOR = 1;
     K_SIGUIENTE = 2;
     K_MAX_YEAR = 2100;
     K_MIN_YEAR = 1899;

     
     K_PARAM_PLANTA = 5;
     K_PARAM_TIPO = 6;

     K_EXTRA_WIDTH = 5;
     K_MIN_WIDTH = 1165;

implementation

uses DCliente,
     DGlobal,
     DSistema,
     DCatalogos,
     DTablas,
     DRecursos,
     DConsultas,
     DProcesos,
     DInterfase,
     DReportes,
     ZAsciiTools,
     ZGlobalTRESS,
     ZArbolTress,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     FCalendario,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     ZetaDialogo,
     WizardTimbradoThread,
     WizardCancelarTimbrado,
     FWizardRegistroContribuyente,
     FWizardCancelacionContribuyente,
     WizardRecibosTimbrado,
     WizardRecibosXEmpleado,
     WizardEnvioRecibosTimbrado,
     WizardConciliacionTimbrado,
     ZAccesosTress, 
     ZAccesosMgr,
     ZetaBuscador_DevEx,
     DBaseCliente,
     FTimbramexHelper,
     FCatPeriodosNomina_DevEx,
     FWizardFoliosfiscalespendientes,
     ZetaDialogoLinkTimbrado, RegularExpressions;

{$R *.dfm}

function TTressShell.fEspruebas:boolean;
begin
{$ifdef PROFILE}
     result := FALSE;
{$else}
     result := dmCliente.EsTRESSPruebas;
{$endif}
end;


procedure TTressShell.FormCreate(Sender: TObject);
const
     K_BATCH_SINTAXIS = '%s';
begin
     _A_OtraEmpresa.Enabled := false;
     _C_ConfigurarConceptos.Enabled := False;
     _A_SalirSistema.Enabled := false;
     _H_AcercaDe.Enabled := false;
     _H_Contenido.Enabled := False;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     HelpContext:= H00001_Pantalla_principal;

     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );

     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmInterfase := TdmInterfase.Create( Self );
      dmReportes := TdmReportes.Create( Self );

     //Folios Repetidos
     FParamsTimbradoNomina := TZetaParams.Create;
     inherited;

     FTimbradoPos := nil;

     with dmCliente do
     begin
          //SistemaYearCB.ValorEntero := YearDefault;
          SistemaYear_DevEx.Text :=  IntToStr (YearDefault);
          //ModoSuper := true;
     end;
     //PeriodoNumeroCB.ValorEntero := 1;
     PeriodoNumeroCB_DevEx2.ValorEntero := 1;
     lModoBatch := False;
     StatusBar.Visible := False;
     StatusBar_DevEx.Visible := True;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmInterfase );
     FreeAndNil( dmProcesos );
     FreeAndNil( dmRecursos );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmTablas );
     //FreeAndNil( dmCatalogos );
     FreeAndNil( dmSistema );
     FreeAndNil( dmReportes ) ;
     //Folios Repetidos
     if not ( FParamsTimbradoNomina.ItemClass = nil ) then
        FreeAndNil( FParamsTimbradoNomina );
  //   FreeAndNil( Global );
     inherited;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.FreeAll;
     {$endif}
end;

procedure TTressShell.DoOpenAll;
const
     K_TAMANO_ARBOLITOS = 2;

   procedure LanzarMensajeErrorConLink(sMensajeConLink: string);
   Const
        FORMATO_INICIO_LINKLABEL = '<a href="">';
        FORMATO_FIN_LINKLABEL = '</a>';
   var
      sLinkObtenido : string;
      sPrimeraParteMensaje, sUltimaParteMensaje, sMensajeCompletoFormato : string;
      iPosicionInicialLink, iPosicionFinalLink, LenghtHastaFinalEnlace, LenghtTodoElMensaje: integer;
   begin
         sLinkObtenido := TRegEx.Match(sMensajeConLink, '(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?').Value;

         iPosicionInicialLink := Pos(sLinkObtenido, sMensajeConLink) - 1;  //Menos uno para no tomar el primer valor del link

         iPosicionFinalLink := length(sLinkObtenido);

         sPrimeraParteMensaje := copy(sMensajeConLink,1,iPosicionInicialLink);

         LenghtHastaFinalEnlace := iPosicionInicialLink + iPosicionFinalLink + 1;//M�s uno para no tomar el �ltimo valor del link

         LenghtTodoElMensaje := length(sMensajeConLink);

         sUltimaParteMensaje := copy(sMensajeConLink, LenghtHastaFinalEnlace , LenghtTodoElMensaje );

         sMensajeCompletoFormato := sPrimeraParteMensaje + FORMATO_INICIO_LINKLABEL + sLinkObtenido + FORMATO_FIN_LINKLABEL + sUltimaParteMensaje;

         ZErrorLink_Timbrado( 'Notificaci�n del Sistema de Timbrado ' , sLinkObtenido, sMensajeCompletoFormato, 0);
   end;

   procedure UIVerificaVersionServer;
   var
     xVersionServer : Extended;
     sMensajeError, sMensajeValidacion : string ;
   begin
        sMensajeError := VACIO;
        sMensajeValidacion := VACIO;
        if not VerificaVersionServer(xVersionServer, sMensajeError, sMensajeValidacion) then
        begin
             if strLleno( sMensajeValidacion ) then
             begin
                  LanzarMensajeErrorConLink (sMensajeValidacion);
             end;

             if strLleno( sMensajeError ) then
                ZetaDialogo.ZError('Timbrado de N�mina', sMensajeError, 0);
        end;

   end;

begin
     try
        inherited DoOpenAll;

        if ( not CheckDerechosTress ) then
           db.DataBaseError( 'No tiene derechos para utilizar este ejecutable en esta Empresa' );

        Global.Conectar;
        with dmInterfase do
        begin
             AplicarPatchTimbradoComparte;
             if Not AplicarPatchTimbradoDatos then
                ZetaDialogo.ZWarning('Timbrado de N�mina','No se actualiz� correctamente la base de datos, favor de ingresar nuevamente a la aplicaci�n',0,mbOk);
        end;
        with dmCliente do
        begin
             InitActivosSistema;
             InitArrayTPeriodo;//acl;
             InitActivosAsistencia;
             InitActivosEmpleado;
             InitActivosPeriodo;

        end;
        CargaPeriodoActivos;
        CargaSistemaActivos;
        RefrescaCuentas;
        RefrescaRazonesSociales;


        SetArbolitosLength( K_TAMANO_ARBOLITOS );
        CreaNavBar;

        RevisaDerechos;


        with PeriodoTipoCB_DevEx2 do
        begin
             ListaFija:=lfTipoPeriodoConfidencial;
             Llave:= IntToStr( Ord( dmCliente.GetDatosPeriodoActivo.Tipo ) );

             // Si viene vacio el valor le asigna 0
             if ( StrVacio ( Llave ) ) then
             begin
                  PeriodoTipoCB_DevEx2.Indice:= 0;
                  dmCliente.PeriodoTipo := eTipoPeriodo( StrToInt( PeriodoTipoCB_DevEx2.Llave ) );
                  CambioPeriodoActivo;
             end;
        end;

        {AV: 2010-11-16 Asigna El Global Usar Validacion Status Activo de Tablas y Cat�logos al ZetaClienteDataSet }
        ZetaClientDataSet.GlobalSoloKeysActivos := Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS );
        // 2013-Feb-05: Mejora en Confidencialidad
        ZetaClientDataSet.GlobalListaConfidencialidad := dmCliente.Confidencialidad;
        UIVerificaVersionServer;
        llenaopcionesGrupos;
        AbreFormaInicial;

     except
        on Error : Exception do
        begin
             DoCloseAll;
        end;
     end;
end;

function TTressShell.LeerURLRegistro(var sValorRegistro, sRuta: string): boolean;
const
     RutaURL = 'Software\Grupo Tress\TressWin\Client\';
     RutaURLWOW6432 = 'Software\Wow6432Node\Grupo Tress\TressWin\Client\';
var
     oRegistro : TRegistry;
     sRutaRegistro: string;
     I: Integer;
begin
      Result := False;
      sValorRegistro := VACIO;
      try
              for I := 1 to 2 do
              begin
                   sRutaRegistro := VACIO;
                   case I of
                       1: sRutaRegistro := RutaURL;
                       2: sRutaRegistro := RutaURLWOW6432;
                   end;
                  oRegistro:= TRegistry.Create(KEY_READ);
                  oRegistro.RootKey:= HKEY_LOCAL_MACHINE;
                  if (oRegistro.KeyExists(sRutaRegistro)) then
                  begin
                        oRegistro.OpenKeyReadOnly(RutaURL);
                        if (oRegistro.ValueExists(sRuta)) then
                        begin
                             sValorRegistro := oRegistro.ReadString(sRuta);
                             if sValorRegistro <> VACIO then
                             begin
                                  Result := True;
                             end;
                        end;
                  end;
                  oRegistro.CloseKey();
                  oRegistro.Free;

                  if Result then
                  begin
                       break;
                  end;
              end;
      Except
      end;
end;


procedure TTressShell.AddLlaveTimbradoWebRegistro(sValor, sLlave : string);
const
     RutaURL = 'Software\Grupo Tress\TressWin\Client\';
     RutaURLWOW6432 = 'Software\Wow6432Node\Grupo Tress\TressWin\Client\';

var
     oRegistro : TRegistry;
     sRutaRegistro: string;
     I: Integer;

begin
      for I := 1 to 2 do
      begin
           sRutaRegistro := VACIO;
           case I of
               1: sRutaRegistro := RutaURL;
               2: sRutaRegistro := RutaURLWOW6432;
           end;
      AddValorRegistro(sValor, sRutaRegistro, sLlave);
      end;
end;

procedure TTressShell.AddValorRegistro(sValor, sRuta, sLlave: string);
var
 oRegistro : TRegistry;
begin
    try
        oRegistro:= TRegistry.Create(KEY_READ);
        oRegistro.RootKey:= HKEY_LOCAL_MACHINE;
        if (not oRegistro.KeyExists(sRuta)) then
        begin
              oRegistro.Access:= KEY_WRITE or KEY_WOW64_64KEY;
              oRegistro.OpenKey(sRuta,True);
              if (oRegistro.ValueExists(sLlave)) then
              begin
                   oRegistro.WriteString(sLlave, sValor);
              end
              else
              begin

                   oRegistro.WriteString(sLlave, sValor);
              end;
        end
        else
        begin
              oRegistro.Access:= KEY_WRITE or KEY_WOW64_64KEY;
              oRegistro.OpenKey(sRuta,True);
              if (oRegistro.ValueExists(sLlave)) then
              begin
                   oRegistro.WriteString(sLlave, sValor);
              end
              else
              begin
                   oRegistro.WriteString(sLlave, sValor);
              end;
        end;
        oRegistro.CloseKey();
        oRegistro.Free;
    EXCEPT

    end;
end;

procedure TTressShell.CrearValoresInicialesTimbradoWeb;
const
     URLTimbrado = 'https://login.recibodigital.mx';
     LabelBotonTimbrado = 'Ir a Administrador de Timbrado';
var
    sValorRegistro, sTimbradoSistemaLabel, sLlave, sValor : string;
begin
     try
            sValorRegistro := VACIO;
            sLlave := VACIO;
            sLlave := 'TimbradoSistemaURL';
            if not LeerURLRegistro(sValorRegistro, sLlave) then
            begin
                 AddLlaveTimbradoWebRegistro(URLTimbrado, sLlave); //GUARDA VALOR URL
            end;

            sValorRegistro := VACIO;
            sLlave := VACIO;
            sLlave := 'TimbradoSistemaLabel';
            if LeerURLRegistro(sValorRegistro, sLlave) then
            begin
                 btnAbrirEnlaceTimbradoWeb.Caption := '  '+sValorRegistro;
            end
            else
            begin
                 AddLlaveTimbradoWebRegistro(LabelBotonTimbrado, sLlave); //GUARDA VALOR LABEL
                 btnAbrirEnlaceTimbradoWeb.Caption := '  '+LabelBotonTimbrado;
                 btnAbrirEnlaceTimbradoWeb.Hint := 'Ir a '+URLTimbrado;
            end;
     Except
     end;
end;

Procedure TTressShell.LLenaOpcionesGrupos;
var
I:integer;
begin
  if(dmCliente.EmpresaAbierta) then
  begin
        for I:=0 to DevEx_NavBar.Groups.Count-1 do
        begin
              if (DevEx_NavBar.Groups[I].Caption = 'Timbrado')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=1;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=1;
              end;
              if (I = 0) then
              begin
                    if(DevEx_NavBar.Groups.Count = 1) then
                    begin
                          // @DChavez : Cuando no tiene derecho se asigna los iconos que corresponden al navbar por el index fijo en la lista
                          if( DevEx_NavBar.Groups[0].Caption = 'No tiene derechos' )   then
                          begin
                               DevEx_NavBar.Groups[0].LargeImageIndex := 1;
                               DevEx_NavBar.Groups[0].SmallImageIndex := 1;
                          end
                          else
                          begin
                               DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                               DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                          end;
                    end
                    else
                    begin
                          if (DevEx_NavBar.Groups[1].Caption = 'Timbrado')  then
                          begin
                                DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                                DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                          end;
                    end;
              end;
              DevEx_NavBar.Groups[I].UseSmallImages:=false;
        end;
  end
  else
  begin
        if(DevEx_NavBar.Groups.Count >0) then
        begin
              DevEx_NavBar.Groups[0].UseSmallImages:=false;
              DevEx_NavBar.Groups[0].LargeImageIndex:=9;
              DevEx_NavBar.Groups[0].SmallImageIndex:=9;
        end;
  end;
end;

procedure TTressShell.DeshabilitarNavegacion;
begin
          if ( PeriodoNumeroCB_DevEx2.ValorEntero = 0 ) then
          begin
              PeriodoPrimero_DevEx.Enabled := False;
              PeriodoAnterior_DevEx.Enabled := False;
              PeriodoSiguiente_DevEx.Enabled := False;
              PeriodoUltimo_DevEx.Enabled := False;
              PanelRangoFechas.Visible := false;
          end;

end;

procedure TTressShell.HabilitarNavegacion;
begin
          if ( PeriodoNumeroCB_DevEx2.ValorEntero > 0 ) then
          begin
              PeriodoPrimero_DevEx.Enabled := true;
              PeriodoAnterior_DevEx.Enabled := true;
              PeriodoSiguiente_DevEx.Enabled := true;
              PeriodoUltimo_DevEx.Enabled := true;
              PanelRangoFechas.Visible := true;
          end;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmConsultas );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
          ZetaClientTools.CierraDatasets( dmReportes );
     end;
     inherited DoCloseAll;
end;


procedure TTressShell._Per_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoPrimeroEspecialTimbra then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoAnteriorEspecialTimbra then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TTressShell._Per_AnteriorUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoDownEnabled;
end;

procedure TTressShell._Per_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoSiguienteEspecialTimbra then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_SiguienteUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoUpEnabled;
end;

procedure TTressShell._Per_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoUltimoEspecialTimbra then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     //SetNumeroPeriodoInterfase; //@(am): Se comenta porque se detecto que es codigo legacy que interfiere con la implementacion de Tipos de Nomina 
     with dmCliente do
     begin
          SistemaYear_DevEx.Text := IntToStr ( YearDefault );

     end;
     RefrescaSistemaActivos;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre, sArchBit, sArchErr, sTipo: String;
begin
     with dmCliente do
     begin
          sNombre := FechaToStr( FechaDefault ) + FormatDateTime( 'hhmmss', Time ) + '_' + sTipo + '.LOG';
          sArchBit := ZetaMessages.SetFileNameDefaultPath( 'BITACORA-' + sNombre );
          sArchErr := ZetaMessages.SetFileNameDefaultPath( 'ERRORES-' + sNombre );
     end;

end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.SetEstatusPendienteEmpleadosResultadoDSLimpiar;
var
   oTimbradoPendiente: TProcesoTimbradoPendiente;
begin
     if not ( FParamsTimbradoNomina.FindParam('HUBO_RESPUESTA') = nil ) and not ( FParamsTimbradoNomina.FindParam('TIMBRADO') = nil ) then
     begin
          oTimbradoPendiente := TProcesoTimbradoPendiente.Create;
          oTimbradoPendiente.ParamsTimbradoNomina := FParamsTimbradoNomina;
          oTimbradoPendiente.SetEstatusPendienteEmpleadosTimbradosLimpiar;
          oTimbradoPendiente.Destroy;
     end;

     if not ( FParamsTimbradoNomina.FindParam('HUBO_RESPUESTA') = nil ) and not ( FParamsTimbradoNomina.FindParam('CANCELANDO_TIMBRADO') = nil ) then
     begin
          oTimbradoPendiente := TProcesoTimbradoPendiente.Create;
          oTimbradoPendiente.ParamsTimbradoNomina := FParamsTimbradoNomina;
          oTimbradoPendiente.SetEstatusPendienteEmpleadosCanceladosLimpiar;
          oTimbradoPendiente.Destroy;
     end;
     RefrescarPosicionGridTimbrados;
end;

procedure TTressShell.RefrescarPosicionGridTimbrados;
begin
     dmInterfase.cdsPeriodosAfectadosTotal.Refrescar;
     GoPosTimbradoBookMark;
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          //dmInterfase.HandleProcessEnd( WParam, LParam );
          dmProcesos.HandleProcessEnd( WParam, LParam );
          //Llamar la actualizacion de Limpieza de Status pendiente
          SetEstatusPendienteEmpleadosResultadoDSLimpiar;
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }
procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

//{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     { Cambios a Valores Activos }
     if Dentro( enPeriodo , Entidades ) then
     begin
          RefrescaPeriodoInfo;
          CargaPeriodoActivos;
     end;
end;

procedure TTressShell.RefrescaPeriodoInfo;
begin
     dmCliente.RefrescaPeriodo;
end;

procedure TTressShell.RefrescaPeriodo;
begin
     RefrescaPeriodoInfo;
     CambiaPeriodoActivos;
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

procedure TTressShell.zFechaValidDate(Sender: TObject);
begin
     inherited;
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     dmInterfase.Anio := dmCliente.YearDefault;
     SetNombreLogs;
     dmInterfase.cdsPeriodosAfectadosTotal.Refrescar;
     DataSourcePeriodos.DataSet :=   dmInterfase.cdsPeriodosAfectadosTotal;
end;

procedure TTressShell.AbreFormaConsulta(const TipoForma: eFormaConsulta);
begin
     inherited;
//
end;

procedure TTressShell.AbreFormaInicial;
begin
    if EmpresaAbierta then
    begin
         if (CheckDerecho(GetPropConsulta(efcTimDatosPeriodos).IndexDerechos,K_DERECHO_CONSULTA )) then
         begin
              AbreFormaConsulta( efcTimDatosPeriodos );
         end;
    end;
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     // No Implementar
end;

procedure TTressShell._P_ProcesarExecute(Sender: TObject);
begin
     //No Implementar
     dmInterfase.Manual := TRUE;
     inherited;
//
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     CargaPeriodoActivos;
end;





procedure TTressShell.CambiaSistemaActivos;
begin
     // No Implementar
end;

procedure TTressShell.ArchivoSeekClick(Sender: TObject);
begin
     AbreRuta;
end;

procedure TTressShell.AbreRuta;
begin
end;

procedure TTressShell.SetNumeroPeriodoInterfase;
begin
     with dmCliente do
     begin
          if ( PeriodoNumeroCB_DevEx2.ValorEntero <> 0 ) then
          begin
               if SetPeriodoNumeroEspecialTimbra( PeriodoNumeroCB_DevEx2.ValorEntero ) then
                  RefrescaPeriodoActivos(false);
          end
          else
          begin
               PeriodoFechaInicial.Caption := VACIO;
               PeriodoFechaFinal.Caption := VACIO;

          end;
     end;
end;

procedure TTressShell.PeriodoTipoCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          Application.ProcessMessages;
          try
             PeriodoTipo := eTipoPeriodo( StrToIntDef( PeriodoTipoCB_DevEx2.Llave, 0 ) );
             CambioPeriodoActivo;
             dmInterfase.cdsPeriodosAfectadosTotal.Refrescar;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;


procedure TTressShell.PosicionarPeriodoNominaGrid(iPeriodo: integer);
var
   sPeriodo : string;
   sFormaNombre : string;
begin
      try
          if FormaActiva.Caption = 'Periodos de N�mina' then
          begin
                sPeriodo := inttostr(iPeriodo);
                if iPeriodo >0 then
                begin
                     if  (dmInterfase.cdsPeriodosAfectadosTotal <> nil) and not( dmInterfase.cdsPeriodosAfectadosTotal.state in [dsInactive] ) then
                      begin
                            sPeriodo := inttostr(iPeriodo);
                            with dmInterfase.cdsPeriodosAfectadosTotal do
                            begin
                                 Locate( 'PE_NUMERO', sPeriodo, [ loCaseInsensitive, loPartialKey ] );
                            end;
                      end;
                 end;
          end;
      Except

      end;
end;

procedure TTressShell.SetPeriodoValActivo(iPeriodo: integer);
var
   sPeriodo : string;
   sFormaNombre : string;
begin
      try
          if FormaActiva.Caption = 'Periodos de N�mina' then
          begin
                sPeriodo := inttostr(iPeriodo);
                if iPeriodo >0 then
                begin
                     if  (dmInterfase.cdsPeriodosAfectadosTotal <> nil) and not( dmInterfase.cdsPeriodosAfectadosTotal.state in [dsInactive] ) then
                      begin
                            sPeriodo := inttostr(iPeriodo);
                               PeriodoNumeroCB_DevEx2.ValorEntero := 1;
                      end;
                 end;
          end;
      Except

      end;
end;

procedure TTressShell.PeriodoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        //lOk := dmCliente.SetPeriodoNumero( PeriodoNumeroCB.ValorEntero );
        lOk := dmCliente.SetPeriodoNumeroEspecialTimbra( PeriodoNumeroCB_DevEx2.ValorEntero );
        if lOk then
           RefrescaPeriodoActivos(false)
        else
            ZetaDialogo.zInformation( 'Error', '!No existe el per�odo de n�mina!', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.CargaPeriodoActivos;
begin
     with dmCliente do
     begin
          PeriodoTipoCB_DevEx2.Llave := IntToStr( Ord( PeriodoTipo ) );
          PeriodoNumeroCB_DevEx2.ValorEntero := PeriodoNumero;
     end;
     RefrescaPeriodoActivos(false);
end;


procedure TTressShell.CambioNumeroPeriodoDesdeGrid(iNumeroPeriodo: integer);
begin
     try
        PeriodoNumeroCB_DevEx2.ValorEntero := iNumeroPeriodo;
     Except

     end;
end;

procedure TTressShell.CambioPeriodoActivo;
begin
     PeriodoNumeroCB_DevEx2.ValorEntero := dmCliente.PeriodoNumero;
     RefrescaPeriodoActivos(false);
end;

procedure TTressShell.LimpiaIconoPeriodo;
var
   TBitmap: TcxAlphaBitmap;
   TPngImage: TdxPngImage;
begin
    //DevEx(by Angie): Se asigna el �cono gris como default para timbrado.
     TBitmap := TcxAlphaBitmap.CreateSize(24, 24);
     TPngImage := nil;
     TimbradoImagenes.GetBitmap( 0, TBitmap );

     with  PanelPeriodoStatus do
     begin
          Font.Color := K_STATUS_COLOR_SINDEFINIR;
          Caption := K_ESPACIO+'Sin Definir'+K_ESPACIO;
          Hint := 'Per�odos ' + PeriodoTipoCB_DevEx2.Text + ' No Han Sido Definidos';
          //DevEx(by Angie): En caso no tener definidos periodos se asigna icono gris y se borra el hint para timbrado
          TPngImage := TdxPNGImage.CreateFromBitmap( TBitmap );
          TimbradoStatus_DevEx.Picture.Graphic := TPngImage;
          TimbradoStatus_DevEx.Hint := VACIO;
     end;
end;

procedure TTressShell.RefrescaPeriodoActivos(lCambioPeriodoDesdeGrid:Boolean);
const
     K_ESPACIO = ' ';
     K_SIN_DEFINIR = 0;
     K_STATUS_NUEVA = 0;
     K_STATUS_PRENOMINA_PARCIAL = 1;
     K_STATUS_SIN_CALCULAR = 2;
     K_STATUS_CALCULADA_PARCIAL = 3;
     K_STATUS_CALCULADA_TOTAL = 4;
     K_STATUS_AFECTADA_PARCIAL =5;
     K_STATUS_AFECTADA_TOTAL = 6;
var
   FechaDescrip, EstatusTimbrado: String;
   i, iTimbradas, iPendientes, iTotalEmpleados, iPendientesTimbradas, iCancelacionPendientesTimbradas: Integer;
   Parametros, RecibeParametros: TZetaParams;
   TBitmap: TcxAlphaBitmap;
   TPngImage: TdxPngImage;
procedure LimpiaTextos;
begin
     PanelPeriodoStatus.Caption := VACIO;
     PanelRangoFechas.Caption := VACIO;
end;
begin
     //RefrescaCuentas;

     with dmCliente do
     begin
          with GetDatosPeriodoActivo do
          begin
               PeriodoFechaInicial.Caption := FechaCorta( InicioAsis );
               PeriodoFechaFinal.Caption := FechaCorta( FinAsis );
          end;
     end;


     with dmCliente do
     begin
          with GetDatosPeriodoActivo do
          begin
               //DevEx (by am): Se define el Rango de Fechas de la nomina agregando el texto a la misma etiqueta donde se encuentra el status.

               LimpiaTextos;
               FechaDescrip := FechaCorta( Inicio );
               if  (( Length( FechaDescrip ) > 0 ) AND ( FechaDescrip <> K_ESPACIO )) then
               begin
                      TempPanelRangoFechas := K_ESPACIO + FechaDescrip + K_ESPACIO +'al'+ K_ESPACIO + FechaCorta( Fin )+K_ESPACIO;
                      //DevEx(by am): Solo se muestra el panel si el width es mayor a 1165, para dar prioridad a que los valores de la nomina se vean de izq. a derecha
                      if (Self.Width > K_MIN_WIDTH) then
                         PanelRangoFechas.Caption := TempPanelRangoFechas;
               end;

               {***DevEx (by am): Se define el Status de la nomina.***}
               if ( Numero = K_SIN_DEFINIR ) then
               begin
                    LimpiaIconoPeriodo;
               end
               else
               begin
                    with PanelPeriodoStatus,PanelPeriodoStatus.Font do
                    begin
                         if ( Ord( Status ) in [K_STATUS_NUEVA, K_STATUS_PRENOMINA_PARCIAL, K_STATUS_SIN_CALCULAR, K_STATUS_CALCULADA_PARCIAL,K_STATUS_CALCULADA_TOTAL]) then
                            Color := K_STATUS_COLOR_NUEVA
                         else if ( Ord( Status ) in [K_STATUS_AFECTADA_PARCIAL,K_STATUS_AFECTADA_TOTAL]) then
                              begin
                                   Color := K_STATUS_COLOR_AFECTADA;
                              end
                         else
                             Color := clWindowText;

                         if ( Status < spAfectadaParcial ) then
                         begin
                                PeriodoNumeroCB_DevEx2.ValorEntero := 0;//PeriodoNumero;
                                LimpiaIconoPeriodo;
                                DeshabilitarNavegacion;
                         end
                         else
                         begin
                              HabilitarNavegacion;

                              if not lCambioPeriodoDesdeGrid then
                              begin
                                   PosicionarPeriodoNominaGrid(PeriodoNumeroCB_DevEx2.ValorEntero);
                              end;

                               //DevEx(by Angie): Agregar imagen de timbrado segun su estatus
                               EstatusTimbrado := ZetaCommonLists.ObtieneElemento( lfStatusTimbrado, ord( StatusTimbrado ) );
                               Caption :=K_ESPACIO+ZetaCommonTools.GetDescripcionStatusPeriodo(Status, StatusTimbrado ) +K_ESPACIO;

                               TBitmap := TcxAlphaBitmap.CreateSize(24, 24);
                               TPngImage := nil;
                               TimbradoImagenes.GetBitmap( 0, TBitmap );


                               for i := Low( StatusTimbradoArray ) to High( StatusTimbradoArray ) do
                               begin
                                    if EstatusTimbrado = StatusTimbradoArray[ i ] then
                                    begin
                                         try
                                            TBitmap.Clear;
                                            if( ( ord( StatusTimbrado ) = 0 ) and ( ord( Status ) <= 4 ) ) then
                                            begin
                                                 TimbradoImagenes.GetBitmap( 0, TBitmap );
                                            end
                                            else
                                                TimbradoImagenes.GetBitmap( i, TBitmap );

                                            TPngImage := TdxPngImage.CreateFromBitmap( TBitmap );
                                            TimbradoStatus_DevEx.Picture.Graphic := TpngImage;
                                            try
                                               Parametros := TZetaParams.Create;
                                               with Parametros do
                                               begin
                                                    AddInteger('Anio', dmCliente.GetDatosPeriodoActivo.Year );
                                                    AddInteger('Tipo', dmCliente.GetDatosPeriodoActivo.Tipo );
                                                    AddInteger('Numero', dmCliente.GetDatosPeriodoActivo.Numero );
                                                    AddString('Nivel0', dmCliente.Confidencialidad );
                                               end;

                                               RecibeParametros := TZetaParams.Create;

                                               RecibeParametros.VarValues := dmProcesos.GetStatusTimbrado( Parametros );
                                               iTimbradas := RecibeParametros.ParamValues['Timbradas'];
                                               iPendientes := RecibeParametros.ParamValues['Pendientes'];
                                               //Timbrado Pendiente
                                               iPendientesTimbradas := RecibeParametros.ParamValues['TimbradoPendientes'];
                                               iCancelacionPendientesTimbradas := RecibeParametros.ParamValues['CancelacionPendiente'];

                                               iTotalEmpleados := iTimbradas + iPendientes;
                                            finally
                                                   Parametros.Free;
                                                   RecibeParametros.Free;
                                            end;

                                            //DevEx (by Angie): Asignando el mensaje al Hint dependendiendo de los estados
                                               if( ( ord( StatusTimbrado ) = 0 ) and ( ( ord( Status ) = 5 ) or ( ord( Status ) = 6 ) ) ) then
                                                   TimbradoStatus_DevEx.Hint := 'Sin timbrar'
                                               else
                                                   if( ( ord( StatusTimbrado ) = 1 ) and ( ( ord( Status ) = 5 ) or ( ord( Status ) = 6 ) ) ) then
                                                       TimbradoStatus_DevEx.Hint := 'Timbrada Parcial ' + IntToStr(iTimbradas) + '/' + IntToStr(iTotalEmpleados)
                                                   else
                                                       if( ( ord( StatusTimbrado ) = 2 ) and ( ( ord( Status ) = 6 ) ) ) then
                                                           TimbradoStatus_DevEx.Hint := 'Timbrada '
                                                       else
                                                           if( ( ord( StatusTimbrado ) = 3 ) and ( ( ord( Status ) = 5 ) or ( ord( Status ) = 6 ) ) ) then
                                                               TimbradoStatus_DevEx.Hint := 'Timbrado pendiente ( existen ' + IntToStr(iPendientesTimbradas) + ' recibos que ocupan intentar timbrarse de nuevo )'
                                                           else
                                                               if( ( ord( StatusTimbrado ) = 4 ) and ( ( ord( Status ) = 5 ) or ( ord( Status ) = 6 ) ) ) then
                                                                   TimbradoStatus_DevEx.Hint := 'Cancelaci�n pendiente ( existen ' + IntToStr(iCancelacionPendientesTimbradas) + ' recibos que ocupan intentar cancelarse de nuevo )'
                                                               else
                                                                   TimbradoStatus_DevEx.Hint := VACIO;
                                         finally
                                                TPngImage.Free;
                                                TBitmap.Free;
                                         end;
                                    end;
                               end;
                         end;

                         //DevEx(by am): Si el panel del rango de fechas no esta visible se le pone esta informacion al hint del status
                         if (Self.Width > K_MIN_WIDTH) then
                            Hint := VACIO
                         else
                             Hint := TempPanelRangoFechas;
                    end;
               end;

               GetFechaLimiteShell; //cargar fecha asistencia StatusBar_DevEx

               //Proceso para colocar un Periodo Valido en caso que exista, cuando se coloca por default SinDefinir al Inicio de Shell, de esta manera descarta el sin definir de TRESS.EXE Valores activos
               if (Status <= spCalculadaTotal) then
               begin
                     if GetPeriodoSiguienteEspecialTimbra then
                     begin
                          CambioPeriodoActivo;
                     end
                     else
                     begin
                          if GetPeriodoAnteriorEspecialTimbra then
                          begin
                               CambioPeriodoActivo;
                          end;
                     end;
               end;
          end;
     end;
end;

procedure TTressShell.FormResize(Sender: TObject);
begin
  inherited;
  if (Self.Width <= K_MIN_WIDTH) then
  begin
       if not ZetaCommonTools.StrLleno( TempPanelRangoFechas ) then  //Por si reduce mas de una vez
       begin
          TempPanelRangoFechas:= PanelRangoFechas.Caption;
          PanelPeriodoStatus.Hint := TempPanelRangoFechas;
       end;
       PanelRangoFechas.Caption:='';
       PanelRangoFechas.Width := 1;
  end
  else
  begin
      if ZetaCommonTools.StrLleno( TempPanelRangoFechas ) then  //Por si aumenta mas de una vez
      begin
           PanelRangoFechas.Caption:= TempPanelRangoFechas;
           TempPanelRangoFechas := '';
           PanelRangoFechas.Width := Self.Canvas.TextWidth(PanelRangoFechas.Caption)+K_EXTRA_WIDTH;
           PanelPeriodoStatus.Hint := VACIO;
      end;
  end;
end;

procedure TTressShell.btnActualizarContribuyenteClick(Sender: TObject);
begin
  inherited;
  SetNombreLogs;
end;

procedure TTressShell.btnAbrirEnlaceTimbradoWebClick(Sender: TObject);
var
    sTimbradoSistemaURL, sTimbradoSistemaLabel, sRuta : string;
begin
  inherited;
      try
          sTimbradoSistemaURL := VACIO;
          sRuta := 'TimbradoSistemaURL';
          if LeerURLRegistro(sTimbradoSistemaURL, sRuta) then
          begin
               //abre en el navegador ruta tomada del registro
               ShellExecute(Application.Handle, 'open', PChar(sTimbradoSistemaURL), nil, nil, SW_NORMAL);
          end
          else
          begin
               //abre en el navegador url predeterminada
               ShellExecute(Application.Handle, 'open', PChar('https://login.recibodigital.mx'), nil, nil, SW_NORMAL);
          end;
      Except
      end;
end;


procedure TTressShell.dxYearChange(Sender: TObject);
var
   oCursor: TCursor;
begin
  inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        CambioPeriodoActivo;
        RefrescaSistemaActivos;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.btModificarRSClick(Sender: TObject);
var
WizardRegistroContribuyente :  TWizardRegistroContribuyente;
begin
  inherited;
     WizardRegistroContribuyente :=  TWizardRegistroContribuyente.Create( Self );
     WizardRegistroContribuyente.ShowModal;
     WizardRegistroContribuyente.Free;
end;

procedure TTressShell.RefrescaCuentas;
begin
     dmInterfase.cdsCuentasTimbramex.Refrescar;
     DataSourceCuentas.DataSet :=  dmInterfase.cdsCuentasTimbramex;
end;

procedure TTressShell.RefrescaRazonesSociales;
begin
     dmInterfase.cdsRSocial.Refrescar;
     DataSourceRSocial.DataSet :=  dmInterfase.cdsRSocial;
     DataSourceEstadoDeCuenta.DataSet := dmInterfase.cdsSaldoTimbrado;
     DataSourceEstadoCuentaDetalle.DataSet := dmInterfase.cdsSaldoTimbradoDetalle;
end;

procedure TTressShell.btAgregarCuentaClick(Sender: TObject);
begin
  inherited;
  dmInterfase.cdsCuentasTimbramex.Agregar;
end;

procedure TTressShell.BorrarClick(Sender: TObject);
begin
  inherited;
  dmInterfase.cdsCuentasTimbramex.Borrar;
end;

procedure TTressShell.ModificarClick(Sender: TObject);
begin
  inherited;
  dmInterfase.cdsCuentasTimbramex.Modificar;
end;

procedure TTressShell.RefrescarBtnClick(Sender: TObject);
begin
  inherited;
   dmInterfase.cdsCuentasTimbramex.Refrescar;
end;



procedure TTressShell.cxGridDBTableView2CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  dmInterfase.cdsCuentasTimbramex.Modificar;
end;

procedure TTressShell.btRefrescarRSClick(Sender: TObject);
begin
  inherited;
  dmInterfase.cdsRSocial.Refrescar; 
  end;

function TTressShell.FormHelp(Command: Word; Data: Integer;
  var CallHelp: Boolean): Boolean;
begin
  CallHelp := TRUE;
  ShellExecute(Handle, 'open', PChar(ExtractFilePath(Application.ExeName) +Application.HelpFile), nil, nil, SW_SHOW);
end;

procedure TTressShell._H_ContenidoExecute(Sender: TObject);
begin
  Application.HelpFile := 'TimbradoNomina.chm';
  ShellExecute(Handle, 'open', PChar(ExtractFilePath(Application.ExeName) +Application.HelpFile), nil, nil, SW_SHOW);
end;

procedure TTressShell._R_RegistrarContribuyenteExecute(Sender: TObject);
var
  WizardRegistroContribuyente :  TWizardRegistroContribuyente;
  sFormaActiva : string;
  xVersionServer : Extended;
  sMensajeError, sMensajeValidacion : string ;
begin
  inherited;
 if ( dmInterfase.cdsRsocial.IsEmpty ) then
 begin
      ZWarning( Self.Caption, 'No hay Razones Sociales a Registrar', 0, mbOK );
 end
 else
 begin
        if not VerificaConexionServer(xVersionServer, sMensajeError, sMensajeValidacion) then
        begin
             if strLleno( sMensajeError ) then
             begin
                ZetaDialogo.ZError('Timbrado de N�mina', sMensajeError, 0);
             end;
        end
        else
        begin
            WizardRegistroContribuyente :=  TWizardRegistroContribuyente.Create( Self );
            WizardRegistroContribuyente.ShowModal;
            WizardRegistroContribuyente.Free;
        end;
 end; 
end;

procedure TTressShell._V_ArbolOriginalExecute(Sender: TObject);
begin
     inherited;
     if ZConfirm( 'Edici�n Del Arbol De Formas', 'Este Cambio Es S�lo Temporal' + CR_LF + '� Desea Continuar ? ', 0, mbYes ) then
     begin
          {***DevEx(@am): El metodo ZArbolTress.MuestraArbolOriginal, renombra todo el arbol de sistema TRESS en vista la vista clasica,
                          lo cual no es necesario, pues los nombres del arbol del sistema no pueden ser cambiados, los unicos que puden cambiarse
                          son los del arbol del usuario, sin embargo como es Legacy se dejo tal y como esta ese codigo.
                          Para la nueva imagens olo se cambiaran los nombres para el primer grupo, que es donde estaria ubicado el arbol del usuario,
                          en caso de contar con uno***}
          if DevEx_NavBar.Groups.Count>0 then //Renoombrara el arbol del Grupo Cero, el arbol del usuario seimpre sea el cero.
             ZArbolTress.MuestraArbolOriginal( DevEx_NavBar.Groups[0].Control.Controls[0] as TcxTreeView );
     end;
end;

procedure TTressShell._R_CancelarContribuyenteExecute(Sender: TObject);
var
WizardCancelacionContribuyente :  TWizardCancelacionContribuyente;
lContinuar : Boolean;
begin
  inherited;

  lContinuar := TRUE;

   if ( dmInterfase.cdsRsocial.IsEmpty ) then
 begin
      ZWarning( Self.Caption, 'No hay Razones Sociales', 0, mbOK );
 end
 else
         WizardCancelacionContribuyente :=  TWizardCancelacionContribuyente.Create( Self );
         WizardCancelacionContribuyente.ShowModal;
         WizardCancelacionContribuyente.Free;
 end;

procedure TTressShell.cxGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;

  if _R_RegistrarContribuyente.Enabled  then
   _R_RegistrarContribuyenteExecute(Sender);
end;


procedure TTressShell.SetPosTimbradoBookMark;
begin
      try
           FTimbradoPos := nil;
           if DataSourcePeriodos.DataSet <> nil then
              with DataSourcePeriodos.DataSet do
              begin
                   FTimbradoPos := GetBookmark;
              end;
      Except
      end;
end;

procedure TTressShell.GoPosTimbradoBookMark;
begin
     if DataSourcePeriodos.DataSet <> nil then
        with DataSourcePeriodos.DataSet do
        begin
            if ( FTimbradoPos <> nil ) then
            begin
                 if BookmarkValid(FTimbradoPos) then
                 begin
                       GotoBookMark( FTimbradoPos );
                       FreeBookMark( FTimbradoPos );
                 end;
            end;
        end;
end;

function TTressShell.RegresarParametrosNomina: TZetaParams;
var
   oParametros: TZetaParams;
begin
     oParametros := TZetaParams.Create;
     oParametros.AddInteger('PE_YEAR', dmCliente.YearDefault );
     oParametros.AddInteger('PE_TIPO', Ord( dmCliente.PeriodoTipo ) );
     oParametros.AddInteger('PE_NUMERO', dmCliente.PeriodoNumero );
     Result := oParametros;
end;


procedure TTressShell.gridPeriodosDBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
  inherited;
 ZGridModeTools.BorrarItemGenericoAll( AValueList );
end;

procedure TTressShell._P_TimbrarNominaE;
begin
   if (PeriodoNumeroCB_DevEx2.ValorEntero = 0) {( dmInterfase.cdsPeriodosTimbrar.IsEmpty )} then
   begin
        ZWarning( Self.Caption, 'No hay n�minas para timbrar', 0, mbOK );
   end
   else
   begin
         SetPosTimbradoBookMark;

         with dmInterfase do
             begin
                  cdsPeriodosAfectados.Refrescar;
                  cdsPeriodosTimbrar.Data := cdsPeriodosAfectados.Data;
                  cdsPeriodosTimbrar.Filter := '(PE_TIMBRO in (0,1,3) )' ;
                  cdsPeriodosTimbrar.Filtered := TRUE;
         end;

         {$ifdef WSPROFILE_SQL}
         SQLProfile( 'TimbrarNomina', dmInterfase.cdsPeriodosTimbrar.Filter );
         dmInterfase.cdsPeriodosTimbrar.SaveToFile(Format('SQLPROFILE_PeriodosTimbrar_%d_%d_%d.xml', [dmCliente.YearDefault, Ord( dmCliente.PeriodoTipo ), dmCliente.PeriodoNumero]), dfXML);
         {$endif}

         if ( dmInterfase.cdsPeriodosTimbrar.IsEmpty ) then
         begin
              with dmCliente.GetDatosPeriodoActivo do
              begin
                   if ( ord( StatusTimbrado ) = Ord( estiCancelacionPendiente ) ) then
                   begin
                        ZWarning( Self.Caption, 'La n�mina no puede ser timbrada, esta en cancelaci�n pendiente', 0, mbOK );
                   end
                   else if ( ord( StatusTimbrado ) = Ord( estiTimbrado ) ) then
                   begin
                        ZWarning( Self.Caption, 'Esta n�mina ya est� timbrada', 0, mbOK );
                   end;
              end;
         end
         else
         if ( dmInterfase.cdsPeriodosTimbrar.FieldByName('PE_STATUS').AsInteger <  Ord(spAfectadaParcial ) ) then
         begin
              ZWarning( Self.Caption, 'La n�mina no puede ser Timbrada, debe estar en estatus Afectada (Parcial o Total)', 0, mbOK );
         end
         else
         begin
              dmInterfase.HuboRespuestaTimbramex :=  FALSE;
            {$ifdef MULTIPAQUETE}
              WizardTimbradoThreadForm :=  TWizardTimbradoThreadForm.Create( Self );
              WizardTimbradoThreadForm.ParamsTimbradoNomina := RegresarParametrosNomina;
              WizardTimbradoThreadForm.ShowModal;
              WizardTimbradoThreadForm.Free;
            {$else}
              WizardTimbradoForm :=  TWizardTimbradoForm.Create( Self );
              WizardTimbradoForm.ShowModal;
              WizardTimbradoForm.Free;
            {$endif}

              FParamsTimbradoNomina := RegresarParametrosNomina;
              FParamsTimbradoNomina.AddBoolean('TIMBRADO', True );
              FParamsTimbradoNomina.AddBoolean('HUBO_RESPUESTA', dmInterfase.HuboRespuestaTimbramex );

              if ( dmInterfase.HuboRespuestaTimbramex ) then
              begin
                   dmInterfase.TimbrarNomina( FParamsTimbradoNomina );
              end
              else
              begin
                   //Agregar respuesta
                   SetEstatusPendienteEmpleadosResultadoDSLimpiar;                   
              end;
			  dmInterfase.HuboRespuestaTimbramex :=  FALSE; 	
         end;

   end;
end;

procedure TTressShell._P_TimbrarNominaExecute(Sender: TObject);
begin
   inherited;
_P_TimbrarNominaE;

end;

procedure TTressShell.btREfrescaPeriodosClick(Sender: TObject);
begin
  inherited;
  dmInterfase.cdsPeriodosAfectadosTotal.Refrescar;
end;

procedure TTressShell._Per_GenerarPolizaExecute(Sender: TObject);
begin

end;

procedure TTressShell._P_CancelarTimbradoNominaExecute(Sender: TObject);
begin
   inherited;
            _P_CancelarTimbradoNominaE;

end;

procedure TTressShell._P_CancelarTimbradoNominaE;
begin
     if (PeriodoNumeroCB_DevEx2.ValorEntero = 0) then
   begin
        ZWarning( Self.Caption, 'No hay n�minas timbradas', 0, mbOK );
   end
   else
   begin
         SetPosTimbradoBookMark;

         with dmInterfase do
         begin
                 cdsPeriodosAfectados.Refrescar;
                 cdsPeriodosCancelar.Data := cdsPeriodosAfectados.Data;
                 cdsPeriodosCancelar.Filter := '(PE_TIMBRO in (1,2,4) ) ' ;
                 cdsPeriodosCancelar.Filtered := TRUE;
         end;


         if ( dmInterfase.cdsPeriodosCancelar.IsEmpty ) then
         begin
              with dmCliente.GetDatosPeriodoActivo do
              begin
                   if ( ord( StatusTimbrado ) = Ord( estiTimbradoPendiente ) ) then
                   begin
                        ZWarning( Self.Caption, 'La n�mina no puede ser cancelada, esta en timbrado pendiente', 0, mbOK );
                   end
                   else
                   begin
                        ZWarning( Self.Caption, 'Esta n�mina no est� Timbrada', 0, mbOK );
                   end;
              end;
         end
         else
         begin
              dmInterfase.HuboRespuestaTimbramex :=  FALSE;

              WizardCancelarTimbradoForm :=  TWizardCancelarTimbradoForm.Create( Self );
              WizardCancelarTimbradoForm.ParamsTimbradoNomina := RegresarParametrosNomina;
              WizardCancelarTimbradoForm.ShowModal;

              WizardCancelarTimbradoForm.Free;

              FParamsTimbradoNomina := RegresarParametrosNomina;
              FParamsTimbradoNomina.AddBoolean('CANCELANDO_TIMBRADO', True );
              FParamsTimbradoNomina.AddBoolean('HUBO_RESPUESTA', dmInterfase.HuboRespuestaTimbramex );
              if ( dmInterfase.HuboRespuestaTimbramex ) then
                 dmInterfase.CancelarTimbradoNomina(FParamsTimbradoNomina)
              else
              begin
                   //Agregar respuesta
                   SetEstatusPendienteEmpleadosResultadoDSLimpiar;
              end;

              dmInterfase.HuboRespuestaTimbramex :=  FALSE;
         end;
   end;
end;

procedure TTressShell._P_GetTimbradosE;
begin
       if (PeriodoNumeroCB_DevEx2.ValorEntero = 0) then
   begin
        ZWarning( Self.Caption, 'No hay n�minas timbradas', 0, mbOK );
   end
   else
   begin
        SetPosTimbradoBookMark;

        with dmInterfase do
           begin
                cdsPeriodosAfectados.Refrescar;
                cdsPeriodosRecibos.Data := cdsPeriodosAfectados.Data;
                cdsPeriodosRecibos.Filter := '(PE_TIMBRO in (1,2) ) ' ;
                cdsPeriodosRecibos.Filtered := TRUE;

       if ( dmInterfase.cdsPeriodosRecibos.IsEmpty ) then
       begin
            ZWarning( Self.Caption, 'No es n�mina Timbrada', 0, mbOK );
       end
       else
       begin
        WizardRecibosTimbradoForm :=  TWizardRecibosTimbradoForm.Create( Self );
        WizardRecibosTimbradoForm.ShowModal;
        WizardRecibosTimbradoForm.Free;
        end;
   end;

 end;
end;

procedure TTressShell._P_GetTimbradosExecute(Sender: TObject);
begin
  inherited;
           _P_GetTimbradosE;

end;

procedure TTressShell._P_GetTimbradosXEmpleadoE;
begin
   if (PeriodoNumeroCB_DevEx2.ValorEntero = 0) then
   begin
        ZWarning( Self.Caption, 'No hay n�minas timbradas', 0, mbOK );
   end
   else
   begin
        SetPosTimbradoBookMark;

             WizardRecibosXEmpleadoForm :=  TWizardRecibosXEmpleadoForm.Create( Self );
             WizardRecibosXEmpleadoForm.ShowModal;
             WizardRecibosXEmpleadoForm.Free;

        //------------------------------------------------------------------------------
        dmInterfase.cdsPeriodosAfectados.Refrescar;
        dmInterfase.cdsPeriodosRecibos.Data := dmInterfase.cdsPeriodosAfectados.Data;
        dmInterfase.cdsPeriodosRecibos.Filter := '(PE_TIMBRO in (1,2) ) ' ;
        dmInterfase.cdsPeriodosRecibos.Filtered := TRUE;
   end;
end;

procedure TTressShell._P_GetTimbradosXEmpleadoExecute(Sender: TObject);
begin
  inherited;
_P_GetTimbradosXEmpleadoE;

end;

function TTressShell.CheckDerechosTress: Boolean;
begin
Result := RevisaCualquiera( D_EMPLEADOS ) or
               RevisaCualquiera( D_NOMINA ) or
               RevisaCualquiera( D_CONSULTAS ) or
               RevisaCualquiera( D_CATALOGOS ) or
               RevisaCualquiera( D_TABLAS ) ;
end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
  inherited;
   _V_Cerrar.Execute;
end;

procedure TTressShell.RevisaDerechos;
var
   iContadorDerechosDesactivados : integer;
begin
  iContadorDerechosDesactivados := 0;

  if(  Revisa( D_TIM_PROC_TIMBRAR_NOMINA )  ) then
  begin
        _P_TimbrarNomina.Enabled := true;
  end
  else
  begin
        _P_TimbrarNomina.Enabled := false;
        inc(iContadorDerechosDesactivados);
  end;

  if(  Revisa( D_TIM_PROC_CANCELAR_TIMBRADO )  ) then
  begin
        _P_CancelarTimbradoNomina.Enabled := true;
  end
  else
  begin
        _P_CancelarTimbradoNomina.Enabled := false;
        inc(iContadorDerechosDesactivados);
  end;

  if(  Revisa( D_TIM_PROC_TOMAR_RECIBOS )  ) then
  begin
        _P_GetTimbrados.Enabled := true;
  end
  else
  begin
  _P_GetTimbrados.Enabled := false;
        inc(iContadorDerechosDesactivados);
  end;

  if(  Revisa( D_TIM_PROC_ADQUIRIR_FOLIOS )  ) then
  begin
        _A_FoliosFiscales.Enabled := true;
  end
  else
  begin
  _A_FoliosFiscales.Enabled := false;
        inc(iContadorDerechosDesactivados);
  end;

  if(  Revisa( D_TIM_PROC_TOMAR_RECIBOS_EMPLEADO )  ) then
  begin
        _P_GetTimbradosXEmpleado.Enabled := true;
  end
  else
  begin
  _P_GetTimbradosXEmpleado.Enabled := false;
        inc(iContadorDerechosDesactivados);
  end;

  if(  Revisa( D_TIM_REG_REGISTRAR_CONTRIBUYENTE )  ) then
  begin
        _R_RegistrarContribuyente.Enabled := true;
  end
  else
  begin
  _R_RegistrarContribuyente.Enabled := false;
        inc(iContadorDerechosDesactivados);
  end;

  if(  Revisa( D_TIM_REG_CANCELAR_CONTRIBUYENTE)  ) then
  begin
        _R_CancelarContribuyente.Enabled := true;
  end
  else
  begin
  _R_CancelarContribuyente.Enabled := false;
        inc(iContadorDerechosDesactivados);
  end;

  if(  Revisa( D_TIM_PROC_ENVIAR_RECIBOS)  ) then
  begin
        _P_EnviarTimbrados.Enabled := true;
  end
  else
  begin
  _P_EnviarTimbrados.Enabled := false;
        inc(iContadorDerechosDesactivados);
  end;

  if iContadorDerechosDesactivados = 8 then
  begin
        TabProcesos.Visible :=  false;
  end
  else
  begin
      TabProcesos.Visible :=  true;
  end;

end;

procedure TTressShell.SistemaYearCBLookUp(Sender: TObject;
  var lOk: Boolean);
var
   oCursor: TCursor;
begin
  inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        CambioPeriodoActivo;
        RefrescaSistemaActivos;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.n1Click(Sender: TObject);
begin
  inherited;
   ZetaBuscador_DevEx.BuscarCodigo( 'N�mero', 'Periodos Afectados', 'Numero', dmInterfase.cdsPeriodosAfectadosTotal );
end;



procedure TTressShell._P_EnviarTimbradosE;
begin
   if (PeriodoNumeroCB_DevEx2.ValorEntero = 0) then
   begin
        ZWarning( Self.Caption, 'No hay n�minas timbradas', 0, mbOK );
   end
   else
   begin
        SetPosTimbradoBookMark;

        with dmInterfase do
           begin
                cdsPeriodosAfectados.Refrescar;
                cdsPeriodosRecibos.Data := cdsPeriodosAfectados.Data;
                cdsPeriodosRecibos.Filter := '(PE_TIMBRO in (1,2) ) ' ;
                cdsPeriodosRecibos.Filtered := TRUE;

       if ( dmInterfase.cdsPeriodosRecibos.IsEmpty ) then
       begin
            ZWarning( Self.Caption, 'No es n�mina Timbrada', 0, mbOK );
       end
       else
       begin
        WizardEnvioRecibosTimbradoForm :=  TWizardEnvioRecibosTimbradoForm.Create( Self );
        WizardEnvioRecibosTimbradoForm.ShowModal;
        WizardEnvioRecibosTimbradoForm.Free;
        end;
   end;
 end;
end;

procedure TTressShell._P_EnviarTimbradosExecute(Sender: TObject);
begin
  inherited;
           _P_EnviarTimbradosE;

end;

procedure TTressShell.btBuscarCuentaClick(Sender: TObject);
begin
  inherited;
  ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Cuenta de Timbrado', 'CT_CODIGO', dmInterfase.cdsCuentasTimbramex );
end;

procedure TTressShell.btConsultarClick(Sender: TObject);
begin
  inherited;
//
end;

procedure TTressShell.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     with Application do
     begin
          CanClose := ZetaDialogo.ZConfirm( 'Salir de ' + Title, '� Desea Salir de ' + Title + ' ?', 0, mbYes );
     end;
end;

procedure TTressShell.AsistenciaFechavalid(Valor: TDate);
begin
 dmCliente.FechaAsistencia :=Valor;
end;

procedure TTressShell.RefrescaIMSSActivoPublico;
begin
//
end;

procedure TTressShell.btnYearBefore_DevExClick(Sender: TObject);
begin
  inherited;
  CalculaAnio ( K_ANTERIOR );
end;

procedure TTressShell.btnYearNext_DevExClick(Sender: TObject);
begin
  inherited;
  CalculaAnio ( K_SIGUIENTE );
end;

procedure TTressShell.SistemaYear_DevExPropertiesChange(Sender: TObject);
begin
  inherited;
  if ( Length( SistemaYear_DevEx.Text ) = 4 ) then
  begin
        CalculaAnio ( K_MANUAL);
  end;
end;


procedure TTressShell.CalculaAnio ( Operacion:Integer );
var
   oCursor: TCursor;
   NewYearValue:Integer;
function ValidaNewYear ( NewYearValue:Integer ):Integer;
 begin
     if NewYearValue > K_MAX_YEAR then
     begin
        NewYearValue := K_MAX_YEAR;
        btnYearNext_DevEx.Enabled := FALSE;  //Deshabilita btnYearSiguiente
     end
     else if NewYearValue < K_MIN_YEAR then
     begin
        NewYearValue := K_MIN_YEAR;
        btnYearBefore_DevEx.Enabled := FALSE;//Deshabilita btnYearAnterior
     end
     else
     begin
        HabilitaBotonesAnio; //Habilita ambos botones
     end;
     Result := NewYearValue;
 end;
begin
     NewYearValue := dmCliente.YearDefault;
     case Operacion of
          K_ANTERIOR:
               NewYearValue := dmCliente.YearDefault -1;
          K_SIGUIENTE:
               NewYearValue := dmCliente.YearDefault +1;
          K_MANUAL:
               NewYearValue := StrToInt ( SistemaYear_DevEx.Text );
     end;

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
              YearDefault := ValidaNewYear ( NewYearValue );
              SistemaYear_DevEx.Text := IntToStr ( YearDefault );
        end;

        CambioPeriodoActivo;
        RefrescaSistemaActivos;



     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.HabilitaBotonesAnio;
begin
     btnYearNext_DevEx.Enabled := TRUE;
     btnYearBefore_DevEx.Enabled := TRUE;
end;




procedure TTressShell.ApplyMinWidth;
begin
//
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
     _A_OtraEmpresa.Enabled := true;
     _C_ConfigurarConceptos.Enabled := true;
     _A_SalirSistema.Enabled := true;
     _H_AcercaDe.Enabled := true;
     _H_Contenido.Enabled := true;

    CargaVista;
    CargaTraducciones;

    CrearValoresInicialesTimbradoWeb;
end;

procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)

end;


procedure TTressShell.AsignacionTabOrder_DevEx;
begin
     DevEx_ShellRibbon.TabOrder:= 1;
     PanelNavBar.TabOrder := 2;
     DevEx_BandaValActivos.TabOrder :=3;
     PanelConsulta.TabOrder :=4;
end;


procedure TTressShell.CargaVista;
begin
  inherited;
     //Mostar Banda Valores activos
     DevEx_BandaValActivos.Color := TColor($F9E6ED);
     DevEx_BandaValActivos.Visible := True;

     //Asignacion de TabOrders para el shell
     AsignacionTabOrder_DevEx;

     Application.ProcessMessages;
end;

procedure TTressShell.DoOpenSistema;
begin
     Application.ProcessMessages;
     Application.Terminate;
end;
procedure TTressShell._A_Bitacora_DevExExecute(Sender: TObject);
begin
  inherited;
            AbreFormaConsulta( efcSistBitacora );
end;

procedure TTressShell._A_ExploradorReportesExecute(Sender: TObject);
begin
  inherited;
            AbreFormaConsulta( efcReportes );
end;

procedure TTressShell._A_FoliosFiscalesExecute(Sender: TObject);
var
WizardFoliosfiscalespendientes :  TWizardFoliosfiscalespendientes;
begin
  inherited;
     WizardFoliosfiscalespendientes :=  TWizardFoliosfiscalespendientes.Create( Self );
     WizardFoliosfiscalespendientes.ShowModal;
     WizardFoliosfiscalespendientes.Free;
end;

procedure TTressShell._A_ListaProcesosExecute(Sender: TObject);
begin
  inherited;
            AbreFormaConsulta( efcSistProcesos );
end;

procedure TTressShell._A_SQL_DevExExecute(Sender: TObject);
begin
  inherited;
            AbreFormaConsulta( efcQueryGral );
end;

procedure TTressShell._C_ConciliarTimbradoExecute(Sender: TObject);
var
   lModoDemo: Boolean;
begin
     inherited;
     lModoDemo := Autorizacion.EsDemo;
     if ( not Autorizacion.OkModulo( okPortal )) then
     begin
          ZetaDialogo.zInformation( '', 'M�dulo de conciliador de timbrado no est� autorizado,' + CR_LF + 'consulte a su distribuidor para adquirirlo.' , 0);
     end
     else if lModoDemo then
     begin
          ZetaDialogo.zInformation( '', 'Autorizaci�n en modo demo, consulte a su distribuidor para adquirir el m�dulo.', 0 );
     end
     else
     begin
          _C_ConciliarTimbradoE;
     end;
end;

procedure TTressShell._E_RefrescarExecute(Sender: TObject);
begin
     RefrescarShellPeriodos;
     inherited;
end;

procedure TTressShell.RefrescarShellPeriodos;
begin
     with dmCliente do
     begin
          if (empresaAbierta) then
          begin
               RefrescaPeriodo;
               RefrescaPeriodoActivos(false);
          end ;
     end;
end;

procedure TTressShell._C_ConciliarTimbradoE;
begin
          SetPosTimbradoBookMark;
          WizardConciliacionTimbradoForm :=  TWizardConciliacionTimbradoForm.Create( Self );
          WizardConciliacionTimbradoForm.ShowModal;
          WizardConciliacionTimbradoForm.Free;
          //------------------------------------------------------------------------------
          dmInterfase.cdsPeriodosAfectados.Refrescar;
          dmInterfase.cdsPeriodosRecibos.Data := dmInterfase.cdsPeriodosAfectados.Data;
          dmInterfase.cdsPeriodosRecibos.Filter := '(PE_TIMBRO in (1,2) ) ' ;
          dmInterfase.cdsPeriodosRecibos.Filtered := TRUE;

          _E_Refrescar.Execute;
end;

end.
