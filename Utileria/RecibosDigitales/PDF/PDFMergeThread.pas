unit PDFMergeThread;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,DebenuPDFLibrary1115;

type
  TPDFMergeThread=class(TThread)
  private
    sArchivo,sArchivo2,sBaseName,sPathDestino,sRespuesta : String;
  public
   constructor Create(CreateSuspended : boolean; sArchivo, sArchivo2,sBaseName, sPathDestino : string);
   procedure Execute; override;
   procedure TerminatedMerge;
   function GeneraPDF(sArchivo, sArchivo2, sBaseName, sPathDestino: String): String;
   function TerminateThread:Boolean;
   procedure  TerminarProceso;
  end;

implementation



constructor TPDFMergeThread.Create(CreateSuspended : boolean; sArchivo, sArchivo2,sBaseName, sPathDestino : string);
begin
  Self.sArchivo := sArchivo;
  Self.sArchivo2 := sArchivo2;
  Self.sBaseName := sBaseName;
  Self.sPathDestino := sPathDestino;
  inherited Create(CreateSuspended);
end;


procedure TPDFMergeThread.TerminarProceso;
begin
     inherited Destroy;
end;


procedure TPDFMergeThread.Execute;
begin

  while not Self.Terminated do
  begin
     GeneraPDF(sArchivo,sArchivo2,sBaseName,sPathDestino);
     TerminatedMerge;
  end;

end;

procedure TPDFMergeThread.TerminatedMerge;
begin
     Self.Terminate;
end;

function TPDFMergeThread.TerminateThread: Boolean;
begin
     Result := Self.Terminated;
end;


function TPDFMergeThread.GeneraPDF(sArchivo, sArchivo2, sBaseName, sPathDestino: String) : String;
var
   DPL : TDebenuPDFLibrary1115;



   function BorrarPDF(fichero: String): Boolean;
   begin
      try
         if FileExists(fichero) then begin
            if (DeleteFile(fichero)) then
            begin
                 Result := True;
            end
            else
            begin
                 Result := False;
            end
         end
         else
         begin
              Result := False;
         end;
       except on Error: Exception do
              Result := False;
       end;
   end;


begin
     try
        DPL  := TDebenuPDFLibrary1115.Create;
        DPL.UnlockKey('jy9p34jr83a5sr8517jj95b6y');
        DPL.AddToFileList('FilesToMerge', sArchivo);
        DPL.AddToFileList('FilesToMerge', sArchivo2);
        DPL.MergeFileListFast('FilesToMerge', sPathDestino + sBaseName);
     finally
         BorrarPDF( sArchivo );
         BorrarPDF( sArchivo2 ); 
     end;
     Result := sPathDestino + sBaseName;
end;



end.

