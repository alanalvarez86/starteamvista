unit FCatCuentasTimbrado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, System.Actions,
  cxContainer, Vcl.Mask, ZetaDialogo;

type
  TCatCuentasTimbrado_DevEx = class(TBaseGridLectura_DevEx)
    DataSourceCuentas: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    { Private declarations }
    procedure Refrescar;
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
  end;

var
  CatCuentasTimbrado_DevEx: TCatCuentasTimbrado_DevEx;

implementation

uses dInterfase,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.dfm}

procedure TCatCuentasTimbrado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Refrescar;
     CanLookup := false;

     HelpContext:= H32140_CuentasTimbrado;
end;

procedure TCatCuentasTimbrado_DevEx.Refrescar;
begin
     dmInterfase.cdsCuentasTimbramex.Refrescar;
     DataSource.DataSet :=  dmInterfase.cdsCuentasTimbramex;
end;

procedure TCatCuentasTimbrado_DevEx.Connect;
begin
     inherited;
     with dmInterfase do
     begin
          cdsCuentasTimbramex.Conectar;
          DataSource.DataSet:= cdsCuentasTimbramex;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCuentasTimbrado_DevEx.Refresh;
begin
     inherited;
     dmInterfase.cdsCuentasTimbramex.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCuentasTimbrado_DevEx.Agregar;
begin
     inherited;
     dmInterfase.cdsCuentasTimbramex.Agregar;
end;

procedure TCatCuentasTimbrado_DevEx.Borrar;
begin
     inherited;
     dmInterfase.cdsCuentasTimbramex.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;


procedure TCatCuentasTimbrado_DevEx.Modificar;
begin
     inherited;
     dmInterfase.cdsCuentasTimbramex.Modificar;
end;


procedure TCatCuentasTimbrado_DevEx.FormShow(Sender: TObject);
begin
 ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CT_CODIGO'), K_SIN_TIPO , '', skCount);
  ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TCatCuentasTimbrado_DevEx.ZetaDBGridDBTableViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
    //dmInterfase.cdsTiposSAT.Modificar;
end;




end.
