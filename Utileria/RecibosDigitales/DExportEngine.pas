unit DExportEngine;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$ifdef TRESS_DELPHIXE5_UP}System.UITypes,{$endif}
  gtCstGfxEng, gtMetafileEng, gtWMFEng, gtTXTEng, gtCstSpdEng, gtCstXLSEng,
  gtExXLSEng, gtXLSEng, gtCstHTMLEng, gtExHTMLEng, gtHTMLEng, gtRTFEng,
  gtCstDocEng, gtCstPlnEng, gtCstPDFEng, gtExPDFEng, gtPDFEng, gtClasses3,
  gtXportIntf, gtPDFCrypt3,
  gtQProEng, gtXHTMLEng, gtEMFEng, gtBMPEng, gtGIFEng,
  gtJPEGEng, gtSVGEng, gtPNGEng, gtTIFFEng, gtSLKEng, gtDIFEng, gtLotusEng,

  QuickRpt, IniFiles,
  ZetaCommonLists,
  ZetaCommonTools,
  ZDocumentDlg_DevEx, gtQRXportIntf;  //Edit by mp: Cargando Zdocumentdlg_DevEx

type

  TdmExportEngine = class(TDataModule)
    QRExportInterface: TgtQRExportInterface;
    PDFEngine: TgtPDFEngine;
    RTFEngine: TgtRTFEngine;
    HTMLEngine: TgtHTMLEngine;
    ExcelEngine: TgtExcelEngine;
    TextEngine: TgtTextEngine;
    WMFEngine: TgtWMFEngine;
    XHTMLEngine: TgtXHTMLEngine;
    QuattroProEngine: TgtQuattroProEngine;
    LotusEngine: TgtLotusEngine;
    DIFEngine: TgtDIFEngine;
    SLKEngine: TgtSYLKEngine;
    TIFFEngine: TgtTIFFEngine;
    PNGEngine: TgtPNGEngine;
    SVGEngine: TgtSVGEngine;
    JPEGEngine: TgtJPEGEngine;
    BMPEngine: TgtBMPEngine;
    EMFEngine: TgtEMFEngine;
  private
    { Private declarations }
    FIniFile : TExportIniFile;
    procedure CargaPreferencias( Engine: TgtCustomDocumentEngine; const eTipo: eTipoFormato; var lVariasPaginas: Boolean );
    procedure CargaPreferenciasHtml( Engine: TgtCustomHTMLEngine; var lVariasPaginas: Boolean );
    procedure CargaPreferenciasTexto( Engine: TgtTextEngine; var lVariasPaginas: Boolean );
    procedure CargaPreferenciasXls( Engine: TgtCustomSpreadSheetEngine; var lVariasPaginas: Boolean );
    procedure CargaPreferenciasSVG( Engine: TgtSVGEngine; var lVariasPaginas: Boolean );
    procedure CargaPreferenciasRTF( Engine: TgtRTFEngine; var lVariasPaginas: Boolean );
    procedure CargaPreferenciasPDF( Engine: TgtPDFEngine; var lVariasPaginas: Boolean );
  public
    { Public declarations }
    procedure Exporta( Reporte: TQuickRep; const eTipo: eTipoFormato; const sArchivo: string; FDefaultStream: TStream = NIL);
  end;

var
  dmExportEngine: TdmExportEngine;

procedure Preferencias( const eTipo: eTipoFormato );
function Encrypt( const sSource: String ): String;
{$IFDEF TRESS_DELPHIXE5_UP}
function Decrypt( const sSource: String ): AnsiString;
{$ELSE}
function Decrypt( const sSource: String ): String;
{$ENDIF}


implementation
uses

    FHtmlDlg_DevEx,    //Edit by mp:Cargando formas de edicion de Documento_DevEx
    FTextDlg_DevEx,
    FXlsDlg_DevEx,
    FRtfDlg_DevEx,
    FPdfDlg_DevEx,
    ZetaCommonClasses,
    ZReportDialog,DCliente;
{$R *.DFM}

procedure Preferencias( const eTipo: eTipoFormato );
 var
    FDialogo_DevEx:TDOcumentDlg_DevEx;
begin
     case eTipo of

          tfHTML, tfHTM: FDialogo_DevEx := THtmlDlg_DevEx.Create( Application );
          tfTXT: FDialogo_DevEx := TTextDlg_DevEx.Create( Application );
          tfXLS, tfDIF, tfWB1, tfWK2, tfSLK: FDialogo_DevEx := TXLSDlg_DevEx.Create( Application );
          tfRTF: FDialogo_DevEx := TRtfDlg_DevEx.Create( Application );
          tfPDF: FDialogo_DevEx := TPdfDlg_DevEx.Create( Application )

     else
         FDialogo_DevEx := TDocumentDlg_DevEx.Create( Application );
     end;
     try
        with FDialogo_DevEx do
        begin
             Formato := eTipo;
             ShowModal;
        end;
     finally
            FreeAndNil( FDialogo_DevEx );
     end;
end;

const
     K_KEY = 'mAzER0sKy';

{$IFDEF TRESS_DELPHIXE5_UP}
function Decrypt( const sSource: String ): AnsiString;
{$ELSE}
function Decrypt( const sSource: String ): String;
{$ENDIF}
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iTmpSrcAsc: Integer;
begin
     Result := '';
     if StrLleno( sSource ) then
     begin
          try
             sKey := K_KEY;
             iKeyLen := Length( sKey );
             iKeyPos := 0;
             iOffset := StrToInt( '$' + Copy( sSource, 1, 2 ) );
             iSrcPos := 3;
             repeat
                   iSrcAsc := StrToInt( '$' + Copy( sSource, iSrcPos, 2 ) );
                   if ( iKeyPos < iKeyLen ) then
                      iKeyPos := iKeyPos + 1
                   else
                       iKeyPos := 1;
                   iTmpSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
                   if ( iTmpSrcAsc <= iOffset ) then
                      iTmpSrcAsc := 255 + iTmpSrcAsc - iOffset
                   else
                       iTmpSrcAsc := iTmpSrcAsc - iOffset;
                   Result := Result + Chr( iTmpSrcAsc );
                   iOffset := iSrcAsc;
                   iSrcPos := iSrcPos + 2;
             until ( iSrcPos >= Length( sSource ) );
          except
          end;
     end;
end;

function Encrypt( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iRange: Integer;
begin
     if StrLleno( sSource ) then
     begin
          sKey := K_KEY;
          iKeyLen := Length( sKey );
          iKeyPos := 0;
          iRange := 256;
          Randomize;
          iOffset := Random( iRange );
          Result := Format( '%1.2x', [ iOffset ] );
          for iSrcPos := 1 to Length( sSource ) do
          begin
               iSrcAsc := ( Ord( sSource[ iSrcPos ] ) + iOffset ) mod 255;
               if ( iKeyPos < iKeyLen ) then
                  iKeyPos := iKeyPos + 1
               else
                   iKeyPos := 1;
               iSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
               Result := Result + Format( '%1.2x',[ iSrcAsc ] );
               iOffset := iSrcAsc;
          end;
     end
     else
         Result := '';
end;


{ ******************   TdmExportEngine   ***************** }

procedure TdmExportEngine.CargaPreferenciasHtml( Engine: TgtCustomHTMLEngine; var lVariasPaginas: Boolean );
begin
     with FIniFile do
     begin
          with Engine do
          begin
               with Preferences do
               begin
                    ActiveHyperLinks := LeeLogico( K_ActiveLinks, TRUE );
                    PageEndLines := LeeLogico( K_PageEndLines, TRUE );
                    OptimizeForIe := LeeLogico( K_OptimizeforIE, TRUE );
                    SingleFile := LeeLogico( K_SingleFile, TRUE );
                    lVariasPaginas := NOT SingleFile;
               end;
               with Navigator do
               begin
                    Enabled := LeeLogico( K_ShowNavigator, TRUE );
                    LinkBackColor := LeeEntero( K_ColorBackGround, clWhite );
                    LinkHoverBackColor := LeeEntero( K_ColorBackGroundHover, clBlue );
                    LinkHoverForeColor := LeeEntero( K_ColorForeGroundHover, clWhite );
                    with LinkFont do
                    begin
                         Name := LeeTexto( K_FontName, K_FontNameDefault );
                         Color := LeeEntero( K_FontColor, clNavy );
                         Size := LeeEntero( K_FontSize, K_FontSizeNumber );
                         Style := [];
                         if LeeLogico( K_FontBold, FALSE ) then
                            Style := Style + [fsBold];
                         if LeeLogico( K_FontItalic, FALSE ) then
                            Style := Style + [fsItalic];
                         if LeeLogico( K_FontUnderline, FALSE ) then
                            Style := Style + [fsUnderline];
                         if LeeLogico( K_FontStrikeOut, FALSE ) then
                            Style := Style + [fsStrikeOut];
                    end;

                    NavigatorType := TgtHTMLNavigatorType( LeeEntero( K_NavigatorType, 0 ) );
                    NavigatorOrientation := TgtHTMLNavigatorOrientation( LeeEntero( K_NavigatorOrientation, 0 ) );
                    NavigatorPosition := TgtHTMLNavigatorPosition( LeeEntero( K_NavigatorPosition, 0 ) );
                    UseTextLinks := LeeLogico( K_NavLinksUseText, TRUE );
                    LinkTextFirst := LeeTexto( K_NavLinksFirstText,  K_Primero );
                    LinkTextPrev := LeeTexto( K_NavLinksPriorText,   K_Anterior  );
                    LinkTextNext := LeeTexto( K_NavLinksNextText,    K_Siguiente );
                    LinkTextLast := LeeTexto( K_NavLinksLastText,    K_Ultimo  );

                    LinkImageFirst := LeeTexto( K_NavLinksFirstGraphic, VACIO );
                    LinkImagePrev := LeeTexto( K_NavLinksPriorGraphic, VACIO );
                    LinkImageNext := LeeTexto( K_NavLinksLastGraphic, VACIO );
                    LinkImageLast := LeeTexto( K_NavLinksNextGraphic, VACIO );
               end;
          end;
     end;
end;

procedure TdmExportEngine.CargaPreferenciasTexto( Engine: TgtTextEngine; var lVariasPaginas: Boolean);
begin
     with Engine do
     begin
          with FIniFile do
          begin
               PageEndLines := LeeLogico( K_PageEndLines, TRUE );
               InsertPageBreaks := LeeLogico( K_InsertPageBreaks, TRUE );
               SingleFile  := LeeLogico( K_SingleFile, TRUE );
          end;
          lVariasPaginas := NOT SingleFile ;
     end;
end;

procedure TdmExportEngine.CargaPreferenciasSVG( Engine: TgtSVGEngine; var lVariasPaginas: Boolean );
begin
     Engine.Preferences.ActiveHyperLinks := TRUE;
end;

procedure TdmExportEngine.CargaPreferenciasRTF( Engine: TgtRTFEngine; var lVariasPaginas: Boolean );
begin
     with Engine do
     begin
          with FIniFile do
          begin
               with Preferences do
               begin
                    ActiveHyperLinks := LeeLogico( K_ActiveLinks, TRUE );
                    GraphicDataInBinary := LeeLogico( K_GraphicDataInBinary, TRUE );
               end;
               DocumentEncodingType := TgtRTFEncodingType( LeeEntero( K_DocumentEncondingType, 0 ) );
          end;
     end;
end;

procedure TdmExportEngine.CargaPreferenciasPDF( Engine: TgtPDFEngine; var lVariasPaginas: Boolean );
begin
     with Engine do
     begin
          with FIniFile do
          begin
               FontEncoding := TgtPDFFontEncoding( LeeEntero(  K_EncodingFont ) );
               with Preferences do
               begin
                    ActiveHyperLinks := LeeLogico( K_ActiveLinks, TRUE );
                    EmbedTrueTypeFonts := TgtTTFontEmbedding( LeeEntero( K_TrueTypeFont, 0 ) );
               end;
               with Compression do
               begin
                    Enabled := LeeLogico( K_CompressionEnabled, TRUE );
                    Level := TgtCompressionLevel( LeeEntero( K_CompressionLevel, Ord(clMaxCompress) ) );
               end;
               with Encryption do
               begin
                    Enabled := LeeLogico( K_EncryptionEnabled, FALSE );
                    Level := TgtPDFEncryptionLevel( LeeEntero( K_EncryptionLevel, Ord(el40bit) ) );
                    OwnerPassword := Decrypt( LeeTexto( K_EncryptionOwner, VACIO ) );
                    UserPassWord := Decrypt( LeeTexto( K_EncryptionUser, VACIO ) );
                    UserPermissions := [];
                    if LeeLogico( K_UserPermissionModify, TRUE ) then
                       UserPermissions := UserPermissions + [AllowModify];
                    if LeeLogico( K_UserPermissionPrint, TRUE )then
                            UserPermissions := UserPermissions + [AllowPrint];
                    if LeeLogico( K_UserPermissionCopy, TRUE  )then
                            UserPermissions := UserPermissions + [ AllowCopy];
                    if LeeLogico( K_UserPermissionAnnotation  )then
                            UserPermissions := UserPermissions + [AllowAnnotation];
                    if LeeLogico( K_UserPermissionFormFill  )then
                            UserPermissions := UserPermissions + [AllowFormFill];
                    if LeeLogico( K_UserPermissionAccessibility  )then
                            UserPermissions := UserPermissions + [AllowAccessibility];
                    if LeeLogico( K_UserPermissionDocumentAssembly  )then
                            UserPermissions := UserPermissions + [AllowDocAssembly];
                    if LeeLogico( K_UserPermissionHighResolutionPrint  )then
                            UserPermissions := UserPermissions + [AllowHighResPrint];
               end;

               with ViewerPreferences do
               begin
                    PageLayout := TgtPageLayout( LeeEntero( K_PageLayout ) );
                    PageMode := TgtPageMode( LeeEntero( K_PageMode ) );
                    HideMenuBar := LeeLogico( K_MenuBar );
                    HideToolBar := LeeLogico( K_ToolBar  );
                    HideWindowUI := LeeLogico( K_NavigationControls );
                    with PresentationMode do
                    begin
                         Duration := LeeEntero( K_PageTransitionDuration );
                         TransitionEffect := TgtTransitionEffect( LeeEntero( K_PageTransitionEffect ) );

                    end;
               end;

          end;
     end;
end;


procedure TdmExportEngine.CargaPreferenciasXls( Engine: TgtCustomSpreadSheetEngine; var lVariasPaginas: Boolean );
begin
     with Engine do
     begin
          with FIniFile do
          begin
               with Formatting do
               begin
                    ColumnSpacing := LeeEntero( 'Column Spacing', 0 );
                    RowSpacing := LeeEntero( 'Row Spacing', -1 );
                    ScaleX := LeeEntero( 'Scale X', 1 );
                    ScaleY := LeeEntero( 'Scale Y', 1 );
                    ReferencePoint := TgtReferencePoint( LeeEntero( 'Reference', 0 ) );
               end;

               Preferences.AutoFormatCells := LeeLogico( 'Auto Format Cells', FALSE );
          end;
     end;

     if ( Engine is TgtExcelEngine ) then
     begin
          with TgtExcelEngine( Engine ).Preferences do
          begin
               with FIniFile do
               begin
                    PageMargins := LeeLogico( 'Page Margins', FALSE );
                    PrintGridLines := LeeLogico( 'Print Grid Lines', FALSE );
                    PagesPerWorkSheet := LeeEntero( 'Pages Per Worksheet', 0 );
               end;
          end;
     end;
end;


procedure TdmExportEngine.CargaPreferencias(  Engine : TgtCustomDocumentEngine; const eTipo: eTipoFormato; var lVariasPaginas: Boolean );
begin
     if FIniFile = NIL then
        FIniFile := TExportIniFile.Create;

     FIniFile.Formato := eTipo;

     with Engine do
     begin
          lVariasPaginas := FALSE;
          PagesToRender := FIniFile.LeeTexto( K_PagesToRender );

          ItemsToRender := [];

          if FIniFile.LeeLogico( K_RenderText, TRUE ) then
             ItemsToRender := ItemsToRender + [irText];
          if FIniFile.LeeLogico( K_RenderShapes, NOT(eTipo in [tfXLS]) ) then
             ItemsToRender := ItemsToRender + [irShape];
          if FIniFile.LeeLogico( K_RenderImage, TRUE ) then
             ItemsToRender := ItemsToRender + [irImage];
     end;

     case eTipo of
          tfHTML, tfHTM: CargaPreferenciasHtml( TgtCustomHTMLEngine( Engine ), lVariasPaginas );
          tfTXT: CargaPreferenciasTexto( TgtTextEngine( Engine ), lVariasPaginas );
          tfXLS, tfDIF,tfWB1,tfWK2,tfSLK: CargaPreferenciasXls( TgtCustomSpreadSheetEngine( Engine ), lVariasPaginas );
          tfRTF: CargaPreferenciasRTF( TgtRTFEngine( Engine ), lVariasPaginas );
          tfPDF: CargaPreferenciasPDF( TgtPDFEngine( Engine ), lVariasPaginas );
          tfSVG: CargaPreferenciasSVG( TgtSVGEngine( Engine ), lVariasPaginas );
     end;
end;

procedure TdmExportEngine.Exporta( Reporte: TQuickRep; const eTipo: eTipoFormato; const sArchivo: string; FDefaultStream: TStream = NIL);
 function HayVariasPaginas: Boolean;
 begin
      with Reporte do
           Result := ( eTipo in [tfWMF,tfEMF,tfBMP,tfPNG,tfJPEG,tfSVG] );
 end;

 var
    oEngine: TgtCustomDocumentEngine;
    lVariasPaginas: Boolean;
    sNuevoArchivo, sExt: string;
    oCursor: TCursor;


begin
     oCursor:= Screen.Cursor;
     Screen.Cursor := crHourGlass;

     try

        oEngine := NIL;
        case eTipo of
             tfHTML: oEngine := HtmlEngine;
             tfTXT: oEngine := TextEngine;
             tfXLS: oEngine := ExcelEngine;
             tfRTF: oEngine := RtfEngine;
             tfWMF: oEngine := WmfEngine;
             tfPDF: oEngine := PdfEngine;
             tfBMP: oEngine := BMPEngine;
             tfTIF: oEngine := TIFFEngine;
             tfPNG: oEngine := PNGEngine;
             tfJPEG:oEngine := JPEGEngine;
             tfEMF: oEngine := EMFEngine;
             tfSVG: oEngine := SVGEngine;
             tfHTM: oEngine := XHTMLEngine;
             tfWB1: oEngine := QuattroProEngine;
             tfWK2: oEngine := LotusEngine;
             tfDIF: oEngine := DIFEngine;
             tfSLK: oEngine := SLKEngine;
        end;

        if ( oEngine <> NIL ) then
        begin
             if ( oEngine is TgtPDFEngine ) then
             begin
                  with TgtPDFEngine( oEngine ) do
                  begin
                       Preferences.OutputToUserStream := ( FDefaultStream <> NIL );
                       UserStream := FDefaultStream;
                  end;
             end;
             
             with QRExportInterface do
             begin

                  Engine := oEngine;
                  with IgtDocumentEngine( Engine ) do
                  begin
                       Preferences.ShowSetupDialog := FALSE;
                       Preferences.OpenAfterCreate := FALSE;
                       FileName := sArchivo;
                       sExt := ExtractFileExt( sArchivo );
                       if StrLleno( sExt ) then
                       begin
                            if Pos( '.', sExt ) <> 0 then
                               FileExtension := Copy( sExt, Pos( '.', sExt ) + 1, Length( sExt ) ) ;
                       end;

                       CargaPreferencias( Engine, eTipo, lVariasPaginas );
                  end;

                  RenderDocument( Reporte, True );

                  with IgtDocumentEngine( Engine ) do
                  begin
                       sNuevoArchivo := FileName ;
                       if lVariasPaginas OR  HayVariasPaginas then
                          sNuevoArchivo := sNuevoArchivo + '1';
                       sNuevoArchivo :=  sNuevoArchivo + '.' + FileExtension;

                  end;
                  Engine := NIL;

             end;
             {$ifndef TRESSEMAIL}
             {$ifndef TRESSWEB}
             {$ifndef XMLREPORT}
             {$ifndef KIOSCO2}

             ZReportDialog.MuestraDialogo( 'Reporte Exportado', Format( 'El Archivo " %s " fue Exportado con Exito' + CR_LF +
                                                                        '� Desea Verlo ?', [ ExtractFileName( sArchivo ) ] ), sNuevoArchivo );
             {$endif}
             {$endif}
             {$endif}
             {$endif}
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


end.





