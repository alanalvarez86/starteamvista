program Consola;

uses
  Forms,
  Sysutils,
  ComObj,
  ActiveX,
  ZetaClientTools,
  ShellApi,
  ZBaseImportaShell in '..\..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  DCliente in '..\..\Datamodules\DCliente.pas',
  FLoginShell in 'FLoginShell.pas' {LoginShell};

{$R *.RES}

     procedure MuestraSplash;
     begin
          SplashScreen := TSplashScreen.Create( Application );
          with SplashScreen do
          begin
               Show;
               Update;
          end;
     end;

     procedure CierraSplash;
     begin
          with SplashScreen do
          begin
               Close;
               Free;
          end;
     end;

begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.CreateForm(TLoginShell, LoginShell);

     if (ParamCount > 0) then
     begin
          with LoginShell do
          begin
               MuestraSplash;
               if Login( False ) then
               begin
                    CierraSplash;
                    Ejecuta(ParamStr(1), SalidaCredenciales);
                    Free;
               end
               else
               begin
                    CierraSplash;
                    Free;
               end;

          end;
     end
     else
          Application.MessageBox('No se enviaron los parametros del ejecutable','Error',0);
end.
