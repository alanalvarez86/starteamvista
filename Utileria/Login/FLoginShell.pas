unit FLoginShell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseShell, ExtCtrls, ComCtrls, OCL, ZetaWinApiTools;

type
  TLoginShell = class(TBaseShell)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }

  protected
    { Protected declarations }
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;

  public
    { Public declarations }
    function SalidaCredenciales : String;
    function GetEjecutable : String;
    procedure Ejecuta( const sEjecutable, sCredenciales : String );

  end;

var
  LoginShell: TLoginShell;

const
     P_EJECUTABLE = 'EJECUTABLE';

implementation

uses DCliente, DGlobal, DSistema, DTablas,
     DRecursos, DConsultas, DProcesos,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet,
     FCalendario,
     FileCtrl,
     ZetaDialogo,
     ZetaServerTools,
     ZetaFilesTools,
     DCatalogos,
     DBaseSistema;

{$R *.dfm}

procedure TLoginShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmSistema );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
          {$ifdef DOS_CAPAS}
          ZetaSQLBroker.FreeAll;
          {$endif}
     end;
     inherited DoCloseAll;
end;

procedure TLoginShell.DoOpenAll;
begin
     try
         inherited DoOpenAll;
         Global.Conectar;
      except
         on Error : Exception do
         begin
              DoCloseAll;
         end;
      end;
end;

procedure TLoginShell.Ejecuta(const sEjecutable, sCredenciales: String);
begin
     zetaClientTools.ExecuteFile(sEjecutable, ZetaServerTools.Encrypt(scredenciales), VACIO, 1);
end;

procedure TLoginShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmSistema := TdmSistema.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmCliente := TdmCliente.Create( Self );
     inherited;
end;

procedure TLoginShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmCliente.Free;
     dmCatalogos.Free;
     dmConsultas.Free;
     dmRecursos.Free;
     dmTablas.Free;
     dmSistema.Free;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
end;

function TLoginShell.GetEjecutable: String;
begin
     Result := ListaParametros.Values[ P_EJECUTABLE ];
end;


function TLoginShell.SalidaCredenciales: String;
var
   iUsuario : Integer;
   sUsuario, sContrasena : String;
begin
     result := VACIO;
     with dmSistema do
     begin
          iUsuario := dmCliente.Usuario;
          with cdsUsuarios do
          begin
               Refrescar;

               if ( Locate('US_CODIGO',iUsuario, []) ) then
               begin
                    sUsuario := FieldByName('US_CORTO').AsString;
                    sContrasena := FieldByName('US_PASSWRD').AsString;
               end;
          end;
          Result:= Format('%s|%s|%s', [ sUsuario, ZetaWinApiTools.GetComputerName, sContrasena ]);
     end;
end;

end.
