unit FExportacionArchivosXML;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, DB, Provider, DBClient,
  ZetaClientDataSet, IBODataset, IB_Components, DBTables, ADODB,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  cxButtons, FileCtrl, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxGroupBox, cxClasses, dxSkinsForm, cxRadioGroup, ZetaDialogo, ZetaRegistryCliente,
  ImgList, cxShellBrowserDialog;

type
  TFGeneraXMLBDFirebird = class(TForm)
    Panel1: TPanel;
    ibEmpresa: TIB_Connection;
    qryMaster: TIBOQuery;
    qryCambios: TIBOQuery;
    ClientDataSet1: TZetaClientDataSet;
    prvUnico: TDataSetProvider;
    DataSource1: TDataSource;
    ClientDataSet2: TClientDataSet;
    SaveDialog1: TSaveDialog;
    qryTablas: TIBOQuery;
    DevEx_SkinController: TdxSkinController;
    XmlExpr: TcxImageList;
    xmlExprLarge: TcxImageList;
    cxGroupBox2: TcxGroupBox;
    GroupBox3: TcxGroupBox;
    cxGroupBox3: TcxGroupBox;
    Label8: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    GroupBox2: TcxGroupBox;
    Label7: TLabel;
    cxGroupBox4: TcxGroupBox;
    Clave: TEdit;
    Label5: TLabel;
    Label4: TLabel;
    Usuario: TEdit;
    BD: TEdit;
    Label6: TLabel;
    Label3: TLabel;
    Servidor: TEdit;
    cxGroupBox5: TcxGroupBox;
    memBitacora: TMemo;
    cxGroupBox6: TcxGroupBox;
    btnSalir: TcxButton;
    OpenDialog: TOpenDialog;
    btnExportar: TcxButton;
    cxOpenDialog: TcxShellBrowserDialog;
    BitacoraSeek: TcxButton;
    ArchBitacora: TcxTextEdit;
    btnBitacora: TcxButton;
    Label9: TLabel;
    BuscarDirectorioDatos: TcxButton;
    btnConectar: TcxButton;
    DirectorioDatos: TcxTextEdit;
    procedure btnConectarClick(Sender: TObject);

    procedure btnExportarClick(Sender: TObject);
    procedure BuscarDirectorioDatosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure memBitacoraKeyPress(Sender: TObject; var Key: Char);
    procedure btnSalirClick(Sender: TObject);
    procedure btnBitacoraClick(Sender: TObject);
    procedure BitacoraSeekClick(Sender: TObject);
    procedure cxPathClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    lFModoBatch: Boolean;

    procedure leerNombresTablas;
    procedure leerTablas(sNombreTabla :string);
    function GetCampos( const sTabla: String ): String;
    procedure HabilitaControles( lEnabled : Boolean );
    procedure BorrarColumnasBlob;
    procedure verificaScrollBarsBitacora(sLinea : string);

    procedure desHabilitaControlesBaseDeDatos;
    procedure habilitaControlesBaseDeDatos;
    procedure GrabaBitacora(oMemo: TMemo; const sArchivo: String; sMensaje:string);
    procedure setNombreLogs;

    function stGetCompName:string;



  public
    { Public declarations }
  protected
    function buscaDirectorio(const Value:string):string;
  end;

var
  FGeneraXMLBDFirebird: TFGeneraXMLBDFirebird;

implementation

uses Math,
     ZetaClientTools,
     ZAsciiTools,
     ZetaMessages,
     ZetaCommonTools;

{$R *.dfm}

procedure TFGeneraXMLBDFirebird.BorrarColumnasBlob;
begin
    //ClientDataSet1.SaveToFile( Archivo.Text, dfXML );
end;

procedure TFGeneraXMLBDFirebird.btnConectarClick(Sender: TObject);
begin
     try
        with ibEmpresa do
        begin
             if Connected = True then begin
                Connected := False;
                HabilitaControles( false );
             end
             else begin
                Connected := False;
                UserName  := Usuario.Text;
                Password  := Clave.Text;
                DataBaseName := Servidor.Text + ':' + BD.Text;
                Connected := True;
                HabilitaControles( Connected );
             end;
        end;
        qryMaster.IB_Connection := ibEmpresa;
        qryMaster.IB_Transaction := ibEmpresa.DefaultTransaction;
        qryCambios.IB_Connection := ibEmpresa;
        qryCambios.IB_Transaction := ibEmpresa.DefaultTransaction;
     except
           on Error: Exception do
           begin
               ZetaDialogo.ZWarning( 'Problema de conexión', 'Verifique los datos de conexión a la base de datos.', 0, mbOK );
           end;
     end;
end;

function TFGeneraXMLBDFirebird.GetCampos(const sTabla: String): String;
var
   i: Integer;
   sCampo: String;
begin
     Result := '';
     with qryCambios do
     begin
          Active := False;
          SQL.Text := Format( 'select * from %s', [ sTabla ] );
          Active := True;
          for i := 0 to FieldCount - 1 do
          begin
               sCampo := Fields[i].FieldName;
               if ( Result <> '' ) then
                  Result := Result + ',' + sCampo
               else
                   Result := sCampo;
          end;
          Active := False;
     end;

end;

procedure TFGeneraXMLBDFirebird.HabilitaControles(lEnabled: Boolean);
begin
     if lEnabled then begin
         desHabilitaControlesBaseDeDatos;
         GroupBox2.Enabled := true;
         DirectorioDatos.SetFocus;
         btnConectar.OptionsImage.ImageIndex := 16;
         btnConectar.Caption := 'Desconectar';
         btnConectar.Hint := 'Desconectar la base de datos';
         btnConectar.Width:= 95;
         btnExportar.Enabled := true;
     end
     else begin
         habilitaControlesBaseDeDatos;
         GroupBox2.Enabled := false;
         cxGroupBox4.Enabled := true;
         Servidor.SetFocus;
         btnConectar.Caption := 'Conectar';
         btnConectar.Hint := 'Conectar a la base de datos';
         btnConectar.OptionsImage.ImageIndex := 15;
         btnConectar.Width:= 83;
         btnExportar.Enabled := false;
     end;
end;

procedure TFGeneraXMLBDFirebird.btnExportarClick(Sender: TObject);
begin
     if not DirectoryExists( directorioDatos.Text ) then
        ZetaDialogo.zInformation( 'Captura Incorrecta', 'La ruta de salida indicada no existe.', 0 )
     else begin
        BorrarColumnasBlob;
        leerNombresTablas;
        ZetaDialogo.zInformation( 'Confirmación', 'Los archivos XML se generaron con éxito.', 0 );
        memBitacora.Enabled:= true;
        memBitacora.Color := clWindow;
     end;
end;

procedure TFGeneraXMLBDFirebird.leerTablas( sNombreTabla:string );
var
   iRecsOut: Integer;
   sTabla: String;
begin
     try
        with qryMaster do
        begin
             Active := False;
             IndexDefs.Clear;
             FieldDefs.Clear;
             SQL.Text := Format( 'select %s from %s', [ GetCampos( sNombreTabla ), sNombreTabla ] );;
             Active := True;
        end;
        savedialog1.FileName := sNombreTabla;

        iRecsOut := 0;
        ClientDataSet1.Data := prvUnico.GetRecords( -1, iRecsOut, Ord(grMetaData)+Ord(grReset));
        sNombreTabla := DirectorioDatos.Text +'\'+ sNombreTabla + '.xml';
        ClientDataSet1.SaveToFile( sNombreTabla, dfXML );
        qryMaster.Active := False;
     except
           on Error: Exception do
           begin
                ShowMessage( Error.Message );
           end;
     end;

end;

procedure TFGeneraXMLBDFirebird.leerNombresTablas;
var
   sCampo: String;
begin
     with qryTablas do
     begin
          Active := False;
          SQL.Text :=  'select T.RDB$RELATION_NAME  AS TABLE_NAME from RDB$RELATIONS T where( ( T.RDB$SYSTEM_FLAG = 0 ) or '+
                        '( T.RDB$SYSTEM_FLAG is NULL ) )and ( T.RDB$DBKEY_LENGTH = 8 ) and '+
                        '( ( select COUNT(*) from RDB$VIEW_RELATIONS V '+
                        'where ( V.RDB$VIEW_NAME = T.RDB$RELATION_NAME ) ) = 0 )  order by T.RDB$RELATION_NAME';
          Active := True;
          memBitacora.Lines.Add( 'Inicio de exportación de archivos de '+ExtractFileName(BD.Text)  +' '+ dateTimeToStr(now) );
          self.GrabaBitacora( memBitacora, ArchBitacora.Text ,'Inicio de exportación de archivos desde '+ExtractFileName(BD.Text)  +' ' + dateTimeToStr(now) );
          while not eof do
          begin
               sCampo := Fields[0].Value;
               memBitacora.Lines.Add(DirectorioDatos.Text +'\'+ sCampo + '.xml');
               verificaScrollBarsBitacora(DirectorioDatos.Text +'\'+ sCampo + '.xml');
               self.GrabaBitacora( memBitacora, ArchBitacora.Text, DirectorioDatos.Text +'\'+ sCampo + '.xml' );
               leerTablas(sCampo);
               Next;
          end;
          memBitacora.Lines.Add( 'Fin de exportación de archivos de '+ExtractFileName(BD.Text)  +' '+ dateTimeToStr(now) );
          verificaScrollBarsBitacora('Fin de exportación de archivos de '+ExtractFileName(BD.Text)  +' ' + dateTimeToStr(now));
          self.GrabaBitacora( memBitacora, ArchBitacora.Text,'Fin de exportación de archivos. - '+ExtractFileName(BD.Text)  +' ' + dateTimeToStr(now)   );

          Active := False;
     end;
end;

procedure TFGeneraXMLBDFirebird.BuscarDirectorioDatosClick(
  Sender: TObject);
begin
    DirectorioDatos.Text := BuscaDirectorio( DirectorioDatos.Text );
end;

function TFGeneraXMLBDFirebird.buscaDirectorio(
  const Value: string): string;
var
   sDirectory: String;
begin
     sDirectory := Value;
     if SelectDirectory( sDirectory, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
        Result := sDirectory
     else
         Result := Value;

end;

procedure TFGeneraXMLBDFirebird.FormCreate(Sender: TObject);
begin
    InitClientRegistry;
    setNombreLogs;
end;

procedure TFGeneraXMLBDFirebird.memBitacoraKeyPress(Sender: TObject;
  var Key: Char);
begin
     memBitacora.SelStart := memBitacora.GetTextLen-1;
     memBitacora.SelLength := 0;
     memBitacora.SelText := '';
end;

procedure TFGeneraXMLBDFirebird.btnSalirClick(Sender: TObject);
begin
    with ibEmpresa do begin
    if Connected = True then
       Connected := False;
    end;
    FGeneraXMLBDFirebird.close;
end;

procedure TFGeneraXMLBDFirebird.btnBitacoraClick(Sender: TObject);
begin
    cxOpenDialog.Title := 'Seleccione la ruta de salida.';
    cxOpenDialog.Path := ExtractFilePath( ArchBitacora.Text + '\');
    if cxOpenDialog.Execute then
      ArchBitacora.Text := cxOpenDialog.Path;
end;

procedure TFGeneraXMLBDFirebird.BitacoraSeekClick(Sender: TObject);
begin
     OpenDialog.Title := 'Seleccione archivo de base de datos Firebird';
     BD.Text := ZetaClientTools.AbreDialogo( OpenDialog, BD.Text, '*.gdb' );
end;

procedure TFGeneraXMLBDFirebird.desHabilitaControlesBaseDeDatos;
begin
    // Habilita o deshabilita controles
    servidor.Enabled :=false;
    BD.Enabled :=false;
    usuario.Enabled :=false;
    clave.Enabled :=false;
    BitacoraSeek.Enabled := false;
    DirectorioDatos.Enabled := true;
    BuscarDirectorioDatos.Enabled := true;
    memBitacora.Color := clMenuBar;
    memBitacora.Enabled := true;
    btnBitacora.Enabled := true;
    ArchBitacora.Enabled := true;

    //Manejo de colores
    DirectorioDatos.Style.Color := clWindow;
    servidor.Color := clMenuBar;
    BD.Color := clMenuBar;
    usuario.Color := clMenuBar;
    clave.Color := clMenuBar;

end;

procedure TFGeneraXMLBDFirebird.habilitaControlesBaseDeDatos;
begin
    // Habilita o deshabilita controles
    servidor.Enabled :=true;
    BD.Enabled :=true;
    usuario.Enabled :=true;
    clave.Enabled :=true;
    BitacoraSeek.Enabled:= true;
    DirectorioDatos.Enabled := false;
    BuscarDirectorioDatos.Enabled := false;
    memBitacora.Clear;
    memBitacora.Enabled := false;
    btnBitacora.Enabled := false;
    ArchBitacora.Enabled := false;

    //Manejo de colores
    DirectorioDatos.Style.Color := clMenuBar;
    servidor.Color := clWindow;
    BD.Color := clWindow;
    usuario.Color := clWindow;
    clave.Color := clWindow;
    memBitacora.Color := clMenuBar;
end;

procedure TFGeneraXMLBDFirebird.GrabaBitacora(oMemo: TMemo; const sArchivo: String; sMensaje:string );
var
     fArchivo : TextFile;
begin
     try
        AssignFile(fArchivo, sArchivo);
        {$I-}
        Append(fArchivo);
        if IOResult <> 0 then
          Rewrite(fArchivo);
        {$I+}
        if IOResult = 0 then begin
          Writeln(fArchivo, sMensaje);
          CloseFile(fArchivo);
        end;
     except
        on Error : Exception do
        begin
            GrabaBitacora(memBitacora, sArchivo,'Error al crear archivo ' + sArchivo);
            ZetaDialogo.ZError( 'Error al exportar.', 'Se produjo un erro al exportar ' + sArchivo, 0);
        end;
     end;
end;

procedure TFGeneraXMLBDFirebird.cxPathClick(Sender: TObject);
begin
    cxOpenDialog.Title := 'Seleccione la ruta de salida.';
    cxOpenDialog.Path := ExtractFilePath( DirectorioDatos.Text + '\');
    if cxOpenDialog.Execute then
    begin
      DirectorioDatos.Text := cxOpenDialog.Path;
    end;
end;

procedure TFGeneraXMLBDFirebird.setNombreLogs;
var
   sNombre: String;
begin
     sNombre :='ExportacionArchivosXML.LOG';
     ArchBitacora.Text := ZetaMessages.SetFileNameDefaultPath( sNombre );
end;

procedure TFGeneraXMLBDFirebird.verificaScrollBarsBitacora(sLinea : string);
begin
    if (Abs(memBitacora.Font.Height) + 4) * memBitacora.Lines.Count > memBitacora.Height then
        memBitacora.ScrollBars := ssVertical;
end;



function TFGeneraXMLBDFirebird.stGetCompName: string;
var
    sName: array[0..255] of char;
    sS: Cardinal;
begin
    sS := SizeOf(sName);
    GetComputerName(sName, sS);
    Result := StrPas(sName);
end;

procedure TFGeneraXMLBDFirebird.FormShow(Sender: TObject);
begin
     Servidor.Text := stGetCompName;
end;

end.
