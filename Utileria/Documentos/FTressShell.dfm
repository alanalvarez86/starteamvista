inherited TressShell: TTressShell
  Left = 161
  Top = 136
  Width = 949
  Height = 607
  Caption = 'Relacionar Documentos'
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 530
    Width = 933
    Panels = <
      item
        Width = 80
      end
      item
        Width = 150
      end
      item
        Width = 55
      end
      item
        Width = 40
      end
      item
        Width = 110
      end>
  end
  inherited PageControl: TPageControl
    Top = 75
    Width = 933
    Height = 30
    ActivePage = TabErrores
    Align = alTop
    Visible = False
    inherited TabBitacora: TTabSheet
      inherited MemBitacora: TMemo
        Width = 925
      end
      inherited PanelBottom: TPanel
        Top = -39
        Width = 925
        inherited BitacoraSeek: TSpeedButton
          Left = 649
          Top = 22
          Visible = False
        end
        inherited LblGuardarBIT: TLabel
          Left = 13
          Width = 42
          Caption = 'Bit'#225'cora:'
        end
        inherited ArchBitacora: TEdit
          Left = 57
          Width = 387
          Color = clInfoBk
          ReadOnly = True
        end
        object btnAbrirBitacora: TBitBtn
          Left = 448
          Top = 5
          Width = 37
          Height = 25
          Hint = 'Abrir Bit'#225'cora'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnAbrirBitacoraClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            5555555FFFFFFFFFF5555550000000000555557777777777F5555550FFFFFFFF
            0555557F5FFFF557F5555550F0000FFF0555557F77775557F5555550FFFFFFFF
            0555557F5FFFFFF7F5555550F000000F0555557F77777757F5555550FFFFFFFF
            0555557F5FFFFFF7F5555550F000000F0555557F77777757F5555550FFFFFFFF
            0555557F5FFF5557F5555550F000FFFF0555557F77755FF7F5555550FFFFF000
            0555557F5FF5777755555550F00FF0F05555557F77557F7555555550FFFFF005
            5555557FFFFF7755555555500000005555555577777775555555555555555555
            5555555555555555555555555555555555555555555555555555}
          NumGlyphs = 2
        end
      end
    end
    inherited TabErrores: TTabSheet
      inherited Panel1: TPanel
        Top = -39
        Width = 925
        inherited ErroresSeek: TSpeedButton
          Left = 513
          Visible = False
        end
        inherited LblGuardarERR: TLabel
          Left = 13
          Width = 42
          Caption = 'Bit'#225'cora:'
        end
        inherited ArchErrores: TEdit
          Left = 57
          Width = 387
          Color = clInfoBk
          ReadOnly = True
        end
        object btnVerErrores: TBitBtn
          Left = 448
          Top = 5
          Width = 37
          Height = 25
          Hint = 'Abrir Bit'#225'cora de Errores'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnVerErroresClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            5555555FFFFFFFFFF5555550000000000555557777777777F5555550FFFFFFFF
            0555557F5FFFF557F5555550F0000FFF0555557F77775557F5555550FFFFFFFF
            0555557F5FFFFFF7F5555550F000000F0555557F77777757F5555550FFFFFFFF
            0555557F5FFFFFF7F5555550F000000F0555557F77777757F5555550FFFFFFFF
            0555557F5FFF5557F5555550F000FFFF0555557F77755FF7F5555550FFFFF000
            0555557F5FF5777755555550F00FF0F05555557F77557F7555555550FFFFF005
            5555557FFFFF7755555555500000005555555577777775555555555555555555
            5555555555555555555555555555555555555555555555555555}
          NumGlyphs = 2
        end
      end
      inherited MemErrores: TMemo
        Width = 925
        Height = 314
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 41
    Width = 933
    inherited BtnProcesar: TBitBtn
      Width = 140
      Hint = 'Revisi'#243'n Documentos '
      Caption = '&Revisar Documentos'
    end
    inherited BtnDetener: TBitBtn
      Left = 156
      Visible = False
    end
    inherited BtnSalir: TBitBtn
      Left = 816
      Width = 110
    end
  end
  inherited PanelEncabezado: TPanel
    Width = 933
    Height = 41
    Visible = False
    inherited LblArchivo: TLabel
      Left = 52
      Width = 58
      Caption = 'Folder Ra'#237'z:'
    end
    inherited ArchivoSeek: TSpeedButton
      Left = 460
      Top = 6
    end
    inherited ArchOrigen: TEdit
      Left = 115
      Width = 345
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 105
    Width = 933
    Height = 58
    Align = alTop
    TabOrder = 4
    DesignSize = (
      933
      58)
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 321
      Height = 56
      Align = alLeft
      Caption = ' Renumerar Empleados Asignados '
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 24
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label3: TLabel
        Left = 117
        Top = 24
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object cbTipo: TComboBox
        Left = 34
        Top = 21
        Width = 79
        Height = 21
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 0
        Text = 'Sumar'
        Items.Strings = (
          'Sumar'
          'Restar')
      end
      object txtRenumera: TZetaNumero
        Left = 149
        Top = 21
        Width = 64
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
        OnChange = txtRenumeraChange
        OnExit = txtRenumeraExit
      end
      object btnRenumerar: TBitBtn
        Left = 222
        Top = 18
        Width = 91
        Height = 26
        Hint = 'Procesar Archivo de Origen'
        Caption = 'Renumerar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = btnRenumerarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555000000
          000055555F77777777775555000FFFFFFFF0555F777F5FFFF55755000F0F0000
          FFF05F777F7F77775557000F0F0FFFFFFFF0777F7F7F5FFFFFF70F0F0F0F0000
          00F07F7F7F7F777777570F0F0F0FFFFFFFF07F7F7F7F5FFFFFF70F0F0F0F0000
          00F07F7F7F7F777777570F0F0F0FFFFFFFF07F7F7F7F5FFF55570F0F0F0F000F
          FFF07F7F7F7F77755FF70F0F0F0FFFFF00007F7F7F7F5FF577770F0F0F0F00FF
          0F057F7F7F7F77557F750F0F0F0FFFFF00557F7F7F7FFFFF77550F0F0F000000
          05557F7F7F77777775550F0F0000000555557F7F7777777555550F0000000555
          55557F7777777555555500000005555555557777777555555555}
        NumGlyphs = 2
      end
    end
    object GroupBox3: TGroupBox
      Left = 322
      Top = 1
      Width = 375
      Height = 56
      Align = alLeft
      Caption = ' Marcar Documentos Borrados  '
      TabOrder = 1
      object btnBorrar: TBitBtn
        Left = 19
        Top = 18
        Width = 102
        Height = 26
        Hint = 'Borrar Documento Seleccionado'
        Caption = 'Borrar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = btnBorrarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
          555557777F777555F55500000000555055557777777755F75555005500055055
          555577F5777F57555555005550055555555577FF577F5FF55555500550050055
          5555577FF77577FF555555005050110555555577F757777FF555555505099910
          555555FF75777777FF555005550999910555577F5F77777775F5500505509990
          3055577F75F77777575F55005055090B030555775755777575755555555550B0
          B03055555F555757575755550555550B0B335555755555757555555555555550
          BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
          50BB555555555555575F555555555555550B5555555555555575}
        NumGlyphs = 2
      end
      object btnBorrarTodos: TBitBtn
        Left = 243
        Top = 18
        Width = 102
        Height = 26
        Hint = 'Borrar Todos Los Documentos'
        Caption = 'Borrar Todos'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = btnBorrarTodosClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
      object btnNoBorrar: TBitBtn
        Left = 131
        Top = 18
        Width = 102
        Height = 26
        Hint = 'Quitar marca de Borrado del Documento Seleccionado'
        Caption = 'NO Borrar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnNoBorrarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
          300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
          330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
          333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
          339977FF777777773377000BFB03333333337773FF733333333F333000333333
          3300333777333333337733333333333333003333333333333377333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
      end
    end
    object btnVerDoc: TBitBtn
      Left = 816
      Top = 18
      Width = 110
      Height = 26
      Hint = 'Ver Documento'
      Anchors = [akRight, akBottom]
      Caption = 'Ver Documento'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnVerDocClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333FF3333333333333C0C333333333333F777F3333333333CC0F0C3
        333333333777377F33333333C30F0F0C333333337F737377F333333C00FFF0F0
        C33333F7773337377F333CC0FFFFFF0F0C3337773F33337377F3C30F0FFFFFF0
        F0C37F7373F33337377F00FFF0FFFFFF0F0C7733373F333373770FFFFF0FFFFF
        F0F073F33373F333373730FFFFF0FFFFFF03373F33373F333F73330FFFFF0FFF
        00333373F33373FF77333330FFFFF000333333373F333777333333330FFF0333
        3333333373FF7333333333333000333333333333377733333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
    end
  end
  object GroupBox1: TGroupBox [5]
    Left = 0
    Top = 163
    Width = 933
    Height = 367
    Align = alClient
    TabOrder = 5
    object GridDocumentos: TZetaDBGrid
      Left = 2
      Top = 15
      Width = 929
      Height = 307
      Align = alClient
      DataSource = dsHuerfanos
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = GridDocumentosDrawColumnCell
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'DO_BORRA'
          ReadOnly = True
          Title.Caption = 'Borrar'
          Width = 35
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'CB_CODIGO'
          ReadOnly = True
          Title.Caption = '# Empleado Asignado'
          Width = 116
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'DO_TIPO'
          ReadOnly = True
          Title.Caption = 'Tipo'
          Width = 87
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'DO_NOMBRE'
          ReadOnly = True
          Title.Caption = 'Nombre Documento'
          Width = 299
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NUEVO_EMP'
          Title.Caption = 'Nuevo # Empleado'
          Width = 103
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'NOM_EMP'
          ReadOnly = True
          Title.Caption = 'Nombre'
          Width = 280
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 2
      Top = 322
      Width = 929
      Height = 43
      Align = alBottom
      TabOrder = 1
      DesignSize = (
        929
        43)
      object AplicaCambios: TBitBtn
        Left = 805
        Top = 8
        Width = 116
        Height = 29
        Hint = 'Aceptar y Escribir Datos'
        Anchors = [akRight, akBottom]
        Caption = '&Aplicar Cambios'
        ModalResult = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = AplicaCambiosClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object btnExcluir: TBitBtn
        Left = 15
        Top = 10
        Width = 75
        Height = 25
        Hint = 'Excluir Registro'
        Caption = 'Excluir'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = btnExcluirClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF33333333333330003333333333333777333333333333
          300033FFFFFF3333377739999993333333333777777F3333333F399999933333
          3300377777733333337733333333333333003333333333333377333333333333
          3333333333333333333F333333333333330033333F33333333773333C3333333
          330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
          333333377F33333333FF3333C333333330003333733333333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
      end
      object btnRevertir: TBitBtn
        Left = 96
        Top = 10
        Width = 75
        Height = 25
        Hint = 'Revertir Cambios Realizados'
        Caption = '&Revertir'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = btnRevertirClick
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777777777777777777777777777777777777777777777777777777777777777
          7777777777777778477777444447777748777744447777777477774447777777
          7477774474777777747777477744777748777777777744448777777777777777
          7777777777777777777777777777777777777777777777777777}
      end
    end
  end
  inherited LogoffTimer: TTimer
    Left = 520
    Top = 8
  end
  inherited ActionList: TActionList
    Left = 554
    Top = 12
  end
  inherited ShellMenu: TMainMenu
    Left = 656
    Top = 8
    inherited Archivo1: TMenuItem
      inherited CancelarProceso1: TMenuItem
        Caption = '&Revisar Documentos'
      end
      inherited CancelarProceso2: TMenuItem
        Visible = False
      end
    end
  end
  inherited OpenDialog: TOpenDialog
    Left = 750
    Top = 2
  end
  inherited ArbolImages: TImageList
    Left = 612
    Top = 16
  end
  inherited Email: TNMSMTP
    Left = 704
    Top = 0
  end
  object dsHuerfanos: TDataSource
    OnDataChange = dsHuerfanosDataChange
    Left = 672
    Top = 224
  end
  object cdsHuerfanos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cdsHuerfanosAfterInsert
    AlCrearCampos = cdsHuerfanosAlCrearCampos
    Left = 664
    Top = 256
  end
  object Hdbc: THdbc
    Attributes.Strings = (
      '')
    ConnectionPooling = cpDefault
    Version = '5.07'
    Left = 576
    Top = 48
  end
  object OEDataSet: TOEDataSet
    hDbc = Hdbc
    hStmt.SkipByPosition = True
    hStmt.SkipByCursor = True
    hStmt.StringTrimming = stTrimNone
    hStmt.Target.Target = (
      'OE5.07'
      ''
      ''
      ''
      '')
    SQL = 
      'select NO_HORAS from NOMINA where'#13#10'( PE_YEAR = 2000 ) and'#13#10'( PE_' +
      'TIPO = 1 ) and'#13#10'( PE_NUMERO = 1 ) and'#13#10'( CB_CODIGO = 30 )'
    ParamCheck = False
    Params = <>
    Cached = True
    CachedUpdates = True
    EmptyFieldToNull = True
    AutoCalcFields = False
    Left = 608
    Top = 48
  end
  object cdsDocSimple: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 728
    Top = 256
  end
end
