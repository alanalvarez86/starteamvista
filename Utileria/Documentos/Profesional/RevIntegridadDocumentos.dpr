program RevIntegridadDocumentos;

uses
  Forms,
  ComObj,
  ActiveX,
  ZetaClientTools,
  DInterfase in '..\DInterfase.pas' {dmInterfase: TDataModule},
  ZBaseImportaShell in '..\..\..\Tools\ZBaseImportaShell.pas',
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in '..\..\..\Tools\ZetaSplash.pas' {SplashScreen},
  FTressShell in '..\FTressShell.pas' {TressShell};

{$R *.RES}

procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     MuestraSplash;
     Application.Title := 'Revisa integridad de documentos de empleados ';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
        begin
             if Login( False ) then
             begin
                  CierraSplash;
                  Show;
                  Update;
                  BeforeRun;
                  Application.Run;
             end
             else
             begin
                  CierraSplash;
                  Free;
             end;
        end;
end.
