program RevIntegridadDocumentos;

uses
  Forms,
  ComObj,
  ActiveX,
  ZetaClientTools,
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBaseImportaShell in '..\..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  DCliente in '..\..\Datamodules\DCliente.pas';

{$R *.RES}

procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     MuestraSplash;
     Application.Title := 'Revisa integridad de documentos de empleados';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
        begin
             if Login( False ) then
             begin
                  CierraSplash;
                  Show;
                  Update;
                  BeforeRun;
                  Application.Run;
             end
             else
             begin
                  CierraSplash;
                  Free;
             end;
        end;
end.
