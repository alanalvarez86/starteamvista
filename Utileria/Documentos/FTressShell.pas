unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell, Psock, NMsmtp, ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, ZBaseConsulta, ZetaClientDataSet,ZetaKeyLookup,
  Grids, DBGrids, ZetaDBGrid, DB, DBClient, ODSI,
  {$ifdef DOS_CAPAS}
  DZetaServerProvider,
  ZetaSQLBroker,
  {$endif}
  OCL;

type
  TTressShell = class(TBaseImportaShell)
    btnAbrirBitacora: TBitBtn;
    btnVerErrores: TBitBtn;
    Panel2: TPanel;
    dsHuerfanos: TDataSource;
    cdsHuerfanos: TZetaClientDataSet;
    GroupBox1: TGroupBox;
    GridDocumentos: TZetaDBGrid;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    cbTipo: TComboBox;
    Label3: TLabel;
    txtRenumera: TZetaNumero;
    btnRenumerar: TBitBtn;
    Hdbc: THdbc;
    OEDataSet: TOEDataSet;
    GroupBox3: TGroupBox;
    btnBorrar: TBitBtn;
    btnBorrarTodos: TBitBtn;
    btnVerDoc: TBitBtn;
    cdsDocSimple: TZetaClientDataSet;
    Panel3: TPanel;
    AplicaCambios: TBitBtn;
    btnExcluir: TBitBtn;
    btnRevertir: TBitBtn;
    btnNoBorrar: TBitBtn;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure _P_ProcesarExecute(Sender: TObject);
    procedure btnAbrirBitacoraClick(Sender: TObject);
    procedure btnVerErroresClick(Sender: TObject);
    procedure btnRenumerarClick(Sender: TObject);
    procedure AplicaCambiosClick(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
    procedure btnVerDocClick(Sender: TObject);
    procedure btnBorrarTodosClick(Sender: TObject);
    procedure cdsHuerfanosAlCrearCampos(Sender: TObject);
    procedure btnRevertirClick(Sender: TObject);
    procedure dsHuerfanosDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure GridDocumentosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnNoBorrarClick(Sender: TObject);
    procedure txtRenumeraChange(Sender: TObject);
    procedure cdsHuerfanosAfterInsert(DataSet: TDataSet);
    procedure txtRenumeraExit(Sender: TObject);
  private
    { Private declarations }
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
    procedure SetNombreLogs;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure RenumerarEmpleados;
    {$ifndef DOS_CAPAS}
    procedure Conectar;
    procedure Desconectar;
    {$endif}
    procedure EjecutaScript(Script: string);
    procedure BorrarDocumentos;
    procedure BorrarDocumento(Empleado: Integer; Tipo: string);
    procedure cdsHuerfanosCB_CODIGOChange(Sender: TField);
    procedure SetupControles;
    procedure MyMouseWheel(Sender: TObject; Shift: TShiftState;WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);

  protected
    { Protected declarations }
    procedure ActivaControles( const lProcesando: Boolean ); override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
    procedure CancelarProceso;override;
  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    FPintar:Boolean;
    {$ifdef DOS_CAPAS}
    oZetaProviderDocs: TdmZetaServerProvider;
    {$endif}
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
  end;
  TDBGridHack = class(TDBGrid);

var
  TressShell: TTressShell;

const
     K_DOC_HUERFANOS = 'select cb_codigo,DO_NOMBRE,DO_TIPO,''N'' as DO_BORRA ,0 as Nuevo_Emp,''                                        '' as Nom_Emp  from DOCUMENTO D where D.CB_CODIGO not in (select CB_CODIGO FROM COLABORA )';
     K_EMP_NOT_FOUND = 'Empleado No Encontrado';
     Q_UPDATE_DOCS   = 'Update DOCUMENTO set CB_CODIGO = %d where CB_CODIGO = %d and DO_TIPO= ''%s'' ';
     Q_DEL_DOCS      = 'DELETE FROM DOCUMENTO where CB_CODIGO = %d and DO_TIPO = ''%s'' ';
     Q_SEL_BLOB      = 'Select DO_BLOB,DO_EXT,DO_NOMBRE from DOCUMENTO where CB_CODIGO = %d and DO_TIPO = ''%s'' ';

implementation

uses DCliente, DGlobal, DSistema, DTablas,
     DRecursos, DConsultas, DProcesos,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     FCalendario,
     FileCtrl,
     ZetaDialogo,
     ZetaServerTools,
     ZetaFilesTools,Variants,
     DCatalogos;

{$R *.dfm}

{ TTressShell }

procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     oZetaProviderDocs := TdmZetaServerProvider.Create(Self);
     {$endif}
     dmSistema := TdmSistema.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     inherited;
     TDBGridHack( GridDocumentos ).OnMouseWheel:= MyMouseWheel;
     FPintar := False;

end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmCatalogos.Free;
     dmConsultas.Free;
     dmRecursos.Free;
     dmTablas.Free;
     dmSistema.Free;
     {$ifdef DOS_CAPAS}
     FreeAndNil(oZetaProviderDocs);
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}

end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     SetupControles;
     btnRevertir.Enabled := False; 
end;


procedure TTressShell.MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
     Handled := True;
     with GridDocumentos.DataSource.DataSet do
     begin
          if WheelDelta > 0 then
             Prior
          else
              Next;
     end;
end;


procedure TTressShell.DoOpenAll;
begin
     try
         inherited DoOpenAll;
         Global.Conectar;
         with dmCliente do
         begin
              //InitActivosSistema;
              //InitActivosAsistencia;
              //InitActivosEmpleado;
              //InitActivosPeriodo;
         end;
         InitBitacora;
         CargaSistemaActivos;
      except
         on Error : Exception do
         begin
              ReportaErrorDialogo( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message );
              DoCloseAll;
         end;
      end;
      ActivaControles( FALSE );
      if NOT cdsHuerfanos.IsEmpty THEN
         cdsHuerfanos.EmptyDataSet;

end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmSistema );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
          {$ifdef DOS_CAPAS}
          ZetaSQLBroker.FreeAll;
          {$endif}
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.DoProcesar;
begin
     //No implementado
end;


procedure TTressShell.ActivaControles(const lProcesando: Boolean);
begin
     inherited;
      with dmCliente do
     begin
          //lblCalsificDef.Enabled := EmpresaAbierta and ( not lProcesando );
          //ClasificDef.Enabled := EmpresaAbierta and ( not lProcesando );
          if lProcesando and ( BtnDetener.CanFocus ) then
             BtnDetener.SetFocus
          else if EmpresaAbierta and ( ArchOrigen.CanFocus ) then
             ArchOrigen.SetFocus
          else if BtnSalir.CanFocus then
             BtnSalir.SetFocus;
     end;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     SetNombreLogs;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre: String;
begin
     with dmCliente do
     begin
          sNombre :='IMP_DOCS';
          ArchBitacora.Text := ZetaMessages.SetFileNameDefaultPath( 'BIT-' + sNombre +'_'+ IntToStr(Usuario)+'_'+ FechaToStr(FechaDefault) +'_'+ HoraAsStr( Time )+ '.LOG' );
          ArchErrores.Text := ZetaMessages.SetFileNameDefaultPath( 'ERR-' + sNombre +'_'+ IntToStr(Usuario) +'_'+ FechaToStr(FechaDefault) +'_'+ HoraAsStr( Time )+ '.LOG' );
     end;
end;

procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
         //dmInterfase.HandleProcessEnd( WParam, LParam );
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }

function TTressShell.FormaActiva: TBaseConsulta;
begin
      Result:= Nil;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo,iPeriodoBorrado: Integer);
begin
     //
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
      //
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
    //
end;

procedure TTressShell.ChangeTimerInfo;
begin
     //
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     //
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     //
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     //
end;

procedure TTressShell.RefrescaEmpleado;
begin
     //
end;

procedure TTressShell.RefrescaIMSS;
begin
      //
end;



procedure TTressShell.ReinitPeriodo;
begin
     //
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
      //
end;

procedure TTressShell.SetBloqueo;
begin
      //
end;


procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     //
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
    //No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     //No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     //No Implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     //No Implementar
end;

procedure TTressShell.CancelarProceso;
begin
     //No Implementarlo;
end;

procedure TTressShell.ArchivoSeekClick(Sender: TObject);
var sDirectory:string;
begin
     //inherited;
     if FileCtrl.SelectDirectory( sDirectory, [ ], 0 ) then
        ArchOrigen.Text := sDirectory;
end;

procedure TTressShell._P_ProcesarExecute(Sender: TObject);
begin
     //inherited;
     cdsHuerfanos.Data := dmConsultas.ServerConsultas.GetQueryGralTodos(dmCliente.Empresa, K_DOC_HUERFANOS );
     dsHuerfanos.DataSet := cdsHuerfanos;
     if cdsHuerfanos.IsEmpty then
     begin
          ZInformation(Self.Caption ,'Todos los Documentos est�n correctamente relacionados a los Empleados en Sistema TRESS',0);
     end
     else
         btnRevertir.Enabled := True;
end;

procedure TTressShell.btnAbrirBitacoraClick(Sender: TObject);
begin
     inherited;
     //No Implementar
end;

procedure TTressShell.btnVerErroresClick(Sender: TObject);
begin
     inherited;
     //No implementar
end;



procedure TTressShell.btnRenumerarClick(Sender: TObject);
begin
     inherited;
     if ZConfirm(Self.Caption ,'Se renumeran TODOS los Documentos con los valores indicados � Favor de Confirmar ? ',0,mbOk)then
     begin
          RenumerarEmpleados;
     end;
end;

procedure TTressShell.RenumerarEmpleados;
var
   NuevoCodigo:Integer;
begin
     with cdsHuerfanos do
     begin
          First;
          while Not Eof do
          begin
               Edit;
               if cbTipo.ItemIndex = 0 then
                  NuevoCodigo := FieldByName('CB_CODIGO').AsInteger + txtRenumera.ValorEntero
               else
                   NuevoCodigo := FieldByName('CB_CODIGO').AsInteger - txtRenumera.ValorEntero;

               FieldByName('NUEVO_EMP').AsInteger := NuevoCodigo;
               Post;
               Next;
          end
     end;
end;

procedure TTressShell.AplicaCambiosClick(Sender: TObject);

   procedure ActualizaDocumentos (NuevoEmp,EmpleadoAnterior:Integer;TipoDoc:String);
   begin
        EjecutaScript(Format( Q_UPDATE_DOCS,[NuevoEmp,EmpleadoAnterior,TipoDoc]));
   end;
begin
     inherited;
     if ZConfirm(Self.Caption ,'Los cambios realizados seran aplicados al Servidor, � Desea Aplicarlos ?',0,mbOk)then
     begin
          with cdsHuerfanos do
          begin
               First;
               while Not Eof do
               begin
                    if( ( FieldByName('NOM_EMP').AsString <> K_EMP_NOT_FOUND)  and ( FieldByName('NUEVO_EMP').AsInteger <> 0 ) and ( not zStrToBool( FieldByName('DO_BORRA').AsString ) ) )then
                    begin
                         ActualizaDocumentos(FieldByName('NUEVO_EMP').AsInteger,FieldByName('CB_CODIGO').AsInteger,FieldByName('DO_TIPO').AsString );
                    end;
                    Next;
               end;
          end;
          BorrarDocumentos;
          _P_ProcesarExecute(Self);
          SetupControles; 
     end;
end;

procedure TTressShell.BorrarDocumento(Empleado:Integer;Tipo:string);
begin
     EjecutaScript(Format(Q_DEL_DOCS,[Empleado,Tipo]));
end;


procedure TTressShell.BorrarDocumentos;
begin
      with cdsHuerfanos do
      begin
           First;
           while not eof do
           begin
                if zStrToBool( FieldByName('DO_BORRA').AsString)then
                     BorrarDocumento (FieldByName('CB_CODIGO').AsInteger,FieldByName('DO_TIPO').AsString);
                Next;
           end;
      end;
end;

{$ifndef DOS_CAPAS}
procedure TTressShell.Conectar;
var
   oCursor: TCursor;
   sDataBase,sServer:string;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with HDBC do
        begin
             Connected := False;
             Driver := 'SQL Server';
             with Attributes do
             begin
                  Clear;
                  with dmCliente do
                  begin
                       ZetaServerTools.GetServerDatabase( Empresa[P_ALIAS], sServer, sDatabase );
                       Add( Format( 'Server=%s', [ sServer ] ) );     // Este dato tambi�n deber� viajar
                       Add( Format( 'Database=%s', [ sDataBase ] ) );
                       Add( Format( 'Language=%s', [ 'us_english' ] ) );
                       UserName := Empresa[P_USER_NAME];
                       Password :=  ZetaServerTools.Decrypt( Empresa[ P_PASSWORD ] );//Empresa[P_PASSWORD];
                       Connected := True;
                  end;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.Desconectar;
begin
     with HDBC do
     begin
          Connected := False;
     end;
end;
{$endif}

procedure TTressShell.EjecutaScript(Script:string);
{$ifdef DOS_CAPAS}
        procedure EjecutarIBProvider;
        var
           FQuery :TZetaCursor;
        begin
             with oZetaProviderDocs do
             begin
                  EmpresaActiva := dmCliente.Empresa;
                  FQuery := CreateQuery(Script);
                  try
                     EmpiezaTransaccion;
                     try
                        Ejecuta(FQuery);
                        TerminaTransaccion( TRUE );
                     except
                           on Error: Exception do
                           begin
                                RollBackTransaccion;
                           end;
                     end;
                  finally
                         FreeAndNil(FQuery);
                  end;
             end;
        end;
{$endif}
begin
{$ifdef DOS_CAPAS}
     EjecutarIBProvider;
{$ELSE}
     Conectar;
     try
        with OEDataset do
        begin
             try
                Active := False;
                SQL := Script;
                Prepare;
                ExecSQL;
             except
                   on Error: Exception do
                   begin
                        Application.HandleException( Error );
                        Active := False;
                   end;
             end;
        end;
     finally
            Desconectar;
     end;
{$ENDIF}
end;


procedure TTressShell.btnBorrarClick(Sender: TObject);
begin
     inherited;
     if ZConfirm(Self.Caption ,'� Desea Borrar el Documento Seleccionado ?',0,mbOk)then
     begin
          with cdsHuerfanos do
          begin
               Edit;
               FieldByName('DO_BORRA').AsString := K_GLOBAL_SI;
               Post;
          end;
     end;
end;

procedure TTressShell.btnVerDocClick(Sender: TObject);
var
   Empleado:Integer;
begin
     inherited;
     with cdsHuerfanos do
     begin
          Empleado := FieldByName('CB_CODIGO').AsInteger;
          cdsDocSimple.Data := dmConsultas.ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( Q_SEL_BLOB ,[ Empleado , FieldByName('DO_TIPO').AsString ] ) );
     end;
     ZetaFilesTools.AbreDocumento( cdsDocSimple, 'DO_BLOB', 'DO_EXT' );
end;

procedure TTressShell.btnBorrarTodosClick(Sender: TObject);
begin
     inherited;
     if ZConfirm(Self.Caption ,'� Est�s Seguro de Borrar TODOS los Documentos Mostrados ?',0,mbOk)then
     begin
          with cdsHuerfanos do
          begin
               First;
               while not eof do
               begin
                    Edit;
                    FieldByName('DO_BORRA').AsString := K_GLOBAL_SI;
                    Post;
                    Next;
               end;
          end;
     end;
end;

procedure TTressShell.cdsHuerfanosAlCrearCampos(Sender: TObject);
begin
     inherited;
     cdsHuerfanos.FieldByName('NUEVO_EMP').OnChange := cdsHuerfanosCB_CODIGOChange;

end;

procedure TTressShell.cdsHuerfanosCB_CODIGOChange(Sender: TField);
var
   NombreEmpleado:string;
begin
     with cdsHuerfanos do
     begin
          NombreEmpleado := dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( Sender.AsInteger) );
          if NombreEmpleado = VACIO then
          begin
               FieldByName('NOM_EMP').AsString := K_EMP_NOT_FOUND
          end
          else
              FieldByName('NOM_EMP').AsString :=  NombreEmpleado;
     end;
end;


procedure TTressShell.btnRevertirClick(Sender: TObject);
begin
     inherited;
     with cdsHuerfanos do
     begin
          DisableControls;
          UndoLastChange( True );
          EnableControls;
     end;
end;

procedure TTressShell.SetupControles;
var
   bHayDatos:Boolean;
begin
     bHayDatos := not cdsHuerfanos.IsEmpty;

     btnRenumerar.Enabled := bHayDatos;
     btnBorrar.Enabled := bHayDatos;
     btnBorrarTodos.Enabled := bHayDatos;
     btnExcluir.Enabled := bHayDatos;
     //btnRevertir.Enabled := bHayDatos;
     btnVerDoc.Enabled := bHayDatos;
     AplicaCambios.Enabled := bHayDatos;
     btnNoBorrar.Enabled := bHayDatos;
end;

procedure TTressShell.dsHuerfanosDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     SetupControles;
     if Field = nil then
     begin
          if not cdsHuerfanos.IsEmpty then
          begin
               if zStrToBool( cdsHuerfanos.FieldByName( 'DO_BORRA' ).AsString) then
               begin
                    btnNoBorrar.Enabled := True;
                    btnBorrar.Enabled := False;
               end
               else
               begin
                    btnNoBorrar.Enabled := False;
                    btnBorrar.Enabled := True;
               end;
          end;
     end;
end;


procedure TTressShell.btnExcluirClick(Sender: TObject);
begin
     inherited;
     cdsHuerfanos.Delete;
end;

procedure TTressShell.GridDocumentosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     with GridDocumentos, Canvas, Brush do
     begin
         if UpperCase(Column.Field.FieldName) = 'DO_BORRA' then
         begin
              FPintar := zStrToBool(Column.Field.AsString);
         end;
         if FPintar then
         begin
              Font.Color := clRed;
              Font.Style :=  [fsStrikeOut];

         end;
          DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
end;

procedure TTressShell.btnNoBorrarClick(Sender: TObject);
begin
     inherited;
     with cdsHuerfanos do
     begin
          Edit;
          FieldByName('DO_BORRA').AsString := K_GLOBAL_NO;
          Post;
     end;
end;

procedure TTressShell.txtRenumeraChange(Sender: TObject);
begin
     inherited;
    
end;
//Impedir agregar registros
procedure TTressShell.cdsHuerfanosAfterInsert(DataSet: TDataSet);
begin
     inherited;
     DataSet.Delete;
end;

procedure TTressShell.txtRenumeraExit(Sender: TObject);
begin
     inherited;
      btnRenumerar.Enabled := ( txtRenumera.Valor > 0 ) and ( not cdsHuerfanos.IsEmpty)  ;
end;

end.
