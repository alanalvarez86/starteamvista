program ValidacionIDSE;

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule};

{$R *.res}
{$R WindowsXP.res}
{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\..\Traducciones\Spanish.RES}
begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Validación IDSE';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            Show;
            Update;
            //WindowState := wsMaximized;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            Free;
       end;
  end;
end.
