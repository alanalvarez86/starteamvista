unit FEditBitacora_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, ZetaDBTextBox, StdCtrls,
  DBCtrls, Buttons, FBaseEditBitacora_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxNavigator, cxDBNavigator, cxButtons,
  cxContainer, cxEdit, ImgList, cxTextEdit, cxMemo, cxDBEdit;

type
  TFormaBitacora_DevEx = class(TBaseEditBitacora_DevEx)
    EmpleadoLBL: TLabel;
    Label3: TLabel;
    CB_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormaBitacora_DevEx: TFormaBitacora_DevEx;

implementation

uses ZetaCommonClasses;

{$R *.dfm}


procedure TFormaBitacora_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H60652_Bitacora;
end;

end.
