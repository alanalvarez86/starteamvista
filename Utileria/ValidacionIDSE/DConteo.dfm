object dmConteo: TdmConteo
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 280
  Width = 629
  object cdsNivel5: TZetaClientDataSet
    Tag = 5
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsNivel0CalcFields
    Left = 304
    Top = 28
  end
  object cdsNivel4: TZetaClientDataSet
    Tag = 4
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsNivel0CalcFields
    Left = 248
    Top = 28
  end
  object cdsNivel3: TZetaClientDataSet
    Tag = 3
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsNivel0CalcFields
    Left = 192
    Top = 28
  end
  object cdsNivel2: TZetaClientDataSet
    Tag = 2
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsNivel0CalcFields
    Left = 136
    Top = 28
  end
  object cdsNivel1: TZetaClientDataSet
    Tag = 1
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsNivel0CalcFields
    Left = 80
    Top = 28
  end
  object cdsNivel0: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsNivel0CalcFields
    Left = 24
    Top = 28
  end
end
