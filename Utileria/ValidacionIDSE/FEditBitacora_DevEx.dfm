inherited FormaBitacora_DevEx: TFormaBitacora_DevEx
  Left = 544
  Top = 282
  Height = 350
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EmpleadoLBL: TLabel [0]
    Left = 26
    Top = 93
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empleado:'
  end
  inherited PanelBotones: TPanel
    Top = 276
    inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
      Width = 118
    end
  end
  inherited Panel1: TPanel
    inherited LabFecMov: TLabel
      Top = 111
    end
    inherited B_FEC_MOD: TZetaDBTextBox
      Top = 111
    end
    inherited MensajeLBL: TLabel
      Top = 129
    end
    inherited BI_TEXTO: TZetaDBTextBox
      Top = 129
    end
    inherited Label1: TLabel
      Left = 37
    end
    object Label3: TLabel
      Left = 26
      Top = 93
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object CB_CODIGO: TZetaDBTextBox
      Left = 79
      Top = 92
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'CB_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited BI_DATA: TcxDBMemo
    Style.IsFontAssigned = True
    Height = 123
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
