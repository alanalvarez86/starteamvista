unit ZArbolTress;

interface

uses SysUtils, ComCtrls, Dialogs, DB,Controls,
     ZArbolTools, ZNavBarTools, cxtreeview;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
procedure MuestraArbolOriginal( oArbol: TTreeView );
//DevEx
procedure CreaNavBarUsuario ( oNavBarMgr: TNavBarMgr; const sFileName: String ; Arbolitos: array of TcxTreeView ; ArbolitoUsuario : TcxTreeView);
procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
procedure GetDatosNavBarUsuario( oNavBarMgr: TNavBarMgr; DataSet: TDataSet; const sFileName: String );
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
procedure CreaArbolDefaultSistema( oArbolMgr: TArbolMgr );

implementation

uses DGlobal,
     DSistema,
     DCliente,
     ZetaDespConsulta,
     ZGlobalTress,
     ZetaCommonTools,
     ZAccesosMgr,
     ZetaTipoEntidad, 
	 cxEdit;

const
     K_EMPLEADOS                        = 1;
     K_EMP_DATOS                        = 2;
     K_EMP_DATOS_IDENTIFICA             = 3;
     K_EMP_DATOS_CONTRATA               = 4;
     K_EMP_DATOS_AREA                   = 5;
     K_EMP_DATOS_PERCEP                 = 6;
     K_EMP_DATOS_OTROS                  = 7;
     K_EMP_DATOS_ADICION                = 8;
     K_EMP_CURRICULUM                   = 9;
     K_EMP_CURR_PERSONALES              = 10;
     K_EMP_CURR_FOTO                    = 11;
     K_EMP_CURR_PARIENTES               = 12;
     K_EMP_CURR_EXPERIENCIA             = 13;
     K_EMP_CURR_CURSOS                  = 14;
     K_EMP_CURR_PUESTOS                 = 15;
     K_EMP_EXPEDIENTE                   = 16;
     K_EMP_EXP_KARDEX                   = 17;
     K_EMP_EXP_VACA                     = 18;
     K_EMP_EXP_INCA                     = 19;
     K_EMP_EXP_PERM                     = 20;

     K_EMP_EXP_CURSOS_TOMADOS           = 21;
     K_EMP_EXP_PAGOS_IMSS               = 22;
     K_EMP_NOMINA                       = 23;
     K_EMP_NOM_AHORROS                  = 24;
     K_EMP_NOM_PRESTAMOS                = 25;
     K_EMP_NOM_ACUMULA                  = 26;
     K_EMP_NOM_HISTORIAL                = 27;
     K_EMP_REGISTRO                     = 28;
     K_EMP_PROCESOS                     = 43;
     {******** Asistencia **********}
     K_ASISTENCIA                       = 53;
     K_ASIS_DATOS                       = 54;
     K_ASIS_DATOS_TARJETA_DIARIA        = 55;
     K_ASIS_DATOS_PRENOMINA             = 56;
     K_ASIS_DATOS_CALENDARIO            = 57;
     K_ASIS_REGISTRO                    = 58;
     K_ASIS_PROCESOS                    = 61;
     {******** N�mina **********}
     K_NOMINA                           = 69;
     K_NOM_DATOS                        = 70;
     K_NOM_DATOS_TOTALES                = 71;
     K_NOM_DATOS_DIAS_HORAS             = 72;
     K_NOM_DATOS_MONTOS                 = 73;
     K_NOM_DATOS_PRENOMINA              = 74;
     K_NOM_DATOS_CLASIFI                = 75;
     K_NOM_EXCEPCIONES                  = 76;
     K_NOM_EXCEP_DIAS_HORAS             = 77;
     K_NOM_EXCEP_MONTOS                 = 78;
     K_NOM_REGISTRO                     = 79;
     K_NOM_REG_NOMINA_NUEVA             = 80;
     K_NOM_REG_BORRA_ACTIVA             = 81;
     K_NOM_REG_EXCEP_DIA_HORA           = 82;
     K_NOM_REG_EXCEP_MONTO              = 83;
     K_NOM_REG_LIQUIDACION              = 84;
     K_NOM_PROCESOS                     = 85;
     K_NOM_PROC_CALCULAR                = 86;
     K_NOM_PROC_AFECTAR                 = 87;
     K_NOM_PROC_FOLIAR_RECIBOS          = 88;
     K_NOM_PROC_IMP_LISTADO             = 89;
     K_NOM_PROC_IMP_RECIBOS             = 90;
     K_NOM_PROC_CALC_POLIZA             = 91;
     K_NOM_PROC_IMP_POLIZA              = 92;
     K_NOM_PROC_EXP_POLIZA              = 93;
     K_NOM_PROC_DESAFECTAR              = 94;
     K_NOM_PROC_LIMPIA_ACUM             = 95;
     K_EMP_REG_CIERRE_VACA              = 96;
     K_CAT_NOMINA_PARAMETROS            = 97;
     K_NOM_PROC_IMP_MOVIMIEN            = 98;
     K_NOM_PROC_EXP_MOVIMIEN            = 99;
     K_NOM_PROC_PAGAR_FUERA             = 100;
     K_NOM_PROC_CANCELA_PASADA          = 101;
     K_NOM_PROC_LIQUIDA_GLOBAL          = 102;
     K_NOM_PROC_NETO_BRUTO              = 103;
     K_NOM_PROC_RASTREO                 = 104;
     K_NOM_ANUALES                      = 105;
     K_NOM_ANUAL_PERIODOS               = 106;
     K_NOM_ANUAL_CALC_AGUINALDO         = 107;
     K_NOM_ANUAL_CALC_AHORRO            = 108;
     K_NOM_ANUAL_CALC_PTU               = 109;
     K_NOM_ANUAL_CREDITO                = 110;
     K_NOM_ANUAL_COMPARA_ISPT           = 111;
     {******** Pagos IMSS **********}
     K_IMSS                             = 112;
     K_IMSS_DATOS                       = 113;
     K_IMSS_DATOS_TOTALES               = 114;
     K_IMSS_DATOS_EMP_MEN               = 115;
     K_IMSS_DATOS_EMP_BIM               = 116;
     K_IMSS_HISTORIA                    = 117;
     K_IMSS_HIST_EMP_MEN                = 118;
     K_IMSS_HIST_EMP_BIM                = 119;
     K_IMSS_REGISTRO                    = 120;
     K_IMSS_REG_NUEVO                   = 121;
     K_IMSS_REG_BORRAR_ACTIVO           = 122;
     K_IMSS_PROCESOS                    = 123;
     K_IMSS_PROC_CALCULAR               = 124;
     K_IMSS_PROC_RECARGOS               = 125;
     K_IMSS_PROC_REVISAR_SUA            = 126;
     K_IMSS_PROC_EXP_SUA                = 127;
     K_IMSS_PROC_CALC_RIESGO            = 128;
     {******** Consultas **********}
     K_CONSULTAS                        = 129;
     K_CONS_REPORTES                    = 130;
     K_CONS_CONTEO_PERSONAL             = 131;
     K_CONS_GRAFICA_CONTEO              = 132;
     K_CONS_SQL                         = 133;
     {******** Cat�logos **********}
     K_CATALOGOS                        = 134;
     K_CAT_CONTRATACION                 = 135;
     K_CAT_CONT_PUESTOS                 = 136;
     K_CAT_CONT_CLASIFI                 = 137;
     K_CAT_CONT_PER_FIJAS               = 138;
     K_CAT_CONT_TURNOS                  = 139;
     K_CAT_CONT_HORARIOS                = 140;
     K_CAT_CONT_PRESTA                  = 141;
     K_CAT_CONT_CONTRATO                = 142;
     K_CAT_NOMINA                       = 143;
     K_CAT_NOMINA_CONCEPTOS             = 144;
     K_CAT_NOMINA_PERIODOS              = 145;
     K_CAT_NOMINA_FOLIOS                = 146;
     K_CAT_GENERALES                    = 147;
     K_CAT_GRALES_PATRONALES            = 148;
     K_CAT_GRALES_FEST_GRAL             = 149;
     K_CAT_GRALES_FEST_TURNO            = 150;
     K_CAT_GRALES_CONDICIONES           = 151;
     K_CAT_GRALES_EVENTOS               = 152;
     K_CAT_CAPACITACION                 = 153;
     K_CAT_CAPA_CURSOS                  = 154;
     K_CAT_CAPA_MAESTROS                = 155;
     K_CAT_CAPA_MATRIZ_CURSO            = 156;
     K_CAT_CAPA_MATRIZ_PUESTO           = 157;
     K_CAT_CAPA_CALENDARIO              = 158;
     K_CAT_CONFIGURACION                = 159;
     K_CAT_CONFI_GLOBALES               = 160;
     K_CONS_BITACORA                    = 161;
     K_CAT_CONFI_DICCIONARIO            = 162;
     K_CONS_PROCESOS                    = 163; { GA 22/Feb/2000:ANTES ERA UN HUECO 163 }
     {******** Tablas **********}
     K_TABLAS                           = 164;
     K_TAB_PERSONALES                   = 165;
     K_TAB_PER_EDOCIVIL                 = 166;
     K_TAB_PER_VIVE_EN                  = 167;
     K_TAB_PER_VIVE_CON                 = 168;
     K_TAB_PER_ESTUDIOS                 = 169;
     K_TAB_PER_TRANSPORTE               = 170;
     K_TAB_PER_ESTADOS                  = 171;
     K_TAB_AREAS                        = 172;
     K_TAB_AREAS_NIVEL1                 = 173;
     K_TAB_AREAS_NIVEL2                 = 174;
     K_TAB_AREAS_NIVEL3                 = 175;
     K_TAB_AREAS_NIVEL4                 = 176;
     K_TAB_AREAS_NIVEL5                 = 177;
     K_TAB_AREAS_NIVEL6                 = 178;
     K_TAB_AREAS_NIVEL7                 = 179;
     K_TAB_AREAS_NIVEL8                 = 180;
     K_TAB_AREAS_NIVEL9                 = 181;
     K_TAB_ADICIONALES                  = 182;
     K_TAB_ADICION_TABLA1               = 183;
     K_TAB_ADICION_TABLA2               = 184;
     K_TAB_ADICION_TABLA3               = 185;
     K_TAB_ADICION_TABLA4               = 186;
     K_TAB_ADICION_TABLA5               = 426;
     K_TAB_ADICION_TABLA6               = 427;
     K_TAB_ADICION_TABLA7               = 428;
     K_TAB_ADICION_TABLA8               = 429;
     K_TAB_ADICION_TABLA9               = 430;
     K_TAB_ADICION_TABLA10              = 431;
     K_TAB_ADICION_TABLA11              = 432;
     K_TAB_ADICION_TABLA12              = 433;
     K_TAB_ADICION_TABLA13              = 434;
     K_TAB_ADICION_TABLA14              = 435;
     K_TAB_HISTORIAL                    = 187;
     K_TAB_HIST_TIPO_KARDEX             = 188;
     K_TAB_HIST_MOT_BAJA                = 189;
     K_TAB_HIST_INCIDEN                 = 190;
     K_CAT_CAPA_TIPO_CURSO              = 191;
     K_TAB_NOMINA                       = 192;
     K_TAB_NOM_TIPO_AHORRO              = 193;
     K_TAB_NOM_TIPO_PRESTA              = 194;
     K_TAB_NOM_MONEDAS                  = 195;
     K_TAB_OFICIALES                    = 196;
     K_TAB_OFI_SAL_MINIMOS              = 197;
     K_TAB_OFI_CUOTAS_IMSS              = 198;
     K_TAB_OFI_ISPT_NUMERICAS           = 199;
     K_TAB_OFI_GRADOS_RIESGO            = 200;
     K_TAB_COLONIAS                     = 293;
     K_TAB_VAL_UMA                      = 716;
     {******** Sistema **********}
     K_SISTEMA                          = 201;
     K_SIST_DATOS                       = 202;
     K_SIST_DATOS_EMPRESAS              = 203;
     K_SIST_DATOS_USUARIOS              = 204;
     K_SIST_DATOS_GRUPOS                = 205;
     K_SIST_DATOS_POLL_PENDIENTES       = 206;
     K_SIST_REGISTRO                    = 207;
     K_SIST_REG_EMPRESA_NUEVA           = 208;
     K_SIST_REG_BORRAR_EMPRESA          = 209;
     K_SIST_PROCESOS                    = 210;
     K_NOM_PROC_CANCELAR_VACA           = 211;
     K_SIST_PROC_BORRAR_BAJAS           = 212;
     K_SIST_PROC_BORRAR_TARJETAS        = 213;
     { *********** Nuevos ***********}
     K_NOM_PROC_COPIAR_NOMINA           = 214;
     K_EMP_EXP_CURSOS_PROGRAMADOS       = 215;
     K_EMP_REG_CAMBIO_MULTIPLE          = 216;
     {********** Reportes ***********}
     K_REPORTES_EMPLEADOS               = 217;
     K_REPORTES_ASISTENCIA              = 218;
     K_REPORTES_NOMINA                  = 219;
     K_REPORTES_IMSS                    = 220;
     K_REPORTES_CONSULTAS               = 221;
     K_REPORTES_CATALOGOS               = 222;
     K_REPORTES_TABLAS                  = 223;
     { *********** Nuevos ***********}
     K_IMSS_PROC_CALC_TASA_INFO         = 224;
     { PENDIENTE ??? }
     //K_CHECADAS_EMPLEADO  = 225;
     K_NOM_PROC_RECALC_ACUMULADOS       = 226;
     K_EMP_PROC_TRANSFERENCIA           = 227;
     K_NOM_ANUAL_PAGA_AGUINALDO         = 228;
     { PENDIENTE: Repetido con 108? }
     K_NOM_ANUAL_CALC_AHORROS           = 229;
     K_NOM_ANUAL_CIERRE_AHORRO          = 230;
     K_NOM_ANUAL_CIERRE_PRESTA          = 231;
     K_SIST_PROC_BORRA_NOMINAS          = 232;
     K_NOM_ANUAL_IMP_AGUINALDO          = 233;
     K_NOM_ANUAL_IMP_AHORRO             = 234;
     K_NOM_ANUAL_DIF_COMPARA            = 235;
     K_NOM_ANUAL_IMP_COMPARA            = 236;
     K_NOM_ANUAL_IMP_CREDITO            = 237;
     K_NOM_ANUAL_IMP_PTU                = 238;
     K_NOM_ANUAL_PAGA_PTU               = 239;
     {************ Cafeter�a **********}
     K_EMP_CAFETERIA                    = 240;
     K_EMP_CAFE_COMIDAS_DIA             = 241;
     K_EMP_CAFE_COMIDAS_PERIODO         = 242;
     K_EMP_CAFE_INVITACIONES            = 243;
     K_CAT_CAFETERIA                    = 244;
     K_CAT_CAFE_REGLAS                  = 245;
     K_CAT_CAFE_INVITADORES             = 246;
     K_REPORTES_CAFETERIA               = 247;
     K_REPORTES_SUPERVISORES            = 248;
     K_SIST_DATOS_IMPRESORAS            = 249;
     K_NOM_EXCEP_GLOBALES               = 250;
     {************ Accesos **********}
     K_CAT_ACCESOS_REGLAS               = 291;

     { ********* Supervisores ****** }
     K_NOM_REG_EXCEP_GLOBAL             = 251;
     K_SUPER_AJUSTAR_ASISTENCIA         = 252;
     K_SUPER_PERMISOS                   = 253;
     K_SUPER_KARDEX                     = 254;
     K_SUPER_CONSULTAR_PRENOMINA        = 255;
     K_SUPER_BITACORA                   = 256;
     K_SUPER_ASIGNAR_EMPLEADOS          = 257;
     K_SUPER_DATOS_EMPLEADOS            = 258;
     { PENDIENTE: Repetida 251 }
     K_NOM_REG_EXCEP_MONTO_GLOBAL       = 259;
     K_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL= 260;
     { HUECO 261 }
     K_SUPER_AUTORIZACION_COLECTIVA     = 262;
     K_SUPER_TIPO_KARDEX                = 263;
     K_NOM_PROC_CALC_DIFERENCIAS        = 264;
     K_ASIS_DATOS_AUTORIZACIONES        = 265;
     K_TAB_HIST_AUT_EXTRAS              = 266;
     K_TAB_OFI_TIPOS_CAMBIO             = 267;
     K_REPORTES_CONFIDENCIALES          = 268;
     K_NOM_REG_PAGO_RECIBOS             = 269;
     K_NOM_PROC_RE_FOLIAR               = 270;
     K_NOM_PROC_IMP_PAGO_RECIBO         = 271;
     K_SUPER_VER_BAJAS                  = 272;
     K_SIST_NIVEL0                      = 282;
     K_CAT_HERRAMIENTAS                 = 283;
     K_CAT_HERR_TOOLS                   = 284;
     K_TAB_HERR_TALLAS                  = 285;
     K_TAB_HERR_MOTTOOL                 = 286;
     K_EMP_EXP_TOOLS                    = 287;
     K_CLASIFI_CURSO                    = 288;
     K_CONS_ORGANIGRAMA                 = 289;
     K_ASIS_DATOS_CLASIFI_TEMP          = 292;
     K_CAT_CAPA_AULAS 	                 = 294;
     K_CAT_CAPA_PREREQUISITOS           = 299;
     {******** Capacitacion *******}
     K_CAPACITACION 		                  = 471;
     K_EMP_EXP_SESIONES                 = 290;
     K_CAPA_RESERVAS 	                  = 296;

     {************** PRESUPUESTOS ***************}
     K_PRESUPUESTOS                     = 555;
     K_PRE_PROCESOS                     = 559;
     K_PRE_CONFIGURACION                = 556;
     K_PRE_CONFIGURACION_ALTAS          = 557;
     K_PRE_ESCENARIOS                   = 560; // (JB) Escenarios de presupuestos T1060 CR1872
     K_PRE_CONFIGURACION_SUPUESTO_RH    = 558;
     
	 K_FULL_ARBOL_TRESS                 =9999;



     //MA( Ver 2.7 ) NOTA: Cuando se agreguen constantes que corresponden a nodos tipo FOLDER dentro de el
     //�rbol de formas en Tress, es necesario que el valor de la constante sea igual al valor del derecho
     //de acceso en ZAccesosTress.pas.
     //                   Ej.- K_CAPACITACION = D_CAPACITACION        ( Sugerencia para versiones futuras ) 


function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma := TRUE;
     Result.Caption := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma := FALSE;
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;
//DevEx: Funcion para definir Grupos del NavBar
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     case iNodo of
          // Presupuestos
          K_PRESUPUESTOS            : Result := Folder( 'Presupuestos', iNodo );
          K_PRE_CONFIGURACION       : Result := Folder( 'Configuraci�n', iNodo );
          K_PRE_CONFIGURACION_ALTAS : Result := Forma( 'Contrataciones', efcPreAltasMultiples );
          K_PRE_ESCENARIOS          : Result := Forma( 'Escenarios', efcPreEscenarios ); // (JB) Escenarios de presupuestos T1060 CR1872
          K_PRE_CONFIGURACION_SUPUESTO_RH : Result := Forma( 'Supuestos de Personal', efcPreSupuestosRH );

          // Empleados
          K_EMPLEADOS               : Result := Folder( 'Empleados', iNodo );
          K_EMP_DATOS               : Result := Folder( 'Datos', iNodo );
          K_EMP_DATOS_CONTRATA      : Result := Forma( 'Contrataci�n', efcEmpContratacion );
          K_EMP_DATOS_AREA          : Result := Forma( 'Area', efcEmpArea );
          K_EMP_DATOS_PERCEP        : Result := Forma( 'Percepciones', efcEmpPercepcion );

          K_EMP_EXPEDIENTE          : Result := Folder( 'Expediente', iNodo );
          K_EMP_EXP_KARDEX          : Result := Forma( 'Kardex', efcHisKardex );

          K_EMP_NOMINA              : Result := Folder( 'N�mina', iNodo );
          K_EMP_NOM_AHORROS         : Result := Forma( 'Ahorros', efcHisAhorros );
          K_EMP_NOM_PRESTAMOS       : Result := Forma( 'Pr�stamos', efcHisPrestamos );
          K_EMP_NOM_ACUMULA         : Result := Forma( 'Acumulados', efcHisAcumulados );

          // N�minas
          K_NOMINA                  : Result := Folder( 'N�minas', iNodo );
          K_NOM_DATOS               : Result := Folder( 'Datos', iNodo );
          K_NOM_DATOS_TOTALES       : Result := Forma( 'Totales', efcNomTotales );
          K_NOM_DATOS_MONTOS        : Result := Forma( 'Montos', efcNomMontos );
          K_NOM_DATOS_CLASIFI       : Result := Forma( 'Clasificaci�n', efcNomDatosClasifi );
          K_NOM_EXCEPCIONES         : Result := Folder( 'Excepciones', iNodo );
          K_NOM_EXCEP_DIAS_HORAS    : Result := Forma( 'D�as/Horas', efcNomExcepciones );
          K_NOM_EXCEP_MONTOS        : Result := Forma( 'Montos', efcNomExcepMontos );
          K_NOM_EXCEP_GLOBALES      : Result := Forma( 'Globales', efcNomExcepGlobales );

          // Consultas
          K_CONSULTAS               : Result := Folder( 'Consultas', iNodo );
          K_CONS_REPORTES           : Result := Forma( 'Reportes', efcReportes );
          K_CONS_SQL                : Result := Forma( 'SQL', efcQueryGral );

          // Cat�logos
          K_CATALOGOS               : Result := Folder( 'Cat�logos', iNodo );
          K_CAT_CONTRATACION        : Result := Folder( 'Contrataci�n', iNodo );
          K_CAT_CONT_PUESTOS        : Result := Forma( 'Puestos', efcCatPuestos );
          K_CAT_CONT_CLASIFI        : Result := Forma( 'Clasificaciones', efcCatClasifi );
          K_CAT_CONT_PER_FIJAS      : Result := Forma( 'Percepciones Fijas', efcCatOtrasPer );
          K_CAT_CONT_TURNOS         : Result := Forma( 'Turnos', efcCatTurnos );
          K_CAT_CONT_HORARIOS       : Result := Forma( 'Horarios', efcCatHorarios );
          K_CAT_CONT_PRESTA         : Result := Forma( 'Prestaciones', efcCatPrestaciones );
          K_CAT_CONT_CONTRATO       : Result := Forma( 'Tipo de Contrato', efcCatContratos );

          K_CAT_NOMINA              : Result := Folder( 'N�mina', iNodo );
          K_CAT_NOMINA_CONCEPTOS    : Result := Forma( 'Conceptos', efcCatConceptos );
          K_CAT_NOMINA_PERIODOS     : Result := Forma( 'Per�odos', efcCatPeriodos );
          K_CAT_NOMINA_PARAMETROS   : Result := Forma( 'Par�metros', efcNomParams );

          K_CAT_GENERALES           : Result := Folder( 'Generales', iNodo );
          K_CAT_GRALES_PATRONALES   : Result := Forma( 'Registros Patronales', efcCatRPatron );
          K_CAT_GRALES_CONDICIONES  : Result := Forma( 'Condiciones', efcCatCondiciones );
          K_CAT_GRALES_EVENTOS      : Result := Forma( 'Cambios y Recortes', efcCatEventos );

          K_CAT_CONFIGURACION       : Result := Folder( 'Configuraci�n', iNodo );
          K_CAT_CONFI_GLOBALES      : Result := Forma( 'Globales de Empresa', efcSistGlobales );
          K_CONS_BITACORA           : Result := Forma( 'Bit�cora', efcSistBitacora );
          K_CONS_PROCESOS           : Result := Forma( 'Procesos', efcSistProcesos );

          //Tablas
          K_TABLAS                  : Result := Folder( 'Tablas', iNodo );
          K_TAB_AREAS               : Result := Folder( 'Areas', iNodo );
          K_TAB_AREAS_NIVEL1        : Result := Forma( Global.NombreNivel( 1 ), efcTONivel1 );
          K_TAB_AREAS_NIVEL2        : Result := Forma( Global.NombreNivel( 2 ), efcTONivel2 );
          K_TAB_AREAS_NIVEL3        : Result := Forma( Global.NombreNivel( 3 ), efcTONivel3 );
          K_TAB_AREAS_NIVEL4        : Result := Forma( Global.NombreNivel( 4 ), efcTONivel4 );
          K_TAB_AREAS_NIVEL5        : Result := Forma( Global.NombreNivel( 5 ), efcTONivel5 );
          K_TAB_AREAS_NIVEL6        : Result := Forma( Global.NombreNivel( 6 ), efcTONivel6 );
          K_TAB_AREAS_NIVEL7        : Result := Forma( Global.NombreNivel( 7 ), efcTONivel7 );
          K_TAB_AREAS_NIVEL8        : Result := Forma( Global.NombreNivel( 8 ), efcTONivel8 );
          K_TAB_AREAS_NIVEL9        : Result := Forma( Global.NombreNivel( 9 ), efcTONivel9 );

          K_TAB_NOMINA              : Result := Folder( 'N�mina', iNodo );
          K_TAB_NOM_TIPO_AHORRO     : Result := Forma( 'Tipo de Ahorro', efcTOTAhorro );
          K_TAB_NOM_TIPO_PRESTA     : Result := Forma( 'Tipo de Pr�stamo', efcTOTPresta );

          K_TAB_OFICIALES           : Result := Folder( 'Oficiales', iNodo );
          K_TAB_OFI_SAL_MINIMOS     : Result := Forma( 'Salarios M�nimos', efcSistSalMin );
          K_TAB_OFI_CUOTAS_IMSS     : Result := Forma( 'Cuotas de IMSS', efcSistLeyIMSS );
          K_TAB_OFI_ISPT_NUMERICAS  : Result := Forma( 'ISPT y Num�ricas', efcSistNumerica );
          K_TAB_OFI_GRADOS_RIESGO   : Result := Forma( 'Grados de Riesgo', efcSistRiesgo );
          K_TAB_OFI_TIPOS_CAMBIO    : Result := Forma( 'Tipos de Cambios', efcTipoCambio );
          K_TAB_VAL_UMA             : Result := Forma( 'Unidad de Medida y Actualizaci�n' , efcUma ) ;

        else
           Result := Folder( '', 0 );
     end;
end;

procedure CreaArbolDefault( oArbolMgr : TArbolMgr );
var
   i, iNiveles: Integer;

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
     if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then
        NodoNivel( 2, iNodo );
end;

begin  // CreaArbolDefault

     // ************ Presupuestos *****************
     NodoNivel( 0, K_PRESUPUESTOS );
      NodoNivel( 1, K_PRE_CONFIGURACION );
       NodoNivel( 2, K_CAT_NOMINA_PERIODOS );
       NodoNivel( 2, K_CAT_NOMINA_CONCEPTOS );
       NodoNivel( 2, K_CAT_NOMINA_PARAMETROS );
       NodoNivel( 2, K_PRE_CONFIGURACION_ALTAS );
       NodoNivel( 2, K_CAT_GRALES_EVENTOS );
       NodoNivel( 2, K_PRE_ESCENARIOS ); // (JB) Escenarios de presupuestos T1060 CR1872
       NodoNivel( 2, K_PRE_CONFIGURACION_SUPUESTO_RH );
      NodoNivel( 1, K_PRE_PROCESOS );

    // ********* Empleados ********
    //NodoNivel( 0, K_EMPLEADOS );
      NodoNivel( 1, K_EMP_DATOS );

{$ifndef PRESUPUESTOS}
        NodoNivel( 2, K_EMP_DATOS_IDENTIFICA );
{$endif}

        NodoNivel( 2, K_EMP_DATOS_CONTRATA );
        NodoNivel( 2, K_EMP_DATOS_AREA );
        NodoNivel( 2, K_EMP_DATOS_PERCEP );

{$ifndef PRESUPUESTOS}
        NodoNivel( 2, K_EMP_DATOS_OTROS );

        if Global.HayAdicionales then
           NodoNivel( 2, K_EMP_DATOS_ADICION );

      NodoNivel( 1, K_EMP_CURRICULUM );
        NodoNivel( 2, K_EMP_CURR_PERSONALES );
        NodoNivel( 2, K_EMP_CURR_FOTO );
        NodoNivel( 2, K_EMP_CURR_PARIENTES );
        NodoNivel( 2, K_EMP_CURR_EXPERIENCIA );
        NodoNivel( 2, K_EMP_CURR_CURSOS );
        NodoNivel( 2, K_EMP_CURR_PUESTOS );
{$endif}

      NodoNivel( 1, K_EMP_EXPEDIENTE );
        NodoNivel( 2, K_EMP_EXP_KARDEX );

{$ifndef PRESUPUESTOS}
        NodoNivel( 2, K_EMP_EXP_VACA );
        NodoNivel( 2, K_EMP_EXP_INCA );
        NodoNivel( 2, K_EMP_EXP_PERM );
        NodoNivel( 2, K_EMP_EXP_CURSOS_TOMADOS );
        NodoNivel( 2, K_EMP_EXP_CURSOS_PROGRAMADOS );
        NodoNivel( 2, K_EMP_EXP_PAGOS_IMSS );
        NodoNivel( 2, K_EMP_EXP_TOOLS );
{$endif}

      NodoNivel( 1, K_EMP_NOMINA );
        NodoNivel( 2, K_EMP_NOM_AHORROS );
        NodoNivel( 2, K_EMP_NOM_PRESTAMOS );
        NodoNivel( 2, K_EMP_NOM_ACUMULA );

{$ifndef PRESUPUESTOS}
        NodoNivel( 2, K_EMP_NOM_HISTORIAL );
      NodoNivel( 1, K_EMP_CAFETERIA );
        NodoNivel( 2, K_EMP_CAFE_COMIDAS_DIA );
        NodoNivel( 2, K_EMP_CAFE_COMIDAS_PERIODO );
        NodoNivel( 2, K_EMP_CAFE_INVITACIONES );
      NodoNivel( 1, K_EMP_REGISTRO );
      NodoNivel( 1, K_EMP_PROCESOS );
    // ********* Asistencia **********
    if NodoNivel( 0, K_ASISTENCIA ) then
    begin
         if NodoNivel( 1, K_ASIS_DATOS ) then
         begin
              NodoNivel( 2, K_ASIS_DATOS_TARJETA_DIARIA );
              NodoNivel( 2, K_ASIS_DATOS_PRENOMINA );
              NodoNivel( 2, K_ASIS_DATOS_CALENDARIO );
              NodoNivel( 2, K_ASIS_DATOS_AUTORIZACIONES );
              NodoNivel( 2, K_ASIS_DATOS_CLASIFI_TEMP );
         end;
         NodoNivel( 1, K_ASIS_REGISTRO );
         NodoNivel( 1, K_ASIS_PROCESOS );
    end;
{$endif}

    // ********* N�minas ************
    //if NodoNivel( 0, K_NOMINA ) then
    begin
         if NodoNivel( 1, K_NOM_DATOS ) then
         begin
              NodoNivel( 2, K_NOM_DATOS_TOTALES );
{$ifndef PRESUPUESTOS}
              NodoNivel( 2, K_NOM_DATOS_DIAS_HORAS );
{$endif}
              NodoNivel( 2, K_NOM_DATOS_MONTOS );
{$ifndef PRESUPUESTOS}
              NodoNivel( 2, K_NOM_DATOS_PRENOMINA );
{$endif}
              NodoNivel( 2, K_NOM_DATOS_CLASIFI );
         end;
         if NodoNivel( 1, K_NOM_EXCEPCIONES ) then
         begin
              NodoNivel( 2, K_NOM_EXCEP_DIAS_HORAS );
              NodoNivel( 2, K_NOM_EXCEP_MONTOS );
              NodoNivel( 2, K_NOM_EXCEP_GLOBALES );
         end;
{$ifndef PRESUPUESTOS}
         NodoNivel( 1, K_NOM_REGISTRO );
         NodoNivel( 1, K_NOM_PROCESOS );
         NodoNivel( 1, K_NOM_ANUALES );
{$endif}
    end;
{$ifndef PRESUPUESTOS}
    // ******** Pagos IMSS ***********
    NodoNivel( 0, K_IMSS );
      NodoNivel( 1, K_IMSS_DATOS );
        NodoNivel( 2, K_IMSS_DATOS_TOTALES );
        NodoNivel( 2, K_IMSS_DATOS_EMP_MEN );
        NodoNivel( 2, K_IMSS_DATOS_EMP_BIM );
      NodoNivel( 1, K_IMSS_HISTORIA );
        NodoNivel( 2, K_IMSS_HIST_EMP_MEN );
        NodoNivel( 2, K_IMSS_HIST_EMP_BIM );
      NodoNivel( 1, K_IMSS_REGISTRO );
      NodoNivel( 1, K_IMSS_PROCESOS );
    // ******** Capacitaci�n **********
    NodoNivel( 0, K_CAPACITACION );
      NodoNivel( 1, K_EMP_EXP_SESIONES );
      NodoNivel( 1, K_CAPA_RESERVAS );
{$endif}
    // ******** Consultas ***********
    NodoNivel( 0, K_CONSULTAS );
      NodoNivel( 1, K_CONS_REPORTES );
{$ifndef PRESUPUESTOS}
      NodoNivel( 1, K_CONS_CONTEO_PERSONAL );
      NodoNivel( 1, K_CONS_ORGANIGRAMA );
      NodoNivel( 1, K_CONS_GRAFICA_CONTEO );
{$endif}
      NodoNivel( 1, K_CONS_SQL );
      NodoNivel( 1, K_CONS_BITACORA );
      NodoNivel( 1, K_CONS_PROCESOS );
    // ******* Cat�logos *************
    NodoNivel( 0, K_CATALOGOS );
      NodoNivel( 1, K_CAT_CONTRATACION );
        NodoNivel( 2, K_CAT_CONT_PUESTOS );
        NodoNivel( 2, K_CAT_CONT_CLASIFI );
        NodoNivel( 2, K_CAT_CONT_PER_FIJAS );
        NodoNivel( 2, K_CAT_CONT_TURNOS );
        NodoNivel( 2, K_CAT_CONT_HORARIOS );
        NodoNivel( 2, K_CAT_CONT_PRESTA );
        NodoNivel( 2, K_CAT_CONT_CONTRATO );
{$ifndef PRESUPUESTOS}
      NodoNivel( 1, K_CAT_NOMINA );
        NodoNivel( 2, K_CAT_NOMINA_CONCEPTOS );
        NodoNivel( 2, K_CAT_NOMINA_PARAMETROS );
        NodoNivel( 2, K_CAT_NOMINA_PERIODOS );
        NodoNivel( 2, K_CAT_NOMINA_FOLIOS );
{$endif}
      NodoNivel( 1, K_CAT_GENERALES );
        NodoNivel( 2, K_CAT_GRALES_PATRONALES );
{$ifndef PRESUPUESTOS}
        NodoNivel( 2, K_CAT_GRALES_FEST_GRAL );
        NodoNivel( 2, K_CAT_GRALES_FEST_TURNO );
{$endif}
        NodoNivel( 2, K_CAT_GRALES_CONDICIONES );
        NodoNivel( 2, K_CAT_GRALES_EVENTOS );
{$ifndef PRESUPUESTOS}
        NodoNivel( 2, K_CAT_ACCESOS_REGLAS );
      NodoNivel( 1, K_CAT_CAPACITACION );
        NodoNivel( 2, K_CAT_CAPA_TIPO_CURSO );
        NodoNivel( 2, K_CLASIFI_CURSO );
        NodoNivel( 2, K_CAT_CAPA_CURSOS );
        NodoNivel( 2, K_CAT_CAPA_MAESTROS );
        NodoNivel( 2, K_CAT_CAPA_MATRIZ_CURSO );
        NodoNivel( 2, K_CAT_CAPA_MATRIZ_PUESTO );
        NodoNivel( 2, K_CAT_CAPA_CALENDARIO );
        NodoNivel( 2, K_CAT_CAPA_AULAS );
        NodoNivel( 2, K_CAT_CAPA_PREREQUISITOS );
{$endif}
      NodoNivel( 1, K_CAT_CONFIGURACION );
        NodoNivel( 2, K_CAT_CONFI_GLOBALES );
{$ifndef PRESUPUESTOS}
        NodoNivel( 2, K_CAT_CONFI_DICCIONARIO );
      NodoNivel( 1, K_CAT_CAFETERIA );
        NodoNivel( 2, K_CAT_CAFE_REGLAS );
        NodoNivel( 2, K_CAT_CAFE_INVITADORES );
      NodoNivel( 1, K_CAT_HERRAMIENTAS );
        NodoNivel( 2, K_CAT_HERR_TOOLS );
        NodoNivel( 2, K_TAB_HERR_TALLAS );
        NodoNivel( 2, K_TAB_HERR_MOTTOOL );
{$endif}
    // ******* Tablas *************

    NodoNivel( 0, K_TABLAS );
{$ifndef PRESUPUESTOS}
      NodoNivel( 1, K_TAB_PERSONALES );
        NodoNivel( 2, K_TAB_PER_EDOCIVIL );
        NodoNivel( 2, K_TAB_PER_VIVE_EN );
        NodoNivel( 2, K_TAB_PER_VIVE_CON );
        NodoNivel( 2, K_TAB_PER_ESTUDIOS );
        NodoNivel( 2, K_TAB_PER_TRANSPORTE );
        NodoNivel( 2, K_TAB_PER_ESTADOS );
        NodoNivel( 2, K_TAB_COLONIAS );
{$endif}

      NodoNivel( 1, K_TAB_AREAS );
        iNiveles := Global.NumNiveles;
        for i := 1 to iNiveles do
            NodoNivel( 2, K_TAB_AREAS + i );
{$ifndef PRESUPUESTOS}
      NodoNivel( 1, K_TAB_ADICIONALES );
{
        for i := 1 to K_GLOBAL_TAB_MAX do
          if StrLleno( Global.AdTabla( i ) ) then
             NodoNivel( 2, K_TAB_ADICIONALES + i );
}
        AgregaNodoTabAdic( enExtra1, K_TAB_ADICION_TABLA1 );
        AgregaNodoTabAdic( enExtra2, K_TAB_ADICION_TABLA2 );
        AgregaNodoTabAdic( enExtra3, K_TAB_ADICION_TABLA3 );
        AgregaNodoTabAdic( enExtra4, K_TAB_ADICION_TABLA4 );
        AgregaNodoTabAdic( enExtra5, K_TAB_ADICION_TABLA5 );
        AgregaNodoTabAdic( enExtra6, K_TAB_ADICION_TABLA6 );
        AgregaNodoTabAdic( enExtra7, K_TAB_ADICION_TABLA7 );
        AgregaNodoTabAdic( enExtra8, K_TAB_ADICION_TABLA8 );
        AgregaNodoTabAdic( enExtra9, K_TAB_ADICION_TABLA9 );
        AgregaNodoTabAdic( enExtra10, K_TAB_ADICION_TABLA10 );
        AgregaNodoTabAdic( enExtra11, K_TAB_ADICION_TABLA11 );
        AgregaNodoTabAdic( enExtra12, K_TAB_ADICION_TABLA12 );
        AgregaNodoTabAdic( enExtra13, K_TAB_ADICION_TABLA13 );
        AgregaNodoTabAdic( enExtra14, K_TAB_ADICION_TABLA14 );
      NodoNivel( 1, K_TAB_HISTORIAL );
        NodoNivel( 2, K_TAB_HIST_TIPO_KARDEX );
        NodoNivel( 2, K_TAB_HIST_MOT_BAJA );
        NodoNivel( 2, K_TAB_HIST_INCIDEN );
        NodoNivel( 2, K_TAB_HIST_AUT_EXTRAS );
{$endif}

      NodoNivel( 1, K_TAB_NOMINA );
        NodoNivel( 2, K_TAB_NOM_TIPO_AHORRO );
        NodoNivel( 2, K_TAB_NOM_TIPO_PRESTA );
{$ifndef PRESUPUESTOS}
        NodoNivel( 2, K_TAB_NOM_MONEDAS );
{$endif}
      NodoNivel( 1, K_TAB_OFICIALES );
        NodoNivel( 2, K_TAB_OFI_SAL_MINIMOS );
        NodoNivel( 2, K_TAB_OFI_CUOTAS_IMSS );
        NodoNivel( 2, K_TAB_OFI_ISPT_NUMERICAS );
        NodoNivel( 2, K_TAB_OFI_GRADOS_RIESGO );
        NodoNivel( 2, K_TAB_OFI_TIPOS_CAMBIO );
        NodoNivel( 2, K_TAB_VAL_UMA );
{$ifndef PRESUPUESTOS}
    // ******* Sistema *************
    NodoNivel( 0, K_SISTEMA );
      NodoNivel( 1, K_SIST_DATOS );
        NodoNivel( 2, K_SIST_DATOS_EMPRESAS );
        NodoNivel( 2, K_SIST_DATOS_GRUPOS );
        NodoNivel( 2, K_SIST_DATOS_USUARIOS );
        NodoNivel( 2, K_SIST_DATOS_POLL_PENDIENTES );
        NodoNivel( 2, K_SIST_DATOS_IMPRESORAS );
        NodoNivel( 2, K_SIST_NIVEL0 );
      NodoNivel( 1, K_SIST_REGISTRO );
      NodoNivel( 1, K_SIST_PROCESOS );
{$endif}
end;

procedure GetDatosArbolUsuario( oArbolMgr: TArbolMgr; DataSet: TDataSet; const sFileName: String );
var
   iUsuario: Integer;
   lImportando: Boolean;
begin
     // SUPONE que 'cdsUsuarios' est� posicionado en el usuario correcto
     with Dataset, oArbolMgr.DatosUsuario do
     begin
          iUsuario          := FieldByName( 'US_CODIGO' ).AsInteger;
          NombreNodoInicial := FieldByName( 'US_NOMBRE' ).AsString;
          IncluirSistema    := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
          NodoDefault       := FieldByName( 'US_FORMA' ).AsInteger;
          TieneArbol        := FALSE;
     end;
     lImportando := StrLleno( sFileName );
     // Se trae los registros de Arbol del Servidor
     with dmSistema, cdsArbol, oArbolMgr.DatosUsuario do
     begin
            if ( lImportando ) then
              cdsArbol.LoadFromFile( sFileName )
            else
               GetArbolUsuario( iUsuario );

        if not IsEmpty then
        begin
             // Busca el Primer registro para obtener nombre del Nodo
             if ( FieldByName( 'AB_ORDEN' ).AsInteger <= 0 ) then
             begin
               // En caso de IMPORTACION
               // En el primer registro va 'IncluirSistema' y 'Forma Default'
               if ( lImportando ) then
               begin
                    IncluirSistema := FieldByName( 'AB_NIVEL' ).AsInteger > 0;
                    NodoDefault    := FieldByName( 'AB_NODO' ).AsInteger;
               end
               else // Si viene de Base de Datos, permite renombrar RAIZ
               begin
                    if StrLleno( FieldByName( 'AB_DESCRIP' ).AsString ) then
                       NombreNodoInicial:= FieldByName('AB_DESCRIP').AsString;
               end;
               Next;
             end;
             TieneArbol := Not Eof;
        end;
        if ( NodoDefault = 0 ) then
           NodoDefault     := K_EMP_REGISTRO;
     end;
end;


//DevEx
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
var
   NodoRaiz: TNodoInfo;
   DataSet: TDataSet;

  procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
  var
     oNodoInfo : TNodoInfo;
  begin
       oNodoInfo := GetNodoInfo( iNodo );
       oNodoInfo.Caption := sCaption;
       oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
  end;

begin  // CreaArbolUsuario
       if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

        GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );
        with oArbolMgr.DatosUsuario do
            begin
            oArbolMgr.NumDefault := NodoDefault;

            // Agrega el Nodo Raiz con el Nombre del Usuario
            if ( oArbolMgr.Configuracion ) or ( TieneArbol ) then
               with NodoRaiz do
               begin
                    Caption := NombreNodoInicial;
                    EsForma := FALSE;
                    IndexDerechos := K_SIN_RESTRICCION;
                    oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
               end;

            if TieneArbol then
              with dmSistema.cdsArbol do
                   while not Eof do
                   begin
			//Para la Vista Actual no agregara las opciones de las constantes.}
                             {Checamos que no se de los nodos eliminados para la vista actual
                                  porque ya se encuentran en el ribbon.}
                                  if (( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_REPORTES ) and
                                  ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_SQL      ) and
                                  ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_BITACORA ) and
                                  ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_PROCESOS ) and
                                  ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONSULTAS     )) then
                                  begin
                                        NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger,
                                        FieldByName( 'AB_NODO' ).AsInteger,
                                        FieldByName( 'AB_DESCRIP' ).AsString );
                                  end;//End if 2

                            Next;
                      end;

            // Agregar el Arbol de Sistema cuando:
            //  Se trate del Shell y
            //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
            //     -Usuario NO tiene su propio Arbol
          if ( not oArbolMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
                ZArbolTress.CreaArbolDefault( oArbolMgr );

          // Si por derechos no tiene nada, agregar nodo
          //Edit By MP : Valido si el arbol TTreeview existe
          if(oArbolMgr.Arbol = nil) then
          begin
             	  if ( oArbolMgr.Arbol_DevEx.Items.Count = 0 ) then //Si no existe utilizo el arbol_Devex
               	begin
               		  with NodoRaiz do
                  	begin
                        Caption := 'No tiene derechos';
                        EsForma := FALSE;
                        IndexDerechos := K_SIN_RESTRICCION;
                        oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                  	end;
               	end;
          end
          else
          begin
              if ( oArbolMgr.Arbol.Items.Count = 0 ) then
              begin
                with NodoRaiz do
                begin
                  Caption := 'No tiene derechos';
                  EsForma := FALSE;
                  IndexDerechos := K_SIN_RESTRICCION;
                  oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                end;
          end;
        end;
            end;
          end;
//fin Edit
end;

procedure MuestraArbolOriginal( oArbol: TTreeView );
var
   oNodo: TTreeNode;
   iNodo: Integer;
begin
     oArbol.Items.BeginUpdate;
     oNodo := oArbol.Items.GetFirstNode;
     while ( oNodo <> NIL ) do
     begin
          iNodo := Integer( oNodo.Data );
          if ( iNodo > 0 ) then
             oNodo.Text := GetNodoInfo( iNodo ).Caption;
          oNodo := oNodo.GetNext;
     end;
     oArbol.Items.EndUpdate;
end;

//DevEx
procedure CreaNavBarUsuario (  oNavBarMgr: TNavBarMgr; const sFileName: String; Arbolitos: array of TcxtreeView ; ArbolitoUsuario : TcxTreeView);
var
   DataSet: TDataSet;
   NodoRaiz: TGrupoInfo;
//Edit by MP: Metodo Modificado para usar un TcxTreeview
procedure CreaArbolito( Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario

                     CreaArbolitoUsuario( oArbolMgr, '' );
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
     if ( oNavBarMgr.Configuracion ) then
       DataSet := dmSistema.cdsUsuarios
     else
         DataSet := dmCliente.cdsUsuario;


     GetDatosNavBarUsuario(oNavBarMgr, DataSet, sFileName );
     //CreaNavBarUsuario
     with oNavBarMgr.DatosUsuario do
     begin
          oNavBarMgr.NumDefault := NodoDefault;
          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oNavBarMgr.Configuracion ) or ( TieneArbol ) then
          begin
             with NodoRaiz do
             begin
                  if(dmcliente.EmpresaAbierta) then
                     Caption := NombreNodoInicial
                  else
                      Caption := 'Sistema';
                  //EsForma := FALSE;
                  IndexDerechos := K_SIN_RESTRICCION;
                  if(dmcliente.EmpresaAbierta) then
                  begin
                    if(oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)) then
                      begin
                          CreaArbolito( ArbolitoUsuario ); //Pendiente cambiar el indice
                      end;
                    end;
             end;
          end;
          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oNavBarMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
             begin
                if (oNavBarMgr.NumDefault = 0 ) then
                    oNavBarMgr.NumDefault :=  K_Empleados; //Por si solo se agregara el arbol del sistema

                CreaNavBarDefault(oNavBarMgr, Arbolitos );               
             end;

             // Si por derechos no tiene nada, agregar grupo
             if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
             begin
                with NodoRaiz do
                begin
                     Caption := 'No tiene derechos';
                     //EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)
                end;
             end;
          end;
     end;
end;



procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
var
   NodoRaiz: TNodoInfo;
   DataSet: TDataSet;

  procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
  var
     oNodoInfo : TNodoInfo;
  begin
       oNodoInfo := GetNodoInfo( iNodo );
       oNodoInfo.Caption := sCaption;
       oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
  end;

begin  // CreaArbolUsuario
       if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

       GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );
       with oArbolMgr.DatosUsuario do
       begin
          oArbolMgr.NumDefault := NodoDefault;
          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oArbolMgr.Configuracion ) or ( TieneArbol ) then
              with NodoRaiz do
              begin
                   Caption := NombreNodoInicial;
                   EsForma := FALSE;
                   IndexDerechos := K_SIN_RESTRICCION;
                   oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
              end;
          if TieneArbol then
          begin
            with dmSistema.cdsArbol do
            begin
                while not Eof do
                begin
                    NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger,
                               FieldByName( 'AB_NODO' ).AsInteger,
                               FieldByName( 'AB_DESCRIP' ).AsString );
                    Next;
                end;
            end;
          end;
          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oArbolMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
             begin
                if (oArbolMgr.Arbol = nil) then
                begin
                   if ( oArbolMgr.Arbol_DevEx.Items.Count = 0 ) then
                   begin
                      with NodoRaiz do
                      begin
                          Caption := 'No tiene derechos';
                          EsForma := FALSE;
                          IndexDerechos := K_SIN_RESTRICCION;
                          oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                      end;
                   end;
                end
                else
                begin
                  // Si por derechos no tiene nada, agregar nodo
                  if ( oArbolMgr.Arbol.Items.Count = 0 ) then
                  begin
                    with NodoRaiz do
                    begin
                        Caption := 'No tiene derechos';
                        EsForma := FALSE;
                        IndexDerechos := K_SIN_RESTRICCION;
                        oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                    end;
                  end;
                end;
             end;
          end;
       end;
//Fin Edit
end;

procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
     Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     //CreaArbolitoUsuario( oArbolMgr, '',K_GLOBAL );
                     //Antes metodo: CreaArbolUsuario
                     with oArbolMgr do
                     begin
                          //Configuracion := TRUE;
                          if(dmCliente.EmpresaAbierta) then
                          begin
                            //oArbolMgr.NumDefault := K_GLOBAL ;
                            if(K_global = K_FULL_ARBOL_TRESS) then
                              ZArbolTress.CreaArbolDefault( oArbolMgr)
                            else
                              CreaArbolitoDefault( oArbolMgr, K_GLOBAL  )
                          end
                          else
                            CreaArbolDefaultSistema( oArbolMgr);
                            //Edit by MP: si el arbol es nil, utilizar el Arbol_Devex
                          if(arbol = nil) then
                            arbol_Devex.FullCollapse
                          else
                            Arbol.FullCollapse;
                          // Fin Edit
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario}
     // ************ Presupuestos *****************
     if not dmCliente.EmpresaAbierta then
     begin
        if ( NodoNivel( K_Sistema, 1 )) then
          CreaArbolito ( K_SISTEMA, Arbolitos[1]);
     end
     else
     begin
        if( NodoNivel( K_PRESUPUESTOS, 1 )) then
        begin
          CreaArbolito ( K_PRESUPUESTOS, Arbolitos[1]);
        end;
        if( NodoNivel( K_Empleados, 2 )) then
        begin
          CreaArbolito ( K_EMPLEADOS, Arbolitos[2]);
        end;
        if( NodoNivel( K_NOMINA, 3 )) then
        begin
          CreaArbolito ( K_NOMINA, Arbolitos[3]);
        end;
        if( NodoNivel( K_CONSULTAS, 4 )) then
        begin
          CreaArbolito ( K_CONSULTAS, Arbolitos[4]);
        end;
        if( NodoNivel( K_CATALOGOS, 5 )) then
        begin
          CreaArbolito ( K_CATALOGOS, Arbolitos[5]);
        end;
        if( NodoNivel( K_TABLAS, 6 )) then
        begin
          CreaArbolito ( K_TABLAS, Arbolitos[6]);
        end;
        if(nodoNivel(K_FULL_ARBOL_TRESS,7))then
        begin
          CreaArbolito(K_Full_Arbol_Tress,Arbolitos[7]);
        end;
     end;
end;



//DevEx
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
 case iNodo of 
          // Presupuestos
          K_PRESUPUESTOS            : Result := Grupo( 'Presupuestos', iNodo );
          K_PRE_CONFIGURACION       : Result := Grupo( 'Configuraci�n', iNodo );

          // Empleados
          K_EMPLEADOS               : Result := Grupo( 'Empleados', iNodo );
          K_EMP_DATOS               : Result := Grupo( 'Datos', iNodo );
          K_EMP_EXPEDIENTE          : Result := Grupo( 'Expediente', iNodo );
          K_EMP_NOMINA              : Result := Grupo( 'N�mina', iNodo );

          // N�minas
          K_NOMINA                  : Result := Grupo( 'N�minas', iNodo );
          K_NOM_DATOS               : Result := Grupo( 'Datos', iNodo );
          K_NOM_EXCEPCIONES         : Result := Grupo( 'Excepciones', iNodo );

          // Consultas
          K_CONSULTAS               : Result := Grupo( 'Consultas', iNodo );

          // Cat�logos
          K_CATALOGOS               : Result := Grupo( 'Cat�logos', iNodo );
          K_CAT_CONTRATACION        : Result := Grupo( 'Contrataci�n', iNodo );

          //Tablas
          K_TABLAS                  : Result := Grupo( 'Tablas', iNodo );
          K_TAB_NOMINA              : Result := Grupo( 'N�mina', iNodo );
          K_TAB_OFICIALES           : Result := Grupo( 'Oficiales', iNodo );  

          K_FULL_ARBOL_TRESS        : Result := Grupo('�rbol Cl�sico',iNodo);
     end;

end;


procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
var
   i, iNiveles: Integer;

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
     if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then

          NodoNivel(1,iNodo);
end;

begin  // CreaArbolDefault
    case iAccesoGlobal of
    // ************ Presupuestos *****************
     K_PRESUPUESTOS :
     begin
     //NodoNivel( 0, K_PRESUPUESTOS );
      NodoNivel( 0, K_PRE_CONFIGURACION );
       NodoNivel( 1, K_CAT_NOMINA_PERIODOS );
       NodoNivel( 1, K_CAT_NOMINA_CONCEPTOS );
       NodoNivel( 1, K_CAT_NOMINA_PARAMETROS );
       NodoNivel( 1, K_PRE_CONFIGURACION_ALTAS );
       NodoNivel( 1, K_CAT_GRALES_EVENTOS );
       NodoNivel( 1, K_PRE_ESCENARIOS ); // (JB) Escenarios de presupuestos T1060 CR1872
       NodoNivel( 1, K_PRE_CONFIGURACION_SUPUESTO_RH );
      NodoNivel( 1, K_PRE_PROCESOS );
     end;
    // ********* Empleados ********
    K_EMPLEADOS :
    begin
    //NodoNivel( 0, K_EMPLEADOS );
      NodoNivel( 0, K_EMP_DATOS );

        NodoNivel( 1, K_EMP_DATOS_CONTRATA );
        NodoNivel( 1, K_EMP_DATOS_AREA );
        NodoNivel( 1, K_EMP_DATOS_PERCEP );
      NodoNivel( 0, K_EMP_EXPEDIENTE );
        NodoNivel( 1, K_EMP_EXP_KARDEX );
      NodoNivel( 0, K_EMP_NOMINA );
        NodoNivel( 1, K_EMP_NOM_AHORROS );
        NodoNivel( 1, K_EMP_NOM_PRESTAMOS );
        NodoNivel( 1, K_EMP_NOM_ACUMULA );
      end;

    // ********* N�minas ************
    K_NOMINA :
    begin
    //if NodoNivel( 0, K_NOMINA ) then
    begin
         if NodoNivel( 0, K_NOM_DATOS ) then
         begin
              NodoNivel( 1, K_NOM_DATOS_TOTALES );
              NodoNivel( 1, K_NOM_DATOS_MONTOS );
              NodoNivel( 1, K_NOM_DATOS_CLASIFI );
         end;
         if NodoNivel( 0, K_NOM_EXCEPCIONES ) then
         begin
              NodoNivel( 1, K_NOM_EXCEP_DIAS_HORAS );
              NodoNivel( 1, K_NOM_EXCEP_MONTOS );
              NodoNivel( 1, K_NOM_EXCEP_GLOBALES );
         end;
    end;
    end; //case K_NOMINAS


    // ******** Consultas ***********
    K_CONSULTAS :
    begin
    //NodoNivel( 0, K_CONSULTAS );
      NodoNivel( 0, K_CONS_REPORTES );
      NodoNivel( 0, K_CONS_SQL );
      NodoNivel( 0, K_CONS_BITACORA );
      NodoNivel( 0, K_CONS_PROCESOS );
    end;
    // ******* Cat�logos *************
    K_CATALOGOS :
    begin
    //NodoNivel( 0, K_CATALOGOS );
      NodoNivel( 0, K_CAT_CONTRATACION );
        NodoNivel( 1, K_CAT_CONT_PUESTOS );
        NodoNivel( 1, K_CAT_CONT_CLASIFI );
        NodoNivel( 1, K_CAT_CONT_PER_FIJAS );
        NodoNivel( 1, K_CAT_CONT_TURNOS );
        NodoNivel( 1, K_CAT_CONT_HORARIOS );
        NodoNivel( 1, K_CAT_CONT_PRESTA );
        NodoNivel( 1, K_CAT_CONT_CONTRATO );
      NodoNivel( 0, K_CAT_GENERALES );
        NodoNivel( 1, K_CAT_GRALES_PATRONALES );
        NodoNivel( 1, K_CAT_GRALES_CONDICIONES );
        NodoNivel( 1, K_CAT_GRALES_EVENTOS );
      NodoNivel( 0, K_CAT_CONFIGURACION );
        NodoNivel( 1, K_CAT_CONFI_GLOBALES );
     end;
    // ******* Tablas *************
    K_TABLAS :
    begin
    //NodoNivel( 0, K_TABLAS );
      NodoNivel( 0, K_TAB_AREAS );
        iNiveles := Global.NumNiveles;
        for i := 1 to iNiveles do
            NodoNivel( 1, K_TAB_AREAS + i );
      NodoNivel( 0, K_TAB_NOMINA );
        NodoNivel( 1, K_TAB_NOM_TIPO_AHORRO );
        NodoNivel( 1, K_TAB_NOM_TIPO_PRESTA );

      NodoNivel( 0, K_TAB_OFICIALES );
        NodoNivel( 1, K_TAB_OFI_SAL_MINIMOS );
        NodoNivel( 1, K_TAB_OFI_CUOTAS_IMSS );
        NodoNivel( 1, K_TAB_OFI_ISPT_NUMERICAS );
        NodoNivel( 1, K_TAB_OFI_GRADOS_RIESGO );
        NodoNivel( 1, K_TAB_OFI_TIPOS_CAMBIO );
        NodoNivel( 1, K_TAB_VAL_UMA );
    end;
end;
end;


procedure CreaArbolDefaultSistema( oArbolMgr : TArbolMgr );

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
     if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then
        NodoNivel( 2, iNodo );
end;

begin  // CreaArbolDefaultSistema

end;

//DevEx
procedure GetDatosNavBarUsuario( oNavBarMgr: TNavBarMgr; DataSet: TDataSet; const sFileName: String );
var
   iUsuario: Integer;
   lImportando: Boolean;
   oDatosUsuario: TDatosArbolUsuario;
begin
     // SUPONE que 'cdsUsuarios' est� posicionado en el usuario correcto
     with Dataset, oDatosUsuario do
     begin
          iUsuario          := FieldByName( 'US_CODIGO' ).AsInteger;
          NombreNodoInicial := FieldByName( 'US_NOMBRE' ).AsString;
          IncluirSistema    := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
          NodoDefault       := FieldByName( 'US_FORMA' ).AsInteger;
          TieneArbol        := FALSE;
     end;
     lImportando := StrLleno( sFileName );
     // Se trae los registros de Arbol del Servidor
     with dmSistema, cdsArbol, oDatosUsuario do
     begin
            if ( lImportando ) then
              cdsArbol.LoadFromFile( sFileName )
            else
               GetArbolUsuario( iUsuario );
        if not IsEmpty then
        begin
             // Busca el Primer registro para obtener nombre del Nodo
             if ( FieldByName( 'AB_ORDEN' ).AsInteger <= 0 ) then
             begin
               // En caso de IMPORTACION
               // En el primer registro va 'IncluirSistema' y 'Forma Default'
               if ( lImportando ) then
               begin
                    IncluirSistema := FieldByName( 'AB_NIVEL' ).AsInteger > 0;
                    NodoDefault    := FieldByName( 'AB_NODO' ).AsInteger;
               end
               else // Si viene de Base de Datos, permite renombrar RAIZ
               begin
                    if StrLleno( FieldByName( 'AB_DESCRIP' ).AsString ) then
                       NombreNodoInicial:= FieldByName('AB_DESCRIP').AsString;
               end;
               Next;
             end;
             TieneArbol := Not Eof;
        end;
        if ( NodoDefault = 0 ) then
           NodoDefault     := K_Empleados; //K_EMP_REGISTRO;
     end;
     oNavBarMgr.DatosUsuario := oDatosUsuario;
end;

end.
