unit FEditCatEventosAltas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, StdCtrls,
  Mask, ZetaEdit, ZetaNumero, ZetaKeyCombo, ZetaFecha, ComCtrls,
  ZetaKeyLookup_DevEx, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC;

type
  TEditCatEventosAltas_DevEx = class(TBaseEdicion_DevEx)
    PanelGeneral: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EV_ACTIVO: TDBCheckBox;
    EV_CODIGO: TZetaDBEdit;
    EV_DESCRIP: TDBEdit;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    PageControl: TcxPageControl;
    TabContratacion: TcxTabSheet;
    EV_PUESTOLbl: TLabel;
    CB_CLASIFIlbl: TLabel;
    CB_HORARIOlbl: TLabel;
    EV_PUESTO: TZetaDBKeyLookup_DevEx;
    EV_CLASIFI: TZetaDBKeyLookup_DevEx;
    EV_TURNO: TZetaDBKeyLookup_DevEx;
    TabArea: TcxTabSheet;
    EV_NIVEL1lbl: TLabel;
    EV_NIVEL2lbl: TLabel;
    EV_NIVEL3lbl: TLabel;
    EV_NIVEL4lbl: TLabel;
    EV_NIVEL5lbl: TLabel;
    EV_NIVEL6lbl: TLabel;
    EV_NIVEL7lbl: TLabel;
    EV_NIVEL8lbl: TLabel;
    EV_NIVEL9lbl: TLabel;
    EV_NIVEL9: TZetaDBKeyLookup_DevEx;
    EV_NIVEL8: TZetaDBKeyLookup_DevEx;
    EV_NIVEL7: TZetaDBKeyLookup_DevEx;
    EV_NIVEL6: TZetaDBKeyLookup_DevEx;
    EV_NIVEL5: TZetaDBKeyLookup_DevEx;
    EV_NIVEL4: TZetaDBKeyLookup_DevEx;
    EV_NIVEL3: TZetaDBKeyLookup_DevEx;
    EV_NIVEL2: TZetaDBKeyLookup_DevEx;
    EV_NIVEL1: TZetaDBKeyLookup_DevEx;
    TabSalario: TcxTabSheet;
    EV_OTRAS_1Lbl: TLabel;
    EV_OTRAS_2Lbl: TLabel;
    EV_OTRAS_3Lbl: TLabel;
    EV_OTRAS_4Lbl: TLabel;
    EV_OTRAS_5Lbl: TLabel;
    EV_OTRAS_1: TZetaDBKeyLookup_DevEx;
    EV_OTRAS_2: TZetaDBKeyLookup_DevEx;
    EV_OTRAS_3: TZetaDBKeyLookup_DevEx;
    EV_OTRAS_4: TZetaDBKeyLookup_DevEx;
    EV_OTRAS_5: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    EV_PATRON: TZetaDBKeyLookup_DevEx;
    EV_AUTOSAL: TDBCheckBox;
    EV_SALARIOlbl: TLabel;
    EV_SALARIO: TZetaDBNumero;
    CB_PER_VARlbl: TLabel;
    EV_PER_VAR: TZetaDBNumero;
    CB_ZONA_GElbl: TLabel;
    EV_ZONA_GE: TDBComboBox;
    CB_TABLASSlbl: TLabel;
    EV_TABLASS: TZetaDBKeyLookup_DevEx;
    TabOtros: TcxTabSheet;
    LblNivel0: TLabel;
    EV_NIVEL0: TZetaDBKeyLookup_DevEx;
    GrupoContrato: TGroupBox;
    Label5: TLabel;
    EV_CONTRAT: TZetaDBKeyLookup_DevEx;
    EV_BAJA: TDBCheckBox;
    EV_BAN_ELE: TDBCheckBox;
    Label6: TLabel;
    EV_NOMINA: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EV_AUTOSALClick(Sender: TObject);
  private
    { Private declarations }
    procedure InitControlesLookup;
    procedure SetCamposNivel;
    procedure SetControlesNivel0;
    procedure SetControlesSalario;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCatEventosAltas_DevEx: TEditCatEventosAltas_DevEx;

implementation

uses dCliente, dCatalogos, dTablas, dSistema, dPresupuestos, dGlobal,
     ZetaCommonClasses,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaBuscador_DevEx,
     ZAccesosTress,
     ZetaCommonLists;

{$R *.DFM}

procedure TEditCatEventosAltas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := EV_CODIGO;
     InitControlesLookup;
     IndexDerechos:= D_PRESUP_CONTRATACIONES;
     HelpContext := H_CAT_EVENTOS_ALTAS;
end;

procedure TEditCatEventosAltas_DevEx.FormShow(Sender: TObject);
begin
     SetCamposNivel;
     SetControlesNivel0;
     inherited;
     SetControlesSalario;
     PageControl.ActivePage := TabContratacion;
     EV_NOMINA.ListaFija:=lfTipoPeriodo; //acl
end;

procedure TEditCatEventosAltas_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsSSocial.Conectar;
          cdsTurnos.Conectar;
          cdsContratos.Conectar;
          cdsRPatron.Conectar;
          cdsOtrasPer.Conectar;
     end;
     with dmTablas do
     begin
          if EV_NIVEL1.Visible then cdsNivel1.Conectar;
          if EV_NIVEL2.Visible then cdsNivel2.Conectar;
          if EV_NIVEL3.Visible then cdsNivel3.Conectar;
          if EV_NIVEL4.Visible then cdsNivel4.Conectar;
          if EV_NIVEL5.Visible then cdsNivel5.Conectar;
          if EV_NIVEL6.Visible then cdsNivel6.Conectar;
          if EV_NIVEL7.Visible then cdsNivel7.Conectar;
          if EV_NIVEL8.Visible then cdsNivel8.Conectar;
          if EV_NIVEL9.Visible then cdsNivel9.Conectar;
     end;

     dmCliente.cdsEmpleadoLookUp.Conectar;

     if EV_NIVEL0.Visible then dmSistema.cdsNivel0.Conectar;

     DataSource.Dataset := dmPresupuestos.cdsEventosAltas;
end;

procedure TEditCatEventosAltas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Contratación', 'Contratación', 'EV_CODIGO', dmPresupuestos.cdsEventosAltas );
end;

procedure TEditCatEventosAltas_DevEx.InitControlesLookup;
begin
     with dmCatalogos do
     begin
          EV_PUESTO.LookupDataSet := cdsPuestos;
          EV_CLASIFI.LookupDataSet := cdsClasifi;
          EV_TABLASS.LookupDataSet := cdsSSocial;
          EV_TURNO.LookupDataSet := cdsTurnos;
          EV_CONTRAT.LookupDataSet := cdsContratos;
          EV_OTRAS_1.LookupDataSet := cdsOtrasPer;
          EV_OTRAS_2.LookupDataSet := cdsOtrasPer;
          EV_OTRAS_3.LookupDataSet := cdsOtrasPer;
          EV_OTRAS_4.LookupDataSet := cdsOtrasPer;
          EV_OTRAS_5.LookupDataSet := cdsOtrasPer;
          EV_PATRON.LookupDataSet := cdsRPatron;
     end;
     with dmTablas do
     begin
          EV_NIVEL1.LookupDataSet := cdsNivel1;
          EV_NIVEL2.LookupDataSet := cdsNivel2;
          EV_NIVEL3.LookupDataSet := cdsNivel3;
          EV_NIVEL4.LookupDataSet := cdsNivel4;
          EV_NIVEL5.LookupDataSet := cdsNivel5;
          EV_NIVEL6.LookupDataSet := cdsNivel6;
          EV_NIVEL7.LookupDataSet := cdsNivel7;
          EV_NIVEL8.LookupDataSet := cdsNivel8;
          EV_NIVEL9.LookupDataSet := cdsNivel9;
     end;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     EV_NIVEL0.LookupDataSet := dmSistema.cdsNivel0;
end;

procedure TEditCatEventosAltas_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, EV_NIVEL1lbl, EV_NIVEL1 );
     SetCampoNivel( 2, iNiveles, EV_NIVEL2lbl, EV_NIVEL2 );
     SetCampoNivel( 3, iNiveles, EV_NIVEL3lbl, EV_NIVEL3 );
     SetCampoNivel( 4, iNiveles, EV_NIVEL4lbl, EV_NIVEL4 );
     SetCampoNivel( 5, iNiveles, EV_NIVEL5lbl, EV_NIVEL5 );
     SetCampoNivel( 6, iNiveles, EV_NIVEL6lbl, EV_NIVEL6 );
     SetCampoNivel( 7, iNiveles, EV_NIVEL7lbl, EV_NIVEL7 );
     SetCampoNivel( 8, iNiveles, EV_NIVEL8lbl, EV_NIVEL8 );
     SetCampoNivel( 9, iNiveles, EV_NIVEL9lbl, EV_NIVEL9 );
end;

procedure TEditCatEventosAltas_DevEx.SetControlesSalario;
var
   lEnabled : Boolean;
begin
     lEnabled := ( not EV_AUTOSAL.Checked );
     ZetaClientTools.SetControlEnabled( EV_SALARIO, EV_SALARIOLbl, lEnabled );
     ZetaClientTools.SetControlEnabled( EV_OTRAS_1, EV_OTRAS_1Lbl, lEnabled );
     ZetaClientTools.SetControlEnabled( EV_OTRAS_2, EV_OTRAS_2Lbl, lEnabled );
     ZetaClientTools.SetControlEnabled( EV_OTRAS_3, EV_OTRAS_3Lbl, lEnabled );
     ZetaClientTools.SetControlEnabled( EV_OTRAS_4, EV_OTRAS_4Lbl, lEnabled );
     ZetaClientTools.SetControlEnabled( EV_OTRAS_5, EV_OTRAS_5Lbl, lEnabled );
end;

procedure TEditCatEventosAltas_DevEx.SetControlesNivel0;
begin
     EV_NIVEL0.Visible := dmSistema.HayNivel0 and ZetaCommonTools.strVacio( dmCliente.Confidencialidad );
     LblNivel0.Visible := EV_NIVEL0.Visible;
end;

procedure TEditCatEventosAltas_DevEx.EV_AUTOSALClick(Sender: TObject);
begin
     inherited;
     if Editing then
     begin
          if EV_AUTOSAL.Checked and strVacio( dmPresupuestos.cdsEventosAltas.FieldByName( 'EV_CLASIFI' ).AsString ) then
          begin
               ZetaDialogo.ZError( self.Caption, 'Para definir salario x tabulador, se debe asignar la clasificación' +
                                                 CR_LF + 'revise sus datos de contratación', 0 );
               EV_AUTOSAL.Checked := FALSE;
          end;
          SetControlesSalario;
     end;
end;

end.
