inherited GridEditEscenario_DevEx: TGridEditEscenario_DevEx
  Left = 356
  Top = 304
  Caption = 'Edicion de Escenarios'
  ClientHeight = 210
  ClientWidth = 486
  PixelsPerInch = 96
  TextHeight = 13
  object DBCodigoLBL: TLabel [0]
    Left = 73
    Top = 56
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object lblNombre: TLabel [1]
    Left = 50
    Top = 78
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBInglesLBL: TLabel [2]
    Left = 78
    Top = 100
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object Label2: TLabel [3]
    Left = 69
    Top = 122
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label1: TLabel [4]
    Left = 79
    Top = 144
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  inherited PanelBotones: TPanel
    Top = 174
    Width = 486
    TabOrder = 5
    inherited OK_DevEx: TcxButton
      Left = 320
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 400
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 486
    TabOrder = 7
    inherited ValorActivo2: TPanel
      Width = 160
      inherited textoValorActivo2: TLabel
        Width = 154
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 8
  end
  object ES_CODIGO: TZetaDBEdit [8]
    Left = 113
    Top = 52
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
    Text = 'ES_CODIGO'
    ConfirmEdit = True
    DataField = 'ES_CODIGO'
    DataSource = DataSource
  end
  object ES_ELEMENT: TDBEdit [9]
    Left = 113
    Top = 74
    Width = 280
    Height = 21
    DataField = 'ES_ELEMENT'
    DataSource = DataSource
    TabOrder = 1
  end
  object ES_INGLES: TDBEdit [10]
    Left = 113
    Top = 96
    Width = 280
    Height = 21
    DataField = 'ES_INGLES'
    DataSource = DataSource
    TabOrder = 2
  end
  object ES_NUMERO: TZetaDBNumero [11]
    Left = 113
    Top = 118
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 3
    Text = '0.00'
    DataField = 'ES_NUMERO'
    DataSource = DataSource
  end
  object ES_TEXTO: TDBEdit [12]
    Left = 113
    Top = 140
    Width = 280
    Height = 21
    DataField = 'ES_TEXTO'
    DataSource = DataSource
    TabOrder = 4
  end
  inherited DataSource: TDataSource
    Left = 340
    Top = 41
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 2621632
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 264
    Top = 40
    DockControlHeights = (
      0
      0
      28
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 2621672
  end
end
