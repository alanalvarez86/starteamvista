unit FGridEscenarios_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, 
     StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
     cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
     cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TGridEscenario_DevEx = class(TBaseGridLectura_DevEx)
    ES_CODIGO: TcxGridDBColumn;
    ES_ELEMENT: TcxGridDBColumn;
    ES_INGLES: TcxGridDBColumn;
    ES_NUMERO: TcxGridDBColumn;
    ES_TEXTO: TcxGridDBColumn;
    LLAVE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    //function ConceptoValido( var sMensaje : string ): Boolean;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  GridEscenario_DevEx: TGridEscenario_DevEx;

implementation

uses DPresupuestos,
     ZetaDialogo,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZAccesosTress, ZetaBuscador_DevEx;

{$R *.dfm}

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TGridEscenario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     IndexDerechos := D_PRESUP_ESCENARIOS;
     HelpContext := H00000_Escenarios_de_presupuesto;
end;

procedure TGridEscenario_DevEx.Connect;
begin
     with dmPresupuestos do
     begin
          cdsEscenarios.Conectar;
          DataSource.DataSet:= cdsEscenarios;
     end;
end;

procedure TGridEscenario_DevEx.Agregar;
begin
     dmPresupuestos.cdsEscenarios.Agregar;
end;

procedure TGridEscenario_DevEx.Borrar;
begin
     dmPresupuestos.cdsEscenarios.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TGridEscenario_DevEx.Modificar;
begin
     dmPresupuestos.cdsEscenarios.Modificar;
end;

procedure TGridEscenario_DevEx.Refresh;
begin
     dmPresupuestos.cdsEscenarios.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TGridEscenario_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEscenarios, dmPresupuestos.cdsEscenarios );
end;


procedure TGridEscenario_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('ES_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TGridEscenario_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'ESCENARIO', 'ESCENARIO', 'ES_CODIGO', dmPresupuestos.cdsEscenarios );
end;


end.
