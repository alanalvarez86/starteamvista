program TressPresupuestos;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  dCatalogos in '..\DataModules\dCatalogos.pas' {dmCatalogos: TDataModule},
  DDiccionario in 'DDiccionario.pas' {dmDiccionario: TDataModule},
  DGlobal in '..\DataModules\DGlobal.pas',
  DNomina in '..\DataModules\DNomina.pas' {dmNomina: TDataModule},
  dRecursos in '..\DataModules\dRecursos.pas' {dmRecursos: TDataModule},
  DReportes in '..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  dSistema in '..\DataModules\dSistema.pas' {dmSistema: TDataModule},
  dtablas in '..\DataModules\dtablas.pas' {dmTablas: TDataModule},
  ZBaseDlgModal_DevEx in '..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  ZBaseEdicion_DevEx in '..\Tools\ZBaseEdicion_DevEx.pas' {BaseEdicion_DevEx},
  ZBaseGlobal_DevEx in '..\Tools\ZBaseGlobal_DevEx.pas' {BaseGlobal_DevEx},
  ZBaseEscogeGrid_DevEx in '..\Tools\ZBaseEscogeGrid_DevEx.pas' {BaseEscogeGrid_DevEx},
  ZcxBaseWizard in '..\Tools\ZcxBaseWizard.pas' {cxBaseWizard},
  ZcxWizardBasico in '..\Tools\ZcxWizardBasico.pas' {CXWizardBasico},
  ZCXBaseWizardFiltro in '..\Tools\ZCXBaseWizardFiltro.pas' {BaseCXWizardFiltro},
  FWizSistBaseFiltro in '..\Wizards\FWizSistBaseFiltro.pas' {WizSistBaseFiltro},
  ZBaseGridEdicion_DevEx in '..\Tools\ZBaseGridEdicion_DevEx.pas' {BaseGridEdicion_DevEx},
  ZBaseSelectGrid_DevEx in '..\Tools\ZBaseSelectGrid_DevEx.pas' {BaseGridSelect_DevEx},
  FTressShell in 'FTressShell.pas' {TressShell},
  ZetaWizardFeedBack_DevEx in '..\Tools\ZetaWizardFeedBack_DevEx.pas' {WizardFeedback_DevEx},
  ZetaBuscaEntero_DevEx in '..\Tools\ZetaBuscaEntero_DevEx.pas' {BuscaEntero_DevEx},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DCliente in '..\DataModules\DCliente.pas' {dmCliente: TDataModule},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FBaseReportes_DevEx in '..\Reportes\FBaseReportes_DevEx.pas' {BaseReportes_DevEx},
  FEditBaseReportes_DevEx in '..\Reportes\FEditBaseReportes_DevEx.pas' {EditBaseReportes_DevEx},
  FEditReportes_DevEx in '..\Reportes\FEditReportes_DevEx.pas' {EditReportes_DevEx},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  dBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  dmailmerge in '..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule},
  FWizNomDefinirPeriodos_Devex in '..\Wizards\FWizNomDefinirPeriodos_Devex.pas' {WizNomDefinirPeriodos_Devex},
  DPresupuestos in '..\DataModules\DPresupuestos.pas' {dmPresupuestos: TDataModule},
  ZetaDialogo in '..\Tools\ZetaDialogo.pas',
  ZBaseNavBarShell in '..\Tools\ZBaseNavBarShell.pas' {BaseNavBarShell},
  ZBaseGridLectura_DevEx in '..\Tools\ZBaseGridLectura_DevEx.pas' {BaseGridLectura_DevEx},
  ZBasicoNavBarShell in '..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell};

{$R *.RES}
{$R WindowsXP.res}
{$R ..\Traducciones\Spanish.RES} 


begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Tress Presupuestos de N�mina';
  Application.HelpFile := 'Tress.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if dmCliente.Sentinel_EsProfesional_MSSQL then
       begin
            ZError('Tress Presupuestos de N�mina','Aplicaci�n no disponible para licencia Profesional de Sistema TRESS',0 );
       end
       else
       begin
            if Login( False ) then
            begin
                 Show;
                 Update;
                 BeforeRun;
                 Application.Run;
            end;
       end;
       Free;
  end;
end.
