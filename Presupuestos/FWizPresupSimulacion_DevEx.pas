unit FWizPresupSimulacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, {$ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ZetaFecha,
  ZetaKeyCombo, ComCtrls, Mask, ZetaNumero, 
  ZetaCommonClasses, ZetaEdit, ZCXBaseWizardFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, cxCheckBox, dxSkinsDefaultPainters, cxProgressBar;

type
  TWizPresupSimulacion_DevEx = class(TBaseCXWizardFiltro)
    iYearLBL: TLabel;
    YearPresup: TZetaNumero;
    GBSupuestosRH: TcxGroupBox;
    LblFechaRH1: TLabel;
    LblFechaRH2: TLabel;
    RBTodosSupuestos: TcxRadioButton;
    RBRangoSupuestos: TcxRadioButton;
    FechaRH1: TZetaFecha;
    FechaRH2: TZetaFecha;
    TabAguinaldo: TdxWizardControlPage;
    Label2: TLabel;
    FechaInicialLBL: TLabel;
    FechaFinalLBL: TLabel;
    FaltasLBL: TLabel;
    IncapacidadesLBL: TLabel;
    AnticiposLBL: TLabel;
    Label3: TLabel;
    ZKAguinaldoMes: TZetaKeyCombo;
    ZAguinaldoFechaIni: TZetaFecha;
    ZAguinaldoFechaFin: TZetaFecha;
    AguinaldoFaltas: TcxMemo;
    AguinaldoIncapacidades: TcxMemo;
    AguinaldoAnticipos: TcxMemo;
    RGAguinaldoProp: TcxRadioGroup;
    BtnAnticipos: TcxButton;
    BtnIncapacidades: TcxButton;
    BtnFaltas: TcxButton;
    TabPTU: TdxWizardControlPage;
    Label4: TLabel;
    Label10: TLabel;
    UtilidadLBL: TLabel;
    SumaDiasLBL: TLabel;
    PercepcionesLBL: TLabel;
    FiltroDirectorLBL: TLabel;
    FiltroEventualLBL: TLabel;
    FiltroPlantaLBL: TLabel;
    SalarioTopeLBL: TLabel;
    ConceptoLBL: TLabel;
    ZKPTUMes: TZetaKeyCombo;
    ZNPTUUtilidad: TZetaNumero;
    PTUSumaDias: TcxMemo;
    PTUPercepciones: TcxMemo;
    PTUFiltroDirector: TcxMemo;
    BtnSumaDias: TcxButton;
    BtnPercepciones: TcxButton;
    BtnFiltroDirector: TcxButton;
    BtnFiltroPlanta: TcxButton;
    BtnFiltroEventual: TcxButton;
    PTUFiltroPlanta: TcxMemo;
    PTUFiltroEventual: TcxMemo;
    ZNPTUSalarioTope: TZetaNumero;
    KLPTUConcepto: TZetaKeyLookup_DevEx;
    ZKPTUPeriodo: TZetaKeyCombo;
    ZKAguinaldoPeriodo: TZetaKeyCombo;
    GBAguinaldo: TcxGroupBox;
    GBPTU: TcxGroupBox;
    ProgressPanelGlobal: TPanel;
    CBAguinaldoCalcular: TcxCheckBox;
    CBPTUCalcular: TcxCheckBox;
    GroupBox1: TcxGroupBox;
    lblMesInicial: TLabel;
    MesInicial: TZetaKeyCombo;
    MesFinal: TZetaKeyCombo;
    lblMesFinal: TLabel;
    RBRangoNominas: TcxRadioButton;
    RBRangoMeses: TcxRadioButton;
    GroupBox2: TcxGroupBox;
    ES_CODIGO: TZetaKeyLookup_DevEx;
    ProgressBarGlobal: TcxProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure YearPresupExit(Sender: TObject);
    procedure RBTodosSupuestosClick(Sender: TObject);
    procedure MesesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBAguinaldoCalcularClick(Sender: TObject);
    procedure CBPTUCalcularClick(Sender: TObject);
    procedure BtnFaltasClick(Sender: TObject);
    procedure BtnIncapacidadesClick(Sender: TObject);
    procedure BtnAnticiposClick(Sender: TObject);
    procedure BtnSumaDiasClick(Sender: TObject);
    procedure BtnPercepcionesClick(Sender: TObject);
    procedure BtnFiltroDirectorClick(Sender: TObject);
    procedure BtnFiltroPlantaClick(Sender: TObject);
    procedure BtnFiltroEventualClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RBRangoNominasClick(Sender: TObject);
  private
    { Private declarations }
    FParameterAguinaldo: TZetaParams;
    FParameterPTU: TZetaParams;
    procedure InitFechasSupuestos;
    procedure InitFechasAguinaldo;
    procedure CallMeBackStartLocal( Sender: TObject; const sMsg: String; const iMax: Integer );
    procedure CallMeBackLocal( Sender: TObject; const sMsg: String; const iStep: Integer; var Continue: Boolean );
    procedure CallMeBackEndLocal( Sender: TObject; const sMsg: String );
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizPresupSimulacion_DevEx: TWizPresupSimulacion_DevEx;

implementation

uses dCliente, dPresupuestos, dCatalogos, DGlobal,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZetaTipoEntidad,
     ZetaDialogo,
     ZConstruyeFormula,
     ZGlobalTress;

{$R *.dfm}

procedure TWizPresupSimulacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FParameterAguinaldo := TZetaParams.Create;
     FParameterPTU := TZetaParams.Create;
     ParametrosControl := YearPresup;
     { Parametros }
     YearPresup.Valor := ( dmCliente.YearDefault + 1 );  // Sugiere el sig. a�o
     MesInicial.Itemindex := Ord( mtEnero );
     MesFinal.ItemIndex := Ord( mtDiciembre );
     RBTodosSupuestos.Checked := TRUE;
     InitFechasSupuestos;
     { Aguinaldo }
     CBAguinaldoCalcular.Checked := TRUE;
     ZKAguinaldoMes.ItemIndex := Ord( mtDiciembre );
     ZKAguinaldoPeriodo.ItemIndex := Ord( pmUltimo );
     AguinaldoFaltas.Tag := K_GLOBAL_DEF_AGUINALDO_FALTAS;
     AguinaldoIncapacidades.Tag := K_GLOBAL_DEF_AGUINALDO_INCAPACIDADES;
     AguinaldoAnticipos.Tag := K_GLOBAL_DEF_AGUINALDO_ANTICIPOS;
     RGAguinaldoProp.ItemIndex := 0;
     InitFechasAguinaldo;
     { PTU }
     CBPTUCalcular.Checked := TRUE;
     ZKPTUMes.ItemIndex := Ord( mtMayo );
     ZKPTUPeriodo.ItemIndex := Ord( pmUltimo );
     ZNPTUUtilidad.Tag := K_GLOBAL_DEF_PTU_UTILIDAD_REPARTIR;
     PTUSumaDias.Tag := K_GLOBAL_DEF_PTU_SUMA_DIAS;
     PTUPercepciones.Tag := K_GLOBAL_DEF_PTU_PERCEPCIONES;
     PTUFiltroDirector.Tag := K_GLOBAL_DEF_PTU_FILTRO_DIRECTOR;
     PTUFiltroPlanta.Tag := K_GLOBAL_DEF_PTU_FILTRO_PLANTA;
     PTUFiltroEventual.Tag := K_GLOBAL_DEF_PTU_FILTRO_EVENTUAL;
     ZNPTUSalarioTope.Tag := K_GLOBAL_DEF_PTU_SALARIO_TOPE;
     KLPTUConcepto.LookupDataset := dmCatalogos.cdsConceptos;
     HelpContext := H_SIMULACION_PRESUPUESTOS;

end;

procedure TWizPresupSimulacion_DevEx.FormDestroy(Sender: TObject);
begin
     FParameterPTU.Free;
     FParameterAguinaldo.Free;
     inherited;
end;

procedure TWizPresupSimulacion_DevEx.FormShow(Sender: TObject);
const
     K_MARGEN_PANEL = 23;
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmPresupuestos.cdsEscenarios.Conectar; // (JB) Escenarios de presupuestos T1060 CR1872
     inherited;
     with ProgressBarGlobal do
     begin
            Left := K_MARGEN_PANEL + 1;
            Width := iMax( 0, ProgressPanelGlobal.Width - ( K_MARGEN_PANEL * 2 ) );
     end;
     //DevEx
     Advertencia.Caption := 'Al aplicar el proceso se iniciar� la simulaci�n, la barra avanzar� conforme vaya aplicando los supuestos.';
     Parametros.PageIndex := 0;
     TabAguinaldo.PageIndex  := 1;
     TabPTU.PageIndex  := 2;
     FiltrosCondiciones.PageIndex := 3;
     Ejecucion.PageIndex := 4;

end;

procedure TWizPresupSimulacion_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddInteger( 'Year', YearPresup.ValorEntero );
          AddInteger( 'MesInicial', MesInicial.Valor );
          AddInteger( 'MesFinal', MesFinal.Valor );
          AddBoolean( 'TodosSupuestosRH', RBTodosSupuestos.Checked );
          AddBoolean( 'TodasNominas', RBRangoNominas.Checked );
          AddDate( 'FechaRH1', FechaRH1.Valor );
          AddDate( 'FechaRH2', FechaRH2.Valor );
          AddBoolean( 'CalcularAguinaldo', CBAguinaldoCalcular.Checked );
          AddInteger( 'MesAguinaldo', ZKAguinaldoMes.Valor );
          AddInteger( 'PeriodoAguinaldo', ZKAguinaldoPeriodo.Valor );
          AddBoolean( 'CalcularPTU', CBPTUCalcular.Checked );
          AddInteger( 'MesPTU', ZKPTUMes.Valor );
          AddInteger( 'PeriodoPTU', ZKPTUPeriodo.Valor );
          // (JB) Escenarios de presupuestos T1060 CR1872
          //      Se agregan parametros
          AddString( 'Escenario', ES_CODIGO.Llave );
          AddString( 'EscenarioDescripcion', ES_CODIGO.Descripcion );
     end;
     with FParameterAguinaldo do
     begin
          // Calculo
          AddInteger( 'Year', YearPresup.ValorEntero );
          AddInteger( 'Proporcional', iMax( 0, RGAguinaldoProp.ItemIndex ) );
          AddDate( 'dInicial', ZAguinaldoFechaIni.Valor );
          AddDate( 'dFinal', ZAguinaldoFechaFin.Valor );
          AddString( 'Faltas', AguinaldoFaltas.Text );
          AddString( 'Incapacidades', AguinaldoIncapacidades.Text );
          AddString( 'Anticipos', AguinaldoAnticipos.Text );
          // Pago
          AddInteger( 'YearAguinaldo', YearPresup.ValorEntero );
     end;
     with FParameterPTU do
     begin
          // Calculo
          AddInteger( 'Year', YearPresup.ValorEntero );
          AddFloat( 'Utilidad', ZNPTUUtilidad.Valor );
          AddFloat( 'SalarioTope', ZNPTUSalarioTope.Valor );
          AddString( 'Percepciones', PTUPercepciones.Text );
          AddString( 'SumaDias', PTUSumaDias.Text );
          AddString( 'FiltroDirector', PTUFiltroDirector.Text );
          AddString( 'FiltroPlanta', PTUFiltroPlanta.Text );
          AddString( 'FiltroEventual', PTUFiltroEventual.Text );
          // Pago
          AddInteger( 'YearPTU', YearPresup.ValorEntero );
          AddInteger( 'Concepto', KLPTUConcepto.Valor );
     end;

     with Descripciones do
     begin
          AddString( 'Escenario', ES_CODIGO.Llave + ': ' + ES_CODIGO.Descripcion );
          AddInteger( 'A�o', YearPresup.ValorEntero );

          //calculo de nominas
          if ( RBRangoMeses.Checked ) then
          begin
               AddString( 'Calcular n�minas', RBRangoMeses.Caption);
               AddString( 'Mes inicial', ObtieneElemento( lfMeses, ord( MesInicial.Valor) - 1 ) );
               AddString( 'Mes final', ObtieneElemento( lfMeses, ord( MesFinal.Valor ) - 1 ) );
          end
          else if ( RBRangoNominas.Checked ) then
          begin
               AddString( 'Calcular n�minas', RBRangoNominas.Caption);
          end;

          //aplicar supuestos de kardex
          if ( RBRangoSupuestos.Checked ) then
          begin
               AddString( 'Aplicar supuestos kardex', RBRangoSupuestos.Caption);
               AddDate( 'Fecha inicial', FechaRH1.Valor );
               AddDate( 'Fecha final', FechaRH2.Valor );
          end
          else if ( RBTodosSupuestos.Checked ) then
          begin
               AddString( 'Aplicar supuestos kardex', RBTodosSupuestos.Caption);
          end;

          //calcular aguinaldo
          AddBoolean( 'Calcular aguinaldo', CBAguinaldoCalcular.Checked );
         { if ( CBAguinaldoCalcular.Checked ) then
          begin
               AddString( 'Aguinaldo: Pagar en mes', ObtieneElemento( lfMeses, ord( ZKAguinaldoMes.Valor ) - 1 ) );
               AddString( 'Aguinaldo: Periodo', ObtieneElemento( lfTipoPeriodoMes, ZKAguinaldoPeriodo.Valor ) );
               AddDate( 'Aguinaldo: Fecha inicial', FechaRH1.Valor );
               AddDate( 'Aguinaldo: Fecha final', FechaRH2.Valor );
               AddString( 'Aguinaldo: descontar faltas', AguinaldoFaltas.Text );
               AddString( 'Aguinaldo: Descontar incap.', AguinaldoIncapacidades.Text );
               AddString( 'Aguinaldo: Descontar antic.', AguinaldoAnticipos.Text );
               AddString( 'Aguinaldo: Proporcional a ', RGAguinaldoProp.Properties.Items[RGAguinaldoProp.ItemIndex].Caption );
          end;
          }
          //calcular PTU
          AddBoolean( 'Calcular PTU', CBPTUCalcular.Checked );
         { if ( CBPTUCalcular.Checked ) then
          begin
               AddString( 'PTU: Pagar en mes', ObtieneElemento( lfMeses, ord( ZKPTUMes.Valor ) - 1 )  );
               AddString( 'PTU: Periodo',  ObtieneElemento( lfTipoPeriodoMes , ZKPTUPeriodo.Valor ) );
               AddFloat( 'PTU: Utilidad', ZNPTUUtilidad.Valor );
               AddString( 'PTU: Suma d�as', PTUSumaDias.Text );
               AddString( 'PTU: Percepciones', PTUPercepciones.Text );
               AddString( 'PTU: Filtro director', PTUFiltroDirector.Text );
               AddString( 'PTU: Filtro planta', PTUFiltroPlanta.Text );
               AddString( 'PTU: Filtro eventual', PTUFiltroEventual.Text );
               AddFloat( 'PTU: Salario tope', ZNPTUSalarioTope.Valor );
               AddString( 'PTU: Concepto', KLPTUConcepto.Llave + ': ' + KLPTUConcepto.Descripcion ) ;
          end;
          }

     end;

end;

procedure TWizPresupSimulacion_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     ParametrosControl := nil;  //DevEx
     with Wizard do
     begin
          if CanMove and Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if ( YearPresup.ValorEntero <= 0 ) then
                       CanMove := Error( '� A�o no fu� especificado !', YearPresup )
                    else if ( MesInicial.Valor > MesFinal.Valor ) then
                       CanMove := Error( '� Mes final debe ser mayor o igual al inicial !', MesFinal )
                    // (JB) Escenarios de presupuestos T1060 CR1872
                    else if ( strVacio( ES_CODIGO.Llave ) ) then
                       CanMove := Error( '� Debe seleccionar un Escenario de Presupuestos !', ES_CODIGO );
               end
               else if EsPaginaActual( TabPTU ) and ( CBPTUCalcular.Checked ) then
               begin
                    if ( ZNPTUUtilidad.Valor <= 0 ) then
                       CanMove := Error( '� Utilidad a repartir debe ser mayor que cero !', ZNPTUUtilidad )
                    else if ZetaCommonTools.StrVacio( PTUSumaDias.Text ) then
                       CanMove := Error( '� F�rmula de suma de d�as no fu� especificada !', PTUSumaDias )
                    else if ZetaCommonTools.StrVacio( PTUPercepciones.Text ) then
                       CanMove := Error( '� F�rmula de percepciones no fu� especificada !', PTUPercepciones )
                    else if ( KLPTUConcepto.Valor = 0 ) then
                       CanMove := Error( '� No se especific� el concepto de n�mina !', KLPTUConcepto );
               end;
          end;
     end;
end;

procedure TWizPresupSimulacion_DevEx.InitFechasAguinaldo;
begin
     with YearPresup do
     begin
          ZAguinaldoFechaIni.Valor := FirstDayOfYear( ValorEntero );
          ZAguinaldoFechaFin.Valor := LastDayOfYear( ValorEntero );
     end;
end;

procedure TWizPresupSimulacion_DevEx.InitFechasSupuestos;
begin
     with YearPresup do
     begin
          if ( MesInicial.Valor <= 12 ) then    // Si es Mes13 no se puede sugerir fecha
             FechaRH1.Valor := FirstDayOfMonth( EncodeDate( ValorEntero, MesInicial.Valor, 1 ) )
          else
              FechaRH1.Valor := FirstDayOfYear( ValorEntero );
          if ( MesFinal.Valor <= 12 ) then      // Si es Mes13 no se puede sugerir fecha
             FechaRH2.Valor := LastDayOfMonth( EncodeDate( ValorEntero, MesFinal.Valor, 1 ) )
          else
              FechaRH2.Valor := LastDayOfYear( ValorEntero );
     end;
end;

procedure TWizPresupSimulacion_DevEx.YearPresupExit(Sender: TObject);
begin
     inherited;
     InitFechasSupuestos;
     InitFechasAguinaldo;
end;

procedure TWizPresupSimulacion_DevEx.MesesChange(Sender: TObject);
begin
     inherited;
     InitFechasSupuestos;
end;

procedure TWizPresupSimulacion_DevEx.RBTodosSupuestosClick(Sender: TObject);
var
   lEnabled : Boolean;
begin
     inherited;
     lEnabled := ( RBRangoSupuestos.Checked );
     LblFechaRH1.Enabled := lEnabled;
     FechaRH1.Enabled := lEnabled;
     LblFechaRH2.Enabled := lEnabled;
     FechaRH2.Enabled := lEnabled;
end;

procedure TWizPresupSimulacion_DevEx.CBAguinaldoCalcularClick(Sender: TObject);
begin
     inherited;
     ZetaClientTools.SetEnabledControl( GBAguinaldo, CBAguinaldoCalcular.Checked );
end;

procedure TWizPresupSimulacion_DevEx.CBPTUCalcularClick(Sender: TObject);
begin
     inherited;
     ZetaClientTools.SetEnabledControl( GBPTU, CBPTUCalcular.Checked );
end;

procedure TWizPresupSimulacion_DevEx.BtnFaltasClick(Sender: TObject);
begin
     inherited;
     AguinaldoFaltas.Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, AguinaldoFaltas.Text, AguinaldoFaltas.SelStart, evAguinaldo );
end;

procedure TWizPresupSimulacion_DevEx.BtnIncapacidadesClick(Sender: TObject);
begin
     inherited;
     AguinaldoIncapacidades.Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, AguinaldoIncapacidades.Text, AguinaldoIncapacidades.SelStart, evAguinaldo );
end;

procedure TWizPresupSimulacion_DevEx.BtnAnticiposClick(Sender: TObject);
begin
     inherited;
     AguinaldoAnticipos.Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado , AguinaldoAnticipos.Text, AguinaldoAnticipos.SelStart, evAguinaldo );
end;

procedure TWizPresupSimulacion_DevEx.BtnSumaDiasClick(Sender: TObject);
begin
     inherited;
     PTUSumaDias.Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, PTUSumaDias.Text, PTUSumaDias.SelStart, evBase );
end;

procedure TWizPresupSimulacion_DevEx.BtnPercepcionesClick(Sender: TObject);
begin
     inherited;
     PTUPercepciones.Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, PTUPercepciones.Text, PTUPercepciones.SelStart, evBase );
end;

procedure TWizPresupSimulacion_DevEx.BtnFiltroDirectorClick(Sender: TObject);
begin
     inherited;
     PTUFiltroDirector.Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, PTUFiltroDirector.Text, PTUFiltroDirector.SelStart, evBase );
end;

procedure TWizPresupSimulacion_DevEx.BtnFiltroPlantaClick(Sender: TObject);
begin
     inherited;
     PTUFiltroPlanta.Text := ZConstruyeFormula.GetFormulaConst( enEmpleado, PTUFiltroPlanta.Text, PTUFiltroPlanta.SelStart, evBase );
end;

procedure TWizPresupSimulacion_DevEx.BtnFiltroEventualClick(Sender: TObject);
begin
     inherited;
     PTUFiltroEventual.Text := ZConstruyeFormula.GetFormulaConst( enEmpleado, PTUFiltroEventual.Text, PTUFiltroEventual.SelStart, evBase );
end;

procedure TWizPresupSimulacion_DevEx.CallMeBackStartLocal(Sender: TObject; const sMsg: String; const iMax: Integer);
begin
     with ProgressBarGlobal do
     begin
          //Max := iMax;
          Properties.Max := iMax;
          //Step := 1;
     end;

     with ProgressPanelGlobal do
     begin
          if strVacio( sMsg ) then
               Caption := 'Iniciando presupuesto'
          else
              Caption := sMsg;
     end;
end;

procedure TWizPresupSimulacion_DevEx.CallMeBackLocal(Sender: TObject; const sMsg: String; const iStep: Integer; var Continue: Boolean);
begin
     with ProgressBarGlobal do
     begin
          Position := iStep;
          with ProgressPanelGlobal do
          begin
               if strVacio( sMsg ) and ( Properties.Max > 0 ) then
                  Caption := Format( 'Avance %4.1n', [ 100 * iStep / Properties.Max ] ) + ' %'
               else
                   Caption := sMsg;
          end;
     end;
     Application.ProcessMessages;
     Continue := not Wizard.Cancelado;
end;

procedure TWizPresupSimulacion_DevEx.CallMeBackEndLocal(Sender: TObject; const sMsg: String);
begin
     with ProgressBarGlobal do
     begin
          Position := Properties.Max;
     end;
     with ProgressPanelGlobal do
     begin
          if strVacio( sMsg ) then
             Caption := 'Presupuesto terminado'
          else
              Caption := sMsg;
     end;
end;

function TWizPresupSimulacion_DevEx.EjecutarWizard: Boolean;
begin
     try
{$ifndef DOS_CAPAS}
 //*       AnimationPanel.Visible := TRUE;
{$endif}
        with dmPresupuestos do
        begin
             OnCallBackStart := CallMeBackStartLocal;
             OnCallBack := CallMeBackLocal;
             OnCallBackEnd := CallMeBackEndLocal;
             Result := SimularPresupuesto( ParameterList, FParameterAguinaldo, FParameterPTU );

             //DevEx
             if Result then
                ZInformation(Caption, 'La simulaci�n termin� correctamente.', 0)
             else
                 ZError(Caption, 'Error en proceso de simulaci�n.', 0);
        end;
     finally
            with dmPresupuestos do
            begin
                 OnCallBackStart := nil;
                 OnCallBack := nil;
                 OnCallBackEnd := nil;
            end;
{$ifndef DOS_CAPAS}
 //*           AnimationPanel.Visible := FALSE;
{$endif}
     end;
end;


procedure TWizPresupSimulacion_DevEx.RBRangoNominasClick(Sender: TObject);
var
   lEnabled : Boolean;
begin
     inherited;
     lEnabled := ( RBRangoMeses.Checked );
     lblMesInicial.Enabled := lEnabled;
     MesInicial.Enabled := lEnabled;
     lblMesFinal.Enabled := lEnabled;
     MesFinal.Enabled := lEnabled;

     RBTodosSupuestos.Checked := not lEnabled;
     RBRangoSupuestos.Checked := lEnabled;
end;

procedure TWizPresupSimulacion_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := YearPresup
     else
         ParametrosControl := nil;
     inherited;
end;

end.
