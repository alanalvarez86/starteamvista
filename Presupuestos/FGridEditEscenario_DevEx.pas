unit FGridEditEscenario_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, StdCtrls,
  ZetaNumero, Mask, ZetaEdit, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TGridEditEscenario_DevEx = class(TBaseEdicion_DevEx)
    DBCodigoLBL: TLabel;
    lblNombre: TLabel;
    DBInglesLBL: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    ES_CODIGO: TZetaDBEdit;
    ES_ELEMENT: TDBEdit;
    ES_INGLES: TDBEdit;
    ES_NUMERO: TZetaDBNumero;
    ES_TEXTO: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
     procedure Connect; override;
  public
    { Public declarations }
  end;

var
  GridEditEscenario_DevEx: TGridEditEscenario_DevEx;

implementation

uses DPresupuestos,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.dfm}

{ TGridEditEscenario }

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TGridEditEscenario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := ES_CODIGO;
     IndexDerechos := D_PRESUP_ESCENARIOS;
     HelpContext:= H00001_Escenarios_de_presupuesto;

end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TGridEditEscenario_DevEx.Connect;
begin
     with dmPresupuestos do
     begin
          DataSource.DataSet := cdsEscenarios;
     end;
end;

end.
