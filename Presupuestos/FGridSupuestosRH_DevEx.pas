unit FGridSupuestosRH_DevEx;

interface

uses
  Windows, Messages, SysUtils, {$ifndef VER130}Variants,{$endif} Classes, Graphics, Controls, Forms,
  Dialogs,  DB,  DBCtrls,
  ExtCtrls, StdCtrls, ZetaKeyCombo, Mask,
  ZetaFecha, ZetaMessages, ZBaseGridEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons, Grids,
  DBGrids, ZetaDBGrid;

type
  TGridSupuestosRH_DevEx = class(TBaseGridEdicion_DevEx)
    ZFecha: TZetaDBFecha;
    zCombo: TZetaDBKeyCombo;
    btnImportarSupuestos: TdxBarButton; // (JB) Escenarios de presupuestos T1060 CR1872
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridColEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnImportarSupuestosClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEvento;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure PreparaNuevoRegistro;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Buscar; override;
    procedure Agregar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    procedure Disconnect; override;
  public
    { Public declarations }
  end;

var
  GridSupuestosRH_DevEx: TGridSupuestosRH_DevEx;

implementation

uses dSistema, dCatalogos, DPresupuestos,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaBusqueda_DevEx,
     DBClient;

{$R *.dfm}

procedure TGridSupuestosRH_DevEx.FormShow(Sender: TObject);
begin
     inherited;

     if ( dmPresupuestos.cdsSupuestosRH.State = dsInsert ) then

        PreparaNuevoRegistro
     else
     begin
          ZetaDBGrid.SelectedField := dmPresupuestos.cdsSupuestosRH.FieldByName( 'SR_FECHA' );
          Application.ProcessMessages;
     end;
end;

procedure TGridSupuestosRH_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos:= D_PRESUP_SUPUESTO_PER;
     //(JB) Escenarios de presupuestos T1060 CR1872
     //     Se agrega el titulo a la ventana con el nombre del Escenario que se esta trabajando.
     Caption := 'Supuestos de Personal: Escenario ' + dmPresupuestos.cdsEscenarios.FieldByName('ES_ELEMENT').AsString;
     HelpContext := H_SUPUESTOS_RH;
end;

procedure TGridSupuestosRH_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmCatalogos.cdsEventos.Conectar;
     with dmPresupuestos do
     begin
          cdsEventosAltas.Conectar;
          cdssupuestosRH.Conectar;
          DataSource.DataSet:= cdsSupuestosRH;
     end;
end;

procedure TGridSupuestosRH_DevEx.Agregar;
begin
     inherited;
     PreparaNuevoRegistro;
//     Application.ProcessMessages;
//CV_  SeleccionaPrimerColumna; //Se elimina esta linea : 23-mar-2011
end;

procedure TGridSupuestosRH_DevEx.PreparaNuevoRegistro;
begin
     ZetaDBGrid.SelectedField := dmPresupuestos.cdsSupuestosRH.FieldByName( 'SR_CUANTOS' );
     SendMessage( ZetaDBGrid.Handle, WM_CHAR, Word( Chr( 48 ) ), 0 );
     keybd_event( 13, 0, 0, 0 );

     keybd_event( 36, 0, 0, 0 );
end;

procedure TGridSupuestosRH_DevEx.Buscar;
begin
     if ( ZetaDBGrid.SelectedField.FieldName = 'EV_CODIGO' ) then
        BuscaEvento;
end;

procedure TGridSupuestosRH_DevEx.BuscaEvento;
var
   sCodigo, sDescripcion: String;
   oLookupDataSet : TZetaLookupDataSet;
begin
     with dmPresupuestos.cdssupuestosRH do
     begin
          sCodigo := FieldByName( 'EV_CODIGO' ).AsString;
          if ( eTipoSupuestoRH( FieldByName( 'SR_TIPO' ).AsInteger ) = srEvento ) then
             oLookupDataSet := dmCatalogos.cdsEventos
          else
              oLookupDataSet := dmPresupuestos.cdsEventosAltas;

          if oLookupDataSet.Search_DevEx( Format( '( EV_ACTIVO = ''%s'' )', [ K_GLOBAL_SI ] ), sCodigo, sDescripcion ) then
          begin
               if ( sCodigo <> FieldByName( 'EV_CODIGO' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'EV_CODIGO' ).AsString := sCodigo;
               end;
          end;
     end;
end;

procedure TGridSupuestosRH_DevEx.KeyPress(var Key: Char);
begin
     //inherited;
     if GridEnfocado then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               if ( ZetaDBGrid.SelectedField.FieldName = 'EV_CODIGO' ) or
                  ( ZetaDBGrid.SelectedField.FieldName = 'DESCRIPCION' ) or
                  ( ZetaDBGrid.SelectedField.FieldName = 'PRIORIDAD' ) then
               begin
                    ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
                    SeleccionaSiguienteRenglon;
               end
               else
                   ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   SeleccionaPrimerColumna;
              end;

          if ( ( Key <> Chr(9) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'SR_FECHA' ) then
               begin
                    if ( zFecha.Visible ) then
                    begin
                         zFecha.SetFocus;
                         SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                         Key := #0;
                    end;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'SR_TIPO' ) then
               begin
                    if ( zCombo.Visible ) then
                    begin
                         zCombo.SetFocus;
                         SendMessage( zCombo.Handle, WM_Char, Word( Key ), 0 );
                         Key := #0;
                    end;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'EV_CODIGO' ) and ZetaCommonTools.EsMinuscula( Key ) then
               begin
                    Key := Chr( Ord( Key ) - 32 );
               end;
          end;
     end
     else
     begin
          if ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
          begin
               if ( ActiveControl = zFecha ) then
               begin
                    Key := #0;
                    with ZetaDBGrid do
                    begin
                         SetFocus;
                         SelectedField := dmPresupuestos.cdsSupuestosRH.FieldByName( 'SR_TIPO' );
                    end;
               end
               else if ( ActiveControl = zCombo ) then
               begin
                    Key := #0;
                    with ZetaDBGrid do
                    begin
                         SetFocus;
                         SelectedField := dmPresupuestos.cdsSupuestosRH.FieldByName( 'SR_CUANTOS' );
                    end;
               end;
          end;
     end;
end;

procedure TGridSupuestosRH_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'SR_FECHA' ) and ( zFecha.Visible ) then
                   zFecha.Visible:= False
               else if ( SelectedField.FieldName = 'SR_TIPO' ) and ( zCombo.Visible ) then
                  zCombo.Visible := False;
          end;
     end;
end;

procedure TGridSupuestosRH_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
          State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) and ( Column.Visible ) then
     begin
          if ( Column.FieldName = 'SR_FECHA' ) then
          begin
               with zFecha do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    //Width := Column.Width + 2;
                    Width := Rect.Right - Rect.Left + 2;
                    Visible := True;
                    Valor := Column.Field.AsDateTime;
               end;
          end;
          if ( Column.FieldName = 'SR_TIPO' ) then
          begin
               with zCombo do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    Width := Rect.Right - Rect.Left + 2;
                    Visible := True;
                    Valor := Column.Field.AsInteger;
               end;
          end;
     end;
     with ZetaDBGrid do
     begin
          with Canvas.Font do
          begin
               if ( not zStrToBool( dmPresupuestos.cdsSupuestosRH.FieldByName( 'ACTIVO' ).AsString ) ) then
                  Color := clRed;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

procedure TGridSupuestosRH_DevEx.ZetaDBGridColEnter(Sender: TObject);
begin
     inherited;
     if ( ZetaDBGrid.SelectedIndex = 0 ) then
        ZetaDBGrid.SelectedIndex := 1;
end;

procedure TGridSupuestosRH_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          with ZetaDBGrid do
          begin
               if ( SelectedField.FieldName = 'EV_CODIGO' ) or
                  ( SelectedField.FieldName = 'DESCRIPCION' ) or
                  ( SelectedField.FieldName = 'PRIORIDAD' ) then
               begin
                    SelectedIndex := PosicionaSiguienteColumna;
                    SeleccionaSiguienteRenglon;
               end
               else
                   ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
          end;
     end;
end;

// (JB) Escenarios de presupuestos T1060 CR1872
procedure TGridSupuestosRH_DevEx.btnImportarSupuestosClick(Sender: TObject);
var
     sEscenario, sDescription, sOldEscenario : String;
     begin
     sOldEscenario := dmPresupuestos.EscenarioActivo; //Escenario Activo
     with dmPresupuestos do
     begin
          //cdsSupuestosRH.Active := False;
          //cdsSupuestosRH.DisableControls;
          try
             if ShowSearchForm ( cdsEscenarios,
                              Format( 'ES_CODIGO <> %s', [ Comillas( sOldEscenario ) ] ) ,
                              sEscenario,
                              sDescription ) then
                  CopiaSupuestos( sEscenario, sOldEscenario );
          finally
                 //cdsSupuestosRH.EnableControls;
          end;
     end;
end;

procedure TGridSupuestosRH_DevEx.Disconnect;
begin
     dmPresupuestos.cdsSupuestosRH.Refrescar;
     inherited;
end;

end.
