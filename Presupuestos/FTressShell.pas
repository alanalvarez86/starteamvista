unit FTressShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FTressShell.pas                            ::
  :: Descripci�n: Programa principal de Tress                ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, ExtCtrls, ToolWin, Mask, DBCtrls, Db, ImgList,
     StdCtrls, DBGrids, ActnList,
     ZBaseConsulta,
     ZBaseShell,
     ZetaClientDataset,
     ZetaCommonLists,
     ZetaDBTextBox,
     ZetaDespConsulta,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaMessages,
     ZetaStateComboBox,
     ZetaTipoEntidad,
     cxPCdxBarPopupMenu, cxGraphics,
     cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
     dxSkinsdxRibbonPainter, dxSkinsdxNavBarPainter, dxSkinsdxBarPainter,
     cxLocalization, dxBar, dxSkinsForm, dxNavBar, cxButtons, cxClasses,
     dxRibbon, cxPC, cxContainer, cxEdit, cxImage, dxGDIPlusClasses, cxLabel,
     cxTextEdit, cxGroupBox, dxBarExtItems, dxNavBarBase, dxNavBarCollns,
     cxTreeView, cxCustomData, cxStyles, cxTL, cxTLdxBarBuiltInMenu,
     cxInplaceContainer, cxMaskEdit, cxDropDownEdit, ZetaCXStateComboBox,
     dxBarExtDBItems, dxStatusBar, dxRibbonStatusBar, cxBarEditItem, cxButtonEdit,
     dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,
     dxSkinscxPCPainter, ZBaseNavBarShell, Menus, System.Actions,
  dxBarBuiltInMenu, dxRibbonCustomizationForm;

type

  TTressShell = class(TBaseNavBarShell)
    _Emp_Primero: TAction;
    _Emp_Anterior: TAction;
    _Emp_Siguiente: TAction;
    _Emp_Ultimo: TAction;
    _Emp_Baja: TAction;
    _Emp_Reingreso: TAction;
    _Emp_Cambio_Salario: TAction;
    _Emp_Cambio_Turno: TAction;
    _Emp_Cambio_Puesto: TAction;
    _Emp_Cambio_Area: TAction;
    _Emp_Cambio_Contrato: TAction;
    _Emp_Cambio_Prestacion: TAction;
    _Emp_Cambios_Multiples: TAction;
    _Emp_Vacaciones: TAction;
    _Emp_Cerrar_Vacaciones: TAction;
    _Emp_Incapacidad: TAction;
    _Emp_Permiso: TAction;
    _Emp_Kardex: TAction;
    _Emp_Curso: TAction;
    _Emp_Comidas: TAction;
    _Emp_Recalcular_Integrados: TAction;
    _Emp_Promediar_Variables: TAction;
    _Emp_Salario_Global: TAction;
    _Emp_Aplicar_Tabulador: TAction;
    _Emp_Vacaciones_Globales: TAction;
    _Emp_Cancelar_Vacaciones: TAction;
    _Emp_CierreGlobal_Vacaciones: TAction;
    _Emp_Aplicar_Eventos: TAction;
    _Emp_Importar_Kardex: TAction;
    _Emp_Cancelar_Kardex: TAction;
    _Emp_Curso_Global: TAction;
    _Emp_Transferencia: TAction;
    _Per_Primero: TAction;
    _Per_Anterior: TAction;
    _Per_Siguiente: TAction;
    _Per_Ultimo: TAction;
    _Per_NominaNueva: TAction;
    _Per_BorrarNominaActiva: TAction;
    _Per_ExcepcionDiaHora: TAction;
    _Per_ExcepcionMonto: TAction;
    _Per_ExcepcionesGlobales: TAction;
    _Per_Liquidacion: TAction;
    _Per_PagoRecibos: TAction;
    _Per_CalcularNomina: TAction;
    _Per_AfectarNomina: TAction;
    _Per_FoliarRecibos: TAction;
    _Per_ImprimirListado: TAction;
    _Per_ImprimirRecibos: TAction;
    _Per_GenerarPoliza: TAction;
    _Per_ImprimirPoliza: TAction;
    _Per_ExportarPoliza: TAction;
    _Per_Desafectar: TAction;
    _Per_LimpiarAcumulados: TAction;
    _Per_RecalcularAcumulados: TAction;
    _Per_ImportarMovimientos: TAction;
    _Per_ExportarMovimientos: TAction;
    _Per_ImportarPagoRecibos: TAction;
    _Per_RefoliarRecibos: TAction;
    _Per_CopiarNomina: TAction;
    _Per_CalcularDiferencias: TAction;
    _Per_PagarFuera: TAction;
    _Per_CancelarPasadas: TAction;
    _Per_LiquidacionGlobal: TAction;
    _Per_CalcularNetoBruto: TAction;
    _Per_RastrearCalculo: TAction;
    _Per_DefinirPeriodos: TAction;
    _Per_CalcularAguinaldos: TAction;
    _Per_ImprimirAguinaldos: TAction;
    _Per_PagarAguinaldos: TAction;
    _Per_CalcularReparto: TAction;
    _Per_ImprimirReparto: TAction;
    _Per_PagarReparto: TAction;
    _Per_CierreAhorros: TAction;
    _Per_CierrePrestamos: TAction;
    _Per_CalcularPTU: TAction;
    _Per_ImprimirPTU: TAction;
    _Per_PagarPTU: TAction;
    _Per_CalcularDeclaracion: TAction;
    _Per_ImprimirDeclaracion: TAction;
    _Per_CalcularComparativo: TAction;
    _Per_ImprimirComparativo: TAction;
    _Per_PagarComparativo: TAction;
    _Emp_EntregaHerramienta: TAction;
    _Emp_EntregaGlobal: TAction;
    _Emp_RegresarHerramienta: TAction;
    _Emp_Entregar_Herramienta: TAction;
    _Emp_Regresar_Herramienta: TAction;
    _A_Preferencias: TAction;

    _E_BuscarEmpleado: TAction;
    _E_CambiarEmpleado: TAction;

    _V_ArbolOriginal: TAction;
    _V_Empleados: TAction;
    _V_Asistencia: TAction;
    _V_Nomina: TAction;
    _V_PagosIMSS: TAction;
    _V_Sistema: TAction;

    _Per_CreditoAplicadoMensual: TAction;
    _Emp_Importar_Altas: TAction;
    _Emp_Renumerar: TAction;
    _Emp_Banca_Elec: TAction;
    _Emp_Credito_Infonavit: TAction;
    _Emp_Permiso_Global: TAction;
    _Emp_Prestamos: TAction;
    _Per_CalculaRetroactivos: TAction;
    _Emp_Borrar_Curso_Global: TAction;
    _Per_DeclaracionAnual: TAction;
    _Per_ImprimirDeclaracionAnual: TAction;
    _Emp_Registro_Comidas_Grupales: TAction;
    _Emp_Corregir_Fechas_Caf: TAction;
    _Emp_Importar_Asistencia_Sesiones: TAction;
    _Emp_Cancelar_Cierre_Vacaciones: TAction;
    _Simulacion_Presupuestos: TAction;

    _Imprime_simulacion: TAction;

    _Importar_Kardex: TAction;

    _GenerarPoliza: TAction;
    _ImprimirPoliza: TAction;
    _ExportarPoliza: TAction;
    _PrepararPresupuestos: TAction;

    TabPresupuestos: TdxRibbonTab;
    ToolBarDefinir: TdxBar;
    ToolBarNomina: TdxBar;
    dxBarLargeButtonPeriodos: TdxBarLargeButton;
    dxBarLargeButtonSimulacionPresupuestos: TdxBarLargeButton;
    dxBarLargeButtonImportarKardex: TdxBarLargeButton;
    dxBarLargeButtonImprimirSimulacion: TdxBarLargeButton;
    dxBarLargeButtonPolizas: TdxBarLargeButton;
    dxBarSubItemPolizas: TdxBarSubItem;
    dxBarButtonGenerarPoliza: TdxBarButton;
    dxBarButtonExportarPoliza: TdxBarButton;
    dxBarButtonImpirmirPoliza: TdxBarButton;
    dxBarLargeButtonAgregarBaseDatos: TdxBarLargeButton;
    ToolBarSimulacion: TdxBar;
    ToolBarCambios: TdxBar;
    dxBarLargeButtonImportatKardex: TdxBarLargeButton;
    ToolBarBaseDatos: TdxBar;
    dxBarSubItem1: TdxBarSubItem;
    TabArchivo_btnPreferencias: TdxBarLargeButton;
    Image24_StatusEmpleado: TcxImageList;
    PanelValorAEmpleado: TPanel;
    PanelValorAEmpleado_2: TPanel;
    EmpleadoPrettyName_DevEx: TLabel;
    ImagenStatus_DevEx: TcxImage;
    PanelValorEmpleado_1: TPanel;
    EmpleadoLBL_DevEx: TLabel;
    btnBuscarEmp_DevEx: TcxButton;
    btnEmplPrimero_DevEx: TcxButton;
    btnEmpAnterior_DevEx: TcxButton;
    btnEmpSiguiente_DevEx: TcxButton;
    btnEmpUltimo_DevEx: TcxButton;
    EmpleadoNumeroCB_DevEx: TcxStateComboBox;
    PanelContenedor: TPanel;
    PanelValorNomina: TPanel;
    PanelControlesNomina: TPanel;
    PeriodoTipoLBL_DevEx: TLabel;
    PeriodoPrimero_DevEx: TcxButton;
    PeriodoAnterior_DevEx: TcxButton;
    PeriodoSiguiente_DevEx: TcxButton;
    PeriodoUltimo_DevEx: TcxButton;
    PeriodoTipoCB_DevEx2: TcxStateComboBox;
    PeriodoNumeroCB_DevEx2: TcxStateComboBox;
    PanelRangoFechas: TPanel;
    PanelPeriodoStatus: TPanel;
    ToolBarStatusBarAnio : TdxBar;

    btnYearBefore_DevEx: TdxBarButton;
    btnYearNext_DevEx: TdxBarButton;
    dxStaticFechaSistema: TdxBarStatic;
    dxStaticFechaSistemaValor: TdxBarStatic;
    SistemaYear_DevEx: TdxBarEdit;
    dxBarStaticAnio: TdxBarStatic;
    dxStaticSistemaFechaDia: TdxBarStatic;
    dxBarSubItemoSimulacion: TdxBarSubItem;
    dxBarButtonSimulacionPresupuestos: TdxBarButton;
    dxBarButton4: TdxBarButton;
    
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure PeriodoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure PeriodoTipoCBLookUp(Sender: TObject; var lOk: Boolean);

    procedure _A_CatalogoUsuariosExecute(Sender: TObject);
    procedure _A_PreferenciasExecute(Sender: TObject);
    procedure _E_BuscarEmpleadoExecute(Sender: TObject);
    procedure _E_CambiarEmpleadoExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _E_RefrescarUpdate(Sender: TObject);
    procedure _V_ArbolOriginalExecute(Sender: TObject);
    procedure _V_EmpleadosExecute(Sender: TObject);
    procedure _V_NominaExecute(Sender: TObject);
    procedure _V_SistemaExecute(Sender: TObject);
    procedure _Emp_PrimeroExecute(Sender: TObject);
    procedure _Emp_PrimeroUpdate(Sender: TObject);
    procedure _Emp_AnteriorExecute(Sender: TObject);
    procedure _Emp_SiguienteExecute(Sender: TObject);
    procedure _Emp_UltimoExecute(Sender: TObject);
    procedure _Emp_UltimoUpdate(Sender: TObject);
    procedure _Per_PrimeroExecute(Sender: TObject);
    procedure _Per_AnteriorExecute(Sender: TObject);
    procedure _Per_AnteriorUpdate(Sender: TObject);
    procedure _Per_SiguienteExecute(Sender: TObject);
    procedure _Per_SiguienteUpdate(Sender: TObject);
    procedure _Per_UltimoExecute(Sender: TObject);
    procedure _Per_DefinirPeriodosExecute(Sender: TObject);
    procedure _Simulacion_PresupuestosExecute(Sender: TObject);
    procedure _Imprime_simulacionExecute(Sender: TObject);
    procedure _Importar_KardexExecute(Sender: TObject);  
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
    procedure _Per_ImprimirPolizaExecute(Sender: TObject);
    procedure _Per_ExportarPolizaExecute(Sender: TObject);
    procedure _PrepararPresupuestosExecute(Sender: TObject);
    procedure btnYearBefore_DevExClick(Sender: TObject);
    procedure btnYearNext_DevExClick(Sender: TObject);
    procedure SistemaYear_DevExPropertiesChange(Sender: TObject);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure ImagenStatus_DevExDblClick(Sender: TObject);

  private
    { Private declarations }
    FTempPanelRangoFechas:String;

//    function ValidaStatusNomina(const sProceso: String): Boolean;
    procedure CambioEmpleadoActivo;
    procedure CargaPeriodoActivos;
    procedure CargaSistemaActivos;
    procedure RefrescaPeriodoActivos;
    procedure RefrescaPeriodoInfo;
    procedure RefrescaEmpleadoActivos;
    procedure RefrescaEmpleadoInfo;
    procedure RevisaDerechos;
    procedure HabilitarBotonesRibbon(Habilitar: Boolean);


    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;

    procedure CalculaAnio ( Operacion:Integer );
    procedure HabilitaBotonesAnio;
    procedure RefrescaSistemaActivos;
  protected
    { Protected declarations }
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String;
             var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean; override;
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    procedure CargaDerechos; override;
    procedure CargaTraducciones; override;
    procedure DoOpenSistema; override;
  public
    { Public declarations }
    function FormaActivaNomina: Boolean;
    function GetLookUpDataSet(const eEntidad: TipoEntidad): TZetaLookUpDataSet;override;
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure CambiaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambioPeriodoActivo;
    procedure CambiaSistemaActivos;
    procedure CargaEmpleadoActivos;
    procedure LLenaOpcionesGrupos; override;

    procedure RefrescaEmpleado;
    procedure RefrescaPeriodo;
    procedure ReinitPeriodo;
    procedure CargaVista; override; //DevEx
    
    {$ifdef PRESUPUESTOS}
    procedure SetDataChange(const Entidades: array of TipoEntidad); override;
    {$else}
    procedure SetDataChange(const Entidades: ListaEntidades); override;
    {$endif}
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure SetAsistencia( const dFecha: TDate );
    procedure RefrescaIMSS;
    procedure AbreFormaSimulaciones(const TipoForma: eFormaConsulta);
    procedure RefrescaNavegacionInfo;
    property TempPanelRangoFechas: String  read FTempPanelRangoFechas write FTempPanelRangoFechas;

  end;

const
     K_PANEL_EMPLEADO = 1;
     K_PANEL_PERIODO = 2;
     K_PANEL_IMSS = 3;
     K_PANEL_NAVEGA = 4;
     K_PANEL_FECHASISTEMA = 5;

     K_MANUAL = 0;
     K_ANTERIOR = 1;
     K_SIGUIENTE = 2;
     K_MAX_YEAR = 2100;
     K_MIN_YEAR = 1899;

     K_MIN_WIDTH = 1165;     //ancho de forma
     K_EXTRA_WIDTH = 5;

var
  TressShell: TTressShell;
  //DevEx(by am): Arreglo para guardar los status del Empleado
  StatusEmpleado: array [0..3] of string = ('Activo', 'Reingreso','Baja','Antes ingreso');


implementation

{$R *.DFM}

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaDialogo,
     ZetaBuscaPeriodo_DevEx,
     ZetaBuscaEmpleado_DevEx,
     ZetaRegistryCliente,
     ZAccesosMgr,
     ZAccesosTress,
     ZArbolTress,
     ZBaseDlgModal_DevEx,
     ZGlobalTress,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
{$endif}
     TressHelp,
     //DAsistencia,
     //DCafeteria,
     DCatalogos,
     DCliente,
     DConsultas,
     DDiccionario,
     DGlobal,
     DNomina,
     DProcesos,
     DRecursos,
     DReportes,
     DSistema,
     DTablas,
     //DConteo,
     FArbolConfigura_DevEx,
     FAutoClasses,
     //FCalendario,
     FEscogeReporte_DevEx, DPresupuestos;

{ ************** TTressShell ****************** }

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmNomina := TdmNomina.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmPresupuestos := TdmPresupuestos.Create( self );

     inherited;
     HelpContext                           := H00001_Pantalla_principal;
     TabArchivo_btnConfigurarEmpresa.HelpContext  := H60651_Globales_empresa;
     btnBuscarEmp_DevEx.HelpContext              := H00012_Busqueda_empleados;
     FHelpGlosario := 'Tress.chm';
     ZAccesosMgr.SetValidacion( TRUE );
     HabilitarBotonesRibbon(False);

end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     //FFormManager.Free;
     inherited;
     dmPresupuestos.Free;
     dmDiccionario.Free;
     dmReportes.Free;
     dmNomina.Free;
     dmRecursos.Free;
     dmTablas.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     dmCliente.Free;
     Global.Free;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
{$endif}
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     CargaVista;
     CargaTraducciones;
end;

//DevEx
procedure TTressShell.CargaVista;
begin
     inherited;
     //Para avisar a los controles que hubo movimientos
     Application.ProcessMessages;
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
     ZetaClientTools.SetEnabled( PanelValorAEmpleado, EmpresaAbierta );
     ZetaClientTools.SetEnabled( PanelValorNomina, EmpresaAbierta );
     btnYearBefore_DevEx.Enabled := EmpresaAbierta;
     SistemaYear_DevEx.Enabled := EmpresaAbierta;
     btnYearNext_DevEx.Enabled := EmpresaAbierta;
     if not EmpresaAbierta then
     begin
          StatusBarMsg( '', K_PANEL_EMPLEADO );
          StatusBarMsg( '', K_PANEL_PERIODO );
     end;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        dmSistema.LeeAdicionales;   // Carga Campos Adicionales - Se ocupa para cuando se conecta Global
        Global.Conectar;
        with dmCliente do
        begin
             InitActivosSistema;
             InitArrayTPeriodo;//acl
             InitActivosEmpleado;
             InitActivosPeriodo;
             with PeriodoTipoCB_DevEx2 do
             begin
                  ListaFija:=lfTipoPeriodo; //acl
                  Llave:= IntToStr( Ord( dmCliente.GetDatosPeriodoActivo.Tipo ) );
                  if ( StrVacio ( Llave ) ) then
                  begin
                       PeriodoTipoCB_DevEx2.Indice:= 0;
                       dmCliente.PeriodoTipo := eTipoPeriodo( StrToInt( PeriodoTipoCB_DevEx2.Llave ) );
                       CambioPeriodoActivo;
                  end;
             end;
              HabilitarBotonesRibbon( EmpresaAbierta );
        end;
        CargaEmpleadoActivos;

        CargaPeriodoActivos;
        CargaSistemaActivos;
        SetArbolitosLength(12);
        CreaNavBar;
        RevisaDerechos;
        LLenaOpcionesGrupos;

     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
end;

procedure TTressShell.CargaDerechos;
begin
     inherited CargaDerechos;
     {$ifndef PRESUPUESTOS}
     En el modulo
     { Pren�mina se utiliza tanto por Asistencia como por N�mina }
     { La forma obtiene sus derechos a partir de la primera ruta }
     { de acceso que es utilizada: si se abre primero por Asistencia }
     { se usa D_ASIS_DATOS_PRENOMINA; si se abre primero por N�mina }
     { se usa D_NOM_DATOS_PRENOMINA }
     ZAccesosMgr.DerechosIguales( D_ASIS_DATOS_PRENOMINA, D_NOM_DATOS_PRENOMINA );
     {$endif}
end;

procedure TTressShell.RevisaDerechos;

{
function Revisa( const iPos: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( iPos, K_DERECHO_CONSULTA );
end;
}

begin
     {$ifndef PRESUPUESTOS}
     _Emp_Alta.Enabled := Revisa( D_EMP_REG_ALTA );
     _Emp_Baja.Enabled := Revisa( D_EMP_REG_BAJA );
     _Emp_Reingreso.Enabled := Revisa( D_EMP_REG_REINGRESO );
     _Emp_Cambio_Salario.Enabled := Revisa( D_EMP_REG_CAMBIO_SALARIO );
     _Emp_Cambio_Turno.Enabled := Revisa( D_EMP_REG_CAMBIO_TURNO );
     _Emp_Cambio_Puesto.Enabled := Revisa( D_EMP_REG_CAMBIO_PUESTO );
     _Emp_Cambio_Area.Enabled := Revisa( D_EMP_REG_CAMBIO_AREA );
     _Emp_Cambio_Contrato.Enabled := Revisa( D_EMP_REG_CAMBIO_CONTRATO );
     _Emp_Cambio_Prestacion.Enabled := Revisa( D_EMP_REG_CAMBIO_PRESTA );
     _Emp_Cambios_Multiples.Enabled := Revisa( D_EMP_REG_CAMBIO_MULTIPLE );
     _Emp_Vacaciones.Enabled := Revisa( D_EMP_REG_VACACIONES );
     _Emp_Cerrar_Vacaciones.Enabled := Revisa( D_EMP_REG_CIERRE_VACA );
     _Emp_Incapacidad.Enabled := Revisa( D_EMP_REG_INCAPACIDAD );
     _Emp_Permiso.Enabled := Revisa( D_EMP_REG_PERMISO );
     _Emp_Kardex.Enabled := Revisa( D_EMP_REG_KARDEX );
     _Emp_Curso.Enabled := Revisa( D_EMP_REG_CURSO_TOMADO );
     _Emp_Comidas.Enabled := Revisa( D_EMP_REG_COMIDAS );
     _Emp_Prestamos.Enabled := Revisa( D_EMP_REG_PRESTAMO );
     _Emp_Banca_Elec.Enabled := CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_BANCA );
     _Emp_EntregaHerramienta.Enabled := Revisa( D_EMP_REG_ENTREGA_TOOLS );
     _Emp_EntregaGlobal.Enabled := Revisa( D_EMP_REG_ENTREGA_GLOBAL );
     _Emp_RegresarHerramienta.Enabled := Revisa( D_EMP_REG_REGRESA_TOOLS );
     _Emp_Recalcular_Integrados.Enabled := Revisa( D_EMP_PROC_RECALC_INT );
     _Emp_Promediar_Variables.Enabled := Revisa( D_EMP_PROC_PROM_VARIABLES );
     _Emp_Salario_Global.Enabled := Revisa( D_EMP_PROC_SALARIO_GLOBAL );
     _Emp_Aplicar_Tabulador.Enabled := Revisa( D_EMP_PROC_TABULADOR );
     _Emp_Vacaciones_Globales.Enabled := Revisa( D_EMP_PROC_VACA_GLOBAL );
     _Emp_Cancelar_Vacaciones.Enabled := Revisa( D_EMP_PROC_CANCELAR_VACA );
     _Emp_CierreGlobal_Vacaciones.Enabled := Revisa( D_EMP_REG_CIERRE_VACA );
     _Emp_Aplicar_Eventos.Enabled := Revisa( D_EMP_PROC_APLICA_EVENTOS );
     // _Emp_Importar_Kardex.Enabled := Revisa( D_EMP_PROC_IMPORTA_KARDEX );
     _Emp_Cancelar_Kardex.Enabled := Revisa( D_EMP_PROC_CANCELA_KARDEX );
     _Emp_Importar_Altas.Enabled := Revisa( D_EMP_PROC_IMPORTAR_ALTAS );
     _Emp_Curso_Global.Enabled := Revisa( D_EMP_PROC_CURSO_GLOBAL );
     _Emp_Renumerar.Enabled := Revisa( D_EMP_PROC_RENUMERA );
     _Emp_Permiso_Global.Enabled := Revisa( D_EMP_PROC_PERMISO_GLOBAL );
     _Emp_Transferencia.Enabled := Revisa( D_EMP_PROC_TRANSFERENCIA );
     _Emp_CierreGlobal_Vacaciones.Enabled := Revisa( D_EMP_PROC_CIERRE_VACACIONES );
     _Emp_Entregar_Herramienta.Enabled := Revisa( D_EMP_PROC_ENTREGAR_HERRAMIENTA );
     _Emp_Regresar_Herramienta.Enabled := Revisa( D_EMP_PROC_REGRESAR_HERRAMIENTA );
     _Emp_Credito_Infonavit.Enabled := CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_SIST_KARDEX );
     _Emp_Borrar_Curso_Global.Enabled := Revisa( D_EMP_PROC_BORRAR_CURSO_GLOBAL );
     _Emp_Registro_Comidas_Grupales.Enabled := Revisa( D_EMP_PROC_COMIDAS_GRUPALES );
     _Emp_Corregir_Fechas_Caf.Enabled := Revisa( D_EMP_PROC_CORREGIR_FECHAS_CAFE );
     //Grid de Aprobaciones
     AsistenciaProcesos.Enabled := Revisa( D_ASIS_PROCESOS );
     _Asi_POLL.Enabled := Revisa( D_ASIS_PROC_POLL_RELOJES );
     _Asi_Procesar_Tarjetas.Enabled := Revisa( D_ASIS_PROC_PROC_TARJETAS );
//     _Asi_Prenomina.Enabled := Revisa( D_ASIS_PROC_CALC_PRENOMINA );
     _Asi_Prenomina.Enabled := RevisaCualquiera( D_ASIS_PROC_CALC_PRENOMINA );
     _Asi_ExtrasPermisosGlobales.Enabled := Revisa( D_ASIS_PROC_EXTRAS_GLOBAL );
     _Asi_Registros_Automaticos.Enabled := Revisa( D_ASIS_PROC_REG_AUTOMATICO );
     _Asi_Corregir_Fechas.Enabled := Revisa( D_ASIS_PROC_FECHAS_GLOBAL );
     _Asi_Recalcular_Tarjetas.Enabled := RevisaCualquiera( D_ASIS_PROC_RECALC_TARJETA );
     IMSSRegistro.Enabled := Revisa( D_IMSS_REGISTRO );
     _IMSS_PagoNuevo.Enabled := Revisa( D_IMSS_REG_NUEVO );
     _IMSS_BorrarPago.Enabled := Revisa( D_IMSS_REG_BORRAR_ACTIVO );
     IMSSProcesos.Enabled := Revisa( D_IMSS_PROCESOS );
     _IMSS_CalcularPago.Enabled := Revisa( D_IMSS_PROC_CALCULAR );
     _IMSS_CalcularRecargos.Enabled := Revisa( D_IMSS_PROC_RECARGOS );
     { AP(22/May/2008): Se elimin� el proceso
     _IMSS_CalcularTasa.Enabled := Revisa( D_IMSS_PROC_CALC_TASA_INFO );}
     _IMSS_RevisarSUA.Enabled := Revisa( D_IMSS_PROC_REVISAR_SUA );
     _IMSS_ExportarSUA.Enabled := Revisa( D_IMSS_PROC_EXP_SUA );
     _IMSS_CalcularPrima.Enabled := Revisa( D_IMSS_PROC_CALC_RIESGO );
     SistemaRegistro.Enabled := Revisa( D_SIST_REGISTRO );
     _Sist_EmpresaNueva.Enabled := Revisa( D_SIST_REG_EMPRESA_NUEVA );
     SistemaProcesos.Enabled := Revisa( D_SIST_PROCESOS );
     _Sist_BorrarBajas.Enabled := Revisa( D_SIST_PROC_BORRAR_BAJAS );
     _Sist_BorrarNominas.Enabled := Revisa( D_SIST_PROC_BORRA_NOMINAS );
     _Sist_Borrar_Tarjetas.Enabled := Revisa( D_SIST_PROC_BORRAR_TARJETAS  );
     _Sist_Borrar_POLL.Enabled := Revisa( D_SIST_BORRAR_POLL );
     _Sist_Borrar_Bitacora.Enabled := Revisa( D_SIST_BORRAR_BITACORA );
     _Sist_Borrar_Herramientas.Enabled := Revisa( D_SIST_BORRAR_HERRAMIENTA );
     _Sist_Actualizar_NumeroTarjeta.Enabled := Revisa( D_SIST_PROC_NUMERO_TARJETA );
     PeriodoRegistro.Enabled := Revisa( D_NOM_REGISTRO );
     _Per_NominaNueva.Enabled := Revisa( D_NOM_REG_NOMINA_NUEVA );
     _Per_BorrarNominaActiva.Enabled := Revisa( D_NOM_REG_BORRA_ACTIVA );
     _Per_ExcepcionDiaHora.Enabled := Revisa( D_NOM_REG_EXCEP_DIA_HORA );
     _Per_ExcepcionMonto.Enabled := Revisa( D_NOM_REG_EXCEP_MONTO );
     _Per_ExcepcionesGlobales.Enabled := Revisa( D_NOM_REG_EXCEP_MONTO_GLOBAL );
     _Per_Liquidacion.Enabled := Revisa( D_NOM_REG_LIQUIDACION );
     _Per_PagoRecibos.Enabled := Revisa( D_NOM_REG_PAGO_RECIBOS );
     PeriodoProcesos.Enabled := Revisa( D_NOM_PROCESOS );
//     _Per_CalcularNomina.Enabled := Revisa( D_NOM_PROC_CALCULAR );
     _Per_CalcularNomina.Enabled := RevisaCualquiera( D_NOM_PROC_CALCULAR );
     _Per_AfectarNomina.Enabled := Revisa( D_NOM_PROC_AFECTAR );
     _Per_FoliarRecibos.Enabled := Revisa( D_NOM_PROC_FOLIAR_RECIBOS );
     _Per_ImprimirListado.Enabled := Revisa( D_NOM_PROC_IMP_LISTADO );
     _Per_ImprimirRecibos.Enabled := Revisa( D_NOM_PROC_IMP_RECIBOS );
     _Per_GenerarPoliza.Enabled := Revisa( D_NOM_PROC_CALC_POLIZA );
     _Per_ImprimirPoliza.Enabled := Revisa( D_NOM_PROC_IMP_POLIZA );
     _Per_ExportarPoliza.Enabled := Revisa( D_NOM_PROC_EXP_POLIZA );
     _Per_CreditoAplicadoMensual.Enabled := Revisa( D_NOM_PROC_CRED_APLICADO );     
     _Per_Desafectar.Enabled := Revisa( D_NOM_PROC_DESAFECTAR );
     _Per_LimpiarAcumulados.Enabled := Revisa( D_NOM_PROC_LIMPIA_ACUM );
     _Per_RecalcularAcumulados.Enabled := Revisa( D_NOM_PROC_RECALC_ACUMULADOS );
     _Per_ImportarMovimientos.Enabled := RevisaCualquiera( D_NOM_PROC_IMP_MOVIMIEN );
     _Per_ExportarMovimientos.Enabled := Revisa( D_NOM_PROC_EXP_MOVIMIEN );
     _Per_ImportarPagoRecibos.Enabled := Revisa( D_NOM_PROC_IMP_PAGO_RECIBO );
     _Per_RefoliarRecibos.Enabled := Revisa( D_NOM_PROC_RE_FOLIAR );
     _Per_CalculaRetroactivos.Enabled := Revisa( D_NOM_PROC_RETROACTIVOS );
     _Per_CopiarNomina.Enabled := Revisa( D_NOM_PROC_COPIAR_NOMINA );
     _Per_CalcularDiferencias.Enabled := Revisa( D_NOM_PROC_CALC_DIFERENCIAS );
     _Per_PagarFuera.Enabled := Revisa( D_NOM_PROC_PAGAR_FUERA );
     _Per_CancelarPasadas.Enabled := Revisa( D_NOM_PROC_CANCELA_PASADA );
     _Per_LiquidacionGlobal.Enabled := Revisa( D_NOM_PROC_LIQUIDA_GLOBAL );
     _Per_CalcularNetoBruto.Enabled := Revisa( D_NOM_PROC_NETO_BRUTO );
     _Per_RastrearCalculo.Enabled := Revisa( D_NOM_PROC_RASTREO );
     PeriodoAnuales.Enabled := Revisa( D_NOM_ANUALES );
     _Per_DefinirPeriodos.Enabled := Revisa( D_NOM_ANUAL_PERIODOS );
     _Per_CalcularAguinaldos.Enabled := Revisa( D_NOM_ANUAL_CALC_AGUINALDO );
     _Per_ImprimirAguinaldos.Enabled := Revisa( D_NOM_ANUAL_IMP_AGUINALDO );
     _Per_PagarAguinaldos.Enabled := Revisa( D_NOM_ANUAL_PAGA_AGUINALDO );
     _Per_CalcularReparto.Enabled := Revisa( D_NOM_ANUAL_CALC_AHORRO );
     _Per_ImprimirReparto.Enabled := Revisa( D_NOM_ANUAL_IMP_AHORRO );
     _Per_PagarReparto.Enabled := Revisa( D_NOM_ANUAL_CALC_AHORROS ); { �Repetido con D_NOM_ANUAL_CALC_AHORRO (108)? }
     _Per_CierreAhorros.Enabled := Revisa( D_NOM_ANUAL_CIERRE_AHORRO );
     _Per_CierrePrestamos.Enabled := Revisa( D_NOM_ANUAL_CIERRE_PRESTA );
     _Per_CalcularPTU.Enabled := Revisa( D_NOM_ANUAL_CALC_PTU );
     _Per_ImprimirPTU.Enabled := Revisa( D_NOM_ANUAL_IMP_PTU );
     _Per_PagarPTU.Enabled := Revisa( D_NOM_ANUAL_PAGA_PTU );
     _Per_CalcularDeclaracion.Enabled := Revisa( D_NOM_ANUAL_CREDITO );
     _Per_ImprimirDeclaracion.Enabled := Revisa( D_NOM_ANUAL_IMP_CREDITO );
     _Per_CalcularComparativo.Enabled := Revisa( D_NOM_ANUAL_COMPARA_ISPT );
     _Per_ImprimirComparativo.Enabled := Revisa( D_NOM_ANUAL_IMP_COMPARA );
     _Per_PagarComparativo.Enabled := Revisa( D_NOM_ANUAL_DIF_COMPARA );
     _Per_DeclaracionAnual.Enabled := Revisa( D_NOM_ANUAL_DECLARACION_ANUAL );
     _Per_ImprimirDeclaracionAnual.Enabled := Revisa( D_NOM_ANUAL_IMP_DECLARACION_ANUAL );
     _Emp_Importar_Asistencia_Sesiones.Enabled := Revisa( D_EMP_PROC_IMP_ASISTENCIA_SESIONES );
     _Emp_Cancelar_Cierre_Vacaciones.Enabled := Revisa( D_EMP_PROC_CANCELA_CIERRE_VACA );
     {$endif}

     _Simulacion_Presupuestos.Enabled:= Revisa( D_PRESUP_PROC_SIMULA );
     _Imprime_simulacion.Enabled:= Revisa( D_PRESUP_PROC_IMPRIME );
     _Importar_Kardex.Enabled := Revisa( D_PRESUP_IMP_KARDEX );

     _GenerarPoliza.Enabled := Revisa( D_PRESUP_PROC_GEN_POLIZA );
     _ImprimirPoliza.Enabled := Revisa( D_PRESUP_PROC_IMP_POLIZA );
     _ExportarPoliza.Enabled := Revisa( D_PRESUP_PROC_EXP_POLIZA );

     _PrepararPresupuestos.Enabled := Revisa( D_SIST_PROC_BD_PRESUPUESTOS );

end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmDiccionario.CierraEmpresa;
          dmCliente.CierraEmpresa;
          CierraDatasets( dmPresupuestos );
          CierraDatasets( dmConsultas );
          CierraDatasets( dmDiccionario );
//          CierraDatasets( dmCafeteria );
          CierraDatasets( dmReportes );
          CierraDatasets( dmNomina );
//          CierraDatasets( dmAsistencia );
          CierraDatasets( dmRecursos );
          CierraDatasets( dmTablas );
          CierraDatasets( dmCatalogos );
          CierraDatasets( dmSistema );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean): Boolean;
begin
     Result := ( eEntidad in [enEmpleado,enPeriodo] );
     if Result then
     begin
          case eEntidad of
              enEmpleado : lEncontrado := ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( VACIO, sKey, sDescription );
              enPeriodo : lEncontrado := ZetaBuscaPeriodo_DevEx.BuscaPeriodoDialogo( oLookUp, VACIO, sKey, sDescription );
          end;
     end;
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          enClasifi: Result := dmCatalogos.cdsClasifi;
          enEmpleado: Result := dmCliente.cdsEmpleadoLookUp;
          enConcepto: Result := dmCatalogos.cdsConceptos;
          enContrato: Result := dmCatalogos.cdsContratos;
          enCurso: Result := dmCatalogos.cdsCursos;
          enEdoCivil: Result := dmTablas.cdsEstadoCivil;
          enEntidad: Result := dmTablas.cdsEstado;
          enMunicipio: Result := dmTablas.cdsMunicipios;
          enEstudios: Result := dmTablas.cdsEstudios;
          enEvento: Result := dmCatalogos.cdsEventos;
          enExtra1: Result := dmTablas.cdsExtra1;
          enExtra2: Result := dmTablas.cdsExtra2;
          enExtra3: Result := dmTablas.cdsExtra3;
          enExtra4: Result := dmTablas.cdsExtra4;
          enFolio: Result := dmCatalogos.cdsFolios;
          enHorario: Result := dmCatalogos.cdsHorarios;
          enIncidencia: Result := dmTablas.cdsIncidencias;
          enMaestros: Result := dmCatalogos.cdsMaestros;
          enMoneda: Result := dmTablas.cdsMonedas;
          enMotBaja: Result := dmTablas.cdsMotivoBaja;
          enNivel1: Result := dmTablas.cdsNivel1;
          enNivel2: Result := dmTablas.cdsNivel2;
          enNivel3: Result := dmTablas.cdsNivel3;
          enNivel4: Result := dmTablas.cdsNivel4;
          enNivel5: Result := dmTablas.cdsNivel5;
          enNivel6: Result := dmTablas.cdsNivel6;
          enNivel7: Result := dmTablas.cdsNivel7;
          enNivel8: Result := dmTablas.cdsNivel8;
          enNivel9: Result := dmTablas.cdsNivel9;
          enOtrasPer: Result := dmCatalogos.cdsOtrasPer;
          enPeriodo: Result := dmCatalogos.cdsPeriodo;
          enPrestacion: Result := dmCatalogos.cdsPrestaci;
          enPRiesgo: Result := dmCatalogos.cdsPRiesgo;
          enPuesto: Result := dmCatalogos.cdsPuestos;
          enQuerys: Result := dmCatalogos.cdsCondiciones;
          enRPatron: Result := dmCatalogos.cdsRPatron;
          enSSocial: Result := dmCatalogos.cdsSSocial;
          enTAhorro: Result := dmTablas.cdsTAhorro;
          enTCurso: Result := dmTablas.cdsTipoCursos;
          enCCurso: Result := dmTablas.cdsClasifiCurso;
          enTKardex: Result := dmTablas.cdsMovKardex;
          enTPresta: Result := dmTablas.cdsTPresta;
          enTransporte: Result := dmTablas.cdsTransporte;
          enTurno: Result := dmCatalogos.cdsTurnos;
          enViveCon: Result := dmTablas.cdsViveCon;
          enViveEn: Result := dmTablas.cdsHabitacion;
{         ER - 2.5 : Se cambi� a TZetaClientDataSet por que no se ocupa un lookup hacia �l - La llave es Flotante: A80_LI
          enArt_80: Result := dmTablas.cdsArt80;
}
          enNumerica: Result := dmTablas.cdsNumericas;
          enUsuarios: Result := dmSistema.cdsUsuarios;
          enNomParam: Result := dmCatalogos.cdsNomParam;
          enOrdFolio: Result := dmCatalogos.cdsOrdFolios;
          enRegla: Result := dmCatalogos.cdsReglas;
          enInvitador: Result := dmCatalogos.cdsInvitadores;
          enMotAuto: Result :=  dmTablas.cdsMotAuto;
          enNivel0: Result := dmSistema.cdsNivel0;
          enTools: Result := dmCatalogos.cdsTools;
          enMotTool: Result := dmTablas.cdsMotTool;
          enTalla: Result := dmTablas.cdsTallas;
          enReporte:  Result := dmReportes.cdsLookupReportes;
          enArea: Result := dmCatalogos.cdsAreas;
          enAccRegla: Result := dmCatalogos.cdsAccReglas;
          enColonia: Result := dmTablas.cdsColonia;
          enGrupoAdicional: Result := dmSistema.cdsGruposAdic;
          enAula: Result := dmCatalogos.cdsAulas;
          enEventoAlta: Result := dmPresupuestos.cdsEventosAltas;
          enBancos : Result := dmTablas.cdsBancos;
     else
         Result := nil;
     end;
end;

{
function TTressShell.ValidaStatusNomina( const sProceso: String ): Boolean;
begin
     if dmCliente.NominaYaAfectada then
     begin
          ZetaDialogo.zInformation( sProceso, 'La N�mina Ya Est� Afectada' + CR_LF + 'No Se Puede Realizar El Proceso', 0 );
          Result := False;
     end
     else
         Result := True;
end;
}

function TTressShell.FormaActivaNomina: Boolean;
begin
    Result := ( FormaActiva <> NIL ) and
              ( FormaActiva.TipoValorActivo1 = stEmpleado ) and
              ( FormaActiva.TipoValorActivo2 = stPeriodo );
end;

procedure TTressShell.ChangeTimerInfo;
begin
     SetTimerInfo;
end;

{ ********** Manejo de Valores Activos ************ }

{$ifdef PRESUPUESTOS}
procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TTressShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}

begin
     dmRecursos.NotifyDataChange( Entidades, Estado );
     dmNomina.NotifyDataChange( Entidades, Estado );
     dmCatalogos.NotifyDataChange( Entidades, Estado );
     dmSistema.NotifyDataChange( Entidades, Estado );
     dmPresupuestos.NotifyDataChange( Entidades, Estado );
     inherited NotifyDataChange( Entidades, Estado );
end;

procedure TTressShell.ReinitPeriodo;
begin
     with dmCliente do
     begin
          if ( PeriodoNumero = 0 ) then
             InitActivosPeriodo;
     end;
end;

{$ifdef PRESUPUESTOS}
procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
{$else}
procedure TTressShell.SetDataChange(const Entidades: ListaEntidades);
{$endif}
begin
     { Cambios a Valores Activos }
     {$ifdef PRESUPUESTOS}
     if Dentro( enPeriodo , Entidades ) then
     {$else}
     if ( enPeriodo in Entidades ) then
     {$endif}
     begin
          RefrescaPeriodoInfo;
          CargaPeriodoActivos;
     end;
     {$ifdef PRESUPUESTOS}
     if Dentro( enEmpleado , Entidades ) then
     {$else}
     if ( enEmpleado in Entidades ) then
     {$endif}
     begin
          RefrescaEmpleadoInfo;
          RefrescaEmpleadoActivos;
     end;
     { Cambios a otros datasets }
     inherited SetDataChange( Entidades );
     //NotifyDataChange( Entidades, stNinguno );
end;

procedure TTressShell.EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        lOk := dmCliente.SetEmpleadoNumero( EmpleadoNumeroCB_DevEx.ValorEntero );
        if lOk then
           CambioEmpleadoActivo
        else
            ZetaDialogo.zInformation( 'Error', '! Empleado no encontrado !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TTressShell.PeriodoTipoCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          Application.ProcessMessages;
          try
             {$ifdef ANTES}
             PeriodoTipo := eTipoPeriodo( PeriodoTipoCB.Indice );
             {$else}
             PeriodoTipo := eTipoPeriodo( StrToIntDef( PeriodoTipoCB_DevEx2.Llave, 0 ) );
             {$endif}
             CambioPeriodoActivo;
             if ( GetDatosPeriodoActivo.Numero = 0 ) then
                ZetaDialogo.zInformation( 'Aviso', 'No se ha definido ning�n per�odo ' + PeriodoTipoCB_DevEx2.Text, 0 );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TTressShell.PeriodoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        lOk := dmCliente.SetPeriodoNumero( PeriodoNumeroCB_DevEx2.ValorEntero );
        if lOk then
        begin
             RefrescaPeriodoActivos;
             CambioValoresActivos( stPeriodo )
        end
        else
            ZetaDialogo.zInformation( 'Error', '! No existe el per�odo de N�mina !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TTressShell.CargaEmpleadoActivos;
begin
     with dmCliente do
     begin
          EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
     end;
     RefrescaEmpleadoActivos;
end;

procedure TTressShell.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;

procedure TTressShell.RefrescaEmpleadoActivos;
var
   oColor : TColor;
   i:Integer;
   sCaption, sHint : String;

   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
   with dmCliente do
   begin
        StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
        with GetDatosEmpleadoActivo do
        begin
             EmpleadoPrettyName_DevEx.Caption := Nombre;
             GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);
             //DevEx (by am): Inicializando Bitmat y PNG
             ABitmap := TcxAlphaBitmap.CreateSize(24, 24);
             APngImage := nil;
             for i := Low(StatusEmpleado) to High(StatusEmpleado) do
             begin
                  if (sCaption = StatusEmpleado[i]) then
                  begin
                       try
                          //DevEx (by am): Creando el Bitmap
                          ABitmap.Clear;
                          Image24_StatusEmpleado.GetBitmap(i, ABitmap);
                          //DevEx (by am): Creando el PNG
                          APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                          ImagenStatus_DevEx.Picture.Graphic := APngImage;
                          ImagenStatus_DevEx.Hint := sHint;
                       finally
                              APngImage.Free;
                              ABitmap.Free;
                       end;
                       Break;
                  end;
             end;
        end;
   end;
end;

procedure TTressShell.RefrescaEmpleadoInfo;
begin
     dmCliente.RefrescaEmpleado;
end;

procedure TTressShell.RefrescaEmpleado;
begin
     RefrescaEmpleadoInfo;
     CambiaEmpleadoActivos;
end;

procedure TTressShell.CargaPeriodoActivos;
begin
     with dmCliente do
     begin
          {$ifdef ANTES}
          PeriodoTipoCB.Indice := Ord( PeriodoTipo );
          {$else}
          PeriodoTipoCB_DevEx2.Llave := IntToStr( Ord( PeriodoTipo ) );
          {$endif}
          PeriodoNumeroCB_DevEx2.ValorEntero := PeriodoNumero;
     end;
     RefrescaPeriodoActivos;
end;

procedure TTressShell.CambioPeriodoActivo;
begin
     PeriodoNumeroCB_DevEx2.ValorEntero := dmCliente.PeriodoNumero;
     RefrescaPeriodoActivos;
     CambioValoresActivos( stPeriodo );
end;

procedure TTressShell.RefrescaPeriodoActivos;
const
     K_ESPACIO = ' ';
     K_VACIO ='';
     K_SIN_DEFINIR = 0;
     K_STATUS_NUEVA = 0;
     K_STATUS_PRENOMINA_PARCIAL = 1;
     K_STATUS_SIN_CALCULAR = 2;
     K_STATUS_CALCULADA_PARCIAL = 3;
     K_STATUS_CALCULADA_TOTAL = 4;
     K_STATUS_AFECTADA_PARCIAL =5;
     K_STATUS_AFECTADA_TOTAL = 6;
var
   FechaDescrip: String;
procedure LimpiaTextos;
begin
     PanelPeriodoStatus.Caption := K_VACIO;
     PanelRangoFechas.Caption := K_VACIO;
end;
begin
   with dmCliente do
   begin
      StatusBarMsg( GetPeriodoDescripcion, K_PANEL_PERIODO );
      with GetDatosPeriodoActivo do
      begin
               //DevEx (by am): Se define el Rango de Fechas de la nomina agregando el texto a la misma etiqueta donde se encuentra el status.

               LimpiaTextos;
               FechaDescrip := FechaCorta( Inicio );
               if  (( Length( FechaDescrip ) > 0 ) AND ( FechaDescrip <> K_ESPACIO )) then
               begin
                      TempPanelRangoFechas := K_ESPACIO + FechaDescrip + K_ESPACIO +'al'+ K_ESPACIO + FechaCorta( Fin )+K_ESPACIO;
                      //DevEx(by am): Solo se muestra el panel si el width es mayor a 1165, para dar prioridad a que los valores de la nomina se vean de izq. a derecha
                      if (Self.Width > K_MIN_WIDTH) then
                         PanelRangoFechas.Caption := TempPanelRangoFechas;
               end;
               {***DevEx (by am): Se define el Status de la nomina.***}
               if ( Numero = K_SIN_DEFINIR ) then
               begin
                    with  PanelPeriodoStatus do
                    begin
                         Font.Color := K_STATUS_COLOR_SINDEFINIR;
                         Caption := K_ESPACIO+'Sin Definir'+K_ESPACIO;
                         Hint := 'Per�odos ' + PeriodoTipoCB_DevEx2.Text + ' no han sido definidos';
                    end;
               end
               else
               begin
                    with PanelPeriodoStatus,PanelPeriodoStatus.Font do
                    begin
                         if ( Ord( Status ) in [K_STATUS_NUEVA, K_STATUS_PRENOMINA_PARCIAL, K_STATUS_SIN_CALCULAR, K_STATUS_CALCULADA_PARCIAL,K_STATUS_CALCULADA_TOTAL]) then
                            Color := K_STATUS_COLOR_NUEVA
                         else if ( Ord( Status ) in [K_STATUS_AFECTADA_PARCIAL,K_STATUS_AFECTADA_TOTAL]) then
                              begin
                                    if ( StatusTimbrado >=  estiTimbradoParcial ) then
                                       Color := K_STATUS_COLOR_TIMBRADA
                                    else
                                       Color := K_STATUS_COLOR_AFECTADA;
                              end
                         else
                             Color := clWindowText;
                         Caption :=K_ESPACIO+ZetaCommonTools.GetDescripcionStatusPeriodo(Status, StatusTimbrado ) +K_ESPACIO;
                         //DevEx(by am): Si el panel del rango de fechas no esta visible se le pone esta informacion al hint del status
                         if (Self.Width > K_MIN_WIDTH) then
                            Hint := K_VACIO
                         else
                             Hint := TempPanelRangoFechas;
                    end;
               end;
               {***DevEx(by am): Ajustar tamano del panel segun el caption
               Se le suma un width extra por el cambio de FONT al panel, ya que el metodo TextWidth toma en cuenta el font de la forma para el calculo***}
               //PanelPeriodoStatus.Width := Self.Canvas.TextWidth(PanelPeriodoStatus.Caption) + K_EXTRA_WIDTH;
               //PanelRangoFechas.Width := Self.Canvas.TextWidth(PanelRangoFechas.Caption) + K_EXTRA_WIDTH;
      end;
   end;
end;

procedure TTressShell.RefrescaPeriodoInfo;
begin
     dmCliente.RefrescaPeriodo;
end;

procedure TTressShell.RefrescaPeriodo;
begin
     RefrescaPeriodoInfo;
     CambiaPeriodoActivos;
end;


procedure TTressShell.CargaSistemaActivos;
begin

     dxStaticFechaSistemaValor.Caption := DateToStr(dmCliente.FechaDefault);
     SistemaYear_DevEx.Text :=  IntToStr (dmCliente.YearDefault);
     RefrescaSistemaActivos;
end;


procedure TTressShell.CambiaEmpleadoActivos;
begin
     CargaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;


procedure TTressShell.CambiaPeriodoActivos;
begin
     CargaPeriodoActivos;
     CambioValoresActivos( stPeriodo );
end;

procedure TTressShell.BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
begin
     with dmCliente do
     begin
          if ( iTipo = Ord( PeriodoTipo ) ) and ( iPeriodoBorrado = PeriodoNumero ) then
          begin
               if not GetPeriodoSiguiente then
                  GetPeriodoAnterior;
               {$ifdef ANTES}
               PeriodoTipo := eTipoPeriodo( PeriodoTipoCB.Indice );
               {$else}
               PeriodoTipo := eTipoPeriodo( StrToIntDef( PeriodoTipoCB_DevEx2.Llave, 0 ) );
               {$endif}
               CambioPeriodoActivo;
          end;
     end;
     if HayFormaAbierta then
        FormaActiva.ReConnect;
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     CargaSistemaActivos;
     CambioValoresActivos( stSistema );
end;

 
{ ************ Eventos del ActionList *********** }

procedure TTressShell._E_RefrescarUpdate(Sender: TObject);
begin
     _E_Refrescar.Enabled := HayFormaAbierta;
     //or PestanaPeriodoActiva;
end;

procedure TTressShell._A_CatalogoUsuariosExecute(Sender: TObject);
begin
     inherited;
     AbreFormaConsulta( efcSistUsuarios );
end;

procedure TTressShell._A_PreferenciasExecute(Sender: TObject);
begin
     inherited;
     // Ser�a mejor llamar a 'cdsArbol.Modificar'
     // y que ese m�todo se encargue de lo dem�s.
     dmSistema.PosicionaUsuarioActivo;
     ZBaseDlgModal_DevEx.ShowDlgModal( ArbolConfigura_DevEx, TArbolConfigura_DevEx );

end;

procedure TTressShell._E_RefrescarExecute(Sender: TObject);
begin
//*     if PestanaPeriodoActiva then
     begin
          dmCliente.RefrescaPeriodo;
          RefrescaPeriodoActivos;
     end;
     inherited;
end;

procedure TTressShell._E_BuscarEmpleadoExecute(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        EmpleadoNumeroCB_DevEx.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;

procedure TTressShell._E_CambiarEmpleadoExecute(Sender: TObject);
begin
     inherited;
     EmpleadoNumeroCB_DevEx.SetFocus;
end;

procedure TTressShell._V_ArbolOriginalExecute(Sender: TObject);
begin
     inherited;
     if ZConfirm( 'Edici�n del �rbol de formas', 'Este cambio es s�lo temporal' + CR_LF + '� Desea continuar ? ', 0, mbYes ) then

end;

procedure TTressShell._V_EmpleadosExecute(Sender: TObject);
begin
     inherited;
     _E_CambiarEmpleadoExecute( Sender );
end;

procedure TTressShell._V_NominaExecute(Sender: TObject);
begin
     inherited;
     PeriodoNumeroCB_DevEx2.SetFocus;
     //TAction( Sender ).Checked := True;
end;

procedure TTressShell._V_SistemaExecute(Sender: TObject);
begin
     inherited;
     SistemaYear_DevEx.SetFocus;
     //TAction( Sender ).Checked := True;
end;

procedure TTressShell._Emp_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoPrimero then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_PrimeroUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoDownEnabled;
end;

procedure TTressShell._Emp_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoAnterior then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoSiguiente then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoUltimo then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_UltimoUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoUpEnabled;
end;

procedure TTressShell._Per_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoPrimero then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoAnterior then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_AnteriorUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoDownEnabled;
end;

procedure TTressShell._Per_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoSiguiente then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_SiguienteUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoUpEnabled;
end;

procedure TTressShell._Per_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoUltimo then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_DefinirPeriodosExecute(Sender: TObject);
begin
     inherited;
     //NO BORRAR
     DProcesos.ShowWizard( prNODefinirPeriodos );
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell.GetFechaLimiteShell;
begin
//
end;

procedure TTressShell.SetBloqueo;
begin
//
end;

procedure TTressShell.SetAsistencia(const dFecha: TDate);
begin

end;

procedure TTressShell.RefrescaIMSS;
begin

end;

procedure TTressShell._Simulacion_PresupuestosExecute(Sender: TObject);
begin
     dmPresupuestos.ShowWizardSimulacion;
end;


procedure TTressShell._Imprime_simulacionExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporte( 'Imprimir simulaci�n', enCuboPresup, TRListado, 0 );

end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: eFormaConsulta);
begin
     AbreFormaConsulta(TipoForma);
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     //TODO : Mejoras de Navegador
end;

procedure TTressShell._Importar_KardexExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHImportarKardex );
end;

procedure TTressShell._Per_GenerarPolizaExecute(Sender: TObject);
begin
     inherited;
     if ( dmCliente.GetDatosPeriodoActivo.Status < spCalculadaTotal ) then
        ZInformation( 'P�liza Contable', 'La N�mina no ha sido calculada completamente', 0 )
     else
     begin
         DProcesos.ShowWizard( prNOPolizaContable );
         //Se hace aqui, porque los Wizards,no hacen el dataChange si hubo errores.
         //En este caso, se quiere que se actualice la forma de POLHEAD en cualquier caso.
         SetDataChange( [ enPolHead ] )
     end;
end;

procedure TTressShell._Per_ImprimirPolizaExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporte( 'Imprimir P�liza Contable', enPoliza, TRListado, K_GLOBAL_DEF_POLIZA );
end;

procedure TTressShell._Per_ExportarPolizaExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporte( 'Exportar P�liza Contable', enPoliza, TRListado, K_GLOBAL_DEF_ASCII_POLIZA );
end;

procedure TTressShell._PrepararPresupuestosExecute(Sender: TObject);
begin
     DProcesos.ShowWizard(prSISTPrepararPresupuestos);
end;


procedure TTressShell.btnYearBefore_DevExClick(Sender: TObject);
begin
  inherited;
  CalculaAnio ( K_ANTERIOR );
end;

procedure TTressShell.btnYearNext_DevExClick(Sender: TObject);
begin
  inherited;
  CalculaAnio ( K_SIGUIENTE );
end;

procedure TTressShell.SistemaYear_DevExPropertiesChange(Sender: TObject);
begin
  inherited;
  if ( Length( Trim(SistemaYear_DevEx.Text) ) = 4 ) then
  begin
        CalculaAnio ( K_MANUAL);
  end;
end;



//DevEx : Calcula el valor del a�o del Sistema con los nuevos componentes.
procedure TTressShell.CalculaAnio ( Operacion:Integer );
var
   oCursor: TCursor;
   NewYearValue:Integer;
function ValidaNewYear ( NewYearValue:Integer ):Integer;
 begin
     if NewYearValue > K_MAX_YEAR then
     begin
        NewYearValue := K_MAX_YEAR;
        btnYearNext_DevEx.Enabled := FALSE;  //Deshabilita btnYearSiguiente
     end
     else if NewYearValue < K_MIN_YEAR then
     begin
        NewYearValue := K_MIN_YEAR;
        btnYearBefore_DevEx.Enabled := FALSE;//Deshabilita btnYearAnterior
     end
     else
     begin
        HabilitaBotonesAnio; //Habilita ambos botones
     end;
     Result := NewYearValue;
 end;
begin
     NewYearValue := dmCliente.YearDefault;
     case Operacion of
          K_ANTERIOR:
               NewYearValue := dmCliente.YearDefault -1;
          K_SIGUIENTE:
               NewYearValue := dmCliente.YearDefault +1;
          K_MANUAL:
               NewYearValue := StrToInt ( SistemaYear_DevEx.Text );
     end;

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
              YearDefault := ValidaNewYear ( NewYearValue );
              SistemaYear_DevEx.Text := IntToStr ( YearDefault );
        end;

        CambioPeriodoActivo;
        RefrescaSistemaActivos;
        CambioValoresActivos( stSistema );

     finally
            Screen.Cursor := oCursor;
     end;
end;


//DevEx : Botones para Habilitar los botones de los componentes de mes y a�o
procedure TTressShell.HabilitaBotonesAnio;
begin
     btnYearNext_DevEx.Enabled := TRUE;
     btnYearBefore_DevEx.Enabled := TRUE;
end;

//DevEx
procedure TTressShell.RefrescaSistemaActivos;
begin

     dxStaticSistemaFechaDia.Caption := DiaSemana( dmCliente.FechaDefault );
end;


//DevEx
procedure TTressShell.LLenaOpcionesGrupos;
var
I:integer;
begin
  if(dmCliente.EmpresaAbierta)
  then
  begin
    for I:=0 to DevEx_NavBar.Groups.Count-1 do
    begin
      if (DevEx_NavBar.Groups[I].Caption = 'Presupuestos')    then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=12;
        DevEx_NavBar.Groups[I].SmallImageIndex:=12;
      end;
      if (DevEx_NavBar.Groups[I].Caption = 'Empleados') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=1;
        DevEx_NavBar.Groups[I].SmallImageIndex:=1;

      end;
      if (DevEx_NavBar.Groups[I].Caption = 'N�minas') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=3;
        DevEx_NavBar.Groups[I].SmallImageIndex:=3;
      end;
      if (DevEx_NavBar.Groups[I].Caption = 'Consultas') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=6;
        DevEx_NavBar.Groups[I].SmallImageIndex:=6;
      end;
      if (DevEx_NavBar.Groups[I].Caption = 'Cat�logos') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=7;
        DevEx_NavBar.Groups[I].SmallImageIndex:=7;
      end;
      if (DevEx_NavBar.Groups[I].Caption ='Tablas') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=8;
        DevEx_NavBar.Groups[I].SmallImageIndex:=8;
      end;
      if(DevEx_NavBar.Groups[I].Caption = '�rbol Cl�sico')  then
      begin
         DevEx_NavBar.Groups[I].LargeImageIndex:=10;
         DevEx_NavBar.Groups[I].SmallImageIndex:=10;
      end;
      if (I = 0) then
      begin
            if(DevEx_NavBar.Groups.Count = 1) then
            begin
                  DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                  DevEx_NavBar.Groups[0].SmallImageIndex:=0;
            end
            else
            begin
                 //indicar el nombre del primer grupo de la aplicacion
                  if (DevEx_NavBar.Groups[1].Caption = 'Presupuestos')  then
                  begin
                      DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                      DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                  end;
            end;
      end;
      DevEx_NavBar.Groups[I].UseSmallImages:=false;
    end;
  end
  else
  begin
    if(DevEx_NavBar.Groups.Count >0) then
    begin
      DevEx_NavBar.Groups[0].UseSmallImages:=false;
      DevEx_NavBar.Groups[0].LargeImageIndex:=9;
      DevEx_NavBar.Groups[0].SmallImageIndex:=9;
    end;
  end;
end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
     inherited;
     _V_Cerrar.Execute;
end;


procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)

end;

//Visualizar al KARDEX al dar doble clic sobre la imagen del status del empleado
procedure TTressShell.ImagenStatus_DevExDblClick(Sender: TObject);
begin
     inherited;
     AbreFormaConsulta( efcHisKardex );
end;

procedure TTressShell.HabilitarBotonesRibbon(Habilitar: Boolean);
begin
     dxBarLargeButtonAgregarBaseDatos.Enabled := Habilitar;
     dxBarLargeButtonPeriodos.Enabled := Habilitar;
     dxBarSubItemoSimulacion.Enabled := Habilitar;
     dxBarLargeButtonImportatKardex.Enabled := Habilitar;
     dxBarSubItemPolizas.Enabled := Habilitar;
end;

procedure TTressShell.DoOpenSistema;
begin
end;


end.



