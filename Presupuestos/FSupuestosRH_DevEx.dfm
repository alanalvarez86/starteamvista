inherited SupuestosRH_DevEx: TSupuestosRH_DevEx
  Left = 571
  Top = 264
  Caption = 'Supuestos de Personal'
  ClientHeight = 212
  ClientWidth = 558
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 558
    inherited ValorActivo2: TPanel
      Width = 299
      inherited textoValorActivo2: TLabel
        Width = 293
      end
    end
  end
  object PanelBoton: TPanel [1]
    Left = 0
    Top = 19
    Width = 558
    Height = 46
    Align = alTop
    TabOrder = 2
    object lblEscenario: TLabel
      Left = 40
      Top = 18
      Width = 50
      Height = 13
      Caption = 'Escenario:'
    end
    object Escenarios: TZetaKeyLookup_DevEx
      Left = 95
      Top = 14
      Width = 300
      Height = 21
      LookupDataset = dmPresupuestos.cdsEscenarios
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = EscenariosValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 65
    Width = 558
    Height = 147
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OnCustomDrawCell = ZetaDBGridDBTableViewCustomDrawCell
      DataController.DataSource = DataSource
      object SR_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'SR_FECHA'
      end
      object SR_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'SR_TIPO'
      end
      object EV_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'EV_CODIGO'
      end
      object SR_CUANTOS: TcxGridDBColumn
        Caption = 'Cantidad'
        DataBinding.FieldName = 'SR_CUANTOS'
      end
      object DESCRIPCION: TcxGridDBColumn
        Caption = 'Supuesto'
        DataBinding.FieldName = 'DESCRIPCION'
      end
      object PRIORIDAD: TcxGridDBColumn
        Caption = 'Prioridad'
        DataBinding.FieldName = 'PRIORIDAD'
      end
      object US_DESCRIP: TcxGridDBColumn
        Caption = 'Modificado Por'
        DataBinding.FieldName = 'US_DESCRIP'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 400
    Top = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 1049080
  end
  inherited ActionList: TActionList
    Left = 466
    Top = 16
  end
  inherited PopupMenu1: TPopupMenu
    Left = 432
    Top = 16
  end
end
