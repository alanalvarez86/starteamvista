library Global;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  Global_TLB in '..\MTS\Global_TLB.pas',
  DServerGlobal in '..\MTS\DServerGlobal.pas' {dmServerGlobal: TMtsDataModule} {dmServerGlobal: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Global.TLB}

{$R *.RES}

begin
end.
