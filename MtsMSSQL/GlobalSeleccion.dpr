library GlobalSeleccion;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  GlobalSeleccion_TLB in '..\MTS\GlobalSeleccion_TLB.pas',
  DServerGlobalSeleccion in '..\MTS\DServerGlobalSeleccion.pas' {dmServerGlobalSeleccion: TMtsDataModule} {dmServerGlobalSeleccion: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\GlobalSeleccion.TLB}

{$R *.RES}

begin
end.
