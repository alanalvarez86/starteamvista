object dmImportarReporte: TdmImportarReporte
  OldCreateOrder = True
  Height = 201
  Width = 236
  object cdsEditReporte: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    BeforePost = cdsEditReporteBeforePost
    AfterCancel = cdsEditReporteAfterCancel
    OnNewRecord = cdsEditReporteNewRecord
    OnReconcileError = cdsEditReporteReconcileError
    Left = 40
    Top = 8
  end
  object cdsCampoRep: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CR_TIPO;CR_POSICIO;CR_SUBPOS'
    Params = <>
    OnNewRecord = cdsCampoRepNewRecord
    Left = 40
    Top = 64
  end
end
