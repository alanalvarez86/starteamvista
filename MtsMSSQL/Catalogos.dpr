library Catalogos;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DServerCatalogos in '..\MTS\DServerCatalogos.pas',
  Catalogos_TLB in '..\MTS\Catalogos_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ../MTS/Catalogos.TLB}

{$R *.RES}

begin
end.
