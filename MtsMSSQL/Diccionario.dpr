library Diccionario;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DServerDiccionario in '..\MTS\DServerDiccionario.pas' {dmServerDiccionario: TMtsDataModule} {dmServerDiccionario: CoClass},
  Diccionario_TLB in '..\MTS\Diccionario_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Diccionario.TLB}

{$R *.RES}

begin
end.
