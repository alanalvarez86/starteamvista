library TimbradoNomina;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}


uses
  MidasLib,
  ComServ,
  TimbradoNomina_TLB in '..\MTS\TimbradoNomina_TLB.pas',
  DServerNominaTimbrado in '..\MTS\DServerNominaTimbrado.pas' {dmServerNomina: TMtsDataModule} {dmServerNomina: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\TimbradoNomina.TLB}



{$R *.RES}

begin
end.
