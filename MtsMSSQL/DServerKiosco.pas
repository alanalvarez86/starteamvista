unit DServerKiosco;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {bdemts,} DataBkr, DBClient,
  MtsRdm, Mtx, Kiosco_TLB, DB, ZetaServerDataSet,
  DSuperGlobal,
  DZetaServerProvider,
  ZCreator,
  ZetaPortalClasses,
  ZetaCommonLists,
  DSeguridad,
  Variants;

type
    eSQLScript = ( Q_COMPANYS_NINGUNA,
                   Q_KIOSCO2_GET_BOTONES,
                   Q_KIOSCO2_GET_CARRUSEL,
                   Q_KIOSCO2_GET_ESTILO,
                   Q_KIOSCO2_GET_PANTALLA,
                   Q_KIOSCO2_GET_SHOW,
                   Q_KIOSCO2_GET_KIOSCOS,
                   Q_KIOSCO2_INSERT_BOTON,
                   Q_KIOSCO2_INSERT_KSHOWINF,
                   Q_KIOSCO2_BITACORA,
                   Q_KIOSCO2_INSERT_ACCESO_BOTON,
                   Q_KIOSCO2_ACCESO_BOTON,
                   Q_KIOSCO2_ACCESO_EMPLEADO,
                   Q_EMPLEADOS_ACTUALIZA_PASS,
                   Q_GLOBAL_NIVELES,
                   Q_KIOSCO_EMPLEADO,
                   Q_DELETE_BOTON_ACC,
                   Q_DELETE_BOTON,
                   Q_USUARIOS_CONFI_REP_EMAIL,
                   Q_CONFIDENCIALIDADES,
                   Q_DEL_CONFI_USERS,
                   Q_INS_CONFI_USERS,
                   Q_GET_IMPRESIONES,
                   Q_KIOSCO2_ENCUESTAS,
                   Q_GET_MAX_ENCUESTA,
                   Q_OPCIONES_ENCUESTAS,
                   Q_KIOSCO2_INS_OPCIONES,
                   Q_CONSULTA_EMP_ID,

                   //BIOMETRICO
                   Q_CONSULTA_EMP_BIO,
                   Q_HUELLA_REPORTA_INSERTA,
                   Q_HUELLA_REPORTA_ELIMINA,
                   Q_HUELLA_OBTENER_MAQUINA,
                   Q_HUELLA_OBTENER_TODOS,
                   Q_TOTAL_DISPOSIT
                   );
    TdmServerKiosco = class(TMtsDataModule, IdmServerKiosco)
    cdsDatos: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    oZetaCreator: TZetaCreator;
    FQuery: TZetaCursor;
    FListaUsuarios: TZetaCursor;
    procedure SetTablaInfo(const eTabla: eTipoTabla);
    procedure InitCreator;
    function GetNombreTabla( const eTabla : eTipoTabla ) : string;
    function Fix( const sValue: String): String;
    function HayKioscoEnEmpresa(const iTipo: integer): Boolean;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;

{$ifdef DOS_CAPAS}
  public
    { Public declarations }
{$endif}
    function GetGlobales: OleVariant; safecall;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant; safecall;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString): OleVariant; safecall;
    function GetBotones(const sPantalla: WideString): OleVariant; safecall;
    function GetCarrusel(const sShow: WideString): OleVariant; safecall;
    function GetCarruselesInfo(const Codigo: WideString): OleVariant; safecall;
    function GetComparte: OleVariant; safecall;
    function GetEstilo(const sEstilo: WideString): OleVariant; safecall;
    function GetKioscos: OleVariant; safecall;
    function GetPantalla(const sPantalla: WideString): OleVariant; safecall;
    function GetShow(const sShow: WideString): OleVariant; safecall;
    function GetTabla(Tabla: Integer): OleVariant; safecall;
    function GetValoresActivos(const Kiosco: WideString; Empleado: Integer; const Empresa: WideString): WideString; safecall;
    function GrabaCarruseles(Delta, DeltaCarruselInfo: OleVariant; const Codigo: WideString; var CarruselInfo: OleVariant; var ErrorCount, ErrorCountInfo: Integer): OleVariant; safecall;
    function GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GrabaMarcosInfo(Delta: OleVariant; DeltaBotones: OleVariant; DeltaAccesos: OleVariant;
                             const Codigo: WideString; var oAccesoBotones: OleVariant;
                             var ErrorCount: Integer; var ErrorCountBotones: Integer): OleVariant; safecall;
    function GrabaTabla(Tabla: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetBitacora(Parametros: OleVariant): OleVariant; safecall;
    function GrabaBitacora(oParametros: OleVariant): OleVariant; safecall;
    function GetAccesoBoton(const sPantalla: WideString): OleVariant; safecall;
    function GetAccesoEmp(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function ReseteaPassword(Empresa: OleVariant; Empleados: OleVariant): Integer; safecall;
    function KioscoEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function SetClaveEmpleadoKiosco(Empresa: OleVariant; Empleado: Integer; const Clave: WideString): Integer; safecall;
    function GetUsuariosConfidencialidad(const Empresa: WideString ): OleVariant; safecall;
    function GrabaUsuariosConfidencialidad(const EmpresaCodigo: WideString; Delta: OleVariant;out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuariosSuscritos(const sEmpresa: WideString; const sConfiden: WideString;lPorConfiden: WordBool; out oUsuarios: OleVariant): OleVariant; safecall;
    function GetImpresionesEmpleado(Empleado: Integer;const Empresa:WideString;  oParams: OleVariant; out iImpresione: Integer): OleVariant;safecall;
    function GetOpcionesEncuesta(Encuesta: Integer): OleVariant; safecall;
    function GetEncuestas(Parametros: OleVariant): OleVariant; safecall;
    function GetMaxEncuesta: Integer; safecall;
    function GrabaEncuestas(oDelta: OleVariant; oOpciones: OleVariant; var ErrorCount: Integer;var Valor: OleVariant; const Codigo: WideString): OleVariant;safecall;
    function GetProximidadEmpleado(const sProximidad: WideString): OleVariant; safecall;
    //BIOMETRICO
    function ReportaHuella(Params: OleVariant): OleVariant; safecall;
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; safecall;
    function GetBiometricoEmpleado(iBiometrico: Integer): OleVariant; safecall;
  end;

var
  dmServerKiosco: TdmServerKiosco;

implementation

uses DServerReporteador,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaServerTools,
     ZGlobalTress,
     ZetaSQLBroker;
const
     Q_NIVEL_CAMPO = ', C.CB_NIVEL%0:d, N%0:d.TB_ELEMENT as TB_ELEMENT%0:d';
     Q_NIVEL_NO_JOIN = ', C.CB_NIVEL%0:d, ''%1:s'' as TB_ELEMENT%0:d';
     Q_NIVEL_JOIN = 'left outer join NIVEL%0:d N%0:d on ( N%0:d.TB_CODIGO = C.CB_NIVEL%0:d ) ';

{$R *.DFM}

function GetScript( const eScript: eSQLScript ): String;
begin
     case eScript of

          Q_COMPANYS_NINGUNA: Result := 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_NIVEL0, CM_DATOS, CM_DIGITO ' +
                                        'from COMPANY where ( CM_CODIGO = ''%s'' )';

          Q_KIOSCO2_GET_BOTONES: Result := ' select KS_CODIGO,KB_ORDEN,KB_TEXTO,KB_BITMAP,KB_ACCION, '+
                                                   'KB_URL,KB_SCREEN,KB_ALTURA,KB_LUGAR,KB_SEPARA,KB_REPORTE,KB_POS_BIT,KB_RESTRIC,KB_USA_EMPL, KB_IMP_DIR '+
                                           ' from KBOTON '+
                                           ' where( KS_CODIGO = %s ) '+
                                           ' order by KB_ORDEN ';
          Q_KIOSCO2_GET_CARRUSEL: Result := 'select KW_CODIGO,KI_ORDEN,KI_TIMEOUT,KI_URL,KI_REFRESH '+
                                            'from KSHOWINF '+
                                            'where(KW_CODIGO = %s) '+
                                            'order by KI_ORDEN';
          Q_KIOSCO2_GET_ESTILO: Result := 'select KE_CODIGO,KE_NOMBRE,KE_COLOR,KE_F_NAME,KE_F_SIZE,KE_F_COLR,KE_F_BOLD,KE_F_ITAL,KE_F_SUBR,KE_BITMAP '+
                                          'from KESTILO where(KE_CODIGO = %s)';
          Q_KIOSCO2_GET_PANTALLA: Result := 'select KS_CODIGO,KS_NOMBRE,KS_PF_SIZE,KS_PF_COLR,KS_PF_ALIN,KS_PF_URL,KS_PB_SIZE, '+
                                            'KS_PB_COLR,KS_PB_ALIN,KS_BTN_ALT,KS_BTN_SEP,KS_EST_NOR,KS_EST_SEL, KS_BITMAP '+
                                            'from KSCREEN where(KS_CODIGO = %s)';
          Q_KIOSCO2_GET_SHOW: Result := 'select KW_CODIGO,KW_NOMBRE,KW_SCR_DAT,KW_TIM_DAT,KW_GET_NIP,CM_CODIGO from KSHOW where(KW_CODIGO = %s)';
          Q_KIOSCO2_GET_KIOSCOS: Result := 'select KW_CODIGO, KW_NOMBRE from KSHOW order by KW_CODIGO ';
          Q_KIOSCO2_INSERT_BOTON: Result := 'INSERT INTO KBOTON (KS_CODIGO,	KB_ORDEN,	KB_TEXTO,	KB_BITMAP,	KB_ACCION, '+
                                                                'KB_URL,	KB_SCREEN,	KB_ALTURA,	KB_LUGAR,	KB_SEPARA,KB_REPORTE,KB_POS_BIT, KB_RESTRIC, KB_USA_EMPL, KB_IMP_DIR ) '+
                                                        'VALUES (:KS_CODIGO,	:KB_ORDEN,	:KB_TEXTO,	:KB_BITMAP,	:KB_ACCION, '+
                                                               	':KB_URL,	:KB_SCREEN,	:KB_ALTURA,	:KB_LUGAR,	:KB_SEPARA,:KB_REPORTE,:KB_POS_BIT, :KB_RESTRIC, :KB_USA_EMPL, :KB_IMP_DIR) ';
          Q_KIOSCO2_INSERT_KSHOWINF: Result := 'INSERT INTO KSHOWINF (KW_CODIGO,KI_ORDEN,KI_TIMEOUT,KI_URL,KI_REFRESH ) '+
                                                            ' VALUES (:KW_CODIGO,:KI_ORDEN,:KI_TIMEOUT,:KI_URL,:KI_REFRESH ) ';

          Q_KIOSCO2_BITACORA: Result :=  'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, '+
                                         'BI_TEXTO, CB_CODIGO, CM_CODIGO, '+
                                         'BI_ACCION, BI_FEC_MOV, BI_KIOSCO, BI_UBICA ' +
                                         'from BITKIOSCO where ' +
                                          '( %0:s >= %1:s and %0:s < %2:s ) and '+
                                          '( ( %3:d = -1 ) or ( BI_TIPO = %3:d ) ) and '+
                                          '( ( %4:d <= 0 ) or ( BI_ACCION = %4:d  ) ) and '+
                                          '( ( %5:d = 0 ) or ( CB_CODIGO = %5:d ) ) and '+
                                          '( ( %6:s = %7:s ) or ( CM_CODIGO = %6:s ) ) '+
                                          'order by BI_FECHA DESC, BI_HORA DESC';
          Q_KIOSCO2_INSERT_ACCESO_BOTON : Result :=  'INSERT INTO KBOTON_ACC( KS_CODIGO, KB_ORDEN, GR_CODIGO ) '+
                                                     'VALUES ( :KS_CODIGO, :KB_ORDEN, :GR_CODIGO )';
          {
          Q_KIOSCO2_ACCESO_BOTON: Result := 'select KS_CODIGO, KB_ORDEN, GR_CODIGO '+
                                            'from KBOTON_ACC where  ' +
                                            '( KS_CODIGO = %s ) and '+
                                            '( KB_ORDEN = %d )';  }

          Q_KIOSCO2_ACCESO_BOTON: Result := 'select KS_CODIGO, KB_ORDEN, GR_CODIGO '+
                                            'from KBOTON_ACC where  ' +
                                            '( KS_CODIGO = %s ) ';

          Q_KIOSCO2_ACCESO_EMPLEADO: Result:= 'select USUARIO.CM_CODIGO, USUARIO.CB_CODIGO, USUARIO.GR_CODIGO, KBOTON_ACC.KB_ORDEN, '+
                                              ' KBOTON_ACC.KS_CODIGO from USUARIO left outer join KBOTON_ACC ' +
                                              ' on ( KBOTON_ACC.GR_CODIGO = USUARIO.GR_CODIGO ) where ( USUARIO.CB_CODIGO = %d and USUARIO.CM_CODIGO = %s ) ';

          Q_EMPLEADOS_ACTUALIZA_PASS: Result := 'UPDATE COLABORA SET CB_PASSWRD = %s WHERE CB_CODIGO = %d';

          Q_GLOBAL_NIVELES: Result := 'select GL_CODIGO, GL_FORMULA from GLOBAL where ( GL_CODIGO > %d ) and ( GL_CODIGO <= %d ) order by GL_CODIGO';

          Q_KIOSCO_EMPLEADO: Result := 'select C.CB_CODIGO, ' + K_PRETTYNAME + ' as PRETTYNAME, C.CB_APE_PAT, C.CB_APE_MAT, C.CB_NOMBRES, I.IM_BLOB, '+
                                       'C.CB_ACTIVO, C.CB_FEC_ING, C.CB_FEC_BAJ, C.CB_DER_GOZ, C.CB_DER_PAG, C.CB_FEC_VAC, C.CB_CREDENC, '+
                                       'C.CB_CALLE, C.CB_COLONIA, C.CB_CODPOST, C.CB_TEL, C.CB_RFC, C.CB_SEGSOC, C.CB_CURP, C.CB_FEC_NAC, C.CB_LUG_NAC, '+
                                       'C.CB_FEC_ANT, C.CB_FEC_CON, C.CB_CONTRAT, CO.TB_DIAS, C.CB_PUESTO, P.PU_DESCRIP, C.CB_TURNO, C.CB_PASSWRD, T.TU_DESCRIP, ' +
                                       'C.CB_NOMINA, C.CB_CLASIFI, C.CB_NIVEL0, CL.TB_ELEMENT %s ' +
                                       'from COLABORA C '+
                                       'left outer join IMAGEN I on ( I.CB_CODIGO = C.CB_CODIGO ) and ( I.IM_TIPO = ''%s'' ) '+
                                       'left outer join CONTRATO CO on ( CO.TB_CODIGO = C.CB_CONTRAT ) '+
                                       'left outer join PUESTO P on ( P.PU_CODIGO = C.CB_PUESTO ) '+
                                       'left outer join CLASIFI CL on ( CL.TB_CODIGO = C.CB_CLASIFI ) '+
                                       'left outer join TURNO T on ( T.TU_CODIGO = C.CB_TURNO ) %s '+
                                       'where ( C.CB_CODIGO = %d )';

          Q_DELETE_BOTON_ACC: Result:=  'delete from KBOTON_ACC where ( KS_CODIGO = %s )';

          Q_DELETE_BOTON: Result:= 'DELETE FROM KBOTON WHERE KS_CODIGO = %s';

          Q_USUARIOS_CONFI_REP_EMAIL : Result := 'select KC.TB_CODIGO,KC.KEM_USERS,KC.CM_CODIGO,N.TB_ELEMENT from KEM_CONFI KC '+
                                                 ' left outer join NIVEL0 N on KC.TB_CODIGO = N.TB_CODIGO ' +
                                                 ' where KC.CM_CODIGO = %s ';

          Q_CONFIDENCIALIDADES : Result := 'select TB_CODIGO,TB_ELEMENT from NIVEL0';

          Q_DEL_CONFI_USERS : Result := 'delete from KEM_CONFI where CM_CODIGO = ''%s''';

          Q_INS_CONFI_USERS : Result := 'insert into KEM_CONFI ( CM_CODIGO,TB_CODIGO,KEM_USERS ) values ( :CM_CODIGO,:TB_CODIGO,:KEM_USERS )';

          Q_GET_IMPRESIONES :Result := 'select Count(*)as Impresiones from KIOS_IMPR where CB_CODIGO = %d and CM_CODIGO = ''%s'' and '+
                                       ' PE_TIPO = %d and '+
                                       ' PE_NUMERO = %d and '+
                                       ' PE_YEAR = %d ';
          Q_KIOSCO2_ENCUESTAS: Result := 'select E.ENC_CODIGO, E.ENC_NOMBRE, E.ENC_PREGUN, E.ENC_CONFID, E.ENC_VOTOSE,' +
                                         'E.ENC_STATUS, E.US_CODIGO, E.ENC_FECINI, E.ENC_FECFIN, E.ENC_HORINI, ' +
                                         'E.ENC_HORFIN, E.ENC_MRESUL, E.ENC_FILTRO, E.ENC_COMPAN, E.ENC_KIOSCS, ' +
                                         'E.ENC_OPCION, E.ENC_MSJCON, ' +
                                         '( select KO.OP_TITULO from KOPCIONES KO where KO.ENC_CODIGO = E.ENC_CODIGO ' +
                                         '  and KO.OP_ORDEN = dbo.SP_MAX_VOTOS_ENCUESTA( E.ENC_CODIGO, 1 ) ) OPCION, ' +
                                         '( select count(*) from KVOTOS KV where KV.ENC_CODIGO = E.ENC_CODIGO ) VOTOS ' +
                                         'from KENCUESTAS E where ' +
                                         '( ( %0:d = -1 ) or ( E.ENC_STATUS = %0:d ) ) and ' +
                                         '( ( E.ENC_FECINI <= %1:s ) and ( E.ENC_FECFIN >= %2:s ) ) ' +
                                         'order by ENC_FECINI DESC, ENC_HORINI DESC';
          Q_GET_MAX_ENCUESTA: Result := 'select max(ENC_CODIGO) as MAXIMO from KENCUESTAS';
          Q_OPCIONES_ENCUESTAS: Result := 'select O.ENC_CODIGO,O.OP_ORDEN,O.OP_TITULO,O.OP_DESCRIP,O.US_CODIGO, ' +
                                          'dbo.SP_VOTOS_ENCUESTA( %0:d, O.OP_ORDEN ) as VOTOS ' +
                                          'from KOPCIONES O where ' +
                                          '(O.ENC_CODIGO = %0:d ) ' +
                                          'order by O.OP_ORDEN';
          Q_KIOSCO2_INS_OPCIONES:Result :=  'INSERT INTO KOPCIONES (OP_ORDEN,OP_TITULO,OP_DESCRIP,ENC_CODIGO,US_CODIGO ) '+
                                                            ' VALUES (:OP_ORDEN,:OP_TITULO,:OP_DESCRIP,:ENC_CODIGO,:US_CODIGO ) ';
          Q_CONSULTA_EMP_ID : Result := 'select CM_DIGITO, CB_CODIGO from EMP_ID, COMPANY where EMP_ID.CM_CODIGO = COMPANY.CM_CODIGO and ID_NUMERO =''%s''';

          //BIOMENRICO
          Q_CONSULTA_EMP_BIO : Result := 'SELECT EB.ID_NUMERO, EB.CM_CODIGO, EB.CB_CODIGO, EB.GP_CODIGO, EB.IV_CODIGO, C.CM_DIGITO ' +
                                         'FROM EMP_BIO AS EB INNER JOIN COMPANY AS C ON EB.CM_CODIGO = C.CM_CODIGO ' +
                                         'WHERE (EB.ID_NUMERO = %d)';
          Q_HUELLA_REPORTA_INSERTA: Result := 'exec SP_REPORTAHUELLA ''%s'', %s';
          Q_HUELLA_REPORTA_ELIMINA: Result := 'delete from COM_HUELLA where CH_CODIGO = ''%s''';

          Q_HUELLA_OBTENER_MAQUINA: Result := 'SELECT V.HU_ID, V.ID_NUMERO, V.CM_CODIGO, V.CB_CODIGO, V.HU_INDICE, V.HU_HUELLA, V.IV_CODIGO, V.HU_TIMEST, V.TIPO, C.CH_CODIGO ' +
                                              'FROM V_HUELLAS AS V LEFT OUTER JOIN ' +
                                                   'COM_HUELLA AS C ON V.HU_ID = C.HU_ID AND C.CH_CODIGO = ''%s'' ' +
                                              'WHERE (C.CH_CODIGO IS NULL) %s'; //Mejoras de Biometrico
          Q_HUELLA_OBTENER_TODOS: Result := 'SELECT H.HU_ID, E.ID_NUMERO, E.CM_CODIGO, E.CB_CODIGO, H.HU_INDICE, H.HU_HUELLA, H.HU_TIMEST ' +
                                            'FROM HUELLAGTI AS H INNER JOIN EMP_BIO AS E ON H.CM_CODIGO = E.CM_CODIGO AND H.CB_CODIGO = E.CB_CODIGO'; //BIOMETRICO
          Q_TOTAL_DISPOSIT: Result := 'select count(*) as Total from DISXCOM where DI_TIPO = %d';
     else
         Result := '';
     end;
end;

class procedure TdmServerKiosco.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerKiosco.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerKiosco.MtsDataModuleDestroy(Sender: TObject);
begin
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

function TdmServerKiosco.GetGlobales: OleVariant;
var
   oSuperGlobal: TdmSuperGlobal;
begin
     oSuperGlobal := TdmSuperGlobal.Create;
     try
        try
           with oZetaProvider do
           begin
                EmpresaActiva := Comparte;
           end;
           with oSuperGlobal do
           begin
                Provider := oZetaProvider;
                Result := ProcesaLectura;
           end;
           SetComplete;
        except
              SetAbort;
              raise;
        end;
     finally
            FreeAndNil( oSuperGlobal );
     end;
end;

function TdmServerKiosco.EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
  out Error: WideString): OleVariant;
var
   Reporte: TdmServerReporteador;
begin
     InitCreator;
     Reporte := TdmServerReporteador.Create( oZetaProvider, oZetaCreator );
     try
        Result := Reporte.EvaluaParam( Empresa,
                                       AgenteSQL,
                                       Parametros,
                                       Error );
     finally
            FreeAndNil(Reporte);
     end;
     SetComplete;
end;

procedure TdmServerKiosco.SetTablaInfo(const eTabla: eTipoTabla);
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              ekCarruseles: SetInfo( 'KSHOW', 'KW_CODIGO,KW_NOMBRE,KW_SCR_DAT,KW_TIM_DAT,KW_GET_NIP,CM_CODIGO', 'KW_CODIGO' );
              ekCarruselesInfo: SetInfo( 'KSHOWINF', 'KW_CODIGO,KI_ORDEN,KI_TIMEOUT,KI_URL,KI_REFRESH', 'KW_CODIGO,KI_ORDEN' );
              ekMarcosInformacion: SetInfo( 'KSCREEN', 'KS_CODIGO,KS_NOMBRE,KS_PF_SIZE,KS_PF_COLR,KS_PF_ALIN,KS_PF_URL,KS_PB_SIZE,KS_PB_COLR,KS_PB_ALIN,KS_BTN_ALT,KS_BTN_SEP,KS_EST_NOR,KS_EST_SEL,KS_BITMAP', 'KS_CODIGO' );
              ekBotones: SetInfo('KBOTON','KS_CODIGO,KB_ORDEN,KB_TEXTO,KB_BITMAP,KB_ACCION,KB_URL,KB_SCREEN,KB_ALTURA,KB_LUGAR,KB_SEPARA,KB_REPORTE,KB_POS_BIT,KB_RESTRIC,KB_USA_EMPL', 'KS_CODIGO,KB_ORDEN' );
              ekEstilosBotones: SetInfo('KESTILO', 'KE_CODIGO,KE_NOMBRE,KE_COLOR,KE_F_NAME,KE_F_SIZE,KE_F_COLR,KE_F_BOLD,KE_F_ITAL,KE_F_SUBR,KE_BITMAP', 'KE_CODIGO' );
              eAccesoBoton: SetInfo( 'KBOTON_ACC', 'KS_CODIGO, KB_ORDEN, GR_CODIGO ', 'KS_CODIGO, KB_ORDEN');
              eConfiUsuarios : SetInfo ('KEM_CONFI','CM_CODIGO,KEM_USERS,TB_CODIGO','CM_CODIGO,TB_CODIGO');
              eKImpresiones : SetInfo ('KIOS_IMPR','CM_CODIGO,CB_CODIGO,PE_TIPO,PE_YEAR,PE_NUMERO,KI_FECHA,KI_KIOSCO,KI_HORA,RE_CODIGO','KI_HORA');
              eEncuestas: SetInfo('KENCUESTAS','ENC_CODIGO,ENC_NOMBRE,ENC_PREGUN,ENC_CONFID,ENC_VOTOSE,ENC_STATUS,US_CODIGO,ENC_FECINI,ENC_FECFIN,ENC_HORINI,ENC_HORFIN,ENC_MRESUL,ENC_FILTRO,ENC_COMPAN,ENC_KIOSCS,ENC_OPCION,ENC_MSJCON','ENC_CODIGO');
              eOpcEncuesta: SetInfo('KOPCIONES','ENC_CODIGO,OP_ORDEN,OP_TITULO,OP_DESCRIP,US_CODIGO,OP_TOTAL','OP_ORDEN,ENC_CODIGO');
              eVotosEncuesta: SetInfo('KVOTOS','ENC_CODIGO,CB_CODIGO,CM_CODIGO,CB_CREDENC,VO_VALOR,VO_FECHA,VO_HORA,VO_KIOSCO','ENC_CODIGO,CB_CODIGO,CM_CODIGO,CB_CREDENC');
          else
              SetInfo( GetNombreTabla( eTabla ), 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO', 'TB_CODIGO' );
          end;
     end;
end;

function TdmServerKiosco.GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
  out Error: WideString): OleVariant;
var
   Reporte: TdmServerReporteador;
begin
     InitCreator;
     Reporte := TdmServerReporteador.Create( oZetaProvider, oZetaCreator );
     try
        with oZetaProvider do
        begin
             EmpresaActiva := Empresa;
             AsignaparamList( Parametros );
        end;
        Result := Reporte.GeneraSQL( Empresa,
                                     AgenteSQL,
                                     Parametros,
                                     Error );
     finally
            FreeAndNil(Reporte);
     end;
     SetComplete;
end;

function TdmServerKiosco.GetBotones( const sPantalla: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_BOTONES ), [ EntreComillas(sPantalla) ] ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetCarrusel(const sShow: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_CARRUSEL ), [ EntreComillas(sShow) ] ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetCarruselesInfo( const Codigo: WideString): OleVariant;
begin
     SetTablaInfo( ekCarruselesInfo );
     with oZetaProvider do
     begin
          with TablaInfo do
          begin
               Filtro := Format( 'KW_CODIGO = %s', [ EntreComillas( Codigo ) ]);
               Result := GetTabla( Comparte );
          end;
     end;
end;

function TdmServerKiosco.GetComparte: OleVariant;
const
     K_COMPARTE_CODIGO = 'XCOMPARTEY';
begin
     try
        with cdsDatos do
        begin
             with oZetaProvider do
             begin
                  EmpresaActiva := Comparte;
                  Lista := OpenSQL( Comparte, Format( GetScript( Q_COMPANYS_NINGUNA ), [ K_COMPARTE_CODIGO ] ), False );
             end;
             Append;
             FieldByName( 'CM_CODIGO' ).AsString := K_COMPARTE_CODIGO;
             FieldByName( 'CM_NOMBRE' ).AsString := 'Base De Datos Comparte';
             with oZetaProvider do
             begin
                  FieldByName( 'CM_ALIAS' ).AsString := Comparte[ P_ALIAS ];
                  FieldByName( 'CM_USRNAME' ).AsString := Comparte[ P_USER_NAME ];
                  FieldByName( 'CM_PASSWRD' ).AsString := ZetaServerTools.Encrypt( Comparte[ P_PASSWORD ] );
                  FieldByName( 'CM_DATOS' ).AsString := Comparte[ P_DATABASE ];
             end;
             FieldByName( 'CM_NIVEL0' ).AsString := '';
             FieldByName( 'CM_DIGITO' ).AsString := '';
             Post;
             Result := Lista;
        end;
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

function TdmServerKiosco.GetEstilo(const sEstilo: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_ESTILO ), [ EntreComillas(sEstilo) ] ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetKioscos: OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, GetScript( Q_KIOSCO2_GET_KIOSCOS ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetPantalla( const sPantalla: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_PANTALLA ), [ EntreComillas(sPantalla) ] ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetShow(const sShow: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_KIOSCO2_GET_SHOW ), [ EntreComillas(sShow) ] ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetTabla(Tabla: Integer): OleVariant;
{const
     K_CLAVE_USUARIO = 'US_PASSWRD';}
var
   eTabla: eTipoTabla;
begin
     eTabla := eTipoTabla( Tabla );
     try{Se coment� cuando se traspaso lo referente a kiosko de comparte.dll a kiosko.dll
        case eTabla of
             eTNoticia:
             begin
                  with oZetaProvider do
                  begin
                       Result := OpenSQL( Comparte, GetScript( Q_TIPO_NOTICIAS ), True );
                  end;
             end;
        else
            begin}
                 SetTablaInfo( eTabla );
                 with oZetaProvider do
                 begin
                      {
                      with TablaInfo do
                      begin
                           case eTabla of
                                eUsuarios: Filtro := 'US_CODIGO <> 0';
                           end;
                      end;   }
                      Result := GetTabla( Comparte );
                 end;
                 {case eTabla of
                      eUsuarios: Result := DecodificaClaves( Result, K_CLAVE_USUARIO );
                 end;
            end;
        end;  }
     except
           SetOLEVariantToNull( Result );
           raise;
     end;
     SetComplete;
end;

function TdmServerKiosco.GetValoresActivos(const Kiosco: WideString; Empleado: Integer; const Empresa: WideString): WideString;
var
   sValorIni: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          InitGlobales;
          sValorIni := VerificaDirURL( GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ ) ) + GetGlobalString( K_KIOSCO_VALORES_ACTIVOS_URL );
     end;
     Result := Format( sValorIni, [ Kiosco, Empleado, Empresa  ] );
     SetComplete;
end;

function TdmServerKiosco.GrabaCarruseles(Delta, DeltaCarruselInfo: OleVariant; const Codigo: WideString;
                                          var CarruselInfo: OleVariant; var ErrorCount, ErrorCountInfo: Integer): OleVariant;
const
      K_DELETE = 'DELETE FROM KSHOWINF WHERE KW_CODIGO = %s';
var
   iPosicion: integer;
begin
     ErrorCount := 0;
     ErrorCountInfo := 0;
     //FCarrusel := Codigo;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if ( Delta = Null ) then
             Result := Delta
          else
          begin
               SetTablaInfo( ekCarruseles );
               Result := GrabaTabla( Comparte, Delta, ErrorCount );
          end;
          {if ( ErrorCount = 0 ) then
          begin
               if ( DeltaCarruselInfo = null ) then
                  CarruselInfo := DeltaCarruselInfo
               else
               begin
                    SetTablaInfo( ekCarruselesInfo );
                    TablaInfo.BeforeUpdateRecord := BeforeUpdateCarruselInfo;
                    CarruselInfo := GrabaTablaGrid( Comparte, DeltaCarruselInfo, ErrorCountInfo );
               end;
          end;}
          if ( ErrorCount = 0 ) then
          begin
               if ( DeltaCarruselInfo <> null ) and StrLleno( Codigo ) then
               begin
                    FQuery := CreateQuery( GetScript( Q_KIOSCO2_INSERT_KSHOWINF ) );
                    try
                        ExecSQL( EmpresaActiva, Format( K_DELETE, [ Comillas( Codigo ) ] ) );
                        iPosicion := 0;

                        with cdsDatos do
                        begin
                             IndexFieldNames := VACIO;
                             Data := DeltaCarruselInfo;
                             IndexFieldNames := 'KW_CODIGO;KI_ORDEN';
                             First;
                             while NOT EOF do
                             begin
                                  Inc(iPosicion);
                                  ParamAsString( FQuery, 'KW_CODIGO', Codigo );
                                  ParamAsInteger( FQuery, 'KI_ORDEN',  iPosicion );
                                  ParamAsFloat( FQuery,  'KI_TIMEOUT',  FieldByName('KI_TIMEOUT').AsFloat );
                                  ParamAsString( FQuery,  'KI_URL', FieldByName('KI_URL').AsString );
                                  ParamAsString( FQuery, 'KI_REFRESH', FieldByName('KI_REFRESH').AsString );
                                  Ejecuta(FQuery);

                                  Next;
                             end;
                        end;
                    finally
                           FreeAndNil(FQuery)
                    end;
               end;
               CarruselInfo := DeltaCarruselInfo
          end;
     end;
     SetComplete;
end;

function TdmServerKiosco.GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant;
var
   oSuperGlobal: TdmSuperGlobal;
begin
     oSuperGlobal := TdmSuperGlobal.Create;
     try
        try
           with oZetaProvider do
           begin
                EmpresaActiva := Comparte;
           end;
           with oSuperGlobal do
           begin
                Provider := oZetaProvider;
                ActualizaDiccion := False;
                Result := ProcesaEscritura( Delta );
                ErrorCount := Errores;
           end;
           SetComplete;
        except
              SetAbort;
              raise;
        end;
     finally
            FreeAndNil( oSuperGlobal );
     end;
end;

function TdmServerKiosco.GrabaMarcosInfo(Delta: OleVariant; DeltaBotones: OleVariant; DeltaAccesos: OleVariant; const Codigo: WideString;
                                         var oAccesoBotones: OleVariant; var ErrorCount: Integer; var ErrorCountBotones: Integer): OleVariant;
 var
    iPosicion: integer;

     procedure GrabaAccesoBoton;
     var
        oAccesos: TClientDataSet;
     begin
          with oZetaProvider do
          begin
               if ( DeltaAccesos <> null ) then
               begin
                    //Borra los registros de ese marco y los vuelve a insertar
                    FQuery := CreateQuery( GetScript( Q_KIOSCO2_INSERT_ACCESO_BOTON ) );
                    ExecSQL( EmpresaActiva, Format( GetScript( Q_DELETE_BOTON_ACC ), [ Comillas( Codigo ) ] ) );

                    oAccesos:= TClientDataSet.Create( Self );
                    try
                       with oAccesos do
                       begin
                            Data := DeltaAccesos;
                            First;
                            while NOT EOF do
                            begin
                                 //cdsDatos nada mas trae los registros de un marco en especifico. El locate sobre KS_CODIGO es nada mas para asegurarnos
                                 if cdsDatos.Locate ('KS_CODIGO;KB_ORDEN', VarArrayOf( [ Codigo, FieldByName('KB_ORDEN').AsInteger ] ), [] ) and
                                    ( cdsDatos.FieldByName('KB_RESTRIC').AsString = K_GLOBAL_SI ) then
                                 begin
                                      ParamAsString( FQuery, 'KS_CODIGO', Codigo );
                                      ParamAsInteger( FQuery, 'KB_ORDEN',  FieldByName('KB_ORDEN').AsInteger );
                                      ParamAsInteger( FQuery, 'GR_CODIGO', FieldByName('GR_CODIGO').AsInteger );
                                      Ejecuta(FQuery);
                                 end;
                                 Next;
                            end;
                       end;
                       oAccesoBotones:= GetAccesoBoton( Codigo );
                    finally
                           FreeAndNil( oAccesos );
                           FreeAndNil(FQuery);
                    end;
               end;
          end;
     end;

begin
     ErrorCount := 0;
     ErrorCountBotones := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if VarIsNull( Delta ) then
             Result := Delta
          else
          begin
               SetTablaInfo( ekMarcosInformacion );
               //TablaInfo.BeforeUpdateRecord := BeforeUpdateSesion;
               Result := GrabaTabla( Comparte, Delta, ErrorCount );
          end;
          if ( ErrorCount = 0 ) then
          begin
               EmpiezaTransaccion;
               try
               if ( StrLleno ( Codigo ) ) then
               begin
                    if not( VarIsNull( DeltaBotones ) ) then
                    begin
                         FQuery := CreateQuery( GetScript( Q_KIOSCO2_INSERT_BOTON ) );
                         ExecSQL( EmpresaActiva, Format( GetScript( Q_DELETE_BOTON ), [ Comillas( Codigo ) ] ) );
                         iPosicion := 0;

                         with cdsDatos do
                         try
                              IndexFieldNames := VACIO;
                              Data := DeltaBotones;
                              IndexFieldNames := 'KS_CODIGO;KB_ORDEN';
                              First;
                              while NOT EOF do
                              begin
                                   Inc(iPosicion);
                                   ParamAsString( FQuery, 'KS_CODIGO', Codigo );
                                   ParamAsInteger( FQuery, 'KB_ORDEN',  iPosicion );
                                   ParamAsString( FQuery,  'KB_TEXTO',  FieldByName('KB_TEXTO').AsString );
                                   ParamAsString( FQuery,  'KB_BITMAP', FieldByName('KB_BITMAP').AsString );
                                   ParamAsInteger( FQuery, 'KB_ACCION', FieldByName('KB_ACCION').AsInteger );
                                   ParamAsString( FQuery,  'KB_URL',    FieldByName('KB_URL').AsString );
                                   ParamAsString( FQuery,  'KB_SCREEN', FieldByName('KB_SCREEN').AsString );
                                   ParamAsInteger( FQuery, 'KB_ALTURA', FieldByName('KB_ALTURA').AsInteger );
                                   ParamAsInteger( FQuery, 'KB_LUGAR',  FieldByName('KB_LUGAR').AsInteger );
                                   ParamAsInteger( FQuery, 'KB_SEPARA',  FieldByName('KB_SEPARA').AsInteger );
                                   ParamAsInteger( FQuery, 'KB_REPORTE',  FieldByName('KB_REPORTE').AsInteger );
                                   ParamAsInteger( FQuery, 'KB_POS_BIT',  FieldByName('KB_POS_BIT').AsInteger );
                                   ParamAsString( FQuery, 'KB_RESTRIC', FieldByName('KB_RESTRIC').AsString );
                                   ParamAsString( FQuery, 'KB_USA_EMPL', FieldByName('KB_USA_EMPL').AsString );
                                   ParamAsString( FQuery, 'KB_IMP_DIR', FieldByName('KB_IMP_DIR').AsString );
                                 //  ParamAsString( FQuery,  'KB_EMPRESA',  FieldByName('KB_EMPRESA').AsString );
                                   Ejecuta(FQuery);
                                   Next;
                              end;
                         finally
                                FreeAndNil(FQuery);
                         end;
                    end;
                    //oBotones := DeltaBotones;
                    GrabaAccesoBoton;
                    TerminaTransaccion( TRUE );
               end;
               except
                    on Error: Exception do
                    begin
                         RollBackTransaccion;
                         Inc(ErrorCountBotones);
                         raise;
                    end;
              end;
          end;
     end;
     SetComplete;
end;

function TdmServerKiosco.GrabaTabla(Tabla: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant;
var
   eTabla: eTipoTabla;
begin
     eTabla := eTipoTabla( Tabla );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          SetTablaInfo( eTabla );
     end;
     {Se coment� cuando se traspaso lo referente a kiosko, de comparte.dll a kiosko.dll
     case eTabla of
          eUsuarios:
          begin
               FSeguridadInfo := TSeguridadInfo.Create( oZetaProvider );
               try
                  FSeguridadInfo.PasswordLogBegin;
                  try
                     with oZetaProvider do
                     begin
                          with TablaInfo do
                          begin
                               BeforeUpdateRecord := ClaveUsuarioCodifica;
                               if FSeguridadInfo.UsaPasswordLog then
                                  AfterUpdateRecord := ClaveUsuarioGraba;
                          end;
                          Result := GrabaTabla( Comparte, Delta, ErrorCount );
                     end;
                  finally
                         FSeguridadInfo.PasswordLogEnd;
                  end;
               finally
                      FreeAndNil( FSeguridadInfo );
               end;
          end;
     else
         begin }
              with oZetaProvider do
              begin
                   Result := GrabaTabla( Comparte, Delta, ErrorCount );
              end; {
         end;
     end;}
     SetComplete;
end;

function TdmServerKiosco.GetNombreTabla(const eTabla: eTipoTabla): string;
begin
     case eTabla of
          ekCarruseles: Result := 'KSHOW';
          ekCarruselesInfo: Result := 'KSHOWINF';
          ekMarcosInformacion: Result := 'KSCREEN';
          ekEstilosBotones: Result := 'KESTILO';
          ekBotones: Result := 'KBOTON';
     end;
end;

procedure TdmServerKiosco.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

function TdmServerKiosco.GetBitacora(Parametros: OleVariant): OleVariant;
var
   sSQL, CampoFecha: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               CampoFecha := 'BI_FECHA';
               sSQL := Format( GetScript( Q_KIOSCO2_BITACORA ),
                             [ CampoFecha,
                               DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ),
                               DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate + 1 ),
                               ParamByName( 'Tipo' ).AsInteger,
                               ParamByName( 'Accion' ).AsInteger,
                               ParamByName( 'EmpleadoKiosco' ).AsInteger,
                               EntreComillas( ParamByName( 'Empresa' ).AsString ),
                               EntreComillas(VACIO) ] );
          end;
          Result := OpenSQL( EmpresaActiva, sSQL, True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GrabaBitacora(oParametros: OleVariant): OleVariant;
var
   iEmpleado: Integer;
   eTipo: eTipoBitacora;
   eClase: eClaseBitacora;
   sEmpresa, sTexto, sKiosco, sMarco: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( oParametros );
          with ParamList do
          begin
               eClase:= eClaseBitacora( ParamByName('Clase').AsInteger);
               eTipo:= eTipoBitacora( ParamByName('Tipo').AsInteger );
               sEmpresa:= ParamByName('Empresa').AsString;
               sTexto:= ParamByName('Texto').AsString;
               sMarco:= ParamByName('Marco').AsString;
               sKiosco:= ParamByName('Kiosco').AsString;
               iEmpleado:= ParamByName('Empleado').AsInteger;
          end;
          EscribeBitacoraKiosco(eTipo,sEmpresa,eClase,sTexto,sMarco,sKiosco,iEmpleado);
     end;
end;

function TdmServerKiosco.GetAccesoBoton(const sPantalla: WideString ): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Comparte;
          Result:= OpenSQL( EmpresaActiva,
                            Format( GetScript( Q_KIOSCO2_ACCESO_BOTON ), [ EntreComillas( sPantalla ) ] ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetAccesoEmp(Empresa: OleVariant; iEmpleado: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Comparte;
          Result:= OpenSQL( EmpresaActiva,
                            Format( GetScript( Q_KIOSCO2_ACCESO_EMPLEADO ), [ iEmpleado, EntreComillas( Empresa[ P_CODIGO ] ) ] ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.SetClaveEmpleadoKiosco(Empresa: OleVariant; Empleado: Integer; const Clave: WideString): Integer;
var
   Encriptada: string;
begin
     Result := 0;
     Encriptada := ZetaServerTools.Encrypt( Clave );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          ExecSQL( Empresa, Format( GetScript( Q_EMPLEADOS_ACTUALIZA_PASS ), [ EntreComillas( Encriptada ), Empleado ] ) );
     end;
     SetComplete;
end;

function TdmServerKiosco.ReseteaPassword(Empresa, Empleados: OleVariant): Integer;
begin
     Result := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          EjecutaAndFree( Format( GetScript( Q_EMPLEADOS_ACTUALIZA_PASS ), [ EntreComillas( ' ' ), Integer( Empleados ) ] ) );
     end;
     SetComplete;
end;

function TdmServerKiosco.KioscoEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant;
var
   FDataset: TZetaCursor;
   iNivel: Integer;
   sNivel, sCampos, sJoin: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FDataset := CreateQuery( Format( GetScript( Q_GLOBAL_NIVELES ), [ K_GLOBAL_NIVEL_BASE, K_GLOBAL_NIVEL_BASE + K_GLOBAL_NIVEL_MAX ] ) );
          try
             with FDataset do
             begin
                  Active := True;
                  sCampos := '';
                  sJoin := '';
                  while not Eof do
                  begin
                       iNivel := FieldByName( 'GL_CODIGO' ).AsInteger - K_GLOBAL_NIVEL_BASE;
                       sNivel := Trim( Fix( FieldByName( 'GL_FORMULA' ).AsString ) );
                       if ZetaCommonTools.StrVacio( sNivel ) then
                       begin
                            sNivel := K_TOKEN_NIL;
                            { Si el nivel est� deshabilitado, enviar K_TOKEN_NIL en el valor de TB_ELEMENT%d }
                            sCampos := sCampos + Format( Q_NIVEL_NO_JOIN, [ iNivel, K_TOKEN_NIL ] );
                       end
                       else
                       begin
                            sCampos := sCampos + Format( Q_NIVEL_CAMPO, [ iNivel ] );
                       end;
                       { Concatenar la descripci�n del Nivel }
                       sCampos := sCampos + Format( ', ''%s'' as GL_NIVEL%d', [ sNivel, iNivel ] );
                       sJoin := sJoin + Format( Q_NIVEL_JOIN, [ iNivel ] );
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          cdsDatos.Data := OpenSQL( Empresa, Format( GetScript( Q_KIOSCO_EMPLEADO ), [ sCampos, K_FOTO, sJoin, Empleado ] ), True );
          with cdsDatos do
          begin
               if not IsEmpty then
               begin
                    Edit;
                    with FieldByName( 'CB_PASSWRD' ) do
                    begin
                         AsString := ZetaServerTools.Decrypt( AsString );
                    end;
                    Post;
               end;
          end;
          Result := cdsDatos.Data;
     end;
     SetComplete;
end;

{
function TdmServerKiosco.GrabaAccesoBoton(oDelta: OleVariant; ErrorCount: Integer; iOrdenBoton: Integer;
                                                  const sPantalla: WideString): OleVariant;
const
     //K_DELETE = 'delete from KBOTON_ACC where ( KS_CODIGO = %s ) and ( KB_ORDEN = %d ) ';
     K_DELETE = 'delete from KBOTON_ACC where ( KS_CODIGO = %s )';
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          EmpiezaTransaccion;
          try
             //ExecSQL( EmpresaActiva, Format( K_DELETE, [ EntreComillas( sPantalla ), iOrdenBoton ] ) );
             ExecSQL( EmpresaActiva, Format( K_DELETE, [ EntreComillas( sPantalla ) ] ) );
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( FALSE );
                     raise;
                end;
          end;
          if VarIsNull( oDelta ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eAccesoBoton );
               Result := GrabaTabla( EmpresaActiva, oDelta, ErrorCount );
          end;
     end;
     SetComplete;
end;  }

function TdmServerKiosco.Fix(const sValue: String): String;
const
     LETRA_A_MINUSCULA = Ord( '�' );
     LETRA_A_MAYUSCULA = Ord( '�' );
     LETRA_E_MINUSCULA = Ord( '�' );
     {
     LETRA_E_MAYUSCULA = Ord( '�' );
     }
     LETRA_I_MINUSCULA = Ord( '�' );
     {
     LETRA_I_MAYUSCULA = Ord( '�' );
     }
     LETRA_O_MINUSCULA = Ord( '�' );
     {
     LETRA_O_MAYUSCULA = Ord( '�' );
     }
     LETRA_U_MINUSCULA = Ord( '�' );
     {
     LETRA_U_MAYUSCULA = Ord( '�' );
     }
     LETRA_ENE_MINUSCULA = Ord( '�' );
     LETRA_ENE_MAYUSCULA = Ord( '�' );
     LETRA_U_DIERESIS = Ord( '�' );
     LETRA_ADMIRACION = Ord( '�' );
     LETRA_PREGUNTA = Ord( '�' );
var
   i, iLen: Integer;
   cValue: Char;
begin
     iLen  := Length( sValue );
     Result := '';
     for i := 1 to iLen do
     begin
        cValue := sValue[ i ];
        case Ord( cValue ) of
               LETRA_A_MINUSCULA: cValue := 'a';
               LETRA_A_MAYUSCULA: cValue := 'a';
               LETRA_E_MINUSCULA: cValue := 'e';
               {
               LETRA_E_MAYUSCULA: cValue := 'e';
               }
               LETRA_I_MINUSCULA: cValue := 'i';
               {
               LETRA_I_MAYUSCULA: cValue := 'i';
               }
               LETRA_O_MINUSCULA: cValue := 'o';
               {
               LETRA_O_MAYUSCULA: cValue := 'o';
               }
               LETRA_U_MINUSCULA: cValue := 'u';
               {
               LETRA_U_MAYUSCULA: cValue := 'u';
               }
               LETRA_ENE_MINUSCULA: cValue := 'n';
               LETRA_ENE_MAYUSCULA: cValue := 'N';
               LETRA_U_DIERESIS: cValue := 'u';
               LETRA_ADMIRACION: cValue := '!';
               LETRA_PREGUNTA: cValue := '?';
        end;
        Result := Result + cValue;
     end;
end;

function TdmServerKiosco.GetUsuariosConfidencialidad(const Empresa: WideString): OleVariant;
var
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          cdsDatos.Data := OpenSQL( EmpresaActiva, Format( GetScript( Q_USUARIOS_CONFI_REP_EMAIL ), [ EntreComillas( Empresa) ] ), True );
          FDataset := CreateQuery( GetScript( Q_CONFIDENCIALIDADES ) );
          try
             with FDataset do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       if not cdsDatos.Locate('TB_CODIGO',FieldByName('TB_CODIGO').AsString ,[])then
                       begin
                            cdsDatos.Append;
                            cdsDatos.FieldByName('TB_CODIGO').AsString := FieldByName('TB_CODIGO').AsString;
                            cdsDatos.FieldByName('CM_CODIGO').AsString := 'DEMO';
                            cdsDatos.FieldByName('KEM_USERS').AsString := VACIO;
                            cdsDatos.FieldByName('TB_ELEMENT').AsString := FieldByName('TB_ELEMENT').AsString;
                       end;
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          Result := cdsDatos.Data;
     end;
     SetComplete;
end;

function TdmServerKiosco.GrabaUsuariosConfidencialidad(const EmpresaCodigo:WideString;Delta: OleVariant; out ErrorCount: Integer): OleVariant;
var
   sListaUsuarios: String;

  procedure BorrarListaUsuarios;
  begin
       with oZetaProvider do
       begin
            EjecutaAndFree( Format ( GetScript( Q_DEL_CONFI_USERS ),[EmpresaCodigo] ) );
       end;
  end;

begin
     with oZetaProvider do
     begin
          try
             EmpresaActiva := Comparte;
             EmpiezaTransaccion;
             BorrarListaUsuarios;
             cdsDatos.Data := Delta;
             FQuery := CreateQuery( GetScript( Q_INS_CONFI_USERS ) );
             try
                with cdsDatos do
                begin
                     First;
                     while not Eof do
                     begin
                          sListaUsuarios:= FieldByName('KEM_USERS').AsString;
                          if( not ValidaListaNumeros( sListaUsuarios ) ) then
                          begin
                               DataBaseError( 'La lista de usuarios no es una lista v�lida ');
                          end
                          else
                          begin
                               ParamAsString( FQuery, 'CM_CODIGO', EmpresaCodigo );
                               ParamAsString( FQuery, 'TB_CODIGO', FieldByName('TB_CODIGO').AsString );
                               ParamAsString( FQuery, 'KEM_USERS', sListaUsuarios);
                               Ejecuta(FQuery);
                          end;

                          Next;
                     end;
                end;
             finally
                    FreeAndNil(FQuery);
             end;
             Result := GetUsuariosConfidencialidad(EmpresaCodigo);
             TerminaTransaccion( TRUE );
          except
                on Error: Exception do
                begin
                     RollBackTransaccion;
                     Inc(ErrorCount);
                     raise;
                end;
          end;
          SetComplete;
     end;
end;

function TdmServerKiosco.GetUsuariosSuscritos(const sEmpresa: WideString; const sConfiden: WideString;
                                  lPorConfiden: WordBool; out oUsuarios: OleVariant): OleVariant;
const
     K_USERS_EMPRESA = 'select CM_KUSERS  USUARIOS from COMPANY where ( CM_CODIGO = %s )';
     K_USERS_EMPRESA_CONFI = 'select KEM_USERS USUARIOS from KEM_CONFI where ( CM_CODIGO = %s ) and ( TB_CODIGO = %s )';

     K_GET_USUARIO= 'select US_CODIGO, US_EMAIL, US_FORMATO, US_ACTIVO, US_NOMBRE, US_CORTO  ' +
                     'from USUARIO ' +
                     'where US_EMAIL <> '''' %s';
var
   sScript, sUsuarios: String;

   function GetSiguiente: String;
   var
     iPos: Word;
   begin
        iPos := Pos( ',', sUsuarios );

        if ( iPos > 0 ) then
        begin
             Result := Copy( sUsuarios, 0, ( iPos - 1 ) );
             sUsuarios:= Copy( sUsuarios, ( iPos + 1 ), MaxInt );
        end
        else
        begin
             {Un elemento solamente}
             Result:= sUsuarios;
             sUsuarios:= VACIO;
        end;
   end;

begin
     { GetUsuariosSuscritos: Regresa los usuarios suscritos a la empresa al recibir correos }
     with oZetaProvider do
     begin
          EmpresaActiva:= Comparte;
          if StrVacio( sConfiden ) or ( not lPorConfiden ) then
          begin
               sScript:=  Format( K_USERS_EMPRESA,[ EntreComillas( sEmpresa ) ])
          end
          else
          begin
               sScript:= Format( K_USERS_EMPRESA_CONFI,[ EntreComillas( sEmpresa ), EntreComillas( sConfiden ) ])
          end;

          with cdsDatos do
          begin
               InitTempDataSet;
               AddIntegerField( 'US_CODIGO' );
               IndexFieldNames:= 'US_CODIGO';
               CreateTempDataset;
          end;

          FListaUsuarios:= CreateQuery( sScript );
           try
               with FListaUsuarios do
               begin
                  Active:= True;
                  sUsuarios:= FieldByName('USUARIOS').AsString;

                  oUsuarios:= OpenSQL( oZetaProvider.Comparte,
                                        Format( K_GET_USUARIO, [StrLlenoDef(sUsuarios, ' AND ( US_CODIGO in ( '+ sUsuarios + ' ) )')] ),
                                        TRUE );

                  while StrLleno( sUsuarios ) do
                  begin
                       cdsDatos.Append;
                       cdsDatos.FieldByName('US_CODIGO').AsInteger:= StrToIntDef( GetSiguiente, 0);
                       cdsDatos.Next;
                  end;

                  Active:= False;
               end
           finally
                  FreeAndNil( FListaUsuarios );
           end;

          Result:= cdsDatos.Data;
     end;
     SetComplete;
end;

function TdmServerKiosco.GetImpresionesEmpleado(Empleado: Integer;const Empresa:WideString;oParams: OleVariant; out iImpresione: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Comparte;
          AsignaParamList( oParams );
          FQuery:= CreateQuery;
          try
             with ParamList do
             begin
                  AbreQueryScript( FQuery,Format( GetScript( Q_GET_IMPRESIONES ),[ Empleado,
                                                                                     Empresa,
                                                                                     ParamByName('Tipo').AsInteger,
                                                                                     ParamByName( 'Numero' ).AsInteger,
                                                                                     ParamByName( 'Year' ).AsInteger ] ) );
                  iImpresione := FQuery.FieldByName( 'Impresiones' ).AsInteger;
             end;
          finally
                 FreeAndNil(FQuery);
          end;
     end;
     SetComplete;
end;

function TdmServerKiosco.GetOpcionesEncuesta( Encuesta: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_OPCIONES_ENCUESTAS ), [ Encuesta ] ), True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetEncuestas(Parametros: OleVariant): OleVariant;
var
   sSQL: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          AsignaParamList( Parametros );
          with ParamList do
          begin
               sSQL := Format( GetScript( Q_KIOSCO2_ENCUESTAS ),
                             [ ParamByName( 'Status' ).AsInteger,
                               DateToStrSQLC( ParamByName( 'FechaFinal' ).AsDate ),
                               DateToStrSQLC( ParamByName( 'FechaInicial' ).AsDate ) ] );
          end;
          Result := OpenSQL( EmpresaActiva, sSQL, True );
     end;
     SetComplete;
end;

function TdmServerKiosco.GetMaxEncuesta: Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva:= Comparte;
          FQuery:= CreateQuery;
          try
             AbreQueryScript( FQuery, GetScript( Q_GET_MAX_ENCUESTA ) );
             Result := FQuery.FieldByName( 'MAXIMO' ).AsInteger;
          finally
                 FreeAndNil(FQuery);
          end;
     end;
     SetComplete;
end;

function TdmServerKiosco.GrabaEncuestas(oDelta: OleVariant; oOpciones: OleVariant; var ErrorCount: Integer;var Valor: OleVariant; const Codigo: WideString): OleVariant;
var
   iEncuesta,iPosicion :Integer;
const
      K_DELETE = 'DELETE FROM KOPCIONES WHERE ENC_CODIGO = %s';
begin
     iEncuesta := ErrorCount;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          if ( iEncuesta = 0 ) then
             Result := oDelta
          else
          begin
               SetTablaInfo( eEncuestas );
               Result := GrabaTabla( EmpresaActiva, oDelta, ErrorCount );
          end;
          {if ( ( iEncuesta = 0 ) or ( ErrorCount = 0 ) ) and ( iOpciones > 0 ) then
          begin
               SetTablaInfo( eOpcEncuesta );
               Valor := GrabaTabla( EmpresaActiva, oOpciones, ErrorOpcCount );
          end;}

          {if ( iEncuesta = 0 ) then
          begin}
               if ( oOpciones <> null ) and StrLleno( Codigo ) then
               begin
                    FQuery := CreateQuery( GetScript( Q_KIOSCO2_INS_OPCIONES ) );
                    try
                        ExecSQL( EmpresaActiva, Format( K_DELETE, [ Codigo ] ) );
                        iPosicion := 0;

                        with cdsDatos do
                        begin
                             IndexFieldNames := VACIO;
                             Data := oOpciones;
                             IndexFieldNames := 'ENC_CODIGO;OP_ORDEN';
                             First;
                             while NOT EOF do
                             begin
                                  Inc(iPosicion);
                                  ParamAsInteger( FQuery, 'ENC_CODIGO', StrToInt(Codigo) );
                                  ParamAsInteger( FQuery, 'OP_ORDEN',  iPosicion );
                                  ParamAsString( FQuery,  'OP_TITULO',  FieldByName('OP_TITULO').AsString );
                                  ParamAsString( FQuery,  'OP_DESCRIP', FieldByName('OP_DESCRIP').AsString );
                                  ParamAsInteger( FQuery, 'US_CODIGO', FieldByName('US_CODIGO').AsInteger );
                                  Ejecuta(FQuery);

                                  Next;
                             end;
                        end;
                    finally
                           FreeAndNil(FQuery);
                    end;
               end;
               Valor := oOpciones
          {end;}
     end;
     SetComplete;
end;

function TdmServerKiosco.GetProximidadEmpleado(const sProximidad: WideString): OleVariant; safecall;
begin
     Result := 0;
     with oZetaProvider do
     begin
          if StrLleno( sProximidad )then
          begin
               EmpresaActiva := Comparte;
               Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_CONSULTA_EMP_ID ), [ sProximidad ] ) , true );
          end;
     end;
     SetComplete;
end;

//TODO 1 -cBiometrico -oJosue: Probar la checada de un biometrico
function TdmServerKiosco.GetBiometricoEmpleado(iBiometrico: Integer): OleVariant; safecall;
begin
     Result := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, Format( GetScript( Q_CONSULTA_EMP_BIO ), [ iBiometrico ] ) , true );
     end;
     SetComplete;
end;

//TODO 1 -cBiometrico -oJosue: Probar la sincronizacion ReportaHuella
function TdmServerKiosco.ReportaHuella(Params: OleVariant): OleVariant;
var
   sQuery : String;
   sRenglones : TStringList;
   i : Integer;

   function ExisteParametro( sNombreParametro : String ) : Boolean;
   begin
        Result := ( oZetaProvider.ParamList.IndexOf(sNombreParametro) >= 0 );
   end;

   procedure Split (const Delimiter: Char; Input: string; const Strings: TStrings) ;
   begin
        Assert(Assigned(Strings)) ;
        Strings.Clear;
        Strings.Delimiter := Delimiter;
        Strings.DelimitedText := Input;
   end;

begin
     with oZetaProvider do
     begin
          AsignaParamList( Params );

          if ( ExisteParametro( 'ComputerName' ) ) then
          begin
               EmpresaActiva := Comparte;
               if ( ExisteParametro( 'HuellaId' ) ) then
               begin
                    sRenglones := TStringList.Create;
                    try
                       Split(',', ParamList.ParamByName('HuellaId').AsString, sRenglones) ;
                       if ( sRenglones.Count > 0 ) then
                            for i := 0 to sRenglones.Count - 1 do
                            begin
                                 sQuery := Format( GetScript( Q_HUELLA_REPORTA_INSERTA ), [ ParamList.ParamByName('ComputerName').AsString, sRenglones[i] ]);
                                 EjecutaAndFree( sQuery );
                            end;
                    finally
                           sRenglones.Free;
                    end;
               end
               else
               begin
                    sQuery := Format( GetScript( Q_HUELLA_REPORTA_ELIMINA ), [ ParamList.ParamByName('ComputerName').AsString ]);
                    EjecutaAndFree( sQuery );
               end;
          end
     end;
     SetComplete;
end;

function TdmServerKiosco.HayKioscoEnEmpresa(const iTipo: integer): Boolean;
var
  FCount: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FCount := oZetaProvider.CreateQuery( Format( GetScript( Q_TOTAL_DISPOSIT ), [ iTipo ] ) );
          try
             with FCount do
             begin
                  Active := True;
                  Result := FCount.FieldByName( 'Total' ).AsInteger > 0;
                  Active := False;
             end;
          finally
                 FreeAndNil(FCount);
          end;
     end;
end;

//TODO 1 -cBiometrico -oJosue: Probar la sincronizacion
function TdmServerKiosco.ObtieneTemplates(Parametros: OleVariant): OleVariant;
var
   sScript, sMensaje, sTitle, sFiltro, sKioscoId, sComputadora : String;
   iTipoKiosco: integer;
begin
     sMensaje := VACIO;
     sTitle := VACIO;
     sFiltro := VACIO;

     with oZetaProvider do
     begin
          AsignaParamList( Parametros );
          iTipoKiosco := ParamList.ParamByName( 'TipoKiosco' ).AsInteger;
          sKioscoId := ParamList.ParamByName( 'KioscoId' ).AsString;
          sComputadora := ParamList.ParamByName( 'ComputerName' ).AsString;
          if HayKioscoEnEmpresa( ParamList.ParamByName('TipoKiosco').AsInteger ) then
             sFiltro := Format( 'and V.CM_CODIGO in ( select DI.CM_CODIGO from DISXCOM DI where DI.DI_NOMBRE = ''%s'' and DI.DI_TIPO = %d )', [ sKioscoId, iTipoKiosco ] );

          if strLleno( sComputadora ) then
             sScript := Format( GetScript( Q_HUELLA_OBTENER_MAQUINA ) , [ sComputadora, sFiltro ] )
          else
              sScript := GetScript( Q_HUELLA_OBTENER_TODOS );

          EmpresaActiva := Comparte;
          Result := OpenSQL( EmpresaActiva, sScript, true );
     end;
     SetComplete;
end;

initialization
  TComponentFactory.Create(ComServer, TdmServerKiosco, Class_dmServerKiosco, ciMultiInstance, tmApartment);
end.