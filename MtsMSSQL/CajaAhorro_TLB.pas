unit CajaAhorro_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 5/13/2013 6:42:34 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20_Vsn_2013\MtsMSSQL\CajaAhorro.tlb (1)
// LIBID: {8383EC8D-C650-40F5-AD10-40863B5EA134}
// LCID: 0
// Helpfile: 
// HelpString: Tress Caja de ahorro
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\System32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  CajaAhorroMajorVersion = 1;
  CajaAhorroMinorVersion = 0;

  LIBID_CajaAhorro: TGUID = '{8383EC8D-C650-40F5-AD10-40863B5EA134}';

  IID_IdmServerCajaAhorro: TGUID = '{9016788B-E8A3-459D-8AA7-DDDD2DBFA113}';
  CLASS_dmServerCajaAhorro: TGUID = '{A48C5EBA-6063-4F3A-8F82-FCE4F8716FB6}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerCajaAhorro = interface;
  IdmServerCajaAhorroDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerCajaAhorro = IdmServerCajaAhorro;


// *********************************************************************//
// Interface: IdmServerCajaAhorro
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9016788B-E8A3-459D-8AA7-DDDD2DBFA113}
// *********************************************************************//
  IdmServerCajaAhorro = interface(IAppServer)
    ['{9016788B-E8A3-459D-8AA7-DDDD2DBFA113}']
    function GetTiposAhorro(Empresa: OleVariant): OleVariant; safecall;
    function GetCatalogo(Empresa: OleVariant; iTabla: Integer; const Filtro: WideString): OleVariant; safecall;
    function GrabaCatalogo(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function GetSiguienteCheque(Empresa: OleVariant; const CtaBancaria: WideString): Integer; safecall;
    function InitRegPrestamos(Empresa: OleVariant; out CtasMovs: OleVariant): OleVariant; safecall;
    function GrabaRegPrestamo(Empresa: OleVariant; oDelta: OleVariant; oDeltaMov: OleVariant; 
                              Parametros: OleVariant; out ErrorCount: Integer; 
                              out ErrorCountMov: Integer; out ResultsMov: OleVariant; 
                              ForzarGrabarPrestamos: WordBool; var MensajePrestamo: WideString): OleVariant; safecall;
    function GetLiquidaAhorro(Empresa: OleVariant; iEmpleado: Integer; 
                              const sTipoAhorro: WideString; out CtasMovs: OleVariant): OleVariant; safecall;
    procedure RegistrarLiquidacion(Empresa: OleVariant; LiquidaAhorro: OleVariant; 
                                   CtasMovs: OleVariant); safecall;
    function GetCuentasMovtos(Empresa: OleVariant; const sFiltro: WideString): OleVariant; safecall;
    function GrabaCuentasMovtos(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetAhorro(Empresa: OleVariant; Empleado: Integer; const TipoAhorro: WideString; 
                       out Prestamos: OleVariant; out ACarAbo: OleVariant): OleVariant; safecall;
    function GrabaSaldosAhorro(Empresa: OleVariant; Delta: OleVariant; DeltaACarAbo: OleVariant; 
                               var ErrorCount: Integer; var ErrorCountCarAbo: Integer): OleVariant; safecall;
    function GrabaSaldosPrestamo(Empresa: OleVariant; Delta: OleVariant; DeltaPCarAbo: OleVariant; 
                                 Parametros: OleVariant; out ErrorCount: Integer; 
                                 out ErrorCountCarAbo: Integer; ForzarGrabarPrestamos: WordBool; 
                                 var MensajePrestamo: WideString): OleVariant; safecall;
    function GetTotalesCajaFondo(Empresa: OleVariant; const TipoAhorro: WideString; 
                                 var Cuentas: OleVariant): OleVariant; safecall;
    function GetCuentasBancarias(Empresa: OleVariant; Parametros: OleVariant; 
                                 var SaldoInicial: Double): OleVariant; safecall;
    function GetTotalesRetenciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function GetPeriodos(Empresa: OleVariant; Year: Integer; Tipo: Integer): OleVariant; safecall;
    function GetCtasBancarias(Empresa: OleVariant): OleVariant; safecall;
    function GetNominaEmpleado(Empresa: OleVariant; Empleado: Integer): Integer; safecall;
    function ValidaCheque(Empresa: OleVariant; const CtaBancaria: WideString; Cheque: Integer): Integer; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerCajaAhorroDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9016788B-E8A3-459D-8AA7-DDDD2DBFA113}
// *********************************************************************//
  IdmServerCajaAhorroDisp = dispinterface
    ['{9016788B-E8A3-459D-8AA7-DDDD2DBFA113}']
    function GetTiposAhorro(Empresa: OleVariant): OleVariant; dispid 1;
    function GetCatalogo(Empresa: OleVariant; iTabla: Integer; const Filtro: WideString): OleVariant; dispid 301;
    function GrabaCatalogo(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; dispid 302;
    function GetSiguienteCheque(Empresa: OleVariant; const CtaBancaria: WideString): Integer; dispid 303;
    function InitRegPrestamos(Empresa: OleVariant; out CtasMovs: OleVariant): OleVariant; dispid 304;
    function GrabaRegPrestamo(Empresa: OleVariant; oDelta: OleVariant; oDeltaMov: OleVariant; 
                              Parametros: OleVariant; out ErrorCount: Integer; 
                              out ErrorCountMov: Integer; out ResultsMov: OleVariant; 
                              ForzarGrabarPrestamos: WordBool; var MensajePrestamo: WideString): OleVariant; dispid 305;
    function GetLiquidaAhorro(Empresa: OleVariant; iEmpleado: Integer; 
                              const sTipoAhorro: WideString; out CtasMovs: OleVariant): OleVariant; dispid 306;
    procedure RegistrarLiquidacion(Empresa: OleVariant; LiquidaAhorro: OleVariant; 
                                   CtasMovs: OleVariant); dispid 307;
    function GetCuentasMovtos(Empresa: OleVariant; const sFiltro: WideString): OleVariant; dispid 308;
    function GrabaCuentasMovtos(Empresa: OleVariant; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 309;
    function GetAhorro(Empresa: OleVariant; Empleado: Integer; const TipoAhorro: WideString; 
                       out Prestamos: OleVariant; out ACarAbo: OleVariant): OleVariant; dispid 310;
    function GrabaSaldosAhorro(Empresa: OleVariant; Delta: OleVariant; DeltaACarAbo: OleVariant; 
                               var ErrorCount: Integer; var ErrorCountCarAbo: Integer): OleVariant; dispid 311;
    function GrabaSaldosPrestamo(Empresa: OleVariant; Delta: OleVariant; DeltaPCarAbo: OleVariant; 
                                 Parametros: OleVariant; out ErrorCount: Integer; 
                                 out ErrorCountCarAbo: Integer; ForzarGrabarPrestamos: WordBool; 
                                 var MensajePrestamo: WideString): OleVariant; dispid 312;
    function GetTotalesCajaFondo(Empresa: OleVariant; const TipoAhorro: WideString; 
                                 var Cuentas: OleVariant): OleVariant; dispid 314;
    function GetCuentasBancarias(Empresa: OleVariant; Parametros: OleVariant; 
                                 var SaldoInicial: Double): OleVariant; dispid 313;
    function GetTotalesRetenciones(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 315;
    function GetPeriodos(Empresa: OleVariant; Year: Integer; Tipo: Integer): OleVariant; dispid 316;
    function GetCtasBancarias(Empresa: OleVariant): OleVariant; dispid 317;
    function GetNominaEmpleado(Empresa: OleVariant; Empleado: Integer): Integer; dispid 318;
    function ValidaCheque(Empresa: OleVariant; const CtaBancaria: WideString; Cheque: Integer): Integer; dispid 319;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerCajaAhorro provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerCajaAhorro exposed by              
// the CoClass dmServerCajaAhorro. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerCajaAhorro = class
    class function Create: IdmServerCajaAhorro;
    class function CreateRemote(const MachineName: string): IdmServerCajaAhorro;
  end;

implementation

uses ComObj;

class function CodmServerCajaAhorro.Create: IdmServerCajaAhorro;
begin
  Result := CreateComObject(CLASS_dmServerCajaAhorro) as IdmServerCajaAhorro;
end;

class function CodmServerCajaAhorro.CreateRemote(const MachineName: string): IdmServerCajaAhorro;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerCajaAhorro) as IdmServerCajaAhorro;
end;

end.
