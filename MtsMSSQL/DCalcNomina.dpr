library DCalcNomina;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DCalcNomina_TLB in '..\MTS\DCalcNomina_TLB.pas',
  DServerCalcNomina in '..\MTS\DServerCalcNomina.pas' {dmServerCalcNomina: TMtsDataModule} {dmServerCalcNomina: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\DCalcNomina.TLB}

{$R *.RES}

begin
end.
