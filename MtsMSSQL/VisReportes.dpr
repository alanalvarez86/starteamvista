library VisReportes;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DServerVisReportes in '..\MTS\DServerVisReportes.pas' {dmServerVisReportes: TMtsDataModule} {dmServerVisReportes: CoClass},
  VisReportes_TLB in '..\MTS\VisReportes_TLB.pas',
  DEntidadesTress in '..\VisitantesMGR\DEntidadesTress.pas',
  ZCreator in '..\VisitantesMGR\ZCreator.pas',
  EditorDResumen in '..\Medicos\EditorDResumen.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\VisReportes.TLB}

{$R *.RES}

begin
end.
