library Reportes;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

{%ToDo 'Reportes.todo'}
uses
  MidasLib,
  ComServ,
  Reportes_TLB in '..\MTS\Reportes_TLB.pas',
  DServerReportes in '..\MTS\DServerReportes.pas' {dmServerReportes: TMtsDataModule} {dmServerReportes: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Reportes.TLB}

{$R *.RES}

begin
end.
