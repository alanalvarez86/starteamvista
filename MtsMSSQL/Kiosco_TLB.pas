unit Kiosco_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 8/7/2014 11:28:03 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2014\MtsMSSQL\Kiosco (1)
// LIBID: {683A3CD7-7712-4C80-BD93-FF122C8F5BC2}
// LCID: 0
// Helpfile:
// HelpString: Kiosco Library
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
//   (2) v1.0 Midas, (midas.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Windows, Midas, Classes, Variants, StdVCL, Graphics, OleServer, ActiveX;



// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  KioscoMajorVersion = 1;
  KioscoMinorVersion = 0;

  LIBID_Kiosco: TGUID = '{683A3CD7-7712-4C80-BD93-FF122C8F5BC2}';

  IID_IdmServerKiosco: TGUID = '{D4C6014A-D813-4417-9157-4582F95B5191}';
  CLASS_dmServerKiosco: TGUID = '{0499AE52-D3FD-448E-A23E-3B7A4652D4B9}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IdmServerKiosco = interface;
  IdmServerKioscoDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  dmServerKiosco = IdmServerKiosco;


// *********************************************************************//
// Interface: IdmServerKiosco
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D4C6014A-D813-4417-9157-4582F95B5191}
// *********************************************************************//
  IdmServerKiosco = interface(IAppServer)
    ['{D4C6014A-D813-4417-9157-4582F95B5191}']
    function GetGlobales: OleVariant; safecall;
    function GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetTabla(Tabla: Integer): OleVariant; safecall;
    function GrabaTabla(Tabla: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
                       out Error: WideString): OleVariant; safecall;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
                         out Error: WideString): OleVariant; safecall;
    function GetComparte: OleVariant; safecall;
    function GetBotones(const sPantalla: WideString): OleVariant; safecall;
    function GetCarrusel(const sShow: WideString): OleVariant; safecall;
    function GetEstilo(const sEstilo: WideString): OleVariant; safecall;
    function GetPantalla(const sPantalla: WideString): OleVariant; safecall;
    function GetShow(const sShow: WideString): OleVariant; safecall;
    function GetKioscos: OleVariant; safecall;
    function GetValoresActivos(const Kiosco: WideString; Empleado: Integer;
                               const Empresa: WideString): WideString; safecall;
    function GetCarruselesInfo(const Codigo: WideString): OleVariant; safecall;
    function GrabaCarruseles(Delta: OleVariant; DeltaCarruselInfo: OleVariant;
                             const Codigo: WideString; var CarruselInfo: OleVariant;
                             var ErrorCount: Integer; var ErrorCountInfo: Integer): OleVariant; safecall;
    function GrabaMarcosInfo(Delta: OleVariant; DeltaBotones: OleVariant; DeltaAccesos: OleVariant;
                             const Codigo: WideString; var oAccesoBotones: OleVariant;
                             var ErrorCount: Integer; var ErrorCountBotones: Integer): OleVariant; safecall;
    function GetBitacora(Parametros: OleVariant): OleVariant; safecall;
    function GrabaBitacora(oParametros: OleVariant): OleVariant; safecall;
    function GetAccesoBoton(const sPantalla: WideString): OleVariant; safecall;
    function GetAccesoEmp(Empresa: OleVariant; iEmpleado: Integer): OleVariant; safecall;
    function ReseteaPassword(Empresa: OleVariant; Empleados: OleVariant): Integer; safecall;
    function KioscoEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; safecall;
    function SetClaveEmpleadoKiosco(Empresa: OleVariant; Empleado: Integer; const Clave: WideString): Integer; safecall;
    function GetUsuariosConfidencialidad(const Empresa: WideString): OleVariant; safecall;
    function GrabaUsuariosConfidencialidad(const EmpresaCodigo: WideString; Delta: OleVariant;
                                           out ErrorCount: Integer): OleVariant; safecall;
    function GetUsuariosSuscritos(const sEmpresa: WideString; const sConfiden: WideString;
                                  lPorConfiden: WordBool; out oUsuarios: OleVariant): OleVariant; safecall;
    function GetImpresionesEmpleado(Empleado: Integer; const Empresa: WideString;
                                    oParams: OleVariant; out iImpresione: Integer): OleVariant; safecall;
    function GetOpcionesEncuesta(Encuesta: Integer): OleVariant; safecall;
    function GetEncuestas(Parametros: OleVariant): OleVariant; safecall;
    function GetMaxEncuesta: Integer; safecall;
    function GrabaEncuestas(oDelta: OleVariant; oOpciones: OleVariant; var ErrorCount: Integer;
                            var Valor: OleVariant; const Codigo: WideString): OleVariant; safecall;
    function GetProximidadEmpleado(const sProximidad: WideString): OleVariant; safecall;
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; safecall;
    function ReportaHuella(Params: OleVariant): OleVariant; safecall;
    function GetBiometricoEmpleado(iBiometrico: Integer): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerKioscoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D4C6014A-D813-4417-9157-4582F95B5191}
// *********************************************************************//
  IdmServerKioscoDisp = dispinterface
    ['{D4C6014A-D813-4417-9157-4582F95B5191}']
    function GetGlobales: OleVariant; dispid 301;
    function GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 302;
    function GetTabla(Tabla: Integer): OleVariant; dispid 303;
    function GrabaTabla(Tabla: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 304;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
                       out Error: WideString): OleVariant; dispid 305;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
                         out Error: WideString): OleVariant; dispid 306;
    function GetComparte: OleVariant; dispid 307;
    function GetBotones(const sPantalla: WideString): OleVariant; dispid 308;
    function GetCarrusel(const sShow: WideString): OleVariant; dispid 309;
    function GetEstilo(const sEstilo: WideString): OleVariant; dispid 310;
    function GetPantalla(const sPantalla: WideString): OleVariant; dispid 311;
    function GetShow(const sShow: WideString): OleVariant; dispid 312;
    function GetKioscos: OleVariant; dispid 313;
    function GetValoresActivos(const Kiosco: WideString; Empleado: Integer;
                               const Empresa: WideString): WideString; dispid 314;
    function GetCarruselesInfo(const Codigo: WideString): OleVariant; dispid 315;
    function GrabaCarruseles(Delta: OleVariant; DeltaCarruselInfo: OleVariant;
                             const Codigo: WideString; var CarruselInfo: OleVariant;
                             var ErrorCount: Integer; var ErrorCountInfo: Integer): OleVariant; dispid 316;
    function GrabaMarcosInfo(Delta: OleVariant; DeltaBotones: OleVariant; DeltaAccesos: OleVariant;
                             const Codigo: WideString; var oAccesoBotones: OleVariant;
                             var ErrorCount: Integer; var ErrorCountBotones: Integer): OleVariant; dispid 317;
    function GetBitacora(Parametros: OleVariant): OleVariant; dispid 318;
    function GrabaBitacora(oParametros: OleVariant): OleVariant; dispid 319;
    function GetAccesoBoton(const sPantalla: WideString): OleVariant; dispid 320;
    function GetAccesoEmp(Empresa: OleVariant; iEmpleado: Integer): OleVariant; dispid 321;
    function ReseteaPassword(Empresa: OleVariant; Empleados: OleVariant): Integer; dispid 322;
    function KioscoEmpleado(Empresa: OleVariant; Empleado: Integer): OleVariant; dispid 6;
    function SetClaveEmpleadoKiosco(Empresa: OleVariant; Empleado: Integer; const Clave: WideString): Integer; dispid 21;
    function GetUsuariosConfidencialidad(const Empresa: WideString): OleVariant; dispid 323;
    function GrabaUsuariosConfidencialidad(const EmpresaCodigo: WideString; Delta: OleVariant;
                                           out ErrorCount: Integer): OleVariant; dispid 324;
    function GetUsuariosSuscritos(const sEmpresa: WideString; const sConfiden: WideString;
                                  lPorConfiden: WordBool; out oUsuarios: OleVariant): OleVariant; dispid 325;
    function GetImpresionesEmpleado(Empleado: Integer; const Empresa: WideString;
                                    oParams: OleVariant; out iImpresione: Integer): OleVariant; dispid 326;
    function GetOpcionesEncuesta(Encuesta: Integer): OleVariant; dispid 327;
    function GetEncuestas(Parametros: OleVariant): OleVariant; dispid 328;
    function GetMaxEncuesta: Integer; dispid 329;
    function GrabaEncuestas(oDelta: OleVariant; oOpciones: OleVariant; var ErrorCount: Integer;
                            var Valor: OleVariant; const Codigo: WideString): OleVariant; dispid 330;
    function GetProximidadEmpleado(const sProximidad: WideString): OleVariant; dispid 331;
    function ObtieneTemplates(Parametros: OleVariant): OleVariant; dispid 332;
    function ReportaHuella(Params: OleVariant): OleVariant; dispid 333;
    function GetBiometricoEmpleado(iBiometrico: Integer): OleVariant; dispid 334;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer;
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer;
                           Options: Integer; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerKiosco provides a Create and CreateRemote method to
// create instances of the default interface IdmServerKiosco exposed by
// the CoClass dmServerKiosco. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CodmServerKiosco = class
    class function Create: IdmServerKiosco;
    class function CreateRemote(const MachineName: string): IdmServerKiosco;
  end;

implementation

uses ComObj;

class function CodmServerKiosco.Create: IdmServerKiosco;
begin
  Result := CreateComObject(CLASS_dmServerKiosco) as IdmServerKiosco;
end;

class function CodmServerKiosco.CreateRemote(const MachineName: string): IdmServerKiosco;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerKiosco) as IdmServerKiosco;
end;

end.

