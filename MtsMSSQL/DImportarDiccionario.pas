{* Importaci�n de Diccionario de Datos.
  Proceso de Generaci�n de Estructura de Base de Datos de Empleados, Carga de Valores Iniciales, Sugeridos y Diccionario
  @author Desconocido
}
unit DImportarDiccionario;

interface

uses
  DB, DZetaServerProvider;

type
  TMetodoGraba = procedure(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind) of object;

  function ImportarDiccionarioXML(RutaArchivosConfiguracion: String; ZetaProvider, ZetaProviderLog: TdmZetaServerProvider;
    GrabaCambios: TMetodoGraba; Empresas: String; const UsandoPatch:Boolean = False): OleVariant;

  function PrendeDerechosEntidades(sEmpresa: String; ZetaProvider, ZetaProviderLog: TdmZetaServerProvider;
  GrabaCambios: TMetodoGraba; GrabarBitacora, SoloEntidadesNuevas: Boolean): OleVariant;

implementation

uses
  SysUtils, Classes, Variants, DBClient, DXMLTools, {ZetaClientDataSet,} ZetaCommonLists, ZetaCommonClasses, ZReportConst,
  ZetaCommonTools, ZetaServerTools;

const
  // ToDo: Verificar si se puede declarar aqui, ya que en ZReportConst est� condicionada a RDD y no compila con ConfiguradorTress
  crClasificacionSistema = 999;

type
  TImportadorDiccionario = class
    function Execute: OleVariant;
    function ExecutePrendeDerechosEntidades (sListaEmpresas: String; bGrabarBitacora, bSoloEntidadesNuevas: Boolean; cdsEntidades: TClientDataSet): OleVariant;
  private
    FPosicion                 : Integer;
    FQuery                    : TZetaCursor;
    FCodigo                   : Integer;
    sRutaArchivosConfiguracion: String;
    sEmpresas                 : String;
    GrabaCambiosBitacora      : TMetodoGraba;
    oZetaProvider             : TdmZetaServerProvider;
    oZetaProviderLog          : TdmZetaServerProvider;
    FUsandoPatch              : Boolean;
    function GetSQL(const iSQLNumero: Integer): String;
    procedure cdsGrabaDatasetReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure SetTablaInfoDiccionario(const eTabla: eTipoDiccionario);
    procedure BeforeUpdateClasificacion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateDiccionario(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind);
    procedure BeforeUpdateListasFijas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateModulosEntidad(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateModulos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind;
      var Applied: Boolean);
    procedure BeforeUpdateClasifiEntidad(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateRelacion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind;
      var Applied: Boolean);
    procedure BeforeUpdateCamposDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateOrdenDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateFiltrosDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BorraRegistro(const iCodigo, iPosicion: Integer; const sCampo, sTabla, sCampoLlave: String);
  public
    constructor Create(RutaArchivosConfiguracion: String; ZetaProvider, ZetaProviderLog: TdmZetaServerProvider;
      GrabaCambios: TMetodoGraba; Empresas: String);
  end;

const
  Q_ENTIDADES               = 97;
  Q_ENTIDADES_SIN_NIVEL0    = 98;
  Q_GETLISTA_CLASIFI        = 99;
  Q_INSERT_DERECHO_ENTIDAD  = 100;
  Q_DELETE_DERECHO_ENTIDAD  = 101;
  Q_UPDATE_VERSION          = 102;
  Q_REFRESH_DICCION         = 103;
  Q_MAX_DATOS_CLASIFI       = 104;
  Q_BORRA_MODULOS_ENTIDADES = 105;
  Q_MAX_ORDEN_MODULOS       = 106;
  Q_MAX_MODULOS             = 107;
  Q_BORRA_CLAS_ENTIDADES    = 108;
  Q_MAX_ORDEN_CLASIFI       = 109;
  Q_MAX_RELACION            = 110;
  Q_MAX_DATOS_DEFAULT       = 111;
  Q_BORRA_REGISTRO          = 112;

  K_ENCABEZADO      = 'ENCABEZADO';
  K_DICCIONARIO     = 'DICCIONARIO__DE__DATOS';
  K_VERSION         = 'VERSION';
  K_CAMBIOS         = 'CAMBIOS';
  K_TABLAS          = 'TABLAS';
  K_ATRIBUTOS       = 'CAMPOS';
  K_ATRIBUTO        = 'CAMPO';
  K_RELACION        = 'RELACION';
  K_RELACIONES      = 'RELACIONES';
  K_CAMPO_DEF       = 'CAMPO__DEFAULT';
  K_CAMPOS_DEF      = 'CAMPOS__DEFAULT';
  K_FILTRO_DEF      = 'FILTRO__DEFAULT';
  K_FILTROS_DEF     = 'FILTROS__DEFAULT';
  K_ORDEN_DEF       = 'CRITERIO__DEFAULT';
  K_ORDENES_DEF     = 'CRITERIOS__DEFAULT';
  K_MODULO          = 'MODULO';
  K_MODULOS         = 'MODULOS';
  K_CLASIFICACION   = 'CLASIFICACION';
  K_CLASIFICACIONES = 'CLASIFICACIONES';
  K_LISTA_FIJA      = 'LISTA__FIJA';
  K_LISTA_FIJAS     = 'LISTA__FIJAS';
  K_LISTA_VALOR     = 'LISTA__VALOR';
  K_LISTA_VALORES   = 'LISTA__VALORES';

function ImportarDiccionarioXML(RutaArchivosConfiguracion: String; ZetaProvider, ZetaProviderLog: TdmZetaServerProvider;
  GrabaCambios: TMetodoGraba; Empresas: String; const UsandoPatch:Boolean = False): OleVariant;
var
  Importador: TImportadorDiccionario;
begin
  Importador := TImportadorDiccionario.Create(RutaArchivosConfiguracion, ZetaProvider, ZetaProviderLog, GrabaCambios, Empresas);
  Importador.FUsandoPatch := UsandoPatch;
  Result     := Importador.Execute;
  FreeAndNil(Importador);
end;

function PrendeDerechosEntidades(sEmpresa: String; ZetaProvider, ZetaProviderLog: TdmZetaServerProvider;
  GrabaCambios: TMetodoGraba; GrabarBitacora, SoloEntidadesNuevas: Boolean): OleVariant;
var
  Importador: TImportadorDiccionario;
  cdsEntidades      : TClientDataSet;
begin
  Importador := TImportadorDiccionario.Create('', ZetaProvider, ZetaProviderLog, GrabaCambios, '');
  cdsEntidades := TClientDataSet.Create(nil);
  Importador.FUsandoPatch := False;
  Result     := Importador.ExecutePrendeDerechosEntidades (sEmpresa, GrabarBitacora, SoloEntidadesNuevas, cdsEntidades);
  FreeAndNil(cdsEntidades);
  FreeAndNil(Importador);
end;

{* Constructor de la clase }
constructor TImportadorDiccionario.Create(RutaArchivosConfiguracion: String; ZetaProvider, ZetaProviderLog: TdmZetaServerProvider;
  GrabaCambios: TMetodoGraba; Empresas: String);
begin
  inherited Create;
  FQuery                     := nil;
  sRutaArchivosConfiguracion := RutaArchivosConfiguracion;
  sEmpresas                  := Empresas;
  oZetaProvider              := ZetaProvider;
  if Assigned(ZetaProviderLog) then
    oZetaProviderLog := ZetaProviderLog
  else
    oZetaProviderLog := ZetaProvider;
  GrabaCambiosBitacora       := GrabaCambios;
end;

{* Importa el diccionario de datos a la Base de Datos. Es una copia del m�todo ImportarDiccionario de ReporteadorDD.
  @Param sArchivo Archivo XML con los datos a importar.
  @return Una referencia a un objeto XXX conteniendo el resultado del proceso
}
function TImportadorDiccionario.Execute: OleVariant;
const
  K_DICCION = '\Diccionario\DICCION.xml';
var
  xmlImport         : TdmXMLTools;
  xmlNodePadre      : TZetaXMLNode;
  xmlNode           : TZetaXMLNode;
  xmlNodeChild      : TZetaXMLNode;
  xmlNodeCambios    : TZetaXMLNode;
  iVersion          : Integer;
  cdsListaClasifi   : TClientDataSet;
  cdsEntidades      : TClientDataSet;
  cdsCampos         : TClientDataSet;
  cdsRelaciones     : TClientDataSet;
  cdsDefaultCampos  : TClientDataSet;
  cdsDefaultFiltros : TClientDataSet;
  cdsDefaultOrden   : TClientDataSet;
  cdsModulos        : TClientDataSet;
  cdsClasificaciones: TClientDataSet;
  cdsListasFijas    : TClientDataSet;
  cdsValores        : TClientDataSet;
  oDerechoEntidad   : TZetaCursor;
  oDeleteDerechos   : TZetaCursor;

  procedure EditaUS_CODIGO(Node: TZetaXMLNode; cdsDataset: TClientDataSet; const sField: String);
  begin
    cdsDataset.FieldByName(sField).AsInteger := 0;
  end;

  procedure EditaField(Node: TZetaXMLNode; cdsDataset: TClientDataSet; const sField: String);
  var
    iValue: Integer;
    sValue: String;
  begin
    with cdsDataset do begin
      with FindField(sField) do begin
        case DataType of
          ftInteger, ftSmallint: begin
              iValue := xmlImport.AttributeAsInteger(Node, sField, AsInteger);
              if AsInteger <> iValue then
                AsInteger := iValue;
            end;
          ftString: begin
              sValue := xmlImport.AttributeAsString(Node, sField, AsString);
              if AsString <> sValue then
                AsString := sValue
            end;
        end;
      end;
    end;
  end;

  procedure EditaTabla(Node: TZetaXMLNode; cdsDataset: TClientDataSet; const lExiste: Boolean);
  var
    i: Integer;
  begin
    if Node <> nil then begin
      // FNodoBitacora := Node;
      with cdsDataset do begin
        if lExiste then begin
          if (FieldByName('US_CODIGO').AsInteger = 0) OR ((FieldByName('US_CODIGO').AsInteger <> 0) and
            // (eConflictoImp(oZetaProvider.ParamList.ParamByName('Respetar').AsInteger) = ciEncimar)) then
              (eConflictoImp(0) = ciEncimar)) then
            Edit;
        end
        else
          Append;

        if State <> dsBrowse then begin
          // FieldByName(K_XML_NODE).AsString := Node.Xml;
          for i := 0 to FieldCount - 1 do begin
            if (Fields[i].FieldName <> K_CAMPO_LLAVE_PORTAL) then begin
              if (Fields[i].FieldName <> 'US_CODIGO') then
                EditaField(Node, cdsDataset, Fields[i].FieldName)
              else
                EditaUS_CODIGO(Node, cdsDataset, Fields[i].FieldName);
            end;
          end;
          Post;
        end;
      end;
    end;
  end;

  procedure ImportaTabla(Node: TZetaXMLNode);
  begin
    with oZetaProvider do begin
      Node := xmlImport.GetNode(K_ENCABEZADO, Node);
      if (Node <> nil) then begin
        if cdsEntidades = nil then begin
          cdsEntidades := TClientDataSet.Create(nil);
          try
            cdsEntidades.Data := OpenSQL(EmpresaActiva, GetSQL(Q_ENTIDADES), TRUE);
          except
            on Error: Exception do begin
              cdsEntidades.Data := OpenSQL(EmpresaActiva, GetSQL(Q_ENTIDADES_SIN_NIVEL0), TRUE);
            end;
          end;
        end;
        EditaTabla(Node, cdsEntidades, cdsEntidades.Locate('EN_CODIGO', xmlImport.AttributeAsInteger(Node, 'EN_CODIGO', 0), []));
      end;
    end;
  end;

  procedure ImportaTablaDetalle(Node: TZetaXMLNode; var cdsDataset: TClientDataSet; const eTipo: eTipoDiccionario;
    const sTags, sTag, sField1, sField2: String);
  var
    NodeChild: TZetaXMLNode;
  begin
    with oZetaProvider do begin
      Node := xmlImport.GetNode(sTags, Node);

      if (Node <> nil) then begin
        if cdsDataset = nil then begin
          cdsDataset                  := TClientDataSet.Create(nil);
          cdsDataset.OnReconcileError := cdsGrabaDatasetReconcileError;
          // SetTablaInfo(eTipo, '"CAST( ' + EntreComillas(' ') +' AS varchar(1000)) ' + K_XML_NODE + '"');
          SetTablaInfoDiccionario(eTipo);
          cdsDataset.Data := GetTabla(EmpresaActiva);
        end;

        NodeChild := xmlImport.GetFirstChild(Node);
        while NodeChild <> nil do begin
          EditaTabla(NodeChild, cdsDataset, cdsDataset.Locate(sField1 + ';' + sField2,
              VarArrayof([xmlImport.AttributeAsString(NodeChild, sField1, ''), xmlImport.AttributeAsString(NodeChild, sField2,
                    '')]), []));

          NodeChild := NodeChild.NextSibling;
        end;
      end;
    end;
  end;

  function ValidaClasificacion(sRC_CODIGO: String): String;
  begin
    with oZetaProvider do begin
      if cdsListaClasifi = nil then begin
        cdsListaClasifi      := TClientDataSet.Create(nil);
        cdsListaClasifi.Data := OpenSQL(EmpresaActiva, GetSQL(Q_GETLISTA_CLASIFI), TRUE);
      end;

      if cdsListaClasifi.Locate('RC_CODIGO', VarArrayof([sRC_CODIGO]), []) then
        Result := sRC_CODIGO
      else
        Result := InTToStr(Ord(crClasificacionSistema));
    end;
  end;

  procedure ImportaTablaDetalleClasifiPorEntidad(Node: TZetaXMLNode; var cdsDataset: TClientDataSet; const sTags, sTag: String);
  var
    NodeChild             : TZetaXMLNode;
    sEN_CODIGO, sRC_CODIGO: String;
  begin
    with oZetaProvider do begin
      Node := xmlImport.GetNode(sTags, Node);

      if (Node <> nil) then begin
        if cdsDataset = nil then begin
          cdsDataset                  := TClientDataSet.Create(nil);
          cdsDataset.OnReconcileError := cdsGrabaDatasetReconcileError;
          SetTablaInfoDiccionario(eClasifiPorEntidad);
          cdsDataset.Data := GetTabla(EmpresaActiva);
        end;

        NodeChild := xmlImport.GetFirstChild(Node);
        while NodeChild <> nil do begin
          sEN_CODIGO := xmlImport.AttributeAsString(NodeChild, 'EN_CODIGO', '');
          sRC_CODIGO := xmlImport.AttributeAsString(NodeChild, 'RC_CODIGO', '');
          if strLleno(sEN_CODIGO) and strLleno(sRC_CODIGO) then begin
            sRC_CODIGO                        := ValidaClasificacion(sRC_CODIGO);
            NodeChild.Attributes['RC_CODIGO'] := sRC_CODIGO;
            EditaTabla(NodeChild, cdsDataset, cdsDataset.Locate('EN_CODIGO;RC_CODIGO', VarArrayof([sEN_CODIGO, sRC_CODIGO]), []));
          end;
          NodeChild := NodeChild.NextSibling;
        end;
      end;
    end;
  end;

  procedure ImportaListaFija(Node: TZetaXMLNode);
  begin
    with oZetaProvider do begin
      Node := xmlImport.GetNode(K_ENCABEZADO, Node);
      if Node <> nil then begin
        if cdsListasFijas = nil then begin
          cdsListasFijas := TClientDataSet.Create(nil);
          SetTablaInfoDiccionario(eListasFijas);
          cdsListasFijas.Data := GetTabla(EmpresaActiva);
        end;
        EditaTabla(Node, cdsListasFijas, cdsListasFijas.Locate('LV_CODIGO', xmlImport.AttributeAsInteger(Node, 'LV_CODIGO', 0), []));
      end;
    end;
  end;

  function GetDeltaNull( cdsDataSet: TClientDataSet ): OleVariant;
  begin
       with cdsDataSet do
       begin
            if ( ChangeCount > 0 ) then
               Result := Delta
            else
               TVarData( Result ).VType := varNull;
       end;
  end;

  procedure GrabaDatasetGrid(cdsDataset: TClientDataSet; const eTabla: eTipoDiccionario);
  var
    ErrorCount: Integer;
  begin
    if (cdsDataset <> nil) then begin
      with cdsDataset do begin
        if ChangeCount > 0 then begin
          with oZetaProvider do begin
            SetTablaInfoDiccionario(eTabla);
            try
              Reconcile(GrabaTablaGrid(EmpresaActiva, GetDeltaNull( cdsDataSet ), ErrorCount));
            except
              oZetaProviderLog.Log.Error(0, 'Error al Grabar', 'Se encontr� un error al Grabar los datos ' + cdsDataset.Name);
            end;
          end;
        end;
        Close;
      end;
    end;
  end;  

begin
  { ImportarDiccionarioXML }
  xmlImport := TdmXMLTools.Create(nil);

  iVersion := 0;

  cdsListaClasifi    := nil;
  cdsEntidades       := nil;
  cdsCampos          := nil;
  cdsRelaciones      := nil;
  cdsDefaultCampos   := nil;
  cdsDefaultFiltros  := nil;
  cdsDefaultOrden    := nil;
  cdsModulos         := nil;
  cdsClasificaciones := nil;
  cdsListasFijas     := nil;
  cdsValores         := nil;
  oDerechoEntidad    := nil;
  oDeleteDerechos    := nil;

  try
    with xmlImport do begin
      if FUsandoPatch  then
          XMLDocument.LoadFromFile(sRutaArchivosConfiguracion )
      else
          XMLDocument.LoadFromFile(sRutaArchivosConfiguracion + K_DICCION);

      XMLDocument.LoadFromXML(XMLDocument.XML.Text);
    end;

    with oZetaProvider do begin

      { RCM: El proceso que mande llamar a este importador debe abrir el proceso correspondiente ya que la importaci�n del reporte
        puede ser parte de mas procesos, como la actualizaci�n de versi�n de la BD }


      with xmlImport do begin
        xmlNodePadre := XMLDocument.DocumentElement;
        if (xmlNodePadre = nil) OR (xmlNodePadre.NodeName <> K_DICCIONARIO) then begin
          oZetaProviderLog.Log.Error(0, 'Archivo inv�lido', 'El archivo especificado no tiene el formato XML correcto');
        end else begin
          // Se busca el n�mero de version en el archivo.
          // Si existe el Tag, se actualiza el n�mero de versi�n.
          // La actualizacion se realiza hasta que se termine la importacion y no haya errores.
          xmlNode := GetNode(K_VERSION, xmlNodePadre);
          if xmlNode <> nil then begin
            try
              iVersion := xmlNode.NodeValue;
            except
              on Error: Exception do begin
                Error.Message := 'El n�mero de versi�n de diccionario de datos no fu� actualizado' + CR_LF + Error.Message;
                oZetaProviderLog.Log.Excepcion(0, 'El n�mero de versi�n no es v�lido', Error);
              end;

            end;
          end;

          xmlNode        := GetNode(K_CAMBIOS, xmlNodePadre);
          xmlNodeCambios := xmlNode;

          if (xmlNode <> nil) then begin
            try
              xmlNode := GetNode(K_TABLAS, xmlNode);
              if (xmlNode <> nil) then begin
                xmlNodeChild := GetFirstChild(xmlNode);
                while xmlNodeChild <> nil do begin
                  ImportaTabla(xmlNodeChild);
                  ImportaTablaDetalle(xmlNodeChild, cdsCampos, eCamposPorEntidad, K_ATRIBUTOS, K_ATRIBUTO, 'EN_CODIGO', 'AT_CAMPO');
                  ImportaTablaDetalle(xmlNodeChild, cdsRelaciones, eRelacionesPorEntidad, K_RELACIONES, K_RELACION, 'EN_CODIGO',
                    'RL_ENTIDAD');
                  ImportaTablaDetalle(xmlNodeChild, cdsDefaultCampos, eCamposDefault, K_CAMPOS_DEF, K_CAMPO_DEF, 'EN_CODIGO',
                    'RD_ORDEN');
                  ImportaTablaDetalle(xmlNodeChild, cdsDefaultFiltros, eFiltrosDefault, K_FILTROS_DEF, K_FILTRO_DEF, 'EN_CODIGO',
                    'RF_ORDEN');
                  ImportaTablaDetalle(xmlNodeChild, cdsDefaultOrden, eOrdenDefault, K_ORDENES_DEF, K_ORDEN_DEF, 'EN_CODIGO',
                    'RO_ORDEN');
                  ImportaTablaDetalle(xmlNodeChild, cdsModulos, eModulosPorEntidad, K_MODULOS, K_MODULO, 'EN_CODIGO', 'MO_CODIGO');
                  {$IFDEF ANTES}
                  ImportaTablaDetalle(xmlNodeChild, cdsClasificaciones, eClasifiPorEntidad, K_CLASIFICACIONES, K_CLASIFICACION,
                    'EN_CODIGO', 'RC_CODIGO');
                  {$ENDIF}
                  ImportaTablaDetalleClasifiPorEntidad(xmlNodeChild, cdsClasificaciones, K_CLASIFICACIONES, K_CLASIFICACION);
                  xmlNodeChild := xmlNodeChild.NextSibling;
                end;

                GrabaDatasetGrid(cdsEntidades, eEntidades);
                GrabaDatasetGrid(cdsCampos, eCamposPorEntidad);
                GrabaDatasetGrid(cdsRelaciones, eRelacionesPorEntidad);
                GrabaDatasetGrid(cdsDefaultCampos, eCamposDefault);
                GrabaDatasetGrid(cdsDefaultFiltros, eFiltrosDefault);
                GrabaDatasetGrid(cdsDefaultOrden, eOrdenDefault);
                GrabaDatasetGrid(cdsModulos, eModulosPorEntidad);
                GrabaDatasetGrid(cdsClasificaciones, eClasifiPorEntidad);
              end;

              xmlNode := GetNode(K_LISTA_FIJAS, xmlNodeCambios);
              if (xmlNode <> nil) then begin
                xmlNodeChild := GetFirstChild(xmlNode);
                while xmlNodeChild <> nil do begin
                  ImportaListaFija(xmlNodeChild);
                  ImportaTablaDetalle(xmlNodeChild, cdsValores, eListasFijasValor, K_LISTA_VALORES, K_LISTA_VALORES, 'LV_CODIGO', 'VL_CODIGO');
                  xmlNodeChild := xmlNodeChild.NextSibling;
                end;

                GrabaDatasetGrid(cdsListasFijas, eListasFijas);
                GrabaDatasetGrid(cdsValores, eListasFijasValor);
              end;

            except
              on Error: Exception do begin
                oZetaProviderLog.Log.Excepcion(0, 'Error al Importar Archivo', Error);
                raise Exception.Create('Error al Importar Archivo: ' + Error.Message);
              end;

            end;
          end else
            oZetaProviderLog.Log.Evento(clbNinguno, 0, Date, 'No hay cambios a importar', 'El archivo especificado no contiene ning�n cambio a importar');

          if not oZetaProviderLog.Log.HayErrores and (iVersion <> 0) then
          begin
            FQuery := CreateQuery(Format(GetSQL(Q_UPDATE_VERSION), [InTToStr(iVersion)]));
            Ejecuta(FQuery);
            FreeAndNil(FQuery);
          end;
         
          {
            2014-05-23
            sEmpresas lleno equivale a una ejecucion de ImportarDiccionarioXML desde DMotorPatch.
            La otra ejecuci�n posible es desde los wizards de creaci�n de base de datos,
            pero estos enviar�n siempre sEmpresas = VACIO.
          }
          if StrLleno(sEmpresas) then
              ExecutePrendeDerechosEntidades(sEmpresas, TRUE, TRUE, cdsEntidades);

          //oZetaProviderLog.Log.Evento(clbNinguno, 0, Date, 'Archivo XML', XMLAsText);

          oZetaProvider.EjecutaAndFree(GetSQL(Q_REFRESH_DICCION));

        end;
      end;
    end;
  finally
    FreeAndNil(cdsListaClasifi);
    FreeAndNil(cdsEntidades);
    FreeAndNil(cdsCampos);
    FreeAndNil(cdsRelaciones);
    FreeAndNil(cdsDefaultCampos);
    FreeAndNil(cdsDefaultFiltros);
    FreeAndNil(cdsDefaultOrden);
    FreeAndNil(cdsModulos);
    FreeAndNil(cdsClasificaciones);
    FreeAndNil(cdsListasFijas);
    FreeAndNil(cdsValores);
    FreeAndNil(oDerechoEntidad);
    FreeAndNil(oDeleteDerechos);
    FreeAndNil(xmlImport);

    //oZetaProviderLog.Log.Evento(clbMotorPatchImportarDiccionario, 0, Date, 'Importar Diccionario', 'Termina proceso');
  end;

end;

function TImportadorDiccionario.GetSQL(const iSQLNumero: Integer): String;
begin
  case iSQLNumero of

    Q_GETLISTA_CLASIFI:
      Result := 'select RC_CODIGO, RC_NOMBRE,RC_ORDEN,RC_ACTIVO,RC_VERSION,US_CODIGO ' + 'from R_CLASIFI ' + 'order by RC_ORDEN';

    Q_ENTIDADES:
      Result := 'SELECT EN_CODIGO, Cast( (EN_CODIGO) as Varchar(10)) numtabla, EN_TITULO, ' +
        'EN_TABLA,	EN_DESCRIP,	EN_PRIMARY,EN_ATDESC, EN_ALIAS,US_CODIGO,EN_ACTIVO,EN_VERSION, ' + '''S'' AS EXISTE, EN_NIVEL0 ' +
        'FROM R_ENTIDAD ORDER BY EN_CODIGO';

    Q_ENTIDADES_SIN_NIVEL0:
      Result := 'SELECT EN_CODIGO, Cast( (EN_CODIGO) as Varchar(10)) numtabla, EN_TITULO, ' +
        'EN_TABLA,	EN_DESCRIP,	EN_PRIMARY,EN_ATDESC, EN_ALIAS,US_CODIGO,EN_ACTIVO,EN_VERSION, ' + '''S'' AS EXISTE ' +
        'FROM R_ENTIDAD ORDER BY EN_CODIGO';

    Q_UPDATE_VERSION:
      Result := 'update GLOBAL set GL_FORMULA = %s where GL_CODIGO = 226';

    Q_REFRESH_DICCION:
      Result := 'execute SP_REFRESH_DICCION';

    Q_INSERT_DERECHO_ENTIDAD:
      {Result := 'insert into R_ENT_ACC( CM_CODIGO, GR_CODIGO, EN_CODIGO, RE_DERECHO ) ' +
        'SELECT :EMPRESA, GR_CODIGO, :ENTIDAD_PARAM1, 1 FROM V_GRUPO where ' +
        '( ( select count(*) from R_ENTIDAD where EN_CODIGO = :ENTIDAD_PARAM2  ) > 0 ) ';}
        Result := 'insert into R_ENT_ACC( CM_CODIGO, GR_CODIGO, EN_CODIGO, RE_DERECHO ) ' +
        'SELECT ''%s'', GR_CODIGO, %d, 1 FROM V_GRUPO where ' +
        '( ( select count(*) from R_ENTIDAD where EN_CODIGO = %d  ) > 0 ) ';

    Q_DELETE_DERECHO_ENTIDAD:
      // Result := 'delete from R_ENT_ACC where EN_CODIGO = :ENTIDAD and CM_CODIGO = :EMPRESA';
      Result := 'delete from R_ENT_ACC where EN_CODIGO = %d and CM_CODIGO = ''%s'' ';

    Q_MAX_DATOS_DEFAULT:
      Result := 'select ISNULL( MAX(%s_ORDEN),0)+1 MAXIMO from %s where EN_CODIGO = %d';

    Q_BORRA_MODULOS_ENTIDADES:
      Result := 'Delete from R_MOD_ENT where EN_CODIGO = %d';

    Q_MAX_ORDEN_MODULOS:
      Result := 'select ISNULL( MAX(ME_ORDEN),0)+1 MAXIMO from R_MOD_ENT where MO_CODIGO = %d';

    Q_BORRA_CLAS_ENTIDADES:
      Result := 'Delete from R_CLAS_ENT where EN_CODIGO = %d';

    Q_MAX_ORDEN_CLASIFI:
      Result := 'select ISNULL( MAX(CE_ORDEN),0)+1 MAXIMO, COUNT(*) CUANTOS from R_CLAS_ENT A ' +
        'right outer join R_CLASIFI B ON ( A.RC_CODIGO = B.RC_CODIGO ) ' + 'where ( B.RC_CODIGO = %d )';

    Q_MAX_RELACION:
      Result := 'select ISNULL( MAX(RL_ORDEN), 0 )+1 ORDEN from R_RELACION WHERE EN_CODIGO = %d ';
  end;
end;

procedure TImportadorDiccionario.cdsGrabaDatasetReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
  i, iTabla                   : Integer;
  sMensaje1, sMensaje2, sError: String;
begin
  Action := raSkip;
  with E do begin
    { PK_VIOLATION }
    if PK_VIOLATION(E) then // if ( ErrorCode = 3604 ) or ( Pos( 'PRIMARY', Message ) > 0 ) then
      sError := '� C�digo Ya Existe !'
    else if (Pos('FK_R_MOD_ENT_MODULO', UpperCase(Message)) > 0) then
      sError := Format('El m�dulo %d no existe', [DataSet.FieldByName('MO_CODIGO').AsInteger]) + CR_LF + Message
    else if (Pos('FK_R_CLAS_ENT_CLASIFI', UpperCase(Message)) > 0) then
      sError := Format('La clasificaci�n %d no existe', [DataSet.FieldByName('RC_CODIGO').AsInteger]) + CR_LF + Message
    else
      sError := Message;
    if (Context <> '') then
      sError := sError + CR_LF + Context;
  end;

  if (DataSet.FindField('EN_CODIGO') <> nil) then begin
    with DataSet.FieldByName('EN_CODIGO') do begin
      iTabla    := AsInteger;
      sMensaje1 := 'Error al Graba la Tabla # ' + AsString;
    end;
  end else begin
    iTabla    := 0;
    sMensaje1 := 'Error al Grabar la Tabla ' + DataSet.Name;
  end;

  sMensaje2 := '';

  for i       := 0 to DataSet.FieldCount - 1 do
    sMensaje2 := ConcatString(sMensaje2, DataSet.Fields[i].FieldName + ':' + DataSet.Fields[i].AsString, CR_LF);

  sMensaje2 := 'Error : ' + sError + CR_LF + 'Registro que no se grab� : ' + CR_LF + sMensaje2;

  oZetaProviderLog.Log.Error(iTabla, sMensaje1, sMensaje2);

end;

procedure TImportadorDiccionario.SetTablaInfoDiccionario(const eTabla: eTipoDiccionario);
begin
  with oZetaProvider.TablaInfo do begin
    case eTabla of
      eClasifi: begin
          SetInfo('R_CLASIFI', 'RC_CODIGO, RC_ORDEN, RC_NOMBRE,RC_ACTIVO,RC_VERSION,US_CODIGO', 'RC_CODIGO');
          BeforeUpdateRecord := BeforeUpdateClasificacion;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
      eEntidades: begin
          SetInfo('R_ENTIDAD',
            'EN_CODIGO, EN_TITULO, EN_TABLA,EN_DESCRIP,EN_PRIMARY,EN_ATDESC,EN_ALIAS,EN_ACTIVO,EN_VERSION,US_CODIGO,EN_NIVEL0',
            'EN_CODIGO');
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eListasFijas: begin
          SetInfo('R_LISTAVAL', 'LV_CODIGO,LV_NOMBRE,LV_ACTIVO,LV_VERSION,US_CODIGO', 'LV_CODIGO');
          BeforeUpdateRecord := BeforeUpdateListasFijas;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
      eListasFijasValor: begin
          SetInfo('R_VALOR', 'LV_CODIGO,VL_CODIGO,VL_DESCRIP,VL_VERSION,US_CODIGO', 'LV_CODIGO,VL_CODIGO');
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eModulos: begin
          SetInfo('R_MODULO', 'MO_CODIGO,MO_NOMBRE,MO_ORDEN,MO_ACTIVO,MO_VERSION,US_CODIGO', 'MO_CODIGO');
          BeforeUpdateRecord := BeforeUpdateModulos;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
      eAccesosClasifi:
        SetInfo('R_CLAS_ACC', 'RC_CODIGO, RA_DERECHO, GR_CODIGO, CM_CODIGO', 'CM_CODIGO, GR_CODIGO, RC_CODIGO');
      eAccesosEntidades:
        SetInfo('R_ENT_ACC', 'EN_CODIGO, RE_DERECHO, GR_CODIGO, CM_CODIGO', 'CM_CODIGO, GR_CODIGO, EN_CODIGO');
      eModulosPorEntidad: begin
          SetInfo('R_MOD_ENT', 'MO_CODIGO, ME_ORDEN, EN_CODIGO,ME_VERSION,US_CODIGO', 'MO_CODIGO,ME_ORDEN');
          BeforeUpdateRecord := BeforeUpdateModulosEntidad;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
      eCamposPorEntidad: begin
          SetInfo('R_ATRIBUTO', 'EN_CODIGO, AT_CAMPO, AT_TITULO, AT_TIPO, AT_ANCHO, AT_MASCARA, AT_TRANGO, ' +
              'AT_ENTIDAD, LV_CODIGO, AT_CLAVES, AT_DESCRIP, AT_TCORTO, AT_SISTEMA, AT_VALORAC, ' +
              'AT_TOTAL, AT_FILTRO, AT_CONFI,AT_ACTIVO,AT_VERSION,US_CODIGO', 'EN_CODIGO, AT_CAMPO');
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eClasifiPorEntidad: begin
          SetInfo('R_CLAS_ENT', 'RC_CODIGO, CE_ORDEN, EN_CODIGO,CE_VERSION,US_CODIGO', 'RC_CODIGO, CE_ORDEN');
          BeforeUpdateRecord := BeforeUpdateClasifiEntidad;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
      eRelacionesPorEntidad: begin
          SetInfo('R_RELACION', 'EN_CODIGO, RL_ENTIDAD, RL_CAMPOS, RL_ORDEN, RL_INNER,RL_ACTIVO,RL_VERSION,US_CODIGO',
            'EN_CODIGO,RL_ORDEN');
          BeforeUpdateRecord := BeforeUpdateRelacion;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
      eCamposDefault: begin
          SetInfo('R_DEFAULT', 'EN_CODIGO, RD_ORDEN, RD_ENTIDAD, AT_CAMPO,RD_VERSION,US_CODIGO', 'EN_CODIGO,		RD_ORDEN ');
          BeforeUpdateRecord := BeforeUpdateCamposDefault;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
      eOrdenDefault: begin
          SetInfo('R_ORDEN', 'EN_CODIGO, RO_ORDEN, RO_ENTIDAD, AT_CAMPO,RO_VERSION,US_CODIGO', 'EN_CODIGO,RO_ORDEN');
          BeforeUpdateRecord := BeforeUpdateOrdenDefault;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
      eFiltrosDefault: begin
          SetInfo('R_FILTRO', 'EN_CODIGO, RF_ORDEN, RF_ENTIDAD, AT_CAMPO,RF_VERSION,US_CODIGO', 'EN_CODIGO,		RF_ORDEN');
          BeforeUpdateRecord := BeforeUpdateFiltrosDefault;
          AfterUpdateRecord  := AfterUpdateDiccionario;
        end;
    end;
  end;
end;

procedure TImportadorDiccionario.BeforeUpdateClasificacion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  case UpdateKind of
    ukInsert: begin
        FQuery        := oZetaProvider.CreateQuery(GetSQL(Q_MAX_DATOS_CLASIFI));
        FQuery.Active := TRUE;

        DeltaDS.Edit;
        DeltaDS.FieldByName('RC_CODIGO').AsInteger := FQuery.Fields[0].AsInteger;
        { Se permite c�digos menores de 1000 }
        DeltaDS.FieldByName('RC_ORDEN').AsInteger := FQuery.Fields[1].AsInteger;
        DeltaDS.Post;

        FQuery.Free;
      end;
    ukDelete: begin
        BorraRegistro(DeltaDS.FieldByName('RC_CODIGO').AsInteger, DeltaDS.FieldByName('RC_ORDEN').AsInteger, 'RC_ORDEN',
          'R_CLASIFI', 'RC_CODIGO');
        Applied := TRUE;
      end;
  end;
end;

procedure TImportadorDiccionario.AfterUpdateDiccionario(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind);
begin
  GrabaCambiosBitacora(Sender, SourceDS, DeltaDS, UpdateKind);
end;

procedure TImportadorDiccionario.BeforeUpdateListasFijas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);

begin
  if (UpdateKind in [ukModify, ukInsert]) then begin
    FCodigo := CampoAsVar(DeltaDS.FieldByName('LV_CODIGO'));
    if (DeltaDS.FindField('LV_DUMMY') <> nil) then
      Applied := CambiaCampo(DeltaDS.FieldByName('LV_DUMMY'));
  end;
end;

procedure TImportadorDiccionario.BeforeUpdateModulosEntidad(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
var
  iModulo: Integer;
begin
  with oZetaProvider do begin
    if (UpdateKind <> ukDelete) then begin
      if (FCodigo = -1) then begin
        FCodigo := CampoAsVar(DeltaDS.FieldByName('EN_CODIGO'));
        EmpiezaTransaccion;
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_BORRA_MODULOS_ENTIDADES), [FCodigo]));
        TerminaTransaccion(TRUE);
      end;

      iModulo       := CampoAsVar(DeltaDS.FieldByName('MO_CODIGO'));
      FQuery        := CreateQuery(Format(GetSQL(Q_MAX_ORDEN_MODULOS), [iModulo]));
      FQuery.Active := TRUE;

      DeltaDS.Edit;
      DeltaDS.FieldByName('ME_ORDEN').AsInteger := FQuery.Fields[0].AsInteger;
      DeltaDS.Post;

      FQuery.Free;
    end;
  end;
end;

procedure TImportadorDiccionario.BeforeUpdateModulos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  case UpdateKind of
    ukInsert: begin
        FQuery        := oZetaProvider.CreateQuery(GetSQL(Q_MAX_MODULOS));
        FQuery.Active := TRUE;

        DeltaDS.Edit;
        DeltaDS.FieldByName('MO_ORDEN').AsInteger := FQuery.Fields[1].AsInteger;
        DeltaDS.Post;

        FQuery.Free;
      end;

    ukDelete: begin
        BorraRegistro(DeltaDS.FieldByName('MO_CODIGO').AsInteger, DeltaDS.FieldByName('MO_ORDEN').AsInteger, 'MO_ORDEN',
          'R_MODULO', 'MO_CODIGO');
        Applied := TRUE;
      end;
  end;
end;

procedure TImportadorDiccionario.BeforeUpdateClasifiEntidad(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
var
  iClasifi: Integer;
begin
  with oZetaProvider do begin
    if (UpdateKind <> ukDelete) then begin
      if (FCodigo = -1) then begin
        FCodigo := CampoAsVar(DeltaDS.FieldByName('EN_CODIGO'));
        EmpiezaTransaccion;
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_BORRA_CLAS_ENTIDADES), [FCodigo]));
        TerminaTransaccion(TRUE);
      end;

      iClasifi      := CampoAsVar(DeltaDS.FieldByName('RC_CODIGO'));
      FQuery        := CreateQuery(Format(GetSQL(Q_MAX_ORDEN_CLASIFI), [iClasifi]));
      FQuery.Active := TRUE;

      DeltaDS.Edit;
      DeltaDS.FieldByName('CE_ORDEN').AsInteger := FQuery.Fields[0].AsInteger;

      if FQuery.FieldByName('CUANTOS').AsInteger = 0 then
        // Quiere decir que la clasificacion no existe.
        DeltaDS.FieldByName('RC_CODIGO').AsInteger := crClasificacionSistema;

      DeltaDS.Post;

      FQuery.Free;
    end;
  end;
end;

procedure TImportadorDiccionario.BeforeUpdateRelacion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with oZetaProvider do begin
    case UpdateKind of
      ukInsert: begin
          FQuery        := CreateQuery(Format(GetSQL(Q_MAX_RELACION), [DeltaDS.FieldByName('EN_CODIGO').AsInteger]));
          FQuery.Active := TRUE;

          DeltaDS.Edit;
          DeltaDS.FieldByName('RL_ORDEN').AsInteger := FQuery.Fields[0].AsInteger;
          DeltaDS.Post;

          FQuery.Free;
        end;
    end;
  end;
end;

procedure TImportadorDiccionario.BeforeUpdateCamposDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with oZetaProvider do begin
    case UpdateKind of
      ukInsert: begin
          FQuery := CreateQuery(Format(GetSQL(Q_MAX_DATOS_DEFAULT),
              ['RD', 'R_DEFAULT', DeltaDS.FieldByName('EN_CODIGO').AsInteger]));
          FQuery.Active := TRUE;
          FPosicion     := FQuery.Fields[0].AsInteger;

          DeltaDS.Edit;
          DeltaDS.FieldByName('RD_ORDEN').AsInteger := FPosicion;
          DeltaDS.Post;

          FQuery.Free;
        end;
      ukDelete: begin
          BorraRegistro(DeltaDS.FieldByName('EN_CODIGO').AsInteger, DeltaDS.FieldByName('RD_ORDEN').AsInteger, 'RD_ORDEN',
            'R_DEFAULT', 'EN_CODIGO');
          Applied := TRUE;
        end;
    end;
  end;

end;

procedure TImportadorDiccionario.BeforeUpdateOrdenDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with oZetaProvider do begin
    case UpdateKind of
      ukInsert: begin
          FQuery := CreateQuery(Format(GetSQL(Q_MAX_DATOS_DEFAULT),
              ['RO', 'R_ORDEN', DeltaDS.FieldByName('EN_CODIGO').AsInteger]));
          FQuery.Active := TRUE;
          FPosicion     := FQuery.Fields[0].AsInteger;

          DeltaDS.Edit;
          DeltaDS.FieldByName('RO_ORDEN').AsInteger := FPosicion;
          DeltaDS.Post;

          FQuery.Free;
        end;
      ukDelete: begin
          BorraRegistro(DeltaDS.FieldByName('EN_CODIGO').AsInteger, DeltaDS.FieldByName('RO_ORDEN').AsInteger, 'RO_ORDEN',
            'R_ORDEN', 'EN_CODIGO');
          Applied := TRUE;
        end;
    end;
  end;
end;

procedure TImportadorDiccionario.BeforeUpdateFiltrosDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with oZetaProvider do begin
    case UpdateKind of
      ukInsert: begin
          FQuery := CreateQuery(Format(GetSQL(Q_MAX_DATOS_DEFAULT),
              ['RF', 'R_FILTRO', DeltaDS.FieldByName('EN_CODIGO').AsInteger]));
          FQuery.Active := TRUE;
          FPosicion     := FQuery.Fields[0].AsInteger;

          DeltaDS.Edit;
          DeltaDS.FieldByName('RF_ORDEN').AsInteger := FPosicion;
          DeltaDS.Post;

          FQuery.Free;
        end;
      ukDelete: begin
          BorraRegistro(DeltaDS.FieldByName('EN_CODIGO').AsInteger, DeltaDS.FieldByName('RF_ORDEN').AsInteger, 'RF_ORDEN',
            'R_FILTRO', 'EN_CODIGO');
          Applied := TRUE;
        end;
    end;
  end;
end;

procedure TImportadorDiccionario.BorraRegistro(const iCodigo, iPosicion: Integer; const sCampo, sTabla, sCampoLlave: String);
begin
  with oZetaProvider do begin
    EmpiezaTransaccion;
    try
      ExecSQL(EmpresaActiva, Format(GetSQL(Q_BORRA_REGISTRO), [Comillas(sTabla), Comillas(sCampo), Comillas(sCampoLlave), iCodigo,
            iPosicion]));
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do begin
        RollBackTransaccion;
        raise;
      end;
    end;
  end;
end;

function TImportadorDiccionario.ExecutePrendeDerechosEntidades (sListaEmpresas: String; bGrabarBitacora, bSoloEntidadesNuevas: Boolean; cdsEntidades: TClientDataSet): OleVariant;

    procedure InsertaDerecho(const iEntidad: Integer; const sEmpresa: String);
    begin
      with oZetaProvider do begin
        { Cuando se borra una entidad no se borra el derecho, nos protegemos por esta situaci�n }
        {ParamAsInteger(oDeleteDerechos, 'Entidad', iEntidad);
        ParamAsString(oDeleteDerechos, 'Empresa', sEmpresa);}
        // Ejecuta(oDeleteDerechos);
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_DELETE_DERECHO_ENTIDAD), [iEntidad, sEmpresa]));

        {ParamAsInteger(oDerechoEntidad, 'Entidad_Param1', iEntidad);
        ParamAsInteger(oDerechoEntidad, 'Entidad_Param2', iEntidad);
        ParamAsString(oDerechoEntidad, 'Empresa', sEmpresa);}
        // Ejecuta(oDerechoEntidad);
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_INSERT_DERECHO_ENTIDAD), [sEmpresa, iEntidad, iEntidad]));
      end;
    end;

  var
    oEmpresas      : TStrings;
    i              : Integer;
  begin

      with oZetaProvider do
      begin

        if not bSoloEntidadesNuevas then
        begin
            cdsEntidades := TClientDataSet.Create(nil);
            try
              cdsEntidades.Data := OpenSQL(EmpresaActiva, GetSQL(Q_ENTIDADES), TRUE);
            except
              on Error: Exception do
              begin
                cdsEntidades.Data := OpenSQL(EmpresaActiva, GetSQL(Q_ENTIDADES_SIN_NIVEL0), TRUE);
              end;
            end;
        end;

        if (cdsEntidades <> nil) then
        begin
          try
            with cdsEntidades do begin

              oEmpresas       := TStringList.Create;

              try
                Open;
                if StrVacio(sListaEmpresas) then
                  raise Exception.Create('Error al Obtener la Informaci�n de la Empresa')
                else
                  oEmpresas.CommaText := sListaEmpresas;
                for i                 := 0 to oEmpresas.Count - 1 do begin
                  First;
                  while not EOF do begin
                    if (not zStrToBool(FieldByName('EXISTE').AsString)) or (not bSoloEntidadesNuevas) then
                    begin
                      EmpiezaTransaccion;
                      try
                        InsertaDerecho(FieldByName('EN_CODIGO').AsInteger, oEmpresas[i]);
                        TerminaTransaccion(TRUE);
                      except
                        on Error: Exception do
                        begin
                          RollBackTransaccion;
                          if bGrabarBitacora then
                            oZetaProviderLog.Log.Error(FieldByName('EN_CODIGO').AsInteger, 'Error al Otorgar Derechos',
                              Format('Se encontr� un error al Asignar Derechos sobre Empresa %s, Entidad %s: ',
                                [oEmpresas[i], FieldByName('EN_TABLA').AsString]) + CR_LF + Error.Message)
                           else
                              raise Exception.Create('Entidad: ' + IntToStr (FieldByName('EN_CODIGO').AsInteger) + CR_LF +
                                Format('Se encontr� un error al Asignar Derechos sobre Empresa %s, Entidad %s: ',
                                  [oEmpresas[i], FieldByName('EN_TABLA').AsString]) + CR_LF + Error.Message);
                        end;
                      end;
                    end;
                    Next;
                  end;
                end;
              finally
                Close;
                FreeAndNil(oEmpresas);
              end;
            end;
          except
            on Error: Exception do
            begin
              if bGrabarBitacora then
                  oZetaProviderLog.Log.Error(0, 'Error al Otorgar Derechos', Error.Message)
              else
                  raise Exception.Create(Error.Message);
            end;
          end;
        end;
    end;
  end;

end.
