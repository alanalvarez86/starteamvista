library PortalTress;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  PortalTress_TLB in '..\MTS\PortalTress_TLB.pas',
  DServerPortalTress in '..\MTS\DServerPortalTress.pas' {dmPortalTress: TMtsDataModule};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\PortalTress.TLB}

{$R *.RES}

begin

end.
