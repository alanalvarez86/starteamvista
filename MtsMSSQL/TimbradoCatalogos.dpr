library TimbradoCatalogos;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DServerCatalogosTimbrado in '..\MTS\DServerCatalogosTimbrado.pas',
  TimbradoCatalogos_TLB in '..\MTS\TimbradoCatalogos_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ../MTS/TimbradoCatalogos.TLB}

{$R *.RES}

begin
end.
