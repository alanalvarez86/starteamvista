library DAnualNomina;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DAnualNomina_TLB in '..\MTS\DAnualNomina_TLB.pas',
  DServerAnualNomina in '..\MTS\DServerAnualNomina.pas' {dmServerAnualNomina: TMtsDataModule} {dmServerAnualNomina: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\DAnualNomina.TLB}

{$R *.RES}

begin
end.
