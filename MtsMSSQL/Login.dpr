library Login;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}



{$R *.dres}

uses
  MidasLib,
  ComServ,
  Login_TLB in '..\MTS\Login_TLB.pas',
  DServerLogin in '..\MTS\DServerLogin.pas' {dmServerLogin: TMtsDataModule} {dmServerLogin: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Login.TLB}

{$R *.RES}

begin
end.
