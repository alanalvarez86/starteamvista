library SelReportes;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  SelReportes_TLB in '..\MTS\SelReportes_TLB.pas',
  DServerSelReportes in '..\MTS\DServerSelReportes.pas' {dmServerSelReportes: TMtsDataModule} {dmServerSelReportes: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\SelReportes.TLB}

{$R *.RES}

begin
end.
