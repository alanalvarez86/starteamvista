program Cafetera;

uses
  Forms,
  DServerBase in '..\Servidor\Asta\DServerBase.pas' {dmServerBase: TDataModule},
  FServerBase in '..\Servidor\Asta\FServerBase.pas' {ServerBase},
  FCafetera in '..\ServerCafe\FCafetera.pas' {CafeServer};

{$R *.RES}

begin
     if not FCafetera.YaEstaCorriendo then
     begin
          Application.Initialize;
          Application.Title := 'Cafetera';
          Application.CreateForm(TCafeServer, CafeServer);
          with CafeServer do
          begin
               Show;
               Update;
               Start;
          end;
          Application.Run;
     end;
end.
