library Reporteador;

uses
  ComServ,
  Reporteador_TLB in '..\MTS\Reporteador_TLB.pas',
  DServerReporting in '..\MTS\DServerReporting.pas' {dmServerReporting: TMtsDataModule};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Reporteador.TLB}

{$R *.RES}

begin
end.
