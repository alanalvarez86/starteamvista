library Labor;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  Labor_TLB in '..\MTS\Labor_TLB.pas',
  DServerLabor in '..\MTS\DServerLabor.pas' {dmServerLabor: TMtsDataModule};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Labor.TLB}

{$R *.RES}

begin
end.
