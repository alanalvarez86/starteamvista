library Super;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}

uses
  MidasLib,
  ComServ,
  Super_TLB in '..\MTS\Super_TLB.pas',
  DServerSuper in '..\MTS\DServerSuper.pas' {dmServerSuper: TMtsDataModule} {dmServerSuper: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Super.TLB}

{$R *.RES}

begin
end.
