library Cafeteria;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DServerCafeteria in '..\MTS\DServerCafeteria.pas',
  Cafeteria_TLB in '..\MTS\Cafeteria_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Cafeteria.TLB}

{$R *.RES}

begin
end.
