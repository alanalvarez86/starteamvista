unit DServerReporteadorDD;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DServerDiccionario.pas                     ::
  :: Descripci�n: Programa principal de Diccionario.dll      ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ComServ, ComObj, VCLCom, StdVcl, DataBkr,
  DBClient, MtsRdm, Mtx, DB,
  Variants,
  {$IFNDEF DOS_CAPAS}
  ReporteadorDD_TLB,
  {$ENDIF}
  DZetaServerProvider, DXMLTools, ZCreator, ZSuperSQL, ZetaTipoEntidad, ZetaSQLBroker, ZetaCommonLists,
  ZetacommonClasses, ZetaServerDataSet;

type

  TdmXMLTools_RDD = class(TdmXMLTools)
  private
    FActualNode: TZetaXMLNode;
    procedure SetActualNode(Node: TZetaXMLNode);
  public
    property ActualNode: TZetaXMLNode read FActualNode write SetActualNode;
  end;

  TdmServerReporteadorDD = class(TMtsDataModule {$IFNDEF DOS_CAPAS}, IdmServerReporteadorDD
    {$ENDIF} )
    cdsTemporal: TServerDataSet;
    cdsLista: TServerDataSet;
    procedure dmServerReporteadorDDCreate(Sender: TObject);
    procedure dmServerReporteadorDDDestroy(Sender: TObject);
  private
    { Private declarations }
    {$IFDEF BITACORA_DLLS}
    FListaLog  : TAsciiLog;
    FPaletita  : string;
    FEmpresa   : string;
    FArchivoLog: string;
    {$ENDIF}
    oZetaProvider: TdmZetaServerProvider;
    {$IFNDEF DOS_CAPAS}
    oZetaCreator  : TZetaCreator;
    oZetaSQLBroker: TSQLBroker;
    {$ENDIF}
    { -------------------------------------------------------------------------- }
    { -------------------------  DServerReportes  --------------------------- }
    FReporte : Integer;
    FAlta    : Boolean;
    FPosicion: Integer;
    { -------------------------------------------------------------------------- }

    { ------------------------- Metodos de DServerDiccionario  ----------------- }
    FQuery : TZetaCursor;
    FCodigo: Integer;
    { -------------------------------------------------------------------------- }
    FClaseDiccionario: eTipoDiccionario;
    FListaParametros : string;
    // FNodoBitacora: TZetaXMLNode;
    procedure InitCreator;
    function GetSQL(const iSQLNumero: Integer): string;
    function GetDerechoEntidadSQL: string;
    procedure SetTablaInfo(const eTabla: eTipoDiccionario);

    { -------------------------------------------------------------------------- }
    { -------------------------  DServerReportes  ------------------------------ }
    function GetMaxReporte: Integer;
    procedure SetTablaInfoCampoRep;
    procedure SetTablaInfoReporte;
    procedure BeforeUpdatePoliza(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure UpdateDataPoliza(Sender: TObject; DataSet: TzProviderClientDataSet);
    procedure BeforeUpdateReporte(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateCampoRep(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeReporteUpdatePoliza(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    { -------------------------------------------------------------------------- }

    { -------------------------------------------------------------------------- }
    { ------------------------- Metodos de DServerDiccionario  ----------------- }
    procedure BeforeUpdateListasFijas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateClasifiEntidad(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateModulosEntidad(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateFiltrosDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateOrdenDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateCamposDefault(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateClasificacion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateModulos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BorraRegistro(const iCodigo, iPosicion: Integer; const sCampo, sTabla, sCampoLlave: string);
    procedure BeforeUpdateRelacion(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure AfterUpdateDiccionario(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind);

    { -------------------------------------------------------------------------- }
    procedure GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
      UpdateKind: TUpdateKind);
    function ExportarDiccionarioDataset(DataSet: TDataSet; var sOutput: string): OleVariant;
    function GetSQLBroker: TSQLBroker;
    procedure ClearBroker;
    procedure InitBroker;
    procedure cdsGrabaDatasetReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);

    property SQLBroker: TSQLBroker read GetSQLBroker;
    procedure DiccionarioListaTablasBuildDataSet;
    procedure CargaParametrosExportarDiccionario;
    procedure CargaParametrosImportarDiccionario;
    function ImportarDiccionarioXML: OleVariant;
    function GetFiltroUsuario: string;
    function GetFiltroVersion: string;
    procedure InitLog(Empresa: OleVariant; const sPaletita: string);
    procedure EndLog;

  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function SQLValido(Empresa: OleVariant; const Formula: WideString; Entidad: Integer; var MsgError: WideString)
      : WordBool; safecall;
    {$IFDEF DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    {--------------------------------------------------------------------------}
    {------------------------- Metodos de DServerDiccionario  -----------------}
    function GrabaDiccion(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetListaFunciones(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetListaGlobal(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function PruebaFormula(Empresa, Parametros: OleVariant;var Formula: WideString; Entidad: Integer;ParamsRep: OleVariant): WordBool; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetDiccionario(Empresa: OleVariant; lverConfidencial: WordBool; const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetListaClasifi(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetTablasPorClasifi(Empresa: OleVariant; Clasifi: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetRelaciones(Empresa: OleVariant; Entidad: Integer): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function CamposPorTabla(Empresa: OleVariant; Entidad: Integer): OleVariant;{$ifndef DOS_CAPAS}safecall;{$endif}
    function GetTablaGenerica(Empresa: OleVariant; Entidad: Integer;var sDatosLookup: WideString): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetDerechosClasifi(Empresa: OleVariant; Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetDerechosEntidades(Empresa: OleVariant; Grupo: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaAccesosClasifi(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaAccesosEntidad(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetDatosDefault(Empresa: OleVariant; Entidad: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetListasFijas(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetListasFijasValores(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaListasFijas(Empresa, Delta, DeltaValores: OleVariant;var ErrorCount: Integer; iVersion, iUsuario: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function EntidadesPorModulo(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function OrdenCambiar(Empresa: OleVariant; Dataset, Codigo, OrdenViejo, OrdenNuevo: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetModulos(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaDataset(Empresa, Delta: OleVariant; TablaInfo: Integer; var ErrorCount, Posicion: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEntidades(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    procedure GetEditEntidades( Empresa: OleVariant; Entidad: Integer; var Campos, Relaciones, Modulos, Clasifi, CamposDef, OrdenDef, FiltrosDef: OleVariant); {$ifndef DOS_CAPAS}safecall;{$endif}
    function AgregarCamposFaltantes(Empresa, Campos, Entidad: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetReportes(Empresa: OleVariant; iClasifActivo, iFavoritos, iSuscripciones: Integer; const sClasificaciones: WideString; lverConfidencial: WordBool): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetReportesTodos(Empresa: OleVariant; const sClasificaciones: WideString; lVerConfidencial: WordBool): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEditReportes(Empresa: OleVariant; iReporte: Integer; var CampoRep: OleVariant): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetLookUpReportes(Empresa: OleVariant; lVerConfidencial: WordBool): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaReporte(Empresa, oDelta, oCampoRep: OleVariant; out ErrorCount: Integer; var iReporte: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEscogeReporte(Empresa: OleVariant; Entidad, Tipo: Integer; lVerConfidencial: WordBool): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEscogeReportes(Empresa: OleVariant; const Entidades,Tipos: WideString; lVerConfidencial: WordBool): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function CampoRepFiltros(Empresa: OleVariant; Reporte: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function BorraReporte(Empresa: OleVariant; iReporte: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaPoliza(Empresa, Delta, CampoRep: OleVariant; out ErrorCount: Integer; var iReporte: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GetSuscripcion(Empresa: OleVariant; iFrecuencia: Integer;const sUsuarios, sReportes: WideString;var Usuarios: OleVariant): OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    function BorraFavoritos(Empresa: OleVariant; iReporte: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function AgregaFavoritos(Empresa: OleVariant; iReporte: Integer): Integer; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetPlantilla(const Nombre: WideString): OleVariant;  {$ifndef DOS_CAPAS}safecall;{$endif}
    {--------------------------------------------------------------------------}

    { -------------------------------------------------------------------------- }
    { -------------------------  DServerReporteador  --------------------------- }
    function GetFoto(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString): OleVariant;
    {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    function GetDocumento(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString; var Extension: WideString)
      : OleVariant; {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL, Parametros: OleVariant; out Error: WideString): OleVariant;
    {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant; out Error: WideString)
      : WordBool; {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    function DirectorioPlantillas(Empresa: OleVariant): WideString; {$IFNDEF DOS_CAPAS}safecall;
    {$ENDIF}
    function GetPrimaryKey(Empresa: OleVariant; const TableName: WideString): WideString;
    {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    function DiccionarioListaTablasGetLista(Empresa, Parametros: OleVariant): OleVariant;
    {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    function ExportarDiccionario(Empresa, Parametros: OleVariant; var XMLFile: WideString): OleVariant;
    {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    function ExportarDiccionarioLista(Empresa, Lista, Parametros: OleVariant; var XMLFile: WideString): OleVariant;
    {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    function ImportarDiccionario(Empresa, Parametros: OleVariant): OleVariant;
    {$IFNDEF DOS_CAPAS}safecall; {$ENDIF}
    { -------------------------------------------------------------------------- }
  end;

implementation

uses
  DSuperReporte, DServerReportesIBO, ZBaseCreator, ZetaSQL, ZetaCommonTools, ZetaServerTools, ZGlobalTress,
  ZReportConst,
  {$IFNDEF TRESSCFG}
  ZEvaluador,
  {$ENDIF}
  ZetaQrExpr, ZDiccionTools, ZAccesosTress, ZetaEntidad, ZAgenteSQLClient, XMLDoc, XMLIntf;

const
  { -------------------------------------------------------------------------- }
  { -------------------------  DServerDiccionario  --------------------------- }

  // CONSTANTES PARA LOS SCRIPTS
  Q_GET_PRIMARY_KEY              = 1;
  Q_DICCIONARIO                  = 5;
  Q_GET_TABLAS                   = 6;
  Q_GETLISTA_FUNCIONES           = 7;
  Q_GETLISTA_GLOBAL              = 8;
  Q_GET_DESCR_GRUPO              = 9;
  Q_GETLISTA_CLASIFI             = 10;
  Q_GETLISTA_CLASIFI_DERECHOS    = 11;
  Q_GETTABLAS_X_CLASIFI          = 12;
  Q_GETTABLAS_X_CLASIFI_DERECHOS = 13;
  Q_GETRELACIONES                = 15;
  Q_GETRELACIONES_DERECHOS       = 14;
  Q_CAMPOS_X_TABLA               = 16;
  Q_CAMPOS_X_TABLA_DERECHOS      = 17;
  Q_CAMPOS_DEFAULT               = 21;
  Q_CAMPOS_DEFAULT_DERECHOS      = 22;
  Q_TABLA_GENERAL                = 18;
  Q_TABLA_GENERAL_SELECT         = 19;
  Q_DERECHOS_CLASIFI             = 23;
  Q_DERECHOS_ENTIDADES           = 24;
  Q_LISTAS_FIJAS                 = 25;
  Q_BORRA_CLAS_DERECHOS          = 27;
  Q_ENTIDADES_X_MODULO           = 28;
  Q_BORRA_LISTA_VALORES          = 30;
  Q_INSERT_LISTA_VALORES         = 31;
  Q_RELACIONES_ENTIDAD           = 32;
  Q_BORRA_CLAS_ENTIDADES         = 33;
  Q_BORRA_MODULOS_ENTIDADES      = 34;
  Q_MAX_ORDEN_CLASIFI            = 35;
  Q_MAX_ORDEN_MODULOS            = 36;
  Q_NOMBRE_TABLA                 = 37;
  Q_MAX_DATOS_DEFAULT            = 38;
  Q_MAX_DATOS_CLASIFI            = 39;
  Q_ORDEN_CAMBIA                 = 26;
  Q_ORDEN_CAMBIA_CODIGO          = 40;
  Q_BORRA_REGISTRO               = 29;
  Q_MAX_MODULOS                  = 41;
  Q_MAX_RELACION                 = 42;
  Q_ENTIDADES                    = 43;
  Q_ENTIDAD                      = 44;
  Q_R_ATRIBUTO_LISTA_FIJA        = 45;
  Q_R_VALOR                      = 46;
  Q_UPDATE_VERSION               = 47;
  Q_REFRESH_DICCION              = 48;
  Q_INSERT_DERECHO_ENTIDAD       = 49;
  Q_DELETE_DERECHO_ENTIDAD       = 50;
  Q_ENTIDADES_SIN_NIVEL0         = 51;
  Q_TABLA_GENERAL_SELECT_TIPO_PERIODO = 52;

  { -------------------------------------------------------------------------- }

  { -------------------------------------------------------------------------- }
  { -------------------------  DServerReportes  --------------------------- }
  K_DELETE_FAVORITOS = 'delete FROM MISREPOR WHERE ( RE_CODIGO = %d ) and ( US_CODIGO = %d )';

  { -------------------------------------------------------------------------- }

  // Constantes para Exportacion/Importacion del Diccionario de Datos a formato CML
  K_DICCIONARIO = 'DICCIONARIO__DE__DATOS';
  K_VERSION     = 'VERSION';
  K_CAMBIOS     = 'CAMBIOS';
  K_BORRADOS    = 'BORRADOS';

  K_TABLAS          = 'TABLAS';
  K_TABLA           = 'TABLA';
  K_ENCABEZADO      = 'ENCABEZADO';
  K_ATRIBUTO        = 'CAMPO';
  K_ATRIBUTOS       = 'CAMPOS';
  K_AT_VERSION      = 'AT_VERSION';
  K_CAMPO_DEF       = 'CAMPO__DEFAULT';
  K_CAMPOS_DEF      = 'CAMPOS__DEFAULT';
  K_RD_VERSION      = 'RD_VERSION';
  K_FILTRO_DEF      = 'FILTRO__DEFAULT';
  K_FILTROS_DEF     = 'FILTROS__DEFAULT';
  K_RF_VERSION      = 'RF_VERSION';
  K_ORDEN_DEF       = 'CRITERIO__DEFAULT';
  K_ORDENES_DEF     = 'CRITERIOS__DEFAULT';
  K_RO_VERSION      = 'RO_VERSION';
  K_RELACION        = 'RELACION';
  K_RELACIONES      = 'RELACIONES';
  K_RL_VERSION      = 'RL_VERSION';
  K_CLASIFICACION   = 'CLASIFICACION';
  K_CLASIFICACIONES = 'CLASIFICACIONES';
  K_RC_VERSION      = 'CE_VERSION';
  K_MODULO          = 'MODULO';
  K_MODULOS         = 'MODULOS';
  K_MO_VERSION      = 'ME_VERSION';
  K_LISTA_FIJA      = 'LISTA__FIJA';
  K_LISTA_FIJAS     = 'LISTA__FIJAS';
  K_LISTA_VALOR     = 'LISTA__VALOR';
  K_LISTA_VALORES   = 'LISTA__VALORES';
  // K_XML_NODE = 'xmlNode';
  K_SQL_FILTRO_TABLA = 'where EN_CODIGO = %d ';
  K_FILTRO_LISTAFIJA = 'and AT_TRANGO = 2';
  K_CAMPO_VERSION    = '@VERSION';

  Q_LISTA_ATRIBUTOS       = 'select * from R_ATRIBUTO ';
  Q_LISTA_CAMPOS_DEF      = 'select * from R_DEFAULT ';
  Q_LISTA_FILTROS_DEF     = 'select * from R_FILTRO ';
  Q_LISTA_ORDEN_DEF       = 'select * from R_ORDEN ';
  Q_LISTA_RELACIONES      = 'select * from R_RELACION ';
  Q_LISTA_CLASIFICACIONES = 'select * from R_CLAS_ENT ';
  Q_LISTA_MODULOS         = 'select * from R_MOD_ENT ';

  Arreglo: array [0 .. 27] of string = (Q_LISTA_ATRIBUTOS, K_ATRIBUTO, K_ATRIBUTOS, K_AT_VERSION, Q_LISTA_CAMPOS_DEF,
    K_CAMPO_DEF, K_CAMPOS_DEF, K_RD_VERSION, Q_LISTA_FILTROS_DEF, K_FILTRO_DEF, K_FILTROS_DEF, K_RF_VERSION,
    Q_LISTA_ORDEN_DEF, K_ORDEN_DEF, K_ORDENES_DEF, K_RO_VERSION, Q_LISTA_RELACIONES, K_RELACION, K_RELACIONES,
    K_RL_VERSION, Q_LISTA_MODULOS, K_MODULO, K_MODULOS, K_MO_VERSION, Q_LISTA_CLASIFICACIONES, K_CLASIFICACION,
    K_CLASIFICACIONES, K_RC_VERSION);

  {$R *.DFM}
  { TdmXMLTools_RDD }

procedure TdmXMLTools_RDD.SetActualNode(Node: TZetaXMLNode);
begin
  FActualNode := Node;
  SetCurrentNode(Node);
end;

class procedure TdmServerReporteadorDD.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerReporteadorDD.dmServerReporteadorDDCreate(Sender: TObject);
begin
     FCodigo := -1;
     oZetaProvider := DZetaServerProvider.GetZetaProvider(Self);
     oZetaProvider.OptimizeCursorB8 := TRUE;
end;

procedure TdmServerReporteadorDD.dmServerReporteadorDDDestroy(Sender: TObject);
begin
  ZetaSQLBroker.FreeZetaCreator(oZetaCreator);
  DZetaServerProvider.FreeZetaProvider(oZetaProvider);
end;

procedure TdmServerReporteadorDD.InitCreator;
begin
  if not Assigned(oZetaCreator) then
    oZetaCreator := ZetaSQLBroker.GetZetaCreator(oZetaProvider);
end;

{$IFDEF DOS_CAPAS}

procedure TdmServerReporteadorDD.CierraEmpresa;
begin
end;
{$ENDIF}

procedure TdmServerReporteadorDD.SetTablaInfo(const eTabla: eTipoDiccionario);
begin
  FClaseDiccionario := eTabla;
  with oZetaProvider.TablaInfo do begin
    case eTabla of

      { -------------------------------------------------------------------------- }
      { -------------------------  DServerDiccionario  --------------------------- }
      eClasifi: begin
          SetInfo('R_CLASIFI', 'RC_CODIGO, RC_ORDEN, RC_NOMBRE,RC_ACTIVO,RC_VERSION,US_CODIGO', 'RC_CODIGO');
          BeforeUpdateRecord := BeforeUpdateClasificacion;
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;

      eEntidades: begin
          SetInfo('R_ENTIDAD',
            'EN_CODIGO, EN_TITULO, EN_TABLA,EN_DESCRIP,EN_PRIMARY,EN_ATDESC,EN_ALIAS,EN_ACTIVO,EN_VERSION,US_CODIGO,EN_NIVEL0',
            'EN_CODIGO');
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eListasFijas: begin
          SetInfo('R_LISTAVAL', 'LV_CODIGO,LV_NOMBRE,LV_ACTIVO,LV_VERSION,US_CODIGO', 'LV_CODIGO');
          BeforeUpdateRecord := BeforeUpdateListasFijas;
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eListasFijasValor: begin
          SetInfo('R_VALOR', 'LV_CODIGO,VL_CODIGO,VL_DESCRIP,VL_VERSION,US_CODIGO', 'LV_CODIGO,VL_CODIGO');
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eModulos: begin
          SetInfo('R_MODULO', 'MO_CODIGO,MO_NOMBRE,MO_ORDEN,MO_ACTIVO,MO_VERSION,US_CODIGO', 'MO_CODIGO');
          BeforeUpdateRecord := BeforeUpdateModulos;
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eAccesosClasifi:
        SetInfo('R_CLAS_ACC', 'RC_CODIGO, RA_DERECHO, GR_CODIGO, CM_CODIGO', 'CM_CODIGO, GR_CODIGO, RC_CODIGO');
      eAccesosEntidades:
        SetInfo('R_ENT_ACC', 'EN_CODIGO, RE_DERECHO, GR_CODIGO, CM_CODIGO', 'CM_CODIGO, GR_CODIGO, EN_CODIGO');
      eModulosPorEntidad: begin
          SetInfo('R_MOD_ENT', 'MO_CODIGO, ME_ORDEN, EN_CODIGO,ME_VERSION,US_CODIGO', 'MO_CODIGO,ME_ORDEN');
          BeforeUpdateRecord := BeforeUpdateModulosEntidad;
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;

      eCamposPorEntidad: begin
          SetInfo('R_ATRIBUTO', 'EN_CODIGO, AT_CAMPO, AT_TITULO, AT_TIPO, AT_ANCHO, AT_MASCARA, AT_TRANGO, ' +
              'AT_ENTIDAD, LV_CODIGO, AT_CLAVES, AT_DESCRIP, AT_TCORTO, AT_SISTEMA, AT_VALORAC, ' +
              'AT_TOTAL, AT_FILTRO, AT_CONFI,AT_ACTIVO,AT_VERSION,US_CODIGO', 'EN_CODIGO, AT_CAMPO');
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eClasifiPorEntidad: begin
          SetInfo('R_CLAS_ENT', 'RC_CODIGO, CE_ORDEN, EN_CODIGO,CE_VERSION,US_CODIGO', 'RC_CODIGO, CE_ORDEN');
          BeforeUpdateRecord := BeforeUpdateClasifiEntidad;
          AfterUpdateRecord := AfterUpdateDiccionario;

        end;

      eRelacionesPorEntidad: begin
          SetInfo('R_RELACION', 'EN_CODIGO, RL_ENTIDAD, RL_CAMPOS, RL_ORDEN, RL_INNER,RL_ACTIVO,RL_VERSION,US_CODIGO',
            'EN_CODIGO,RL_ORDEN');
          BeforeUpdateRecord := BeforeUpdateRelacion;
          AfterUpdateRecord := AfterUpdateDiccionario;

        end;
      eCamposDefault: begin
          SetInfo('R_DEFAULT', 'EN_CODIGO, RD_ORDEN, RD_ENTIDAD, AT_CAMPO,RD_VERSION,US_CODIGO',
            'EN_CODIGO,		RD_ORDEN ');
          BeforeUpdateRecord := BeforeUpdateCamposDefault;
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eOrdenDefault: begin
          SetInfo('R_ORDEN', 'EN_CODIGO, RO_ORDEN, RO_ENTIDAD, AT_CAMPO,RO_VERSION,US_CODIGO', 'EN_CODIGO,RO_ORDEN');
          BeforeUpdateRecord := BeforeUpdateOrdenDefault;
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      eFiltrosDefault: begin
          SetInfo('R_FILTRO', 'EN_CODIGO, RF_ORDEN, RF_ENTIDAD, AT_CAMPO,RF_VERSION,US_CODIGO',
            'EN_CODIGO,		RF_ORDEN');
          BeforeUpdateRecord := BeforeUpdateFiltrosDefault;
          AfterUpdateRecord := AfterUpdateDiccionario;
        end;
      { -------------------------------------------------------------------------- }
    end;
  end;
end;

function TdmServerReporteadorDD.GetSQL(const iSQLNumero: Integer): string;
begin
  case iSQLNumero of
    Q_GET_PRIMARY_KEY:
      Result := 'select c.COLUMN_NAME ' + 'from INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk , ' +
        'INFORMATION_SCHEMA.KEY_COLUMN_USAGE c ' + 'where pk.TABLE_NAME = %s ' +
        'and CONSTRAINT_TYPE = ''PRIMARY KEY'' ' + 'and c.TABLE_NAME = pk.TABLE_NAME ' +
        'and c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME ' + 'order by ORDINAL_POSITION';
    { -------------------------------------------------------------------------- } { -------------------------------------------------------------------------- } { -------------------------  DServerDiccionario  --------------------------- }
    Q_DICCIONARIO:
      Result := 'SELECT ' + '( SELECT D2.DI_NOMBRE FROM DICCION D2 ' + '  WHERE ( D2.DI_CLASIFI = -1 ) ' +
        '  AND ( D2.DI_CALC = D1.DI_CLASIFI ) ) DI_TABLA, ' + 'D1.DI_CLASIFI , D1.DI_NOMBRE , D1.DI_TITULO , ' +
        'D1.DI_CALC , D1.DI_ANCHO , D1.DI_MASCARA , ' + 'D1.DI_TFIELD , D1.DI_ORDEN , D1.DI_REQUIER , ' +
        'D1.DI_TRANGO , D1.DI_NUMERO , D1.DI_VALORAC , ' + 'D1.DI_RANGOAC , D1.DI_CLAVES , D1.DI_TCORTO , ' +
        'D1.DI_CONFI ' + 'FROM DICCION D1   ' + 'WHERE D1.DI_CLASIFI<>156 %s ' + 'ORDER BY D1.DI_CLASIFI,D1.DI_NOMBRE ';
    Q_GETLISTA_FUNCIONES:
      Result := 'select DI_NOMBRE, DI_TITULO, DI_CLAVES, DI_TITULO ' + 'from   DICCION ' +
        'where  ( DI_CLASIFI = 104 ) ' + 'order by DI_NOMBRE';
    Q_GETLISTA_GLOBAL:
      Result := 'select GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO, US_CODIGO, GL_CAPTURA, GL_NIVEL ' + 'from   GLOBAL '
        + 'order by GL_DESCRIP';

    Q_GETLISTA_CLASIFI:
      Result := 'select RC_CODIGO, RC_NOMBRE,RC_ORDEN,RC_ACTIVO,RC_VERSION,US_CODIGO ' + 'from R_CLASIFI ' +
        'order by RC_ORDEN';

    Q_GETLISTA_CLASIFI_DERECHOS:
      Result := 'select R_CLASIFI.RC_CODIGO, R_CLASIFI.RC_NOMBRE, R_CLASIFI.RC_ORDEN,R_CLASIFI.US_CODIGO,R_CLASIFI.RC_VERSION,R_CLASIFI.RC_ACTIVO '
        + 'from R_CLAS_ACC ' + 'left outer join R_CLASIFI on R_CLASIFI.RC_CODIGO = R_CLAS_ACC.RC_CODIGO ' +
        'where R_CLAS_ACC.CM_CODIGO = %s and R_CLAS_ACC.GR_CODIGO = %d ' + 'and R_CLAS_ACC.RA_DERECHO >0 ' +
        'order by R_CLASIFI.RC_ORDEN ';

    Q_GETTABLAS_X_CLASIFI:
      Result := ' select R_CLAS_ENT.RC_CODIGO, R_CLAS_ENT.CE_ORDEN, R_CLAS_ENT.EN_CODIGO, ' +
        'R_ENTIDAD.EN_TITULO, R_ENTIDAD.EN_TABLA, R_CLAS_ENT.US_CODIGO, R_CLAS_ENT.CE_VERSION, ' +
        'R_ENTIDAD.EN_NIVEL0 from R_CLAS_ENT ' +
        'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = R_CLAS_ENT.EN_CODIGO ' + ' %s ' + 'order by CE_ORDEN ';

    Q_GETTABLAS_X_CLASIFI_DERECHOS:
      Result := Format(GetSQL(Q_GETTABLAS_X_CLASIFI), [GetDerechoEntidadSQL]);

    Q_GETRELACIONES:
      Result := 'SELECT %d TABLAPRINCIPAL, R_ENTIDAD.EN_CODIGO, R_ENTIDAD.EN_TITULO, R_ENTIDAD.EN_TABLA,  R_ENTIDAD.US_CODIGO '
        + 'from R_ENTIDAD WHERE EN_CODIGO in (%s)';

    Q_GETRELACIONES_DERECHOS:
      Result := 'SELECT %d TABLAPRINCIPAL, R_ENTIDAD.EN_CODIGO, R_ENTIDAD.EN_TITULO, R_ENTIDAD.EN_TABLA, R_ENTIDAD.US_CODIGO FROM R_ENTIDAD  '
        + 'WHERE EN_CODIGO IN ( SELECT R_ENT_ACC.EN_CODIGO FROM R_ENT_ACC WHERE ' +
        'CM_CODIGO = %s and GR_CODIGO = %d AND RE_DERECHO>0 AND ' + 'EN_CODIGO IN( %s )  ) ';

    Q_CAMPOS_X_TABLA:
      Result := 'select %0:s TABLAPRINCIPAL, ' +
        '( case AT_TRANGO when 1 then (select R2.EN_TABLA +''.''+R2.EN_ATDESC from R_ENTIDAD R2 where R2.EN_CODIGO= AT_ENTIDAD) else '''' end ) EN_ATDESC , '
        + 'R_ENTIDAD.EN_TABLA, ' + 'R_ATRIBUTO.EN_CODIGO, R_ATRIBUTO.AT_CAMPO, R_ATRIBUTO.AT_TITULO, ' +
        'R_ATRIBUTO.AT_TIPO, R_ATRIBUTO.AT_ANCHO, R_ATRIBUTO.AT_MASCARA, ' +
        'R_ATRIBUTO.AT_FILTRO, R_ATRIBUTO.AT_FILTROA, R_ATRIBUTO.AT_TRANGO, ' +
        'R_ATRIBUTO.AT_ENTIDAD, R_ATRIBUTO.LV_CODIGO, R_ATRIBUTO.AT_CLAVES, ' +
        'R_ATRIBUTO.AT_DESCRIP, R_ATRIBUTO.AT_TCORTO, R_ATRIBUTO.AT_SISTEMA, ' +
        'R_ATRIBUTO.AT_VALORAC, R_ATRIBUTO.AT_TOTAL, R_ATRIBUTO.AT_CONFI, ' +
        'R_ATRIBUTO.AT_ACTIVO, R_ATRIBUTO.AT_VERSION, R_ATRIBUTO.US_CODIGO ' + 'from R_ATRIBUTO ' +
        'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = R_ATRIBUTO.EN_CODIGO ' +
        'where  %1:s ( (R_ATRIBUTO.EN_CODIGO = %0:s) OR (R_ATRIBUTO.EN_CODIGO in ( %2:s ))) ';

    Q_CAMPOS_X_TABLA_DERECHOS: begin
        Result := Format(GetDerechoEntidadSQL, ['%1:s']);
        Result := Format(GetSQL(Q_CAMPOS_X_TABLA), ['%0:s', Parentesis(Result) + ' and ', '%2:s']);
      end;

    Q_CAMPOS_DEFAULT:
      Result := 'SELECT %0:d TABLAPRINCIPAL, 1 TIPO, RO_ORDEN, RO_ENTIDAD EN_CODIGO,  AT_CAMPO FROM R_ORDEN ' +
        'WHERE EN_CODIGO = %0:d  ' + 'UNION ' +
        'SELECT %0:d TABLAPRINCIPAL, 2 TIPO, RD_ORDEN, RD_ENTIDAD EN_CODIGO,  AT_CAMPO FROM R_DEFAULT ' +
        'WHERE EN_CODIGO = %0:d ' + 'UNION ' +
        'SELECT %0:d TABLAPRINCIPAL, 3 TIPO, RF_ORDEN, RF_ENTIDAD EN_CODIGO,  AT_CAMPO FROM R_FILTRO ' +
        'WHERE EN_CODIGO = %0:d ' + 'ORDER BY 1,2';

    Q_TABLA_GENERAL:
      Result := 'select EN_TABLA,EN_PRIMARY,EN_ATDESC, EN_TITULO from R_ENTIDAD where EN_CODIGO = %d';

    Q_TABLA_GENERAL_SELECT:
      Result := 'select %s from %s order by 1';

    Q_TABLA_GENERAL_SELECT_TIPO_PERIODO:
      Result := 'select %0:s, %1:s = ( select top 1 Items from dbo.FN_SPLIT( %1:s, ''/'' )), %2:s from %3:s';

    Q_DERECHOS_CLASIFI:
      Result := 'select R_CLAS_ACC.RA_DERECHO, R_CLAS_ACC.RC_CODIGO, R_CLAS_ACC.GR_CODIGO, R_CLAS_ACC.CM_CODIGO ' +
        'from R_CLAS_ACC ' + 'where R_CLAS_ACC.CM_CODIGO = %s and R_CLAS_ACC.GR_CODIGO = %d ';

    Q_DERECHOS_ENTIDADES:
      Result := 'select R_ENT_ACC.RE_DERECHO, R_ENT_ACC.EN_CODIGO, R_ENT_ACC.GR_CODIGO, R_ENT_ACC.CM_CODIGO ' +
        'from R_ENT_ACC ' + 'where R_ENT_ACC.CM_CODIGO = %s and R_ENT_ACC.GR_CODIGO = %d ';

    Q_BORRA_REGISTRO:
      Result := 'exec DBO.SP_BORRA_REGISTRO  %s, %s, %s, %d, %d';

    Q_ORDEN_CAMBIA:
      Result := 'exec DBO.SP_CAMBIA_ORDEN %s, %s, %d, %d';

    Q_ORDEN_CAMBIA_CODIGO:
      Result := 'exec DBO.SP_CAMBIA_ORDEN_CODIGO %s, %s, %s, %d, %d, %d';

    Q_BORRA_CLAS_DERECHOS:
      Result := 'Delete from R_CLAS_ACC where CM_CODIGO = %s and GR_CODIGO = %d';

    Q_ENTIDADES_X_MODULO:
      Result := 'SELECT RE.EN_CODIGO, EN_TITULO, EN_TABLA, RM.ME_ORDEN, ' + 'COALESCE( RO.MO_CODIGO, 40000) MO_CODIGO, '
        + 'COALESCE( MO_NOMBRE, ''< Sin M�dulo >'') MO_NOMBRE, ' + 'COALESCE( RO.MO_ORDEN, 40000 ) AS MO_ORDEN ' +
        'FROM R_ENTIDAD RE ' + 'LEFT OUTER JOIN R_MOD_ENT RM ON RM.EN_CODIGO = RE.EN_CODIGO ' +
        'LEFT OUTER JOIN R_MODULO RO ON RO.MO_CODIGO = RM.MO_CODIGO ' + 'ORDER BY MO_ORDEN, ME_ORDEN, EN_TITULO ';

    Q_LISTAS_FIJAS:
      Result := 'Select LV_CODIGO, LV_NOMBRE, 0 LV_DUMMY,LV_ACTIVO,LV_VERSION,US_CODIGO from R_LISTAVAL %s order by LV_CODIGO';

    Q_BORRA_LISTA_VALORES:
      Result := 'delete from R_VALOR where LV_CODIGO = %d';

    Q_INSERT_LISTA_VALORES:
      Result := 'INSERT INTO R_Valor( LV_CODIGO, VL_CODIGO, VL_DESCRIP,VL_VERSION,US_CODIGO ) ' +
        'VALUES(%d, %d, %s,%d,%d)';

    Q_RELACIONES_ENTIDAD:
      Result := 'select R_RELACION.EN_CODIGO, R_RELACION.RL_ENTIDAD ,R_RELACION.RL_CAMPOS,	R_RELACION.RL_ORDEN, ' +
        'R_RELACION.RL_INNER ,R_RELACION.RL_ACTIVO,R_RELACION.RL_VERSION,R_RELACION.US_CODIGO, R_ENTIDAD.EN_TITULO, R_ENTIDAD.EN_TABLA from R_RELACION '
        + 'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO= R_RELACION.RL_ENTIDAD ' +
        'WHERE R_RELACION.EN_CODIGO = %d ' + 'ORDER BY R_RELACION.RL_ORDEN';

    Q_BORRA_CLAS_ENTIDADES:
      Result := 'Delete from R_CLAS_ENT where EN_CODIGO = %d';

    Q_BORRA_MODULOS_ENTIDADES:
      Result := 'Delete from R_MOD_ENT where EN_CODIGO = %d';

    Q_MAX_ORDEN_CLASIFI:
      Result := 'select ISNULL( MAX(CE_ORDEN),0)+1 MAXIMO, COUNT(*) CUANTOS from R_CLAS_ENT A ' +
        'right outer join R_CLASIFI B ON ( A.RC_CODIGO = B.RC_CODIGO ) ' + 'where ( B.RC_CODIGO = %d )';

    Q_MAX_ORDEN_MODULOS:
      Result := 'select ISNULL( MAX(ME_ORDEN),0)+1 MAXIMO from R_MOD_ENT where MO_CODIGO = %d';

    Q_MAX_DATOS_DEFAULT:
      Result := 'select ISNULL( MAX(%s_ORDEN),0)+1 MAXIMO from %s where EN_CODIGO = %d';

    Q_MAX_DATOS_CLASIFI:
      Result := 'select ISNULL( MAX(RC_CODIGO),0)+1 CODIGO, ISNULL( MAX(RC_ORDEN),0)+1 ORDEN from R_CLASIFI where ( RC_ORDEN < 999 )';

    Q_MAX_MODULOS:
      Result := 'select ISNULL( MAX(MO_CODIGO),0)+1 CODIGO, ISNULL( MAX(MO_ORDEN),0)+1 ORDEN from R_MODULO ';

    Q_MAX_RELACION:
      Result := 'select ISNULL( MAX(RL_ORDEN), 0 )+1 ORDEN from R_RELACION WHERE EN_CODIGO = %d ';

    Q_NOMBRE_TABLA:
      Result := 'select EN_TABLA from R_ENTIDAD where EN_CODIGO = %d';

    Q_ENTIDADES:
      Result := 'SELECT EN_CODIGO, Cast( (EN_CODIGO) as Varchar(10)) numtabla, EN_TITULO, ' +
        'EN_TABLA,	EN_DESCRIP,	EN_PRIMARY,EN_ATDESC, EN_ALIAS,US_CODIGO,EN_ACTIVO,EN_VERSION, ' +
        '''S'' AS EXISTE, EN_NIVEL0 ' + 'FROM R_ENTIDAD ORDER BY EN_CODIGO';

    Q_ENTIDADES_SIN_NIVEL0:
      Result := 'SELECT EN_CODIGO, Cast( (EN_CODIGO) as Varchar(10)) numtabla, EN_TITULO, ' +
        'EN_TABLA,	EN_DESCRIP,	EN_PRIMARY,EN_ATDESC, EN_ALIAS,US_CODIGO,EN_ACTIVO,EN_VERSION, ' + '''S'' AS EXISTE ' +
        'FROM R_ENTIDAD ORDER BY EN_CODIGO';
    { -------------------------------------------------------------------------- }

    Q_ENTIDAD:
      Result := 'select * from R_ENTIDAD ';

    Q_R_ATRIBUTO_LISTA_FIJA:
      Result := 'select LV_CODIGO from R_ATRIBUTO ';

    Q_R_VALOR:
      Result := 'select * from R_VALOR where LV_CODIGO = %d order by VL_CODIGO';

    Q_UPDATE_VERSION:
      Result := 'update GLOBAL set GL_FORMULA = %s where GL_CODIGO = 226';

    Q_REFRESH_DICCION:
      Result := 'execute SP_REFRESH_DICCION';

    Q_INSERT_DERECHO_ENTIDAD:
      Result := 'insert into R_ENT_ACC( CM_CODIGO, GR_CODIGO, EN_CODIGO, RE_DERECHO ) ' +
        'SELECT :EMPRESA, GR_CODIGO, :ENTIDAD_PARAM1, 1 FROM V_GRUPO where ' +
        '( ( select count(*) from R_ENTIDAD where EN_CODIGO = :ENTIDAD_PARAM2  ) > 0 ) ';

    Q_DELETE_DERECHO_ENTIDAD:
      Result := 'delete from R_ENT_ACC where EN_CODIGO = :ENTIDAD and CM_CODIGO = :EMPRESA';
  end;
end;

procedure TdmServerReporteadorDD.InitLog(Empresa: OleVariant; const sPaletita: string);
begin
  {$IFDEF BITACORA_DLLS}
  FPaletita := sPaletita;
  FArchivoLog := 'c:\BitacoraTress ' + FormatDateTime('dd_mmm_yy', Date()) + '.txt';

  if FListaLog = NIL then begin
    try
      FListaLog := TAsciiLog.Create;
      FListaLog.Init(FArchivoLog);
      try
        if not varisNull(Empresa) then
          try
            FEmpresa := ' -- Alias: ' + Empresa[P_CODIGO] + ' Usuario: ' + InTToStr(Empresa[P_USUARIO])
          except
            FEmpresa := ' Error al leer Empresa[]';
          end
        else
          FEmpresa := '';
      except
        FEmpresa := ' Error al leer Empresa[]';
      end;

      FListaLog.WriteTexto(FormatDateTime('dd/mmm/yy hh:nn:ss', Now) + ' DServerReporteador :: INICIA :: ' + FPaletita +
          FEmpresa);
      FListaLog.Close;
    except
    end;
  end;
  {$ENDIF}
end;

procedure TdmServerReporteadorDD.EndLog;
begin
  {$IFDEF BITACORA_DLLS}
  try
    if FArchivoLog = '' then
      FArchivoLog := 'c:\BitacoraTress ' + FormatDateTime('dd_mmm_yy', Date()) + '.txt';
    if FListaLog = NIL then begin
      FListaLog := TAsciiLog.Create;
      FListaLog.Init(FArchivoLog);
      FListaLog.WriteTexto(FormatDateTime('dd/mmm/yy hh:nn:ss', Now) + ' DServerReporteador :: TERMINA :: ' + FPaletita
          + ' ' + FEmpresa);
    end else begin
      // FListaLog.Open(FArchivoLog);
      FListaLog.Init(FArchivoLog);
      FListaLog.WriteTexto(FormatDateTime('dd/mmm/yy hh:nn:ss', Now) + ' DServerReporteador :: TERMINA :: ' + FPaletita
          + ' ' + FEmpresa);
      // FListaLog.Close;
    end;
    FListaLog.Close;
    FreeAndNil(FListaLog);
  except

  end;
  {$ENDIF}
end;

{ ---------------------------------------------------------------- }
{ ***************** PARTE PRIVADA ******************************* }

function TdmServerReporteadorDD.GetDerechoEntidadSQL: string;
begin
  with oZetaProvider do begin
    Result := Format(' %s IN ' + '( SELECT R_ENT_ACC.EN_CODIGO FROM R_ENT_ACC WHERE  ' +
        'CM_CODIGO = %s and GR_CODIGO = %d AND RE_DERECHO>0 )  ', ['%s', EntreComillas(CodigoEmpresaActiva),
        CodigoGrupo]);
  end;
end;

{ *********** Interfases ( Paletitas ) ************ }
{ -------------------------------------------------------------------------- }
{ -------------------------  DServerDiccionario  --------------------------- }
function TdmServerReporteadorDD.GrabaDiccion(Empresa, oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
  InitLog(Empresa, 'GrabaDiccion');
  with oZetaProvider, TablaInfo do begin
    SetInfo('DICCION', 'DI_CLASIFI, DI_NOMBRE, DI_TITULO, DI_CALC, DI_ANCHO, DI_MASCARA, DI_TFIELD, DI_ORDEN, ' +
        'DI_REQUIER,DI_TRANGO,DI_NUMERO,DI_VALORAC,DI_RANGOAC,DI_CLAVES,DI_TCORTO,DI_CONFI', 'DI_CLASIFI,DI_NOMBRE');
    Result := GrabaTabla(Empresa, oDelta, ErrorCount);
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetListaFunciones(Empresa: OleVariant): OleVariant;
var
  oEvaluador: TZetaEvaluador;
  i         : Integer;
begin
  InitLog(Empresa, 'GetListaFunciones');
  { .$ifndef RDD }
  // Result := oZetaProvider.OpenSQL( Empresa, GetSQL( Q_GETLISTA_FUNCIONES ), TRUE );
  { .$else }
  with cdsTemporal do begin
    AddStringField('DI_NOMBRE', 10);
    AddStringField('DI_TITULO', 90);
    AddStringField('DI_CLAVES', 10);
    CreateDataset;
  end;
  InitCreator;
  with oZetaCreator do begin
    RegistraFunciones([efComunes, efReporte]);
  end;
  oEvaluador := TZetaEvaluador.Create(oZetaCreator);
  for i := 0 to oEvaluador.LibreriaFunciones.EntryList.Count - 1 do begin
    with cdsTemporal do begin
      Append;
      with oEvaluador.LibreriaFunciones.Entry[i] do begin
        FieldByName('DI_NOMBRE').AsString := Name;
        FieldByName('DI_TITULO').AsString := Description;
      end;
      Post;
    end;

  end;
  cdsTemporal.IndexFieldNames := 'DI_NOMBRE';
  Result := cdsTemporal.Data;
  { .$endif }
  FreeAndNil(oEvaluador);
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetListaGlobal(Empresa: OleVariant): OleVariant;
begin
  Result := oZetaProvider.OpenSQL(Empresa, GetSQL(Q_GETLISTA_GLOBAL), TRUE);
end;

function TdmServerReporteadorDD.PruebaFormula(Empresa, Parametros: OleVariant; var Formula: WideString;
  Entidad: Integer; ParamsRep: OleVariant): WordBool;
var
  oEvaluador   : TZetaEvaluador;
  oSuperReporte: TdmSuperReporte;
  TipoFormula  : eTipoGlobal;
  sTransformada: string;
  lConstante   : Boolean;
  eFunciones   : TipoFunciones;
  eEvaluador   : eTipoEvaluador;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    AsignaParamList(Parametros);
    InitGlobales;
    eEvaluador := eTipoEvaluador(ParamList.ParamByName('TipoEvaluador').AsInteger);
  end;
  oSuperReporte := NIL;

  InitCreator;
  if not(varisNull(ParamsRep) OR VarIsEmpty(ParamsRep)) then begin
    oSuperReporte := TdmSuperReporte.Create(oZetaCreator);
    oSuperReporte.Parametros := ParamsRep;
  end;

  with oZetaCreator do begin
    eFunciones := [efComunes, efAnuales, efNomina, efReporte, efPoliza];
    if (eEvaluador = evAguinaldo) then
      eFunciones := eFunciones + [efAguinaldo];
    if (eEvaluador = evReglasPrestamo) then
      eFunciones := eFunciones + [efReglasPrestamos];
    PreparaEntidades;
    RegistraFunciones(eFunciones);

    SuperReporte := oSuperReporte;
  end;
  oEvaluador := TZetaEvaluador.Create(oZetaCreator);

  { CV: 28-Nov-2001.
    Lo que se esta logrando aqui, es que el Objecto CalcNomina tenga un valor DIFERENTE de
    NIL. Esto es para que cuando se evalue una formula que nada mas pertenezca al calculo de nomina,
    esta funcion tenga acceso a este objeto. }
  if (eEvaluador = evNomina) then
    oEvaluador.CalcNomina := Self;

  { Para que funcione con PARAM():
    -Se tiene que crear un oSuperReporte, y este asignarselo a oZetaCreator
    -Al oSuperReporte, se le asignan los parametros
    -Destruir al final el oSuperReporte }
  try
    oEvaluador.Entidad := TipoEntidad(Entidad);
    sTransformada := Formula;
    Result := oEvaluador.GetRequiere(Formula, TipoFormula, sTransformada, lConstante);
    if not Result then
      Formula := sTransformada; // Regresa el mensaje de Error en 'Formula'
  finally
    FreeAndNil(oEvaluador);
    FreeAndNil(oSuperReporte);
  end;
end;

function TdmServerReporteadorDD.GetDiccionario(Empresa: OleVariant; lverConfidencial: WordBool;
  const Filtro: WideString): OleVariant;
var
  sFiltro: string;
begin
  // Los Confidenciales se Resuelven en el Cliente.
  // Por que tiene cache el Diccion.
  // No se pueden refrescar los datos dependiendo los derechos
  // de usuario. Se mandan todos los datos y que el cliente
  // se haga cargo de los confidenciales.
  sFiltro := Filtro;
  if not lverConfidencial then
    sFiltro := ConcatFiltros(sFiltro, 'D1.DI_CONFI = ''N''');
  if strLleno(sFiltro) then
    sFiltro := 'and ' + sFiltro;
  Result := oZetaProvider.OpenSQL(Empresa, Format(GetSQL(Q_DICCIONARIO), [sFiltro]), TRUE);
end;

function TdmServerReporteadorDD.GetListaClasifi(Empresa: OleVariant): OleVariant;
var
  sScript: string;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
  end;

  if (oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION) then
    sScript := GetSQL(Q_GETLISTA_CLASIFI)
  else
    sScript := Format(GetSQL(Q_GETLISTA_CLASIFI_DERECHOS), [EntreComillas(oZetaProvider.CodigoEmpresaActiva),
        oZetaProvider.CodigoGrupo]);

  Result := oZetaProvider.OpenSQL(Empresa, sScript, TRUE);
end;

function TdmServerReporteadorDD.GetTablasPorClasifi(Empresa: OleVariant; Clasifi: Integer): OleVariant;
var
  sScript: string;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
  end;

  if (oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION) then
    sScript := Format(GetSQL(Q_GETTABLAS_X_CLASIFI), [VACIO])
  else
    sScript := Format(GetSQL(Q_GETTABLAS_X_CLASIFI_DERECHOS), ['where R_CLAS_ENT.EN_CODIGO']);

  Result := oZetaProvider.OpenSQL(Empresa, sScript, TRUE);
end;

function TdmServerReporteadorDD.GetRelaciones(Empresa: OleVariant; Entidad: Integer): OleVariant;
var
  sScript: string;
  oLista : TStringList;
begin

  with oZetaProvider do begin
    EmpresaActiva := Empresa;
  end;

  InitCreator;
  oLista := TStringList.Create;
  oLista.Sorted := TRUE;
  oLista.Add(InTToStr(Entidad));
  try
    oZetaCreator.PreparaEntidades;
    oZetaCreator.dmEntidadesTress.RelacionesEntidad(TipoEntidad(Entidad), oLista, TRUE);

    with oZetaProvider do begin
      if (CodigoGrupo = D_GRUPO_SIN_RESTRICCION) then
        sScript := Format(GetSQL(Q_GETRELACIONES), [Entidad, oLista.CommaText])
      else
        sScript := Format(GetSQL(Q_GETRELACIONES_DERECHOS), [Entidad, EntreComillas(CodigoEmpresaActiva), CodigoGrupo,
            oLista.CommaText]);

      Result := OpenSQL(Empresa, sScript, TRUE);
    end;
  finally
    FreeAndNil(oLista);
  end;
  SetComplete;
end;

function TdmServerReporteadorDD.CamposPorTabla(Empresa: OleVariant; Entidad: Integer): OleVariant;
var
  sScript: string;
  oLista : TStringList;
begin
  InitLog(Empresa, 'CamposPorTabla');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
  end;

  InitCreator;
  oLista := TStringList.Create;
  oLista.Sorted := TRUE;
  oLista.Add(InTToStr(Entidad));
  try
    oZetaCreator.PreparaEntidades;
    oZetaCreator.dmEntidadesTress.RelacionesEntidad(TipoEntidad(Entidad), oLista, TRUE);

    if (oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION) then
      sScript := Format(GetSQL(Q_CAMPOS_X_TABLA), [InTToStr(Entidad), VACIO, oLista.CommaText])
    else
      sScript := Format(GetSQL(Q_CAMPOS_X_TABLA_DERECHOS), [InTToStr(Entidad), 'R_ATRIBUTO.EN_CODIGO',
          oLista.CommaText]);

    Result := oZetaProvider.OpenSQL(Empresa, sScript, TRUE);
  finally
    FreeAndNil(oLista);
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetTablaGenerica(Empresa: OleVariant; Entidad: Integer; var sDatosLookup: WideString): OleVariant;
const
     K_CAMPO_PERIODO = 'TP_NIVEL0';
     K_CAMPO_TP_NIVEL0 = 2;
var
  oLista: TStrings;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    FQuery := CreateQuery(Format(GetSQL(Q_TABLA_GENERAL), [Entidad]));
  end;
  FQuery.Active := TRUE;

  oLista := TStringList.Create;
  try
    with FQuery do begin
      oLista.Add(FieldByName('EN_ATDESC').AsString);
      oLista.Add(FieldByName('EN_PRIMARY').AsString);
      {*** https://gtress.tpondemand.com/entity/15114: Tipo de periodo este filtrada por confidencialidad. ***}
      if TipoEntidad(Entidad) = enTPeriodo then
      begin
           oLista.Add( K_CAMPO_PERIODO );
      end;
      {*** FIN ***}
      if TipoEntidad(Entidad) = enTPeriodo then
      begin
           Result := oZetaProvider.OpenSQL(Empresa, Format(GetSQL(Q_TABLA_GENERAL_SELECT_TIPO_PERIODO),
              [FieldByName('EN_PRIMARY').AsString, FieldByName('EN_ATDESC').AsString, K_CAMPO_PERIODO, FieldByName('EN_TABLA').AsString]), TRUE);
      end
      else
          Result := oZetaProvider.OpenSQL(Empresa, Format(GetSQL(Q_TABLA_GENERAL_SELECT),
              [oLista.CommaText, FieldByName('EN_TABLA').AsString]), TRUE);

      sDatosLookup := COMILLA + FieldByName('EN_TITULO').AsString + COMILLA + ',' + oLista[0];
      if oLista.Count > 1 then
        sDatosLookup := sDatosLookup + ',' + oLista[1];
      {*** https://gtress.tpondemand.com/entity/15114: Tipo de periodo este filtrada por confidencialidad. ***}
      if ( oLista.Count > K_CAMPO_TP_NIVEL0 ) and ( TipoEntidad(Entidad) = enTPeriodo ) then
        sDatosLookup := sDatosLookup + ',' + oLista[K_CAMPO_TP_NIVEL0];
      {*** FIN ***}
    end;
  finally
    FreeAndNil(oLista);
  end;
end;

function TdmServerReporteadorDD.GetDerechosClasifi(Empresa: OleVariant; Grupo: Integer): OleVariant;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;

    Result := oZetaProvider.OpenSQL(Empresa, Format(GetSQL(Q_DERECHOS_CLASIFI), [EntreComillas(CodigoEmpresaActiva),
          Grupo]), TRUE);
  end;
end;

function TdmServerReporteadorDD.GetDatosDefault(Empresa: OleVariant; Entidad: Integer): OleVariant;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    Result := oZetaProvider.OpenSQL(Empresa, Format(GetSQL(Q_CAMPOS_DEFAULT), [Entidad]), TRUE);
  end;
end;

function TdmServerReporteadorDD.GetDerechosEntidades(Empresa: OleVariant; Grupo: Integer): OleVariant;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;

    Result := oZetaProvider.OpenSQL(Empresa, Format(GetSQL(Q_DERECHOS_ENTIDADES), [EntreComillas(CodigoEmpresaActiva),
          Grupo]), TRUE);
  end;
end;

function TdmServerReporteadorDD.GrabaAccesosClasifi(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant;
  out ErrorCount: Integer): OleVariant;
begin
  if varisNull(Delta) then begin
    ErrorCount := 0;
    Result := Null;
  end
  else
    with oZetaProvider do begin
      EmpresaActiva := Empresa;
      SetTablaInfo(eAccesosClasifi);
      Result := GrabaTabla(EmpresaActiva, Delta, ErrorCount);
    end;
end;

function TdmServerReporteadorDD.GrabaAccesosEntidad(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant;
  out ErrorCount: Integer): OleVariant;
begin
  InitLog(Empresa, 'GrabaAccesosEntidad');
  if varisNull(Delta) then begin
    ErrorCount := 0;
    Result := Null;
  end else begin
    with oZetaProvider do begin
      SetTablaInfo(eAccesosEntidades);
      Result := GrabaTabla(Empresa, Delta, ErrorCount);
    end;
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetListasFijas(Empresa: OleVariant): OleVariant;
begin
  InitLog(Empresa, 'GetListasFijas');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;

    Result := oZetaProvider.OpenSQL(Empresa, Format(GetSQL(Q_LISTAS_FIJAS), [VACIO]), TRUE);
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetListasFijasValores(Empresa: OleVariant): OleVariant;
begin
  InitLog(Empresa, 'GetListasFijasValores');
  SetTablaInfo(eListasFijasValor);
  with oZetaProvider do begin
    Result := GetTabla(Empresa);
  end;
  EndLog;
  SetComplete;

end;

function TdmServerReporteadorDD.GrabaListasFijas(Empresa, Delta, DeltaValores: OleVariant; var ErrorCount: Integer;
  iVersion, iUsuario: Integer): OleVariant;
var
  oLista: TStrings;
  i     : Integer;
begin
  InitLog(Empresa, 'GrabaListasFijas');
  ErrorCount := 0;
  SetTablaInfo(eListasFijas);
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    if not varisNull(Delta) then
      Result := GrabaTabla(Empresa, Delta, ErrorCount)
    else
      Result := Null;

    if (ErrorCount = 0) and (FCodigo > 0) then begin
      with oZetaProvider do begin
        EmpiezaTransaccion;
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_BORRA_LISTA_VALORES), [FCodigo]));
        TerminaTransaccion(TRUE);
      end;

      oLista := TStringList.Create;
      try
        oLista.CommaText := DeltaValores;
        EmpiezaTransaccion;

        for i := 0 to oLista.Count - 1 do begin
          ExecSQL(EmpresaActiva, Format(GetSQL(Q_INSERT_LISTA_VALORES), [FCodigo, i, Comillas(Copy(oLista[i], 1, 30)),
                iVersion, iUsuario]));
        end;
        TerminaTransaccion(TRUE);
      finally
        FreeAndNil(oLista);
      end;
    end;
  end;
  EndLog;
  SetComplete;
end;

procedure TdmServerReporteadorDD.BeforeUpdateListasFijas(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  if (UpdateKind in [ukModify, ukInsert]) then begin
    FCodigo := CampoAsVar(DeltaDS.FieldByName('LV_CODIGO'));
    if (DeltaDS.FindField('LV_DUMMY') <> NIL) then
      Applied := CambiaCampo(DeltaDS.FieldByName('LV_DUMMY'));
  end;
end;

function TdmServerReporteadorDD.OrdenCambiar(Empresa: OleVariant; DataSet, Codigo, OrdenViejo, OrdenNuevo: Integer): OleVariant;
var
  sTabla, sCampo, sCodigo: string;
begin

  InitLog(Empresa, 'OrdenCambiar');
  case eTipoDiccionario(DataSet) of
    eClasifi: begin
        sTabla := 'R_CLASIFI';
        sCampo := 'RC_ORDEN';
        sCodigo := 'RC_CODIGO';
      end;
    eModulos: begin
        sTabla := 'R_MODULO';
        sCampo := 'MO_ORDEN';
        sCodigo := 'MO_CODIGO';
      end;
    eModulosPorEntidad: begin
        sTabla := 'R_MOD_ENT';
        sCampo := 'ME_ORDEN';
        sCodigo := 'MO_CODIGO';
      end;
    eClasifiPorEntidad: begin
        sTabla := 'R_CLAS_ENT';
        sCampo := 'CE_ORDEN';
        sCodigo := 'RC_CODIGO';
      end;
    eRelacionesPorEntidad: begin
        sTabla := 'R_RELACION';
        sCampo := 'RL_ORDEN';
        sCodigo := 'EN_CODIGO';
      end;
    eCamposDefault: begin
        sTabla := 'R_DEFAULT';
        sCampo := 'RD_ORDEN';
        sCodigo := 'EN_CODIGO';
      end;
    eOrdenDefault: begin
        sTabla := 'R_ORDEN';
        sCampo := 'RO_ORDEN';
        sCodigo := 'EN_CODIGO';
      end;
    eFiltrosDefault: begin
        sTabla := 'R_FILTRO';
        sCampo := 'RF_ORDEN';
        sCodigo := 'EN_CODIGO';
      end;
  end;

  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    EmpiezaTransaccion;
    try
      SetTablaInfo(eTipoDiccionario(DataSet));
      if eTipoDiccionario(DataSet) in [eClasifi, eModulos] then begin
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_ORDEN_CAMBIA), [Comillas(sTabla), Comillas(sCampo), OrdenViejo,
              OrdenNuevo]));
      end else begin
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_ORDEN_CAMBIA_CODIGO), [Comillas(sTabla), Comillas(sCampo),
              Comillas(sCodigo), Codigo, OrdenViejo, OrdenNuevo]));
        TablaInfo.Filtro := sCodigo + '=' + InTToStr(Codigo);
      end;
      TerminaTransaccion(TRUE);
      if eTipoDiccionario(DataSet) = eRelacionesPorEntidad then
        Result := OpenSQL(Empresa, Format(GetSQL(Q_RELACIONES_ENTIDAD), [Codigo]), TRUE)
      else
        Result := GetTabla(Empresa);
    except
      on Error: Exception do begin
        RollBackTransaccion;
        raise;
      end;
    end;
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.EntidadesPorModulo(Empresa: OleVariant): OleVariant;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    Result := oZetaProvider.OpenSQL(Empresa, GetSQL(Q_ENTIDADES_X_MODULO), TRUE);
  end;
end;

function TdmServerReporteadorDD.GetModulos(Empresa: OleVariant): OleVariant;
begin
  InitLog(Empresa, 'GetModulos');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    SetTablaInfo(eModulos);
    Result := GetTabla(Empresa);
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GrabaDataset(Empresa, Delta: OleVariant; TablaInfo: Integer;
  var ErrorCount, Posicion: Integer): OleVariant;
begin
  InitLog(Empresa, 'GrabaDataset');
  if varisNull(Delta) then begin
    ErrorCount := 0;
    Result := Null;
  end else begin
    SetTablaInfo(eTipoDiccionario(TablaInfo));
    with oZetaProvider do begin
      EmpresaActiva := Empresa;
      Result := GrabaTabla(Empresa, Delta, ErrorCount);
      if (ErrorCount = 0) then
        Posicion := FPosicion
      else
        Posicion := -1;
    end;
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetEntidades(Empresa: OleVariant): OleVariant;
begin
  InitLog(Empresa, 'GetEntidades');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    // SetTablaInfo( eEntidades );
    Result := oZetaProvider.OpenSQL(Empresa, GetSQL(Q_ENTIDADES), TRUE);
  end;
  EndLog;
  SetComplete;
end;

procedure TdmServerReporteadorDD.GetEditEntidades(Empresa: OleVariant; Entidad: Integer;
  var Campos, Relaciones, Modulos, Clasifi, CamposDef, OrdenDef, FiltrosDef: OleVariant);
var
  sFiltro: string;
begin
  InitLog(Empresa, 'GetEditEntidades');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    sFiltro := Format('EN_CODIGO = %d', [Entidad]);

    SetTablaInfo(eModulosPorEntidad);
    TablaInfo.Filtro := sFiltro;
    Modulos := GetTabla(Empresa);

    SetTablaInfo(eCamposPorEntidad);
    TablaInfo.Filtro := sFiltro;
    Campos := GetTabla(Empresa);

    Relaciones := oZetaProvider.OpenSQL(Empresa, Format(GetSQL(Q_RELACIONES_ENTIDAD), [Entidad]), TRUE);

    SetTablaInfo(eClasifiPorEntidad);
    TablaInfo.Filtro := sFiltro;
    Clasifi := GetTabla(Empresa);

    SetTablaInfo(eCamposDefault);
    TablaInfo.Filtro := sFiltro;
    CamposDef := GetTabla(Empresa);

    SetTablaInfo(eOrdenDefault);
    TablaInfo.Filtro := sFiltro;
    OrdenDef := GetTabla(Empresa);

    SetTablaInfo(eFiltrosDefault);
    TablaInfo.Filtro := sFiltro;
    FiltrosDef := GetTabla(Empresa);
  end;
  EndLog;
  SetComplete;
end;

procedure TdmServerReporteadorDD.BeforeUpdateClasifiEntidad(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
  iClasifi: Integer;
begin
  with oZetaProvider do begin
    if (UpdateKind <> ukDelete) then begin
      if (FCodigo = -1) then begin
        FCodigo := CampoAsVar(DeltaDS.FieldByName('EN_CODIGO'));
        EmpiezaTransaccion;
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_BORRA_CLAS_ENTIDADES), [FCodigo]));
        TerminaTransaccion(TRUE);
      end;

      iClasifi := CampoAsVar(DeltaDS.FieldByName('RC_CODIGO'));
      FQuery := CreateQuery(Format(GetSQL(Q_MAX_ORDEN_CLASIFI), [iClasifi]));
      FQuery.Active := TRUE;

      DeltaDS.Edit;
      DeltaDS.FieldByName('CE_ORDEN').AsInteger := FQuery.Fields[0].AsInteger;

      if FQuery.FieldByName('CUANTOS').AsInteger = 0 then
        // Quiere decir que la clasificacion no existe.
        DeltaDS.FieldByName('RC_CODIGO').AsInteger := crClasificacionSistema;

      DeltaDS.Post;

      FQuery.Free;
    end;
  end;
end;

procedure TdmServerReporteadorDD.BeforeUpdateModulosEntidad(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
  iModulo: Integer;
begin
  with oZetaProvider do begin
    if (UpdateKind <> ukDelete) then begin
      if (FCodigo = -1) then begin
        FCodigo := CampoAsVar(DeltaDS.FieldByName('EN_CODIGO'));
        EmpiezaTransaccion;
        ExecSQL(EmpresaActiva, Format(GetSQL(Q_BORRA_MODULOS_ENTIDADES), [FCodigo]));
        TerminaTransaccion(TRUE);
      end;

      iModulo := CampoAsVar(DeltaDS.FieldByName('MO_CODIGO'));
      FQuery := CreateQuery(Format(GetSQL(Q_MAX_ORDEN_MODULOS), [iModulo]));
      FQuery.Active := TRUE;

      DeltaDS.Edit;
      DeltaDS.FieldByName('ME_ORDEN').AsInteger := FQuery.Fields[0].AsInteger;
      DeltaDS.Post;

      FQuery.Free;
    end;
  end;
end;

function TdmServerReporteadorDD.AgregarCamposFaltantes(Empresa, Campos, Entidad: OleVariant): OleVariant;
var
  sCampo: string;
  i     : Integer;
begin
  InitLog(Empresa, 'AgregarCamposFaltantes');
  cdsTemporal.Data := Campos;

  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    InitGlobales;
    FQuery := CreateQuery(Format(GetSQL(Q_NOMBRE_TABLA), [Integer(Entidad)]));

    SetTablaInfo(eCamposPorEntidad);

    with FQuery do begin
      Active := TRUE;
      PreparaQuery(FQuery, Format(GetSQL(Q_TABLA_GENERAL_SELECT), ['*', (FQuery.Fields[0].AsString)]));
      Active := TRUE;
      for i := 0 to (FieldCount - 1) do begin
        sCampo := Fields[i].FieldName;

        if not cdsTemporal.Locate('AT_CAMPO', sCampo, []) and (sCampo <> K_CAMPO_LLAVE_PORTAL) then begin
          cdsTemporal.Append;
          cdsTemporal.FieldByName('EN_CODIGO').AsInteger := Entidad;
          cdsTemporal.FieldByName('AT_CAMPO').AsString := sCampo;
          cdsTemporal.FieldByName('AT_TITULO').AsString := sCampo;
          cdsTemporal.FieldByName('AT_CLAVES').AsString := sCampo;
          cdsTemporal.FieldByName('AT_DESCRIP').AsString := sCampo;
          cdsTemporal.FieldByName('AT_TCORTO').AsString := sCampo;
          cdsTemporal.FieldByName('AT_SISTEMA').AsString := K_GLOBAL_SI;
          cdsTemporal.FieldByName('AT_VERSION').AsInteger := GetGlobalInteger(K_GLOBAL_VERSION_RDD);
          ZDiccionTools.DefaultDato(cdsTemporal, Fields[i]);

          cdsTemporal.Post;
          ActualizaTabla(cdsTemporal, ukInsert);

        end;
      end;
      Active := FALSE

    end;
    TablaInfo.Filtro := Format('EN_CODIGO = %d', [Integer(Entidad)]);
    Result := GetTabla(EmpresaActiva);
  end;
  EndLog;
  SetComplete;
end;

procedure TdmServerReporteadorDD.BeforeUpdateCamposDefault(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with oZetaProvider do begin
    case UpdateKind of
      ukInsert: begin
          FQuery := CreateQuery(Format(GetSQL(Q_MAX_DATOS_DEFAULT),
              ['RD', 'R_DEFAULT', DeltaDS.FieldByName('EN_CODIGO').AsInteger]));
          FQuery.Active := TRUE;
          FPosicion := FQuery.Fields[0].AsInteger;

          DeltaDS.Edit;
          DeltaDS.FieldByName('RD_ORDEN').AsInteger := FPosicion;
          DeltaDS.Post;

          FQuery.Free;
        end;
      ukDelete: begin
          BorraRegistro(DeltaDS.FieldByName('EN_CODIGO').AsInteger, DeltaDS.FieldByName('RD_ORDEN').AsInteger,
            'RD_ORDEN', 'R_DEFAULT', 'EN_CODIGO');
          Applied := TRUE;
        end;
    end;
  end;

end;

procedure TdmServerReporteadorDD.BeforeUpdateFiltrosDefault(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with oZetaProvider do begin
    case UpdateKind of
      ukInsert: begin
          FQuery := CreateQuery(Format(GetSQL(Q_MAX_DATOS_DEFAULT),
              ['RF', 'R_FILTRO', DeltaDS.FieldByName('EN_CODIGO').AsInteger]));
          FQuery.Active := TRUE;
          FPosicion := FQuery.Fields[0].AsInteger;

          DeltaDS.Edit;
          DeltaDS.FieldByName('RF_ORDEN').AsInteger := FPosicion;
          DeltaDS.Post;

          FQuery.Free;
        end;
      ukDelete: begin
          BorraRegistro(DeltaDS.FieldByName('EN_CODIGO').AsInteger, DeltaDS.FieldByName('RF_ORDEN').AsInteger,
            'RF_ORDEN', 'R_FILTRO', 'EN_CODIGO');
          Applied := TRUE;
        end;
    end;
  end;
end;

procedure TdmServerReporteadorDD.BeforeUpdateOrdenDefault(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with oZetaProvider do begin
    case UpdateKind of
      ukInsert: begin
          FQuery := CreateQuery(Format(GetSQL(Q_MAX_DATOS_DEFAULT),
              ['RO', 'R_ORDEN', DeltaDS.FieldByName('EN_CODIGO').AsInteger]));
          FQuery.Active := TRUE;
          FPosicion := FQuery.Fields[0].AsInteger;

          DeltaDS.Edit;
          DeltaDS.FieldByName('RO_ORDEN').AsInteger := FPosicion;
          DeltaDS.Post;

          FQuery.Free;
        end;
      ukDelete: begin
          BorraRegistro(DeltaDS.FieldByName('EN_CODIGO').AsInteger, DeltaDS.FieldByName('RO_ORDEN').AsInteger,
            'RO_ORDEN', 'R_ORDEN', 'EN_CODIGO');
          Applied := TRUE;
        end;
    end;
  end;
end;

procedure TdmServerReporteadorDD.BeforeUpdateRelacion(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with oZetaProvider do begin
    case UpdateKind of
      ukInsert: begin
          FQuery := CreateQuery(Format(GetSQL(Q_MAX_RELACION), [DeltaDS.FieldByName('EN_CODIGO').AsInteger]));
          FQuery.Active := TRUE;

          DeltaDS.Edit;
          DeltaDS.FieldByName('RL_ORDEN').AsInteger := FQuery.Fields[0].AsInteger;
          DeltaDS.Post;

          FQuery.Free;
        end;
    end;
  end;
end;

procedure TdmServerReporteadorDD.BorraRegistro(const iCodigo, iPosicion: Integer;
  const sCampo, sTabla, sCampoLlave: string);
begin
  with oZetaProvider do begin
    EmpiezaTransaccion;
    try
      ExecSQL(EmpresaActiva, Format(GetSQL(Q_BORRA_REGISTRO), [Comillas(sTabla), Comillas(sCampo),
            Comillas(sCampoLlave), iCodigo, iPosicion]));
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do begin
        RollBackTransaccion;
        raise;
      end;
    end;
  end;
end;

procedure TdmServerReporteadorDD.BeforeUpdateClasificacion(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  case UpdateKind of
    ukInsert: begin
        FQuery := oZetaProvider.CreateQuery(GetSQL(Q_MAX_DATOS_CLASIFI));
        FQuery.Active := TRUE;

        DeltaDS.Edit;
        DeltaDS.FieldByName('RC_CODIGO').AsInteger := FQuery.Fields[0].AsInteger;
        { Se permite c�digos menores de 1000 }
        DeltaDS.FieldByName('RC_ORDEN').AsInteger := FQuery.Fields[1].AsInteger;
        DeltaDS.Post;

        FQuery.Free;
      end;
    ukDelete: begin
        BorraRegistro(DeltaDS.FieldByName('RC_CODIGO').AsInteger, DeltaDS.FieldByName('RC_ORDEN').AsInteger, 'RC_ORDEN',
          'R_CLASIFI', 'RC_CODIGO');
        Applied := TRUE;
      end;
  end;
end;

procedure TdmServerReporteadorDD.BeforeUpdateModulos(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  case UpdateKind of
    ukInsert: begin
        FQuery := oZetaProvider.CreateQuery(GetSQL(Q_MAX_MODULOS));
        FQuery.Active := TRUE;

        DeltaDS.Edit;
        DeltaDS.FieldByName('MO_ORDEN').AsInteger := FQuery.Fields[1].AsInteger;
        DeltaDS.Post;

        FQuery.Free;
      end;

    ukDelete: begin
        BorraRegistro(DeltaDS.FieldByName('MO_CODIGO').AsInteger, DeltaDS.FieldByName('MO_ORDEN').AsInteger, 'MO_ORDEN',
          'R_MODULO', 'MO_CODIGO');
        Applied := TRUE;
      end;
  end;
end;
{ -------------------------------------------------------------------------- }

{ -------------------------------------------------------------------------- }
{ -------------------------  DServerReportes  --------------------------- }
procedure TdmServerReporteadorDD.SetTablaInfoReporte;
begin
  with oZetaProvider do begin
    TablaInfo.SetInfo('REPORTE', 'RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,' +
        'RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,RE_CFECHA,' + 'RE_HOJA,RE_ALTO,RE_ANCHO,RE_PRINTER,RE_COPIAS,' +
        'RE_PFILE,RE_ARCHIVO,RE_VERTICA,US_CODIGO,RE_FILTRO,' + 'RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,'
        + 'RE_MAR_DER,RE_MAR_INF,RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,' + 'QU_CODIGO,RE_CLASIFI,RE_CANDADO,RE_IFECHA',
      'RE_CODIGO');
  end;
end;

procedure TdmServerReporteadorDD.SetTablaInfoCampoRep;
begin
  with oZetaProvider do begin
    TablaInfo.SetInfo('CAMPOREP',
      'RE_CODIGO, CR_TIPO, CR_POSICIO, CR_CLASIFI, CR_SUBPOS, CR_TABLA, CR_TITULO, CR_REQUIER, ' +
        'CR_CALC, CR_MASCARA, CR_ANCHO, CR_OPER, CR_TFIELD, CR_SHOW, CR_DESCRIP, CR_COLOR, ' +
        'CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA', 'RE_CODIGO,CR_TIPO,CR_POSICIO,CR_SUBPOS');
  end;
end;

{$IFDEF RDD}

function TdmServerReporteadorDD.GetReportes(Empresa: OleVariant; iClasifActivo, iFavoritos, iSuscripciones: Integer;
  const sClasificaciones: WideString; lverConfidencial: WordBool): OleVariant;
const
  Q_CAMPOS = 'select SUSCRIP.US_CODIGO US_SUSCRITO, MISREPOR.US_CODIGO US_FAVORITO, ' +
    'REPORTE.RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_FECHA,REPORTE.US_CODIGO,RE_ENTIDAD, ' + 'RE_REPORTE,RE_CANDADO,RE_CLASIFI, '
    + 'R_ENTIDAD.EN_NIVEL0, ' +
    'COALESCE ( EN_TITULO , ''<Tabla #'' + Cast((RE_ENTIDAD) as VarChar) +'': No Existe>'' ) RE_TABLA ' +
    'from REPORTE ';

  Q_GET_REPORTE = Q_CAMPOS + 'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD ' +
    'LEFT OUTER join SUSCRIP on SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and SUSCRIP.US_CODIGO = %d' +
    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d' + 'where %s %s ' +
    'order by RE_NOMBRE ';

  Q_GET_REPORTE_DERECHOS = Q_CAMPOS +
    'left outer join  R_ENT_ACC on CM_CODIGO = %s and GR_CODIGO = %d AND R_ENT_ACC.EN_CODIGO= REPORTE.RE_ENTIDAD ' +
    'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD ' +
    'LEFT OUTER join SUSCRIP on SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and SUSCRIP.US_CODIGO = %d ' +
    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d ' +
    'where (R_ENT_ACC.RE_DERECHO > 0) AND %s %s ' + 'order by RE_NOMBRE ';

  function GetFiltro: string;
  const
    K_FILTRO_FAVORITOS =
      'REPORTE.RE_CODIGO in( select MISREPOR.RE_CODIGO from MISREPOR where MISREPOR.US_CODIGO = %d ) %s';
    K_FILTRO_SUSCRIP = 'REPORTE.RE_CODIGO in( select SUSCRIP.RE_CODIGO from SUSCRIP where SUSCRIP.US_CODIGO = %d ) %s';
    K_CLASIFICACIONES = 'and RE_CLASIFI in ( %s )';
  var
    sFiltroClasificaciones: string;
  begin
    sFiltroClasificaciones := VACIO;
    if strLleno(sClasificaciones) then
      sFiltroClasificaciones := Format(K_CLASIFICACIONES, [sClasificaciones]);

    if (iClasifActivo = iFavoritos) then begin
      Result := Format(K_FILTRO_FAVORITOS, [oZetaProvider.UsuarioActivo, sFiltroClasificaciones]);
    end else if (iClasifActivo = iSuscripciones) then begin
      Result := Format(K_FILTRO_SUSCRIP, [oZetaProvider.UsuarioActivo, sFiltroClasificaciones]);
    end else begin
      Result := Format('RE_CLASIFI = %d ', [iClasifActivo]);
    end;
  end;

begin
     InitLog( Empresa, 'GetReportes');
     oZetaProvider.EmpresaActiva := Empresa;
     //Si el ususario es parte del grupo de Administradores
     if  oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION then
     begin
          if lVerConfidencial then
             Result := oZetaProvider.OpenSQL( Empresa,
                                              Format( Q_GET_REPORTE,
                                                      [ oZetaProvider.UsuarioActivo,
                                                        oZetaProvider.UsuarioActivo,
                                                        GetFiltro,'' ] ),
                                                      TRUE )
          else
              Result := oZetaProvider.OpenSQL( Empresa,
                                               Format( Q_GET_REPORTE,
                                                      [ oZetaProvider.UsuarioActivo,
                                                        oZetaProvider.UsuarioActivo,
                                                        GetFiltro,
                                                        '' ] ),
                                                      TRUE );
     end
     else
     begin
          if lVerConfidencial then
             Result := oZetaProvider.OpenSQL( Empresa,
                                              Format( Q_GET_REPORTE_DERECHOS,
                                                      [ Comillas(oZetaProvider.CodigoEmpresaActiva),
                                                        oZetaProvider.CodigoGrupo,
                                                        oZetaProvider.UsuarioActivo,
                                                        oZetaProvider.UsuarioActivo,
                                                        GetFiltro,'' ] ),
                                                      TRUE )
          else
              Result := oZetaProvider.OpenSQL( Empresa,
                                               Format( Q_GET_REPORTE_DERECHOS,
                                                      [ Comillas(oZetaProvider.CodigoEmpresaActiva),
                                                        oZetaProvider.CodigoGrupo,
                                                        oZetaProvider.UsuarioActivo,
                                                        oZetaProvider.UsuarioActivo,
                                                        GetFiltro,
                                                        '' ] ),
                                                      TRUE );
     end;
     EndLog; SetComplete;
end;

{***DevEx: Metodo agregado para traer todos los Reportes de la base de datos sin filtrar por las clasificaciones***}
{$IFDEF ANTES}
{***(@AM): Implementacion hasta VS 2015***}
function TdmServerReporteadorDD.GetReportesTodos(Empresa: OleVariant;const sClasificaciones: WideString;lVerConfidencial: WordBool): OleVariant;
const
     Q_CAMPOS = 'select SUSCRIP.US_CODIGO US_SUSCRITO, MISREPOR.US_CODIGO US_FAVORITO, '+
                'REPORTE.RE_CODIGO, CONVERT(varchar, REPORTE.RE_CODIGO) as RE_CODIGO_TEXT, RE_NOMBRE,RE_TIPO,RE_FECHA,REPORTE.US_CODIGO,RE_ENTIDAD, '+
                'RE_REPORTE,RE_CANDADO,RE_CLASIFI,'+
                'R_ENTIDAD.EN_NIVEL0, '+
                'COALESCE ( EN_TITULO , ''<Tabla #'' + Cast((RE_ENTIDAD) as VarChar) +'': No Existe>'' ) RE_TABLA '+
                'from REPORTE ';

     Q_GET_REPORTE= Q_CAMPOS +
                    'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD '+
                    'LEFT OUTER join SUSCRIP on SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and SUSCRIP.US_CODIGO = %d'+
                    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d'+
                    ' %s '+
                    'order by RE_NOMBRE ';
     Q_GET_REPORTE_DERECHOS = Q_CAMPOS +
                    'left outer join  R_ENT_ACC on CM_CODIGO = %s and GR_CODIGO = %d AND R_ENT_ACC.EN_CODIGO= REPORTE.RE_ENTIDAD '+
                    'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD '+
                    'LEFT OUTER join SUSCRIP on SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and SUSCRIP.US_CODIGO = %d '+
                    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d '+
                    'where (R_ENT_ACC.RE_DERECHO > 0) %s '+
                    'order by RE_NOMBRE ';
                    //'where (R_ENT_ACC.RE_DERECHO > 0) AND %s ' no hay filtro acomodar el AND dentro de la %s cuando se decida si se aplica filtro o no
     
function GetClasificaciones: string;
   const
        K_CLASIFICACIONES = 'where RE_CLASIFI in ( %s )';

   var
      sFiltroClasificaciones: string;
   begin
        sFiltroClasificaciones := VACIO;
        if strLleno ( sClasificaciones ) then
           sFiltroClasificaciones := Format( K_CLASIFICACIONES, [ sClasificaciones ] );
        Result := sFiltroClasificaciones;
   end;
function GetClasificaciones_sin_Filtro: string;
   const
        K_CLASIFICACIONES = 'and RE_CLASIFI in ( %s )';

   var
      sFiltroClasificaciones: string;
   begin
        sFiltroClasificaciones := VACIO;
        if strLleno ( sClasificaciones ) then
           sFiltroClasificaciones := Format( K_CLASIFICACIONES, [ sClasificaciones ] );
        Result := sFiltroClasificaciones;
   end;
begin
     InitLog( Empresa, 'GetReportes');
     oZetaProvider.EmpresaActiva := Empresa;
     if  oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION then
     begin
         if lVerConfidencial then  //En realidad validar lVerConfidencial se necesita? El query que se ejecuta es igual
         Result := oZetaProvider.OpenSQL( Empresa,
                                         Format( Q_GET_REPORTE,
                                                 [oZetaProvider.UsuarioActivo,
                                                  oZetaProvider.UsuarioActivo,
                                                  GetClasificaciones] ),
                                                 TRUE )
         else
         Result := oZetaProvider.OpenSQL( Empresa,
                                          Format( Q_GET_REPORTE,
                                                 [ oZetaProvider.UsuarioActivo,
                                                   oZetaProvider.UsuarioActivo,
                                                   GetClasificaciones,
                                                   ''] ),
                                                 TRUE );
     end
     else
     begin
          if lVerConfidencial then
             Result := oZetaProvider.OpenSQL( Empresa,
                                              Format( Q_GET_REPORTE_DERECHOS,
                                                      [ Comillas(oZetaProvider.CodigoEmpresaActiva),
                                                        oZetaProvider.CodigoGrupo,
                                                        oZetaProvider.UsuarioActivo,
                                                        oZetaProvider.UsuarioActivo,
                                                        GetClasificaciones_sin_filtro ] ),
                                                      TRUE )
          else
              Result := oZetaProvider.OpenSQL( Empresa,
                                               Format( Q_GET_REPORTE_DERECHOS,
                                                      [ Comillas(oZetaProvider.CodigoEmpresaActiva),
                                                        oZetaProvider.CodigoGrupo,
                                                        oZetaProvider.UsuarioActivo,
                                                        oZetaProvider.UsuarioActivo,
                                                        GetClasificaciones_sin_filtro ] ),
                                                      TRUE );
     end;
     EndLog; SetComplete;
end;
{$ELSE}
{***(@AM): Implementacion apartir de VS 2016***}
function TdmServerReporteadorDD.GetReportesTodos(Empresa: OleVariant;const sClasificaciones: WideString;lVerConfidencial: WordBool): OleVariant;
const

     P_CODIGO = 5;


     Q_CAMPOS = 'select V_SUSCRIP.US_CODIGO US_SUSCRITO, MISREPOR.US_CODIGO US_FAVORITO, '+
                'REPORTE.RE_CODIGO, CONVERT(varchar, REPORTE.RE_CODIGO) as RE_CODIGO_TEXT, RE_NOMBRE,RE_TIPO,RE_FECHA,REPORTE.US_CODIGO,RE_ENTIDAD, '+
                'RE_REPORTE,RE_CANDADO,RE_CLASIFI,'+
                'R_ENTIDAD.EN_NIVEL0, '+
                'COALESCE ( EN_TITULO , ''<Tabla #'' + Cast((RE_ENTIDAD) as VarChar) +'': No Existe>'' ) RE_TABLA %s '+
                'from REPORTE ';

     Q_GET_REPORTE= //Q_CAMPOS +
                    '%s left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD '+
                    'LEFT OUTER join V_SUSCRIP on V_SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and V_SUSCRIP.US_CODIGO = %d' +
                    'and V_SUSCRIP.CM_CODIGO = ''%s'' ' +
                    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d '+
                    ' %s '+
                    'order by RE_NOMBRE ';
     Q_GET_REPORTE_DERECHOS = //Q_CAMPOS +
                    '%s left outer join  R_ENT_ACC on CM_CODIGO = %s and GR_CODIGO = %d AND R_ENT_ACC.EN_CODIGO= REPORTE.RE_ENTIDAD '+
                    'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD '+
                    'LEFT OUTER join V_SUSCRIP on V_SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and V_SUSCRIP.US_CODIGO = %d' +
                    'and V_SUSCRIP.CM_CODIGO = ''%s'' ' +
                    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d '+
                    '%s where (R_ENT_ACC.RE_DERECHO > 0) %s '+
                    'order by RE_NOMBRE ';
                    //'where (R_ENT_ACC.RE_DERECHO > 0) AND %s ' no hay filtro acomodar el AND dentro de la %s cuando se decida si se aplica filtro o no
     Q_CAMPO_DESC_USUARIO = ', V_USUARIO.US_NOMBRE US_DESCRIP_SERVIDOR ';
     Q_LEFT_JOIN_VISTA_USUARIO = ' LEFT OUTER join V_USUARIO on V_USUARIO.US_CODIGO = REPORTE.US_CODIGO ';
var
  sCampos: String;
function GetClasificaciones: string;
   const
        K_CLASIFICACIONES = 'where RE_CLASIFI in ( %s )';

   var
      sFiltroClasificaciones: string;
   begin
        sFiltroClasificaciones := VACIO;
        if strLleno ( sClasificaciones ) then
           sFiltroClasificaciones := Format( K_CLASIFICACIONES, [ sClasificaciones ] );
        Result := sFiltroClasificaciones;
   end;
function GetClasificaciones_sin_Filtro: string;
   const
        K_CLASIFICACIONES = 'and RE_CLASIFI in ( %s )';

   var
      sFiltroClasificaciones: string;
   begin
        sFiltroClasificaciones := VACIO;
        if strLleno ( sClasificaciones ) then
           sFiltroClasificaciones := Format( K_CLASIFICACIONES, [ sClasificaciones ] );
        Result := sFiltroClasificaciones;
   end;
begin
     InitLog( Empresa, 'GetReportes');
     oZetaProvider.EmpresaActiva := Empresa;
     if oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION then
     begin
         try
             sCampos := Format(Q_CAMPOS,[Q_CAMPO_DESC_USUARIO]);
             Result := oZetaProvider.OpenSQL( Empresa,
                                               Format( Q_GET_REPORTE,
                                                       [sCampos,
                                                        oZetaProvider.UsuarioActivo,
                                                        Empresa[P_CODIGO],
                                                        oZetaProvider.UsuarioActivo,
                                                        Q_LEFT_JOIN_VISTA_USUARIO + GetClasificaciones] ),
                                                       TRUE );
         except
             {(***@am): Si la vista para traer la desc. del usuario que edita el reporte no funciona, se trae el query sin este dato.
             En el cliente aparece el codigo en lugar de la descripcion.***}
             sCampos := Format(Q_CAMPOS,['']);
             Result := oZetaProvider.OpenSQL( Empresa,
                                               Format( Q_GET_REPORTE,
                                                       [sCampos,
                                                        oZetaProvider.UsuarioActivo,
                                                        Empresa[P_CODIGO],
                                                        oZetaProvider.UsuarioActivo,
                                                        GetClasificaciones] ),
                                                       TRUE );
         end;
     end
     else
     begin
          try
              sCampos := Format(Q_CAMPOS,[Q_CAMPO_DESC_USUARIO]);
              Result := oZetaProvider.OpenSQL( Empresa,
                                                  Format( Q_GET_REPORTE_DERECHOS,
                                                          [ sCampos,
                                                            Comillas(oZetaProvider.CodigoEmpresaActiva),
                                                            oZetaProvider.CodigoGrupo,
                                                            oZetaProvider.UsuarioActivo,
                                                            Empresa[P_CODIGO],
                                                            oZetaProvider.UsuarioActivo,
                                                            Q_LEFT_JOIN_VISTA_USUARIO,
                                                            GetClasificaciones_sin_filtro ] ),
                                                          TRUE );
          except
              {(***@am): Si la vista para traer la desc. del usuario que edita el reporte no funciona, se trae el query sin este dato.
              En el cliente aparece el codigo en lugar de la descripcion.***}
              sCampos := Format(Q_CAMPOS,['']);
              Result := oZetaProvider.OpenSQL( Empresa,
                                                  Format( Q_GET_REPORTE_DERECHOS,
                                                          [ sCampos,
                                                            Comillas(oZetaProvider.CodigoEmpresaActiva),
                                                            oZetaProvider.CodigoGrupo,
                                                            oZetaProvider.UsuarioActivo,
                                                            Empresa[P_CODIGO],
                                                            oZetaProvider.UsuarioActivo,
                                                            '',
                                                            GetClasificaciones_sin_filtro ] ),
                                                          TRUE );
          end;
     end;
     EndLog; SetComplete;
end;
{$ENDIF}
{$ELSE}

function TdmServerReporteadorDD.GetReportes(Empresa: OleVariant; iClasifActivo, iFavoritos: Integer;
  iSuscripciones: Integer; const sClasificaciones: WideString; lverConfidencial: WordBool): OleVariant;
const

  Q_GET_REPORTE = 'select ' +
    'SUSCRIP.US_CODIGO US_SUSCRITO, MISREPOR.US_CODIGO US_FAVORITO, REPORTE.RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_FECHA, ' +
    'REPORTE.US_CODIGO,RE_ENTIDAD,RE_REPORTE,RE_CANDADO,RE_CLASIFI ' + 'from REPORTE ' +
    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD ' +
    'LEFT OUTER join SUSCRIP on SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and SUSCRIP.US_CODIGO = %d' +
    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d' + 'where %s %s ' +
    'order by RE_NOMBRE ';

  function GetFiltro: string;
  const
    K_FILTRO_FAVORITOS =
      'REPORTE.RE_CODIGO in( select MISREPOR.RE_CODIGO from MISREPOR where MISREPOR.US_CODIGO = %d ) %s';
    K_FILTRO_SUSCRIP = 'REPORTE.RE_CODIGO in( select SUSCRIP.RE_CODIGO from SUSCRIP where SUSCRIP.US_CODIGO = %d ) %s';
    K_CLASIFICACIONES = 'and RE_CLASIFI in ( %s )';
  var
    sFiltroClasificaciones: string;
  begin
    sFiltroClasificaciones := VACIO;
    if strLleno(sClasificaciones) then
      sFiltroClasificaciones := Format(K_CLASIFICACIONES, [sClasificaciones]);

    if (iClasifActivo = iFavoritos) then begin
      Result := Format(K_FILTRO_FAVORITOS, [oZetaProvider.UsuarioActivo, sFiltroClasificaciones]);
    end else if (iClasifActivo = iSuscripciones) then begin
      Result := Format(K_FILTRO_SUSCRIP, [oZetaProvider.UsuarioActivo, sFiltroClasificaciones]);
    end else begin
      Result := Format('RE_CLASIFI = %d ', [iClasifActivo]);
    end;
  end;

begin
  InitLog(Empresa, 'GetReportes');
  oZetaProvider.EmpresaActiva := Empresa;
  if lverConfidencial then
    Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_REPORTE, [oZetaProvider.UsuarioActivo,
          oZetaProvider.UsuarioActivo, GetFiltro, '']), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_REPORTE, [oZetaProvider.UsuarioActivo,
          oZetaProvider.UsuarioActivo, GetFiltro, K_CONFIDENCIAL]), TRUE);
  EndLog;
  SetComplete;
end;

{***DevEx: Metodo agregado para traer todos los Reportes de la base de datos sin filtrar por las clasificaciones***}
function TdmServerReporteadorDD.GetReportesTodos(Empresa: OleVariant; const sClasificaciones: WideString; lVerConfidencial: WordBool): OleVariant;

const
     Q_GET_REPORTE='select '+
                   'SUSCRIP.US_CODIGO US_SUSCRITO, MISREPOR.US_CODIGO US_FAVORITO, REPORTE.RE_CODIGO, CONVERT(varchar, REPORTE.RE_CODIGO) as RE_CODIGO_TEXT, RE_NOMBRE,RE_TIPO,RE_FECHA, ' +
                   'REPORTE.US_CODIGO,RE_ENTIDAD,RE_REPORTE,RE_CANDADO,RE_CLASIFI ' +
                    'from REPORTE '+
                    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD '+
                    'LEFT OUTER join SUSCRIP on SUSCRIP.RE_CODIGO = REPORTE.RE_CODIGO and SUSCRIP.US_CODIGO = %d'+
                    'LEFT OUTER join MISREPOR on MISREPOR.RE_CODIGO = REPORTE.RE_CODIGO and MISREPOR.US_CODIGO = %d'+
                    ' %s '+
                    'ORDER BY RE_NOMBRE';
     K_FILTRO_CONFIDENCIAL = ' WHERE DICCION.DI_CONFI =''N'' ';
   function GetFiltro: string;
   const
        K_CLASIFICACIONES = 'RE_CLASIFI in ( %s )';
   var
      sFiltroClasificaciones: string;
   begin
        sFiltroClasificaciones := VACIO;
        if strLleno ( sClasificaciones ) then
           result:= Format( K_CLASIFICACIONES, [ sClasificaciones ] );
        end;
   end;
begin
InitLog( Empresa, 'GetReportes');
     oZetaProvider.EmpresaActiva := Empresa;
     if lVerConfidencial then
        Result := oZetaProvider.OpenSQL( Empresa,
                                         Format( Q_GET_REPORTE,
                                                 [oZetaProvider.UsuarioActivo,
                                                  oZetaProvider.UsuarioActivo,
                                                  GetFiltro,
                                                  ''] ),
                                                 TRUE )
     else
         Result := oZetaProvider.OpenSQL( Empresa,
                                          Format( Q_GET_REPORTE,
                                                 [ oZetaProvider.UsuarioActivo,
                                                   oZetaProvider.UsuarioActivo,
                                                   GetFiltro,
                                                   K_FILTRO_CONFIDENCIAL ] ),
                                                 TRUE );
     EndLog; SetComplete;
end;
{$ENDIF}

function TdmServerReporteadorDD.GetEditReportes(Empresa: OleVariant; iReporte: Integer; var CampoRep: OleVariant): OleVariant;
const
  Q_EDIT_REPORTE = 'select REPORTE.RE_CODIGO, REPORTE.RE_NOMBRE, REPORTE.RE_TIPO, ' +
    'REPORTE.RE_TITULO, REPORTE.RE_ENTIDAD, REPORTE.RE_SOLOT, REPORTE.RE_FECHA, ' +
    'REPORTE.RE_GENERAL, REPORTE.RE_REPORTE, REPORTE.RE_CFECHA, REPORTE.RE_IFECHA, ' +
    'REPORTE.RE_HOJA, REPORTE.RE_ALTO, REPORTE.RE_ANCHO, REPORTE.RE_PRINTER, ' +
    'REPORTE.RE_COPIAS, REPORTE.RE_PFILE, REPORTE.RE_ARCHIVO, REPORTE.RE_VERTICA, ' +
    'REPORTE.US_CODIGO, REPORTE.RE_FILTRO, REPORTE.RE_COLNUM, REPORTE.RE_COLESPA, ' +
    'REPORTE.RE_RENESPA, REPORTE.RE_MAR_SUP, REPORTE.RE_MAR_IZQ, REPORTE.RE_MAR_DER, ' +
    'REPORTE.RE_MAR_INF, REPORTE.RE_NIVEL, REPORTE.RE_FONTNAM, REPORTE.RE_FONTSIZ, ' +
    'REPORTE.QU_CODIGO, REPORTE.RE_CLASIFI, REPORTE.RE_CANDADO, ' +
    'EN_TITULO RE_TABLA, R_ENTIDAD.EN_NIVEL0, R_ENTIDAD.EN_TABLA, REPORTE.US_CODIGO ' + 'from REPORTE ' +
    'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD ' + 'where REPORTE.RE_CODIGO = %d ' +
    'order by RE_NOMBRE ';
  function GetTablaInfo: OleVariant;
  begin
    with oZetaProvider.TablaInfo do
      Filtro := Format('RE_CODIGO=%d', [iReporte]);
    Result := oZetaProvider.GetTabla(Empresa);
  end;

begin
  InitLog(Empresa, 'GetEditReportes');
  {$IFDEF RDD}
  Result := oZetaProvider.OpenSQL(Empresa, Format(Q_EDIT_REPORTE, [iReporte]), TRUE);
  {$ELSE}
  SetTablaInfoReporte;
  Result := GetTablaInfo;
  {$ENDIF}
  SetTablaInfoCampoRep;
  CampoRep := GetTablaInfo;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetLookUpReportes(Empresa: OleVariant; lverConfidencial: WordBool): OleVariant;
{$IFDEF FALSE}
const
  Q_GET_REPORTE = 'select RE_CODIGO,RE_NOMBRE,RE_TIPO, ' + 'RE_FECHA,REPORTE.US_CODIGO,RE_ENTIDAD, ' +
    'RE_FILTRO, QU_FILTRO, DI_CONFI ' + 'from REPORTE ' +
    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD ' +
    'left outer join QUERYS on QUERYS.QU_CODIGO = REPORTE.QU_CODIGO ' + '%s ' + 'order by RE_NOMBRE ';
{$ENDIF}
const
  Q_GET_REPORTE = 'select RE_CODIGO,RE_NOMBRE,RE_TIPO, ' + 'RE_FECHA,REPORTE.US_CODIGO,RE_ENTIDAD, ' +
    'RE_FILTRO, QU_FILTRO ' + 'from REPORTE ' + 'left outer join QUERYS on QUERYS.QU_CODIGO = REPORTE.QU_CODIGO ' +
    'order by RE_NOMBRE ';
  Q_GET_REPORTE_DERECHOS = 'select RE_CODIGO,RE_NOMBRE,RE_TIPO, ' + 'RE_FECHA,REPORTE.US_CODIGO,RE_ENTIDAD, ' +
    'RE_FILTRO, QU_FILTRO ' + 'from REPORTE ' +
    'left outer join  R_ENT_ACC on CM_CODIGO = %s and GR_CODIGO = %d AND R_ENT_ACC.EN_CODIGO= REPORTE.RE_ENTIDAD ' +
    'left outer join QUERYS on QUERYS.QU_CODIGO = REPORTE.QU_CODIGO ' + 'where (R_ENT_ACC.RE_DERECHO > 0) ' +
    'order by RE_NOMBRE ';
begin
  InitLog(Empresa, 'GetLookUpReportes');
  oZetaProvider.EmpresaActiva := Empresa;
  if oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION then begin
    Result := oZetaProvider.OpenSQL(Empresa, Q_GET_REPORTE, TRUE)
  end else begin
    Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_REPORTE_DERECHOS,
        [EntreComillas(oZetaProvider.CodigoEmpresaActiva), oZetaProvider.CodigoGrupo]), TRUE);
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.BorraReporte(Empresa: OleVariant; iReporte: Integer): OleVariant;
const
  K_DELETE = 'DELETE FROM REPORTE WHERE RE_CODIGO = %d';
begin
  InitLog(Empresa, 'BorraReporte');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    try
      EmpiezaTransaccion;
      ExecSQL(Empresa, Format(K_DELETE, [iReporte])); // PENDIENTE
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do begin
        TerminaTransaccion(FALSE);
      end;
    end;
  end;
  EndLog;
  SetComplete;
end;

{$IFDEF RDD}

function TdmServerReporteadorDD.GetEscogeReporte(Empresa: OleVariant; Entidad, Tipo: Integer;
  lverConfidencial: WordBool): OleVariant;
const
  Q_CAMPOS = 'select RE_CODIGO,RE_NOMBRE,RE_ENTIDAD, RE_TIPO,RE_FECHA,REPORTE.US_CODIGO ' + 'from REPORTE ';

  K_ESCOGE_REPORTE = Q_CAMPOS + 'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD ' +
    'where RE_ENTIDAD = %d ' + 'AND RE_TIPO = %d ' + 'order by RE_NOMBRE ';


     K_ESCOGE_REPORTE_DERECHOS = Q_CAMPOS +
                    'left outer join  R_ENT_ACC on CM_CODIGO = %s and GR_CODIGO = %d AND R_ENT_ACC.EN_CODIGO= REPORTE.RE_ENTIDAD '+
                    'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD '+
                    'where (R_ENT_ACC.RE_DERECHO > 0) '+
                    'AND RE_ENTIDAD = %d ' +
                    'AND RE_TIPO = %d '+
                    'order by RE_NOMBRE ';

begin
  InitLog(Empresa, 'GetEscogeReporte');
  oZetaProvider.EmpresaActiva := Empresa;
  if oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION then
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE, [Entidad, Tipo]), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE_DERECHOS,
        [Comillas(oZetaProvider.CodigoEmpresaActiva), oZetaProvider.CodigoGrupo,
          // oZetaProvider.UsuarioActivo,
          // oZetaProvider.UsuarioActivo,
          Entidad, Tipo]), TRUE);
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetEscogeReportes(Empresa: OleVariant; const Entidades, Tipos: WideString;
  lverConfidencial: WordBool): OleVariant;
const
  Q_CAMPOS = 'select RE_CODIGO,RE_NOMBRE,RE_ENTIDAD, RE_TIPO,RE_FECHA,US_CODIGO ' + 'from REPORTE ';

  K_ESCOGE_REPORTE = Q_CAMPOS + 'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD ' +
    'where RE_ENTIDAD = %d ' + 'AND RE_TIPO in ( %s ) ' + 'order by RE_NOMBRE ';

  K_ESCOGE_REPORTE_DERECHOS = Q_CAMPOS +
    'left outer join  R_ENT_ACC on CM_CODIGO = %s and GR_CODIGO = %d AND R_ENT_ACC.EN_CODIGO= REPORTE.RE_ENTIDAD ' +
    'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD ' + 'where (R_ENT_ACC.RE_DERECHO > 0) ' +
    'AND RE_ENTIDAD = %d ' + 'AND RE_TIPO in ( %s ) ' + 'order by RE_NOMBRE ';
begin
  InitLog(Empresa, 'GetEscogeReportes');
  oZetaProvider.EmpresaActiva := Empresa;
  if oZetaProvider.CodigoGrupo = D_GRUPO_SIN_RESTRICCION then
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE, [Entidades, Tipos]), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE_DERECHOS, [Entidades, Tipos]), TRUE);
  EndLog;
  SetComplete;

end;
{$ELSE}

function TdmServerReporteadorDD.GetEscogeReportes(Empresa: OleVariant; const Entidades, Tipos: WideString;
  lverConfidencial: WordBool): OleVariant;
const
  K_ESCOGE_REPORTE = 'select RE_CODIGO,RE_NOMBRE,RE_ENTIDAD, RE_TIPO,RE_FECHA,US_CODIGO ' + 'from REPORTE ' +
    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD ' +
    'where RE_ENTIDAD in (%s) ' + 'and RE_TIPO in (%s)' + 'order by RE_NOMBRE';
begin
  InitLog(Empresa, 'GetEscogeReportes');
  if lverConfidencial then
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE, [Entidades, Tipos, '']), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE,
        [Entidades, Tipos, 'and DICCION.DI_CONFI =''N'' ']), TRUE);
  EndLog;
  SetComplete;

end;

function TdmServerReporteadorDD.GetEscogeReporte(Empresa: OleVariant; Entidad, Tipo: Integer;
  lverConfidencial: WordBool): OleVariant;
const
  K_ESCOGE_REPORTE = 'select RE_CODIGO,RE_NOMBRE,RE_ENTIDAD, RE_TIPO,RE_FECHA,US_CODIGO ' + 'from REPORTE ' +
    'left outer join DICCION on DICCION.DI_CLASIFI = -1 AND DICCION.DI_CALC = REPORTE.RE_ENTIDAD ' +
    'where RE_ENTIDAD = %d ' + 'and RE_TIPO = %d %s' + 'order by RE_NOMBRE';
begin
  InitLog(Empresa, 'GetEscogeReporte');
  if lverConfidencial then
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE, [Entidad, Tipo, '']), TRUE)
  else
    Result := oZetaProvider.OpenSQL(Empresa, Format(K_ESCOGE_REPORTE,
        [Entidad, Tipo, 'and DICCION.DI_CONFI =''N'' ']), TRUE);
  EndLog;
  SetComplete;
end;
{$ENDIF}

function TdmServerReporteadorDD.CampoRepFiltros(Empresa: OleVariant; Reporte: Integer): OleVariant;
const
  Q_FILTROS_DETALLE = 'select * FROM CAMPOREP ' + 'WHERE RE_CODIGO = %d AND CR_TIPO =5 ';
begin
  InitLog(Empresa, 'CampoRepFiltros');
  Result := oZetaProvider.OpenSQL(Empresa, Format(Q_FILTROS_DETALLE, [Reporte]), TRUE);
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GrabaReporte(Empresa, oDelta, oCampoRep: OleVariant; out ErrorCount: Integer;
  var iReporte: Integer): OleVariant;
begin
  InitLog(Empresa, 'GrabaReporte');
  FReporte := iReporte;
  with oZetaProvider do begin
    SetTablaInfoReporte;
    EmpresaActiva := Empresa;
    TablaInfo.BeforeUpdateRecord := BeforeUpdateReporte;
    Result := GrabaTabla(Empresa, oDelta, ErrorCount);
    if (ErrorCount = 0) then begin
      SetTablaInfoCampoRep;
      if FAlta then
        TablaInfo.BeforeUpdateRecord := BeforeUpdateCampoRep
      else
        TablaInfo.BeforeUpdateRecord := NIL;
      Result := GrabaTabla(Empresa, oCampoRep, ErrorCount);
      iReporte := FReporte;
    end;
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GrabaPoliza(Empresa, Delta, CampoRep: OleVariant; out ErrorCount: Integer;
  var iReporte: Integer): OleVariant;
const
  K_DELETE = 'DELETE FROM CAMPOREP WHERE RE_CODIGO = %d AND CR_TIPO <> 0';
begin
  InitLog(Empresa, 'GrabaPoliza');
  FReporte := iReporte;
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    try
      EmpiezaTransaccion;
      if not varisNull(CampoRep) then
        ExecSQL(Empresa, Format(K_DELETE, [iReporte])); // PENDIENTE
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do
        TerminaTransaccion(FALSE);
    end;
    SetTablaInfoReporte;
    TablaInfo.BeforeUpdateRecord := BeforeReporteUpdatePoliza;

    Result := GrabaTabla(Empresa, Delta, ErrorCount);
    if (ErrorCount = 0) and not varisNull(CampoRep) then begin
      SetTablaInfoCampoRep;
      with TablaInfo do begin
        OnUpdateData := UpdateDataPoliza;
        BeforeUpdateRecord := BeforeUpdatePoliza;
      end;
      Result := GrabaTabla(Empresa, CampoRep, ErrorCount);
    end;
  end;
  iReporte := FReporte;
  EndLog;
  SetComplete;
end;

procedure TdmServerReporteadorDD.UpdateDataPoliza(Sender: TObject; DataSet: TzProviderClientDataSet);
const
  Q_POSICION = 'SELECT MAX( CR_POSICIO ) MAXIMO FROM CAMPOREP WHERE RE_CODIGO =%d AND CR_TIPO = 0';
var
  FDataSet: TZetaCursor;
begin
  with oZetaProvider do try
    // CV: CreateQuery--ok
    // Variable local
    FDataSet := CreateQuery(Format(Q_POSICION, [FReporte]));
    with FDataSet do begin
      Open;
      FPosicion := Fields[0].AsInteger + 1;
      Close;
    end;
  finally
    FreeAndNil(FDataSet);
  end;
end;

function TdmServerReporteadorDD.GetMaxReporte: Integer;
const
  K_MAX_REPORTE = 'SELECT MAX(RE_CODIGO) MAXIMO FROM REPORTE';
var
  FDataSet: TZetaCursor;
begin
  with oZetaProvider do try
    // CV: CreateQuery--ok
    FDataSet := CreateQuery(K_MAX_REPORTE);
    FDataSet.Open;
    Result := FDataSet.Fields[0].AsInteger + 1;
    FDataSet.Close;
  finally
    FreeAndNil(FDataSet);
  end;
end;

procedure TdmServerReporteadorDD.BeforeReporteUpdatePoliza(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  // Si es Alta, tengo que obtener el m�ximo Folio
  // Si es Edici�n, tengo que borrar todos los CampoRep anteriores
  if (UpdateKind = ukInsert) then begin
    FReporte := GetMaxReporte;
    DeltaDS.Edit;
    DeltaDS.FieldByName('RE_CODIGO').AsInteger := FReporte;
    DeltaDS.Post;
  end;
end;

procedure TdmServerReporteadorDD.BeforeUpdatePoliza(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  with DeltaDS do
    if (UpdateKind = ukInsert) then begin
      Edit;
      FieldByName('RE_CODIGO').AsInteger := FReporte;
      if (FieldByName('CR_TIPO').AsInteger = 0) and (FieldByName('CR_POSICIO').AsInteger = -1) then begin
        FieldByName('CR_POSICIO').AsInteger := FPosicion;
        Inc(FPosicion);
      end;
      Post;
    end;
end;

procedure TdmServerReporteadorDD.BeforeUpdateCampoRep(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  // En la Alta de Reportes, el RE_CODIGO viene VACIO del Cliente
  // Se asigna del FReporte que se obtuvo en la Alta del registro
  // padre : REPORTE, en su BeforeUpdateRecord
  with DeltaDS do begin
    Edit;
    FieldByName('RE_CODIGO').AsInteger := FReporte;
    Post;
  end;
end;

procedure TdmServerReporteadorDD.BeforeUpdateReporte(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
const
  K_DELETE = 'DELETE FROM CAMPOREP WHERE RE_CODIGO = %d';
  // K_MAX_REPORTE = 'SELECT MAX(RE_CODIGO) FROM REPORTE';
  // var FDataSet : TZetaCursor;
begin
  // Si es Alta, tengo que obtener el m�ximo Folio
  // Si es Edici�n, tengo que borrar todos los CampoRep anteriores
  with oZetaProvider do begin
    if (UpdateKind = ukInsert) then begin
      FReporte := GetMaxReporte;
      DeltaDS.Edit;
      DeltaDS.FieldByName('RE_CODIGO').AsInteger := FReporte;
      DeltaDS.Post;
      FAlta := TRUE;
    end else begin
      FAlta := FALSE;
      ExecSQL(EmpresaActiva, Format(K_DELETE, [FReporte]));
    end;
  end;
end;

function TdmServerReporteadorDD.GetSuscripcion(Empresa: OleVariant; iFrecuencia: Integer;
  const sUsuarios, sReportes: WideString; var Usuarios: OleVariant): OleVariant;
const
  Q_GET_SUSCRIP = 'select RE_CODIGO,US_CODIGO,SU_VACIO,SU_ENVIAR ' + 'from SUSCRIP %s ' +
    'order by RE_CODIGO, US_CODIGO ';
  Q_GET_USUARIO = 'select US_CODIGO, US_EMAIL, US_FORMATO, US_ACTIVO, US_NOMBRE, US_CORTO  ' + 'from USUARIO ' +
    'where US_EMAIL <> '''' %s';
var
  sFiltro: string;
begin
  InitLog(Empresa, 'GetSuscripcion');
  if iFrecuencia >= 0 then
    sFiltro := Format('SU_FRECUEN=%d', [iFrecuencia]);

  if strLleno(sUsuarios) then
    sFiltro := ConcatFiltros(sFiltro, '( US_CODIGO in ( ' + sUsuarios + ' ) )');
  if strLleno(sReportes) then
    sFiltro := ConcatFiltros(sFiltro, '( RE_CODIGO in ( ' + sReportes + ' ) )');
  if strLleno(sFiltro) then
    sFiltro := 'WHERE ' + sFiltro;

  Result := oZetaProvider.OpenSQL(Empresa, Format(Q_GET_SUSCRIP, [sFiltro]), TRUE);
  Usuarios := oZetaProvider.OpenSQL(oZetaProvider.Comparte,
    Format(Q_GET_USUARIO, [StrLlenoDef(sUsuarios, ' AND ( US_CODIGO in ( ' + sUsuarios + ' ) )')]), TRUE);
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.BorraFavoritos(Empresa: OleVariant; iReporte: Integer): OleVariant;
begin
  InitLog(Empresa, 'BorraFavoritos');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    try
      EmpiezaTransaccion;
      ExecSQL(Empresa, Format(K_DELETE_FAVORITOS, [iReporte, UsuarioActivo]));
      TerminaTransaccion(TRUE);
    except
      on Error: Exception do begin
        TerminaTransaccion(FALSE);
      end;
    end;
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.AgregaFavoritos(Empresa: OleVariant; iReporte: Integer): Integer;
const
  K_INSERT_FAVORITOS = 'insert into MISREPOR (US_CODIGO,RE_CODIGO) values(:US_CODIGO,:RE_CODIGO)';
var
  FCursor: TZetaCursor;
begin
  InitLog(Empresa, 'AgregaFavoritos');
  Result := 0;
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    try
      EmpiezaTransaccion;
      ExecSQL(Empresa, Format(K_DELETE_FAVORITOS, [iReporte, UsuarioActivo]));
      FCursor := CreateQuery(K_INSERT_FAVORITOS);
      try
        try
          ParamAsInteger(FCursor, 'US_CODIGO', UsuarioActivo);
          ParamAsInteger(FCursor, 'RE_CODIGO', iReporte);
          Result := Ejecuta(FCursor);
          TerminaTransaccion(TRUE);
        except
          on Error: Exception do begin
            TerminaTransaccion(FALSE);
          end;
        end;
      finally
        FreeAndNil(FCursor);
      end;
    except
      on Error: Exception do begin
        TerminaTransaccion(FALSE);
      end;
    end;
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetPlantilla(const Nombre: WideString): OleVariant;
var
  FStream: TMemoryStream;
  FBlob  : TBlobField;
begin
  with cdsTemporal do begin
    InitTempDataset;
    AddBlobField('CampoBlob', 0);
    CreateTempDataset;
    FBlob := TBlobField(FieldByName('CampoBlob'));
  end;

  FStream := TMemoryStream.Create;
  try
    if FileExists(Nombre) then begin
      FStream.LoadFromFile(Nombre);
      FBlob.DataSet.Edit;
      FBlob.LoadFromStream(FStream);
      FBlob.DataSet.Post;
      Result := TClientDataset(FBlob.DataSet).Data;
    end else
      Result := Null;
  finally
    FreeAndNil(FStream)
  end;
end;
{ -------------------------------------------------------------------------- }

{ -------------------------------------------------------------------------- }
{ -------------------------  DServerReporteador  --------------------------- }
function TdmServerReporteadorDD.GeneraSQL(Empresa: OleVariant; var AgenteSQL, Parametros: OleVariant;
  out Error: WideString): OleVariant;
var
  Reporte: TdmServerReportes;
begin
  InitLog(Empresa, 'GeneraSQL');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    AsignaParamList(Parametros);
  end;
  InitCreator;
  Reporte := TdmServerReportes.Create(oZetaProvider, oZetaCreator);
  try
    Result := Reporte.GeneraSQL(Empresa, AgenteSQL, Parametros, Error);
  finally
    oZetaProvider.VerConfidencial := TRUE;
    FreeAndNil(Reporte);
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
  out Error: WideString): WordBool;
var
  Reporte: TdmServerReportes;
begin
  InitLog(Empresa, 'EvaluaParam');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    AsignaParamList(Parametros);
  end;

  InitCreator;
  Reporte := TdmServerReportes.Create(oZetaProvider, oZetaCreator);
  try
    Result := Reporte.EvaluaParam(Empresa, AgenteSQL, Parametros, Error);
  finally
    FreeAndNil(Reporte);
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetFoto(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString): OleVariant;
const
  K_SQL = 'SELECT IM_BLOB FROM IMAGEN WHERE CB_CODIGO = %d AND IM_TIPO = ''%s''';
var
  FBlob  : TBlobField;
  FImagen: TZetaCursor;
begin
  oZetaProvider.EmpresaActiva := Empresa;
  FImagen := oZetaProvider.CreateQuery;
  try
    oZetaProvider.AbreQueryScript(FImagen, Format(K_SQL, [Empleado, Tipo]));
    // Result := FImagen.Fields[0].AsVariant;
    with cdsTemporal do begin
      InitTempDataset;
      AddBlobField('CampoBlob', 0);
      CreateTempDataset;
      FBlob := TBlobField(FieldByName('CampoBlob'));
    end;
    FBlob.DataSet.Edit;
    if ( FImagen.Fields[0].Value <> Null  ) then
        FBlob.Value := FImagen.Fields[0].Value;


    FBlob.DataSet.Post;
    Result := TClientDataset(FBlob.DataSet).Data;
  finally
    FreeAndNil(FImagen);
  end;
end;

// (JB) Anexar al expediente del trabajador documentos CR1880 T1107
function TdmServerReporteadorDD.GetDocumento(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString;
  var Extension: WideString): OleVariant;
const
  K_SQL = 'SELECT DO_BLOB, DO_EXT FROM DOCUMENTO WHERE CB_CODIGO = %d AND DO_TIPO = ''%s''';
var
  FBlob  : TBlobField;
  FImagen: TZetaCursor;
begin
  oZetaProvider.EmpresaActiva := Empresa;
  FImagen := oZetaProvider.CreateQuery;
  try
    oZetaProvider.AbreQueryScript(FImagen, Format(K_SQL, [Empleado, Tipo]));
    with cdsTemporal do begin
      InitTempDataset;
      AddBlobField('DO_BLOB', 0);
      CreateTempDataset;
      FBlob := TBlobField(FieldByName('DO_BLOB'));
    end;
    Extension := FImagen.Fields[1].AsString;
    FBlob.DataSet.Edit;
    if ( FImagen.Fields[0].Value <> Null  ) then
        FBlob.Value := FImagen.Fields[0].Value;
    FBlob.DataSet.Post;
    Result := TClientDataset(FBlob.DataSet).Data;
  finally
    FreeAndNil(FImagen);
  end;
end;

function TdmServerReporteadorDD.DirectorioPlantillas(Empresa: OleVariant): WideString;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    InitGlobales;
    Result := GetGlobalString(K_GLOBAL_DIR_PLANT);
  end;
end;

function TdmServerReporteadorDD.GetPrimaryKey(Empresa: OleVariant; const TableName: WideString): WideString;
var
  oLista: TStrings;
begin
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    FQuery := CreateQuery(Format(GetSQL(Q_GET_PRIMARY_KEY), [EntreComillas(TableName)]));
  end;
  FQuery.Active := TRUE;

  oLista := TStringList.Create;
  try
    with FQuery do
      while not EOF do begin
        oLista.Add(Fields[0].AsString);
        Next;
      end;
    Result := oLista.CommaText;
  finally
    oLista.Free;
  end;

end;

procedure TdmServerReporteadorDD.GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
  eClase  : eClaseBitacora;
  sTitulo : string;
  sDetalle: string;

  procedure DatosBitacora(eClaseCatalogo: eClaseBitacora; sDescripcion, sKeyField: string);
  var
    oField: TField;
  begin
    eClase := eClaseCatalogo;
    sTitulo := sDescripcion;

    if strLleno(sKeyField) then begin
      oField := DeltaDS.FieldByName(sKeyField);

      if oField is TNumericField then
        sTitulo := sTitulo + ' ' + InTToStr(ZetaServerTools.CampoOldAsVar(oField))
      else
        sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar(oField);
    end;
  end;

  function GetEntidad: string;
  var
    Nombre: string;
    Codigo: Integer;
  begin
    InitCreator;
    oZetaCreator.PreparaEntidades;
    Codigo := ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('EN_CODIGO'));
    Nombre := oZetaCreator.dmEntidadesTress.NombreEntidad(Codigo);
    Result := InTToStr(Codigo) + '=' + Nombre;
  end;

begin
  if UpdateKind in [ukModify, ukDelete] then begin

    eClase := clbNinguno;

    case FClaseDiccionario of
      eClasifi:
        DatosBitacora(clbRepCfgClasificaciones, 'Clasificaci�n:', 'RC_CODIGO');

      eListasFijas:
        DatosBitacora(clbRepCfgListaValores, 'Lista Valor:', 'LV_CODIGO');

      eListasFijasValor:
        DatosBitacora(clbRepCfgValores,
          'Lista Fija:' + InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('LV_CODIGO'))) + ' Valor: ' +
            InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('VL_CODIGO'))), VACIO);

      eModulos:
        DatosBitacora(clbRepCfgModulos, 'M�dulo:', 'MO_CODIGO');

      eEntidades:
        DatosBitacora(clbRepCfgEntidades, 'Entidad:' + GetEntidad, VACIO);

      eCamposPorEntidad: begin
          DatosBitacora(clbRepCfgCampos, 'Entidad:' + GetEntidad, VACIO);
          sDetalle := ' Campo: ' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('AT_CAMPO')) + CR_LF;
        end;
      eCamposDefault: begin
          DatosBitacora(clbRepCfgCamposDefault, 'Entidad:' + GetEntidad, VACIO);
          sDetalle := ' Campo:' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('AT_CAMPO')) + CR_LF;
        end;
      eOrdenDefault: begin
          DatosBitacora(clbRepCfgOrden, 'Entidad:' + GetEntidad, VACIO);
          sDetalle := ' Campo:' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('AT_CAMPO')) + CR_LF;
        end;
      eFiltrosDefault: begin
          DatosBitacora(clbRepCfgFiltros, 'Entidad:' + GetEntidad, VACIO);
          sDetalle := ' Campo:' + ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('AT_CAMPO')) + CR_LF;
        end;
      eRelacionesPorEntidad: begin
          DatosBitacora(clbRepCfgRelaciones, 'Entidad: ' + GetEntidad, VACIO);
          sDetalle := ' Tabla:' + InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RL_ENTIDAD'))) + CR_LF;
          sDetalle := sDetalle + ' Campos de la relaci�n:' + ZetaServerTools.CampoOldAsVar
            (DeltaDS.FieldByName('RL_CAMPOS')) + CR_LF;
        end;
      eClasifiPorEntidad: begin
          DatosBitacora(clbRepCfgEntidadClasificacioes,
            'Clasificaci�n: ' + InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('RC_CODIGO'))), VACIO);
          sDetalle := ' Entidad:' + GetEntidad + CR_LF;
        end;
      eModulosPorEntidad: begin
          DatosBitacora(clbRepCfgEntidadModulos,
            'M�dulo: ' + InTToStr(ZetaServerTools.CampoOldAsVar(DeltaDS.FieldByName('MO_CODIGO'))), VACIO);
          sDetalle := ' Entidad:' + GetEntidad;
        end;

      else
        raise Exception.Create
          ('No ha sido Integrado el Cat�logo al Case de <DServerReporteadorDD.GrabaCambiosBitacora>')
    end;

    with oZetaProvider do
      if UpdateKind = ukModify then
        CambioCatalogo(sTitulo, eClase, DeltaDS, 0, sDetalle)
      else
        BorraCatalogo(sTitulo, eClase, DeltaDS, 0, sDetalle);
  end;
end;

procedure TdmServerReporteadorDD.AfterUpdateDiccionario(Sender: TObject; SourceDS: TDataSet;
  DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
begin
  GrabaCambiosBitacora(Sender, SourceDS, DeltaDS, UpdateKind);
end;

function TdmServerReporteadorDD.ImportarDiccionario(Empresa, Parametros: OleVariant): OleVariant;
begin
  InitLog(Empresa, 'ImportarDiccionario');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    AsignaParamList(Parametros);
  end;

  CargaParametrosImportarDiccionario;
  Result := ImportarDiccionarioXML;
  EndLog;
  SetComplete;
end;

procedure TdmServerReporteadorDD.cdsGrabaDatasetReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
  i, iTabla                   : Integer;
  sMensaje1, sMensaje2, sError: string;
begin
  Action := raSkip;
  with E do begin
    { PK_VIOLATION }
    if PK_VIOLATION(E) then // if ( ErrorCode = 3604 ) or ( Pos( 'PRIMARY', Message ) > 0 ) then
      sError := '� C�digo Ya Existe !'
    else if (Pos('FK_R_MOD_ENT_MODULO', UpperCase(Message)) > 0) then
      sError := Format('El m�dulo %d no existe', [DataSet.FieldByName('MO_CODIGO').AsInteger]) + CR_LF + Message
    else if (Pos('FK_R_CLAS_ENT_CLASIFI', UpperCase(Message)) > 0) then
      sError := Format('La clasificaci�n %d no existe', [DataSet.FieldByName('RC_CODIGO').AsInteger]) + CR_LF + Message
    else
      sError := Message;
    if (Context <> '') then
      sError := sError + CR_LF + Context;
  end;

  if (DataSet.FindField('EN_CODIGO') <> NIL) then begin
    with DataSet.FieldByName('EN_CODIGO') do begin
      iTabla := AsInteger;
      sMensaje1 := 'Error al Graba la Tabla # ' + AsString;
    end;
  end else begin
    iTabla := 0;
    sMensaje1 := 'Error al Grabar la Tabla ' + DataSet.Name;
  end;

  sMensaje2 := '';

  for i := 0 to DataSet.FieldCount - 1 do
    sMensaje2 := ConcatString(sMensaje2, DataSet.Fields[i].FieldName + ':' + DataSet.Fields[i].AsString, CR_LF);

  sMensaje2 := 'Error : ' + sError + CR_LF + 'Registro que no se grab� : ' + CR_LF + sMensaje2;

  oZetaProvider.Log.Error(iTabla, sMensaje1, sMensaje2);

end;

function TdmServerReporteadorDD.ImportarDiccionarioXML: OleVariant;
var
  xmlImport         : TdmXMLTools_RDD;
  xmlNodePadre      : TZetaXMLNode;
  xmlNode           : TZetaXMLNode;
  xmlNodeChild      : TZetaXMLNode;
  xmlNodeCambios    : TZetaXMLNode;
  iVersion          : Integer;
  cdsListaClasifi   : TClientDataSet;
  cdsEntidades      : TClientDataSet;
  cdsCampos         : TClientDataSet;
  cdsRelaciones     : TClientDataSet;
  cdsDefaultCampos  : TClientDataSet;
  cdsDefaultFiltros : TClientDataSet;
  cdsDefaultOrden   : TClientDataSet;
  cdsModulos        : TClientDataSet;
  cdsClasificaciones: TClientDataSet;
  cdsListasFijas    : TClientDataSet;
  cdsValores        : TClientDataSet;
  oDerechoEntidad   : TZetaCursor;
  oDeleteDerechos   : TZetaCursor;

  procedure EditaUS_CODIGO(Node: TZetaXMLNode; cdsDataset: TClientDataSet; const sField: string);
  begin
    cdsDataset.FieldByName(sField).AsInteger := 0;
  end;

  procedure EditaField(Node: TZetaXMLNode; cdsDataset: TClientDataSet; const sField: string);
  var
    iValue: Integer;
    sValue: string;
  begin
    with cdsDataset do begin
      with FindField(sField) do begin
        case DataType of
          ftInteger, ftSmallint: begin
              iValue := xmlImport.AttributeAsInteger(Node, sField, AsInteger);
              if AsInteger <> iValue then
                AsInteger := iValue;
            end;
          ftString: begin
              sValue := xmlImport.AttributeAsString(Node, sField, AsString);
              if AsString <> sValue then
                AsString := sValue
            end;
        end;
      end;
    end;
  end;

  procedure EditaTabla(Node: TZetaXMLNode; cdsDataset: TClientDataSet; const lExiste: Boolean);
  var
    i: Integer;
  begin
    if Node <> NIL then begin
      // FNodoBitacora := Node;
      with cdsDataset do begin
        if lExiste then begin
          if (FieldByName('US_CODIGO').AsInteger = 0) OR
            ((FieldByName('US_CODIGO').AsInteger <> 0) and
              (eConflictoImp(oZetaProvider.ParamList.ParamByName('Respetar').AsInteger) = ciEncimar)) then
            Edit;
        end
        else
          Append;

        if State <> dsBrowse then begin
          // FieldByName(K_XML_NODE).AsString := Node.Xml;
          for i := 0 to FieldCount - 1 do begin
            if (Fields[i].FieldName <> K_CAMPO_LLAVE_PORTAL) then begin
              if (Fields[i].FieldName <> 'US_CODIGO') then
                EditaField(Node, cdsDataset, Fields[i].FieldName)
              else
                EditaUS_CODIGO(Node, cdsDataset, Fields[i].FieldName);
            end;
          end;
          Post;
        end;
      end;
    end;
  end;

  procedure ImportaTabla(Node: TZetaXMLNode);
  begin
    with oZetaProvider do begin
      Node := xmlImport.GetNode(K_ENCABEZADO, Node);
      if (Node <> NIL) then begin
        if cdsEntidades = NIL then begin
          cdsEntidades := TClientDataSet.Create(NIL);
          try
            cdsEntidades.Data := OpenSQL(EmpresaActiva, GetSQL(Q_ENTIDADES), TRUE);
          except
            on Error: Exception do begin
              cdsEntidades.Data := OpenSQL(EmpresaActiva, GetSQL(Q_ENTIDADES_SIN_NIVEL0), TRUE);
            end;
          end;
        end;
        EditaTabla(Node, cdsEntidades, cdsEntidades.Locate('EN_CODIGO', xmlImport.AttributeAsInteger(Node,
              'EN_CODIGO', 0), []));
      end;
    end;
  end;

  procedure ImportaTablaDetalle(Node: TZetaXMLNode; var cdsDataset: TClientDataSet; const eTipo: eTipoDiccionario;
    const sTags, sTag, sField1, sField2: string);
  var
    NodeChild: TZetaXMLNode;
  begin
    with oZetaProvider do begin
      Node := xmlImport.GetNode(sTags, Node);

      if (Node <> NIL) then begin
        if cdsDataset = NIL then begin
          cdsDataset := TClientDataSet.Create(NIL);
          cdsDataset.OnReconcileError := cdsGrabaDatasetReconcileError;
          // SetTablaInfo(eTipo, '"CAST( ' + EntreComillas(' ') +' AS varchar(1000)) ' + K_XML_NODE + '"');
          SetTablaInfo(eTipo);
          cdsDataset.Data := GetTabla(EmpresaActiva);
        end;

        NodeChild := xmlImport.GetFirstChild(Node);
        while NodeChild <> NIL do begin
          EditaTabla(NodeChild, cdsDataset, cdsDataset.Locate(sField1 + ';' + sField2,
              VarArrayof([xmlImport.AttributeAsString(NodeChild, sField1, ''), xmlImport.AttributeAsString(NodeChild,
                    sField2, '')]), []));

          NodeChild := NodeChild.NextSibling;
        end;
      end;
    end;
  end;

  function ValidaClasificacion(sRC_CODIGO: string): string;
  begin
    with oZetaProvider do begin
      if cdsListaClasifi = NIL then begin
        cdsListaClasifi := TClientDataSet.Create(NIL);
        cdsListaClasifi.Data := OpenSQL(EmpresaActiva, GetSQL(Q_GETLISTA_CLASIFI), TRUE);
      end;

      if cdsListaClasifi.Locate('RC_CODIGO', VarArrayof([sRC_CODIGO]), []) then
        Result := sRC_CODIGO
      else
        Result := InTToStr(Ord(crClasificacionSistema));
    end;
  end;

  procedure ImportaTablaDetalleClasifiPorEntidad(Node: TZetaXMLNode; var cdsDataset: TClientDataSet;
    const sTags, sTag: string);
  var
    NodeChild             : TZetaXMLNode;
    sEN_CODIGO, sRC_CODIGO: string;
  begin
    with oZetaProvider do begin
      Node := xmlImport.GetNode(sTags, Node);

      if (Node <> NIL) then begin
        if cdsDataset = NIL then begin
          cdsDataset := TClientDataSet.Create(NIL);
          cdsDataset.OnReconcileError := cdsGrabaDatasetReconcileError;
          SetTablaInfo(eClasifiPorEntidad);
          cdsDataset.Data := GetTabla(EmpresaActiva);
        end;

        NodeChild := xmlImport.GetFirstChild(Node);
        while NodeChild <> NIL do begin
          sEN_CODIGO := xmlImport.AttributeAsString(NodeChild, 'EN_CODIGO', '');
          sRC_CODIGO := xmlImport.AttributeAsString(NodeChild, 'RC_CODIGO', '');
          if strLleno(sEN_CODIGO) and strLleno(sRC_CODIGO) then begin
            sRC_CODIGO := ValidaClasificacion(sRC_CODIGO);
            NodeChild.Attributes['RC_CODIGO'] := sRC_CODIGO;
            EditaTabla(NodeChild, cdsDataset, cdsDataset.Locate('EN_CODIGO;RC_CODIGO',
                VarArrayof([sEN_CODIGO, sRC_CODIGO]), []));
          end;
          NodeChild := NodeChild.NextSibling;
        end;
      end;
    end;
  end;

  procedure ImportaListaFija(Node: TZetaXMLNode);
  begin
    with oZetaProvider do begin
      Node := xmlImport.GetNode(K_ENCABEZADO, Node);
      if Node <> NIL then begin
        if cdsListasFijas = NIL then begin
          cdsListasFijas := TClientDataSet.Create(NIL);
          SetTablaInfo(eListasFijas);
          cdsListasFijas.Data := GetTabla(EmpresaActiva);
        end;
        EditaTabla(Node, cdsListasFijas, cdsListasFijas.Locate('LV_CODIGO', xmlImport.AttributeAsInteger(Node,
              'LV_CODIGO', 0), []));
      end;
    end;
  end;
{ delete from r_entidad where en_codigo = 10002
  update r_entidad  set en_titulo = 'ALGGGGO' where en_codigo = 9000

  select * from r_entidad
  order by en_codigo desc }
{$IFDEF ANTES}
  procedure GrabaDataset(cdsDataset: TClientDataSet; const eTabla: eTipoDiccionario);
  var
    ErrorCount: Integer;
  begin
    if (cdsDataset <> NIL) then begin
      with cdsDataset do begin
        if ChangeCount > 0 then begin
          with oZetaProvider do begin
            SetTablaInfo(eTabla);
            try
              Reconcile(GrabaTabla(EmpresaActiva, DeltaNull, ErrorCount));
            except
              Log.Error(0, 'Error al Grabar', 'Se encontr� un error al Grabar los datos ' + cdsDataset.Name);
            end;
          end;
        end;
        Close;
      end;
    end;
  end;
{$ENDIF}

  function GetDeltaNull( cdsDataSet: TClientDataSet ): OleVariant;
  begin
       with cdsDataSet do
       begin
            if ( ChangeCount > 0 ) then
               Result := Delta
            else
               TVarData( Result ).VType := varNull;
       end;
  end;

  procedure GrabaDatasetGrid(cdsDataset: TClientDataSet; const eTabla: eTipoDiccionario);
  var
    ErrorCount: Integer;
  begin
    if (cdsDataset <> NIL) then begin
      with cdsDataset do begin
        if ChangeCount > 0 then begin
          with oZetaProvider do begin
            SetTablaInfo(eTabla);
            try
              Reconcile(GrabaTablaGrid(EmpresaActiva, GetDeltaNull( cdsDataSet ), ErrorCount));
            except
              Log.Error(0, 'Error al Grabar', 'Se encontr� un error al Grabar los datos ' + cdsDataset.Name);
            end;
          end;
        end;
        Close;
      end;
    end;
  end;

  procedure PrendeDerechosEntidades;

    procedure InsertaDerecho(const iEntidad: Integer; const sEmpresa: string);
    begin
      with oZetaProvider do begin
        { Cuando se borra una entidad no se borra el derecho, nos protegemos por esta situaci�n }
        ParamAsInteger(oDeleteDerechos, 'Entidad', iEntidad);
        ParamAsString(oDeleteDerechos, 'Empresa', sEmpresa);
        Ejecuta(oDeleteDerechos);

        ParamAsInteger(oDerechoEntidad, 'Entidad_Param1', iEntidad);
        ParamAsInteger(oDerechoEntidad, 'Entidad_Param2', iEntidad);
        ParamAsString(oDerechoEntidad, 'Empresa', sEmpresa);
        Ejecuta(oDerechoEntidad);
      end;
    end;

  var
    sEmpresasActiva: string;
    oEmpresas      : TStrings;
    i              : Integer;
  begin
    { Asigna los derechos a las entidades solamente cuando se importa la entidad por 1era vez }
    with oZetaProvider do begin
      if (cdsEntidades <> NIL) then begin
        try
          with cdsEntidades do begin
            oEmpresas := TStringList.Create;
            oDerechoEntidad := CreateQuery(GetSQL(Q_INSERT_DERECHO_ENTIDAD));
            oDeleteDerechos := CreateQuery(GetSQL(Q_DELETE_DERECHO_ENTIDAD));

            try
              Open;
              sEmpresasActiva := ParamList.ParamByName('EmpresasConfidenciales').AsString;
              if StrVacio(sEmpresasActiva) then
                raise Exception.Create('Error al Obtener la Informaci�n de la Empresa Activa')
              else
                oEmpresas.CommaText := sEmpresasActiva;
              for i := 0 to oEmpresas.Count - 1 do begin
                First;
                while not EOF do begin
                  if not zStrToBool(FieldByName('EXISTE').AsString) then begin
                    EmpiezaTransaccion;
                    try
                      InsertaDerecho(FieldByName('EN_CODIGO').AsInteger, oEmpresas[i]);
                      TerminaTransaccion(TRUE);
                    except
                      on Error: Exception do begin
                        RollBackTransaccion;
                        Log.Error(FieldByName('EN_CODIGO').AsInteger, 'Error al Otorgar Derechos',
                          Format('Se encontr� un error al Asignar Derechos sobre Empresa %s, Entidad %s: ',
                            [oEmpresas[i], FieldByName('EN_TABLA').AsString]) + CR_LF + Error.Message);
                      end;
                    end;
                  end;
                  Next;
                end;
              end;
            finally
              Close;
              FreeAndNil(oEmpresas);
              FreeAndNil(oDeleteDerechos);
              FreeAndNil(oDerechoEntidad);
            end;
          end;
        except
          on Error: Exception do begin
            Log.Error(0, 'Error al Otorgar Derechos', Error.Message);
          end;
        end;
      end;
    end;
  end;

begin
  { ImportarDiccionarioXML }
  xmlImport := TdmXMLTools_RDD.Create(NIL);
  iVersion := 0;

  cdsListaClasifi := NIL;
  cdsEntidades := NIL;
  cdsCampos := NIL;
  cdsRelaciones := NIL;
  cdsDefaultCampos := NIL;
  cdsDefaultFiltros := NIL;
  cdsDefaultOrden := NIL;
  cdsModulos := NIL;
  cdsClasificaciones := NIL;
  cdsListasFijas := NIL;
  cdsValores := NIL;
  oDerechoEntidad := NIL;
  oDeleteDerechos := NIL;

  try
    with xmlImport do begin
      XMLDocument.LoadFromXML(oZetaProvider.ParamList.ParamByName('XMLFile').AsString);
    end;

    with oZetaProvider do begin
      if OpenProcess(prRDDImportarDiccionario, xmlImport.XMLDocument.XML.Count, FListaParametros) then begin

        with xmlImport do begin
          xmlNodePadre := XMLDocument.DocumentElement;
          if (xmlNodePadre = NIL) OR (xmlNodePadre.NodeName <> K_DICCIONARIO) then begin
            Log.Error(0, 'Archivo inv�lido', 'El archivo especificado no tiene el formato XML correcto');
            Abort;
          end;

          // Se busca el n�mero de version en el archivo.
          // Si existe el Tag, se actualiza el n�mero de versi�n.
          // La actualizacion se realiza hasta que se termine la importacion y no haya errores.
          xmlNode := GetNode(K_VERSION, xmlNodePadre);
          if xmlNode <> NIL then begin
            try
              iVersion := xmlNode.NodeValue;
            except
              on Error: Exception do begin
                Error.Message := 'El n�mero de versi�n de diccionario de datos no fu� actualizado' + CR_LF +
                  Error.Message;
                Log.Excepcion(0, 'El n�mero de versi�n no es v�lido', Error);
              end;

            end;
          end;

          xmlNode := GetNode(K_CAMBIOS, xmlNodePadre);
          xmlNodeCambios := xmlNode;

          if (xmlNode <> NIL) then begin
            try
              xmlNode := GetNode(K_TABLAS, xmlNode);
              if (xmlNode <> NIL) then begin
                xmlNodeChild := GetFirstChild(xmlNode);
                while xmlNodeChild <> NIL do begin
                  ImportaTabla(xmlNodeChild);
                  ImportaTablaDetalle(xmlNodeChild, cdsCampos, eCamposPorEntidad, K_ATRIBUTOS, K_ATRIBUTO, 'EN_CODIGO',
                    'AT_CAMPO');
                  ImportaTablaDetalle(xmlNodeChild, cdsRelaciones, eRelacionesPorEntidad, K_RELACIONES, K_RELACION,
                    'EN_CODIGO', 'RL_ENTIDAD');
                  ImportaTablaDetalle(xmlNodeChild, cdsDefaultCampos, eCamposDefault, K_CAMPOS_DEF, K_CAMPO_DEF,
                    'EN_CODIGO', 'RD_ORDEN');
                  ImportaTablaDetalle(xmlNodeChild, cdsDefaultFiltros, eFiltrosDefault, K_FILTROS_DEF, K_FILTRO_DEF,
                    'EN_CODIGO', 'RF_ORDEN');
                  ImportaTablaDetalle(xmlNodeChild, cdsDefaultOrden, eOrdenDefault, K_ORDENES_DEF, K_ORDEN_DEF,
                    'EN_CODIGO', 'RO_ORDEN');
                  ImportaTablaDetalle(xmlNodeChild, cdsModulos, eModulosPorEntidad, K_MODULOS, K_MODULO, 'EN_CODIGO',
                    'MO_CODIGO');
                  {$IFDEF ANTES}
                  ImportaTablaDetalle(xmlNodeChild, cdsClasificaciones, eClasifiPorEntidad, K_CLASIFICACIONES,
                    K_CLASIFICACION, 'EN_CODIGO', 'RC_CODIGO');
                  {$ENDIF}
                  ImportaTablaDetalleClasifiPorEntidad(xmlNodeChild, cdsClasificaciones, K_CLASIFICACIONES,
                    K_CLASIFICACION);
                  xmlNodeChild := xmlNodeChild.NextSibling;
                end;

                GrabaDatasetGrid(cdsEntidades, eEntidades);
                GrabaDatasetGrid(cdsCampos, eCamposPorEntidad);
                GrabaDatasetGrid(cdsRelaciones, eRelacionesPorEntidad);
                GrabaDatasetGrid(cdsDefaultCampos, eCamposDefault);
                GrabaDatasetGrid(cdsDefaultFiltros, eFiltrosDefault);
                GrabaDatasetGrid(cdsDefaultOrden, eOrdenDefault);
                GrabaDatasetGrid(cdsModulos, eModulosPorEntidad);
                GrabaDatasetGrid(cdsClasificaciones, eClasifiPorEntidad);
              end;

              xmlNode := GetNode(K_LISTA_FIJAS, xmlNodeCambios);
              if (xmlNode <> NIL) then begin
                xmlNodeChild := GetFirstChild(xmlNode);
                while xmlNodeChild <> NIL do begin
                  ImportaListaFija(xmlNodeChild);
                  ImportaTablaDetalle(xmlNodeChild, cdsValores, eListasFijasValor, K_LISTA_VALORES, K_LISTA_VALORES,
                    'LV_CODIGO', 'VL_CODIGO');
                  xmlNodeChild := xmlNodeChild.NextSibling;
                end;

                GrabaDatasetGrid(cdsListasFijas, eListasFijas);
                GrabaDatasetGrid(cdsValores, eListasFijasValor);
              end;

            except
              on Error: Exception do begin
                Log.Excepcion(0, 'Error al Importar Archivo', Error);
              end;

            end;
          end
          else
            Log.Evento(clbNinguno, 0, Date, 'No hay cambios a importar',
              'El archivo especificado no contiene ning�n cambio a importar');

          if not Log.HayErrores and (iVersion <> 0) then begin
            FQuery := CreateQuery(Format(GetSQL(Q_UPDATE_VERSION), [InTToStr(iVersion)]));
            Ejecuta(FQuery);
            FreeAndNil(FQuery);
          end;

          if ParamList.ParamByName('PrenderDerechos').AsBoolean then
            PrendeDerechosEntidades;

          {$IFDEF  RDD_Desarrollo}
          if DirectoryExists('d:\temp\') then
            XMLDocument.SaveToFile('d:\temp\Archivo.xml');
          {$ENDIF}
          {$IFDEF  RDD_Desarrollo}
          oZetaProvider.Log.Evento(clbNinguno, 0, Date, 'Archivo XML', XMLAsText);
          {$ENDIF}
          oZetaProvider.EjecutaAndFree(GetSQL(Q_REFRESH_DICCION));

        end;
        Result := oZetaProvider.CloseProcess;
      end;
    end;
  finally
    FreeAndNil(cdsListaClasifi);
    FreeAndNil(cdsEntidades);
    FreeAndNil(cdsCampos);
    FreeAndNil(cdsRelaciones);
    FreeAndNil(cdsDefaultCampos);
    FreeAndNil(cdsDefaultFiltros);
    FreeAndNil(cdsDefaultOrden);
    FreeAndNil(cdsModulos);
    FreeAndNil(cdsClasificaciones);
    FreeAndNil(cdsListasFijas);
    FreeAndNil(cdsValores);
    FreeAndNil(oDerechoEntidad);
    FreeAndNil(oDeleteDerechos);
    FreeAndNil(xmlImport);
  end;
end;

procedure TdmServerReporteadorDD.CargaParametrosImportarDiccionario;
begin
  with oZetaProvider.ParamList do begin
    FListaParametros := VACIO;
    FListaParametros := 'Datos de Exportaci�n:' + K_PIPE + 'Archivo: ' + ParamByName('Archivo').AsString + K_PIPE +
      'En caso de conflicto respetar: ' + ObtieneElemento(lfConflictoImp, ParamByName('Respetar').AsInteger) + K_PIPE +
      'Prender derechos de Tablas nuevas: ' + BoolToSiNo(ParamByName('PrenderDerechos').AsBoolean);
  end;
end;

procedure TdmServerReporteadorDD.CargaParametrosExportarDiccionario;
begin
  with oZetaProvider.ParamList do begin
    FListaParametros := VACIO;
    FListaParametros := 'Datos de Exportaci�n:' + K_PIPE + 'Archivo: ' + ParamByName('Archivo').AsString + K_PIPE +
      'Tipo de Informaci�n: ' + ObtieneElemento(lfTipoInfoExp, ParamByName('InformacionAExportar').AsInteger) + K_PIPE +
      'Versi�n: ' + ObtieneElemento(lfTipoRangoActivo2, ParamByName('Version').AsInteger);
  end;
end;

function TdmServerReporteadorDD.ExportarDiccionarioLista(Empresa, Lista, Parametros: OleVariant;
  var XMLFile: WideString): OleVariant;
var
  sOutput: string;
begin
  InitLog(Empresa, 'ExportarDiccionarioLista');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    InitGlobales;
    AsignaParamList(Parametros);
  end;
  CargaParametrosExportarDiccionario;
  cdsLista.Lista := Lista;
  Result := ExportarDiccionarioDataset(cdsLista, sOutput);
  XMLFile := sOutput;
  cdsLista.Active := FALSE;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.ExportarDiccionario(Empresa, Parametros: OleVariant; var XMLFile: WideString): OleVariant;
var
  sOutput: string;
begin
  InitLog(Empresa, 'ExportarDiccionario');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    AsignaParamList(Parametros);
  end;

  InitBroker;
  try
    DiccionarioListaTablasBuildDataSet; // obtiene la lista de tablas
    CargaParametrosExportarDiccionario;
    Result := ExportarDiccionarioDataset(SQLBroker.SuperReporte.DataSetReporte, sOutput);
    XMLFile := sOutput;
  finally
    ClearBroker;
  end;
  EndLog;
  SetComplete;

end;

function TdmServerReporteadorDD.DiccionarioListaTablasGetLista(Empresa, Parametros: OleVariant): OleVariant;
begin
  InitLog(Empresa, 'DiccionarioListaTablasGetLista');
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    AsignaParamList(Parametros);
  end;
  InitBroker;
  try
    DiccionarioListaTablasBuildDataSet;
    Result := SQLBroker.SuperReporte.GetReporte;
  finally
    ClearBroker;
  end;
  EndLog;
  SetComplete;
end;

function TdmServerReporteadorDD.GetFiltroVersion: string;
begin
  Result := '';
  with oZetaProvider.ParamList do begin
    case eTipoRangoActivo(ParamByName('Version').AsInteger) of
      raLista:
        Result := Format(K_CAMPO_VERSION + ' IN (%s)', [ParamByName('VersionLista').AsString]);
      raRango: begin
          if strLleno(ParamByName('VersionInicial').AsString) and strLleno(ParamByName('VersionFinal').AsString) then
            Result := Format(Format('(%1:s >= %s AND %1:s <= %s)', [K_CAMPO_VERSION]),
              [ParamByName('VersionInicial').AsString, ParamByName('VersionFinal').AsString])
          else if strLleno(ParamByName('VersionInicial').AsString) then
            Result := Format(K_CAMPO_VERSION + ' >= %s', [ParamByName('VersionInicial').AsString])
          else if strLleno(ParamByName('VersionFinal').AsString) then
            Result := Format(K_CAMPO_VERSION + ' <= %s', [ParamByName('VersionFinal').AsString])
        end;
    end;
  end;
end;

function TdmServerReporteadorDD.GetFiltroUsuario: string;
begin
  Result := '';
  with oZetaProvider.ParamList do begin
    case eTipoInfoExp(ParamByName('InformacionAExportar').AsInteger) of
      ieCambiosOficiales:
        Result := '(US_CODIGO = 0)';
      ieCambiosUsuario:
        Result := '(US_CODIGO <> 0)';
    end;
  end;
end;

procedure TdmServerReporteadorDD.DiccionarioListaTablasBuildDataSet;
var
  sFiltro: string;
begin
  sFiltro := VACIO;
  with SQLBroker do begin
    Init(enRDDEntidad);
    with Agente do begin
      AgregaColumna('R_ENTIDAD.EN_CODIGO', TRUE, Entidad, tgNumero, 0, 'EN_CODIGO');
      AgregaColumna('R_ENTIDAD.EN_TITULO', TRUE, Entidad, tgTexto, 100, 'EN_TITULO');
      AgregaColumna('R_ENTIDAD.EN_TABLA', TRUE, Entidad, tgTexto, 100, 'EN_TABLA');
      AgregaColumna('R_ENTIDAD.EN_DESCRIP', TRUE, Entidad, tgTexto, 255, 'EN_DESCRIP');
      AgregaColumna('R_ENTIDAD.EN_VERSION', TRUE, Entidad, tgNumero, 0, 'EN_VERSION');
      AgregaColumna('R_ENTIDAD.EN_ACTIVO', TRUE, Entidad, tgTexto, 2, 'EN_ACTIVO');

      with oZetaProvider do begin
        { **** Filtro de Version **** }
        // AgregaFiltro( StrTransAll( GetFiltroVersion, K_CAMPO_VERSION, 'R_ENTIDAD.EN_VERSION') , TRUE, Entidad );

        { **** Filtro de Tablas de Sistema **** }
        sFiltro := VACIO;
        if ParamList.ParamByName('TipoSistema').AsBoolean then begin
          sFiltro := '(R_ENTIDAD.EN_CODIGO < 9000)'
        end;
        // Filtro de Tablas de reporteador
        if ParamList.ParamByName('TipoReporteador').AsBoolean then begin
          sFiltro := ConcatString(sFiltro, ' (R_ENTIDAD.EN_CODIGO  >= 9000 and R_ENTIDAD.EN_CODIGO <= 10000 )', ' OR ');
        end;
        // Filtro de Tablas de Cliente
        if ParamList.ParamByName('TipoCliente').AsBoolean then begin
          sFiltro := ConcatString(sFiltro, ' (R_ENTIDAD.EN_CODIGO > 10000) ', ' OR ');
        end;

        ParamList.ParamByName('RangoLista').AsString := ConcatFiltros(Parentesis(sFiltro),
          ParamList.ParamByName('RangoLista').AsString);
        ParamList.ParamByName('RangoLista').AsString := Parentesis(ParamList.ParamByName('RangoLista').AsString);

        { **** Filtro de Tipo de Informacion a Exportar **** }
        // AgregaFiltro( GetFiltroUsuario, TRUE, Entidad );
      end;

      AgregaOrden('R_ENTIDAD.EN_CODIGO', TRUE, Entidad);
    end;
    AgregaRangoCondicion(oZetaProvider.ParamList);
    BuildDataset(oZetaProvider.EmpresaActiva);

    {$IFDEF RDD_Desarrollo}
    if (oZetaProvider.Log <> NIL) then
      oZetaProvider.Log.Evento(clbNinguno, 0, Date, 'Lista de Tablas', 'SQL Generado BuildDataset: ' + CR_LF +
          Agente.SQL.Text);
    {$ENDIF}
  end;
end;

function TdmServerReporteadorDD.ExportarDiccionarioDataset(DataSet: TDataSet; var sOutput: string): OleVariant;
var
  xmlExport     : TdmXMLTools_RDD;
  xmlNode       : TZetaXMLNode;
  xmlNodeTablas : TZetaXMLNode;
  xmlNodeCambios: TZetaXMLNode;
  sExportado    : string;
  iExportados   : Integer;
  FListaFijas   : TStrings;

  function WriteToXML(const sSQL, sParentNode, sParentTag, sTag: string): Boolean;
  begin
    oZetaProvider.PreparaQuery(FQuery, sSQL);

    with FQuery do begin
      Active := TRUE;

      Result := not EOF;
      if Result then begin
        if (xmlNode = NIL) then begin
          xmlNode := xmlExport.WriteStartElement(sParentNode);
          xmlExport.WriteAttributeInteger('EN_CODIGO', FieldByName('EN_CODIGO').AsInteger);
        end;

        if (sParentTag <> '') then
          xmlExport.WriteStartElement(sParentTag);
      end;

      while not EOF do begin
        try
          xmlExport.XMLBuildDataRow(FQuery, sTag);
        except
          on Error: Exception do begin
            oZetaProvider.Log.Excepcion(0, Format('Error al Exportar %s', [sParentTag]), Error);
          end;
        end;
        Next;
      end;
      if Result then begin
        sExportado := sExportado + CR_LF + Format('%s: %d registros exportados',
          [sTag, xmlExport.CurrentNode.ChildNodes.Count]);
        iExportados := iExportados + xmlExport.CurrentNode.ChildNodes.Count;
      end
      else
        sExportado := sExportado + CR_LF + Format('%s: %d registros exportados', [sTag, 0]);

      Close;
    end;
  end;

  function ConcatFiltrosRDD(const SQL, sCampoVersion: string): string;
  begin
    Result := SQL + ConcatFiltros(Format(K_SQL_FILTRO_TABLA, [DataSet.FieldByName('EN_CODIGO').AsInteger]),
      ConcatFiltros(StrTransAll(GetFiltroVersion, K_CAMPO_VERSION, sCampoVersion), GetFiltroUsuario));
  end;

  procedure ExportaListasFijas;
  var
    FValores: TZetaCursor;
    i       : Integer;
    Campo   : TField;
  begin
    FValores := NIL;

    if FListaFijas.Count > 0 then begin
      with oZetaProvider do begin

        PreparaQuery(FQuery, Format(GetSQL(Q_LISTAS_FIJAS),
            [Format('where LV_CODIGO in (%s)', [FListaFijas.CommaText])]));
        with FQuery do begin
          Active := TRUE;
          if not EOF then begin
            xmlExport.WriteStartElement(K_LISTA_FIJAS);
            FValores := CreateQuery;
          end;

          while not EOF do begin
            xmlExport.WriteStartElement(K_LISTA_FIJA);
            xmlExport.WriteAttributeInteger('LV_CODIGO', FieldByName('LV_CODIGO').AsInteger);

            // Escribe el encabezado de la Lista Fija
            xmlExport.WriteStartElement(K_ENCABEZADO);
            try
              for i := 0 to (FieldCount - 1) do begin
                Campo := Fields[i];
                xmlExport.WriteAttributeString(Campo.FieldName, xmlExport.GetFieldInfo(Campo));
              end;
            finally
              xmlExport.WriteEndElement;
            end;

            AbreQueryScript(FValores, Format(GetSQL(Q_R_VALOR), [FieldByName('LV_CODIGO').AsInteger]));

            xmlExport.WriteStartElement(K_LISTA_VALORES);
            xmlExport.XMLBuildDataRow(FValores, K_LISTA_VALOR);
            xmlExport.WriteEndElement;
            xmlExport.WriteEndElement;
            Next;
          end;

          if (FValores <> NIL) then
            xmlExport.WriteEndElement;
          FreeAndNil(FValores);
        end;
      end;
    end;

  end;

var
  i          : Integer;
  lEncabezado: Boolean;
begin

  if oZetaProvider.OpenProcess(prRDDExportarDiccionario, DataSet.RecordCount, FListaParametros) then begin
    FQuery := oZetaProvider.CreateQuery;
    xmlExport := TdmXMLTools_RDD.Create(NIL);
    FListaFijas := TStringList.Create;

    with xmlExport do begin
      XMLInit(K_DICCIONARIO);
      WriteValueInteger(K_VERSION, oZetaProvider.GetGlobalInteger(K_GLOBAL_VERSION_RDD));
      xmlNodeCambios := WriteStartElement(K_CAMBIOS);
      WriteEndElement;
      WriteStartElement(K_BORRADOS);
      WriteEndElement;

      with DataSet do begin
        try
          if not EOF then begin
            ActualNode := xmlNodeCambios;
            xmlNodeTablas := WriteStartElement(K_TABLAS);
          end;

          while not EOF and oZetaProvider.CanContinue(FieldByName('EN_CODIGO').AsInteger) do begin
            i := 0;
            iExportados := 0;
            xmlNode := NIL;
            sExportado := VACIO;

            ActualNode := xmlNodeTablas;

            // Se escribe el encabezado de la tabla
            lEncabezado := WriteToXML(ConcatFiltrosRDD(GetSQL(Q_ENTIDAD), 'EN_VERSION'), K_TABLA, '', K_ENCABEZADO);

            // Se exportan cada uno de los elementos de cada Entidad
            while i < High(Arreglo) do begin
              if WriteToXML(ConcatFiltrosRDD(Arreglo[i], Arreglo[i + 3]), K_TABLA, Arreglo[i + 2], Arreglo[i + 1]) then
                WriteEndElement;
              i := i + 4;
            end;

            if lEncabezado then
              WriteEndElement;

            if iExportados > 0 then begin
              oZetaProvider.Log.Evento(clbNinguno, FieldByName('EN_CODIGO').AsInteger, Date,
                Format('Entidad %d:%s exportada', [FieldByName('EN_CODIGO').AsInteger,
                    FieldByName('EN_TITULO').AsString]), sExportado);
            end;

            // Se revisa si se requiere exportar Listas Fijas (R_LISTAVAL)
            oZetaProvider.PreparaQuery(FQuery, ConcatFiltrosRDD(GetSQL(Q_R_ATRIBUTO_LISTA_FIJA), Arreglo[3]) +
                K_FILTRO_LISTAFIJA);
            with FQuery do begin
              Active := TRUE;
              while not EOF do begin
                FListaFijas.Add(FQuery.Fields[0].AsString);
                Next;
              end;
              Close;
            end;

            Next;
          end;
          Close;

          ActualNode := xmlNodeCambios;
          ExportaListasFijas;

        finally
          {$IFDEF  RDD_Desarrollo}
          if DirectoryExists('d:\temp\') then
            XMLDocument.SaveToFile('d:\temp\Archivo.xml');
          {$ENDIF}
          XMLEnd;

          {$IFDEF  RDD_Desarrollo}
          oZetaProvider.Log.Evento(clbNinguno, 0, Date, 'Archivo XML', XMLAsText);
          {$ENDIF}

            sOutput := XMLAsText;
            sOutput := StringReplace( sOutPut, '<?xml version="1.0" standalone="yes"?>', '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>' , [rfReplaceAll, rfIgnoreCase] );
        end;
      end;
    end;
    FreeAndNil(FListaFijas);
    FreeAndNil(FQuery);
    Result := oZetaProvider.CloseProcess;
  end;
end;

function TdmServerReporteadorDD.GetSQLBroker: TSQLBroker;
begin
  Result := Self.oZetaSQLBroker;
end;

procedure TdmServerReporteadorDD.ClearBroker;
begin
  ZetaSQLBroker.FreeSQLBroker(oZetaSQLBroker);
end;

procedure TdmServerReporteadorDD.InitBroker;
begin
  InitCreator;
  oZetaCreator.RegistraFunciones([efComunes]);
  oZetaSQLBroker := ZetaSQLBroker.GetSQLBroker(oZetaCreator);
end;

function TdmServerReporteadorDD.SQLValido(Empresa: OleVariant; const Formula: WideString; Entidad: Integer;
  var MsgError: WideString): WordBool;
var
  oEvaluador         : TZetaEvaluador;
  sFormula, sMsgError: string;
begin
  Result := TRUE;
  MsgError := VACIO;
  oZetaProvider.EmpresaActiva := Empresa;
  InitCreator;
  oZetaCreator.PreparaEntidades;
  oEvaluador := TZetaEvaluador.Create(oZetaCreator);
  try
    with oEvaluador do begin
      oEvaluador.Entidad := enEmpleado; // LA TABLA PRINCIPAL
      if (strLleno(Formula)) then begin
        sFormula := Formula;
        Result := FiltroSQLValido(sFormula, sMsgError);
        MsgError := sMsgError;
      end;
    end;
  finally
    FreeAndNil(oEvaluador);
  end;
end;

{ -------------------------------------------------------------------------- }

{$IFNDEF DOS_CAPAS}


initialization

TComponentFactory.Create(ComServer, TdmServerReporteadorDD, Class_dmServerReporteadorDD, ciMultiInstance,
  ZetaServerTools.GetThreadingModel);
{$ENDIF}

end.
