{ * Importaci�n de Reportes RP1.
  Proceso de Importaci�n de reportes
  @comment Es un extracto de la unidad DBaseReportes.pas
  @author Desconocido
}
unit DImportarReporte;

interface

uses
  Classes, DB, DBClient, DZetaServerProvider, ZetaTipoEntidad, ZetaCommonLists, Provider;

type

  TdmImportarReporte = class(TDataModule)
    cdsEditReporte: TClientDataSet;
    cdsCampoRep: TClientDataSet;
    procedure cdsEditReporteAfterCancel(DataSet: TDataSet);
    procedure cdsEditReporteBeforePost(DataSet: TDataSet);
    procedure cdsEditReporteNewRecord(DataSet: TDataSet);
    procedure cdsEditReporteReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure ErrorEnUpdate(Sender: TObject; DataSet: TzProviderClientDataset; E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
    procedure cdsCampoRepNewRecord(DataSet: TDataSet);
//    procedure cdsEditReporteAlAdquirirDatos(Sender: TObject);
//    procedure cdsEditReporteAlEnviarDatos(Sender: TObject);
  private
    oZetaProvider   : TdmZetaServerProvider;
    oZetaProviderLog: TdmZetaServerProvider;
    sArchivo        : string;
    sError          : string;
    FReporte        : Integer;
    function ExisteEntidad(const Index: TipoEntidad): Boolean;
    function ObtieneEntidad(const Index: TipoEntidad): String;
    function ExisteClasificacion(const Index: eClasifiReporte): Boolean;
    function ObtieneClasificacion(const Index: eClasifiReporte): String;
    function GetMaxReporte: Integer;
    procedure SetTablaInfoCampoRep;
    procedure BeforeUpdateCampoRep(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure BeforeUpdateReporte(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure cdsEditReporteAdquirirDatos;
    function cdsEditReportesEnviar: Boolean;
  public
    constructor Create(ZetaProvider, ZetaProviderLog: TdmZetaServerProvider; Archivo: string);
    function Execute: Boolean;
  end;

  function ImportarReporteRP1(ZetaProvider, ZetaProviderLog: TdmZetaServerProvider; sArchivo: string; var Error: string): Boolean;

//var
  //dmImportarReporte: TdmImportarReporte;

implementation

uses
  SysUtils, {Windows, Controls, }ZetaCommonClasses, ZetaCommonTools, ZReportToolsConsts;

{$R *.DFM}

{* Invoca la importaci�n de reporte}
function ImportarReporteRP1(ZetaProvider, ZetaProviderLog: TdmZetaServerProvider; sArchivo: string; var Error: string): Boolean;
var
 dmImportarReporte: TdmImportarReporte;
begin
  dmImportarReporte := TdmImportarReporte.Create(ZetaProvider, ZetaProviderLog, sArchivo);
  try
    Result := dmImportarReporte.Execute;
    Error := dmImportarReporte.sError;
  finally
    FreeAndNil(dmImportarReporte);
  end;
end;

{* Constructor de la clase}
constructor TdmImportarReporte.Create(ZetaProvider, ZetaProviderLog: TdmZetaServerProvider; Archivo: string);
begin
  inherited Create(nil);
  oZetaProvider    := ZetaProvider;
  if Assigned(ZetaProviderLog) then
    oZetaProviderLog := ZetaProviderLog
  else
    oZetaProviderLog := ZetaProvider;
  sArchivo         := Archivo;
  sError           := '';
end;

{ * Importaci�n de Reportes RP1.
  @return TRUE si pudo aplicar el proceso, FALSE en caso contrario.}
function TdmImportarReporte.Execute: Boolean;
const
  K_ERROR     = 'ERROR';
  K_NO_VALIDO = 'El archivo de importaci�n no es v�lido';
  K_IMPORTA   = 'Importaci�n de Reportes';
  K_CLASIF_NOEXISTE = 'La Clasificaci�n #%d no existe. ' + CR_LF + 'El reporte se importar� en la Clasificaci�n %s';
var
  oEncabezado, oImpReporte: TStringList;
//  oDatosImport            : TDatosImport;
  iCount                  : Integer;

  function LeeSeccion(var i: Integer; DataSet: TDataSet): Boolean;
  var
    sRenglon, sField, sFormat, sValue: string;
    IgualPos                         : Integer;

    function EsCorchete: Boolean;
    begin
      Result := Copy(oImpReporte[i], 1, 1) = '[';
    end;

    function QuitaComillas: string;
    begin
      Result := sValue;
      if Copy(Result, 1, 1) = UnaCOMILLA then begin
        Result := Copy(Result, 2, Length(Result));
        if Copy(Result, Length(Result), 1) = UnaCOMILLA then
          Result := Copy(Result, 1, Length(Result) - 1);
      end;
    end;

  var
    oField: TField;
  begin
    Result := TRUE;
    if EsCorchete then
      i := i + 1;

    while (i < iCount) AND (NOT EsCorchete) do begin
      with DataSet do begin
        sRenglon := oImpReporte[i];
        if StrLleno(sRenglon) then begin
          IgualPos := Pos('=', sRenglon);
          sField   := Copy(sRenglon, 1, IgualPos - 1);
          sValue   := Copy(sRenglon, IgualPos + 1, Length(sRenglon));
          oField   := FindField(sField);
          if Assigned(oField) AND (oField.DataType <> ftAutoInc) then begin // RCM
            with oField do begin
              if DataType in [ftDate, ftDateTime] then
                try
                  sFormat  := FormatSettings.ShortDateFormat;
                  FormatSettings.ShortDateFormat := 'mm/dd/yyyy';
                  AsString := sValue;
                finally
                  FormatSettings.ShortDateFormat := sFormat;
                end else if oField is TNumericField then
                AsString := sValue
              else
                AsString := QuitaComillas;
            end;
          end else
            Result := FALSE;
        end;
        i := i + 1;
      end;
    end;
  end;

var
  i               : Integer;
  sFuente         : string;
  Clasificacion   : eClasifiReporte;
begin
  Result := False;
  oImpReporte   := TStringList.Create;
  oEncabezado   := TStringList.Create;

  // Iniciar evento de importaci�n de reportes
  oZetaProviderLog.Log.Evento(clbMotorPatchImportarReporte, 0, Date, Format('Importando de Reporte %s:',[ExtractFileName(sArchivo)]), Format('Archivo:%s',[sArchivo]));

  try
    oImpReporte.LoadFromFile(sArchivo);
    iCount := oImpReporte.Count;
    if Pos('TOTAL=', UpperCase(oImpReporte[iCount - 1])) > 0 then
      oImpReporte.Delete(iCount - 1);
    iCount := oImpReporte.Count;
    if Pos('[TOTAL_DETALLE]', UpperCase(oImpReporte[iCount - 1])) > 0 then
      oImpReporte.Delete(iCount - 1);
    iCount       := oImpReporte.Count;
    // oDatosImport := TDatosImport.Create(self);

    if oImpReporte[0] = K_GENERAL then
      i := 1
    else
      i := 0;

    while oImpReporte[i] <> K_REPORTE do begin
      oEncabezado.Add(oImpReporte[i]);
      i := i + 1;
    end;
    oEncabezado.Add('RE_CLASIFI=' + oImpReporte.Values['RE_CLASIFI']);
    with oEncabezado do begin
      sError := Values['NOMBRE'];
      if sError = '' then begin
        sError := K_NO_VALIDO;
        Exit;
      end;
      Clasificacion := eClasifiReporte(StrToInt(Values['RE_CLASIFI']));
    end;

    sError := '';
    //if Result then begin
      with cdsEditReporte do begin
        Active := FALSE;
        cdsEditReporteAdquirirDatos;
        Append;
        LeeSeccion(i, cdsEditReporte);
        if not ExisteEntidad(TipoEntidad(FieldByName('RE_ENTIDAD').AsInteger)) then begin
          //raise Exception.Create( ObtieneEntidad(TipoEntidad(FieldByName('RE_ENTIDAD').AsInteger)) + CR_LF + 'El reporte no se puede importar');
          oZetaProviderLog.Log.Error(0, ObtieneEntidad(TipoEntidad(FieldByName('RE_ENTIDAD').AsInteger)) + CR_LF + 'El reporte no se puede importar', '');
          Cancel;
        end else begin
          FieldByName('RE_CODIGO').AsInteger := 0;
          FieldByName('RE_NOMBRE').AsString  := oEncabezado.Values['NOMBRE'];// oDatosImport.NombreNuevo; // ToDo: Est� correcto?
          FieldByName('RE_FECHA').AsDateTime := DATE;
          FieldByName('US_CODIGO').AsInteger := oZetaProvider.UsuarioActivo;
          if ExisteClasificacion(Clasificacion) then  //if ExisteClasificacion(oDatosImport.Clasificacion) then // ToDo: Est� correcto?
            FieldByName('RE_CLASIFI').AsInteger := Ord(Clasificacion)
          else begin
            FieldByName('RE_CLASIFI').AsInteger := 0;//Ord(FClasifActivo);
            //raise Exception.Create(Format('La Clasificaci�n #%d no existe. ' + CR_LF + 'El reporte se importar� en la Clasificaci�n %s',
            //    [Ord(Clasificacion), ObtieneClasificacion(0{FClasifActivo})]));
            oZetaProviderLog.Log.Advertencia(clbMotorPatchImportarReporte, 0, Date(),
                                        Format(K_CLASIF_NOEXISTE, [Ord(Clasificacion), ObtieneClasificacion(0{FClasifActivo})]), '');
          end;
          Post;
          cdsCampoRep.EmptyDataSet;
//          cdsCampoRep.Conectar;
          while i < iCount do begin
            cdsCampoRep.Append;
            if LeeSeccion(i, cdsCampoRep) then
              cdsCampoRep.Post
            else
              cdsCampoRep.Cancel;
          end;
          //cdsEditReportesEnviar;
          Result := cdsEditReportesEnviar;
          // Grabar bit�cora de la importacion de reportes
          //oZetaProviderLog.Log.Evento(clbMotorPatchImportarReporte, 0, Date, 'Importaci�n de Reporte', 'Termina proceso');
        end;
      end; // with cdsEditReporte do
  finally
    FreeAndNil(oEncabezado);
    FreeAndNil(oImpReporte);
  end;
end;

function TdmImportarReporte.ExisteEntidad(const Index: TipoEntidad): Boolean;
begin
  {$IFDEF RDD}
  dmDiccionario.LlenaArregloEntidades;
  with dmDiccionario.cdsEntidades do begin
    Conectar;
    Result := Locate('EN_CODIGO', Ord(Index), []);
  end;
  {$ELSE}
  Result := TRUE;
  {$ENDIF}
end;

function TdmImportarReporte.ObtieneEntidad(const Index: TipoEntidad): String;

    function EntidadValida( const Index: TipoEntidad ): Boolean;
    begin
         {$ifdef RDD|RDDTRESSCFG}
         //Siempre regresa TRUE, porque ahora hay un constraint de REPORTES hacia R_ENTIDAD
         //que no permite que haya Reportes de tablas que no existen.
         Result :=  TRUE;
         {$else}
    {$WARNINGS OFF}
         Result :=  ( Index >=  Low(TipoEntidad) ) and
                    ( Index <=  High(TipoEntidad) );
         {$endif}
    {$WARNINGS ON}
    end;

    function ObtieneEntidad( const Index: TipoEntidad ): String;
    begin
         if EntidadValida( Index ) then
         begin
              Result :=  aTipoEntidad[ TipoEntidad( Index ) ];
         end
         else
         begin
              Result := '< Tabla Para Uso Futuro >';
         end;
    end;

begin
  {$IFDEF RDD}
  if ExisteEntidad(Index) then
    Result := aTipoEntidad[Index]
  else
    Result := Format('Tabla #%d no Existe', [Ord(Index)]);
  {$ELSE}
  Result := ObtieneEntidad(TipoEntidad(Index));
  {$ENDIF}
end;

function TdmImportarReporte.ExisteClasificacion(const Index: eClasifiReporte): Boolean;
begin
  {$IFDEF RDD}
  with dmDiccionario.cdsClasificaciones do begin
    Conectar;
    Result := Locate('RC_CODIGO', Ord(Index), []);
  end;
  {$ELSE}
  Result := TRUE;
  {$ENDIF}
end;

function TdmImportarReporte.ObtieneClasificacion(const Index: eClasifiReporte): String;
begin
  {$IFDEF RDD}
  case Index of
    crFavoritos:
      Result := K_MIS_FAVORITOS_DES;
    crSuscripciones:
      Result := K_MIS_SUSCRIPCIONES_DES
    else begin
        with dmDiccionario.cdsClasificaciones do begin
          if ExisteClasificacion(Index) then
            Result := StrDef(FieldByName('RC_NOMBRE').AsString,
              Format('Clasificaci�n #%d', [Ord(Index)]))
          else
            Result := Format('Clasificaci�n #%d no Existe', [Ord(Index)]);
        end;
      end;
  end;

  {$ELSE}
  Result := ObtieneElemento(lfClasifiReporte, Ord(Index));
  {$ENDIF}
end;

procedure TdmImportarReporte.cdsCampoRepNewRecord(DataSet: TDataSet);
begin
  with cdsCampoRep do begin
    FieldByName('RE_CODIGO').AsInteger  := cdsEditReporte.FieldByName('RE_CODIGO').AsInteger;
    FieldByName('CR_TIPO').AsInteger    := 0;
    FieldByName('CR_POSICIO').AsInteger := -1;
    FieldByName('CR_SUBPOS').AsInteger  := -1;
    FieldByName('CR_REQUIER').AsString  := '';
    FieldByName('CR_CALC').AsInteger    := 0;
    FieldByName('CR_MASCARA').AsString  := '';
    FieldByName('CR_ANCHO').AsInteger   := 0;
    FieldByName('CR_OPER').AsInteger    := 0;
    FieldByName('CR_TFIELD').AsInteger  := 0;
    FieldByName('CR_SHOW').AsInteger    := 0;
    FieldByName('CR_DESCRIP').AsString  := '';
    FieldByName('CR_BOLD').AsString     := '';
    FieldByName('CR_ITALIC').AsString   := '';
    FieldByName('CR_SUBRAYA').AsString  := '';
    FieldByName('CR_STRIKE').AsString   := '';
    FieldByName('CR_ALINEA').AsInteger  := 0;
    FieldByName('CR_COLOR').AsInteger   := 0;
  end;
end;

procedure TdmImportarReporte.cdsEditReporteAfterCancel(DataSet: TDataSet);
begin
  cdsCampoRep.CancelUpdates;
end;

procedure TdmImportarReporte.SetTablaInfoCampoRep;
begin
     with oZetaProvider do
     begin
      TablaInfo.SetInfo('CAMPOREP',
        'RE_CODIGO, CR_TIPO, CR_POSICIO, CR_CLASIFI, CR_SUBPOS, CR_TABLA, CR_TITULO, CR_REQUIER, ' +
        'CR_CALC, CR_MASCARA, CR_ANCHO, CR_OPER, CR_TFIELD, CR_SHOW, CR_DESCRIP, CR_COLOR, ' +
        'CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA', 'RE_CODIGO,CR_TIPO,CR_POSICIO,CR_SUBPOS');
     end;
end;

procedure TdmImportarReporte.cdsEditReporteAdquirirDatos;
const
Q_EDIT_REPORTE = 'select REPORTE.RE_CODIGO, REPORTE.RE_NOMBRE, REPORTE.RE_TIPO, ' +
    'REPORTE.RE_TITULO, REPORTE.RE_ENTIDAD, REPORTE.RE_SOLOT, REPORTE.RE_FECHA, ' +
    'REPORTE.RE_GENERAL, REPORTE.RE_REPORTE, REPORTE.RE_CFECHA, REPORTE.RE_IFECHA, ' +
    'REPORTE.RE_HOJA, REPORTE.RE_ALTO, REPORTE.RE_ANCHO, REPORTE.RE_PRINTER, ' +
    'REPORTE.RE_COPIAS, REPORTE.RE_PFILE, REPORTE.RE_ARCHIVO, REPORTE.RE_VERTICA, ' +
    'REPORTE.US_CODIGO, REPORTE.RE_FILTRO, REPORTE.RE_COLNUM, REPORTE.RE_COLESPA, ' +
    'REPORTE.RE_RENESPA, REPORTE.RE_MAR_SUP, REPORTE.RE_MAR_IZQ, REPORTE.RE_MAR_DER, ' +
    'REPORTE.RE_MAR_INF, REPORTE.RE_NIVEL, REPORTE.RE_FONTNAM, REPORTE.RE_FONTSIZ, ' +
    'REPORTE.QU_CODIGO, REPORTE.RE_CLASIFI, REPORTE.RE_CANDADO, ' +
    'EN_TITULO RE_TABLA, R_ENTIDAD.EN_NIVEL0, R_ENTIDAD.EN_TABLA ' + 'from REPORTE ' +
    'left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO = REPORTE.RE_ENTIDAD ' + 'where REPORTE.RE_CODIGO = %d ' +
    'order by RE_NOMBRE ';
begin
     with oZetaProvider do
     begin
          cdsEditReporte.Data := OpenSQL(EmpresaActiva, Format(Q_EDIT_REPORTE, [0]), TRUE);
          SetTablaInfoCampoRep;
          TablaInfo.Filtro := 'RE_CODIGO = 0';
          cdsCampoRep.Data := GetTabla(EmpresaActiva);
     end;
end;


procedure TdmImportarReporte.BeforeUpdateCampoRep(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  // En la Alta de Reportes, el RE_CODIGO viene VACIO del Cliente
  // Se asigna del FReporte que se obtuvo en la Alta del registro
  // padre : REPORTE, en su BeforeUpdateRecord
  with DeltaDS do begin
    Edit;
    FieldByName('RE_CODIGO').AsInteger := FReporte;
    Post;
  end;
end;

procedure TdmImportarReporte.BeforeUpdateReporte(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;
          UpdateKind: TUpdateKind; var Applied: Boolean);
const
     K_DELETE = 'DELETE FROM CAMPOREP WHERE RE_CODIGO = %d';
     // K_MAX_REPORTE = 'SELECT MAX(RE_CODIGO) FROM REPORTE';
     // var FDataSet : TZetaCursor;
begin
     // Si es Alta, tengo que obtener el m�ximo Folio
     // Si es Edici�n, tengo que borrar todos los CampoRep anteriores
     with oZetaProvider do
     begin
          if (UpdateKind = ukInsert) then
          begin
               FReporte := GetMaxReporte;
               DeltaDS.Edit;
               DeltaDS.FieldByName('RE_CODIGO').AsInteger := FReporte;
               DeltaDS.Post;
          end
          else
          begin
               ExecSQL(EmpresaActiva, Format(K_DELETE, [FReporte]));
          end;
     end;
end;

function TdmImportarReporte.cdsEditReportesEnviar: Boolean;
var
   ErrorCount:Integer;

   procedure SetTablaInfoReporte;
   begin
        with oZetaProvider do
        begin
             TablaInfo.SetInfo('REPORTE', 'RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,' +
                                          'RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,RE_CFECHA,' +
                                          'RE_HOJA,RE_ALTO,RE_ANCHO,RE_PRINTER,RE_COPIAS,' +
                                          'RE_PFILE,RE_ARCHIVO,RE_VERTICA,US_CODIGO,RE_FILTRO,' +
                                          'RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,' +
                                          'RE_MAR_DER,RE_MAR_INF,RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,' +
                                          'QU_CODIGO,RE_CLASIFI,RE_CANDADO,RE_IFECHA',
                               'RE_CODIGO');
         end;
   end;

begin
     with oZetaProvider do
     begin
          SetTablaInfoReporte;
          TablaInfo.BeforeUpdateRecord := BeforeUpdateReporte;
          TablaInfo.OnUpdateError := ErrorenUpdate;
          GrabaTabla(EmpresaActiva, cdsEditReporte.Delta, ErrorCount);
          Result := (ErrorCount = 0);
          if Result then
          begin
               SetTablaInfoCampoRep;
               TablaInfo.BeforeUpdateRecord := BeforeUpdateCampoRep;
               GrabaTabla(EmpresaActiva, cdsCampoRep.Delta, ErrorCount);
               Result := (ErrorCount = 0);
         end;
     end;
end;


function TdmImportarReporte.GetMaxReporte: Integer;
const
  K_MAX_REPORTE = 'SELECT MAX(RE_CODIGO) MAXIMO FROM REPORTE';
var
  FDataSet: TZetaCursor;
begin
  with oZetaProvider do try
    // CV: CreateQuery--ok
    FDataSet := CreateQuery(K_MAX_REPORTE);
    FDataSet.Open;
    Result := FDataSet.Fields[0].AsInteger + 1;
    FDataSet.Close;
  finally
    FreeAndNil(FDataSet);
  end;
end;

procedure TdmImportarReporte.cdsEditReporteBeforePost(DataSet: TDataSet);
begin
  with cdsEditReporte do begin
    FieldByName('RE_FECHA').AsDateTime := Now;
    FieldByName('US_CODIGO').AsInteger := oZetaProvider.UsuarioActivo; // dmCliente.Usuario;
    with FieldByName('RE_COPIAS') do
      AsInteger := iMax(AsInteger, 1);
  end;
end;

procedure TdmImportarReporte.cdsEditReporteNewRecord(DataSet: TDataSet);
begin
  with cdsEditReporte do begin
    Randomize;
    FieldByName('RE_TIPO').AsInteger := 0;
    FieldByName('RE_CLASIFI').AsInteger := 0;
    FieldByName('RE_NOMBRE').AsString   := 'Reporte Nuevo ' + IntToStr(Random(9999));
    FieldByName('RE_TITULO').AsString   := 'T�tulo ' + FieldByName('RE_NOMBRE').AsString;
    FieldByName('RE_VERTICA').AsString  := K_GLOBAL_NO;
    FieldByName('RE_SOLOT').AsString    := K_GLOBAL_NO;
    FieldByName('RE_PFILE').AsInteger   := Ord(tfImpresora);
    FieldByName('RE_REPORTE').AsString  := K_TEMPLATE;
    FieldByName('RE_COPIAS').AsInteger  := 1;
    FieldByName('RE_PRINTER').AsString  := K_IMP_DEFAULT;
    FieldByName('RE_GENERAL').AsString  := '';
    FieldByName('RE_COLNUM').AsInteger  := 0;
    FieldByName('RE_MAR_SUP').AsInteger := 0;
    FieldByName('RE_ANCHO').AsInteger   := 66;
    FieldByName('RE_RENESPA').AsInteger := 66;
    FieldByName('RE_COLESPA').AsInteger := 0;
    FieldByName('RE_ALTO').AsInteger    := 0;
    FieldByName('RE_HOJA').AsInteger    := 0;
    FieldByName('RE_CANDADO').AsInteger := Ord(cnAbierto);
    FieldByName('RE_FONTSIZ').AsInteger := 0;
  end;
end;

procedure TdmImportarReporte.cdsEditReporteReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if {(E.ErrorCode = 1) and} (Pos('DUPLICATE', UpperCase(E.Message)) > 0) then begin
        E.Message := 'El Nombre del Reporte est� Repetido' + sArchivo;
        sError := E.Message;
        oZetaProviderLog.Log.Advertencia(clbMotorPatchImportarReporte, 0, Date(), sError, '');
        Action := raCancel;
     end;
//     Action      := ZReconcile.HandleReconcileError(DataSet, UpdateKind, E);
end;

procedure TdmImportarReporte.ErrorEnUpdate(Sender: TObject; DataSet: TzProviderClientDataset; E: EUpdateError;
  UpdateKind: TUpdateKind; var Response: TResolverResponse);
begin
     if (Pos('DUPLICATE', UpperCase(E.Message)) > 0) then
     begin
        E.Message := 'El Nombre del Reporte est� Repetido ' + sArchivo;
        sError := E.Message;
        oZetaProviderLog.Log.Advertencia(clbMotorPatchImportarReporte, 0, Date(), sError, '');
     end;
     Response := rrSkip;
end;

end.
