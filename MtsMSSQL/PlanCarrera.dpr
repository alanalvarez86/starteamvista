library PlanCarrera;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}

uses
  MidasLib,
  ComServ,
  DServerPlanCarrera in '..\MTS\DServerPlanCarrera.pas' {dmServerPlanCarrera: TMtsDataModule} {dmServerPlanCarrera: CoClass},
  PlanCarrera_TLB in '..\MTS\PlanCarrera_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\PlanCarrera.TLB}

{$R *.RES}

begin
end.
