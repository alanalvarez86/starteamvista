library GlobalVisitantes;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DServerGlobalVisitantes in '..\MTS\DServerGlobalVisitantes.pas' {dmServerGlobalVisitantes: TMtsDataModule} {dmServerGlobalVisitantes: CoClass},
  GlobalVisitantes_TLB in '..\MTS\GlobalVisitantes_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\GlobalVisitantes.TLB}

{$R *.RES}

begin
end.
