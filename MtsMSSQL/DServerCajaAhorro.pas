unit dServerCajaAhorro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {BdeMts,} DataBkr, DBClient,
  MtsRdm, Mtx,
  Variants,
  {$ifndef DOS_CAPAS}
  CajaAhorro_TLB,
  {$endif}
  DZetaServerProvider, DB, ZetaSQLBroker,ZCreator, ZetaServerDataSet, DPrestamosServer;

type
  eTipoTabla = ( eTAhorro,           //0
                 eTPresta,           //1
                 eCtasBancarias,     //2
                 eTiposDeposito,     //3
                 eAhorro,            //4
                 eRegPrestamos,      //5
                 eCtasMovimientos,   //6
                 eACarAbo,           //7
                 ePCarAbo,
                 ePeriodos );
type
  TdmServerCajaAhorro = class(TMtsDataModule {$ifndef DOS_CAPAS}, IdmServerCajaAhorro {$endif} )
    cdsTemporal: TServerDataSet;
    cdsDataset: TServerDataSet;
    cdsTotales: TServerDataSet;
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FCursorGral: TZetaCursor;
      // Cursor de uso general
    {$ifndef DOS_CAPAS}
    oZetaCreator: TZetaCreator;
    {$endif}
    //procedure InitCreator;
    //procedure InitQueries;

    procedure SetTablaInfo(const eTabla: eTipoTabla);
    procedure AfterUpdateAhorros(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure AfterUpdatePrestamos(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
    procedure BeforeUpdatePrestamos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);

  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    //procedure AfterUpdateCuentas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
    function GetTiposAhorro(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;  {$endif}
    function GetCatalogo(Empresa: OleVariant; iTabla: Integer; const Filtro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCatalogo(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetNominaEmpleado(Empresa: OleVariant; Empleado: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function InitRegPrestamos(Empresa: OleVariant;out CtasMovs: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function ValidaCheque(Empresa: OleVariant; const CtaBancaria: WideString; Cheque: Integer): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSiguienteCheque(Empresa: OleVariant;const CtaBancaria: WideString): Integer; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaRegPrestamo(Empresa, oDelta, oDeltaMov, Parametros: OleVariant; out ErrorCount, ErrorCountMov: Integer; out ResultsMov: OleVariant; ForzarGrabarPrestamos: WordBool; var MensajePrestamo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetLiquidaAhorro(Empresa: OleVariant; iEmpleado: Integer; const sTipoAhorro: WideString; out CtasMovs: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    procedure RegistrarLiquidacion(Empresa, LiquidaAhorro, CtasMovs: OleVariant); {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCuentasMovtos(Empresa: OleVariant; const sFiltro: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaCuentasMovtos(Empresa, Delta: OleVariant;out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetAhorro(Empresa: OleVariant; Empleado: Integer;  const TipoAhorro: WideString; out Prestamos,  ACarAbo: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSaldosAhorro(Empresa, Delta, DeltaACarAbo: OleVariant; var ErrorCount, ErrorCountCarAbo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSaldosPrestamo(Empresa, Delta, DeltaPCarAbo,Parametros: OleVariant; out ErrorCount, ErrorCountCarAbo: Integer; ForzarGrabarPrestamos: WordBool; var MensajePrestamo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTotalesCajaFondo(Empresa: OleVariant; const TipoAhorro: WideString; var Cuentas: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCuentasBancarias(Empresa, Parametros: OleVariant; var SaldoInicial: Double): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTotalesRetenciones(Empresa,Parametros: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetPeriodos(Empresa: OleVariant; Year, Tipo: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetCtasBancarias(Empresa: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
  end;

var
  dmServerPrestamos : TPrestamosServer;   { Es un var en un dll, �esta bien? }

implementation

{$R *.DFM}

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     DQueries,
     ZetaServerTools;

const
     K_FILTRO_NO_CTAS_MOVS = '( CM_FOLIO = 0 )';

type
    eScript=( Q_TIPOAHORRO,
              Q_SIGCHEQUE,
              Q_AHORRO_LIQUIDA,
              Q_PRESTAMO_LIQUIDA,
              Q_CTA_MOVS_LIQUIDA,
              Q_INSERT_CTAMOVS_LIQUIDA,
              Q_INSERT_ACAR_ABO,
              Q_INSERT_PCAR_ABO,
              Q_UPDATE_AHORRO,
              Q_UPDATE_PRESTAMO,
              Q_AHORRO,
              Q_AFTER_UPDATE_AHORRO,
              Q_PRESTAMO,
              Q_AFTER_UPDATE_PRESTAMO,
              Q_TOTAL_AHORRADO,
              Q_SALDO_PRESTAMOS,
              Q_SALDO_BANCOS,
              Q_CUENTAS,
              Q_SALDO_CTA,
              Q_MOVIMIENTOS,
              Q_TOTALES_RENTENCION_AHORRO,
              Q_TOTALES_RENTENCION_PRESTAMO,
              Q_NOMINA_EMPLEADO,
              Q_VALIDA_CHEQUE,
              Q_AFTER_UPDATE_PRESTAMO_CTA_MOVS,
              Q_UPDATE_PRESTAMO_ESTATUS
            );

function GetSQLScript( const Script: eScript ): String;
begin
     case Script of
          Q_TIPOAHORRO: Result := 'select TB_CODIGO, TB_ELEMENT, TB_PRESTA, TB_CONCEPT, TB_RELATIV, TB_ACTIVO, TB_NIVEL0 from TAHORRO where TB_ACTIVO = ''S'' order by TB_CODIGO';
          Q_SIGCHEQUE: Result :=
                                {$ifdef INTERBASE}
                                'select RESULTADO FROM SIGUIENTE_CHEQUE( %s )';
                                {$ELSE}
                                'select RESULTADO = dbo.SIGUIENTE_CHEQUE( %s )';
                                {$ENDIF}
          Q_AHORRO_LIQUIDA: Result := 'select CB_CODIGO, AH_TIPO, AH_SALDO, AH_FECHA, AH_STATUS, TAHORRO.TB_PRESTA, TAHORRO.TB_RELATIV,'+
                                      'AH_ABONOS, AH_CARGOS, AH_SALDO_I, AH_TOTAL, 0.0 as TASA, 0.0 as DURACION, ' +
                                      '(SELECT SUM(PR_SALDO)  '+
                                      'FROM PRESTAMO '+
                                      'WHERE PRESTAMO.CB_CODIGO=AHORRO.CB_CODIGO '+
                                      'AND PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                                      'AND PR_STATUS = 0) PR_SALDO, '+
                                      '( select AC_ANUAL from ACUMULA where '+
                                         '( AC_YEAR = %d ) and '+
                                         '( CB_CODIGO = %d ) and '+
                                         '( CO_NUMERO = TAHORRO.TB_RELATIV ) ) C_RELATIVO ' +
                                      'from AHORRO '+
                                      'left outer join TAHORRO on AHORRO.AH_TIPO = TAHORRO.TB_CODIGO  '+
                                      'where AHORRO.CB_CODIGO = %d and AHORRO.AH_TIPO = %s ';
                                        {PE_YEAR,PE_TIPO,PE_NUMERO,}
          Q_CTA_MOVS_LIQUIDA: Result := 'select CB_CODIGO,CM_FOLIO,CM_FECHA,CM_TOT_AHO,CM_INTERES,CM_SAL_PRE, '+
                                        'CM_MONTO,CT_CODIGO,CM_CHEQUE,CM_DESCRIP, CM_TIPO, '+
                                        'CM_DEP_RET,CM_BENEFI,CM_STATUS,CM_DEPOSIT,CM_RETIRO,'+
                                        '0.0 as CM_AFAVOR, 0.0 as CM_DISPONIBLE, '' '' as PRETTYNAME, '+
                                        '0.0 as CM_TOT_AHO_EMP, 0.0 as CM_INTERES_EMP, 0.0 as CM_AFAVOR_EMP, '+
                                        '0.0 as CM_DISPONIBLE_EMP, 0.0 as CM_TOTAL_DISPONIBLE '+
                                        'from CTA_MOVS '+
                                        'WHERE CM_FOLIO = 0 ';
          Q_PRESTAMO_LIQUIDA: Result := 'select CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_MONTO, PR_SALDO, PR_PAGOS '+
                                        'from PRESTAMO '+
                                        'left outer join TPRESTA on PRESTAMO.PR_TIPO = TPRESTA.TB_CODIGO '+
                                        'where PRESTAMO.CB_CODIGO = %d and PR_TIPO = %s and PR_STATUS = 0 '+
                                        'order by PR_FECHA' ;
          Q_INSERT_PCAR_ABO: Result := 'insert into PCAR_ABO ( CB_CODIGO, CR_CAPTURA,PR_TIPO, '+
                                                      'CR_FECHA,PR_REFEREN,CR_OBSERVA, '+
                                                      'CR_ABONO,CR_CARGO,US_CODIGO ) '+
                                           'VALUES( :CB_CODIGO,:CR_CAPTURA,:PR_TIPO, '+
                                                   ':CR_FECHA,:PR_REFEREN,:CR_OBSERVA, '+
                                                   ':CR_ABONO,:CR_CARGO,:US_CODIGO )';
          Q_INSERT_CTAMOVS_LIQUIDA: Result := 'insert into CTA_MOVS ( CB_CODIGO, CM_FECHA,CM_TOT_AHO,CM_INTERES,CM_SAL_PRE, '+
                                                  'CM_MONTO,CT_CODIGO,CM_CHEQUE,CM_DESCRIP, CM_TIPO, '+
                                                  'CM_DEP_RET,CM_BENEFI,CM_STATUS ) '+
                                          'VALUES( :CB_CODIGO, :CM_FECHA,:CM_TOT_AHO,:CM_INTERES,:CM_SAL_PRE, '+
                                                  ':CM_MONTO,:CT_CODIGO,:CM_CHEQUE,:CM_DESCRIP, :CM_TIPO, '+
                                                  ':CM_DEP_RET,:CM_BENEFI,:CM_STATUS )';
          Q_INSERT_ACAR_ABO: Result := 'insert into ACAR_ABO( AH_TIPO,	CB_CODIGO,	CR_CAPTURA,	CR_ABONO, '+
                  	                                 'CR_FECHA,	CR_CARGO,	CR_OBSERVA,	US_CODIGO ) '+
                                           'VALUES( :AH_TIPO,	:CB_CODIGO,	:CR_CAPTURA,	:CR_ABONO, '+
                                                   ':CR_FECHA,	:CR_CARGO,	:CR_OBSERVA,	:US_CODIGO ) ';
          Q_UPDATE_AHORRO: Result :=  'update AHORRO set ' +
                                      'AH_STATUS = :AH_STATUS, '+
                                      'AH_ABONOS  = :AH_ABONOS, '+
                                      'AH_CARGOS  = :AH_CARGOS, '+
                                      'AH_SALDO   = :AH_SALDO '+
                                      'where CB_CODIGO = :CB_CODIGO AND AH_TIPO = :AH_TIPO ';
          Q_AFTER_UPDATE_AHORRO: Result :=  'update AHORRO set ' +
                                            'AH_SALDO = AH_SALDO_I + AH_TOTAL + AH_ABONOS-AH_CARGOS ' +
                                            'where CB_CODIGO = %s AND AH_TIPO = %s ';

          Q_UPDATE_PRESTAMO: Result :=
                                       {$IFDEF INTERBASE}
                                       'update PRESTAMO set '+
                                       'PR_CARGOS = ( SELECT RESULTADO FROM ES_NULO_NUMERICO((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0)), '+
                                       'PR_ABONOS = ( SELECT RESULTADO FROM ES_NULO_NUMERICO((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0)), '+
                                       'PR_SALDO = ( PR_MONTO - PR_SALDO_I - PR_TOTAL - ( SELECT RESULTADO FROM ES_NULO_NUMERICO((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0)) + (SELECT RESULTADO FROM ES_NULO_NUMERICO((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0)) ) '+
                                       'where ( CB_CODIGO = :Empleado ) and ( PR_TIPO = :Tipo ) and ( PR_REFEREN = :Referencia )';
                                       {$ELSE}
                                       'update PRESTAMO set '+
                                       'PR_CARGOS = ISNULL((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0), '+
                                       'PR_ABONOS = ISNULL((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0), '+
                                       'PR_SALDO = ( PR_MONTO - PR_SALDO_I - PR_TOTAL - ISNULL((SELECT SUM( CR_ABONO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0) + ISNULL((SELECT SUM( CR_CARGO ) FROM PCAR_ABO C '+
                                       'WHERE ( C.CB_CODIGO = PRESTAMO.CB_CODIGO ) and ( C.PR_TIPO = PRESTAMO.PR_TIPO ) and ( C.PR_REFEREN = PRESTAMO.PR_REFEREN ) ),0) ) '+
                                       'where ( CB_CODIGO = :Empleado ) and ( PR_TIPO = :Tipo ) and ( PR_REFEREN = :Referencia )';
                                       {$ENDIF}
          Q_AFTER_UPDATE_PRESTAMO: Result := 'update PRESTAMO set '+
                                       'PR_SALDO = ( PR_MONTO - PR_SALDO_I - PR_TOTAL - PR_ABONOS + PR_CARGOS ) '+
                                       'where ( CB_CODIGO = %s ) and ( PR_TIPO = %s ) and ( PR_REFEREN = %s )';


          Q_UPDATE_PRESTAMO_ESTATUS: Result := 'update PRESTAMO set '+
                                                      'PR_STATUS = %d '+
                                                      'where ( CB_CODIGO = %d ) and ( PR_TIPO = %s ) and ( PR_REFEREN = %s )';

          Q_AHORRO: Result := 'select TAHORRO.TB_PRESTA, '+
                              'AH_ABONOS,AH_CARGOS,AH_FECHA,AH_FORMULA,AH_NUMERO,AH_SALDO_I,AH_STATUS, '+
                              'AH_SALDO,AH_TIPO,AH_TOTAL,US_CODIGO,CB_CODIGO, AH_SUB_CTA, '+
                              '(SELECT SUM(PR_SALDO)  '+
                              'FROM PRESTAMO '+
                              'WHERE PRESTAMO.CB_CODIGO=AHORRO.CB_CODIGO '+
                              'AND PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                              'AND PR_STATUS = 0) PR_SALDO, '+
                              {$ifdef INTERBASE}
                              '(AH_SALDO - ( select Resultado from ES_NULO_NUMERICO(  '+
                                          '( SELECT SUM(PR_SALDO) FROM PRESTAMO  '+
                                            'WHERE PRESTAMO.CB_CODIGO=AHORRO.CB_CODIGO '+
                                            'AND PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                                            'AND PR_STATUS = 0),0) ) ) AH_NETO '+
                              {$ELSE}
                              '(AH_SALDO - isnull((SELECT SUM(PR_SALDO) '+
                                           'FROM PRESTAMO '+
                                           'WHERE PRESTAMO.CB_CODIGO=AHORRO.CB_CODIGO AND '+
                                           'PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA AND PR_STATUS = 0),0) ) AH_NETO '+
                              {$ENDIF}
                              'from AHORRO '+
                              'left outer join TAHORRO on AHORRO.AH_TIPO = TAHORRO.TB_CODIGO  '+
                              'where AHORRO.CB_CODIGO = %d and AHORRO.AH_TIPO = %s ';
          {
          Q_PRESTAMO: Result := 'select PR_ABONOS,PR_CARGOS,PR_FECHA,PR_FORMULA,PR_MONTO,PR_NUMERO,PR_REFEREN,PR_SALDO_I,PR_STATUS,PR_TIPO,PR_TOTAL,PR_SALDO,US_CODIGO,CB_CODIGO,PR_MONTO_S,PR_INTERES,PR_TASA,PR_MESES,PR_PAGOS,PR_PAG_PER, '+
                                'TPRESTA.TB_ELEMENT,TAHORRO.TB_CODIGO AH_TIPO '+
                                'from PRESTAMO '+
                                'left outer join TAHORRO on TAHORRO.TB_CODIGO = %s '+
                                'left outer join TPRESTA on TPRESTA.TB_CODIGO = TAHORRO.TB_PRESTA '+
                                'where PRESTAMO.CB_CODIGO = %d and PR_STATUS = 0 AND PR_TIPO = TAHORRO.TB_PRESTA ' +
                                'order by PR_FECHA';
          }
          Q_PRESTAMO: Result := 'select PRESTAMO.PR_ABONOS, PRESTAMO.PR_CARGOS, PRESTAMO.PR_FECHA, PRESTAMO.PR_FORMULA, PRESTAMO.PR_MONTO, PRESTAMO.PR_NUMERO, PRESTAMO.PR_REFEREN, '+
                                'PRESTAMO.PR_SALDO_I, PRESTAMO.PR_STATUS, PRESTAMO.PR_TIPO, PRESTAMO.PR_TOTAL, PRESTAMO.PR_SALDO, PRESTAMO.US_CODIGO, PRESTAMO.CB_CODIGO, '+
                                'PRESTAMO.PR_MONTO_S, PRESTAMO.PR_INTERES, PRESTAMO.PR_TASA, PRESTAMO.PR_MESES, PRESTAMO.PR_PAGOS, PRESTAMO.PR_PAG_PER, PRESTAMO.PR_OBSERVA, '+
                                'TPRESTA.TB_ELEMENT,TAHORRO.TB_CODIGO AH_TIPO, PRESTAMO.PR_SUB_CTA, CTA_MOVS.CM_PRESTA '+
                                'from PRESTAMO '+
                                'left outer join TAHORRO on TAHORRO.TB_CODIGO = %s '+
                                'left outer join TPRESTA on TPRESTA.TB_CODIGO = TAHORRO.TB_PRESTA '+
                                'left outer join CTA_MOVS on PRESTAMO.PR_TIPO = CTA_MOVS.PR_TIPO and PRESTAMO.PR_REFEREN = CTA_MOVS.PR_REFEREN and PRESTAMO.PR_FECHA = CTA_MOVS.CM_FECHA and CTA_MOVS.CB_CODIGO = %d '+
                                'where PRESTAMO.CB_CODIGO = %d and PRESTAMO.PR_STATUS = 0 AND PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                                'order by PRESTAMO.PR_FECHA';

          //Folio 2:Totales de caja/fondo
          //1)
          Q_TOTAL_AHORRADO:    Result := 'select COUNT(*) CUANTOS_SOCIOS, SUM (AH_SALDO) TOTAL_AHORRADO '+
                                        'from AHORRO '+
                                        'left outer join TAHORRO on AHORRO.AH_TIPO= TAHORRO.TB_CODIGO '+
                                        'where TAHORRO.TB_CODIGO = %s and AHORRO.AH_STATUS = 0 ';
          //2)
          Q_SALDO_PRESTAMOS:   Result := 'select COUNT(*) NUM_PRESTAMOS, SUM (PR_SALDO) SALDO_PRESTAMOS '+
                                        'from PRESTAMO '+
                                        'left outer join TAHORRO on PRESTAMO.PR_TIPO = TAHORRO.TB_PRESTA '+
                                        'where TAHORRO.TB_CODIGO = %s and PR_STATUS = 0 ';
          //3)
          Q_SALDO_BANCOS:      Result :=
                                        {$ifdef INTERBASE}
                                        'select SUM( ( select Resultado from SALDO_CTA( CTABANCO.CT_CODIGO, %s )) ) SALDO_BANCOS '+
                                        'from CTABANCO '+
                                        'where AH_TIPO = %s and CT_STATUS = 0 ';
                                        {$ELSE}
                                        'select SUM( dbo.SALDO_CTA( CT_CODIGO, %s )) SALDO_BANCOS '+
                                        'from CTABANCO '+
                                        'where AH_TIPO = %s and CT_STATUS = 0 ';
                                        {$ENDIF}
          //4)
          Q_CUENTAS:           Result :=
                                         {$ifdef INTERBASE}
                                         'select CT_CODIGO,CT_NUM_CTA,CT_NOMBRE,CT_BANCO,CT_NUMERO,CT_TEXTO, '+
                                         'AH_TIPO,CT_STATUS,CT_REP_CHK,CT_REP_LIQ, '+
                                         '( select Resultado from SALDO_CTA(CTABANCO.CT_CODIGO, %s ) ) SALDO_HOY '+
                                         'from CTABANCO '+
                                         'where AH_TIPO = %s and CT_STATUS = 0 order by CT_CODIGO ';

                                         {$ELSE}
                                         'select CT_CODIGO,CT_NUM_CTA,CT_NOMBRE,CT_BANCO,CT_NUMERO,CT_TEXTO,AH_TIPO,CT_STATUS,CT_REP_CHK,CT_REP_LIQ, dbo.SALDO_CTA(CT_CODIGO, %s ) SALDO_HOY  '+
                                         'from CTABANCO '+
                                         'where AH_TIPO = %s and CT_STATUS = 0 order by CT_CODIGO ';
                                         {$ENDIF}

          //Folio 3: Estado de cuenta bancaria
          //1)
          Q_SALDO_CTA:         Result :=
                                        {$ifdef INTERBASE}
                                        'select Resultado from SALDO_CTA(%s,%s)';
                                        {$ELSE}
                                        'select dbo.SALDO_CTA(%s,%s) SALDO_INI';
                                        {$ENDIF}

          //2)
          Q_MOVIMIENTOS:        Result := 'select CM_FOLIO,CT_CODIGO,CM_TIPO,CM_DEP_RET,CM_MONTO,CM_DESCRIP,CM_CHEQUE,CM_FECHA,CM_BENEFI,CM_STATUS,CM_TOT_AHO,CM_INTERES,CM_SAL_PRE,CB_CODIGO,PE_YEAR,PE_TIPO,PE_NUMERO,CM_DEPOSIT,CM_RETIRO, '+
                                          {$ifdef INTERBASE}
                                          'cast((CM_DESCRIP ||'' ''|| CM_BENEFI) as VarChar(255)) DESCRIPCION, '+
                                          {$ELSE}
                                          'lTrim(CM_DESCRIP +'' ''+ CM_BENEFI) DESCRIPCION, '+
                                          {$ENDIF}
                                          '0.0 SALDO, 0.0 DEPOSITOS, 0.0 RETIROS '+
                                          'from CTA_MOVS '+
                                          'left outer join TCTAMOVS on TCTAMOVS.TB_CODIGO = CM_TIPO '+
                                          'where (CT_CODIGO = %s ) and (CM_FECHA between %s and %s ) and ( CM_STATUS = %d ) %s '+
                                          'order by CM_FECHA, CM_DEP_RET ';
          Q_TOTALES_RENTENCION_AHORRO: Result := 'select COUNT(*) Num_Ahorro, SUM(MO_DEDUCCI) Suma_Ahorro '+
                                          'from MOVIMIEN '+
                                          'where PE_YEAR = %d and PE_TIPO = %d and PE_NUMERO = %d '+
                                          'and CO_NUMERO = %d and MO_ACTIVO = ''S'' ';
          Q_TOTALES_RENTENCION_PRESTAMO: Result := 'select COUNT(*) Num_Prestamos, SUM(MO_DEDUCCI) Suma_Prestamos '+
                                          'from MOVIMIEN '+
                                          'where PE_YEAR = %d and PE_TIPO = %d and PE_NUMERO = %d '+
                                          'and CO_NUMERO = %d and MO_ACTIVO = ''S''  ';
          Q_NOMINA_EMPLEADO: Result := 'select TU_NOMINA from TURNO where TU_CODIGO = ( select CB_TURNO from COLABORA where CB_CODIGO = %d )';
          Q_VALIDA_CHEQUE: Result := 'select count(*) TOTAL from CTA_MOVS where ( CT_CODIGO = %s ) and ( CM_CHEQUE = %d )';
          Q_AFTER_UPDATE_PRESTAMO_CTA_MOVS: Result := 'update CTA_MOVS set '+
                                                      'CM_MONTO = %s '+
                                                      'where ( CB_CODIGO = %s ) and ( PR_TIPO = %s ) and ( PR_REFEREN = %s )';
          //Q_CANCELAR_PRESTAMOS:Result := 'update PRESTAMO set PR_STATUS = 1 where CB_CODIGO = %d';
     end;
end;

class procedure TdmServerCajaAhorro.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerCajaAhorro.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     dmServerPrestamos:= TPrestamosServer.Create( Self );
end;

procedure TdmServerCajaAhorro.MtsDataModuleDestroy(Sender: TObject);
begin
{$ifndef DOS_CAPAS}
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     FreeAndNil(dmServerPrestamos);
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
{$endif}
end;

{ *********** Clases Auxiliares ************ }

{$ifdef DOS_CAPAS}
procedure TdmServerCajaAhorro.CierraEmpresa;
begin
end;
{$endif}

procedure TdmServerCajaAhorro.SetTablaInfo( const eTabla: eTipoTabla );
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              eTAhorro : SetInfo('TAHORRO', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_CONCEPT,TB_RELATIV,TB_LIQUIDA,TB_ALTA,TB_PRESTA,TB_TASA1,TB_TASA2,TB_TASA3,TB_VAL_RAN,TB_FEC_FIN,TB_FEC_INI,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );
              eTPresta : SetInfo('TPRESTA', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_CONCEPT,TB_RELATIV,TB_LIQUIDA,TB_ALTA,TB_TASA1,TB_TASA2,TB_TASA3,TB_ACTIVO,TB_NIVEL0', 'TB_CODIGO' );
              eCtasBancarias : SetInfo( 'CTABANCO', 'CT_CODIGO,CT_NUM_CTA,CT_NOMBRE,CT_BANCO,CT_NUMERO,CT_TEXTO,AH_TIPO,CT_STATUS,CT_REP_CHK,CT_REP_LIQ', 'CT_CODIGO' );
              eTiposDeposito : SetInfo( 'TCTAMOVS', 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO,TB_SISTEMA', 'TB_CODIGO' );
              eRegPrestamos  :
              begin
                   SetInfo('PRESTAMO','PR_ABONOS,PR_CARGOS,PR_FECHA,PR_FORMULA,PR_MONTO,PR_NUMERO,PR_REFEREN,PR_SALDO_I,PR_STATUS,PR_TIPO,PR_TOTAL,PR_SALDO,US_CODIGO,CB_CODIGO,PR_MONTO_S,PR_INTERES,PR_TASA,PR_MESES,PR_PAGOS,PR_PAG_PER,PR_SUB_CTA,PR_OBSERVA', 'CB_CODIGO,PR_TIPO,PR_REFEREN' );
                   AfterUpdateRecord := AfterUpdatePrestamos;
              end;
              eAhorro :
              begin
                   SetInfo( 'AHORRO','AH_ABONOS,AH_CARGOS,AH_FECHA,AH_FORMULA,AH_NUMERO,AH_SALDO_I,AH_STATUS,AH_SALDO,AH_TIPO,AH_TOTAL,US_CODIGO,CB_CODIGO,AH_SUB_CTA', 'CB_CODIGO,AH_TIPO' );
                   AfterUpdateRecord := AfterUpdateAhorros;
              end;
              eCtasMovimientos: SetInfo( 'CTA_MOVS', 'CM_FOLIO,CT_CODIGO,CM_TIPO,CM_DEP_RET,CM_MONTO,CM_DESCRIP,CM_CHEQUE,CM_FECHA,CM_BENEFI,CM_STATUS,CM_TOT_AHO,CM_INTERES,CM_SAL_PRE,CB_CODIGO,PE_YEAR,PE_TIPO,PE_NUMERO,CM_DEPOSIT,CM_RETIRO,PR_TIPO,PR_REFEREN,CM_PRESTA', 'CM_FOLIO' );
              eACarAbo : SetInfo( 'ACAR_ABO', 'AH_TIPO,CB_CODIGO,CR_CAPTURA,CR_ABONO,CR_FECHA,CR_CARGO,CR_OBSERVA,US_CODIGO', 'CB_CODIGO,AH_TIPO,CR_FECHA' );
              ePCarAbo : SetInfo( 'PCAR_ABO', 'CB_CODIGO,CR_CAPTURA,PR_TIPO,CR_FECHA,PR_REFEREN,CR_OBSERVA,CR_ABONO,CR_CARGO,US_CODIGO', 'CB_CODIGO,PR_TIPO,PR_REFEREN,CR_FECHA' );
              ePeriodos    : SetInfo( 'PERIODO', 'PE_YEAR,PE_TIPO,PE_NUMERO,PE_DESCRIP,PE_USO,PE_STATUS,PE_INC_BAJ, '+
                                      'PE_SOLO_EX,PE_FEC_INI,PE_FEC_FIN,PE_FEC_PAG,PE_MES,PE_DIAS,PE_DIAS_AC, '+
{$ifdef QUINCENALES}
                                      'PE_ASI_INI, PE_ASI_FIN, '+
{$endif}
                                      'PE_DIA_MES,PE_POS_MES,PE_PER_MES,PE_PER_TOT,PE_FEC_MOD,PE_AHORRO,PE_PRESTAM, '+
                                      'PE_NUM_EMP,PE_TOT_PER,PE_TOT_NET,PE_TOT_DED,US_CODIGO', 'PE_YEAR,PE_TIPO,PE_NUMERO' );

          end;
     end;
end;

function TdmServerCajaAhorro.GetTiposAhorro(Empresa: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, GetSQLScript( Q_TIPOAHORRO ), True );
     SetComplete;
end;

function TdmServerCajaAhorro.GetCatalogo(Empresa: OleVariant; iTabla: Integer; const Filtro: WideString): OleVariant;
begin
     try
        SetTablaInfo( eTipoTabla( iTabla ) );
        with oZetaProvider do
        begin
             TablaInfo.Filtro := Filtro;
             Result := GetTabla( Empresa );
        end;
     except
        SetOLEVariantToNull( Result );
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GrabaCatalogo(Empresa: OleVariant; iTabla: Integer; oDelta: OleVariant;
         out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eTipoTabla( iTabla ) );
     Result := oZetaProvider.GrabaTabla( Empresa, oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerCajaAhorro.GetSiguienteCheque(Empresa: OleVariant; const CtaBancaria: WideString): Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral:= CreateQuery;
          try
             AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_SIGCHEQUE ), [ EntreComillas( CtaBancaria ) ] ) );
             Result := FCursorGral.FieldByName( 'RESULTADO' ).AsInteger;
          finally
             FreeAndNil( FCursorGral );
          end;
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.InitRegPrestamos(Empresa: OleVariant; out CtasMovs: OleVariant): OleVariant;
const
     K_FILTRO_NO_PRESTAMOS = '( CB_CODIGO = 0 ) and ( PR_TIPO = '''' ) and ( PR_REFEREN = '''' )';
begin
     try
        SetTablaInfo( eRegPrestamos );
        with oZetaProvider do
        begin
             TablaInfo.Filtro := K_FILTRO_NO_PRESTAMOS;
             Result := GetTabla( Empresa );
        end;
        SetTablaInfo( eCtasMovimientos );
        with oZetaProvider do
        begin
             TablaInfo.Filtro := K_FILTRO_NO_CTAS_MOVS;
             CtasMovs := GetTabla( Empresa );
        end;
     except
        SetOLEVariantToNull( Result );
        SetOLEVariantToNull( CtasMovs );
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GrabaRegPrestamo(Empresa, oDelta, oDeltaMov, Parametros: OleVariant;
         out ErrorCount, ErrorCountMov: Integer; out ResultsMov: OleVariant; ForzarGrabarPrestamos: WordBool;
         var MensajePrestamo: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );

          SetTablaInfo( eRegPrestamos );
          try
             TablaInfo.BeforeUpdateRecord := BeforeUpdatePrestamos;
             dmServerPrestamos.ZetaProvider.EmpresaActiva := EmpresaActiva;
             dmServerPrestamos.ZetaProvider.AsignaParamList( oZetaProvider.ParamList.VarValues );

             dmServerPrestamos.ForzarGrabaPrestamo := ForzarGrabarPrestamos;

             //TablaInfo.AfterUpdateRecord := AfterUpdateCuentas;
             Result := GrabaTabla( Empresa, oDelta, ErrorCount );
             if ( ErrorCount = 0 ) and ( not VarIsNull( oDeltaMov ) ) then
             begin
                  SetTablaInfo( eCtasMovimientos );
                  ResultsMov := GrabaTabla( Empresa, oDeltaMov, ErrorCountMov );
             end
             else
                 ResultsMov := oDeltaMov;
          finally
                 MensajePrestamo := dmServerPrestamos.MensajePrestamo;
          end;
     end;
end;
{
procedure TdmServerCajaAhorro.AfterUpdateCuentas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
begin
     if ( UpdateKind <> ukDelete ) then
     begin
          with oZetaProvider do
          begin
               FCursorGral := CreateQuery;

               //Se agrega un registro en CTA_MOVS
               PreparaQuery( FCursorGral, GetSQLScript( Q_INSERT_CTAMOVS_LIQUIDA ) );
               with DeltaDS do
               begin
                    ParamAsInteger( FCursorGral, 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger );
                    ParamAsDate( FCursorGral, 'CM_FECHA',     FieldByName('PR_FECHA').AsDateTime );
                    ParamAsFloat( FCursorGral, 'CM_TOT_AHO',  0  );
                    ParamAsFloat( FCursorGral, 'CM_INTERES',  FieldByName('CM_INTERES').AsFloat  );
                    ParamAsFloat( FCursorGral, 'CM_SAL_PRE',  FieldByName('CM_SAL_PRE').AsFloat  );
                    ParamAsFloat( FCursorGral, 'CM_MONTO',    FieldByName('CM_MONTO').AsFloat    );
                    ParamAsString( FCursorGral, 'CT_CODIGO',  FieldByName('CT_CODIGO').AsString  );
                    ParamAsInteger( FCursorGral, 'CM_CHEQUE', FieldByName('CM_CHEQUE').AsInteger );
                    ParamAsString( FCursorGral, 'CM_DESCRIP', FieldByName('CM_DESCRIP').AsString );
                    ParamAsString( FCursorGral, 'CM_TIPO',    FieldByName('CM_TIPO').AsString    );
                    ParamAsString( FCursorGral, 'CM_DEP_RET', FieldByName('CM_DEP_RET').AsString );
                    ParamAsString( FCursorGral, 'CM_BENEFI',  FieldByName('CM_BENEFI').AsString  );
                    ParamAsInteger( FCursorGral, 'CM_STATUS', FieldByName('CM_STATUS').AsInteger );
               end;
               try
                  Ejecuta( FCursorGral );
               except
                     on Error: Exception do
                     begin
                          if ( pos( 'XAK1', Error.Message ) > 0 ) then
                             DatabaseError( 'MARCO' );
                     end;

               end;
               FreeAndNil(FCursorGral);
          end;
     end;
end;
}
function TdmServerCajaAhorro.GetLiquidaAhorro(Empresa: OleVariant; iEmpleado: Integer; const sTipoAhorro: WideString;
  out CtasMovs: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
//          FCursorGral:= CreateQuery;

          Result := OpenSQL( Empresa, Format( GetSQLScript( Q_AHORRO_LIQUIDA ), [ TheYear( Now ),
                                                                                  iEmpleado,
                                                                                  iEmpleado,
                                                                                  EntreComillas( sTipoAhorro ) ] ), True );
          CtasMovs := OpenSQL( Empresa, GetSQLScript( Q_CTA_MOVS_LIQUIDA ), TRUE );
     end;
     SetComplete;
end;

procedure TdmServerCajaAhorro.RegistrarLiquidacion(Empresa, LiquidaAhorro, CtasMovs: OleVariant);
var
  dAbonos, dCargos: TPesos;
  sObservaciones  : string;
  FPrestamo       : TZetaCursor;
  FPrestamoUpdate : TZetaCursor;


  OldStatus, NewStatus : eStatusPrestamo;
  oPrestamos: OleVariant;
  oACarAbo: OleVariant;
  iEmpleado, iEstatus : integer;
  sTipoPrestamo : string;
  sReferencia : string;

  procedure ActualizarEstatusPrestamo;
  begin
       GetAhorro( Empresa, iEmpleado, sTipoPrestamo, oPrestamos, oACarAbo);
       cdsTemporal.Data := oPrestamos;
       with cdsTemporal do
       begin
            OldStatus:= eStatusPrestamo( FieldByName( 'PR_STATUS' ).AsInteger );

            NewStatus:= OldStatus;
            if ( OldStatus <> spCancelado ) then
            begin
                 if ( FieldByName( 'PR_MONTO' ).AsFloat >= 0 ) then
                 begin
                      if ( FieldByName( 'PR_SALDO' ).AsFloat <= 0 ) then
                           NewStatus:= spSaldado
                      else if ( OldStatus <> spCancelado ) then
                           NewStatus:= spActivo;
                 end
                 else
                 begin
                      if ( FieldByName( 'PR_SALDO' ).AsFloat >= 0 ) then
                           NewStatus:= spSaldado
                      else if ( OldStatus <> spCancelado ) then
                           NewStatus:= spActivo;
                 end;
                 if ( OldStatus <> NewStatus ) then
                 begin
                      iEstatus :=  Ord( NewStatus );
                      oZetaProvider.ExecSQL( Empresa, Format( GetSQLScript( Q_UPDATE_PRESTAMO_ESTATUS ), [ iEstatus,
                                             iEmpleado,
                                             EntreComillas( sTipoPrestamo ),
                                             EntreComillas( sReferencia ) ]  ) );
                 end;
            end;
       end;
  end;

begin
  cdsDataset.Data := LiquidaAhorro;
  cdsTemporal.Data := CtasMovs;
  with oZetaProvider do begin
    EmpresaActiva := Empresa;
    EmpiezaTransaccion;
    try
      FCursorGral := CreateQuery;

      // Se agrega un registro en CTA_MOVS
      PreparaQuery(FCursorGral, GetSQLScript(Q_INSERT_CTAMOVS_LIQUIDA));
      with cdsTemporal do begin
        ParamAsInteger(FCursorGral, 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger);
        ParamAsDate(FCursorGral, 'CM_FECHA', FieldByName('CM_FECHA').AsDateTime);
        ParamAsFloat(FCursorGral, 'CM_TOT_AHO', FieldByName('CM_TOT_AHO').AsFloat +
            FieldByName('CM_TOT_AHO_EMP').AsFloat);
        ParamAsFloat(FCursorGral, 'CM_INTERES', FieldByName('CM_INTERES').AsFloat +
            FieldByName('CM_INTERES_EMP').AsFloat);
        ParamAsFloat(FCursorGral, 'CM_SAL_PRE', FieldByName('CM_SAL_PRE').AsFloat);
        ParamAsFloat(FCursorGral, 'CM_MONTO', FieldByName('CM_MONTO').AsFloat);
        ParamAsString(FCursorGral, 'CT_CODIGO', FieldByName('CT_CODIGO').AsString);
        ParamAsInteger(FCursorGral, 'CM_CHEQUE', FieldByName('CM_CHEQUE').AsInteger);
        ParamAsString(FCursorGral, 'CM_DESCRIP', FieldByName('CM_DESCRIP').AsString);
        ParamAsString(FCursorGral, 'CM_TIPO', FieldByName('CM_TIPO').AsString);
        ParamAsString(FCursorGral, 'CM_DEP_RET', FieldByName('CM_DEP_RET').AsString);
        ParamAsString(FCursorGral, 'CM_BENEFI', FieldByName('CM_BENEFI').AsString);
        ParamAsInteger(FCursorGral, 'CM_STATUS', FieldByName('CM_STATUS').AsInteger);
      end;
      Ejecuta(FCursorGral);

      // Se agrega un registro por el monto retirado o liquidado
      PreparaQuery(FCursorGral, GetSQLScript(Q_INSERT_ACAR_ABO));
      with cdsTemporal do begin
        if (FieldByName('CM_TIPO').AsString = 'LIQUID') then
          sObservaciones := 'Liquidaci�n'
        else
          sObservaciones := 'Retiro parcial';

        ParamAsString(FCursorGral, 'AH_TIPO', cdsDataset.FieldByName('AH_TIPO').AsString);
        ParamAsInteger(FCursorGral, 'CB_CODIGO', cdsDataset.FieldByName('CB_CODIGO').AsInteger);
        ParamAsDate(FCursorGral, 'CR_CAPTURA', Date);
        ParamAsDate(FCursorGral, 'CR_FECHA', FieldByName('CM_FECHA').AsDateTime);
        ParamAsFloat(FCursorGral, 'CR_ABONO', 0.0);

        if (cdsTemporal.FieldByName('CM_TIPO').AsString = 'LIQUID') then
          ParamAsFloat(FCursorGral, 'CR_CARGO', FieldByName('CM_DISPONIBLE').AsFloat)
        else
          ParamAsFloat(FCursorGral, 'CR_CARGO', FieldByName('CM_MONTO').AsFloat);
        ParamAsString(FCursorGral, 'CR_OBSERVA', sObservaciones);
        ParamAsInteger(FCursorGral, 'US_CODIGO', UsuarioActivo);
      end;
      Ejecuta(FCursorGral);

      // Si es liquidacion se agrega un registro por los intereses ganados
      PreparaQuery(FCursorGral, GetSQLScript(Q_INSERT_ACAR_ABO));
      with cdsTemporal do begin
        if (FieldByName('CM_TIPO').AsString = 'LIQUID') then begin
          ParamAsString(FCursorGral, 'AH_TIPO', cdsDataset.FieldByName('AH_TIPO').AsString);
          ParamAsInteger(FCursorGral, 'CB_CODIGO', cdsDataset.FieldByName('CB_CODIGO').AsInteger);
          ParamAsDate(FCursorGral, 'CR_CAPTURA', Date);
          ParamAsDate(FCursorGral, 'CR_FECHA', FieldByName('CM_FECHA').AsDateTime + 1);
          ParamAsFloat(FCursorGral, 'CR_ABONO', FieldByName('CM_INTERES').AsFloat);
          ParamAsFloat(FCursorGral, 'CR_CARGO', 0);
          ParamAsString(FCursorGral, 'CR_OBSERVA', 'Intereses ganados');
          ParamAsInteger(FCursorGral, 'US_CODIGO', UsuarioActivo);
          Ejecuta(FCursorGral);
        end;
      end;

      // Si es liquidacion se agrega un registro por el prestamo descontado
      PreparaQuery(FCursorGral, GetSQLScript(Q_INSERT_ACAR_ABO));
      with cdsTemporal do begin
        if (FieldByName('CM_TIPO').AsString = 'LIQUID') then begin
          ParamAsString(FCursorGral, 'AH_TIPO', cdsDataset.FieldByName('AH_TIPO').AsString);
          ParamAsInteger(FCursorGral, 'CB_CODIGO', cdsDataset.FieldByName('CB_CODIGO').AsInteger);
          ParamAsDate(FCursorGral, 'CR_CAPTURA', Date);
          ParamAsDate(FCursorGral, 'CR_FECHA', FieldByName('CM_FECHA').AsDateTime + 2);
          ParamAsFloat(FCursorGral, 'CR_ABONO', 0);
          ParamAsFloat(FCursorGral, 'CR_CARGO', FieldByName('CM_SAL_PRE').AsFloat);
          ParamAsString(FCursorGral, 'CR_OBSERVA', 'Pago de pr�stamos');
          ParamAsInteger(FCursorGral, 'US_CODIGO', UsuarioActivo);
          Ejecuta(FCursorGral);
        end;
      end;

      // Se actualizan los saldos del Ahorro
      with cdsDataset do begin
        PreparaQuery(FCursorGral, GetSQLScript(Q_UPDATE_AHORRO));
        ParamAsInteger(FCursorGral, 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger);
        ParamAsInteger(FCursorGral, 'AH_STATUS', FieldByName('AH_STATUS').AsInteger);
        ParamAsString(FCursorGral, 'AH_TIPO', FieldByName('AH_TIPO').AsString);

        dAbonos := FieldByName('AH_ABONOS').AsFloat;
        if (cdsTemporal.FieldByName('CM_TIPO').AsString = 'LIQUID') then
          dAbonos := dAbonos + cdsTemporal.FieldByName('CM_INTERES').AsFloat;

        ParamAsFloat(FCursorGral, 'AH_ABONOS', dAbonos);

        if (cdsTemporal.FieldByName('CM_TIPO').AsString = 'LIQUID') then
          dCargos := FieldByName('AH_CARGOS').AsFloat + cdsTemporal.FieldByName('CM_DISPONIBLE').AsFloat +
            cdsTemporal.FieldByName('CM_SAL_PRE').AsFloat
        else
          {$IFDEF ANTES}
          dCargos := FieldByName('AH_CARGOS').AsFloat + cdsTemporal.FieldByName('CM_MONTO').AsFloat +
            cdsTemporal.FieldByName('CM_SAL_PRE').AsFloat;
          {$ELSE}
          dCargos := FieldByName('AH_CARGOS').AsFloat + cdsTemporal.FieldByName('CM_MONTO').AsFloat;
        {$ENDIF}
        ParamAsFloat(FCursorGral, 'AH_CARGOS', dCargos);

        ParamAsFloat(FCursorGral, 'AH_SALDO', FieldByName('AH_SALDO_I').AsFloat + FieldByName('AH_TOTAL').AsFloat +
            dAbonos - dCargos);
      end;
      Ejecuta(FCursorGral);

      // Se saldan los prestamos.
      {$IFDEF ANTES}
      if (cdsTemporal.FieldByName('CM_TIPO').AsString = 'LIQUID') then
        sObservaciones := 'Saldado por liquidaci�n'
      else
        sObservaciones := 'Saldado con Retiro Parcial';
      {$ELSE}
      if (cdsTemporal.FieldByName('CM_TIPO').AsString = 'LIQUID') then begin
        sObservaciones := 'Saldado por liquidaci�n';
        {$ENDIF}
        FPrestamo := CreateQuery;
        AbreQueryScript(FPrestamo, Format(GetSQLScript(Q_PRESTAMO_LIQUIDA), [cdsDataset.FieldByName('CB_CODIGO').AsInteger, EntreComillas(cdsDataset.FieldByName('TB_PRESTA').AsString)]));

        FPrestamoUpdate := CreateQuery;
        PreparaQuery(FPrestamoUpdate, GetSQLScript(Q_UPDATE_PRESTAMO));

        with FPrestamo do try
          while NOT EOF do begin
            PreparaQuery(FCursorGral, GetSQLScript(Q_INSERT_PCAR_ABO));
            with cdsTemporal do begin
              ParamAsString(FCursorGral, 'PR_TIPO', FPrestamo.FieldByName('PR_TIPO').AsString);
              ParamAsInteger(FCursorGral, 'CB_CODIGO', FieldByName('CB_CODIGO').AsInteger);
              ParamAsDate(FCursorGral, 'CR_CAPTURA', Date);
              ParamAsDate(FCursorGral, 'CR_FECHA', FieldByName('CM_FECHA').AsDateTime);
              ParamAsFloat(FCursorGral, 'CR_ABONO', FPrestamo.FieldByName('PR_SALDO').AsFloat);
              ParamAsFloat(FCursorGral, 'CR_CARGO', 0);
              ParamAsString(FCursorGral, 'PR_REFEREN', FPrestamo.FieldByName('PR_REFEREN').AsString);
              ParamAsString(FCursorGral, 'CR_OBSERVA', sObservaciones);
              ParamAsInteger(FCursorGral, 'US_CODIGO', UsuarioActivo);
            end;
            Ejecuta(FCursorGral);

            ParamAsString(FPrestamoUpdate, 'Tipo', FPrestamo.FieldByName('PR_TIPO').AsString);
            ParamAsInteger(FPrestamoUpdate, 'Empleado', FieldByName('CB_CODIGO').AsInteger);
            ParamAsString(FPrestamoUpdate, 'Referencia', FPrestamo.FieldByName('PR_REFEREN').AsString);
            iEmpleado :=  FieldByName('CB_CODIGO').AsInteger;
            sReferencia :=  FPrestamo.FieldByName('PR_REFEREN').AsString;
            sTipoPrestamo := FPrestamo.FieldByName('PR_TIPO').AsString ;

            Ejecuta(FPrestamoUpdate);
            Next;
          end;
        finally
          FreeAndNil(FPrestamo);
          FreeAndNil(FPrestamoUpdate);
        end;
      end;
      TerminaTransaccion(True);
      ActualizarEstatusPrestamo;
    except
      on Error: Exception do begin
        RollBackTransaccion;
        EscribeBitacora(tbError, clbHisAhorros, cdsDataset.FieldByName('CB_CODIGO').AsInteger, Now,
          'Error al Registrar Liquidaci�n', Error.Message);
      end;
    end;
    FreeAndNil(FCursorGral);
  end;
end;

function TdmServerCajaAhorro.GetCuentasMovtos( Empresa: OleVariant; const sFiltro: WideString ): OleVariant;
begin
     SetTablaInfo( eCtasMovimientos );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := sFiltro;
          Result := GetTabla( Empresa );
     end;
end;

function TdmServerCajaAhorro.GrabaCuentasMovtos(Empresa, Delta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          if  not VarIsNull( Delta )  then
          begin
               SetTablaInfo( eCtasMovimientos );
               Result := GrabaTabla( Empresa, Delta, ErrorCount );
          end
     end;

end;

function TdmServerCajaAhorro.GetAhorro(Empresa: OleVariant; Empleado: Integer; const TipoAhorro: WideString; out Prestamos,
  ACarAbo: OleVariant): OleVariant;
begin
     Result := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_AHORRO ),[ Empleado, EntreComillas( TipoAhorro ) ] ), True );
     Prestamos := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_PRESTAMO ),[ EntreComillas( TipoAhorro ), Empleado, Empleado ] ), True );

     with oZetaProvider do
     begin
          SetTablaInfo( eACarAbo );
          TablaInfo.Filtro := Format( 'CB_CODIGO = %d and AH_TIPO = %s',[Empleado,EntreComillas( TipoAhorro ) ] );
          ACarAbo := GetTabla( Empresa );
     end;

     SetComplete;
end;

function TdmServerCajaAhorro.GrabaSaldosAhorro(Empresa, Delta, DeltaACarAbo: OleVariant; var ErrorCount, ErrorCountCarAbo: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          if not VarIsNull( Delta )  then
          begin
               SetTablaInfo( eAhorro );
               Result := GrabaTabla( Empresa, Delta, ErrorCount );
          end;

          if ( ErrorCount = 0 ) and not VarIsNull( DeltaACarAbo ) then
          begin
               SetTablaInfo( eACarAbo );
               Result := GrabaTabla( Empresa, DeltaACarAbo, ErrorCountCarAbo );
          end;
     end;
end;

procedure TdmServerCajaAhorro.AfterUpdateAhorros(Sender: TObject; SourceDS: TDataSet;DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
 var
    eClase: eClaseBitacora;
    sTitulo: string;
begin
     if ( UpdateKind <> ukDelete ) then
     begin
          oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_AHORRO ), [ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                               EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AH_TIPO' ) ) ) ]  ) );
     end;

     if ( UpdateKind <> ukInsert ) then
     begin
          eClase  := clbHisAhorros;
          sTitulo := 'Ahorros:' +
                     ' Empleado:' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) ) +
                     ' Tipo:' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AH_TIPO' ) ) ;

          with oZetaProvider do
               if UpdateKind = ukModify then
                  CambioCatalogo( sTitulo, eClase, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) )
               else
                   BorraCatalogo( sTitulo, eClase, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) );
     end;
end;

function TdmServerCajaAhorro.GrabaSaldosPrestamo(Empresa, Delta, DeltaPCarAbo, Parametros: OleVariant; out ErrorCount,
         ErrorCountCarAbo: Integer; ForzarGrabarPrestamos: WordBool;
         var MensajePrestamo: WideString): OleVariant;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList( Parametros );
          try
             if not VarIsNull( Delta )  then
             begin
                  SetTablaInfo( eRegPrestamos );
                  TablaInfo.BeforeUpdateRecord := BeforeUpdatePrestamos;
                  dmServerPrestamos.ZetaProvider.EmpresaActiva := EmpresaActiva;
                  dmServerPrestamos.ZetaProvider.AsignaParamList( oZetaProvider.ParamList.VarValues );
                  dmServerPrestamos.ForzarGrabaPrestamo := ForzarGrabarPrestamos;
                  Result := GrabaTabla( Empresa, Delta, ErrorCount );
             end;

             if ( ErrorCount = 0 ) and not VarIsNull( DeltaPCarAbo ) then
             begin
                  SetTablaInfo( ePCarAbo );
                  Result := GrabaTabla( Empresa, DeltaPCarAbo, ErrorCountCarAbo );
             end;
          finally
                 MensajePrestamo :=  dmServerPrestamos.MensajePrestamo; 
          end;
     end;
end;

procedure TdmServerCajaAhorro.AfterUpdatePrestamos(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
 var
    eClase: eClaseBitacora;
    sTitulo: string;
begin
     if ( UpdateKind <> ukDelete ) then
     begin
          oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_PRESTAMO ), [ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                                 EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_TIPO' ) ) ),
                                                                                                                 EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_REFEREN' ) ) ) ]  ) );

          oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_PRESTAMO_CTA_MOVS ), [ ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'PR_MONTO_S' ) ),
                                                                                                                          ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                                          EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_TIPO' ) ) ),
                                                                                                                          EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_REFEREN' ) ) ) ]  ) );

          if ( DeltaDS.FindField( 'AH_TIPO' ) <> NIL ) then
             oZetaProvider.ExecSQL( oZetaProvider.EmpresaActiva, Format( GetSQLScript( Q_AFTER_UPDATE_AHORRO ), [ ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ),
                                                                                                                  EntreComillas( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'AH_TIPO' ) ) ) ]  ) );

     end;

     if ( UpdateKind <> ukInsert ) then
     begin
          eClase  := clbHisPrestamos;
          sTitulo := 'Pr�stamo:' +
                     ' Empl.:' + IntToStr( ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ) ) +
                     ' Tipo:' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_TIPO' ) )+
                     ' Ref:' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PR_REFEREN' ) ) ;


          with oZetaProvider do
               if UpdateKind = ukModify then
                  CambioCatalogo( sTitulo, eClase, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ), sTitulo + CR_LF )
               else
                   BorraCatalogo( sTitulo, eClase, DeltaDS, ZetaServerTools.CampoAsVar( DeltaDS.FieldByName( 'CB_CODIGO' ) ), sTitulo + CR_LF );
     end;
end;

function TdmServerCajaAhorro.GetTotalesCajaFondo(Empresa: OleVariant; const TipoAhorro: WideString; var Cuentas: OleVariant): OleVariant;
begin
     with cdsTotales do
     begin
          InitTempDataset;
          AddIntegerField( 'Cuantos_Socios' );
          AddFloatField( 'Total_Ahorrado' );
          AddIntegerField( 'Num_Prestamos' );
          AddFloatField( 'Saldo_Prestamos' );
          AddFloatField( 'Saldo_Bancos' );
          CreateTempDataset;

          with oZetaProvider do
          begin
               EmpresaActiva := Empresa;
               FCursorGral:= CreateQuery;
               AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTAL_AHORRADO ), [ EntreComillas( TipoAhorro ) ] ) );
          end;
          Edit;
          FieldByName( 'Cuantos_Socios' ).AsInteger := FCursorGral.FieldByName( 'CUANTOS_SOCIOS' ).AsInteger;
          FieldByName( 'Total_Ahorrado' ).AsInteger := FCursorGral.FieldByName( 'TOTAL_AHORRADO' ).AsInteger;

          with oZetaProvider do
          begin
               AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_SALDO_PRESTAMOS ), [ EntreComillas( TipoAhorro ) ] ) );
          end;
          FieldByName( 'Num_Prestamos' ).AsFloat := FCursorGral.FieldByName( 'NUM_PRESTAMOS' ).AsFloat;
          FieldByName( 'Saldo_Prestamos' ).AsFloat := FCursorGral.FieldByName( 'SALDO_PRESTAMOS' ).AsFloat;

          with oZetaProvider do
          begin
               AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_SALDO_BANCOS ), [  DateToStrSQLC(Date),EntreComillas( TipoAhorro ) ] ) );
          end;
          FieldByName( 'Saldo_Bancos' ).AsFloat := FCursorGral.FieldByName( 'SALDO_BANCOS' ).AsFloat;
          Post;
     end;
     FreeAndNil(FCursorGral);
     Cuentas := oZetaProvider.OpenSQL( Empresa, Format( GetSQLScript( Q_CUENTAS ), [ DateToStrSQLC(Date), EntreComillas( TipoAhorro ) ] ), True );
     Result := cdsTotales.Data;
end;

function TdmServerCajaAhorro.GetCuentasBancarias(Empresa, Parametros: OleVariant; var SaldoInicial: Double): OleVariant;
var
   oParams:TZetaParams;
   sFiltro:string;
begin
     oZetaProvider.EmpresaActiva := Empresa;
     oParams := TZetaParams.Create;
     try
        oParams.VarValues := Parametros;
        if(StrLleno(oParams.ParamByName('Tipo').AsString))then
        begin
             sFiltro := ' and CM_TIPO = '+EntreComillas(oParams.ParamByName('Tipo').AsString);
        end
        else sFiltro := '';

        with oZetaProvider do
        begin
             Result := OpenSQL(Empresa,Format( GetSQLScript( Q_MOVIMIENTOS), [ EntreComillas(oParams.ParamByName('Cuenta').AsString),
                                                                              EntreComillas(DateToStrSQL(oParams.ParamByName('FechaIni').AsDate)),
                                                                              EntreComillas(DateToStrSQL(oParams.ParamByName('FechaFin').AsDate)),
                                                                              oParams.ParamByName('Status').AsInteger,
                                                                              sFiltro
                                                                              ]), True );
             FCursorGral := CreateQuery;
             try
                AbreQueryScript( FCursorGral, Format( GetSQLScript( Q_SALDO_CTA ), [ EntreComillas(oParams.ParamByName('Cuenta').AsString),
                                                                                  EntreComillas(DateToStrSQL(oParams.ParamByName('FechaIni').AsDate-1))] ) );
                SaldoInicial := FCursorGral.Fields[0].AsFloat;
             finally
                    FreeAndNil(FCursorGral);
             end;
        end;
     Finally
            FreeAndNil(oParams);
     end;
end;

function TdmServerCajaAhorro.GetTotalesRetenciones(Empresa, Parametros: OleVariant): OleVariant;
 var
    oParams, Resultado : TZetaParams;
    rSumaAhorro: TPesos;
begin
     try
        oParams := TZetaParams.Create;
        Resultado := TZetaParams.Create;
        oParams.VarValues := Parametros;
        with oZetaProvider do
        try
             EmpresaActiva := Empresa;
             FCursorGral:= CreateQuery;

             with oParams do
                  AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTALES_RENTENCION_AHORRO ),
                                                [ ParamByName('Year').AsInteger,
                                                  ParamByName('Tipo').AsInteger,
                                                  ParamByName('Numero').AsInteger,
                                                  ParamByName('ConceptoAhorro').AsInteger
                                                  ] ) );
             with Resultado do
             begin
                  AddFloat( 'Num_Ahorro', FCursorGral.FieldBYName('Num_Ahorro').AsFLoat );
                  rSumaAhorro := FCursorGral.FieldBYName('Suma_Ahorro').AsFLoat ;
             end;

             with oParams do
             begin
                  if (ParamByName('ConceptoRelativo').AsInteger <> 0 ) then
                  begin
                       AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTALES_RENTENCION_AHORRO ),
                                                     [ ParamByName('Year').AsInteger,
                                                       ParamByName('Tipo').AsInteger,
                                                       ParamByName('Numero').AsInteger,
                                                       ParamByName('ConceptoRelativo').AsInteger
                                                       ] ) );
                       rSumaAhorro := rSumaAhorro + FCursorGral.FieldBYName('Suma_Ahorro').AsFloat;
                  end;
             end;

             with Resultado do
                  AddFloat( 'Suma_Ahorro', rSumaAhorro  );

             with oParams do
                  AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_TOTALES_RENTENCION_PRESTAMO ),
                                                [ ParamByName('Year').AsInteger,
                                                  ParamByName('Tipo').AsInteger,
                                                  ParamByName('Numero').AsInteger,
                                                  ParamByName('ConceptoPrestamo').AsInteger
                                                  ] ) );
             with Resultado do
             begin
                  AddFloat( 'Num_Prestamos', FCursorGral.FieldBYName('Num_Prestamos').AsFLoat );
                  AddFloat( 'Suma_Prestamos', FCursorGral.FieldBYName('Suma_Prestamos').AsFLoat );
             end;
        finally
               FreeAndNil(FCursorGral);
        end;
        Result := Resultado.VarValues;
     finally
            FreeAndNiL(Resultado);
            FreeAndNiL(oParams);
     end;
end;

function TdmServerCajaAhorro.GetPeriodos(Empresa: OleVariant; Year,  Tipo: Integer): OleVariant;
begin
     SetTablaInfo( ePeriodos );
     with oZetaProvider do
     begin
          TablaInfo.Filtro := Format( '( PE_YEAR = %d ) and ( PE_TIPO = %d )', [ Year, Tipo ] );
          Result := GetTabla( Empresa );
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.GetCtasBancarias( Empresa: OleVariant): OleVariant;
begin
     SetComplete
end;

function TdmServerCajaAhorro.GetNominaEmpleado(Empresa: OleVariant; Empleado: Integer): Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral:= CreateQuery;
          try
             AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_NOMINA_EMPLEADO ), [ Empleado ] ) );
             Result := FCursorGral.FieldByName( 'TU_NOMINA' ).AsInteger;
          finally
                 FreeAndNil( FCursorGral );
          end;
     end;
     SetComplete;
end;

function TdmServerCajaAhorro.ValidaCheque(Empresa: OleVariant; const CtaBancaria: WideString; Cheque: Integer): Integer;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          FCursorGral:= CreateQuery;
          try
             AbreQueryScript( FCursorGral, Format( GetSQLScript ( Q_VALIDA_CHEQUE ), [ EntreComillas( CtaBancaria ), Cheque ] ) );
             Result := FCursorGral.FieldByName( 'TOTAL' ).AsInteger;
          finally
             FreeAndNil( FCursorGral );
          end;
     end;
     SetComplete;
end;

procedure TdmServerCajaAhorro.BeforeUpdatePrestamos(Sender: TObject;SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet;UpdateKind: TUpdateKind; var Applied: Boolean);
begin
     dmServerPrestamos.ValidaReglasPrestamos(UpdateKind,DeltaDS );
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerCajaAhorro, Class_dmServerCajaAhorro, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}

end.



