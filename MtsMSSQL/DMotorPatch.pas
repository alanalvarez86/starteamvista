{* Actualiza la Base de Datos Empleados a una versi�n espec�fica, aplicando las versiones intermedias.
  @author Ricardo Carrillo Morales 03/Mar/2014
  @author Enrique Zazueta ~13/Mar/2014
}
unit DMotorPatch;

interface

uses
  DB, ZetaCommonClasses, DZetaServerProvider;

type
  TMetodoGraba = procedure(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind)
    of object;

  function MotorPatch(Parametros: TZetaParams; ZetaProvider: TdmZetaServerProvider; ZetaProviderLog: TdmZetaServerProvider = nil;
    GrabaCambios: TMetodoGraba = nil): Boolean;

implementation

uses
  SysUtils, Classes, Variants, ZetaServerTools, ZetaSQLScriptParser, ZetaCommonLists, ZGlobalTress, ZetaCommonTools,
  ZetaWinAPITools, ZetaServerDataSet, MotorPatchUtils, {$IFNDEF RDDTRESSCFG}DImportarReporte,{$ENDIF} DImportarDiccionario;

type

  TMotorPatch = class
  private
    oParams             : TZetaParams;
    oZetaProvider       : TdmZetaServerProvider;
    oZetaProviderLog    : TdmZetaServerProvider;
    oQuery              : TZADOQuery;
    iExitos             : Integer;
    iPasos              : Integer;
    iErrores            : Integer;
    iAdvertencias       : Integer;
    GrabaCambiosBitacora: TMetodoGraba;
    TipoPatch           : eClaseBitacora;
    DetallesBD          : TDetallesBD;
    function EjecutaQuery(Sender: TObject; const Sentences: string): Boolean;
  public
    constructor Create(Parametros: TZetaParams; ZetaProvider, ZetaProviderLog: TdmZetaServerProvider; GrabaCambios: TMetodoGraba);
    function Execute: Boolean;
  end;

{* Invoca el Motor de Patch. @author Ricardo Carrillo Morales}
function MotorPatch(Parametros: TZetaParams; ZetaProvider: TdmZetaServerProvider; ZetaProviderLog: TdmZetaServerProvider = nil;
  GrabaCambios: TMetodoGraba = nil): Boolean;
var
  Motor: TMotorPatch;
begin
  Motor  := TMotorPatch.Create(Parametros, ZetaProvider, ZetaProviderLog, GrabaCambios);
  Result := Motor.Execute;
  FreeAndNil(Motor);
end;

{* Constructor de la clase. @author Ricardo Carrillo Morales}
constructor TMotorPatch.Create(Parametros: TZetaParams; ZetaProvider, ZetaProviderLog: TdmZetaServerProvider;
  GrabaCambios: TMetodoGraba);
begin
  oParams       := Parametros;
  oZetaProvider := ZetaProvider;
  if Assigned(ZetaProviderLog) then
    oZetaProviderLog := ZetaProviderLog
  else
    oZetaProviderLog := ZetaProvider;
  GrabaCambiosBitacora := GrabaCambios;
end;

{* Ejecuta una sentencia SQL, no importa si es de un especial o de versi�n. @author Ricardo Carrillo Morales}
function TMotorPatch.EjecutaQuery(Sender: TObject; const Sentences: string): Boolean;
var
  sSQL: string;
begin
  Result := oZetaProviderLog.CanContinue;
  if not Result then
    Exit;
  try
    sSQL := Trim(StringReplace(Sentences, K_GATITO_COMPARTE, DetallesBD.ComparteNom, [rfReplaceAll, rfIgnoreCase]));
    oQuery := oZetaProvider.CreateQuery(sSQL) as TZADOQuery;
    try
      oQuery.ExecSQL;
    finally
      oQuery.Free;
    end;
    Inc(iExitos);
  except
    on e: Exception do begin
      // Es un error permitido?
      if EsAdvertencia(e.Message) then begin
        // Agregar LogAdvertencia
        oZetaProviderLog.Log.Advertencia(TipoPatch, 0, Int(Now), e.Message, e.Message + CR_LF + sSQL);
        Inc(iAdvertencias);
      end else begin
        // Agregar LogError
        oZetaProviderLog.Log.Error(0, e.Message, e.Message + CR_LF + sSQL);
        Inc(iErrores);
        // Result := False;
      end;
    end;
  end;
end;

{ * Actualiza la Base de Datos Empleados desde una version inicial de Patch hasta una versi�n destino, aplicando las versiones
  intermedias.
  @author Ricardo Carrillo Morales 03/Mar/2014
  @Param Parametros Contiene los valores necesarios para aplicar el patch.
  @return TRUE si pudo aplicar los parches sin errores pero con advertencias, FALSE en caso contrario.
}
function TMotorPatch.Execute: Boolean;
var
  iVersionInicial : Integer;
  iVersionDestino : Integer;
  iVersionPatch   : Integer;
  iVersionAnterior: Integer;
  sArchivo        : string;
  sPatch          : string;
  sDiccion        : string;
  sError          : string;
  sServer         : string;
  sDatabase       : string;
  oScriptParser   : TZetaSQLScriptParser;
  oEspeciales     : TStringList;
  oReportes       : TStringList;
  bConteo         : Boolean;

  // Formato: Especial_XXX_NNN.sql   XXX = cualquier cosa NNN = N�mero de versi�n
  function EsScriptEspecial(const Nombre: string; const Version: Integer): Boolean;
  var
    I            : Integer;
    sNombre, sVer: string;
  begin
    Result  := False;
    sNombre := UpperCase(ExtractFileName(Nombre));
    if not (Pos('ESPECIAL', sNombre) = 1) and (UpperCase((sNombre)) = 'SQL') then
      Exit;
    I    := RPos('_', sNombre);
    sVer := '';
    if I > 0 then
      sVer := Copy(sNombre, I + 1, Length(sNombre) - I - 4);
    Result := (sVer = IntToStr(Version));
  end;

  procedure ContarSentencias;
  var
    I: Integer;
  begin
    iPasos := 0;
    bConteo := True;
    // Calcular cuantas operaciones se aplicar�n
    oEspeciales.Clear;
    if oParams.ParamByName('DB_APLICAESPECIALES').AsBoolean or DetallesBD.EsComparte then
      SearchFile(DetallesBD.PathEspeciales, K_ESPECIALES_WILDCARD, oEspeciales);

    while iVersionPatch <= iVersionDestino do begin
      // Patch de versi�n (contar sentencias)
      sArchivo := Format(sPatch, [iVersionPatch]);
      if FileExists(sArchivo) then begin
        oScriptParser.LoadFromFile(sArchivo);
        Inc(iPasos, oScriptParser.Sentences.Count);
      end;

      // Diccionario (contar archivos)
      if oParams.ParamByName('DB_APLICADICCIONARIO').AsBoolean and bConteo then begin
        sArchivo := Format(sDiccion, [iVersionPatch]);
        if bConteo and FileExists(sArchivo) then begin
          Inc(iPasos);
          bConteo := (DetallesBD.DBTipo IN [tc3Datos, tc3Prueba, tcPresupuesto]) // Contar un solo archivo, solo Empleados tiene "Diccion ###.xml"
        end;
      end;

      // Especiales (contar sentencias)
      for I := 0 to oEspeciales.Count - 1 do
        if EsScriptEspecial(oEspeciales[I], iVersionPatch) then begin
          oScriptParser.LoadFromFile(oEspeciales[I]);
          Inc(iPasos, oScriptParser.Sentences.Count);
          Break;
        end;
      Inc(iVersionPatch);
    end;

    // Reportes (contar archivos)
    oReportes.Clear;
    if oParams.ParamByName('DB_APLICAREPORTES').AsBoolean and (DetallesBD.PathReportes <> '') then
      SearchFile(DetallesBD.PathReportes, K_REPORTES_WILDCARD, oReportes);
    Inc(iPasos, oReportes.Count);

  end;

  procedure PreparaPatch;
  begin
    oScriptParser := TZetaSQLScriptParser.Create;
    iVersionInicial  := oParams.ParamByName('DB_VERSION').AsInteger;
    iVersionDestino  := oParams.ParamByName('DB_APLICAVERSION').AsInteger;
    iVersionAnterior := iVersionInicial;
    iVersionPatch    := iVersionInicial + 1;

    oEspeciales := TStringList.Create;
    oReportes   := TStringList.Create;
    iPasos      := 0;

    if DetallesBD.PathPatch <> '' then begin
      sPatch   := DetallesBD.PathPatch  + K_ESTRUCTURA_MASK;
      sDiccion := DetallesBD.PathDiccionario + DetallesBD.DiccionarioMask;

      oScriptParser.OnExecSQL := EjecutaQuery;

      // Calcular las sentencias de Patch y Diccionario
      ContarSentencias;
    end;
  end;

  procedure ImportarDiccionario;
  var
    cdsXML : TServerDataSet;
//    oQuery: TZADOQuery;
  begin
    if not oParams.ParamByName('DB_APLICADICCIONARIO').AsBoolean then
      Exit;
    Sleep(1000);//Para ordenar por tiempo
    sArchivo := Format(sDiccion, [iVersionPatch]);
    if FileExists(sArchivo) and oZetaProviderLog.CanContinue then begin
      try
        oZetaProviderLog.Log.Evento(TipoPatch, 0, Date, Format('Inicia Importaci�n de Diccionario :%d',[iVersionPatch]));
        // Borrar el diccionario anterior
        if DetallesBD.DBTipo IN [tc3Datos, tc3Prueba, tcPresupuesto]  then // Empleados tiene su propio formato de DICCIONARIO
        begin
          ImportarDiccionarioXML(sArchivo, oZetaProvider, oZetaProviderLog, GrabaCambiosBitacora, DetallesBD.Empresas, True);
        end
        else
        begin // El resto lee una tabla desde un XML con el formato de MIDAS
          cdsXML := TServerDataSet.Create(nil);
          //oQuery := oZetaProvider.CreateQuery('update') as TZADOQUery;
          try
            oZetaProvider.EjecutaAndFree('delete from DICCION');
            cdsXML.LoadFromFile(sArchivo);
            if GrabaDataSet( cdsXML, 'DICCION', VACIO, oZetaProvider, iAdvertencias, iErrores) then
            begin
                 Inc(iExitos);
                 oZetaProvider.EjecutaAndFree('execute procedure SP_REFRESH_DICCION');
            end;
          finally
            FreeAndNil(cdsXML);
          //  FreeAndNil(oQuery);
          end;
        end;
        Sleep(1000);//Para ordenar por tiempo
        oZetaProviderLog.Log.Evento(TipoPatch, 0, Date, Format('Termina Importaci�n de Diccionario:%d',[iVersionPatch]));
      except
        on e: Exception do begin
          oZetaProviderLog.Log.Advertencia(0, Format('Error al Importar Diccionario: %d',[iVersionPatch]), e.Message);
          Inc(iAdvertencias);
        end;
      end
    end else begin
      oZetaProviderLog.Log.Advertencia(0,'No Existe Archivo de Diccionario', 'No Existe Archivo de DICCION : ' + sArchivo);
      Inc(iAdvertencias);
    end;
  end;

  procedure ImportarReportes;
  var
    I: Integer;
  begin
    if not oParams.ParamByName('DB_APLICAREPORTES').AsBoolean then
      Exit;
    Sleep(1000);//Para ordenar por tiempo

    {$IFNDEF RDDTRESSCFG}
    TipoPatch := clbMotorPatchImportarReporte;
    if oReportes.Count = 0 then begin
      // Agregar LogInfo: No se encontr� folder de reportes
      oZetaProviderLog.Log.Evento(TipoPatch, 0, Date, 'No se encontr� folder de reportes.');
    end;
    for I := 0 to oReportes.Count - 1 do begin
      try
        if not ImportarReporteRP1(oZetaProvider, oZetaProviderLog, oReportes[I], sError) then begin
          oZetaProviderLog.Log.Advertencia(TipoPatch, 0, Int(Now), 'Al Importar Reporte ' + ExtractFileName(oReportes[I]), sError);
          Inc(iAdvertencias);

        end;
        oZetaProviderLog.CanContinue;
      except
        on e: Exception do begin
          oZetaProviderLog.Log.Advertencia(0, 'Error al Importar Reportes', e.Message);
          Inc(iAdvertencias);
        end;
      end;
    end;
    {$ENDIF}
  end;

  procedure ImportarEspeciales;
  var
    I: Integer;
  begin
    if not oParams.ParamByName('DB_APLICAESPECIALES').AsBoolean then
      Exit;
    Sleep(1000);//Para ordenar por tiempo
    TipoPatch := clbMotorPatchScriptEspecial;
    oZetaProviderLog.Log.Evento(TipoPatch, 0, Date, Format('Inicio de script especiales:%d',[ iVersionPatch]));
    for I := 0 to oEspeciales.Count - 1 do begin
      if EsScriptEspecial(oEspeciales[I], iVersionPatch) then begin
        oScriptParser.LoadFromFile(oEspeciales[I]);
        oScriptParser.Execute;
      end else begin
        // TODO : Como aplicar el Patch Especial por cada Base de Datos
        oZetaProviderLog.Log.Advertencia(TipoPatch, 0, Int(Now), Format('Archivo:%s no tiene formato requerido', [oEspeciales[I]]),
          'El Formato requerido para archivos de scrips especiales es : Especiales_###.sql');
        Inc(iAdvertencias);
      end;
    end;
    oZetaProviderLog.Log.Evento(TipoPatch, 0, Date, Format('Termin� de aplicar script especiales: %d',[ iVersionPatch]));
  end;

  function GetResumenCambioVersion: string;
  begin
    Result := Format(' Resumen del cambio de Version de %d a %d' + CR_LF +
                     '  Errores              = %d' + CR_LF +
                     '  Advertencias         = %d' + CR_LF +
                     '  Sentencias Correctas = %d' + CR_LF,
                     [iVersionInicial, iVersionDestino, iErrores, iAdvertencias, iExitos]
                    );
  end;

begin
  Result := False;
  if not oParams.ParamByName('DB_APLICAPATCH').AsBoolean then
    Exit;

  // Inicia el Proceso
  with oZetaProvider do
    try
      DetallesBD := TDetallesBD.Create(oZetaProvider, oParams.ParamByName('DB_DATOS').AsString);
      PreparaPatch;
      ZetaServerTools.GetServerDatabase(oParams.ParamByName('DB_DATOS').AsString, sServer, sDatabase);
      if oZetaProviderLog.OpenProcess(prPatchCambioVersion, iPasos, Format(MSG_CAMBIO_VERSION, [sDatabase, iVersionInicial, iVersionDestino]), '') then begin
        oParams.ParamValues['DB_PROCESO'] := oZetaProviderLog.Log.Folio;
        iExitos       := 0;
        iErrores      := 0;
        iAdvertencias := 0;
        iVersionPatch := iVersionInicial + 1;
        bConteo       := True;
        while (iErrores = 0) and (iVersionPatch <= iVersionDestino) do begin
          // Existe Archivo script?
          sArchivo := Format(sPatch, [iVersionPatch]);
          if FileExists(sArchivo) then begin
            // Leer Archivo Patch Scripts (Version Patch)
            oScriptParser.LoadFromFile(sArchivo);
            oZetaProviderLog.Log.Evento(clbMotorPatchCambioVersion, 0, Date, Format('Inicia actualizaci�n de versi�n a: %d',
                [iVersionPatch]), Format('Inicia proceso de sentencias: %s', [sArchivo]));
            TipoPatch := clbMotorPatchCambioVersion; // Usado por EjecutaQuery()
            if oScriptParser.Execute then begin// Por cada Sentencia del script... (Execute procesa cada sentencia antes de regresar)
              // Cambiar version a Base de Datos, si no tenemos errores graves
              if iErrores = 0 then begin
                DetallesBD.DBVersion := iVersionPatch;
              end;
              // Importar Diccionario
              oZetaProviderLog.CanContinue;
              if bConteo then begin
                sArchivo := Format(sDiccion, [iVersionPatch]);
                if FileExists(sArchivo) then begin
                  ImportarDiccionario;
                  bConteo := (DetallesBD.DBTipo IN [tc3Datos, tc3Prueba, tcPresupuesto]) // Importar un solo archivo, solo Empleados tiene "Diccion ###.xml"
                end
                else
                    oZetaProviderLog.Log.Advertencia(0,Format('No Existe Archivo de Diccionario: %s',[sArchivo]));
              end;
              // Importar Especiales
              ImportarEspeciales;
              // Agregar LogInfo Cambio de version Terminada
              oZetaProviderLog.Log.Evento(clbMotorPatchCambioVersion, 0, Date, Format('Termin� Actualizaci�n de Versi�n: %d', [iVersionPatch]), GetResumenCambioVersion);
            end else begin
              oZetaProviderLog.Log.Evento(clbMotorPatchCambioVersion, 0, Date, Format('Termin� Actualizaci�n: %d con Errores ', [iVersionPatch]), GetResumenCambioVersion);
              // Break;
            end; // if oScriptParser.Execute
            iVersionAnterior := iVersionPatch;
          end; // if FileExists(sPatch)

          // VersionPatch = SiguienteVersion(Inicial)
          Inc(iVersionPatch);
        end; // while (iVersionPatch <= iVersionDestino)

        // Importar Reportes
        if iErrores = 0 then begin
          ImportarReportes;
        end
      end; // if ZProvider.OpenProcess
      oZetaProviderLog.CloseProcess;
    finally
      oParams.AddInteger('PRO_EXITOS', iExitos);
      oParams.AddInteger('PRO_ERRORES', iErrores);
      oParams.AddInteger('PRO_ADVERTENCIAS', iAdvertencias);
      FreeAndNil(oScriptParser);
      FreeAndNil(oEspeciales);
      FreeAndNil(oReportes);
      FreeAndNil(DetallesBD);
      Result := (iErrores = 0);
    end;
end;

end.
