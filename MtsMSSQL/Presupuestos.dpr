library Presupuestos;

{$IFDEF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}

uses
  MidasLib,
  ComServ,
  DServerPresupuestos in '..\MTS\DServerPresupuestos.pas' {dmServerPresupuestos: TMtsDataModule},
  Presupuestos_TLB in '..\MTS\Presupuestos_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Presupuestos.TLB}

{$R *.RES}

begin
end.
