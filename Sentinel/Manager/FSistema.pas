unit FSistema;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, DBCtrls, Mask, ComCtrls, Db,
     ExtCtrls, Buttons,
     ZetaFecha,
     ZetaNumero,
     ZetaKeyCombo, Grids, DBGrids, ZetaEdit, ZetaLicenseClasses, ZetaCommonTools,ZetaFilesTools, ShellAPI, DXMLTools;

type
  TFormSistema = class(TForm)
    dsSistema: TDataSource;
    PageControl: TPageControl;
    Generales: TTabSheet;
    Direccion: TTabSheet;
    Panel2: TPanel;
    OKBtn: TBitBtn;
    CancelarBtn: TBitBtn;
    SI_COMENTAlbl: TLabel;
    SI_COMENTA: TDBMemo;
    Autorizacion: TTabSheet;
    TelefonoGB: TGroupBox;
    SI_TEL1lbl: TLabel;
    SI_TEL2lbl: TLabel;
    SI_FAXlbl: TLabel;
    SI_EMAILlbl: TLabel;
    SI_TEL1: TDBEdit;
    SI_TEL2: TDBEdit;
    SI_FAX: TDBEdit;
    SI_EMAIL: TDBEdit;
    GerenciaGB: TGroupBox;
    SI_GTE_RH: TDBEdit;
    SI_GTE_CON: TDBEdit;
    SI_GTE_SIS: TDBEdit;
    SI_GTE_RHlbl: TLabel;
    SI_GTE_CONlbl: TLabel;
    SI_GTE_SISlbl: TLabel;
    SI_EMA_SIS: TDBEdit;
    SI_EMA_SISlbl: TLabel;
    SI_EMA_CON: TDBEdit;
    SI_EMA_CONlbl: TLabel;
    SI_EMA_RH: TDBEdit;
    SI_EMA_RHlbl: TLabel;
    dsVersion: TDataSource;
    dsGrupo: TDataSource;
    dsCiudad: TDataSource;
    dsDistribuidor: TDataSource;
    DomicilioGB: TGroupBox;
    SI_CALLElbl: TLabel;
    SI_CALLE: TDBEdit;
    SI_COLONIA: TDBEdit;
    SI_COLONIAlbl: TLabel;
    CT_CODIGOlbl: TLabel;
    CT_CODIGO: TDBLookupComboBox;
    Ciudades: TBitBtn;
    DatosGB: TGroupBox;
    dsGiro: TDataSource;
    dsPais: TDataSource;
    PA_CODIGOlbl: TLabel;
    PA_CODIGO: TDBLookupComboBox;
    SI_MIGRADO: TDBCheckBox;
    SI_FEC_SOLlbl: TLabel;
    SI_FEC_SOL: TZetaDBFecha;
    SI_EMP_EMP: TZetaDBNumero;
    SI_EMP_EMPlbl: TLabel;
    SI_SIS_VIElbl: TLabel;
    SI_SIS_VIE: TDBEdit;
    SI_PROBLEMA: TDBCheckBox;
    Imagen: TImage;
    IdentificacionGB: TGroupBox;
    SI_FOLIO: TDBEdit;
    SI_FOLIOlbl: TLabel;
    SI_RAZ_SOClbl: TLabel;
    SI_RAZ_SOC: TDBEdit;
    GR_CODIGO: TDBLookupComboBox;
    GR_CODIGOlbl: TLabel;
    SI_PERSONAlbl: TLabel;
    SI_PERSONA: TDBEdit;
    GI_CODIGO: TDBLookupComboBox;
    GI_CODIGOlbl: TLabel;
    SI_GIROlbl: TLabel;
    SI_GIRO: TDBEdit;
    dsTerminales: TDataSource;
    sdGuardar: TSaveDialog;
    dlgLicencia: TSaveDialog;
    dsHistorico3DT: TDataSource;
    tabSentinel: TTabSheet;
    GridSentinelas: TDBGrid;
    Panel7: TPanel;
    dsSentinelas: TDataSource;
    NavegadorSentinelas: TDBNavigator;
    Panel8: TPanel;
    btnArchivoLic: TBitBtn;
    GroupBox4: TGroupBox;
    DBGridAutoriza: TDBGrid;
    dsAutoriza: TDataSource;
    PageControl1: TPageControl;
    GeneraAutorizacion: TTabSheet;
    NotaGB: TGroupBox;
    AU_COMENTA: TDBMemo;
    Panel9: TPanel;
    PrestamosGB: TGroupBox;
    AU_PRE_1: TDBCheckBox;
    AU_PRE_2: TDBCheckBox;
    AU_PRE_3: TDBCheckBox;
    AU_PRE_4: TDBCheckBox;
    AU_PRE_5: TDBCheckBox;
    AU_PRE_6: TDBCheckBox;
    AU_PRE_0: TDBCheckBox;
    AU_PRE_9: TDBCheckBox;
    AU_PRE_8: TDBCheckBox;
    AU_PRE_7: TDBCheckBox;
    AU_PRE_10: TDBCheckBox;
    AU_PRE_11: TDBCheckBox;
    AU_PRE_12: TDBCheckBox;
    AU_PRE_13: TDBCheckBox;
    AU_PRE_14: TDBCheckBox;
    AU_PRE_15: TDBCheckBox;
    AU_PRE_16: TDBCheckBox;
    AU_PRE_17: TDBCheckBox;
    AU_PRE_18: TDBCheckBox;
    AU_PRE_19: TDBCheckBox;
    AU_PRE_20: TDBCheckBox;
    PanelVencimientoPrestamo: TPanel;
    AU_FEC_PRElbl: TLabel;
    AU_FEC_PRE: TZetaDBFecha;
    AU_PRE_21: TDBCheckBox;
    AU_PRE_22: TDBCheckBox;
    AU_PRE_23: TDBCheckBox;
    ModulosGB: TGroupBox;
    AU_MOD_0: TDBCheckBox;
    AU_MOD_1: TDBCheckBox;
    AU_MOD_2: TDBCheckBox;
    AU_MOD_3: TDBCheckBox;
    AU_MOD_4: TDBCheckBox;
    AU_MOD_5: TDBCheckBox;
    AU_MOD_6: TDBCheckBox;
    AU_MOD_9: TDBCheckBox;
    AU_MOD_7: TDBCheckBox;
    AU_MOD_8: TDBCheckBox;
    AU_MOD_10: TDBCheckBox;
    AU_MOD_11: TDBCheckBox;
    AU_MOD_12: TDBCheckBox;
    AU_MOD_13: TDBCheckBox;
    AU_MOD_14: TDBCheckBox;
    AU_MOD_15: TDBCheckBox;
    AU_MOD_16: TDBCheckBox;
    AU_MOD_17: TDBCheckBox;
    AU_MOD_18: TDBCheckBox;
    AU_MOD_19: TDBCheckBox;
    AU_MOD_20: TDBCheckBox;
    PanelVencimientoAutorizacion: TPanel;
    AU_DEFINITlbl: TLabel;
    AU_FEC_AUTlbl: TLabel;
    AU_FEC_AUT: TZetaDBFecha;
    AU_DEFINIT: TZetaDBKeyCombo;
    AU_MOD_21: TDBCheckBox;
    AU_MOD_22: TDBCheckBox;
    AU_MOD_23: TDBCheckBox;
    SistemaGB: TGroupBox;
    AU_NUM_EMPlbl: TLabel;
    AU_USERSlbl: TLabel;
    VE_CODIGOlbl: TLabel;
    AU_PLATFORMlbl: TLabel;
    AU_NUM_EMP: TDBComboBox;
    VE_CODIGO: TDBLookupComboBox;
    AU_SQL_BD: TDBRadioGroup;
    AU_PLATFORM: TZetaDBKeyCombo;
    AU_USERS: TZetaDBNumero;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    AU_VAL_LIM: TZetaDBKeyCombo;
    AU_BD_LIM: TZetaDBKeyCombo;
    PanelSuperior: TPanel;
    KitWarning1: TLabel;
    AU_KIT_DIS: TDBCheckBox;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Panel4: TPanel;
    dbNavegador: TDBNavigator;
    btnGeneraArchivo: TBitBtn;
    GroupBox1: TGroupBox;
    LblTE_NUMERO: TLabel;
    LblTE_TEXTO: TLabel;
    LblTE_NSERIE: TLabel;
    lblTE_DESCRIP: TLabel;
    Label6: TLabel;
    TE_ACTIVO: TDBCheckBox;
    TE_TEXTO: TDBMemo;
    TE_NSERIE: TDBEdit;
    TE_DESCRIP: TDBEdit;
    TE_NUMERO: TZetaDBNumero;
    dbPostCancel: TDBNavigator;
    cbxTipoTerminal: TComboBox;
    dbGridTerminales: TDBGrid;
    Panel10: TPanel;
    zSentinelas: TZetaKeyCombo;
    Label2: TLabel;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    btnSalvaryAbrir3DT: TSpeedButton;
    Panel6: TPanel;
    DBNavigator1: TDBNavigator;
    HI_3DTXML: TDBMemo;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    HistoAutorizaciones: TTabSheet;
    DBGrid2: TDBGrid;
    Panel11: TPanel;
    btnGeneraAutorizacion: TBitBtn;
    dsActual: TDataSource;
    BitBtn1: TBitBtn;
    ShowAvailable: TSpeedButton;
    bbGrabaSentinel: TBitBtn;
    DI_CODIGOlbl: TLabel;
    DI_CODIGO: TDBLookupComboBox;
    Label1: TLabel;
    LIST_SUCURSALES: TComboBox;
    btnRemoverSentinel: TBitBtn;
    dsHistorico3DTTemporal: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure AU_PRE_1Click(Sender: TObject);
    procedure AU_DEFINITChange(Sender: TObject);
    procedure AU_KIT_DISClick(Sender: TObject);
    procedure SI_COMENTAExit(Sender: TObject);
    procedure ShowAvailableClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CiudadesClick(Sender: TObject);
    procedure SI_SIS_VIEExit(Sender: TObject);
    procedure SI_PROBLEMAClick(Sender: TObject);
    procedure VE_CODIGOClick(Sender: TObject);
    procedure btnGeneraArchivoClick(Sender: TObject);
    procedure btnArchivoLicClick(Sender: TObject);
    procedure btnSalvaryAbrir3DTClick(Sender: TObject);
	procedure cbxTipoTerminalChange(Sender: TObject);
	procedure dsTerminalesDataChange(Sender: TObject; Field: TField);
    procedure dsSentinelasDataChange(Sender: TObject; Field: TField);
    procedure zSentinelasChange(Sender: TObject);
    procedure btnGeneraAutorizacionClick(Sender: TObject);
    procedure CancelarBtnClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure bbGrabaSentinelClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure btnRemoverSentinelClick(Sender: TObject);
  private
    { Private declarations }
    FAlive: Boolean;
    FInserting: Boolean;
    FNumSentinel :Integer;
    FGlobalLicenseValues : TGlobalLicenseValues;
    function EsKitDistribuidor: Boolean;
    function EsVersion20: Boolean;
    function EsVersionValidacion: Boolean;
    procedure LoadEmpleadosSimple(Lista: TStrings);
    procedure SetControls;
    procedure Desconectar;
    procedure LlenarSentinelas;
    procedure FiltrarArchivos3dt;
    procedure FiltrarTerminales;
    procedure SetSucursal;
    procedure UpdateSucursal;
    procedure ActualizaAutorizacion;
  public
    { Public declarations }
    function Conectar( const lInserting: Boolean ): Boolean;
  end;


var
  FormSistema: TFormSistema;

procedure CapturaSistema( const lInserting: Boolean );
function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
procedure AbreDocumentoBDE( DataSet : TDataSet; const sBlobField, sExtension : string );

implementation

uses ZetaDialogo,
     ZetaServerTools,
     DSentinel,
     FAutoMaster,
     FAutoClasses,
     FPrincipal,
     FLibres,
     DBTables,
     FSeleccionaSentinel;

{$R *.DFM}

procedure CapturaSistema( const lInserting: Boolean );
begin
     if ( FormSistema = nil ) then
        FormSistema := TFormSistema.Create( Application );
     with FormSistema do
     begin
          if Conectar( lInserting ) then
             ShowModal;
     end;
end;

procedure TFormSistema.btnSalvaryAbrir3DTClick(Sender: TObject);
begin
    if (not dsHistorico3DT.DataSet.IsEmpty ) then
    begin
        AbreDocumentoBDE( dsHistorico3DT.DataSet, 'HI_3DTXML', 'XML' );
    end;
end;

{ *********** TFormSistema ********* }

procedure TFormSistema.FormCreate(Sender: TObject);
var
   i: Integer;

function GetModuleName( const eModule: TModulos ): String;
begin
     Result := FAutoClasses.GetModuloName( eModule );
end;

procedure GenerarFechasAU_VAL_LIM;
var
    i  : integer;
    sFecha : string;
begin
     with AU_VAL_LIM.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for i:= 0 to  15 do
             begin
                  sFecha := FechaAsStr(  IncMonth( FGlobalLicenseValues.GetFechaZero , i) );
                  Add( Format( '%d=%s', [ i, sFecha ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure GenerarLimitesAU_BD_LIM;
var
    i  : integer;
begin
     with AU_BD_LIM.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for i:= 0 to  15 do
             begin
                  Add( Format( '%d=%d', [ i, GetIntervaloLimiteBDConfig(i) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

begin

     FAutoClasses.LoadPlataformas( AU_PLATFORM.Items );
     FAutoClasses.LoadSQLEngines( AU_SQL_BD.Items );
     FGlobalLicenseValues := TGlobalLicenseValues.Create( nil );
     GenerarFechasAU_VAL_LIM;
     GenerarLimitesAU_BD_LIM;

     Self.LoadEmpleadosSimple( AU_NUM_EMP.Items );
     with AU_SQL_BD do
     begin
          with Values do
          begin
               Clear;
               for i := 0 to ( Items.Count - 1 ) do
               begin
                    Add( IntToStr( i ) );
               end;
          end;
     end;
     AU_MOD_0.Caption := GetModuleName( okRecursos );
     AU_MOD_1.Caption := GetModuleName( okNomina );
     AU_MOD_2.Caption := GetModuleName( okIMSS );
     AU_MOD_3.Caption := GetModuleName( okAsistencia );
     AU_MOD_4.Caption := GetModuleName( okCursos );
     AU_MOD_5.Caption := GetModuleName( okSupervisores );
     AU_MOD_6.Caption := GetModuleName( okCafeteria );
     AU_MOD_7.Caption := GetModuleName( okLabor );
     AU_MOD_8.Caption := GetModuleName( okKiosko );
     AU_MOD_9.Caption := GetModuleName( okValidador );
     AU_MOD_10.Caption := GetModuleName( okGraficas );
     AU_MOD_11.Caption := GetModuleName( okReportesMail );
     AU_MOD_12.Caption := GetModuleName( okHerramientas );
     AU_MOD_13.Caption := GetModuleName( okL5Poll );
     AU_MOD_14.Caption := GetModuleName( okSeleccion );
     AU_MOD_15.Caption := GetModuleName( okEnfermeria );
     AU_MOD_16.Caption := GetModuleName( okMonitor );
     AU_MOD_17.Caption := GetModuleName( okPortal );
     AU_MOD_18.Caption := GetModuleName( okPlanCarrera );
     AU_MOD_19.Caption := GetModuleName( okAccesos );
     AU_MOD_20.Caption := GetModuleName( okVisitantes );
     AU_MOD_21.Caption := GetModuleName( okEvaluacion );
     AU_MOD_22.Caption := GetModuleName( okWorkflow );
     AU_MOD_23.Caption := GetModuleName( okTressEnLinea );
     AU_PRE_0.Caption := GetModuleName( okRecursos );
     AU_PRE_1.Caption := GetModuleName( okNomina );
     AU_PRE_2.Caption := GetModuleName( okIMSS );
     AU_PRE_3.Caption := GetModuleName( okAsistencia );
     AU_PRE_4.Caption := GetModuleName( okCursos );
     AU_PRE_5.Caption := GetModuleName( okSupervisores );
     AU_PRE_6.Caption := GetModuleName( okCafeteria );
     AU_PRE_7.Caption := GetModuleName( okLabor );
     AU_PRE_8.Caption := GetModuleName( okKiosko );
     AU_PRE_9.Caption := GetModuleName( okValidador );
     AU_PRE_10.Caption := GetModuleName( okGraficas );
     AU_PRE_11.Caption := GetModuleName( okReportesMail );
     AU_PRE_12.Caption := GetModuleName( okHerramientas );
     AU_PRE_13.Caption := GetModuleName( okL5Poll );
     AU_PRE_14.Caption := GetModuleName( okSeleccion );
     AU_PRE_15.Caption := GetModuleName( okEnfermeria );
     AU_PRE_16.Caption := GetModuleName( okMonitor );
     AU_PRE_17.Caption := GetModuleName( okPortal );
     AU_PRE_18.Caption := GetModuleName( okPlanCarrera );
     AU_PRE_19.Caption := GetModuleName( okAccesos );
     AU_PRE_20.Caption := GetModuleName( okVisitantes );
     AU_PRE_21.Caption := GetModuleName( okEvaluacion );
     AU_PRE_22.Caption := GetModuleName( okWorkflow );
     AU_PRE_23.Caption := GetModuleName( okTressEnLinea );
end;

procedure TFormSistema.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmSentinel.AbrirSistema( dsSistema, dsVersion, dsGrupo, dsCiudad, dsDistribuidor, dsGiro, dsPais,dsTerminales, dsHistorico3DT, dsSentinelas,dsAutoriza,dsActual );
        FAlive := True;
     finally
            Screen.Cursor := oCursor;
     end;
     PageControl.ActivePage := Generales;
     SetControls;
     LlenarSentinelas;
     SetSucursal;
     zSentinelas.ItemIndex := 0;
     FNumSentinel := zSentinelas.LlaveEntero;
end;

procedure TFormSistema.SetSucursal;
VAR
   iIndex:Integer;
begin
     iIndex := LIST_SUCURSALES.Items.IndexOf(dsSistema.DataSet.FieldByName('SI_SUCURSAL').AsString );
     if iIndex > -1 then
        LIST_SUCURSALES.ItemIndex := iIndex
     else
         LIST_SUCURSALES.ItemIndex := -1;
end;


procedure TFormSistema.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Desconectar;
end;

procedure TFormSistema.LoadEmpleadosSimple(Lista: TStrings);
var
   i: Word;
   iEmpleados: Integer;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             i := 0;
             iEmpleados := UnLoadEmpleados( i );
             while ( iEmpleados > 0 ) do
             begin
                  if ( iEmpleados <> K_TOP_EMPLOYEES ) then
                     Add( Format( '%d', [ iEmpleados ] ) );
                  Inc( i );
                  iEmpleados := UnLoadEmpleados( i );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

function TFormSistema.Conectar( const lInserting: Boolean ): Boolean;
begin
     try
        if lInserting then
        begin
             with dmSentinel do
             begin
                  AgregaSistema;
             end;
             FInserting := True;
        end
        else
        begin
             with dmSentinel do
             begin
                  ModificaSistema;
             end;
             FInserting := False;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Atenci�n !', 'Se Encontr� Un Error', Error, 0 );
                Result := False;
           end;
     end;
end;

procedure TFormSistema.Desconectar;
begin
     FAlive := False;
     with dmSentinel do
     begin
          ResetDatasource( dsVersion );
          ResetDatasource( dsGrupo );
          ResetDatasource( dsCiudad );
          ResetDatasource( dsDistribuidor );
          ResetDatasource( dsGiro );
          ResetDatasource( dsPais );
          ResetDatasource( dsSistema );
          CancelaCambios;
     end;
end;

function TFormSistema.EsKitDistribuidor: Boolean;
begin
     Result := AU_KIT_DIS.Checked;
end;

function TFormSistema.EsVersion20: Boolean;
begin
     if VE_CODIGO.Field <> nil then
        Result := DSentinel.EsVer20( VE_CODIGO.Field.AsString )
     else
         Result := True;
end;

function TFormSistema.EsVersionValidacion: Boolean;
begin
     if VE_CODIGO.Field <> nil then
        Result := DSentinel.EsVerValidacion( VE_CODIGO.Field.AsString )
     else
         Result := False;
end;

procedure TFormSistema.SetControls;
var
   lOk, lTemporal: Boolean;
begin
     if FAlive then
     begin
          with dsSistema do
          begin
               if Assigned( Dataset ) then
                  lOk := Dataset.Active
               else
                   lOk := False;
          end;
     end
     else
         lOk := False;
     if lOk then
     begin
          {
          ModulosGB.Enabled := not EsKitDistribuidor;
          }
          PrestamosGB.Enabled := not EsKitDistribuidor;
          KitWarning1.Visible := EsKitDistribuidor;
          //AU_NUM_EMPlbl.Enabled := not EsKitDistribuidor;
          //AU_NUM_EMP.Enabled := not EsKitDistribuidor;
          AU_NUM_EMP.Text := IntToStr(K_DEMO_EMPLOYEES);
          AU_USERSlbl.Enabled := not EsKitDistribuidor;
          AU_USERS.Enabled := not EsKitDistribuidor;
          AU_MOD_0.Enabled := not EsKitDistribuidor;
          AU_MOD_1.Enabled := not EsKitDistribuidor;
          AU_MOD_2.Enabled := not EsKitDistribuidor;
          AU_MOD_3.Enabled := not EsKitDistribuidor;
          AU_MOD_4.Enabled := not EsKitDistribuidor;
          AU_MOD_5.Enabled := not EsKitDistribuidor;
          AU_MOD_6.Enabled := not EsKitDistribuidor;
          AU_MOD_7.Enabled := not EsKitDistribuidor;
          AU_MOD_8.Enabled := not EsKitDistribuidor;
          AU_MOD_9.Enabled := not EsKitDistribuidor;
          AU_MOD_10.Enabled := not EsKitDistribuidor;
          AU_MOD_11.Enabled := not EsKitDistribuidor;
          AU_MOD_12.Enabled := not EsKitDistribuidor;
          AU_MOD_13.Enabled := not EsKitDistribuidor;
          AU_MOD_14.Enabled := not EsKitDistribuidor;
          AU_MOD_15.Enabled := not EsKitDistribuidor;
          AU_MOD_16.Enabled := not EsKitDistribuidor;
          AU_MOD_17.Enabled := not EsKitDistribuidor;
          AU_MOD_18.Enabled := not EsKitDistribuidor;
          AU_MOD_19.Enabled := not EsKitDistribuidor;
          AU_MOD_20.Enabled := not EsKitDistribuidor;
          AU_MOD_21.Enabled := not EsKitDistribuidor;
          AU_MOD_22.Enabled := not EsKitDistribuidor;
          AU_MOD_23.Enabled := not EsKitDistribuidor;
          AU_DEFINITlbl.Enabled := not EsKitDistribuidor;
          AU_DEFINIT.Enabled := not EsKitDistribuidor;
          AU_PRE_0.Enabled := not EsKitDistribuidor;
          AU_PRE_1.Enabled := not EsKitDistribuidor;
          AU_PRE_2.Enabled := not EsKitDistribuidor;
          AU_PRE_3.Enabled := not EsKitDistribuidor;
          AU_PRE_4.Enabled := not EsKitDistribuidor;
          AU_PRE_5.Enabled := not EsKitDistribuidor;
          AU_PRE_6.Enabled := not EsKitDistribuidor;
          AU_PRE_7.Enabled := not EsKitDistribuidor;
          AU_PRE_8.Enabled := not EsKitDistribuidor;
          AU_PRE_9.Enabled := not EsKitDistribuidor;
          AU_PRE_10.Enabled := not EsKitDistribuidor;
          AU_PRE_11.Enabled := not EsKitDistribuidor;
          AU_PRE_12.Enabled := not EsKitDistribuidor;
          AU_PRE_13.Enabled := not EsKitDistribuidor;
          AU_PRE_14.Enabled := not EsKitDistribuidor;
          AU_PRE_15.Enabled := not EsKitDistribuidor;
          AU_PRE_16.Enabled := not EsKitDistribuidor;
          AU_PRE_17.Enabled := not EsKitDistribuidor;
          AU_PRE_18.Enabled := not EsKitDistribuidor;
          AU_PRE_19.Enabled := not EsKitDistribuidor;
          AU_PRE_20.Enabled := not EsKitDistribuidor;
          AU_PRE_21.Enabled := not EsKitDistribuidor;
          AU_PRE_22.Enabled := not EsKitDistribuidor;
          AU_PRE_23.Enabled := not EsKitDistribuidor;
          with AU_FEC_PRE do
          begin
               Enabled := ( AU_PRE_0.Checked or
                            AU_PRE_1.Checked or
                            AU_PRE_2.Checked or
                            AU_PRE_3.Checked or
                            AU_PRE_4.Checked or
                            AU_PRE_5.Checked or
                            AU_PRE_6.Checked or
                            AU_PRE_7.Checked or
                            AU_PRE_8.Checked or
                            AU_PRE_9.Checked or
                            AU_PRE_10.Checked or
                            AU_PRE_11.Checked or
                            AU_PRE_12.Checked or
                            AU_PRE_13.Checked or
                            AU_PRE_14.Checked or
                            AU_PRE_15.Checked or
                            AU_PRE_16.Checked or
                            AU_PRE_17.Checked or
                            AU_PRE_18.Checked or
                            AU_PRE_19.Checked or
                            AU_PRE_20.Checked or
                            AU_PRE_21.Checked or
                            AU_PRE_22.Checked or
                            AU_PRE_23.Checked ) and
                            not EsKitDistribuidor;
               if Enabled then
                  dmSentinel.HayPrestados := True
               else
               begin
                    Valor := 0;
                    dmSentinel.HayPrestados := False;
               end;
               AU_FEC_PRElbl.Enabled := Enabled;
          end;
          with AU_DEFINIT do
          begin
               if EsKitDistribuidor then
                  ItemIndex := 0;
               lTemporal := ( ItemIndex = 0 );
          end;
          {
          if EsKitDistribuidor then
          begin
               AU_SQL_BD.ItemIndex := Ord( engInterbase );
               AU_PLATFORM.ItemIndex := Ord( ptProfesional );
          end;
          }
          AU_PLATFORMlbl.Enabled := EsVersion20;
          AU_PLATFORM.Enabled := EsVersion20;
          AU_SQL_BD.Enabled := EsVersion20;
          AU_BD_LIM.Enabled := EsVersionValidacion;
          AU_VAL_LIM.Enabled := EsVersionValidacion;


          with AU_FEC_AUT do
          begin
               Enabled := lTemporal or EsKitDistribuidor;
               if lTemporal then
               begin
                    if ( Valor = 0 ) then
                       Valor := Now;
               end
               else
                   Valor := NullDateTime;
               AU_FEC_AUTlbl.Enabled := Enabled;
          end;
          with SI_COMENTA.Font do
          begin
               if SI_PROBLEMA.Checked then
               begin
                    Color := clRed;
                    Imagen.Picture.Icon.Handle := LoadIcon( 0, IDI_HAND );
               end
               else
               begin
                    Color := clBlack;
                    Imagen.Picture.Icon.ReleaseHandle;
               end;
               SI_PROBLEMA.Font.Color := Color;
               SI_COMENTAlbl.Font.Color := Color;
          end;
     end;
     //SN_SERVER.Text := dmSentinel.GetServerVirtual(StrToInt(SN_NUMERO.Text));
end;

procedure TFormSistema.SI_PROBLEMAClick(Sender: TObject);
begin
     SetControls;
end;

procedure TFormSistema.VE_CODIGOClick(Sender: TObject);
begin
     SetControls;
end;

procedure TFormSistema.AU_PRE_1Click(Sender: TObject);
begin
     SetControls;
end;

procedure TFormSistema.AU_DEFINITChange(Sender: TObject);
begin
     SetControls;
end;

procedure TFormSistema.AU_KIT_DISClick(Sender: TObject);
begin
     SetControls;
end;

procedure TFormSistema.CiudadesClick(Sender: TObject);
begin
     FPrincipal.FormPrincipal.CatalogosCiudadesClick(Self);
end;

procedure TFormSistema.SI_COMENTAExit(Sender: TObject);
begin
     PageControl.ActivePage := Direccion;
end;

procedure TFormSistema.SI_SIS_VIEExit(Sender: TObject);
begin
     PageControl.ActivePage := Autorizacion;
end;

procedure TFormSistema.ShowAvailableClick(Sender: TObject);
var
   iLibre: Integer;
begin
     if FLibres.EscogeLibre( iLibre ) then
     begin
          with dsSentinelas.DataSet do
          begin
               DisableControls;
               try
                  Filtered := False;
                  Locate('SN_NUMERO',iLibre,[] );
                  Edit;
                  FieldByName('SI_FOLIO').AsInteger := dmSentinel.GetFolioSistema;
                  Post;
                  Filtered := True;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TFormSistema.UpdateSucursal;
begin
     dmSentinel.tSistema.FieldByName('SI_SUCURSAL').AsString := LIST_SUCURSALES.Text
end;


procedure TFormSistema.OKBtnClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     { Asegura que se graben los cambios }
     with dmSentinel do
     begin
          UpdateSucursal;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try

             Application.ProcessMessages;
             {if SentinelConectado then
             begin}
                  //TODO
                  //ActualizaSentinelTerminal(FNumSentinel);
                  //Guardar Nombre de Servidor Virtual
                  //ActualizaSentinelVirtual(SN_VIRTUAL.ValorEntero,SN_SERVER.Text);
                  //GrabaCambios;
                  GrabarSistema;
                  if tSistema.State = dsBrowse then
                  begin
                       RefrescarLook;
                       ModalResult := mrOK;
                  end;
             {end;}
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TFormSistema.btnGeneraArchivoClick(Sender: TObject);
var
   oDatos2 : TStrings;
begin
     try
        with dsTerminales.DataSet do
        begin
             DisableControls;
             if not IsEmpty then
             begin
                  oDatos2 := TStringList.Create;
                  try
                     First;
                     oDatos2.Clear;
                     while not eof do
                     begin
                          if UpperCase(FieldByName('TE_ACTIVO').AsString) = 'S' then
                          begin
                               oDatos2.Add( ZetaServerTools.Encrypt( IntToStr( FieldByName('SN_NUMERO').AsInteger ) +'|'+ FieldByName('TE_NSERIE').AsString +'|'+IntToStr( FieldByName('TE_TIPO').AsInteger ) ) );
                          end;
                          Next;
                     end;
                     if oDatos2.Count > 0 then
                     begin
                          sdGuardar.FileName := 'Terminales.dat';
                          if sdGuardar.Execute then
                          begin
                               oDatos2.SaveToFile( sdGuardar.FileName );
                               ZetaDialogo.ZInformation('Sentinel',Format('Se gener� Archivo: %s con �xito !',[ sdGuardar.FileName ]),0 );
                          end;
                     end
                     else
                         ZetaDialogo.ZError( 'Sentinel', 'No existen Terminales Activas A Procesar', 0 );
                  finally
                         FreeAndNil( oDatos2 );
                  end;
             end
             else
                 ZetaDialogo.ZError( 'Sentinel', 'No existen Terminales A Procesar', 0 );
             First;
        end;
     finally
            dsTerminales.DataSet.EnableControls;
     end;
end;

procedure TFormSistema.btnArchivoLicClick(Sender: TObject);
const
   K_SERVIDOR_TAG = 'Servidor';
   K_FINGER_PRINT_DATA_TAG = 'FingerPrintData';
   K_OS_TAG ='OS';
   K_BIOS_TAG = 'Bios';
   K_SERIAL_NUMBER_TAG = 'SerialNumber';
   K_VACIO = '';
var
   oDatos2 : TStrings;
   sServidor, sOSSerialNumber, sBIOSSerialNumber,sSentinel:string;
   lServidorActual, lInfoCompleta:Boolean;
   XMLFile           : WideString;
   XML               :TdmXMLTools;
   Memo              : TDBMemo;
   xmlNodePadre      : TZetaXMLNode;
   xmlNodeChild      : TZetaXMLNode;
   xmlNodeOS         : TZetaXMLNode;
   xmlNodeBIOS       : TZetaXMLNode;

function EncuentraNodosPadre:Boolean;
var lEncontrados: Boolean;
begin
     lEncontrados := FALSE;
     with XML do
     begin
          xmlNodePadre := XMLDocument.DocumentElement;
          xmlNodePadre := GetNode(K_SERVIDOR_TAG,xmlNodePadre);
          if xmlNodePadre <> nil then
          begin
               sServidor := xmlNodePadre.Attributes['Nombre'];
               xmlNodePadre := GetNode(K_FINGER_PRINT_DATA_TAG, xmlNodePadre);
               if xmlNodePadre <> nil then
               begin
                    xmlNodeChild:= GetFirstChild(xmlNodePadre);
                    while xmlNodeChild <> nil do
                    begin
                         if xmlNodeChild.NodeName = K_OS_TAG then
                            xmlNodeOS := xmlNodeChild;
                         if xmlNodeChild.NodeName = K_BIOS_TAG then
                            xmlNodeBIOS := xmlNodeChild;
                         if (xmlNodeOS <> nil) and (xmlNodeBIOS <> nil) then
                         begin
                              lEncontrados := TRUE;
                              break; //Si ya encontro ambos nodos, termina.
                         end;
                         xmlNodeChild := xmlNodeChild.NextSibling;
                    end;
               end;
          end;
     end;
     Result := lEncontrados;
end;
function EncuentraDatos3DT: Boolean;
begin
     sOSSerialNumber := K_VACIO;
     sBIOSSerialNumber := K_VACIO;
     if (xmlNodeOS <> nil) and (xmlNodeBIOS <> nil) then
     begin
          xmlNodeChild := xmlNodeOS.ChildNodes.FindNode(K_SERIAL_NUMBER_TAG);
          if xmlNodeChild <> nil then
             sOSSerialNumber := xmlNodeChild.NodeValue; //OleVariant
          xmlNodeChild := xmlNodeBIOS.ChildNodes.FindNode(K_SERIAL_NUMBER_TAG);
          if xmlNodeChild <> nil then
             sBIOSSerialNumber := xmlNodeChild.NodeValue;
     end;
     Result := ((sOSSerialNumber <> K_VACIO) AND (sBIOSSerialNumber <> K_VACIO));
end;
begin
     lServidorActual := True;
     lInfoCompleta := False;

     sSentinel :=  dsSentinelas.DataSet.FieldByName('SN_NUMERO').AsString;
     with dmSentinel do
     begin
          //Crea temporales
          Memo := TDBMemo.Create(Self);
          oDatos2 := TStringList.Create;
          XML := TdmXMLTools.Create(NIL);

          try
             //Seleccion Ultimo 3DT
             GetHistorico3DTOrdenado(GetFolioSistema , StrToInt(sSentinel) );
             //Valida si se encontro algo
             if  tqHistorico3DTOrdenado.RecordCount > 0 then
                 lInfoCompleta := TRUE
             else
             begin
                  //Si no encontro un sentinel asociado busca registros con valores en cero:
                       //si el usuario selecciona uno se repite la operacion
                  if FSeleccionaSentinel.SeleccionaSentinel(GetFolioSistema, StrToInt(sSentinel) ) then
                  begin
                       GetHistorico3DTOrdenado(GetFolioSistema , StrToInt(sSentinel) );
                       if  tqHistorico3DTOrdenado.RecordCount > 0 then
                           lInfoCompleta := TRUE;
                  end;
             end;
             //Si ya se tiene un sentinel asociado
             if lInfoCompleta  then
             begin
                  dsHistorico3DTTemporal.DataSet := tqHistorico3DTOrdenado;
                  //Get al blob 3DT en la BD.
                  Memo.Parent := self;
                  Memo.DataField := 'HI_3DTXML';
                  Memo.DataSource := dsHistorico3DTTemporal;
                  //Crea objeto XML
                  XMLFile := Memo.Text;
                  XML := TdmXMLTools.Create(nil);
                  if StrLleno( XMLFile) then
                  begin
                       with XML do
                       begin
                            XMLDocument.XML.Text := XMLFile;
                            XMLDocument.LoadFromXML(XMLDocument.XML.Text);
                       end;
                       if EncuentraNodosPadre then
                          lInfoCompleta := EncuentraDatos3DT;  //Estan todos los datos del 3DT necesarios para general la licencia.
                  end;
                  //Si LInfoCompleta, procede con creacion de la licencia.
                  if lInfoCompleta then
                  begin
                  if dmSentinel.DetectarMaestroProxy then
                  begin
                       if dmSentinel.tSentinel.FieldByName('SN_TIPO').AsInteger = 99 then
                       begin
                            if sServidor <> '' then
                            begin
                                 if AnsiUpperCase(sServidor)  <> AnsiUpperCase(dsSentinelas.DataSet.FieldByName('SN_SERVER').AsString) then
                                 begin
                                      if not ZetaDialogo.ZConfirm('Sentinel', 'El nombre del servidor en la Base de Datos es distinto al del archivo 3DT'+CR_LF+' � Desea actualizar el nombre del servidor en la base de datos ?',0,mbOK)then
                                         lServidorActual := False
                                      else
                                      begin
                                         if not (dsSentinelas.DataSet.State in [dsEdit, dsInsert]) then
                                            dsSentinelas.Edit;
                                         dsSentinelas.DataSet.FieldByName('SN_SERVER').AsString := sServidor;
                                      end;
                                 end;
                                 if lServidorActual then //Si el nombre del servidor en la BD y en 3DT es igual
                                 begin
                                      with dlgLicencia do
                                      begin
                                           FileName := Format('%s_Virtual.lic',[sSentinel]);
                                           //Agregar las variables separadas por |
                                           //No todas las propiedades existen, hay que modificar DatosSistema3dt
                                           oDatos2.Add( ZetaServerTools.Encrypt( sServidor +'|'+ sBIOSSerialNumber+ '|' + sOSSerialNumber + '|' +sSentinel +'|'+
                                                                    SI_FOLIO.Text +'|'+ DateToStr(AU_FEC_AUT.Valor) ) );
                                           if Execute then
                                           begin
                                                oDatos2.SaveToFile( dlgLicencia.FileName );
                                                ZetaDialogo.ZInformation('Sentinel',Format('Se gener� Archivo: %s con �xito !',[ dlgLicencia.FileName ]),0 );
                                           end;
                                      end;
                                 end;
                            end
                            else
                            begin
                                 ZetaDialogo.ZWarning('Sentinel','Definir Nombre del Servidor Virtual ',0,mbOk);
                            end;

                       end
                       else
                           ZetaDialogo.ZError('Sentinel','Seleccionar Sentinel Tipo Virtual',0);
                  end;
                  end
                  else
                      ZetaDialogo.ZError('Sentinel','Los datos del historial 3DT estan incompletos.',0);
             end
             else
                 ZetaDialogo.ZError('Sentinel','Debe haber almenos un archivo 3DT asociado a este sentinel.',0);
          finally
                 FreeAndNil(oDatos2);
                 FreeAndNil(Memo);
                 XML.Free;
          end;
     end;
end;

procedure TFormSistema.cbxTipoTerminalChange(Sender: TObject);
begin
     with dmSentinel do
     begin
         TipoTerminal := cbxTipoTerminal.ItemIndex;

         if tTerminales.State <> dsEdit then
            dmSentinel.tTerminales.Edit;
         cbxTipoTerminal.ItemIndex := TipoTerminal;
     end;
end;

procedure TFormSistema.dsTerminalesDataChange(Sender: TObject;
  Field: TField);
begin
     if Field = nil then
     begin
          cbxTipoTerminal.ItemIndex := dmSentinel.tTerminales.FieldByName('TE_TIPO').AsInteger
     end;
end;
procedure AbreDocumentoBDE( DataSet : TDataSet; const sBlobField, sExtension : string );
var
   sArchivo : string;
begin
     with Dataset do
     begin
          with TBlobField( FieldByName( sBlobField ) ) do
          begin
               if NOT IsNull then
               begin
                    sArchivo := GetTempFile( K_PREFIJO_TEMP, sExtension );
                    if StrLleno( sArchivo ) then
                    begin
                         //CV: Nota: el archivo temporal que se esta generando en este momento,
                         //no se puede borrar en este metodo, porque, si se borra, las aplicaciones
                         //que no abren de forma exclusiva los archivos, se cuatrapean.
                         SaveTofile( sArchivo );
                         try
                            ExecuteFile ( sArchivo, '', '', SW_SHOWDEFAULT );
                         except
                               On E:Exception do
                                  ZetaDialogo.ZError('Problemas al Abrir Documento', 'El Documento no Contiene Informaci�n', 0 );
                         end;
                    end;
               end
               else
                  ZetaDialogo.ZInformation( 'Problemas al Abrir Documento', 'El Documento no Contiene Informaci�n', 0 );
          end;
     end;
end;

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[0..79] of Char;
begin
     Result := ShellExecute( Application.MainForm.Handle,
                             nil,
                             StrPCopy( zFileName, FileName ),
                             StrPCopy( zParams, Params ),
                             StrPCopy( zDir, DefaultDir ),
                             ShowCmd );
end;


procedure TFormSistema.dsSentinelasDataChange(Sender: TObject;
  Field: TField);
begin
     if ( Field = nil ) then
     begin
          with dmSentinel do
          begin
               with dmSentinel do
               begin
                    if not FInserting then
                    begin
                         AbrirPrincipalHistoria(dsSentinelas.DataSet.FieldByName('SN_NUMERO').AsInteger);
                         ActualizaAutorizacion;
                    end;
               end;
          end;
     end;
end;

procedure TFormSistema.ActualizaAutorizacion;
begin
     with dmSentinel.tAutoriza do
     begin
          Cancel;
          Locate('AU_FOLIO',dmSentinel.tqAutoriza.FieldByName('AU_FOLIO').AsInteger,[]);
          Edit;
     end;
end;


procedure TFormSistema.LlenarSentinelas();
begin
     with dsSentinelas.DataSet do
     begin
          zSentinelas.Lista.Clear;
          First;
          while not Eof do
          begin
               //zSentinelas.Items.Add(FieldByName('SN_NUMERO').AsString +'='+FieldByName('SN_NOMBRE').AsString);
               if FieldByName('SN_ACTIVO').AsInteger = 1 then
               begin
                    zSentinelas.Lista.AddObject(FieldByName('SN_NUMERO').AsString +'='+FieldByName('SN_NUMERO').AsString +':'+FieldByName('SN_NOMBRE').AsString ,TObject(FieldByName('SN_NUMERO').AsInteger));
               end;
               Next;
          end;
     end;
end;

procedure TFormSistema.zSentinelasChange(Sender: TObject);
begin
     dsSentinelas.DataSet.Locate('SN_NUMERO',(zSentinelas.LlaveEntero),[]);
     FiltrarTerminales;
     FiltrarArchivos3dt;

end;

procedure TFormSistema.btnGeneraAutorizacionClick(Sender: TObject);
var
   oCursor:TCursor;
begin
     with dmSentinel.tAutoriza do
     begin
          if State = dsBrowse then
             Edit;
     end;
         
     AU_FEC_AUT.Perform( CM_EXIT, 0, 0);
     Application.ProcessMessages;
     AU_FEC_PRE.Perform( CM_EXIT, 0, 0);
     Application.ProcessMessages;
     with dmSentinel do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             Application.ProcessMessages;
             tAutoriza.FieldByName('SN_NUMERO').AsInteger := zSentinelas.LlaveEntero;
             tAutoriza.FieldByName('SI_FOLIO').AsInteger := GetFolioSistema;
             tAutoriza.FieldByName('DI_CODIGO').AsString := dsSistema.DataSet.FieldByName('DI_CODIGO').AsString;
             if ValidaCampos then
             begin
                  //TODO
                  //ActualizaSentinelTerminal(FNumSentinel);
                  //Guardar Nombre de Servidor Virtual
                  //GrabaCambios;
                  AgregaAutorizacion;
                  ShowMessage(Format('Autorizaci�n: %d Generada con Exito',[dmSentinel.tAutoriza.FieldByName('AU_FOLIO').AsInteger] ) );
                  PageControl1.ActivePage := HistoAutorizaciones;
                  AbrirPrincipalHistoria(zSentinelas.LlaveEntero);
                  //ModalResult := mrOK;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TFormSistema.FiltrarTerminales;
begin
     with dmSentinel do
     begin
          with tTerminales do
          begin
               Filtered := False;
               Filter := 'SI_FOLIO = ' + IntToStr(tqActual.FieldByName( 'SI_FOLIO' ).AsInteger) +' and  SN_NUMERO = ' +IntToStr(zSentinelas.LlaveEntero);
               Filtered := True;
          end;
     end;
end;


procedure TFormSistema.FiltrarArchivos3dt;
begin

end;

procedure TFormSistema.CancelarBtnClick(Sender: TObject);
begin
     with dmSentinel do
     begin
          tSistema.Cancel;
          RefrescarLook;
     end;
     Close;
end;

procedure TFormSistema.PageControlChange(Sender: TObject);
begin
     if PageControl.ActivePage =  Autorizacion then
     begin
          LlenarSentinelas;
          zSentinelas.ItemIndex := 0;
          dsSentinelas.DataSet.Locate('SN_NUMERO',(zSentinelas.LlaveEntero),[]);
          PageControl1.ActivePage := GeneraAutorizacion;
     end;

end;

procedure TFormSistema.BitBtn1Click(Sender: TObject);
begin
     if dmSentinel.DetectarMaestroProxy then
     begin
          with dsSentinelas.DataSet do
          begin
               Append;
               FieldByName('SI_FOLIO').AsInteger := dmSentinel.GetFolioSistema;
               FieldByName('SN_NUMERO').AsInteger := dmSentinel.SiguienteSentinelVirtual;
               FieldByName('SN_TIPO').AsInteger := 99;
               FieldByName('SN_ACTIVO').AsInteger := 1;
               FieldByName('SN_FEC_INI').AsDateTime := Now;
          end;
     end;
end;

procedure TFormSistema.bbGrabaSentinelClick(Sender: TObject);
begin
     With dmSentinel do
     begin

          if DetectarMaestroProxy then
          begin
               if tSentinel.FieldByName('SN_TIPO').AsInteger <> 99 then
               begin
                    IF SentinelConectado then
                    begin
                         GrabarSentinelSistema( GetFolioSistema, tSentinel.FieldByName('SN_NUMERO').AsInteger );
                    end;
               end
               else
                   ZetaDialogo.ZError('Sentinel','Seleccionar un Sentinel Tipo USB',0); 
          end;
     end;
end;

procedure TFormSistema.DBGrid2DblClick(Sender: TObject);
begin
     FormPrincipal.ShowHistoria(dsAutoriza.Dataset);  
end;

procedure TFormSistema.btnRemoverSentinelClick(Sender: TObject);
var
   iSentinel:Integer;
begin
     if ZetaDialogo.ZConfirm('Sentinel','�Seguro de Remover Sentinel de Empresa?',0,mbOk)then
     begin
          if dsAutoriza.DataSet.RecordCount = 0 then
          begin
               iSentinel := dsSentinelas.DataSet.FieldByName('SN_NUMERO').AsInteger;

               with dmSentinel.tSentinel do
               begin
                    DisableControls;
                    try
                       Filtered := False;
                       Locate('SN_NUMERO',iSentinel,[]);
                       if FieldByName('SN_TIPO').AsInteger = 99 then
                       begin
                            Delete;
                       end
                       else
                       begin
                            Edit;
                            FieldByName('SI_FOLIO').AsInteger := 0;
                            Post;
                       end;
                       Filtered := True;
                       finally
                              EnableControls;
                       end;
               end;
          end
          else
              ZetaDialogo.ZError('Sentinel','No se puede Remover Sentinel con Historial de Claves ;Favor de Des-Activarlo',0); 
     end;
end;

end.
