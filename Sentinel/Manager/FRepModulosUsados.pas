unit FRepModulosUsados;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, Quickrpt, Qrctrls;

type
  TRepModulosUsados = class(TForm)
    qrp1: TQuickRep;
    PageHeaderBand1: TQRBand;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRTitulo: TQRLabel;
    QRImage1: TQRImage;
    QRLabel18: TQRLabel;
    QRSysData1: TQRSysData;
    QRShape4: TQRShape;
    Banda: TQRBand;
    SI_FOLIO: TQRDBText;
    SI_RAZ_SOC: TQRDBText;
    AU_FOLIOlbl: TQRLabel;
    SI_RAZ_SOClbl: TQRLabel;
    AU_MOD_0: TQRDBText;
    AU_MOD_0lbl: TQRLabel;
    AU_MOD_1: TQRDBText;
    AU_MOD_1lbl: TQRLabel;
    QRLabel3: TQRLabel;
    AU_MOD_2: TQRDBText;
    QRLabel4: TQRLabel;
    AU_MOD_3: TQRDBText;
    QRLabel6: TQRLabel;
    AU_MOD_4: TQRDBText;
    AU_MOD_5lbl: TQRLabel;
    AU_MOD_5: TQRDBText;
    AU_MOD_6: TQRDBText;
    AU_MOD_6lbl: TQRLabel;
    AU_MOD_7lbl: TQRLabel;
    AU_MOD_7: TQRDBText;
    AU_MOD_8: TQRDBText;
    AU_MOD_9: TQRDBText;
    AU_MOD_10: TQRDBText;
    AU_MOD_11: TQRDBText;
    AU_MOD_12: TQRDBText;
    AU_MOD_13: TQRDBText;
    AU_MOD_8lbl: TQRLabel;
    AU_MOD_9lbl: TQRLabel;
    AU_MOD_10lbl: TQRLabel;
    AU_MOD_11lbl: TQRLabel;
    AU_MOD_12lbl: TQRLabel;
    AU_MOD_13lbl: TQRLabel;
    AU_MOD_14: TQRDBText;
    AU_MOD_15: TQRDBText;
    AU_MOD_16: TQRDBText;
    AU_MOD_14lbl: TQRLabel;
    AU_MOD_15lbl: TQRLabel;
    AU_MOD_16lbl: TQRLabel;
    AU_MOD_17: TQRDBText;
    AU_MOD_18: TQRDBText;
    AU_MOD_17lbl: TQRLabel;
    AU_MOD_18lbl: TQRLabel;
    AU_MOD_19: TQRDBText;
    AU_MOD_19lbl: TQRLabel;
    AU_MOD_20: TQRDBText;
    AU_MOD_20lbl: TQRLabel;
    AU_MOD_21: TQRDBText;
    AU_MOD_22: TQRDBText;
    AU_MOD_21lbl: TQRLabel;
    AU_MOD_22lbl: TQRLabel;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    procedure QRSysData1Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    FImpresora: Boolean;
    procedure SetImpresora(const Value: Boolean);
  public
    { Public declarations }
    property Impresora: Boolean read FImpresora write SetImpresora;
  end;

var
  RepModulosUsados: TRepModulosUsados;

implementation

uses DRep;

{$R *.DFM}

procedure TRepModulosUsados.QRSysData1Print(sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy', Now );
end;

procedure TRepModulosUsados.SetImpresora(const Value: Boolean);
begin
     FImpresora := Value;
     PageHeaderBand1.Enabled := FImpresora;
end;

end.
