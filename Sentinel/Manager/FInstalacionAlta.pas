unit FInstalacionAlta;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Mask,
     ZetaNumero,
     ZetaKeyCombo,
     ZetaDBTextBox;

type
  TInstalacionAlta = class(TForm)
    PanelBotones: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    InstalacionLBL: TLabel;
    SistemaLBL: TLabel;
    RazonSocialLBL: TLabel;
    RazonSocial: TZetaTextBox;
    Sistema: TZetaTextBox;
    Instalacion: TZetaNumero;
    SentinelLBL: TLabel;
    Sentinel: TZetaNumero;
    DistribuidorLBL: TLabel;
    Distribuidor: TZetaKeyCombo;
    ComentarioLBL: TLabel;
    Comentario: TMemo;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSistema: Integer;
  public
    { Public declarations }
  end;

var
  InstalacionAlta: TInstalacionAlta;

implementation

uses ZetaDialogo,
     DSentinel;

{$R *.DFM}

procedure TInstalacionAlta.FormShow(Sender: TObject);
var
   oCursor: TCursor;
   iSentinel, iInstalacion: Integer;
   sDistribuidor: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmSentinel do
        begin
             with tqSistema do
             begin
                  FSistema := FieldByName( 'SI_FOLIO' ).AsInteger;
                  Sistema.Caption := IntToStr( FSistema );
                  RazonSocial.Caption := FieldByName( 'SI_RAZ_SOC' ).AsString;
             end;
             ListaDistribuidores( Distribuidor.Lista );
             InstalacionNueva( FSistema, iSentinel, iInstalacion, sDistribuidor );
             Sentinel.Valor := iSentinel;
             Instalacion.Valor := iInstalacion;
             with Distribuidor do
             begin
                  ItemIndex := Lista.IndexOfName( sDistribuidor );
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TInstalacionAlta.OKClick(Sender: TObject);
var
   oCursor: TCursor;
   iSentinel, iInstalacion: Integer;
   sDistribuidor, sComentario, sClave: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iSentinel := Sentinel.ValorEntero;
        iInstalacion := Instalacion.ValorEntero;
        sDistribuidor := Distribuidor.Llave;
        sComentario := Comentario.Lines.Text;
        if dmSentinel.InstalacionGenerar( FSistema, iSentinel, iInstalacion, sDistribuidor, sComentario, sClave ) then
        begin
             ZetaDialogo.zInformation( 'Clave Fu� Generada', 'Se Gener� La Clave ' + sClave, 0 );
             ModalResult := mrOk;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
