unit FSeleccionaSentinel;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls,
     ZBaseEscogeGrid,
     ZetaDBGrid;

type
  TEscogeSentinel = class(TBaseEscogeGrid)
    procedure GridDblClick(Sender: TObject);
  private
    { Private declarations }
    FComentario: string;
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    property Comentario: string read FComentario;
    { Public declarations }
  end;

function SeleccionaSentinel(iFolio,iSentinel: Integer): boolean;

var
  EscogeSentinel: TEscogeSentinel;

implementation

uses DSentinel,
     ZetaCommonClasses;

{$R *.DFM}

function SeleccionaSentinel(iFolio,iSentinel: Integer): boolean;
var
   SeleccionaSentinel: TEscogeSentinel;
begin
     try
        SeleccionaSentinel := TEscogeSentinel.Create( Application.MainForm );
        with SeleccionaSentinel do
        begin
           ShowModal;
           Result := ( ModalResult = mrOk );
           if Result then
                //Actualiza no. sentinel y folio para el registro seleccionado.
                dmSentinel.ModificaHistorico3DT(iFolio,iSentinel, SeleccionaSentinel.Comentario);
        end;
     finally
        FreeAndNil( SeleccionaSentinel );
     end;
end;

{ *********** TEscogeGlobal ********* }

procedure TEscogeSentinel.Connect;
begin
     with dmSentinel do
     begin
         GetHistorico3DTOrdenado(0 , 0 );
         DataSource.DataSet := tqHistorico3DTOrdenado;
     end;
end;

procedure TEscogeSentinel.GridDblClick(Sender: TObject);
begin
  inherited;
   FComentario := DataSource.DataSet.FieldByName('HI_COMENTA').AsString;
   ModalResult := mrOk;
end;

end.

