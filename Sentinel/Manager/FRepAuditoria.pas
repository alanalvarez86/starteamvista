unit FRepAuditoria;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, Quickrpt, Qrctrls;

type
  TRepAudit = class(TForm)
    qrp1: TQuickRep;
    PageHeaderBand1: TQRBand;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRTitulo: TQRLabel;
    QRImage1: TQRImage;
    QRLabel18: TQRLabel;
    QRSysData1: TQRSysData;
    QRShape4: TQRShape;
    QRGroup1: TQRGroup;
    DI_NOMBRElbl: TQRLabel;
    SI_FOLIO: TQRDBText;
    SI_RAZ_SOClbl: TQRLabel;
    SI_RAZ_SOC: TQRDBText;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRInicio: TQRSysData;
    QRLabel2: TQRLabel;
    QRFinal: TQRSysData;
    AU_FOLIO: TQRDBText;
    AU_FECHA: TQRDBText;
    SN_NUMERO: TQRDBText;
    VE_CODIGO: TQRDBText;
    AU_NUM_EMP: TQRDBText;
    AU_USERS: TQRDBText;
    AU_SQL_BD: TQRDBText;
    AU_PLATFORM: TQRDBText;
    AU_FOLIOlbl: TQRLabel;
    AU_FECHAlbl: TQRLabel;
    SN_NUMEROlbl: TQRLabel;
    VE_CODIGOlbl: TQRLabel;
    AU_NUM_EMPlbl: TQRLabel;
    AU_USERSlbl: TQRLabel;
    AU_SQL_BDlbl: TQRLabel;
    AU_PLATFORMlbl: TQRLabel;
    AU_TIPOS: TQRDBText;
    AU_FEC_AUT: TQRDBText;
    AU_FEC_AUTlbl: TQRLabel;
    AU_MOD_0: TQRDBText;
    AU_MOD_0lbl: TQRLabel;
    AU_MOD_1: TQRDBText;
    AU_MOD_1lbl: TQRLabel;
    QRLabel3: TQRLabel;
    AU_MOD_2: TQRDBText;
    QRLabel4: TQRLabel;
    AU_MOD_3: TQRDBText;
    QRLabel6: TQRLabel;
    AU_MOD_4: TQRDBText;
    AU_MOD_5lbl: TQRLabel;
    AU_MOD_5: TQRDBText;
    AU_MOD_6: TQRDBText;
    AU_MOD_6lbl: TQRLabel;
    AU_MOD_7lbl: TQRLabel;
    AU_MOD_7: TQRDBText;
    AU_MOD_8: TQRDBText;
    AU_MOD_9: TQRDBText;
    AU_MOD_10: TQRDBText;
    AU_MOD_11: TQRDBText;
    AU_MOD_12: TQRDBText;
    AU_MOD_13: TQRDBText;
    AU_MOD_8lbl: TQRLabel;
    AU_MOD_9lbl: TQRLabel;
    AU_MOD_10lbl: TQRLabel;
    AU_MOD_11lbl: TQRLabel;
    AU_MOD_12lbl: TQRLabel;
    AU_MOD_13lbl: TQRLabel;
    procedure QRSysData1Print(sender: TObject; var Value: String);
    procedure QRInicioPrint(sender: TObject; var Value: String);
    procedure QRFinalPrint(sender: TObject; var Value: String);
  private
    { Private declarations }
    FFinal: TDateTime;
    FInicio: TDateTime;
    FImpresora: Boolean;
    procedure SetImpresora(const Value: Boolean);
  public
    { Public declarations }
    property Impresora: Boolean read FImpresora write SetImpresora;
    property Inicio: TDateTime read FInicio write FInicio;
    property Final: TDateTime read FFinal write FFinal;
  end;

var
  RepAudit: TRepAudit;

implementation

uses DRep;

{$R *.DFM}

procedure TRepAudit.QRSysData1Print(sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy', Now );
end;

procedure TRepAudit.QRInicioPrint(sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy', FInicio );
end;

procedure TRepAudit.QRFinalPrint(sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy', FFinal );
end;

procedure TRepAudit.SetImpresora(const Value: Boolean);
begin
     FImpresora := Value;
     PageHeaderBand1.Enabled := FImpresora;
end;

end.
