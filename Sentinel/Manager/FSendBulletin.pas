unit FSendBulletin;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Psock, NMsmtp, ExtCtrls,
     ComCtrls, IniFiles, IB_Components, IBDataset, Db,
     ZetaAsciiFile;

{$define UNO_POR_UNO}

type
  TBulletinSender = class(TForm)
    Email: TNMSMTP;
    IBODatabase: TIBODatabase;
    eMailGB: TGroupBox;
    ServidorLBL: TLabel;
    UsuarioLBL: TLabel;
    RemitenteLBL: TLabel;
    RemitenteDireccionLBL: TLabel;
    Servidor: TEdit;
    Usuario: TEdit;
    Remitente: TEdit;
    RemitenteDireccion: TEdit;
    DatabaseGB: TGroupBox;
    DatabaseLBL: TLabel;
    DBUsuarioLBL: TLabel;
    DBPasswordLBL: TLabel;
    DBFiltroLBL: TLabel;
    Database: TEdit;
    DBUsuario: TEdit;
    DBPassword: TEdit;
    DBFiltro: TEdit;
    PanelInferior: TPanel;
    Enviar: TBitBtn;
    Conectar: TBitBtn;
    Desconectar: TBitBtn;
    Salir: TBitBtn;
    IBOQuery: TIBOQuery;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    OpenDialog: TOpenDialog;
    ArchivoFind: TSpeedButton;
    StatusBar: TStatusBar;
    TemaLBL: TLabel;
    Tema: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EnviarClick(Sender: TObject);
    procedure EmailAuthenticationFailed(var Handled: Boolean);
    procedure EmailConnectionFailed(Sender: TObject);
    procedure EmailConnectionRequired(var Handled: Boolean);
    procedure EmailFailure(Sender: TObject);
    procedure EmailInvalidHost(var Handled: Boolean);
    procedure EmailRecipientNotFound(Recipient: String);
    procedure ConectarClick(Sender: TObject);
    procedure IBODatabaseAfterConnect(Sender: TIB_Connection);
    procedure IBODatabaseAfterDisconnect(Sender: TIB_Connection);
    procedure DesconectarClick(Sender: TObject);
    procedure ArchivoFindClick(Sender: TObject);
  private
    { Private declarations }
    FRecords: Integer;
    FBitacora: TAsciiLog;
    function ConectaHost: Boolean;
    function DesconectaHost: Boolean;
    function GetFromAddress: String;
    function GetFromName: String;
    function GetHost: String;
    function GetUserID: String;
    {$ifdef UNO_POR_UNO}
    procedure SendEmail( const sDestinatario: String );
    {$else}
    procedure SendEmail;
    {$endif}
    procedure SetControls;
    procedure ShowStatus(const sMensaje: String);
    procedure MuestraError(const sError: String);
    procedure MuestraExcepcion(const sError: String; Error: Exception);
  public
    { Public declarations }
  end;

var
  BulletinSender: TBulletinSender;

implementation

uses ZetaDialogo;

{$R *.DFM}

{ ******** TBkup ************ }

procedure TBulletinSender.FormCreate(Sender: TObject);
begin
     FBitacora := TAsciiLog.Create;
end;

procedure TBulletinSender.FormShow(Sender: TObject);
begin
     SetControls;
end;

procedure TBulletinSender.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FBitacora );
end;

procedure TBulletinSender.MuestraError( const sError: String );
begin
     FBitacora.WriteTexto( sError );
end;

procedure TBulletinSender.MuestraExcepcion( const sError: String; Error: Exception );
begin
     FBitacora.WriteException( 0, sError, Error );
end;

procedure TBulletinSender.SetControls;
begin
     with IBODatabase do
     begin
          Conectar.Enabled := not Connected;
          Desconectar.Enabled := Connected;
          Enviar.Enabled := Connected;
          if Connected then
             ShowStatus( 'Conectado' )
          else
              ShowStatus( '' );
     end;
end;

procedure TBulletinSender.ShowStatus( const sMensaje: String );
begin
     StatusBar.SimpleText := sMensaje;
end;

function TBulletinSender.GetFromAddress: String;
begin
     Result := RemitenteDireccion.Text;
end;

function TBulletinSender.GetFromName: String;
begin
     Result := Remitente.Text;
end;

function TBulletinSender.GetHost: String;
begin
     Result := Servidor.Text;
end;

function TBulletinSender.GetUserID: String;
begin
     Result := Usuario.Text;
end;

function TBulletinSender.ConectaHost: Boolean;
begin
     with Email do
     begin
          if not Connected then
          begin
               ClearParameters;
               Host := GetHost;
               UserID := GetUserId;
               PostMessage.FromAddress := GetFromAddress;
               PostMessage.FromName := GetFromName;
               Connect;
          end;
          Result := Connected;
     end;
end;

function TBulletinSender.DesconectaHost: Boolean;
begin
     with Email do
     begin
          if Connected then
          begin
               Disconnect;
          end;
          Result := Connected;
     end;
end;

{$ifdef UNO_POR_UNO}
procedure TBulletinSender.SendEmail( const sDestinatario: String );
{$else}
procedure TBulletinSender.SendEmail;
{$endif}
begin
     with Email do
     begin
          with PostMessage do
          begin
               Date := DateToStr( Now );
               Subject := Tema.Text;
               Attachments.Clear;
               ToCarbonCopy.Clear;
               {$ifdef UNO_POR_UNO}
               ToBlindCarbonCopy.Clear;
               with ToAddress do
               begin
                    Clear;
                    Add( sDestinatario );
               end;
               {$else}
               ToAddress.Clear;
               with ToBlindCarbonCopy do
               begin
                    Clear;
                    with IBOQuery do
                    begin
                         while not Eof do
                         begin
                              Add( FieldByName( 'BL_EMAIL' ).AsString );
                              Next;
                         end;
                    end;
               end;
               {$endif}
               with Body do
               begin
                    Clear;
                    LoadFromFile( Archivo.Text );
               end;
          end;
          SendMail;
          Sleep( 500 );
     end;
end;

procedure TBulletinSender.EmailAuthenticationFailed(var Handled: Boolean);
begin
     MuestraError( 'eMail Authentication Failed' );
     Handled := True;
end;

procedure TBulletinSender.EmailConnectionFailed(Sender: TObject);
begin
     MuestraError( 'eMail Connection Failed' );
end;

procedure TBulletinSender.EmailConnectionRequired(var Handled: Boolean);
begin
     MuestraError( 'eMail Connection Required' );
     Handled := True;
end;

procedure TBulletinSender.EmailFailure(Sender: TObject);
begin
     MuestraError( 'eMail General Failure' );
end;

procedure TBulletinSender.EmailInvalidHost(var Handled: Boolean);
begin
     MuestraError( Format( 'Servidor Inv�lido: %s', [ eMail.Host ] ) );
     Handled := True;
end;

procedure TBulletinSender.EmailRecipientNotFound(Recipient: String);
begin
     MuestraError( Format( 'Destinatario No Encontrado: %s', [ Recipient ] ) );
end;

procedure TBulletinSender.IBODatabaseAfterConnect(Sender: TIB_Connection);
begin
     SetControls;
end;

procedure TBulletinSender.IBODatabaseAfterDisconnect(Sender: TIB_Connection);
begin
     SetControls;
end;

procedure TBulletinSender.ArchivoFindClick(Sender: TObject);
begin
     with Archivo do
     begin
          with OpenDialog do
          begin
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TBulletinSender.ConectarClick(Sender: TObject);
const
     Q_EMAIL_COUNT = 'select COUNT(*) from BOLETIN %s';
     Q_EMAIL_LIST = 'select BL_EMAIL from BOLETIN %s order by BL_EMAIL';
var
   oCursor: TCursor;
   sFiltro: String;
begin
     sFiltro := DBFiltro.Text;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with IBOQuery do
           begin
                Active := False;
           end;
           with IBODatabase do
           begin
                Connected := False;
                DatabaseName := Self.Database.Text;
                UserName := DBUsuario.Text;
                Password := DBPassword.Text;
                Connected := True;
           end;
           with IBOQuery do
           begin
                Active := False;
                with SQL do
                begin
                     Clear;
                     if ( sFiltro = '' ) then
                        Add( Format( Q_EMAIL_COUNT, [ '' ] ) )
                     else
                         Add( Format( Q_EMAIL_COUNT, [ Format( 'where %s', [ sFiltro ] ) ] ) );
                end;
                Active := True;
                if IsEmpty then
                   FRecords := 0
                else
                    FRecords := Fields[ 0 ].AsInteger;
                Active := False;
                if ( FRecords > 0 ) then
                begin
                     ShowStatus( Format( '%6.0n Direcciones', [ FRecords / 1 ] ) );
                     with SQL do
                     begin
                          Clear;
                          if ( sFiltro = '' ) then
                             Add( Format( Q_EMAIL_LIST, [ '' ] ) )
                          else
                              Add( Format( Q_EMAIL_LIST, [ Format( 'where %s', [ sFiltro ] ) ] ) );
                     end;
                     Active := True;
                end
                else
                    DatabaseError( 'No Hay Direcciones De e-Mail Que Cumplan Con El Filtro Especificado' );
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
                   with IBODatabase do
                   begin
                        Connected := False;
                   end;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBulletinSender.DesconectarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with IBOQuery do
           begin
                Active := False;
           end;
           with IBODatabase do
           begin
                Connected := False;
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBulletinSender.EnviarClick(Sender: TObject);
var
   oCursor: TCursor;
   {$ifdef UNO_POR_UNO}
   i, j: Integer;
   sDireccion: String;
   {$endif}
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FBitacora.Init( ChangeFileExt( Application.ExeName, '.log' ) );
        try
           with FBitacora do
           begin
                WriteTexto( '' );
                WriteTimeStamp( StringOfChar( '*', 10 ) + ' %s ' + StringOfChar( '*', 10 ) );
                WriteTexto( '' );
           end;
           try
              if ConectaHost then
              begin
                   try
                      with IBOQuery do
                      begin
                           if Active then
                           begin
                                {$ifdef UNO_POR_UNO}
                                i := 0;
                                j := 0;
                                while not Eof do
                                begin
                                     Inc( i );
                                     ShowStatus( Format( 'Enviando Mensaje a Direcci�n %d de %d', [ i, FRecords ] ) );
                                     sDireccion := FieldByName( 'BL_EMAIL' ).AsString;
                                     try
                                        SendEMail( sDireccion );
                                        Inc( j );
                                     except
                                           on Error: Exception do
                                           begin
                                                MuestraExcepcion( Format( 'Error Al Enviar e-Mail A %s', [ sDireccion ] ), Error );
                                           end;
                                     end;
                                     Next;
                                end;
                                ShowStatus( Format( 'El Mensaje Fu� Enviado A %d de %d Direcciones ( %5.2n %s )', [ j, FRecords, 100 * j / FRecords, '%' ] ) );
                                {$else}
                                try
                                   SendEMail;
                                   ShowStatus( Format( 'El Mensaje Fu� Enviado A %d Direcciones', [ FRecords ] ) );
                                except
                                      on Error: Exception do
                                      begin
                                           MuestraExcepcion( 'Error Al Enviar e-Mails', Error );
                                      end;
                                end;
                                {$endif}
                           end;
                      end;
                   except
                         on Error: Exception do
                         begin
                              Application.HandleException( Error );
                         end;
                   end;
              end
              else
                  MuestraError( Format( 'El Servidor De Email ( %s ) No Se Pudo Conectar', [ GetHost ] ) );
           finally
                  DesconectaHost;
           end;
           with FBitacora do
           begin
                WriteTexto( '' );
                WriteTimeStamp( StringOfChar( '*', 10 ) + ' %s ' + StringOfChar( '*', 10 ) );
                WriteTexto( '' );
           end;
        finally
               FBitacora.Close;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
