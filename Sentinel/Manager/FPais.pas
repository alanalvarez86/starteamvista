unit FPais;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     DBCtrls, ExtCtrls, StdCtrls,
     FCatalogo, Buttons;

type
  TFormPais = class(TFormCatalogo)
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPais: TFormPais;

implementation

uses DSentinel;

{$R *.DFM}

procedure TFormPais.FormShow(Sender: TObject);
begin
     inherited;
     dmSentinel.AbrirPais( dsCatalogo );
end;

procedure TFormPais.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmSentinel.ResetDatasource( dsCatalogo );
end;

end.
