unit CuboSentinel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TeeProcs, TeEngine, Chart, mxgraph, Grids, mxgrid, ComCtrls, mxDB, Db,
  DBTables, mxtables, mxstore, ExtCtrls, mxpivsrc, Series;

type
  TCubo = class(TForm)
    DecisionPivot1: TDecisionPivot;
    DecisionCube1: TDecisionCube;
    DecisionQuery1: TDecisionQuery;
    DecisionSource1: TDecisionSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DecisionGrid1: TDecisionGrid;
    DecisionGraph1: TDecisionGraph;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Cubo: TCubo;

implementation

uses DSentinel;

{$R *.DFM}

procedure TCubo.FormShow(Sender: TObject);
begin
     PageControl1.ActivePage := TabSheet1;
end;

end.
