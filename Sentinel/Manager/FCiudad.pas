unit FCiudad;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     DBCtrls, ExtCtrls, StdCtrls,
     FCatalogo, Buttons;

type
  TFormCiudad = class(TFormCatalogo)
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCiudad: TFormCiudad;

implementation

uses DSentinel;

{$R *.DFM}

procedure TFormCiudad.FormShow(Sender: TObject);
begin
     inherited;
     dmSentinel.AbrirCiudad( dsCatalogo );
end;

procedure TFormCiudad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmSentinel.ResetDataSource( dsCatalogo );
end;

end.
