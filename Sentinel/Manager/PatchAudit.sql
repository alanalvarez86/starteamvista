/* Extract Database f-server:D:\Datos\Users\Lety\Sentinel\Sentinel.gdb */
/* connect "DELL-GERMAN2:D:\Sentinel\sentinel.gdb" USER "SYSDBA" PASSWORD "m"; */
connect "TRESS_2001:E:\Admon\Sentinel\sentinel.gdb" USER "SYSDBA" PASSWORD "m"; 

/* Table: AUDIT, Owner: SYSDBA */
CREATE TABLE AUDIT (
       SI_FOLIO    FOLIOSISTEMA,
       SN_NUMERO   NUMSENTINEL,
       AD_FOLIO    FOLIOCHICO,
       AD_FECHA    FECHA,
       US_CODIGO   USUARIO,
       AD_VERSION  DESC_LARGA,
       AD_NUM_EMP  EMPLEADOS,
       AD_USERS    FOLIOCHICO,
       AD_SQL_BD   FOLIOCHICO,
       AD_PLATFORM FOLIOCHICO,
       AD_KIT_DIS  BOOLEANO,
       AD_ARCHIVO  DESC_LARGA );

ALTER TABLE AUDIT
       ADD PRIMARY KEY ( SI_FOLIO, SN_NUMERO, AD_FOLIO );

CREATE DOMAIN CodigoEmpresa CHAR(10) DEFAULT '' NOT NULL;

/* Table: AUDIT_CIA, Owner: SYSDBA */
CREATE TABLE AUDIT_CIA (
       SI_FOLIO    FOLIOSISTEMA,
       SN_NUMERO   NUMSENTINEL,
       AD_FOLIO    FOLIOCHICO,
       CM_CODIGO   CodigoEmpresa,
       CM_NOMBRE   DESC_LARGA,
       AC_ACTIVOS  EMPLEADOS,
       AC_INACTIV  EMPLEADOS,
       AC_TOTALES  EMPLEADOS,
       AC_NIVEL0   CODIGO,
       AC_DB_NAME  DESC_LARGA,
       AC_ULT_ALT  FECHA,
       AC_ULT_BAJ  FECHA,
       AC_ULT_TAR  FECHA,
       AC_ULT_NOM  DESC_LARGA );

ALTER TABLE AUDIT_CIA
       ADD PRIMARY KEY ( SI_FOLIO, SN_NUMERO, AD_FOLIO, CM_CODIGO );

commit;
set term !!;

CREATE TRIGGER TD_AUDIT FOR AUDIT AFTER DELETE
AS
begin
     delete from AUDIT_CIA where
     ( AUDIT_CIA.SI_FOLIO = OLD.SI_FOLIO ) and
     ( AUDIT_CIA.SN_NUMERO = OLD.SN_NUMERO ) and
     ( AUDIT_CIA.AD_FOLIO = OLD.AD_FOLIO );
end !!

CREATE TRIGGER TU_AUDIT FOR AUDIT AFTER UPDATE
AS
begin
     if ( ( NEW.SI_FOLIO <> OLD.SI_FOLIO ) or
        ( NEW.SN_NUMERO <> OLD.SN_NUMERO ) or
        ( NEW.AD_FOLIO <> OLD.AD_FOLIO ) ) then
     begin
          update AUDIT_CIA set AUDIT_CIA.SI_FOLIO = NEW.SI_FOLIO,
                               AUDIT_CIA.SN_NUMERO = NEW.SN_NUMERO,
                               AUDIT_CIA.AD_FOLIO = NEW.AD_FOLIO
          where ( AUDIT_CIA.SI_FOLIO = OLD.SI_FOLIO ) and
                ( AUDIT_CIA.SN_NUMERO = OLD.SN_NUMERO ) and
                ( AUDIT_CIA.AD_FOLIO = OLD.AD_FOLIO );
     end
end !!

drop procedure CHECK_LIMIT_EMP;

create procedure CHECK_LIMIT_EMP( Fecha DATE, Version VARCHAR( 255 ) )
returns( SI_FOLIO Integer, SN_NUMERO Integer, AD_FOLIO Integer, AD_SUM_EMP Integer )
as
  declare variable Empleados Integer;
  declare variable Recoleccion Date;
begin
     for
        select SI_FOLIO, SN_NUMERO, AD_FOLIO, AD_NUM_EMP, AD_FECHA
        from AUDIT
        where ( AD_FECHA >= :Fecha ) and ( AD_VERSION = :Version )
        order by SI_FOLIO, SN_NUMERO, AD_FOLIO
        into :SI_FOLIO, :SN_NUMERO, :AD_FOLIO, :Empleados, :Recoleccion
     do
     begin
          select SUM( AC_ACTIVOS )
          from AUDIT_CIA
          where ( AC_NIVEL0 = '' ) and
                ( ( :Recoleccion - AC_ULT_ALT ) <= 31 ) and
                ( SI_FOLIO = :SI_FOLIO ) and
                ( SN_NUMERO = :SN_NUMERO ) and
                ( AD_FOLIO = :AD_FOLIO )
          into :AD_SUM_EMP;
          if ( AD_SUM_EMP > Empleados ) then
          begin
               suspend;
          end
     end
end !!

/*
select T.SI_FOLIO, T.SN_NUMERO, T.AD_FOLIO,
S.SI_RAZ_SOC
from  CHECK_LIMIT_EMP( '01/01/2005' ) T
left outer join SISTEMA S on ( S.SI_FOLIO = T.SI_FOLIO )

select T.SI_FOLIO, T.SN_NUMERO, T.AD_FOLIO, 
S.SI_RAZ_SOC, A.AD_ARCHIVO
from  CHECK_LIMIT_EMP( '01/01/2005', '2.6' ) T
left outer join SISTEMA S on ( S.SI_FOLIO = T.SI_FOLIO )
left outer join AUDIT A on ( A.SI_FOLIO = T.SI_FOLIO ) and ( A.SN_NUMERO = T.SN_NUMERO ) and ( A.AD_FOLIO = T.AD_FOLIO )
*/

set term ; ^
commit work ;
set autoddl on;
set term ^ ;

commit work ^
set term ; ^

