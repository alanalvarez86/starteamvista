unit FEstados;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs,
     Db, Grids, DBGrids, DBCtrls, ExtCtrls,
     FCatalogo, StdCtrls, Buttons;

type
  TFormEstados = class(TFormCatalogo)
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormEstados: TFormEstados;

implementation

uses DSentinel;

{$R *.DFM}

procedure TFormEstados.FormShow(Sender: TObject);
begin
     inherited;
     dmSentinel.AbrirEstado( dsCatalogo );
end;

procedure TFormEstados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmSentinel.ResetDatasource( dsCatalogo );
end;

end.
