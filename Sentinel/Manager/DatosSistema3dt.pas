
{*****************************************************}
{                                                     }
{                  XML Data Binding                   }
{                                                     }
{         Generated on: 3/26/2012 10:55:10 AM         }
{       Generated from: C:\TEMP\DatosSistema3dt.xml   }
{   Settings stored in: C:\TEMP\DatosSistema3dt.xdb   }
{                                                     }
{*****************************************************}

unit DatosSistema3dt;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTressDTType = interface;
  IXMLServidorType = interface;
  IXMLProcesadoresType = interface;
  IXMLMemoriaType = interface;
  IXMLHDDType = interface;
  IXMLDiscoType = interface;
  IXMLODBCDriversType = interface;
  IXMLSentinelType = interface;
  IXMLModuloType = interface;
  IXMLUserDataType = interface;
  IXMLCompanyType = interface;
  IXMLCompanyTypeList = interface;
  IXMLNominaType = interface;
  IXMLISPTType = interface;
  IXMLConceptoType = interface;
  IXMLINFONAVITProvType = interface;
  IXMLAsistenciaType = interface;
  IXMLTotalesType = interface;

{ IXMLTressDTType }

  IXMLTressDTType = interface(IXMLNode)
    ['{899BBBC3-F438-4530-8DAE-7425A4F616B5}']
    { Property Accessors }
    function Get_Fecha: WideString;
    function Get_Servidor: IXMLServidorType;
    function Get_Sentinel: IXMLSentinelType;
    function Get_UserData: IXMLUserDataType;
    function Get_Company: IXMLCompanyTypeList;
    function Get_Totales: IXMLTotalesType;
    procedure Set_Fecha(Value: WideString);
    { Methods & Properties }
    property Fecha: WideString read Get_Fecha write Set_Fecha;
    property Servidor: IXMLServidorType read Get_Servidor;
    property Sentinel: IXMLSentinelType read Get_Sentinel;
    property UserData: IXMLUserDataType read Get_UserData;
    property Company: IXMLCompanyTypeList read Get_Company;
    property Totales: IXMLTotalesType read Get_Totales;
  end;

{ IXMLServidorType }

  IXMLServidorType = interface(IXMLNode)
    ['{A34D8334-1586-46A0-A1D6-DD13233BBE90}']
    { Property Accessors }
    function Get_Tipo: Integer;
    function Get_VersionOS: WideString;
    function Get_Nombre: WideString;
    function Get_Usuario: WideString;
    function Get_EsAdministrador: WideString;
    function Get_Procesadores: IXMLProcesadoresType;
    function Get_Memoria: IXMLMemoriaType;
    function Get_HDD: IXMLHDDType;
    function Get_ODBCDrivers: IXMLODBCDriversType;
    procedure Set_Tipo(Value: Integer);
    procedure Set_VersionOS(Value: WideString);
    procedure Set_Nombre(Value: WideString);
    procedure Set_Usuario(Value: WideString);
    procedure Set_EsAdministrador(Value: WideString);
    { Methods & Properties }
    property Tipo: Integer read Get_Tipo write Set_Tipo;
    property VersionOS: WideString read Get_VersionOS write Set_VersionOS;
    property Nombre: WideString read Get_Nombre write Set_Nombre;
    property Usuario: WideString read Get_Usuario write Set_Usuario;
    property EsAdministrador: WideString read Get_EsAdministrador write Set_EsAdministrador;
    property Procesadores: IXMLProcesadoresType read Get_Procesadores;
    property Memoria: IXMLMemoriaType read Get_Memoria;
    property HDD: IXMLHDDType read Get_HDD;
    property ODBCDrivers: IXMLODBCDriversType read Get_ODBCDrivers;
  end;

{ IXMLProcesadoresType }

  IXMLProcesadoresType = interface(IXMLNode)
    ['{A6A759AE-6828-4321-BA0F-1E65412D65A8}']
    { Property Accessors }
    function Get_Cantidad: Integer;
    procedure Set_Cantidad(Value: Integer);
    { Methods & Properties }
    property Cantidad: Integer read Get_Cantidad write Set_Cantidad;
  end;

{ IXMLMemoriaType }

  IXMLMemoriaType = interface(IXMLNode)
    ['{5AB38971-9261-40D7-88E7-096D1B89D592}']
    { Property Accessors }
    function Get_RecursosLibres: WideString;
    function Get_MemoriaRAM: WideString;
    function Get_LibreRAM: WideString;
    function Get_Virtual: WideString;
    function Get_LibreVirtual: WideString;
    procedure Set_RecursosLibres(Value: WideString);
    procedure Set_MemoriaRAM(Value: WideString);
    procedure Set_LibreRAM(Value: WideString);
    procedure Set_Virtual(Value: WideString);
    procedure Set_LibreVirtual(Value: WideString);
    { Methods & Properties }
    property RecursosLibres: WideString read Get_RecursosLibres write Set_RecursosLibres;
    property MemoriaRAM: WideString read Get_MemoriaRAM write Set_MemoriaRAM;
    property LibreRAM: WideString read Get_LibreRAM write Set_LibreRAM;
    property Virtual: WideString read Get_Virtual write Set_Virtual;
    property LibreVirtual: WideString read Get_LibreVirtual write Set_LibreVirtual;
  end;

{ IXMLHDDType }

  IXMLHDDType = interface(IXMLNodeCollection)
    ['{845C5CD1-1F22-40AE-B7DF-AB8D7E56C989}']
    { Property Accessors }
    function Get_Disco(Index: Integer): IXMLDiscoType;
    { Methods & Properties }
    function Add: IXMLDiscoType;
    function Insert(const Index: Integer): IXMLDiscoType;
    property Disco[Index: Integer]: IXMLDiscoType read Get_Disco; default;
  end;

{ IXMLDiscoType }

  IXMLDiscoType = interface(IXMLNode)
    ['{43590435-D03E-4A5C-82DA-AD8624CB22DC}']
    { Property Accessors }
    function Get_Drive: WideString;
    function Get_Capacidad: WideString;
    function Get_Libre: WideString;
    function Get_Ocupacion: WideString;
    procedure Set_Drive(Value: WideString);
    procedure Set_Capacidad(Value: WideString);
    procedure Set_Libre(Value: WideString);
    procedure Set_Ocupacion(Value: WideString);
    { Methods & Properties }
    property Drive: WideString read Get_Drive write Set_Drive;
    property Capacidad: WideString read Get_Capacidad write Set_Capacidad;
    property Libre: WideString read Get_Libre write Set_Libre;
    property Ocupacion: WideString read Get_Ocupacion write Set_Ocupacion;
  end;

{ IXMLODBCDriversType }

  IXMLODBCDriversType = interface(IXMLNode)
    ['{D4C2F632-079A-4684-A5AA-0193357F1BF8}']
    { Property Accessors }
    function Get_ODBC: WideString;
    function Get_BDE: WideString;
    function Get_Interbase: WideString;
    procedure Set_ODBC(Value: WideString);
    procedure Set_BDE(Value: WideString);
    procedure Set_Interbase(Value: WideString);
    { Methods & Properties }
    property ODBC: WideString read Get_ODBC write Set_ODBC;
    property BDE: WideString read Get_BDE write Set_BDE;
    property Interbase: WideString read Get_Interbase write Set_Interbase;
  end;

{ IXMLSentinelType }

  IXMLSentinelType = interface(IXMLNodeCollection)
    ['{D4C209BE-7C24-4F00-83D6-6F339927F213}']
    { Property Accessors }
    function Get_Empresa: Integer;
    function Get_Empleados: Integer;
    function Get_Licencias: Integer;
    function Get_Numero: Integer;
    function Get_LimiteConfigBD: Integer;
    function Get_IniciaValidacion: Integer;
    function Get_Version: WideString;
    function Get_EsKitDistribuidor: WideString;
    function Get_Plataforma: WideString;
    function Get_BaseDeDatos: WideString;
    function Get_Vencimiento: WideString;
    function Get_CaducidadPrestamos: WideString;
    function Get_Modulo(Index: Integer): IXMLModuloType;
    procedure Set_Empresa(Value: Integer);
    procedure Set_Empleados(Value: Integer);
    procedure Set_Licencias(Value: Integer);
    procedure Set_Numero(Value: Integer);
    procedure Set_LimiteConfigBD(Value: Integer);
    procedure Set_IniciaValidacion(Value: Integer);
    procedure Set_Version(Value: WideString);
    procedure Set_EsKitDistribuidor(Value: WideString);
    procedure Set_Plataforma(Value: WideString);
    procedure Set_BaseDeDatos(Value: WideString);
    procedure Set_Vencimiento(Value: WideString);
    procedure Set_CaducidadPrestamos(Value: WideString);
    { Methods & Properties }
    function Add: IXMLModuloType;
    function Insert(const Index: Integer): IXMLModuloType;
    property Empresa: Integer read Get_Empresa write Set_Empresa;
    property Empleados: Integer read Get_Empleados write Set_Empleados;
    property Licencias: Integer read Get_Licencias write Set_Licencias;
    property Numero: Integer read Get_Numero write Set_Numero;
    property LimiteConfigBD: Integer read Get_LimiteConfigBD write Set_LimiteConfigBD;
    property IniciaValidacion: Integer read Get_IniciaValidacion write Set_IniciaValidacion;
    property Version: WideString read Get_Version write Set_Version;
    property EsKitDistribuidor: WideString read Get_EsKitDistribuidor write Set_EsKitDistribuidor;
    property Plataforma: WideString read Get_Plataforma write Set_Plataforma;
    property BaseDeDatos: WideString read Get_BaseDeDatos write Set_BaseDeDatos;
    property Vencimiento: WideString read Get_Vencimiento write Set_Vencimiento;
    property CaducidadPrestamos: WideString read Get_CaducidadPrestamos write Set_CaducidadPrestamos;
    property Modulo[Index: Integer]: IXMLModuloType read Get_Modulo; default;
  end;

{ IXMLModuloType }

  IXMLModuloType = interface(IXMLNode)
    ['{D5780E07-0116-4C80-93CD-0A9A64A13E5F}']
    { Property Accessors }
    function Get_Numero: Integer;
    function Get_Nombre: WideString;
    function Get_Autorizado: WideString;
    function Get_Prestado: WideString;
    procedure Set_Numero(Value: Integer);
    procedure Set_Nombre(Value: WideString);
    procedure Set_Autorizado(Value: WideString);
    procedure Set_Prestado(Value: WideString);
    { Methods & Properties }
    property Numero: Integer read Get_Numero write Set_Numero;
    property Nombre: WideString read Get_Nombre write Set_Nombre;
    property Autorizado: WideString read Get_Autorizado write Set_Autorizado;
    property Prestado: WideString read Get_Prestado write Set_Prestado;
  end;

{ IXMLUserDataType }

  IXMLUserDataType = interface(IXMLNode)
    ['{3265319B-9486-471B-B75F-3C7D6ABF5B40}']
    { Property Accessors }
    function Get_UsuariosTotales: Integer;
    procedure Set_UsuariosTotales(Value: Integer);
    { Methods & Properties }
    property UsuariosTotales: Integer read Get_UsuariosTotales write Set_UsuariosTotales;
  end;

{ IXMLCompanyType }

  IXMLCompanyType = interface(IXMLNode)
    ['{A8AF684B-EA4B-480C-8FF7-79F9EB3CE968}']
    { Property Accessors }
    function Get_CM_CODIGO: WideString;
    function Get_CM_NOMBRE: WideString;
    function Get_CM_ACTIVOS: Integer;
    function Get_CM_INACTIV: Integer;
    function Get_CM_TOTAL: Integer;
    function Get_CM_NIVEL0: WideString;
    function Get_CM_DATOS: WideString;
    function Get_DB_SIZE: WideString;
    function Get_CM_ULT_ALT: WideString;
    function Get_CM_ULT_BAJ: WideString;
    function Get_CM_ULT_TAR: WideString;
    function Get_CM_ULT_NOM: WideString;
    function Get_PE_YEAR: Integer;
    function Get_PE_TIPO: Integer;
    function Get_PE_NUMERO: Integer;
    function Get_PE_FEC_INI: WideString;
    function Get_CM_CONNECT: WideString;
    function Get_PC_SI_QTY: Integer;
    function Get_PC_SI_FEC: WideString;
    function Get_PC_AN_QTY: Integer;
    function Get_PC_AN_FEC: WideString;
    function Get_PC_NOMLAVG: WideString;
    function Get_PC_NOM_AVG: WideString;
    function Get_PC_FOA_QTY: Integer;
    function Get_PC_FOA_FEC: WideString;
    function Get_PC_FON_QTY: Integer;
    function Get_PC_FON_FEC: WideString;
    function Get_PC_FOC_QTY: Integer;
    function Get_PC_FOC_FEC: WideString;
    function Get_PC_LB_QTY: Integer;
    function Get_PC_LB_FEC: WideString;
    function Get_PC_EM_QTY: Integer;
    function Get_PC_EM_FEC: WideString;
    function Get_RE_CU_QTY: Integer;
    function Get_RE_CF_QTY: Integer;
    function Get_RE_CF_FEC: WideString;
    function Get_RE_RG_QTY: Integer;
    function Get_RE_RG_FEC: WideString;
    function Get_RE_SM_QTY: Integer;
    function Get_RE_SM_FEC: WideString;
    function Get_RE_PC_QTY: Integer;
    function Get_RE_PC_FEC: WideString;
    function Get_RE_EV_QTY: Integer;
    function Get_RE_EV_FEC: WideString;
    function Get_Nomina: IXMLNominaType;
    function Get_Asistencia: IXMLAsistenciaType;
    procedure Set_CM_CODIGO(Value: WideString);
    procedure Set_CM_NOMBRE(Value: WideString);
    procedure Set_CM_ACTIVOS(Value: Integer);
    procedure Set_CM_INACTIV(Value: Integer);
    procedure Set_CM_TOTAL(Value: Integer);
    procedure Set_CM_NIVEL0(Value: WideString);
    procedure Set_CM_DATOS(Value: WideString);
    procedure Set_DB_SIZE(Value: WideString);
    procedure Set_CM_ULT_ALT(Value: WideString);
    procedure Set_CM_ULT_BAJ(Value: WideString);
    procedure Set_CM_ULT_TAR(Value: WideString);
    procedure Set_CM_ULT_NOM(Value: WideString);
    procedure Set_PE_YEAR(Value: Integer);
    procedure Set_PE_TIPO(Value: Integer);
    procedure Set_PE_NUMERO(Value: Integer);
    procedure Set_PE_FEC_INI(Value: WideString);
    procedure Set_CM_CONNECT(Value: WideString);
    procedure Set_PC_SI_QTY(Value: Integer);
    procedure Set_PC_SI_FEC(Value: WideString);
    procedure Set_PC_AN_QTY(Value: Integer);
    procedure Set_PC_AN_FEC(Value: WideString);
    procedure Set_PC_NOMLAVG(Value: WideString);
    procedure Set_PC_NOM_AVG(Value: WideString);
    procedure Set_PC_FOA_QTY(Value: Integer);
    procedure Set_PC_FOA_FEC(Value: WideString);
    procedure Set_PC_FON_QTY(Value: Integer);
    procedure Set_PC_FON_FEC(Value: WideString);
    procedure Set_PC_FOC_QTY(Value: Integer);
    procedure Set_PC_FOC_FEC(Value: WideString);
    procedure Set_PC_LB_QTY(Value: Integer);
    procedure Set_PC_LB_FEC(Value: WideString);
    procedure Set_PC_EM_QTY(Value: Integer);
    procedure Set_PC_EM_FEC(Value: WideString);
    procedure Set_RE_CU_QTY(Value: Integer);
    procedure Set_RE_CF_QTY(Value: Integer);
    procedure Set_RE_CF_FEC(Value: WideString);
    procedure Set_RE_RG_QTY(Value: Integer);
    procedure Set_RE_RG_FEC(Value: WideString);
    procedure Set_RE_SM_QTY(Value: Integer);
    procedure Set_RE_SM_FEC(Value: WideString);
    procedure Set_RE_PC_QTY(Value: Integer);
    procedure Set_RE_PC_FEC(Value: WideString);
    procedure Set_RE_EV_QTY(Value: Integer);
    procedure Set_RE_EV_FEC(Value: WideString);
    { Methods & Properties }
    property CM_CODIGO: WideString read Get_CM_CODIGO write Set_CM_CODIGO;
    property CM_NOMBRE: WideString read Get_CM_NOMBRE write Set_CM_NOMBRE;
    property CM_ACTIVOS: Integer read Get_CM_ACTIVOS write Set_CM_ACTIVOS;
    property CM_INACTIV: Integer read Get_CM_INACTIV write Set_CM_INACTIV;
    property CM_TOTAL: Integer read Get_CM_TOTAL write Set_CM_TOTAL;
    property CM_NIVEL0: WideString read Get_CM_NIVEL0 write Set_CM_NIVEL0;
    property CM_DATOS: WideString read Get_CM_DATOS write Set_CM_DATOS;
    property DB_SIZE: WideString read Get_DB_SIZE write Set_DB_SIZE;
    property CM_ULT_ALT: WideString read Get_CM_ULT_ALT write Set_CM_ULT_ALT;
    property CM_ULT_BAJ: WideString read Get_CM_ULT_BAJ write Set_CM_ULT_BAJ;
    property CM_ULT_TAR: WideString read Get_CM_ULT_TAR write Set_CM_ULT_TAR;
    property CM_ULT_NOM: WideString read Get_CM_ULT_NOM write Set_CM_ULT_NOM;
    property PE_YEAR: Integer read Get_PE_YEAR write Set_PE_YEAR;
    property PE_TIPO: Integer read Get_PE_TIPO write Set_PE_TIPO;
    property PE_NUMERO: Integer read Get_PE_NUMERO write Set_PE_NUMERO;
    property PE_FEC_INI: WideString read Get_PE_FEC_INI write Set_PE_FEC_INI;
    property CM_CONNECT: WideString read Get_CM_CONNECT write Set_CM_CONNECT;
    property PC_SI_QTY: Integer read Get_PC_SI_QTY write Set_PC_SI_QTY;
    property PC_SI_FEC: WideString read Get_PC_SI_FEC write Set_PC_SI_FEC;
    property PC_AN_QTY: Integer read Get_PC_AN_QTY write Set_PC_AN_QTY;
    property PC_AN_FEC: WideString read Get_PC_AN_FEC write Set_PC_AN_FEC;
    property PC_NOMLAVG: WideString read Get_PC_NOMLAVG write Set_PC_NOMLAVG;
    property PC_NOM_AVG: WideString read Get_PC_NOM_AVG write Set_PC_NOM_AVG;
    property PC_FOA_QTY: Integer read Get_PC_FOA_QTY write Set_PC_FOA_QTY;
    property PC_FOA_FEC: WideString read Get_PC_FOA_FEC write Set_PC_FOA_FEC;
    property PC_FON_QTY: Integer read Get_PC_FON_QTY write Set_PC_FON_QTY;
    property PC_FON_FEC: WideString read Get_PC_FON_FEC write Set_PC_FON_FEC;
    property PC_FOC_QTY: Integer read Get_PC_FOC_QTY write Set_PC_FOC_QTY;
    property PC_FOC_FEC: WideString read Get_PC_FOC_FEC write Set_PC_FOC_FEC;
    property PC_LB_QTY: Integer read Get_PC_LB_QTY write Set_PC_LB_QTY;
    property PC_LB_FEC: WideString read Get_PC_LB_FEC write Set_PC_LB_FEC;
    property PC_EM_QTY: Integer read Get_PC_EM_QTY write Set_PC_EM_QTY;
    property PC_EM_FEC: WideString read Get_PC_EM_FEC write Set_PC_EM_FEC;
    property RE_CU_QTY: Integer read Get_RE_CU_QTY write Set_RE_CU_QTY;
    property RE_CF_QTY: Integer read Get_RE_CF_QTY write Set_RE_CF_QTY;
    property RE_CF_FEC: WideString read Get_RE_CF_FEC write Set_RE_CF_FEC;
    property RE_RG_QTY: Integer read Get_RE_RG_QTY write Set_RE_RG_QTY;
    property RE_RG_FEC: WideString read Get_RE_RG_FEC write Set_RE_RG_FEC;
    property RE_SM_QTY: Integer read Get_RE_SM_QTY write Set_RE_SM_QTY;
    property RE_SM_FEC: WideString read Get_RE_SM_FEC write Set_RE_SM_FEC;
    property RE_PC_QTY: Integer read Get_RE_PC_QTY write Set_RE_PC_QTY;
    property RE_PC_FEC: WideString read Get_RE_PC_FEC write Set_RE_PC_FEC;
    property RE_EV_QTY: Integer read Get_RE_EV_QTY write Set_RE_EV_QTY;
    property RE_EV_FEC: WideString read Get_RE_EV_FEC write Set_RE_EV_FEC;
    property Nomina: IXMLNominaType read Get_Nomina;
    property Asistencia: IXMLAsistenciaType read Get_Asistencia;
  end;

{ IXMLCompanyTypeList }

  IXMLCompanyTypeList = interface(IXMLNodeCollection)
    ['{020A04F6-82A7-4FF4-816E-7474B0087205}']
    { Methods & Properties }
    function Add: IXMLCompanyType;
    function Insert(const Index: Integer): IXMLCompanyType;
    function Get_Item(Index: Integer): IXMLCompanyType;
    property Items[Index: Integer]: IXMLCompanyType read Get_Item; default;
  end;

{ IXMLNominaType }

  IXMLNominaType = interface(IXMLNode)
    ['{EB4E7080-F874-4A12-A61F-ABE925C71CEC}']
    { Property Accessors }
    function Get_ISPT: IXMLISPTType;
    function Get_INFONAVITAporta: WideString;
    function Get_INFONAVITProv: IXMLINFONAVITProvType;
    function Get_Periodos: WideString;
    procedure Set_INFONAVITAporta(Value: WideString);
    procedure Set_Periodos(Value: WideString);
    { Methods & Properties }
    property ISPT: IXMLISPTType read Get_ISPT;
    property INFONAVITAporta: WideString read Get_INFONAVITAporta write Set_INFONAVITAporta;
    property INFONAVITProv: IXMLINFONAVITProvType read Get_INFONAVITProv;
    property Periodos: WideString read Get_Periodos write Set_Periodos;
  end;

{ IXMLISPTType }

  IXMLISPTType = interface(IXMLNode)
    ['{AFE5B9D3-019C-45C4-8A60-8F5287316CFF}']
    { Property Accessors }
    function Get_Concepto: IXMLConceptoType;
    { Methods & Properties }
    property Concepto: IXMLConceptoType read Get_Concepto;
  end;

{ IXMLConceptoType }

  IXMLConceptoType = interface(IXMLNode)
    ['{80FE9477-9DF2-427B-8CD5-A285120A3EB6}']
    { Property Accessors }
    function Get_No: Integer;
    procedure Set_No(Value: Integer);
    { Methods & Properties }
    property No: Integer read Get_No write Set_No;
  end;

{ IXMLINFONAVITProvType }

  IXMLINFONAVITProvType = interface(IXMLNode)
    ['{10403BA1-2B3F-4BD8-B4B5-F60E45C1C4CC}']
    { Property Accessors }
    function Get_Concepto: IXMLConceptoType;
    { Methods & Properties }
    property Concepto: IXMLConceptoType read Get_Concepto;
  end;

{ IXMLAsistenciaType }

  IXMLAsistenciaType = interface(IXMLNode)
    ['{55577F1B-0239-4A70-9B82-C74D446F9DC2}']
    { Property Accessors }
    function Get_Checadas: WideString;
    procedure Set_Checadas(Value: WideString);
    { Methods & Properties }
    property Checadas: WideString read Get_Checadas write Set_Checadas;
  end;

{ IXMLTotalesType }

  IXMLTotalesType = interface(IXMLNode)
    ['{B39CAFBF-6B36-4432-9EE3-C3AD38129261}']
    { Property Accessors }
    function Get_TotalActivos: Integer;
    function Get_ActivosProduccion: Integer;
    function Get_ActivosPruebas: Integer;
    function Get_TotalBajas: Integer;
    function Get_BajasProduccion: Integer;
    function Get_BajasPruebas: Integer;
    function Get_UltimaAltaBD: WideString;
    function Get_CantAltasBD: Integer;
    function Get_CantAsignaComparte: Integer;
    procedure Set_TotalActivos(Value: Integer);
    procedure Set_ActivosProduccion(Value: Integer);
    procedure Set_ActivosPruebas(Value: Integer);
    procedure Set_TotalBajas(Value: Integer);
    procedure Set_BajasProduccion(Value: Integer);
    procedure Set_BajasPruebas(Value: Integer);
    procedure Set_UltimaAltaBD(Value: WideString);
    procedure Set_CantAltasBD(Value: Integer);
    procedure Set_CantAsignaComparte(Value: Integer);
    { Methods & Properties }
    property TotalActivos: Integer read Get_TotalActivos write Set_TotalActivos;
    property ActivosProduccion: Integer read Get_ActivosProduccion write Set_ActivosProduccion;
    property ActivosPruebas: Integer read Get_ActivosPruebas write Set_ActivosPruebas;
    property TotalBajas: Integer read Get_TotalBajas write Set_TotalBajas;
    property BajasProduccion: Integer read Get_BajasProduccion write Set_BajasProduccion;
    property BajasPruebas: Integer read Get_BajasPruebas write Set_BajasPruebas;
    property UltimaAltaBD: WideString read Get_UltimaAltaBD write Set_UltimaAltaBD;
    property CantAltasBD: Integer read Get_CantAltasBD write Set_CantAltasBD;
    property CantAsignaComparte: Integer read Get_CantAsignaComparte write Set_CantAsignaComparte;
  end;

{ Forward Decls }

  TXMLTressDTType = class;
  TXMLServidorType = class;
  TXMLProcesadoresType = class;
  TXMLMemoriaType = class;
  TXMLHDDType = class;
  TXMLDiscoType = class;
  TXMLODBCDriversType = class;
  TXMLSentinelType = class;
  TXMLModuloType = class;
  TXMLUserDataType = class;
  TXMLCompanyType = class;
  TXMLCompanyTypeList = class;
  TXMLNominaType = class;
  TXMLISPTType = class;
  TXMLConceptoType = class;
  TXMLINFONAVITProvType = class;
  TXMLAsistenciaType = class;
  TXMLTotalesType = class;

{ TXMLTressDTType }

  TXMLTressDTType = class(TXMLNode, IXMLTressDTType)
  private
    FCompany: IXMLCompanyTypeList;
  protected
    { IXMLTressDTType }
    function Get_Fecha: WideString;
    function Get_Servidor: IXMLServidorType;
    function Get_Sentinel: IXMLSentinelType;
    function Get_UserData: IXMLUserDataType;
    function Get_Company: IXMLCompanyTypeList;
    function Get_Totales: IXMLTotalesType;
    procedure Set_Fecha(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLServidorType }

  TXMLServidorType = class(TXMLNode, IXMLServidorType)
  protected
    { IXMLServidorType }
    function Get_Tipo: Integer;
    function Get_VersionOS: WideString;
    function Get_Nombre: WideString;
    function Get_Usuario: WideString;
    function Get_EsAdministrador: WideString;
    function Get_Procesadores: IXMLProcesadoresType;
    function Get_Memoria: IXMLMemoriaType;
    function Get_HDD: IXMLHDDType;
    function Get_ODBCDrivers: IXMLODBCDriversType;
    procedure Set_Tipo(Value: Integer);
    procedure Set_VersionOS(Value: WideString);
    procedure Set_Nombre(Value: WideString);
    procedure Set_Usuario(Value: WideString);
    procedure Set_EsAdministrador(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProcesadoresType }

  TXMLProcesadoresType = class(TXMLNode, IXMLProcesadoresType)
  protected
    { IXMLProcesadoresType }
    function Get_Cantidad: Integer;
    procedure Set_Cantidad(Value: Integer);
  end;

{ TXMLMemoriaType }

  TXMLMemoriaType = class(TXMLNode, IXMLMemoriaType)
  protected
    { IXMLMemoriaType }
    function Get_RecursosLibres: WideString;
    function Get_MemoriaRAM: WideString;
    function Get_LibreRAM: WideString;
    function Get_Virtual: WideString;
    function Get_LibreVirtual: WideString;
    procedure Set_RecursosLibres(Value: WideString);
    procedure Set_MemoriaRAM(Value: WideString);
    procedure Set_LibreRAM(Value: WideString);
    procedure Set_Virtual(Value: WideString);
    procedure Set_LibreVirtual(Value: WideString);
  end;

{ TXMLHDDType }

  TXMLHDDType = class(TXMLNodeCollection, IXMLHDDType)
  protected
    { IXMLHDDType }
    function Get_Disco(Index: Integer): IXMLDiscoType;
    function Add: IXMLDiscoType;
    function Insert(const Index: Integer): IXMLDiscoType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDiscoType }

  TXMLDiscoType = class(TXMLNode, IXMLDiscoType)
  protected
    { IXMLDiscoType }
    function Get_Drive: WideString;
    function Get_Capacidad: WideString;
    function Get_Libre: WideString;
    function Get_Ocupacion: WideString;
    procedure Set_Drive(Value: WideString);
    procedure Set_Capacidad(Value: WideString);
    procedure Set_Libre(Value: WideString);
    procedure Set_Ocupacion(Value: WideString);
  end;

{ TXMLODBCDriversType }

  TXMLODBCDriversType = class(TXMLNode, IXMLODBCDriversType)
  protected
    { IXMLODBCDriversType }
    function Get_ODBC: WideString;
    function Get_BDE: WideString;
    function Get_Interbase: WideString;
    procedure Set_ODBC(Value: WideString);
    procedure Set_BDE(Value: WideString);
    procedure Set_Interbase(Value: WideString);
  end;

{ TXMLSentinelType }

  TXMLSentinelType = class(TXMLNodeCollection, IXMLSentinelType)
  protected
    { IXMLSentinelType }
    function Get_Empresa: Integer;
    function Get_Empleados: Integer;
    function Get_Licencias: Integer;
    function Get_Numero: Integer;
    function Get_LimiteConfigBD: Integer;
    function Get_IniciaValidacion: Integer;
    function Get_Version: WideString;
    function Get_EsKitDistribuidor: WideString;
    function Get_Plataforma: WideString;
    function Get_BaseDeDatos: WideString;
    function Get_Vencimiento: WideString;
    function Get_CaducidadPrestamos: WideString;
    function Get_Modulo(Index: Integer): IXMLModuloType;
    procedure Set_Empresa(Value: Integer);
    procedure Set_Empleados(Value: Integer);
    procedure Set_Licencias(Value: Integer);
    procedure Set_Numero(Value: Integer);
    procedure Set_LimiteConfigBD(Value: Integer);
    procedure Set_IniciaValidacion(Value: Integer);
    procedure Set_Version(Value: WideString);
    procedure Set_EsKitDistribuidor(Value: WideString);
    procedure Set_Plataforma(Value: WideString);
    procedure Set_BaseDeDatos(Value: WideString);
    procedure Set_Vencimiento(Value: WideString);
    procedure Set_CaducidadPrestamos(Value: WideString);
    function Add: IXMLModuloType;
    function Insert(const Index: Integer): IXMLModuloType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLModuloType }

  TXMLModuloType = class(TXMLNode, IXMLModuloType)
  protected
    { IXMLModuloType }
    function Get_Numero: Integer;
    function Get_Nombre: WideString;
    function Get_Autorizado: WideString;
    function Get_Prestado: WideString;
    procedure Set_Numero(Value: Integer);
    procedure Set_Nombre(Value: WideString);
    procedure Set_Autorizado(Value: WideString);
    procedure Set_Prestado(Value: WideString);
  end;

{ TXMLUserDataType }

  TXMLUserDataType = class(TXMLNode, IXMLUserDataType)
  protected
    { IXMLUserDataType }
    function Get_UsuariosTotales: Integer;
    procedure Set_UsuariosTotales(Value: Integer);
  end;

{ TXMLCompanyType }

  TXMLCompanyType = class(TXMLNode, IXMLCompanyType)
  protected
    { IXMLCompanyType }
    function Get_CM_CODIGO: WideString;
    function Get_CM_NOMBRE: WideString;
    function Get_CM_ACTIVOS: Integer;
    function Get_CM_INACTIV: Integer;
    function Get_CM_TOTAL: Integer;
    function Get_CM_NIVEL0: WideString;
    function Get_CM_DATOS: WideString;
    function Get_DB_SIZE: WideString;
    function Get_CM_ULT_ALT: WideString;
    function Get_CM_ULT_BAJ: WideString;
    function Get_CM_ULT_TAR: WideString;
    function Get_CM_ULT_NOM: WideString;
    function Get_PE_YEAR: Integer;
    function Get_PE_TIPO: Integer;
    function Get_PE_NUMERO: Integer;
    function Get_PE_FEC_INI: WideString;
    function Get_CM_CONNECT: WideString;
    function Get_PC_SI_QTY: Integer;
    function Get_PC_SI_FEC: WideString;
    function Get_PC_AN_QTY: Integer;
    function Get_PC_AN_FEC: WideString;
    function Get_PC_NOMLAVG: WideString;
    function Get_PC_NOM_AVG: WideString;
    function Get_PC_FOA_QTY: Integer;
    function Get_PC_FOA_FEC: WideString;
    function Get_PC_FON_QTY: Integer;
    function Get_PC_FON_FEC: WideString;
    function Get_PC_FOC_QTY: Integer;
    function Get_PC_FOC_FEC: WideString;
    function Get_PC_LB_QTY: Integer;
    function Get_PC_LB_FEC: WideString;
    function Get_PC_EM_QTY: Integer;
    function Get_PC_EM_FEC: WideString;
    function Get_RE_CU_QTY: Integer;
    function Get_RE_CF_QTY: Integer;
    function Get_RE_CF_FEC: WideString;
    function Get_RE_RG_QTY: Integer;
    function Get_RE_RG_FEC: WideString;
    function Get_RE_SM_QTY: Integer;
    function Get_RE_SM_FEC: WideString;
    function Get_RE_PC_QTY: Integer;
    function Get_RE_PC_FEC: WideString;
    function Get_RE_EV_QTY: Integer;
    function Get_RE_EV_FEC: WideString;
    function Get_Nomina: IXMLNominaType;
    function Get_Asistencia: IXMLAsistenciaType;
    procedure Set_CM_CODIGO(Value: WideString);
    procedure Set_CM_NOMBRE(Value: WideString);
    procedure Set_CM_ACTIVOS(Value: Integer);
    procedure Set_CM_INACTIV(Value: Integer);
    procedure Set_CM_TOTAL(Value: Integer);
    procedure Set_CM_NIVEL0(Value: WideString);
    procedure Set_CM_DATOS(Value: WideString);
    procedure Set_DB_SIZE(Value: WideString);
    procedure Set_CM_ULT_ALT(Value: WideString);
    procedure Set_CM_ULT_BAJ(Value: WideString);
    procedure Set_CM_ULT_TAR(Value: WideString);
    procedure Set_CM_ULT_NOM(Value: WideString);
    procedure Set_PE_YEAR(Value: Integer);
    procedure Set_PE_TIPO(Value: Integer);
    procedure Set_PE_NUMERO(Value: Integer);
    procedure Set_PE_FEC_INI(Value: WideString);
    procedure Set_CM_CONNECT(Value: WideString);
    procedure Set_PC_SI_QTY(Value: Integer);
    procedure Set_PC_SI_FEC(Value: WideString);
    procedure Set_PC_AN_QTY(Value: Integer);
    procedure Set_PC_AN_FEC(Value: WideString);
    procedure Set_PC_NOMLAVG(Value: WideString);
    procedure Set_PC_NOM_AVG(Value: WideString);
    procedure Set_PC_FOA_QTY(Value: Integer);
    procedure Set_PC_FOA_FEC(Value: WideString);
    procedure Set_PC_FON_QTY(Value: Integer);
    procedure Set_PC_FON_FEC(Value: WideString);
    procedure Set_PC_FOC_QTY(Value: Integer);
    procedure Set_PC_FOC_FEC(Value: WideString);
    procedure Set_PC_LB_QTY(Value: Integer);
    procedure Set_PC_LB_FEC(Value: WideString);
    procedure Set_PC_EM_QTY(Value: Integer);
    procedure Set_PC_EM_FEC(Value: WideString);
    procedure Set_RE_CU_QTY(Value: Integer);
    procedure Set_RE_CF_QTY(Value: Integer);
    procedure Set_RE_CF_FEC(Value: WideString);
    procedure Set_RE_RG_QTY(Value: Integer);
    procedure Set_RE_RG_FEC(Value: WideString);
    procedure Set_RE_SM_QTY(Value: Integer);
    procedure Set_RE_SM_FEC(Value: WideString);
    procedure Set_RE_PC_QTY(Value: Integer);
    procedure Set_RE_PC_FEC(Value: WideString);
    procedure Set_RE_EV_QTY(Value: Integer);
    procedure Set_RE_EV_FEC(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCompanyTypeList }

  TXMLCompanyTypeList = class(TXMLNodeCollection, IXMLCompanyTypeList)
  protected
    { IXMLCompanyTypeList }
    function Add: IXMLCompanyType;
    function Insert(const Index: Integer): IXMLCompanyType;
    function Get_Item(Index: Integer): IXMLCompanyType;
  end;

{ TXMLNominaType }

  TXMLNominaType = class(TXMLNode, IXMLNominaType)
  protected
    { IXMLNominaType }
    function Get_ISPT: IXMLISPTType;
    function Get_INFONAVITAporta: WideString;
    function Get_INFONAVITProv: IXMLINFONAVITProvType;
    function Get_Periodos: WideString;
    procedure Set_INFONAVITAporta(Value: WideString);
    procedure Set_Periodos(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLISPTType }

  TXMLISPTType = class(TXMLNode, IXMLISPTType)
  protected
    { IXMLISPTType }
    function Get_Concepto: IXMLConceptoType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLConceptoType }

  TXMLConceptoType = class(TXMLNode, IXMLConceptoType)
  protected
    { IXMLConceptoType }
    function Get_No: Integer;
    procedure Set_No(Value: Integer);
  end;

{ TXMLINFONAVITProvType }

  TXMLINFONAVITProvType = class(TXMLNode, IXMLINFONAVITProvType)
  protected
    { IXMLINFONAVITProvType }
    function Get_Concepto: IXMLConceptoType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLAsistenciaType }

  TXMLAsistenciaType = class(TXMLNode, IXMLAsistenciaType)
  protected
    { IXMLAsistenciaType }
    function Get_Checadas: WideString;
    procedure Set_Checadas(Value: WideString);
  end;

{ TXMLTotalesType }

  TXMLTotalesType = class(TXMLNode, IXMLTotalesType)
  protected
    { IXMLTotalesType }
    function Get_TotalActivos: Integer;
    function Get_ActivosProduccion: Integer;
    function Get_ActivosPruebas: Integer;
    function Get_TotalBajas: Integer;
    function Get_BajasProduccion: Integer;
    function Get_BajasPruebas: Integer;
    function Get_UltimaAltaBD: WideString;
    function Get_CantAltasBD: Integer;
    function Get_CantAsignaComparte: Integer;
    procedure Set_TotalActivos(Value: Integer);
    procedure Set_ActivosProduccion(Value: Integer);
    procedure Set_ActivosPruebas(Value: Integer);
    procedure Set_TotalBajas(Value: Integer);
    procedure Set_BajasProduccion(Value: Integer);
    procedure Set_BajasPruebas(Value: Integer);
    procedure Set_UltimaAltaBD(Value: WideString);
    procedure Set_CantAltasBD(Value: Integer);
    procedure Set_CantAsignaComparte(Value: Integer);
  end;

{ Global Functions }

function GetTressDT(Doc: IXMLDocument): IXMLTressDTType;
function LoadTressDT(const FileName: WideString): IXMLTressDTType;
function NewTressDT: IXMLTressDTType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetTressDT(Doc: IXMLDocument): IXMLTressDTType;
begin
  Result := Doc.GetDocBinding('TressDT', TXMLTressDTType, TargetNamespace) as IXMLTressDTType;
end;

function LoadTressDT(const FileName: WideString): IXMLTressDTType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('TressDT', TXMLTressDTType, TargetNamespace) as IXMLTressDTType;
end;

function NewTressDT: IXMLTressDTType;
begin
  Result := NewXMLDocument.GetDocBinding('TressDT', TXMLTressDTType, TargetNamespace) as IXMLTressDTType;
end;

{ TXMLTressDTType }

procedure TXMLTressDTType.AfterConstruction;
begin
  RegisterChildNode('Servidor', TXMLServidorType);
  RegisterChildNode('Sentinel', TXMLSentinelType);
  RegisterChildNode('UserData', TXMLUserDataType);
  RegisterChildNode('Company', TXMLCompanyType);
  RegisterChildNode('Totales', TXMLTotalesType);
  FCompany := CreateCollection(TXMLCompanyTypeList, IXMLCompanyType, 'Company') as IXMLCompanyTypeList;
  inherited;
end;

function TXMLTressDTType.Get_Fecha: WideString;
begin
  Result := AttributeNodes['Fecha'].Text;
end;

procedure TXMLTressDTType.Set_Fecha(Value: WideString);
begin
  SetAttribute('Fecha', Value);
end;

function TXMLTressDTType.Get_Servidor: IXMLServidorType;
begin
  Result := ChildNodes['Servidor'] as IXMLServidorType;
end;

function TXMLTressDTType.Get_Sentinel: IXMLSentinelType;
begin
  Result := ChildNodes['Sentinel'] as IXMLSentinelType;
end;

function TXMLTressDTType.Get_UserData: IXMLUserDataType;
begin
  Result := ChildNodes['UserData'] as IXMLUserDataType;
end;

function TXMLTressDTType.Get_Company: IXMLCompanyTypeList;
begin
  Result := FCompany;
end;

function TXMLTressDTType.Get_Totales: IXMLTotalesType;
begin
  Result := ChildNodes['Totales'] as IXMLTotalesType;
end;

{ TXMLServidorType }

procedure TXMLServidorType.AfterConstruction;
begin
  RegisterChildNode('Procesadores', TXMLProcesadoresType);
  RegisterChildNode('Memoria', TXMLMemoriaType);
  RegisterChildNode('HDD', TXMLHDDType);
  RegisterChildNode('ODBCDrivers', TXMLODBCDriversType);
  inherited;
end;

function TXMLServidorType.Get_Tipo: Integer;
begin
  Result := AttributeNodes['Tipo'].NodeValue;
end;

procedure TXMLServidorType.Set_Tipo(Value: Integer);
begin
  SetAttribute('Tipo', Value);
end;

function TXMLServidorType.Get_VersionOS: WideString;
begin
  Result := AttributeNodes['VersionOS'].Text;
end;

procedure TXMLServidorType.Set_VersionOS(Value: WideString);
begin
  SetAttribute('VersionOS', Value);
end;

function TXMLServidorType.Get_Nombre: WideString;
begin
  Result := AttributeNodes['Nombre'].Text;
end;

procedure TXMLServidorType.Set_Nombre(Value: WideString);
begin
  SetAttribute('Nombre', Value);
end;

function TXMLServidorType.Get_Usuario: WideString;
begin
  Result := AttributeNodes['Usuario'].Text;
end;

procedure TXMLServidorType.Set_Usuario(Value: WideString);
begin
  SetAttribute('Usuario', Value);
end;

function TXMLServidorType.Get_EsAdministrador: WideString;
begin
  Result := AttributeNodes['EsAdministrador'].Text;
end;

procedure TXMLServidorType.Set_EsAdministrador(Value: WideString);
begin
  SetAttribute('EsAdministrador', Value);
end;

function TXMLServidorType.Get_Procesadores: IXMLProcesadoresType;
begin
  Result := ChildNodes['Procesadores'] as IXMLProcesadoresType;
end;

function TXMLServidorType.Get_Memoria: IXMLMemoriaType;
begin
  Result := ChildNodes['Memoria'] as IXMLMemoriaType;
end;

function TXMLServidorType.Get_HDD: IXMLHDDType;
begin
  Result := ChildNodes['HDD'] as IXMLHDDType;
end;

function TXMLServidorType.Get_ODBCDrivers: IXMLODBCDriversType;
begin
  Result := ChildNodes['ODBCDrivers'] as IXMLODBCDriversType;
end;

{ TXMLProcesadoresType }

function TXMLProcesadoresType.Get_Cantidad: Integer;
begin
  Result := AttributeNodes['Cantidad'].NodeValue;
end;

procedure TXMLProcesadoresType.Set_Cantidad(Value: Integer);
begin
  SetAttribute('Cantidad', Value);
end;

{ TXMLMemoriaType }

function TXMLMemoriaType.Get_RecursosLibres: WideString;
begin
  Result := AttributeNodes['RecursosLibres'].Text;
end;

procedure TXMLMemoriaType.Set_RecursosLibres(Value: WideString);
begin
  SetAttribute('RecursosLibres', Value);
end;

function TXMLMemoriaType.Get_MemoriaRAM: WideString;
begin
  Result := AttributeNodes['MemoriaRAM'].Text;
end;

procedure TXMLMemoriaType.Set_MemoriaRAM(Value: WideString);
begin
  SetAttribute('MemoriaRAM', Value);
end;

function TXMLMemoriaType.Get_LibreRAM: WideString;
begin
  Result := AttributeNodes['LibreRAM'].Text;
end;

procedure TXMLMemoriaType.Set_LibreRAM(Value: WideString);
begin
  SetAttribute('LibreRAM', Value);
end;

function TXMLMemoriaType.Get_Virtual: WideString;
begin
  Result := AttributeNodes['Virtual'].Text;
end;

procedure TXMLMemoriaType.Set_Virtual(Value: WideString);
begin
  SetAttribute('Virtual', Value);
end;

function TXMLMemoriaType.Get_LibreVirtual: WideString;
begin
  Result := AttributeNodes['LibreVirtual'].Text;
end;

procedure TXMLMemoriaType.Set_LibreVirtual(Value: WideString);
begin
  SetAttribute('LibreVirtual', Value);
end;

{ TXMLHDDType }

procedure TXMLHDDType.AfterConstruction;
begin
  RegisterChildNode('Disco', TXMLDiscoType);
  ItemTag := 'Disco';
  ItemInterface := IXMLDiscoType;
  inherited;
end;

function TXMLHDDType.Get_Disco(Index: Integer): IXMLDiscoType;
begin
  Result := List[Index] as IXMLDiscoType;
end;

function TXMLHDDType.Add: IXMLDiscoType;
begin
  Result := AddItem(-1) as IXMLDiscoType;
end;

function TXMLHDDType.Insert(const Index: Integer): IXMLDiscoType;
begin
  Result := AddItem(Index) as IXMLDiscoType;
end;

{ TXMLDiscoType }

function TXMLDiscoType.Get_Drive: WideString;
begin
  Result := AttributeNodes['Drive'].Text;
end;

procedure TXMLDiscoType.Set_Drive(Value: WideString);
begin
  SetAttribute('Drive', Value);
end;

function TXMLDiscoType.Get_Capacidad: WideString;
begin
  Result := AttributeNodes['Capacidad'].Text;
end;

procedure TXMLDiscoType.Set_Capacidad(Value: WideString);
begin
  SetAttribute('Capacidad', Value);
end;

function TXMLDiscoType.Get_Libre: WideString;
begin
  Result := AttributeNodes['Libre'].Text;
end;

procedure TXMLDiscoType.Set_Libre(Value: WideString);
begin
  SetAttribute('Libre', Value);
end;

function TXMLDiscoType.Get_Ocupacion: WideString;
begin
  Result := AttributeNodes['Ocupacion'].Text;
end;

procedure TXMLDiscoType.Set_Ocupacion(Value: WideString);
begin
  SetAttribute('Ocupacion', Value);
end;

{ TXMLODBCDriversType }

function TXMLODBCDriversType.Get_ODBC: WideString;
begin
  Result := AttributeNodes['ODBC'].Text;
end;

procedure TXMLODBCDriversType.Set_ODBC(Value: WideString);
begin
  SetAttribute('ODBC', Value);
end;

function TXMLODBCDriversType.Get_BDE: WideString;
begin
  Result := AttributeNodes['BDE'].Text;
end;

procedure TXMLODBCDriversType.Set_BDE(Value: WideString);
begin
  SetAttribute('BDE', Value);
end;

function TXMLODBCDriversType.Get_Interbase: WideString;
begin
  Result := AttributeNodes['Interbase'].Text;
end;

procedure TXMLODBCDriversType.Set_Interbase(Value: WideString);
begin
  SetAttribute('Interbase', Value);
end;

{ TXMLSentinelType }

procedure TXMLSentinelType.AfterConstruction;
begin
  RegisterChildNode('Modulo', TXMLModuloType);
  ItemTag := 'Modulo';
  ItemInterface := IXMLModuloType;
  inherited;
end;

function TXMLSentinelType.Get_Empresa: Integer;
begin
  Result := AttributeNodes['Empresa'].NodeValue;
end;

procedure TXMLSentinelType.Set_Empresa(Value: Integer);
begin
  SetAttribute('Empresa', Value);
end;

function TXMLSentinelType.Get_Empleados: Integer;
begin
  Result := AttributeNodes['Empleados'].NodeValue;
end;

procedure TXMLSentinelType.Set_Empleados(Value: Integer);
begin
  SetAttribute('Empleados', Value);
end;

function TXMLSentinelType.Get_Licencias: Integer;
begin
  Result := AttributeNodes['Licencias'].NodeValue;
end;

procedure TXMLSentinelType.Set_Licencias(Value: Integer);
begin
  SetAttribute('Licencias', Value);
end;

function TXMLSentinelType.Get_Numero: Integer;
begin
  Result := AttributeNodes['Numero'].NodeValue;
end;

procedure TXMLSentinelType.Set_Numero(Value: Integer);
begin
  SetAttribute('Numero', Value);
end;

function TXMLSentinelType.Get_LimiteConfigBD: Integer;
begin
  Result := AttributeNodes['LimiteConfigBD'].NodeValue;
end;

procedure TXMLSentinelType.Set_LimiteConfigBD(Value: Integer);
begin
  SetAttribute('LimiteConfigBD', Value);
end;

function TXMLSentinelType.Get_IniciaValidacion: Integer;
begin
  Result := AttributeNodes['IniciaValidacion'].NodeValue;
end;

procedure TXMLSentinelType.Set_IniciaValidacion(Value: Integer);
begin
  SetAttribute('IniciaValidacion', Value);
end;

function TXMLSentinelType.Get_Version: WideString;
begin
  Result := AttributeNodes['Version'].Text;
end;

procedure TXMLSentinelType.Set_Version(Value: WideString);
begin
  SetAttribute('Version', Value);
end;

function TXMLSentinelType.Get_EsKitDistribuidor: WideString;
begin
  Result := AttributeNodes['EsKitDistribuidor'].Text;
end;

procedure TXMLSentinelType.Set_EsKitDistribuidor(Value: WideString);
begin
  SetAttribute('EsKitDistribuidor', Value);
end;

function TXMLSentinelType.Get_Plataforma: WideString;
begin
  Result := AttributeNodes['Plataforma'].Text;
end;

procedure TXMLSentinelType.Set_Plataforma(Value: WideString);
begin
  SetAttribute('Plataforma', Value);
end;

function TXMLSentinelType.Get_BaseDeDatos: WideString;
begin
  Result := AttributeNodes['BaseDeDatos'].Text;
end;

procedure TXMLSentinelType.Set_BaseDeDatos(Value: WideString);
begin
  SetAttribute('BaseDeDatos', Value);
end;

function TXMLSentinelType.Get_Vencimiento: WideString;
begin
  Result := AttributeNodes['Vencimiento'].Text;
end;

procedure TXMLSentinelType.Set_Vencimiento(Value: WideString);
begin
  SetAttribute('Vencimiento', Value);
end;

function TXMLSentinelType.Get_CaducidadPrestamos: WideString;
begin
  Result := AttributeNodes['CaducidadPrestamos'].Text;
end;

procedure TXMLSentinelType.Set_CaducidadPrestamos(Value: WideString);
begin
  SetAttribute('CaducidadPrestamos', Value);
end;

function TXMLSentinelType.Get_Modulo(Index: Integer): IXMLModuloType;
begin
  Result := List[Index] as IXMLModuloType;
end;

function TXMLSentinelType.Add: IXMLModuloType;
begin
  Result := AddItem(-1) as IXMLModuloType;
end;

function TXMLSentinelType.Insert(const Index: Integer): IXMLModuloType;
begin
  Result := AddItem(Index) as IXMLModuloType;
end;

{ TXMLModuloType }

function TXMLModuloType.Get_Numero: Integer;
begin
  Result := AttributeNodes['Numero'].NodeValue;
end;

procedure TXMLModuloType.Set_Numero(Value: Integer);
begin
  SetAttribute('Numero', Value);
end;

function TXMLModuloType.Get_Nombre: WideString;
begin
  Result := AttributeNodes['Nombre'].Text;
end;

procedure TXMLModuloType.Set_Nombre(Value: WideString);
begin
  SetAttribute('Nombre', Value);
end;

function TXMLModuloType.Get_Autorizado: WideString;
begin
  Result := AttributeNodes['Autorizado'].Text;
end;

procedure TXMLModuloType.Set_Autorizado(Value: WideString);
begin
  SetAttribute('Autorizado', Value);
end;

function TXMLModuloType.Get_Prestado: WideString;
begin
  Result := AttributeNodes['Prestado'].Text;
end;

procedure TXMLModuloType.Set_Prestado(Value: WideString);
begin
  SetAttribute('Prestado', Value);
end;

{ TXMLUserDataType }

function TXMLUserDataType.Get_UsuariosTotales: Integer;
begin
  Result := AttributeNodes['UsuariosTotales'].NodeValue;
end;

procedure TXMLUserDataType.Set_UsuariosTotales(Value: Integer);
begin
  SetAttribute('UsuariosTotales', Value);
end;

{ TXMLCompanyType }

procedure TXMLCompanyType.AfterConstruction;
begin
  RegisterChildNode('Nomina', TXMLNominaType);
  RegisterChildNode('Asistencia', TXMLAsistenciaType);
  inherited;
end;

function TXMLCompanyType.Get_CM_CODIGO: WideString;
begin
  Result := AttributeNodes['CM_CODIGO'].Text;
end;

procedure TXMLCompanyType.Set_CM_CODIGO(Value: WideString);
begin
  SetAttribute('CM_CODIGO', Value);
end;

function TXMLCompanyType.Get_CM_NOMBRE: WideString;
begin
  Result := AttributeNodes['CM_NOMBRE'].Text;
end;

procedure TXMLCompanyType.Set_CM_NOMBRE(Value: WideString);
begin
  SetAttribute('CM_NOMBRE', Value);
end;

function TXMLCompanyType.Get_CM_ACTIVOS: Integer;
begin
  Result := AttributeNodes['CM_ACTIVOS'].NodeValue;
end;

procedure TXMLCompanyType.Set_CM_ACTIVOS(Value: Integer);
begin
  SetAttribute('CM_ACTIVOS', Value);
end;

function TXMLCompanyType.Get_CM_INACTIV: Integer;
begin
  Result := AttributeNodes['CM_INACTIV'].NodeValue;
end;

procedure TXMLCompanyType.Set_CM_INACTIV(Value: Integer);
begin
  SetAttribute('CM_INACTIV', Value);
end;

function TXMLCompanyType.Get_CM_TOTAL: Integer;
begin
  Result := AttributeNodes['CM_TOTAL'].NodeValue;
end;

procedure TXMLCompanyType.Set_CM_TOTAL(Value: Integer);
begin
  SetAttribute('CM_TOTAL', Value);
end;

function TXMLCompanyType.Get_CM_NIVEL0: WideString;
begin
  Result := AttributeNodes['CM_NIVEL0'].Text;
end;

procedure TXMLCompanyType.Set_CM_NIVEL0(Value: WideString);
begin
  SetAttribute('CM_NIVEL0', Value);
end;

function TXMLCompanyType.Get_CM_DATOS: WideString;
begin
  Result := AttributeNodes['CM_DATOS'].Text;
end;

procedure TXMLCompanyType.Set_CM_DATOS(Value: WideString);
begin
  SetAttribute('CM_DATOS', Value);
end;

function TXMLCompanyType.Get_DB_SIZE: WideString;
begin
  Result := AttributeNodes['DB_SIZE'].Text;
end;

procedure TXMLCompanyType.Set_DB_SIZE(Value: WideString);
begin
  SetAttribute('DB_SIZE', Value);
end;

function TXMLCompanyType.Get_CM_ULT_ALT: WideString;
begin
  Result := AttributeNodes['CM_ULT_ALT'].Text;
end;

procedure TXMLCompanyType.Set_CM_ULT_ALT(Value: WideString);
begin
  SetAttribute('CM_ULT_ALT', Value);
end;

function TXMLCompanyType.Get_CM_ULT_BAJ: WideString;
begin
  Result := AttributeNodes['CM_ULT_BAJ'].Text;
end;

procedure TXMLCompanyType.Set_CM_ULT_BAJ(Value: WideString);
begin
  SetAttribute('CM_ULT_BAJ', Value);
end;

function TXMLCompanyType.Get_CM_ULT_TAR: WideString;
begin
  Result := AttributeNodes['CM_ULT_TAR'].Text;
end;

procedure TXMLCompanyType.Set_CM_ULT_TAR(Value: WideString);
begin
  SetAttribute('CM_ULT_TAR', Value);
end;

function TXMLCompanyType.Get_CM_ULT_NOM: WideString;
begin
  Result := AttributeNodes['CM_ULT_NOM'].Text;
end;

procedure TXMLCompanyType.Set_CM_ULT_NOM(Value: WideString);
begin
  SetAttribute('CM_ULT_NOM', Value);
end;

function TXMLCompanyType.Get_PE_YEAR: Integer;
begin
  Result := AttributeNodes['PE_YEAR'].NodeValue;
end;

procedure TXMLCompanyType.Set_PE_YEAR(Value: Integer);
begin
  SetAttribute('PE_YEAR', Value);
end;

function TXMLCompanyType.Get_PE_TIPO: Integer;
begin
  Result := AttributeNodes['PE_TIPO'].NodeValue;
end;

procedure TXMLCompanyType.Set_PE_TIPO(Value: Integer);
begin
  SetAttribute('PE_TIPO', Value);
end;

function TXMLCompanyType.Get_PE_NUMERO: Integer;
begin
  Result := AttributeNodes['PE_NUMERO'].NodeValue;
end;

procedure TXMLCompanyType.Set_PE_NUMERO(Value: Integer);
begin
  SetAttribute('PE_NUMERO', Value);
end;

function TXMLCompanyType.Get_PE_FEC_INI: WideString;
begin
  Result := AttributeNodes['PE_FEC_INI'].Text;
end;

procedure TXMLCompanyType.Set_PE_FEC_INI(Value: WideString);
begin
  SetAttribute('PE_FEC_INI', Value);
end;

function TXMLCompanyType.Get_CM_CONNECT: WideString;
begin
  Result := AttributeNodes['CM_CONNECT'].Text;
end;

procedure TXMLCompanyType.Set_CM_CONNECT(Value: WideString);
begin
  SetAttribute('CM_CONNECT', Value);
end;

function TXMLCompanyType.Get_PC_SI_QTY: Integer;
begin
  Result := AttributeNodes['PC_SI_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_PC_SI_QTY(Value: Integer);
begin
  SetAttribute('PC_SI_QTY', Value);
end;

function TXMLCompanyType.Get_PC_SI_FEC: WideString;
begin
  Result := AttributeNodes['PC_SI_FEC'].Text;
end;

procedure TXMLCompanyType.Set_PC_SI_FEC(Value: WideString);
begin
  SetAttribute('PC_SI_FEC', Value);
end;

function TXMLCompanyType.Get_PC_AN_QTY: Integer;
begin
  Result := AttributeNodes['PC_AN_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_PC_AN_QTY(Value: Integer);
begin
  SetAttribute('PC_AN_QTY', Value);
end;

function TXMLCompanyType.Get_PC_AN_FEC: WideString;
begin
  Result := AttributeNodes['PC_AN_FEC'].Text;
end;

procedure TXMLCompanyType.Set_PC_AN_FEC(Value: WideString);
begin
  SetAttribute('PC_AN_FEC', Value);
end;

function TXMLCompanyType.Get_PC_NOMLAVG: WideString;
begin
  Result := AttributeNodes['PC_NOMLAVG'].Text;
end;

procedure TXMLCompanyType.Set_PC_NOMLAVG(Value: WideString);
begin
  SetAttribute('PC_NOMLAVG', Value);
end;

function TXMLCompanyType.Get_PC_NOM_AVG: WideString;
begin
  Result := AttributeNodes['PC_NOM_AVG'].Text;
end;

procedure TXMLCompanyType.Set_PC_NOM_AVG(Value: WideString);
begin
  SetAttribute('PC_NOM_AVG', Value);
end;

function TXMLCompanyType.Get_PC_FOA_QTY: Integer;
begin
  Result := AttributeNodes['PC_FOA_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_PC_FOA_QTY(Value: Integer);
begin
  SetAttribute('PC_FOA_QTY', Value);
end;

function TXMLCompanyType.Get_PC_FOA_FEC: WideString;
begin
  Result := AttributeNodes['PC_FOA_FEC'].Text;
end;

procedure TXMLCompanyType.Set_PC_FOA_FEC(Value: WideString);
begin
  SetAttribute('PC_FOA_FEC', Value);
end;

function TXMLCompanyType.Get_PC_FON_QTY: Integer;
begin
  Result := AttributeNodes['PC_FON_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_PC_FON_QTY(Value: Integer);
begin
  SetAttribute('PC_FON_QTY', Value);
end;

function TXMLCompanyType.Get_PC_FON_FEC: WideString;
begin
  Result := AttributeNodes['PC_FON_FEC'].Text;
end;

procedure TXMLCompanyType.Set_PC_FON_FEC(Value: WideString);
begin
  SetAttribute('PC_FON_FEC', Value);
end;

function TXMLCompanyType.Get_PC_FOC_QTY: Integer;
begin
  Result := AttributeNodes['PC_FOC_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_PC_FOC_QTY(Value: Integer);
begin
  SetAttribute('PC_FOC_QTY', Value);
end;

function TXMLCompanyType.Get_PC_FOC_FEC: WideString;
begin
  Result := AttributeNodes['PC_FOC_FEC'].Text;
end;

procedure TXMLCompanyType.Set_PC_FOC_FEC(Value: WideString);
begin
  SetAttribute('PC_FOC_FEC', Value);
end;

function TXMLCompanyType.Get_PC_LB_QTY: Integer;
begin
  Result := AttributeNodes['PC_LB_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_PC_LB_QTY(Value: Integer);
begin
  SetAttribute('PC_LB_QTY', Value);
end;

function TXMLCompanyType.Get_PC_LB_FEC: WideString;
begin
  Result := AttributeNodes['PC_LB_FEC'].Text;
end;

procedure TXMLCompanyType.Set_PC_LB_FEC(Value: WideString);
begin
  SetAttribute('PC_LB_FEC', Value);
end;

function TXMLCompanyType.Get_PC_EM_QTY: Integer;
begin
  Result := AttributeNodes['PC_EM_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_PC_EM_QTY(Value: Integer);
begin
  SetAttribute('PC_EM_QTY', Value);
end;

function TXMLCompanyType.Get_PC_EM_FEC: WideString;
begin
  Result := AttributeNodes['PC_EM_FEC'].Text;
end;

procedure TXMLCompanyType.Set_PC_EM_FEC(Value: WideString);
begin
  SetAttribute('PC_EM_FEC', Value);
end;

function TXMLCompanyType.Get_RE_CU_QTY: Integer;
begin
  Result := AttributeNodes['RE_CU_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_RE_CU_QTY(Value: Integer);
begin
  SetAttribute('RE_CU_QTY', Value);
end;

function TXMLCompanyType.Get_RE_CF_QTY: Integer;
begin
  Result := AttributeNodes['RE_CF_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_RE_CF_QTY(Value: Integer);
begin
  SetAttribute('RE_CF_QTY', Value);
end;

function TXMLCompanyType.Get_RE_CF_FEC: WideString;
begin
  Result := AttributeNodes['RE_CF_FEC'].Text;
end;

procedure TXMLCompanyType.Set_RE_CF_FEC(Value: WideString);
begin
  SetAttribute('RE_CF_FEC', Value);
end;

function TXMLCompanyType.Get_RE_RG_QTY: Integer;
begin
  Result := AttributeNodes['RE_RG_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_RE_RG_QTY(Value: Integer);
begin
  SetAttribute('RE_RG_QTY', Value);
end;

function TXMLCompanyType.Get_RE_RG_FEC: WideString;
begin
  Result := AttributeNodes['RE_RG_FEC'].Text;
end;

procedure TXMLCompanyType.Set_RE_RG_FEC(Value: WideString);
begin
  SetAttribute('RE_RG_FEC', Value);
end;

function TXMLCompanyType.Get_RE_SM_QTY: Integer;
begin
  Result := AttributeNodes['RE_SM_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_RE_SM_QTY(Value: Integer);
begin
  SetAttribute('RE_SM_QTY', Value);
end;

function TXMLCompanyType.Get_RE_SM_FEC: WideString;
begin
  Result := AttributeNodes['RE_SM_FEC'].Text;
end;

procedure TXMLCompanyType.Set_RE_SM_FEC(Value: WideString);
begin
  SetAttribute('RE_SM_FEC', Value);
end;

function TXMLCompanyType.Get_RE_PC_QTY: Integer;
begin
  Result := AttributeNodes['RE_PC_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_RE_PC_QTY(Value: Integer);
begin
  SetAttribute('RE_PC_QTY', Value);
end;

function TXMLCompanyType.Get_RE_PC_FEC: WideString;
begin
  Result := AttributeNodes['RE_PC_FEC'].Text;
end;

procedure TXMLCompanyType.Set_RE_PC_FEC(Value: WideString);
begin
  SetAttribute('RE_PC_FEC', Value);
end;

function TXMLCompanyType.Get_RE_EV_QTY: Integer;
begin
  Result := AttributeNodes['RE_EV_QTY'].NodeValue;
end;

procedure TXMLCompanyType.Set_RE_EV_QTY(Value: Integer);
begin
  SetAttribute('RE_EV_QTY', Value);
end;

function TXMLCompanyType.Get_RE_EV_FEC: WideString;
begin
  Result := AttributeNodes['RE_EV_FEC'].Text;
end;

procedure TXMLCompanyType.Set_RE_EV_FEC(Value: WideString);
begin
  SetAttribute('RE_EV_FEC', Value);
end;

function TXMLCompanyType.Get_Nomina: IXMLNominaType;
begin
  Result := ChildNodes['Nomina'] as IXMLNominaType;
end;

function TXMLCompanyType.Get_Asistencia: IXMLAsistenciaType;
begin
  Result := ChildNodes['Asistencia'] as IXMLAsistenciaType;
end;

{ TXMLCompanyTypeList }

function TXMLCompanyTypeList.Add: IXMLCompanyType;
begin
  Result := AddItem(-1) as IXMLCompanyType;
end;

function TXMLCompanyTypeList.Insert(const Index: Integer): IXMLCompanyType;
begin
  Result := AddItem(Index) as IXMLCompanyType;
end;
function TXMLCompanyTypeList.Get_Item(Index: Integer): IXMLCompanyType;
begin
  Result := List[Index] as IXMLCompanyType;
end;

{ TXMLNominaType }

procedure TXMLNominaType.AfterConstruction;
begin
  RegisterChildNode('ISPT', TXMLISPTType);
  RegisterChildNode('INFONAVITProv', TXMLINFONAVITProvType);
  inherited;
end;

function TXMLNominaType.Get_ISPT: IXMLISPTType;
begin
  Result := ChildNodes['ISPT'] as IXMLISPTType;
end;

function TXMLNominaType.Get_INFONAVITAporta: WideString;
begin
  Result := ChildNodes['INFONAVITAporta'].Text;
end;

procedure TXMLNominaType.Set_INFONAVITAporta(Value: WideString);
begin
  ChildNodes['INFONAVITAporta'].NodeValue := Value;
end;

function TXMLNominaType.Get_INFONAVITProv: IXMLINFONAVITProvType;
begin
  Result := ChildNodes['INFONAVITProv'] as IXMLINFONAVITProvType;
end;

function TXMLNominaType.Get_Periodos: WideString;
begin
  Result := ChildNodes['Periodos'].Text;
end;

procedure TXMLNominaType.Set_Periodos(Value: WideString);
begin
  ChildNodes['Periodos'].NodeValue := Value;
end;

{ TXMLISPTType }

procedure TXMLISPTType.AfterConstruction;
begin
  RegisterChildNode('Concepto', TXMLConceptoType);
  inherited;
end;

function TXMLISPTType.Get_Concepto: IXMLConceptoType;
begin
  Result := ChildNodes['Concepto'] as IXMLConceptoType;
end;

{ TXMLConceptoType }

function TXMLConceptoType.Get_No: Integer;
begin
  Result := AttributeNodes['No'].NodeValue;
end;

procedure TXMLConceptoType.Set_No(Value: Integer);
begin
  SetAttribute('No', Value);
end;

{ TXMLINFONAVITProvType }

procedure TXMLINFONAVITProvType.AfterConstruction;
begin
  RegisterChildNode('Concepto', TXMLConceptoType);
  inherited;
end;

function TXMLINFONAVITProvType.Get_Concepto: IXMLConceptoType;
begin
  Result := ChildNodes['Concepto'] as IXMLConceptoType;
end;

{ TXMLAsistenciaType }

function TXMLAsistenciaType.Get_Checadas: WideString;
begin
  Result := ChildNodes['Checadas'].Text;
end;

procedure TXMLAsistenciaType.Set_Checadas(Value: WideString);
begin
  ChildNodes['Checadas'].NodeValue := Value;
end;

{ TXMLTotalesType }

function TXMLTotalesType.Get_TotalActivos: Integer;
begin
  Result := AttributeNodes['TotalActivos'].NodeValue;
end;

procedure TXMLTotalesType.Set_TotalActivos(Value: Integer);
begin
  SetAttribute('TotalActivos', Value);
end;

function TXMLTotalesType.Get_ActivosProduccion: Integer;
begin
  Result := AttributeNodes['ActivosProduccion'].NodeValue;
end;

procedure TXMLTotalesType.Set_ActivosProduccion(Value: Integer);
begin
  SetAttribute('ActivosProduccion', Value);
end;

function TXMLTotalesType.Get_ActivosPruebas: Integer;
begin
  Result := AttributeNodes['ActivosPruebas'].NodeValue;
end;

procedure TXMLTotalesType.Set_ActivosPruebas(Value: Integer);
begin
  SetAttribute('ActivosPruebas', Value);
end;

function TXMLTotalesType.Get_TotalBajas: Integer;
begin
  Result := AttributeNodes['TotalBajas'].NodeValue;
end;

procedure TXMLTotalesType.Set_TotalBajas(Value: Integer);
begin
  SetAttribute('TotalBajas', Value);
end;

function TXMLTotalesType.Get_BajasProduccion: Integer;
begin
  Result := AttributeNodes['BajasProduccion'].NodeValue;
end;

procedure TXMLTotalesType.Set_BajasProduccion(Value: Integer);
begin
  SetAttribute('BajasProduccion', Value);
end;

function TXMLTotalesType.Get_BajasPruebas: Integer;
begin
  Result := AttributeNodes['BajasPruebas'].NodeValue;
end;

procedure TXMLTotalesType.Set_BajasPruebas(Value: Integer);
begin
  SetAttribute('BajasPruebas', Value);
end;

function TXMLTotalesType.Get_UltimaAltaBD: WideString;
begin
  Result := AttributeNodes['UltimaAltaBD'].Text;
end;

procedure TXMLTotalesType.Set_UltimaAltaBD(Value: WideString);
begin
  SetAttribute('UltimaAltaBD', Value);
end;

function TXMLTotalesType.Get_CantAltasBD: Integer;
begin
  Result := AttributeNodes['CantAltasBD'].NodeValue;
end;

procedure TXMLTotalesType.Set_CantAltasBD(Value: Integer);
begin
  SetAttribute('CantAltasBD', Value);
end;

function TXMLTotalesType.Get_CantAsignaComparte: Integer;
begin
  Result := AttributeNodes['CantAsignaComparte'].NodeValue;
end;

procedure TXMLTotalesType.Set_CantAsignaComparte(Value: Integer);
begin
  SetAttribute('CantAsignaComparte', Value);
end;

end. 