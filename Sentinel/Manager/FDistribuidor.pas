unit FDistribuidor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     StdCtrls, Buttons, ExtCtrls, DBCtrls;

type
  TFormDistribuidor = class(TForm)
    DBGrid1: TDBGrid;
    PanelInferior: TPanel;
    Salir: TBitBtn;
    DBNavigator: TDBNavigator;
    DataSource: TDataSource;
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
    procedure FormShow(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormDistribuidor: TFormDistribuidor;

implementation

uses DSentinel,
     FEditDistribuidor;

{$R *.DFM}

procedure TFormDistribuidor.FormShow(Sender: TObject);
begin
     dmSentinel.AbrirDistribuidor( Datasource );
end;

procedure TFormDistribuidor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     dmSentinel.ResetDatasource( Datasource );
end;

procedure TFormDistribuidor.DBGrid1DblClick(Sender: TObject);
begin
     FEditDistribuidor.EditarDistribuidor( False );
end;

procedure TFormDistribuidor.DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
begin
     if ( Button = nbInsert ) then
        FEditDistribuidor.EditarDistribuidor( True );
end;

procedure TFormDistribuidor.DataSourceStateChange(Sender: TObject);
begin
     with DataSource do
     begin
          if Assigned( Dataset ) then
             Salir.Enabled := ( Dataset.State = dsBrowse )
          else
              Salir.Enabled := True;
     end;
end;

end.
