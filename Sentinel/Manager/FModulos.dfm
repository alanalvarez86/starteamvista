�
 TMODULELISTER 0�
  TPF0TModuleListerModuleListerLeftjTop� Width0Height�ActiveControl
ModuleListCaption"Listar M�dulos Usados Por ClientesColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TRadioGroupSalidaLeft Top� Width(Height;AlignalBottomCaption Salida 	ItemIndex Items.StringsPantalla / ImpresoraHoja de Excel TabOrderOnClickSalidaClick  TPanelPanelInferiorLeft TopRWidth(Height$AlignalBottomTabOrder TBitBtnOKLeft@TopWidthKHeightCaption&OKDefault	TabOrder OnClickOKClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnCancelarLeft� TopWidthKHeightCaption	&CancelarTabOrderKindbkCancel   TPanelPanelArchivoLeft TopWidth(Height7AlignalBottom
BevelOuterbvNoneTabOrder TLabel
ArchivoLBLLeftTopWidth'HeightCaption	Archi&vo:FocusControlArchivo  TSpeedButtonArchivoFindLeftTopWidthHeightHintBuscar Archivo de Excel
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333333�3333330 ?����37w 0  330 w�ww?37w���333ws3?33?࿿ 33 w�3ws�3w�����33 w�3?���w࿿   33w�3www??�������w�?���sw�    3�w�wwww3w �3333ws�s333?30 3333 37w3333w3333333 3333333w333333333333333�3333330 3333337w3333330 3333337w3333333333333333	NumGlyphsParentShowHintShowHint	OnClickArchivoFindClick  TEditArchivoLeft/TopWidth� HeightTabOrder   	TCheckBoxSistemasVigentesLeft0Top Width� HeightCaptionS�lo Sistemas VigentesChecked	State	cbCheckedTabOrder   	TGroupBox	ModulosGBLeft Top Width(Height� AlignalClientCaption! Seleccione Los M�dulos Deseados TabOrder  TCheckListBox
ModuleListLeftTopWidth$Height� AlignalClientBorderStylebsNoneColor	clBtnFace
ItemHeightTabOrder OnClickModuleListClick   TSaveDialog
SaveDialog
DefaultExtxlsFilter'Excel ( *.xls )|*.xls|Todos ( *.* )|*.*FilterIndex TitleSeleccione La Hoja De ExcelLeft� Top   