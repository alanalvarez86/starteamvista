unit FPrincipal;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Menus, Db, Grids,
     DBGrids, ExtCtrls, StdCtrls, Buttons, ShellAPI,
     FAutoClasses,
     FAutoServer,
     FAutoMaster, ZetaKeyCombo, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxSkinsForm, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter;

type
  TFormPrincipal = class(TForm)
    MainMenu: TMainMenu;
    Archivo: TMenuItem;
    Editar: TMenuItem;
    N1: TMenuItem;
    CatalogosCiudades: TMenuItem;
    CatalogosEstados: TMenuItem;
    CatalogosUsuarios: TMenuItem;
    CatalogosVersiones: TMenuItem;
    ArchivoSentinel: TMenuItem;
    CatalogosDistribuidores: TMenuItem;
    CatalogosGrupos: TMenuItem;
    N3: TMenuItem;
    Catalogos: TMenuItem;
    ArchivoSalir: TMenuItem;
    ArchivoImprimir: TMenuItem;
    N2: TMenuItem;
    EditarAgregar: TMenuItem;
    EditarModificar: TMenuItem;
    EditarBorrar: TMenuItem;
    PanelSuperior: TPanel;
    AgregarBtn: TSpeedButton;
    Modificar: TSpeedButton;
    BorrarBtn: TSpeedButton;
    ImprimirBtn: TSpeedButton;
    CatalogosSentinelas: TMenuItem;
    Salir: TBitBtn;
    Consultas: TMenuItem;
    Sentinel: TMenuItem;
    SentinelLeer: TMenuItem;
    SentinelGrabar: TMenuItem;
    SentinelRead: TSpeedButton;
    SentinelWrite: TSpeedButton;
    ReportWriter: TSpeedButton;
    N4: TMenuItem;
    ConsultasDisena: TMenuItem;
    dsSistema: TDataSource;
    dsAutoriza: TDataSource;
    PistaLBL: TLabel;
    BuscarText: TEdit;
    Buscar: TSpeedButton;
    Busquedas: TMenuItem;
    BusquedaPorDistribuidor: TMenuItem;
    BusquedaPorRazonSocial: TMenuItem;
    BusquedasTodos: TMenuItem;
    Splitter: TSplitter;
    N5: TMenuItem;
    CatalogosGiros: TMenuItem;
    CatalogosPaises: TMenuItem;
    N6: TMenuItem;
    ConsultasVencimientos: TMenuItem;
    ConsultasCambios: TMenuItem;
    ConsultasUsoSentinelas: TMenuItem;
    ArchivoClavesInstalacion: TMenuItem;
    ClavesInstalacion: TSpeedButton;
    ConsultasModulosCliente: TMenuItem;
    N7: TMenuItem;
    Importar3DTArchivos1: TMenuItem;
    N8: TMenuItem;
    mnuTerminales: TMenuItem;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    SI_FOLIO: TcxGridDBColumn;
    SI_RAZ_SOC: TcxGridDBColumn;
    DI_NOMBRE: TcxGridDBColumn;
    GR_CODIGO: TcxGridDBColumn;
    CT_DESCRIP: TcxGridDBColumn;
    Panel1: TPanel;
    DBGridAutoriza: TDBGrid;
    PanelCentral: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    zSentinelas: TZetaKeyCombo;
    SI_SUCURSAL: TcxGridDBColumn;
    dxSkinController1: TdxSkinController;
    procedure CatalogosUsuariosClick(Sender: TObject);
    procedure CatalogosDistribuidoresClick(Sender: TObject);
    procedure CatalogosCiudadesClick(Sender: TObject);
    procedure CatalogosEstadosClick(Sender: TObject);
    procedure CatalogosGruposClick(Sender: TObject);
    procedure CatalogosVersionesClick(Sender: TObject);
    procedure ArchivoSentinelClick(Sender: TObject);
    procedure ArchivoSalirClick(Sender: TObject);
    procedure BuscarTextChange(Sender: TObject);
    procedure EditarModificarClick(Sender: TObject);
    procedure EditarAgregarClick(Sender: TObject);
    procedure EditarBorrarClick(Sender: TObject);
    procedure ArchivoImprimirClick(Sender: TObject);
    procedure DBGridAutorizaDblClick(Sender: TObject);
    procedure CatalogosSentinelasClick(Sender: TObject);
    procedure SentinelLeerClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SentinelReadClick(Sender: TObject);
    procedure SentinelWriteClick(Sender: TObject);
    procedure SentinelGrabarClick(Sender: TObject);
    procedure ConsultasDisenaClick(Sender: TObject);
    procedure BuscarTextExit(Sender: TObject);
    procedure dsSistemaDataChange(Sender: TObject; Field: TField);
    procedure BusquedaPorRazonSocialClick(Sender: TObject);
    procedure BusquedaPorDistribuidorClick(Sender: TObject);
    procedure BusquedasTodosClick(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
    procedure CatalogosGirosClick(Sender: TObject);
    procedure CatalogosPaisesClick(Sender: TObject);
    procedure DBGridSistemaDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ConsultasVencimientosClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConsultasCambiosClick(Sender: TObject);
    procedure ConsultasUsoSentinelasClick(Sender: TObject);
    procedure ArchivoClavesInstalacionClick(Sender: TObject);
    procedure ConsultasModulosClienteClick(Sender: TObject);
    procedure Importar3DTArchivos1Click(Sender: TObject);
    procedure mnuTerminalesClick(Sender: TObject);
    procedure zSentinelasChange(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
    FFilterChange: Boolean;
    function SetFilter: Boolean;
    procedure AgregaSistema;
    procedure ModificaSistema;
    procedure BorraSistema;
    procedure Reportes;
    procedure ResetFilter;
    procedure LlenarSentinelas;
  public
    { Public declarations }
    procedure Conectar;
    procedure ShowHistoria( oQuery: TDataset );
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

uses FCalendario,
     FUsuarios,
     FDistribuidor,
     FCiudad,
     FEstados,
     FPais,
     FGiro,
     FGrupos,
     FSentinels,
     FVersiones,
     FSistema,
     FHistoria,
     FInstalaciones,
     FAltaSen,
     FRepAuto,
     FVencimientos,
     FAuditoria,
     FModulos,
     FImportar3DT,
     DRep,
     DSentinel,
     ZetaCommonTools,
     FBusquedaTerminal,
     ZetaDialogo, DBTables;

{$R *.DFM}

procedure TFormPrincipal.FormCreate(Sender: TObject);
begin
     FCalendario.SetDefaultDlgFecha( Now );
     FFilterChange := False;
     dmSentinel := TdmSentinel.Create( Self );
     Caption := Format( 'Usuarios de Sistema TRESS v%s', [ FAutoClasses.UnloadVersion( 0 ) ] );
end;

procedure TFormPrincipal.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmSentinel );
end;

procedure TFormPrincipal.Conectar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with AutoMaster do
        begin
             SentinelStart;
             VersionMinima := K_VERSION_13_TXT;
        end;
        dmSentinel.AbrirPrincipal( dsSistema, dsAutoriza );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TFormPrincipal.dsSistemaDataChange(Sender: TObject; Field: TField);
begin
     if ( Field = nil ) then
     begin
          with dmSentinel do
          begin
               AbrirSentinelPorSistema;
               LlenarSentinelas;
               zSentinelas.ItemIndex := 0;
               AbrirPrincipalHistoria(zSentinelas.LlaveEntero);
          end;
     end;
end;

procedure TFormPrincipal.AgregaSistema;
begin
     FSistema.CapturaSistema( True );
end;

procedure TFormPrincipal.ModificaSistema;
begin
     FSistema.CapturaSistema( False );
end;

procedure TFormPrincipal.BorraSistema;
var
   oCursor: TCursor;
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Este Sistema ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmSentinel.BorraSistema;
          finally
                 Screen.Cursor := oCursor;
          end;
          dmSentinel.RefrescarLook;  
     end;
end;

procedure TFormPrincipal.ShowHistoria( oQuery: TDataset );
begin
     if ( FormHistoria = nil ) then
        FormHistoria := TFormHistoria.Create( Self );
     with FormHistoria do
     begin
          Query := oQuery;
          ShowModal;
     end;
end;

procedure TFormPrincipal.ResetFilter;
begin
     dmSentinel.FilterReset( True );
     Buscar.Enabled := False;
end;

function TFormPrincipal.SetFilter: Boolean;
begin
     with dmSentinel do
     begin
          Result := FilterSet( BuscarText.Text );
          if not Result then
          begin
               ZetaDialogo.zError( '� Atenci�n !', 'No Hay Empresas Que Cumplan Con El Filtro Especificado', 0 );
               BuscarText.SetFocus;
          end;
     end;
     Buscar.Enabled := False;
     FFilterChange := False;
end;

procedure TFormPrincipal.DBGridAutorizaDblClick(Sender: TObject);
begin
     ShowHistoria( dsAutoriza.Dataset );
end;

procedure TFormPrincipal.DBGridSistemaDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
//var
  // lOK: Boolean;
begin
     {with DBGridSistema do
     begin
          lOk := ( DataSource.Dataset.FieldByName( 'SI_PROBLEMA' ).AsInteger = 0 );
          if ( gdSelected in State ) then
          begin
               with Canvas do
               begin
                    if lOK then
                    begin
                         Font.Color := clWhite;
                         Brush.Color := clNavy;
                    end
                    else
                    begin
                         Font.Color := clWhite;
                         Brush.Color := clRed;
                    end;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    with Font do
                    begin
                         if lOk then
                            Color := DBGridSistema.Font.Color
                         else
                             Color := clRed;
                    end;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;  }
end;

{ ********** Eventos del Men� ********** }

procedure TFormPrincipal.ArchivoSentinelClick(Sender: TObject);
begin
     if ( FormSentinels = nil ) then
        FormSentinels := TFormSentinels.Create( Self );
     FormSentinels.ShowModal;
end;

procedure TFormPrincipal.ArchivoClavesInstalacionClick(Sender: TObject);
begin
     if ( FormInstalacion = nil ) then
        FormInstalacion := TFormInstalacion.Create( Self );
     FormInstalacion.ShowModal;
end;

procedure TFormPrincipal.ArchivoImprimirClick(Sender: TObject);
begin
     DRep.InitReportes;
     dmRep.ImprimeClaves;
end;

procedure TFormPrincipal.ArchivoSalirClick(Sender: TObject);
begin
     Close;
end;

procedure TFormPrincipal.CatalogosUsuariosClick(Sender: TObject);
begin
     if ( FormUsuarios = nil ) then
        FormUsuarios := TFormUsuarios.Create( Self );
     FormUsuarios.Show;
end;

procedure TFormPrincipal.CatalogosDistribuidoresClick(Sender: TObject);
begin
     if ( FormDistribuidor = nil ) then
        FormDistribuidor := TFormDistribuidor.Create( Self );
     FormDistribuidor.ShowModal;
end;

procedure TFormPrincipal.CatalogosCiudadesClick(Sender: TObject);
begin
     if ( FormCiudad = nil ) then
        FormCiudad := TFormCiudad.Create( Self );
     FormCiudad.Show;
end;

procedure TFormPrincipal.CatalogosEstadosClick(Sender: TObject);
begin
     if ( FormEstados = nil ) then
        FormEstados := TFormEstados.Create( Self );
     FormEstados.Show;
end;

procedure TFormPrincipal.CatalogosPaisesClick(Sender: TObject);
begin
     if ( FormPais = nil ) then
        FormPais := TFormPais.Create( Self );
     FormPais.Show;
end;

procedure TFormPrincipal.CatalogosGruposClick(Sender: TObject);
begin
     if ( FormGrupos = nil ) then
        FormGrupos := TFormGrupos.Create( Self );
     FormGrupos.Show;
end;

procedure TFormPrincipal.CatalogosGirosClick(Sender: TObject);
begin
     if ( FormGiro = nil ) then
        FormGiro := TFormGiro.Create( Self );
     FormGiro.Show;
end;

procedure TFormPrincipal.CatalogosVersionesClick(Sender: TObject);
begin
     if ( FormVersiones = nil ) then
        FormVersiones := TFormVersiones.Create( Self );
     FormVersiones.Show;
end;

procedure TFormPrincipal.BuscarTextChange(Sender: TObject);
begin
     with BuscarText do
     begin
          FFilterChange := True;
          if ( Length( Trim( Text ) ) = 0 ) then
             ResetFilter
          else
              Buscar.Enabled := True;
     end;
end;

procedure TFormPrincipal.BuscarTextExit(Sender: TObject);
begin
     if FFilterChange then
        SetFilter;
end;

procedure TFormPrincipal.BuscarClick(Sender: TObject);
begin
     SetFilter;
end;

procedure TFormPrincipal.EditarModificarClick(Sender: TObject);
begin
     ModificaSistema;
end;

procedure TFormPrincipal.EditarAgregarClick(Sender: TObject);
begin
     AgregaSistema;
end;

procedure TFormPrincipal.EditarBorrarClick(Sender: TObject);
begin
     BorraSistema;
end;

procedure TFormPrincipal.CatalogosSentinelasClick(Sender: TObject);
begin
     if AltaSen = nil then
        Altasen := TAltaSen.Create( Self );
     AltaSen.ShowModal;
end;

procedure TFormPrincipal.SentinelLeerClick(Sender: TObject);
begin
     dmSentinel.LeerSentinel;
end;

procedure TFormPrincipal.SentinelReadClick(Sender: TObject);
begin
     SentinelLeer.Click;
end;

procedure TFormPrincipal.SentinelWriteClick(Sender: TObject);
begin
     dmSentinel.GrabarSentinel;
end;

procedure TFormPrincipal.SentinelGrabarClick(Sender: TObject);
begin
     SentinelWrite.Click;
end;

procedure TFormPrincipal.Reportes;
begin
     ShellExecute( Handle, 'Open', 'Artist.exe', 'NULL', 'NULL', SW_SHOWNORMAL );
end;

procedure TFormPrincipal.ConsultasVencimientosClick(Sender: TObject);
begin
     DRep.InitReportes;
     if ( Vencimientos = nil ) then
        Vencimientos := TVencimientos.Create( Self );
     with Vencimientos do
     begin
          ShowModal;
     end;
end;

procedure TFormPrincipal.ConsultasCambiosClick(Sender: TObject);
begin
     DRep.InitReportes;
     if ( Auditoria = nil ) then
        Auditoria := TAuditoria.Create( Self );
     with Auditoria do
     begin
          ShowModal;
     end;
end;

procedure TFormPrincipal.ConsultasUsoSentinelasClick(Sender: TObject);
begin
     DRep.InitReportes;
     dmRep.ImprimeUsoSentinel;
end;

procedure TFormPrincipal.ConsultasModulosClienteClick(Sender: TObject);
begin
     DRep.InitReportes;
     if ( ModuleLister = nil ) then
        ModuleLister := TModuleLister.Create( Self );
     with ModuleLister do
     begin
          ShowModal;
     end;
end;

procedure TFormPrincipal.ConsultasDisenaClick(Sender: TObject);
begin
     Reportes;
end;

procedure TFormPrincipal.BusquedaPorRazonSocialClick(Sender: TObject);
begin
     BusquedaPorRazonSocial.Checked := True;
     PistaLBL.Caption := 'Nombre:';
     dmSentinel.FilterSetCriteria( cfRazonSocial );
     Buscar.Enabled := ( BuscarText.Text <> '' );
end;

procedure TFormPrincipal.BusquedaPorDistribuidorClick(Sender: TObject);
begin
     BusquedaPorDistribuidor.Checked := True;
     PistaLBL.Caption := 'Distribuidor:';
     dmSentinel.FilterSetCriteria( cfDistribuidor );
     Buscar.Enabled := ( BuscarText.Text <> '' );
end;

procedure TFormPrincipal.BusquedasTodosClick(Sender: TObject);
begin
     BusquedasTodos.Checked := True;
     PistaLBL.Caption := 'Pista:';
     dmSentinel.FilterSetCriteria( cfTodos );
     Buscar.Enabled := ( BuscarText.Text <> '' );
end;

procedure TFormPrincipal.Importar3DTArchivos1Click(Sender: TObject);
begin
    if ( Importar3DT = nil ) then
        Importar3DT := TImportar3DT.Create( Self );
     with Importar3DT do
     begin
          ShowModal;
     end;

     FreeAndNil ( Importar3DT );
end;

procedure TFormPrincipal.mnuTerminalesClick(Sender: TObject);
begin
     if ( BusquedaTeminales = nil ) then
        BusquedaTeminales := TBusquedaTeminales.Create( Self );
     BusquedaTeminales.Show;
end;


procedure TFormPrincipal.LlenarSentinelas();

function GetTexto(Tipo:Integer):string;
begin
     If Tipo = 0 then
        Result := 'Paralelo'
     else
         If Tipo = 1 then
            Result := 'USB'
     else
         If Tipo = 2 then
            Result := 'USB-Virtual'
     else
         If Tipo = 99 then
            Result := 'Virtual'
         else
             Result := 'Desconocido';

end;

function GetActivo(Activo:Integer):string;
begin
     Result := '';
     if Activo = 0 then
        Result := ' [ x Desactivado ] ';
end;


begin
     with dmSentinel.qSentinelPorSist do
     begin
          zSentinelas.Lista.Clear;
          First;
          while not Eof do
          begin
               //zSentinelas.Items.Add(FieldByName('SN_NUMERO').AsString +'='+FieldByName('SN_NOMBRE').AsString);
               zSentinelas.Lista.AddObject(FieldByName('SN_NUMERO').AsString +'='+FieldByName('SN_NUMERO').AsString +':'+FieldByName('SN_NOMBRE').AsString+' - '+GetTexto(FieldByName('SN_TIPO').AsInteger)+GetActivo(FieldByName('SN_ACTIVO').AsInteger) ,TObject(FieldByName('SN_NUMERO').AsInteger));
               Next;
          end;
     end;
end;

procedure TFormPrincipal.zSentinelasChange(Sender: TObject);
begin
     dmSentinel.AbrirPrincipalHistoria(zSentinelas.LlaveEntero);
end;

procedure TFormPrincipal.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
     ModificaSistema;
end;

end.
