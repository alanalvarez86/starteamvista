unit FSentinels;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     DBCtrls, ExtCtrls, StdCtrls, Buttons, DBTables,
     FCatalogo, Mask, ZetaNumero;

type
  TFormSentinels = class(TFormCatalogo)
    DBGrid2: TDBGrid;
    dsHistoria: TDataSource;
    PanelSuperior: TPanel;
    PanelMedio: TPanel;
    NoSentinelLBL: TLabel;
    NoSentinel: TZetaNumero;
    NoSentinelSeek: TBitBtn;
    Borrar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dsCatalogoDataChange(Sender: TObject; Field: TField);
    procedure dsCatalogoStateChange(Sender: TObject);
    procedure NoSentinelSeekClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure NoSentinelChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormSentinels: TFormSentinels;

implementation

uses ZetaDialogo,
     FPrincipal,
     DSentinel;

{$R *.DFM}

procedure TFormSentinels.FormShow(Sender: TObject);
begin
     inherited;
     dmSentinel.AbrirSentinel( dsCatalogo, dsHistoria );
     NoSentinelSeek.Enabled := False;
     ActiveControl := NoSentinel;
end;

procedure TFormSentinels.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     with dmSentinel do
     begin
          CerrarSentinel;
          ResetDatasource( dsCatalogo );
          ResetDatasource( dsHistoria );
     end;
end;

procedure TFormSentinels.dsCatalogoDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
     begin
          dmSentinel.AbrirSentinelHistoria;
     end;
end;

procedure TFormSentinels.dsCatalogoStateChange(Sender: TObject);
var
   lEnabled: Boolean;
begin
     inherited;
     with dsCatalogo do
     begin
          if Assigned( Dataset ) then
             lEnabled := ( Dataset.State = dsBrowse )
          else
              lEnabled := False;
     end;
     NoSentinelLBL.Enabled := lEnabled;
     NoSentinel.Enabled := lEnabled;
     NoSentinelSeek.Enabled := lEnabled;
     Borrar.Enabled := lEnabled;
end;

procedure TFormSentinels.NoSentinelChange(Sender: TObject);
begin
     inherited;
     NoSentinelSeek.Enabled := True;
end;

procedure TFormSentinels.NoSentinelSeekClick(Sender: TObject);
var
   iSentinel: Integer;
begin
     inherited;
     iSentinel := NoSentinel.ValorEntero;
     if ( iSentinel > 0 ) then
     begin
          if dsCatalogo.Dataset.Locate( 'SN_NUMERO', iSentinel, [] ) then
          begin
               NoSentinelSeek.Enabled := False;
               DBGrid1.SetFocus;
          end
          else
          begin
               ZetaDialogo.zError( '� Error !', '# de Sentinel No Existe', 0 );
               NoSentinel.SetFocus;
          end;
     end
     else
         ZetaDialogo.zError( '� Error !', '# de Sentinel A Buscar No Fu� Especificado', 0 );
end;

procedure TFormSentinels.DBGrid2DblClick(Sender: TObject);
begin
     inherited;
     FPrincipal.FormPrincipal.ShowHistoria( dsHistoria.Dataset );
end;

procedure TFormSentinels.BorrarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Borrar Sentinel', '� Desea Borrar Este Sentinel ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             try
                dsCatalogo.Dataset.Delete;
             except
                   on Error: Exception do
                   begin
                        ZetaDialogo.zExcepcion( '� Error Al Borrar Sentinel !', 'Hubo Un Error Al Borrar El Sentinel', Error, 0 );
                   end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

end.
