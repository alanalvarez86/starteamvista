object EmailCFG: TEmailCFG
  Left = 214
  Top = 145
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Emails Configuration'
  ClientHeight = 465
  ClientWidth = 710
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000666666666666666666666666660000
    0066666666666666666666666666000000000000000000000000000000000000
    000FFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFFFFFFFFFF06666
    660FFFFFFFFFF77FFFFFFFFFFFF06666660FFFFFFF7777FF777FFFFFFFF00000
    000000FFFFFFFFFFFFFFFFFFFFF06666660FFFFFFF777777777FFFFFFFF06666
    660FFFFFFFFFFFFFFFFFFFFFFFF06600000000FFFF777777777FFFFFFFF06666
    660FFFFFFFFFFFFFFFFFFFFFFFF06666660FFFFFFFFFFFFFFFFFFFFFFFF06000
    000000000FFFFFFFFFF1F1F191F06666660FFFFFFFFFFFFFFFFFFFF979F06666
    660F7777FFFFFFFFFFF1F1F191F00000000FFFFFFFFFFFFFFFFFFFFFFFF06666
    6600000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC000000FC00
    0000FC000000FC000000FC000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sBar: TStatusBar
    Left = 0
    Top = 444
    Width = 710
    Height = 21
    BiDiMode = bdRightToLeft
    Panels = <
      item
        Width = 100
      end
      item
        Alignment = taRightJustify
        Width = 150
      end
      item
        Alignment = taRightJustify
        Width = 50
      end>
    ParentBiDiMode = False
    SimplePanel = False
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 80
    Width = 710
    Height = 364
    Align = alBottom
    DataSource = dsBoletin
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'BL_EMAIL'
        Title.Caption = 'Corre Electr�nico'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BL_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BL_EMPRESA'
        Title.Caption = 'Empresa'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BL_ACTIVO'
        Title.Caption = 'Activo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BL_ALTA'
        Title.Caption = 'Alta'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BL_BAJA'
        Title.Caption = 'Baja'
        Visible = True
      end>
  end
  object gbFiltro: TGroupBox
    Left = 0
    Top = 0
    Width = 345
    Height = 49
    Caption = ' Filtro '
    TabOrder = 6
    object rbActivos: TRadioButton
      Left = 12
      Top = 19
      Width = 61
      Height = 17
      Caption = 'Activos'
      TabOrder = 0
      OnClick = rbActivosClick
    end
    object rbBajas: TRadioButton
      Left = 124
      Top = 19
      Width = 57
      Height = 17
      Caption = 'Bajas'
      TabOrder = 1
      OnClick = rbBajasClick
    end
    object rbAmbos: TRadioButton
      Left = 232
      Top = 19
      Width = 57
      Height = 17
      Caption = 'Ambos'
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = rbAmbosClick
    end
  end
  object gbActions: TGroupBox
    Left = 352
    Top = 0
    Width = 353
    Height = 49
    Caption = ' Acciones '
    TabOrder = 3
    object BBAgregar: TBitBtn
      Left = 32
      Top = 16
      Width = 75
      Height = 25
      Caption = '&Agregar'
      TabOrder = 0
      OnClick = BBAgregarClick
      Kind = bkOK
    end
    object BBBorrar: TBitBtn
      Left = 134
      Top = 16
      Width = 75
      Height = 25
      Caption = '&Borrar'
      TabOrder = 1
      OnClick = BBBorrarClick
      Kind = bkNo
    end
    object BBModificar: TBitBtn
      Left = 238
      Top = 16
      Width = 75
      Height = 25
      Caption = '&Modificar'
      Default = True
      ModalResult = 1
      TabOrder = 2
      OnClick = BBModificarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object edEmail: TEdit
    Left = 13
    Top = 56
    Width = 201
    Height = 21
    TabOrder = 0
    OnChange = edEmailChange
  end
  object edNombre: TEdit
    Left = 213
    Top = 56
    Width = 202
    Height = 21
    TabOrder = 1
    OnChange = edNombreChange
  end
  object edEmpresa: TEdit
    Left = 414
    Top = 56
    Width = 150
    Height = 21
    TabOrder = 2
    OnChange = edEmpresaChange
  end
  object MainMenu1: TMainMenu
    Left = 528
    Top = 200
    object Archivo: TMenuItem
      Caption = '&Archivo'
      object ArchivoImportarAltas: TMenuItem
        Caption = 'Importar &Altas'
        OnClick = ArchivoImportarAltasClick
      end
      object ArchivoImportarBajas: TMenuItem
        Caption = 'Importar &Bajas'
        OnClick = ArchivoImportarBajasClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object ArchivoSalir: TMenuItem
        Caption = '&Salir'
        ShortCut = 16499
        OnClick = ArchivoSalirClick
      end
    end
    object eMails: TMenuItem
      Caption = 'e Mails'
      object eMailsEnviarBoletin: TMenuItem
        Caption = 'Enviar Bolet�n'
        OnClick = eMailsEnviarBoletinClick
      end
      object EnviarCorreoAUnaLista1: TMenuItem
        Caption = 'Enviar Correo A Una Lista'
        OnClick = EnviarCorreoAUnaLista1Click
      end
    end
  end
  object dbBoletin: TDatabase
    DatabaseName = 'dbBoletin'
    DriverName = 'INTRBASE'
    LoginPrompt = False
    Params.Strings = (
      'SERVER NAME=F-SERVER:D:\Datos\Users\Lety\Sentinel\sentinel.gdb'
      'USER NAME=SYSDBA'
      'PASSWORD=m'
      ' ')
    SessionName = 'Default'
    Left = 584
    Top = 200
  end
  object tqBoletin: TQuery
    CachedUpdates = True
    AfterOpen = tqBoletinAfterOpen
    BeforePost = tqBoletinBeforePost
    OnNewRecord = tqBoletinNewRecord
    OnUpdateRecord = tqBoletinUpdateRecord
    DatabaseName = 'dbBoletin'
    UpdateObject = UpdateSQL
    Left = 612
    Top = 200
  end
  object dsBoletin: TDataSource
    DataSet = tqBoletin
    Left = 668
    Top = 200
  end
  object UpdateSQL: TUpdateSQL
    ModifySQL.Strings = (
      'update BOLETIN set'
      'BL_EMAIL = :BL_EMAIL,'
      'BL_NOMBRE = :BL_NOMBRE,'
      'BL_EMPRESA = :BL_EMPRESA,'
      'BL_ACTIVO = :BL_ACTIVO,'
      'BL_ALTA = :BL_ALTA,'
      'BL_BAJA = :BL_BAJA'
      'where ( UPPER( BL_EMAIL ) = UPPER( :OLD_BL_EMAIL) )'
      '')
    InsertSQL.Strings = (
      
        'insert into BOLETIN( BL_EMAIL, BL_NOMBRE, BL_EMPRESA, BL_ACTIVO,' +
        ' '
      'BL_ALTA, BL_BAJA ) values '
      
        '( :BL_EMAIL, :BL_NOMBRE, :BL_EMPRESA, :BL_ACTIVO, :BL_ALTA, :BL_' +
        'BAJA )')
    DeleteSQL.Strings = (
      'delete from BOLETIN '
      'where ( UPPER( BL_EMAIL ) =  UPPER( :OLD_BL_EMAIL ) )')
    Left = 640
    Top = 200
  end
  object OpenDialog: TOpenDialog
    Filter = 'Textos ( *.txt )|*.txt|Datos (*.dat)|*.dat|Todos ( *.* )|*.*'
    FilterIndex = 0
    Title = 'Escoja El Archivo A Importar'
    Left = 600
    Top = 56
  end
  object tqImpBorrar: TQuery
    CachedUpdates = True
    AfterOpen = tqBoletinAfterOpen
    OnNewRecord = tqBoletinNewRecord
    OnUpdateRecord = tqBoletinUpdateRecord
    DatabaseName = 'dbBoletin'
    SQL.Strings = (
      'update BOLETIN set'
      'BL_ACTIVO = :BL_ACTIVO,'
      'BL_BAJA = :BL_BAJA'
      'where ( UPPER( BL_EMAIL ) = UPPER( :BL_EMAIL ))'
      ''
      ' ')
    Left = 588
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'BL_ACTIVO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'BL_BAJA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'BL_EMAIL'
        ParamType = ptUnknown
      end>
  end
  object tqImpUpdate: TQuery
    CachedUpdates = True
    AfterOpen = tqBoletinAfterOpen
    OnNewRecord = tqBoletinNewRecord
    OnUpdateRecord = tqBoletinUpdateRecord
    DatabaseName = 'dbBoletin'
    SQL.Strings = (
      'update BOLETIN set'
      'BL_ACTIVO = :BL_ACTIVO,'
      'BL_BAJA = :BL_BAJA,'
      'BL_ALTA = :BL_ALTA'
      'where ( UPPER( BL_EMAIL ) = UPPER( :BL_EMAIL ))'
      ''
      ' ')
    Left = 588
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'BL_ACTIVO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'BL_BAJA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'BL_ALTA'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'BL_EMAIL'
        ParamType = ptUnknown
      end>
  end
  object tqImpAgregar: TQuery
    CachedUpdates = True
    AfterOpen = tqBoletinAfterOpen
    OnNewRecord = tqBoletinNewRecord
    OnUpdateRecord = tqBoletinUpdateRecord
    DatabaseName = 'dbBoletin'
    SQL.Strings = (
      'insert into BOLETIN( BL_EMAIL, BL_ACTIVO, BL_ALTA ) values '
      '( :BL_EMAIL, :BL_ACTIVO, :BL_ALTA )'
      ' ')
    Left = 588
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'BL_EMAIL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'BL_ACTIVO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'BL_ALTA'
        ParamType = ptUnknown
      end>
  end
  object tqSender: TQuery
    CachedUpdates = True
    AfterOpen = tqBoletinAfterOpen
    OnNewRecord = tqBoletinNewRecord
    OnUpdateRecord = tqBoletinUpdateRecord
    DatabaseName = 'dbBoletin'
    Left = 588
    Top = 328
  end
  object tqMailSender: TQuery
    CachedUpdates = True
    DatabaseName = 'dbBoletin'
    Left = 588
    Top = 360
  end
end
