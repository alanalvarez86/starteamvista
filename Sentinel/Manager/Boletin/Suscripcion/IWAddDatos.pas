unit IWAddDatos;
{PUBDIST}

interface

uses Classes, Controls, SysUtils,
     IWAppForm, IWApplication, IWTypes, IWCompButton, IWCompEdit,
     IWControl, IWCompLabel, IWHTMLControls, IWExtCtrls, IWLayoutMgr,
  IWTemplateProcessorHTML;

type
  TAddSuscripcion = class(TIWAppForm)
    DireccionLBL: TIWLabel;
    BLEMAIL: TIWEdit;
    AgregarBTN: TIWButton;
    NombreLBL: TIWLabel;
    BLNOMBRE: TIWEdit;
    EmpresaLBL: TIWLabel;
    BLEMPRESA: TIWEdit;
    RegresarLNK: TIWLink;
    IWTemplate: TIWTemplateProcessorHTML;
    procedure RegresarLNKClick(Sender: TObject);
    procedure AgregarBTNClick(Sender: TObject);
    procedure IWAppFormAfterRender(Sender: TObject);
  private
    { Private declarations }
    FExito: Boolean;
    function GetDireccion: String;
    function GetEmpresa: String;
    function GetNombre: String;
  protected
    { Protected declarations }
    property Direccion: String read GetDireccion;
    property Nombre: String read GetNombre;
    property Empresa: String read GetEmpresa;
    procedure ShowOneError(const sError: String);
    procedure ShowOneMessage(const sError: String);
  public
    { Public declarations }
    property Exito: Boolean read FExito write FExito;
  end;

implementation

{$R *.dfm}

uses ServerController,
     IWAddExito,
     IWMensaje,
     DSuscripciones;

procedure TAddSuscripcion.IWAppFormAfterRender(Sender: TObject);
begin
     FExito := False;
end;

function TAddSuscripcion.GetDireccion: String;
begin
     Result := BLEMAIL.Text;
end;

function TAddSuscripcion.GetEmpresa: String;
begin
     Result := BLEMPRESA.Text;
end;

function TAddSuscripcion.GetNombre: String;
begin
     Result := BLNOMBRE.Text;
end;

procedure TAddSuscripcion.ShowOneError( const sError: String );
begin
     with TMensaje.Create( WebApplication ) do
     begin
          SetError( sError );
          Show;
     end;
end;

procedure TAddSuscripcion.ShowOneMessage( const sError: String );
begin
     with TMensaje.Create( WebApplication ) do
     begin
          SetMensaje( sError );
          Show;
     end;
end;

procedure TAddSuscripcion.AgregarBTNClick(Sender: TObject);
begin
     FExito := DSuscripciones.dmSuscripciones.SuscripcionAgregar( Direccion, Nombre, Empresa );
     if FExito then
     begin
          with TAddExito.Create( WebApplication ) do
          begin
               SetDireccion( Self.Direccion );
               Show;
          end;
          Release;
     end
     else
         ShowOneError( Format( 'La Suscripción De %s No Pudo Ser Borrada', [ Direccion ] ) );
end;

procedure TAddSuscripcion.RegresarLNKClick(Sender: TObject);
begin
     Release;
end;

end.
