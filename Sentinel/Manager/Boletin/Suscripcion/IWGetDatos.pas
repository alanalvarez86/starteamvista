unit IWGetDatos;
{PUBDIST}

interface

uses Classes, Controls, SysUtils,
     IWAppForm, IWApplication, IWTypes, IWCompButton,
     IWCompEdit, IWControl, IWCompLabel,
     IWHTMLControls, IWLayoutMgr, IWTemplateProcessorHTML;

type
  TGetDireccion = class(TIWAppForm)
    DireccionLBL: TIWLabel;
    BLEMAIL: TIWEdit;
    AgregarBTN: TIWButton;
    BorrarBTN: TIWButton;
    SalirLNK: TIWLink;
    IWTemplate: TIWTemplateProcessorHTML;
    procedure AgregarBTNClick(Sender: TObject);
    procedure BorrarBTNClick(Sender: TObject);
    procedure SalirLNKClick(Sender: TObject);
    procedure IWAppFormDefaultAction(Sender: TObject);
  private
    { Private declarations }
    function GetDireccion: String;
    procedure ClearDireccion;
    procedure ShowOneError(const sError: String);
    procedure ShowOneMessage(const sError: String);
  protected
    { Protected declarations }
    property Direccion: String read GetDireccion;
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

uses ServerController,
     DSuscripciones,
     IWMensaje,
     IWAddDatos,
     IWAddExito;

function TGetDireccion.GetDireccion: String;
begin
     Result := BLEMAIL.Text;
end;

procedure TGetDireccion.ClearDireccion;
begin
     BLEMAIL.Text := '';
end;

procedure TGetDireccion.ShowOneError( const sError: String );
begin
     with TMensaje.Create( WebApplication ) do
     begin
          SetError( sError );
          Show;
     end;
end;

procedure TGetDireccion.ShowOneMessage( const sError: String );
begin
     with TMensaje.Create( WebApplication ) do
     begin
          SetMensaje( sError );
          Show;
     end;
end;

procedure TGetDireccion.AgregarBTNClick(Sender: TObject);
var
   sDireccion: String;
begin
     sDireccion := Direccion;
     if ( Length( sDireccion ) = 0 ) then
        ShowOneError( 'Por Favor Capture Una Direcci�n De Correo Electr�nico V�lida' )
     else
     begin
          with TAddSuscripcion.Create( WebApplication ) do
          begin
               BLEMAIL.Text := sDireccion;
               Show;
          end;
     end;
end;

procedure TGetDireccion.BorrarBTNClick(Sender: TObject);
var
   sDireccion: String;
begin
     sDireccion := Direccion;
     if ( Length( sDireccion ) = 0 ) then
        ShowOneError( 'Por Favor Capture Una Direcci�n De Correo Electr�nico V�lida' )
     else
     begin
          if DSuscripciones.dmSuscripciones.SuscripcionBorrar( sDireccion ) then
          begin
               ShowOneMessage( Format( 'La Suscripci�n De %s Ha Sido Borrada', [ sDireccion ] ) );
               ClearDireccion;
               ActiveControl := BLEMAIL;
          end
          else
              ShowOneError( Format( 'La Suscripci�n De %s No Pudo Ser Borrada ( %s )', [ sDireccion, DSuscripciones.dmSuscripciones.ErrorMsg ] ) );
     end;
end;

procedure TGetDireccion.SalirLNKClick(Sender: TObject);
begin
     WebApplication.Terminate( '' );
end;

procedure TGetDireccion.IWAppFormDefaultAction(Sender: TObject);
begin
     WebApplication.Terminate( '' );
end;

end.
