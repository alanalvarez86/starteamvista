object AddExito: TAddExito
  Left = 0
  Top = 0
  Width = 349
  Height = 296
  Background.Fixed = False
  HandleTabs = False
  StyleSheet.Filename = 'D:\Sentinel\Boletin\Suscripcion\Templates\AddExito.html'
  SupportedBrowsers = [brIE, brNetscape6]
  TemplateProcessor = IWTemplate
  DesignLeft = 475
  DesignTop = 276
  object IWTimer: TIWTimer
    Left = 101
    Top = 32
    Width = 24
    Height = 24
    ZIndex = 0
    Enabled = True
    Interval = 3000
    OnTimer = IWTimerTimer
  end
  object ExitoLBL: TIWLabel
    Left = 36
    Top = 94
    Width = 267
    Height = 24
    ZIndex = 0
    Font.Color = clBlack
    Font.Enabled = True
    Font.Size = 15
    Font.Style = [fsBold]
    Caption = 'gandrade@tress.com.mxs'
  end
  object IWLabel1: TIWLabel
    Left = 52
    Top = 142
    Width = 253
    Height = 24
    ZIndex = 0
    Font.Color = 4227072
    Font.Enabled = True
    Font.Size = 15
    Font.Style = [fsBold]
    Caption = 'Fue Agregada A La Lista'
  end
  object IWLabel2: TIWLabel
    Left = 20
    Top = 174
    Width = 344
    Height = 24
    ZIndex = 0
    Font.Color = 4227072
    Font.Enabled = True
    Font.Size = 15
    Font.Style = [fsBold]
    Caption = 'De Suscriptores Del Bolet�n Tress'
  end
  object IWLabel3: TIWLabel
    Left = 12
    Top = 54
    Width = 364
    Height = 24
    ZIndex = 0
    Font.Color = 4227072
    Font.Enabled = True
    Font.Size = 15
    Font.Style = [fsBold]
    Caption = 'La Direcci�n De Correo Electr�nico'
  end
  object RegresarLNK: TIWLink
    Left = 96
    Top = 223
    Width = 105
    Height = 33
    ZIndex = 0
    Caption = 'Regresar'
    Font.Color = clNone
    Font.Enabled = True
    Font.Size = 15
    Font.Style = []
    ScriptEvents = <>
    DoSubmitValidation = False
    OnClick = RegresarLNKClick
  end
  object IWTemplate: TIWTemplateProcessorHTML
    Enabled = True
    MasterFormTag = True
    TagType = ttIntraWeb
    Left = 64
    Top = 24
  end
end
