object dmSuscripciones: TdmSuscripciones
  OldCreateOrder = False
  Left = 253
  Top = 189
  Height = 479
  Width = 569
  object Hdbc: THdbc
    Connected = True
    Driver = 'INTERSOLV InterBase ODBC Driver (*.gdb)'
    UserName = 'SYSDBA'
    Password = 'm'
    Attributes.Strings = (
      'Database=F-SERVER:D:\Datos\Users\Lety\Sentinel\sentinel.gdb'
      'ApplicationUsingThreads=1'
      'LockTimeOut=0'
      'WorkArounds=1')
    ConnectionPooling = cpDefault
    Version = '5.06'
    Left = 40
    Top = 24
  end
  object OEAgregar: TOEDataSet
    hDbc = Hdbc
    hStmt.CursorType = 0
    hStmt.SkipByPosition = True
    hStmt.SkipByCursor = True
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    SQL = 
      'insert into BOLETIN( BL_EMAIL, BL_NOMBRE, BL_EMPRESA, BL_ACTIVO,' +
      ' '#13#10'BL_ALTA ) values '#13#10'( :BL_EMAIL, :BL_NOMBRE, :BL_EMPRESA, :BL_' +
      'ACTIVO, :BL_ALTA )'#13#10
    Params = <
      item
        DataType = ftUnknown
        Name = 'BL_EMAIL'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'BL_NOMBRE'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'BL_EMPRESA'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'BL_ACTIVO'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'BL_ALTA'
        ParamType = ptInput
      end>
    Cached = True
    Left = 96
    Top = 24
  end
  object OECambiar: TOEDataSet
    hDbc = Hdbc
    hStmt.CursorType = 0
    hStmt.SkipByPosition = True
    hStmt.SkipByCursor = True
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    SQL = 
      'update BOLETIN set'#13#10'BL_NOMBRE = :BL_NOMBRE,'#13#10'BL_EMPRESA = :BL_EM' +
      'PRESA,'#13#10'BL_ACTIVO = :BL_ACTIVO,'#13#10'BL_ALTA = :BL_ALTA'#13#10'where ( UPP' +
      'ER( BL_EMAIL ) = UPPER( :BL_EMAIL) )'#13#10
    Params = <
      item
        DataType = ftUnknown
        Name = 'BL_EMAIL'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'BL_NOMBRE'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'BL_EMPRESA'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'BL_ACTIVO'
        ParamType = ptInput
      end
      item
        DataType = ftUnknown
        Name = 'BL_ALTA'
        ParamType = ptInput
      end>
    Cached = True
    Left = 96
    Top = 72
  end
  object OEBorrar: TOEDataSet
    hDbc = Hdbc
    hStmt.CursorType = 0
    hStmt.SkipByPosition = True
    hStmt.SkipByCursor = True
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    SQL = 
      'delete from BOLETIN '#13#10'where ( UPPER( BL_EMAIL ) =  UPPER( :BL_EM' +
      'AIL ) )'#13#10#13#10
    Params = <
      item
        DataType = ftUnknown
        Name = 'BL_EMAIL'
        ParamType = ptInput
      end>
    Cached = True
    Left = 96
    Top = 120
  end
  object OEExiste: TOEDataSet
    hDbc = Hdbc
    hStmt.CursorType = 0
    hStmt.SkipByPosition = True
    hStmt.SkipByCursor = True
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    SQL = 
      'select COUNT(*) CUANTOS from BOLETIN'#13#10'where ( UPPER( BL_EMAIL ) ' +
      '=  UPPER( :BL_EMAIL ) )'#13#10
    Params = <
      item
        DataType = ftUnknown
        Name = 'BL_EMAIL'
        ParamType = ptInput
      end>
    Cached = True
    Left = 160
    Top = 24
  end
end
