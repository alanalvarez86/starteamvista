program SuscripcionMGR;
{PUBDIST}

uses
  IWInitStandAlone,
  ServerController in 'ServerController.pas' {IWServerController: TDataModule},
  IWGetDatos in 'IWGetDatos.pas' {GetDireccion: TIWForm1},
  DSuscripciones in 'DSuscripciones.pas' {dmSuscripciones: TDataModule},
  IWAddDatos in 'IWAddDatos.pas' {AddSuscripcion: TIWAppForm},
  IWMensaje in 'IWMensaje.pas' {Mensaje: TIWAppForm};

{$R *.res}

begin
  IWRun(TGetDireccion, TIWServerController);
end.
