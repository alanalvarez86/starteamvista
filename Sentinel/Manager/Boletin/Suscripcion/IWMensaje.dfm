object Mensaje: TMensaje
  Left = 0
  Top = 0
  Width = 349
  Height = 296
  Background.Fixed = False
  HandleTabs = False
  StyleSheet.Filename = 'D:\Sentinel\Boletin\Suscripcion\Templates\Mensaje.html'
  SupportedBrowsers = [brIE, brNetscape6]
  TemplateProcessor = IWTemplate
  DesignLeft = 475
  DesignTop = 276
  object IWTimer: TIWTimer
    Left = 101
    Top = 32
    Width = 24
    Height = 24
    ZIndex = 0
    Enabled = True
    Interval = 3000
    OnTimer = IWTimerTimer
  end
  object MensajeLBL: TIWLabel
    Left = 36
    Top = 94
    Width = 267
    Height = 24
    ZIndex = 0
    Font.Color = clBlack
    Font.Enabled = True
    Font.Size = 15
    Font.Style = [fsBold]
    Caption = 'gandrade@tress.com.mxs'
  end
  object RegresarLNK: TIWLink
    Left = 96
    Top = 223
    Width = 105
    Height = 33
    ZIndex = 0
    Caption = 'Regresar'
    Font.Color = clNone
    Font.Enabled = True
    Font.Size = 15
    Font.Style = []
    ScriptEvents = <>
    DoSubmitValidation = False
    OnClick = RegresarLNKClick
  end
  object IWTemplate: TIWTemplateProcessorHTML
    Enabled = True
    MasterFormTag = True
    TagType = ttIntraWeb
    Left = 64
    Top = 24
  end
end
