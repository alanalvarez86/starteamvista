unit DEmails;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Menus, ComCtrls, Grids, Db, ExtCtrls, StdCtrls, Mask,  DBTables, DBGrids,
     ToolWin, Buttons;

const
     K_ALTA = 1;
type
  TEmailCFG = class(TForm)
    MainMenu1: TMainMenu;
    Archivo: TMenuItem;
    ArchivoSalir: TMenuItem;
    sBar: TStatusBar;
    dbBoletin: TDatabase;
    tqBoletin: TQuery;
    dsBoletin: TDataSource;
    DBGrid1: TDBGrid;
    UpdateSQL: TUpdateSQL;
    gbFiltro: TGroupBox;
    rbActivos: TRadioButton;
    rbBajas: TRadioButton;
    rbAmbos: TRadioButton;
    gbActions: TGroupBox;
    BBAgregar: TBitBtn;
    BBBorrar: TBitBtn;
    BBModificar: TBitBtn;
    edEmail: TEdit;
    edNombre: TEdit;
    edEmpresa: TEdit;
    ArchivoImportarAltas: TMenuItem;
    ArchivoImportarBajas: TMenuItem;
    OpenDialog: TOpenDialog;
    tqImpBorrar: TQuery;
    tqImpUpdate: TQuery;
    tqImpAgregar: TQuery;
    tqSender: TQuery;
    tqMailSender: TQuery;
    eMails: TMenuItem;
    eMailsEnviarBoletin: TMenuItem;
    N1: TMenuItem;
    EnviarCorreoAUnaLista1: TMenuItem;
    procedure ArchivoSalirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rbActivosClick(Sender: TObject);
    procedure rbBajasClick(Sender: TObject);
    procedure rbAmbosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tqBoletinUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure tqBoletinNewRecord(DataSet: TDataSet);
    procedure tqBoletinAfterOpen(DataSet: TDataSet);
    procedure edEmailChange(Sender: TObject);
    procedure edNombreChange(Sender: TObject);
    procedure edEmpresaChange(Sender: TObject);
    procedure ArchivoImportarAltasClick(Sender: TObject);
    procedure ArchivoImportarBajasClick(Sender: TObject);
    procedure tqBoletinBeforePost(DataSet: TDataSet);
    procedure eMailsEnviarBoletinClick(Sender: TObject);
    procedure EnviarCorreoAUnaLista1Click(Sender: TObject);
  protected
    { Protected declarations }
  private
    { Private declarations }
    function CargaGrid(const sWhere: string): boolean;
    function EscogeSQL: string;
    function Editar: Boolean;
    function Nuevo: Boolean;
    function Borrar: Boolean;
    procedure BL_ACTIVOOnChange(Sender: TField);
    procedure Llena;
    function GetImportFileName(var sFileName: String): Boolean;
    procedure Importar(const lAltas: Boolean);
  public
    { Public declarations }
  end;

var
  EmailCFG: TEmailCFG;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     FSendBulletin,
     FEditRecord;

const
     K_WHERE_ACTIVO = 'WHERE BL_ACTIVO <> %d';
     K_WHERE_BAJA = 'WHERE BL_ACTIVO = %d';
     K_BAJA = 0;
     K_PROCENTAJE = '%';
     K_ESPACIO = ' ';

{$R *.DFM}

procedure TEmailCFG.FormCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     sBar.Panels[ 0 ].Text := DateToStr( Date );
     sBar.panels[ 2 ].Text := TimeToStr( Time );
end;

procedure TEmailCFG.FormShow(Sender: TObject);
var
   sSQLWhere : string;
begin
     dbBoletin.Connected := True;
     sSQLWhere := EscogeSQL;
     if not( CargaGrid( sSQLWhere ) ) then
        ZetaDialogo.ZError( 'Emails Configuration', 'Error en Base de Datos', 0 );
end;

function TEmailCFG.EscogeSQL: string;
begin
     Result := VACIO;
     if ( rbAmbos.Checked ) then
        Result := VACIO
     else
     begin
         if ( rbActivos.Checked ) then
            Result := Format( K_WHERE_ACTIVO, [ K_BAJA ] )
         else
             Result := Format( K_WHERE_BAJA, [ K_BAJA ] );
     end;
end;

procedure TEmailCFG.ArchivoSalirClick(Sender: TObject);
begin
     Close;
end;

function TEmailCFG.CargaGrid(const sWhere : string ) : boolean;
const
     K_QUERY = 'select BL_EMAIL, BL_NOMBRE, BL_ALTA, BL_ACTIVO, BL_EMPRESA, BL_BAJA from BOLETIN %s order by BL_EMAIL';
begin
     with tqBoletin do
     begin
          Close;
          with SQL do
          begin
               Clear;
               Add( Format( K_QUERY, [ sWhere ] ) );
          end;
          Active := True;
          Result := Active;
          sBar.Panels[ 1 ].Text := 'Registros: ' + IntToStr( RecordCount );
     end;
end;

procedure TEmailCFG.rbActivosClick(Sender: TObject);
begin
     if rbActivos.Checked then
     begin
          if Not( CargaGrid( Format( K_WHERE_ACTIVO, [ K_BAJA ] ) ) ) then
             ZetaDialogo.ZError( 'Emails Configuration', 'Error en Base de Datos', 0 );
     end;
end;

procedure TEmailCFG.rbBajasClick(Sender: TObject);
begin
     if rbBajas.Checked then
     begin
          if Not( CargaGrid( Format( K_WHERE_BAJA, [ K_BAJA ] ) ) ) then
             ZetaDialogo.ZError( 'Emails Configuration', 'Error en Base de Datos', 0 );
     end;
end;

procedure TEmailCFG.rbAmbosClick(Sender: TObject);
begin
     if rbAmbos.Checked then
     begin
          if Not( CargaGrid( VACIO ) ) then
             ZetaDialogo.ZError( 'Emails Configuration', 'Error en Base de Datos', 0 );
     end;
end;

function TEmailCFG.Nuevo: Boolean;
begin
     Result := False;
     with tqBoletin do
     begin
          Insert;
          with TEditRecord.Create( Self ) do
          begin
               try
                  BL_ACTIVO.Checked := True;
                  with BL_ALTA do
                  begin
                       Enabled := True;
                       Valor := Now;
                  end;
                  with BL_BAJA do
                  begin
                       Valor := BL_ALTA.Valor;
                       Enabled := False;
                  end;
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       Post;
                       Result := True;
                  end
                  else
                  begin
                       Cancel;
                  end;
               finally
                      Free;
               end;
          end;
     end;
end;

function TEmailCFG.Borrar: Boolean;
begin
     Result := False;
     with tqBoletin do
     begin
          Edit;
          with TEditRecord.Create( Self ) do
          begin
               try
                  BL_EMAIL.Enabled := False;
                  BL_NOMBRE.Enabled := False;
                  BL_EMPRESA.Enabled := False;
                  BL_ALTA.Enabled := False;
                  BL_BAJA.Enabled := False;
                  BL_ACTIVO.Enabled := False;
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       Delete;
                       Result := True;
                  end
                  else
                  begin
                       Cancel;
                  end;
               finally
                      Free;
               end;
          end;
     end;
end;

function TEmailCFG.Editar: Boolean;
begin
     Result := False;
     with tqBoletin do
     begin
          Edit;
          with TEditRecord.Create( Self ) do
          begin
               try
                  BL_ALTA.Enabled := False;
                  BL_BAJA.Enabled := False;
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       Post;
                       Result := True;
                  end
                  else
                  begin
                       Cancel;
                  end;
               finally
                      Free;
               end;
          end;
     end;
end;

procedure TEmailCFG.tqBoletinUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
begin
     case UpdateKind of
          ukModify:
          begin
               with UpdateSQL do
               begin
                    SetParams( ukModify );
                    Apply( ukModify );
               end;
               UpdateAction := uaApplied;
          end;
          ukInsert:
          begin
               with UpdateSQL do
               begin
                    SetParams( ukInsert );
                    Apply( ukInsert );
               end;
               UpdateAction := uaApplied;
          end;
          ukDelete:
          begin
               with UpdateSQL do
               begin
                    SetParams( ukDelete );
                    Apply( ukDelete );
               end;
               UpdateAction := uaApplied;
          end;
     end;
end;

procedure TEmailCFG.DBGrid1DblClick(Sender: TObject);
begin
     if Editar then
     begin
          dbBoletin.ApplyUpdates( [ tqBoletin ] );
     end;
end;

procedure TEmailCFG.FormDestroy(Sender: TObject);
begin
     dbBoletin.Connected := False;
end;

procedure TEmailCFG.Llena;
begin
     CargaGrid( VACIO );
     rbAmbos.Checked := True;
end;

procedure TEmailCFG.BBAgregarClick(Sender: TObject);
begin
     if Nuevo then
     begin
          dbBoletin.ApplyUpdates( [ tqBoletin ] );
          Llena;
     end;
end;

procedure TEmailCFG.BBBorrarClick(Sender: TObject);
begin
     if Borrar then
     begin
          dbBoletin.ApplyUpdates( [ tqBoletin ] );
          Llena;
     end;
end;

procedure TEmailCFG.BBModificarClick(Sender: TObject);
begin
     if Editar then
     begin
          dbBoletin.ApplyUpdates( [ tqBoletin ] );
     end;
end;

procedure TEmailCFG.tqBoletinNewRecord(DataSet: TDataSet);
begin
     With DataSet do
     Begin
          FieldByName( 'BL_ACTIVO' ).AsInteger := K_ALTA;
          FieldByName( 'BL_ALTA' ).AsDateTime := Trunc( Now );
     end;
end;

procedure TEmailCFG.BL_ACTIVOOnChange( Sender: TField );
begin
     if Assigned( Sender ) then
     begin
          Case Sender.ASInteger of
               0: Sender.DataSet.FieldByName( 'BL_BAJA' ).ASDateTime := Trunc( Now );
               1: Sender.DataSet.FieldByName( 'BL_ALTA' ).ASDateTime := Trunc( Now );
          end;
     end;
end;

procedure TEmailCFG.tqBoletinAfterOpen(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'BL_ACTIVO' ).OnChange := BL_ACTIVOOnChange;
     end;
end;

procedure TEmailCFG.tqBoletinBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          with FieldByName( 'BL_EMAIL' ) do
          begin
               AsString := AnsiLowerCase( AsString );
          end;
     end;
end;

procedure TEmailCFG.edEmailChange(Sender: TObject);
var
   sWhere: string;
begin
     sWhere := 'where UPPER( BL_EMAIL ) like ' + EntreComillas( K_PROCENTAJE + AnsiUpperCase( Trim( edEmail.Text ) ) + K_PROCENTAJE ) + K_ESPACIO;
     CargaGrid( sWhere );
end;

procedure TEmailCFG.edNombreChange(Sender: TObject);
var
   sWhere: string;
begin
     sWhere := 'where UPPER( BL_NOMBRE ) like ' + EntreComillas( K_PROCENTAJE + AnsiUpperCase( Trim( edNombre.Text ) ) + K_PROCENTAJE ) + K_ESPACIO;
     CargaGrid( sWhere );
end;

procedure TEmailCFG.edEmpresaChange(Sender: TObject);
var
   sWhere: string;
begin
     sWhere := 'where UPPER( BL_EMPRESA ) like ' + EntreComillas( K_PROCENTAJE + AnsiUpperCase( Trim( edEmpresa.Text ) ) + K_PROCENTAJE ) + K_ESPACIO;
     CargaGrid( sWhere );
end;

function TEmailCFG.GetImportFileName( var sFileName: String ): Boolean;
begin
     with OpenDialog do
     begin
          InitialDir := ExtractFilePath( sFileName );
          FileName := sFileName;
          if Execute then
          begin
               sFileName := FileName;
               Result := True;
          end
          else
              Result := False;
     end;
end;

procedure TEmailCFG.Importar( const lAltas: Boolean );
var
   sFileName: String;
   FDatos: TStrings;
   oCursor: TCursor;
   i: Integer;

procedure ImportaAlta( const sEMail: String );
var
   lFound: Boolean;
begin
     with dbBoletin do
     begin
          StartTransaction;
          try
             with tqImpUpdate do
             begin
                  ParamByName( 'BL_ACTIVO' ).AsInteger := K_ALTA;
                  ParamByName( 'BL_BAJA' ).AsDate := NullDateTime;
                  ParamByName( 'BL_ALTA' ).AsDate := Now;
                  ParamByName( 'BL_EMAIL' ).AsString := sEmail;
                  ExecSQL;
                  lFound := ( RowsAffected > 0 );
             end;
             if not lFound then
             begin
                  with tqImpAgregar do
                  begin
                       ParamByName( 'BL_ACTIVO' ).AsInteger := K_ALTA;
                       ParamByName( 'BL_ALTA' ).AsDate := Now;
                       ParamByName( 'BL_EMAIL' ).AsString := AnsiLowerCase( sEmail );
                       ExecSQL;
                  end;
             end;
             Commit;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

procedure ImportaBaja( const sEMail: String );
begin
     with dbBoletin do
     begin
          StartTransaction;
          try
             with tqImpBorrar do
             begin
                  ParamByName( 'BL_ACTIVO' ).AsInteger := K_BAJA;
                  ParamByName( 'BL_BAJA' ).AsDate := Now;
                  ParamByName( 'BL_EMAIL' ).AsString := sEmail;
                  ExecSQL;
             end;
             Commit;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

begin
     if lAltas then
        sFileName := Format( '%sAltas.txt', [ ExtractFilePath( Application.ExeName ) ] )
     else
         sFileName := Format( '%sBajas.txt', [ ExtractFilePath( Application.ExeName ) ] );
     if GetImportFileName( sFileName ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             FDatos := TStringList.Create;
             try
                with FDatos do
                begin
                     LoadFromFile( sFileName );
                     for i := 0 to ( Count - 1 ) do
                     begin
                          if lAltas then
                             ImportaAlta( Strings[ i ] )
                          else
                              ImportaBaja( Strings[ i ] );
                     end;
                end;
             finally
                    FreeAndNil( FDatos );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TEmailCFG.ArchivoImportarAltasClick(Sender: TObject);
begin
     Importar( True );
end;

procedure TEmailCFG.ArchivoImportarBajasClick(Sender: TObject);
{$ifdef FALSE}
const
     Q_INSERT = 'insert into BOLETIN( BL_EMAIL, BL_NOMBRE, BL_EMPRESA, BL_ACTIVO, BL_ALTA, BL_BAJA ) values '+
                '( ''%s'', ''%s'', ''%s'', %d, ''%s'', ''%s'' );';
var
   FLista: TStrings;
{$endif}
begin
     Importar( False );
     {$ifdef FALSE}
     CargaGrid( VACIO );
     FLista := TStringList.Create;
     try
        FLista.Add( 'connect "F-SERVER:D:\Datos\Users\Lety\Sentinel\sentinel.gdb" user "SYSDBA" password "m";' );
        with tqBoletin do
        begin
             DisableControls;
             First;
             while not Eof do
             begin
                  FLista.Add( Format( Q_INSERT, [ AnsiLowerCase( FieldByName( 'BL_EMAIL' ).AsString ),
                                                  FieldByName( 'BL_NOMBRE' ).AsString,
                                                  FieldByName( 'BL_EMPRESA' ).AsString,
                                                  FieldByName( 'BL_ACTIVO' ).AsInteger,
                                                  FormatDateTime( 'mm/dd/yyyy', FieldByName( 'BL_ALTA' ).AsDateTime ),
                                                  FormatDateTime( 'mm/dd/yyyy', FieldByName( 'BL_BAJA' ).AsDateTime ) ] ) );
                  Next;
             end;
             EnableControls;
             First;
        end;
        FLista.SaveToFile( 'D:\Sentinel\Boletin\agrega.sql' );
     finally
            FreeAndNil( FLista );
     end;
     {$endif}
end;

procedure TEmailCFG.eMailsEnviarBoletinClick(Sender: TObject);
var
   BulletinSender: TBulletinSender;
begin
     BulletinSender := TBulletinSender.Create( Self );
     try
        with BulletinSender do
        begin
             ShowModal;
        end;
     finally
            FreeAndNil( BulletinSender );
     end;
end;

procedure TEmailCFG.EnviarCorreoAUnaLista1Click(Sender: TObject);
var
   BulletinSender: TBulletinSender;
begin
     BulletinSender := TBulletinSender.Create( Self );
     try
        with BulletinSender do
        begin
             UsarLista := True;
             ShowModal;
        end;
     finally
            FreeAndNil( BulletinSender );
     end;
end;

end.
