�
 TFORMSISTEMA 0�  TPF0TFormSistemaFormSistemaLeft9Top ActiveControlPageControlBorderStylebsDialogCaptionUsuario de Sistema TressClientHeight�ClientWidthQColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPageControlPageControlLeft Top WidthQHeight�
ActivePagetabSentinelAlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnChangePageControlChange 	TTabSheet	GeneralesCaption	GeneralesFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont TLabelSI_COMENTAlblLeftTopuWidth=Height	AlignmenttaRightJustifyCaptionComentarios:  TDBMemo
SI_COMENTALeft]ToprWidth�Height� 	DataField
SI_COMENTA
DataSource	dsSistemaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont
ScrollBars
ssVerticalTabOrderOnExitSI_COMENTAExit  	TGroupBox
GerenciaGBLeft Top� WidthIHeight� AlignalTopCaption
 Gerencia Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelSI_GTE_RHlblLeftTopWidthBHeight	AlignmenttaRightJustifyCaptionGerente R.H.:  TLabelSI_GTE_CONlblLeftTop=WidthNHeight	AlignmenttaRightJustifyCaptionGerente Contab:  TLabelSI_GTE_SISlblLeftToplWidthVHeight	AlignmenttaRightJustifyCaptionGerente Sistemas:  TLabelSI_EMA_SISlblLeft;Top� Width Height	AlignmenttaRightJustifyCaptionE-Mail:  TLabelSI_EMA_CONlblLeft>TopTWidthHeight	AlignmenttaRightJustifyCaptionEMail:  TLabelSI_EMA_RHlblLeft;Top%Width Height	AlignmenttaRightJustifyCaptionE-Mail:  TDBEdit	SI_GTE_RHLeft]TopWidth�Height	DataField	SI_GTE_RH
DataSource	dsSistemaTabOrder   TDBEdit
SI_GTE_CONLeft]Top9Width�Height	DataField
SI_GTE_CON
DataSource	dsSistemaTabOrder  TDBEdit
SI_GTE_SISLeft]TophWidth�Height	DataField
SI_GTE_SIS
DataSource	dsSistemaTabOrder  TDBEdit
SI_EMA_SISLeft]Top~Width�Height	DataField
SI_EMA_SIS
DataSource	dsSistemaTabOrder  TDBEdit
SI_EMA_CONLeft]TopPWidth�Height	DataField
SI_EMA_CON
DataSource	dsSistemaTabOrder  TDBEdit	SI_EMA_RHLeft]Top!Width�Height	DataField	SI_EMA_RH
DataSource	dsSistemaTabOrder   TDBCheckBoxSI_PROBLEMALeft Top_WidthjHeight	AlignmenttaLeftJustifyCaption   ¿ Con Problemas ?	DataFieldSI_PROBLEMA
DataSource	dsSistemaTabOrderValueChecked1ValueUnchecked0OnClickSI_PROBLEMAClick  	TGroupBoxIdentificacionGBLeft Top WidthIHeight� AlignalTopCaption    Identificación TabOrder  TLabelSI_FOLIOlblLeftBTopWidthHeight	AlignmenttaRightJustifyCaptionFolio:  TLabelSI_RAZ_SOClblLeftTop'WidthBHeight	AlignmenttaRightJustifyCaption   Razón Social:  TLabelGR_CODIGOlblLeft;Top=Width Height	AlignmenttaRightJustifyCaptionGrupo:  TLabelSI_PERSONAlblLeft-TopiWidth.Height	AlignmenttaRightJustifyCaption	Contacto:  TLabelGI_CODIGOlblLeftCTopWidthHeight	AlignmenttaRightJustifyCaptionTipo:  TLabel
SI_GIROlblLeftETop� WidthHeight	AlignmenttaRightJustifyCaptionGiro:  TLabelDI_CODIGOlblLeft$TopSWidth7Height	AlignmenttaRightJustifyCaptionDistribuidor:  TLabelLabel1Left/Top� Width,Height	AlignmenttaRightJustifyCaption	Sucursal:  TDBEditSI_FOLIOLeft]TopWidth9HeightTabStop	DataFieldSI_FOLIO
DataSource	dsSistemaEnabledTabOrder   TDBEdit
SI_RAZ_SOCLeft]Top#Width�Height	DataField
SI_RAZ_SOC
DataSource	dsSistemaTabOrder  TDBLookupComboBox	GR_CODIGOLeft]Top9WidthHeight	DataField	GR_CODIGO
DataSource	dsSistemaKeyField	GR_CODIGO	ListField
GR_DESCRIP
ListSourcedsGrupoTabOrder  TDBEdit
SI_PERSONALeft]TopeWidthHeight	DataField
SI_PERSONA
DataSource	dsSistemaTabOrder  TDBLookupComboBox	GI_CODIGOLeft]Top{WidthHeight	DataField	GI_CODIGO
DataSource	dsSistemaKeyField	GI_CODIGO	ListField
GI_DESCRIP
ListSourcedsGiroTabOrder  TDBEditSI_GIROLeft]Top� WidthHeight	DataFieldSI_GIRO
DataSource	dsSistemaTabOrder  TDBLookupComboBox	DI_CODIGOLeft]TopOWidthHeight	DataField	DI_CODIGO
DataSource	dsSistemaKeyField	DI_CODIGO	ListField	DI_NOMBRE
ListSourcedsDistribuidorTabOrder  	TComboBoxLIST_SUCURSALESLeft]Top� WidthHeight
ItemHeightTabOrderItems.StringsTijuanaDistribuidoresMexicali	MonterreyDistrito FederalSonora   Juárez	QueretaroReynosaCotemarOtros   Torreón     	TTabSheet	DireccionCaption
   Dirección 	TGroupBox
TelefonoGBLeft TopaWidthIHeightxAlignalTopCaption    Teléfonos / Fax Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabel
SI_TEL1lblLeftTopWidth6Height	AlignmenttaRightJustifyCaption   Teléfono 1:  TLabel
SI_TEL2lblLeftTop.Width6Height	AlignmenttaRightJustifyCaption   Teléfono 2:  TLabel	SI_FAXlblLeft7TopCWidthHeight	AlignmenttaRightJustifyCaptionFax:  TLabelSI_EMAILlblLeft+Top[Width Height	AlignmenttaRightJustifyCaptionE-Mail:  TDBEditSI_TEL1LeftMTopWidth� Height	DataFieldSI_TEL1
DataSource	dsSistemaTabOrder   TDBEditSI_TEL2LeftMTop*Width� Height	DataFieldSI_TEL2
DataSource	dsSistemaTabOrder  TDBEditSI_FAXLeftMTopAWidth� Height	DataFieldSI_FAX
DataSource	dsSistemaTabOrder  TDBEditSI_EMAILLeftMTopXWidthHeight	DataFieldSI_EMAIL
DataSource	dsSistemaTabOrder   	TGroupBoxDomicilioGBLeft Top WidthIHeightaAlignalTopCaption Domicilio Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelSI_CALLElblLeft1TopWidthHeight	AlignmenttaRightJustifyCaptionCalle:  TLabelSI_COLONIAlblLeft%Top,Width&Height	AlignmenttaRightJustifyCaptionColonia:  TLabelCT_CODIGOlblLeft'TopDWidth$Height	AlignmenttaRightJustifyCaptionCiudad:  TDBEditSI_CALLELeftNTopWidth�Height	DataFieldSI_CALLE
DataSource	dsSistemaTabOrder   TDBEdit
SI_COLONIALeftNTop(Width�Height	DataField
SI_COLONIA
DataSource	dsSistemaTabOrder  TDBLookupComboBox	CT_CODIGOLeftNTop@Width� Height	DataField	CT_CODIGO
DataSource	dsSistemaKeyField	CT_CODIGO	ListField
CT_DESCRIP
ListSourcedsCiudadTabOrder  TBitBtnCiudadesLeftTop>WidthZHeightHintAccesar CiudadesCaption	&CiudadesParentShowHintShowHint	TabOrderOnClickCiudadesClick
Glyph.Data
j  f  BMf      v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� UUUUUUUUUU  Uwwwwwwwwu  P        u  P�������u  P���DOD�u  P�������u  P���DOD�u  P�������u  P���DOD�u  P��� x��u  P���  xD�u  P���p x�u  P���@�� �u  P������ u  P    ��0  P����p�C   P      ��0  UUUUUU��C  UUUUUUP���  UUUUUUU��     	TGroupBoxDatosGBLeft Top� WidthIHeight�AlignalClientCaption    Datos Estadísticos Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelPA_CODIGOlblLeft	TopWidthAHeight	AlignmenttaRightJustifyCaptionNacionalidad:  TLabelSI_FEC_SOLlblLeftTopAWidth=Height	AlignmenttaRightJustifyCaptionSolicitado El:  TLabelSI_EMP_EMPlblLeftTopXWidth7Height	AlignmenttaRightJustifyCaption
Empleados:  TLabelSI_SIS_VIElblLeftTopoWidthBHeight	AlignmenttaRightJustifyCaptionSistema Viejo:  TDBLookupComboBox	PA_CODIGOLeftMTopWidth� Height	DataField	PA_CODIGO
DataSource	dsSistemaKeyField	PA_CODIGO	ListField
PA_DESCRIP
ListSourcedsPaisTabOrder   TDBCheckBox
SI_MIGRADOLeftTop(WidthBHeight	AlignmenttaLeftJustifyCaption   Migración:	DataField
SI_MIGRADO
DataSource	dsSistemaTabOrderValueChecked1ValueUnchecked0  TZetaDBFecha
SI_FEC_SOLLeftMTop<WidthsHeightCursorcrArrowTabOrderText	18/Apr/00Valor      �@	DataField
SI_FEC_SOL
DataSource	dsSistema  TZetaDBNumero
SI_EMP_EMPLeftMTopTWidthHHeightMascaramnDiasTabOrderText0	DataField
SI_EMP_EMP
DataSource	dsSistema  TDBEdit
SI_SIS_VIELeftMTopkWidthHeight	DataField
SI_SIS_VIE
DataSource	dsSistemaTabOrderOnExitSI_SIS_VIEExit    	TTabSheetAutorizacionCaptionAutorizaciones TPageControlPageControl1Left Top!WidthIHeightW
ActivePageGeneraAutorizacionAlignalClientTabOrder  	TTabSheetGeneraAutorizacionCaption   Generar AutorizaciónFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont 	TGroupBoxNotaGBLeft Top�WidthAHeighttAlignalBottomCaption     Notas Sobre Esta Autorización Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBMemo
AU_COMENTALeftTopWidth=HeightcAlignalClient	DataField
AU_COMENTA
DataSourcedsActual
ScrollBars
ssVerticalTabOrder    TPanelPanel9Left Top� WidthAHeightAlignalClient
BevelInner	bvLoweredFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder 	TGroupBoxPrestamosGBLeft�TopWidthdHeightAlignalRightCaption    Módulos Prestados Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBCheckBoxAU_PRE_1LeftTopWidth� HeightCaption   Nómina:	DataFieldAU_PRE_1
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_2LeftTop,Width� HeightCaptionIMSS / INFONAVIT:	DataFieldAU_PRE_2
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_3LeftTop<Width� HeightCaptionAsistencia:	DataFieldAU_PRE_3
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_4LeftTopLWidth� HeightCaptionCursos:	DataFieldAU_PRE_4
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_5LeftTop\Width� HeightCaptionSupervisores:	DataFieldAU_PRE_5
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_6LeftToplWidth� HeightCaption   Cafetería:	DataFieldAU_PRE_6
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_0LeftTopWidth� HeightCaptionRecursos Humanos:	DataFieldAU_PRE_0
DataSourcedsActualTabOrder ValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_9LeftTop� Width� HeightCaptionTress Analiza:	DataFieldAU_PRE_9
DataSourcedsActualTabOrder	ValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_8LeftTop� Width� HeightCaptionKiosko:	DataFieldAU_PRE_8
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBoxAU_PRE_7LeftTop|Width� HeightCaptionLabor:	DataFieldAU_PRE_7
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_10LeftTop� Width� HeightCaption
   Gráficas:	DataField	AU_PRE_10
DataSourcedsActualTabOrder
ValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_11LeftTop� Width� HeightCaptionReportes x E-Mail:	DataField	AU_PRE_11
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_12Left� Top	Width� HeightCaption
Resguardo:	DataField	AU_PRE_12
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_13Left� TopWidth� HeightCaptionL5Poll:	DataField	AU_PRE_13
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_14Left� Top)Width� HeightCaption   Selección:	DataField	AU_PRE_14
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_15Left� Top9Width� HeightCaption   Servicio Médico:	DataField	AU_PRE_15
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_16Left� TopIWidth� HeightCaptionMonitor:	DataField	AU_PRE_16
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_17Left� TopYWidth� HeightCaptionPortal:	DataField	AU_PRE_17
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_18Left� TopiWidth� HeightCaptionPlan de Carrera:	DataField	AU_PRE_18
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_19Left� TopyWidth� HeightCaptionAccesos:	DataField	AU_PRE_19
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_20Left� Top� Width� HeightCaptionVisitantes:	DataField	AU_PRE_20
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TPanelPanelVencimientoPrestamoLeftTop� Width`HeightAlignalBottom
BevelOuterbvNoneTabOrder TLabelAU_FEC_PRElblLeftTopWidth=Height	AlignmenttaRightJustifyCaptionVencimiento:  TZetaDBFecha
AU_FEC_PRELeftOTopWidthhHeightCursorcrArrowTabOrder Text	16/Mar/00UseEnterKey	Valor      ��@	DataField
AU_FEC_PRE
DataSourcedsActual   TDBCheckBox	AU_PRE_21Left� Top� Width� HeightCaption
Evaluacion	DataField	AU_PRE_21
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_22Left� Top� Width� HeightCaptionWorkflow	DataField	AU_PRE_22
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0OnClickAU_PRE_1Click  TDBCheckBox	AU_PRE_23Left� Top� Width� HeightCaption   TRESS En Línea	DataField	AU_PRE_23
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0   	TGroupBox	ModulosGBLeftTopWidthaHeightAlignalLeftCaption    Módulos Autorizados Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TDBCheckBoxAU_MOD_0Left
TopWidth� HeightCaption
Empleados:	DataFieldAU_MOD_0
DataSourcedsActualReadOnly	TabOrder ValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_1Left
TopWidth� HeightCaption   Nómina:	DataFieldAU_MOD_1
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_2Left
Top,Width� HeightCaptionIMSS / INFONAVIT:	DataFieldAU_MOD_2
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_3Left
Top<Width� HeightCaptionAsistencia:	DataFieldAU_MOD_3
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_4Left
TopLWidth� HeightCaptionCursos:	DataFieldAU_MOD_4
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_5Left
Top[Width� HeightCaptionSupervisores:	DataFieldAU_MOD_5
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_6Left
TopkWidth� HeightCaption   Cafetería:	DataFieldAU_MOD_6
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_9Left
Top� Width� HeightCaptionTress Analiza:	DataFieldAU_MOD_9
DataSourcedsActualTabOrder	ValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_7Left
Top{Width� HeightCaptionLabor:	DataFieldAU_MOD_7
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_8Left
Top� Width� HeightCaptionKiosko:	DataFieldAU_MOD_8
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_10Left
Top� Width� HeightCaption
   Gráficas:	DataField	AU_MOD_10
DataSourcedsActualTabOrder
ValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_11Left
Top� Width� HeightCaptionReportes x E-Mail:	DataField	AU_MOD_11
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_12Left� Top	Width� HeightCaption
Resguardo:	DataField	AU_MOD_12
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_13Left� TopWidth� HeightCaptionL5Poll:	DataField	AU_MOD_13
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_14Left� Top)Width� HeightCaption   Selección:	DataField	AU_MOD_14
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_15Left� Top9Width� HeightCaption   Servicio Médico:	DataField	AU_MOD_15
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_16Left� TopIWidth� HeightCaptionMonitor:	DataField	AU_MOD_16
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_17Left� TopYWidth� HeightCaptionPortal:	DataField	AU_MOD_17
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_18Left� TopiWidth� HeightCaptionPlan de Carrera:	DataField	AU_MOD_18
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_19Left� TopyWidth� HeightCaptionAccesos:	DataField	AU_MOD_19
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_20Left� Top� Width� HeightCaptionVisitantes:	DataField	AU_MOD_20
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TPanelPanelVencimientoAutorizacionLeftTop� Width]HeightAlignalBottom
BevelOuterbvNoneTabOrder TLabelAU_DEFINITlblLeftTopWidthHeight	AlignmenttaRightJustifyCaptionClave:  TLabelAU_FEC_AUTlblLeft� TopWidth=Height	AlignmenttaRightJustifyCaptionVencimiento:  TZetaDBFecha
AU_FEC_AUTLeft� TopWidthhHeightCursorcrArrowTabOrderText	16/Mar/00Valor      ��@	DataField
AU_FEC_AUT
DataSourcedsActual  TZetaDBKeyCombo
AU_DEFINITLeft0TopWidthhHeightAutoComplete	BevelKindbkFlatStylecsDropDownListCtl3D
ItemHeightParentCtl3DTabOrder OnChangeAU_DEFINITChangeItems.StringsTemporal
Definitiva 	ListaFija	lfNingunaListaVariablelvPuestoOffset OpcionalEsconderVacios	DataField
AU_DEFINIT
DataSourcedsActualLlaveNumerica	   TDBCheckBox	AU_MOD_21Left� Top� Width� HeightCaption
Evaluacion	DataField	AU_MOD_21
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_22Left� Top� Width� HeightCaptionWorkflow	DataField	AU_MOD_22
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_23Left� Top� Width� HeightCaption   TRESS En Línea	DataField	AU_MOD_23
DataSourcedsActualTabOrderValueChecked1ValueUnchecked0    	TGroupBox	SistemaGBLeft TopWidthAHeightxAlignalTopCaption	 Sistema Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelAU_NUM_EMPlblLeftTopWidth7Height	AlignmenttaRightJustifyCaption
Empleados:  TLabelAU_USERSlblLeft%Top(Width,Height	AlignmenttaRightJustifyCaption	Usuarios:  TLabelVE_CODIGOlblLeftRTopWidth&Height	AlignmenttaRightJustifyCaption	   Versión:  TLabelAU_PLATFORMlblLeftCTop(Width5Height	AlignmenttaRightJustifyCaptionPlataforma:  TDBComboBox
AU_NUM_EMPLeftSTopWidthnHeight	DataField
AU_NUM_EMP
DataSourcedsActual
ItemHeightItems.Strings10030060015002750400060008000150001000000 TabOrder   TDBLookupComboBox	VE_CODIGOLeftzTopWidthnHeight	DataField	VE_CODIGO
DataSourcedsActualKeyField	VE_CODIGO	ListField
VE_DESCRIP
ListSource	dsVersionTabOrderOnClickVE_CODIGOClick  TDBRadioGroup	AU_SQL_BDLeft�Top
Width� Height9Caption Base de Datos 	DataField	AU_SQL_BD
DataSourcedsActualTabOrder  TZetaDBKeyComboAU_PLATFORMLeftzTop$WidthnHeightAutoComplete	BevelKindbkFlatStylecsDropDownListCtl3D
ItemHeightParentCtl3DTabOrder	ListaFija	lfNingunaListaVariablelvPuestoOffset OpcionalEsconderVacios	DataFieldAU_PLATFORM
DataSourcedsActualLlaveNumerica	  TZetaDBNumeroAU_USERSLeftSTop$WidthnHeightMascaramnDiasTabOrderText0	DataFieldAU_USERS
DataSourcedsActual  	TGroupBox	GroupBox2LeftTopDWidth�Height/Caption    Límites de Licencia Global: TabOrder TLabelLabel3LeftTopWidth� Height	AlignmenttaRightJustifyCaption   Fecha de Inicio de Validación:  TLabelLabel4LeftGTopWidth� Height	AlignmenttaRightJustifyCaption%   Límite de Config. de BD Producción:  TZetaDBKeyCombo
AU_VAL_LIMLeft� TopWidth� HeightAutoComplete	BevelKindbkFlatStylecsDropDownListCtl3D
ItemHeightParentCtl3DTabOrder 	ListaFija	lfNingunaListaVariablelvPuestoOffset OpcionalEsconderVacios	DataField
AU_VAL_LIM
DataSourcedsActualLlaveNumerica	  TZetaDBKeyCombo	AU_BD_LIMLeft�TopWidth� HeightAutoComplete	BevelKindbkFlatStylecsDropDownListCtl3D
ItemHeightParentCtl3DTabOrder	ListaFija	lfNingunaListaVariablelvPuestoOffset OpcionalEsconderVacios	DataField	AU_BD_LIM
DataSourcedsActualLlaveNumerica	    TPanelPanelSuperiorLeft Top WidthAHeightAlignalTop
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelKitWarning1LeftrTopWidth�HeightCaptionC   Se Autorizan Todos Los Módulos - Vecimiento Automático en 6 MesesColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold ParentColor
ParentFont  TDBCheckBox
AU_KIT_DISLeftTopWidthhHeightTabStopCaptionKit de Distribuidor	DataField
AU_KIT_DIS
DataSourcedsActualTabOrder ValueChecked1ValueUnchecked0OnClickAU_KIT_DISClick   TPanelPanel11Left TopWidthAHeight)AlignalBottom
BevelOuterbvNoneTabOrder TBitBtnbtnGeneraAutorizacionLeft4TopWidth� Height!Caption   Generar AutorizaciónTabOrder OnClickbtnGeneraAutorizacionClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333�33;3�33337;33;�33�7�3733;�3;�3;�7�737s3�w{�w�33w���w33     �33wwwwws33����333?����33 � �33?w7w7�������w?3��w;p   7s�wwww3����337s33733���33s���330    �337wwww�3�3;�3�33w373w�;�3;�3;�7s3737;33;�33�7337s33s�33;333;s3373337	NumGlyphs    	TTabSheet	TabSheet3Caption
TerminalesFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ImageIndex
ParentFont TPanelPanel3Left TopWidthAHeight(AlignalBottomTabOrder  TPanelPanel4LeftTopWidth?Height#AlignalTopParentShowHintShowHint	TabOrder 
DesignSize?#  TDBNavigatordbNavegadorLeftTopWidth Height
DataSourcedsTerminalesVisibleButtonsnbFirstnbPriornbNextnbLastnbInsertnbDeletenbEdit	nbRefresh Flat	Hints.StringsPrimeroAnterior	SiguienteUltimoAgregarBorrar	ModificarAceptarCancelar	Refrescar ParentShowHintShowHint	TabOrder   TBitBtnbtnGeneraArchivoLeftyTopWidth� HeightHint Se genera las terminales ActivasAnchorsakRightakBottom CaptionGenerar Archivo TerminalesParentShowHintShowHint	TabOrderOnClickbtnGeneraArchivoClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333�333333�3333333w�333333�3?����w�   ����3wwwwwww��𙙙�37wwww�𙙙��?�wwwww 𙙙�w7wwwws�𙙙�3?�wwww3 ���3w337ws3����33?���w33 �  33w7wws33���333?��s333��3333w7�3333��3333��s3333   33333www333333333333333333333	NumGlyphs   	TGroupBox	GroupBox1Left Top-WidthyHeight� Caption	 Detalle TabOrder TLabelLblTE_NUMEROLeft<Top|Width(HeightCaption   Número:  TLabelLblTE_TEXTOLeft'TopPWidth=HeightCaptionComentarios:  TLabelLblTE_NSERIELeftTop&WidthRHeightCaption   Número de Serie:  TLabellblTE_DESCRIPLeft)Top=Width;HeightCaption   Descripción:  TLabelLabel6Left!TopWidthCHeightCaptionTipo Terminal:  TDBCheckBox	TE_ACTIVOLeft@Top� Width5Height	AlignmenttaLeftJustifyCaptionActivo:	DataField	TE_ACTIVO
DataSourcedsTerminalesTabOrderValueCheckedSValueUncheckedN  TDBMemoTE_TEXTOLeftiTopOWidthdHeight)	DataFieldTE_TEXTO
DataSourcedsTerminales	MaxLength� 
ScrollBars
ssVerticalTabOrder  TDBEdit	TE_NSERIELeftiTop"WidthcHeight	DataField	TE_NSERIE
DataSourcedsTerminales	MaxLength2TabOrder  TDBEdit
TE_DESCRIPLeftiTop9WidthcHeight	DataField
TE_DESCRIP
DataSourcedsTerminales	MaxLength2TabOrder  TZetaDBNumero	TE_NUMEROLeftiTopzWidthyHeightMascaramnHorasTabOrderText0.00	DataField	TE_NUMERO
DataSourcedsTerminales  TDBNavigatordbPostCancelLeftKTop� Width� Height
DataSourcedsTerminalesVisibleButtonsnbPostnbCancel Hints.StringsPrimeroAnterior	SiguienteUltimoAgregarBorrar	ModificarAceptarCancelar	Refrescar ParentShowHintShowHint	TabOrder  	TComboBoxcbxTipoTerminalLeftiTop
Width� Height
ItemHeight	ItemIndex TabOrder TextTerminal ZKOnChangecbxTipoTerminalChangeItems.StringsTerminal ZKSYNEL     TDBGriddbGridTerminalesLeft Top WidthAHeightAlignalClient
DataSourcedsTerminalesFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclBlackTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName	TE_NSERIETitle.Caption   Número de SerieWidth� Visible	 Expanded	FieldNameTE_TIPOTitle.CaptionTipoWidthVVisible	 Expanded	FieldName
TE_DESCRIPTitle.Caption   DescripciónWidth� Visible	 Expanded	FieldName	TE_ACTIVOTitle.CaptionActivoWidth&Visible	 Expanded	FieldNameTE_TEXTOTitle.CaptionComentariosWidth� Visible	 Expanded	FieldName	SN_NUMEROTitle.Caption	Sentinel Visible	     	TTabSheet	TabSheet4CaptionArchivos 3DT
ImageIndex TPanelPanel5Left Top� WidthAHeightpAlignalBottomTabOrder  TSpeedButtonbtnSalvaryAbrir3DTLeft�Top�WidthHeight
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwww      w�����w�����w������w������{��������������     wx�p���wwx�w  {���wwww�w��wwwwww�wwwwwww�wwwwwwwwwwwwwwwwwOnClickbtnSalvaryAbrir3DTClick  TPanelPanel6LeftTopWidth?Height#AlignalTopParentShowHintShowHint	TabOrder  TDBNavigatorDBNavigator1LeftTopWidth� Height
DataSourcedsHistorico3DTVisibleButtonsnbFirstnbPriornbNextnbLastnbDelete	nbRefresh Flat	Hints.StringsPrimeroAnterior	SiguienteUltimoAgregarBorrar	ModificarAceptarCancelar	Refrescar ParentShowHintShowHint	TabOrder    TDBMemo	HI_3DTXMLLeftTopMWidth?Height"AlignalClient	DataField	HI_3DTXML
DataSourcedsHistorico3DTFont.CharsetDEFAULT_CHARSET
Font.Color
clGrayTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	
ScrollBarsssBothTabOrder  TPanelPanel1LeftTop$Width?Height)AlignalTopCaption	DocumentoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid1Left Top WidthAHeight� AlignalClient
DataSourcedsHistorico3DTFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style OptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit 
ParentFontTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclBlackTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName	SN_NUMEROTitle.CaptionSentinelWidthOVisible	 Expanded	FieldName
HI_FEC_DOCTitle.CaptionFecha de DocumentoWidthqVisible	 Expanded	FieldName
HI_FEC_INGTitle.CaptionFecha IngresoWidth|Visible	 Expanded	FieldName
HI_COMENTATitle.CaptionComentariosWidthIVisible	 Expanded	FieldName	HI_NOMBRETitle.CaptionArchivoWidthVisible	     	TTabSheetHistoAutorizacionesCaptionAutorizacionesFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ImageIndex
ParentFont TDBGridDBGrid2Left Top WidthAHeighttAlignalClient
DataSource
dsAutorizaOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclBlackTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickDBGrid2DblClickColumnsExpanded	FieldNameAU_FOLIOTitle.AlignmenttaCenterTitle.Caption#WidthVisible	 Expanded	FieldNameAU_FECHATitle.AlignmenttaCenterTitle.CaptionFecha y HoraWidthGVisible	 Expanded	FieldName	SN_NUMEROTitle.CaptionSentinelWidth-Visible	 Expanded	FieldName
VE_DESCRIPTitle.Caption   VersiónWidth?Visible	 Expanded	FieldName	AU_CLAVE1Title.CaptionClave 1WidthFVisible	 Expanded	FieldName	AU_CLAVE2Title.CaptionClave 2Width� Visible	 Expanded	FieldName
AU_PAR_AUTTitle.Caption
   ParámetroWidth4Visible	 Expanded	FieldName
AU_FEC_AUTTitle.Caption
Venc. Aut.Visible	 Expanded	FieldName
AU_FEC_PRETitle.CaptionVenc. Pres.Visible	 Expanded	FieldName
AU_KIT_DISTitle.Caption   ¿Kit?WidthVisible	 Expanded	FieldName	DI_CODIGOTitle.CaptionDistribuidorVisible	      TPanelPanel10Left Top WidthIHeight!AlignalTop
BevelOuterbvNoneFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel2LeftTopWidth)HeightCaption	Sentinel:  TZetaKeyCombozSentinelasLeft>TopWidth� HeightAutoComplete	BevelKindbkFlatStylecsDropDownListCtl3D
ItemHeightParentCtl3DTabOrder OnChangezSentinelasChange	ListaFija	lfNingunaListaVariablelvPuestoOffset OpcionalEsconderVacios    	TTabSheettabSentinelCaption
Sentinelas
ImageIndex TDBGridGridSentinelasLeft Top)WidthIHeight� AlignalTop
DataSourcedsSentinelasFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclBlackTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsColorclInfoBkExpanded	FieldName	SN_NUMEROReadOnly	Title.CaptionSentinelWidthQVisible	 Expanded	FieldName	SN_NOMBRETitle.Caption   DescripciónWidth� Visible	 Expanded	FieldNameSN_TIPOTitle.CaptionTipoWidthVVisible	 Expanded	FieldName	SN_SERVERTitle.CaptionServidorWidthxVisible	 Expanded	FieldName	SN_ACTIVOTitle.CaptionActivoWidth&Visible	    TPanelPanel7Left Top WidthIHeight)AlignalTopTabOrder TSpeedButtonShowAvailableLeft�TopWidth� HeightHintVer Sentinelas DisponiblesCaption   Físicos DisponiblesFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwp wwwwww0wwwwp0�wwwwwww 0�� w  3�p33 ����{{;���;s������� ����0;;p���������;;p���p���    w � wwww
ParentFontParentShowHintShowHint	OnClickShowAvailableClick  TDBNavigatorNavegadorSentinelasLeftTopWidth Height
DataSourcedsSentinelasVisibleButtonsnbEditnbPostnbCancel	nbRefresh Hints.StringsFirst recordPrior recordNext recordLast recordAgregar SentinelaBorrar SentinelaEditar SentinelaGrabar CambiosCancelar Cambios
Refrescar  TabOrder   TBitBtnBitBtn1Left0TopWidth� HeightCaptionNuevo Sentinel VirtualFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickBitBtn1Click
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs   TPanelPanel8Left Top!WidthIHeight)AlignalTopTabOrder
DesignSizeI)  TBitBtnbtnArchivoLicLeft�TopWidth� HeightHint#Generar Archivo de Licencia VirtualAnchorsakRightakBottom CaptionGenerar Archivo LicenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrder OnClickbtnArchivoLicClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUP UUUUUUWwUUUUUUP�UUUUUU��UUUUUP �UUUUUWwWUUUUUP��UUUUU��WUUUUP ;UUUUWwUuUUUU ��UUUUUwUWUUUUU;UU��_UuUW  �UUWwuwuWUUp��;UUwUWuUuUU����UUUUUUWUUU���UUUU�UUUU��UUUWUu�UUw���UUUu�uUW�UUP�3��UUUW_UUW�UUU��UUUUu��wUUUUW  uUUUUWwwuUUU	NumGlyphs  TBitBtnbbGrabaSentinelLeft�TopWidth� HeightHintQuemar Sentinel SeleccionadoAnchorsakRightakBottom Caption   Grabar Sentinel FísicoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrderOnClickbbGrabaSentinelClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333  3333?wws�333 ���333w3?7s�30w��p3373337?3����333333w��w333�33�� ��337w�33wp ww337w333�����3s�����s30     337wwwww33333333333333333333333s�s333w� �s333s�w3s3330���33337?��33333  33333wws333333333333333333	NumGlyphs  TBitBtnbtnRemoverSentinelLeftyTopWidth� HeightHintRemover Sentinel de la cuenta AnchorsakRightakBottom CaptionRemover SentinelFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrderOnClickbtnRemoverSentinelClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333333�3333330 3333337w3333330 3���337w9���33337ww333?9���333 7wws333w3333333 3333333w333333333333333?3333333 33?3333w33�3333 333333w3<�333333����?�<����Ù37wwwww�<����Ù37wwwwsw33<�33333373333�33�3330 33s3337w3333330 3333337w3333333333333333	NumGlyphs   	TGroupBox	GroupBox4Left TopJWidthIHeight.AlignalClientCaptionAutorizacionesFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TDBGridDBGridAutorizaLeftTopWidthEHeightAlignalClient
DataSource
dsAutorizaOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclNavyTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameAU_FOLIOTitle.AlignmenttaCenterTitle.Caption#WidthVisible	 Expanded	FieldNameAU_FECHATitle.AlignmenttaCenterTitle.CaptionFecha y HoraWidthGVisible	 Expanded	FieldName	SN_NUMEROTitle.CaptionSentinelWidth-Visible	 Expanded	FieldName
VE_DESCRIPTitle.Caption   VersiónWidth?Visible	 Expanded	FieldName	AU_CLAVE1Title.CaptionClave 1WidthFVisible	 Expanded	FieldName	AU_CLAVE2Title.CaptionClave 2Width� Visible	 Expanded	FieldName
AU_PAR_AUTTitle.Caption
   ParámetroWidth4Visible	 Expanded	FieldName
AU_FEC_AUTTitle.Caption
Venc. Aut.Visible	 Expanded	FieldName
AU_FEC_PRETitle.CaptionVenc. Pres.Visible	 Expanded	FieldName
AU_KIT_DISTitle.Caption   ¿Kit?WidthVisible	 Expanded	FieldName	DI_CODIGOTitle.CaptionDistribuidorVisible	       TPanelPanel2Left Top�WidthQHeight%AlignalBottomTabOrder
DesignSizeQ%  TImageImagenLeftTopWidth)Height#AlignalLeftCenter	  TBitBtnOKBtnLeft�TopWidthKHeightAnchorsakTopakRight Caption&OKTabOrder TabStopOnClick
OKBtnClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnCancelarBtnLeftTopWidthKHeightAnchorsakTopakRight Cancel	Caption	&CancelarModalResultTabOrderTabStopOnClickCancelarBtnClick
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  33�33333333?333333  39�33�3333��33?33  3939�338�3?��3  39��338�8��3�3  33�338�3��38�  339�333�3833�3  333�33338�33?�3  3331�33333�33833  3339�333338�3�33  333��33333833�33  33933333�33�33  33����333838�8�3  33�39333�3��3�3  33933�333��38�8�  33333393338�33���  3333333333333338�3  333333333333333333  	NumGlyphs   TDataSource	dsSistemaAutoEditLeft�TopK  TDataSource	dsVersionAutoEditLeft�TopK  TDataSourcedsGrupoAutoEditLeft�Top�   TDataSourcedsCiudadAutoEditLeft�Topm  TDataSourcedsDistribuidorAutoEditLeft�Top�   TDataSourcedsGiroAutoEditLeft�Top�   TDataSourcedsPaisAutoEditLeft�Top�   TDataSourcedsTerminalesOnDataChangedsTerminalesDataChangeLeft�Top�   TSaveDialog	sdGuardarFileNameTerminales.datFilterDatos|*.datLeft�Topi  TSaveDialogdlgLicencia
DefaultExt.licFileNameVirtual.licFilterLicencia Virtual|*.licLeftdTopq  TDataSourcedsHistorico3DTLeft�Topg  TDataSourcedsSentinelasOnDataChangedsSentinelasDataChangeLeftTopp  TDataSource
dsAutorizaLeftHTop�  TDataSourcedsActualLeft�Top   TDataSourcedsHistorico3DTTemporalLeft�Top�    