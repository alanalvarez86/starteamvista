unit FAltaSen;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Mask,
     ZetaFecha,
     ZetaNumero,
     ZetaKeyCombo;

type
  TAltaSen = class(TForm)
    IncioLBL: TLabel;
    FinalLBL: TLabel;
    FechaLBL: TLabel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    FechaCompra: TZetaFecha;
    Inicio: TZetaNumero;
    Final: TZetaNumero;
    Tipo: TZetaKeyCombo;
    TipoLBL: TLabel;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AltaSen: TAltaSen;

implementation

uses ZetaDialogo,
     DSentinel;

{$R *.DFM}

procedure TAltaSen.FormShow(Sender: TObject);
begin
     FechaCompra.Valor := Date();
     Final.Valor := 0;
     with Inicio do
     begin
          Valor := 0;
          SetFocus;
     end;
     Tipo.ItemIndex := 0;
end;

procedure TAltaSen.OKClick(Sender: TObject);
var
   iPrimero, iUltimo: Integer;
   dStart: TDate;
   oCursor: TCursor;
begin
     iPrimero := Inicio.ValorEntero;
     iUltimo := Final.ValorEntero;
     dStart := FechaCompra.Valor;
     if ( iPrimero = 0 ) or ( iUltimo = 0 ) or ( dStart = 0 ) then
        ZetaDialogo.zError( 'ˇ Atención !', 'Capture Todos Los Datos', 0 )
     else
         if ( iPrimero <= iUltimo ) then
         begin
              oCursor := Screen.Cursor;
              Screen.Cursor := crHourglass;
              try
                 dmSentinel.AgregaSentinel( iPrimero, iUltimo, dStart, Tipo.ItemIndex );
              finally
                     Screen.Cursor := oCursor;
              end;
         end
         else
             ZetaDialogo.zError( 'ˇ Atención !', 'Indique La Numeración Correcta', 0 );
end;

end.
