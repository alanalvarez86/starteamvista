unit DSentinel;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, ShellApi,
     Controls, Forms, Dialogs, StdCtrls, Db, DBTables,
     FAutoClasses,
     Variants,
     FAutoServer,
     FAutoMaster;

const         
     CR_LF = Chr( 13 ) + Chr( 10 );
     K_INT_TRUE = 1;
     K_INT_FALSE = 0;
     NullDateTime = 0;
     K_VERSION_218_TXT = '2.18';
type
  TCriterio = ( cfTodos, cfRazonSocial, cfDistribuidor );
  TdmSentinel = class(TDataModule)
    tCiudad: TTable;
    tEstado: TTable;
    tGrupo: TTable;
    tDistribu: TTable;
    tUsuario: TTable;
    tVersion: TTable;
    tqSistema: TQuery;
    tqAutoriza: TQuery;
    tqActual: TQuery;
    tqAgregaSistema: TQuery;
    tqBorraSistema: TQuery;
    tqAgregaAutoriza: TQuery;
    tqModificaSistema: TQuery;
    qSentinel: TQuery;
    tSentinel: TTable;
    dbSentinel: TDatabase;
    qVersion: TQuery;
    qAsignado: TQuery;
    qUsuario: TQuery;
    qLibre: TQuery;
    qHistoSen: TQuery;
    tqValidaSentinel: TQuery;
    tqRazonSocial: TQuery;
    tGiro: TTable;
    tPais: TTable;
    tqExisteSentinel: TQuery;
    tqExistenClaves: TQuery;
    tqInstalaciones: TQuery;
    tqMaxInstalacion: TQuery;
    tqAddInstalacion: TQuery;
    tqUsoSentinel: TQuery;
    tTerminales: TTable;
    tqTerminales: TQuery;
    tqActualizaTerminal: TQuery;
    tqSentVirtual: TQuery;
    tqServerVirt: TQuery;
    tqAgregaHistorico3DT: TQuery;
    tHistorico3DT: TTable;
    tqBuscaTerminal: TQuery;
    qSentinelPorSist: TQuery;
    tqSistemaLook: TQuery;
    tSistema: TTable;
    tAutoriza: TTable;
    qGetNextSentVirtual: TQuery;
    tqUltimaAutoriza: TQuery;
    tqHistorico3DTOrdenado: TQuery;
    tqModificaHist3DT: TQuery;
    procedure dmSentinelDestroy(Sender: TObject);
    procedure AU_COMENTAChange(Sender: TField);
    procedure qSentinelAfterOpen(DataSet: TDataSet);
    procedure tqActualAfterEdit(DataSet: TDataSet);
    procedure tqActualAfterOpen(DataSet: TDataSet);
    procedure tqActualBeforeEdit(DataSet: TDataSet);
    procedure tqActualUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
    procedure tqActualNewRecord(DataSet: TDataSet);
    procedure tqAutorizaAU_FEC_AUTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure tqAutorizaAU_FEC_PREGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure tqAutorizaAU_KIT_DISGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure tqAutorizaAfterOpen(DataSet: TDataSet);
    procedure tqSistemaFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure tqInstalacionesAfterOpen(DataSet: TDataSet);
    procedure tTerminalesBeforePost(DataSet: TDataSet);
    procedure tTerminalesNewRecord(DataSet: TDataSet);
    procedure tTerminalesAfteropen(DataSet: TDataSet);
    procedure tqTerminalesTE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure tSentinelBeforePost(DataSet: TDataSet);
    procedure tAutorizaBeforePost(DataSet: TDataSet);
    procedure tAutorizaAfterInsert(DataSet: TDataSet);
    procedure tSistemaBeforePost(DataSet: TDataSet);
    procedure tSentinelAfterOpen(DataSet: TDataSet);
    procedure tSistemaNewRecord(DataSet: TDataSet);

  private
    { Private declarations }
    FAUComentaChanged: Boolean;
    FFiltro: String;
    FFiltroTipo: TCriterio;
    FBookMark: TBookMark;
    FUsuario: String;
    FNuevo: Integer;
    FNuevoSentinela: Integer;
    FPrevioSentinela: Integer;
    FPosicion: Integer;
    FEsAlta: Boolean;
    FAgregaAuto: Boolean;
    FHayPrestados: Boolean;
    FTipoTerminal:Integer;
    function ActualAbrir( const iFolio: Integer ): Boolean;
    function ActualCerrar: Boolean;
    function Cambio( const sNombre: String ): Boolean;
    function CambiaronModulos: Boolean;
    function CambioGeneral: Boolean;
    function Conectar: Boolean;
    function FolioActual: Integer;
    function ValidaSentinel( const iCodigo, iSistema: Integer; var sMensaje: String ): Boolean;
    function ValidaSentinela( const iCodigo, iSistema: Integer; var sMensaje: String ): Boolean;
    procedure BookMarkGet;
    procedure BookMarkGoTo;
    procedure BookMarkRelease;
    procedure CampoFechaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CopiaParams( Fuente: TDataSet; Destino : TQuery );
    procedure FieldsToMaster;
    procedure MarcaFechaHora;
    procedure PosicionaActual( const iFolio: Integer );
    procedure PreparaQuery( Query: TQuery );
    procedure ValidaKit;
    procedure AbrirTerminales(Datasource: TDatasource);
    procedure AbrirHistorico3DT( Datasource: TDatasource );
    procedure AbrirSentinelas(Datasource: TDatasource);
    function SistemaAbrir(const iFolio: Integer): Boolean;
    procedure PosicionaSistema(const iFolio: Integer);
    procedure RefrescarAutorizacion;
    procedure TipoSentinelGetText(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure SentinelActivoGetText(Sender: TField; var Text: String;DisplayText: Boolean);
    function SiguienteSistema: Integer;
    function GetUltimaAutorizacion: Integer;
  public
    { Public declarations }
    property SentinelPrevio: Integer read FPrevioSentinela;
    property EsAlta: Boolean read FEsAlta;
    property HayPrestados: Boolean read FHayPrestados write FHayPrestados;
    property TipoTerminal: Integer read FTipoTerminal write FTipoTerminal;
    function ConectarLibres: Boolean;
    function FilterSet( const Texto: String ): Boolean;
    function GetFolioSistema: Integer;
    function GetSentinelDB: Integer;
    function InstalacionGenerar( const iSistema, iSentinel, iInstalacion: Integer; const sDistribuidor, sComentario: String; var sClave: String ): Boolean;
    function SentinelConectado: Boolean;
    function ValidaCampos: Boolean;
    function ValidaClaves( const iSistema: Integer; const sClave1, sClave2: String ): Boolean;
    function ValidaIngreso( const sNombre, sClave: String ): Boolean;
    function GetServerVirtual(const iSentinel:Integer):string;
    procedure AbrirCiudad( Datasource: TDatasource );
    procedure AbrirDistribuidor( Datasource: TDatasource );
    procedure AbrirEstado( Datasource: TDatasource );
    procedure AbrirGiro( Datasource: TDatasource );
    procedure AbrirGrupo( Datasource: TDatasource );
    procedure AbrirLibres( Datasource: TDatasource );
    procedure AbrirHistoria( Datasource: TDatasource; Query: TDataset );
    procedure AbrirInstalaciones( dsSistema, dsInstalacion: TDatasource );
    procedure AbrirPais( Datasource: TDatasource );
    procedure AbrirPrincipal( Datasource, dsAutoriza: TDatasource );
    procedure AbrirPrincipalHistoria(Sentinel:Integer);
    procedure AbrirSentinel( Datasource, dsHistoria: TDatasource );
    procedure AbrirSentinelHistoria;
    procedure AbrirSistema( Datasource, dsVersion, dsGrupo, dsCiudad, dsDistribuidor, dsGiro, dsPais,dsTerminales, dsHistorico3DT,dsSentinelas,dsAutorizaHistoria,dsActual: TDatasource );
    procedure GetHistorico3DTOrdenado( iFolio, iNumeroSentinela: Integer);
    procedure AbrirUsuario( Datasource: TDatasource );
    procedure AbrirVersion( Datasource: TDatasource );
    procedure ActualizaSentinel;
    procedure AgregaSentinel( const iPrimero, iUltimo: Integer; const dStart: TDate; const iTipo: Integer );
    procedure AgregaSistema;
    procedure BorraSistema;
    procedure CancelaCambios;
    procedure CerrarSentinel;
    procedure FilterReset( const lReset: Boolean );
    procedure FilterSetCriteria( const Value: TCriterio );
    procedure GrabaCambios;
    procedure GrabarSentinel;
    procedure InstalacionNueva( const iSistema: Integer; var iSentinel, iInstalacion: Integer; var sDistribuidor: String );
    procedure ListaDistribuidores( Lista: TStrings );
    procedure LeeDatosPrevios;
    procedure LeerSentinel;
    procedure ModificaSistema;
    procedure Refrescar;
    procedure ResetDatasource(Datasource: TDatasource);
    procedure ActualizaSentinelTerminal(const NumAnterior: Integer );
    procedure ActualizaSentinelVirtual(const Sentinel: Integer;const Server:string );
    procedure AgregaHistorico3DT( iSN_NUMERO, iSI_FOLIO : integer; dFechaDoc : TDateTime; sComentarios, sArchivo : string);
    procedure ModificaHistorico3DT( iFolio, iNumeroSentinela: Integer; sComentario:string);
    procedure GetTerminalesBusqueda(dsTerminales: TDataSource;const Pista: String);
    procedure AbrirSentinelPorSistema;
    procedure AgregaAutorizacion;
    procedure GrabarSistema;
    procedure RefrescarLook;
    function SiguienteSentinelVirtual: Integer;
    procedure GrabarSentinelSistema( const iFolio, iSerieDB: Integer );
    function DetectarMaestroProxy : Boolean;
  end;

var
  dmSentinel: TdmSentinel;

function EsVer20( const sVersion: String ): Boolean;
function EsVerValidacion( const sVersion: String ): Boolean;
function IntToBoolText( const iValor: Integer ): String;
function UnloadEmpleados( const iNivel: Word ): Integer;
procedure CallNotePad( const sFileName: String );
function GetExceptionInfo( Error: Exception ): String;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     FAltaSen,
     FHistoria,
     FLecturaSen,
     FSistema;

const
     T_ALTA = 'Alta';
     T_AUTORIZACION = 'Autorizados';
     T_GENERAL = 'General';

{$R *.DFM}

function EsVer20( const sVersion: String ): Boolean;
begin
     Result := ( sVersion > K_VERSION_13_TXT );
end;

function EsVerValidacion( const sVersion: String ): Boolean;
begin
     Result := ( sVersion >= K_VERSION_218_TXT );
end;

function ConvierteBoleano( const iValor: Integer ): Boolean;
begin
     Result := ( iValor > 0 );
end;

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      ShowCmd );
end;

procedure CallNotePad( const sFileName: String );
begin
     ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( sFileName ), ExtractFilePath( sFileName ), SW_SHOWDEFAULT );
end;

function IntToBoolText( const iValor: Integer ): String;
begin
     case iValor of
          1: Result := 'Si'
     else
         Result := 'No';
     end;
end;

function UnloadEmpleados( const iNivel: Word ): Integer;
begin
     case iNivel of
          0: Result := 100;
          1: Result := K_DEMO_EMPLOYEES;
          2: Result := 600;
          3: Result := 1500;
          4: Result := 1750;
          5: Result := 2750;
          6: Result := 4000;
          7: Result := 5000;
          8: Result := 6000;
          9: Result := 7000;
         10: Result := 8000;
         11: Result := 15000;
         12: Result := K_TOP_EMPLOYEES;
     else
         Result := 0;
     end;
end;


function GetExceptionInfo( Error: Exception ): String;
const
     DBIERR_KEYVIOL = 9729;
     DBIERR_FOREIGNKEYERR = 9733;
var
   i: Word;
begin
     Result := '';
     if ( Error is EDBEngineError ) then
     begin
          with Error as EDBEngineError do
          begin
               if ( ErrorCount > 0 ) then
               begin
                    case Errors[ 0 ].ErrorCode of
                         DBIERR_KEYVIOL: Result := '� C�digo Ya Existe !';
                    else
                        begin
                             for i := 0 to ( ErrorCount - 1 ) do
                             begin
                                  with Errors[ i ] do
                                  begin
                                       Result := Result +
                                                 CR_LF +
                                                 '( ' +
                                                 IntToStr( Category ) +
                                                 ':' +
                                                 IntToStr( ErrorCode ) +
                                                 ' ):' +
                                                 Message;
                                  end;
                             end;
                        end;
                    end;
               end
               else
                   Result := 'Error Tipo EDBEngineError Desconocido';
          end;
     end
     else
         Result := Error.Message;
end;


{ *********** TdmSentinel ********** }

procedure TdmSentinel.dmSentinelDestroy(Sender: TObject);
begin
     BookMarkRelease;
     FilterReset( False );
end;

function TdmSentinel.Conectar: Boolean;
begin
     try
        with dbSentinel do
        begin
             Connected := True;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZExcepcion( '� Error !', 'Error Al Conectar Base de Datos', Error, 0 );
                Result := False;
           end;
     end;
end;

procedure TdmSentinel.AbrirCiudad( Datasource: TDatasource );
begin
     tCiudad.Active := True;
     Datasource.Dataset := tCiudad;
end;

procedure TdmSentinel.AbrirDistribuidor( Datasource: TDatasource );
begin
     tDistribu.Active := True;
     Datasource.Dataset := tDistribu;
end;

procedure TdmSentinel.AbrirEstado( Datasource: TDatasource );
begin
     tEstado.Active := True;
     Datasource.Dataset := tEstado;
end;

procedure TdmSentinel.AbrirGiro( Datasource: TDatasource );
begin
     tGiro.Active := True;
     Datasource.Dataset := tGiro;
end;

procedure TdmSentinel.AbrirGrupo( Datasource: TDatasource );
begin
     tGrupo.Active := True;
     Datasource.Dataset := tGrupo;
end;

procedure TdmSentinel.AbrirLibres( Datasource: TDatasource );
begin
     with Datasource do
     begin
          Dataset := qLibre;
     end;
end;

procedure TdmSentinel.AbrirHistoria( Datasource: TDatasource; Query: TDataset );
begin
     Datasource.Dataset := Query;
end;

procedure TdmSentinel.AbrirInstalaciones( dsSistema, dsInstalacion: TDatasource );
begin
     dsSistema.Dataset := tqSistema;
     with tqInstalaciones do
     begin
          DisableControls;
          Active := False;
          ParamByName( 'SI_FOLIO' ).AsInteger := GetFolioSistema;
          Active := True;
          EnableControls;
     end;
     dsInstalacion.Dataset := tqInstalaciones;
end;

procedure TdmSentinel.AbrirPais( Datasource: TDatasource );
begin
     tPais.Active := True;
     Datasource.Dataset := tPais;
end;

procedure TdmSentinel.AbrirTerminales( Datasource: TDatasource );
begin
     tTerminales.Active := True;
     with tTerminales do
     begin
          Filtered := False;
          Filter := 'SI_FOLIO = ' + IntToStr(GetFolioSistema);
          Filtered := True;
     end;
     Datasource.Dataset := tTerminales;
end;

procedure TdmSentinel.AbrirHistorico3DT( Datasource: TDatasource );
begin
     tHistorico3DT.Active := True;
     with tHistorico3DT do
     begin
          Filtered := False;
          Filter := 'SI_FOLIO = ' + IntToStr(GetFolioSistema);
          Filtered := True;
     end;
     Datasource.Dataset := tHistorico3DT;
end;

procedure TdmSentinel.AbrirSentinelas( Datasource: TDatasource );
begin
     tSentinel.Active := True;
     with tSentinel do
     begin
          Filtered := False;
          Filter := 'SI_FOLIO = ' + IntToStr(GetFolioSistema);
          Filtered := True;
     end;
     Datasource.Dataset := tSentinel;
end;


procedure TdmSentinel.AbrirPrincipal( Datasource, dsAutoriza: TDatasource );
begin
     tqSistemaLook.Active := True;
     Datasource.Dataset := tqSistemaLook;
     dsAutoriza.Dataset := tqAutoriza;
end;

procedure TdmSentinel.AbrirPrincipalHistoria (Sentinel:Integer);
begin
     with tqAutoriza do
     begin
          DisableControls;
          Active := False;
          ParamByName( 'SI_FOLIO' ).AsInteger := GetFolioSistema;
          ParamByName( 'SN_NUMERO' ).AsInteger := Sentinel;
          Active := True;
          EnableControls;
     end;
end;

procedure TdmSentinel.AbrirSentinel( Datasource, dsHistoria: TDatasource );
begin
     with qSentinel do
     begin
          DisableControls;
          Active := False;
          Active := True;
          EnableControls;
     end;
     Datasource.Dataset := qSentinel;
     dsHistoria.Dataset := qHistoSen;
end;

procedure TdmSentinel.AbrirSentinelHistoria;
begin
     with qHistoSen do
     begin
          DisableControls;
          Active := False;
          ParamByName( 'SN_NUMERO' ).AsInteger := qSentinel.FieldByName( 'SN_NUMERO' ).AsInteger;
          Active := True;
          EnableControls;
     end;
end;

procedure TdmSentinel.AbrirSistema( Datasource, dsVersion, dsGrupo, dsCiudad, dsDistribuidor, dsGiro, dsPais,dsTerminales, dsHistorico3DT, dsSentinelas,dsAutorizaHistoria,dsActual: TDatasource );
begin
     qVersion.Active := True;
     dsVersion.Dataset := qVersion;
     AbrirGrupo( dsGrupo );
     AbrirCiudad( dsCiudad );                             
     AbrirDistribuidor( dsDistribuidor );
     AbrirGiro( dsGiro );
     AbrirPais( dsPais );
     AbrirTerminales( dsTerminales );
     AbrirHistorico3DT( dsHistorico3DT );
     AbrirSentinelas(dsSentinelas);
     dsAutorizaHistoria.DataSet := tqAutoriza;
     dsActual.DataSet := tAutoriza;
     Datasource.Dataset := tSistema; 
end;

procedure TdmSentinel.AbrirUsuario( Datasource: TDatasource );
begin
     tUsuario.Active := True;
     Datasource.Dataset := tUsuario;
end;

procedure TdmSentinel.AbrirVersion( Datasource: TDatasource );
begin
     tVersion.Active := True;
     Datasource.Dataset := tVersion;
end;

procedure TdmSentinel.CerrarSentinel;
begin
     qSentinel.Active := False;
     qHistoSen.Active := False;
end;

procedure TdmSentinel.ResetDatasource( Datasource: TDatasource );
begin
     try
        Datasource.Dataset := nil;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function TdmSentinel.ConectarLibres: Boolean;
begin
     with qLibre do
     begin
          DisableControls;
          Active := False;
          Active := True;
          EnableControls;
          Result := not IsEmpty;
     end;
end;

function TdmSentinel.ValidaIngreso( const sNombre, sClave: String ): Boolean;
begin
     if Conectar then
     begin
          PreparaQuery( qUsuario );
          with qUsuario do
          begin
               Active := False;
               ParamByName( 'US_CODIGO' ).AsString := UpperCase( sNombre );
               Active := True;
               if IsEmpty then
                  Result := False
               else
                   Result := ( sClave = FieldByName( 'US_PASSWRD' ).AsString );
               if Result then
                  FUsuario := FieldByName( 'US_CODIGO' ).AsString
               else
                   FUsuario := '';
               Active := False;
          end;
     end
     else
         Result := False;
end;

function TdmSentinel.GetSentinelDB: Integer;
begin
     Result := tSistema.FieldByName( 'SN_NUMERO' ).AsInteger
end;

function TdmSentinel.GetFolioSistema: Integer;
begin
     IF FEsAlta then
        Result := tSistema.FieldByName( 'SI_FOLIO' ).AsInteger
     else
         Result := tqSistemaLook.FieldByName( 'SI_FOLIO' ).AsInteger;
end;

procedure TdmSentinel.FilterSetCriteria( const Value: TCriterio );
begin
     FFiltroTipo := Value;
end;

procedure TdmSentinel.FilterReset( const lReset: Boolean );
var
   lActive: Boolean;
begin
     with tqSistemaLook do
     begin
          lActive := Active;
          try
             if Filtered then
             begin
                  Active := False;
                  Filtered := False;
             end
             else
                 Active := False;
          finally
                 Active := lReset and lActive;
          end;
     end;
     FFiltro := '';
end;

function TdmSentinel.FilterSet( const Texto: String ): Boolean;
begin
     FFiltro := AnsiUpperCase( Texto );
     if ( FFiltro = '' ) then
     begin
          FilterReset( True );
          Result := True;
     end
     else
     begin
          with tqSistemaLook do
          begin
               Filtered := True;
               Result := not Eof;
          end;
          if not Result then
             FilterReset( True );
     end;
end;

procedure TdmSentinel.CampoFechaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   dValue: TDate;
begin
     if DisplayText then
     begin
          dValue := Sender.AsDateTime;
          if ( dValue = NullDateTime ) then
             Text := ''
          else
              Text := FormatDateTime( 'dd/mmm/yyyy', dValue );
     end;
end;

procedure TdmSentinel.tqSistemaFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     with DataSet do
     begin
          if ( FieldCount > 0 ) then
          begin
               case FFiltroTipo of
                    cfTodos: Accept := ( Pos( FFiltro, AnsiUpperCase( FieldByName( 'SI_RAZ_SOC' ).AsString ) ) > 0 ) or
                                       ( Pos( FFiltro, AnsiUpperCase( FieldByName( 'DI_CODIGO' ).AsString ) ) > 0 );
                    cfRazonSocial: Accept := Pos( FFiltro, AnsiUpperCase( FieldByName( 'SI_RAZ_SOC' ).AsString ) ) > 0;
                    cfDistribuidor: Accept := Pos( FFiltro, AnsiUpperCase( FieldByName( 'DI_CODIGO' ).AsString ) ) > 0;
               end;
          end
          else
              Accept := False;
     end;
end;

function TdmSentinel.FolioActual: Integer;
begin
     Result := GetFolioSistema;
end;

procedure TdmSentinel.PreparaQuery( Query: TQuery );
begin
     with Query do
     begin
          if not Prepared then
             Prepare;
     end;
end;

function TdmSentinel.ActualAbrir( const iFolio: Integer ): Boolean;
begin
     PreparaQuery( tqActual );
     with tqActual do
     begin
          Active := False;
          Params[ 0 ].AsInteger := iFolio;
          Active := True;
     end;
     with tAutoriza do
     begin
          Active := False;
          Filtered := False;
          Filter := Format('SI_FOLIO = %d',[iFolio]);
          Filtered := True;
          Active := True;
          Locate('AU_FOLIO',tqActual.FieldByName('AU_FOLIO').AsInteger,[]);
          Result := not IsEmpty;
     end;
end;

function TdmSentinel.SistemaAbrir( const iFolio: Integer ): Boolean;
begin
     tSistema.Active := True;
     with tSistema do
     begin
          Result  := Locate('SI_FOLIO',iFolio,[]);
     end;
end;


function TdmSentinel.ActualCerrar: Boolean;
begin
     with tqActual do
     begin
          Active := False;
          Result := True;
     end;
end;

procedure TdmSentinel.PosicionaActual( const iFolio: Integer );
begin
     if ActualAbrir( iFolio ) then
     begin
          with tAutoriza do
          begin
               FPosicion := FieldByName( 'SI_FOLIO' ).AsInteger;
          end;
     end
     else
         FPosicion := 0;
end;

procedure TdmSentinel.PosicionaSistema( const iFolio: Integer );
begin
     if SistemaAbrir( iFolio ) then
     begin
          with tSistema do
          begin
               FPosicion := FieldByName( 'SI_FOLIO' ).AsInteger;
          end;
     end
     else
         FPosicion := 0;
end;

procedure TdmSentinel.AgregaSistema;
begin
     with AutoMaster do
     begin
          if DetectarMaestroProxy then
          begin
          {$ifdef SIN_MAESTRO}
               FNuevo := 999999;
          {$else}
               FNuevo := GetSerialNumber;
          {$endif}
               if ( FNuevo > 0 ) then
               begin
                    FEsAlta := True;
                    PreparaQuery( tqAgregaSistema );
                    //PreparaQuery( tqAgregaAutoriza );
                    PosicionaSistema( -1 );
                    //PosicionaActual( -1 );
                    tSistema.Insert;
               end
               else
               begin
                     raise Exception.Create( StatusMsg );
               end;
          end
          else
          begin
               raise Exception.Create( StatusMsg );
          end;
     end;
end;

procedure TdmSentinel.BookMarkGet;
begin
     BookMarkRelease;
     FBookMark := tqSistemaLook.GetBookmark;
end;

procedure TdmSentinel.BookMarkGoto;
begin
     if Assigned( FBookMark  ) then
        tqSistemaLook.GotoBookmark( FBookMark );
     BookMarkRelease;
end;

procedure TdmSentinel.BookMarkRelease;
begin
     if Assigned( FBookMark ) then
     begin
          tqSistemaLook.FreeBookMark( FBookMark );
          FBookMark := nil;
     end;
end;

procedure TdmSentinel.ModificaSistema;
begin
     with AutoMaster do
     begin
          if DetectarMaestroProxy then
          begin
               FEsAlta := False;
               PreparaQuery( tqModificaSistema );
               //PreparaQuery( tqAgregaAutoriza );
               FPrevioSentinela := 0;
               FNuevoSentinela := 0;
               BookMarkGet;
               PosicionaSistema( FolioActual );
               PosicionaActual( FolioActual );
               //LeeDatosPrevios;
               tAutoriza.Edit;
               tSistema.Edit;
          end
          else
          begin
               raise Exception.Create( StatusMsg );
          end; 
     end;
end;

procedure TdmSentinel.BorraSistema;
begin
     try
        with AutoMaster do
        begin
             if DetectarMaestroProxy then
             begin
                  FEsAlta := False;
                  with dbSentinel do
                  begin
                       StartTransaction;
                       try
                          PreparaQuery( tqBorraSistema );
                          with tqBorraSistema do
                          begin
                               Params[ 0 ].AsInteger := FolioActual;
                               ExecSQL;
                          end;
                          Commit;
                       except
                             on Error: Exception do
                             begin
                                  Rollback;
                                  Application.HandleException( Error );
                             end;
                       end;
                  end;
                  {
                  PosicionaActual( FolioActual );
                  }
                  Refrescar;
             end
             else
             begin
                  raise Exception.Create( StatusMsg );
             end;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TdmSentinel.GrabaCambios;
var
   iFolio, iSerieDB: Integer;
   sClave1, sClave2: String;
   lGrabar: Boolean;
begin
     FAgregaAuto := False;
     with tAutoriza do
     begin
          iFolio := FieldByName( 'SI_FOLIO' ).AsInteger;
          iSerieDB := FieldByName( 'SN_NUMERO' ).AsInteger;
          lGrabar := True;
          if ( FieldByName( 'AU_DEFINIT' ).AsInteger = K_INT_TRUE ) then
             FieldByName( 'AU_FEC_AUT' ).AsDateTime := EncodeDate( 1899, 12, 30 );
          if not HayPrestados then
             FieldByName( 'AU_FEC_PRE' ).AsDateTime := EncodeDate( 1899, 12, 30 );
          ValidaKit; {se ocup� debido a dbcheckboxes que no se graban sin click 'manual' �por cached updates?}
          if FEsAlta then
          begin
               FieldByName( 'AU_TIPOS' ).AsString := T_ALTA;
          end
          else
          begin
               if CambiaronModulos then
               begin
                    {
                    Sentinel := FieldByName( 'SN_NUMERO' ).AsInteger;
                    }
                    FieldsToMaster;
                    while lGrabar and not AutoMaster.CalculaClaves( FieldByName( 'SN_NUMERO' ).AsInteger, sClave1, sClave2 ) do
                    begin
                         if not ZetaDialogo.zConfirm( '� Atenci�n !', 'No Se Encontr� El Sentinel Maestro' + CR_LF + '� Desea Reintentar ?', 0, mbYes ) then
                         begin
                              CancelaCambios;
                              lGrabar := False;
                         end
                         else
                         begin
                              if ( sClave1 = '' ) or ( sClave2 = '' ) then
                              begin
                                   if not ZetaDialogo.zConfirm( '� Atenci�n !', 'Las Claves No Fueron Calculadas' + CR_LF + '� Desea Reintentar ?', 0, mbYes ) then
                                      CancelaCambios;
                                   lGrabar := False;
                              end
                              else
                                  if not ValidaClaves( iFolio, sClave1, sClave2 ) then
                                  begin
                                       if not ZetaDialogo.zConfirm( '� Atenci�n !', 'Las Claves Ya Existen' + CR_LF + '� Desea Reintentar ?', 0, mbYes ) then
                                          CancelaCambios;
                                       lGrabar := False;
                                  end;
                         end;
                    end;
                    if lGrabar then
                    begin
                         FieldByName( 'AU_PAR_AUT' ).AsInteger := AutoMaster.Modulos;
                         FieldByName( 'AU_PAR_PRE' ).AsInteger := AutoMaster.Prestamos;
                         FieldByName( 'AU_CLAVE1' ).AsString := sClave1;
                         FieldByName( 'AU_CLAVE2' ).AsString := sClave2;
                         FieldByName( 'AU_TIPOS' ).AsString := T_AUTORIZACION;
                         FAgregaAuto := True;
                    end;
               end
               else
                   if CambioGeneral then
                   begin
                        FieldByName( 'AU_TIPOS' ).AsString := T_GENERAL;
                        FAgregaAuto := True;
                   end;
          end;
          if lGrabar then
          begin
               try
                  if ( State in [ dsEdit, dsInsert ] ) then
                     Post;
                  DataBase.ApplyUpdates( [ tqActual ] );
                  try
                     if FEsAlta then
                        GrabarSentinelSistema( iFolio, iSerieDB );
                  except
                        on Error: Exception do
                        begin
                             ZetaDialogo.ZExcepcion( '� Error !', 'Error Al Iniciar Sentinela', Error, 0 );
                        end;
                  end;
                  FEsAlta := False;
               except
                     on Error: Exception do
                     begin
                          ZetaDialogo.ZExcepcion( '� Error !', 'Error Al Escribir Datos', Error, 0 );
                     end;
               end;
          end;
          Active := False;
     end;
     Refrescar;
     BookMarkGoTo;
end;

procedure TdmSentinel.CancelaCambios;
begin
     BookMarkRelease;
     with tqActual do
     begin
          if Active then
          begin
               if ( State in [ dsEdit, dsInsert ] ) then
                  Cancel;
               if UpdatesPending then
                  CancelUpdates;
               Active := False;
          end;
     end;
end;

procedure TdmSentinel.AU_COMENTAChange(Sender: TField);
begin
     FAUComentaChanged := True;
end;

function TdmSentinel.Cambio( const sNombre: String ): Boolean;
begin
     if ( sNombre = 'AU_COMENTA' ) then
        Result := FAUComentaChanged
     else
     begin
          with tAutoriza.FieldByName( sNombre ) do
          begin
               Result := ( Value <> OldValue );
          end;
     end;
end;

function TdmSentinel.CambiaronModulos: Boolean;

function CambioModulos( const sPrefix: String ): Boolean;
var
   i: Integer;
begin
     i := 0;
     Result := False;
     while not Result and ( i <= Ord( High( TModulos ) ) ) do
     begin
          Result := Result or Cambio( Format( sPrefix, [ i ] ) );
          i := i + 1;
     end;
end;

begin
     Result := Cambio( 'AU_NUM_EMP' ) or
               Cambio( 'AU_PLATFORM' ) or
               Cambio( 'AU_SQL_BD' ) or
               Cambio( 'AU_USERS' ) or
               Cambio( 'AU_CLAVE1' ) or
               Cambio( 'AU_CLAVE2' ) or
               Cambio( 'AU_FEC_AUT' ) or
               Cambio( 'AU_FEC_PRE' ) or
               Cambio( 'AU_KIT_DIS' ) or
               Cambio( 'AU_DEFINIT' ) or
               Cambio( 'AU_VAL_LIM' ) or
               Cambio( 'AU_BD_LIM' ) or
               Cambio( 'SN_NUMERO' ) or
               Cambio( 'VE_CODIGO' ) or
               CambioModulos( 'AU_MOD_%d' ) or
               CambioModulos( 'AU_PRE_%d' );
end;

function TdmSentinel.CambioGeneral: Boolean;
begin
     Result := Cambio( 'DI_CODIGO' ) or
               Cambio( 'AU_COMENTA' );
end;

procedure TdmSentinel.tqActualUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TUpdateAction);
begin
     try
        case UpdateKind of
             ukModify:
             begin
                  CopiaParams( DataSet, tqModificaSistema );
                  tqModificaSistema.ExecSQL;
                  if FAgregaAuto then
                  begin
                       CopiaParams( DataSet, tqAgregaAutoriza );
                       tqAgregaAutoriza.ExecSQL;
                  end;
             end;
             ukInsert:
             begin
                  CopiaParams( DataSet, tqAgregaSistema );
                  tqAgregaSistema.ExecSQL;
                  //CopiaParams( DataSet, tqAgregaAutoriza );
                  //tqAgregaAutoriza.ExecSQL;
             end;
             ukDelete:
             begin
                  {
                  tqBorraSistema.Params[ 0 ].AsInteger := FolioActual;
                  tqBorraSistema.ExecSQL;
                  }
             end;
        end;
        UpdateAction := uaApplied;
     except
           raise;
     end;
end;

procedure TdmSentinel.CopiaParams( Fuente: TDataSet; Destino: TQuery );
var
   i: Integer;
   Campo: TField;
   sNombre: String;
begin
     with Destino do
     begin
          for i := 0 to ( ParamCount - 1 ) do
          begin
               sNombre := Params[ i ].Name;
               Campo := Fuente.FindField( sNombre );
               if ( Campo <> nil ) then
               begin
                    case Campo.DataType of
                         ftString:
                           Params[ i ].AsString := Campo.AsString;
                         ftSmallint, ftInteger, ftWord :
                           Params[ i ].AsInteger := Campo.AsInteger;
                         ftFloat, ftCurrency, ftBCD:
                           Params[ i ].AsFloat := Campo.AsFloat;
                         ftDate, ftTime, ftDateTime:
                           Params[ i ].AsDateTime := Campo.AsDateTime;
                    else
                        Params[ i ].AsMemo := Campo.AsString;
                    end;
               end
               else
                   ShowMessage( sNombre );
          end;
     end;
end;

procedure TdmSentinel.MarcaFechaHora;
begin
     with tAutoriza do
     begin
          FieldByName( 'AU_FECHA' ).AsDateTime := Date();
          FieldByName( 'US_CODIGO' ).AsString := FUsuario;
     end;
end;

procedure TdmSentinel.tqActualAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'AU_COMENTA' ).OnChange := AU_COMENTAChange;
     end;
end;

procedure TdmSentinel.tqActualAfterEdit(DataSet: TDataSet);
begin
     with DataSet do
     begin
          // Incrementa el Folio de la Historia
          FieldByName( 'AU_FOLIO' ).AsInteger := FieldByName( 'AU_FOLIO' ).AsInteger + 1;
          MarcaFechaHora;
     end;
end;

procedure TdmSentinel.tqActualNewRecord(DataSet: TDataSet);
var
   i: Integer;
begin
     with DataSet do
     begin
          //tqSistema.Last;  // Para obtener el MAX(SI_FOLIO) + 1
          FieldByName( 'SI_FOLIO' ).AsInteger := tSistema.FieldByName( 'SI_FOLIO' ).AsInteger;
          // Como es ALTA, siempre es 1
          FieldByName( 'AU_FOLIO' ).AsInteger := 1;
          FieldByName( 'AU_KIT_DIS' ).AsInteger := K_INT_FALSE;
          // Estas constantes podr�an ser configurables
          FieldByName( 'GR_CODIGO' ).AsString := 'GRAL';
          FieldByName( 'CT_CODIGO' ).AsString := 'TIJ';
          FieldByName( 'DI_CODIGO' ).AsString := '';
          FieldByName( 'AU_USERS' ).AsInteger := K_USUARIOS_KIT;
          FieldByName( 'VE_CODIGO' ).AsString := K_VERSION_20_TXT;
          FieldByName( 'AU_PLATFORM' ).AsInteger := Ord( ptProfesional );
          FieldByName( 'AU_SQL_BD' ).AsInteger := Ord( engInterbase );
          // Defaults para los MODULOS
          FieldByName( 'AU_NUM_EMP' ).AsInteger := 300;
          FieldByName( 'AU_MOD_0' ).AsInteger := K_INT_TRUE;
          FieldByName( 'AU_PRE_0' ).AsInteger := K_INT_FALSE;
          for i := 1 to Ord( High( TModulos ) ) do
          begin
               FieldByName( Format( 'AU_MOD_%d', [ i ] ) ).AsInteger := K_INT_FALSE;
               FieldByName( Format( 'AU_PRE_%d', [ i ] ) ).AsInteger := K_INT_FALSE;
          end;
          // Fecha, Hora y Usuario que AGREGO.
          MarcaFechaHora;
          FieldByName( 'AU_DEFINIT' ).AsInteger := K_INT_FALSE;
          FieldByName( 'AU_FEC_AUT' ).AsDateTime := Date() + 8;
          FieldByName( 'AU_FEC_PRE' ).AsDateTime := Date() + 8;
          FieldByName( 'SI_FEC_SOL' ).AsDateTime := Date();
          FieldByName( 'SN_NUMERO' ).AsInteger := FNuevo;
          FieldByName( 'SI_MIGRADO' ).AsInteger := K_INT_FALSE;
          FieldByName( 'SI_PROBLEMA' ).AsInteger := K_INT_FALSE;
     end;
     FAUComentaChanged := False;
end;

procedure TdmSentinel.tqActualBeforeEdit(DataSet: TDataSet);
begin
     FAUComentaChanged := False;
end;

procedure TdmSentinel.RefrescarLook;
var
   sFiltro: String;
begin
     sFiltro := FFiltro;
     with tqSistemaLook do
     begin
          Active := False;
          FilterReset( False );
          Active := True;
     end;
     FilterSet( sFiltro );
     BookMarkGoto;
end;

procedure TdmSentinel.RefrescarAutorizacion;
begin
     with tAutoriza do
     begin
          DisableControls;
          Active := False;
          Active := True;
          EnableControls;
     end;
     

end;

procedure TdmSentinel.AgregaSentinel( const iPrimero, iUltimo: Integer; const dStart: TDate; const iTipo: Integer );
var
   i, iVeces: Integer;
   Query:TQuery;
begin
     iVeces := iUltimo - iPrimero;
     Query := TQuery.Create(Self);
     Query.DatabaseName := 'dbSentinel';
     try
        for i := 0 to iVeces do
        begin
             {with tSentinel do
             begin
                  Active := True;
                  try
                     AppendRecord( [ iPrimero + i, 'Uno', dStart, iTipo,'','',0,1 ] );
                  except
                        on Error: EDatabaseError do
                        begin
                             ZetaDialogo.ZExcepcion( '� Hubo Errores !', 'Verifique La Numeraci�n', Error, 0 );
                             Break;
                        end;
                        on Error: Exception do
                        begin
                             ZetaDialogo.ZExcepcion( '� Hubo Errores !', 'Hubo Errores Al Grabar Los N�meros De Sentinelas', Error, 0 );
                             Break;
                        end;
                  end;
                  Active := False;
             end;}
             Query.SQL.Text := Format('INSERT INTO SENTINEL (SN_NUMERO,SN_MODELO,SN_FEC_INI,SN_TIPO,SN_SERVER,SN_NOMBRE,SI_FOLIO,SN_ACTIVO) VALUES '+
                                              ' ( %d,%s,%s,%d,%s,%s,%d,%d )',[iPrimero + i,EntreComillas('Uno'),DateToStrSQLC(dStart),iTipo,EntreComillas(''),EntreComillas(''),0,1]);
             try
                Query.ExecSQL;
             except
                   on Error: EDatabaseError do
                   begin
                        ZetaDialogo.ZExcepcion( '� Hubo Errores !', 'Verifique La Numeraci�n', Error, 0 );
                        Break;
                   end;
             end;
        end;
     finally
            Query.Free; 
     end;
end;

procedure TdmSentinel.LeeDatosPrevios;
begin
     FPrevioSentinela := tqActual.FieldByName('SN_NUMERO').asInteger;
end;

procedure TdmSentinel.ActualizaSentinel;
begin
     with qSentinel do
     begin
          if Locate( 'SN_NUMERO', FPrevioSentinela, [] ) then
          begin
               if ( State <> dsEdit ) then
                  Edit;
               FieldByName( 'SN_DISPO' ).AsString := 'S';
               Post;
          end;
          if Locate( 'SN_NUMERO', FNuevoSentinela, [] ) then
          begin
               if ( State <> dsEdit ) then
                  Edit;
               FieldByName( 'SN_DISPO' ).AsString := 'N';
               Post;
          end;
     end;
end;

function TdmSentinel.ValidaSentinela( const iCodigo, iSistema: Integer; var sMensaje: String ): Boolean;
begin
     sMensaje := '';
     Result := False;
     with tqExisteSentinel do
     begin
          Active := False;
          ParamByName( 'SN_NUMERO' ).AsInteger := iCodigo;
          Active := True;
          if IsEmpty then
             sMensaje := 'Sentinela No Ha Sido Dado De Alta'
          else
          begin
               Result := ( Fields[ 0 ].AsInteger = 1 );
               if not Result then
                  sMensaje := 'Sentinela No Ha Sido Agregado Al Cat�logo';
          end;
          Active := False;
     end;
     if Result then
     begin
          with tqValidaSentinel do
          begin
               Active := False;
               ParamByName( 'SN_NUMERO' ).AsInteger := iCodigo;
               ParamByName( 'SI_FOLIO' ).AsInteger := iSistema;
               Active := True;
               if not IsEmpty then
               begin
                    Result := ( Fields[ 0 ].AsInteger = 0 );
                    if not Result then
                       sMensaje := 'Sentinela No Disponible ( Asignado a Otro Sistema )';
               end;
               Active := False;
          end;
     end;
end;

function TdmSentinel.ValidaSentinel( const iCodigo, iSistema: Integer; var sMensaje: String ): Boolean;
begin
     Result := ValidaSentinela( iCodigo, iSistema, sMensaje );
     if Result then
        FNuevoSentinela := iCodigo;
end;

Procedure TdmSentinel.LeerSentinel;
var
   sCaption: String;
begin
     with AutoMaster do
     begin
          if Cargar and SentinelOK then
          begin
               with tqRazonSocial do
               begin
                    Active := False;
                    ParamByName( 'SI_FOLIO' ).AsInteger := Empresa;
                    Active := True;
                    if IsEmpty then
                       sCaption := 'No Est� Registrado Este N�mero De Sistema'
                    else
                        sCaption := FieldByName( 'SI_RAZ_SOC' ).AsString;
                    Active := False;
               end;
               with TLecturaSen.Create( Self ) do
               begin
                    try
                       Nombre.Caption := sCaption;
                       Caption := 'Lectura de Sentinel';
                       ShowModal;
                    finally
                           Free;
                    end;
               end;
          end
          else
          begin
               case Length( StatusMsg ) of
                    31: ZetaDialogo.zError( '� Error !', 'No Se Ha Conectado Un Sentinel V�lido' + CR_LF + StatusMsg, 0 );
                    36: ZetaDialogo.zError( '� Error !', 'Sentinel #  ' + IntToStr( NumeroSerie ) + ' No Ha Sido Inicializado' + CR_LF + StatusMsg, 0 );
               else
                   ZetaDialogo.zError( '� Error !', 'Sentinela Inv�lido' + CR_LF + StatusMsg, 0 );
               end;
          end;
     end;
end;

procedure TdmSentinel.GrabarSentinelSistema( const iFolio, iSerieDB: Integer );
var
   iSerie: Integer;
begin
     with AutoMaster do
     begin
          iSerie := GetSerialNumber;
          if ( iSerie > 0 ) then
          begin
               if ( iSerie = iSerieDB ) then
               begin
                    FieldsToMaster;
                    if Escribir then
                       ZetaDialogo.zInformation( '� Atenci�n !', 'El Sentinel Se Grab� Exitosamente', 0 )
                    else
                        ZetaDialogo.zError( '� Atenci�n !', Format( 'Error Al Escribir Sentinela: %s', [ StatusMsg ] ), 0 );
               end
               else
                   ZetaDialogo.zError( '� Error !', 'Los N�meros De Sentinel No Corresponden' + CR_LF + 'No Se Permite Grabar', 0 );
          end
          else
              ZetaDialogo.zError( '� Error !', 'Sentinel Inv�lido' + CR_LF + StatusMsg, 0 );
     end;
end;

procedure TdmSentinel.GrabarSentinel;
var
   iFolio: Integer;
   oCursor: TCursor;
begin
     with AutoMaster do
     begin
          if DetectarMaestroProxy then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               iFolio := GetFolioSistema;
               try
                  PosicionaActual( iFolio );
                  GrabarSentinelSistema( iFolio, GetSentinelDB )
               finally
                      tqActual.Active := False;
                      Screen.Cursor := oCursor;
               end;
          end
          else
              raise Exception.Create( StatusMsg );
     end;
end;

procedure TdmSentinel.FieldsToMaster;
var
   eModulo: TModulos;
begin
     with AutoMaster do
     begin
          with tAutoriza do
          begin
               Empresa := FieldByName( 'SI_FOLIO' ).AsInteger;
               Empleados :=  FieldByName( 'AU_NUM_EMP' ).AsInteger;
               { Lo siguiente es producto de left outer join }
               Version := FieldByName( 'VE_CODIGO' ).AsString;
               Vencimiento := FieldByName( 'AU_FEC_AUT' ).AsDateTime;
               Caducidad := FieldByName( 'AU_FEC_PRE' ).AsDateTime;
               EsKit := ( FieldByName( 'AU_KIT_DIS' ).AsInteger <> 0 );
               Usuarios := FieldByName( 'AU_USERS' ).AsInteger;
               Plataforma := TPlataforma( FieldByName( 'AU_PLATFORM' ).AsInteger );
               SQLEngine := TSQLEngine( FieldByName( 'AU_SQL_BD' ).AsInteger );
               IniciaValidacion := FieldByName( 'AU_VAL_LIM' ).AsInteger;
               LimiteConfigBD := FieldByName( 'AU_BD_LIM' ).AsInteger;


               for eModulo := Low( TModulos ) to High( TModulos ) do
               begin
                    SetModulos( eModulo, FieldByName( Format( 'AU_MOD_%d', [ Ord( eModulo ) ] ) ).AsInteger );
                    SetPrestamos( eModulo, FieldByName( Format( 'AU_PRE_%d', [ Ord( eModulo ) ] ) ).AsInteger );
               end;
          end;
     end;
end;

procedure TdmSentinel.ValidaKit;
var
   i: Integer;
begin
     with tAutoriza do
     begin
          if ( FieldByName( 'AU_KIT_DIS' ).AsInteger = K_INT_TRUE ) then
          begin
               i := 0;
               while ( i <= Ord( High( TModulos ) ) ) do
               begin
                    FieldByName( Format( 'AU_MOD_%d', [ i ] ) ).AsInteger := K_INT_TRUE;
                    i := i + 1
               end;
               FieldByName( 'AU_DEFINIT' ).AsInteger := K_INT_FALSE;
               if ( FieldByName( 'VE_CODIGO' ).AsString = '3.0' ) then
               begin
                    if (FieldByName( 'AU_NUM_EMP' ).AsInteger = K_NO_EMPLOYEE_LIMIT) then
                    begin
                         FieldByName( 'AU_NUM_EMP' ).AsInteger := K_DEMO_EMPLOYEES;
                    end
               end
               else
                   FieldByName( 'AU_NUM_EMP' ).AsInteger := K_NO_EMPLOYEE_LIMIT;

               FieldByName( 'AU_USERS' ).AsInteger := K_USUARIOS_KIT;
               if ( FieldByName('AU_FEC_AUT' ).AsDateTime = NullDateTime ) or ( FieldByName( 'AU_FEC_AUT' ).AsDateTime = Date() + 8 ) then
                  FieldByName( 'AU_FEC_AUT' ).AsDateTime := Date() + 31;
          end;
     end;
end;

function TdmSentinel.ValidaCampos: Boolean;
const
     K_MENSAJE = 'La cantidad de empleados ( %0:d ) no aparece en la lista preautorizada%1:s%1:s� Desea continuar?';
var
   sMensaje: String;
   i, iEmpleados, iNivelEmpleados: Integer;
   lFound: Boolean;
begin
     try
        with tAutoriza do
        begin
             if ZetaCommonTools.StrVacio( FieldByName( 'VE_CODIGO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar la VERSION' );
             if ZetaCommonTools.StrVacio( FieldByName( 'SN_NUMERO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar el NUMERO DE SENTINEL' );

             //if FEsAlta and not ValidaSentinela( FieldByName( 'SN_NUMERO' ).AsInteger, sMensaje ) then
             //   raise Exception.Create( sMensaje );

             if ( ( FieldByName( 'AU_DEFINIT' ).AsInteger = K_INT_TRUE ) and ( tSentinel.FieldByName('SN_SERVER').AsString  <> '' ) ) then
             begin
                   raise Exception.Create( 'No se puede generar una autorizaci�n Definitiva a un sentinel virtual' );
             end;

             if ( FieldByName( 'AU_DEFINIT' ).AsInteger = K_INT_FALSE ) then
             begin
                  if ( FieldByName( 'AU_FEC_AUT').AsDatetime = NullDateTime ) then
                     FieldByName( 'AU_FEC_AUT' ).AsDatetime := Date() + 8
                  else
                      if ( ( tSentinel.FieldByName('SN_SERVER').AsString  <> '' ) and ( (Date + 365)  < FieldByName( 'AU_FEC_AUT').AsDateTime ) )then
                         raise Exception.Create( 'No se puede generar una autorizaci�n a un sentinel virtual mayor a un A�o' );


                  if ( FieldByName( 'AU_FEC_PRE').AsDatetime = NullDateTime ) and HayPrestados then
                     FieldByName( 'AU_FEC_PRE' ).AsDatetime := Date() + 8;
                  if ( ( tSentinel.FieldByName('SN_SERVER').AsString = '' ) and ( FieldByName( 'AU_FEC_AUT' ).AsDatetime < Date ) ) then
                     raise Exception.Create( 'Fecha De Vencimiento Ya Caduc�' );
                  if   ( tSentinel.FieldByName('SN_SERVER').AsString = '' )and(  FieldByName( 'AU_FEC_PRE' ).AsDatetime < Date ) and HayPrestados then
                     raise Exception.Create( 'Fecha De Caducidad de Pr�stamos Ya Venci�' );
             end;
             if not DSentinel.EsVer20( FieldByName( 'VE_CODIGO' ).AsString ) then
                FieldByName( 'AU_PLATFORM' ).AsInteger := Ord( ptProfesional );

             //Cambio Pro MSSQL
             //if ( FieldByName( 'AU_PLATFORM' ).AsInteger = Ord( ptProfesional ) ) then
                //FieldByName( 'AU_SQL_BD' ).AsInteger := Ord( engInterbase );

             if not DSentinel.EsVerValidacion( FieldByName( 'VE_CODIGO' ).AsString) then
             begin
                  FieldByName( 'AU_BD_LIM' ).AsInteger := 0;
                  FieldByName( 'AU_VAL_LIM' ).AsInteger := 0;
             end;

             with FieldByName( 'SN_NUMERO' ) do
             begin

                  if ValidaSentinel( AsInteger, FieldByName( 'SI_FOLIO' ).AsInteger, sMensaje ) then
                     Result := True
                  else
                  begin
                       ZetaDialogo.zError( '� Error !', sMensaje + CR_LF + 'Debe Indicar Un N�mero V�lido', 0 );
                       FocusControl;
                       Result := False;
                  end;
             end;
             if Result then
             begin
                  iEmpleados := FieldByName( 'AU_NUM_EMP' ).AsInteger;
                  i := 0;
                  iNivelEmpleados := UnLoadEmpleados( i );
                  lFound := False;
                  while ( iNivelEmpleados > 0 ) and ( iNivelEmpleados <> K_TOP_EMPLOYEES ) and not lFound do
                  begin
                       lFound := ( iNivelEmpleados = iEmpleados );
                       Inc( i );
                       iNivelEmpleados := UnLoadEmpleados( i );
                  end;
                  if not lFound and not ZetaDialogo.ZConfirm( '� Atenci�n !', Format( K_MENSAJE, [ iEmpleados, CR_LF ] ), 0, mbNo ) then
                  begin
                       Result := False;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

function TdmSentinel.ValidaClaves( const iSistema: Integer; const sClave1, sClave2: String ): Boolean;
begin
     with tqExistenClaves do
     begin
          Active := False;
          ParamByName( 'SI_FOLIO' ).AsInteger := iSistema;
          ParamByName( 'AU_CLAVE1' ).AsString := sClave1;
          ParamByName( 'AU_CLAVE2' ).AsString := sClave2;
          Active := True;
          Result := ( Fields[ 0 ].AsInteger = 0 );
          Active := False;
     end;
end;

function TdmSentinel.SentinelConectado: Boolean;
var
   iSerial, iSerie: Integer;
begin
     //if FEsAlta then
     //begin
          iSerial := tSentinel.FieldByName( 'SN_NUMERO' ).AsInteger;
          with AutoMaster do
          begin
               iSerie := GetSerialNumber;
               if ( iSerie = 0 ) then
                  raise Exception.Create( StatusMsg )
               else
                   if ( iSerie <> iSerial ) then
                      raise Exception.Create( '# de Serie Especificado ( ' + IntToStr( iSerial ) + ' ) No Corresponde Al Del Sentinela ( ' + IntToStr( iSerie ) + ' )' )
                   else
                       Result := True;
          end;
     {end
     else
         Result := True;}
end;

procedure TdmSentinel.tqAutorizaAU_FEC_AUTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   dValue: TDate;
begin
     if DisplayText then
     begin
          dValue := Sender.AsDateTime;
          if ( dValue = NullDateTime ) then
             Text := 'Definitiva'
          else
              Text := FormatDateTime( 'dd/mmm/yyyy', dValue );
     end;
end;

procedure TdmSentinel.tqAutorizaAU_FEC_PREGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   dValue: TDate;
begin
     if DisplayText then
     begin
          dValue := Sender.AsDateTime;
          if ( dValue = NullDateTime ) then
             Text := 'No Tiene'
          else
              Text := FormatDateTime( 'dd/mmm/yyyy', dValue );
     end;
end;

procedure TdmSentinel.tqAutorizaAU_KIT_DISGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if ( Sender.AsInteger = K_INT_FALSE ) then
             Text := ' '
          else
              Text := 'Si';
     end;
end;

procedure TdmSentinel.tqAutorizaAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'AU_FECHA' ).OnGetText := CampoFechaGetText;
          FieldByName( 'AU_FEC_AUT' ).OnGetText := tqAutorizaAU_FEC_AUTGetText;
          FieldByName( 'AU_FEC_PRE' ).OnGetText := tqAutorizaAU_FEC_PREGetText;
          FieldByName( 'AU_KIT_DIS' ).OnGetText := tqAutorizaAU_KIT_DISGetText;
     end;
end;

procedure TdmSentinel.qSentinelAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'SN_FEC_INI' ).OnGetText := CampoFechaGetText;
          FieldByName( 'SN_ACTIVO' ).OnGetText := SentinelActivoGetText;
          FieldByName( 'SN_TIPO' ).OnGetText := TipoSentinelGetText;
     end;
end;

procedure TdmSentinel.tqInstalacionesAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'IN_FECHA' ).OnGetText := CampoFechaGetText;
     end;
end;

procedure TdmSentinel.ListaDistribuidores( Lista: TStrings );
begin
     with Lista do
     begin
          Clear;
          BeginUpdate;
          try
             with tDistribu do
             begin
                  Active := True;
                  DisableControls;
                  try
                     while not Eof do
                     begin
                          Add( Format( '%s=%s', [ FieldByName( 'DI_CODIGO' ).AsString, FieldByName( 'DI_NOMBRE' ).AsString  ] ) );
                          Next;
                     end;
                  finally
                         EnableControls;
                  end;
                  Active := False;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmSentinel.InstalacionNueva( const iSistema: Integer; var iSentinel, iInstalacion: Integer; var sDistribuidor: String );
begin
     iSentinel := 0;
     iInstalacion := 0;
     sDistribuidor := '';
     if ActualAbrir( iSistema ) then
     begin
          with tqActual do
          begin
               iSentinel := FieldByName( 'SN_NUMERO' ).AsInteger;
               sDistribuidor := FieldByName( 'DI_CODIGO' ).AsString;
          end;
          with tqMaxInstalacion do
          begin
               Active := False;
               ParamByName( 'SI_FOLIO' ).AsInteger := iSistema;
               ParamByName( 'SN_NUMERO' ).AsInteger := iSentinel;
               Active := True;
               iInstalacion := ZetaCommonTools.iMax( 0, Fields[ 0 ].AsInteger ) + 1;
               Active := False;
          end;
     end;
     ActualCerrar;

end;

function TdmSentinel.InstalacionGenerar( const iSistema, iSentinel, iInstalacion: Integer; const sDistribuidor, sComentario: String; var sClave: String ): Boolean;
var
   lAsignado: Boolean;
begin
     Result := False;
     try
        with AutoMaster do
        begin
             if DetectarMaestroProxy then
             begin
                  with tqUsoSentinel do
                  begin
                       Active := False;
                       ParamByName( 'SI_FOLIO' ).AsInteger := iSistema;
                       ParamByName( 'SN_NUMERO' ).AsInteger := iSentinel;
                       Active := True;
                       lAsignado := ( Fields[ 0 ].AsInteger > 0 );
                       Active := False;
                  end;
                  if not lAsignado then
                  begin
                       raise Exception.Create( Format( 'El Sentinel %d No Ha Sido Asignado Al Sistema # %d', [ iSentinel, iSistema ] ) );
                  end;
                  sClave := CalculaClaveInstalacion( iSistema, iSentinel, iInstalacion );
                  with dbSentinel do
                  begin
                       StartTransaction;
                       try
                          PreparaQuery( tqAddInstalacion );
                          with tqAddInstalacion do
                          begin
                               ParamByName( 'SI_FOLIO' ).AsInteger := iSistema;
                               ParamByName( 'SN_NUMERO' ).AsInteger := iSentinel;
                               ParamByName( 'IN_FOLIO' ).AsInteger := iInstalacion;
                               ParamByName( 'US_CODIGO' ).AsString := FUsuario;
                               ParamByName( 'IN_FECHA' ).AsDateTime := Trunc( Now );
                               ParamByName( 'DI_CODIGO' ).AsString := sDistribuidor;
                               ParamByName( 'IN_CLAVE' ).AsString := sClave;
                               ParamByName( 'IN_COMENTA' ).AsString := sComentario;
                               ExecSQL;
                          end;
                          Commit;
                          Result := True;
                       except
                             on Error: Exception do
                             begin
                                  Rollback;
                                  Application.HandleException( Error );
                             end;
                       end;
                  end;
             end
             else
             begin
                  raise Exception.Create( StatusMsg );
             end;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TdmSentinel.tTerminalesBeforePost(DataSet: TDataSet);
var
   sMensaje:string;
begin
     with tTerminales do
     begin
          if FieldByName('TE_NSERIE').AsString = '' then
          begin
               DatabaseError('N�mero de Serie No puede Quedar Vac�o');
          end;

          //FieldByName('SN_NUMERO').AsInteger := tqActual.FieldByName('SN_NUMERO').AsInteger;
          FieldByName('SI_FOLIO').AsInteger := tqActual.FieldByName('SI_FOLIO').AsInteger;
          FieldByName('TE_TIPO').AsInteger := FTipoTerminal;

          if FieldByName('SN_NUMERO').AsInteger = 0  then
          begin
               DatabaseError('N�mero de Sentinel Debe Ser Mayor a Cero');
          end;

          if Not ValidaSentinela ( FieldByName('SN_NUMERO').AsInteger, FieldByName('SI_FOLIO').AsInteger ,sMensaje )then
          begin
               if not ZetaDialogo.ZWarningConfirm('Confirmaci�n de Sentinel',Format('%s  � Quieres Guardarlo ?',[sMensaje]),0,mbYes)then
               begin
                    DatabaseError('Registro Cancelado');
               end;
          end;
     end;
end;

procedure TdmSentinel.tTerminalesNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName('TE_ACTIVO').AsString := 'S';
          FieldByName('TE_NSERIE').FocusControl;
          FieldByName('TE_TIPO').AsInteger := 0;
          FieldByName('SN_NUMERO').AsInteger := FormSistema.zSentinelas.LlaveEntero;
     end;
end;

procedure TdmSentinel.ActualizaSentinelTerminal(const NumAnterior: Integer);
begin
     with dbSentinel do
     begin
           StartTransaction;
           try
              PreparaQuery( tqActualizaTerminal );
              with tqActualizaTerminal do
              begin
                   ParamByName('SentAnterior').AsInteger := NumAnterior;
                   ParamByName('Sentinel').AsInteger := tqActual.fieldbyname('SN_NUMERO').AsInteger;
                   ParamByName('SI_FOLIO').AsInteger := tqActual.fieldbyname('SI_FOLIO').AsInteger;
                   ExecSQL;
              end;
              Commit;
            except
                  on Error: Exception do
                  begin
                       Rollback;
                       Application.HandleException( Error );
                  end;
            end;
     end;
end;

procedure TdmSentinel.ActualizaSentinelVirtual(const Sentinel: Integer;const Server: string);
begin
     with dbSentinel do
     begin
           StartTransaction;
           try
              PreparaQuery( tqSentVirtual);
              with tqSentVirtual do
              begin
                   ParamByName('Sentinel').AsInteger := Sentinel;
                   ParamByName('Server').AsString := Server;
                   ExecSQL;
              end;
              Commit;
            except
                  on Error: Exception do
                  begin
                       Rollback;
                       Application.HandleException( Error );
                  end;
            end;
     end;
end;

function TdmSentinel.GetServerVirtual(const iSentinel: Integer): string;
begin
     with dbSentinel do
     begin
           try
              PreparaQuery( tqServerVirt);
              with tqServerVirt do
              begin
                   ParamByName('Sentinel').AsInteger := iSentinel;
                   Active := True;
                   Result := ( Fields[ 0 ].AsString );
                   Active := False;
              end;
            except
                  on Error: Exception do
                  begin
                       Rollback;
                       Application.HandleException( Error );
                  end;
            end;
     end;
end;

function TdmSentinel.DetectarMaestroProxy: Boolean;
begin
{$ifdef SIN_MAESTRO}
     Result := TRUE;
{$else}
     Result := AutoMaster.DetectarMaestro;
{$endif}
end;

procedure TdmSentinel.AgregaHistorico3DT(iSN_NUMERO, iSI_FOLIO: integer;
  dFechaDoc: TDateTime; sComentarios, sArchivo: string);
begin
     with tqAgregaHistorico3DT do
     begin
          ParamByName( 'SN_NUMERO' ).AsInteger := iSN_NUMERO;
          ParamByName( 'SI_FOLIO' ).AsInteger := iSI_FOLIO;
          ParamByName( 'HI_FEC_DOC' ).AsDateTime := dFechaDoc;
          ParamByName( 'HI_FEC_ING' ).AsDateTime := Now;
          ParamByName( 'HI_COMENTA' ).AsString := sComentarios;
          ParamByName( 'HI_3DTXML' ).LoadFromFile( sArchivo, ftBlob );
          ParamByName( 'HI_NOMBRE' ).AsString := sArchivo;          
          ExecSQL;
     end;
end;

procedure TdmSentinel.tTerminalesAfterOpen(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'TE_TIPO' ).OnGetText := tqTerminalesTE_TIPOGetText;
     end;
end;

procedure TdmSentinel.tqTerminalesTE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          case Sender.AsInteger of 
          0 : Text := 'Terminal ZK';
          1 : Text := 'SYNEL';
          else
              Text := 'Desconocida';
          end;
     end;
end;

procedure TdmSentinel.GetTerminalesBusqueda(dsTerminales:TDataSource;const Pista:String );
begin
     with dbSentinel do
     begin
           try
              PreparaQuery( tqBuscaTerminal);
              with tqBuscaTerminal do
              begin
                   Active:= False;
                   ParamByName('Pista1').AsString:= '%'+Pista+'%';
                   ParamByName('Pista2').AsString:= '%'+Pista+'%';
                   ParamByName('Pista3').AsString:= '%'+Pista+'%';
                   Active := True;
                   dsTerminales.DataSet := tqBuscaTerminal;
              end;
            except
                  on Error: Exception do
                  begin
                       Application.HandleException( Error );
                  end;
            end;
     end;

end;

procedure TdmSentinel.GetHistorico3DTOrdenado( iFolio, iNumeroSentinela: Integer);
begin
     with dbSentinel do
     begin
           try
              PreparaQuery( tqHistorico3DTOrdenado);
              with tqHistorico3DTOrdenado do
              begin
                   Active:= False;
                   ParamByName('SI_FOLIO').AsInteger:= iFolio;
                   ParamByName('SN_NUMERO').AsInteger:= iNumeroSentinela;
                   Active := True;
                   tqHistorico3DTOrdenado.First;
                   FieldByName('HI_FEC_ING').OnGetText:= CampoFechaGetText;
                   FieldByName('HI_FEC_ING').Alignment := taLeftJustify;
              end;
            except
                  on Error: Exception do
                  begin
                       Application.HandleException( Error );
                  end;
            end;
     end;
end;

procedure TdmSentinel.ModificaHistorico3DT( iFolio, iNumeroSentinela: Integer; sComentario:string);
begin
     with dbSentinel do
     begin
           try
              StartTransaction;
              PreparaQuery( tqModificaHist3DT);
              with tqModificaHist3DT do
              begin
                   ParamByName('SI_FOLIO').AsInteger:= iFolio;
                   ParamByName('SN_NUMERO').AsInteger:= iNumeroSentinela;
                   ParamByName('HI_COMENTA').AsString:= sComentario;
                   ExecSQL;
              end;
              Commit;
            except
                  on Error: Exception do
                  begin
                       Rollback;
                       Application.HandleException( Error );
                  end;
            end;
     end;
end;

procedure TdmSentinel.AbrirSentinelPorSistema;
begin
     with qSentinelPorSist do
     begin
          DisableControls;
          Active := False;
          ParamByName( 'Sistema' ).AsInteger := GetFolioSistema;
          Active := True;
          EnableControls;
     end;
end;


procedure TdmSentinel.tSentinelBeforePost(DataSet: TDataSet);
begin
     With DataSet do
     begin
          if ( FieldByName('SN_NUMERO').AsInteger = 0 )then
          begin
               DatabaseError('Capturar un N�mero de Sentinel');
          end;

          if ( FieldByName('SN_TIPO').AsInteger = 99 ) AND ( FieldByName('SN_NUMERO').AsInteger < 20000 ) then
          begin
               DatabaseError('N�mero de Sentinel Virtual Inv�lido ');
          end;

          if ( FieldByName('SN_TIPO').AsInteger = 99 ) AND ( FieldByName('SN_SERVER').AsString = ''  ) then
          begin
               DatabaseError('Capturar Nombre de Servidor Virtual');
          end;


     end
end;

{
with AutoMaster do
     begin
          if DetectarMaestroProxy then
          begin
               PreparaQuery( tqAgregaAutoriza );
               CopiaParams( tqActual , tqAgregaAutoriza );
               PosicionaActual( -1 );
               tqActual.Insert;
          end;
     end;
}

procedure TdmSentinel.AgregaAutorizacion;
var
   iFolio, iSerieDB: Integer;
   sClave1, sClave2: String;
   lGrabar: Boolean;
begin
     FAgregaAuto := False;
     PreparaQuery( tqAgregaAutoriza );
     with tAutoriza do
     begin
          iFolio := FieldByName( 'SI_FOLIO' ).AsInteger;
          //iSerieDB := FieldByName( 'SN_NUMERO' ).AsInteger;
          lGrabar := True;
          if ( FieldByName( 'AU_DEFINIT' ).AsInteger = K_INT_TRUE ) then
             FieldByName( 'AU_FEC_AUT' ).AsDateTime := EncodeDate( 1899, 12, 30 );
          if not HayPrestados then
             FieldByName( 'AU_FEC_PRE' ).AsDateTime := EncodeDate( 1899, 12, 30 );
          ValidaKit; {se ocup� debido a dbcheckboxes que no se graban sin click 'manual' �por cached updates?}
          FieldsToMaster;
          {$IFDEF SIN_MAESTRO}
          sClave1 := 'No Maestro C1';
          sClave2 := 'No Maestro C2';
          {$ELSE}
          while lGrabar and not AutoMaster.CalculaClaves( FieldByName( 'SN_NUMERO' ).AsInteger, sClave1, sClave2 ) do
          begin
               if not ZetaDialogo.zConfirm( '� Atenci�n !', 'No Se Encontr� El Sentinel Maestro' + CR_LF + '� Desea Reintentar ?', 0, mbYes ) then
               begin
                    CancelaCambios;
                    lGrabar := False;
               end
               else
               begin
                    if ( sClave1 = '' ) or ( sClave2 = '' ) then
                    begin
                         if not ZetaDialogo.zConfirm( '� Atenci�n !', 'Las Claves No Fueron Calculadas' + CR_LF + '� Desea Reintentar ?', 0, mbYes ) then
                            CancelaCambios;
                         lGrabar := False;
                    end
                    else
                        if not ValidaClaves( iFolio, sClave1, sClave2 ) then
                        begin
                             if not ZetaDialogo.zConfirm( '� Atenci�n !', 'Las Claves Ya Existen' + CR_LF + '� Desea Reintentar ?', 0, mbYes ) then
                                CancelaCambios;
                             lGrabar := False;
                        end;
               end;
          end;
          {$ENDIF}
          if lGrabar then
          begin
               FieldByName( 'AU_PAR_AUT' ).AsInteger := AutoMaster.Modulos;
               FieldByName( 'AU_PAR_PRE' ).AsInteger := AutoMaster.Prestamos;
               FieldByName( 'AU_CLAVE1' ).AsString := sClave1;
               FieldByName( 'AU_CLAVE2' ).AsString := sClave2;
               FieldByName( 'AU_TIPOS' ).AsString := T_AUTORIZACION;
               FAgregaAuto := True;
          end;
          if lGrabar then
          begin
               try
                  //if ( State in [ dsEdit, dsInsert ] ) then
                  //   Post;
                  CopiaParams( tAutoriza, tqAgregaAutoriza );
                  tqAgregaAutoriza.ExecSQL;

                  //DataBase.ApplyUpdates( [ tqActual ] );
                  { Guardar Sentinel 
                   try
                     if FEsAlta then
                        GrabarSentinelSistema( iFolio, iSerieDB );
                  except
                        on Error: Exception do
                        begin
                             ZetaDialogo.ZExcepcion( '� Error !', 'Error Al Iniciar Sentinela', Error, 0 );
                        end;
                  end;     }
                  FEsAlta := False;
               except
                     on Error: Exception do
                     begin
                          ZetaDialogo.ZExcepcion( '� Error !', 'Error Al Escribir Datos', Error, 0 );
                     end;
               end;
          end;
          Active := False;
     end;
     Refrescar;
     BookMarkGoTo;

end;


procedure TdmSentinel.GrabarSistema;
begin
     with tSistema do
     begin
          try
             if ( State in [ dsEdit, dsInsert ] ) then
                Post;
            // DataBase.ApplyUpdates( [ tSistema ] );
          
          except
                on Error: Exception do
                begin
                     ZetaDialogo.ZExcepcion( '� Error !', 'Error Al Grabar Datos del Sistema ', Error, 0 );
                end;
          end;
     end;
end;

procedure TdmSentinel.tAutorizaBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          {if ZetaCommonTools.StrVacio( FieldByName( 'VE_CODIGO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar la VERSION' );
             if ZetaCommonTools.StrVacio( FieldByName( 'SN_NUMERO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar el NUMERO DE SENTINEL' );

             if FEsAlta and not ValidaSentinela( FieldByName( 'SN_NUMERO' ).AsInteger, sMensaje ) then
                raise Exception.Create( sMensaje );

             if ( ( FieldByName( 'AU_DEFINIT' ).AsInteger = K_INT_TRUE ) and ( tSentinel.FieldByName('SN_SERVER').AsString  <> '' ) ) then
             begin
                   raise Exception.Create( 'No se puede generar una autorizaci�n Definitiva a un sentinel virtual' );
             end;

             if ( FieldByName( 'AU_DEFINIT' ).AsInteger = K_INT_FALSE ) then
             begin
                  if ( FieldByName( 'AU_FEC_AUT').AsDatetime = NullDateTime ) then
                     FieldByName( 'AU_FEC_AUT' ).AsDatetime := Date() + 8
                  else
                      if ( ( tSentinel.FieldByName('SN_SERVER').AsString  <> '' ) and ( (Date + 365)  < FieldByName( 'AU_FEC_AUT').AsDateTime ) )then
                         raise Exception.Create( 'No se puede generar una autorizaci�n a un sentinel virtual mayor a un A�o' );


                  if ( FieldByName( 'AU_FEC_PRE').AsDatetime = NullDateTime ) and HayPrestados then
                     FieldByName( 'AU_FEC_PRE' ).AsDatetime := Date() + 8;
                  if ( ( tSentinel.FieldByName('SN_SERVER').AsString = '' ) and ( FieldByName( 'AU_FEC_AUT' ).AsDatetime < Date ) ) then
                     raise Exception.Create( 'Fecha De Vencimiento Ya Caduc�' );
                  if   ( tSentinel.FieldByName('SN_SERVER').AsString = '' )and(  FieldByName( 'AU_FEC_PRE' ).AsDatetime < Date ) and HayPrestados then
                     raise Exception.Create( 'Fecha De Caducidad de Pr�stamos Ya Venci�' );
             end;

             if not DSentinel.EsVer20( FieldByName( 'VE_CODIGO' ).AsString ) then
                FieldByName( 'AU_PLATFORM' ).AsInteger := Ord( ptProfesional );
             if ( FieldByName( 'AU_PLATFORM' ).AsInteger = Ord( ptProfesional ) ) then
                FieldByName( 'AU_SQL_BD' ).AsInteger := Ord( engInterbase );

             if not DSentinel.EsVerValidacion( FieldByName( 'VE_CODIGO' ).AsString) then
             begin
                  FieldByName( 'AU_BD_LIM' ).AsInteger := 0;
                  FieldByName( 'AU_VAL_LIM' ).AsInteger := 0;
             end;

             with FieldByName( 'SN_NUMERO' ) do
             begin
                   TODO
                  if ValidaSentinel( AsInteger, FieldByName( 'SI_FOLIO' ).AsInteger, sMensaje ) then
                     Result := True
                  else
                  begin
                       ZetaDialogo.zError( '� Error !', sMensaje + CR_LF + 'Debe Indicar Un N�mero V�lido', 0 );
                       FocusControl;
                       Result := False;
                  end;
                  Result := True;
             end
             if Result then
             begin
                  iEmpleados := FieldByName( 'AU_NUM_EMP' ).AsInteger;
                  i := 0;
                  iNivelEmpleados := UnLoadEmpleados( i );
                  lFound := False;
                  while ( iNivelEmpleados > 0 ) and ( iNivelEmpleados <> K_TOP_EMPLOYEES ) and not lFound do
                  begin
                       lFound := ( iNivelEmpleados = iEmpleados );
                       Inc( i );
                       iNivelEmpleados := UnLoadEmpleados( i );
                  end;
                  if not lFound and not ZetaDialogo.ZConfirm( '� Atenci�n !', Format( K_MENSAJE, [ iEmpleados, CR_LF ] ), 0, mbNo ) then
                  begin
                       Result := False;
                  end;
             end;    }

     end;
     FAUComentaChanged := False;

end;

procedure TdmSentinel.tAutorizaAfterInsert(DataSet: TDataSet);
begin
     with tAutoriza do
     begin
          FieldByName( 'AU_FOLIO' ).AsInteger :=  GetUltimaAutorizacion + 1; //FieldByName( 'AU_FOLIO' ).AsInteger + 1;
          MarcaFechaHora;
     end;
end;

function TdmSentinel.GetUltimaAutorizacion:Integer;
begin
     with tqUltimaAutoriza do
     begin
          Active := False;
          Params[0].AsInteger := GetFolioSistema;
          Active := True;
          Result := FieldByName('Ultimo').AsInteger;
     end;
end;


procedure TdmSentinel.tSistemaBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( 'GR_CODIGO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar el GRUPO' );
          if ZetaCommonTools.StrVacio( FieldByName( 'GI_CODIGO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar El TIPO de Empresa' );
          if ZetaCommonTools.StrVacio( FieldByName( 'DI_CODIGO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar el DISTRIBUIDOR' );
          if ZetaCommonTools.StrVacio( FieldByName( 'CT_CODIGO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar la CIUDAD' );
          if ZetaCommonTools.StrVacio( FieldByName( 'PA_CODIGO' ).AsString ) then
                raise Exception.Create( 'Falta Especificar La NACIONALIDAD' );

          
     end;
end;

procedure TdmSentinel.Refrescar;
begin
     RefrescarAutorizacion;
     ActualAbrir(GetFolioSistema );
end;

function TdmSentinel.SiguienteSentinelVirtual:Integer;
begin
     with qGetNextSentVirtual do
     begin
          Active := False;
          Active := True;
          If FieldByName('ULTIMO').AsInteger > 0 then
             Result := FieldByName('ULTIMO').AsInteger + 1
          else
              Result := 20000;
     end;
end;

function TdmSentinel.SiguienteSistema:Integer;
begin
     with tqSistema do
     begin
          Active := False;
          Active := True;
          If FieldByName('ULTIMO').AsInteger > 0 then
             Result := FieldByName('ULTIMO').AsInteger + 1
          else
              Result := 0;
     end;
end;


procedure TdmSentinel.tSentinelAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'SN_ACTIVO' ).OnGetText := SentinelActivoGetText;
          FieldByName( 'SN_TIPO' ).OnGetText := TipoSentinelGetText;
     end;
end;

procedure TdmSentinel.SentinelActivoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if ( Sender.AsInteger = K_INT_FALSE ) then
             Text := 'No'
          else
              Text := 'Si';
     end;
end;

procedure TdmSentinel.TipoSentinelGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          Text := 'Desconocido';
          if ( Sender.AsInteger = 0 ) then
             Text := 'Paralelo';
          if ( Sender.AsInteger = 1 ) then
             Text := 'USB';
          if ( Sender.AsInteger = 2 ) then
             Text := 'USB - Virtual';
          if ( Sender.AsInteger = 99 ) then
             Text := 'Virtual';
     end;
end;

procedure TdmSentinel.tSistemaNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'SI_FOLIO' ).AsInteger := SiguienteSistema;
          FieldByName( 'GR_CODIGO' ).AsString := 'GRAL';
          FieldByName( 'DI_CODIGO' ).AsString := 'GTI';
          FieldByName( 'CT_CODIGO' ).AsString := 'TIJ';
          FieldByName( 'SI_FEC_SOL' ).AsDateTime := Date();
          FieldByName( 'SI_MIGRADO' ).AsInteger := K_INT_FALSE;
          FieldByName( 'SI_PROBLEMA' ).AsInteger := K_INT_FALSE;
     end;
end;

end.
