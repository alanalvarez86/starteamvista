inherited EscogeSentinel: TEscogeSentinel
  Top = 222
  Caption = 'Escoge archivo 3DT'
  ClientHeight = 265
  ClientWidth = 488
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Top = 230
    Width = 488
    inherited OK: TBitBtn
      Left = 325
      Visible = False
    end
    inherited Cancelar: TBitBtn
      Left = 408
      Hint = 'Cancelar Operaci'#243'n '
    end
  end
  inherited Panel2: TPanel
    Width = 488
    Visible = False
  end
  inherited Grid: TZetaDBGrid
    Width = 488
    Height = 189
    OnDblClick = GridDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'SI_FOLIO'
        Title.Caption = 'Folio'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SN_NUMERO'
        Title.Caption = 'Sentinel'
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HI_FEC_ING'
        Title.Caption = 'Fecha Ingreso'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HI_COMENTA'
        Title.Caption = 'Comentario'
        Width = 276
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 344
  end
end
