unit FRepAuto;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, Quickrpt, Qrctrls,
     FAutoClasses,ZetaLicenseClasses;

type
  TRepAuto = class(TForm)
    qrp1: TQuickRep;
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    SN_NUMERO: TQRDBText;
    SI_RAZ_SOC: TQRDBText;
    AU_CLAVE1: TQRDBText;
    AU_CLAVE2: TQRDBText;
    AU_MOD_1: TQRDBText;
    AU_MOD_2: TQRDBText;
    AU_MOD_3: TQRDBText;
    AU_MOD_4: TQRDBText;
    AU_MOD_5: TQRDBText;
    AU_MOD_6: TQRDBText;
    SI_RAZ_SOClbl: TQRLabel;
    SN_NUMEROlbl: TQRLabel;
    AU_CLAVE1lbl: TQRLabel;
    AU_CLAVE2lbl: TQRLabel;
    AU_MOD_3lbl: TQRLabel;
    AU_MOD_1lbl: TQRLabel;
    AU_MOD_2lbl: TQRLabel;
    AU_MOD_4lbl: TQRLabel;
    AU_MOD_6lbl: TQRLabel;
    AU_MOD_5lbl: TQRLabel;
    AU_DEFINIT: TQRDBText;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    AU_PRE_1: TQRDBText;
    AU_PRE_2: TQRDBText;
    AU_PRE_3: TQRDBText;
    AU_PRE_4: TQRDBText;
    AU_PRE_5: TQRDBText;
    AU_PRE_6: TQRDBText;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    AU_DEFINITlbl: TQRLabel;
    QRTitulo: TQRLabel;
    QRImage1: TQRImage;
    QRSysData1: TQRSysData;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    PageFooterBand1: TQRBand;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape7: TQRShape;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape8: TQRShape;
    AU_MOD_0lbl: TQRLabel;
    AU_MOD_0: TQRDBText;
    AU_PRE_0: TQRDBText;
    AU_PAR_AUTlbl: TQRLabel;
    SI_FOLIOlbl: TQRLabel;
    AU_USERSlbl: TQRLabel;
    AU_NUM_EMPlbl: TQRLabel;
    SI_FOLIO: TQRDBText;
    VE_DESCRIP: TQRDBText;
    AU_NUM_EMP: TQRDBText;
    AU_USERS: TQRDBText;
    AU_PAR_AUT: TQRDBText;
    Instrucciones: TQRMemo;
    InstruccionesHD: TQRLabel;
    AU_KIT_DIS: TQRDBText;
    AU_MOD_9: TQRDBText;
    AU_PRE_9: TQRDBText;
    QRObsoleta: TQRLabel;
    AU_MOD_7lbl: TQRLabel;
    AU_MOD_7: TQRDBText;
    AU_PRE_7: TQRDBText;
    AU_MOD_8lbl: TQRLabel;
    AU_MOD_8: TQRDBText;
    AU_PRE_8: TQRDBText;
    AU_MOD_10: TQRDBText;
    AU_MOD_11: TQRDBText;
    AU_MOD_12: TQRDBText;
    AU_MOD_13: TQRDBText;
    AU_MOD_9lbl: TQRLabel;
    AU_MOD_10lbl: TQRLabel;
    AU_MOD_11lbl: TQRLabel;
    AU_MOD_12lbl: TQRLabel;
    AU_MOD_13lbl: TQRLabel;
    AU_PRE_10: TQRDBText;
    AU_PRE_11: TQRDBText;
    AU_PRE_12: TQRDBText;
    AU_PRE_13: TQRDBText;
    VE_DESCRIPlbl: TQRLabel;
    AU_KIT_DISlbl: TQRLabel;
    CaducidadModulos: TQRLabel;
    CaducidadPrestamos: TQRLabel;
    AU_PLATFORMlbl: TQRLabel;
    AU_PLATFORM: TQRDBText;
    AU_SQL_BD: TQRDBText;
    AU_SQL_BDlbl: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape6: TQRShape;
    QRShape10: TQRShape;
    AU_MOD_14lbl: TQRLabel;
    AU_MOD_14: TQRDBText;
    AU_PRE_14: TQRDBText;
    QRShape11: TQRShape;
    AU_MOD_15lbl: TQRLabel;
    AU_MOD_15: TQRDBText;
    AU_PRE_15: TQRDBText;
    AU_MOD_16lbl: TQRLabel;
    AU_MOD_17lbl: TQRLabel;
    AU_MOD_18lbl: TQRLabel;
    AU_MOD_16: TQRDBText;
    AU_MOD_17: TQRDBText;
    AU_MOD_18: TQRDBText;
    AU_PRE_16: TQRDBText;
    AU_PRE_17: TQRDBText;
    AU_PRE_18: TQRDBText;
    AU_MOD_19lbl: TQRLabel;
    AU_MOD_19: TQRDBText;
    AU_PRE_19: TQRDBText;
    AU_MOD_20lbl: TQRLabel;
    AU_MOD_20: TQRDBText;
    AU_PRE_20: TQRDBText;
    AU_MOD_21lbl: TQRLabel;
    AU_MOD_21: TQRDBText;
    AU_PRE_21: TQRDBText;
    AU_MOD_22lbl: TQRLabel;
    AU_MOD_22: TQRDBText;
    AU_PRE_22: TQRDBText;
    AU_MOD_23lbl: TQRLabel;
    AU_MOD_23: TQRDBText;
    AU_PRE_23: TQRDBText;
    QRLabel3: TQRLabel;
    AU_BD_LIM: TQRDBText;
    procedure QRSysData1Print(sender: TObject; var Value: String);
    procedure SI_RAZ_SOCPrint(sender: TObject; var Value: String);
    procedure AU_PLATFORMPrint(sender: TObject; var Value: String);
    procedure AU_SQL_BDPrint(sender: TObject; var Value: String);
    procedure FormCreate(Sender: TObject);
    procedure AU_BD_LIMPrint(sender: TObject; var Value: String);
  private
    { Private declarations }
    function GetModuleName(const eModule: TModulos): String;
  public
    { Public declarations }
  end;

var
  RepAuto: TRepAuto;

implementation

uses DRep;

{$R *.DFM}

procedure TRepAuto.FormCreate(Sender: TObject);
begin
     with qrp1 do
     begin
          Dataset := dmRep.qRepAuto;
     end;
     AU_MOD_0lbl.Caption := GetModuleName( okRecursos );
     AU_MOD_1lbl.Caption := GetModuleName( okNomina );
     AU_MOD_2lbl.Caption := GetModuleName( okIMSS );
     AU_MOD_3lbl.Caption := GetModuleName( okAsistencia );
     AU_MOD_4lbl.Caption := GetModuleName( okCursos );
     AU_MOD_5lbl.Caption := GetModuleName( okSupervisores );
     AU_MOD_6lbl.Caption := GetModuleName( okCafeteria );
     AU_MOD_7lbl.Caption := GetModuleName( okLabor );
     AU_MOD_8lbl.Caption := GetModuleName( okKiosko );
     AU_MOD_9lbl.Caption := GetModuleName( okValidador );
     AU_MOD_10lbl.Caption := GetModuleName( okGraficas );
     AU_MOD_11lbl.Caption := GetModuleName( okReportesMail );
     AU_MOD_12lbl.Caption := GetModuleName( okHerramientas );
     AU_MOD_13lbl.Caption := GetModuleName( okL5Poll );
     AU_MOD_14lbl.Caption := GetModuleName( okSeleccion );
     AU_MOD_15lbl.Caption := GetModuleName( okEnfermeria );
     AU_MOD_16lbl.Caption := GetModuleName( okMonitor );
     AU_MOD_17lbl.Caption := GetModuleName( okPortal );
     AU_MOD_18lbl.Caption := GetModuleName( okPlanCarrera );
     AU_MOD_19lbl.Caption := GetModuleName( okAccesos );
     AU_MOD_20lbl.Caption := GetModuleName( okVisitantes );
     AU_MOD_21lbl.Caption := GetModuleName( okEvaluacion );
     AU_MOD_22lbl.Caption := GetModuleName( okWorkflow );
end;

function TRepAuto.GetModuleName( const eModule: TModulos ): String;
begin
     Result := FAutoClasses.GetModuloName( eModule ) + ':';
end;

procedure TRepAuto.QRSysData1Print(sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now );
end;

procedure TRepAuto.SI_RAZ_SOCPrint(sender: TObject; var Value: String);
begin
     Value := Copy( Value, 1, 100 );
end;

procedure TRepAuto.AU_PLATFORMPrint(sender: TObject; var Value: String);
begin
     Value := FAutoClasses.GetPlatformName( TPlataforma( StrToIntDef( Value, 0 ) ) );
end;

procedure TRepAuto.AU_SQL_BDPrint(sender: TObject; var Value: String);
begin
     Value := FAutoClasses.GetSQLEngineName( TSQLEngine( StrToIntDef( Value, 0 ) ) );
end;



procedure TRepAuto.AU_BD_LIMPrint(sender: TObject; var Value: String);
begin
     Value := IntToStr( GetIntervaloLimiteBDConfig(StrToIntDef( Value, 0 )) );
end;

end.
