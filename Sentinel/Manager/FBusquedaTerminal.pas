unit FBusquedaTerminal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, StdCtrls, ZetaEdit, Grids, DBGrids, ZetaDBGrid,
  Buttons;

type
  TBusquedaTeminales = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    ZetaDBGrid1: TZetaDBGrid;
    Panel2: TPanel;
    Label1: TLabel;
    txtPista: TZetaEdit;
    dsTerminales: TDataSource;
    btnBuscar: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure txtPistaChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BusquedaTeminales: TBusquedaTeminales;

implementation

uses
    DSentinel;

{$R *.dfm}

procedure TBusquedaTeminales.BitBtn1Click(Sender: TObject);
begin
     Close();
end;

procedure TBusquedaTeminales.btnBuscarClick(Sender: TObject);
begin
     dmSentinel.GetTerminalesBusqueda(dsTerminales,txtPista.Text); 
end;

procedure TBusquedaTeminales.txtPistaChange(Sender: TObject);
begin
     dmSentinel.GetTerminalesBusqueda(dsTerminales,txtPista.Text);
end;

end.
