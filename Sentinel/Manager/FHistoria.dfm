�
 TFORMHISTORIA 0�F  TPF0TFormHistoriaFormHistoriaLeft"Top� BorderStylebsDialogCaptionConsulta de Historia de ClavesClientHeight/ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�HeightQAlignalTop
BevelOuterbvNoneTabOrder  TLabelLabel2LeftTopWidth0HeightCaptionNombre:Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel3LeftTop	Width=HeightCaption
Sistema #:Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel1LeftTop,WidthCHeightCaptionMovimiento #:  TLabelLabel5Left'Top>Width!HeightCaptionFecha:  TZetaDBTextBoxAU_FECHALeftKTop<WidthqHeightAutoSizeCaptionAU_FECHAShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldAU_FECHA
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxAU_FOLIOLeftKTop*WidthqHeightAutoSizeCaptionAU_FOLIOShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldAU_FOLIO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
SI_RAZ_SOCLeftKTopWidthvHeightAutoSizeCaption
SI_RAZ_SOCFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataField
SI_RAZ_SOC
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxSI_FOLIOLeftKTopWidthqHeightAutoSizeCaptionSI_FOLIOFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldSI_FOLIO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m   TPageControlPageControlLeft TopQWidth�Height�
ActivePageActualAlignalClientTabOrder 	TTabSheetActualCaptionSistema ActualEnabled TLabelLabel6Left/TopWidth7Height	AlignmenttaRightJustifyCaptionDistribuidor:  TLabelLabel4Left TopWidthfHeight	AlignmenttaRightJustifyCaption   Límite de Empleados:  TLabelLabel14Left6Top&Width0Height	AlignmenttaRightJustifyCaption
Licencias:  TLabelLabel9Left$Top8WidthBHeight	AlignmenttaRightJustifyCaption# de Sentinel:  TLabelLabel7LeftTop\WidthUHeight	AlignmenttaRightJustifyCaptionKit de Distribuidor:  TLabelLabel8Left@TopJWidth&Height	AlignmenttaRightJustifyCaption	   Versión:  TZetaDBTextBox	DI_CODIGOLeftjTopWidth� HeightAutoSizeCaption	DI_CODIGOShowAccelCharBrush.Color	clBtnFaceBorder		DataField	DI_CODIGO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
AU_NUM_EMPLeftjTopWidth� HeightAutoSizeCaption
AU_NUM_EMPShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AU_NUM_EMP
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBoxAU_USERSLeftjTop%Width� HeightAutoSizeCaptionAU_USERSShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldAU_USERS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	SN_NUMEROLeftjTop7Width� HeightAutoSizeCaption	SN_NUMEROShowAccelCharBrush.Color	clBtnFaceBorder		DataField	SN_NUMERO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
VE_DESCRIPLeftjTopIWidth� HeightAutoSizeCaption
VE_DESCRIPShowAccelCharBrush.Color	clBtnFaceBorder		DataField
VE_DESCRIP
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelLabel10LeftLTop� WidthHeight	AlignmenttaRightJustifyCaptionNota:  TZetaDBTextBox
AU_KIT_DISLeftjTop[Width/HeightAutoSizeCaption
AU_KIT_DISShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AU_KIT_DIS
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m	OnGetTextAU_KIT_DISGetText  TLabelLabel11Left1TopnWidth5Height	AlignmenttaRightJustifyCaptionPlataforma:  TZetaDBTextBoxAU_PLATFORMLeftjTopmWidth� HeightAutoSizeCaptionAU_PLATFORMShowAccelCharBrush.Color	clBtnFaceBorder		DataFieldAU_PLATFORM
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m	OnGetTextAU_PLATFORMGetText  TZetaDBTextBox	AU_SQL_BDLeftjTopWidth� HeightAutoSizeCaption	AU_SQL_BDShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_SQL_BD
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m	OnGetTextAU_SQL_BDGetText  TLabelLabel12LeftTop� WidthIHeight	AlignmenttaRightJustifyCaptionBase de Datos:  TLabelLabel13Left8Top� Width.Height	AlignmenttaRightJustifyCaption	Clave# 1:  TLabelLabel15Left5Top� Width1Height	AlignmenttaRightJustifyCaption
Clave # 2:  TZetaDBTextBox	AU_CLAVE2LeftjTop� Width� HeightAutoSizeCaption	AU_CLAVE2ShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_CLAVE2
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	AU_CLAVE1LeftjTop� Width� HeightAutoSizeCaption	AU_CLAVE1ShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_CLAVE1
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelLabel16LeftTop� Width]Height	AlignmenttaRightJustifyCaption   Límite de BD Prod.:  TZetaDBTextBox	AU_BD_LIMLeftjTop� Width� HeightAutoSizeCaption	AU_BD_LIMShowAccelCharBrush.Color	clBtnFaceBorder		DataField	AU_BD_LIM
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m	OnGetTextAU_BD_LIMGetText  TDBMemo
AU_COMENTALeftjTop� WidthnHeight� TabStopColor	clBtnFace	DataField
AU_COMENTA
DataSource
DataSourceReadOnly	
ScrollBarsssBothTabOrder    	TTabSheetModulosCaption   Módulos 	TGroupBox	GroupBox2Left Top Width
Height�AlignalLeftCaption Autorizados TabOrder  TLabelAU_FEC_AUTlblLeft#Top�Width=HeightCaptionVencimiento:  TZetaDBTextBox
AU_FEC_AUTLeftbTop�WidthoHeightAutoSizeCaption
AU_FEC_AUTShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AU_FEC_AUT
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TDBCheckBoxAU_MOD_0LeftTopWidth� HeightCaption
Empleados:	DataFieldAU_MOD_0
DataSource
DataSourceReadOnly	TabOrder ValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_1LeftTop!Width� HeightCaption   Nómina:	DataFieldAU_MOD_1
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_2LeftTop0Width� HeightCaptionIMSS / INFONAVIT:	DataFieldAU_MOD_2
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_3LeftTop?Width� HeightCaptionAsistencia:	DataFieldAU_MOD_3
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_4LeftTopNWidth� HeightCaptionCursos:	DataFieldAU_MOD_4
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_5LeftTop]Width� HeightCaptionSupervisores:	DataFieldAU_MOD_5
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_6LeftToplWidth� HeightCaption   Cafetería:	DataFieldAU_MOD_6
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_9LeftTop� Width� HeightCaptionTress Analiza:	DataFieldAU_MOD_9
DataSource
DataSourceReadOnly	TabOrder	ValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_7LeftTop{Width� HeightCaptionLabor:	DataFieldAU_MOD_7
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_MOD_8LeftTop� Width� HeightCaptionKiosko:	DataFieldAU_MOD_8
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_10LeftTop� Width� HeightCaption
   Gráficas:	DataField	AU_MOD_10
DataSource
DataSourceReadOnly	TabOrder
ValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_11LeftTop� Width� HeightCaptionReportes Email:	DataField	AU_MOD_11
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_12LeftTop� Width� HeightCaption
Resguardo:	DataField	AU_MOD_12
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_13LeftTop� Width� HeightCaption
TRESSPoll:	DataField	AU_MOD_13
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_14LeftTop� Width� HeightCaption   Selección de Personal:	DataField	AU_MOD_14
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_16LeftTopWidth� HeightCaptionMonitor:	DataField	AU_MOD_16
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_15LeftTop� Width� HeightCaption   Servicios Médicos:	DataField	AU_MOD_15
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_18LeftTop"Width� HeightCaptionPlan de Carrera:	DataField	AU_MOD_18
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_17LeftTopWidth� HeightCaptionPortal:	DataField	AU_MOD_17
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_19LeftTop1Width� HeightCaptionAccesos:	DataField	AU_MOD_19
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_20LeftTop@Width� HeightCaptionVisitantes:	DataField	AU_MOD_20
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_21LeftTopOWidth� HeightCaption   Evaluación	DataField	AU_MOD_21
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_MOD_22LeftTop_Width� HeightCaptionWorkflow	DataField	AU_MOD_22
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxDBCheckBox1LeftTopoWidth� HeightCaption   TRESS En Línea	DataField	AU_MOD_23
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0   	TGroupBox	GroupBox3Left
Top Width� Height�AlignalClientCaption Prestados TabOrder TLabelAU_FEC_PRElblLeft*Top�Width=Height	AlignmenttaRightJustifyCaptionVencimiento:  TZetaDBTextBox
AU_FEC_PRELeftjTop�WidthoHeightAutoSizeCaption
AU_FEC_PREShowAccelCharBrush.Color	clBtnFaceBorder		DataField
AU_FEC_PRE
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TDBCheckBoxAU_PRE_0LeftTopWidth� HeightCaptionRecursos Humanos:	DataFieldAU_PRE_0
DataSource
DataSourceReadOnly	TabOrder ValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_1LeftTop!Width� HeightCaption   Nómina:	DataFieldAU_PRE_1
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_2LeftTop0Width� HeightCaptionIMSS / INFONAVIT:	DataFieldAU_PRE_2
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_3LeftTop?Width� HeightCaptionAsistencia:	DataFieldAU_PRE_3
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_4LeftTopNWidth� HeightCaptionCursos:	DataFieldAU_PRE_4
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_5LeftTop]Width� HeightCaptionSupervisores:	DataFieldAU_PRE_5
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_6LeftToplWidth� HeightCaption   Cafetería:	DataFieldAU_PRE_6
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_9LeftTop� Width� HeightCaptionTress Analiza:	DataFieldAU_PRE_9
DataSource
DataSourceReadOnly	TabOrder	ValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_7LeftTop{Width� HeightCaptionLabor:	DataFieldAU_PRE_7
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxAU_PRE_8LeftTop� Width� HeightCaptionKiosko:	DataFieldAU_PRE_8
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_10LeftTop� Width� HeightCaption
   Gráficas:	DataField	AU_PRE_10
DataSource
DataSourceReadOnly	TabOrder
ValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_11LeftTop� Width� HeightCaptionReportes x E-Mail:	DataField	AU_PRE_11
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_12LeftTop� Width� HeightCaptionHerramientas:	DataField	AU_PRE_12
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_13LeftTop� Width� HeightCaptionL5Poll:	DataField	AU_PRE_13
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_14LeftTop� Width� HeightCaption   Selección:	DataField	AU_PRE_14
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_15LeftTop� Width� HeightCaption   Servicios Médicos:	DataField	AU_PRE_15
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_16LeftTopWidth� HeightCaptionMonitor:	DataField	AU_PRE_16
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_18LeftTop Width� HeightCaptionPlan de Carrera:	DataField	AU_PRE_18
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_17LeftTopWidth� HeightCaptionPortal:	DataField	AU_PRE_17
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_19LeftTop/Width� HeightCaptionAccesos:	DataField	AU_PRE_19
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_20LeftTop>Width� HeightCaptionVisitantes:	DataField	AU_PRE_20
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_21LeftTopMWidth� HeightCaption   Evaluación	DataField	AU_PRE_21
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBox	AU_PRE_22LeftTop]Width� HeightCaptionWorkflow	DataField	AU_PRE_22
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0  TDBCheckBoxDBCheckBox2LeftTopmWidth� HeightCaption   TRESS En Línea	DataField	AU_PRE_23
DataSource
DataSourceReadOnly	TabOrderValueChecked1ValueUnchecked0     TPanelPanel3Left TopWidth�Height AlignalBottomTabOrder TSpeedButtonImprimirBtnLeftTopWidthHeightHint   Imprimir Autorización
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 0      ?��������������wwwwwww�������wwwwwww        ���������������wwwwwww�������wwwwwww�������wwwwww        wwwwwww30����337���?330� 337�wss330����337��?�330�  337�swws330���3337��73330��3337�ss3330�� 33337��w33330  33337wws333	NumGlyphsParentShowHintShowHint	OnClickImprimirBtnClick  TBitBtnBitBtn1Left�TopWidthKHeightCancel	Caption&SalirDefault	ModalResultTabOrder 
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 8����w���������33?  DD@���DD������3��  33MP��33�����38��  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33DDDDD3333������?  33333333333�����3?  333   333333?���3?  333
��333333����3?  333   333333����3?  	NumGlyphs  TDBNavigatorDBNavigator1Left(TopWidthpHeight
DataSource
DataSourceVisibleButtonsnbFirstnbPriornbNextnbLast Flat	TabOrder  TBitBtnEscribirLeft� TopWidthKHeightHint Escribri Claves En Archivo TextoCaption	&EscribirParentShowHintShowHint	TabOrderOnClickEscribirClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333�33;3�3333�;�w{�w{�7����s3�    33wwwwww330����337�333330����337�333330����337�333330����3?��333��������ww�333w;������7w�3?�ww30��  337�3wws330���3337�37�330��3337�3w�330�� ;�337��w7�3�  3�33www3w�;�3;�3;�7s37s37s�33;333;s3373337	NumGlyphs  TBitBtnEnviarLeft� TopWidthKHeightHintEnviar Clave Por e-MailCaptionEn&viarParentShowHintShowHint	TabOrderVisible
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333333333333333?�������        wwwwwwww�������33��337��w����37w3337�������33���37��ww��37wws?��������?�333www�����ws3337�������������        wwwwwwww33333333333333333333333333333333333333333333333333333333333333333333333333333333	NumGlyphs   TDataSource
DataSourceLeftDTop  TSaveDialog
SaveDialog
DefaultExttxtFilter(Textos ( *.txt )|*.txt|Todos ( *.* )|*.*FilterIndex TitleEscoja El Archivo A GenerarLeftTop�    