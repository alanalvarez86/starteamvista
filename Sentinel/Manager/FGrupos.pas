unit FGrupos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     DBCtrls, ExtCtrls,
     FCatalogo, StdCtrls, Buttons;

type
  TFormGrupos = class(TFormCatalogo)
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormGrupos: TFormGrupos;

implementation

uses DSentinel;

{$R *.DFM}

procedure TFormGrupos.FormShow(Sender: TObject);
begin
     inherited;
     dmSentinel.AbrirGrupo( dsCatalogo );
end;

procedure TFormGrupos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmSentinel.ResetDatasource( dsCatalogo );
end;

end.
