unit DRep;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, Quickrpt, Qrctrls, QRExport, IBDataset;

type
  TdmRep = class(TDataModule)
    qRepAuto: TIBOQuery;
    qGetMax: TIBOQuery;
    qRepVence: TIBOQuery;
    qAuditoria: TIBOQuery;
    qUsoSentinel: TIBOQuery;
    qInstalaciones: TIBOQuery;
    qModulos: TIBOQuery;
    procedure qRepAutoAfterOpen(DataSet: TDataSet);
    procedure qRepVenceAfterOpen(DataSet: TDataSet);
    procedure qAuditoriaAfterOpen(DataSet: TDataSet);
    procedure qModulosAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FObsoleta: Boolean;
    function BoolToFullYesNo( const iValue: Integer ): String;
    function GetUltimoFolio( const iSistema: Integer ): Integer;
    procedure InitModulosUsados;
    procedure InitAuditoria;
    procedure InitRepAuto;
    procedure InitRepVence;
    procedure InitUsoSentinel;
    procedure InitInstalaciones;
    procedure ImprimeUnaClave( const iSistema, iFolio: Integer; const lImprimir: Boolean; const sArchivo: String );
    procedure FechaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FechaCortaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FloatGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ModuloGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PlatformGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SQLGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure TiposGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  public
    { Public declarations }
    function ImprimeAuditoria( const dInicial, dFinal: TDate; const lImprimir: Boolean; const sArchivo: String ): Boolean;
    function ImprimeModulosUsados( const iModulos: DWord; const lKits, lVigentes, lImprimir: Boolean; const sArchivo: String ): Boolean;
    function ImprimeVencimientos( const dInicial, dFinal: TDate ): Boolean;
    function ImprimeUsoSentinel: Boolean;
    procedure ImprimeClaves;
    procedure ImprimeClaveAnterior( const iSistema, iFolio: Integer );
    procedure ImprimeClaveInstalacion( const iSistema, iSentinel, iFolio: Integer );
    procedure ImprimeClaveHTML(const iSistema, iFolio: Integer; const sArchivo: String);
  end;

var
  dmRep: TdmRep;

procedure InitReportes;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     DSentinel,
     FRepVence,
     FRepAuditoria,
     FRepAuto,
     FRepUsoSentinel,
     FRepInstalaciones,
     FRepModulosUsados,     
     FSentinel,
     FAutoClasses,
     FAutoServer,
     FAutoMaster,
     FPrincipal;

{$R *.DFM}

procedure InitReportes;
begin
     if ( dmRep = nil ) then
        dmRep := TdmRep.Create( FormPrincipal );
end;

{ *********** TdmRep ************ }

procedure TdmRep.InitRepAuto;
begin
     if ( RepAuto = nil ) then
     begin
          RepAuto := TRepAuto.Create( Self );
     end;
end;

procedure TdmRep.InitRepVence;
begin
     if ( RepVence = nil ) then
     begin
          RepVence := TRepVence.Create( Self );
     end;
end;

procedure TdmRep.InitAuditoria;
begin
     if ( RepAudit = nil ) then
     begin
          RepAudit := TRepAudit.Create( Self );
     end;
end;

procedure TdmRep.InitModulosUsados;
begin
     if ( RepModulosUsados = nil ) then
     begin
          RepModulosUsados := TRepModulosUsados.Create( Self );
     end;
end;

procedure TdmRep.InitUsoSentinel;
begin
     if ( RepUsoSentinel = nil ) then
     begin
          RepUsoSentinel := TRepUsoSentinel.Create( Self );
     end;
end;

procedure TdmRep.InitInstalaciones;
begin
     if ( RepInstalaciones = nil ) then
     begin
          RepInstalaciones := TRepInstalaciones.Create( Self );
     end;
end;

function TdmRep.GetUltimoFolio( const iSistema: Integer ): Integer;
begin
     with qGetMax do
     begin
          Active := False;
          ParamByName( 'SI_FOLIO' ).AsInteger := iSistema;
          Active := True;
          Result := Fields[ 0 ].AsInteger;
          Active := False;
     end;
end;

procedure TdmRep.ImprimeClaves;
var
   iSistema: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iSistema := dmSentinel.GetFolioSistema;
        FObsoleta := False;
        ImprimeUnaClave( iSistema, GetUltimoFolio( iSistema ), True, '' );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmRep.ImprimeClaveHTML( const iSistema, iFolio: Integer; const sArchivo: String );
begin
     try
        FObsoleta := ( iFolio <> GetUltimoFolio( iSistema ) );
        ImprimeUnaClave( iSistema, iFolio, False, sArchivo );
     finally
            FObsoleta := False;
     end;
end;

procedure TdmRep.ImprimeClaveAnterior( const iSistema, iFolio: Integer );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FObsoleta := ( iFolio <> GetUltimoFolio( iSistema ) );
        ImprimeUnaClave( iSistema, iFolio, True, '' );
     finally
            Screen.Cursor := oCursor;
            FObsoleta := False;
     end;
end;

procedure TdmRep.ImprimeUnaClave( const iSistema, iFolio: Integer; const lImprimir: Boolean; const sArchivo: String );
var
   lVencido, lCaducado, lInstrucciones, lEsVer20: Boolean;

procedure SetLabel( Etiqueta: TControl; const lShow: Boolean );
begin
     with Etiqueta do
     begin
          Enabled := lShow;
          Visible := lShow;
     end;
end;

procedure SetDueDate( Etiqueta: TQRLabel; Campo: TField; const lShow: Boolean; const sTipo: String );
begin
     with Campo do
     begin
          if lShow and ( AsDateTime > 0 ) then
          begin
               SetLabel( Etiqueta, True );
               Etiqueta.Caption := Format( 'Fecha de Vencimiento de M�dulos %s : %s', [ sTipo, FormatDateTime( 'dd/mmm/yyyy', AsDateTime ) ] );
          end
          else
              SetLabel( Etiqueta, False );
     end;
end;

begin
     InitRepAuto;
     with qRepAuto do
     begin
          Active := False;
          ParamByName( 'SI_FOLIO' ).AsInteger := iSistema;
          ParamByName( 'AU_FOLIO' ).AsInteger := iFolio;
          Active := True;
          if IsEmpty then
          begin
               lInstrucciones := False;
               lVencido := False;
               lCaducado := False;
               lEsVer20 := True;
          end
          else
          begin
               lInstrucciones := ( FieldByName( 'AU_CLAVE1' ).AsString <> '' ) and
                                 ( FieldByName( 'AU_CLAVE2' ).AsString <> '' );
               lVencido := ( FieldByName( 'AU_FEC_AUT' ).AsDateTime <> 0 );
               lCaducado := ( FieldByName( 'AU_FEC_PRE' ).AsDateTime <> 0 );
               lEsVer20 := DSentinel.EsVer20( FieldByName( 'VE_CODIGO' ).AsString );
          end;
          with RepAuto do
          begin
               SetDueDate( CaducidadModulos, FieldByName( 'AU_FEC_AUT' ), lVencido, 'Autorizados' );
               SetDueDate( CaducidadPrestamos, FieldByName( 'AU_FEC_PRE' ), lCaducado, 'Prestados' );
          end;
     end;
     with RepAuto do
     begin
          with QRTitulo do
          begin
               if lInstrucciones then
                  Caption := 'Claves Para Actualizaci�n Del Sistema Tress'
               else
                   Caption := 'Configuraci�n Inicial Del Sistema Tress';
          end;
          with Instrucciones.Lines do
          begin
               Clear;
               if lInstrucciones then
               begin
                    if lEsVer20 then
                    begin
                         Add( 'La actualizaci�n del Sentinel se realiza ejecutando el programa "TressCFG.exe"' );
                         Add( 'Este programa se instala junto con el resto del software de "Tress Servidor"' );
                         Add( '' );
                         Add( '1.- Localice "TressCFG.exe" en la computadora donde se instal� "Tress Servidor"' );
                         Add( '2.- Coloque el Sentinel en puerto paralelo � USB de la computadora' );
                         Add( '3.- Corra el programa "TessCFG.exe"' );
                         Add( '4.- En el men� "Sentinel - Actualizar Sentinel", capture los valores indicados en esta forma' );
                         Add( '5.- Presione el bot�n "OK"' );
                    end
                    else
                    begin
                         Add( 'La actualizaci�n del Sentinel se realiza ejecutando el programa "TressCFG.exe"' );
                         Add( 'Este programa se instala junto con el resto del software de "Tress Profesional"' );
                         Add( '' );
                         Add( '1.- Localice "TressCFG.exe" en la computadora donde se instal� "Tress Profesional"' );
                         Add( '2.- Coloque el Sentinel en puerto paralelo � USB de la computadora' );
                         Add( '3.- Corra el programa "TessCFG.exe"' );
                         Add( '4.- En el men� "Sentinel - Actualizar Sentinel", capture los valores indicados en esta forma' );
                         Add( '5.- Presione el bot�n "OK"' );
                    end;
               end;
          end;
          SetLabel( InstruccionesHD, lInstrucciones );
          SetLabel( Instrucciones, lInstrucciones );
          SetLabel( AU_PAR_AUT, lInstrucciones );
          SetLabel( AU_PAR_AUTlbl, lInstrucciones );
          SetLabel( AU_CLAVE1, lInstrucciones );
          SetLabel( AU_CLAVE1lbl, lInstrucciones );
          SetLabel( AU_CLAVE2, lInstrucciones );
          SetLabel( AU_CLAVE2lbl, lInstrucciones );
          SetLabel( QRObsoleta, FObsoleta and lInstrucciones );
          SetLabel( AU_SQL_BD, lEsVer20 );
          SetLabel( AU_SQL_BDlbl, lEsVer20 );
          SetLabel( AU_PLATFORM, lEsVer20 );
          SetLabel( AU_PLATFORMlbl, lEsVer20 );
          with qrp1 do
          begin
               if lImprimir then
                  Preview
               else
               begin
                    Prepare;
                    ExportToFilter( TQRHTMLDocumentFilter.Create( sArchivo ) );
               end;
          end;
     end;
     with qRepAuto do
     begin
          Active := False;
     end;
end;

function TdmRep.BoolToFullYesNo( const iValue: Integer ): String;
begin
     if ( iValue = 1 ) then
        Result := 'SI'
     else
         Result := 'NO';
end;

procedure TdmRep.ModuloGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          Text := BoolToFullYesNo( Sender.AsInteger );
     end;
end;

procedure TdmRep.FechaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          Text := FormatDateTime( 'dd/mmm/yyyy', Sender.AsDateTime );
     end;
end;

procedure TdmRep.FechaCortaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if ( Sender.AsDateTime = NullDateTime ) then
             Text := 'Definitiva'
          else
              Text := FormatDateTime( 'dd/mmm/yy', Sender.AsDateTime );
     end;
end;

procedure TdmRep.FloatGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          Text := Format( '%6.0n', [ Sender.AsFloat ] );
     end;
end;

procedure TdmRep.SQLGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          case TSQLEngine( Sender.AsInteger ) of
               engInterbase: Text := 'IB';
               engMSSQL: Text := 'MSSQL';
          else
              Text := '???';
          end;
     end;
end;

procedure TdmRep.PlatformGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          case TPlataforma( Sender.AsInteger ) of
               ptCorporativa: Text := 'Corp';
               ptProfesional: Text := 'Pro';
          else
              Text := '???';
          end;
     end;
end;

procedure TdmRep.TiposGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          Text := Sender.AsString;
          if ( Text = 'Autorizados' ) then
             Text := 'Claves';
     end;
end;

procedure TdmRep.qRepAutoAfterOpen(DataSet: TDataSet);
var
   i: Integer;
begin
     with Dataset do
     begin
          for i := 0 to Ord( High( TModulos ) ) do
          begin
               FieldByName( Format( 'AU_MOD_%d', [ i ] ) ).OnGetText := ModuloGetText;
               FieldByName( Format( 'AU_PRE_%d', [ i ] ) ).OnGetText := ModuloGetText;
          end;
          FieldByName( 'AU_DEFINIT' ).OnGetText := ModuloGetText;
          FieldByName( 'AU_KIT_DIS' ).OnGetText := ModuloGetText;
     end;
end;

function TdmRep.ImprimeAuditoria( const dInicial, dFinal: TDate; const lImprimir: Boolean; const sArchivo: String ): Boolean;
const
     Q_AUDIT = 'select A.TT_TIPO,'+
               'A.SI_FOLIO,'+
               'A.SN_NUMERO,'+
               'A.VE_CODIGO,'+
               'A.AU_FOLIO,'+
               'A.AU_FECHA,'+
               'A.AU_FEC_AUT,'+
               'A.AU_TIPOS,'+
               'A.AU_NUM_EMP,'+
               'A.AU_MOD_0,'+
               'A.AU_MOD_1,'+
               'A.AU_MOD_2,'+
               'A.AU_MOD_3,'+
               'A.AU_MOD_4,'+
               'A.AU_MOD_5,'+
               'A.AU_MOD_6,'+
               'A.AU_MOD_7,'+
               'A.AU_MOD_8,'+
               'A.AU_MOD_9,'+
               'A.AU_MOD_10,'+
               'A.AU_MOD_11,'+
               'A.AU_MOD_12,'+
               'A.AU_MOD_13,'+
               'A.AU_MOD_14,'+
               'A.AU_MOD_15,'+
               'A.AU_MOD_16,'+
               'A.AU_MOD_17,'+
               'A.AU_MOD_18,'+
               'A.AU_MOD_19,'+
               'A.AU_MOD_20,'+
               'A.AU_USERS,'+
               'A.AU_KIT_DIS,'+
               'A.AU_SQL_BD,'+
               'A.AU_PLATFORM,'+
               'S.SI_RAZ_SOC '+
               'from SP_CAMBIOS( ''%s'', ''%s'' ) A '+
               'join SISTEMA S on ( S.SI_FOLIO = A.SI_FOLIO ) '+
               'order by A.SI_FOLIO, A.AU_FOLIO';
begin
     with qAuditoria do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( Format( Q_AUDIT, [ ZetaCommonTools.DateToStrSQL( dInicial ), ZetaCommonTools.DateToStrSQL( dFinal ) ] ) );
          end;
          Prepare;
          Active := True;
          if IsEmpty then
          begin
               ZetaDialogo.zInformation( '� Atenci�n !', 'No Hubo Autorizaciones En El Per�odo Especificado', 0 );
               Result := False;
          end
          else
          begin
               InitAuditoria;
               with RepAudit do
               begin
                    Inicio := dInicial;
                    Final := dFinal;
                    Impresora := lImprimir;
                    if lImprimir then
                       qrp1.Preview
                    else
                    begin
                         with qrp1 do
                         begin
                              Prepare;
                              ExportToFilter( TQRXLSFilter.Create( sArchivo ) );
                         end;
                    end;
               end;
               Result := True;
          end;
          Active := False;
     end;
end;

procedure TdmRep.qAuditoriaAfterOpen(DataSet: TDataSet);
var
   i: Integer;
begin
     with Dataset do
     begin
          FieldByName( 'AU_FEC_AUT' ).OnGetText := FechaCortaGetText;
          FieldByName( 'AU_FECHA' ).OnGetText := FechaCortaGetText;
          FieldByName( 'AU_NUM_EMP' ).OnGetText := FloatGetText;
          FieldByName( 'AU_SQL_BD' ).OnGetText := SQLGetText;
          FieldByName( 'AU_PLATFORM' ).OnGetText := PlatformGetText;
          FieldByName( 'AU_TIPOS' ).OnGetText := TiposGetText;
          for i := 0 to Ord( High( TModulos ) ) do
          begin
               FieldByName( Format( 'AU_MOD_%d', [ i ] ) ).OnGetText := ModuloGetText;
          end;
     end;
end;

function TdmRep.ImprimeModulosUsados( const iModulos: DWord; const lKits, lVigentes, lImprimir: Boolean; const sArchivo: String ): Boolean;
const
     Q_MODULOS = 'select SI_FOLIO, SI_RAZ_SOC, SI_EMPRESA, '+
                 'AU_MOD_0, AU_MOD_1, AU_MOD_2, AU_MOD_3, AU_MOD_4, '+
                 'AU_MOD_5, AU_MOD_6, AU_MOD_7, AU_MOD_8, AU_MOD_9, '+
                 'AU_MOD_10, AU_MOD_11, AU_MOD_12, AU_MOD_13, AU_MOD_14, '+
                 'AU_MOD_15, AU_MOD_16, AU_MOD_17, AU_MOD_18, AU_MOD_19, '+
                 'AU_MOD_20 from ACTUAL where %s order by SI_RAZ_SOC';
     K_FILTRO_KIT = '( AU_KIT_DIS = %d )';
     K_FILTRO_VIGENCIA = '( ( AU_DEFINIT = %d ) or ( AU_FEC_AUT > ''%s'' ) )';
var
   eModulo: TModulos;
   i: Integer;
   sFiltro: String;
begin
     for eModulo := Low( TModulos ) to High( TModulos ) do
     begin
          i := Ord( eModulo );
          if FAutoClasses.BitSet( iModulos, i ) then
          begin
               sFiltro := ZetaCommonTools.ConcatString( sFiltro, Format( '( AU_MOD_%d = %d )', [ i, K_INT_TRUE ] ), ' or ' );
          end;
     end;
     sFiltro := Format( '( %s )', [ sFiltro ] );
     if lKits then
        sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_FILTRO_KIT, [ K_INT_TRUE ] ) )
     else
         sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_FILTRO_KIT, [ K_INT_FALSE ] ) );
     if lVigentes then
        sFiltro := ZetaCommonTools.ConcatFiltros( sFiltro, Format( K_FILTRO_VIGENCIA, [ K_INT_TRUE, ZetaCommonTools.DateToStrSQL( Date ) ] ) );
     with qModulos do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( Format( Q_MODULOS, [ sFiltro ] ) );
          end;
          Prepare;
          Active := True;
          if IsEmpty then
          begin
               ZetaDialogo.zInformation( '� Atenci�n !', 'No Hay Clientes Quienes Usen Los M�dulos Indicados', 0 );
               Result := False;
          end
          else
          begin
               InitModulosUsados;
               with RepModulosUsados do
               begin
                    Impresora := lImprimir;
                    if lImprimir then
                       qrp1.Preview
                    else
                    begin
                         with qrp1 do
                         begin
                              Prepare;
                              ExportToFilter( TQRXLSFilter.Create( sArchivo ) );
                         end;
                    end;
               end;
               Result := True;
          end;
          Active := False;
     end;
end;

procedure TdmRep.qModulosAfterOpen(DataSet: TDataSet);
var
   i: Integer;
begin
     with Dataset do
     begin
          for i := 0 to Ord( High( TModulos ) ) do
          begin
               FieldByName( Format( 'AU_MOD_%d', [ i ] ) ).OnGetText := ModuloGetText;
          end;
     end;
end;

function TdmRep.ImprimeVencimientos( const dInicial, dFinal: TDate ): Boolean;
const
     Q_VENCIMIENTOS = 'select A.SI_FOLIO, A.SI_RAZ_SOC, A.DI_CODIGO, A.AU_FEC_AUT, '+
                      'A.SI_PERSONA, A.SI_TEL1, A.SI_TEL2, A.SI_FAX, A.SI_EMAIL, '+
                      'D.DI_NOMBRE, D.DI_PERSONA, D.DI_EMAIL, D.DI_TEL1, D.DI_TEL2, '+
                      'C.CT_DESCRIP, E.ET_DESCRIP '+
                      'from ACTUAL A '+
                      'left outer join DISTRIBU D on ( D.DI_CODIGO = A.DI_CODIGO ) '+
                      'left outer join CIUDAD C on ( C.CT_CODIGO = A.CT_CODIGO ) '+
                      'left outer join ESTADO E on ( E.ET_CODIGO = A.ET_CODIGO ) '+
                      'where ( A.AU_FEC_AUT >= ''%s'' ) and ( A.AU_FEC_AUT <= ''%s'' ) '+
                      'order by A.DI_CODIGO, A.AU_FEC_AUT, A.SI_FOLIO';
begin
     with qRepVence do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( Format( Q_VENCIMIENTOS, [ ZetaCommonTools.DateToStrSQL( dInicial ), ZetaCommonTools.DateToStrSQL( dFinal ) ] ) );
          end;
          Prepare;
          Active := True;
          if IsEmpty then
          begin
               ZetaDialogo.zInformation( '� Atenci�n !', 'No Hay Claves Por Vencer En El Per�odo Especificado', 0 );
               Result := False;
          end
          else
          begin
               InitRepVence;
               with RepVence do
               begin
                    qrp1.Preview;
               end;
               Result := True;
          end;
          Active := False;
     end;
end;

procedure TdmRep.qRepVenceAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'AU_FEC_AUT' ).OnGetText := FechaGetText;
     end;
end;

function TdmRep.ImprimeUsoSentinel: Boolean;
begin
     with qUsoSentinel do
     begin
          Active := True;
          if IsEmpty then
          begin
               ZetaDialogo.zInformation( '� Atenci�n !', 'No Hay Sentinelas Usados', 0 );
               Result := False;
          end
          else
          begin
               InitUsoSentinel;
               with RepUsoSentinel do
               begin
                    qrp1.Preview;
               end;
               Result := True;
          end;
          Active := False;
     end;
end;

procedure TdmRep.ImprimeClaveInstalacion( const iSistema, iSentinel, iFolio: Integer );
begin
     with qInstalaciones do
     begin
          Active := False;
          ParamByName( 'SI_FOLIO' ).AsInteger := iSistema;
          ParamByName( 'SN_NUMERO' ).AsInteger := iSentinel;
          ParamByName( 'IN_FOLIO' ).AsInteger := iFolio;
          Active := True;
          if IsEmpty then
          begin
               ZetaDialogo.zInformation( '� Atenci�n !', Format( 'No Hay Claves De Instalacion Para El Sistema # %d, Sentinel # %d y Folio %d', [ iSistema, iSentinel, iFolio ] ), 0 );
          end
          else
          begin
               InitInstalaciones;
               with RepInstalaciones do
               begin
                    qrp1.Preview;
               end;
          end;
          Active := False;
     end;
end;

end.
