unit FUsuarios;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, Grids, DBGrids, DBCtrls, ExtCtrls, StdCtrls,
     FCatalogo, Buttons;

type
  TFormUsuarios = class(TFormCatalogo)
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormUsuarios: TFormUsuarios;

implementation

uses DSentinel;

{$R *.DFM}

procedure TFormUsuarios.FormShow(Sender: TObject);
begin
     inherited;
     dmSentinel.AbrirUsuario( dsCatalogo );
end;

procedure TFormUsuarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     dmSentinel.ResetDatasource( dsCatalogo );
end;

end.
