/* Extract Database f-server:D:\Datos\Users\Lety\Sentinel\Sentinel.gdb */
/* connect "DELL-GERMAN2:D:\Sentinel\sentinel.gdb" USER "SYSDBA" PASSWORD "m"; */
/* connect "TRESS_2001:E:\Admon\Sentinel\sentinel.gdb" USER "SYSDBA" PASSWORD "m"; */

/* Autorizaci�n del m�dulo de Accesos */
/* alter table AUTORIZA add AU_MOD_19 BOOLEANO; */

/* Autorizaci�n del m�dulo de Visitantes */
/* alter table AUTORIZA add AU_MOD_20 BOOLEANO;*/

/* Pr�stamo del m�dulo de Accesos */
/* alter table AUTORIZA add AU_PRE_19 BOOLEANO;*/

/* Pr�stamo del m�dulo de Visitantes */
/* alter table AUTORIZA add AU_PRE_20 BOOLEANO;*/

/* Drop Views */
drop view ASIGNACION;
drop view ASIGNADO;
drop view LIBRE;
drop view ACTUAL;

/* Create Views */
create view ACTUAL (SI_FOLIO, SI_RAZ_SOC, SI_EMPRESA, SI_OTRO, SI_CALLE, GR_CODIGO, SI_COLONIA, SI_PERSONA, CT_CODIGO, PA_CODIGO, SI_TEL1, SI_TEL2, SI_FAX, SI_EMAIL, SI_COMENTA, SI_GTE_RH, SI_GTE_CON, SI_GTE_SIS, SI_EMA_RH, SI_EMA_CON, SI_EMA_SIS, SI_GIRO, GI_CODIGO, SI_MIGRADO, SI_PROBLEMA, SI_FEC_SOL, SI_EMP_EMP, SI_SIS_VIE, AU_FOLIO, AU_FECHA, SN_NUMERO, DI_CODIGO, VE_CODIGO, US_CODIGO, AU_NUM_EMP,
AU_MOD_0, AU_MOD_1, AU_MOD_2, AU_MOD_3, AU_MOD_4, AU_MOD_5, AU_MOD_6, AU_MOD_7, AU_MOD_8, AU_MOD_9, AU_MOD_10, AU_MOD_11, AU_MOD_12, AU_MOD_13, AU_MOD_14, AU_MOD_15, AU_MOD_16, AU_MOD_17, AU_MOD_18, AU_MOD_19, AU_MOD_20,
AU_USERS, AU_KIT_DIS, AU_CLAVE1, AU_CLAVE2, AU_FEC_GRA, AU_COMENTA,
AU_PRE_0, AU_PRE_1, AU_PRE_2, AU_PRE_3, AU_PRE_4, AU_PRE_5, AU_PRE_6, AU_PRE_7, AU_PRE_8, AU_PRE_9, AU_PRE_10, AU_PRE_11, AU_PRE_12, AU_PRE_13, AU_PRE_14, AU_PRE_15, AU_PRE_16, AU_PRE_17, AU_PRE_18, AU_PRE_19, AU_PRE_20,
AU_FEC_AUT, AU_FEC_PRE, AU_PAR_AUT, AU_PAR_PRE, AU_DEFINIT, AU_TIPOS, ET_CODIGO, AU_SQL_BD, AU_PLATFORM) AS
select S.SI_FOLIO,
       S.SI_RAZ_SOC,
       S.SI_EMPRESA,
       S.SI_OTRO,
       S.SI_CALLE,
       S.GR_CODIGO,
       S.SI_COLONIA,
       S.SI_PERSONA,
       S.CT_CODIGO,
       S.PA_CODIGO,
       S.SI_TEL1,
       S.SI_TEL2,
       S.SI_FAX,
       S.SI_EMAIL,
       S.SI_COMENTA,
       S.SI_GTE_RH,
       S.SI_GTE_CON,
       S.SI_GTE_SIS,
       S.SI_EMA_RH,
       S.SI_EMA_CON,
       S.SI_EMA_SIS,
       S.SI_GIRO,
       S.GI_CODIGO,
       S.SI_MIGRADO,
       S.SI_PROBLEMA,
       S.SI_FEC_SOL,
       S.SI_EMP_EMP,
       S.SI_SIS_VIE,
       A.AU_FOLIO,
       A.AU_FECHA,
       A.SN_NUMERO,
       A.DI_CODIGO,
       A.VE_CODIGO,
       A.US_CODIGO,
       A.AU_NUM_EMP,
       A.AU_MOD_0,
       A.AU_MOD_1,
       A.AU_MOD_2,
       A.AU_MOD_3,
       A.AU_MOD_4,
       A.AU_MOD_5,
       A.AU_MOD_6,
       A.AU_MOD_7,
       A.AU_MOD_8,
       A.AU_MOD_9,
       A.AU_MOD_10,
       A.AU_MOD_11,
       A.AU_MOD_12,
       A.AU_MOD_13,
       A.AU_MOD_14,
       A.AU_MOD_15,
       A.AU_MOD_16,
       A.AU_MOD_17,
       A.AU_MOD_18,
       A.AU_MOD_19,
       A.AU_MOD_20,
       A.AU_USERS,
       A.AU_KIT_DIS,
       A.AU_CLAVE1,
       A.AU_CLAVE2,
       A.AU_FEC_GRA,
       A.AU_COMENTA,
       A.AU_PRE_0,
       A.AU_PRE_1,
       A.AU_PRE_2,
       A.AU_PRE_3,
       A.AU_PRE_4,
       A.AU_PRE_5,
       A.AU_PRE_6,
       A.AU_PRE_7,
       A.AU_PRE_8,
       A.AU_PRE_9,
       A.AU_PRE_10,
       A.AU_PRE_11,
       A.AU_PRE_12,
       A.AU_PRE_13,
       A.AU_PRE_14,
       A.AU_PRE_15,
       A.AU_PRE_16,
       A.AU_PRE_17,
       A.AU_PRE_18,
       A.AU_PRE_19,
       A.AU_PRE_20,
       A.AU_FEC_AUT,
       A.AU_FEC_PRE,
       A.AU_PAR_AUT,
       A.AU_PAR_PRE,
       A.AU_DEFINIT,
       A.AU_TIPOS,
       C.ET_CODIGO,
       A.AU_SQL_BD,
       A.AU_PLATFORM
from SISTEMA S
left outer join AUTORIZA A on ( A.SI_FOLIO = S.SI_FOLIO ) and ( A.AU_FOLIO = S.AU_FOLIO )
left outer join CIUDAD C on ( C.CT_CODIGO = S.CT_CODIGO );

create view ASIGNACION (SN_NUMERO, SI_FOLIO) as
select SN_NUMERO,
       ( select MAX( A.SI_FOLIO ) from AUTORIZA A where
       ( A.SN_NUMERO = S.SN_NUMERO ) and
       ( A.AU_FECHA = ( select MAX( X.AU_FECHA ) from AUTORIZA X where ( X.SN_NUMERO = A.SN_NUMERO ) ) ) ) as SI_FOLIO
from SENTINEL S;

create view ASIGNADO (SN_NUMERO, SI_FOLIO) as
select SENTINEL.SN_NUMERO, ACTUAL.SI_FOLIO from SENTINEL
left outer join ACTUAL on ( ACTUAL.SN_NUMERO = SENTINEL.SN_NUMERO );

create view LIBRE (SN_NUMERO) as
select SN_NUMERO from SENTINEL where( ( select COUNT(*) from ACTUAL where ( ACTUAL.SN_NUMERO = SENTINEL.SN_NUMERO ) ) = 0 );

set term ; ^
commit work ;
set autoddl on;
set term ^ ;

/* Create Stored Procedures */

ALTER PROCEDURE SP_INITIAL_STATE (FOLIO INTEGER, INICIO DATE)
RETURNS (TT_TIPO INTEGER,
SI_FOLIO INTEGER,
SN_NUMERO INTEGER,
VE_CODIGO CHAR(8),
AU_FOLIO SMALLINT,
AU_FECHA DATE,
AU_FEC_AUT DATE,
AU_TIPOS VARCHAR(50),
AU_NUM_EMP INTEGER,
AU_MOD_0 SMALLINT,
AU_MOD_1 SMALLINT,
AU_MOD_2 SMALLINT,
AU_MOD_3 SMALLINT,
AU_MOD_4 SMALLINT,
AU_MOD_5 SMALLINT,
AU_MOD_6 SMALLINT,
AU_MOD_7 SMALLINT,
AU_MOD_8 SMALLINT,
AU_MOD_9 SMALLINT,
AU_MOD_10 SMALLINT,
AU_MOD_11 SMALLINT,
AU_MOD_12 SMALLINT,
AU_MOD_13 SMALLINT,
AU_MOD_14 SMALLINT,
AU_MOD_15 SMALLINT,
AU_MOD_16 SMALLINT,
AU_MOD_17 SMALLINT,
AU_MOD_18 SMALLINT,
AU_MOD_19 SMALLINT,
AU_MOD_20 SMALLINT,
AU_USERS SMALLINT,
AU_KIT_DIS SMALLINT,
AU_SQL_BD SMALLINT,
AU_PLATFORM SMALLINT)
AS

begin
     select 0 as TT_TIPO,
            A.SI_FOLIO,
            A.SN_NUMERO,
            A.VE_CODIGO,
            A.AU_FOLIO,
            A.AU_FECHA,
            A.AU_FEC_AUT,
            'ANTES' as AU_TIPOS,
            A.AU_NUM_EMP,
            A.AU_MOD_0,
            A.AU_MOD_1,
            A.AU_MOD_2,
            A.AU_MOD_3,
            A.AU_MOD_4,
            A.AU_MOD_5,
            A.AU_MOD_6,
            A.AU_MOD_7,
            A.AU_MOD_8,
            A.AU_MOD_9,
            A.AU_MOD_10,
            A.AU_MOD_11,
            A.AU_MOD_12,
            A.AU_MOD_13,
            A.AU_MOD_14,
            A.AU_MOD_15,
            A.AU_MOD_16,
            A.AU_MOD_17,
            A.AU_MOD_18,
            A.AU_MOD_19,
            A.AU_MOD_20,
            A.AU_USERS,
            A.AU_KIT_DIS,
            A.AU_SQL_BD,
            A.AU_PLATFORM
     from AUTORIZA A where
     ( A.SI_FOLIO = :Folio ) and
     ( A.AU_FOLIO = ( select MAX( X.AU_FOLIO ) from AUTORIZA X where
     ( X.SI_FOLIO = A.SI_FOLIO ) and
     ( UPPER( X.AU_TIPOS ) <> 'GENERAL' ) and
     ( X.AU_FECHA < :Inicio ) ) )
     into TT_TIPO,
          SI_FOLIO,
          SN_NUMERO,
          VE_CODIGO,
          AU_FOLIO,
          AU_FECHA,
          AU_FEC_AUT,
          AU_TIPOS,
          AU_NUM_EMP,
          AU_MOD_0,
          AU_MOD_1,
          AU_MOD_2,
          AU_MOD_3,
          AU_MOD_4,
          AU_MOD_5,
          AU_MOD_6,
          AU_MOD_7,
          AU_MOD_8,
          AU_MOD_9,
          AU_MOD_10,
          AU_MOD_11,
          AU_MOD_12,
          AU_MOD_13,
          AU_MOD_14,
          AU_MOD_15,
          AU_MOD_16,
          AU_MOD_17,
          AU_MOD_18,
          AU_MOD_19,
          AU_MOD_20,
          AU_USERS,
          AU_KIT_DIS,
          AU_SQL_BD,
          AU_PLATFORM;
     if ( SI_FOLIO is NULL ) then
     begin
          SI_FOLIO = Folio;
          SN_NUMERO = 0;
          VE_CODIGO = '';
          AU_FOLIO = 0;
          AU_FECHA = '01/01/1900';
          AU_NUM_EMP = 0;
          AU_MOD_0 = 0;
          AU_MOD_1 = 0;
          AU_MOD_2 = 0;
          AU_MOD_3 = 0;
          AU_MOD_4 = 0;
          AU_MOD_5 = 0;
          AU_MOD_6 = 0;
          AU_MOD_7 = 0;
          AU_MOD_8 = 0;
          AU_MOD_9 = 0;
          AU_MOD_10 = 0;
          AU_MOD_11 = 0;
          AU_MOD_12 = 0;
          AU_MOD_13 = 0;
          AU_MOD_14 = 0;
          AU_MOD_15 = 0;
          AU_MOD_16 = 0;
          AU_MOD_17 = 0;
          AU_MOD_18 = 0;
          AU_MOD_19 = 0;
          AU_MOD_20 = 0;
          AU_USERS = 0;
          AU_KIT_DIS = 0;
          AU_SQL_BD = 0;
          AU_PLATFORM = 0;
     end
     suspend;
end ^

ALTER PROCEDURE SP_CAMBIOS (INICIO DATE,
FINAL DATE)
RETURNS (TT_TIPO INTEGER,
SI_FOLIO INTEGER,
SN_NUMERO INTEGER,
VE_CODIGO CHAR(8),
AU_FOLIO SMALLINT,
AU_FECHA DATE,
AU_FEC_AUT DATE,
AU_TIPOS VARCHAR(50),
AU_NUM_EMP INTEGER,
AU_MOD_0 SMALLINT,
AU_MOD_1 SMALLINT,
AU_MOD_2 SMALLINT,
AU_MOD_3 SMALLINT,
AU_MOD_4 SMALLINT,
AU_MOD_5 SMALLINT,
AU_MOD_6 SMALLINT,
AU_MOD_7 SMALLINT,
AU_MOD_8 SMALLINT,
AU_MOD_9 SMALLINT,
AU_MOD_10 SMALLINT,
AU_MOD_11 SMALLINT,
AU_MOD_12 SMALLINT,
AU_MOD_13 SMALLINT,
AU_MOD_14 SMALLINT,
AU_MOD_15 SMALLINT,
AU_MOD_16 SMALLINT,
AU_MOD_17 SMALLINT,
AU_MOD_18 SMALLINT,
AU_MOD_19 SMALLINT,
AU_MOD_20 SMALLINT,
AU_USERS SMALLINT,
AU_KIT_DIS SMALLINT,
AU_SQL_BD SMALLINT,
AU_PLATFORM SMALLINT)
AS

  declare variable Folio Integer;
  declare variable Cambios SmallInt;
  declare variable xSN_NUMERO Integer;
  declare variable xVE_CODIGO Char(8);
  declare variable xAU_FOLIO SmallInt;
  declare variable xAU_FECHA Date;
  declare variable xAU_NUM_EMP Integer;
  declare variable xAU_MOD_0 SmallInt;
  declare variable xAU_MOD_1 SmallInt;
  declare variable xAU_MOD_2 SmallInt;
  declare variable xAU_MOD_3 SmallInt;
  declare variable xAU_MOD_4 SmallInt;
  declare variable xAU_MOD_5 SmallInt;
  declare variable xAU_MOD_6 SmallInt;
  declare variable xAU_MOD_7 SmallInt;
  declare variable xAU_MOD_8 SmallInt;
  declare variable xAU_MOD_9 SmallInt;
  declare variable xAU_MOD_10 SmallInt;
  declare variable xAU_MOD_11 SmallInt;
  declare variable xAU_MOD_12 SmallInt;
  declare variable xAU_MOD_13 SmallInt;
  declare variable xAU_MOD_14 SmallInt;
  declare variable xAU_MOD_15 SmallInt;
  declare variable xAU_MOD_16 SmallInt;
  declare variable xAU_MOD_17 SmallInt;
  declare variable xAU_MOD_18 SmallInt;
  declare variable xAU_MOD_19 SmallInt;
  declare variable xAU_MOD_20 SmallInt;
  declare variable xAU_USERS SmallInt;
  declare variable xAU_KIT_DIS SmallInt;
  declare variable xAU_SQL_BD SmallInt;
  declare variable xAU_PLATFORM SmallInt;
  declare variable Cambio SmallInt;
begin
     for
        select S.SI_FOLIO from SISTEMA S where
        ( ( select COUNT( * ) from AUTORIZA A where
        ( A.SI_FOLIO = S.SI_FOLIO ) and
        ( UPPER( A.AU_TIPOS ) <> 'GENERAL' ) and
        ( A.AU_FECHA >= :Inicio )and
        ( A.AU_FECHA <= :Final ) ) > 0 )
        order by S.SI_FOLIO
        into :Folio
     do
     begin
          select TT_TIPO,
                 SI_FOLIO,
                 SN_NUMERO,
                 VE_CODIGO,
                 AU_FOLIO,
                 AU_FECHA,
                 AU_FEC_AUT,
                 AU_TIPOS,
                 AU_NUM_EMP,
                 AU_MOD_0,
                 AU_MOD_1,
                 AU_MOD_2,
                 AU_MOD_3,
                 AU_MOD_4,
                 AU_MOD_5,
                 AU_MOD_6,
                 AU_MOD_7,
                 AU_MOD_8,
                 AU_MOD_9,
                 AU_MOD_10,
                 AU_MOD_11,
                 AU_MOD_12,
                 AU_MOD_13,
                 AU_MOD_14,
                 AU_MOD_15,
                 AU_MOD_16,
                 AU_MOD_17,
                 AU_MOD_18,
                 AU_MOD_19,
                 AU_MOD_20,
                 AU_USERS,
                 AU_KIT_DIS,
                 AU_SQL_BD,
                 AU_PLATFORM
          from SP_INITIAL_STATE( :Folio, :Inicio )
          into TT_TIPO,
               SI_FOLIO,
               SN_NUMERO,
               VE_CODIGO,
               AU_FOLIO,
               AU_FECHA,
               AU_FEC_AUT,
               AU_TIPOS,
               AU_NUM_EMP,
               AU_MOD_0,
               AU_MOD_1,
               AU_MOD_2,
               AU_MOD_3,
               AU_MOD_4,
               AU_MOD_5,
               AU_MOD_6,
               AU_MOD_7,
               AU_MOD_8,
               AU_MOD_9,
               AU_MOD_10,
               AU_MOD_11,
               AU_MOD_12,
               AU_MOD_13,
               AU_MOD_14,
               AU_MOD_15,
               AU_MOD_16,
               AU_MOD_17,
               AU_MOD_18,
               AU_MOD_19,
               AU_MOD_20,
               AU_USERS,
               AU_KIT_DIS,
               AU_SQL_BD,
               AU_PLATFORM;
          xSN_NUMERO = SN_NUMERO;
          xVE_CODIGO = VE_CODIGO;
          xAU_FOLIO = AU_FOLIO;
          xAU_FECHA = AU_FECHA;
          xAU_NUM_EMP = AU_NUM_EMP;
          xAU_MOD_0 = AU_MOD_0;
          xAU_MOD_1 = AU_MOD_1;
          xAU_MOD_2 = AU_MOD_2;
          xAU_MOD_3 = AU_MOD_3;
          xAU_MOD_4 = AU_MOD_4;
          xAU_MOD_5 = AU_MOD_5;
          xAU_MOD_6 = AU_MOD_6;
          xAU_MOD_7 = AU_MOD_7;
          xAU_MOD_8 = AU_MOD_8;
          xAU_MOD_9 = AU_MOD_9;
          xAU_MOD_10 = AU_MOD_10;
          xAU_MOD_11 = AU_MOD_11;
          xAU_MOD_12 = AU_MOD_12;
          xAU_MOD_13 = AU_MOD_13;
          xAU_MOD_14 = AU_MOD_14;
          xAU_MOD_15 = AU_MOD_15;
          xAU_MOD_16 = AU_MOD_16;
          xAU_MOD_17 = AU_MOD_17;
          xAU_MOD_18 = AU_MOD_18;
          xAU_MOD_19 = AU_MOD_19;
          xAU_MOD_20 = AU_MOD_20;
          xAU_USERS = AU_USERS;
          xAU_KIT_DIS = AU_KIT_DIS;
          xAU_SQL_BD = AU_SQL_BD;
          xAU_PLATFORM = AU_PLATFORM;
          Cambios = 0;
          for
             select 0 as TT_TIPO,
                    A.SI_FOLIO,
                    A.SN_NUMERO,
                    A.VE_CODIGO,
                    A.AU_FOLIO,
                    A.AU_FECHA,
                    A.AU_FEC_AUT,
                    A.AU_TIPOS,
                    A.AU_NUM_EMP,
                    A.AU_MOD_0,
                    A.AU_MOD_1,
                    A.AU_MOD_2,
                    A.AU_MOD_3,
                    A.AU_MOD_4,
                    A.AU_MOD_5,
                    A.AU_MOD_6,
                    A.AU_MOD_7,
                    A.AU_MOD_8,
                    A.AU_MOD_9,
                    A.AU_MOD_10,
                    A.AU_MOD_11,
                    A.AU_MOD_12,
                    A.AU_MOD_13,
                    A.AU_MOD_14,
                    A.AU_MOD_15,
                    A.AU_MOD_16,
                    A.AU_MOD_17,
                    A.AU_MOD_18,
                    A.AU_MOD_19,
                    A.AU_MOD_20,
                    A.AU_USERS,
                    A.AU_KIT_DIS,
                    A.AU_SQL_BD,
                    A.AU_PLATFORM
             from AUTORIZA A where
             ( A.SI_FOLIO = :Folio ) and
             ( A.AU_FECHA >= :Inicio ) and
             ( A.AU_FECHA <= :Final )
             order by A.AU_FOLIO
             into TT_TIPO,
                  SI_FOLIO,
                  SN_NUMERO,
                  VE_CODIGO,
                  AU_FOLIO,
                  AU_FECHA,
                  AU_FEC_AUT,
                  AU_TIPOS,
                  AU_NUM_EMP,
                  AU_MOD_0,
                  AU_MOD_1,
                  AU_MOD_2,
                  AU_MOD_3,
                  AU_MOD_4,
                  AU_MOD_5,
                  AU_MOD_6,
                  AU_MOD_7,
                  AU_MOD_8,
                  AU_MOD_9,
                  AU_MOD_10,
                  AU_MOD_11,
                  AU_MOD_12,
                  AU_MOD_13,
                  AU_MOD_14,
                  AU_MOD_15,
                  AU_MOD_16,
                  AU_MOD_17,
                  AU_MOD_18,
                  AU_MOD_19,
                  AU_MOD_20,
                  AU_USERS,
                  AU_KIT_DIS,
                  AU_SQL_BD,
                  AU_PLATFORM
          do
          begin
               if ( SN_NUMERO <> xSN_NUMERO ) then
               begin
                    TT_TIPO = TT_TIPO + 1;
                    xSN_NUMERO = SN_NUMERO;
               end
               if ( VE_CODIGO <> xVE_CODIGO ) then
               begin
                    TT_TIPO = TT_TIPO + 2;
                    xVE_CODIGO = VE_CODIGO;
               end
               if ( AU_NUM_EMP <> xAU_NUM_EMP ) then
               begin
                    TT_TIPO = TT_TIPO + 4;
                    xAU_NUM_EMP = AU_NUM_EMP;
               end
               if ( AU_USERS <> xAU_USERS ) then
               begin
                    TT_TIPO = TT_TIPO + 8;
                    xAU_USERS = AU_USERS;
               end
               if ( AU_KIT_DIS <> xAU_KIT_DIS ) then
               begin
                    TT_TIPO = TT_TIPO + 16;
                    xAU_KIT_DIS = AU_KIT_DIS;
               end
               if ( AU_SQL_BD <> xAU_SQL_BD ) then
               begin
                    TT_TIPO = TT_TIPO + 32;
                    xAU_SQL_BD = AU_SQL_BD;
               end
               if ( AU_PLATFORM <> xAU_PLATFORM ) then
               begin
                    TT_TIPO = TT_TIPO + 64;
                    xAU_PLATFORM = AU_PLATFORM;
               end
               if ( AU_MOD_0 <> xAU_MOD_0 ) then
               begin
                    TT_TIPO = TT_TIPO + 128;
                    xAU_MOD_0 = AU_MOD_0;
               end
               if ( AU_MOD_1 <> xAU_MOD_1 ) then
               begin
                    TT_TIPO = TT_TIPO + 256;
                    xAU_MOD_1 = AU_MOD_1;
               end
               if ( AU_MOD_2 <> xAU_MOD_2 ) then
               begin
                    TT_TIPO = TT_TIPO + 512;
                    xAU_MOD_2 = AU_MOD_2;
               end
               if ( AU_MOD_3 <> xAU_MOD_3 ) then
               begin
                    TT_TIPO = TT_TIPO + 1024;
                    xAU_MOD_3 = AU_MOD_3;
               end
               if ( AU_MOD_4 <> xAU_MOD_4 ) then
               begin
                    TT_TIPO = TT_TIPO + 2048;
                    xAU_MOD_4 = AU_MOD_4;
               end
               if ( AU_MOD_5 <> xAU_MOD_5 ) then
               begin
                    TT_TIPO = TT_TIPO + 4096;
                    xAU_MOD_5 = AU_MOD_5;
               end
               if ( AU_MOD_6 <> xAU_MOD_6 ) then
               begin
                    TT_TIPO = TT_TIPO + 8192;
                    xAU_MOD_6 = AU_MOD_6;
               end
               if ( AU_MOD_7 <> xAU_MOD_7 ) then
               begin
                    TT_TIPO = TT_TIPO + 16384;
                    xAU_MOD_7 = AU_MOD_7;
               end
               if ( AU_MOD_8 <> xAU_MOD_8 ) then
               begin
                    TT_TIPO = TT_TIPO + 32768;
                    xAU_MOD_8 = AU_MOD_8;
               end
               if ( AU_MOD_9 <> xAU_MOD_9 ) then
               begin
                    TT_TIPO = TT_TIPO + 65536;
                    xAU_MOD_9 = AU_MOD_9;
               end
               if ( AU_MOD_10 <> xAU_MOD_10 ) then
               begin
                    TT_TIPO = TT_TIPO + 131072;
                    xAU_MOD_10 = AU_MOD_10;
               end
               if ( AU_MOD_11 <> xAU_MOD_11 ) then
               begin
                    TT_TIPO = TT_TIPO + 262144;
                    xAU_MOD_11 = AU_MOD_11;
               end
               if ( AU_MOD_12 <> xAU_MOD_12 ) then
               begin
                    TT_TIPO = TT_TIPO + 524288;
                    xAU_MOD_12 = AU_MOD_12;
               end
               if ( AU_MOD_13 <> xAU_MOD_13 ) then
               begin
                    TT_TIPO = TT_TIPO + 1048576;
                    xAU_MOD_13 = AU_MOD_13;
               end
               if ( AU_MOD_14 <> xAU_MOD_14 ) then
               begin
                    TT_TIPO = TT_TIPO + 2097152;
                    xAU_MOD_14 = AU_MOD_14;
               end
               if ( AU_MOD_15 <> xAU_MOD_15 ) then
               begin
                    TT_TIPO = TT_TIPO + 4194304;
                    xAU_MOD_15 = AU_MOD_15;
               end
               if ( AU_MOD_16 <> xAU_MOD_16 ) then
               begin
                    TT_TIPO = TT_TIPO + 8388608;
                    xAU_MOD_16 = AU_MOD_16;
               end
               if ( AU_MOD_17 <> xAU_MOD_17 ) then
               begin
                    TT_TIPO = TT_TIPO + 16777216;
                    xAU_MOD_17 = AU_MOD_17;
               end
               if ( AU_MOD_18 <> xAU_MOD_18 ) then
               begin
                    TT_TIPO = TT_TIPO + 33554432;
                    xAU_MOD_18 = AU_MOD_18;
               end
               if ( AU_MOD_19 <> xAU_MOD_19 ) then
               begin
                    TT_TIPO = TT_TIPO + 67108864;
                    xAU_MOD_19 = AU_MOD_19;
               end
               if ( AU_MOD_20 <> xAU_MOD_20 ) then
               begin
                    TT_TIPO = TT_TIPO + 134217728;
                    xAU_MOD_20 = AU_MOD_20;
               end
               if ( TT_TIPO > 0 ) then
               begin
                    Cambios = 1;
                    suspend;
               end
          end
          if ( ( Cambios > 0 ) and ( xAU_FOLIO > 0 ) ) then
          begin
               select TT_TIPO,
                      SI_FOLIO,
                      SN_NUMERO,
                      VE_CODIGO,
                      AU_FOLIO,
                      AU_FECHA,
                      AU_FEC_AUT,
                      AU_TIPOS,
                      AU_NUM_EMP,
                      AU_MOD_0,
                      AU_MOD_1,
                      AU_MOD_2,
                      AU_MOD_3,
                      AU_MOD_4,
                      AU_MOD_5,
                      AU_MOD_6,
                      AU_MOD_7,
                      AU_MOD_8,
                      AU_MOD_9,
                      AU_MOD_10,
                      AU_MOD_11,
                      AU_MOD_12,
                      AU_MOD_13,
                      AU_MOD_14,
                      AU_MOD_15,
                      AU_MOD_16,
                      AU_MOD_17,
                      AU_MOD_18,
                      AU_MOD_19,
                      AU_MOD_20,
                      AU_USERS,
                      AU_KIT_DIS,
                      AU_SQL_BD,
                      AU_PLATFORM
               from SP_INITIAL_STATE( :Folio, :Inicio )
               into TT_TIPO,
                    SI_FOLIO,
                    SN_NUMERO,
                    VE_CODIGO,
                    AU_FOLIO,
                    AU_FECHA,
                    AU_FEC_AUT,
                    AU_TIPOS,
                    AU_NUM_EMP,
                    AU_MOD_0,
                    AU_MOD_1,
                    AU_MOD_2,
                    AU_MOD_3,
                    AU_MOD_4,
                    AU_MOD_5,
                    AU_MOD_6,
                    AU_MOD_7,
                    AU_MOD_8,
                    AU_MOD_9,
                    AU_MOD_10,
                    AU_MOD_11,
                    AU_MOD_12,
                    AU_MOD_13,
                    AU_MOD_14,
                    AU_MOD_15,
                    AU_MOD_16,
                    AU_MOD_17,
                    AU_MOD_18,
                    AU_MOD_19,
                    AU_MOD_20,
                    AU_USERS,
                    AU_KIT_DIS,
                    AU_SQL_BD,
                    AU_PLATFORM;
               suspend;
          end
     end
end ^

commit work ^
set term ; ^

