/* Extract Database f-server:D:\Datos\Users\Lety\Sentinel\Sentinel.gdb */
/* connect "DELL-GERMAN2:D:\Sentinel\sentinel.gdb" USER "SYSDBA" PASSWORD "m"; */
/* connect "TRESS_2001:E:\Admon\Sentinel\sentinel.gdb" USER "SYSDBA" PASSWORD "m"; */

/* Table: AUDIT, Owner: SYSDBA */
CREATE TABLE AUDIT (
       SI_FOLIO    FOLIOSISTEMA,
       SN_NUMERO   NUMSENTINEL,
       VE_CODIGO   CODIGO,
       AD_FOLIO    FOLIOCHICO,
       AD_FECHA    FECHA,
       US_CODIGO   USUARIO,
       AD_NUM_EMP  EMPLEADOS,
       AD_USERS    FOLIOCHICO,
       AD_SQL_BD   FOLIOCHICO,
       AD_PLATFORM FOLIOCHICO,
       AD_KIT_DIS  BOOLEANO );

ALTER TABLE AUDIT
       ADD PRIMARY KEY ( SI_FOLIO, SN_NUMERO, VE_CODIGO, AD_FOLIO );

CREATE DOMAIN CodigoEmpresa CHAR(10) DEFAULT '' NOT NULL;

/* Table: AUDIT_CIA, Owner: SYSDBA */
CREATE TABLE AUDIT_CIA (
       SI_FOLIO    FOLIOSISTEMA,
       SN_NUMERO   NUMSENTINEL,
       VE_CODIGO   CODIGO,
       AD_FOLIO    FOLIOCHICO,
       CM_CODIGO   CodigoEmpresa,
       CM_NOMBRE   DESC_LARGA,
       AC_ACTIVOS  EMPLEADOS,
       AC_INACTIV  EMPLEADOS,
       AC_TOTALES  EMPLEADOS,
       AC_NIVEL0   CODIGO,
       AC_DB_NAME  DESC_LARGA,
       AC_ULT_ALT  FECHA,
       AC_ULT_BAJ  FECHA,
       AC_ULT_TAR  FECHA,
       AC_ULT_NOM  DESC_LARGA );

ALTER TABLE AUDIT_CIA
       ADD PRIMARY KEY ( SI_FOLIO, SN_NUMERO, VE_CODIGO, AD_FOLIO, CM_CODIGO );

CREATE TRIGGER TD_AUDIT

set term ; ^
commit work ;
set autoddl on;
set term ^ ;


commit work ^
set term ; ^

