/* Extract Database f-server:D:\Datos\Users\Lety\Sentinel\Sentinel.gdb */
CREATE DATABASE "TRESS_2001:E:\Admon\Sentinel\sentinel.gdb" PAGE_SIZE 4096;

/* Domain definitions */
CREATE DOMAIN MEMO AS BLOB SUB_TYPE 0 SEGMENT SIZE 80;
CREATE DOMAIN FECHA AS DATE
         DEFAULT "12/30/1899"
 NOT NULL;
CREATE DOMAIN BOOLEANO AS SMALLINT
         DEFAULT 0
 NOT NULL;
CREATE DOMAIN EMPLEADOS AS INTEGER
         DEFAULT 0
 NOT NULL;
CREATE DOMAIN FOLIOCHICO AS SMALLINT
         DEFAULT 0
 NOT NULL;
CREATE DOMAIN FOLIOSISTEMA AS INTEGER
         DEFAULT 0
 NOT NULL;
CREATE DOMAIN NUMSENTINEL AS INTEGER
         DEFAULT 0
 NOT NULL;
CREATE DOMAIN PARAMETROS AS INTEGER
         DEFAULT 0
 NOT NULL;
CREATE DOMAIN STATUS AS SMALLINT
         DEFAULT 0
 NOT NULL;
CREATE DOMAIN USUARIO AS SMALLINT
         DEFAULT 0
 NOT NULL;
CREATE DOMAIN CODIGO AS CHAR(8)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN CODIGO1 AS CHAR(1)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN DESCRIPCION AS VARCHAR(30)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN HORA AS CHAR(5)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN OBSERVACIONES AS VARCHAR(50)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN PASSWD AS VARCHAR(30)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN USUARIOCORTO AS VARCHAR(15)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN DESC_LARGA AS VARCHAR(255)
         default ""
 NOT NULL;
CREATE DOMAIN EMAIL AS VARCHAR(50)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN EMPRESA AS VARCHAR(80)
         DEFAULT ""
 NOT NULL;
CREATE DOMAIN INSTALLCODE AS VARCHAR(255)
         DEFAULT ''
 NOT NULL;
CREATE DOMAIN COMENTARIO AS VARCHAR(255)
         DEFAULT ''
 NOT NULL;

/* Table: AUTORIZA, Owner: SYSDBA */
CREATE TABLE AUTORIZA (SI_FOLIO FOLIOSISTEMA,
        AU_FOLIO FOLIOCHICO,
        AU_FECHA FECHA,
        DI_CODIGO CODIGO,
        SN_NUMERO NUMSENTINEL,
        VE_CODIGO CODIGO,
        US_CODIGO USUARIOCORTO,
        AU_NUM_EMP EMPLEADOS,
        AU_MOD_0 BOOLEANO,
        AU_MOD_1 BOOLEANO,
        AU_MOD_2 BOOLEANO,
        AU_MOD_3 BOOLEANO,
        AU_MOD_4 BOOLEANO,
        AU_MOD_5 BOOLEANO,
        AU_MOD_6 BOOLEANO,
        AU_MOD_7 BOOLEANO,
        AU_MOD_8 BOOLEANO,
        AU_MOD_9 BOOLEANO,
        AU_USERS FOLIOCHICO,
        AU_KIT_DIS BOOLEANO,
        AU_CLAVE1 DESCRIPCION,
        AU_CLAVE2 DESCRIPCION,
        AU_FEC_GRA FECHA,
        AU_FEC_AUT FECHA,
        AU_COMENTA MEMO,
        AU_FEC_PRE FECHA,
        AU_PAR_AUT PARAMETROS,
        AU_PAR_PRE PARAMETROS,
        AU_PRE_0 BOOLEANO,
        AU_PRE_1 BOOLEANO,
        AU_PRE_2 BOOLEANO,
        AU_PRE_3 BOOLEANO,
        AU_PRE_4 BOOLEANO,
        AU_PRE_5 BOOLEANO,
        AU_PRE_6 BOOLEANO,
        AU_PRE_7 BOOLEANO,
        AU_PRE_8 BOOLEANO,
        AU_PRE_9 BOOLEANO,
        AU_DEFINIT BOOLEANO,
        AU_TIPOS OBSERVACIONES,
        AU_PRE_10 BOOLEANO,
        AU_PRE_11 BOOLEANO,
        AU_PRE_12 BOOLEANO,
        AU_MOD_10 BOOLEANO,
        AU_MOD_11 BOOLEANO,
        AU_MOD_12 BOOLEANO,
        AU_MOD_13 BOOLEANO,
        AU_PRE_13 BOOLEANO,
        AU_SQL_BD FOLIOCHICO,
        AU_PLATFORM FOLIOCHICO,
        AU_MOD_14 BOOLEANO,
        AU_MOD_15 BOOLEANO,
        AU_MOD_16 BOOLEANO,
        AU_PRE_14 BOOLEANO,
        AU_PRE_15 BOOLEANO,
        AU_PRE_16 BOOLEANO,
PRIMARY KEY (SI_FOLIO, AU_FOLIO));

/* Table: BOLETIN, Owner: SYSDBA */
CREATE TABLE BOLETIN (BL_EMAIL EMAIL,
        BL_NOMBRE OBSERVACIONES,
        BL_EMPRESA EMPRESA,
        BL_ALTA FECHA,
        BL_BAJA FECHA,
        BL_ACTIVO BOOLEANO,
PRIMARY KEY (BL_EMAIL));

/* Table: CIUDAD, Owner: SYSDBA */
CREATE TABLE CIUDAD (CT_CODIGO CODIGO,
        CT_DESCRIP DESCRIPCION,
        ET_CODIGO CODIGO,
PRIMARY KEY (CT_CODIGO));

/* Table: CRISOL_Y2K, Owner: SYSDBA */
CREATE TABLE CRISOL_Y2K (CR_FOLIO FOLIOSISTEMA,
        CR_FECHA FECHA,
        SN_NUMERO NUMSENTINEL,
        CR_MONO FOLIOCHICO,
        CR_RED FOLIOCHICO,
        CR_CONCHE FOLIOCHICO,
        CR_MERCAD FOLIOCHICO,
        CR_ALMACEN FOLIOCHICO,
        CR_COBRAD FOLIOCHICO,
PRIMARY KEY (CR_FOLIO));

/* Table: DISTRIBU, Owner: SYSDBA */
CREATE TABLE DISTRIBU (DI_CODIGO CODIGO,
        DI_NOMBRE DESCRIPCION,
        CT_CODIGO CODIGO,
        DI_PERSONA DESCRIPCION,
        DI_EMAIL DESCRIPCION,
        DI_TEL1 DESCRIPCION,
        DI_TEL2 DESCRIPCION,
        DI_FAX DESCRIPCION,
PRIMARY KEY (DI_CODIGO));

/* Table: ESTADO, Owner: SYSDBA */
CREATE TABLE ESTADO (ET_CODIGO CODIGO,
        ET_DESCRIP DESCRIPCION,
PRIMARY KEY (ET_CODIGO));

/* Table: GIRO, Owner: SYSDBA */
CREATE TABLE GIRO (GI_CODIGO CODIGO,
        GI_DESCRIP DESCRIPCION,
PRIMARY KEY (GI_CODIGO));

/* Table: GRUPO, Owner: SYSDBA */
CREATE TABLE GRUPO (GR_CODIGO CODIGO,
        GR_DESCRIP DESCRIPCION,
PRIMARY KEY (GR_CODIGO));

/* Table: INSTALACION, Owner: SYSDBA */
CREATE TABLE INSTALACION (SN_NUMERO NUMSENTINEL,
        SI_FOLIO FOLIOSISTEMA,
        DI_CODIGO CODIGO,
        US_CODIGO USUARIOCORTO,
        IN_FOLIO FOLIOCHICO,
        IN_FECHA FECHA,
        IN_CLAVE INSTALLCODE,
        IN_COMENTA COMENTARIO,
PRIMARY KEY (SN_NUMERO, SI_FOLIO, IN_FOLIO));

/* Table: PAIS, Owner: SYSDBA */
CREATE TABLE PAIS (PA_CODIGO CODIGO,
        PA_DESCRIP DESCRIPCION,
PRIMARY KEY (PA_CODIGO));

/* Table: SENTINEL, Owner: SYSDBA */
CREATE TABLE SENTINEL (SN_NUMERO NUMSENTINEL,
        SN_MODELO CODIGO,
        SN_FEC_INI FECHA,
        SN_TIPO STATUS,
PRIMARY KEY (SN_NUMERO));

/* Table: SISTEMA, Owner: SYSDBA */
CREATE TABLE SISTEMA (SI_FOLIO FOLIOSISTEMA,
        SI_EMPRESA OBSERVACIONES,
        SI_OTRO OBSERVACIONES,
        GR_CODIGO CODIGO,
        SI_PERSONA OBSERVACIONES,
        CT_CODIGO CODIGO,
        SI_TEL1 DESCRIPCION,
        SI_TEL2 DESCRIPCION,
        SI_FAX DESCRIPCION,
        SI_EMAIL OBSERVACIONES,
        SI_COMENTA MEMO,
        SI_GTE_RH DESCRIPCION,
        SI_GTE_CON DESCRIPCION,
        SI_GTE_SIS DESCRIPCION,
        SI_EMA_RH OBSERVACIONES,
        SI_EMA_CON OBSERVACIONES,
        SI_EMA_SIS OBSERVACIONES,
        SI_GIRO OBSERVACIONES,
        SI_RAZ_SOC DESC_LARGA,
        SI_CALLE DESC_LARGA,
        SI_COLONIA DESC_LARGA,
        SI_MIGRADO BOOLEANO,
        SI_FEC_SOL FECHA,
        PA_CODIGO CODIGO,
        GI_CODIGO CODIGO,
        SI_EMP_EMP EMPLEADOS,
        SI_SIS_VIE DESCRIPCION,
        SI_PROBLEMA BOOLEANO,
PRIMARY KEY (SI_FOLIO));

/* Table: USUARIO, Owner: SYSDBA */
CREATE TABLE USUARIO (US_CODIGO USUARIOCORTO,
        US_NOMBRE DESCRIPCION,
        DI_CODIGO CODIGO,
        US_PASSWRD PASSWD,
        US_NIVEL STATUS,
PRIMARY KEY (US_CODIGO));

/* Table: VERSION, Owner: SYSDBA */
CREATE TABLE VERSION (VE_CODIGO CODIGO,
        VE_DESCRIP DESCRIPCION,
PRIMARY KEY (VE_CODIGO));

/*  Index definitions for all user tables */
CREATE DESCENDING INDEX XIE1AUTORIZA ON AUTORIZA(SI_FOLIO, AU_FOLIO);
ALTER TABLE CIUDAD ADD FOREIGN KEY (ET_CODIGO) REFERENCES ESTADO(ET_CODIGO);
ALTER TABLE SISTEMA ADD FOREIGN KEY (GR_CODIGO) REFERENCES GRUPO(GR_CODIGO);
ALTER TABLE SISTEMA ADD FOREIGN KEY (CT_CODIGO) REFERENCES CIUDAD(CT_CODIGO);
ALTER TABLE DISTRIBU ADD FOREIGN KEY (CT_CODIGO) REFERENCES CIUDAD(CT_CODIGO);
ALTER TABLE USUARIO ADD FOREIGN KEY (DI_CODIGO) REFERENCES DISTRIBU(DI_CODIGO);
ALTER TABLE AUTORIZA ADD FOREIGN KEY (DI_CODIGO) REFERENCES DISTRIBU(DI_CODIGO);
ALTER TABLE AUTORIZA ADD FOREIGN KEY (US_CODIGO) REFERENCES USUARIO(US_CODIGO);
ALTER TABLE AUTORIZA ADD FOREIGN KEY (VE_CODIGO) REFERENCES VERSION(VE_CODIGO);
ALTER TABLE AUTORIZA ADD FOREIGN KEY (SN_NUMERO) REFERENCES SENTINEL(SN_NUMERO);
ALTER TABLE AUTORIZA ADD FOREIGN KEY (SI_FOLIO) REFERENCES SISTEMA(SI_FOLIO);
ALTER TABLE SISTEMA ADD FOREIGN KEY (GI_CODIGO) REFERENCES GIRO(GI_CODIGO) ON UPDATE CASCADE ON DELETE SET DEFAULT;
ALTER TABLE SISTEMA ADD FOREIGN KEY (PA_CODIGO) REFERENCES PAIS(PA_CODIGO) ON UPDATE CASCADE ON DELETE SET DEFAULT;



/* View: ACTUAL, Owner: SYSDBA */
CREATE VIEW ACTUAL (SI_FOLIO, SI_RAZ_SOC, SI_EMPRESA, SI_OTRO, SI_CALLE, GR_CODIGO, SI_COLONIA, SI_PERSONA, CT_CODIGO, PA_CODIGO, SI_TEL1, SI_TEL2, SI_FAX, SI_EMAIL, SI_COMENTA, SI_GTE_RH, SI_GTE_CON, SI_GTE_SIS, SI_EMA_RH, SI_EMA_CON, SI_EMA_SIS, SI_GIRO, GI_CODIGO, SI_MIGRADO, SI_PROBLEMA, SI_FEC_SOL, SI_EMP_EMP, SI_SIS_VIE, AU_FOLIO, AU_FECHA, SN_NUMERO, DI_CODIGO, VE_CODIGO, US_CODIGO, AU_NUM_EMP, AU_MOD_0, AU_MOD_1, AU_MOD_2, AU_MOD_3, AU_MOD_4, AU_MOD_5, AU_MOD_6, AU_MOD_7, AU_MOD_8, AU_MOD_9, AU_MOD_10, AU_MOD_11, AU_MOD_12, AU_MOD_13, AU_MOD_14, AU_MOD_15, AU_MOD_16, AU_USERS, AU_KIT_DIS, AU_CLAVE1, AU_CLAVE2, AU_FEC_GRA, AU_COMENTA, AU_PRE_0, AU_PRE_1, AU_PRE_2, AU_PRE_3, AU_PRE_4, AU_PRE_5, AU_PRE_6, AU_PRE_7, AU_PRE_8, AU_PRE_9, AU_PRE_10, AU_PRE_11, AU_PRE_12, AU_PRE_13, AU_PRE_14, AU_PRE_15, AU_PRE_16, AU_FEC_AUT, AU_FEC_PRE, AU_PAR_AUT, AU_PAR_PRE, AU_DEFINIT, AU_TIPOS, ET_CODIGO, AU_SQL_BD, AU_PLATFORM) AS
select S.SI_FOLIO,       S.SI_RAZ_SOC,
       S.SI_EMPRESA,
       S.SI_OTRO,
       S.SI_CALLE,
       S.GR_CODIGO,
       S.SI_COLONIA,
       S.SI_PERSONA,
       S.CT_CODIGO,
       S.PA_CODIGO,
       S.SI_TEL1,
       S.SI_TEL2,
       S.SI_FAX,
       S.SI_EMAIL,
       S.SI_COMENTA,
       S.SI_GTE_RH,
       S.SI_GTE_CON,
       S.SI_GTE_SIS,
       S.SI_EMA_RH,
       S.SI_EMA_CON,
       S.SI_EMA_SIS,
       S.SI_GIRO,
       S.GI_CODIGO,
       S.SI_MIGRADO,
       S.SI_PROBLEMA,
       S.SI_FEC_SOL,
       S.SI_EMP_EMP,
       S.SI_SIS_VIE,
       A.AU_FOLIO,
       A.AU_FECHA,
       A.SN_NUMERO,
       A.DI_CODIGO,
       A.VE_CODIGO,
       A.US_CODIGO,
       A.AU_NUM_EMP,
       A.AU_MOD_0,
       A.AU_MOD_1,
       A.AU_MOD_2,
       A.AU_MOD_3,
       A.AU_MOD_4,
       A.AU_MOD_5,
       A.AU_MOD_6,
       A.AU_MOD_7,
       A.AU_MOD_8,
       A.AU_MOD_9,
       A.AU_MOD_10,
       A.AU_MOD_11,
       A.AU_MOD_12,
       A.AU_MOD_13,
       A.AU_MOD_14,
       A.AU_MOD_15,
       A.AU_MOD_16,
       A.AU_USERS,
       A.AU_KIT_DIS,
       A.AU_CLAVE1,
       A.AU_CLAVE2,
       A.AU_FEC_GRA,
       A.AU_COMENTA,
       A.AU_PRE_0,
       A.AU_PRE_1,
       A.AU_PRE_2,
       A.AU_PRE_3,
       A.AU_PRE_4,
       A.AU_PRE_5,
       A.AU_PRE_6,
       A.AU_PRE_7,
       A.AU_PRE_8,
       A.AU_PRE_9,
       A.AU_PRE_10,
       A.AU_PRE_11,
       A.AU_PRE_12,
       A.AU_PRE_13,
       A.AU_PRE_14,
       A.AU_PRE_15,
       A.AU_PRE_16,
       A.AU_FEC_AUT,
       A.AU_FEC_PRE,
       A.AU_PAR_AUT,
       A.AU_PAR_PRE,
       A.AU_DEFINIT,
       A.AU_TIPOS,
       C.ET_CODIGO,
       A.AU_SQL_BD,
       A.AU_PLATFORM
from SISTEMA S
left outer join AUTORIZA A on ( A.SI_FOLIO = S.SI_FOLIO )
left outer join CIUDAD C on ( C.CT_CODIGO = S.CT_CODIGO )
where ( A.AU_FOLIO = ( select MAX( X.AU_FOLIO ) from AUTORIZA X where ( X.SI_FOLIO = A.SI_FOLIO ) ) )
;

/* View: ASIGNACION, Owner: SYSDBA */
CREATE VIEW ASIGNACION (SN_NUMERO, SI_FOLIO) AS
select SN_NUMERO,
       ( select MAX( A.SI_FOLIO ) from AUTORIZA A where
( A.SN_NUMERO = S.SN_NUMERO ) and
( A.AU_FECHA = ( select MAX( X.AU_FECHA ) from AUTORIZA X where ( X.SN_NUMERO = A.SN_NUMERO ) ) ) ) as SI_FOLIO
from SENTINEL S;

/* View: ASIGNADO, Owner: SYSDBA */
CREATE VIEW ASIGNADO (SN_NUMERO, SI_FOLIO) AS
select SENTINEL.SN_NUMERO, ACTUAL.SI_FOLIO from SENTINEL
left outer join ACTUAL on ( ACTUAL.SN_NUMERO = SENTINEL.SN_NUMERO );

/* View: LIBRE, Owner: SYSDBA */
CREATE VIEW LIBRE (SN_NUMERO) AS
select SN_NUMERO from SENTINEL where( ( select COUNT(*) from ACTUAL where ( ACTUAL.SN_NUMERO = SENTINEL.SN_NUMERO ) ) = 0 );

/*  Exceptions */
CREATE EXCEPTION ERWIN_PARENT_INSERT_RESTRICT "Cannot INSERT Parent table because Child table exists.";
CREATE EXCEPTION ERWIN_PARENT_UPDATE_RESTRICT "Cannot UPDATE Parent table because Child table exists.";
CREATE EXCEPTION ERWIN_PARENT_DELETE_RESTRICT "Cannot DELETE Parent table because Child table exists.";
CREATE EXCEPTION ERWIN_CHILD_INSERT_RESTRICT "Cannot INSERT Child table because Parent table does not exist.";
CREATE EXCEPTION ERWIN_CHILD_UPDATE_RESTRICT "Cannot UPDATE Child table because Parent table does not exist.";
CREATE EXCEPTION ERWIN_CHILD_DELETE_RESTRICT "Cannot DELETE Child table because Parent table does not exist.";
CREATE EXCEPTION SENTINEL_ASIGNADO_A_SISTEMA "Sentinel No Se Puede Borrar Porque Est� Asignado A Un Sistema";
CREATE EXCEPTION USUARIO_AUTORIZACIONES "Usuario No Puede Ser Borrado Porque Ya Gener� Claves";
CREATE EXCEPTION CRISOL_Y2K_SENTINEL_NO_EXISTE "Sentinel No Ha Sido Dado de Alta";
CREATE EXCEPTION USUARIO_CLAVES "Usuario No Puede Ser Borrado Porque Ya Gener� Claves De Instalaci�n";
CREATE EXCEPTION DISTRIB_USUARIOS "Distribuidor No Puede Ser Borrado Porque Tiene Usuarios Asignados";
CREATE EXCEPTION DISTRIB_AUTORIZACIONES "Distribuidor No Puede Ser Borrado Porque Tiene Claves Generadas";
CREATE EXCEPTION DISTRIB_CLAVES "Distribuidor No Puede Ser Borrado Porque Tiene Claves De Instalaci�n Generadas";
COMMIT WORK;
SET AUTODDL OFF;
SET TERM ^ ;

/* Stored procedures */
CREATE PROCEDURE AGREGA_AUTO AS BEGIN EXIT; END ^
CREATE PROCEDURE SP_INITIAL_STATE AS BEGIN EXIT; END ^
CREATE PROCEDURE SP_CAMBIOS AS BEGIN EXIT; END ^

ALTER PROCEDURE AGREGA_AUTO (CR_FECHA DATE,
SN_NUMERO SMALLINT,
CR_MONO SMALLINT,
CR_RED SMALLINT,
CR_CONCHE SMALLINT,
CR_MERCAD SMALLINT,
CR_ALMACEN SMALLINT,
CR_COBRAD SMALLINT)
AS 

  declare variable iExiste Integer;
 declare variable iFolio Integer;
begin
  select COUNT(*) from SENTINEL
  where ( SENTINEL.SN_NUMERO = :SN_NUMERO )
  into iExiste;
  if ( iExiste = 0 ) then
  begin
          exception CRISOL_Y2K_SENTINEL_NO_EXISTE;
  end
  else
  begin
     select COUNT(*) from CRISOL_Y2K into iFolio;
     iFolio = iFolio + 1;
     insert into CRISOL_Y2K( CR_FOLIO, CR_FECHA, SN_NUMERO, CR_MONO, CR_RED,  CR_CONCHE, CR_MERCAD,  CR_ALMACEN, CR_COBRAD )
values ( :iFolio, :CR_FECHA, :SN_NUMERO, :CR_MONO, :CR_RED, :CR_CONCHE,
:CR_MERCAD, :CR_ALMACEN, :CR_COBRAD );
end
end
 ^

ALTER PROCEDURE SP_INITIAL_STATE (FOLIO INTEGER,
INICIO DATE)
RETURNS (TT_TIPO INTEGER,
SI_FOLIO INTEGER,
SN_NUMERO INTEGER,
VE_CODIGO CHAR(8),
AU_FOLIO SMALLINT,
AU_FECHA DATE,
AU_FEC_AUT DATE,
AU_TIPOS VARCHAR(50),
AU_NUM_EMP INTEGER,
AU_MOD_0 SMALLINT,
AU_MOD_1 SMALLINT,
AU_MOD_2 SMALLINT,
AU_MOD_3 SMALLINT,
AU_MOD_4 SMALLINT,
AU_MOD_5 SMALLINT,
AU_MOD_6 SMALLINT,
AU_MOD_7 SMALLINT,
AU_MOD_8 SMALLINT,
AU_MOD_9 SMALLINT,
AU_MOD_10 SMALLINT,
AU_MOD_11 SMALLINT,
AU_MOD_12 SMALLINT,
AU_MOD_13 SMALLINT,
AU_MOD_14 SMALLINT,
AU_MOD_15 SMALLINT,
AU_MOD_16 SMALLINT,
AU_USERS SMALLINT,
AU_KIT_DIS SMALLINT,
AU_SQL_BD SMALLINT,
AU_PLATFORM SMALLINT)
AS 

begin
     select 0 as TT_TIPO,
            A.SI_FOLIO,
            A.SN_NUMERO,
            A.VE_CODIGO,
            A.AU_FOLIO,
            A.AU_FECHA,
            A.AU_FEC_AUT,
            'ANTES' as AU_TIPOS,
            A.AU_NUM_EMP,
            A.AU_MOD_0,
            A.AU_MOD_1,
            A.AU_MOD_2,
            A.AU_MOD_3,
            A.AU_MOD_4,
            A.AU_MOD_5,
            A.AU_MOD_6,
            A.AU_MOD_7,
            A.AU_MOD_8,
            A.AU_MOD_9,
            A.AU_MOD_10,
            A.AU_MOD_11,
            A.AU_MOD_12,
            A.AU_MOD_13,
            A.AU_MOD_14,
            A.AU_MOD_15,
            A.AU_MOD_16,
            A.AU_USERS,
            A.AU_KIT_DIS,
            A.AU_SQL_BD,
            A.AU_PLATFORM
     from AUTORIZA A where
     ( A.SI_FOLIO = :Folio ) and
     ( A.AU_FOLIO = ( select MAX( X.AU_FOLIO ) from AUTORIZA X where
     ( X.SI_FOLIO = A.SI_FOLIO ) and
     ( UPPER( X.AU_TIPOS ) <> 'GENERAL' ) and
     ( X.AU_FECHA < :Inicio ) ) )
     into TT_TIPO,          SI_FOLIO,          SN_NUMERO,
          VE_CODIGO,
          AU_FOLIO,
          AU_FECHA,
          AU_FEC_AUT,
          AU_TIPOS,
          AU_NUM_EMP,
          AU_MOD_0,
          AU_MOD_1,
          AU_MOD_2,
          AU_MOD_3,
          AU_MOD_4,
          AU_MOD_5,
          AU_MOD_6,
          AU_MOD_7,
          AU_MOD_8,
          AU_MOD_9,
          AU_MOD_10,
          AU_MOD_11,
          AU_MOD_12,
          AU_MOD_13,
          AU_MOD_14,
          AU_MOD_15,
          AU_MOD_16,
          AU_USERS,
          AU_KIT_DIS,
          AU_SQL_BD,
          AU_PLATFORM;
     if ( SI_FOLIO is NULL ) then
     begin
          SI_FOLIO = Folio;
          SN_NUMERO = 0;
          VE_CODIGO = '';
          AU_FOLIO = 0;
          AU_FECHA = '01/01/1900';
          AU_NUM_EMP = 0;
          AU_MOD_0 = 0;
          AU_MOD_1 = 0;
          AU_MOD_2 = 0;
          AU_MOD_3 = 0;
          AU_MOD_4 = 0;
          AU_MOD_5 = 0;
          AU_MOD_6 = 0;
          AU_MOD_7 = 0;
          AU_MOD_8 = 0;
          AU_MOD_9 = 0;
          AU_MOD_10 = 0;
          AU_MOD_11 = 0;
          AU_MOD_12 = 0;
          AU_MOD_13 = 0;
          AU_MOD_14 = 0;
          AU_MOD_15 = 0;
          AU_MOD_16 = 0;
          AU_USERS = 0;
          AU_KIT_DIS = 0;
          AU_SQL_BD = 0;
          AU_PLATFORM = 0;
     end
     suspend;
end
 ^

ALTER PROCEDURE SP_CAMBIOS (INICIO DATE,
FINAL DATE)
RETURNS (TT_TIPO INTEGER,
SI_FOLIO INTEGER,
SN_NUMERO INTEGER,
VE_CODIGO CHAR(8),
AU_FOLIO SMALLINT,
AU_FECHA DATE,
AU_FEC_AUT DATE,
AU_TIPOS VARCHAR(50),
AU_NUM_EMP INTEGER,
AU_MOD_0 SMALLINT,
AU_MOD_1 SMALLINT,
AU_MOD_2 SMALLINT,
AU_MOD_3 SMALLINT,
AU_MOD_4 SMALLINT,
AU_MOD_5 SMALLINT,
AU_MOD_6 SMALLINT,
AU_MOD_7 SMALLINT,
AU_MOD_8 SMALLINT,
AU_MOD_9 SMALLINT,
AU_MOD_10 SMALLINT,
AU_MOD_11 SMALLINT,
AU_MOD_12 SMALLINT,
AU_MOD_13 SMALLINT,
AU_MOD_14 SMALLINT,
AU_MOD_15 SMALLINT,
AU_MOD_16 SMALLINT,
AU_USERS SMALLINT,
AU_KIT_DIS SMALLINT,
AU_SQL_BD SMALLINT,
AU_PLATFORM SMALLINT)
AS 

  declare variable Folio Integer;
  declare variable Cambios SmallInt;
  declare variable xSN_NUMERO Integer;
  declare variable xVE_CODIGO Char(8);
  declare variable xAU_FOLIO SmallInt;
  declare variable xAU_FECHA Date;
  declare variable xAU_NUM_EMP Integer;
  declare variable xAU_MOD_0 SmallInt;
  declare variable xAU_MOD_1 SmallInt;
  declare variable xAU_MOD_2 SmallInt;
  declare variable xAU_MOD_3 SmallInt;
  declare variable xAU_MOD_4 SmallInt;
  declare variable xAU_MOD_5 SmallInt;
  declare variable xAU_MOD_6 SmallInt;
  declare variable xAU_MOD_7 SmallInt;
  declare variable xAU_MOD_8 SmallInt;
  declare variable xAU_MOD_9 SmallInt;
  declare variable xAU_MOD_10 SmallInt;
  declare variable xAU_MOD_11 SmallInt;
  declare variable xAU_MOD_12 SmallInt;
  declare variable xAU_MOD_13 SmallInt;
  declare variable xAU_MOD_14 SmallInt;
  declare variable xAU_MOD_15 SmallInt;
  declare variable xAU_MOD_16 SmallInt;
  declare variable xAU_USERS SmallInt;
  declare variable xAU_KIT_DIS SmallInt;
  declare variable xAU_SQL_BD SmallInt;
  declare variable xAU_PLATFORM SmallInt;
  declare variable Cambio SmallInt;
begin
     for
        select S.SI_FOLIO from SISTEMA S where
        ( ( select COUNT( * ) from AUTORIZA A where
        ( A.SI_FOLIO = S.SI_FOLIO ) and
        ( UPPER( A.AU_TIPOS ) <> 'GENERAL' ) and
        ( A.AU_FECHA >= :Inicio )and
        ( A.AU_FECHA <= :Final ) ) > 0 )
        order by S.SI_FOLIO
        into :Folio
     do
     begin
          select TT_TIPO,
                 SI_FOLIO,
                 SN_NUMERO,
                 VE_CODIGO,
                 AU_FOLIO,
                 AU_FECHA,
                 AU_FEC_AUT,
                 AU_TIPOS,
                 AU_NUM_EMP,
                 AU_MOD_0,
                 AU_MOD_1,
                 AU_MOD_2,
                 AU_MOD_3,
                 AU_MOD_4,
                 AU_MOD_5,
                 AU_MOD_6,
                 AU_MOD_7,
                 AU_MOD_8,
                 AU_MOD_9,
                 AU_MOD_10,
                 AU_MOD_11,
                 AU_MOD_12,
                 AU_MOD_13,
                 AU_MOD_14,
                 AU_MOD_15,
                 AU_MOD_16,
                 AU_USERS,
                 AU_KIT_DIS,
                 AU_SQL_BD,
                 AU_PLATFORM
          from SP_INITIAL_STATE( :Folio, :Inicio )
          into TT_TIPO,               SI_FOLIO,               SN_NUMERO,
               VE_CODIGO,
               AU_FOLIO,
               AU_FECHA,
               AU_FEC_AUT,
               AU_TIPOS,
               AU_NUM_EMP,
               AU_MOD_0,
               AU_MOD_1,
               AU_MOD_2,
               AU_MOD_3,
               AU_MOD_4,
               AU_MOD_5,
               AU_MOD_6,
               AU_MOD_7,
               AU_MOD_8,
               AU_MOD_9,
               AU_MOD_10,
               AU_MOD_11,
               AU_MOD_12,
               AU_MOD_13,
               AU_MOD_14,
               AU_MOD_15,
               AU_MOD_16,
               AU_USERS,
               AU_KIT_DIS,
               AU_SQL_BD,
               AU_PLATFORM;
          xSN_NUMERO = SN_NUMERO;
          xVE_CODIGO = VE_CODIGO;
          xAU_FOLIO = AU_FOLIO;
          xAU_FECHA = AU_FECHA;
          xAU_NUM_EMP = AU_NUM_EMP;
          xAU_MOD_0 = AU_MOD_0;
          xAU_MOD_1 = AU_MOD_1;
          xAU_MOD_2 = AU_MOD_2;
          xAU_MOD_3 = AU_MOD_3;
          xAU_MOD_4 = AU_MOD_4;
          xAU_MOD_5 = AU_MOD_5;
          xAU_MOD_6 = AU_MOD_6;
          xAU_MOD_7 = AU_MOD_7;
          xAU_MOD_8 = AU_MOD_8;
          xAU_MOD_9 = AU_MOD_9;
          xAU_MOD_10 = AU_MOD_10;
          xAU_MOD_11 = AU_MOD_11;
          xAU_MOD_12 = AU_MOD_12;
          xAU_MOD_13 = AU_MOD_13;
          xAU_MOD_14 = AU_MOD_14;
          xAU_MOD_15 = AU_MOD_15;
          xAU_MOD_16 = AU_MOD_16;
          xAU_USERS = AU_USERS;
          xAU_KIT_DIS = AU_KIT_DIS;
          xAU_SQL_BD = AU_SQL_BD;
          xAU_PLATFORM = AU_PLATFORM;
          Cambios = 0;
          for
             select 0 as TT_TIPO,
                    A.SI_FOLIO,
                    A.SN_NUMERO,
                    A.VE_CODIGO,
                    A.AU_FOLIO,
                    A.AU_FECHA,
                    A.AU_FEC_AUT,
                    A.AU_TIPOS,
                    A.AU_NUM_EMP,
                    A.AU_MOD_0,
                    A.AU_MOD_1,
                    A.AU_MOD_2,
                    A.AU_MOD_3,
                    A.AU_MOD_4,
                    A.AU_MOD_5,
                    A.AU_MOD_6,
                    A.AU_MOD_7,
                    A.AU_MOD_8,
                    A.AU_MOD_9,
                    A.AU_MOD_10,
                    A.AU_MOD_11,
                    A.AU_MOD_12,
                    A.AU_MOD_13,
                    A.AU_MOD_14,
                    A.AU_MOD_15,
                    A.AU_MOD_16,
                    A.AU_USERS,
                    A.AU_KIT_DIS,
                    A.AU_SQL_BD,
                    A.AU_PLATFORM
             from AUTORIZA A where
             ( A.SI_FOLIO = :Folio ) and
             ( A.AU_FECHA >= :Inicio ) and
             ( A.AU_FECHA <= :Final )
             order by A.AU_FOLIO
             into TT_TIPO,                  SI_FOLIO,                  SN_NUMERO,
                  VE_CODIGO,
                  AU_FOLIO,
                  AU_FECHA,
                  AU_FEC_AUT,
                  AU_TIPOS,
                  AU_NUM_EMP,
                  AU_MOD_0,
                  AU_MOD_1,
                  AU_MOD_2,
                  AU_MOD_3,
                  AU_MOD_4,
                  AU_MOD_5,
                  AU_MOD_6,
                  AU_MOD_7,
                  AU_MOD_8,
                  AU_MOD_9,
                  AU_MOD_10,
                  AU_MOD_11,
                  AU_MOD_12,
                  AU_MOD_13,
                  AU_MOD_14,
                  AU_MOD_15,
                  AU_MOD_16,
                  AU_USERS,
                  AU_KIT_DIS,
                  AU_SQL_BD,
                  AU_PLATFORM
          do
          begin
               if ( SN_NUMERO <> xSN_NUMERO ) then
               begin
                    TT_TIPO = TT_TIPO + 1;
                    xSN_NUMERO = SN_NUMERO;
               end
               if ( VE_CODIGO <> xVE_CODIGO ) then
               begin
                    TT_TIPO = TT_TIPO + 2;
                    xVE_CODIGO = VE_CODIGO;
               end
               if ( AU_NUM_EMP <> xAU_NUM_EMP ) then
               begin
                    TT_TIPO = TT_TIPO + 4;
                    xAU_NUM_EMP = AU_NUM_EMP;
               end
               if ( AU_USERS <> xAU_USERS ) then
               begin
                    TT_TIPO = TT_TIPO + 8;
                    xAU_USERS = AU_USERS;
               end
               if ( AU_KIT_DIS <> xAU_KIT_DIS ) then
               begin
                    TT_TIPO = TT_TIPO + 16;
                    xAU_KIT_DIS = AU_KIT_DIS;
               end
               if ( AU_SQL_BD <> xAU_SQL_BD ) then
               begin
                    TT_TIPO = TT_TIPO + 32;
                    xAU_SQL_BD = AU_SQL_BD;
               end
               if ( AU_PLATFORM <> xAU_PLATFORM ) then
               begin
                    TT_TIPO = TT_TIPO + 64;
                    xAU_PLATFORM = AU_PLATFORM;
               end
               if ( AU_MOD_0 <> xAU_MOD_0 ) then
               begin
                    TT_TIPO = TT_TIPO + 128;
                    xAU_MOD_0 = AU_MOD_0;
               end
               if ( AU_MOD_1 <> xAU_MOD_1 ) then
               begin
                    TT_TIPO = TT_TIPO + 256;
                    xAU_MOD_1 = AU_MOD_1;
               end
               if ( AU_MOD_2 <> xAU_MOD_2 ) then
               begin
                    TT_TIPO = TT_TIPO + 512;
                    xAU_MOD_2 = AU_MOD_2;
               end
               if ( AU_MOD_3 <> xAU_MOD_3 ) then
               begin
                    TT_TIPO = TT_TIPO + 1024;
                    xAU_MOD_3 = AU_MOD_3;
               end
               if ( AU_MOD_4 <> xAU_MOD_4 ) then
               begin
                    TT_TIPO = TT_TIPO + 2048;
                    xAU_MOD_4 = AU_MOD_4;
               end
               if ( AU_MOD_5 <> xAU_MOD_5 ) then
               begin
                    TT_TIPO = TT_TIPO + 4096;
                    xAU_MOD_5 = AU_MOD_5;
               end
               if ( AU_MOD_6 <> xAU_MOD_6 ) then
               begin
                    TT_TIPO = TT_TIPO + 8192;
                    xAU_MOD_6 = AU_MOD_6;
               end
               if ( AU_MOD_7 <> xAU_MOD_7 ) then
               begin
                    TT_TIPO = TT_TIPO + 16384;
                    xAU_MOD_7 = AU_MOD_7;
               end
               if ( AU_MOD_8 <> xAU_MOD_8 ) then
               begin
                    TT_TIPO = TT_TIPO + 32768;
                    xAU_MOD_8 = AU_MOD_8;
               end
               if ( AU_MOD_9 <> xAU_MOD_9 ) then
               begin
                    TT_TIPO = TT_TIPO + 65536;
                    xAU_MOD_9 = AU_MOD_9;
               end
               if ( AU_MOD_10 <> xAU_MOD_10 ) then
               begin
                    TT_TIPO = TT_TIPO + 131072;
                    xAU_MOD_10 = AU_MOD_10;
               end
               if ( AU_MOD_11 <> xAU_MOD_11 ) then
               begin
                    TT_TIPO = TT_TIPO + 262144;
                    xAU_MOD_11 = AU_MOD_11;
               end
               if ( AU_MOD_12 <> xAU_MOD_12 ) then
               begin
                    TT_TIPO = TT_TIPO + 524288;
                    xAU_MOD_12 = AU_MOD_12;
               end
               if ( AU_MOD_13 <> xAU_MOD_13 ) then
               begin
                    TT_TIPO = TT_TIPO + 1048576;
                    xAU_MOD_13 = AU_MOD_13;
               end
               if ( AU_MOD_14 <> xAU_MOD_14 ) then
               begin
                    TT_TIPO = TT_TIPO + 2097152;
                    xAU_MOD_14 = AU_MOD_14;
               end
               if ( AU_MOD_15 <> xAU_MOD_15 ) then
               begin
                    TT_TIPO = TT_TIPO + 4194304;
                    xAU_MOD_15 = AU_MOD_15;
               end
               if ( AU_MOD_16 <> xAU_MOD_16 ) then
               begin
                    TT_TIPO = TT_TIPO + 8388608;
                    xAU_MOD_16 = AU_MOD_16;
               end
               if ( TT_TIPO > 0 ) then
               begin
                    Cambios = 1;
                    suspend;
               end
          end
          if ( ( Cambios > 0 ) and ( xAU_FOLIO > 0 ) ) then
          begin
               select TT_TIPO,
                      SI_FOLIO,
                      SN_NUMERO,
                      VE_CODIGO,
                      AU_FOLIO,
                      AU_FECHA,
                      AU_FEC_AUT,
                      AU_TIPOS,
                      AU_NUM_EMP,
                      AU_MOD_0,
                      AU_MOD_1,
                      AU_MOD_2,
                      AU_MOD_3,
                      AU_MOD_4,
                      AU_MOD_5,
                      AU_MOD_6,
                      AU_MOD_7,
                      AU_MOD_8,
                      AU_MOD_9,
                      AU_MOD_10,
                      AU_MOD_11,
                      AU_MOD_12,
                      AU_MOD_13,
                      AU_MOD_14,
                      AU_MOD_15,
                      AU_MOD_16,
                      AU_USERS,
                      AU_KIT_DIS,
                      AU_SQL_BD,
                      AU_PLATFORM
               from SP_INITIAL_STATE( :Folio, :Inicio )
               into TT_TIPO,                    SI_FOLIO,                    SN_NUMERO,
                    VE_CODIGO,
                    AU_FOLIO,
                    AU_FECHA,
                    AU_FEC_AUT,
                    AU_TIPOS,
                    AU_NUM_EMP,
                    AU_MOD_0,
                    AU_MOD_1,
                    AU_MOD_2,
                    AU_MOD_3,
                    AU_MOD_4,
                    AU_MOD_5,
                    AU_MOD_6,
                    AU_MOD_7,
                    AU_MOD_8,
                    AU_MOD_9,
                    AU_MOD_10,
                    AU_MOD_11,
                    AU_MOD_12,
                    AU_MOD_13,
                    AU_MOD_14,
                    AU_MOD_15,
                    AU_MOD_16,
                    AU_USERS,
                    AU_KIT_DIS,
                    AU_SQL_BD,
                    AU_PLATFORM;
               suspend;
          end
     end
end
 ^
SET TERM ; ^
COMMIT WORK ;
SET AUTODDL ON;
SET TERM ^ ;

/* Triggers only will work for SQL triggers */
CREATE TRIGGER TD_ESTADO FOR ESTADO                          
ACTIVE AFTER DELETE POSITION 0 
AS
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* DELETE trigger on ESTADO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
    /* ESTADO R/24 CIUDAD ON PARENT DELETE RESTRICT */
    select count(*)
      from CIUDAD
      where
        /*  %JoinFKPK(CIUDAD,OLD," = "," and") */
        CIUDAD.ET_CODIGO = OLD.ET_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
END
 ^
CREATE TRIGGER TU_ESTADO FOR ESTADO                          
ACTIVE AFTER UPDATE POSITION 0 
AS
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* UPDATE trigger on ESTADO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* ESTADO R/24 CIUDAD ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.ET_CODIGO <> NEW.ET_CODIGO) THEN
  BEGIN
    update CIUDAD
      set
        /*  %JoinFKPK(CIUDAD,NEW," = ",",") */
        CIUDAD.ET_CODIGO = NEW.ET_CODIGO
      where
        /*  %JoinFKPK(CIUDAD,OLD," = "," and") */
        CIUDAD.ET_CODIGO = OLD.ET_CODIGO;
  END


  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
END
 ^
CREATE TRIGGER TD_CIUDAD FOR CIUDAD                          
ACTIVE AFTER DELETE POSITION 0 
AS
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* DELETE trigger on CIUDAD */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
    /* CIUDAD Tiene DISTRIBU ON PARENT DELETE RESTRICT */
    select count(*)
      from DISTRIBU
      where
        /*  %JoinFKPK(DISTRIBU,OLD," = "," and") */
        DISTRIBU.CT_CODIGO = OLD.CT_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END

    /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
    /* CIUDAD Tiene SISTEMA ON PARENT DELETE RESTRICT */
    select count(*)
      from SISTEMA
      where
        /*  %JoinFKPK(SISTEMA,OLD," = "," and") */
        SISTEMA.CT_CODIGO = OLD.CT_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
END
 ^
CREATE TRIGGER TU_CIUDAD FOR CIUDAD                          
ACTIVE AFTER UPDATE POSITION 0 
AS
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* UPDATE trigger on CIUDAD */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* CIUDAD Tiene DISTRIBU ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.CT_CODIGO <> NEW.CT_CODIGO) THEN
  BEGIN
    update DISTRIBU
      set
        /*  %JoinFKPK(DISTRIBU,NEW," = ",",") */
        DISTRIBU.CT_CODIGO = NEW.CT_CODIGO
      where
        /*  %JoinFKPK(DISTRIBU,OLD," = "," and") */
        DISTRIBU.CT_CODIGO = OLD.CT_CODIGO;
  END

  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* CIUDAD Tiene SISTEMA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.CT_CODIGO <> NEW.CT_CODIGO) THEN
  BEGIN
    update SISTEMA
      set
        /*  %JoinFKPK(SISTEMA,NEW," = ",",") */
        SISTEMA.CT_CODIGO = NEW.CT_CODIGO
      where
        /*  %JoinFKPK(SISTEMA,OLD," = "," and") */
        SISTEMA.CT_CODIGO = OLD.CT_CODIGO;
  END


  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
END
 ^
CREATE TRIGGER TD_GRUPO FOR GRUPO                           
ACTIVE AFTER DELETE POSITION 0 
AS
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* DELETE trigger on GRUPO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
    /* GRUPO Le pertenecen SISTEMA ON PARENT DELETE RESTRICT */
    select count(*)
      from SISTEMA
      where
        /*  %JoinFKPK(SISTEMA,OLD," = "," and") */
        SISTEMA.GR_CODIGO = OLD.GR_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
END
 ^
CREATE TRIGGER TU_GRUPO FOR GRUPO                           
ACTIVE AFTER UPDATE POSITION 0 
AS
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* UPDATE trigger on GRUPO */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
  /* GRUPO Le pertenecen SISTEMA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.GR_CODIGO <> NEW.GR_CODIGO) THEN
  BEGIN
    update SISTEMA
      set
        /*  %JoinFKPK(SISTEMA,NEW," = ",",") */
        SISTEMA.GR_CODIGO = NEW.GR_CODIGO
      where
        /*  %JoinFKPK(SISTEMA,OLD," = "," and") */
        SISTEMA.GR_CODIGO = OLD.GR_CODIGO;
  END


  /* ERwin Builtin Thu Mar 11 17:06:59 1999 */
END
 ^
CREATE TRIGGER TD_VERSION FOR VERSION                         
ACTIVE AFTER DELETE POSITION 0 
AS
  /* ERwin Builtin Thu Mar 11 17:07:00 1999 */
  /* DELETE trigger on VERSION */
DECLARE VARIABLE numrows INTEGER;
BEGIN
    /* ERwin Builtin Thu Mar 11 17:07:00 1999 */
    /* VERSION Registrada AUTORIZA ON PARENT DELETE RESTRICT */
    select count(*)
      from AUTORIZA
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.VE_CODIGO = OLD.VE_CODIGO into numrows;
    IF (numrows > 0) THEN
    BEGIN
      EXCEPTION ERWIN_PARENT_DELETE_RESTRICT;
    END


  /* ERwin Builtin Thu Mar 11 17:07:00 1999 */
END
 ^
CREATE TRIGGER TU_VERSION FOR VERSION                         
ACTIVE AFTER UPDATE POSITION 0 
AS
  /* ERwin Builtin Thu Mar 11 17:07:00 1999 */
  /* UPDATE trigger on VERSION */
DECLARE VARIABLE numrows INTEGER;
BEGIN
  /* ERwin Builtin Thu Mar 11 17:07:00 1999 */
  /* VERSION Registrada AUTORIZA ON PARENT UPDATE CASCADE */
  IF
    /* %JoinPKPK(OLD,NEW," <> "," or ") */
    (OLD.VE_CODIGO <> NEW.VE_CODIGO) THEN
  BEGIN
    update AUTORIZA
      set
        /*  %JoinFKPK(AUTORIZA,NEW," = ",",") */
        AUTORIZA.VE_CODIGO = NEW.VE_CODIGO
      where
        /*  %JoinFKPK(AUTORIZA,OLD," = "," and") */
        AUTORIZA.VE_CODIGO = OLD.VE_CODIGO;
  END


  /* ERwin Builtin Thu Mar 11 17:07:00 1999 */
END
 ^
CREATE TRIGGER TD_SISTEMA FOR SISTEMA                         
ACTIVE AFTER DELETE POSITION 0 
AS
BEGIN
    delete from AUTORIZA where ( AUTORIZA.SI_FOLIO = OLD.SI_FOLIO );
    delete from INSTALACION where ( INSTALACION.SI_FOLIO = OLD.SI_FOLIO );
END
 ^
CREATE TRIGGER TU_SISTEMA FOR SISTEMA                         
ACTIVE AFTER UPDATE POSITION 0 
AS
BEGIN
  IF (OLD.SI_FOLIO <> NEW.SI_FOLIO) THEN
  BEGIN
       update AUTORIZA set AUTORIZA.SI_FOLIO = NEW.SI_FOLIO
       where ( AUTORIZA.SI_FOLIO = OLD.SI_FOLIO );
       update INSTALACION set INSTALACION.SI_FOLIO = NEW.SI_FOLIO
       where ( INSTALACION.SI_FOLIO = OLD.SI_FOLIO );
  END
END
 ^
CREATE TRIGGER TD_SENTINEL FOR SENTINEL                        
ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable NUMROWS INTEGER;
BEGIN
     select COUNT(*) from AUTORIZA where
     ( AUTORIZA.SN_NUMERO = OLD.SN_NUMERO ) into :NUMROWS;
     IF ( NUMROWS > 0 ) THEN
     BEGIN
          EXCEPTION SENTINEL_ASIGNADO_A_SISTEMA;
     END
     delete from INSTALACION where ( INSTALACION.SN_NUMERO = OLD.SN_NUMERO );
END
 ^
CREATE TRIGGER TU_SENTINEL FOR SENTINEL                        
ACTIVE AFTER UPDATE POSITION 0 
AS
BEGIN
     IF (OLD.SN_NUMERO <> NEW.SN_NUMERO) THEN
     BEGIN
          update AUTORIZA set AUTORIZA.SN_NUMERO = NEW.SN_NUMERO
          where ( AUTORIZA.SN_NUMERO = OLD.SN_NUMERO );
          update INSTALACION set INSTALACION.SN_NUMERO = NEW.SN_NUMERO
          where ( INSTALACION.SN_NUMERO = OLD.SN_NUMERO );
     END
END
 ^
CREATE TRIGGER TD_USUARIO FOR USUARIO                         
ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable NUMROWS INTEGER;
BEGIN
     select COUNT(*) from AUTORIZA
     where ( AUTORIZA.US_CODIGO = OLD.US_CODIGO )into :NUMROWS;
     IF ( NUMROWS > 0 ) THEN
     BEGIN
          EXCEPTION USUARIO_AUTORIZACIONES;
     END
     select COUNT(*) from INSTALACION
     where ( INSTALACION.US_CODIGO = OLD.US_CODIGO )into :NUMROWS;
     IF ( NUMROWS > 0 ) THEN
     BEGIN
          EXCEPTION USUARIO_CLAVES;
     END
END
 ^
CREATE TRIGGER TU_USUARIO FOR USUARIO                         
ACTIVE AFTER UPDATE POSITION 0 
AS
BEGIN
  IF (OLD.US_CODIGO <> NEW.US_CODIGO) THEN
  BEGIN
       update AUTORIZA set AUTORIZA.US_CODIGO = NEW.US_CODIGO
       where ( AUTORIZA.US_CODIGO = OLD.US_CODIGO );
       update INSTALACION set INSTALACION.US_CODIGO = NEW.US_CODIGO
       where ( INSTALACION.US_CODIGO = OLD.US_CODIGO );
  END
END
 ^
CREATE TRIGGER TD_DISTRIBU FOR DISTRIBU                        
ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable NUMROWS INTEGER;
BEGIN
     select COUNT(*) from USUARIO
     where ( USUARIO.DI_CODIGO = OLD.DI_CODIGO ) into :NUMROWS;
     IF (NUMROWS > 0) THEN
     BEGIN
          EXCEPTION DISTRIB_USUARIOS;
     END
     select COUNT(*) from AUTORIZA
     where ( AUTORIZA.DI_CODIGO = OLD.DI_CODIGO ) into :NUMROWS;
     IF (NUMROWS > 0) THEN
     BEGIN
          EXCEPTION DISTRIB_AUTORIZACIONES;
     END
     select COUNT(*) from INSTALACION
     where ( INSTALACION.DI_CODIGO = OLD.DI_CODIGO ) into :NUMROWS;
     IF (NUMROWS > 0) THEN
     BEGIN
          EXCEPTION DISTRIB_CLAVES;
     END
END
 ^
CREATE TRIGGER TU_DISTRIBU FOR DISTRIBU                        
ACTIVE AFTER UPDATE POSITION 0 
AS
BEGIN
     IF (OLD.DI_CODIGO <> NEW.DI_CODIGO) THEN
     BEGIN
          update USUARIO set USUARIO.DI_CODIGO = NEW.DI_CODIGO
          where ( USUARIO.DI_CODIGO = OLD.DI_CODIGO );
          update AUTORIZA set AUTORIZA.DI_CODIGO = NEW.DI_CODIGO
          where ( AUTORIZA.DI_CODIGO = OLD.DI_CODIGO );
          update INSTALACION set INSTALACION.DI_CODIGO = NEW.DI_CODIGO
          where ( INSTALACION.DI_CODIGO = OLD.DI_CODIGO );
     END
END
 ^
COMMIT WORK ^
SET TERM ; ^

/* Grant permissions for this database */

