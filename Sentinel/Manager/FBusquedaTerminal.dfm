object BusquedaTeminales: TBusquedaTeminales
  Left = 176
  Top = 252
  Width = 1036
  Height = 440
  Caption = 'B'#250'squeda de Terminales'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 357
    Width = 1020
    Height = 45
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      1020
      45)
    object BitBtn1: TBitBtn
      Left = 931
      Top = 11
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Salir'
      ModalResult = 1
      TabOrder = 0
      OnClick = BitBtn1Click
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object ZetaDBGrid1: TZetaDBGrid
    Left = 0
    Top = 49
    Width = 1020
    Height = 308
    Align = alClient
    DataSource = dsTerminales
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit, dgMultiSelect]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TE_TIPO'
        Title.Caption = 'Tipo'
        Width = 84
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_NSERIE'
        Title.Caption = 'Serie/MacAddress'
        Width = 127
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_DESCRIP'
        Title.Caption = 'Nombre'
        Width = 177
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SI_RAZ_SOC'
        Title.Caption = 'Razon Social'
        Width = 319
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_TEXTO'
        Title.Caption = 'Comentario'
        Width = 187
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SN_NUMERO'
        Title.Caption = 'Sentinel'
        Width = 84
        Visible = True
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1020
    Height = 49
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 32
      Top = 16
      Width = 26
      Height = 13
      Caption = 'Pista:'
    end
    object txtPista: TZetaEdit
      Left = 61
      Top = 13
      Width = 264
      Height = 21
      Hint = 
        'Pista sobre: MacAddress,Razon Social,Comentario, Sensible a May'#250 +
        'sculas'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = txtPistaChange
    end
    object btnBuscar: TBitBtn
      Left = 336
      Top = 8
      Width = 81
      Height = 25
      Caption = 'Buscar'
      Default = True
      TabOrder = 1
      OnClick = btnBuscarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      Layout = blGlyphRight
      NumGlyphs = 2
    end
  end
  object dsTerminales: TDataSource
    Left = 520
    Top = 8
  end
end
