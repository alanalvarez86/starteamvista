unit FAcceso;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
     Buttons, ExtCtrls, Dialogs;

type
  TAcceso = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    OKBtn: TBitBtn;
    CancelBtn: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
    function GetUsuario: String;
    function GetClave: String;
  public
    { Public declarations }
    property Usuario: String read GetUsuario;
    property Clave: String read GetClave;
  end;

var
  Acceso: TAcceso;

function Login: Boolean;

implementation

uses ZetaDialogo,
     DSentinel;

{$R *.DFM}

function Login: Boolean;
begin
     with TAcceso.Create( Application ) do
     begin
          try
             Result := ( ShowModal = mrOk );
          finally
                 Free;
          end;
     end;
end;

{ *********** TAcceso ************* }

procedure TAcceso.FormShow(Sender: TObject);
begin
     Edit1.SetFocus;
end;

function TAcceso.GetUsuario: String;
begin
     Result := Edit1.Text;
end;

function TAcceso.GetClave: String;
begin
     Result := Edit2.Text;
end;

procedure TAcceso.OKBtnClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     if ( Usuario = '' ) then
     begin
          Beep;
          Edit1.SetFocus;
     end
     else
         if ( Clave = '' ) then
         begin
              Beep;
              Edit2.SetFocus;
         end
         else
         begin
              oCursor := Screen.Cursor;
              Screen.Cursor := crHourglass;
              try
                 if dmSentinel.ValidaIngreso( Usuario, Clave ) then
                    ModalResult := mrOk
                 else
                     ZetaDialogo.ZError( '� Error !', 'Usuario o Clave Inv�lida' + CR_LF + 'La Clave Es Sensible A May�sculas', 0 );
              finally
                     Screen.Cursor := oCursor;
              end;
         end;
end;

end.
