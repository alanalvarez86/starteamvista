unit DSentinel;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DSentinel.pas                              ::
  :: Descripci�n: Programa principal de Sentinel3s.exe       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, SvcMgr, Dialogs;

type
  TSentinel3Service = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceExecute(Sender: TService);
  private
    { Private declarations }
    FFecha: TDate;
    FIniciando: Integer;
    function Inicializa: Boolean;
    procedure EscribeBitacora(const sMensaje: String);
  public
    { Public declarations }
    function GetServiceController: TServiceController; override;
  end;

var
  Sentinel3Service: TSentinel3Service;

implementation

{$R *.DFM}

uses DSentinelWrite,
     ZetaWinAPITools;

procedure ServiceController( CtrlCode: DWord ); stdcall;
begin
     Sentinel3Service.Controller( CtrlCode );
end;

function TSentinel3Service.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;

{ ******* TSentinel ******** }

procedure TSentinel3Service.ServiceCreate(Sender: TObject);
begin
     FFecha := 0;
     FIniciando := 10;
     if ZetaWinAPITools.GetInterbaseIsInstalled then
     begin
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := 'InterBaseGuardian';
          end;
     end
     else
         if ZetaWinAPITools.GetFirebirdIsInstalled then
         begin
              with TDependency.Create( Dependencies ) do
              begin
                   IsGroup := False;
                   Name := 'FirebirdGuardianDefaultInstance';
              end;
         end;
end;

procedure TSentinel3Service.ServiceExecute(Sender: TService);
const
     K_UN_SEGUNDO = 1000;
begin
     while not Terminated do
     begin
          if ( FFecha <> Date ) then
          begin
               if not Inicializa and ( FIniciando > 0 ) then
               begin
                    Sleep( 28 * K_UN_SEGUNDO );
                    FIniciando := FIniciando - 1;
               end
               else
                   FFecha := Date;
          end;
          Sleep( 2 * K_UN_SEGUNDO );
          ServiceThread.ProcessRequests( False );
     end;
end;

procedure TSentinel3Service.EscribeBitacora(const sMensaje: String);
begin
     try
        LogMessage( sMensaje, EVENTLOG_INFORMATION_TYPE, 0, 0 );
     except
           LogMessage( 'Error al Escribir a Bit�cora', EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

function TSentinel3Service.Inicializa: Boolean;
begin
     Result := DSentinelWrite.RenovarAutorizacion( EscribeBitacora );
end;

end.
