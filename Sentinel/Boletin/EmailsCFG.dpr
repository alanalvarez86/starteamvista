program EmailsCFG;

uses
  Forms,
  DEmails in 'DEmails.pas' {EmailCFG},
  FEditRecord in 'FEditRecord.pas' {EditRecord};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Emails Configuration';
  Application.CreateForm(TEmailCFG, EmailCFG);
  Application.Run;
end.
