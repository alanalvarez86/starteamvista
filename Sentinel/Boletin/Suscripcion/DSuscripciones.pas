unit DSuscripciones;

interface

uses SysUtils, Classes, Db, ODSI, OCL,
     {$IFDEF Linux}
     QForms
     {$ELSE}
     Forms
     {$ENDIF};

type
  TdmSuscripciones = class(TDataModule)
    Hdbc: THdbc;
    OEAgregar: TOEDataSet;
    OECambiar: TOEDataSet;
    OEBorrar: TOEDataSet;
    OEExiste: TOEDataSet;
  private
    { Private declarations }
    FErrorMsg: String;
    function OpenDatabase: Boolean;
    procedure ClearErrorMsg;
    procedure CommitTransaccion;
    procedure EmpiezaTransaccion;
    procedure RollBackTransaccion;
  public
    { Public declarations }
    property ErrorMsg: String read FErrorMsg;
    function SuscripcionAgregar( const sDireccion, sNombre, sEmpresa: String ): Boolean;
    function SuscripcionBorrar( const sDireccion: String ): Boolean;
  end;

function dmSuscripciones: TdmSuscripciones;

implementation

{$R *.DFM}

uses IWInit,
     ServerController;

// Since we are threaded we cannot use global variables to store form / datamodule references
// so we store them in WebApplication.Data and we could reference that each time, but by creating
// a function like this our other code looks "normal" almost as if its referencing a global.
// This function is not necessary but it makes the code in the main form which references this
// datamodule a lot neater.
// Without this function ever time we would reference this datamodule we would use:
//   TDataModule1(WebApplication.Data).Datamodule.<method / component>
// By creating this procedure it becomes:
//   TDataModule1.<method / component>
// Which is just like normal Delphi code.
function dmSuscripciones: TdmSuscripciones;
begin
  Result := TUserSession( RWebApplication.Data ).DSuscripciones;
end;

{ TdmSuscripciones }

procedure TdmSuscripciones.ClearErrorMsg;
begin
     FErrorMsg := '';
end;

function TdmSuscripciones.OpenDatabase: Boolean;
begin
     Result := False;
     try
        with HDBC do
        begin
             Connected := True;
             Result := Connected;
        end;
     except
           on Error: Exception do
           begin
                FErrorMsg := Error.Message;
           end;
     end;
end;

procedure TdmSuscripciones.EmpiezaTransaccion;
begin
     HDBC.StartTransact;
end;

procedure TdmSuscripciones.CommitTransaccion;
begin
     HDBC.Commit;
     HDBC.EndTransact;
end;

procedure TdmSuscripciones.RollBackTransaccion;
begin
     HDBC.Rollback;
     HDBC.EndTransact;
end;

function TdmSuscripciones.SuscripcionAgregar(const sDireccion, sNombre, sEmpresa: String): Boolean;
const
     K_ALTA = 1;
var
   lExiste: Boolean;
begin
     Result := False;
     ClearErrorMsg;
     if OpenDatabase then
     begin
          try
             with OEExiste do
             begin
                  Active := False;
                  ParamByName( 'BL_EMAIL' ).AsString := sDireccion;
                  Active := True;
                  lExiste := ( FieldByName( 'CUANTOS' ).AsInteger > 0 );
                  Active := False;
             end;
             EmpiezaTransaccion;
             try
                if lExiste then
                begin
                     with OECambiar do
                     begin
                          ParamByName( 'BL_EMAIL' ).AsString := Copy( sDireccion, 1, 50 );
                          ParamByName( 'BL_NOMBRE' ).AsString := Copy( sNombre, 1, 50 );
                          ParamByName( 'BL_EMPRESA' ).AsString := Copy( sEmpresa, 1, 80 );
                          ParamByName( 'BL_ACTIVO' ).AsInteger := K_ALTA;
                          ParamByName( 'BL_ALTA' ).AsDateTime := Trunc( Now );
                          ExecSQL;
                     end;
                end
                else
                begin
                     with OEAgregar do
                     begin
                          ParamByName( 'BL_EMAIL' ).AsString := Copy( sDireccion, 1, 50 );
                          ParamByName( 'BL_NOMBRE' ).AsString := Copy( sNombre, 1, 50 );
                          ParamByName( 'BL_EMPRESA' ).AsString := Copy( sEmpresa, 1, 80 );
                          ParamByName( 'BL_ACTIVO' ).AsInteger := K_ALTA;
                          ParamByName( 'BL_ALTA' ).AsDateTime := Trunc( Now );
                          ExecSQL;
                     end;
                end;
                CommitTransaccion;
                Result := True;
             except
                   on Error: Exception do
                   begin
                        FErrorMsg := Error.Message;
                        RollBackTransaccion;
                   end;
             end;
          except
                on Error: Exception do
                begin
                     FErrorMsg := Error.Message;
                end;
          end;
     end;
end;

function TdmSuscripciones.SuscripcionBorrar( const sDireccion: String): Boolean;
begin
     Result := False;
     ClearErrorMsg;
     if OpenDatabase then
     begin
          EmpiezaTransaccion;
          try
             with OEBorrar do
             begin
                  ParamByName( 'BL_EMAIL' ).AsString := sDireccion;
                  ExecSQL;
             end;
             CommitTransaccion;
             Result := True;
          except
                on Error: Exception do
                begin
                     FErrorMsg := Error.Message;
                     RollBackTransaccion;
                end;
          end;
     end;
end;

end.
