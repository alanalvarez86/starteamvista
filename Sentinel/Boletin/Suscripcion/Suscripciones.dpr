library Suscripciones;

uses
  IWInitISAPI,
  ServerController in 'ServerController.pas' {IWServerController: TIWServerControllerBase},
  IWGetDatos in 'IWGetDatos.pas' {GetDireccion: TIWAppForm},
  IWAddDatos in 'IWAddDatos.pas' {AddSuscripcion: TIWAppForm},
  IWAddExito in 'IWAddExito.pas' {AddExito: TIWAppForm},
  DSuscripciones in 'DSuscripciones.pas' {dmSuscripciones: TDataModule};

{$R *.RES}

begin
  IWRun(TGetDireccion, TIWServerController);
end.
