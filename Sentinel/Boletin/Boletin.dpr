library Boletin;

uses
  ComServ,
  Boletin_TLB in 'Boletin_TLB.pas',
  DServerBoletin in 'DServerBoletin.pas' {dmServerBoletin: TMtsDataModule} {dmServerBoletin: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
