unit Boletin_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 14/06/2005 4:24:07 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3Win_20\Sentinel\Boletin\Boletin.tlb (1)
// LIBID: {66142703-6EB5-47F3-926F-2E8944FCD722}
// LCID: 0
// Helpfile: 
// HelpString: Tress Boletin
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\System32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$ifndef VER130}{$WARN SYMBOL_PLATFORM OFF}{$endif}
{$WRITEABLECONST ON}
{$ifndef VER130}{$VARPROPSETTER ON}{$endif}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL{$ifndef VER130}, Variants{$endif};
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  BoletinMajorVersion = 1;
  BoletinMinorVersion = 0;

  LIBID_Boletin: TGUID = '{66142703-6EB5-47F3-926F-2E8944FCD722}';

  IID_IdmServerBoletin: TGUID = '{CE60968F-FBEC-4790-B2E9-C8EDFA6BBE4C}';
  CLASS_dmServerBoletin: TGUID = '{43CAAAA3-45C2-4C58-9D44-108CB67BC6DF}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerBoletin = interface;
  IdmServerBoletinDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerBoletin = IdmServerBoletin;


// *********************************************************************//
// Interface: IdmServerBoletin
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CE60968F-FBEC-4790-B2E9-C8EDFA6BBE4C}
// *********************************************************************//
  IdmServerBoletin = interface(IAppServer)
    ['{CE60968F-FBEC-4790-B2E9-C8EDFA6BBE4C}']
    function GrabaSuscripcion(const Parametros: WideString): WideString; safecall;
    function BorraSuscripcion(const Parametros: WideString): WideString; safecall;
    function SentinelLogin(const Parametros: WideString): WideString; safecall;
    function GetSentinelasDistrib(const Parametros: WideString): WideString; safecall;
    function GetSentinelasCliente(const Parametros: WideString): WideString; safecall;
    function GetSentinelaFolio(const Parametros: WideString): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerBoletinDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {CE60968F-FBEC-4790-B2E9-C8EDFA6BBE4C}
// *********************************************************************//
  IdmServerBoletinDisp = dispinterface
    ['{CE60968F-FBEC-4790-B2E9-C8EDFA6BBE4C}']
    function GrabaSuscripcion(const Parametros: WideString): WideString; dispid 1;
    function BorraSuscripcion(const Parametros: WideString): WideString; dispid 3;
    function SentinelLogin(const Parametros: WideString): WideString; dispid 2;
    function GetSentinelasDistrib(const Parametros: WideString): WideString; dispid 4;
    function GetSentinelasCliente(const Parametros: WideString): WideString; dispid 5;
    function GetSentinelaFolio(const Parametros: WideString): WideString; dispid 7;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerBoletin provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerBoletin exposed by              
// the CoClass dmServerBoletin. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerBoletin = class
    class function Create: IdmServerBoletin;
    class function CreateRemote(const MachineName: string): IdmServerBoletin;
  end;

implementation

uses ComObj;

class function CodmServerBoletin.Create: IdmServerBoletin;
begin
  Result := CreateComObject(CLASS_dmServerBoletin) as IdmServerBoletin;
end;

class function CodmServerBoletin.CreateRemote(const MachineName: string): IdmServerBoletin;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerBoletin) as IdmServerBoletin;
end;

end.
