unit CHILKATXMLLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 2$
// File generated on 06/09/2004 3:28:01 PM from Type Library described below.

// ************************************************************************ //
// Type Lib: D:\3Win_20\Sentinel\Boletin\Chilkat\chilkatXML.dll (1)
// IID\LCID: {101F9C56-A0F3-455C-ABBB-191168ABCF94}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\System32\STDOLE2.TLB)
//   (2) v4.0 StdVCL, (C:\WINDOWS\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  CHILKATXMLLibMajorVersion = 1;
  CHILKATXMLLibMinorVersion = 0;

  LIBID_CHILKATXMLLib: TGUID = '{101F9C56-A0F3-455C-ABBB-191168ABCF94}';

  IID_IChilkatXml: TGUID = '{2A401274-F2E0-462F-8DFC-50F8D8675718}';
  DIID_IChilkatOutgoing: TGUID = '{DE5F017B-A654-4440-A1C5-8B5483C01D71}';
  IID_IChilkatLog: TGUID = '{55396537-E4AB-46D2-A373-2B894EBD439C}';
  IID_IChilkatLog2: TGUID = '{4E05166B-762A-4FBC-85F1-0500C95501B2}';
  CLASS_ChilkatXml: TGUID = '{CE2E4226-494A-4DB2-9B45-7C8586CC01A3}';
  IID_IXmlFactory: TGUID = '{624CD0C7-ACD8-4E28-8393-2137F8D76DCE}';
  CLASS_XmlFactory: TGUID = '{7FAB24D9-F81A-49A3-A0E9-A3198DEDF454}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IChilkatXml = interface;
  IChilkatXmlDisp = dispinterface;
  IChilkatOutgoing = dispinterface;
  IChilkatLog = interface;
  IChilkatLogDisp = dispinterface;
  IChilkatLog2 = interface;
  IChilkatLog2Disp = dispinterface;
  IXmlFactory = interface;
  IXmlFactoryDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ChilkatXml = IChilkatXml;
  XmlFactory = IXmlFactory;


// *********************************************************************//
// Interface: IChilkatXml
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2A401274-F2E0-462F-8DFC-50F8D8675718}
// *********************************************************************//
  IChilkatXml = interface(IDispatch)
    ['{2A401274-F2E0-462F-8DFC-50F8D8675718}']
    function  Get_Version: WideString; safecall;
    function  GetLog: IChilkatLog2; safecall;
    procedure UnlockComponent(const unlockCode: WideString); safecall;
    function  Get_NumAttributes: Integer; safecall;
    function  Get_Tag: WideString; safecall;
    procedure Set_Tag(const pVal: WideString); safecall;
    function  Get_Content: WideString; safecall;
    procedure Set_Content(const pVal: WideString); safecall;
    function  Get_NumChildren: Integer; safecall;
    function  Get_ValidatingParser: Integer; safecall;
    procedure Set_ValidatingParser(pVal: Integer); safecall;
    function  Get_Cdata: Integer; safecall;
    procedure Set_Cdata(pVal: Integer); safecall;
    function  LoadXml(const xmlData: WideString): Integer; safecall;
    function  LoadXmlFile(const fileName: WideString): Integer; safecall;
    procedure SaveXML(const fileName: WideString); safecall;
    function  GetXML: WideString; safecall;
    procedure AppendToContent(const str: WideString); safecall;
    procedure AddAttribute(const name: WideString; const value: WideString); safecall;
    procedure RemoveAttribute(const name: WideString); safecall;
    procedure RemoveAllAttributes; safecall;
    function  GetAttributeName(index: Integer): WideString; safecall;
    function  GetAttributeValue(index: Integer): WideString; safecall;
    function  GetAttrValue(const name: WideString): WideString; safecall;
    function  HasAttrWithValue(const name: WideString; const value: WideString): Integer; safecall;
    procedure SwapNode(const node: IChilkatXml); safecall;
    procedure SwapTree(const tree: IChilkatXml); safecall;
    procedure AddChildTree(const tree: IChilkatXml); safecall;
    function  GetChild(index: Integer): IChilkatXml; safecall;
    function  ExtractChildByIndex(index: Integer): IChilkatXml; safecall;
    function  ExtractChildByName(const Tag: WideString; const attrName: WideString; 
                                 const attrValue: WideString): IChilkatXml; safecall;
    function  NumChildrenHavingTag(const Tag: WideString): Integer; safecall;
    function  GetNthChildWithTag(const Tag: WideString; n: Integer): IChilkatXml; safecall;
    function  NewChild(const Tag: WideString; const Content: WideString): IChilkatXml; safecall;
    function  FindOrAddNewChild(const Tag: WideString): IChilkatXml; safecall;
    function  SearchForTag(const after: IChilkatXml; const Tag: WideString): IChilkatXml; safecall;
    function  SearchForContent(const after: IChilkatXml; const Tag: WideString; 
                               const contentPattern: WideString): IChilkatXml; safecall;
    function  SearchAllForContent(const after: IChilkatXml; const contentPattern: WideString): IChilkatXml; safecall;
    function  SearchForAttribute(const after: IChilkatXml; const Tag: WideString; 
                                 const attr: WideString; const valuePattern: WideString): IChilkatXml; safecall;
    function  GetParent: IChilkatXml; safecall;
    function  Get_AutoFix: Integer; safecall;
    procedure Set_AutoFix(pVal: Integer); safecall;
    procedure Clear; safecall;
    function  FirstChild: IChilkatXml; safecall;
    function  LastChild: IChilkatXml; safecall;
    function  NextSibling: IChilkatXml; safecall;
    function  PreviousSibling: IChilkatXml; safecall;
    function  Get_TreeId: Integer; safecall;
    function  GetChildWithTag(const Tag: WideString): IChilkatXml; safecall;
    function  Get_Standalone: WideString; safecall;
    procedure Set_Standalone(const pVal: WideString); safecall;
    function  Get_Encoding: WideString; safecall;
    procedure Set_Encoding(const pVal: WideString); safecall;
    function  GetRoot: IChilkatXml; safecall;
    procedure SaveXmlLog(const fileName: WideString); safecall;
    function  ConvertToEncoding(const Encoding: WideString): Integer; safecall;
    function  HttpPost(const url: WideString): IChilkatXml; safecall;
    procedure CompressSubtree; safecall;
    procedure CompressNode; safecall;
    procedure DecompressSubtree; safecall;
    procedure DecompressNode; safecall;
    procedure EncryptNode(const password: WideString); safecall;
    procedure DecryptNode(const password: WideString); safecall;
    function  Get_ZipUnlockCode: WideString; safecall;
    procedure Set_ZipUnlockCode(const pVal: WideString); safecall;
    function  Get_CryptUnlockCode: WideString; safecall;
    procedure Set_CryptUnlockCode(const pVal: WideString); safecall;
    property Version: WideString read Get_Version;
    property NumAttributes: Integer read Get_NumAttributes;
    property Tag: WideString read Get_Tag write Set_Tag;
    property Content: WideString read Get_Content write Set_Content;
    property NumChildren: Integer read Get_NumChildren;
    property ValidatingParser: Integer read Get_ValidatingParser write Set_ValidatingParser;
    property Cdata: Integer read Get_Cdata write Set_Cdata;
    property AutoFix: Integer read Get_AutoFix write Set_AutoFix;
    property TreeId: Integer read Get_TreeId;
    property Standalone: WideString read Get_Standalone write Set_Standalone;
    property Encoding: WideString read Get_Encoding write Set_Encoding;
    property ZipUnlockCode: WideString read Get_ZipUnlockCode write Set_ZipUnlockCode;
    property CryptUnlockCode: WideString read Get_CryptUnlockCode write Set_CryptUnlockCode;
  end;

// *********************************************************************//
// DispIntf:  IChilkatXmlDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2A401274-F2E0-462F-8DFC-50F8D8675718}
// *********************************************************************//
  IChilkatXmlDisp = dispinterface
    ['{2A401274-F2E0-462F-8DFC-50F8D8675718}']
    property Version: WideString readonly dispid 1;
    function  GetLog: IChilkatLog2; dispid 2;
    procedure UnlockComponent(const unlockCode: WideString); dispid 3;
    property NumAttributes: Integer readonly dispid 4;
    property Tag: WideString dispid 5;
    property Content: WideString dispid 6;
    property NumChildren: Integer readonly dispid 7;
    property ValidatingParser: Integer dispid 8;
    property Cdata: Integer dispid 9;
    function  LoadXml(const xmlData: WideString): Integer; dispid 10;
    function  LoadXmlFile(const fileName: WideString): Integer; dispid 11;
    procedure SaveXML(const fileName: WideString); dispid 12;
    function  GetXML: WideString; dispid 13;
    procedure AppendToContent(const str: WideString); dispid 14;
    procedure AddAttribute(const name: WideString; const value: WideString); dispid 15;
    procedure RemoveAttribute(const name: WideString); dispid 16;
    procedure RemoveAllAttributes; dispid 17;
    function  GetAttributeName(index: Integer): WideString; dispid 18;
    function  GetAttributeValue(index: Integer): WideString; dispid 19;
    function  GetAttrValue(const name: WideString): WideString; dispid 20;
    function  HasAttrWithValue(const name: WideString; const value: WideString): Integer; dispid 21;
    procedure SwapNode(const node: IChilkatXml); dispid 22;
    procedure SwapTree(const tree: IChilkatXml); dispid 23;
    procedure AddChildTree(const tree: IChilkatXml); dispid 24;
    function  GetChild(index: Integer): IChilkatXml; dispid 25;
    function  ExtractChildByIndex(index: Integer): IChilkatXml; dispid 26;
    function  ExtractChildByName(const Tag: WideString; const attrName: WideString; 
                                 const attrValue: WideString): IChilkatXml; dispid 27;
    function  NumChildrenHavingTag(const Tag: WideString): Integer; dispid 28;
    function  GetNthChildWithTag(const Tag: WideString; n: Integer): IChilkatXml; dispid 29;
    function  NewChild(const Tag: WideString; const Content: WideString): IChilkatXml; dispid 30;
    function  FindOrAddNewChild(const Tag: WideString): IChilkatXml; dispid 31;
    function  SearchForTag(const after: IChilkatXml; const Tag: WideString): IChilkatXml; dispid 32;
    function  SearchForContent(const after: IChilkatXml; const Tag: WideString; 
                               const contentPattern: WideString): IChilkatXml; dispid 33;
    function  SearchAllForContent(const after: IChilkatXml; const contentPattern: WideString): IChilkatXml; dispid 34;
    function  SearchForAttribute(const after: IChilkatXml; const Tag: WideString; 
                                 const attr: WideString; const valuePattern: WideString): IChilkatXml; dispid 35;
    function  GetParent: IChilkatXml; dispid 36;
    property AutoFix: Integer dispid 37;
    procedure Clear; dispid 38;
    function  FirstChild: IChilkatXml; dispid 39;
    function  LastChild: IChilkatXml; dispid 40;
    function  NextSibling: IChilkatXml; dispid 41;
    function  PreviousSibling: IChilkatXml; dispid 42;
    property TreeId: Integer readonly dispid 43;
    function  GetChildWithTag(const Tag: WideString): IChilkatXml; dispid 44;
    property Standalone: WideString dispid 45;
    property Encoding: WideString dispid 46;
    function  GetRoot: IChilkatXml; dispid 47;
    procedure SaveXmlLog(const fileName: WideString); dispid 48;
    function  ConvertToEncoding(const Encoding: WideString): Integer; dispid 49;
    function  HttpPost(const url: WideString): IChilkatXml; dispid 50;
    procedure CompressSubtree; dispid 51;
    procedure CompressNode; dispid 52;
    procedure DecompressSubtree; dispid 53;
    procedure DecompressNode; dispid 54;
    procedure EncryptNode(const password: WideString); dispid 55;
    procedure DecryptNode(const password: WideString); dispid 56;
    property ZipUnlockCode: WideString dispid 57;
    property CryptUnlockCode: WideString dispid 58;
  end;

// *********************************************************************//
// DispIntf:  IChilkatOutgoing
// Flags:     (4096) Dispatchable
// GUID:      {DE5F017B-A654-4440-A1C5-8B5483C01D71}
// *********************************************************************//
  IChilkatOutgoing = dispinterface
    ['{DE5F017B-A654-4440-A1C5-8B5483C01D71}']
    procedure MessageProc(senderID: Integer; messageID: Integer; param1: Integer; param2: Integer); dispid 1;
  end;

// *********************************************************************//
// Interface: IChilkatLog
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {55396537-E4AB-46D2-A373-2B894EBD439C}
// *********************************************************************//
  IChilkatLog = interface(IDispatch)
    ['{55396537-E4AB-46D2-A373-2B894EBD439C}']
    procedure LogError(const message: WideString); safecall;
    procedure LogInfo(const message: WideString); safecall;
    function  GetInfoEntries: WideString; safecall;
    function  GetErrorEntries: WideString; safecall;
    function  Get_LineFormat: WideString; safecall;
    procedure Set_LineFormat(const pVal: WideString); safecall;
    function  Get_Version: WideString; safecall;
    procedure LogError2(const message: WideString; const keyword: WideString); safecall;
    procedure LogInfo2(const message: WideString; const keyword: WideString); safecall;
    procedure ClearLog; safecall;
    function  AppendToFile(const fileName: WideString): Integer; safecall;
    function  GetKeywordEntries(const keyword: WideString): WideString; safecall;
    procedure LogError3(const msg: WideString; const keyword: WideString; const format: WideString); safecall;
    procedure LogInfo3(const msg: WideString; const keyword: WideString; const format: WideString); safecall;
    procedure SetNamedFormat(const formatName: WideString; const formatString: WideString); safecall;
    function  GetNamedFormat(const formatName: WideString): WideString; safecall;
    function  Get_IsHTML: Integer; safecall;
    procedure Set_IsHTML(pVal: Integer); safecall;
    procedure LogDebug3(const msg: WideString; const keyword: WideString; const format: WideString); safecall;
    procedure LogDebug2(const message: WideString; const keyword: WideString); safecall;
    procedure LogDebug(const message: WideString); safecall;
    function  GetDebugEntries: WideString; safecall;
    function  Get_ContinuationPrefix: WideString; safecall;
    procedure Set_ContinuationPrefix(const pVal: WideString); safecall;
    property LineFormat: WideString read Get_LineFormat write Set_LineFormat;
    property Version: WideString read Get_Version;
    property IsHTML: Integer read Get_IsHTML write Set_IsHTML;
    property ContinuationPrefix: WideString read Get_ContinuationPrefix write Set_ContinuationPrefix;
  end;

// *********************************************************************//
// DispIntf:  IChilkatLogDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {55396537-E4AB-46D2-A373-2B894EBD439C}
// *********************************************************************//
  IChilkatLogDisp = dispinterface
    ['{55396537-E4AB-46D2-A373-2B894EBD439C}']
    procedure LogError(const message: WideString); dispid 1;
    procedure LogInfo(const message: WideString); dispid 2;
    function  GetInfoEntries: WideString; dispid 3;
    function  GetErrorEntries: WideString; dispid 4;
    property LineFormat: WideString dispid 5;
    property Version: WideString readonly dispid 6;
    procedure LogError2(const message: WideString; const keyword: WideString); dispid 7;
    procedure LogInfo2(const message: WideString; const keyword: WideString); dispid 8;
    procedure ClearLog; dispid 9;
    function  AppendToFile(const fileName: WideString): Integer; dispid 10;
    function  GetKeywordEntries(const keyword: WideString): WideString; dispid 11;
    procedure LogError3(const msg: WideString; const keyword: WideString; const format: WideString); dispid 12;
    procedure LogInfo3(const msg: WideString; const keyword: WideString; const format: WideString); dispid 13;
    procedure SetNamedFormat(const formatName: WideString; const formatString: WideString); dispid 14;
    function  GetNamedFormat(const formatName: WideString): WideString; dispid 15;
    property IsHTML: Integer dispid 16;
    procedure LogDebug3(const msg: WideString; const keyword: WideString; const format: WideString); dispid 17;
    procedure LogDebug2(const message: WideString; const keyword: WideString); dispid 18;
    procedure LogDebug(const message: WideString); dispid 19;
    function  GetDebugEntries: WideString; dispid 20;
    property ContinuationPrefix: WideString dispid 21;
  end;

// *********************************************************************//
// Interface: IChilkatLog2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4E05166B-762A-4FBC-85F1-0500C95501B2}
// *********************************************************************//
  IChilkatLog2 = interface(IChilkatLog)
    ['{4E05166B-762A-4FBC-85F1-0500C95501B2}']
    procedure EnterContext(const contextName: WideString; reportErrors: Integer); safecall;
    procedure LeaveContext; safecall;
    procedure LogData(const dataTag: WideString; const dataValue: WideString); safecall;
    function  GetXML(includeTimestamps: Integer): WideString; safecall;
    procedure SaveXML(includeTimestamps: Integer; const fileName: WideString); safecall;
    function  Get_UnusedProperty1: Integer; safecall;
    procedure Set_UnusedProperty1(pVal: Integer); safecall;
    function  Get_LastError: WideString; safecall;
    procedure UnusedMethod1(const emailAddress: WideString); safecall;
    function  Get_ComponentName: WideString; safecall;
    procedure Set_ComponentName(const pVal: WideString); safecall;
    function  Get_HighWaterMark: Integer; safecall;
    procedure Set_HighWaterMark(pVal: Integer); safecall;
    procedure CheckHighWater; safecall;
    procedure LogCommonError(const msg: WideString); safecall;
    procedure LogXml(const data: WideString); safecall;
    procedure LogDataLong(const dataTag: WideString; value: Integer); safecall;
    procedure UseSystemEventLog; safecall;
    property UnusedProperty1: Integer read Get_UnusedProperty1 write Set_UnusedProperty1;
    property LastError: WideString read Get_LastError;
    property ComponentName: WideString read Get_ComponentName write Set_ComponentName;
    property HighWaterMark: Integer read Get_HighWaterMark write Set_HighWaterMark;
  end;

// *********************************************************************//
// DispIntf:  IChilkatLog2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4E05166B-762A-4FBC-85F1-0500C95501B2}
// *********************************************************************//
  IChilkatLog2Disp = dispinterface
    ['{4E05166B-762A-4FBC-85F1-0500C95501B2}']
    procedure EnterContext(const contextName: WideString; reportErrors: Integer); dispid 22;
    procedure LeaveContext; dispid 23;
    procedure LogData(const dataTag: WideString; const dataValue: WideString); dispid 24;
    function  GetXML(includeTimestamps: Integer): WideString; dispid 25;
    procedure SaveXML(includeTimestamps: Integer; const fileName: WideString); dispid 26;
    property UnusedProperty1: Integer dispid 27;
    property LastError: WideString readonly dispid 28;
    procedure UnusedMethod1(const emailAddress: WideString); dispid 29;
    property ComponentName: WideString dispid 30;
    property HighWaterMark: Integer dispid 31;
    procedure CheckHighWater; dispid 32;
    procedure LogCommonError(const msg: WideString); dispid 33;
    procedure LogXml(const data: WideString); dispid 34;
    procedure LogDataLong(const dataTag: WideString; value: Integer); dispid 35;
    procedure UseSystemEventLog; dispid 36;
    procedure LogError(const message: WideString); dispid 1;
    procedure LogInfo(const message: WideString); dispid 2;
    function  GetInfoEntries: WideString; dispid 3;
    function  GetErrorEntries: WideString; dispid 4;
    property LineFormat: WideString dispid 5;
    property Version: WideString readonly dispid 6;
    procedure LogError2(const message: WideString; const keyword: WideString); dispid 7;
    procedure LogInfo2(const message: WideString; const keyword: WideString); dispid 8;
    procedure ClearLog; dispid 9;
    function  AppendToFile(const fileName: WideString): Integer; dispid 10;
    function  GetKeywordEntries(const keyword: WideString): WideString; dispid 11;
    procedure LogError3(const msg: WideString; const keyword: WideString; const format: WideString); dispid 12;
    procedure LogInfo3(const msg: WideString; const keyword: WideString; const format: WideString); dispid 13;
    procedure SetNamedFormat(const formatName: WideString; const formatString: WideString); dispid 14;
    function  GetNamedFormat(const formatName: WideString): WideString; dispid 15;
    property IsHTML: Integer dispid 16;
    procedure LogDebug3(const msg: WideString; const keyword: WideString; const format: WideString); dispid 17;
    procedure LogDebug2(const message: WideString; const keyword: WideString); dispid 18;
    procedure LogDebug(const message: WideString); dispid 19;
    function  GetDebugEntries: WideString; dispid 20;
    property ContinuationPrefix: WideString dispid 21;
  end;

// *********************************************************************//
// Interface: IXmlFactory
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {624CD0C7-ACD8-4E28-8393-2137F8D76DCE}
// *********************************************************************//
  IXmlFactory = interface(IDispatch)
    ['{624CD0C7-ACD8-4E28-8393-2137F8D76DCE}']
    function  NewXml: IChilkatXml; safecall;
  end;

// *********************************************************************//
// DispIntf:  IXmlFactoryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {624CD0C7-ACD8-4E28-8393-2137F8D76DCE}
// *********************************************************************//
  IXmlFactoryDisp = dispinterface
    ['{624CD0C7-ACD8-4E28-8393-2137F8D76DCE}']
    function  NewXml: IChilkatXml; dispid 1;
  end;

// *********************************************************************//
// The Class CoChilkatXml provides a Create and CreateRemote method to          
// create instances of the default interface IChilkatXml exposed by              
// the CoClass ChilkatXml. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoChilkatXml = class
    class function Create: IChilkatXml;
    class function CreateRemote(const MachineName: string): IChilkatXml;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TXmlFactory
// Help String      : XmlFactory Class
// Default Interface: IXmlFactory
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TXmlFactory = class(TOleControl)
  private
    FIntf: IXmlFactory;
    function  GetControlInterface: IXmlFactory;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    function  NewXml: IChilkatXml;
    property  ControlInterface: IXmlFactory read GetControlInterface;
    property  DefaultInterface: IXmlFactory read GetControlInterface;
  published
  end;

procedure Register;

implementation

uses ComObj;

class function CoChilkatXml.Create: IChilkatXml;
begin
  Result := CreateComObject(CLASS_ChilkatXml) as IChilkatXml;
end;

class function CoChilkatXml.CreateRemote(const MachineName: string): IChilkatXml;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ChilkatXml) as IChilkatXml;
end;

procedure TXmlFactory.InitControlData;
const
  CControlData: TControlData2 = (
    ClassID: '{7FAB24D9-F81A-49A3-A0E9-A3198DEDF454}';
    EventIID: '';
    EventCount: 0;
    EventDispIDs: nil;
    LicenseKey: nil (*HR:$80004002*);
    Flags: $00000000;
    Version: 401);
begin
  ControlData := @CControlData;
end;

procedure TXmlFactory.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IXmlFactory;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TXmlFactory.GetControlInterface: IXmlFactory;
begin
  CreateControl;
  Result := FIntf;
end;

function  TXmlFactory.NewXml: IChilkatXml;
begin
  Result := DefaultInterface.NewXml;
end;

procedure Register;
begin
  RegisterComponents('ActiveX',[TXmlFactory]);
end;

end.
