unit DServerBoletin;

interface

uses
  Windows, Messages, SysUtils, Classes,
  {
  Graphics, Controls, Forms, Dialogs,
  }
  ComServ, ComObj, VCLCom, StdVcl, BdeMts, DataBkr, DBClient, DB,{$ifndef VER130}Variants,{$endif}
  MtsRdm, Mtx,
  DZetaServerProvider,
{$ifndef DOS_CAPAS}
  Boletin_TLB,
{$endif}
  ZetaXMLTools,
  ZetaServerTools;

type
  TdmServerBoletin = class(TMtsDataModule,{$Ifndef DOS_CAPAS} IdmServerBoletin{$Endif})
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FXML: TZetaXML;
    function BuildEmpresa: OleVariant;
    function GetScript(const iScript: Integer): String;
    procedure SentinelaAfterOpen(DataSet: TDataSet);
    procedure AU_PLATFORMOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure AU_SQL_BDOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure AU_KIT_DISOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function BorraSuscripcion(const Parametros: WideString): WideString;{$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaSuscripcion(const Parametros: WideString): WideString;{$ifndef DOS_CAPAS} safecall; {$endif}
    function SentinelLogin(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSentinelasDistrib(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSentinelasCliente(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetSentinelaFolio(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS} safecall; {$endif}
{$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
{$endif}
  end;

var
  dmServerBoletin: TdmServerBoletin;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerDataset,
     FAutoClasses;

const
     K_TABLE_LOGIN = 'LOGIN';
     K_TABLE_SENTINELA = 'SENTINELA';
     K_TABLE_FOLIOS = 'FOLIOS';
     K_TABLE_MODULOS = 'MODULOS';

     Q_USUARIO_SENTINEL = 1;
     Q_SENTINELAS_DISTRIB = 2;
     Q_SENTINELAS_CLIENTE = 3;
     Q_SENTINELA_FOLIO = 4;
     Q_SENTINELA_MODULOS = 5;
     Q_BORRA_SUSCRIPCION = 6;
     Q_CAMBIA_SUSCRIPCION = 7;
     Q_AGREGA_SUSCRIPCION = 8;
     Q_EXISTE_SUSCRIPCION = 9;

{$R *.DFM}

class procedure TdmServerBoletin.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerBoletin.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := TdmZetaServerProvider.Create( Self );
end;

procedure TdmServerBoletin.MtsDataModuleDestroy(Sender: TObject);
begin
     oZetaProvider.Free;
end;

{$ifdef DOS_CAPAS}
procedure TdmServerBoletin.CierraEmpresa;
begin
end;
{$endif}

function TdmServerBoletin.GetScript(const iScript: Integer): String;
begin
     case iScript of
          Q_USUARIO_SENTINEL : Result := 'select DI_CODIGO, US_NOMBRE, DI_NOMBRE, US_PASSWRD ' +
                                         'from USUARIO  left outer join DISTRIBU on ' +
                                         'USUARIO.DI_CODIGO = DISTRIBU.DI_CODIGO ' +
                                         'where ( upper(US_CODIGO) = Upper(%s) )';
          Q_SENTINELAS_DISTRIB : Result := 'select SI_FOLIO, SI_RAZ_SOC, SN_NUMERO, VE_CODIGO,' +
                                           'AU_NUM_EMP, AU_USERS, AU_PAR_AUT, AU_CLAVE1, AU_CLAVE2,' +
                                           'cast( ''CLIENTE='' || SI_FOLIO || ''&'' || ''NOMBRE='' || SI_RAZ_SOC as CHAR(255) ) CTE_LIGA,' +
                                           'cast( ''CLIENTE='' || SI_FOLIO || ''&'' || ''FOLIO='' || AU_FOLIO || ''&'' || ''SENTINEL='' || SN_NUMERO as CHAR(100) ) FOL_LIGA ' +
                                           'from ACTUAL where ( ACTUAL.DI_CODIGO = %s ) ' +
                                           'order by %d';
          Q_SENTINELAS_CLIENTE : Result := 'select SI_FOLIO, AU_FOLIO, AU_FECHA, SN_NUMERO, VE_CODIGO, AU_CLAVE1,' +
                                           'AU_CLAVE2, AU_PAR_AUT, AU_FEC_AUT, AU_FEC_PRE, AU_KIT_DIS, DI_CODIGO,' +
                                           'cast( ''CLIENTE='' || SI_FOLIO || ''&'' || ''FOLIO='' || AU_FOLIO || ''&'' || ''SENTINEL='' || SN_NUMERO as CHAR(100) ) FOL_LIGA ' +
                                           'from AUTORIZA where ( AUTORIZA.SI_FOLIO = %d ) ' +
                                           'order by AU_FOLIO DESC';
          Q_SENTINELA_FOLIO : Result := 'select A.SI_FOLIO, A.AU_FOLIO, S.SI_RAZ_SOC, A.SN_NUMERO, A.VE_CODIGO, A.AU_NUM_EMP,' +
                                        'A.AU_USERS, A.AU_PLATFORM, A.AU_SQL_BD, A.AU_PAR_AUT, A.AU_CLAVE1, A.AU_CLAVE2,' +
                                        'A.AU_FEC_AUT, A.AU_DEFINIT, A.AU_FEC_PRE, A.AU_KIT_DIS '+
                                        'from AUTORIZA A left outer join SISTEMA S on ( S.SI_FOLIO = A.SI_FOLIO ) ' +
                                        'where ( A.SI_FOLIO = %d ) and ( A.SN_NUMERO = %d ) and ( A.AU_FOLIO = %d )';
          Q_SENTINELA_MODULOS : Result := 'select %s from AUTORIZA ' +
                                          'where ( SI_FOLIO = %d ) and ( SN_NUMERO = %d ) and ( AU_FOLIO = %d )';
          Q_BORRA_SUSCRIPCION : Result := 'delete from BOLETIN '+
                                          'where( UPPER( BL_EMAIL ) = UPPER( %s ) )';
          Q_CAMBIA_SUSCRIPCION : Result := 'update BOLETIN set '+
                                           'BL_NOMBRE = %s, BL_EMPRESA = %s, '+
                                           'BL_ACTIVO = %d, BL_ALTA = %s '+
                                           'where( UPPER( BL_EMAIL )= UPPER( %s ) )';
          Q_AGREGA_SUSCRIPCION: Result := 'insert into BOLETIN( BL_EMAIL, BL_NOMBRE, '+
                                          'BL_EMPRESA, BL_ACTIVO, BL_ALTA ) '+
                                          'values( %s, %s, %s, %d, %s )';
          Q_EXISTE_SUSCRIPCION: Result := 'select COUNT(*) CUANTOS from BOLETIN '+
                                          'where( UPPER( BL_EMAIL ) = UPPER( %s ) )';
     end;
end;

function TdmServerBoletin.BuildEmpresa: OleVariant;
const
{$ifdef ANTES}
     K_RUTA_DATOS = 'TRESS_2001:E:\Admon\Sentinel\Sentinel.gdb';
     K_USUARIO_BD = 'SYSDBA';
     K_PASSWORD = 'm';
{$else}
     K_RUTA_DATOS = 'ADMIN-SERVER:C:\ClavesSentinel\Sentinel.gdb';
     K_USUARIO_BD = 'SYSDBA';
     K_PASSWORD = 'm';
{$endif}
begin
     Result := VarArrayOf( [ K_RUTA_DATOS,                          // P_ALIAS = 0
                             K_USUARIO_BD,                          //P_USERNAME = 1
                             ZetaServerTools.Encrypt( K_PASSWORD ), //P_PASSWORD = 2
                             0,                                     //P_USUARIO = 3
                             '',                                    //P_NIVEL_0 = 4
                             '' ] );                                //P_CODIGO = 5
end;

function TdmServerBoletin.BorraSuscripcion(const Parametros: WideString): WideString;
var
   oNodo: TZetaXmlNode;
begin
      FXML := TZetaXML.Create( oZetaProvider );
      try
         with FXML do
         begin
              if LoadParametros( Parametros ) then
              begin
                   try
                      with oZetaProvider do
                      begin
                           EmpresaActiva := BuildEmpresa;
                           try
                              EmpiezaTransaccion;
                              ExecSQL( EmpresaActiva, Format( GetScript( Q_BORRA_SUSCRIPCION ), [EntreComillas(FXML.ParamAsString('EMAIL'))] ) );
                              TerminaTransaccion(True);
                              AgregaDescripcionXML( Documento.GetRoot, 'Boletin Tress', 'Suscripciones' );
                              oNodo := SetTablaMaster( Documento.GetRoot );
                              oNodo := SetForma( oNodo, 'formsuscripcion', 'suscribe.asp', False );
                              oNodo := SetRenglon( oNodo );
                              oNodo := SetContenido( oNodo, GetTagLbl( Format( 'La Direcci�n de Correo: %s fu� eliminada con �xito.', [ FXML.ParamAsString('EMAIL') ] ) ) );
                              AgregaFooter( Documento.GetRoot, False );
                              Result := GetDocXML;
                           except
                                 RollBackTransaccion;
                           end;
                      end;
                   except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Accesar Informaci�n de Base de Datos: ' + Error.Message );
                        end;
                   end;
             end
             else
                 BuildErrorXML;
         end;
      finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

function TdmServerBoletin.GrabaSuscripcion(const Parametros: WideString): WideString;
const
     K_ALTA = 1;
var
   oNodo: TZetaXmlNode;
   lExiste: Boolean;
   FDataSet: TZetaCursor;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          FDataSet := CreateQuery;
                          EmpresaActiva := BuildEmpresa;
                          PreparaQuery( FDataSet, Format( GetScript( Q_EXISTE_SUSCRIPCION ), [ EntreComillas(FXML.ParamAsString('EMAIL') ) ] ) );
                          with FDataSet do
                          begin
                               Active := True;
                               lExiste := ( Fields[0].AsInteger > 0 );
                               Active := False;
                          end;
                          EmpiezaTransaccion;
                          try
                             if lExiste then
                             begin
                                  ExecSQL( EmpresaActiva, Format( GetScript( Q_CAMBIA_SUSCRIPCION ),[ EntreComillas(FXML.ParamAsString('NOMBRE')), EntreComillas(FXML.ParamAsString('EMPRESA')), K_ALTA, DatetoStrSQLC( Now ), EntreComillas(FXML.ParamAsString('EMAIL')) ] ) );
                             end
                             else
                             begin
                                  ExecSQL( EmpresaActiva, Format( GetScript( Q_AGREGA_SUSCRIPCION ),[ EntreComillas(FXML.ParamAsString('EMAIL')), EntreComillas(FXML.ParamAsString('NOMBRE')), EntreComillas(FXML.ParamAsString('EMPRESA')), K_ALTA, DatetoStrSQLC( Now ) ] ) );
                             end;
                             TerminaTransaccion(True);
                             AgregaDescripcionXML( Documento.GetRoot, 'Boletin Tress', 'Suscripciones' );
                             oNodo := SetTablaMaster( Documento.GetRoot );
                             oNodo := SetForma( oNodo, 'formsuscripcion', 'suscribe.asp', False );
                             oNodo := SetRenglon( oNodo );
                             oNodo := SetContenido( oNodo, GetTagLbl( Format( 'La Direcci�n de Correo: %s fu� agregada con �xito.', [ FXML.ParamAsString('EMAIL') ] ) ) );
                             AgregaFooter( Documento.GetRoot, False );
                             Result := GetDocXML;
                          except
                             RollBackTransaccion;
                          end;
                     end;
                   except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Accesar Informaci�n de Base de Datos: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

{ Consulta de Informaci�n de Sentinelas }

function TdmServerBoletin.SentinelLogin(const Parametros: WideString): WideString;
var
   FCursor: TZetaCursor;
   Datos: TZetaXMLNode;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := BuildEmpresa;
                          FCursor:= CreateQuery;
                          try
                             if AbreQueryScript( FCursor, Format( GetScript( Q_USUARIO_SENTINEL ),
                                                 [ EntreComillas( FXML.ParamAsString( 'USUARIO' ) ) ] ) ) then
                             begin
                                  if ( not FCursor.EOF ) then
                                  begin
                                       if ( FCursor.FieldByName( 'US_PASSWRD' ).AsString = FXML.ParamAsString( 'CLAVE' ) ) then
                                       begin
                                            Datos := Documento.NewChild( K_TABLE_LOGIN, ZetaCommonClasses.VACIO );
                                            with Datos do
                                            begin
                                                 NewChild( 'CODIGO', FCursor.FieldByName( 'DI_CODIGO' ).AsString );
                                                 NewChild( 'DISTRIBUIDOR', FCursor.FieldByName( 'DI_NOMBRE' ).AsString );
                                                 NewChild( 'NOMBRE', FCursor.FieldByName( 'US_NOMBRE' ).AsString );
                                            end;
                                       end
                                       else
                                           BuildErrorXML( Format( 'Clave Incorrecta para el Usuario: %s',
                                                          [ FXML.ParamAsString( 'USUARIO' ) ] ) );
                                  end
                                  else
                                      BuildErrorXML( Format( 'No se Tiene Definido el Usuario: %s',
                                                     [ FXML.ParamAsString( 'USUARIO' ) ] ) );
                             end
                             else
                                 BuildErrorXML( 'Error al Consultar Informaci�n del Usuario' );
                          finally
                                 FreeAndNil( FCursor );
                          end;
                     end;
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Accesar Informaci�n de Base de Datos: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

function TdmServerBoletin.GetSentinelasDistrib(const Parametros: WideString): WideString;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     oZetaProvider.EmpresaActiva := BuildEmpresa;
                     AgregaXMLQueryNET( Documento, K_TABLE_SENTINELA, Format( GetScript( Q_SENTINELAS_DISTRIB ), [
                                        EntreComillas( ParamAsString( 'DISTRIBUIDOR' ) ),
                                        ParamAsInteger( 'CAMPOORDEN' ) ] ) );
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Obtener Lista de Sentinelas: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

function TdmServerBoletin.GetSentinelasCliente(const Parametros: WideString): WideString;
begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     oZetaProvider.EmpresaActiva := BuildEmpresa;
                     AgregaXMLQueryNET( Documento, K_TABLE_FOLIOS, Format( GetScript( Q_SENTINELAS_CLIENTE ), [
                                        ParamAsInteger( 'FOLIO' ) ] ), TRUE );
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Obtener Lista de Sentinelas: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

function TdmServerBoletin.GetSentinelaFolio(const Parametros: WideString): WideString;
const
     K_AUTORIZADOS = 'AU_MOD_%d';
     K_PRESTADOS = 'AU_PRE_%d';
var
   oCursor: TZetaCursor;
   oDataSet : TServerDataSet;

   procedure CrearDataSetModulos;
   begin
        with oDataSet do
        begin
             InitTempDataSet;
             AddStringField( 'MODULO', 30 );
             AddStringField( 'AUTORIZADO', 2 );
             AddStringField( 'PRESTADO', 2 );
             CreateTempDataset;
        end;
   end;

   function GetCamposModulos: String;
   var
      eModulo : TModulos;
   begin
        Result := ZetaCommonClasses.VACIO;
        for eModulo := Low( TModulos ) to High ( TModulos ) do
        begin
             Result := ConcatString( Result, Format( K_AUTORIZADOS, [ Ord( eModulo ) ] ), ',' );
             Result := ConcatString( Result, Format( K_PRESTADOS, [ Ord( eModulo ) ] ), ',' );
        end;
   end;

   procedure AgregaModulos;
   var
      FAuto : TAutorizacion;
      eModulo : TModulos;
   begin
        oCursor:= oZetaProvider.CreateQuery;
        try
           if oZetaProvider.AbreQueryScript( oCursor, Format( GetScript( Q_SENTINELA_MODULOS ), [
                                             GetCamposModulos,
                                             FXML.ParamAsInteger( 'CLIENTE' ),
                                             FXML.ParamAsInteger( 'SENTINEL' ),
                                             FXML.ParamAsInteger( 'FOLIO' ) ] ) ) then
           begin
                FAuto := TAutorizacion.Create;
                try
                   with oCursor do
                   begin
                        if ( not EOF ) then
                        begin
                             for eModulo := Low( TModulos ) to High ( TModulos ) do
                             begin
                                  oDataSet.AppendRecord( [ FAuto.GetModuloStr( eModulo ),
                                           ZetaCommonTools.BoolToSiNo( FieldByName( Format( K_AUTORIZADOS, [ Ord( eModulo ) ] ) ).AsInteger > 0 ),
                                           ZetaCommonTools.BoolToSiNo( FieldByName( Format( K_PRESTADOS, [ Ord( eModulo ) ] ) ).AsInteger > 0 ) ] );
                             end;
                        end
                        else
                            FXML.BuildErrorXML( 'No se Encontr� Informaci�n de M�dulos' );
                   end;
                finally
                       FreeAndNil( FAuto );
                end;
           end
           else
               FXML.BuildErrorXML( 'Error al Consultar Informaci�n de M�dulos' );
        finally
               FreeAndNil( oCursor );
        end;
   end;

begin
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     // Encabezado - Datos Generales del Sentinela
                     with oZetaProvider do
                     begin
                          EmpresaActiva := BuildEmpresa;
                          oCursor:= CreateQuery( Format( GetScript( Q_SENTINELA_FOLIO ), [
                                                 FXML.ParamAsInteger( 'CLIENTE' ),
                                                 FXML.ParamAsInteger( 'SENTINEL' ),
                                                 FXML.ParamAsInteger( 'FOLIO' ) ] ) );
                     end;
                     try
                        oCursor.AfterOpen := SentinelaAfterOpen;
                        AgregaXMLCursorNET( Documento, oCursor, K_TABLE_SENTINELA );
                     finally
                            FreeAndNil( oCursor );
                     end;
                     // Detalle - Modulos Autorizados y Prestados
                     oDataSet := TServerDataSet.Create( oZetaProvider );
                     try
                        CrearDataSetModulos;
                        AgregaModulos;
                        AgregaXMLDataSetDataNET( Documento, oDataSet, K_TABLE_MODULOS );
                     finally
                        FreeAndNil( oDataSet );
                     end;
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Obtener Lista de Sentinelas: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     SetComplete;
end;

procedure TdmServerBoletin.SentinelaAfterOpen(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'AU_PLATFORM' ).OnGetText := AU_PLATFORMOnGetText;
          FieldByName( 'AU_SQL_BD' ).OnGetText := AU_SQL_BDOnGetText;
          FieldByName( 'AU_KIT_DIS' ).OnGetText := AU_KIT_DISOnGetText;
     end;
end;

procedure TdmServerBoletin.AU_PLATFORMOnGetText(Sender: TField; var Text: String;
          DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := VACIO
          else
             Text := FAutoClasses.GetPlatformName( TPlataforma( Sender.AsInteger ) );
     end
     else
         Text := Sender.AsString;
end;

procedure TdmServerBoletin.AU_SQL_BDOnGetText(Sender: TField; var Text: String;
          DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := VACIO
          else
             Text := FAutoClasses.GetSQLEngineName( TSQLEngine( Sender.AsInteger ) );
     end
     else
         Text := Sender.AsString;
end;

procedure TdmServerBoletin.AU_KIT_DISOnGetText(Sender: TField; var Text: String;
          DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := VACIO
          else
             Text := ZetaCommonTools.BoolToSiNo( Sender.AsInteger > 0 );
     end
     else
         Text := Sender.AsString;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerBoletin, Class_dmServerBoletin, ciMultiInstance, ZetaServerTools.GetThreadingModel);
{$endif}
end.