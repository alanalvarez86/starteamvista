unit FSendBulletin;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Psock, NMsmtp, ExtCtrls,
     ComCtrls, IniFiles, Db, DbTables,
     IB_Components, IBDataset,
     ZetaAsciiFile;
     
type
  TBulletinSender = class(TForm)
    Email: TNMSMTP;
    OpenDialog: TOpenDialog;
    StatusBar: TStatusBar;
    ParametrosGB: TGroupBox;
    ServidorLBL: TLabel;
    UsuarioLBL: TLabel;
    RemitenteLBL: TLabel;
    RemitenteDireccionLBL: TLabel;
    Servidor: TEdit;
    Usuario: TEdit;
    Remitente: TEdit;
    RemitenteDireccion: TEdit;
    DireccionesLBL: TLabel;
    Direcciones: TEdit;
    DireccionesSeek: TSpeedButton;
    MensajeGB: TGroupBox;
    ArchivoLBL: TLabel;
    TemaLBL: TLabel;
    ArchivoFind: TSpeedButton;
    Archivo: TEdit;
    Tema: TEdit;
    PanelInferior: TPanel;
    EnviarEMails: TBitBtn;
    Salir: TBitBtn;
    ProbarGB: TGroupBox;
    EMailTestLBL: TLabel;
    DoTest: TCheckBox;
    EMailTest: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EmailAuthenticationFailed(var Handled: Boolean);
    procedure EmailConnectionFailed(Sender: TObject);
    procedure EmailConnectionRequired(var Handled: Boolean);
    procedure EmailFailure(Sender: TObject);
    procedure EmailInvalidHost(var Handled: Boolean);
    procedure EmailRecipientNotFound(Recipient: String);
    procedure ArchivoFindClick(Sender: TObject);
    procedure EnviarEMailsClick(Sender: TObject);
    procedure DireccionesSeekClick(Sender: TObject);
  private
    { Private declarations }
    FRecords: Integer;
    FBitacora: TAsciiLog;
    FUsarLista: Boolean;
    function ConectaHost: Boolean;
    function DesconectaHost: Boolean;
    function GetFromAddress: String;
    function GetFromName: String;
    function GetHost: String;
    function GetUserID: String;
    function IBQuery: TQuery;
    procedure Conectar( const sAddress: String );
    procedure Desconectar;
    procedure Enviar;
    procedure SendEmail( const sDestinatario: String );
    procedure SetControls;
    procedure ShowStatus(const sMensaje: String);
    procedure MuestraError(const sError: String);
    procedure MuestraExcepcion(const sError: String; Error: Exception);
    procedure SetUsarLista(const Value: Boolean);
    procedure EnviarLista;
  public
    { Public declarations }
    property UsarLista: Boolean write SetUsarLista;
  end;

var
  BulletinSender: TBulletinSender;

implementation

uses DEMails,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.DFM}

{ ******** TBulletinSender ************ }

procedure TBulletinSender.FormCreate(Sender: TObject);
begin
     FBitacora := TAsciiLog.Create;
     FUsarLista := False;
end;

procedure TBulletinSender.FormShow(Sender: TObject);
var
   iYear, iMes, iDia: Word;
begin
     with DireccionesLBL do
     begin
          Visible := FUsarLista;
          Enabled := FUsarLista;
     end;
     with Direcciones do
     begin
          Visible := FUsarLista;
          Enabled := FUsarLista;
     end;
     with DireccionesSeek do
     begin
          Visible := FUsarLista;
          Enabled := FUsarLista;
     end;
     if FUsarLista then
     begin
          Caption := 'Enviar Correo A Una Lista De Direcciones';
          Tema.Text := 'Tema del Correo';
          Archivo.Text := 'correo.htm';
     end
     else
     begin
          DecodeDate( Now, iYear, iMes, iDia );
          Tema.Text := Format( 'Bolet�n De %s Del %d', [ ZetaCommonTools.MesConLetraMes( iMes ), iYear ] );
          Archivo.Text := 'boletin.htm';
     end;
     SetControls;
end;

procedure TBulletinSender.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FBitacora );
end;

procedure TBulletinSender.SetUsarLista(const Value: Boolean);
begin
     FUsarLista := Value;
end;

function TBulletinSender.IBQuery: TQuery;
begin
     Result := EmailCFG.tqMailSender;
end;

procedure TBulletinSender.MuestraError( const sError: String );
begin
     FBitacora.WriteTexto( sError );
end;

procedure TBulletinSender.MuestraExcepcion( const sError: String; Error: Exception );
begin
     FBitacora.WriteException( 0, sError, Error );
end;

procedure TBulletinSender.SetControls;
begin
end;

procedure TBulletinSender.ShowStatus( const sMensaje: String );
begin
     StatusBar.SimpleText := sMensaje;
end;

function TBulletinSender.GetFromAddress: String;
begin
     Result := RemitenteDireccion.Text;
end;

function TBulletinSender.GetFromName: String;
begin
     Result := Remitente.Text;
end;

function TBulletinSender.GetHost: String;
begin
     Result := Servidor.Text;
end;

function TBulletinSender.GetUserID: String;
begin
     Result := Usuario.Text;
end;

function TBulletinSender.ConectaHost: Boolean;
begin
     with Email do
     begin
          if not Connected then
          begin
               ClearParameters;
               Host := GetHost;
               UserID := GetUserId;
               PostMessage.FromAddress := GetFromAddress;
               PostMessage.FromName := GetFromName;
               Connect;
          end;
          Result := Connected;
     end;
end;

function TBulletinSender.DesconectaHost: Boolean;
begin
     with Email do
     begin
          if Connected then
          begin
               Disconnect;
          end;
          Result := Connected;
     end;
end;

procedure TBulletinSender.SendEmail( const sDestinatario: String );
begin
     with Email do
     begin
          with PostMessage do
          begin
               Date := DateToStr( Now );
               Subject := Tema.Text;
               Attachments.Clear;
               ToCarbonCopy.Clear;
               ToBlindCarbonCopy.Clear;
               with ToAddress do
               begin
                    Clear;
                    Add( sDestinatario );
               end;
               with Body do
               begin
                    Clear;
                    LoadFromFile( Archivo.Text );
               end;
          end;
          SendMail;
          Sleep( 500 );
     end;
end;

procedure TBulletinSender.EmailAuthenticationFailed(var Handled: Boolean);
begin
     MuestraError( 'eMail Authentication Failed' );
     Handled := True;
end;

procedure TBulletinSender.EmailConnectionFailed(Sender: TObject);
begin
     MuestraError( 'eMail Connection Failed' );
end;

procedure TBulletinSender.EmailConnectionRequired(var Handled: Boolean);
begin
     MuestraError( 'eMail Connection Required' );
     Handled := True;
end;

procedure TBulletinSender.EmailFailure(Sender: TObject);
begin
     MuestraError( 'eMail General Failure' );
end;

procedure TBulletinSender.EmailInvalidHost(var Handled: Boolean);
begin
     MuestraError( Format( 'Servidor Inv�lido: %s', [ eMail.Host ] ) );
     Handled := True;
end;

procedure TBulletinSender.EmailRecipientNotFound(Recipient: String);
begin
     MuestraError( Format( 'Destinatario No Encontrado: %s', [ Recipient ] ) );
end;

procedure TBulletinSender.ArchivoFindClick(Sender: TObject);
begin
     with Archivo do
     begin
          with OpenDialog do
          begin
               DefaultExt := 'htm';
               Filter := 'Archivos HTM (*.htm)|*.htm|Archivos HTML (*.html)|*.html|Textos ( *.txt )|*.txt|Todos ( *.* )|*.*';
               FilterIndex := 0;
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TBulletinSender.Conectar( const sAddress: String );
const
     {$ifdef FALSE}
     Q_EMAIL_TEST = 'gandradeus@msn.com';
     Q_EMAIL_TEST = 'gandrade@tress.com.mx';
     {$endif}
     Q_EMAIL_COUNT = 'select COUNT(*) from BOLETIN %s';
     Q_EMAIL_LIST = 'select BL_EMAIL from BOLETIN %s order by BL_EMAIL';
var
   oCursor: TCursor;
   sFiltro: String;
begin
     if DoTest.Checked then
        sFiltro := Format( '( BOLETIN.BL_EMAIL = ''%s'' )', [ sAddress ] )
     else
         sFiltro := Format( '( BOLETIN.BL_ACTIVO = %d )', [ K_ALTA ] );
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with IBQuery do
           begin
                Active := False;
                with SQL do
                begin
                     Clear;
                     if ( sFiltro = '' ) then
                        Add( Format( Q_EMAIL_COUNT, [ '' ] ) )
                     else
                         Add( Format( Q_EMAIL_COUNT, [ Format( 'where %s', [ sFiltro ] ) ] ) );
                end;
                Active := True;
                if IsEmpty then
                   FRecords := 0
                else
                    FRecords := Fields[ 0 ].AsInteger;
                Active := False;
                if ( FRecords > 0 ) then
                begin
                     ShowStatus( Format( '%6.0n Direcciones', [ FRecords / 1 ] ) );
                     with SQL do
                     begin
                          Clear;
                          if ( sFiltro = '' ) then
                             Add( Format( Q_EMAIL_LIST, [ '' ] ) )
                          else
                              Add( Format( Q_EMAIL_LIST, [ Format( 'where %s', [ sFiltro ] ) ] ) );
                     end;
                     Active := True;
                end
                else
                    DatabaseError( 'No Hay Direcciones De e-Mail Que Cumplan Con El Filtro Especificado' );
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBulletinSender.Desconectar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with IBQuery do
           begin
                Active := False;
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBulletinSender.Enviar;
var
   oCursor: TCursor;
   i, j: Integer;
   sDireccion: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FBitacora.Init( ChangeFileExt( Application.ExeName, '.log' ) );
        try
           with FBitacora do
           begin
                WriteTexto( '' );
                WriteTimeStamp( StringOfChar( '*', 10 ) + ' %s ' + StringOfChar( '*', 10 ) );
                WriteTexto( '' );
           end;
           try
              Conectar( EMailTest.Text );
              if ConectaHost then
              begin
                   try
                      with IBQuery do
                      begin
                           if Active then
                           begin
                                i := 0;
                                j := 0;
                                while not Eof do
                                begin
                                     Inc( i );
                                     ShowStatus( Format( 'Enviando Mensaje a Direcci�n %d de %d', [ i, FRecords ] ) );
                                     sDireccion := FieldByName( 'BL_EMAIL' ).AsString;
                                     try
                                        SendEMail( sDireccion );
                                        Inc( j );
                                     except
                                           on Error: Exception do
                                           begin
                                                MuestraExcepcion( Format( 'Error Al Enviar e-Mail A %s', [ sDireccion ] ), Error );
                                           end;
                                     end;
                                     Next;
                                end;
                                ShowStatus( Format( 'El Mensaje Fu� Enviado A %d de %d Direcciones ( %5.2n %s )', [ j, FRecords, 100 * j / FRecords, '%' ] ) );
                           end;
                      end;
                   except
                         on Error: Exception do
                         begin
                              Application.HandleException( Error );
                         end;
                   end;
              end
              else
                  MuestraError( Format( 'El Servidor De Email ( %s ) No Se Pudo Conectar', [ GetHost ] ) );
              Desconectar;
           finally
                  DesconectaHost;
           end;
           with FBitacora do
           begin
                WriteTexto( '' );
                WriteTimeStamp( StringOfChar( '*', 10 ) + ' %s ' + StringOfChar( '*', 10 ) );
                WriteTexto( '' );
           end;
        finally
               FBitacora.Close;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBulletinSender.EnviarLista;
var
   oCursor: TCursor;
   i, j: Integer;
   sFileName, sDireccion: String;
   FLista: TStrings;
begin
     sFileName := Direcciones.Text;
     if FileExists( sFileName ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             FBitacora.Init( ChangeFileExt( Application.ExeName, '.log' ) );
             try
                with FBitacora do
                begin
                     WriteTexto( '' );
                     WriteTexto( Format( 'Enviando a Lista: %s', [ sFileName ] ) );
                     WriteTimeStamp( StringOfChar( '*', 10 ) + ' %s ' + StringOfChar( '*', 10 ) );
                     WriteTexto( '' );
                end;
                FLista := TStringList.Create;
                try
                   with FLista do
                   begin
                        if DoTest.Checked then
                           Add( EMailTest.Text )
                        else
                            LoadFromFile( sFileName );
                        FRecords := Count;
                   end;
                   try
                      if ConectaHost then
                      begin
                           try
                              j := 0;
                              for i := 0 to ( FLista.Count - 1 ) do
                              begin
                                   ShowStatus( Format( 'Enviando Mensaje a Direcci�n %d de %d', [ i, FRecords ] ) );
                                   sDireccion := FLista.Strings[ i ];
                                   try
                                      SendEMail( sDireccion );
                                      Inc( j );
                                   except
                                         on Error: Exception do
                                         begin
                                              MuestraExcepcion( Format( 'Error Al Enviar e-Mail A %s', [ sDireccion ] ), Error );
                                         end;
                                   end;
                              end;
                              ShowStatus( Format( 'El Mensaje Fu� Enviado A %d de %d Direcciones ( %5.2n %s )', [ j, FRecords, 100 * j / FRecords, '%' ] ) );
                           except
                                 on Error: Exception do
                                 begin
                                      Application.HandleException( Error );
                                 end;
                           end;
                      end
                      else
                          MuestraError( Format( 'El Servidor De Email ( %s ) No Se Pudo Conectar', [ GetHost ] ) );
                      Desconectar;
                   finally
                          DesconectaHost;
                   end;
                finally
                       FreeAndNil( FLista );
                end;
                with FBitacora do
                begin
                     WriteTexto( '' );
                     WriteTimeStamp( StringOfChar( '*', 10 ) + ' %s ' + StringOfChar( '*', 10 ) );
                     WriteTexto( '' );
                end;
             finally
                    FBitacora.Close;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end
     else
         ZetaDialogo.zError( '� Archivo No Existe !', Format( 'El Archivo %s No Existe', [ sFileName ] ), 0 );
end;

procedure TBulletinSender.DireccionesSeekClick(Sender: TObject);
begin
     with Direcciones do
     begin
          with OpenDialog do
          begin
               DefaultExt := 'txt';
               Filter := 'Textos ( *.txt )|*.txt|Todos ( *.* )|*.*';
               FilterIndex := 0;
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TBulletinSender.EnviarEMailsClick(Sender: TObject);
begin
     if FUsarLista then
        EnviarLista
     else
         Enviar;
end;

end.
