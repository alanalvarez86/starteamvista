object BulletinSender: TBulletinSender
  Left = 224
  Top = 162
  Width = 432
  Height = 351
  Caption = 'Enviar Bolet�n Tress'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 304
    Width = 424
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object ParametrosGB: TGroupBox
    Left = 0
    Top = 0
    Width = 424
    Height = 130
    Align = alTop
    Caption = ' Par�metros del Env�o '
    TabOrder = 1
    object ServidorLBL: TLabel
      Left = 84
      Top = 19
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = '&Servidor:'
      FocusControl = Servidor
    end
    object UsuarioLBL: TLabel
      Left = 87
      Top = 41
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = Usuario
    end
    object RemitenteLBL: TLabel
      Left = 75
      Top = 62
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = '&Remitente:'
      FocusControl = Remitente
    end
    object RemitenteDireccionLBL: TLabel
      Left = 8
      Top = 83
      Width = 118
      Height = 13
      Alignment = taRightJustify
      Caption = 'D&irecci�n Del Remitente:'
      FocusControl = RemitenteDireccion
    end
    object DireccionesLBL: TLabel
      Left = 27
      Top = 104
      Width = 99
      Height = 13
      Alignment = taRightJustify
      Caption = '&Lista de Direcciones:'
      Enabled = False
      FocusControl = Direcciones
      Visible = False
    end
    object DireccionesSeek: TSpeedButton
      Left = 383
      Top = 100
      Width = 23
      Height = 22
      Hint = 'Buscar El Archivo Del Mensaje a Enviar'
      Enabled = False
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      Visible = False
      OnClick = DireccionesSeekClick
    end
    object Servidor: TEdit
      Left = 128
      Top = 16
      Width = 253
      Height = 21
      TabOrder = 0
      Text = 'WEBSERVER'
    end
    object Usuario: TEdit
      Left = 128
      Top = 37
      Width = 253
      Height = 21
      TabOrder = 1
      Text = 'boletintress'
    end
    object Remitente: TEdit
      Left = 128
      Top = 58
      Width = 253
      Height = 21
      TabOrder = 2
      Text = 'Boletin Tress'
    end
    object RemitenteDireccion: TEdit
      Left = 128
      Top = 79
      Width = 253
      Height = 21
      TabOrder = 3
      Text = 'boletintress@tress.com.mx'
    end
    object Direcciones: TEdit
      Left = 128
      Top = 100
      Width = 253
      Height = 21
      Enabled = False
      TabOrder = 4
      Text = 'D:\Sentinel\Boletin\direcciones.txt'
      Visible = False
    end
  end
  object MensajeGB: TGroupBox
    Left = 0
    Top = 130
    Width = 424
    Height = 67
    Align = alTop
    Caption = ' Mensaje a Enviar '
    TabOrder = 2
    object ArchivoLBL: TLabel
      Left = 23
      Top = 41
      Width = 101
      Height = 14
      Alignment = taRightJustify
      Caption = 'Arc&hivo Del Mensaje:'
      FocusControl = Archivo
    end
    object TemaLBL: TLabel
      Left = 32
      Top = 18
      Width = 92
      Height = 14
      Alignment = taRightJustify
      Caption = '&Tema Del Mensaje:'
      FocusControl = Tema
    end
    object ArchivoFind: TSpeedButton
      Left = 383
      Top = 36
      Width = 23
      Height = 23
      Hint = 'Buscar El Archivo Del Mensaje a Enviar'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ArchivoFindClick
    end
    object Archivo: TEdit
      Left = 128
      Top = 37
      Width = 253
      Height = 22
      TabOrder = 0
      Text = 'D:\Sentinel\Boletin\boletin.htm'
    end
    object Tema: TEdit
      Left = 128
      Top = 14
      Width = 253
      Height = 22
      TabOrder = 1
      Text = 'Bolet�n De Mayo Del 2002'
    end
  end
  object PanelInferior: TPanel
    Left = 0
    Top = 266
    Width = 424
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object EnviarEMails: TBitBtn
      Left = 216
      Top = 6
      Width = 119
      Height = 25
      Caption = 'Enviar e-mails'
      Default = True
      TabOrder = 0
      OnClick = EnviarEMailsClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Salir: TBitBtn
      Left = 344
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Salir'
      TabOrder = 1
      Kind = bkClose
    end
  end
  object ProbarGB: TGroupBox
    Left = 0
    Top = 197
    Width = 424
    Height = 69
    Align = alClient
    Caption = ' Pruebas de Env�o '
    TabOrder = 4
    object EMailTestLBL: TLabel
      Left = 25
      Top = 42
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Caption = 'Direcci�n de Prueba:'
    end
    object DoTest: TCheckBox
      Left = 48
      Top = 20
      Width = 93
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Es Una Prueba:'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object EMailTest: TEdit
      Left = 128
      Top = 38
      Width = 253
      Height = 21
      TabOrder = 1
      Text = 'mvega@tress.com.mx'
    end
  end
  object Email: TNMSMTP
    Port = 25
    TimeOut = 5000
    ReportLevel = 16
    OnInvalidHost = EmailInvalidHost
    OnConnectionFailed = EmailConnectionFailed
    OnConnectionRequired = EmailConnectionRequired
    PostMessage.LocalProgram = 'TressBulletinSender'
    EncodeType = uuMime
    ClearParams = False
    SubType = mtHtml
    Charset = 'us-ascii'
    OnRecipientNotFound = EmailRecipientNotFound
    OnFailure = EmailFailure
    OnAuthenticationFailed = EmailAuthenticationFailed
    Left = 8
    Top = 16
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'htm'
    Filter = 
      'Archivos HTM (*.htm)|*.htm|Archivos HTML (*.html)|*.html|Textos ' +
      '( *.txt )|*.txt|Todos ( *.* )|*.*'
    FilterIndex = 0
    Title = 'Seleccione el Archivo del Mensaje'
    Left = 42
    Top = 17
  end
end
