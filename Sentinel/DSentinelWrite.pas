unit DSentinelWrite;

{$define VALIDAEMPLEADOSGLOBAL}


{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DSentinelWrite.pas                         ::
  :: Descripci�n: Rutina principal de Sentinel3s.exe         ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     IB_Components, ZetaLicenseMgr, DZetaServerProvider;

type
  TSentinelLog = procedure(const sMensaje: String) of object;
  TdmSentinelWrite = class(TDataModule)
    dbComparte: TIB_Connection;
    tqClaves: TIB_Cursor;
    dbTx: TIB_Transaction;
  private
    { Private declarations }
    FInformation: String;
    {$ifndef VALIDAEMPLEADOSGLOBAL}
    function AbreComparte(const sDatabase, sUserName, sPassword: String): Boolean;
    procedure AutorizacionSetData(Sender: TObject; var StringData: String);
    procedure CierraComparte;
    {$endif}
  public
    { Public declarations }
    function Inicializa( Log: TSentinelLog ): Boolean;
  end;

function RenovarAutorizacion( Log: TSentinelLog ): Boolean;

implementation

{$R *.DFM}

uses ZetaRegistryServer,
     ZetaCommonTools,
     FAutoServer;

function RenovarAutorizacion( Log: TSentinelLog ): Boolean;
begin
     with TdmSentinelWrite.Create( nil ) do
     begin
          try
             Result := Inicializa( Log );
          finally
                 Free;
          end;
     end;
end;

{ ******** TdmSentinelWrite ********* }
{$ifndef VALIDAEMPLEADOSGLOBAL}
function TdmSentinelWrite.AbreComparte( const sDatabase, sUserName, sPassword: String ): Boolean;
begin
     Result := False;
     with dbComparte do
     begin
          try
             Connected := False;
             DatabaseName := sDatabase;
             UserName := sUserName;
             Password := sPassword;
             Connected := True;
             Result := True;
          except
                on Error: Exception do
                begin
                     FInformation := Error.Message;
                end;
          end;
     end;
end;

procedure TdmSentinelWrite.CierraComparte;
begin
     with dbComparte do
     begin
          Connected := False;
     end;
end;

procedure TdmSentinelWrite.AutorizacionSetData(Sender: TObject; var StringData: String);
const
     Q_BORRA_CLAVE = 'delete from CLAVES where ( CL_NUMERO = 1 )';
     Q_AGREGA_CLAVE = 'insert into CLAVES ( CL_NUMERO, CL_TEXTO ) values ( 1, ''%s'' )';
begin
     with dbTx do
     begin
          if not InTransaction then
             StartTransaction;
          try
             with tqClaves do
             begin
                  with SQL do
                  begin
                       Clear;
                       Add( Q_BORRA_CLAVE );
                  end;
                  ExecSQL;
                  with SQL do
                  begin
                       Clear;
                       Add( Format( Q_AGREGA_CLAVE, [ StringData ] ) );
                  end;
                  ExecSQL;
             end;
             Commit;
          except
                on Error: Exception do
                begin
                     Rollback;
                     raise;
                end;
          end;
     end;
end;
{$endif}

{$ifdef VALIDAEMPLEADOSGLOBAL}
function TdmSentinelWrite.Inicializa( Log: TSentinelLog ): Boolean;
const
     ERR_OK = 0;
     ERR_SENTINEL = 1;
     ERR_NO_CONFIG = 2;
     ERR_NO_COMPARTE = 3;
     ERR_NO_SENTINEL = 4;
     ERR_NO_COMPANIES = 5;
var
   iError: Byte;
   FRegistry: TZetaRegistryServer;
   FAutorizacion: TAutoServer;
   oZetaProvider : TdmZetaServerProvider;
   oLicManager : TLicenseMgr;

   function MensajeLOG( const iError: Byte ): String;
   const
        CR_LF = Chr( 13 ) + Chr( 10 );
   begin
        case iError of
             ERR_OK: Result := 'OK: Sentinel Activado Con Exito';
             ERR_SENTINEL: Result := 'ERROR: La Computadora No Tiene El Sentinel, Pero Este Ya Fu� Activado Para El D�a De Hoy';
             ERR_NO_CONFIG: Result := 'ERROR: No Est� Configurado El Cliente De Tress Windows En Esta Computadora';
             ERR_NO_COMPARTE: Result := 'ERROR: Al Abrir Datos De Comparte';
             ERR_NO_SENTINEL: Result := 'ERROR: No Se Encontr� Sentinel';
             ERR_NO_COMPANIES: Result := 'ERROR: No Se Pudieron Leer Las Compa�ias Registradas.';
        end;
        Result := CR_LF + Result + CR_LF + 'STATUS Sentinel: ' + FAutorizacion.StatusMsg + CR_LF + FInformation;
   end;

begin
     FInformation := '';
     FAutorizacion := TAutoServer.Create;
     try
        FRegistry := TZetaRegistryServer.Create;
        try
           if ZetaCommonTools.StrLleno( FRegistry.Database ) and ZetaCommonTools.StrLleno( FRegistry.UserName ) and ZetaCommonTools.StrLleno( FRegistry.Password ) then
           begin
                try
                  oZetaProvider := TdmZetaServerProvider.Create(nil);
                  oLicManager := TLicenseMgr.Create( oZetaProvider );
                  oLicManager.AutoServer := FAutorizacion;

                  with FAutorizacion do
                  begin
                       OnSetData := oLicManager.AutoSetData;
                       OnGetDataAdditional := oLicManager.AutoOnGetDataAdditional;
                  end;

                  if True then
                  begin
                        //Realiza el Conteo
                        try
                           oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;
                           oLicManager.EmpleadosGetGlobal;
                        except
                              on Error: Exception do
                              begin
                                   FInformation := Error.Message;
                                   iError := ERR_NO_COMPANIES;
                              end;
                        end;

                        try
                           if FAutorizacion.EscribirAutorizacion then
                              iError := ERR_OK
                           else
                               iError := ERR_NO_SENTINEL;
                        except
                              on Error: Exception do
                              begin
                                   FInformation := Error.Message;
                                   iError := ERR_SENTINEL;
                              end;
                        end;
                   end
                   else
                       iError := ERR_NO_COMPARTE;

                   with FAutorizacion do
                   begin
                       OnSetData := nil;
                       OnGetDataAdditional := nil;
                   end;
                finally
                       FreeAndNil( oZetaProvider );
                       FreeAndNil( oLicManager );
                end;
           end
           else
               iError := ERR_NO_CONFIG;
        finally
               FreeAndNil( FRegistry );
        end;
        Result := ( iError = ERR_OK );
        Log( MensajeLOG( iError ) );
     finally
            FreeAndNil( FAutorizacion );
     end;
end;
{$else}
function TdmSentinelWrite.Inicializa( Log: TSentinelLog ): Boolean;
const
     ERR_OK = 0;
     ERR_SENTINEL = 1;
     ERR_NO_CONFIG = 2;
     ERR_NO_COMPARTE = 3;
     ERR_NO_SENTINEL = 4;
var
   iError: Byte;
   FRegistry: TZetaRegistryServer;
   FAutorizacion: TAutoServer;

   function MensajeLOG( const iError: Byte ): String;
   const
        CR_LF = Chr( 13 ) + Chr( 10 );
   begin
        case iError of
             ERR_OK: Result := 'OK: Sentinel Activado Con Exito';
             ERR_SENTINEL: Result := 'ERROR: La Computadora No Tiene El Sentinel, Pero Este Ya Fu� Activado Para El D�a De Hoy';
             ERR_NO_CONFIG: Result := 'ERROR: No Est� Configurado El Cliente De Tress Windows En Esta Computadora';
             ERR_NO_COMPARTE: Result := 'ERROR: Al Abrir Datos De Comparte';
             ERR_NO_SENTINEL: Result := 'ERROR: No Se Encontr� Sentinel';
        end;
        Result := CR_LF + Result + CR_LF + 'STATUS Sentinel: ' + FAutorizacion.StatusMsg + CR_LF + FInformation;
   end;

begin
     FInformation := '';
     FAutorizacion := TAutoServer.Create;

     try
        with FAutorizacion do
        begin
             OnSetData := AutorizacionSetData;
        end;
        FRegistry := TZetaRegistryServer.Create;
        try
           if ZetaCommonTools.StrLleno( FRegistry.Database ) and ZetaCommonTools.StrLleno( FRegistry.UserName ) and ZetaCommonTools.StrLleno( FRegistry.Password ) then
           begin
                try
                   if AbreComparte( FRegistry.Database, FRegistry.UserName, FRegistry.Password ) then
                   begin
                        try
                           if FAutorizacion.EscribirAutorizacion then
                              iError := ERR_OK
                           else
                               iError := ERR_NO_SENTINEL;
                        except
                              on Error: Exception do
                              begin
                                   FInformation := Error.Message;
                                   iError := ERR_SENTINEL;
                              end;
                        end;
                   end
                   else
                       iError := ERR_NO_COMPARTE;
                finally
                       CierraComparte;
                end;
           end
           else
               iError := ERR_NO_CONFIG;
        finally
               FreeAndNil( FRegistry );
        end;
        Result := ( iError = ERR_OK );
        Log( MensajeLOG( iError ) );
     finally
            FreeAndNil( FAutorizacion );
     end;
end;
{$endif}

end.
