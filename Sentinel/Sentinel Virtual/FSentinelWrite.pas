unit FSentinelWrite;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Checklst, Mask,
     FAutoServer,
     FAutoClasses,
     ZetaNumero,
     ZetaDBTextBox, ComCtrls, ZetaWinAPITools, ZetaEdit;

type
  TSentinelWrite = class(TForm)
    PageControl: TPageControl;
    tabSentinel: TTabSheet;
    tabServicio: TTabSheet;
    UsuariosLBL: TLabel;
    EmpleadosLBL: TLabel;
    ModulosLBL: TLabel;
    Clave1LBL: TLabel;
    Clave2LBL: TLabel;
    RegistrarAuto: TBitBtn;
    Clave2: TEdit;
    Clave1: TEdit;
    Usuarios: TZetaNumero;
    Empleados: TZetaNumero;
    Modulos: TZetaNumero;
    PanelBotones: TPanel;
    Cancelar: TBitBtn;
    DependenciaLBL: TLabel;
    Dependencia: TEdit;
    Escribir: TBitBtn;
    ReadOnlyLBL: TLabel;
    lblMensajes: TLabel;
    Label1: TLabel;
    lblNombreServer: TLabel;
    tabLicencia: TTabSheet;
    Label2: TLabel;
    ArchivoLicencia: TZetaEdit;
    btnBuscaArchivo: TSpeedButton;
    OpenDialog: TOpenDialog;
    btnEscribirArchivoLicencia: TBitBtn;
    PlataformaVirtual: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure RegistrarAutoClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure EscribirClick(Sender: TObject);
    procedure btnBuscaArchivoClick(Sender: TObject);
    procedure btnEscribirArchivoLicenciaClick(Sender: TObject);
    procedure ArchivoLicenciaChange(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
    procedure CargaDatosSentinel;
  protected
    { Protected declarations }
    procedure Mensajero( const sMensaje: String );
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
  end;

var
  SentinelWrite: TSentinelWrite;

implementation

uses ZetaCommonClasses,
     ZetaDialogo,
     FSentinelRegistry,
     ZetaClientTools,
     ZetaCommonTools;

{$R *.DFM}

procedure TSentinelWrite.FormShow(Sender: TObject);
begin
     CargaDatosSentinel;
end;

procedure TSentinelWrite.Mensajero( const sMensaje: String );
begin
     ShowMessage( sMensaje );
end;

procedure TSentinelWrite.RegistrarAutoClick(Sender: TObject);
const
     CR_LF = Chr( 13 ) + Chr( 10 );
var
   lOk, lEnabled: Boolean;
   oCursor: TCursor;
   FRegistry: TSentinelRegistryServer;
   oSentinelServer: TSentinelServer;
   sClave1, sClave2: String;
begin
     lEnabled := RegistrarAuto.Enabled;
     lOk := False;
     RegistrarAuto.Enabled := False;
     sClave1 := Self.Clave1.Text;
     sClave2 := Self.Clave2.Text;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        oSentinelServer := TSentinelServer.Create;
        try
           with oSentinelServer do
           begin
                SentinelLoad( Self.Empleados.ValorEntero, Self.Usuarios.ValorEntero, Self.Modulos.ValorEntero,TPlataforma(PlataformaVirtual.ItemIndex));
                try
                   lOk := SentinelTest( sClave1, sClave2 );
                   if not lOk then
                   begin
                        ZetaDialogo.ZError( 'ˇ Error !', StatusMsg, 0 );
                   end
                except
                      on Error: Exception do
                      begin
                           ZetaDialogo.ZExcepcion( 'ˇ Error Grave !', 'Error en Claves', Error, 0 );
                      end;
                end;
                if lOk then
                begin
                     FRegistry := TSentinelRegistryServer.Create( True );
                     try
                        try
                           FRegistry.Usuarios := Usuarios;
                           FRegistry.Empleados := Empleados;
                           FRegistry.Modulos := Modulos;
                           FRegistry.Clave1 := sClave1;
                           FRegistry.Clave2 := sClave2;
                           FRegistry.Plataforma := TPlataforma(PlataformaVirtual.ItemIndex );
                           SentinelWrite;
                           ZetaDialogo.ZInformation( 'ˇ Exito !', 'La autorización ha sido registrada', 0 );
                        except
                              on Error: Exception do
                              begin
                                   ZetaDialogo.ZExcepcion( 'ˇ Error !', 'Error al registrar autorización', Error, 0 );
                              end;
                        end;
                     finally
                            FreeAndNil( FRegistry );
                     end;
                end;
           end;
        finally
               FreeAndNil( oSentinelServer );
        end;
     finally
            RegistrarAuto.Enabled := lEnabled;
            Screen.Cursor := oCursor;
     end;
end;

procedure TSentinelWrite.EscribirClick(Sender: TObject);
var
   oCursor: TCursor;
   FRegistry: TSentinelRegistryServer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        FRegistry := TSentinelRegistryServer.Create( True );
        try
           try
              FRegistry.SentinelServerDependency := Dependencia.Text;
              ZetaDialogo.ZInformation( 'ˇ Exito !', 'Los datos del Servicio fueron escritos', 0 );
           except
                 on Error: Exception do
                 begin
                      ZetaDialogo.ZExcepcion( 'ˇ Error !', 'Error al escribir datos del Servicio', Error, 0 );
                 end;
           end;
        finally
               FreeAndNil( FRegistry );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TSentinelWrite.CancelarClick(Sender: TObject);
begin
     Close;
end;

procedure TSentinelWrite.btnBuscaArchivoClick(Sender: TObject);
begin
     Self.ArchivoLicencia.Text := ZetaClientTools.AbreDialogo( OpenDialog, Self.ArchivoLicencia.Text, 'Lic' );
end;

procedure TSentinelWrite.btnEscribirArchivoLicenciaClick(Sender: TObject);
var
   oCursor: TCursor;
   FRegistry: TSentinelRegistryServer;
begin
     // Si existe el archivo especificado.
     if FileExists( Self.ArchivoLicencia.Text )then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;

          try
             FRegistry := TSentinelRegistryServer.Create( True );
             try
                try
                   FRegistry.ArchivoLicencia := Self.ArchivoLicencia.Text;
                   Self.btnEscribirArchivoLicencia.Enabled := False;
                   ZetaDialogo.ZInformation( 'ˇ Exito !', 'Los datos del Archivo Licencia fueron escritos', 0 );
                   CargaDatosSentinel;                   
                except
                      on Error: Exception do
                      begin
                           ZetaDialogo.ZExcepcion( 'ˇ Error !', 'Error al escribir datos del Archivo Licencia', Error, 0 );
                      end;
                end;
             finally
                    FreeAndNil( FRegistry );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end
     else
         ZetaDialogo.ZError( 'ˇ Error !','El archivo especificado no existe', 0 );
end;

procedure TSentinelWrite.ArchivoLicenciaChange(Sender: TObject);
begin
     Self.btnEscribirArchivoLicencia.Enabled := StrLleno( Self.ArchivoLicencia.Text );
end;

procedure TSentinelWrite.CargaDatosSentinel;
var
   FRegistry: TSentinelRegistryServer;
   lCanWrite: Boolean;
begin
     // Inicializa valores.
     lblMensajes.Visible := False;
     Self.Usuarios.Valor := 0;
     Self.Empleados.Valor := 0;
     Self.Modulos.Valor := 0;
     Self.Clave1.Text := VACIO;
     Self.Clave2.Text := VACIO;
     Self.Dependencia.Text := VACIO;
     lCanWrite := false;

     LoadPlataformas(PlataformaVirtual.Items  ); 
     
     FRegistry := TSentinelRegistryServer.Create( True );
     try
        if FSentinelRegistry.CheckComputerName then
        begin
             Self.Usuarios.Valor := FRegistry.Usuarios;
             Self.Empleados.Valor := FRegistry.Empleados;
             Self.Modulos.Valor := FRegistry.Modulos;
             Self.Clave1.Text := FRegistry.Clave1;
             Self.Clave2.Text := FRegistry.Clave2;
             Self.Dependencia.Text := FRegistry.SentinelServerDependency;
             lCanWrite := FRegistry.CanWrite;
        end
        else
        begin
             with Self.ReadOnlyLBL do
             begin
                  Caption := 'Servidor No Autorizado';
                  Visible := True;
             end;
        end;
        if FileExists( FRegistry.ArchivoLicencia ) then
           PageControl.ActivePageIndex := 0
        else
        begin
             ZetaDialogo.ZError( 'ˇ Error !','No se encontró el archivo licencia de Sentinel Virtual', 0 );
             PageControl.ActivePageIndex := 2;
             ArchivoLicencia.SetFocus;
        end;

        Self.RegistrarAuto.Enabled := lCanWrite;
        Self.Escribir.Enabled := lCanWrite;
        Self.UsuariosLBL.Enabled := lCanWrite;
        Self.Usuarios.Enabled := lCanWrite;
        Self.EmpleadosLBL.Enabled := lCanWrite;
        Self.Empleados.Enabled := lCanWrite;
        Self.ModulosLBL.Enabled := lCanWrite;
        Self.Modulos.Enabled := lCanWrite;
        Self.Clave1LBL.Enabled := lCanWrite;
        Self.Clave1.Enabled := lCanWrite;
        Self.Clave2LBL.Enabled := lCanWrite;
        Self.Clave2.Enabled := lCanWrite;
        Self.DependenciaLBL.Enabled := lCanWrite;
        Self.Dependencia.Enabled := lCanWrite;
        Self.ReadOnlyLBL.Visible := not lCanWrite;

        lblNombreServer.Caption := ZetaWinAPITools.GetComputerName;
        Self.ArchivoLicencia.Text := FRegistry.ArchivoLicencia;
        Self.btnEscribirArchivoLicencia.Enabled := False;

     finally
            FreeAndNil( FRegistry );
     end;
end;

end.
