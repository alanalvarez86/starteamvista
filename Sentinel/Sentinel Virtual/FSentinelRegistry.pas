unit FSentinelRegistry;

interface

uses Windows, Messages, SysUtils, Classes,
     FAutoServer,
     FAutoClasses,
     ZetaRegistryServer;

type
  TSentinelLog = procedure(const sMensaje: String) of object;
  TSentinelServer = class( TAutoServer )
  private
    { Private declarations }
    procedure SentinelInit;
  public
    { Public declarations }
    function SentinelTest( const sClave1, sClave2: String ): Boolean;
    function SentinelWrite: Boolean;
    procedure SentinelLoad( const iEmpleados, iUsuarios, iModulos: Integer;PlataformaVirt:TPlataforma );
  end;
  TSentinelRegistryServer = class( TZetaRegistryServer )
  private
    { Private declarations }
    function GetSentinelServerDependency: string;
    procedure SetSentinelServerDependency(const Value: string);
    function GetClave1: String;
    function GetClave2: String;
    function GetEmpleados: Integer;
    function GetModulos: Integer;
    function GetPlataforma: TPlataforma;
    function GetUsuarios: Integer;
    function GetArchivoLicencia: String;
    procedure SetClave1(const Value: String);
    procedure SetClave2(const Value: String);
    procedure SetEmpleados(const Value: Integer);
    procedure SetModulos(const Value: Integer);
    procedure SetPlataforma(const Value: TPlataforma);
    procedure SetUsuarios(const Value: Integer);
    procedure SetArchivoLicencia( const Value: String );
  public
    { Public declarations }
    property SentinelServerDependency: string read GetSentinelServerDependency write SetSentinelServerDependency;
    property Usuarios: Integer read GetUsuarios write SetUsuarios;
    property Empleados: Integer read GetEmpleados write SetEmpleados;
    property Modulos: Integer read GetModulos write SetModulos;
    property Plataforma:TPlataforma read GetPlataforma write SetPlataforma;
    property Clave1: String read GetClave1 write SetClave1;
    property Clave2: String read GetClave2 write SetClave2;
    property ArchivoLicencia: String read GetArchivoLicencia write SetArchivoLicencia;
  end;
  TdmSentinelWrite = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function Inicializa( EventLog, ErrorLog: TSentinelLog ): Boolean;
  end;

function RenovarAutorizacion( EventLog, ErrorLog: TSentinelLog ): Boolean;
function CheckComputerName: Boolean;
procedure ObtenDatosServidor;

implementation

uses ZetaCommonClasses,
     ZetaLicenseMgr,
     ZetaWinAPITools,
     DZetaServerProvider,
     ZetaServerTools;

const
     SENTINEL_SECTION = 'Sentinel';
     SENTINEL_SERVER_DEPENDENCY = 'SentinelServerDependency';
     SENTINEL_EMPLEADOS = 'SentinelEmpleados';
     SENTINEL_USUARIOS = 'SentinelUsuarios';
     SENTINEL_MODULOS = 'SentinelModulos';
     SENTINEL_PLATAFORMA = 'SentinelPlataforma';
     SENTINEL_CLAVE1 = 'SentinelClave1';
     SENTINEL_CLAVE2 = 'SentinelClave2';
     SENTINEL_ARCHIVOLICENCIA = 'SentinelArchivoLicencia';

var
    FServidor: string;
    FNumSentinel: integer;
    FEmpresa: integer;

procedure ObtenDatosServidor;
var
   oArchivo: TStrings;
   sLinea: string;
   FRegistry: TSentinelRegistryServer;
begin
     oArchivo := TStringList.Create;
     FRegistry := TSentinelRegistryServer.Create( True );
     FServidor := VACIO;
     FNumSentinel := 0;
     FEmpresa := 0;
     try
        if FileExists( FRegistry.ArchivoLicencia )then
        begin
             oArchivo.Delimiter := ',';
             oArchivo.LoadFRomFile( FRegistry.ArchivoLicencia );
             if( oArchivo.Count > 0 )then
             begin
                  sLinea := Decrypt( oArchivo.Strings[ 0 ] );
                  oArchivo.Delimiter := '|';
                  oArchivo.DelimitedText := sLinea;
                  if( oArchivo.Count > 3 )then
                  begin
                       FServidor := oArchivo.Strings[0];
                       FNumSentinel := StrToIntDef( oArchivo.Strings[1], 0 );
                       FEmpresa := StrToIntDef( oArchivo.Strings[2], 0 );
                  end;
             end;
        end;
     finally
            FreeAndNil( FRegistry );
            FreeAndNil(oArchivo);
     end;
end;

function CheckComputerName: Boolean;
begin
     ObtenDatosServidor;
     Result := ( AnsiUpperCase( ZetaWinAPITools.GetComputerName ) = AnsiUpperCase( FServidor ) );
end;

function RenovarAutorizacion( EventLog, ErrorLog: TSentinelLog ): Boolean;
var
   FWriter: TdmSentinelWrite;
begin
     FWriter := TdmSentinelWrite.Create;
     try
        Result := FWriter.Inicializa( EventLog, ErrorLog );
     finally
            FreeAndNil( FWriter );
     end;
end;

{ ******** TCafeRegistryServer ********* }

function TSentinelRegistryServer.GetClave1: String;
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_CLAVE1, '' );
end;

function TSentinelRegistryServer.GetClave2: String;
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_CLAVE2, '' );
end;

function TSentinelRegistryServer.GetEmpleados: Integer;
begin
     Result := Registry.ReadInteger( SENTINEL_SECTION, SENTINEL_EMPLEADOS, 0 );
end;

function TSentinelRegistryServer.GetModulos: Integer;
begin
     Result := Registry.ReadInteger( SENTINEL_SECTION, SENTINEL_MODULOS, 0 );
end;

function TSentinelRegistryServer.GetPlataforma: TPlataforma;
begin
     Result := TPlataforma(Registry.ReadInteger( SENTINEL_SECTION, SENTINEL_PLATAFORMA, 0 ));
end;


function TSentinelRegistryServer.GetSentinelServerDependency: string;
const
     DEFAULT_DEPENDENCY = ''; //'MSSQLSERVER';
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_SERVER_DEPENDENCY, DEFAULT_DEPENDENCY );
end;

function TSentinelRegistryServer.GetUsuarios: Integer;
begin
     Result := Registry.ReadInteger( SENTINEL_SECTION, SENTINEL_USUARIOS, 0 );
end;

function TSentinelRegistryServer.GetArchivoLicencia: String;
begin
     Result := Registry.ReadString( SENTINEL_SECTION, SENTINEL_ARCHIVOLICENCIA, '' );
end;

procedure TSentinelRegistryServer.SetClave1(const Value: String);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_CLAVE1, Value );
end;

procedure TSentinelRegistryServer.SetClave2(const Value: String);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_CLAVE2, Value );
end;

procedure TSentinelRegistryServer.SetEmpleados(const Value: Integer);
begin
     Registry.WriteInteger( SENTINEL_SECTION, SENTINEL_EMPLEADOS, Value );
end;

procedure TSentinelRegistryServer.SetModulos(const Value: Integer);
begin
     Registry.WriteInteger( SENTINEL_SECTION, SENTINEL_MODULOS, Value );
end;

procedure TSentinelRegistryServer.SetSentinelServerDependency(const Value: string);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_SERVER_DEPENDENCY, Value );
end;

procedure TSentinelRegistryServer.SetUsuarios(const Value: Integer);
begin
     Registry.WriteInteger( SENTINEL_SECTION, SENTINEL_USUARIOS, Value );
end;

procedure TSentinelRegistryServer.SetArchivoLicencia(const Value: String);
begin
     Registry.WriteString( SENTINEL_SECTION, SENTINEL_ARCHIVOLICENCIA, Value );
end;

procedure TSentinelRegistryServer.SetPlataforma(const Value: TPlataforma);
begin
     Registry.WriteInteger( SENTINEL_SECTION, SENTINEL_PLATAFORMA, Ord(Value) );
end;

{ ********* TSentinelServer ********* }

procedure TSentinelServer.SentinelInit;
begin
     FAutoInfo.NumeroSerie := FNumSentinel;
     Empresa := FEmpresa;
     SQLType := engMSSQL;
     SQLEngine := engMSSQL;
     AppType := atCorporativa;
     Plataforma := ptCorporativa;
     Version := FAutoClasses.UnloadVersion( 0 );
end;

procedure TSentinelServer.SentinelLoad(const iEmpleados, iUsuarios, iModulos: Integer;PlataformaVirt:TPlataforma);
begin
     SentinelInit;
     Plataforma := PlataformaVirt;
     Empleados := iEmpleados;
     Usuarios := iUsuarios;
     Modulos := iModulos;
end;

function TSentinelServer.SentinelTest(const sClave1, sClave2: String): Boolean;
begin
     Result := ActualizarTest( sClave1, sClave2, FAutoInfo.NumeroSerie );
end;

function TSentinelServer.SentinelWrite: Boolean;
var
   oZetaProvider: TdmZetaServerProvider;
   oLicenseMgr: TLicenseMgr;
   sTempData: String;
begin
     oZetaProvider := TdmZetaServerProvider.Create( nil );
     try
        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
        try
           AutoInfoPrepare( False );
           with FSentinel do
           begin
                CargarDatos( FAutoInfo );
                sTempData := GetDataString;
           end;
           oLicenseMgr.AutoSetData( Self, sTempData );
           Result := True;
        finally
               FreeAndNil( oLicenseMgr );
        end;
     finally
            FreeAndNil( oZetaProvider );
     end;
end;

{ ********** TdmSentinelWrite ******** }

function TdmSentinelWrite.Inicializa( EventLog, ErrorLog: TSentinelLog ): Boolean;
var
   oSentinelServer: TSentinelServer;
   FRegistry: TSentinelRegistryServer;
   sClave1, sClave2: String;
begin
     Result := False;
     try
        oSentinelServer := TSentinelServer.Create;
        try
           with oSentinelServer do
           begin
                FRegistry := TSentinelRegistryServer.Create( False );
                try
                   sClave1 := VACIO;
                   sClave2 := VACIO;
                   if FileExists( FRegistry.ArchivoLicencia )then
                   begin
                        SentinelLoad( FRegistry.Empleados, FRegistry.Usuarios, FRegistry.Modulos,FRegistry.Plataforma   );
                        sClave1 := FRegistry.Clave1;
                        sClave2 := FRegistry.Clave2;
                   end
                   else
                       ErrorLog( Format( 'No se encontró archivo licencia de Sentinel Virtual %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) );
                finally
                       FreeAndNil( FRegistry );
                end;
                if SentinelTest( sClave1, sClave2 ) then
                begin
                     if SentinelWrite then
                     begin
                          EventLog( Format( 'Autorización Renovada %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) );
                          Result := True;
                     end
                     else
                     begin
                          ErrorLog( Format( 'Autorización NO FUE Renovada %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) );
                     end;
                end
                else
                begin
                     ErrorLog( 'Error En Autorización: ' + StatusMsg );
                end;
           end;
        finally
               FreeAndNil( oSentinelServer );
        end;
     except
           on Error: Exception do
           begin
                ErrorLog( 'Error Al Autorizar: ' + Error.Message );
           end;
     end;
end;

end.
