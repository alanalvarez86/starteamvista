program Sentinel3s;


{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}


{$R *.dres}

uses
  SvcMgr,
  DSentinel in 'DSentinel.pas' {Sentinel3Service: TService},
  FSentinelRegistry in '..\..\Tools\FSentinelRegistry.pas',
  FExtraerBuild in 'FExtraerBuild.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Sentinel Corporativo';
  Application.CreateForm(TSentinel3Service, Sentinel3Service);
  Application.Run;
end.
