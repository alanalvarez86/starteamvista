unit DSentinel;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DSentinel.pas                              ::
  :: Descripci�n: Programa principal de Sentinel3s.exe       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, SvcMgr, ActiveX;

type
  TSentinel3Service = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure EnviarEmailAdvertencia;
    procedure EnviarEmailNotificacionPendienteTimbrado;
  private
    { Private declarations }
    FFecha: TDateTime;
    FIniciando: Integer;
    function FormatMessage(const sMessage: String): String;
    function Inicializa: Boolean;
    procedure EscribeError(const sMensaje: String);
    procedure EscribeBitacora(const sMensaje: String);
  public
    { Public declarations }
    function GetServiceController: TServiceController; override;
  end;

var
  Sentinel3Service: TSentinel3Service;

implementation

{$R *.DFM}

uses FSentinelRegistry,
     FExtraerBuild,
     FEnviarEmailGrupUsuarios,
     FEnviarEmailNotificacionPendienteTimbrado,
     ZetaCommonTools;

procedure ServiceController( CtrlCode: DWord ); stdcall;
begin
     Sentinel3Service.Controller( CtrlCode );
end;

function TSentinel3Service.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;

{ ******* TSentinel ******** }

procedure TSentinel3Service.ServiceCreate(Sender: TObject);
var
   sDependecy: String;
   FRegistry: TSentinelRegistryServer;
begin
     FFecha := 0;
     FIniciando := 10;
     {$ifdef MSSQL}
     FRegistry := TSentinelRegistryServer.Create( False );
     try
        sDependecy := FRegistry.SentinelServerDependency;
     finally
            FreeAndNil( FRegistry );
     end;
     if ZetaCommonTools.StrLleno( sDependecy ) then
     begin
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := sDependecy;
          end;
     end;
     {$endif}
end;

procedure TSentinel3Service.ServiceStart(Sender: TService; var Started: Boolean);
begin
     CoInitialize(nil);
     FFecha := 0;
     FIniciando := 10;
     if FSentinelRegistry.CheckComputerInfo then //@(am): Cambio validacion licencia
     begin
          Started := True;
     end
     else
     begin
          Started := False;
          EscribeError( 'No puede arrancar en esta computadora' );
     end;
end;

procedure TSentinel3Service.ServiceExecute(Sender: TService);
const
     K_UN_SEGUNDO = 1000;
     K_MEDIO_SEGUNDO = 500;
     VACIO = '';
var
     sError : string;
begin
     while not Terminated do
     begin
          if ( FFecha <> Date ) then
          begin
               if not Inicializa and ( FIniciando > 0 ) then
               begin
                    Sleep( 28 * K_UN_SEGUNDO );
                    FIniciando := FIniciando - 1;
               end
               else
                   FFecha := Date;
                   sError := FExtraerBuild.ProcesarPropiedadesArchivosTress;
                   if (sError <> VACIO) then
                   begin
                        EscribeError(sError);
                   end;
                   EnviarEmailAdvertencia;
                   EnviarEmailNotificacionPendienteTimbrado;
          end;
          Sleep( 1 * K_MEDIO_SEGUNDO );
          ServiceThread.ProcessRequests( False );
     end;
end;

procedure TSentinel3Service.EnviarEmailAdvertencia;
var
   EnviarEmail : TEnviarEmailGrupUsuarios;
   lEnviarEmail: Boolean;
begin
     try
        EnviarEmail := TEnviarEmailGrupUsuarios.Create;
        lEnviarEmail := EnviarEmail.AdvertenciaPorEmail( Date, EscribeBitacora, EscribeError );
        if ( lEnviarEmail ) then
           EnviarEmail.EnviarEmail(EscribeBitacora, EscribeError);
     finally
            FreeAndNil( EnviarEmail );
     end;
end;

procedure TSentinel3Service.EnviarEmailNotificacionPendienteTimbrado;
var
   EnviarNotificacionTimbrado : TEnviarEmailNotificacionPendienteTimbrado;
begin
     try
        EnviarNotificacionTimbrado := TEnviarEmailNotificacionPendienteTimbrado.Create;
        EnviarNotificacionTimbrado.EnviarNotificacionTimbrado(EscribeBitacora, EscribeError);
     finally
            FreeAndNil( EnviarNotificacionTimbrado );
     end;
end;

function TSentinel3Service.FormatMessage(const sMessage: String): String;
begin
     Result := Format( '***   %s   ***', [ sMessage ] );
end;

procedure TSentinel3Service.EscribeBitacora(const sMensaje: String);
begin
     try
        LogMessage( FormatMessage( sMensaje ), EVENTLOG_INFORMATION_TYPE, 0, 0 );
     except
           LogMessage( FormatMessage( 'Error al Escribir a Bit�cora' ), EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

procedure TSentinel3Service.EscribeError(const sMensaje: String);
begin
     try
        LogMessage( FormatMessage( sMensaje ), EVENTLOG_ERROR_TYPE, 0, 0 );
     except
           LogMessage( FormatMessage( 'Error al Escribir a Bit�cora' ), EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

function TSentinel3Service.Inicializa: Boolean;
begin
     Result := FSentinelRegistry.RenovarAutorizacion( EscribeBitacora, EscribeError );
end;

end.
