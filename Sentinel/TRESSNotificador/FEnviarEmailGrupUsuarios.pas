unit FEnviarEmailGrupUsuarios;

interface

uses Windows, SysUtils, Classes, Registry, ADODB, Db,
     FAutoServer,
     FAutoClasses,
     FSentinelRegistry,
     ZetaRegistryServer,
     ZetaCommonLists,
     DEmailService,
     ZCreator,
     ZetaSQLBroker,
     DZetaServerProvider,
     ZetaLicenseMgr,
     ZetaLicenseClasses,
     Contnrs,
     HTTPApp;

type
  TEnviarEmailGrupUsuarios = class( TObject )
  private
  { Private declarations }
   FHost: String;
   FPort:Integer;
   FAuthMethod: eAuthTressEmail;
   FPasword:String;
   FUserID: String;
   FFromAddress: String;
   FFromName: String;
   FLicenseMgr: TLicenseMgr;
   oZetaCreator: TZetaCreator;
   oZetaProvider: TdmZetaServerProvider;
   dmEmailService : TdmEmailService;
   FPlantillaHTML: TStringList;
   FEnvioEmailOnce: Boolean; //Indica si al menos un corro se envio
   function ConfiguracionServerEmail( EventLog, ErrorLog: TSentinelLog ): Boolean;
   function GetUsuarios: TObjectList;
   procedure SendEmail( DatosUsuarioEmail: TDatosUsuarioEmail; sMensajeAdvertencia, sMensajeError: String; EventLog, ErrorLog: TSentinelLog );
   function ConectaHost: Boolean;
   function RecibirAdvertenciaPorEmail: Boolean;
   function ObtenerSubJect(sMensajeAdvertencia, sMensajeError: String): String;
   function ObtenerMensaje(sMensajeAdvertencia, sMensajeError: String): UTF8String;
   procedure ObtenerMensajeHTML( DatosUsuarioEmail: TDatosUsuarioEmail; sMensajeAdvertencia, sMensajeError: String; EventLog, ErrorLog: TSentinelLog );
   function EnviarAdvertenciaoRestriccion( Date: TDateTime; EventLog, ErrorLog: TSentinelLog ): Boolean;
   procedure EscribirRegistry( EventLog, ErrorLog: TSentinelLog; lAdvertencia: Boolean );
   function LeerRegistry( EventLog, ErrorLog: TSentinelLog; lAdvertencia: Boolean ): TDateTime;
  public
  { Public declarations }
   function EnviarEmail( EventLog, ErrorLog: TSentinelLog ): Boolean;
   function ValidarLicencia( var sMsgAdvertencia, sMsgError : string  ) : eEvaluaTotalEmpleados  ;
   function AdvertenciaPorEmail( Date: TDateTime; EventLog, ErrorLog: TSentinelLog ): Boolean;
   constructor Create;
   Destructor  Destroy; override;
end;
var
   EnviarEmailGrupUsuarios  : TEnviarEmailGrupUsuarios;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaWinAPITools,
     ZetaServerTools;

{ TEnviarEmailGrupUsuarios }

{ TEnviarEmailGrupUsuarios }

function TEnviarEmailGrupUsuarios.ConfiguracionServerEmail( EventLog, ErrorLog: TSentinelLog ): Boolean;
const
     K_ARROBA = '@';
begin
     FHost := FLicenseMgr.GetGlobalServidorCorreos;
     FPort := FLicenseMgr.GetGlobalPuertoSMTP;
     FAuthMethod :=  eAuthTressEmail( FLicenseMgr.GetGlobalAutentificacionCorreo - 1 );
     Result := ZetaCommonTools.StrLleno( FHost );
     if not Result then
        ErrorLog( 'Servidor de correos No Configurados' + CR_LF + 'Favor De Indicar El Servidor De Correos' );
     FUserID := FLicenseMgr.GetGlobalUserID;
     FPasword := ZetaServerTools.Decrypt( FLicenseMgr.GetGlobalEmailPSWD );
     if ValidarEmail ( FUserID ) then
        FFromAddress := FUserID
     else
         FFromAddress := FUserID + K_ARROBA + FHost;
     FFromName := VACIO;
end;

function TEnviarEmailGrupUsuarios.RecibirAdvertenciaPorEmail: Boolean;
begin
     Result := not FLicenseMgr.GetGlobalRecibirAdvertenciaPorEmail;
end;

function TEnviarEmailGrupUsuarios.EnviarAdvertenciaoRestriccion( Date: TDateTime; EventLog, ErrorLog: TSentinelLog ): Boolean;
var
   dFehcaAdvertencia: TDateTime;
   dFechaRestriccion: TDateTime;
   iDiasTranscurridos: Integer;
   sMensajeAdvertencia, sMensajeError: String;
   evaluacion : eEvaluaTotalEmpleados;
begin
     Result := FALSE;
     evaluacion := Self.ValidarLicencia(sMensajeAdvertencia, sMensajeError);
     if ( evaluacion >= evEnUmbral ) then
     begin
          if not StrVacio( sMensajeAdvertencia ) then
          begin
               dFehcaAdvertencia := LeerRegistry(EventLog, ErrorLog,TRUE);//LeerAdvertencia
               iDiasTranscurridos := ZetaCommonTools.DaysBetween(dFehcaAdvertencia, Date) - 1;
               if iDiasTranscurridos >= 3 then
                  Result := TRUE;
          end
          else
          begin
               dFechaRestriccion := LeerRegistry(EventLog, ErrorLog,FALSE);//LeerRestriccion
               iDiasTranscurridos := ZetaCommonTools.DaysBetween(dFechaRestriccion, Date) - 1;
               if iDiasTranscurridos >= 3 then
                  Result := TRUE;
          end;
     end;
end;

function TEnviarEmailGrupUsuarios.AdvertenciaPorEmail( Date: TDateTime; EventLog, ErrorLog: TSentinelLog ): Boolean;
begin
     Result := RecibirAdvertenciaPorEmail AND EnviarAdvertenciaoRestriccion( Date, EventLog, ErrorLog );
end;

constructor TEnviarEmailGrupUsuarios.Create;
begin
     oZetaProvider := TdmZetaServerProvider.Create( nil );
     FLicenseMgr := TLicenseMgr.Create( oZetaProvider );
     dmEmailService := TdmEmailService.Create( nil );
     oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
     FPlantillaHTML := TStringList.Create;
     FEnvioEmailOnce := FALSE;
end;

destructor TEnviarEmailGrupUsuarios.Destroy;//Libera Memoria de todos los componentes que intervienen
begin
     FreeAndNil( dmEmailService );
     FreeAndNil( FLicenseMgr );
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     FreeAndNil(FPlantillaHTML);
     inherited;
end;

function TEnviarEmailGrupUsuarios.ValidarLicencia( var sMsgAdvertencia, sMsgError : string  ) : eEvaluaTotalEmpleados  ;
var
   oAutoServer : TAutoServer;
   oGlobalLicense : TGlobalLicenseValues;
begin
     oAutoServer := ZetaSQLBroker.GetAutoServer;
     try
        ZetaSQLBroker.ReadAutoServer( oAutoServer, FLicenseMgr.AutoGetData, FLicenseMgr.AutoSetData );
        oGlobalLicense := TGlobalLicenseValues.Create( oAutoServer );
        oGlobalLicense.TotalGlobal := oAutoServer.EmpleadosConteo;
        Result :=  oGlobalLicense.EvaluarEmpleadosMensajePantallaBD( sMsgAdvertencia, sMsgError );
     finally
            FreeAndNil( oAutoServer );
     end;
end;

function TEnviarEmailGrupUsuarios.EnviarEmail(EventLog, ErrorLog: TSentinelLog): Boolean;
var
   sMensajeAdvertencia, sMensajeError: String;
   evaluacion : eEvaluaTotalEmpleados;
   DatosUsuarioEmail : TDatosUsuarioEmail;
   ListaDatosUsuarioEmail: TObjectList;
   i: Integer;
   lAdvertencia: Boolean;
begin
     evaluacion := Self.ValidarLicencia(sMensajeAdvertencia, sMensajeError);
     if ( evaluacion >= evEnUmbral ) then
     begin
          //********************* ENVIAR EMAIL **********************/
          Result := ConfiguracionServerEmail( EventLog, ErrorLog );
          if Result then
          begin
               ListaDatosUsuarioEmail := GetUsuarios;
               for i := 0 to ( ListaDatosUsuarioEmail.Count - 1 ) do
               begin
                    DatosUsuarioEmail := TDatosUsuarioEmail(ListaDatosUsuarioEmail[i]);
                    SendEmail( DatosUsuarioEmail, sMensajeAdvertencia, sMensajeError, EventLog, ErrorLog );
                    if FEnvioEmailOnce then
                    begin
                         lAdvertencia := not StrVacio(sMensajeAdvertencia);
                         EscribirRegistry( EventLog, ErrorLog, lAdvertencia);
                    end;
               end;
          end;
          //******************** FIN **************************/
     end;
end;

procedure TEnviarEmailGrupUsuarios.SendEmail( DatosUsuarioEmail: TDatosUsuarioEmail; sMensajeAdvertencia, sMensajeError: String; EventLog, ErrorLog: TSentinelLog );
var
   sTipo: String;
   sErrorMsg: String;
   sPlantillaHTML: TStringList;
begin
     if ( ValidarEmail( DatosUsuarioEmail.UsEmail ) ) then
     begin
          with dmEmailService do
          begin
               ConectaHost;
               SubType := emtHtml;
               Subject := ObtenerSubJect(sMensajeAdvertencia, sMensajeError);
               ToAddress.Add( DatosUsuarioEmail.UsEmail );
               ObtenerMensajeHTML( DatosUsuarioEmail, sMensajeAdvertencia, sMensajeError, EventLog, ErrorLog );
               MessageText.AddStrings(FPlantillaHTML);
               if Validate( sErrorMsg ) then
               begin
                    if SendEmail( sErrorMsg ) then
                    begin
                         EventLog( Format( 'Se envi� el correo al usuario:%s', [ DatosUsuarioEmail.UsNombre ] ) );
                         FEnvioEmailOnce := TRUE;
                    end
                    else
                        ErrorLog( 'Error al tratar de enviar el correo al usuario: ' + DatosUsuarioEmail.UsNombre + CR_LF + sErrorMsg );
               end
               else
                   ErrorLog( 'Error al enviar el correo al usuario: '+ DatosUsuarioEmail.UsNombre + CR_LF + sErrorMsg );
          end;
     end
     else
     begin
          ErrorLog ( Format( 'Error al tratar de enviar el correo al usuario:%s, correo invalido ', [ DatosUsuarioEmail.UsNombre ] ) );
     end;
end;

procedure TEnviarEmailGrupUsuarios.EscribirRegistry( EventLog, ErrorLog: TSentinelLog; lAdvertencia: Boolean );
var
   FRegistrySent: TSentinelRegistryServer;
begin
     FRegistrySent := TSentinelRegistryServer.Create( True );
     try
        try
           if lAdvertencia then
              FRegistrySent.SentinelAdvertenciaEnvio := DateToStrSQL( Date )
           else
               FRegistrySent.SentinelRestriccionEnvio := DateToStrSQL( Date );
        except
              on Error: Exception do
              begin
                   ErrorLog( 'Se encontr� un Error en el Registry' + CR_LF + Error.Message );
              end;
        end;
     finally
            FreeAndNil( FRegistrySent );
     end;
end;

function TEnviarEmailGrupUsuarios.LeerRegistry( EventLog, ErrorLog: TSentinelLog; lAdvertencia: Boolean ): TDateTime;
var
   FRegistrySent: TSentinelRegistryServer;
begin
     FRegistrySent := TSentinelRegistryServer.Create( True );
     try
        try
           if lAdvertencia then
              Result :=  StrAsDate( FRegistrySent.SentinelAdvertenciaEnvio )
           else
               Result := StrAsDate( FRegistrySent.SentinelRestriccionEnvio );
        except
              on Error: Exception do
              begin

              end;
        end;
     finally
            FreeAndNil( FRegistrySent );
     end;
end;

function TEnviarEmailGrupUsuarios.GetUsuarios: TObjectList;
begin
     Result := FLicenseMgr.GetUsuariosEmailSeguridad;
end;

function TEnviarEmailGrupUsuarios.ObtenerSubJect(sMensajeAdvertencia, sMensajeError: String): String;
const
     K_SUBJECT_ADVERTENCIA = 'Advertencia de Licencia de Uso de Sistema TRESS';
     K_SUBJECT_ADVERTENCIA_LIMITE = 'L�mite de Licencia de Uso de Sistema TRESS';
begin
     if not StrVacio(sMensajeAdvertencia) then
        Result := K_SUBJECT_ADVERTENCIA
     else
         Result := K_SUBJECT_ADVERTENCIA_LIMITE;
end;

function TEnviarEmailGrupUsuarios.ObtenerMensaje(sMensajeAdvertencia, sMensajeError: String): UTF8String;
const
     K_LINK_ADVERTENCIA = '<a href="http://www.tress.com.mx/esp/SOMOS/PRESENCIA/SUCURSALESDEGTI/tabid/102/Default.aspx">http://www.tress.com.mx/esp/SOMOS/PRESENCIA/SUCURSALESDEGTI/tabid/102/Default.aspx</a>';
     K_TEXTO_ADVERTENCIA = 'http://www.tress.com.mx/esp/SOMOS/PRESENCIA/SUCURSALESDEGTI/tabid/102/Default.aspx';
     K_LINK_RESTRICCION = '<a href="http://www.tress.com.mx/en-us/somos/cont%C3%A1ctanos.aspx">http://www.tress.com.mx/en-us/somos/cont%C3%A1ctanos.aspx</a>';
     K_TEXTO_RESTRICCION = 'http://www.tress.com.mx/en-us/somos/cont%C3%A1ctanos.aspx';
var
   sTextoAdvertencia: String;
   function QuitarAcentos( sTextoAdvertencia: String ): String;
   begin
        sTextoAdvertencia := StringReplace ( sTextoAdvertencia, '�', '&aacute;', [rfReplaceAll]);
        sTextoAdvertencia := StringReplace ( sTextoAdvertencia, '�', '&iacute;', [rfReplaceAll]);
        sTextoAdvertencia := StringReplace ( sTextoAdvertencia, '�', '&oacute;', [rfReplaceAll]);
        sTextoAdvertencia := StringReplace ( sTextoAdvertencia, K_TEXTO_ADVERTENCIA, K_LINK_ADVERTENCIA, [rfReplaceAll]);
        sTextoAdvertencia := StringReplace ( sTextoAdvertencia, K_TEXTO_RESTRICCION, K_LINK_RESTRICCION, [rfReplaceAll]);
        Result := sTextoAdvertencia;
   end;
begin
     if not StrVacio(sMensajeAdvertencia) then
        Result :=  sMensajeAdvertencia
     else
         Result :=  sMensajeError ;
     Result := QuitarAcentos(Result);
end;

procedure TEnviarEmailGrupUsuarios.ObtenerMensajeHTML( DatosUsuarioEmail: TDatosUsuarioEmail; sMensajeAdvertencia, sMensajeError: String; EventLog, ErrorLog: TSentinelLog );
var
   sPlantillaHTML: String;
   RSource:Tresourcestream;
begin
     try
        RSource := TResourceStream.Create(HInstance,'PlantillaHMTLEmail', RT_RCDATA);
        FPlantillaHTML.LoadFromStream(RSource);
        sPlantillaHTML := FPlantillaHTML.Text;
        sPlantillaHTML := StringReplace( sPlantillaHTML, '##NOMBRE', DatosUsuarioEmail.UsNombre , [rfReplaceAll] );
        sPlantillaHTML := StringReplace( sPlantillaHTML, '#MENSAJE', ObtenerMensaje( sMensajeAdvertencia, sMensajeError ) , [rfReplaceAll] );
        FPlantillaHTML.Text := sPlantillaHTML;
     except
          on Error: Exception do
          begin
               ErrorLog( 'Se encontr� un Error ' + CR_LF + Error.Message );
          end;
     end;
end;

function TEnviarEmailGrupUsuarios.ConectaHost: Boolean;
begin
     with dmEmailService do
     begin
          NewEMail;
          MailServer := FHost;
          if FPort > 0 then
             Port := FPort;
          AuthMethod := FAuthMethod;
          User := FUserId;
          Password := FPasword;
          FromAddress := FFromAddress;
          FromName := FFromName;
          Result := TRUE;
     end;
end;

end.

