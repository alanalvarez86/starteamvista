unit FExtraerBuild;

interface

uses Windows, SysUtils, Classes, Registry, ADODB, Db,
     FAutoServer,
     FAutoClasses,
     ZetaRegistryServer,
     FSentinelRegistry;

type
    TRInfoArchivoTress = record
      CompanyName,
      FileDescription,
      FileVersion,
      InternalName,
      LegalCopyright,
      LegalTrademarks,
      OriginalFileName,
      ProductName,
      ProductVersion,
      Comments,
      PrivateBuild,
      SpecialBuild,
      DateModified: string;
  end;

  TExtraerBuild = class( TObject )
  private
  { Private declarations }
  public
  { Public declarations }
  end;

TInfoArhivoTress = class( TObject )
   public
   { Public declarations }
          PATH : string;
          TF_EXT : string;
          TF_FILE : string;
          TF_DESCR : string;
          TF_VERSION : string;
          TF_BUILD : string;
          TF_FEC_MOD : TDate;
          TF_CALLS : integer;
          TF_FEC_ACC : TDate;
          TF_HOST : string;
          constructor Create;
          destructor Destroy; override;
end;


function GetPropiedadesEjecutablesTRESS( const FileName, Extension: String; var Errores : Boolean; var sError : string ): TRInfoArchivoTress;
function GetRutaTress(var sRutaConKeyServerMSSQL, sRutaConKeyClient, sError : string; var bUnaRutaValida : Boolean) : Boolean ;
function ProcesarPropiedadesArchivosTress : string;
procedure GetSubDirectorios(const sDirectorioRaiz: string; sltListaDirectorios: TStrings; var iContadorDirectorios : integer; var sError : string);
procedure FindDocs(const Root: string);
function LeerRegistroTest (const sRutaRegistro : string) : string;
function ProcesarPropiedadesArchivosTressTest: string;
procedure GetSubDirTest(const sDirectorioRaiz: string; sltListaDirectorios: TStrings; var iContadorDirectorios : integer; var sError : string);
procedure MensajeEventLog( EventLog, ErrorLog: TSentinelLog );

var
 ExtraerBuild  : TExtraerBuild;
 OEventLog : TSentinelLog;
 OEventErrorLog : TSentinelLog;


implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaWinAPITools,
     DZetaServerProvider,
     ZetaServerTools;



{ TExtraerBuild }

function LeerRegistroTest (const sRutaRegistro : string) : string;
var
  RegistryEntry: TRegistry;
begin
      try
            RegistryEntry := TRegistry.Create(KEY_ALL_ACCESS);
            RegistryEntry.RootKey := HKEY_LOCAL_MACHINE;
            if (RegistryEntry.KeyExists(sRutaRegistro)) then
            begin
                  RegistryEntry.Access := KEY_ALL_ACCESS;
                  if RegistryEntry.OpenKey(sRutaRegistro, true) then
                  begin
                       Result := RegistryEntry.ReadString('Installation');
                  end;
            end;
            RegistryEntry.CloseKey();
      finally
           RegistryEntry.Free;
      end;
end;

//TExtraerBuild.
function GetRutaTress (var sRutaConKeyServerMSSQL, sRutaConKeyClient, sError : string; var bUnaRutaValida : Boolean) : Boolean ;
Const
      C_KEY_64_SERVER_MSSQL='SOFTWARE\Grupo Tress\TressWin\ServerMSSQL\';
      C_KEY_32_SERVER_MSSQL='SOFTWARE\Wow6432Node\Grupo Tress\TressWin\ServerMSSQL\';
      C_KEY_64_CLIENT='SOFTWARE\Grupo Tress\TressWin\Client\';
      C_KEY_32_CLIENT='SOFTWARE\Wow6432Node\Grupo Tress\TressWin\Client\';
var
    RegistryEntry: TRegistry;
    X, Y : integer;
    sRuta_32_SERVER_MSSQL, sRuta_32_CLIENT, sRuta_64_SERVER_MSSQL,  sRuta_64_CLIENT: string;

    function LeerRegistro (var sRutaRegistro : string) : string;
    begin
          try
                RegistryEntry := TRegistry.Create(KEY_ALL_ACCESS);
                RegistryEntry.RootKey := HKEY_LOCAL_MACHINE;
                if (RegistryEntry.KeyExists(sRutaRegistro)) then
                begin
                      RegistryEntry.Access := KEY_ALL_ACCESS;
                      if RegistryEntry.OpenKey(sRutaRegistro, true) then
                      begin
                           Result := RegistryEntry.ReadString('Installation');
                      end;
                end;
                RegistryEntry.CloseKey();
          finally
               RegistryEntry.Free;
          end;
    end;

    procedure GetRutasRegistro64Bits;
    begin
          sRutaConKeyServerMSSQL := VACIO;
          sRutaConKeyClient := VACIO;
          sRuta_64_SERVER_MSSQL := C_KEY_64_SERVER_MSSQL;
          sRutaConKeyServerMSSQL := LeerRegistro( sRuta_64_SERVER_MSSQL );
          sRuta_64_CLIENT := C_KEY_64_CLIENT;
          sRutaConKeyClient := LeerRegistro( sRuta_64_CLIENT );
    end;

    procedure GetRutasRegistro32Bits;
    begin
         sRutaConKeyServerMSSQL := VACIO;
         sRutaConKeyClient := VACIO;
         sRuta_32_SERVER_MSSQL := C_KEY_32_SERVER_MSSQL;
         sRutaConKeyServerMSSQL := LeerRegistro( sRuta_32_SERVER_MSSQL );
         sRuta_32_CLIENT := C_KEY_32_CLIENT;
         sRutaConKeyClient := LeerRegistro( sRuta_32_CLIENT );

    end;

begin

      sRuta_32_SERVER_MSSQL := VACIO;
      sRuta_32_CLIENT := VACIO;
      sRuta_64_SERVER_MSSQL := VACIO;
      sRuta_64_CLIENT := VACIO;
      Result := False;
      sError := VACIO;
  try
    try
         GetRutasRegistro64Bits;
         if not ((sRutaConKeyServerMSSQL <> VACIO) AND (sRutaConKeyClient <> VACIO)) then
         begin
              GetRutasRegistro32Bits;
         end;

        if ((sRutaConKeyServerMSSQL <> VACIO) AND (sRutaConKeyClient <> VACIO)) then
        begin
             if(sRutaConKeyServerMSSQL <> sRutaConKeyClient) then
             begin
                  bUnaRutaValida := true;
                  Result := True;
             end
             else
             begin
                  bUnaRutaValida := true;
             end;
        end
        else if ((sRutaConKeyServerMSSQL = VACIO) AND (sRutaConKeyClient = VACIO)) then
        begin
              sError := 'No pudo leer la ruta de instalación del Servidor y Cliente de Sistema TRESS.';
              Result := true;
              Exit;
        end
        else if ((sRutaConKeyServerMSSQL = VACIO))then
        begin
              sError := 'No pudo leer la ruta de instalación del Servidor de Sistema TRESS.';
              bUnaRutaValida := true;
              Exit;
        end
        else if ((sRutaConKeyClient = VACIO)) then
        begin
              sError := 'No pudo leer la ruta de instalación del Cliente de Sistema TRESS.';
              bUnaRutaValida := true;
              Exit;
        end;
    except
          on E : Exception do
          begin
            sError := E.Message;
            OEventErrorLog(sError +' en GetRutaTress Exception');
          end;
    end;
  finally
      OEventLog('Se obtuvo ruta de archivos Tress del registro');
  end;
end;

function GetPropiedadesEjecutablesTRESS(const FileName, Extension: string; var Errores : Boolean; var sError : string ): TRInfoArchivoTress;
type
    PLandCodepage = ^TLandCodepage;
    TLandCodepage = record
      wLanguage,
      wCodePage: word;
end;
var
    dummy,
    len: cardinal;
    buf, pntr: pointer;
    lang: string;
begin

 len := 0;
 buf := nil;
 pntr := nil;

  try
     try
        len := GetFileVersionInfoSize({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(FileName), Dummy);
        if len = 0 then
        begin
             //Errores := true;
             Exit;
        end;

        if len > 0 then
        begin
           GetMem(buf, len);
           if not GetFileVersionInfo({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(FileName), 0, len, buf) then
           begin
                //Errores := true;
                Exit;
           end;


           if not VerQueryValue(buf, '\VarFileInfo\Translation\', pntr, len) then
           begin
                sError := 'ES SETUP.EXE';
                OEventErrorLog(sError + ' en GetPropiedadesEjecutablesTRESS');
                Exit; // -- SALTA ERROR CUANDO ES SETUP EXE
           end;


           lang := Format('%.4x%.4x', [PLandCodepage(pntr)^.wLanguage, PLandCodepage(pntr)^.wCodePage]);

           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\CompanyName'), pntr, len){ and (@len <> nil)} then
             Result.CompanyName := PChar(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\FileDescription'), pntr, len){ and (@len <> nil)} then
             result.FileDescription := PChar(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\FileVersion'), pntr, len){ and (@len <> nil)} then
             result.FileVersion := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\InternalName'), pntr, len){ and (@len <> nil)} then
             result.InternalName := PChar(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\LegalCopyright'), pntr, len){ and (@len <> nil)} then
             result.LegalCopyright := PChar(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\LegalTrademarks'), pntr, len){ and (@len <> nil)} then
             result.LegalTrademarks := PChar(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\OriginalFileName'), pntr, len){ and (@len <> nil)} then
             result.OriginalFileName := PChar(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\ProductName'), pntr, len){ and (@len <> nil)} then
             result.ProductName := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\ProductVersion'), pntr, len){ and (@len <> nil)} then
             result.ProductVersion := PChar(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\Comments'), pntr, len){ and (@len <> nil)} then
             result.Comments := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\PrivateBuild'), pntr, len){ and (@len <> nil)} then
             result.PrivateBuild := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\SpecialBuild'), pntr, len){ and (@len <> nil)} then
             result.SpecialBuild := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(pntr);
           if VerQueryValue(buf, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('\StringFileInfo\' + lang + '\DateModified'), pntr, len){ and (@len <> nil)} then
             result.DateModified := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(pntr);


        end;
     except
        on E : Exception do
        begin
            sError := sError + ' mensaje=' + E.Message;
            OEventErrorLog(sError + ' en GetPropiedadesEjecutablesTRESS Exception');
        end;
     end;

  finally

      if ( len > 0 )  then
         FreeMem(buf, len);
  end;
end;


procedure FindDocs(const Root: string);
var
  SearchRec: TSearchRec;
  Folders: array of string;
  Folder: string;
  I: Integer;
  Last: Integer;
begin
  SetLength(Folders, 1);
  Folders[0] := Root;
  I := 0;
  while (I < Length(Folders)) do
  begin
    Folder := IncludeTrailingBackslash(Folders[I]);
    Inc(I);
    { Collect child folders first. }
    if (FindFirst(Folder + '*.*', faDirectory, SearchRec) = 0) then
    begin
      repeat
        if not ((SearchRec.Name = '.') or (SearchRec.Name = '..')) then
        begin
          Last := Length(Folders);
          SetLength(Folders, Succ(Last));
          Folders[Last] := Folder + SearchRec.Name;
        end;
      until (FindNext(SearchRec) <> 0);
      FindClose(SearchRec);
    end;
    { Collect files next.}
    if (FindFirst(Folder + '*.doc', faAnyFile - faDirectory, SearchRec) = 0) then
    begin
      repeat
        if not ((SearchRec.Attr and faDirectory) = faDirectory) then
        begin
          //WriteLn(Folder, SearchRec.Name);
        end;
      until (FindNext(SearchRec) <> 0);
      FindClose(SearchRec);
    end;
  end;
end;

procedure GetSubDirectorios(const sDirectorioRaiz: string; sltListaDirectorios: TStrings; var iContadorDirectorios : integer; var sError : string);
const
      NUMERO_MAXIMO_DIRECTORIOS = 200;
var
    srSearch: TSearchRec;
    sSearchPath: string;
    sltSub: TStrings;
    i: Integer;
begin
      sltSub := TStringList.Create;
      sltListaDirectorios.BeginUpdate;
  try
     try
        sSearchPath := IncludeTrailingPathDelimiter(sDirectorioRaiz);
        if FindFirst(sSearchPath + '*', faDirectory, srSearch) = 0 then
        begin
              repeat
                    if ((srSearch.Attr and faDirectory) = faDirectory) and (srSearch.Name <> '.') and (srSearch.Name <> '..') then
                    begin
                          sltListaDirectorios.Add(sSearchPath + srSearch.Name);
                          sltSub.Add(sSearchPath + srSearch.Name);
                          inc(iContadorDirectorios);
                    end;
              until (FindNext(srSearch) <> 0);
        end;

        FindClose(srSearch);

        if (iContadorDirectorios > NUMERO_MAXIMO_DIRECTORIOS) then
        begin
             sError := 'Demasiados directorios en directorio de instalación, revisar ruta.';
             Exit;
        end;

        for i := 0 to sltSub.Count - 1 do
          GetSubDirectorios(sltSub.Strings[i], sltListaDirectorios, iContadorDirectorios, sError);
     Except
          on E : Exception do
          begin
            sError := E.Message;
            OEventErrorLog(sError + ' en GetSubDirectorios Exception')
          end;
     end;
  finally
    sltListaDirectorios.EndUpdate;
    FreeAndNil(sltSub);
  end;
end;

procedure GetSubDirTest(const sDirectorioRaiz: string; sltListaDirectorios: TStrings; var iContadorDirectorios : integer; var sError : string);
const
      NUMERO_MAXIMO_DIRECTORIOS = 200;
var
    tWFD: WIN32_FIND_DATA;
    FindHandle,Retcode: integer;
    sFile: string;
begin
    sltListaDirectorios.BeginUpdate;
    sFile := sDirectorioRaiz + '*.*';
    try
      try
        FindHandle := FindFirstFile(PChar(sDirectorioRaiz + '\*.*'),tWFD);
        if FindHandle <> INVALID_HANDLE_VALUE then
          Retcode := 0
        else
          Retcode := GetLastError;
        while Retcode = 0 do
        begin
          if (tWFD.dwFileAttributes and faDirectory) = faDirectory then
          begin
            if (String(tWFD.cFileName) <> '.') and (String(tWFD.cFileName) <> '..') then
            begin
              sltListaDirectorios.Add(sDirectorioRaiz +'\'+ tWFD.cFileName);
              inc(iContadorDirectorios);
              if (iContadorDirectorios > NUMERO_MAXIMO_DIRECTORIOS) then
              begin
                   sError := 'Demasiados directorios en directorio de instalación, revisar ruta.';
                   Exit;
              end;
              GetSubDirTest(sDirectorioRaiz +'\'+ tWFD.cFileName, sltListaDirectorios, iContadorDirectorios, sError);
            end;
          end;
          if FindNextFile(FindHandle, tWFD) then
            Retcode := 0
          else
            Retcode := GetLastError;
        end;
        if FindHandle <> INVALID_HANDLE_VALUE then
           Windows.FindClose(FindHandle);
      Except
          on E : Exception do
            sError := E.Message;
      end;
    finally
      sltListaDirectorios.EndUpdate;
    end;
end;

function ProcesarPropiedadesArchivosTressTest: string;
begin
    Result := LeerRegistroTest('SOFTWARE\Grupo Tress\TressWin\ServerMSSQL\');
end;

function ProcesarPropiedadesArchivosTress: string;
const
      CANTIDAD_EXTENSIONES_PROCESAR = 4;
      DIRECTORIOS_SON_DISTINTOS = true;
      EXE = '*.EXE';
      DLL = '*.DLL';
      SQL = '*.SQL';
      XML = '*.XML';
var
   sFuente : string;
   sSource : string;
   bError, bRutasRegistroDistintas : Boolean;
   lArchivosXProcesar: Boolean;
   oBusca:TSearchRec;
   sRutaConKeyServerMSSQL, sRutaConKeyClient : string;
   sExtension, sArchivo : string;
   i: integer;
   oZetaProvider: TdmZetaServerProvider;
   sTipoRuta : string;
   sError : string;
   bUnaRutaValida : Boolean;
   sErrorRutasRegistro : string;

   procedure InsertarEstampaInformacion_db(var InformacionArchivoTRESS : TInfoArhivoTress);
   var
      Proc: TADOStoredProc;
      PATH : ADODB.TParameter;
      TF_EXT : ADODB.TParameter;
      TF_FILE : ADODB.TParameter;
      TF_DESCR : ADODB.TParameter;
      TF_VERSION : ADODB.TParameter;
      TF_BUILD : ADODB.TParameter;
      TF_FEC_MOD : ADODB.TParameter;
      TF_CALLS : ADODB.TParameter;
      TF_FEC_ACC : ADODB.TParameter;
      TF_HOST : ADODB.TParameter;
   begin
     try
       try
          Proc := TADOStoredProc.Create(nil);
          oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;
          oZetaProvider.PreparaStoredProc(Proc, 'SP_SET_TRESSFILE');

          PATH := Proc.Parameters.AddParameter;
          with PATH do
          begin
                Name := '@PATH';
                DataType := ftString;
                Value := InformacionArchivoTRESS.PATH;
          end;

          TF_EXT := Proc.Parameters.AddParameter;
          with TF_EXT do
          begin
                Name := '@TF_EXT';
                DataType := ftString;
                Value := InformacionArchivoTRESS.TF_EXT;
          end;

          TF_FILE := Proc.Parameters.AddParameter;
          with TF_FILE do
          begin
                Name := '@TF_FILE';
                DataType := ftString;
                Value := InformacionArchivoTRESS.TF_FILE;
          end;

          TF_DESCR := Proc.Parameters.AddParameter;
          with TF_DESCR do
          begin
                Name := '@TF_DESCR';
                DataType := ftString;
                Value := InformacionArchivoTRESS.TF_DESCR;
          end;

          TF_VERSION := Proc.Parameters.AddParameter;
          with TF_VERSION do
          begin
                Name := '@TF_VERSION';
                DataType := ftString;
                Value := InformacionArchivoTRESS.TF_VERSION;
          end;

          TF_BUILD := Proc.Parameters.AddParameter;
          with TF_BUILD do
          begin
                Name := '@TF_BUILD';
                DataType := ftString;
                Value := InformacionArchivoTRESS.TF_BUILD;
          end;


          TF_FEC_MOD := Proc.Parameters.AddParameter;
          with TF_FEC_MOD do
          begin
                Name := '@TF_FEC_MOD';
                DataType := ftDate;
                Value := InformacionArchivoTRESS.TF_FEC_MOD;
          end;

          TF_CALLS := Proc.Parameters.AddParameter;
          with TF_CALLS do
          begin
                Name := '@TF_CALLS';
                DataType := ftInteger;
                Value := InformacionArchivoTRESS.TF_CALLS;
          end;

          TF_FEC_ACC := Proc.Parameters.AddParameter;
          with TF_FEC_ACC do
          begin
                Name := '@TF_FEC_ACC';
                DataType := ftDate;
                Value := InformacionArchivoTRESS.TF_FEC_ACC;
          end;

          TF_HOST := Proc.Parameters.AddParameter;
          with TF_HOST do
          begin
                Name := '@TF_HOST';
                DataType := ftString;
                Value := InformacionArchivoTRESS.TF_HOST;
          end;

          Proc.ExecProc;
       except
            on E: Exception do
            begin
                sError := E.Message;
                OEventErrorLog(sError + ' en InsertarEstampaInformacion_db Exception');
            end;
       end;
     finally
       Proc.Free;
     end;
   end;

   procedure InsertarEstampaInformacion_db_csv(var InformacionArchivoTRESS : TInfoArhivoTress; const Stream: TFileStream);
   var
      sTemp: string;
   begin
       try

          sTemp := '';

          sTemp := sTemp + InformacionArchivoTRESS.PATH + ',';
          sTemp := sTemp + InformacionArchivoTRESS.TF_EXT + ',';
          sTemp := sTemp + InformacionArchivoTRESS.TF_FILE + ',';
          sTemp := sTemp + InformacionArchivoTRESS.TF_DESCR + ',';
          sTemp := sTemp + InformacionArchivoTRESS.TF_VERSION + ',';
          sTemp := sTemp + InformacionArchivoTRESS.TF_BUILD + ',';
          sTemp := sTemp + DateToStr(InformacionArchivoTRESS.TF_FEC_MOD) + ',';
          sTemp := sTemp + IntToStr( InformacionArchivoTRESS.TF_CALLS) + ',';
          sTemp := sTemp + DateToStr(InformacionArchivoTRESS.TF_FEC_ACC) + ',';
          sTemp := sTemp + InformacionArchivoTRESS.TF_HOST;

          Stream.Write(sTemp[1], Length(sTemp) * SizeOf(Char));
          Stream.Write(AnsiString(#13#10), Length(AnsiString(#13#10)));

       except
            on E: Exception do
                sError := E.Message;
       end;
   end;

   procedure limpiarObjetoArchivoTress (var oInfoArchivoTress : TInfoArhivoTress);
   begin
        try
            with oInfoArchivoTress do
            begin
                  PATH := VACIO;
                  TF_EXT := VACIO;
                  TF_FILE := VACIO;
                  TF_DESCR := VACIO;
                  TF_VERSION := VACIO;
                  TF_BUILD := VACIO;
                  TF_FEC_MOD := 0;
                  TF_CALLS := 0;
                  TF_FEC_ACC := 0;
                  TF_HOST := VACIO;
            end;
        Except
        end;
   end;

   function ProcesarArchivos ( ListaDirectorios : TStrings ) : Boolean;
   var
      iCantidadDirectorios : integer;
      j: integer;
      bErrores : Boolean;
      FileName,Info : WideString;
      oInfoArchivoTress : TInfoArhivoTress;
      Created, Accessed, Modified: TDateTime;
   begin
        try
           try
              iCantidadDirectorios := ListaDirectorios.Count;
              bErrores := False;
              for j := 0 to iCantidadDirectorios - 1 do
              begin
                  lArchivosXProcesar := FindFirst( VerificaDir( ListaDirectorios[j] ) + sExtension, faArchive, oBusca ) = 0;
                  if (lArchivosXProcesar) then
                  begin
                      repeat
                            try
                                sArchivo := ListaDirectorios[j] +'\'+oBusca.Name;
                                oInfoArchivoTress := TInfoArhivoTress.Create;
                                if ((sExtension = EXE) OR (sExtension = DLL)) then
                                begin
                                      with GetPropiedadesEjecutablesTRESS(sArchivo, sExtension, bErrores, sError) do
                                      begin
                                            if (sError = 'ES SETUP.EXE') then //ESTAMPA DE INFORMACION PARA EL SETUP.EXE
                                            begin
                                                 oInfoArchivoTress.PATH := sArchivo;
                                                 oInfoArchivoTress.TF_EXT :=  StringReplace(sExtension, '*.', '',[rfReplaceAll, rfIgnoreCase]);
                                                 oInfoArchivoTress.TF_FILE := oBusca.Name;
                                                 ZetaWinAPITools.GetInfoDate(sArchivo, Created, Accessed, Modified);


                                                 oInfoArchivoTress.TF_FEC_MOD := Modified;
                                                 oInfoArchivoTress.TF_FEC_ACC := Accessed;


                                                 InsertarEstampaInformacion_db(oInfoArchivoTress);
                                                 //InsertarEstampaInformacion_db_csv(oInfoArchivoTress,Stream);
                                                 limpiarObjetoArchivoTress (oInfoArchivoTress);
                                            end;

                                            if (bErrores) then
                                            begin
                                                 OEventErrorLog('Flag Error activada desde GetPropiedadesEjecutablesTRESS');
                                                 Exit;
                                            end
                                            else if ( sError <> 'ES SETUP.EXE' ) then
                                            begin
                                                 oInfoArchivoTress.PATH := sArchivo;
                                                 oInfoArchivoTress.TF_EXT :=  StringReplace(sExtension, '*.', '',[rfReplaceAll, rfIgnoreCase]);
                                                 oInfoArchivoTress.TF_FILE := oBusca.Name;
                                                 oInfoArchivoTress.TF_DESCR := FileDescription;
                                                 oInfoArchivoTress.TF_VERSION := ProductVersion;
                                                 oInfoArchivoTress.TF_BUILD := FileVersion;
                                                 ZetaWinAPITools.GetInfoDate(sArchivo, Created, Accessed, Modified);


                                                 oInfoArchivoTress.TF_FEC_MOD := Modified;
                                                 oInfoArchivoTress.TF_FEC_ACC := Accessed;

                                                 InsertarEstampaInformacion_db(oInfoArchivoTress);
                                                 //InsertarEstampaInformacion_db_csv(oInfoArchivoTress,Stream);
                                                 limpiarObjetoArchivoTress (oInfoArchivoTress);
                                                 if sError <> VACIO then
                                                 begin
                                                       OEventErrorLog(sError + ' en ProcesarArchivos Exception');
                                                       Exit;
                                                 end;
                                            end;

                                            if (sError = 'ES SETUP.EXE') then
                                            begin
                                                sError := VACIO;
                                            end;
                                      end;
                                end
                                else
                                begin
                                     oInfoArchivoTress.PATH := sArchivo;
                                     oInfoArchivoTress.TF_EXT :=  StringReplace(sExtension, '*.', '',[rfReplaceAll, rfIgnoreCase]);
                                     oInfoArchivoTress.TF_FILE := oBusca.Name;
                                     ZetaWinAPITools.GetInfoDate(sArchivo, Created, Accessed, Modified);
                                     oInfoArchivoTress.TF_FEC_MOD := Modified;
                                     oInfoArchivoTress.TF_FEC_ACC := Accessed;

                                     InsertarEstampaInformacion_db(oInfoArchivoTress);
                                     //InsertarEstampaInformacion_db_csv(oInfoArchivoTress,Stream);
                                     limpiarObjetoArchivoTress (oInfoArchivoTress);
                                     if sError <> VACIO then
                                     begin
                                           OEventErrorLog(sError + ' en ProcesarArchivos');
                                           Exit;
                                     end;
                                end;
                            finally
                                   FreeAndNil(oInfoArchivoTress);
                            end;
                      until FindNext( oBusca ) <> 0;
                  end;
              end;
           except
              on E: Exception do
              begin
                sError := E.Message;
                OEventErrorLog(sError + ' en ProcesarArchivos Exception');
              end;
           end;
        finally
        end;
   end;

   procedure CargarExtensionyProcesar ;
   var
      oListaDirectorios : TStringList;
      j : integer;
      sDirectorioRaiz : string;
      iContadorDirectorios : integer;
   begin
        oListaDirectorios := TStringList.Create;
        iContadorDirectorios := 0;
        sError := VACIO;
        try
            GetSubDirectorios(sSource, oListaDirectorios, iContadorDirectorios, sError);
            OEventLog('Lectura de estructura de archivos Tress finalizado');
            //GetSubDirTest(sSource, oListaDirectorios, iContadorDirectorios, sError); //usa otra libreria
            if ( sError = VACIO ) then
            begin
                  sDirectorioRaiz := sSource;
                  if (sDirectorioRaiz[Length(sDirectorioRaiz)] = '\') then
                  begin
                        SetLength(sDirectorioRaiz, length(sDirectorioRaiz)-1);
                  end;
                  oListaDirectorios.Add(sDirectorioRaiz);
                  for j := 1 to CANTIDAD_EXTENSIONES_PROCESAR do
                  begin
                        case j of
                            1:  sExtension := EXE;
                            2:  sExtension := DLL;
                            3:  sExtension := SQL;
                            4:  sExtension := XML;
                        end;
                        ProcesarArchivos ( oListaDirectorios );
                        OEventLog('Procesamiento de Archivos tress tipo '+ IntToStr(j) + '/4');
                        if sError <> VACIO then
                        begin
                              OEventErrorLog(sError + ' en CargarExtensionyProcesar');
                              Exit;
                        end;
                  end;
            end
        finally
             FreeAndNil(oListaDirectorios);
        end;
   end;

begin
    try
      try
         bError := false;
         sFuente := VACIO;
         sSource := VACIO;
         sError := VACIO;
         sErrorRutasRegistro := VACIO;
         bUnaRutaValida := false;
         oZetaProvider := TdmZetaServerProvider.Create( nil );
         OEventLog('Procesando propiedades de archivos Tress');
         bRutasRegistroDistintas :=  GetRutaTress(sRutaConKeyServerMSSQL, sRutaConKeyClient, sErrorRutasRegistro, bUnaRutaValida);
         if ((sError = VACIO) and (bUnaRutaValida)) then
         begin
               if ((bRutasRegistroDistintas) = DIRECTORIOS_SON_DISTINTOS) then   // Y NINGUNA RUTA ES VACIO
               begin
                    for i := 1 to 2 do
                    begin
                          case i of
                              1:  sSource := sRutaConKeyServerMSSQL;
                              2:  sSource := sRutaConKeyClient;
                          end;

                          if (DirectoryExists(sSource) = true) then
                          begin
                                CargarExtensionyProcesar;
                                if sError <> VACIO then
                                begin
                                      if i = 1 then
                                      begin
                                            sTipoRuta := 'Servidor';
                                      end
                                      else
                                      begin
                                            sTipoRuta := 'Cliente';
                                      end;
                                      sError := ' | Ruta Installation del '+sTipoRuta+' | '+sError;
                                      OEventErrorLog(sError+' en ProcesarPropiedadesArchivosTress');
                                      Result := sError;
                                end;
                          end
                          else
                          begin
                                sError := 'El directorio '+sSource+' no existe, no es posible hacer el barrido.';
                          end;
                    end;
               end
               else
               begin
                    if (sRutaConKeyServerMSSQL <> VACIO) then
                    begin
                         sSource := sRutaConKeyServerMSSQL;
                    end
                    else if (sRutaConKeyClient <> VACIO)  then
                    begin
                         sSource := sRutaConKeyClient;
                    end;

                    if (DirectoryExists(sSource) = true) then
                    begin
                         CargarExtensionyProcesar;
                    end
                    else
                    begin
                         sError := 'El directorio '+sSource+' no existe, no es posible hacer el barrido.';
                    end;

               end;
         end
         else
         begin
               OEventErrorLog(sError +' en ProcesarPropiedadesArchivosTress');
               Result :=  sError;
         end;

         if (sError <> VACIO) or (sErrorRutasRegistro <> VACIO)then
         begin
              if sErrorRutasRegistro <> VACIO then
              begin
                    if (sError <> VACIO) then
                    begin
                        OEventErrorLog(sErrorRutasRegistro + ' - ' +  sError +' en ProcesarPropiedadesArchivosTress');
                        Result := sErrorRutasRegistro + ' - ' +  sError;
                    end
                    else
                    begin
                        OEventErrorLog(sErrorRutasRegistro + ' en ProcesarPropiedadesArchivosTress');
                        Result := sErrorRutasRegistro;
                    end;
              end
              else
              begin
                    OEventErrorLog(sError +' en ProcesarPropiedadesArchivosTress');
                    Result :=  sError;
              end;
         end;

      except
          on E : Exception do
          begin
            sError := E.Message;
            OEventErrorLog(sError +' en ProcesarPropiedadesArchivosTress exception');
          end;
      end;
    finally
           OEventLog('Finalizo proceso de propiedades de archivos Tress');
           FreeAndNil( oZetaProvider );
    end;
end;

procedure MensajeEventLog( EventLog, ErrorLog: TSentinelLog );
begin
  OEventLog := EventLog;
  OEventErrorLog := ErrorLog;
end;

{ TInfoArhivoTress }
constructor TInfoArhivoTress.Create;
begin
      inherited Create;
      PATH := VACIO;
      TF_EXT := VACIO;
      TF_FILE := VACIO;
      TF_DESCR := VACIO;
      TF_VERSION := VACIO;
      TF_BUILD := VACIO;
      TF_FEC_MOD := 0;
      TF_CALLS := 0;
      TF_FEC_ACC := 0;
      TF_HOST := VACIO;
end;

destructor TInfoArhivoTress.Destroy;
begin
      inherited Destroy;
end;

end.

