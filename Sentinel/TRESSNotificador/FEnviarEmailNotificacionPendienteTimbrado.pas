unit FEnviarEmailNotificacionPendienteTimbrado;

interface

uses Windows, SysUtils, Classes, Registry, ADODB, Db,
     FAutoServer,
     FAutoClasses,
     FSentinelRegistry,
     ZetaRegistryServer,
     ZetaCommonLists,
     DEmailService,
     ZCreator,
     ZetaSQLBroker,
     DZetaServerProvider,
     ZetaLicenseMgr,
     ZetaLicenseClasses,
     Contnrs,
     HTTPApp,
     {*** US 13038:  Notificacion Timbrado consultas ***}
     ZetaConsultaTimbrado;

type
  TEnviarEmailNotificacionPendienteTimbrado = class( TObject )
  private
  { Private declarations }
   FHost: String;
   FPort:Integer;
   FAuthMethod: eAuthTressEmail;
   FPasword:String;
   FUserID: String;
   FFromAddress: String;
   FFromName: String;
   FLicenseMgr: TLicenseMgr;
   {*** US 13038:  Notificacion Timbrado consultas ***}
   FConsultaTimbrado: TConsultaTimbrado;
   oZetaCreator: TZetaCreator;
   oZetaProvider: TdmZetaServerProvider;
   dmEmailService : TdmEmailService;
   FPlantillaHTML: TStringList;
   FEnvioEmailOnce: Boolean; //Indica si al menos un corro se envio
   FEmailOnceValido: Boolean; //Indica si al menos una direccion de correo es valido
   FHayPendienteTimbrado: Boolean;//Indica si hay pendiente de timbrado por empresa
   function ConfiguracionServerEmail( EventLog, ErrorLog: TSentinelLog ): Boolean;
   function GetListaEmpresas: TObjectList;
   function GetListaContacto( const CM_CODIGO: String ): TObjectList;
   procedure SendNotificacionTimbrado( DatosListadoEmpresas: TDatosListadoEmpresas; ListaDatosContactos: TObjectList; sMensajeAdvertencia, sMensajeError: String; EventLog, ErrorLog: TSentinelLog );
   function ConectaHost: Boolean;
   function ObtenerSubJect(CM_NOMBRE: String): String;
   procedure ObtenerMensajeHTML( DatosListadoEmpresas: TDatosListadoEmpresas; DatosListadoContactos: TDatosListadoContactos; sTabla: String; EventLog, ErrorLog: TSentinelLog );
   function ObtenerTablaPendienteTimbrado( CM_CODIGO: String; var sTabla: String; EventLog, ErrorLog: TSentinelLog ): Boolean;
   function CaracteresHTML( sTextoHeader: String ): String;
  public
  { Public declarations }
   function EnviarNotificacionTimbrado(EventLog, ErrorLog: TSentinelLog): Boolean;
   constructor Create;
   Destructor  Destroy; override;
end;
var
   EnviarEmailGrupUsuarios  : TEnviarEmailNotificacionPendienteTimbrado;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaWinAPITools,
     ZetaServerTools;

{ TEnviarEmailNotificacionPendienteTimbrado }

function TEnviarEmailNotificacionPendienteTimbrado.ConfiguracionServerEmail( EventLog, ErrorLog: TSentinelLog ): Boolean;
const
     K_ARROBA = '@';
begin
     FHost := FLicenseMgr.GetGlobalServidorCorreos;
     FPort := FLicenseMgr.GetGlobalPuertoSMTP;
     FAuthMethod :=  eAuthTressEmail( FLicenseMgr.GetGlobalAutentificacionCorreo - 1 );
     Result := ZetaCommonTools.StrLleno( FHost );
     if not Result then
        ErrorLog( 'Servidor de correos No Configurados' + CR_LF + 'Favor De Indicar El Servidor De Correos' );
     FUserID := FLicenseMgr.GetGlobalUserID;
     FPasword := ZetaServerTools.Decrypt( FLicenseMgr.GetGlobalEmailPSWD );
     if ValidarEmail ( FUserID ) then
        FFromAddress := FUserID
     else
         FFromAddress := FUserID + K_ARROBA + FHost;
     FFromName := VACIO;
end;

constructor TEnviarEmailNotificacionPendienteTimbrado.Create;
begin
     oZetaProvider := TdmZetaServerProvider.Create( nil );
     FLicenseMgr := TLicenseMgr.Create( oZetaProvider );
     {*** US 13038:  Notificacion Timbrado consultas ***}
     FConsultaTimbrado := TConsultaTimbrado.Create( oZetaProvider );
     dmEmailService := TdmEmailService.Create( nil );
     oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
     FPlantillaHTML := TStringList.Create;
     FEnvioEmailOnce := FALSE;
     FEmailOnceValido := FALSE;
     FHayPendienteTimbrado := FALSE;
end;

destructor TEnviarEmailNotificacionPendienteTimbrado.Destroy;//Libera Memoria de todos los componentes que intervienen
begin
     FreeAndNil( dmEmailService );
     FreeAndNil( FLicenseMgr );
     FreeAndNil( FConsultaTimbrado );
     ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
     FreeAndNil(FPlantillaHTML);
     inherited;
end;


function TEnviarEmailNotificacionPendienteTimbrado.EnviarNotificacionTimbrado(EventLog, ErrorLog: TSentinelLog): Boolean;
var
   sMensajeAdvertencia, sMensajeError: String;
   DatosListadoEmpresas : TDatosListadoEmpresas;
   ListaDatosEmpresas  : TObjectList;
   ListaDatosContactos : TObjectList;
   i: Integer;
begin
     //********************* ENVIAR EMAIL **********************/
     Result := ConfiguracionServerEmail( EventLog, ErrorLog );
     if Result then
     begin
          ListaDatosEmpresas := GetListaEmpresas;
          for i := 0 to ( ListaDatosEmpresas.Count - 1 ) do
          begin
               DatosListadoEmpresas  := TDatosListadoEmpresas(ListaDatosEmpresas[i]);
               ListaDatosContactos   := GetListaContacto( DatosListadoEmpresas.CM_CODIGO );
               SendNotificacionTimbrado( DatosListadoEmpresas, ListaDatosContactos, sMensajeAdvertencia, sMensajeError, EventLog, ErrorLog );
          end;
     end;
     //******************** FIN **************************/
end;

procedure TEnviarEmailNotificacionPendienteTimbrado.SendNotificacionTimbrado( DatosListadoEmpresas: TDatosListadoEmpresas; ListaDatosContactos: TObjectList; sMensajeAdvertencia, sMensajeError: String; EventLog, ErrorLog: TSentinelLog );
var
   sErrorMsg: String;
   i: Integer;
   DatosListadoContactos: TDatosListadoContactos;
   sTabla: String;
   lPendienteTimbrado: Boolean;
begin
     FEmailOnceValido := FALSE;
     FHayPendienteTimbrado := FALSE;
     lPendienteTimbrado := ObtenerTablaPendienteTimbrado( DatosListadoEmpresas.CM_CODIGO, sTabla, EventLog, ErrorLog);//Obtener las nominas pendientes a timbrar por empresa
     try
        if lPendienteTimbrado then
        begin
             FHayPendienteTimbrado := TRUE;
             for i := 0 to ( ListaDatosContactos.Count - 1 ) do
             begin
                  DatosListadoContactos := TDatosListadoContactos(ListaDatosContactos[i]);
                  if ( ValidarEmail( DatosListadoContactos.US_EMAIL ) ) then
                  begin
                       FEmailOnceValido := TRUE;
                       with dmEmailService do
                       begin
                            ConectaHost;
                            SubType := emtHtml;
                            Subject := ObtenerSubJect( DatosListadoEmpresas.CM_NOMBRE );
                            ToAddress.Add( DatosListadoContactos.US_EMAIL );
                            ObtenerMensajeHTML( DatosListadoEmpresas, DatosListadoContactos, sTabla, EventLog, ErrorLog );
                            MessageText.AddStrings( FPlantillaHTML );
                            if Validate( sErrorMsg ) then
                            begin
                                 if SendEmail( sErrorMsg ) then
                                 begin
                                      EventLog( Format( 'Se envi� el correo de notificaci�n de n�minas pendientes a timbrar al usuario: %s', [ DatosListadoContactos.US_NOMBRE ] ) );
                                      FEnvioEmailOnce := TRUE;
                                 end
                                 else
                                 begin
                                      ErrorLog( 'Error al tratar de enviar el correo de notificaci�n de n�minas pendientes a timbrar a: '+ DatosListadoContactos.US_EMAIL + ' de la empresa ' + DatosListadoEmpresas.CM_NOMBRE + CR_LF + sErrorMsg );
                                 end;
                            end
                            else
                            begin
                                 ErrorLog( 'Error al tratar de enviar el correo de notificaci�n de n�minas pendientes a timbrar a: '+ DatosListadoContactos.US_EMAIL + ' de la empresa ' + DatosListadoEmpresas.CM_NOMBRE + CR_LF + sErrorMsg );
                            end;
                       end;
                  end
                  else
                  begin
                       ErrorLog ( Format( 'Error al tratar de enviar el correo de notificaci�n de n�minas pendientes a timbrar ya que el correo %s del usuario %s de la empresa %s es inv�lido', [ DatosListadoContactos.US_EMAIL, DatosListadoContactos.US_NOMBRE, DatosListadoEmpresas.CM_NOMBRE ] ) );
                  end;
             end;
             if ListaDatosContactos.Count = 0 then
                EventLog( Format( 'No fue posible enviar la notificaci�n de n�minas pendientes a timbrar por algunas de las siguientes razones no hay usuarios con el derecho al proceso de Timbrado de Nomina, no son usuarios activos o no tienen correo electr�nico en la empresa: %s ', [ DatosListadoEmpresas.CM_NOMBRE ] ) );
        end;
     except
          on Error: Exception do
          begin
               ErrorLog( 'Se encontr� un Error : ' + DatosListadoEmpresas.CM_CODIGO + CR_LF + Error.Message );
          end;
     end;
end;

function TEnviarEmailNotificacionPendienteTimbrado.GetListaEmpresas: TObjectList;
begin
     Result := FConsultaTimbrado.GetNotificacionTimbradoListaEmpresas;
end;

function TEnviarEmailNotificacionPendienteTimbrado.GetListaContacto( const CM_CODIGO: String ): TObjectList;
begin
     Result := FConsultaTimbrado.GetNotificacionTimbradoListaContacto( CM_CODIGO );
end;

function TEnviarEmailNotificacionPendienteTimbrado.ObtenerSubJect( CM_NOMBRE: String ): String;
begin
     Result := 'N�minas pendientes por timbrar de Sistema TRESS - ' + CM_NOMBRE ;
end;

function TEnviarEmailNotificacionPendienteTimbrado.ObtenerTablaPendienteTimbrado( CM_CODIGO: String; var sTabla: String; EventLog, ErrorLog: TSentinelLog ): Boolean;
var
   sMensajeError: String;
begin
     sTabla := FConsultaTimbrado.EmpleadosNotificacionesTimbradoPendiente( CM_CODIGO, sMensajeError);
     Result := not StrVacio( sTabla );
     if not StrVacio( sMensajeError ) then
     begin
          ErrorLog( 'Se encontr� un Error : ' + CM_CODIGO + CR_LF + sMensajeError );
          Result := FALSE;
     end;
end;

function TEnviarEmailNotificacionPendienteTimbrado.CaracteresHTML( sTextoHeader: String ): String;
begin
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&aacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&Aacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&aacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&Eacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&iacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&Iacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&oacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&Oacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&uacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&Uacute;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&Ntilde;', [rfReplaceAll]);
        sTextoHeader := StringReplace ( sTextoHeader, '�', '&ntilde;', [rfReplaceAll]);
        Result := sTextoHeader;
end;

function ObtenerMensaje( sNombreEmpresa: String ): String;
const
     K_FORMAT_HORA = 't';
begin
     Result := 'A Continuaci�n se muestran los periodos de n�mina que tiene recibos pendientes a timbrar la empresa ' + sNombreEmpresa + ' hasta las ' + ZetaCommonTools.HoraAsStr(now, K_FORMAT_HORA) + ' del d�a ' + ZetaCommonTools.FechaLarga(Now);
end;

procedure TEnviarEmailNotificacionPendienteTimbrado.ObtenerMensajeHTML( DatosListadoEmpresas: TDatosListadoEmpresas; DatosListadoContactos: TDatosListadoContactos; sTabla: String; EventLog, ErrorLog: TSentinelLog );
var
   sPlantillaHTML: String;
   RSource:Tresourcestream;
begin
     try
        RSource := TResourceStream.Create(HInstance,'PlantillaHTMLNotificacionTimbrado', RT_RCDATA);
        FPlantillaHTML.LoadFromStream(RSource);
        sPlantillaHTML := FPlantillaHTML.Text;
        sPlantillaHTML := StringReplace( sPlantillaHTML, '##NOMBRE', DatosListadoContactos.US_NOMBRE , [rfReplaceAll] );
        sPlantillaHTML := StringReplace( sPlantillaHTML, '#MENSAJE', CaracteresHTML( ObtenerMensaje( DatosListadoEmpresas.CM_NOMBRE ) ), [rfReplaceAll] );
        sPlantillaHTML := StringReplace( sPlantillaHTML, '#TABLA', CaracteresHTML( sTabla ), [rfReplaceAll] );
        FPlantillaHTML.Text := sPlantillaHTML;
     except
          on Error: Exception do
          begin
               ErrorLog( 'Se encontr� un Error ' + CR_LF + Error.Message );
          end;
     end;
end;

function TEnviarEmailNotificacionPendienteTimbrado.ConectaHost: Boolean;
begin
     with dmEmailService do
     begin
          NewEMail;
          MailServer := FHost;
          if FPort > 0 then
             Port := FPort;
          AuthMethod := FAuthMethod;
          User := FUserId;
          Password := FPasword;
          FromAddress := FFromAddress;
          FromName := FFromName;
          Result := TRUE;
     end;
end;

end.

