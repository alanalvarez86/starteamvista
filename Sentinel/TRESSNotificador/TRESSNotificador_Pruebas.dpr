program TRESSNotificador_Pruebas;


{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}


{$R *.dres}

uses
  SvcMgr,
  FSentinelRegistry in 'FSentinelRegistry.pas',
  DSentinel in 'DSentinel.pas' {TressNotificadorPruebasService: TService},
  FExtraerBuild in 'FExtraerBuild.pas',
  test in 'test.pas' {Form1};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'TressNotificadorPruebas Corporativo';
  Application.CreateForm(TTressNotificadorPruebasService, TressNotificadorPruebasService);
  Application.Run;
  {
  Application.CreateForm(TForm1, Form1);
  with Form1 do
  begin

            Show;
            Update;
            //WindowState := wsMaximized;
            //BeforeRun;
            Application.Run;
  end; }

end.
