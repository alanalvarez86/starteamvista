unit DSentinel;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DSentinel.pas                              ::
  :: Descripci�n: Programa principal de Sentinel3s.exe       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, SvcMgr, ActiveX;

type
  TTressNotificadorPruebasService = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure EnviarEmailAdvertencia;
    procedure EnviarEmailNotificacionPendienteTimbrado;
    function TestingProcedure: TStringList;
  private
    { Private declarations }
    FFecha: TDateTime;
    FIniciando: Integer;
    function FormatMessage(const sMessage: String): String;
    function Inicializa: Boolean;
    procedure EscribeError(const sMensaje: String);
    procedure EscribeBitacora(const sMensaje: String);
  public
    { Public declarations }
    function GetServiceController: TServiceController; override;
  end;

var
  TressNotificadorPruebasService: TTressNotificadorPruebasService;

implementation

{$R *.DFM}

uses FSentinelRegistry,
     FExtraerBuild,
     FEnviarEmailGrupUsuarios,
     FEnviarEmailNotificacionPendienteTimbrado,
     ZetaCommonTools;

procedure ServiceController( CtrlCode: DWord ); stdcall;
begin
     TressNotificadorPruebasService.Controller( CtrlCode );
end;

function TTressNotificadorPruebasService.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;

{ ******* TSentinel ******** }

procedure TTressNotificadorPruebasService.ServiceCreate(Sender: TObject);
var
   sDependecy: String;
   FRegistry: TSentinelRegistryServer;
begin
     FFecha := 0;
     FIniciando := 10;
     {$ifdef MSSQL}
     FRegistry := TSentinelRegistryServer.Create( False );
     try
        sDependecy := FRegistry.SentinelServerDependency;
     finally
            FreeAndNil( FRegistry );
     end;
     if ZetaCommonTools.StrLleno( sDependecy ) then
     begin
          with TDependency.Create( Dependencies ) do
          begin
               IsGroup := False;
               Name := sDependecy;
          end;
     end;
     {$endif}
end;

procedure TTressNotificadorPruebasService.ServiceStart(Sender: TService; var Started: Boolean);
begin
     CoInitialize(nil);
     FFecha := 0;
     FIniciando := 10;
     if FSentinelRegistry.CheckComputerInfo then //@(am): Cambio validacion licencia
     begin
          Started := True;
     end
     else
     begin
          Started := False;
          EscribeError( 'No puede arrancar en esta computadora' );
     end;
end;

procedure TTressNotificadorPruebasService.ServiceExecute(Sender: TService);
const
     K_UN_SEGUNDO = 1000;
     K_MEDIO_SEGUNDO = 500;
     VACIO = '';
var
     sError : string;
begin
     while not Terminated do
     begin
          if ( FFecha <> Date ) then
          begin
               if not Inicializa and ( FIniciando > 0 ) then
               begin
                   Sleep( 28 * K_UN_SEGUNDO );
                    FIniciando := FIniciando - 1;
               end
               else
               FFecha := Date;
               EscribeBitacora('El servicio inicio en esta computadora');
               FExtraerBuild.MensajeEventLog(EscribeBitacora,EscribeError);
               sError := FExtraerBuild.ProcesarPropiedadesArchivosTress;
               if (sError <> VACIO) then
                    EscribeError(sError);
               EnviarEmailAdvertencia;
               EnviarEmailNotificacionPendienteTimbrado;
               EscribeBitacora('Finalizo ProcesarPropiedadesArchivosTress en ServiceExecute');
          end;
          Sleep( 1 * K_MEDIO_SEGUNDO );
          ServiceThread.ProcessRequests( False );
     end;
end;

procedure TTressNotificadorPruebasService.EnviarEmailAdvertencia;
var
   EnviarEmail : TEnviarEmailGrupUsuarios;
   lEnviarEmail: Boolean;
begin
     try
        EnviarEmail := TEnviarEmailGrupUsuarios.Create;
        lEnviarEmail := EnviarEmail.AdvertenciaPorEmail( Date, EscribeBitacora, EscribeError );
        if ( lEnviarEmail ) then
           EnviarEmail.EnviarEmail(EscribeBitacora, EscribeError);
     finally
            FreeAndNil( EnviarEmail );
     end;
end;

procedure TTressNotificadorPruebasService.EnviarEmailNotificacionPendienteTimbrado;
var
   EnviarNotificacionTimbrado : TEnviarEmailNotificacionPendienteTimbrado;
begin
     try
        EnviarNotificacionTimbrado := TEnviarEmailNotificacionPendienteTimbrado.Create;
        EnviarNotificacionTimbrado.EnviarNotificacionTimbrado(EscribeBitacora, EscribeError);
     finally
            FreeAndNil( EnviarNotificacionTimbrado );
     end;
end;

function TTressNotificadorPruebasService.FormatMessage(const sMessage: String): String;
begin
     Result := Format( '***   %s   ***', [ sMessage ] );
end;

procedure TTressNotificadorPruebasService.EscribeBitacora(const sMensaje: String);
begin
     try
        LogMessage( FormatMessage( sMensaje ), EVENTLOG_INFORMATION_TYPE, 0, 0 );
     except
           LogMessage( FormatMessage( 'Error al Escribir a Bit�cora' ), EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

procedure TTressNotificadorPruebasService.EscribeError(const sMensaje: String);
begin
     try
        LogMessage( FormatMessage( sMensaje ), EVENTLOG_ERROR_TYPE, 0, 0 );
     except
           LogMessage( FormatMessage( 'Error al Escribir a Bit�cora' ), EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

function TTressNotificadorPruebasService.Inicializa: Boolean;
begin
     //Result := FSentinelRegistry.RenovarAutorizacion( EscribeBitacora, EscribeError );
    Result := TRUE;
end;

function TTressNotificadorPruebasService.TestingProcedure: TStringList;
const
     K_UN_SEGUNDO = 1000;
     K_MEDIO_SEGUNDO = 500;
     VACIO = '';
var
     sError, sSource: string;
     oListaDirectorios : TStringList;
     iContadorDirectorios : integer;
begin
    oListaDirectorios := TStringList.Create;
    iContadorDirectorios := 0;
    sError := VACIO;
    try
         //sSource := FExtraerBuild.ProcesarPropiedadesArchivosTressTest;
            //FindDocs(sSource);
         //FExtraerBuild.GetSubDirectorios(sSource, oListaDirectorios, iContadorDirectorios, sError);
         //FExtraerBuild.GetSubDirTest(sSource,oListaDirectorios, iContadorDirectorios, sError);
     //while not Terminated do
     //begin
          if ( FFecha <> Date ) then
          begin
               //if not Inicializa and ( FIniciando > 0 ) then
               //begin
               //    Sleep( 28 * K_UN_SEGUNDO );
               //     FIniciando := FIniciando - 1;
               //end
               //else
               FFecha := Date;

               sError := ProcesarPropiedadesArchivosTress;
               //if (sError <> VACIO) then
                    //EscribeError(sError);
               //EnviarEmailAdvertencia;
               //EnviarEmailNotificacionPendienteTimbrado;
          end;
          //Sleep( 1 * K_MEDIO_SEGUNDO );
          //ServiceThread.ProcessRequests( False );
     Except
          on E : Exception do
            sError := E.Message;
     end;
     Result := oListaDirectorios;
end;

end.
